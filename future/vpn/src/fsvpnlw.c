/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsvpnlw.c,v 1.59 2017/06/08 11:38:47 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#ifdef SECURITY_KERNEL_MAKE_WANTED
#include "vpnwincs.h"
#include "arsec.h"
#endif
#include "vpninc.h"
#include "secv6.h"
#include "lr.h"
#include "cfa.h"
#include "fssnmp.h"
#include "vpncli.h"
#include "fsvpncli.h"
#include "fsvpnwr.h"
#include "ip6util.h"
#ifdef FLOWMGR_WANTED
#include "flowmgr.h"
#endif

#ifdef IKE_WANTED
extern INT1         nmhSetFsikeRAXAuthMode (INT4 i4SetValFsikeRAXAuthMode);
extern INT1         nmhSetFsikeRACMMode (INT4 i4SetValFsikeRACMMode);
#endif

#ifdef IPSECv4_WANTED
extern VOID         Secv4DeleteSecAssocEntries (VOID);
#endif

#ifdef IPSECv6_WANTED
extern INT1         nmhGetFsipv6SecGlobalStatus (INT4
                                                 *pi4RetValFsVpnGlobalStatus);
extern INT1         nmhSetFsipv6SecGlobalStatus (INT4
                                                 i4SetValFsVpnGlobalStatus);
extern INT1         nmhTestv2Fsipv6SecGlobalStatus (UINT4 *pu4ErrorCode,
                                                    INT4
                                                    i4TestValFsVpnGlobalStatus);

extern INT1 Secv6ConstSecList PROTO ((VOID));
extern INT1 Secv6ConstAccessList PROTO ((VOID));
extern VOID Secv6UpdateSecAssocSize PROTO ((VOID));

#endif

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVpnGlobalStatus
 Input       :  The Indices

                The Object 
                retValFsVpnGlobalStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnGlobalStatus (INT4 *pi4RetValFsVpnGlobalStatus)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    return nmhGetFsipv4SecGlobalStatus (pi4RetValFsVpnGlobalStatus);
#else
    int                 rc;
    tNpwnmhGetFsVpnGlobalStatus lv;

    lv.cmd = NMH_GET_FS_VPN_GLOBAL_STATUS;
    lv.pi4RetValFsVpnGlobalStatus = pi4RetValFsVpnGlobalStatus;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/*************************************************************************
 *  Function    :  nmhGetFsVpnRaServer
 *  Input       :  The Indices
 *                 The Object
 *                 retValFsVpnRaServer
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *                 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 * ***********************************************************************/
INT1
nmhGetFsVpnRaServer (INT4 *pi4RetValFsVpnRaServer)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    *pi4RetValFsVpnRaServer = (INT4) gu4VpnRaServer;
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnRaServer lv;

    lv.cmd = NMH_GET_FS_VPN_RA_SERVER;
    lv.pi4RetValFsVpnRaServer = pi4RetValFsVpnRaServer;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnMaxTunnels
 Input       :  The Indices

                The Object 
                retValFsVpnMaxTunnels
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnMaxTunnels (INT4 *pi4RetValFsVpnMaxTunnels)
{
    *pi4RetValFsVpnMaxTunnels = (INT4) gu4VPNMaxTunnels;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsVpnIpPktsIn
 Input       :  The Indices

                The Object 
                retValFsVpnIpPktsIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIpPktsIn (UINT4 *pu4RetValFsVpnIpPktsIn)
{
    UINT4               u4InPkts = VPN_ZERO;

    *pu4RetValFsVpnIpPktsIn = VPN_ZERO;

#ifndef SECURITY_KERNEL_MAKE_WANTED
    nmhGetFsipv4SecIfInPkts (VPN_ZERO, pu4RetValFsVpnIpPktsIn);

#ifdef IPSECv6_WANTED
    Secv6VpnGetIpsecPktStats (SEC_INBOUND, &u4InPkts);
#endif

    *pu4RetValFsVpnIpPktsIn = *pu4RetValFsVpnIpPktsIn + u4InPkts;
    return (SNMP_SUCCESS);
#else
    int                 rc;

    UNUSED_PARAM (u4InPkts);
    tNpwnmhGetFsVpnIpPktsIn lv;

    lv.cmd = NMH_GET_FS_VPN_IP_PKTS_IN;
    lv.pu4RetValFsVpnIpPktsIn = pu4RetValFsVpnIpPktsIn;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnIpPktsOut
 Input       :  The Indices

                The Object 
                retValFsVpnIpPktsOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIpPktsOut (UINT4 *pu4RetValFsVpnIpPktsOut)
{
    UINT4               u4OutPkts = VPN_ZERO;

    *pu4RetValFsVpnIpPktsOut = VPN_ZERO;
#ifndef SECURITY_KERNEL_MAKE_WANTED
    nmhGetFsipv4SecIfOutPkts (VPN_ZERO, pu4RetValFsVpnIpPktsOut);

#ifdef IPSECv6_WANTED
    Secv6VpnGetIpsecPktStats (SEC_OUTBOUND, &u4OutPkts);
#endif

    *pu4RetValFsVpnIpPktsOut = *pu4RetValFsVpnIpPktsOut + u4OutPkts;
    return (SNMP_SUCCESS);
#else
    int                 rc;
    UNUSED_PARAM (u4OutPkts);
    tNpwnmhGetFsVpnIpPktsOut lv;

    lv.cmd = NMH_GET_FS_VPN_IP_PKTS_OUT;
    lv.pu4RetValFsVpnIpPktsOut = pu4RetValFsVpnIpPktsOut;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnPktsSecured
 Input       :  The Indices

                The Object 
                retValFsVpnPktsSecured
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnPktsSecured (UINT4 *pu4RetValFsVpnPktsSecured)
{
    UINT4               u4SecCount = VPN_ZERO;

    *pu4RetValFsVpnPktsSecured = VPN_ZERO;
#ifndef SECURITY_KERNEL_MAKE_WANTED
    nmhGetFsipv4SecIfPktsApply (VPN_ZERO, pu4RetValFsVpnPktsSecured);

#ifdef IPSECv6_WANTED
    Secv6VpnGetIpsecSecurityStats (SEC_APPLY, &u4SecCount);
#endif

    *pu4RetValFsVpnPktsSecured = *pu4RetValFsVpnPktsSecured + u4SecCount;
    return (SNMP_SUCCESS);
#else
    int                 rc;
    UNUSED_PARAM (u4SecCount);
    tNpwnmhGetFsVpnPktsSecured lv;

    lv.cmd = NMH_GET_FS_VPN_PKTS_SECURED;
    lv.pu4RetValFsVpnPktsSecured = pu4RetValFsVpnPktsSecured;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnPktsDropped
 Input       :  The Indices

                The Object 
                retValFsVpnPktsDropped
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnPktsDropped (UINT4 *pu4RetValFsVpnPktsDropped)
{
    UINT4               u4DropCount = VPN_ZERO;

    *pu4RetValFsVpnPktsDropped = VPN_ZERO;
#ifndef SECURITY_KERNEL_MAKE_WANTED
    nmhGetFsipv4SecIfPktsDiscard (VPN_ZERO, pu4RetValFsVpnPktsDropped);

#ifdef IPSECv6_WANTED
    Secv6VpnGetIpsecSecurityStats (SEC_FILTER, &u4DropCount);
#endif

    *pu4RetValFsVpnPktsDropped = *pu4RetValFsVpnPktsDropped + u4DropCount;
    return (SNMP_SUCCESS);
#else
    int                 rc;
    UNUSED_PARAM (u4DropCount);
    tNpwnmhGetFsVpnPktsDropped lv;

    lv.cmd = NMH_GET_FS_VPN_PKTS_DROPPED;
    lv.pu4RetValFsVpnPktsDropped = pu4RetValFsVpnPktsDropped;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnIkeSAsActive
 Input       :  The Indices

                The Object 
                retValFsVpnIkeSAsActive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIkeSAsActive (UINT4 *pu4RetValFsVpnIkeSAsActive)
{
    *pu4RetValFsVpnIkeSAsActive = VPN_ZERO;
#ifdef IKE_WANTED
    IkeGetActiveSACount (pu4RetValFsVpnIkeSAsActive);
#endif
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsVpnIkeNegotiations
 Input       :  The Indices

                The Object 
                retValFsVpnIkeNegotiations
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIkeNegotiations (UINT4 *pu4RetValFsVpnIkeNegotiations)
{
    *pu4RetValFsVpnIkeNegotiations = VPN_ZERO;
#ifdef IKE_WANTED
    /* Since the statistics is stored as global,index is passed as 0 */
    IkeGetNoOfPhase1SessionsSucc (VPN_ZERO, pu4RetValFsVpnIkeNegotiations);
#endif
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsVpnIkeRekeys
 Input       :  The Indices

                The Object 
                retValFsVpnIkeRekeys
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIkeRekeys (UINT4 *pu4RetValFsVpnIkeRekeys)
{
    *pu4RetValFsVpnIkeRekeys = VPN_ZERO;

#ifdef IKE_WANTED
    /* Since the statistics is stored as global,index is passed as 0 */
    IkeGetNoOfIkePhase1Rekeys (VPN_ZERO, pu4RetValFsVpnIkeRekeys);
#endif
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsVpnIkeNegoFailed
 Input       :  The Indices

                The Object 
                retValFsVpnIkeNegoFailed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIkeNegoFailed (UINT4 *pu4RetValFsVpnIkeNegoFailed)
{
    *pu4RetValFsVpnIkeNegoFailed = VPN_ZERO;

#ifdef IKE_WANTED
    /* Since the statistics is stored as global,index is passed as 0 */
    IkeGetNoOfPhase1SessionFailed (VPN_ZERO, pu4RetValFsVpnIkeNegoFailed);
#endif
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsVpnIPSecSAsActive
 Input       :  The Indices

                The Object 
                retValFsVpnIPSecSAsActive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIPSecSAsActive (UINT4 *pu4RetValFsVpnIPSecSAsActive)
{

#ifndef SECURITY_KERNEL_MAKE_WANTED
    UINT4               u4ActiveSaCnt = VPN_ZERO;
    *pu4RetValFsVpnIPSecSAsActive = VPN_ZERO;

    Secv4GetAssocEntryCount (pu4RetValFsVpnIPSecSAsActive);
#ifdef IPSECv6_WANTED
    Secv6VpnGetIpsecSaEntryCount (&u4ActiveSaCnt);
#endif

    *pu4RetValFsVpnIPSecSAsActive += u4ActiveSaCnt;
    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsVpnIPSecSAsActive lv;
    *pu4RetValFsVpnIPSecSAsActive = VPN_ZERO;

    lv.cmd = NMH_GET_FS_VPN_I_P_SEC_S_AS_ACTIVE;
    lv.pu4RetValFsVpnIPSecSAsActive = pu4RetValFsVpnIPSecSAsActive;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnIPSecNegotiations
 Input       :  The Indices

                The Object 
                retValFsVpnIPSecNegotiations
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIPSecNegotiations (UINT4 *pu4RetValFsVpnIPSecNegotiations)
{
    *pu4RetValFsVpnIPSecNegotiations = VPN_ZERO;

#ifdef IKE_WANTED
    /* Since the statistics is stored as global,index is passed as 0 */
    IkeGetNoOfSessionsSucc (VPN_ZERO, pu4RetValFsVpnIPSecNegotiations);
#endif
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsVpnIPSecNegoFailed
 Input       :  The Indices

                The Object 
                retValFsVpnIPSecNegoFailed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIPSecNegoFailed (UINT4 *pu4RetValFsVpnIPSecNegoFailed)
{
    *pu4RetValFsVpnIPSecNegoFailed = VPN_ZERO;
#ifdef IKE_WANTED
    /* Since the statistics is stored as global,index is passed as 0 */
    IkeGetNoOfSessionsFail (VPN_ZERO, pu4RetValFsVpnIPSecNegoFailed);
#endif
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsVpnTotalRekeys
 Input       :  The Indices

                The Object 
                retValFsVpnTotalRekeys
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnTotalRekeys (UINT4 *pu4RetValFsVpnTotalRekeys)
{
    *pu4RetValFsVpnTotalRekeys = VPN_ZERO;

#ifdef IKE_WANTED
    /* Since the statistics is stored as global,index is passed as 0 */
    IkeGetNoOfIkePhase2Rekeys (VPN_ZERO, pu4RetValFsVpnTotalRekeys);
#endif
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsVpnDummyPktGen
 Input       :  The Indices

                The Object 
                retValFsVpnDummyPktGen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnDummyPktGen (INT4 *pi4RetValFsVpnDummyPktGen)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    return nmhGetFsipv4SecDummyPktGen (pi4RetValFsVpnDummyPktGen);
#else
    int                 rc;
    tNpwnmhGetFsVpnDummyPktGen lv;

    lv.cmd = NMH_GET_FS_VPN_DUMMY_PKT_GEN;
    lv.pi4RetValFsVpnDummyPktGen = pi4RetValFsVpnDummyPktGen;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnDummyPktParam
 Input       :  The Indices

                The Object 
                retValFsVpnDummyPktParam
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnDummyPktParam (INT4 *pi4RetValFsVpnDummyPktParam)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    return nmhGetFsipv4SecDummyPktParam (pi4RetValFsVpnDummyPktParam);
#else
    int                 rc;
    tNpwnmhGetFsVpnDummyPktParam lv;

    lv.cmd = NMH_GET_FS_VPN_DUMMY_PKT_PARAM;
    lv.pi4RetValFsVpnDummyPktParam = pi4RetValFsVpnDummyPktParam;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsIkeTraceOption
 Input       :  The Indices

                The Object
                retValFsIkeTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIkeTraceOption (INT4 *pi4RetValFsIkeTraceOption)
{
#ifdef IKE_WANTED
    if (SNMP_FAILURE == nmhGetFsikeGlobalDebug (pi4RetValFsIkeTraceOption))
    {
        return (SNMP_FAILURE);
    }
#else
    UNUSED_PARAM (pi4RetValFsIkeTraceOption);
#endif
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsIpsecTraceOption
 Input       :  The Indices

                The Object
                retValFsIpsecTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsIpsecTraceOption (INT4 *pi4RetValFsIpsecTraceOption)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    if (SNMP_FAILURE ==
        nmhGetFsipv4SecGlobalDebug (pi4RetValFsIpsecTraceOption))
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsIpsecTraceOption lv;

    lv.cmd = NMH_GET_FS_VPN_IPSEC_TRACE;
    lv.pi4RetValFsIpsecTraceOption = pi4RetValFsIpsecTraceOption;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVpnGlobalStatus
 Input       :  The Indices

                The Object 
                setValFsVpnGlobalStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnGlobalStatus (INT4 i4SetValFsVpnGlobalStatus)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
#ifdef FLOWMGR_WANTED
    UINT4               u4SecProto;
#endif
    UINT4               u4SeqNum = VPN_ZERO;
    INT1                i1RetVal = SNMP_SUCCESS;
    tVpnPolicy         *pVpnPolicy = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    i1RetVal = nmhSetFsipv4SecGlobalStatus (i4SetValFsVpnGlobalStatus);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (i1RetVal != SNMP_SUCCESS)
    {
        return i1RetVal;
    }

#ifdef IPSECv6_WANTED
    i1RetVal = nmhSetFsipv6SecGlobalStatus (i4SetValFsVpnGlobalStatus);
    if (i1RetVal != SNMP_SUCCESS)
    {
        return i1RetVal;
    }
#endif

    if (i4SetValFsVpnGlobalStatus == VPN_ENABLE)
    {
        SLL_SCAN (&VpnPolicyList, pVpnPolicy, tVpnPolicy *)
        {
#ifdef FLOWMGR_WANTED
            if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
            {
                /*Delete all the flows which matches with the active vpn accesslist */
                FlIpsecDelAclRelFlows (pVpnPolicy->u4IfIndex,
                                       pVpnPolicy->LocalProtectNetwork.Ip4Addr,
                                       pVpnPolicy->LocalProtectNetwork.
                                       Ip4Subnet,
                                       pVpnPolicy->RemoteProtectNetwork.Ip4Addr,
                                       pVpnPolicy->RemoteProtectNetwork.
                                       Ip4Subnet, pVpnPolicy->u1VpnMode);
            }
#endif
        }

    }

    else if (i4SetValFsVpnGlobalStatus == VPN_DISABLE)
    {
        SLL_SCAN (&VpnPolicyList, pVpnPolicy, tVpnPolicy *)
        {
            /* Delete all the flows that are already established */
#ifdef FLOWMGR_WANTED
            if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
            {
                u4SecProto = pVpnPolicy->u4VpnSecurityProtocol;
                FlIpsecDelTunnel (pVpnPolicy->LocalTunnTermAddr.Ip4Addr,
                                  pVpnPolicy->RemoteTunnTermAddr.Ip4Addr,
                                  u4SecProto);
            }
#endif
        }
    }
    if (i1RetVal == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnGlobalStatus, u4SeqNum,
                              FALSE, VpnDsLock, VpnDsUnLock, VPN_ZERO,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsVpnGlobalStatus));
    }
    return i1RetVal;
#else
    int                 rc;
    tNpwnmhSetFsVpnGlobalStatus lv;

    lv.cmd = NMH_SET_FS_VPN_GLOBAL_STATUS;
    lv.i4SetValFsVpnGlobalStatus = i4SetValFsVpnGlobalStatus;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
#ifdef NPAPI_WANTED
    /* Set the security throughput meter limit according to the 
     * status of VPN module */
    FsSecNpHwSetRateLimit (ISS_SEC_VPN_MODULE, i4SetValFsVpnGlobalStatus);
#endif
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnRaServer
 Input       :  The Indices

                The Object 
                setValFsVpnRaServer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnRaServer (INT4 i4SetValFsVpnRaServer)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    gu4VpnRaServer = (UINT4) i4SetValFsVpnRaServer;
#ifdef IKE_WANTED
    nmhSetFsikeRAXAuthMode ((UINT1) i4SetValFsVpnRaServer);
    nmhSetFsikeRACMMode ((UINT1) i4SetValFsVpnRaServer);
#endif
    RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnRaServer lv;

    lv.cmd = NMH_SET_FS_VPN_RA_SERVER;
    lv.i4SetValFsVpnRaServer = i4SetValFsVpnRaServer;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnRaServer, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsVpnRaServer));
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnDummyPktGen
 Input       :  The Indices

                The Object 
                setValFsVpnDummyPktGen
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnDummyPktGen (INT4 i4SetValFsVpnDummyPktGen)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
#ifndef SECURITY_KERNEL_MAKE_WANTED
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (nmhSetFsipv4SecDummyPktGen (i4SetValFsVpnDummyPktGen) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnDummyPktGen, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsVpnDummyPktGen));
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhSetFsVpnDummyPktGen lv;

    lv.cmd = NMH_SET_FS_VPN_DUMMY_PKT_GEN;
    lv.i4SetValFsVpnDummyPktGen = i4SetValFsVpnDummyPktGen;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif

}

/****************************************************************************
 Function    :  nmhSetFsVpnDummyPktParam
 Input       :  The Indices

                The Object 
                setValFsVpnDummyPktParam
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnDummyPktParam (INT4 i4SetValFsVpnDummyPktParam)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
#ifndef SECURITY_KERNEL_MAKE_WANTED
    if (nmhSetFsipv4SecDummyPktParam (i4SetValFsVpnDummyPktParam) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnDummyPktParam, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsVpnDummyPktParam));
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhSetFsVpnDummyPktParam lv;

    lv.cmd = NMH_SET_FS_VPN_DUMMY_PKT_PARAM;
    lv.i4SetValFsVpnDummyPktParam = i4SetValFsVpnDummyPktParam;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsIkeTraceOption
 Input       :  The Indices

                The Object
                setValFsIkeTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIkeTraceOption (INT4 i4SetValFsIkeTraceOption)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
#ifdef IKE_WANTED
    if (nmhSetFsikeGlobalDebug (i4SetValFsIkeTraceOption) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (i4SetValFsIkeTraceOption);
#endif
    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsIkeTraceOption, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ZERO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsIkeTraceOption));
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsIpsecTraceOption
 Input       :  The Indices

                The Object
                setValFsIpsecTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsIpsecTraceOption (INT4 i4SetValFsIpsecTraceOption)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifndef SECURITY_KERNEL_MAKE_WANTED
    if (nmhSetFsipv4SecGlobalDebug (i4SetValFsIpsecTraceOption) == SNMP_SUCCESS)
    {
#ifdef IPSECv6_WANTED
        if (nmhSetFsipv6SecGlobalDebug (i4SetValFsIpsecTraceOption) ==
            SNMP_SUCCESS)
        {
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsIpsecTraceOption, u4SeqNum,
                                  FALSE, VpnDsLock, VpnDsUnLock, VPN_ZERO,
                                  SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i",
                              i4SetValFsIpsecTraceOption));
            return SNMP_SUCCESS;
        }
#endif
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsIpsecTraceOption, u4SeqNum,
                              FALSE, VpnDsLock, VpnDsUnLock, VPN_ZERO,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsIpsecTraceOption));
        return (SNMP_SUCCESS);
    }
    else
    {
        return (SNMP_FAILURE);
    }
#else
    int                 rc;
    tNpwnmhSetFsIpsecTraceOption lv;

    lv.cmd = NMH_SET_FS_VPN_IPSEC_TRACE;
    lv.i4SetValFsIpsecTraceOption = i4SetValFsIpsecTraceOption;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVpnGlobalStatus
 Input       :  The Indices

                The Object 
                testValFsVpnGlobalStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnGlobalStatus (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsVpnGlobalStatus)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    INT4                i4RetVal = SNMP_SUCCESS;

    i4RetVal = nmhTestv2Fsipv4SecGlobalStatus (pu4ErrorCode,
                                               i4TestValFsVpnGlobalStatus);
    if (i4RetVal != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

#ifdef IPSECv6_WANTED
    i4RetVal = nmhTestv2Fsipv4SecGlobalStatus (pu4ErrorCode,
                                               i4TestValFsVpnGlobalStatus);
    if (i4RetVal != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnGlobalStatus lv;

    lv.cmd = NMH_TESTV2_FS_VPN_GLOBAL_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValFsVpnGlobalStatus = i4TestValFsVpnGlobalStatus;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnRaServer
 Input       :  The Indices

                The Object 
                testValFsVpnRaServer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnRaServer (UINT4 *pu4ErrorCode, INT4 i4TestValFsVpnRaServer)
{
#ifdef IKE_WANTED
    if (gu4RaRefCount > VPN_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
#endif
#ifndef SECURITY_KERNEL_MAKE_WANTED
    if ((i4TestValFsVpnRaServer != TRUE) && (i4TestValFsVpnRaServer != FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhTestv2FsVpnRaServer lv;

    lv.cmd = NMH_TESTV2_FS_VPN_RA_SERVER;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValFsVpnRaServer = i4TestValFsVpnRaServer;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnDummyPktGen
 Input       :  The Indices

                The Object 
                testValFsVpnDummyPktGen
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnDummyPktGen (UINT4 *pu4ErrorCode, INT4 i4TestValFsVpnDummyPktGen)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED

    if (nmhTestv2Fsipv4SecDummyPktGen (pu4ErrorCode,
                                       i4TestValFsVpnDummyPktGen) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnDummyPktGen lv;

    lv.cmd = NMH_TESTV2_FS_VPN_DUMMY_PKT_GEN;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValFsVpnDummyPktGen = i4TestValFsVpnDummyPktGen;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnDummyPktParam
 Input       :  The Indices

                The Object 
                testValFsVpnDummyPktParam
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnDummyPktParam (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsVpnDummyPktParam)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    if (nmhTestv2Fsipv4SecDummyPktParam (pu4ErrorCode,
                                         i4TestValFsVpnDummyPktParam) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnDummyPktParam lv;

    lv.cmd = NMH_TESTV2_FS_VPN_DUMMY_PKT_PARAM;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValFsVpnDummyPktParam = i4TestValFsVpnDummyPktParam;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsIkeTraceOption
 Input       :  The Indices

                The Object
                testValFsIkeTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIkeTraceOption (UINT4 *pu4ErrorCode, INT4 i4TestValFsIkeTraceOption)
{
    if (i4TestValFsIkeTraceOption < VPN_INVALID)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsIpsecTraceOption
 Input       :  The Indices

                The Object
                testValFsIpsecTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsIpsecTraceOption (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsIpsecTraceOption)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    if (i4TestValFsIpsecTraceOption < VPN_INVALID)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsIpsecTraceOption lv;

    lv.cmd = NMH_TESTV2_FS_VPN_IPSEC_TRACE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4TestValFsIpsecTraceOption = i4TestValFsIpsecTraceOption;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVpnGlobalStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVpnGlobalStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsVpnRaServer
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVpnRaServer (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsVpnDummyPktGen
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVpnDummyPktGen (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsVpnDummyPktParam
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVpnDummyPktParam (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsVpnTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVpnTable
 Input       :  The Indices
                FsVpnPolicyName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVpnTable (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhValidateIndexInstanceFsVpnTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_FS_VPN_TABLE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVpnTable
 Input       :  The Indices
                FsVpnPolicyName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVpnTable (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = (tVpnPolicy *) SLL_FIRST (&VpnPolicyList);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pFsVpnPolicyName->i4_Length = (INT4) STRLEN (pVpnPolicy->au1VpnPolicyName);
    MEMCPY (pFsVpnPolicyName->pu1_OctetList, pVpnPolicy->au1VpnPolicyName,
            pFsVpnPolicyName->i4_Length);
    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFirstIndexFsVpnTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_FS_VPN_TABLE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVpnTable
 Input       :  The Indices
                FsVpnPolicyName
                nextFsVpnPolicyName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsVpnTable (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                           tSNMP_OCTET_STRING_TYPE * pNextFsVpnPolicyName)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;
    tVpnPolicy         *pNextVpnPolicy = NULL;

    SLL_SCAN (&VpnPolicyList, pVpnPolicy, tVpnPolicy *)
    {
        if ((pFsVpnPolicyName->i4_Length ==
             (INT4) STRLEN ((size_t) pVpnPolicy->au1VpnPolicyName)) &&
            (MEMCMP
             (pFsVpnPolicyName->pu1_OctetList, pVpnPolicy->au1VpnPolicyName,
              (size_t) pFsVpnPolicyName->i4_Length) == VPN_ZERO))
        {
            pNextVpnPolicy =
                (tVpnPolicy *) SLL_NEXT (&VpnPolicyList,
                                         (t_SLL_NODE *) pVpnPolicy);
            if (pNextVpnPolicy == NULL)
            {
                break;
            }

            pNextFsVpnPolicyName->i4_Length =
                (INT4) STRLEN (pNextVpnPolicy->au1VpnPolicyName);
            MEMCPY (pNextFsVpnPolicyName->pu1_OctetList,
                    pNextVpnPolicy->au1VpnPolicyName,
                    pNextFsVpnPolicyName->i4_Length);

            return (SNMP_SUCCESS);
        }
    }
    return (SNMP_FAILURE);
#else
    int                 rc;
    tNpwnmhGetNextIndexFsVpnTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_FS_VPN_TABLE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pNextFsVpnPolicyName = pNextFsVpnPolicyName;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVpnPolicyType
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnPolicyType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnPolicyType (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                       INT4 *pi4RetValFsVpnPolicyType)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsVpnPolicyType = (INT4) pVpnPolicy->u4VpnPolicyType;

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsVpnPolicyType lv;

    lv.cmd = NMH_GET_FS_VPN_POLICY_TYPE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnPolicyType = pi4RetValFsVpnPolicyType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnCertAlgoType
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnCertAlgoType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnCertAlgoType (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                         INT4 *pi4RetValFsVpnCertAlgoType)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsVpnCertAlgoType = pVpnPolicy->u1AuthAlgoType;

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsVpnCertAlgoType lv;

    lv.cmd = NMH_GET_FS_VPN_CERT_ALGO_TYPE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnCertAlgoType = pi4RetValFsVpnCertAlgoType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnPolicyPriority
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnPolicyPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnPolicyPriority (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                           INT4 *pi4RetValFsVpnPolicyPriority)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnPolicyPriority = (INT4) pVpnPolicy->u4VpnPolicyPriority;

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsVpnPolicyPriority lv;

    lv.cmd = NMH_GET_FS_VPN_POLICY_PRIORITY;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnPolicyPriority = pi4RetValFsVpnPolicyPriority;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnTunTermAddrType
 Input       :  The Indices
                FsVpnPolicyName
                                                                                               The Object
                retValFsVpnTunTermAddrType
 Output      :  The Get Low Lev Routine Take the Indices &                                     store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnTunTermAddrType (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                            INT4 *pi4RetValFsVpnTunTermAddrType)
{

#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnTunTermAddrType = (INT4) pVpnPolicy->u4TunTermAddrType;

    return (SNMP_SUCCESS);

#else
    int                 rc;
    tNpwnmhGetFsVpnTunTermAddrType lv;

    lv.cmd = NMH_GET_FS_VPN_TUN_TERM_ADDR_TYPE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnTunTermAddrType = pi4RetValFsVpnTunTermAddrType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnLocalTunTermAddr
 Input       :  The Indices
                FsVpnPolicyName
                The Object
                retValFsVpnLocalTunTermAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnLocalTunTermAddr (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsVpnLocalTunTermAddr)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->LocalTunnTermAddr.u4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (pRetValFsVpnLocalTunTermAddr->pu1_OctetList,
                &(pVpnPolicy->LocalTunnTermAddr.uIpAddr.Ip4Addr),
                IPVX_IPV4_ADDR_LEN);
        pRetValFsVpnLocalTunTermAddr->i4_Length = IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        MEMCPY (pRetValFsVpnLocalTunTermAddr->pu1_OctetList,
                &(pVpnPolicy->LocalTunnTermAddr.uIpAddr.Ip6Addr),
                IPVX_IPV6_ADDR_LEN);
        pRetValFsVpnLocalTunTermAddr->i4_Length = IPVX_IPV6_ADDR_LEN;
    }
    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsVpnLocalTunTermAddr lv;

    lv.cmd = NMH_GET_FS_VPN_LOCAL_TUN_TERM_ADDR;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pRetValFsVpnLocalTunTermAddr = pRetValFsVpnLocalTunTermAddr;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnRemoteTunTermAddr
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnRemoteTunTermAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnRemoteTunTermAddr (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsVpnRemoteTunTermAddr)
{

#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->RemoteTunnTermAddr.u4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (pRetValFsVpnRemoteTunTermAddr->pu1_OctetList,
                &(pVpnPolicy->RemoteTunnTermAddr.uIpAddr.Ip4Addr),
                IPVX_IPV4_ADDR_LEN);
        pRetValFsVpnRemoteTunTermAddr->i4_Length = IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        MEMCPY (pRetValFsVpnRemoteTunTermAddr->pu1_OctetList,
                &(pVpnPolicy->RemoteTunnTermAddr.uIpAddr.Ip6Addr),
                IPVX_IPV6_ADDR_LEN);
        pRetValFsVpnRemoteTunTermAddr->i4_Length = IPVX_IPV6_ADDR_LEN;
    }

    return (SNMP_SUCCESS);

#else

    int                 rc;
    tNpwnmhGetFsVpnRemoteTunTermAddr lv;

    lv.cmd = NMH_GET_FS_VPN_REMOTE_TUN_TERM_ADDR;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pRetValFsVpnRemoteTunTermAddr = pRetValFsVpnRemoteTunTermAddr;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnProtectNetworkType
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnProtectNetworkType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnProtectNetworkType (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                               INT4 *pi4RetValFsVpnProtectNetworkType)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED

    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnProtectNetworkType =
        (INT4) pVpnPolicy->LocalProtectNetwork.IpAddr.u4AddrType;
    return (SNMP_SUCCESS);

#else

    int                 rc;
    tNpwnmhGetFsVpnProtectNetworkType lv;

    lv.cmd = NMH_GET_FS_VPN_PROTECT_NETWORK_TYPE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnProtectNetworkType = pi4RetValFsVpnProtectNetworkType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnLocalProtectNetwork
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnLocalProtectNetwork
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnLocalProtectNetwork (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsVpnLocalProtectNetwork)
{

#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->LocalProtectNetwork.IpAddr.u4AddrType
        == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (pRetValFsVpnLocalProtectNetwork->pu1_OctetList,
                &(pVpnPolicy->LocalProtectNetwork.IpAddr.uIpAddr.Ip4Addr),
                IPVX_IPV4_ADDR_LEN);
        pRetValFsVpnLocalProtectNetwork->i4_Length = IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        MEMCPY (pRetValFsVpnLocalProtectNetwork->pu1_OctetList,
                &(pVpnPolicy->LocalProtectNetwork.IpAddr.uIpAddr.Ip6Addr),
                IPVX_IPV6_ADDR_LEN);
        pRetValFsVpnLocalProtectNetwork->i4_Length = IPVX_IPV6_ADDR_LEN;
    }

    return (SNMP_SUCCESS);

#else

    int                 rc;
    tNpwnmhGetFsVpnLocalProtectNetwork lv;

    lv.cmd = NMH_GET_FS_VPN_LOCAL_PROTECT_NETWORK;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pRetValFsVpnLocalProtectNetwork = pRetValFsVpnLocalProtectNetwork;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnLocalProtectNetworkPrefixLen
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnLocalProtectNetworkPrefixLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnLocalProtectNetworkPrefixLen (tSNMP_OCTET_STRING_TYPE *
                                         pFsVpnPolicyName, UINT4
                                         *pu4RetValFsVpnLocalProtectNetworkPrefixLen)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pu4RetValFsVpnLocalProtectNetworkPrefixLen =
        pVpnPolicy->LocalProtectNetwork.u4AddrPrefixLen;

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsVpnLocalProtectNetworkPrefixLen lv;

    lv.cmd = NMH_GET_FS_VPN_LOCAL_PROTECT_NETWORK_PREFIX_LEN;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pu4RetValFsVpnLocalProtectNetworkPrefixLen =
        pu4RetValFsVpnLocalProtectNetworkPrefixLen;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnRemoteProtectNetwork
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnRemoteProtectNetwork
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnRemoteProtectNetwork (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsVpnRemoteProtectNetwork)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED

    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->RemoteProtectNetwork.IpAddr.u4AddrType
        == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (pRetValFsVpnRemoteProtectNetwork->pu1_OctetList,
                &(pVpnPolicy->RemoteProtectNetwork.IpAddr.uIpAddr.Ip4Addr),
                IPVX_IPV4_ADDR_LEN);
        pRetValFsVpnRemoteProtectNetwork->i4_Length = IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        MEMCPY (pRetValFsVpnRemoteProtectNetwork->pu1_OctetList,
                &(pVpnPolicy->RemoteProtectNetwork.IpAddr.uIpAddr.Ip6Addr),
                IPVX_IPV6_ADDR_LEN);
        pRetValFsVpnRemoteProtectNetwork->i4_Length = IPVX_IPV6_ADDR_LEN;
    }

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsVpnRemoteProtectNetwork lv;

    lv.cmd = NMH_GET_FS_VPN_REMOTE_PROTECT_NETWORK;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pRetValFsVpnRemoteProtectNetwork = pRetValFsVpnRemoteProtectNetwork;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnRemoteProtectNetworkPrefixLen
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnRemoteProtectNetworkPrefixLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnRemoteProtectNetworkPrefixLen (tSNMP_OCTET_STRING_TYPE *
                                          pFsVpnPolicyName,
                                          UINT4
                                          *pu4RetValFsVpnRemoteProtectNetworkPrefixLen)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pu4RetValFsVpnRemoteProtectNetworkPrefixLen =
        pVpnPolicy->RemoteProtectNetwork.u4AddrPrefixLen;

    return (SNMP_SUCCESS);

#else
    int                 rc;
    tNpwnmhGetFsVpnRemoteProtectNetworkPrefixLen lv;

    lv.cmd = NMH_GET_FS_VPN_REMOTE_PROTECT_NETWORK_PREFIX_LEN;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pu4RetValFsVpnRemoteProtectNetworkPrefixLen =
        pu4RetValFsVpnRemoteProtectNetworkPrefixLen;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnIkeSrcPortRange
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnIkeSrcPortRange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIkeSrcPortRange (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsVpnIkeSrcPortRange)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    SPRINTF ((CHR1 *) pRetValFsVpnIkeSrcPortRange->pu1_OctetList,
             "%hu%c%hu", pVpnPolicy->LocalProtectNetwork.u2StartPort,
             '-', pVpnPolicy->LocalProtectNetwork.u2EndPort);

    pRetValFsVpnIkeSrcPortRange->i4_Length =
        (INT4) STRLEN (pRetValFsVpnIkeSrcPortRange->pu1_OctetList);

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsVpnIkeSrcPortRange lv;

    lv.cmd = NMH_GET_FS_VPN_IKE_SRC_PORT_RANGE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pRetValFsVpnIkeSrcPortRange = pRetValFsVpnIkeSrcPortRange;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnIkeDstPortRange
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnIkeDstPortRange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIkeDstPortRange (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsVpnIkeDstPortRange)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    SPRINTF ((CHR1 *) pRetValFsVpnIkeDstPortRange->pu1_OctetList,
             "%hu%c%hu", pVpnPolicy->RemoteProtectNetwork.u2StartPort,
             '-', pVpnPolicy->RemoteProtectNetwork.u2EndPort);

    pRetValFsVpnIkeDstPortRange->i4_Length =
        (INT4) STRLEN (pRetValFsVpnIkeDstPortRange->pu1_OctetList);

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsVpnIkeDstPortRange lv;

    lv.cmd = NMH_GET_FS_VPN_IKE_DST_PORT_RANGE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pRetValFsVpnIkeDstPortRange = pRetValFsVpnIkeDstPortRange;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnSecurityProtocol
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnSecurityProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnSecurityProtocol (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                             INT4 *pi4RetValFsVpnSecurityProtocol)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnSecurityProtocol = (INT4) pVpnPolicy->u4VpnSecurityProtocol;

    return (SNMP_SUCCESS);

#else
    int                 rc;
    tNpwnmhGetFsVpnSecurityProtocol lv;

    lv.cmd = NMH_GET_FS_VPN_SECURITY_PROTOCOL;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnSecurityProtocol = pi4RetValFsVpnSecurityProtocol;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnInboundSpi
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnInboundSpi
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnInboundSpi (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                       INT4 *pi4RetValFsVpnInboundSpi)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnInboundSpi = VPN_ZERO;

    if (IS_POLICY_VPN_IPSEC_MANUAL (pVpnPolicy))
    {
        *pi4RetValFsVpnInboundSpi =
            (INT4) pVpnPolicy->uVpnKeyMode.IpsecManualKey.u4VpnAhInboundSpi;
    }
    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsVpnInboundSpi lv;

    lv.cmd = NMH_GET_FS_VPN_INBOUND_SPI;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnInboundSpi = pi4RetValFsVpnInboundSpi;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnOutboundSpi
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnOutboundSpi
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnOutboundSpi (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                        INT4 *pi4RetValFsVpnOutboundSpi)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnOutboundSpi = VPN_ZERO;

    if (IS_POLICY_VPN_IPSEC_MANUAL (pVpnPolicy))
    {
        *pi4RetValFsVpnOutboundSpi =
            (INT4) pVpnPolicy->uVpnKeyMode.IpsecManualKey.u4VpnAhOutboundSpi;
    }

    return (SNMP_SUCCESS);

#else
    int                 rc;
    tNpwnmhGetFsVpnOutboundSpi lv;

    lv.cmd = NMH_GET_FS_VPN_OUTBOUND_SPI;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnOutboundSpi = pi4RetValFsVpnOutboundSpi;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnMode
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnMode (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                 INT4 *pi4RetValFsVpnMode)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnMode = (INT4) pVpnPolicy->u1VpnMode;

    return (SNMP_SUCCESS);

#else
    int                 rc;
    tNpwnmhGetFsVpnMode lv;

    lv.cmd = NMH_GET_FS_VPN_MODE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnMode = pi4RetValFsVpnMode;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnAuthAlgo
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnAuthAlgo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnAuthAlgo (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                     INT4 *pi4RetValFsVpnAuthAlgo)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED

    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnAuthAlgo = VPN_ZERO;

    if (IS_POLICY_VPN_IPSEC_MANUAL (pVpnPolicy))
    {
        *pi4RetValFsVpnAuthAlgo =
            (INT4) pVpnPolicy->uVpnKeyMode.IpsecManualKey.u1VpnAuthAlgo;
    }

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsVpnAuthAlgo lv;

    lv.cmd = NMH_GET_FS_VPN_AUTH_ALGO;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnAuthAlgo = pi4RetValFsVpnAuthAlgo;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnAhKey
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnAhKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnAhKey (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                  tSNMP_OCTET_STRING_TYPE * pRetValFsVpnAhKey)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pRetValFsVpnAhKey->i4_Length = VPN_ZERO;

    if (IS_POLICY_VPN_IPSEC_MANUAL (pVpnPolicy))
    {
        pRetValFsVpnAhKey->i4_Length =
            (INT4) STRLEN (pVpnPolicy->uVpnKeyMode.IpsecManualKey.au1VpnAhKey);
        VPN_MEMCPY (pRetValFsVpnAhKey->pu1_OctetList,
                    pVpnPolicy->uVpnKeyMode.IpsecManualKey.au1VpnAhKey,
                    pRetValFsVpnAhKey->i4_Length);
    }
    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsVpnAhKey lv;

    lv.cmd = NMH_GET_FS_VPN_AH_KEY;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pRetValFsVpnAhKey = pRetValFsVpnAhKey;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnEncrAlgo
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnEncrAlgo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnEncrAlgo (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                     INT4 *pi4RetValFsVpnEncrAlgo)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnEncrAlgo = VPN_ZERO;

    if (IS_POLICY_VPN_IPSEC_MANUAL (pVpnPolicy))
    {
        *pi4RetValFsVpnEncrAlgo =
            (INT4) pVpnPolicy->uVpnKeyMode.IpsecManualKey.u1VpnEncryptionAlgo;
    }

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsVpnEncrAlgo lv;

    lv.cmd = NMH_GET_FS_VPN_ENCR_ALGO;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnEncrAlgo = pi4RetValFsVpnEncrAlgo;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnEspKey
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnEspKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnEspKey (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                   tSNMP_OCTET_STRING_TYPE * pRetValFsVpnEspKey)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pRetValFsVpnEspKey->i4_Length = VPN_ZERO;

    if (IS_POLICY_VPN_IPSEC_MANUAL (pVpnPolicy))
    {
        pRetValFsVpnEspKey->i4_Length =
            (INT4) STRLEN (pVpnPolicy->uVpnKeyMode.IpsecManualKey.au2VpnEspKey);

        MEMCPY (pRetValFsVpnEspKey->pu1_OctetList,
                pVpnPolicy->uVpnKeyMode.IpsecManualKey.au2VpnEspKey,
                pRetValFsVpnEspKey->i4_Length);
    }
    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsVpnEspKey lv;

    lv.cmd = NMH_GET_FS_VPN_ESP_KEY;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pRetValFsVpnEspKey = pRetValFsVpnEspKey;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnAntiReplay
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnAntiReplay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnAntiReplay (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                       INT4 *pi4RetValFsVpnAntiReplay)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnAntiReplay =
        (INT4) VPN_POLICY_ANTI_REPLAY_STATUS (pVpnPolicy);

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsVpnAntiReplay lv;

    lv.cmd = NMH_GET_FS_VPN_ANTI_REPLAY;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnAntiReplay = pi4RetValFsVpnAntiReplay;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnPolicyFlag
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnPolicyFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnPolicyFlag (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                       INT4 *pi4RetValFsVpnPolicyFlag)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnPolicyFlag = (INT4) pVpnPolicy->u4VpnPolicyFlag;

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsVpnPolicyFlag lv;

    lv.cmd = NMH_GET_FS_VPN_POLICY_FLAG;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnPolicyFlag = pi4RetValFsVpnPolicyFlag;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnProtocol
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnProtocol (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                     INT4 *pi4RetValFsVpnProtocol)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnProtocol = (INT4) pVpnPolicy->u4VpnProtocol;

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnProtocol lv;

    lv.cmd = NMH_GET_FS_VPN_PROTOCOL;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnProtocol = pi4RetValFsVpnProtocol;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnPolicyIntfIndex
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnPolicyIntfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnPolicyIntfIndex (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                            INT4 *pi4RetValFsVpnPolicyIntfIndex)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnPolicyIntfIndex = (INT4) pVpnPolicy->u4IfIndex;

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnPolicyIntfIndex lv;

    lv.cmd = NMH_GET_FS_VPN_POLICY_INTF_INDEX;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnPolicyIntfIndex = pi4RetValFsVpnPolicyIntfIndex;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnIkePhase1HashAlgo
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnIkePhase1HashAlgo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIkePhase1HashAlgo (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                              INT4 *pi4RetValFsVpnIkePhase1HashAlgo)
{

#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnIkePhase1HashAlgo = VPN_ZERO;

    if (IS_POLICY_VPN_IKE (pVpnPolicy))
    {
        *pi4RetValFsVpnIkePhase1HashAlgo =
            (INT4) pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4HashAlgo;
    }
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnIkePhase1HashAlgo lv;

    lv.cmd = NMH_GET_FS_VPN_IKE_PHASE1_HASH_ALGO;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnIkePhase1HashAlgo = pi4RetValFsVpnIkePhase1HashAlgo;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnIkePhase1EncryptionAlgo
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnIkePhase1EncryptionAlgo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIkePhase1EncryptionAlgo (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                    INT4 *pi4RetValFsVpnIkePhase1EncryptionAlgo)
{

#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnIkePhase1EncryptionAlgo = VPN_ZERO;

    if (IS_POLICY_VPN_IKE (pVpnPolicy))
    {
        *pi4RetValFsVpnIkePhase1EncryptionAlgo =
            (INT4) pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4EncryptionAlgo;
    }

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnIkePhase1EncryptionAlgo lv;

    lv.cmd = NMH_GET_FS_VPN_IKE_PHASE1_ENCRYPTION_ALGO;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnIkePhase1EncryptionAlgo =
        pi4RetValFsVpnIkePhase1EncryptionAlgo;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnIkePhase1DHGroup
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnIkePhase1DHGroup
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIkePhase1DHGroup (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                             INT4 *pi4RetValFsVpnIkePhase1DHGroup)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnIkePhase1DHGroup = VPN_ZERO;

    if (IS_POLICY_VPN_IKE (pVpnPolicy))
    {
        *pi4RetValFsVpnIkePhase1DHGroup =
            (INT4) pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4DHGroup;
    }
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnIkePhase1DHGroup lv;

    lv.cmd = NMH_GET_FS_VPN_IKE_PHASE1_D_H_GROUP;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnIkePhase1DHGroup = pi4RetValFsVpnIkePhase1DHGroup;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnIkePhase1LocalIdType
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnIkePhase1LocalIdType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIkePhase1LocalIdType (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                 INT4 *pi4RetValFsVpnIkePhase1LocalIdType)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (IS_POLICY_VPN_IKE (pVpnPolicy))
    {
        *pi4RetValFsVpnIkePhase1LocalIdType =
            pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyLocalID.i2IdType;
    }
    else
    {
        *pi4RetValFsVpnIkePhase1LocalIdType = VPN_ZERO;
    }

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnIkePhase1LocalIdType lv;

    lv.cmd = NMH_GET_FS_VPN_IKE_PHASE1_LOCAL_ID_TYPE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnIkePhase1LocalIdType = pi4RetValFsVpnIkePhase1LocalIdType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnIkePhase1LocalIdValue
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnIkePhase1LocalIdValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIkePhase1LocalIdValue (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsVpnIkePhase1LocalIdValue)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;
    tIkeUserID         *pLocalID = NULL;
    UINT4               u4TempIpAddr = VPN_ZERO;
    UINT1               au1TempIpAddr[VPN_MAX_ROW_LEN] = { VPN_ZERO };

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (!IS_POLICY_VPN_IKE (pVpnPolicy))
    {
        pRetValFsVpnIkePhase1LocalIdValue->i4_Length = VPN_ZERO;
        return SNMP_SUCCESS;
    }

    pLocalID = &(pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyLocalID);
    pRetValFsVpnIkePhase1LocalIdValue->i4_Length = pLocalID->u2Length;

    switch (pLocalID->i2IdType)
    {
        case VPN_ID_TYPE_IPV4:

            MEMSET (au1TempIpAddr, VPN_ZERO, sizeof (au1TempIpAddr));
            u4TempIpAddr = pLocalID->uID.Ip4Addr;
            VPN_INET_NTOA (au1TempIpAddr, u4TempIpAddr);
            MEMCPY (pRetValFsVpnIkePhase1LocalIdValue->pu1_OctetList,
                    au1TempIpAddr,
                    pRetValFsVpnIkePhase1LocalIdValue->i4_Length);
            break;

        case VPN_ID_TYPE_EMAIL:
            STRCPY (pRetValFsVpnIkePhase1LocalIdValue->pu1_OctetList,
                    pLocalID->uID.au1Email);
            break;

        case VPN_ID_TYPE_FQDN:
            STRCPY (pRetValFsVpnIkePhase1LocalIdValue->pu1_OctetList,
                    pLocalID->uID.au1Fqdn);
            break;

        case VPN_ID_TYPE_IPV6:
            VPN_INET6_NTOA ((CHR1 *)
                            pRetValFsVpnIkePhase1LocalIdValue->pu1_OctetList,
                            pLocalID->uID.Ip6Addr);
            pRetValFsVpnIkePhase1LocalIdValue->i4_Length =
                (INT4) STRLEN (pRetValFsVpnIkePhase1LocalIdValue->
                               pu1_OctetList);
            break;

        case VPN_ID_TYPE_DN:
            STRCPY (pRetValFsVpnIkePhase1LocalIdValue->pu1_OctetList,
                    pLocalID->uID.au1Dn);
            break;

        case VPN_ID_TYPE_KEYID:
            STRCPY (pRetValFsVpnIkePhase1LocalIdValue->pu1_OctetList,
                    pLocalID->uID.au1KeyId);
            break;

        default:
            pRetValFsVpnIkePhase1LocalIdValue->i4_Length = VPN_ZERO;
            break;
    }                            /* switch case */
    return SNMP_SUCCESS;

#else
    int                 rc;
    tNpwnmhGetFsVpnIkePhase1LocalIdValue lv;

    lv.cmd = NMH_GET_FS_VPN_IKE_PHASE1_LOCAL_ID_VALUE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pRetValFsVpnIkePhase1LocalIdValue = pRetValFsVpnIkePhase1LocalIdValue;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnIkePhase1PeerIdType
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnIkePhase1PeerIdType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIkePhase1PeerIdType (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                INT4 *pi4RetValFsVpnIkePhase1PeerIdType)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnIkePhase1PeerIdType = VPN_ZERO;

    if (IS_POLICY_VPN_IKE (pVpnPolicy))
    {
        *pi4RetValFsVpnIkePhase1PeerIdType =
            pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.i2IdType;
    }

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnIkePhase1PeerIdType lv;

    lv.cmd = NMH_GET_FS_VPN_IKE_PHASE1_PEER_ID_TYPE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnIkePhase1PeerIdType = pi4RetValFsVpnIkePhase1PeerIdType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnIkePhase1PeerIdValue
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnIkePhase1PeerIdValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIkePhase1PeerIdValue (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsVpnIkePhase1PeerIdValue)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;
    UINT4               u4TempIpAddr = VPN_ZERO;
    UINT1               au1TempIpAddr[VPN_MAX_ROW_LEN] = { VPN_ZERO };
    tIkeUserID         *pPeerID = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pRetValFsVpnIkePhase1PeerIdValue->i4_Length = VPN_ZERO;

    if (IS_POLICY_VPN_IKE (pVpnPolicy))
    {
        pPeerID = &(pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID);
        pRetValFsVpnIkePhase1PeerIdValue->i4_Length = pPeerID->u2Length;

        switch (pPeerID->i2IdType)
        {
            case VPN_ID_TYPE_IPV4:

                MEMSET (au1TempIpAddr, VPN_ZERO, sizeof (au1TempIpAddr));
                u4TempIpAddr = pPeerID->uID.Ip4Addr;

                VPN_INET_NTOA (au1TempIpAddr, u4TempIpAddr);
                MEMCPY (pRetValFsVpnIkePhase1PeerIdValue->pu1_OctetList,
                        au1TempIpAddr,
                        pRetValFsVpnIkePhase1PeerIdValue->i4_Length);
                break;

            case VPN_ID_TYPE_EMAIL:
                STRCPY (pRetValFsVpnIkePhase1PeerIdValue->pu1_OctetList,
                        pPeerID->uID.au1Email);
                break;

            case VPN_ID_TYPE_FQDN:
                STRCPY (pRetValFsVpnIkePhase1PeerIdValue->pu1_OctetList,
                        pPeerID->uID.au1Fqdn);
                break;

            case VPN_ID_TYPE_IPV6:
                VPN_INET6_NTOA ((CHR1 *)
                                pRetValFsVpnIkePhase1PeerIdValue->pu1_OctetList,
                                pPeerID->uID.Ip6Addr);
                pRetValFsVpnIkePhase1PeerIdValue->i4_Length =
                    (INT4) STRLEN (pRetValFsVpnIkePhase1PeerIdValue->
                                   pu1_OctetList);
                break;

            case VPN_ID_TYPE_DN:
                STRCPY (pRetValFsVpnIkePhase1PeerIdValue->pu1_OctetList,
                        pPeerID->uID.au1Dn);
                break;

            case VPN_ID_TYPE_KEYID:
                STRCPY (pRetValFsVpnIkePhase1PeerIdValue->pu1_OctetList,
                        pPeerID->uID.au1KeyId);
                break;
            default:
                pRetValFsVpnIkePhase1PeerIdValue->i4_Length = VPN_ZERO;
                break;
        }                        /* switch case */
    }                            /* is ike */
    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsVpnIkePhase1PeerIdValue lv;

    lv.cmd = NMH_GET_FS_VPN_IKE_PHASE1_PEER_ID_VALUE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pRetValFsVpnIkePhase1PeerIdValue = pRetValFsVpnIkePhase1PeerIdValue;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnIkePhase1LifeTimeType
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnIkePhase1LifeTimeType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIkePhase1LifeTimeType (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                  INT4 *pi4RetValFsVpnIkePhase1LifeTimeType)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED

    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnIkePhase1LifeTimeType = VPN_ZERO;

    if (IS_POLICY_VPN_IKE (pVpnPolicy))
    {
        *pi4RetValFsVpnIkePhase1LifeTimeType =
            (INT4) pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4LifeTimeType;
    }
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnIkePhase1LifeTimeType lv;

    lv.cmd = NMH_GET_FS_VPN_IKE_PHASE1_LIFE_TIME_TYPE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnIkePhase1LifeTimeType =
        pi4RetValFsVpnIkePhase1LifeTimeType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnIkePhase1LifeTime
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnIkePhase1LifeTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIkePhase1LifeTime (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                              INT4 *pi4RetValFsVpnIkePhase1LifeTime)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnIkePhase1LifeTime = VPN_ZERO;

    if (IS_POLICY_VPN_IKE (pVpnPolicy))
    {
        *pi4RetValFsVpnIkePhase1LifeTime =
            (INT4) pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4LifeTime;
    }

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnIkePhase1LifeTime lv;

    lv.cmd = NMH_GET_FS_VPN_IKE_PHASE1_LIFE_TIME;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnIkePhase1LifeTime = pi4RetValFsVpnIkePhase1LifeTime;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnIkePhase1Mode
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnIkePhase1Mode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIkePhase1Mode (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                          INT4 *pi4RetValFsVpnIkePhase1Mode)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnIkePhase1Mode = VPN_ZERO;

    if (IS_POLICY_VPN_IKE (pVpnPolicy))
    {
        *pi4RetValFsVpnIkePhase1Mode =
            (INT4) pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4Mode;
    }
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnIkePhase1Mode lv;

    lv.cmd = NMH_GET_FS_VPN_IKE_PHASE1_MODE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnIkePhase1Mode = pi4RetValFsVpnIkePhase1Mode;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnIkePhase2AuthAlgo
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnIkePhase2AuthAlgo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIkePhase2AuthAlgo (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                              INT4 *pi4RetValFsVpnIkePhase2AuthAlgo)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnIkePhase2AuthAlgo = VPN_ZERO;

    if (IS_POLICY_VPN_IKE (pVpnPolicy))
    {
        *pi4RetValFsVpnIkePhase2AuthAlgo =
            pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u1AuthAlgo;
    }
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnIkePhase2AuthAlgo lv;

    lv.cmd = NMH_GET_FS_VPN_IKE_PHASE2_AUTH_ALGO;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnIkePhase2AuthAlgo = pi4RetValFsVpnIkePhase2AuthAlgo;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnIkePhase2EspEncryptionAlgo
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnIkePhase2EspEncryptionAlgo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIkePhase2EspEncryptionAlgo (tSNMP_OCTET_STRING_TYPE *
                                       pFsVpnPolicyName,
                                       INT4
                                       *pi4RetValFsVpnIkePhase2EspEncryptionAlgo)
{

#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnIkePhase2EspEncryptionAlgo = VPN_ZERO;

    if (IS_POLICY_VPN_IKE (pVpnPolicy))
    {
        *pi4RetValFsVpnIkePhase2EspEncryptionAlgo =
            pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u1EncryptionAlgo;
    }

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnIkePhase2EspEncryptionAlgo lv;

    lv.cmd = NMH_GET_FS_VPN_IKE_PHASE2_ESP_ENCRYPTION_ALGO;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnIkePhase2EspEncryptionAlgo =
        pi4RetValFsVpnIkePhase2EspEncryptionAlgo;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnIkePhase2LifeTimeType
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnIkePhase2LifeTimeType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIkePhase2LifeTimeType (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                  INT4 *pi4RetValFsVpnIkePhase2LifeTimeType)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnIkePhase2LifeTimeType = VPN_ZERO;

    if (IS_POLICY_VPN_IKE (pVpnPolicy))
    {
        *pi4RetValFsVpnIkePhase2LifeTimeType =
            pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u1LifeTimeType;
    }

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnIkePhase2LifeTimeType lv;

    lv.cmd = NMH_GET_FS_VPN_IKE_PHASE2_LIFE_TIME_TYPE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnIkePhase2LifeTimeType =
        pi4RetValFsVpnIkePhase2LifeTimeType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnIkePhase2LifeTime
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnIkePhase2LifeTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIkePhase2LifeTime (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                              INT4 *pi4RetValFsVpnIkePhase2LifeTime)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnIkePhase2LifeTime = VPN_ZERO;

    if (IS_POLICY_VPN_IKE (pVpnPolicy))
    {
        *pi4RetValFsVpnIkePhase2LifeTime =
            (INT4) pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u4LifeTime;
    }

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnIkePhase2LifeTime lv;

    lv.cmd = NMH_GET_FS_VPN_IKE_PHASE2_LIFE_TIME;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnIkePhase2LifeTime = pi4RetValFsVpnIkePhase2LifeTime;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnIkePhase2DHGroup
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnIkePhase2DHGroup
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIkePhase2DHGroup (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                             INT4 *pi4RetValFsVpnIkePhase2DHGroup)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnIkePhase2DHGroup = VPN_ZERO;

    if (IS_POLICY_VPN_IKE (pVpnPolicy))
    {
        *pi4RetValFsVpnIkePhase2DHGroup =
            pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u1DHGroup;
    }

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnIkePhase2DHGroup lv;

    lv.cmd = NMH_GET_FS_VPN_IKE_PHASE2_D_H_GROUP;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnIkePhase2DHGroup = pi4RetValFsVpnIkePhase2DHGroup;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnIkeVersion
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnIkeVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnIkeVersion (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                       INT4 *pi4RetValFsVpnIkeVersion)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnIkeVersion = (INT4) pVpnPolicy->u1VpnIkeVer;
    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsVpnIkeVersion lv;

    lv.cmd = NMH_GET_FS_VPN_IKE_VERSION;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnIkeVersion = pi4RetValFsVpnIkeVersion;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnPolicyRowStatus
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                retValFsVpnPolicyRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnPolicyRowStatus (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                            INT4 *pi4RetValFsVpnPolicyRowStatus)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnPolicyRowStatus = (INT4) pVpnPolicy->u1VpnPolicyRowStatus;
    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsVpnPolicyRowStatus lv;

    lv.cmd = NMH_GET_FS_VPN_POLICY_ROW_STATUS;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pi4RetValFsVpnPolicyRowStatus = pi4RetValFsVpnPolicyRowStatus;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVpnPolicyType
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnPolicyType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnPolicyType (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                       INT4 i4SetValFsVpnPolicyType)
{
    tVpnPolicy         *pVpnPolicy = NULL;
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pVpnPolicy->u4VpnPolicyType = (UINT4) i4SetValFsVpnPolicyType;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnPolicyType lv;

    lv.cmd = NMH_SET_FS_VPN_POLICY_TYPE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnPolicyType = i4SetValFsVpnPolicyType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnPolicyType, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnPolicyType));
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnPolicyPriority
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnPolicyPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnPolicyPriority (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                           INT4 i4SetValFsVpnPolicyPriority)
{
    tVpnPolicy         *pVpnPolicy = NULL;
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pVpnPolicy->u4VpnPolicyPriority = (UINT4) i4SetValFsVpnPolicyPriority;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnPolicyPriority lv;

    lv.cmd = NMH_SET_FS_VPN_POLICY_PRIORITY;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnPolicyPriority = i4SetValFsVpnPolicyPriority;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnPolicyPriority, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnPolicyPriority));
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnTunTermAddrType
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnTunTermAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnTunTermAddrType (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                            INT4 i4SetValFsVpnTunTermAddrType)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    MEMSET (&(pVpnPolicy->LocalTunnTermAddr), VPN_ZERO, sizeof (tVpnIpAddr));
    MEMSET (&(pVpnPolicy->RemoteTunnTermAddr), VPN_ZERO, sizeof (tVpnIpAddr));
    pVpnPolicy->u4TunTermAddrType = (UINT4) i4SetValFsVpnTunTermAddrType;
    pVpnPolicy->LocalTunnTermAddr.u4AddrType =
        (UINT4) i4SetValFsVpnTunTermAddrType;
    pVpnPolicy->RemoteTunnTermAddr.u4AddrType =
        (UINT4) i4SetValFsVpnTunTermAddrType;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnTunTermAddrType lv;

    lv.cmd = NMH_SET_FS_VPN_TUN_TERM_ADDR_TYPE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnTunTermAddrType = i4SetValFsVpnTunTermAddrType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnTunTermAddrType, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnTunTermAddrType));
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnLocalTunTermAddr
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnLocalTunTermAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnLocalTunTermAddr (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsVpnLocalTunTermAddr)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u4TunTermAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&(pVpnPolicy->LocalTunnTermAddr.uIpAddr.Ip4Addr),
                pSetValFsVpnLocalTunTermAddr->pu1_OctetList,
                pSetValFsVpnLocalTunTermAddr->i4_Length);
        pVpnPolicy->LocalTunnTermAddr.u4AddrType = IPVX_ADDR_FMLY_IPV4;
    }
    else
    {
        MEMCPY (&(pVpnPolicy->LocalTunnTermAddr.uIpAddr.Ip6Addr),
                pSetValFsVpnLocalTunTermAddr->pu1_OctetList,
                pSetValFsVpnLocalTunTermAddr->i4_Length);
        pVpnPolicy->LocalTunnTermAddr.u4AddrType = IPVX_ADDR_FMLY_IPV6;
    }

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnLocalTunTermAddr lv;

    lv.cmd = NMH_SET_FS_VPN_LOCAL_TUN_TERM_ADDR;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pSetValFsVpnLocalTunTermAddr = pSetValFsVpnLocalTunTermAddr;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnLocalTunTermAddr, u4SeqNum,
                          FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s", pFsVpnPolicyName,
                      pSetValFsVpnLocalTunTermAddr));
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnRemoteTunTermAddr
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnRemoteTunTermAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnRemoteTunTermAddr (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValFsVpnRemoteTunTermAddr)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u4TunTermAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&(pVpnPolicy->RemoteTunnTermAddr.uIpAddr.Ip4Addr),
                pSetValFsVpnRemoteTunTermAddr->pu1_OctetList,
                pSetValFsVpnRemoteTunTermAddr->i4_Length);
        pVpnPolicy->RemoteTunnTermAddr.u4AddrType = IPVX_ADDR_FMLY_IPV4;
    }
    else
    {
        MEMCPY (&(pVpnPolicy->RemoteTunnTermAddr.uIpAddr.Ip6Addr),
                pSetValFsVpnRemoteTunTermAddr->pu1_OctetList,
                pSetValFsVpnRemoteTunTermAddr->i4_Length);
        pVpnPolicy->RemoteTunnTermAddr.u4AddrType = IPVX_ADDR_FMLY_IPV6;
    }

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnRemoteTunTermAddr lv;

    lv.cmd = NMH_SET_FS_VPN_REMOTE_TUN_TERM_ADDR;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pSetValFsVpnRemoteTunTermAddr = pSetValFsVpnRemoteTunTermAddr;
    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnRemoteTunTermAddr, u4SeqNum,
                          FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s", pFsVpnPolicyName,
                      pSetValFsVpnRemoteTunTermAddr));
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnCertAlgoType
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnCertAlgoType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnCertAlgoType (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                         INT4 i4SetValFsVpnCertAlgoType)
{
    tVpnPolicy         *pVpnPolicy = NULL;
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }
    pVpnPolicy->u1AuthAlgoType = (UINT1) i4SetValFsVpnCertAlgoType;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnCertAlgoType lv;

    lv.cmd = NMH_SET_FS_VPN_CERT_ALGO_TYPE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnCertAlgoType = i4SetValFsVpnCertAlgoType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnCertAlgoType, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnCertAlgoType));
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnProtectNetworkType
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnProtectNetworkType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnProtectNetworkType (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                               INT4 i4SetValFsVpnProtectNetworkType)
{

    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    MEMSET (&(pVpnPolicy->LocalProtectNetwork), VPN_ZERO, sizeof (tVpnIpAddr));
    MEMSET (&(pVpnPolicy->RemoteProtectNetwork), VPN_ZERO, sizeof (tVpnIpAddr));
    pVpnPolicy->u4ProtectNetType = (UINT4) i4SetValFsVpnProtectNetworkType;
    pVpnPolicy->LocalProtectNetwork.IpAddr.u4AddrType =
        (UINT4) i4SetValFsVpnProtectNetworkType;
    pVpnPolicy->RemoteProtectNetwork.IpAddr.u4AddrType =
        (UINT4) i4SetValFsVpnProtectNetworkType;

#ifdef SECURITY_KERNEL_MAKE_WANTED

    int                 rc;
    tNpwnmhSetFsVpnProtectNetworkType lv;

    lv.cmd = NMH_SET_FS_VPN_PROTECT_NETWORK_TYPE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnProtectNetworkType = i4SetValFsVpnProtectNetworkType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnProtectNetworkType, u4SeqNum,
                          FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnProtectNetworkType));
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnLocalProtectNetwork
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnLocalProtectNetwork
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnLocalProtectNetwork (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValFsVpnLocalProtectNetwork)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u4ProtectNetType == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&(pVpnPolicy->LocalProtectNetwork.IpAddr.uIpAddr.Ip4Addr),
                pSetValFsVpnLocalProtectNetwork->pu1_OctetList,
                pSetValFsVpnLocalProtectNetwork->i4_Length);
        pVpnPolicy->LocalProtectNetwork.IpAddr.u4AddrType = IPVX_ADDR_FMLY_IPV4;

    }
    else
    {
        MEMCPY (&(pVpnPolicy->LocalProtectNetwork.IpAddr.uIpAddr.Ip6Addr),
                pSetValFsVpnLocalProtectNetwork->pu1_OctetList,
                pSetValFsVpnLocalProtectNetwork->i4_Length);
        pVpnPolicy->LocalProtectNetwork.IpAddr.u4AddrType = IPVX_ADDR_FMLY_IPV6;
    }

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnLocalProtectNetwork lv;

    lv.cmd = NMH_SET_FS_VPN_LOCAL_PROTECT_NETWORK;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pSetValFsVpnLocalProtectNetwork = pSetValFsVpnLocalProtectNetwork;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnLocalProtectNetwork, u4SeqNum,
                          FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s", pFsVpnPolicyName,
                      pSetValFsVpnLocalProtectNetwork));
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnLocalProtectNetworkPrefixLen
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnLocalProtectNetworkPrefixLen
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnLocalProtectNetworkPrefixLen (tSNMP_OCTET_STRING_TYPE *
                                         pFsVpnPolicyName,
                                         UINT4
                                         u4SetValFsVpnLocalProtectNetworkPrefixLen)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pVpnPolicy->LocalProtectNetwork.u4AddrPrefixLen =
        u4SetValFsVpnLocalProtectNetworkPrefixLen;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnLocalProtectNetworkPrefixLen lv;

    lv.cmd = NMH_SET_FS_VPN_LOCAL_PROTECT_NETWORK_PREFIX_LEN;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.u4SetValFsVpnLocalProtectNetworkPrefixLen =
        u4SetValFsVpnLocalProtectNetworkPrefixLen;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnLocalProtectNetworkPrefixLen,
                          u4SeqNum, FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE,
                          SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %p", pFsVpnPolicyName,
                      u4SetValFsVpnLocalProtectNetworkPrefixLen));
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnRemoteProtectNetwork
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnRemoteProtectNetwork
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnRemoteProtectNetwork (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValFsVpnRemoteProtectNetwork)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u4ProtectNetType == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&(pVpnPolicy->RemoteProtectNetwork.IpAddr.uIpAddr.Ip4Addr),
                pSetValFsVpnRemoteProtectNetwork->pu1_OctetList,
                pSetValFsVpnRemoteProtectNetwork->i4_Length);
        pVpnPolicy->RemoteProtectNetwork.IpAddr.u4AddrType
            = IPVX_ADDR_FMLY_IPV4;
    }
    else
    {
        MEMCPY (&(pVpnPolicy->RemoteProtectNetwork.IpAddr.uIpAddr.Ip6Addr),
                pSetValFsVpnRemoteProtectNetwork->pu1_OctetList,
                pSetValFsVpnRemoteProtectNetwork->i4_Length);
        pVpnPolicy->RemoteProtectNetwork.IpAddr.u4AddrType
            = IPVX_ADDR_FMLY_IPV6;
    }

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnRemoteProtectNetwork lv;
    lv.cmd = NMH_SET_FS_VPN_REMOTE_PROTECT_NETWORK;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pSetValFsVpnRemoteProtectNetwork = pSetValFsVpnRemoteProtectNetwork;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnRemoteProtectNetwork, u4SeqNum,
                          FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s", pFsVpnPolicyName,
                      pSetValFsVpnRemoteProtectNetwork));
    return (SNMP_SUCCESS);
#endif

}

/****************************************************************************
 Function    :  nmhSetFsVpnRemoteProtectNetworkPrefixLen
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnRemoteProtectNetworkPrefixLen
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnRemoteProtectNetworkPrefixLen (tSNMP_OCTET_STRING_TYPE *
                                          pFsVpnPolicyName,
                                          UINT4
                                          u4SetValFsVpnRemoteProtectNetworkPrefixLen)
{
    tVpnPolicy         *pVpnPolicy = NULL;
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pVpnPolicy->RemoteProtectNetwork.u4AddrPrefixLen =
        u4SetValFsVpnRemoteProtectNetworkPrefixLen;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnRemoteProtectNetworkPrefixLen lv;

    lv.cmd = NMH_SET_FS_VPN_REMOTE_PROTECT_NETWORK_PREFIX_LEN;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.u4SetValFsVpnRemoteProtectNetworkPrefixLen =
        u4SetValFsVpnRemoteProtectNetworkPrefixLen;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnRemoteProtectNetworkPrefixLen,
                          u4SeqNum, FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE,
                          SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %p", pFsVpnPolicyName,
                      u4SetValFsVpnRemoteProtectNetworkPrefixLen));
    return (SNMP_SUCCESS);
#endif

}

/****************************************************************************
 Function    :  nmhSetFsVpnIkeSrcPortRange
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnIkeSrcPortRange
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnIkeSrcPortRange (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsVpnIkeSrcPortRange)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;
    INT4                i4Count = VPN_ZERO;
    UINT1              *pu1Tmp = NULL;
    UINT1               au1TmpArr[VPN_ARR_MAX] = { VPN_ZERO };

    MEMSET (au1TmpArr, VPN_ZERO, VPN_VAL_11);

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pu1Tmp = pSetValFsVpnIkeSrcPortRange->pu1_OctetList;

    if (STRLEN (pu1Tmp) == VPN_ZERO)
    {
        return SNMP_SUCCESS;
    }

    while (*pu1Tmp != '\0')
    {
        if (*pu1Tmp != '-')
        {
            au1TmpArr[i4Count] = *pu1Tmp;
            pu1Tmp++;
            i4Count++;
        }
        else
        {
            au1TmpArr[i4Count] = '\0';
            pVpnPolicy->LocalProtectNetwork.u2StartPort =
                (UINT2) ATOI ((const char *) au1TmpArr);
            pu1Tmp++;
            i4Count = VPN_ZERO;
        }
    }

    au1TmpArr[i4Count] = '\0';
    pVpnPolicy->LocalProtectNetwork.u2EndPort =
        (UINT2) ATOI ((const char *) au1TmpArr);

    if (MEMCMP (pSetValFsVpnIkeSrcPortRange->pu1_OctetList, au1TmpArr,
                STRLEN (au1TmpArr)) == VPN_ZERO)
    {
        pVpnPolicy->LocalProtectNetwork.u2StartPort =
            (UINT2) ATOI ((const char *) au1TmpArr);
    }

#ifndef SECURITY_KERNEL_MAKE_WANTED
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnIkeSrcPortRange, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s", pFsVpnPolicyName,
                      pSetValFsVpnIkeSrcPortRange));
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhSetFsVpnIkeSrcPortRange lv;

    lv.cmd = NMH_SET_FS_VPN_IKE_SRC_PORT_RANGE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pSetValFsVpnIkeSrcPortRange = pSetValFsVpnIkeSrcPortRange;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnIkeDstPortRange
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnIkeDstPortRange
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnIkeDstPortRange (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsVpnIkeDstPortRange)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;
    INT4                i4Count = VPN_ZERO;
    UINT1              *pu1Tmp = NULL;
    UINT1               au1TmpArr[VPN_ARR_MAX] = { VPN_ZERO };

    MEMSET (au1TmpArr, VPN_ZERO, VPN_VAL_11);

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pu1Tmp = pSetValFsVpnIkeDstPortRange->pu1_OctetList;

    if (STRLEN (pu1Tmp) == VPN_ZERO)
    {
        return SNMP_SUCCESS;
    }

    while (*pu1Tmp != '\0')
    {
        if (*pu1Tmp != '-')
        {
            au1TmpArr[i4Count] = *pu1Tmp;
            pu1Tmp++;
            i4Count++;
        }
        else
        {
            au1TmpArr[i4Count] = '\0';
            pVpnPolicy->RemoteProtectNetwork.u2StartPort =
                (UINT2) ATOI ((const char *) au1TmpArr);
            pu1Tmp++;
            i4Count = VPN_ZERO;
        }
    }

    au1TmpArr[i4Count] = '\0';
    pVpnPolicy->RemoteProtectNetwork.u2EndPort =
        (UINT2) ATOI ((const char *) au1TmpArr);

    if (MEMCMP (pSetValFsVpnIkeDstPortRange->pu1_OctetList, au1TmpArr,
                STRLEN (au1TmpArr)) == VPN_ZERO)
    {
        pVpnPolicy->RemoteProtectNetwork.u2StartPort =
            (UINT2) ATOI ((const char *) au1TmpArr);
    }

#ifndef SECURITY_KERNEL_MAKE_WANTED
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnIkeDstPortRange, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s", pFsVpnPolicyName,
                      pSetValFsVpnIkeDstPortRange));
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhSetFsVpnIkeDstPortRange lv;

    lv.cmd = NMH_SET_FS_VPN_IKE_DST_PORT_RANGE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pSetValFsVpnIkeDstPortRange = pSetValFsVpnIkeDstPortRange;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnSecurityProtocol
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnSecurityProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnSecurityProtocol (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                             INT4 i4SetValFsVpnSecurityProtocol)
{

    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pVpnPolicy->u4VpnSecurityProtocol = (UINT4) i4SetValFsVpnSecurityProtocol;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnSecurityProtocol lv;

    lv.cmd = NMH_SET_FS_VPN_SECURITY_PROTOCOL;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnSecurityProtocol = i4SetValFsVpnSecurityProtocol;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnSecurityProtocol, u4SeqNum,
                          FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnSecurityProtocol));
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnInboundSpi
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnInboundSpi
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnInboundSpi (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                       INT4 i4SetValFsVpnInboundSpi)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }
    pVpnPolicy->uVpnKeyMode.IpsecManualKey.u4VpnAhInboundSpi =
        (UINT4) i4SetValFsVpnInboundSpi;

    pVpnPolicy->uVpnKeyMode.IpsecManualKey.u4VpnEspInboundSpi =
        (UINT4) i4SetValFsVpnInboundSpi;
#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnInboundSpi lv;

    lv.cmd = NMH_SET_FS_VPN_INBOUND_SPI;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnInboundSpi = i4SetValFsVpnInboundSpi;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnInboundSpi, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnInboundSpi));
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnOutboundSpi
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnOutboundSpi
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnOutboundSpi (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                        INT4 i4SetValFsVpnOutboundSpi)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }
    pVpnPolicy->uVpnKeyMode.IpsecManualKey.u4VpnAhOutboundSpi =
        (UINT4) i4SetValFsVpnOutboundSpi;

    pVpnPolicy->uVpnKeyMode.IpsecManualKey.u4VpnEspOutboundSpi =
        (UINT4) i4SetValFsVpnOutboundSpi;
#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnOutboundSpi lv;

    lv.cmd = NMH_SET_FS_VPN_OUTBOUND_SPI;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnOutboundSpi = i4SetValFsVpnOutboundSpi;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnOutboundSpi, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnOutboundSpi));
    return (SNMP_SUCCESS);
#endif

}

/****************************************************************************
 Function    :  nmhSetFsVpnMode
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnMode (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                 INT4 i4SetValFsVpnMode)
{
    tVpnPolicy         *pVpnPolicy = NULL;
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }
    pVpnPolicy->u1VpnMode = (UINT1) i4SetValFsVpnMode;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnMode lv;

    lv.cmd = NMH_SET_FS_VPN_MODE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnMode = i4SetValFsVpnMode;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnMode, u4SeqNum, FALSE, VpnDsLock,
                          VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetValFsVpnMode));
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnAuthAlgo
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnAuthAlgo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnAuthAlgo (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                     INT4 i4SetValFsVpnAuthAlgo)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }
    pVpnPolicy->uVpnKeyMode.IpsecManualKey.u1VpnAuthAlgo =
        (UINT1) i4SetValFsVpnAuthAlgo;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnAuthAlgo lv;

    lv.cmd = NMH_SET_FS_VPN_AUTH_ALGO;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnAuthAlgo = i4SetValFsVpnAuthAlgo;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnAuthAlgo, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnAuthAlgo));
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnAhKey
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnAhKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnAhKey (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                  tSNMP_OCTET_STRING_TYPE * pSetValFsVpnAhKey)
{
    tVpnPolicy         *pVpnPolicy = NULL;
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pSetValFsVpnAhKey->i4_Length >= IPSEC_AH_KEY_LEN)
    {
        return (SNMP_FAILURE);
    }
    VPN_MEMSET (pVpnPolicy->uVpnKeyMode.IpsecManualKey.au1VpnAhKey, VPN_ZERO,
                (size_t) (pSetValFsVpnAhKey->i4_Length));
    VPN_MEMCPY (pVpnPolicy->uVpnKeyMode.IpsecManualKey.au1VpnAhKey,
                pSetValFsVpnAhKey->pu1_OctetList, pSetValFsVpnAhKey->i4_Length);
    pVpnPolicy->uVpnKeyMode.IpsecManualKey.au1VpnAhKey[pSetValFsVpnAhKey->
                                                       i4_Length] = '\0';

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnAhKey lv;

    lv.cmd = NMH_SET_FS_VPN_AH_KEY;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pSetValFsVpnAhKey = pSetValFsVpnAhKey;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnAhKey, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s", pFsVpnPolicyName,
                      pSetValFsVpnAhKey));
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnEncrAlgo
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnEncrAlgo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnEncrAlgo (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                     INT4 i4SetValFsVpnEncrAlgo)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }
    pVpnPolicy->uVpnKeyMode.IpsecManualKey.u1VpnEncryptionAlgo =
        (UINT1) i4SetValFsVpnEncrAlgo;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnEncrAlgo lv;

    lv.cmd = NMH_SET_FS_VPN_ENCR_ALGO;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnEncrAlgo = i4SetValFsVpnEncrAlgo;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnEncrAlgo, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnEncrAlgo));
    return (SNMP_SUCCESS);
#endif

}

/****************************************************************************
 Function    :  nmhSetFsVpnEspKey
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnEspKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnEspKey (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                   tSNMP_OCTET_STRING_TYPE * pSetValFsVpnEspKey)
{
    tVpnPolicy         *pVpnPolicy = NULL;
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pSetValFsVpnEspKey->i4_Length >= IPSEC_ESP_KEY_LEN)
    {
        return (SNMP_FAILURE);
    }

    VPN_MEMSET (pVpnPolicy->uVpnKeyMode.IpsecManualKey.au2VpnEspKey, VPN_ZERO,
                (size_t) (pSetValFsVpnEspKey->i4_Length));
    VPN_MEMCPY (pVpnPolicy->uVpnKeyMode.IpsecManualKey.au2VpnEspKey,
                pSetValFsVpnEspKey->pu1_OctetList,
                pSetValFsVpnEspKey->i4_Length);
    pVpnPolicy->uVpnKeyMode.IpsecManualKey.au2VpnEspKey[pSetValFsVpnEspKey->
                                                        i4_Length] = '\0';
#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnEspKey lv;

    lv.cmd = NMH_SET_FS_VPN_ESP_KEY;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pSetValFsVpnEspKey = pSetValFsVpnEspKey;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnEspKey, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s", pFsVpnPolicyName,
                      pSetValFsVpnEspKey));
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnAntiReplay
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnAntiReplay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnAntiReplay (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                       INT4 i4SetValFsVpnAntiReplay)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    VPN_POLICY_ANTI_REPLAY_STATUS (pVpnPolicy) =
        (UINT1) i4SetValFsVpnAntiReplay;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnAntiReplay lv;

    lv.cmd = NMH_SET_FS_VPN_ANTI_REPLAY;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnAntiReplay = i4SetValFsVpnAntiReplay;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnAntiReplay, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnAntiReplay));
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnPolicyFlag
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnPolicyFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnPolicyFlag (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                       INT4 i4SetValFsVpnPolicyFlag)
{

    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }
    pVpnPolicy->u4VpnPolicyFlag = (UINT4) i4SetValFsVpnPolicyFlag;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnPolicyFlag lv;

    lv.cmd = NMH_SET_FS_VPN_POLICY_FLAG;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnPolicyFlag = i4SetValFsVpnPolicyFlag;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnPolicyFlag, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnPolicyFlag));
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnProtocol
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnProtocol (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                     INT4 i4SetValFsVpnProtocol)
{
    tVpnPolicy         *pVpnPolicy = NULL;
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pVpnPolicy->u4VpnProtocol = (UINT4) i4SetValFsVpnProtocol;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnProtocol lv;

    lv.cmd = NMH_SET_FS_VPN_PROTOCOL;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnProtocol = i4SetValFsVpnProtocol;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnProtocol, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnProtocol));
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnPolicyIntfIndex
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnPolicyIntfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnPolicyIntfIndex (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                            INT4 i4SetValFsVpnPolicyIntfIndex)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;
    tVpnPolicy         *pTempVpnPolicy = NULL;
    UINT4               u4IpAddr = VPN_ZERO;
    tIp6Addr           *pIp6Addr = NULL;
    tIp6Addr            NullIp6Addr;
    MEMSET (&NullIp6Addr, VPN_ZERO, sizeof (tIp6Addr));

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    /* rejecting configurations with more than one policy to the same peer */
    SLL_SCAN (&VpnPolicyList, pTempVpnPolicy, tVpnPolicy *)
    {
        if ((pTempVpnPolicy->u1VpnPolicyRowStatus == ACTIVE) &&
            (pTempVpnPolicy->u4IfIndex == (UINT4) i4SetValFsVpnPolicyIntfIndex)
            && (pTempVpnPolicy->RemoteTunnTermAddr.u4AddrType ==
                pVpnPolicy->RemoteTunnTermAddr.u4AddrType))
        {
            if (pVpnPolicy->RemoteTunnTermAddr.u4AddrType ==
                IPVX_ADDR_FMLY_IPV4)
            {
                if (MEMCMP
                    (&(pTempVpnPolicy->RemoteTunnTermAddr.uIpAddr.Ip4Addr),
                     &(pVpnPolicy->RemoteTunnTermAddr.uIpAddr.Ip4Addr),
                     IPVX_IPV4_ADDR_LEN) == 0)
                {
                    CLI_SET_ERR (CLI_VPN_ERR_REDUNDANT_POLICY_TO_PEER);
                    return (SNMP_FAILURE);
                }
            }
            else
            {
                if (MEMCMP
                    (&(pTempVpnPolicy->RemoteTunnTermAddr.uIpAddr.Ip6Addr),
                     &(pVpnPolicy->RemoteTunnTermAddr.uIpAddr.Ip6Addr),
                     IPVX_IPV6_ADDR_LEN) == 0)
                {
                    CLI_SET_ERR (CLI_VPN_ERR_REDUNDANT_POLICY_TO_PEER);
                    return (SNMP_FAILURE);
                }
            }
        }
    }

    pVpnPolicy->u4IfIndex = (UINT4) i4SetValFsVpnPolicyIntfIndex;

    /* TODO  Remove this || check */
    if ((pVpnPolicy->LocalProtectNetwork.IpAddr.u4AddrType ==
         IPVX_ADDR_FMLY_IPV4) ||
        (pVpnPolicy->RemoteProtectNetwork.IpAddr.u4AddrType ==
         IPVX_ADDR_FMLY_IPV4))
    {
        if (CfaGetIfIpAddr ((INT4) pVpnPolicy->u4IfIndex, &u4IpAddr) ==
            OSIX_FAILURE)
        {
            return (SNMP_FAILURE);
        }
        pVpnPolicy->LocalTunnTermAddr.uIpAddr.Ip4Addr = u4IpAddr;
        pVpnPolicy->LocalTunnTermAddr.u4AddrType = IPVX_ADDR_FMLY_IPV4;

        if (pVpnPolicy->LocalTunnTermAddr.uIpAddr.Ip4Addr == VPN_ZERO)
        {
            CLI_SET_ERR (CLI_VPN_INVLAID_INTF_PARAMS);
            return (SNMP_FAILURE);
        }
    }
    else
    {
        pIp6Addr = Sec6UtilGetGlobalAddr (pVpnPolicy->u4IfIndex, NULL);
        /* IPv6 address might take some time to settle. If the routine is called 
         * from MSR, fill the data only when the IPv6 address is updated.
         * This data will be filled later when the indication from cfa comes to
         * security module when the ip address has been settled */
        if ((pIp6Addr != NULL) &&
            (FALSE == Ip6AddrMatch (&NullIp6Addr, pIp6Addr, sizeof (tIp6Addr))))
        {
            /* pIp6Addr is not NULL and the address is not null IPv6 address */
            Ip6AddrCopy (&(pVpnPolicy->LocalTunnTermAddr.uIpAddr.Ip6Addr),
                         pIp6Addr);
            pVpnPolicy->LocalTunnTermAddr.u4AddrType = IPVX_ADDR_FMLY_IPV6;
        }
    }

    pVpnPolicy->u4TunTermAddrType = pVpnPolicy->LocalTunnTermAddr.u4AddrType;

#ifdef SECURITY_KERNEL_MAKE_WANTED

    int                 rc;
    tNpwnmhSetFsVpnPolicyIntfIndex lv;

    lv.cmd = NMH_SET_FS_VPN_POLICY_INTF_INDEX;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnPolicyIntfIndex = i4SetValFsVpnPolicyIntfIndex;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnPolicyIntfIndex, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnPolicyIntfIndex));
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnIkePhase1HashAlgo
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnIkePhase1HashAlgo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnIkePhase1HashAlgo (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                              INT4 i4SetValFsVpnIkePhase1HashAlgo)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4HashAlgo =
        (UINT4) i4SetValFsVpnIkePhase1HashAlgo;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnIkePhase1HashAlgo lv;

    lv.cmd = NMH_SET_FS_VPN_IKE_PHASE1_HASH_ALGO;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnIkePhase1HashAlgo = i4SetValFsVpnIkePhase1HashAlgo;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnIkePhase1HashAlgo, u4SeqNum,
                          FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnIkePhase1HashAlgo));
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnIkePhase1EncryptionAlgo
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnIkePhase1EncryptionAlgo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnIkePhase1EncryptionAlgo (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                    INT4 i4SetValFsVpnIkePhase1EncryptionAlgo)
{

    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4EncryptionAlgo =
        (UINT4) i4SetValFsVpnIkePhase1EncryptionAlgo;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnIkePhase1EncryptionAlgo lv;

    lv.cmd = NMH_SET_FS_VPN_IKE_PHASE1_ENCRYPTION_ALGO;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnIkePhase1EncryptionAlgo =
        i4SetValFsVpnIkePhase1EncryptionAlgo;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnIkePhase1EncryptionAlgo,
                          u4SeqNum, FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE,
                          SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnIkePhase1EncryptionAlgo));
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnIkePhase1DHGroup
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnIkePhase1DHGroup
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnIkePhase1DHGroup (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                             INT4 i4SetValFsVpnIkePhase1DHGroup)
{

    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4DHGroup =
        (UINT4) i4SetValFsVpnIkePhase1DHGroup;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnIkePhase1DHGroup lv;

    lv.cmd = NMH_SET_FS_VPN_IKE_PHASE1_D_H_GROUP;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnIkePhase1DHGroup = i4SetValFsVpnIkePhase1DHGroup;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnIkePhase1DHGroup, u4SeqNum,
                          FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnIkePhase1DHGroup));
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnIkePhase1LocalIdType
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnIkePhase1LocalIdType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnIkePhase1LocalIdType (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                 INT4 i4SetValFsVpnIkePhase1LocalIdType)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyLocalID.i2IdType
        = (INT2) i4SetValFsVpnIkePhase1LocalIdType;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnIkePhase1LocalIdType lv;

    lv.cmd = NMH_SET_FS_VPN_IKE_PHASE1_LOCAL_ID_TYPE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnIkePhase1LocalIdType = i4SetValFsVpnIkePhase1LocalIdType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnIkePhase1LocalIdType, u4SeqNum,
                          FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnIkePhase1LocalIdType));
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnIkePhase1LocalIdValue
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnIkePhase1LocalIdValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnIkePhase1LocalIdValue (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValFsVpnIkePhase1LocalIdValue)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tUtlIn6Addr         In6Addr;
    tVpnPolicy         *pVpnPolicy = NULL;
    tIkeUserID         *pLocalIDVal = NULL;
    UINT1               au1IpAddr[VPN_IP_ADDR_LEN + VPN_ONE] = { VPN_ZERO };

    MEMSET (&In6Addr, VPN_ZERO, sizeof (tUtlIn6Addr));

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pLocalIDVal = &(pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyLocalID);
    pLocalIDVal->u2Length =
        (UINT2) pSetValFsVpnIkePhase1LocalIdValue->i4_Length;
    if (pLocalIDVal->u2Length > VPN_MAX_NAME_LENGTH)
    {
        pLocalIDVal->u2Length = VPN_MAX_NAME_LENGTH;
    }
    switch ((pLocalIDVal->i2IdType))
    {
        case VPN_ID_TYPE_IPV4:
            if (pSetValFsVpnIkePhase1LocalIdValue->i4_Length > VPN_IP_ADDR_LEN)
            {
                return (SNMP_FAILURE);
            }
            STRNCPY (au1IpAddr,
                     pSetValFsVpnIkePhase1LocalIdValue->pu1_OctetList,
                     (size_t) pSetValFsVpnIkePhase1LocalIdValue->i4_Length);
            au1IpAddr[pSetValFsVpnIkePhase1LocalIdValue->i4_Length] = '\0';
            pLocalIDVal->uID.Ip4Addr = OSIX_NTOHL (INET_ADDR (au1IpAddr));
            break;

        case VPN_ID_TYPE_EMAIL:
            if (pSetValFsVpnIkePhase1LocalIdValue->i4_Length >
                VPN_MAX_NAME_LENGTH)
            {
                pSetValFsVpnIkePhase1LocalIdValue->i4_Length =
                    VPN_MAX_NAME_LENGTH;
            }
            STRNCPY (pLocalIDVal->uID.au1Email,
                     pSetValFsVpnIkePhase1LocalIdValue->pu1_OctetList,
                     (size_t) pSetValFsVpnIkePhase1LocalIdValue->i4_Length);
            pLocalIDVal->uID.au1Email[pLocalIDVal->u2Length] = '\0';
            break;

        case VPN_ID_TYPE_IPV6:

            INET_ATON6 (pSetValFsVpnIkePhase1LocalIdValue->pu1_OctetList,
                        &In6Addr);
            MEMCPY (&(pLocalIDVal->uID.Ip6Addr), In6Addr.u1addr,
                    IPVX_IPV6_ADDR_LEN);
            break;

        case VPN_ID_TYPE_FQDN:
            if (pSetValFsVpnIkePhase1LocalIdValue->i4_Length >
                VPN_MAX_NAME_LENGTH)
            {
                pSetValFsVpnIkePhase1LocalIdValue->i4_Length =
                    VPN_MAX_NAME_LENGTH;
            }
            STRNCPY (pLocalIDVal->uID.au1Fqdn,
                     pSetValFsVpnIkePhase1LocalIdValue->pu1_OctetList,
                     (size_t) pSetValFsVpnIkePhase1LocalIdValue->i4_Length);
            pLocalIDVal->uID.au1Fqdn[pLocalIDVal->u2Length] = '\0';
            break;

        case VPN_ID_TYPE_DN:
            if (pSetValFsVpnIkePhase1LocalIdValue->i4_Length >
                VPN_MAX_NAME_LENGTH)
            {
                pSetValFsVpnIkePhase1LocalIdValue->i4_Length =
                    VPN_MAX_NAME_LENGTH;
            }
            STRNCPY (pLocalIDVal->uID.au1Dn,
                     pSetValFsVpnIkePhase1LocalIdValue->pu1_OctetList,
                     (size_t) pSetValFsVpnIkePhase1LocalIdValue->i4_Length);
            pLocalIDVal->uID.au1Dn[pLocalIDVal->u2Length] = '\0';
            break;
        case VPN_ID_TYPE_KEYID:
            if (pSetValFsVpnIkePhase1LocalIdValue->i4_Length >
                VPN_MAX_NAME_LENGTH)
            {
                pSetValFsVpnIkePhase1LocalIdValue->i4_Length =
                    VPN_MAX_NAME_LENGTH;
            }
            STRNCPY (pLocalIDVal->uID.au1KeyId,
                     pSetValFsVpnIkePhase1LocalIdValue->pu1_OctetList,
                     (size_t) pSetValFsVpnIkePhase1LocalIdValue->i4_Length);
            pLocalIDVal->uID.au1KeyId[pLocalIDVal->u2Length] = '\0';
            break;
        default:
            break;
    }

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnIkePhase1LocalIdValue lv;

    lv.cmd = NMH_SET_FS_VPN_IKE_PHASE1_LOCAL_ID_VALUE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pSetValFsVpnIkePhase1LocalIdValue = pSetValFsVpnIkePhase1LocalIdValue;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnIkePhase1LocalIdValue, u4SeqNum,
                          FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s", pFsVpnPolicyName,
                      pSetValFsVpnIkePhase1LocalIdValue));
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnIkePhase1PeerIdType
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnIkePhase1PeerIdType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnIkePhase1PeerIdType (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                INT4 i4SetValFsVpnIkePhase1PeerIdType)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.i2IdType
        = (INT2) i4SetValFsVpnIkePhase1PeerIdType;
#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnIkePhase1PeerIdType lv;

    lv.cmd = NMH_SET_FS_VPN_IKE_PHASE1_PEER_ID_TYPE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnIkePhase1PeerIdType = i4SetValFsVpnIkePhase1PeerIdType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnIkePhase1PeerIdType, u4SeqNum,
                          FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnIkePhase1PeerIdType));
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnIkePhase1PeerIdValue
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnIkePhase1PeerIdValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnIkePhase1PeerIdValue (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValFsVpnIkePhase1PeerIdValue)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tUtlIn6Addr         In6Addr;
    tVpnPolicy         *pVpnPolicy = NULL;
    tIkeUserID         *pPeerID = NULL;
    UINT1               au1IpAddr[VPN_IP_ADDR_LEN + VPN_ONE] = { VPN_ZERO };

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pPeerID = &(pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID);
    pPeerID->u2Length = (UINT2) pSetValFsVpnIkePhase1PeerIdValue->i4_Length;
    if (pPeerID->u2Length > VPN_MAX_NAME_LENGTH)
    {
        pPeerID->u2Length = VPN_MAX_NAME_LENGTH;
    }
    switch (pPeerID->i2IdType)
    {
        case VPN_ID_TYPE_IPV4:
            if (pSetValFsVpnIkePhase1PeerIdValue->i4_Length > VPN_IP_ADDR_LEN)
            {
                return (SNMP_FAILURE);
            }
            STRNCPY (au1IpAddr,
                     pSetValFsVpnIkePhase1PeerIdValue->pu1_OctetList,
                     (size_t) pSetValFsVpnIkePhase1PeerIdValue->i4_Length);
            au1IpAddr[pSetValFsVpnIkePhase1PeerIdValue->i4_Length] = '\0';
            pPeerID->uID.Ip4Addr = OSIX_NTOHL (INET_ADDR (au1IpAddr));
            break;

        case VPN_ID_TYPE_EMAIL:
            if (pSetValFsVpnIkePhase1PeerIdValue->i4_Length >
                VPN_MAX_NAME_LENGTH)
            {
                pSetValFsVpnIkePhase1PeerIdValue->i4_Length =
                    VPN_MAX_NAME_LENGTH;
            }
            STRNCPY (pPeerID->uID.au1Email,
                     pSetValFsVpnIkePhase1PeerIdValue->pu1_OctetList,
                     (size_t) pSetValFsVpnIkePhase1PeerIdValue->i4_Length);
            pPeerID->uID.au1Email[pPeerID->u2Length] = '\0';
            break;

        case VPN_ID_TYPE_FQDN:
            if (pSetValFsVpnIkePhase1PeerIdValue->i4_Length >
                VPN_MAX_NAME_LENGTH)
            {
                pSetValFsVpnIkePhase1PeerIdValue->i4_Length =
                    VPN_MAX_NAME_LENGTH;
            }
            STRNCPY (pPeerID->uID.au1Fqdn,
                     pSetValFsVpnIkePhase1PeerIdValue->pu1_OctetList,
                     (size_t) pSetValFsVpnIkePhase1PeerIdValue->i4_Length);
            pPeerID->uID.au1Fqdn[pPeerID->u2Length] = '\0';
            break;

        case VPN_ID_TYPE_IPV6:

            INET_ATON6 (pSetValFsVpnIkePhase1PeerIdValue->pu1_OctetList,
                        &In6Addr);
            MEMCPY (&(pPeerID->uID.Ip6Addr), In6Addr.u1addr,
                    IPVX_IPV6_ADDR_LEN);
            break;

        case VPN_ID_TYPE_DN:
            if (pSetValFsVpnIkePhase1PeerIdValue->i4_Length >
                VPN_MAX_NAME_LENGTH)
            {
                pSetValFsVpnIkePhase1PeerIdValue->i4_Length =
                    VPN_MAX_NAME_LENGTH;
            }
            STRNCPY (pPeerID->uID.au1Dn,
                     pSetValFsVpnIkePhase1PeerIdValue->pu1_OctetList,
                     (size_t) pSetValFsVpnIkePhase1PeerIdValue->i4_Length);
            pPeerID->uID.au1Dn[pPeerID->u2Length] = '\0';
            break;

        case VPN_ID_TYPE_KEYID:
            if (pSetValFsVpnIkePhase1PeerIdValue->i4_Length >
                VPN_MAX_NAME_LENGTH)
            {
                pSetValFsVpnIkePhase1PeerIdValue->i4_Length =
                    VPN_MAX_NAME_LENGTH;
            }
            STRNCPY (pPeerID->uID.au1KeyId,
                     pSetValFsVpnIkePhase1PeerIdValue->pu1_OctetList,
                     (size_t) pSetValFsVpnIkePhase1PeerIdValue->i4_Length);
            pPeerID->uID.au1KeyId[pPeerID->u2Length] = '\0';
            break;
        default:
            break;
    }
#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnIkePhase1PeerIdValue lv;

    lv.cmd = NMH_SET_FS_VPN_IKE_PHASE1_PEER_ID_VALUE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pSetValFsVpnIkePhase1PeerIdValue = pSetValFsVpnIkePhase1PeerIdValue;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnIkePhase1PeerIdValue, u4SeqNum,
                          FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s", pFsVpnPolicyName,
                      pSetValFsVpnIkePhase1PeerIdValue));
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnIkePhase1LifeTimeType
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnIkePhase1LifeTimeType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnIkePhase1LifeTimeType (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                  INT4 i4SetValFsVpnIkePhase1LifeTimeType)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4LifeTimeType =
        (UINT4) i4SetValFsVpnIkePhase1LifeTimeType;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnIkePhase1LifeTimeType lv;

    lv.cmd = NMH_SET_FS_VPN_IKE_PHASE1_LIFE_TIME_TYPE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnIkePhase1LifeTimeType = i4SetValFsVpnIkePhase1LifeTimeType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnIkePhase1LifeTimeType, u4SeqNum,
                          FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnIkePhase1LifeTimeType));
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnIkePhase1LifeTime
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnIkePhase1LifeTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnIkePhase1LifeTime (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                              INT4 i4SetValFsVpnIkePhase1LifeTime)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4LifeTime
        = (UINT4) i4SetValFsVpnIkePhase1LifeTime;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnIkePhase1LifeTime lv;

    lv.cmd = NMH_SET_FS_VPN_IKE_PHASE1_LIFE_TIME;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnIkePhase1LifeTime = i4SetValFsVpnIkePhase1LifeTime;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnIkePhase1LifeTime, u4SeqNum,
                          FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnIkePhase1LifeTime));
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnIkePhase1Mode
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnIkePhase1Mode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnIkePhase1Mode (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                          INT4 i4SetValFsVpnIkePhase1Mode)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u4VpnPolicyType != VPN_XAUTH)
    {
        pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4Mode =
            (UINT4) i4SetValFsVpnIkePhase1Mode;
    }
    else
    {
        pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4Mode =
            VPN_IKE_MODE_AGGRESSIVE;
    }

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnIkePhase1Mode lv;

    lv.cmd = NMH_SET_FS_VPN_IKE_PHASE1_MODE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnIkePhase1Mode = i4SetValFsVpnIkePhase1Mode;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnIkePhase1Mode, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnIkePhase1Mode));
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnIkePhase2AuthAlgo
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnIkePhase2AuthAlgo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnIkePhase2AuthAlgo (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                              INT4 i4SetValFsVpnIkePhase2AuthAlgo)
{

    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u1AuthAlgo
        = (UINT1) i4SetValFsVpnIkePhase2AuthAlgo;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnIkePhase2AuthAlgo lv;

    lv.cmd = NMH_SET_FS_VPN_IKE_PHASE2_AUTH_ALGO;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnIkePhase2AuthAlgo = i4SetValFsVpnIkePhase2AuthAlgo;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnIkePhase2AuthAlgo, u4SeqNum,
                          FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnIkePhase2AuthAlgo));
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnIkePhase2EspEncryptionAlgo
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnIkePhase2EspEncryptionAlgo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnIkePhase2EspEncryptionAlgo (tSNMP_OCTET_STRING_TYPE *
                                       pFsVpnPolicyName,
                                       INT4
                                       i4SetValFsVpnIkePhase2EspEncryptionAlgo)
{

    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u1EncryptionAlgo =
        (UINT1) i4SetValFsVpnIkePhase2EspEncryptionAlgo;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnIkePhase2EspEncryptionAlgo lv;

    lv.cmd = NMH_SET_FS_VPN_IKE_PHASE2_ESP_ENCRYPTION_ALGO;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnIkePhase2EspEncryptionAlgo =
        i4SetValFsVpnIkePhase2EspEncryptionAlgo;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnIkePhase2EspEncryptionAlgo,
                          u4SeqNum, FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE,
                          SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnIkePhase2EspEncryptionAlgo));
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnIkePhase2LifeTimeType
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnIkePhase2LifeTimeType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnIkePhase2LifeTimeType (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                  INT4 i4SetValFsVpnIkePhase2LifeTimeType)
{

    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u1LifeTimeType =
        (UINT1) i4SetValFsVpnIkePhase2LifeTimeType;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnIkePhase2LifeTimeType lv;

    lv.cmd = NMH_SET_FS_VPN_IKE_PHASE2_LIFE_TIME_TYPE;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnIkePhase2LifeTimeType = i4SetValFsVpnIkePhase2LifeTimeType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnIkePhase2LifeTimeType, u4SeqNum,
                          FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnIkePhase2LifeTimeType));
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnIkePhase2LifeTime
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnIkePhase2LifeTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnIkePhase2LifeTime (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                              INT4 i4SetValFsVpnIkePhase2LifeTime)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u4LifeTime =
        (UINT4) i4SetValFsVpnIkePhase2LifeTime;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnIkePhase2LifeTime lv;

    lv.cmd = NMH_SET_FS_VPN_IKE_PHASE2_LIFE_TIME;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnIkePhase2LifeTime = i4SetValFsVpnIkePhase2LifeTime;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnIkePhase2LifeTime, u4SeqNum,
                          FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnIkePhase2LifeTime));
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnIkePhase2DHGroup
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnIkePhase2DHGroup
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnIkePhase2DHGroup (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                             INT4 i4SetValFsVpnIkePhase2DHGroup)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u1DHGroup =
        (UINT1) i4SetValFsVpnIkePhase2DHGroup;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnIkePhase2DHGroup lv;

    lv.cmd = NMH_SET_FS_VPN_IKE_PHASE2_D_H_GROUP;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnIkePhase2DHGroup = i4SetValFsVpnIkePhase2DHGroup;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnIkePhase2DHGroup, u4SeqNum,
                          FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnIkePhase2DHGroup));
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnIkeVersion
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnIkeVersion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnIkeVersion (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                       INT4 i4SetValFsVpnIkeVersion)
{
    tVpnPolicy         *pVpnPolicy = NULL;
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnPolicy == NULL)
    {
        return (SNMP_FAILURE);
    }

    pVpnPolicy->u1VpnIkeVer = (UINT1) i4SetValFsVpnIkeVersion;

#ifndef SECURITY_KERNEL_MAKE_WANTED
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnIkeVersion, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnIkeVersion));
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhSetFsVpnIkeVersion lv;

    lv.cmd = NMH_SET_FS_VPN_IKE_VERSION;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnIkeVersion = i4SetValFsVpnIkeVersion;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnPolicyRowStatus
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                setValFsVpnPolicyRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnPolicyRowStatus (tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                            INT4 i4SetValFsVpnPolicyRowStatus)
{
    tVpnPolicy         *pVpnPolicy = NULL;
    INT4                i4RetVal = VPN_FAILURE;
    UINT4               u4IpAddr = VPN_ZERO;
    INT4                i4AddrType = VPN_ZERO;
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
#ifdef SECURITY_KERNEL_MAKE_WANTED
    UINT1               u1PreVpnPolicyRowStatus = NOT_IN_SERVICE;
#endif
    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
#ifdef SECURITY_KERNEL_MAKE_WANTED

    if (pVpnPolicy != NULL)
    {
        u1PreVpnPolicyRowStatus = pVpnPolicy->u1VpnPolicyRowStatus;
    }
#endif

    RM_GET_SEQ_NUM (&u4SeqNum);

    switch (i4SetValFsVpnPolicyRowStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:

            if (MemAllocateMemBlock
                (VPN_POLICY_PID,
                 (UINT1 **) (VOID *) &pVpnPolicy) != MEM_SUCCESS)
            {
                return (SNMP_FAILURE);
            }
            if (pVpnPolicy == NULL)
            {
                return (SNMP_FAILURE);
            }

            VpnUtilInitPolicyConfig (pVpnPolicy);
            if (pFsVpnPolicyName->i4_Length > (VPN_MAX_POLICY_NAME_LENGTH))
            {
                MemReleaseMemBlock (VPN_POLICY_PID, (UINT1 *) pVpnPolicy);
                return (SNMP_FAILURE);
            }
            MEMCPY (pVpnPolicy->au1VpnPolicyName,
                    pFsVpnPolicyName->pu1_OctetList,
                    pFsVpnPolicyName->i4_Length);
            pVpnPolicy->au1VpnPolicyName[pFsVpnPolicyName->i4_Length] = '\0';

            TMO_SLL_Add (&VpnPolicyList, (t_SLL_NODE *) pVpnPolicy);
            break;

        case ACTIVE:

            if (pVpnPolicy == NULL)
            {
                return (SNMP_FAILURE);
            }

            if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
            {
                return (SNMP_SUCCESS);
            }
            if ((pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u1EncryptionAlgo ==
                 SEC_AESCTR)
                || (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.
                    u1EncryptionAlgo == SEC_AESCTR192)
                || (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.
                    u1EncryptionAlgo == SEC_AESCTR256))
            {
                if (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u1AuthAlgo ==
                    SEC_ZERO)
                {
                    return (SNMP_FAILURE);
                }
            }

            i4AddrType = (INT4) pVpnPolicy->RemoteTunnTermAddr.u4AddrType;

            if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                if (CfaGetIfIpAddr ((INT4) pVpnPolicy->u4IfIndex,
                                    &u4IpAddr) == OSIX_FAILURE)
                {
                    return (SNMP_FAILURE);
                }
                if (u4IpAddr == VPN_ZERO)
                {
                    /* Safely return without activating the policy as there is
                     * no ipaddr for the supplied interface.
                     * This is most likely happen while restoring the policies
                     * activated on a DHCP interface.
                     */
                    return (SNMP_FAILURE);
                }
            }

#ifndef SECURITY_KERNEL_MAKE_WANTED
            /* Fill IPsec DB to use FS IPSEC Stack */
            if ((pVpnPolicy->u4VpnPolicyType != VPN_XAUTH) &&
                (pVpnPolicy->u4VpnPolicyType != VPN_IKE_RA_PRESHAREDKEY) &&
                (pVpnPolicy->u4VpnPolicyType != VPN_IKE_RA_CERT) &&
                (pVpnPolicy->u4VpnPolicyType != VPN_IKE_XAUTH_CERT))
            {
                if ((i4AddrType == IPVX_ADDR_FMLY_IPV4) &&
                    (VpnFillIpsecParams (pVpnPolicy) == SNMP_FAILURE))
                {
                    return (SNMP_FAILURE);
                }

#ifdef IPSECv6_WANTED
                if ((i4AddrType == IPVX_ADDR_FMLY_IPV6) &&
                    (Secv6VpnFillIpsecParams (pVpnPolicy) == OSIX_FAILURE))
                {
                    return (SNMP_FAILURE);
                }
#endif
            }
#endif
#ifdef IKE_WANTED
            /* Fill IPsec DB to use FS IKE Stack in case of Automatic Keying */
            if (pVpnPolicy->u4VpnPolicyType != VPN_IPSEC_MANUAL)
            {
                if (VpnUtilFillIkeParams (pVpnPolicy) == SNMP_FAILURE)
                {
                    return (SNMP_FAILURE);
                }
            }
#endif /* IKE_WANTED */
            i4RetVal = VpnSetRemoteIdRefCnt (pVpnPolicy, VPN_REF_INCR);

            if (i4RetVal == VPN_FAILURE)
            {
#ifndef SECURITY_KERNEL_MAKE_WANTED
                /* Delete IpSec and IKE DB if configured */
                if ((pVpnPolicy->u4VpnPolicyType != VPN_XAUTH) &&
                    (pVpnPolicy->u4VpnPolicyType != VPN_IKE_RA_PRESHAREDKEY) &&
                    (pVpnPolicy->u4VpnPolicyType != VPN_IKE_RA_CERT) &&
                    (pVpnPolicy->u4VpnPolicyType != VPN_IKE_XAUTH_CERT))
                {
                    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
                    {
                        VpnDeleteIpsecParams (pVpnPolicy);
                    }

#ifdef IPSECv6_WANTED
                    if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
                    {
                        Secv6VpnDeleteIpsecParams (pVpnPolicy);
                    }
#endif
                }
#endif
#ifdef IKE_WANTED
                if (pVpnPolicy->u4VpnPolicyType != VPN_IPSEC_MANUAL)
                {
                    VpnUtilDelIkeParams (pVpnPolicy);
                }
#endif /* IKE_WANTED */
                CLI_SET_ERR (CLI_VPN_ERR_REMID_REF);
                return (SNMP_FAILURE);
            }
            if ((pVpnPolicy->u4VpnPolicyType == VPN_XAUTH) ||
                (pVpnPolicy->u4VpnPolicyType == VPN_IKE_RA_PRESHAREDKEY) ||
                (pVpnPolicy->u4VpnPolicyType == VPN_IKE_RA_CERT) ||
                (pVpnPolicy->u4VpnPolicyType == VPN_IKE_XAUTH_CERT))
            {
                /* This variable indicates , the no.of RAVPN polices 
                 * configured which is requied to block the global configuration 
                 * 'set remote-access {server|client} if there is any active 
                 * RAVPN policies */
                gu4RaRefCount++;
                if (gu4VpnRaServer == RAVPN_SERVER)
                {
                    VpnSetRavpnPoolRefCnt (VPN_REF_INCR);
                }
            }

            if (pVpnPolicy->u4VpnPolicyType == VPN_IKE_CERTIFICATE)
            {
                i4RetVal = VpnSetCertInfoRefCnt (pVpnPolicy, VPN_REF_INCR);
            }
#ifndef SECURITY_KERNEL_MAKE_WANTED
#ifdef IPSECv6_WANTED
            Secv6UpdateSecAssocSize ();
            if (Secv6ConstSecList () != SEC_SUCCESS)
            {
                return (SNMP_FAILURE);
            }
            if (Secv6ConstAccessList () != SEC_SUCCESS)
            {
                return (SNMP_FAILURE);
            }
#endif
#endif
            pVpnPolicy->u1VpnPolicyRowStatus = ACTIVE;
            break;

        case NOT_IN_SERVICE:

            if (pVpnPolicy == NULL)
            {
                return (SNMP_FAILURE);
            }

            i4AddrType = (INT4) pVpnPolicy->RemoteTunnTermAddr.u4AddrType;
            if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
            {
                i4RetVal = VpnSetRemoteIdRefCnt (pVpnPolicy, VPN_REF_DECR);

#ifndef SECURITY_KERNEL_MAKE_WANTED

                if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
                {
                    VpnDeleteIpsecParams (pVpnPolicy);
                }

#ifdef IPSECv6_WANTED
                if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
                {
                    Secv6VpnDeleteIpsecParams (pVpnPolicy);
                }
#endif

#endif
#ifdef IKE_WANTED
                if (pVpnPolicy->u4VpnPolicyType != VPN_IPSEC_MANUAL)
                {
                    VpnUtilDelIkeParams (pVpnPolicy);
                }
#endif /* IKE_WANTED */
                if ((pVpnPolicy->u4VpnPolicyType == VPN_XAUTH) ||
                    (pVpnPolicy->u4VpnPolicyType == VPN_IKE_RA_PRESHAREDKEY) ||
                    (pVpnPolicy->u4VpnPolicyType == VPN_IKE_RA_CERT) ||
                    (pVpnPolicy->u4VpnPolicyType == VPN_IKE_XAUTH_CERT))
                {
                    gu4RaRefCount--;
                    if (gu4VpnRaServer == RAVPN_SERVER)
                    {
                        VpnSetRavpnPoolRefCnt (VPN_REF_DECR);
                    }
#ifdef IKE_WANTED
                    else
                    {
                        if (CfaIfmDeleteVpncInterface (pVpnPolicy->u4IfIndex,
                                                       CFA_TRUE) == CFA_FAILURE)
                        {
                            return (SNMP_FAILURE);
                        }
                    }
#endif /* IKE_WANTED */
                }
                /* Setting the IntfIndex to 0. This will help us in 
                   differentiating the active and inactive policies,
                   especially during restroration and Wan interface 
                   addr type is dhcp
                 */
                if (pVpnPolicy->u4VpnPolicyType == VPN_IKE_CERTIFICATE)
                {
                    i4RetVal = VpnSetCertInfoRefCnt (pVpnPolicy, VPN_REF_DECR);
                }
                pVpnPolicy->u4IfIndex = VPN_ZERO;
            }
            pVpnPolicy->u1VpnPolicyRowStatus = NOT_IN_SERVICE;

            break;

        case DESTROY:
            if (pVpnPolicy == NULL)
            {
                return (SNMP_FAILURE);
            }
            i4AddrType = (INT4) pVpnPolicy->RemoteTunnTermAddr.u4AddrType;

            if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
            {
#ifndef SECURITY_KERNEL_MAKE_WANTED

                if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
                {
                    VpnDeleteIpsecParams (pVpnPolicy);
                }

#ifdef IPSECv6_WANTED
                if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
                {
                    Secv6VpnDeleteIpsecParams (pVpnPolicy);
                }
#endif
#endif
#ifdef IKE_WANTED
                if (pVpnPolicy->u4VpnPolicyType != VPN_IPSEC_MANUAL)
                {
                    VpnUtilDelIkeParams (pVpnPolicy);
                }
#endif /* IKE_WANTED */
                i4RetVal = VpnSetRemoteIdRefCnt (pVpnPolicy, VPN_REF_DECR);
                if (i4RetVal == VPN_FAILURE)
                {
                    CLI_SET_ERR (CLI_VPN_ERR_REMID_REF);
                    pVpnPolicy->u1VpnPolicyRowStatus = NOT_IN_SERVICE;
                    return (SNMP_FAILURE);
                }
                if ((pVpnPolicy->u4VpnPolicyType == VPN_XAUTH) ||
                    (pVpnPolicy->u4VpnPolicyType == VPN_IKE_RA_PRESHAREDKEY) ||
                    (pVpnPolicy->u4VpnPolicyType == VPN_IKE_RA_CERT) ||
                    (pVpnPolicy->u4VpnPolicyType == VPN_IKE_XAUTH_CERT))
                {
                    gu4RaRefCount--;
                    /* Decrement the pool reference count if we are acting 
                     * as RAVPN Server */
                    if (gu4VpnRaServer == RAVPN_SERVER)
                    {
                        VpnSetRavpnPoolRefCnt (VPN_REF_DECR);
                    }
#ifdef IKE_WANTED
                    /* Delete the vpnc interface if it exists */
                    else
                    {
                        if (CfaIfmDeleteVpncInterface (pVpnPolicy->u4IfIndex,
                                                       CFA_TRUE) == CFA_FAILURE)
                        {
                            return (SNMP_FAILURE);
                        }
                    }
#endif
                }
            }

            TMO_SLL_Delete (&VpnPolicyList, (t_SLL_NODE *) pVpnPolicy);
            MemReleaseMemBlock (VPN_POLICY_PID, (UINT1 *) pVpnPolicy);

            break;
        default:
            break;
    }
#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnPolicyRowStatus lv;

    lv.cmd = NMH_SET_FS_VPN_POLICY_ROW_STATUS;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4SetValFsVpnPolicyRowStatus = i4SetValFsVpnPolicyRowStatus;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    if (lv.rval == SNMP_FAILURE)
    {
        if (pVpnPolicy != NULL)
        {
            pVpnPolicy->u1VpnPolicyRowStatus = u1PreVpnPolicyRowStatus;
        }
    }
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnPolicyRowStatus, u4SeqNum, TRUE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnPolicyName,
                      i4SetValFsVpnPolicyRowStatus));
    return (SNMP_SUCCESS);
#endif
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsVpnPolicyType
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnPolicyType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnPolicyType (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                          INT4 i4TestValFsVpnPolicyType)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsVpnPolicyType != VPN_IPSEC_MANUAL)
        && (i4TestValFsVpnPolicyType != VPN_IKE_PRESHAREDKEY)
        && (i4TestValFsVpnPolicyType != VPN_IKE_RA_PRESHAREDKEY)
        && (i4TestValFsVpnPolicyType != VPN_IKE_CERTIFICATE)
        && (i4TestValFsVpnPolicyType != VPN_IKE_RA_CERT)
        && (i4TestValFsVpnPolicyType != VPN_IKE_XAUTH_CERT)
        && (i4TestValFsVpnPolicyType != VPN_XAUTH))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    if ((i4TestValFsVpnPolicyType == VPN_IKE_CERTIFICATE)
        || (i4TestValFsVpnPolicyType == VPN_IKE_RA_CERT)
        || (i4TestValFsVpnPolicyType == VPN_IKE_XAUTH_CERT))
    {
        if (pVpnPolicy->u1AuthAlgoType == VPN_NONE)
        {
            *pu4ErrorCode = SNMP_ERR_GEN_ERR;
            return (SNMP_FAILURE);
        }
    }
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnPolicyType lv;

    lv.cmd = NMH_TESTV2_FS_VPN_POLICY_TYPE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnPolicyType = i4TestValFsVpnPolicyType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnCertAlgoType
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnCertAlgoType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnCertAlgoType (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                            INT4 i4TestValFsVpnCertAlgoType)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsVpnCertAlgoType != VPN_AUTH_RSA)
        && (i4TestValFsVpnCertAlgoType != VPN_AUTH_DSA))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnCertAlgoType lv;

    lv.cmd = NMH_TESTV2_FS_VPN_CERT_ALGO_TYPE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnCertAlgoType = i4TestValFsVpnCertAlgoType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnPolicyPriority
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnPolicyPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnPolicyPriority (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                              INT4 i4TestValFsVpnPolicyPriority)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsVpnPolicyPriority < VPN_MIN_PRIORITY)
        || (i4TestValFsVpnPolicyPriority > VPN_MAX_PRIORITY - VPN_ONE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnPolicyPriority lv;

    lv.cmd = NMH_TESTV2_FS_VPN_POLICY_PRIORITY;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnPolicyPriority = i4TestValFsVpnPolicyPriority;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnTunTermAddrType
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnTunTermAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnTunTermAddrType (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                               INT4 i4TestValFsVpnTunTermAddrType)
{

#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    if ((i4TestValFsVpnTunTermAddrType != IPVX_ADDR_FMLY_IPV4) &&
        (i4TestValFsVpnTunTermAddrType != IPVX_ADDR_FMLY_IPV6))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

#else
    int                 rc;
    tNpwnmhTestv2FsVpnTunTermAddrType lv;

    lv.cmd = NMH_TESTV2_FS_VPN_TUN_TERM_ADDR_TYPE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnTunTermAddrType = i4TestValFsVpnTunTermAddrType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnLocalTunTermAddr
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnLocalTunTermAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnLocalTunTermAddr (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsVpnLocalTunTermAddr)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;
    UINT4               u4IpAddr = VPN_ZERO;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pVpnPolicy->LocalTunnTermAddr.u4AddrType != IPVX_ADDR_FMLY_IPV4) &&
        (pVpnPolicy->LocalTunnTermAddr.u4AddrType != IPVX_ADDR_FMLY_IPV6))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (((pVpnPolicy->LocalTunnTermAddr.u4AddrType == IPVX_ADDR_FMLY_IPV4) &&
         (pTestValFsVpnLocalTunTermAddr->i4_Length != IPVX_IPV4_ADDR_LEN)) ||
        ((pVpnPolicy->LocalTunnTermAddr.u4AddrType == IPVX_ADDR_FMLY_IPV6) &&
         (pTestValFsVpnLocalTunTermAddr->i4_Length != IPVX_IPV6_ADDR_LEN)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pVpnPolicy->LocalTunnTermAddr.u4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&u4IpAddr, pTestValFsVpnLocalTunTermAddr->pu1_OctetList,
                sizeof (tIp4Addr));

        if (!((VPN_IS_ADDR_CLASS_A (u4IpAddr)) ||
              (VPN_IS_ADDR_CLASS_B (u4IpAddr)) ||
              (VPN_IS_ADDR_CLASS_C (u4IpAddr))))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnLocalTunTermAddr lv;

    lv.cmd = NMH_TESTV2_FS_VPN_LOCAL_TUN_TERM_ADDR;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pTestValFsVpnLocalTunTermAddr = pTestValFsVpnLocalTunTermAddr;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnRemoteTunTermAddr
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnRemoteTunTermAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnRemoteTunTermAddr (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValFsVpnRemoteTunTermAddr)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    UINT4               u4IpAddr = VPN_ZERO;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pVpnPolicy->RemoteTunnTermAddr.u4AddrType != IPVX_ADDR_FMLY_IPV4) &&
        (pVpnPolicy->RemoteTunnTermAddr.u4AddrType != IPVX_ADDR_FMLY_IPV6))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (((pVpnPolicy->RemoteTunnTermAddr.u4AddrType == IPVX_ADDR_FMLY_IPV4) &&
         (pTestValFsVpnRemoteTunTermAddr->i4_Length != IPVX_IPV4_ADDR_LEN)) ||
        ((pVpnPolicy->RemoteTunnTermAddr.u4AddrType == IPVX_ADDR_FMLY_IPV6) &&
         (pTestValFsVpnRemoteTunTermAddr->i4_Length != IPVX_IPV6_ADDR_LEN)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pVpnPolicy->RemoteTunnTermAddr.u4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&u4IpAddr, pTestValFsVpnRemoteTunTermAddr->pu1_OctetList,
                sizeof (tIp4Addr));

        if (!((VPN_IS_ADDR_CLASS_A (u4IpAddr)) ||
              (VPN_IS_ADDR_CLASS_B (u4IpAddr)) ||
              (VPN_IS_ADDR_CLASS_C (u4IpAddr))))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        if (SecUtilIpIfIsOurAddress (u4IpAddr) == TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }
    }
    return SNMP_SUCCESS;

#else
    int                 rc;
    tNpwnmhTestv2FsVpnRemoteTunTermAddr lv;
    lv.cmd = NMH_TESTV2_FS_VPN_REMOTE_TUN_TERM_ADDR;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pTestValFsVpnRemoteTunTermAddr = pTestValFsVpnRemoteTunTermAddr;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnProtectNetworkType
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnProtectNetworkType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnProtectNetworkType (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                  INT4 i4TestValFsVpnProtectNetworkType)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    if ((i4TestValFsVpnProtectNetworkType != IPVX_ADDR_FMLY_IPV4) &&
        (i4TestValFsVpnProtectNetworkType != IPVX_ADDR_FMLY_IPV6))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;

#else

    int                 rc;
    tNpwnmhTestv2FsVpnProtectNetworkType lv;

    lv.cmd = NMH_TESTV2_FS_VPN_PROTECT_NETWORK_TYPE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnProtectNetworkType = i4TestValFsVpnProtectNetworkType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif

}

/****************************************************************************
 Function    :  nmhTestv2FsVpnLocalProtectNetwork
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnLocalProtectNetwork
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnLocalProtectNetwork (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsVpnLocalProtectNetwork)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    UINT4               u4IpAddr = VPN_ZERO;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pVpnPolicy->LocalProtectNetwork.IpAddr.u4AddrType
         != IPVX_ADDR_FMLY_IPV4) &&
        (pVpnPolicy->LocalProtectNetwork.IpAddr.u4AddrType
         != IPVX_ADDR_FMLY_IPV6))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (((pVpnPolicy->LocalProtectNetwork.IpAddr.u4AddrType
          == IPVX_ADDR_FMLY_IPV4) &&
         (pTestValFsVpnLocalProtectNetwork->i4_Length != IPVX_IPV4_ADDR_LEN)) ||
        ((pVpnPolicy->LocalProtectNetwork.IpAddr.u4AddrType
          == IPVX_ADDR_FMLY_IPV6) &&
         (pTestValFsVpnLocalProtectNetwork->i4_Length != IPVX_IPV6_ADDR_LEN)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pVpnPolicy->LocalProtectNetwork.IpAddr.u4AddrType
        == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&u4IpAddr, pTestValFsVpnLocalProtectNetwork->pu1_OctetList,
                sizeof (tIp4Addr));

        if (!((VPN_IS_ADDR_CLASS_A (u4IpAddr)) ||
              (VPN_IS_ADDR_CLASS_B (u4IpAddr)) ||
              (VPN_IS_ADDR_CLASS_C (u4IpAddr))))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnLocalProtectNetwork lv;
    lv.cmd = NMH_TESTV2_FS_VPN_LOCAL_PROTECT_NETWORK;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pTestValFsVpnLocalProtectNetwork = pTestValFsVpnLocalProtectNetwork;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnLocalProtectNetworkPrefixLen
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnLocalProtectNetworkPrefixLen
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnLocalProtectNetworkPrefixLen (UINT4 *pu4ErrorCode,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsVpnPolicyName,
                                            UINT4
                                            u4TestValFsVpnLocalProtectNetworkPrefixLen)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pVpnPolicy->LocalProtectNetwork.IpAddr.u4AddrType
         != IPVX_ADDR_FMLY_IPV4) &&
        (pVpnPolicy->LocalProtectNetwork.IpAddr.u4AddrType
         != IPVX_ADDR_FMLY_IPV6))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (((pVpnPolicy->LocalProtectNetwork.IpAddr.u4AddrType
          == IPVX_ADDR_FMLY_IPV4) &&
         (u4TestValFsVpnLocalProtectNetworkPrefixLen > IPVX_IPV4_MAX_MASK_LEN))
        || ((pVpnPolicy->LocalProtectNetwork.IpAddr.u4AddrType
             == IPVX_ADDR_FMLY_IPV6)
            && (u4TestValFsVpnLocalProtectNetworkPrefixLen >
                IP6_ADDR_MAX_PREFIX)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnLocalProtectNetworkPrefixLen lv;
    lv.cmd = NMH_TESTV2_FS_VPN_LOCAL_PROTECT_NETWORK_PREFIX_LEN;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.u4TestValFsVpnLocalProtectNetworkPrefixLen =
        u4TestValFsVpnLocalProtectNetworkPrefixLen;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnRemoteProtectNetwork
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnRemoteProtectNetwork
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnRemoteProtectNetwork (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValFsVpnRemoteProtectNetwork)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    UINT4               u4IpAddr = VPN_ZERO;
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pVpnPolicy->RemoteProtectNetwork.IpAddr.u4AddrType
         != IPVX_ADDR_FMLY_IPV4) &&
        (pVpnPolicy->RemoteProtectNetwork.IpAddr.u4AddrType
         != IPVX_ADDR_FMLY_IPV6))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (((pVpnPolicy->RemoteProtectNetwork.IpAddr.u4AddrType
          == IPVX_ADDR_FMLY_IPV4) &&
         (pTestValFsVpnRemoteProtectNetwork->i4_Length != IPVX_IPV4_ADDR_LEN))
        ||
        ((pVpnPolicy->RemoteProtectNetwork.IpAddr.u4AddrType ==
          IPVX_ADDR_FMLY_IPV6)
         && (pTestValFsVpnRemoteProtectNetwork->i4_Length !=
             IPVX_IPV6_ADDR_LEN)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pVpnPolicy->RemoteProtectNetwork.IpAddr.u4AddrType
        == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&u4IpAddr, pTestValFsVpnRemoteProtectNetwork->pu1_OctetList,
                sizeof (tIp4Addr));

        if (!((VPN_IS_ADDR_CLASS_A (u4IpAddr)) ||
              (VPN_IS_ADDR_CLASS_B (u4IpAddr)) ||
              (VPN_IS_ADDR_CLASS_C (u4IpAddr))))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

#else
    int                 rc;
    tNpwnmhTestv2FsVpnRemoteProtectNetwork lv;
    lv.cmd = NMH_TESTV2_FS_VPN_REMOTE_PROTECT_NETWORK;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pTestValFsVpnRemoteProtectNetwork = pTestValFsVpnRemoteProtectNetwork;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnRemoteProtectNetworkPrefixLen
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnRemoteProtectNetworkPrefixLen
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnRemoteProtectNetworkPrefixLen (UINT4 *pu4ErrorCode,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsVpnPolicyName,
                                             UINT4
                                             u4TestValFsVpnRemoteProtectNetworkPrefixLen)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pVpnPolicy->RemoteProtectNetwork.IpAddr.u4AddrType
         != IPVX_ADDR_FMLY_IPV4) &&
        (pVpnPolicy->RemoteProtectNetwork.IpAddr.u4AddrType
         != IPVX_ADDR_FMLY_IPV6))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (((pVpnPolicy->RemoteProtectNetwork.IpAddr.u4AddrType
          == IPVX_ADDR_FMLY_IPV4) &&
         (u4TestValFsVpnRemoteProtectNetworkPrefixLen > IPVX_IPV4_MAX_MASK_LEN))
        || ((pVpnPolicy->RemoteProtectNetwork.IpAddr.u4AddrType
             == IPVX_ADDR_FMLY_IPV6)
            && (u4TestValFsVpnRemoteProtectNetworkPrefixLen >
                IP6_ADDR_MAX_PREFIX)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

#else
    int                 rc;
    tNpwnmhTestv2FsVpnRemoteProtectNetworkPrefixLen lv;
    lv.cmd = NMH_TESTV2_FS_VPN_REMOTE_PROTECT_NETWORK_PREFIX_LEN;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.u4TestValFsVpnRemoteProtectNetworkPrefixLen =
        u4TestValFsVpnRemoteProtectNetworkPrefixLen;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnIkeSrcPortRange
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnIkeSrcPortRange
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnIkeSrcPortRange (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsVpnIkeSrcPortRange)
{
    tVpnPolicy         *pVpnPolicy = NULL;
    UINT1              *pu1SrcPrtRange = NULL;
    UINT1              *pu1Tmp = NULL;
    UINT1               u1NoOfDel = VPN_ZERO;
#ifndef SECURITY_KERNEL_MAKE_WANTED
    UINT1              *pu1Ptr = NULL;
    UINT4               u4RangeLimit = VPN_ZERO;
#endif

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if ((pTestValFsVpnIkeSrcPortRange->i4_Length < VPN_ZERO) ||
        (pTestValFsVpnIkeSrcPortRange->i4_Length > VPN_PORT_RANGE_LENGTH))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    pu1Tmp = pTestValFsVpnIkeSrcPortRange->pu1_OctetList;
    pu1SrcPrtRange = pu1Tmp;

    /* Verify that the first character and last one is not "-" */
    if ((*pu1Tmp == '-') || (*(pu1Tmp + (STRLEN (pu1Tmp) - VPN_ONE)) == '-'))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    /* Verify that there are no characters other than '-' and digits (0-9)
       and only one '-' is present. */
    while (*pu1Tmp != '\0')
    {
        if (VPN_FALSE != ISDIGIT (*pu1Tmp))
        {
            pu1Tmp++;
        }
        else if (*pu1Tmp == '-')
        {
            u1NoOfDel++;
            pu1Tmp++;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
        }
    }
    /* There should be only one range delimiter '-' in the string */
    if (u1NoOfDel > VPN_ONE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
#ifndef SECURITY_KERNEL_MAKE_WANTED
    /* Verify that the range end limit is not more than 65535 */
    pu1Ptr = (UINT1 *) STRSTR (pu1SrcPrtRange, "-");
    if (pu1Ptr != NULL)
    {
        pu1Ptr = pu1Ptr + VPN_ONE;
        u4RangeLimit = SecUtilAtoL ((CONST CHR1 *) pu1Ptr);
        if (u4RangeLimit > VPN_MAX_SRC_DST_PORT_VALUE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
        }
    }
    else
    {
        u4RangeLimit = SecUtilAtoL ((CONST CHR1 *) pu1SrcPrtRange);
        if (u4RangeLimit > VPN_MAX_SRC_DST_PORT_VALUE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
        }
    }
#endif

#ifndef SECURITY_KERNEL_MAKE_WANTED
    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhTestv2FsVpnIkeSrcPortRange lv;

    lv.cmd = NMH_TESTV2_FS_VPN_IKE_SRC_PORT_RANGE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pTestValFsVpnIkeSrcPortRange = pTestValFsVpnIkeSrcPortRange;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnIkeDstPortRange
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnIkeDstPortRange
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnIkeDstPortRange (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsVpnIkeDstPortRange)
{
    tVpnPolicy         *pVpnPolicy = NULL;
    UINT1              *pu1Tmp = NULL;
    UINT1              *pu1SrcPrtRange = NULL;
    UINT1               u1NoOfDel = VPN_ZERO;
#ifndef SECURITY_KERNEL_MAKE_WANTED
    UINT1              *pu1Ptr = NULL;
    UINT4               u4RangeLimit = VPN_ZERO;
#endif

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if ((pTestValFsVpnIkeDstPortRange->i4_Length < VPN_ZERO) ||
        (pTestValFsVpnIkeDstPortRange->i4_Length > VPN_PORT_RANGE_LENGTH))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    pu1Tmp = pTestValFsVpnIkeDstPortRange->pu1_OctetList;
    pu1SrcPrtRange = pu1Tmp;

    if ((*pu1Tmp == '-') || (*(pu1Tmp + (STRLEN (pu1Tmp) - VPN_ONE)) == '-'))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    while (*pu1Tmp != '\0')
    {
        if (VPN_FALSE != ISDIGIT (*pu1Tmp))
        {
            pu1Tmp++;
        }
        else if (*pu1Tmp == '-')
        {
            u1NoOfDel++;
            pu1Tmp++;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
        }
    }

    /* There should be only one range delimiter '-' in the string */
    if (u1NoOfDel > VPN_ONE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
#ifndef SECURITY_KERNEL_MAKE_WANTED
    /* Verify that the range end limit is not more than 65535 */
    pu1Ptr = (UINT1 *) STRSTR (pu1SrcPrtRange, "-");
    if (pu1Ptr != NULL)
    {
        pu1Ptr = pu1Ptr + VPN_ONE;
        u4RangeLimit = SecUtilAtoL ((CONST CHR1 *) pu1Ptr);
        if (u4RangeLimit > VPN_MAX_SRC_DST_PORT_VALUE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
        }
    }
    else
    {
        u4RangeLimit = SecUtilAtoL ((CONST CHR1 *) pu1SrcPrtRange);
        if (u4RangeLimit > VPN_MAX_SRC_DST_PORT_VALUE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
        }
    }
#endif

#ifndef SECURITY_KERNEL_MAKE_WANTED
    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhTestv2FsVpnIkeDstPortRange lv;

    lv.cmd = NMH_TESTV2_FS_VPN_IKE_DST_PORT_RANGE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pTestValFsVpnIkeDstPortRange = pTestValFsVpnIkeDstPortRange;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnSecurityProtocol
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnSecurityProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnSecurityProtocol (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                INT4 i4TestValFsVpnSecurityProtocol)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsVpnSecurityProtocol != VPN_AH)
        && (i4TestValFsVpnSecurityProtocol != VPN_ESP))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnSecurityProtocol lv;

    lv.cmd = NMH_TESTV2_FS_VPN_SECURITY_PROTOCOL;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnSecurityProtocol = i4TestValFsVpnSecurityProtocol;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnInboundSpi
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnInboundSpi
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnInboundSpi (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                          INT4 i4TestValFsVpnInboundSpi)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE) ||
        (VPN_POLICY_TYPE (pVpnPolicy) != VPN_IPSEC_MANUAL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((UINT4) i4TestValFsVpnInboundSpi <= VPN_MIN_SPI)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnInboundSpi lv;

    lv.cmd = NMH_TESTV2_FS_VPN_INBOUND_SPI;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnInboundSpi = i4TestValFsVpnInboundSpi;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnOutboundSpi
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnOutboundSpi
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnOutboundSpi (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                           INT4 i4TestValFsVpnOutboundSpi)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE) ||
        (VPN_POLICY_TYPE (pVpnPolicy) != VPN_IPSEC_MANUAL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((UINT4) i4TestValFsVpnOutboundSpi <= VPN_MIN_SPI)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

#else
    int                 rc;
    tNpwnmhTestv2FsVpnOutboundSpi lv;

    lv.cmd = NMH_TESTV2_FS_VPN_OUTBOUND_SPI;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnOutboundSpi = i4TestValFsVpnOutboundSpi;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnMode
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnMode (UINT4 *pu4ErrorCode,
                    tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                    INT4 i4TestValFsVpnMode)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsVpnMode != VPN_TRANSPORT)
        && (i4TestValFsVpnMode != VPN_TUNNEL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnMode lv;

    lv.cmd = NMH_TESTV2_FS_VPN_MODE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnMode = i4TestValFsVpnMode;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnAuthAlgo
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnAuthAlgo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnAuthAlgo (UINT4 *pu4ErrorCode,
                        tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                        INT4 i4TestValFsVpnAuthAlgo)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsVpnAuthAlgo != VPN_HMACMD5)
        && (i4TestValFsVpnAuthAlgo != VPN_HMACSHA1)
        && (i4TestValFsVpnAuthAlgo != SEC_XCBCMAC)
        && (i4TestValFsVpnAuthAlgo != HMAC_SHA_256)
        && (i4TestValFsVpnAuthAlgo != HMAC_SHA_384)
        && (i4TestValFsVpnAuthAlgo != HMAC_SHA_512))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#ifndef SECURITY_KERNEL_WANTED
    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        if (i4TestValFsVpnAuthAlgo == VPN_HMACMD5)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnAuthAlgo lv;

    lv.cmd = NMH_TESTV2_FS_VPN_AUTH_ALGO;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnAuthAlgo = i4TestValFsVpnAuthAlgo;

    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        if (i4TestValFsVpnAuthAlgo == VPN_HMACMD5)
        {
            return SNMP_FAILURE;
        }
    }

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnAhKey
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnAhKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnAhKey (UINT4 *pu4ErrorCode,
                     tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                     tSNMP_OCTET_STRING_TYPE * pTestValFsVpnAhKey)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;
    INT4                i4Index = VPN_ZERO;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE) ||
        (VPN_POLICY_TYPE (pVpnPolicy) != VPN_IPSEC_MANUAL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (pVpnPolicy->uVpnKeyMode.IpsecManualKey.u1VpnAuthAlgo)
    {
        case VPN_HMACMD5:

            if (pTestValFsVpnAhKey->i4_Length != VPN_HMACMD5_LEN)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return (SNMP_FAILURE);
            }
            break;

        case VPN_HMACSHA1:
            if ((pTestValFsVpnAhKey->i4_Length != VPN_HMACSHA1_LEN))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return (SNMP_FAILURE);
            }
            break;
        case SEC_XCBCMAC:
            if ((pTestValFsVpnAhKey->i4_Length != VPN_XCBCMAC_LEN))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return (SNMP_FAILURE);
            }
            break;
        case HMAC_SHA_256:
            if ((pTestValFsVpnAhKey->i4_Length != VPN_SHA2_256_LEN))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return (SNMP_FAILURE);
            }
            break;

        case HMAC_SHA_384:
            if ((pTestValFsVpnAhKey->i4_Length != VPN_SHA2_384_LEN))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return (SNMP_FAILURE);
            }
            break;
        case HMAC_SHA_512:
            if ((pTestValFsVpnAhKey->i4_Length != VPN_SHA2_512_LEN))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return (SNMP_FAILURE);
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
    }

    for (i4Index = VPN_ZERO; i4Index < pTestValFsVpnAhKey->i4_Length; i4Index++)
    {
        if (!IF_CHAR_IS_HEX (pTestValFsVpnAhKey->pu1_OctetList[i4Index]))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhTestv2FsVpnAhKey lv;

    lv.cmd = NMH_TESTV2_FS_VPN_AH_KEY;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pTestValFsVpnAhKey = pTestValFsVpnAhKey;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnEncrAlgo
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnEncrAlgo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnEncrAlgo (UINT4 *pu4ErrorCode,
                        tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                        INT4 i4TestValFsVpnEncrAlgo)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pVpnPolicy->u4VpnSecurityProtocol == VPN_AH)
    {
        *pu4ErrorCode = SNMP_ERR_COMMIT_FAILED;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsVpnEncrAlgo != VPN_DES_CBC)
        && (i4TestValFsVpnEncrAlgo != VPN_3DES_CBC)
        && (i4TestValFsVpnEncrAlgo != VPN_AES_128)
        && (i4TestValFsVpnEncrAlgo != VPN_AES_192)
        && (i4TestValFsVpnEncrAlgo != VPN_AES_256))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#ifndef SECURITY_KERNEL_WANTED
    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        if (i4TestValFsVpnEncrAlgo == VPN_DES_CBC)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;

#else
    int                 rc;
    tNpwnmhTestv2FsVpnEncrAlgo lv;

    lv.cmd = NMH_TESTV2_FS_VPN_ENCR_ALGO;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnEncrAlgo = i4TestValFsVpnEncrAlgo;

    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        if (i4TestValFsVpnEncrAlgo == VPN_DES_CBC)
        {
            return SNMP_FAILURE;
        }
    }

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnEspKey
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnEspKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnEspKey (UINT4 *pu4ErrorCode,
                      tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                      tSNMP_OCTET_STRING_TYPE * pTestValFsVpnEspKey)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED

    tVpnPolicy         *pVpnPolicy = NULL;
    UINT4               u4Len = VPN_ZERO;
    INT4                i4Index = VPN_ZERO;
    UINT1               au1TdesKey[VPN_DES_KEY_MAX][VPN_ESP_DES_KEY_LEN +
                                                    VPN_ONE];

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE) ||
        (VPN_POLICY_TYPE (pVpnPolicy) != VPN_IPSEC_MANUAL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pVpnPolicy->u4VpnSecurityProtocol == VPN_AH) ||
        (pVpnPolicy->uVpnKeyMode.IpsecManualKey.u1VpnEncryptionAlgo ==
         VPN_NULLESPALGO))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    switch (pVpnPolicy->uVpnKeyMode.IpsecManualKey.u1VpnEncryptionAlgo)
    {
        case VPN_DES_CBC:
            u4Len = VPN_ESP_DES_KEY_LEN;
            break;
        case VPN_3DES_CBC:
            /* Extra 2 is for seperators; Expecting the same format from
             * snmp also i.e keys seperated by some delimeter for example "."
             */
            u4Len = VPN_ESP_3DES_KEY_LEN + VPN_TWO;
            break;
        case VPN_AES_128:
            u4Len = VPN_ESP_AES_KEY1_LEN;
            break;
        case VPN_AES_192:
            u4Len = VPN_ESP_AES_KEY2_LEN;
            break;
        case VPN_AES_256:
            u4Len = VPN_ESP_AES_KEY3_LEN;
            break;
        default:
            u4Len = VPN_ZERO;
    }

    if (u4Len != (UINT4) pTestValFsVpnEspKey->i4_Length)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return (SNMP_FAILURE);
    }

    for (i4Index = VPN_ZERO; i4Index < pTestValFsVpnEspKey->i4_Length;
         i4Index++)
    {
        if ((i4Index == ESP_FIRST_KEY_TERMINATION) ||
            (i4Index == ESP_SECOND_KEY_TERMINATION))
        {
            /* Dont check the (3DES) key delimeters */
            continue;
        }

        if (!IF_CHAR_IS_HEX (pTestValFsVpnEspKey->pu1_OctetList[i4Index]))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (VPN_3DES_CBC ==
        pVpnPolicy->uVpnKeyMode.IpsecManualKey.u1VpnEncryptionAlgo)
    {
        memset (au1TdesKey[VPN_INDEX_0], VPN_ZERO,
                sizeof (au1TdesKey[VPN_INDEX_0]));
        STRNCPY (au1TdesKey[VPN_INDEX_0], pTestValFsVpnEspKey->pu1_OctetList,
                 VPN_ESP_DES_KEY_LEN);
        memset (au1TdesKey[VPN_INDEX_1], VPN_ZERO,
                sizeof (au1TdesKey[VPN_INDEX_1]));
        /* copying after the 1st key and its delimiter */
        STRNCPY (au1TdesKey[VPN_INDEX_1],
                 pTestValFsVpnEspKey->pu1_OctetList + VPN_ONE +
                 VPN_ESP_DES_KEY_LEN, VPN_ESP_DES_KEY_LEN);
        memset (au1TdesKey[VPN_INDEX_2], VPN_ZERO,
                sizeof (au1TdesKey[VPN_INDEX_2]));
        /* copying after the 2nd key and 2 delimiters */
        STRNCPY (au1TdesKey[VPN_INDEX_2],
                 pTestValFsVpnEspKey->pu1_OctetList + VPN_TWO +
                 VPN_TWO * VPN_ESP_DES_KEY_LEN, VPN_ESP_DES_KEY_LEN);

        if ((STRCMP (au1TdesKey[VPN_INDEX_0],
                     au1TdesKey[VPN_INDEX_1]) == VPN_ZERO) ||
            (STRCMP (au1TdesKey[VPN_INDEX_1],
                     au1TdesKey[VPN_INDEX_2]) == VPN_ZERO) ||
            (STRCMP (au1TdesKey[VPN_INDEX_0],
                     au1TdesKey[VPN_INDEX_2]) == VPN_ZERO))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhTestv2FsVpnEspKey lv;

    lv.cmd = NMH_TESTV2_FS_VPN_ESP_KEY;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pTestValFsVpnEspKey = pTestValFsVpnEspKey;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnAntiReplay
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnAntiReplay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnAntiReplay (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                          INT4 i4TestValFsVpnAntiReplay)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsVpnAntiReplay != VPN_ANTI_REPLAY_ENABLE)
        && (i4TestValFsVpnAntiReplay != VPN_ANTI_REPLAY_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnAntiReplay lv;

    lv.cmd = NMH_TESTV2_FS_VPN_ANTI_REPLAY;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnAntiReplay = i4TestValFsVpnAntiReplay;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnPolicyFlag
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnPolicyFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnPolicyFlag (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                          INT4 i4TestValFsVpnPolicyFlag)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsVpnPolicyFlag != VPN_APPLY)
        && (i4TestValFsVpnPolicyFlag != VPN_BYPASS)
        && (i4TestValFsVpnPolicyFlag != VPN_FILTER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

#else
    int                 rc;
    tNpwnmhTestv2FsVpnPolicyFlag lv;

    lv.cmd = NMH_TESTV2_FS_VPN_POLICY_FLAG;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnPolicyFlag = i4TestValFsVpnPolicyFlag;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnProtocol
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnProtocol (UINT4 *pu4ErrorCode,
                        tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                        INT4 i4TestValFsVpnProtocol)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    switch (i4TestValFsVpnProtocol)
    {
        case VPN_TCP:
        case VPN_UDP:
        case VPN_AH:
        case VPN_ESP:
        case VPN_ICMP:
        case VPN_ANY_PROTOCOL:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhTestv2FsVpnProtocol lv;

    lv.cmd = NMH_TESTV2_FS_VPN_PROTOCOL;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnProtocol = i4TestValFsVpnProtocol;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnPolicyIntfIndex
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnPolicyIntfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnPolicyIntfIndex (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                               INT4 i4TestValFsVpnPolicyIntfIndex)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tIp6Addr            NullIp6Addr;
    tIp6Addr           *pIp6Addr = NULL;
    tVpnPolicy         *pVpnPolicy = NULL;
    tVpnPolicy         *pTempVpnPolicy = NULL;
    UINT4               u4WanIpAddr = VPN_ZERO;
    UINT4               u4HLIndex = VPN_ZERO;
    UINT1               u1NetworkType = VPN_ZERO;
    UINT4               u4VpnIfIndex = (UINT4) i4TestValFsVpnPolicyIntfIndex;

    MEMSET (&NullIp6Addr, VPN_ZERO, sizeof (tIp6Addr));
    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }
    if (SecUtilGetIfNwType (u4VpnIfIndex, &u1NetworkType) == CFA_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_WAN_TYPE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }
    if (u1NetworkType != CFA_NETWORK_TYPE_WAN)
    {
        /* Interface is not valid/is trusted port */
        CLI_SET_ERR (CLI_VPN_ERR_WAN_TYPE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if (pVpnPolicy->RemoteTunnTermAddr.u4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if (CfaGetIfIpAddr ((INT4) u4VpnIfIndex, &u4WanIpAddr) == OSIX_FAILURE)
        {
            /* Interface is not valid/is trusted port */
            CLI_SET_ERR (CLI_VPN_ERR_WAN_IP);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }

        if (u4WanIpAddr == VPN_ZERO)
        {
            /* No ip address exist for the interface */
            CLI_SET_ERR (CLI_VPN_ERR_WAN_IP);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }
    }
    else if (pVpnPolicy->RemoteTunnTermAddr.u4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        pIp6Addr = Sec6UtilGetGlobalAddr (u4VpnIfIndex, NULL);
        if ((pIp6Addr == NULL) || (Ip6AddrMatch (&NullIp6Addr, pIp6Addr,
                                                 sizeof (tIp6Addr)) == TRUE))
        {
            CLI_SET_ERR (CLI_VPN_ERR_WAN_IP);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);
        }
    }

    if (CfaGetHLIfIndex (u4VpnIfIndex, &u4HLIndex) == CFA_SUCCESS)
    {
        CLI_SET_ERR (CLI_VPN_ERR_MP_TYPE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return (SNMP_FAILURE);
    }

    /* rejecting configurations with more than one policy to the same peer */
    SLL_SCAN (&VpnPolicyList, pTempVpnPolicy, tVpnPolicy *)
    {
        if ((pTempVpnPolicy->u1VpnPolicyRowStatus == ACTIVE) &&
            (pTempVpnPolicy->u4IfIndex == u4VpnIfIndex) &&
            (pTempVpnPolicy->RemoteTunnTermAddr.u4AddrType ==
             pVpnPolicy->RemoteTunnTermAddr.u4AddrType))
        {
            if (pVpnPolicy->RemoteTunnTermAddr.u4AddrType ==
                IPVX_ADDR_FMLY_IPV4)
            {
                if (MEMCMP
                    (&(pTempVpnPolicy->RemoteTunnTermAddr.uIpAddr.Ip4Addr),
                     &(pVpnPolicy->RemoteTunnTermAddr.uIpAddr.Ip4Addr),
                     IPVX_IPV4_ADDR_LEN) == 0)
                {
                    CLI_SET_ERR (CLI_VPN_ERR_REDUNDANT_POLICY_TO_PEER);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }
            }
            else
            {
                if (MEMCMP
                    (&(pTempVpnPolicy->RemoteTunnTermAddr.uIpAddr.Ip6Addr),
                     &(pVpnPolicy->RemoteTunnTermAddr.uIpAddr.Ip6Addr),
                     IPVX_IPV6_ADDR_LEN) == 0)
                {
                    CLI_SET_ERR (CLI_VPN_ERR_REDUNDANT_POLICY_TO_PEER);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return (SNMP_FAILURE);
                }
            }
        }
    }

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnPolicyIntfIndex lv;

    lv.cmd = NMH_TESTV2_FS_VPN_POLICY_INTF_INDEX;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnPolicyIntfIndex = i4TestValFsVpnPolicyIntfIndex;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnIkePhase1HashAlgo
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnIkePhase1HashAlgo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnIkePhase1HashAlgo (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                 INT4 i4TestValFsVpnIkePhase1HashAlgo)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE) ||
        (VPN_POLICY_TYPE (pVpnPolicy) == VPN_IPSEC_MANUAL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsVpnIkePhase1HashAlgo != VPN_HMACMD5) &&
        (i4TestValFsVpnIkePhase1HashAlgo != VPN_HMACSHA1) &&
        (i4TestValFsVpnIkePhase1HashAlgo != HMAC_SHA_256) &&
        (i4TestValFsVpnIkePhase1HashAlgo != HMAC_SHA_384) &&
        (i4TestValFsVpnIkePhase1HashAlgo != HMAC_SHA_512))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

#ifndef SECURITY_KERNEL_WANTED
    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        if (i4TestValFsVpnIkePhase1HashAlgo == VPN_HMACMD5)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnIkePhase1HashAlgo lv;

    lv.cmd = NMH_TESTV2_FS_VPN_IKE_PHASE1_HASH_ALGO;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnIkePhase1HashAlgo = i4TestValFsVpnIkePhase1HashAlgo;

    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        if (i4TestValFsVpnIkePhase1HashAlgo == VPN_HMACMD5)
        {
            return SNMP_FAILURE;
        }
    }

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnIkePhase1EncryptionAlgo
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnIkePhase1EncryptionAlgo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnIkePhase1EncryptionAlgo (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsVpnPolicyName,
                                       INT4
                                       i4TestValFsVpnIkePhase1EncryptionAlgo)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE) ||
        (VPN_POLICY_TYPE (pVpnPolicy) == VPN_IPSEC_MANUAL))
    {
        CLI_SET_ERR (CLI_VPN_ERR_VPN_MODE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsVpnIkePhase1EncryptionAlgo != VPN_DES_CBC) &&
        (i4TestValFsVpnIkePhase1EncryptionAlgo != VPN_3DES_CBC) &&
        (i4TestValFsVpnIkePhase1EncryptionAlgo != VPN_AES_128) &&
        (i4TestValFsVpnIkePhase1EncryptionAlgo != VPN_AES_192) &&
        (i4TestValFsVpnIkePhase1EncryptionAlgo != VPN_AES_256))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

#ifndef SECURITY_KERNEL_WANTED
    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        if (i4TestValFsVpnIkePhase1EncryptionAlgo == VPN_DES_CBC)
        {
            return SNMP_FAILURE;
        }
    }
#endif
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnIkePhase1EncryptionAlgo lv;

    lv.cmd = NMH_TESTV2_FS_VPN_IKE_PHASE1_ENCRYPTION_ALGO;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnIkePhase1EncryptionAlgo =
        i4TestValFsVpnIkePhase1EncryptionAlgo;

    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        if (i4TestValFsVpnIkePhase1EncryptionAlgo == VPN_DES_CBC)
        {
            return SNMP_FAILURE;
        }
    }

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnIkePhase1DHGroup
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnIkePhase1DHGroup
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnIkePhase1DHGroup (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                INT4 i4TestValFsVpnIkePhase1DHGroup)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE) ||
        (VPN_POLICY_TYPE (pVpnPolicy) == VPN_IPSEC_MANUAL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsVpnIkePhase1DHGroup != VPN_DH_GROUP1) &&
        (i4TestValFsVpnIkePhase1DHGroup != VPN_DH_GROUP2) &&
        (i4TestValFsVpnIkePhase1DHGroup != VPN_DH_GROUP5) &&
        (i4TestValFsVpnIkePhase1DHGroup != SEC_DH_GROUP_14))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);

    }

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnIkePhase1DHGroup lv;

    lv.cmd = NMH_TESTV2_FS_VPN_IKE_PHASE1_D_H_GROUP;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnIkePhase1DHGroup = i4TestValFsVpnIkePhase1DHGroup;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnIkePhase1LocalIdType
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnIkePhase1LocalIdType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnIkePhase1LocalIdType (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                    INT4 i4TestValFsVpnIkePhase1LocalIdType)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE) ||
        (VPN_POLICY_TYPE (pVpnPolicy) == VPN_IPSEC_MANUAL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    switch (i4TestValFsVpnIkePhase1LocalIdType)
    {
        case VPN_ID_TYPE_IPV4:
        case VPN_ID_TYPE_EMAIL:
        case VPN_ID_TYPE_FQDN:
        case VPN_ID_TYPE_IPV6:
        case VPN_ID_TYPE_DN:
        case VPN_ID_TYPE_KEYID:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhTestv2FsVpnIkePhase1LocalIdType lv;

    lv.cmd = NMH_TESTV2_FS_VPN_IKE_PHASE1_LOCAL_ID_TYPE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnIkePhase1LocalIdType = i4TestValFsVpnIkePhase1LocalIdType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnIkePhase1LocalIdValue
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnIkePhase1LocalIdValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnIkePhase1LocalIdValue (UINT4 *pu4ErrorCode,
                                     tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValFsVpnIkePhase1LocalIdValue)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tIp6Addr            Ip6Addr;
    tVpnPolicy         *pVpnPolicy = NULL;
    UINT4               u4TempIpAddr = VPN_ZERO;
    INT1                i1RetVal = SNMP_SUCCESS;
    UINT1               au1IpAddr[VPN_IP_ADDR_LEN + VPN_ONE] = { VPN_ZERO };

    MEMSET (&Ip6Addr, VPN_ZERO, sizeof (tIp6Addr));

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE) ||
        (VPN_POLICY_TYPE (pVpnPolicy) == VPN_IPSEC_MANUAL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    switch (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyLocalID.i2IdType)
    {
        case VPN_ID_TYPE_IPV4:
            if (pTestValFsVpnIkePhase1LocalIdValue->i4_Length > VPN_IP_ADDR_LEN)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            STRNCPY (au1IpAddr,
                     pTestValFsVpnIkePhase1LocalIdValue->pu1_OctetList,
                     (size_t) pTestValFsVpnIkePhase1LocalIdValue->i4_Length);
            au1IpAddr[pTestValFsVpnIkePhase1LocalIdValue->i4_Length] = '\0';
            u4TempIpAddr = OSIX_NTOHL (INET_ADDR (au1IpAddr));
            if (VpnUtilValidateIpAddress (u4TempIpAddr) != VPN_UCAST_ADDR)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case VPN_ID_TYPE_EMAIL:

            i1RetVal = (INT1)
                VpnUtilValidateEmailAddr (pTestValFsVpnIkePhase1LocalIdValue);
            if (i1RetVal == SNMP_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            }
            break;

        case VPN_ID_TYPE_FQDN:
            if (VpnUtilsIsFqdn
                ((CHR1 *) pTestValFsVpnIkePhase1LocalIdValue->pu1_OctetList,
                 pTestValFsVpnIkePhase1LocalIdValue->i4_Length) == FALSE)
            {
                i1RetVal = SNMP_FAILURE;
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            }

            break;

        case VPN_ID_TYPE_IPV6:
            if (pTestValFsVpnIkePhase1LocalIdValue->i4_Length >
                VPN_IPV6_ADDR_LEN)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            if (INET_ATON6 (pTestValFsVpnIkePhase1LocalIdValue->pu1_OctetList,
                            &Ip6Addr) == VPN_ZERO)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            if (Ip6AddrType (&Ip6Addr) != ADDR6_UNICAST)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        case VPN_ID_TYPE_DN:

            break;
        case VPN_ID_TYPE_KEYID:

            break;

        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            i1RetVal = SNMP_FAILURE;
            break;
    }

    return i1RetVal;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnIkePhase1LocalIdValue lv;

    lv.cmd = NMH_TESTV2_FS_VPN_IKE_PHASE1_LOCAL_ID_VALUE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pTestValFsVpnIkePhase1LocalIdValue = pTestValFsVpnIkePhase1LocalIdValue;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnIkePhase1PeerIdType
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnIkePhase1PeerIdType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnIkePhase1PeerIdType (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                   INT4 i4TestValFsVpnIkePhase1PeerIdType)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE) ||
        (VPN_POLICY_TYPE (pVpnPolicy) == VPN_IPSEC_MANUAL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsVpnIkePhase1PeerIdType != VPN_ID_TYPE_IPV4) &&
        (i4TestValFsVpnIkePhase1PeerIdType != VPN_ID_TYPE_EMAIL) &&
        (i4TestValFsVpnIkePhase1PeerIdType != VPN_ID_TYPE_FQDN) &&
        (i4TestValFsVpnIkePhase1PeerIdType != VPN_ID_TYPE_IPV6) &&
        (i4TestValFsVpnIkePhase1PeerIdType != VPN_ID_TYPE_DN) &&
        (i4TestValFsVpnIkePhase1PeerIdType != VPN_ID_TYPE_KEYID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnIkePhase1PeerIdType lv;

    lv.cmd = NMH_TESTV2_FS_VPN_IKE_PHASE1_PEER_ID_TYPE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnIkePhase1PeerIdType = i4TestValFsVpnIkePhase1PeerIdType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnIkePhase1PeerIdValue
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnIkePhase1PeerIdValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnIkePhase1PeerIdValue (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValFsVpnIkePhase1PeerIdValue)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tIp6Addr            Ip6Addr;
    tVpnPolicy         *pVpnPolicy = NULL;
    UINT4               u4TempIpAddr = VPN_ZERO;
    INT1                i1RetVal = SNMP_SUCCESS;
    UINT1               au1IpAddr[VPN_IP_ADDR_LEN + VPN_ONE] = { VPN_ZERO };

    MEMSET (&Ip6Addr, VPN_ZERO, sizeof (tIp6Addr));

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE) ||
        (VPN_POLICY_TYPE (pVpnPolicy) == VPN_IPSEC_MANUAL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    switch (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.i2IdType)
    {
        case VPN_ID_TYPE_IPV4:
            if (pTestValFsVpnIkePhase1PeerIdValue->i4_Length > VPN_IP_ADDR_LEN)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
            }
            STRNCPY (au1IpAddr,
                     pTestValFsVpnIkePhase1PeerIdValue->pu1_OctetList,
                     (size_t) pTestValFsVpnIkePhase1PeerIdValue->i4_Length);
            au1IpAddr[pTestValFsVpnIkePhase1PeerIdValue->i4_Length] = '\0';
            u4TempIpAddr = OSIX_NTOHL (INET_ADDR (au1IpAddr));
            if (VpnUtilValidateIpAddress (u4TempIpAddr) != VPN_UCAST_ADDR)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case VPN_ID_TYPE_EMAIL:
            i1RetVal = (INT1)
                VpnUtilValidateEmailAddr (pTestValFsVpnIkePhase1PeerIdValue);
            if (i1RetVal == SNMP_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            }
            break;

        case VPN_ID_TYPE_FQDN:
            if (VpnUtilsIsFqdn
                ((CHR1 *) pTestValFsVpnIkePhase1PeerIdValue->pu1_OctetList,
                 pTestValFsVpnIkePhase1PeerIdValue->i4_Length) == FALSE)
            {
                i1RetVal = SNMP_FAILURE;
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            }

            break;

        case VPN_ID_TYPE_IPV6:

            if (pTestValFsVpnIkePhase1PeerIdValue->i4_Length >
                VPN_IPV6_ADDR_LEN)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            if (INET_ATON6 (pTestValFsVpnIkePhase1PeerIdValue->pu1_OctetList,
                            &Ip6Addr) == VPN_ZERO)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            if (Ip6AddrType (&Ip6Addr) != ADDR6_UNICAST)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;

        case VPN_ID_TYPE_DN:
            break;
        case VPN_ID_TYPE_KEYID:

            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            i1RetVal = SNMP_FAILURE;
            break;

    }

    return i1RetVal;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnIkePhase1PeerIdValue lv;

    lv.cmd = NMH_TESTV2_FS_VPN_IKE_PHASE1_PEER_ID_VALUE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.pTestValFsVpnIkePhase1PeerIdValue = pTestValFsVpnIkePhase1PeerIdValue;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnIkePhase1LifeTimeType
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnIkePhase1LifeTimeType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnIkePhase1LifeTimeType (UINT4 *pu4ErrorCode,
                                     tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                     INT4 i4TestValFsVpnIkePhase1LifeTimeType)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE) ||
        (VPN_POLICY_TYPE (pVpnPolicy) == VPN_IPSEC_MANUAL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsVpnIkePhase1LifeTimeType != VPN_LIFE_TYPE_SECS) &&
        (i4TestValFsVpnIkePhase1LifeTimeType != VPN_LIFE_TYPE_KB) &&
        (i4TestValFsVpnIkePhase1LifeTimeType != VPN_LIFE_TYPE_MINS) &&
        (i4TestValFsVpnIkePhase1LifeTimeType != VPN_LIFE_TYPE_HRS) &&
        (i4TestValFsVpnIkePhase1LifeTimeType != VPN_LIFE_TYPE_DAYS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnIkePhase1LifeTimeType lv;

    lv.cmd = NMH_TESTV2_FS_VPN_IKE_PHASE1_LIFE_TIME_TYPE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnIkePhase1LifeTimeType =
        i4TestValFsVpnIkePhase1LifeTimeType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnIkePhase1LifeTime
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnIkePhase1LifeTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnIkePhase1LifeTime (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                 INT4 i4TestValFsVpnIkePhase1LifeTime)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE) ||
        (VPN_POLICY_TYPE (pVpnPolicy) == VPN_IPSEC_MANUAL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if (VPN_POLICY_PHASE1_LTIME_TYPE (pVpnPolicy) == VPN_ZERO)
    {
        /* Life time type should be defined first */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    VPN_CONVERT_LIFETIME_TO_SECS (VPN_POLICY_PHASE1_LTIME_TYPE (pVpnPolicy),
                                  i4TestValFsVpnIkePhase1LifeTime);
    if ((i4TestValFsVpnIkePhase1LifeTime < VPN_MIN_LIFETIME_SECS) ||
        (i4TestValFsVpnIkePhase1LifeTime > VPN_PHASE1_MAX_LIFETIME_SECS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhTestv2FsVpnIkePhase1LifeTime lv;

    lv.cmd = NMH_TESTV2_FS_VPN_IKE_PHASE1_LIFE_TIME;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnIkePhase1LifeTime = i4TestValFsVpnIkePhase1LifeTime;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnIkePhase1Mode
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnIkePhase1Mode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnIkePhase1Mode (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                             INT4 i4TestValFsVpnIkePhase1Mode)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_VPN_ERR_NO_POLICY);
        return (SNMP_FAILURE);
    }

    if ((pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE) ||
        (VPN_POLICY_TYPE (pVpnPolicy) == VPN_IPSEC_MANUAL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VPN_ERR_POLICY_NOT_IKE);
        return (SNMP_FAILURE);
    }

#ifndef SECURITY_KERNEL_WANTED
    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        if (i4TestValFsVpnIkePhase1Mode == VPN_IKE_MODE_AGGRESSIVE)
        {
            CLI_SET_ERR (CLI_VPN_ERR_FIPS_AGGRESSIVE_MODE);
            return SNMP_FAILURE;
        }
    }
#endif

    if ((i4TestValFsVpnIkePhase1Mode != VPN_IKE_MODE_MAIN) &&
        (i4TestValFsVpnIkePhase1Mode != VPN_IKE_MODE_AGGRESSIVE))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_VPN_ERR_PHASEI_INFO);
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsVpnIkePhase1Mode == VPN_IKE_MODE_MAIN) &&
        ((pVpnPolicy->u4VpnPolicyType == VPN_XAUTH) ||
         (pVpnPolicy->u4VpnPolicyType == VPN_IKE_XAUTH_CERT)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_VPN_ERR_PHASEI_RAVPN_MODE);
        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;

#else
    int                 rc;
    tNpwnmhTestv2FsVpnIkePhase1Mode lv;

    lv.cmd = NMH_TESTV2_FS_VPN_IKE_PHASE1_MODE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnIkePhase1Mode = i4TestValFsVpnIkePhase1Mode;

    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        if (i4TestValFsVpnIkePhase1Mode == VPN_IKE_MODE_AGGRESSIVE)
        {
            CLI_SET_ERR (CLI_VPN_ERR_FIPS_AGGRESSIVE_MODE);
            return SNMP_FAILURE;
        }
    }

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnIkePhase2AuthAlgo
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnIkePhase2AuthAlgo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnIkePhase2AuthAlgo (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                 INT4 i4TestValFsVpnIkePhase2AuthAlgo)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE) ||
        (VPN_POLICY_TYPE (pVpnPolicy) == VPN_IPSEC_MANUAL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsVpnIkePhase2AuthAlgo != VPN_HMACMD5)
        && (i4TestValFsVpnIkePhase2AuthAlgo != VPN_HMACSHA1)
        && (i4TestValFsVpnIkePhase2AuthAlgo != SEC_XCBCMAC)
        && (i4TestValFsVpnIkePhase2AuthAlgo != HMAC_SHA_256)
        && (i4TestValFsVpnIkePhase2AuthAlgo != HMAC_SHA_384)
        && (i4TestValFsVpnIkePhase2AuthAlgo != HMAC_SHA_512))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

#ifndef SECURITY_KERNEL_WANTED
    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        if (i4TestValFsVpnIkePhase2AuthAlgo == VPN_HMACMD5)
        {
            return SNMP_FAILURE;
        }
    }
#endif
    return SNMP_SUCCESS;

#else
    int                 rc;
    tNpwnmhTestv2FsVpnIkePhase2AuthAlgo lv;

    lv.cmd = NMH_TESTV2_FS_VPN_IKE_PHASE2_AUTH_ALGO;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnIkePhase2AuthAlgo = i4TestValFsVpnIkePhase2AuthAlgo;

    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        if (i4TestValFsVpnIkePhase2AuthAlgo == VPN_HMACMD5)
        {
            return SNMP_FAILURE;
        }
    }

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnIkePhase2EspEncryptionAlgo
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnIkePhase2EspEncryptionAlgo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnIkePhase2EspEncryptionAlgo (UINT4 *pu4ErrorCode,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsVpnPolicyName,
                                          INT4
                                          i4TestValFsVpnIkePhase2EspEncryptionAlgo)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE) ||
        (VPN_POLICY_TYPE (pVpnPolicy) == VPN_IPSEC_MANUAL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsVpnIkePhase2EspEncryptionAlgo != VPN_DES_CBC)
        && (i4TestValFsVpnIkePhase2EspEncryptionAlgo != VPN_3DES_CBC)
        && (i4TestValFsVpnIkePhase2EspEncryptionAlgo != VPN_NULLESPALGO)
        && (i4TestValFsVpnIkePhase2EspEncryptionAlgo != VPN_AES_128)
        && (i4TestValFsVpnIkePhase2EspEncryptionAlgo != VPN_AES_192)
        && (i4TestValFsVpnIkePhase2EspEncryptionAlgo != VPN_AES_256)
        && (i4TestValFsVpnIkePhase2EspEncryptionAlgo != SEC_AESCTR)
        && (i4TestValFsVpnIkePhase2EspEncryptionAlgo != SEC_AESCTR192)
        && (i4TestValFsVpnIkePhase2EspEncryptionAlgo != SEC_AESCTR256))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

#ifndef SECURITY_KERNEL_WANTED
    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        if (i4TestValFsVpnIkePhase2EspEncryptionAlgo == VPN_DES_CBC)
        {
            return SNMP_FAILURE;
        }
    }
#endif

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnIkePhase2EspEncryptionAlgo lv;

    lv.cmd = NMH_TESTV2_FS_VPN_IKE_PHASE2_ESP_ENCRYPTION_ALGO;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnIkePhase2EspEncryptionAlgo =
        i4TestValFsVpnIkePhase2EspEncryptionAlgo;

    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        if (i4TestValFsVpnIkePhase2EspEncryptionAlgo == VPN_DES_CBC)
        {
            return SNMP_FAILURE;
        }
    }

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnIkePhase2LifeTimeType
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnIkePhase2LifeTimeType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnIkePhase2LifeTimeType (UINT4 *pu4ErrorCode,
                                     tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                     INT4 i4TestValFsVpnIkePhase2LifeTimeType)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE) ||
        (VPN_POLICY_TYPE (pVpnPolicy) == VPN_IPSEC_MANUAL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsVpnIkePhase2LifeTimeType != VPN_LIFE_TYPE_SECS) &&
        (i4TestValFsVpnIkePhase2LifeTimeType != VPN_LIFE_TYPE_KB) &&
        (i4TestValFsVpnIkePhase2LifeTimeType != VPN_LIFE_TYPE_MINS) &&
        (i4TestValFsVpnIkePhase2LifeTimeType != VPN_LIFE_TYPE_HRS) &&
        (i4TestValFsVpnIkePhase2LifeTimeType != VPN_LIFE_TYPE_DAYS))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

#else
    int                 rc;
    tNpwnmhTestv2FsVpnIkePhase2LifeTimeType lv;

    lv.cmd = NMH_TESTV2_FS_VPN_IKE_PHASE2_LIFE_TIME_TYPE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnIkePhase2LifeTimeType =
        i4TestValFsVpnIkePhase2LifeTimeType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnIkePhase2LifeTime
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnIkePhase2LifeTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnIkePhase2LifeTime (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                 INT4 i4TestValFsVpnIkePhase2LifeTime)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED

    tVpnPolicy         *pVpnPolicy = NULL;
    UINT1               u1LifeTimeType = VPN_ZERO;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE) ||
        (VPN_POLICY_TYPE (pVpnPolicy) == VPN_IPSEC_MANUAL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if (VPN_POLICY_PHASE2_LTIME_TYPE (pVpnPolicy) == VPN_ZERO)
    {
        /* Life time type should be defined first */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }
    u1LifeTimeType = VPN_POLICY_PHASE2_LTIME_TYPE (pVpnPolicy);
    if (u1LifeTimeType == VPN_LIFE_TYPE_KB)
    {
        if (i4TestValFsVpnIkePhase2LifeTime < VPN_MIN_LIFETIME_KB
            || i4TestValFsVpnIkePhase2LifeTime > VPN_MAX_LIFETIME_KB)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
        }
    }
    else
    {
        VPN_CONVERT_LIFETIME_TO_SECS (u1LifeTimeType,
                                      i4TestValFsVpnIkePhase2LifeTime);

        if (i4TestValFsVpnIkePhase2LifeTime < VPN_MIN_LIFETIME_SECS
            || i4TestValFsVpnIkePhase2LifeTime > VPN_PHASE2_MAX_LIFETIME_SECS)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return (SNMP_FAILURE);
        }
    }
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnIkePhase2LifeTime lv;

    lv.cmd = NMH_TESTV2_FS_VPN_IKE_PHASE2_LIFE_TIME;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnIkePhase2LifeTime = i4TestValFsVpnIkePhase2LifeTime;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnIkePhase2DHGroup
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnIkePhase2DHGroup
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnIkePhase2DHGroup (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                                INT4 i4TestValFsVpnIkePhase2DHGroup)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if ((pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE) ||
        (VPN_POLICY_TYPE (pVpnPolicy) == VPN_IPSEC_MANUAL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsVpnIkePhase2DHGroup != VPN_DH_GROUP1) &&
        (i4TestValFsVpnIkePhase2DHGroup != VPN_DH_GROUP2) &&
        (i4TestValFsVpnIkePhase2DHGroup != VPN_DH_GROUP5) &&
        (i4TestValFsVpnIkePhase2DHGroup != SEC_DH_GROUP_14))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);

    }
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnIkePhase2DHGroup lv;

    lv.cmd = NMH_TESTV2_FS_VPN_IKE_PHASE2_D_H_GROUP;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnIkePhase2DHGroup = i4TestValFsVpnIkePhase2DHGroup;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnIkeVersion
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnIkeVersion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnIkeVersion (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                          INT4 i4TestValFsVpnIkeVersion)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy =
        VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                          pFsVpnPolicyName->i4_Length);

    if (pVpnPolicy == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    /* IPSEC_MANUAL doesnt work with IKE */
    if ((pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE) ||
        (VPN_POLICY_TYPE (pVpnPolicy) == VPN_IPSEC_MANUAL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsVpnIkeVersion != CLI_VPN_IKE_V1) &&
        (i4TestValFsVpnIkeVersion != CLI_VPN_IKE_V2))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnIkeVersion lv;

    lv.cmd = NMH_TESTV2_FS_VPN_IKE_VERSION;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnIkeVersion = i4TestValFsVpnIkeVersion;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnPolicyRowStatus
 Input       :  The Indices
                FsVpnPolicyName

                The Object 
                testValFsVpnPolicyRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnPolicyRowStatus (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE * pFsVpnPolicyName,
                               INT4 i4TestValFsVpnPolicyRowStatus)
{
    tVpnPolicy         *pVpnPolicy = NULL;

    pVpnPolicy = VpnSnmpGetPolicy (pFsVpnPolicyName->pu1_OctetList,
                                   pFsVpnPolicyName->i4_Length);

    switch (i4TestValFsVpnPolicyRowStatus)
    {
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:

            if ((pVpnPolicy != NULL) ||
                (STRNCASECMP
                 (pFsVpnPolicyName->pu1_OctetList, "all",
                  (pFsVpnPolicyName->i4_Length >
                   VPN_THREE ? pFsVpnPolicyName->i4_Length : VPN_THREE)) ==
                 VPN_ZERO))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return (SNMP_FAILURE);
            }

            if ((pFsVpnPolicyName->i4_Length < VPN_MIN_POLICY_NAME_LENGTH) ||
                (pFsVpnPolicyName->i4_Length > VPN_MAX_POLICY_NAME_LENGTH))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return (SNMP_FAILURE);
            }

            if (VpnValCryptoMapName (pFsVpnPolicyName->pu1_OctetList) ==
                SNMP_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return (SNMP_FAILURE);
            }

            if (TMO_SLL_Count (&VpnPolicyList) == VPN_SYS_MAX_NUM_TUNNELS)
            {
                CLI_SET_ERR (CLI_VPN_ERR_MAX_VPN_POLICIES);
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return (SNMP_FAILURE);
            }

            break;
        case NOT_IN_SERVICE:
        case DESTROY:
            if (pVpnPolicy == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return (SNMP_FAILURE);
            }
            break;
        case ACTIVE:
            if (pVpnPolicy == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return (SNMP_FAILURE);
            }

            if (VpnCheckMandatoryParams (pFsVpnPolicyName) == SNMP_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
            }
            break;
        default:
            return (SNMP_FAILURE);
    }
#ifndef SECURITY_KERNEL_MAKE_WANTED
    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhTestv2FsVpnPolicyRowStatus lv;

    lv.cmd = NMH_TESTV2_FS_VPN_POLICY_ROW_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnPolicyName = pFsVpnPolicyName;
    lv.i4TestValFsVpnPolicyRowStatus = i4TestValFsVpnPolicyRowStatus;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVpnTable
 Input       :  The Indices
                FsVpnPolicyName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVpnTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsVpnRaUsersTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVpnRaUsersTable
 Input       :  The Indices
                FsVpnRaUserName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVpnRaUsersTable (tSNMP_OCTET_STRING_TYPE *
                                           pFsVpnRaUserName)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    if (VpnGetRaVpnUserInfo (pFsVpnRaUserName->pu1_OctetList,
                             pFsVpnRaUserName->i4_Length) != NULL)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
#else
    int                 rc;
    tNpwnmhValidateIndexInstanceFsVpnRaUsersTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_FS_VPN_RA_USERS_TABLE;
    lv.pFsVpnRaUserName = pFsVpnRaUserName;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVpnRaUsersTable
 Input       :  The Indices
                FsVpnRaUserName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVpnRaUsersTable (tSNMP_OCTET_STRING_TYPE * pFsVpnRaUserName)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnRaUserInfo     *pVpnRaUserInfoNode = NULL;

    pVpnRaUserInfoNode = (tVpnRaUserInfo *) TMO_SLL_First (&gVpnRaUserList);

    if (pVpnRaUserInfoNode != NULL)
    {
        pFsVpnRaUserName->i4_Length =
            (INT4) STRLEN (pVpnRaUserInfoNode->au1VpnRaUserName);
        STRNCPY (pFsVpnRaUserName->pu1_OctetList,
                 pVpnRaUserInfoNode->au1VpnRaUserName,
                 (size_t) pFsVpnRaUserName->i4_Length);

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
#else
    int                 rc;
    tNpwnmhGetFirstIndexFsVpnRaUsersTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_FS_VPN_RA_USERS_TABLE;
    lv.pFsVpnRaUserName = pFsVpnRaUserName;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVpnRaUsersTable
 Input       :  The Indices
                FsVpnRaUserName
                nextFsVpnRaUserName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsVpnRaUsersTable (tSNMP_OCTET_STRING_TYPE * pFsVpnRaUserName,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pNextFsVpnRaUserName)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnRaUserInfo     *pVpnRaUserInfoNode = NULL;

    pVpnRaUserInfoNode = VpnGetRaVpnUserInfo (pFsVpnRaUserName->pu1_OctetList,
                                              pFsVpnRaUserName->i4_Length);
    pVpnRaUserInfoNode =
        (tVpnRaUserInfo *) TMO_SLL_Next ((tTMO_SLL *) & gVpnRaUserList,
                                         (tTMO_SLL_NODE *) pVpnRaUserInfoNode);
    if (pVpnRaUserInfoNode != NULL)
    {
        pNextFsVpnRaUserName->i4_Length =
            (INT4) STRLEN (VPN_RA_USER_NAME (pVpnRaUserInfoNode));
        STRNCPY (pNextFsVpnRaUserName->pu1_OctetList,
                 VPN_RA_USER_NAME (pVpnRaUserInfoNode),
                 (size_t) pNextFsVpnRaUserName->i4_Length);

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
#else
    int                 rc;
    tNpwnmhGetNextIndexFsVpnRaUsersTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_FS_VPN_RA_USERS_TABLE;
    lv.pFsVpnRaUserName = pFsVpnRaUserName;
    lv.pNextFsVpnRaUserName = pNextFsVpnRaUserName;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVpnRaUserSecret
 Input       :  The Indices
                FsVpnRaUserName

                The Object 
                retValFsVpnRaUserSecret
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnRaUserSecret (tSNMP_OCTET_STRING_TYPE * pFsVpnRaUserName,
                         tSNMP_OCTET_STRING_TYPE * pRetValFsVpnRaUserSecret)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnRaUserInfo     *pVpnRaUserInfoNode = NULL;

    pVpnRaUserInfoNode = VpnGetRaVpnUserInfo (pFsVpnRaUserName->pu1_OctetList,
                                              pFsVpnRaUserName->i4_Length);
    pRetValFsVpnRaUserSecret->i4_Length =
        (INT4) STRLEN (VPN_RA_USER_SECRET (pVpnRaUserInfoNode));
    STRNCPY (pRetValFsVpnRaUserSecret->pu1_OctetList,
             VPN_RA_USER_SECRET (pVpnRaUserInfoNode),
             (size_t) pRetValFsVpnRaUserSecret->i4_Length);

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnRaUserSecret lv;

    lv.cmd = NMH_GET_FS_VPN_RA_USER_SECRET;
    lv.pFsVpnRaUserName = pFsVpnRaUserName;
    lv.pRetValFsVpnRaUserSecret = pRetValFsVpnRaUserSecret;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnRaUserRowStatus
 Input       :  The Indices
                FsVpnRaUserName

                The Object 
                retValFsVpnRaUserRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnRaUserRowStatus (tSNMP_OCTET_STRING_TYPE * pFsVpnRaUserName,
                            INT4 *pi4RetValFsVpnRaUserRowStatus)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnRaUserInfo     *pVpnRaUserInfoNode = NULL;

    pVpnRaUserInfoNode = VpnGetRaVpnUserInfo (pFsVpnRaUserName->pu1_OctetList,
                                              pFsVpnRaUserName->i4_Length);
    if (pVpnRaUserInfoNode == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnRaUserRowStatus = VPN_RA_USER_STATUS (pVpnRaUserInfoNode);

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnRaUserRowStatus lv;

    lv.cmd = NMH_GET_FS_VPN_RA_USER_ROW_STATUS;
    lv.pFsVpnRaUserName = pFsVpnRaUserName;
    lv.pi4RetValFsVpnRaUserRowStatus = pi4RetValFsVpnRaUserRowStatus;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVpnRaUserSecret
 Input       :  The Indices
                FsVpnRaUserName

                The Object 
                setValFsVpnRaUserSecret
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnRaUserSecret (tSNMP_OCTET_STRING_TYPE * pFsVpnRaUserName,
                         tSNMP_OCTET_STRING_TYPE * pSetValFsVpnRaUserSecret)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tVpnRaUserInfo     *pVpnRaUserInfoNode = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pVpnRaUserInfoNode = VpnGetRaVpnUserInfo (pFsVpnRaUserName->pu1_OctetList,
                                              pFsVpnRaUserName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnRaUserInfoNode == NULL)
    {
        return (SNMP_FAILURE);
    }

    MEMCPY (pVpnRaUserInfoNode->au1VpnRaUserSecret,
            pSetValFsVpnRaUserSecret->pu1_OctetList,
            pSetValFsVpnRaUserSecret->i4_Length);
    VPN_RA_USER_SECRET (pVpnRaUserInfoNode)[pSetValFsVpnRaUserSecret->
                                            i4_Length] = '\0';

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnRaUserSecret lv;

    lv.cmd = NMH_SET_FS_VPN_RA_USER_SECRET;
    lv.pFsVpnRaUserName = pFsVpnRaUserName;
    lv.pSetValFsVpnRaUserSecret = pSetValFsVpnRaUserSecret;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnRaUserSecret, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s", pFsVpnRaUserName,
                      pSetValFsVpnRaUserSecret));
    return SNMP_SUCCESS;
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnRaUserRowStatus
 Input       :  The Indices
                FsVpnRaUserName

                The Object 
                setValFsVpnRaUserRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnRaUserRowStatus (tSNMP_OCTET_STRING_TYPE * pFsVpnRaUserName,
                            INT4 i4SetValFsVpnRaUserRowStatus)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnRaUserInfo     *pVpnRaUserInfoNode = NULL;
    INT1                i1Status = SNMP_SUCCESS;

    pVpnRaUserInfoNode = VpnGetRaVpnUserInfo (pFsVpnRaUserName->pu1_OctetList,
                                              pFsVpnRaUserName->i4_Length);

    RM_GET_SEQ_NUM (&u4SeqNum);
    switch (i4SetValFsVpnRaUserRowStatus)
    {
        case VPN_STATUS_ACTIVE:

            if (pVpnRaUserInfoNode == NULL)
            {
                return (SNMP_FAILURE);
            }

            if (pVpnRaUserInfoNode->i4RowStatus == VPN_STATUS_ACTIVE)
            {
                return SNMP_SUCCESS;
            }

#ifdef IKE_WANTED
            /* Update the RaUserDetails in the coresponding IKE DB */
            i1Status =
                (INT1) VpnUtilIkeUpdateRaUserDetails (pVpnRaUserInfoNode,
                                                      VPN_ADD_RA_USER_INFO);
#endif
            if (i1Status == SNMP_SUCCESS)
            {
                pVpnRaUserInfoNode->i4RowStatus = VPN_STATUS_ACTIVE;
            }

            break;

        case VPN_STATUS_NOT_IN_SERVICE:

            if (pVpnRaUserInfoNode == NULL)
            {
                return (SNMP_FAILURE);
            }

#ifdef IKE_WANTED
            /* Del the RaUserDetails in the coresponding IKE DB */
            i1Status =
                VpnUtilIkeUpdateRaUserDetails (pVpnRaUserInfoNode,
                                               VPN_DEL_RA_USER_INFO);
#endif

            if (i1Status == SNMP_SUCCESS)
            {
                VPN_RA_USER_STATUS (pVpnRaUserInfoNode) =
                    VPN_STATUS_NOT_IN_SERVICE;
            }

            break;

        case VPN_STATUS_CREATE_AND_WAIT:

            /*Allocate the memory for storing RA User parameters in Database */
            if (MemAllocateMemBlock (VPN_RA_USERS_PID,
                                     (UINT1 **) (VOID *) &pVpnRaUserInfoNode)
                != MEM_SUCCESS)
            {
                i1Status = SNMP_FAILURE;
                break;
            }

            /* Initiating a new node and filling the default values */
            VpnRaDBAddUser (pVpnRaUserInfoNode, pFsVpnRaUserName);

            break;

        case VPN_STATUS_DESTROY:

            if (pVpnRaUserInfoNode == NULL)
            {
                return (SNMP_FAILURE);
            }

            if (VPN_RA_USER_STATUS (pVpnRaUserInfoNode) == VPN_STATUS_ACTIVE)
            {
#ifdef IKE_WANTED
                /* Del the RaUserDetails in the coresponding IKE DB */
                i1Status = VpnUtilIkeUpdateRaUserDetails (pVpnRaUserInfoNode,
                                                          VPN_DEL_RA_USER_INFO);
#endif
            }

            if (i1Status == SNMP_SUCCESS)
            {
                TMO_SLL_Delete (&gVpnRaUserList,
                                (tTMO_SLL_NODE *) pVpnRaUserInfoNode);
                MemReleaseMemBlock (VPN_RA_USERS_PID,
                                    (UINT1 *) pVpnRaUserInfoNode);
            }
            break;
        default:
            break;
    }
#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnRaUserRowStatus lv;

    lv.cmd = NMH_SET_FS_VPN_RA_USER_ROW_STATUS;
    lv.pFsVpnRaUserName = pFsVpnRaUserName;
    lv.i4SetValFsVpnRaUserRowStatus = i4SetValFsVpnRaUserRowStatus;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#else
    if (i1Status == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnRaUserRowStatus, u4SeqNum,
                              TRUE, VpnDsLock, VpnDsUnLock, VPN_ONE,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnRaUserName,
                          i4SetValFsVpnRaUserRowStatus));
    }
    return i1Status;
#endif
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVpnRaUserSecret
 Input       :  The Indices
                FsVpnRaUserName

                The Object 
                testValFsVpnRaUserSecret
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnRaUserSecret (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pFsVpnRaUserName,
                            tSNMP_OCTET_STRING_TYPE * pTestValFsVpnRaUserSecret)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnRaUserInfo     *pVpnRaUserInfoNode = NULL;

    pVpnRaUserInfoNode = VpnGetRaVpnUserInfo (pFsVpnRaUserName->pu1_OctetList,
                                              pFsVpnRaUserName->i4_Length);
    if (pVpnRaUserInfoNode == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pVpnRaUserInfoNode->i4RowStatus == VPN_STATUS_ACTIVE)
    {
        /* Can not alter the active users */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pTestValFsVpnRaUserSecret->i4_Length > RA_USER_SECRET_LENGTH) ||
        (pTestValFsVpnRaUserSecret->i4_Length < VPN_MIN_RA_USER_SECRET_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else

    int                 rc;
    tNpwnmhTestv2FsVpnRaUserSecret lv;

    lv.cmd = NMH_TESTV2_FS_VPN_RA_USER_SECRET;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnRaUserName = pFsVpnRaUserName;
    lv.pTestValFsVpnRaUserSecret = pTestValFsVpnRaUserSecret;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnRaUserRowStatus
 Input       :  The Indices
                FsVpnRaUserName

                The Object 
                testValFsVpnRaUserRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnRaUserRowStatus (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE * pFsVpnRaUserName,
                               INT4 i4TestValFsVpnRaUserRowStatus)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnRaUserInfo     *pVpnRaUserInfoNode = NULL;
    pVpnRaUserInfoNode = VpnGetRaVpnUserInfo (pFsVpnRaUserName->pu1_OctetList,
                                              pFsVpnRaUserName->i4_Length);

    switch (i4TestValFsVpnRaUserRowStatus)
    {
        case VPN_STATUS_ACTIVE:
            if (pVpnRaUserInfoNode == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            if (STRLEN (pVpnRaUserInfoNode->au1VpnRaUserSecret) == VPN_ZERO)
            {
                /* No passcode for the user */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case VPN_STATUS_CREATE_AND_WAIT:
            if (pVpnRaUserInfoNode != NULL)
            {
                /* User already exists */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                CLI_SET_ERR (CLI_VPN_ERR_DUP_ENTRY);
                return SNMP_FAILURE;
            }

            if (TMO_SLL_Count (&gVpnRaUserList) == RA_VPN_MAX_USERS_DB)
            {
                /* No.of users exceeds the max */
                CLI_SET_ERR (CLI_VPN_ERR_RAVPN_MAX_USERS);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if (pFsVpnRaUserName->i4_Length > RA_USER_NAME_LENGTH)
            {
                CLI_SET_ERR (CLI_VPN_ERR_USER_NAME_LENGTH);
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return SNMP_FAILURE;
            }

            break;

        case VPN_STATUS_NOT_IN_SERVICE:
        case VPN_STATUS_DESTROY:

            if (pVpnRaUserInfoNode == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnRaUserRowStatus lv;

    lv.cmd = NMH_TESTV2_FS_VPN_RA_USER_ROW_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnRaUserName = pFsVpnRaUserName;
    lv.i4TestValFsVpnRaUserRowStatus = i4TestValFsVpnRaUserRowStatus;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVpnRaUsersTable
 Input       :  The Indices
                FsVpnRaUserName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVpnRaUsersTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsVpnRaAddressPoolTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVpnRaAddressPoolTable
 Input       :  The Indices
                FsVpnRaAddressPoolName
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVpnRaAddressPoolTable (tSNMP_OCTET_STRING_TYPE *
                                                 pFsVpnRaAddressPoolName)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;

    if ((pFsVpnRaAddressPoolName->i4_Length > RA_ADDRESS_POOL_NAME_LENGTH) ||
        (pFsVpnRaAddressPoolName->i4_Length < VPN_RA_MIN_ADDR_POOL_NAME_LEN))
    {
        return (SNMP_FAILURE);
    }

    pVpnRaAddressPoolNode =
        VpnGetRaAddrPool (pFsVpnRaAddressPoolName->pu1_OctetList,
                          pFsVpnRaAddressPoolName->i4_Length);
    if (pVpnRaAddressPoolNode == NULL)
    {
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhValidateIndexInstanceFsVpnRaAddressPoolTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_FS_VPN_RA_ADDRESS_POOL_TABLE;
    lv.pFsVpnRaAddressPoolName = pFsVpnRaAddressPoolName;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVpnRaAddressPoolTable
 Input       :  The Indices
                FsVpnRaAddressPoolName
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVpnRaAddressPoolTable (tSNMP_OCTET_STRING_TYPE *
                                         pFsVpnRaAddressPoolName)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;

    pVpnRaAddressPoolNode =
        (tVpnRaAddressPool *) TMO_SLL_First (&gVpnRaAddressPoolList);

    if (pVpnRaAddressPoolNode != NULL)
    {
        MEMCPY (pFsVpnRaAddressPoolName->pu1_OctetList,
                pVpnRaAddressPoolNode->au1VpnRaAddressPoolName,
                STRLEN (pVpnRaAddressPoolNode->au1VpnRaAddressPoolName));
        pFsVpnRaAddressPoolName->i4_Length =
            (INT4) STRLEN (pVpnRaAddressPoolNode->au1VpnRaAddressPoolName);

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
#else
    int                 rc;
    tNpwnmhGetFirstIndexFsVpnRaAddressPoolTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_FS_VPN_RA_ADDRESS_POOL_TABLE;
    lv.pFsVpnRaAddressPoolName = pFsVpnRaAddressPoolName;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVpnRaAddressPoolTable
 Input       :  The Indices
                FsVpnRaAddressPoolName
                nextFsVpnRaAddressPoolName
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsVpnRaAddressPoolTable (tSNMP_OCTET_STRING_TYPE *
                                        pFsVpnRaAddressPoolName,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNextFsVpnRaAddressPoolName)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;

    TMO_SLL_Scan (&gVpnRaAddressPoolList, pVpnRaAddressPoolNode,
                  tVpnRaAddressPool *)
    {
        if (STRNCASECMP (pVpnRaAddressPoolNode->au1VpnRaAddressPoolName,
                         pFsVpnRaAddressPoolName->pu1_OctetList,
                         pFsVpnRaAddressPoolName->i4_Length) == VPN_ZERO)
        {
            pVpnRaAddressPoolNode =
                (tVpnRaAddressPool *) TMO_SLL_Next
                ((tTMO_SLL *) & gVpnRaAddressPoolList,
                 (tTMO_SLL_NODE *) pVpnRaAddressPoolNode);

            if (pVpnRaAddressPoolNode != NULL)
            {
                MEMCPY (pNextFsVpnRaAddressPoolName->pu1_OctetList,
                        pVpnRaAddressPoolNode->au1VpnRaAddressPoolName,
                        STRLEN (pVpnRaAddressPoolNode->
                                au1VpnRaAddressPoolName));
                pNextFsVpnRaAddressPoolName->i4_Length =
                    (INT4) STRLEN (pVpnRaAddressPoolNode->
                                   au1VpnRaAddressPoolName);

                return SNMP_SUCCESS;
            }

            break;
        }
    }

    return SNMP_FAILURE;
#else
    int                 rc;
    tNpwnmhGetNextIndexFsVpnRaAddressPoolTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_FS_VPN_RA_ADDRESS_POOL_TABLE;
    lv.pFsVpnRaAddressPoolName = pFsVpnRaAddressPoolName;
    lv.pNextFsVpnRaAddressPoolName = pNextFsVpnRaAddressPoolName;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnRaAddressPoolAddrType
 Input       :  The Indices
                FsVpnRaAddressPoolName

                The Object 
                retValFsVpnRaAddressPoolAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnRaAddressPoolAddrType (tSNMP_OCTET_STRING_TYPE *
                                  pFsVpnRaAddressPoolName,
                                  INT4 *pi4RetValFsVpnRaAddressPoolAddrType)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;

    pVpnRaAddressPoolNode =
        VpnGetRaAddrPool (pFsVpnRaAddressPoolName->pu1_OctetList,
                          pFsVpnRaAddressPoolName->i4_Length);

    if (pVpnRaAddressPoolNode == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnRaAddressPoolAddrType =
        (INT4) pVpnRaAddressPoolNode->VpnRaAddressPoolStart.u4AddrType;

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsVpnRaAddressPoolAddrType lv;

    lv.cmd = NMH_GET_FS_VPN_RA_ADDRESS_POOL_ADDR_TYPE;
    lv.pFsVpnRaAddressPoolName = pFsVpnRaAddressPoolName;
    lv.pi4RetValFsVpnRaAddressPoolAddrType =
        pi4RetValFsVpnRaAddressPoolAddrType;
    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnRaAddressPoolStart
 Input       :  The Indices
                FsVpnRaAddressPoolName

                The Object 
                retValFsVpnRaAddressPoolStart
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnRaAddressPoolStart (tSNMP_OCTET_STRING_TYPE *
                               pFsVpnRaAddressPoolName,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsVpnRaAddressPoolStart)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;

    pVpnRaAddressPoolNode =
        VpnGetRaAddrPool (pFsVpnRaAddressPoolName->pu1_OctetList,
                          pFsVpnRaAddressPoolName->i4_Length);

    if (pVpnRaAddressPoolNode == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pVpnRaAddressPoolNode->VpnRaAddressPoolStart.u4AddrType
        == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (pRetValFsVpnRaAddressPoolStart->pu1_OctetList,
                &(pVpnRaAddressPoolNode->VpnRaAddressPoolStart.uIpAddr.Ip4Addr),
                sizeof (tIp4Addr));
        pRetValFsVpnRaAddressPoolStart->i4_Length = IPVX_IPV4_ADDR_LEN;
        return SNMP_SUCCESS;
    }

    if (pVpnRaAddressPoolNode->VpnRaAddressPoolStart.u4AddrType
        == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (pRetValFsVpnRaAddressPoolStart->pu1_OctetList,
                &(pVpnRaAddressPoolNode->VpnRaAddressPoolStart.uIpAddr.Ip6Addr),
                sizeof (tIp6Addr));
        pRetValFsVpnRaAddressPoolStart->i4_Length = IPVX_IPV6_ADDR_LEN;
        return SNMP_SUCCESS;
    }

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsVpnRaAddressPoolStart lv;

    lv.cmd = NMH_GET_FS_VPN_RA_ADDRESS_POOL_START;
    lv.pFsVpnRaAddressPoolName = pFsVpnRaAddressPoolName;
    lv.pRetValFsVpnRaAddressPoolStart = pRetValFsVpnRaAddressPoolStart;
    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnRaAddressPoolEnd
 Input       :  The Indices
                FsVpnRaAddressPoolName

                The Object 
                retValFsVpnRaAddressPoolEnd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnRaAddressPoolEnd (tSNMP_OCTET_STRING_TYPE * pFsVpnRaAddressPoolName,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsVpnRaAddressPoolEnd)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;

    pVpnRaAddressPoolNode =
        VpnGetRaAddrPool (pFsVpnRaAddressPoolName->pu1_OctetList,
                          pFsVpnRaAddressPoolName->i4_Length);

    if (pVpnRaAddressPoolNode == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pVpnRaAddressPoolNode->VpnRaAddressPoolEnd.u4AddrType
        == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (pRetValFsVpnRaAddressPoolEnd->pu1_OctetList,
                &(pVpnRaAddressPoolNode->VpnRaAddressPoolEnd.uIpAddr.Ip4Addr),
                sizeof (tIp4Addr));
        pRetValFsVpnRaAddressPoolEnd->i4_Length = IPVX_IPV4_ADDR_LEN;
    }

    if (pVpnRaAddressPoolNode->VpnRaAddressPoolEnd.u4AddrType
        == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (pRetValFsVpnRaAddressPoolEnd->pu1_OctetList,
                &(pVpnRaAddressPoolNode->VpnRaAddressPoolEnd.uIpAddr.Ip6Addr),
                sizeof (tIp6Addr));
        pRetValFsVpnRaAddressPoolEnd->i4_Length = IPVX_IPV6_ADDR_LEN;
    }

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsVpnRaAddressPoolEnd lv;

    lv.cmd = NMH_GET_FS_VPN_RA_ADDRESS_POOL_END;
    lv.pFsVpnRaAddressPoolName = pFsVpnRaAddressPoolName;
    lv.pRetValFsVpnRaAddressPoolEnd = pRetValFsVpnRaAddressPoolEnd;
    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnRaAddressPoolPrefixLen
 Input       :  The Indices
                FsVpnRaAddressPoolName

                The Object 
                retValFsVpnRaAddressPoolPrefixLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnRaAddressPoolPrefixLen (tSNMP_OCTET_STRING_TYPE *
                                   pFsVpnRaAddressPoolName,
                                   UINT4 *pu4RetValFsVpnRaAddressPoolPrefixLen)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;

    pVpnRaAddressPoolNode =
        VpnGetRaAddrPool (pFsVpnRaAddressPoolName->pu1_OctetList,
                          pFsVpnRaAddressPoolName->i4_Length);

    if (pVpnRaAddressPoolNode == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pu4RetValFsVpnRaAddressPoolPrefixLen =
        pVpnRaAddressPoolNode->u4VpnRaAddressPoolPrefixLen;

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnRaAddressPoolPrefixLen lv;

    lv.cmd = NMH_GET_FS_VPN_RA_ADDRESS_POOL_PREFIX_LEN;
    lv.pFsVpnRaAddressPoolName = pFsVpnRaAddressPoolName;
    lv.pu4RetValFsVpnRaAddressPoolPrefixLen =
        pu4RetValFsVpnRaAddressPoolPrefixLen;
    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnRaAddressPoolRowStatus
 Input       :  The Indices
                FsVpnRaAddressPoolName

                The Object 
                retValFsVpnRaAddressPoolRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnRaAddressPoolRowStatus (tSNMP_OCTET_STRING_TYPE *
                                   pFsVpnRaAddressPoolName,
                                   INT4 *pi4RetValFsVpnRaAddressPoolRowStatus)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;

    pVpnRaAddressPoolNode =
        VpnGetRaAddrPool (pFsVpnRaAddressPoolName->pu1_OctetList,
                          pFsVpnRaAddressPoolName->i4_Length);

    if (pVpnRaAddressPoolNode == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnRaAddressPoolRowStatus = pVpnRaAddressPoolNode->i4RowStatus;

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnRaAddressPoolRowStatus lv;

    lv.cmd = NMH_GET_FS_VPN_RA_ADDRESS_POOL_ROW_STATUS;
    lv.pFsVpnRaAddressPoolName = pFsVpnRaAddressPoolName;
    lv.pi4RetValFsVpnRaAddressPoolRowStatus =
        pi4RetValFsVpnRaAddressPoolRowStatus;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnRaAddressPoolAddrType
 Input       :  The Indices
                FsVpnRaAddressPoolName

                The Object 
                setValFsVpnRaAddressPoolAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnRaAddressPoolAddrType (tSNMP_OCTET_STRING_TYPE *
                                  pFsVpnRaAddressPoolName,
                                  INT4 i4SetValFsVpnRaAddressPoolAddrType)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;

    pVpnRaAddressPoolNode =
        VpnGetRaAddrPool (pFsVpnRaAddressPoolName->pu1_OctetList,
                          pFsVpnRaAddressPoolName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (pVpnRaAddressPoolNode == NULL)
    {
        return (SNMP_FAILURE);
    }

    MEMSET (&(pVpnRaAddressPoolNode->VpnRaAddressPoolStart), VPN_ZERO,
            sizeof (tVpnIpAddr));
    MEMSET (&(pVpnRaAddressPoolNode->VpnRaAddressPoolEnd), VPN_ZERO,
            sizeof (tVpnIpAddr));

    pVpnRaAddressPoolNode->VpnRaAddressPoolStart.u4AddrType =
        (UINT4) i4SetValFsVpnRaAddressPoolAddrType;
    pVpnRaAddressPoolNode->VpnRaAddressPoolEnd.u4AddrType =
        (UINT4) i4SetValFsVpnRaAddressPoolAddrType;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnRaAddressPoolAddrType lv;

    lv.cmd = NMH_SET_FS_VPN_RA_ADDRESS_POOL_ADDR_TYPE;
    lv.pFsVpnRaAddressPoolName = pFsVpnRaAddressPoolName;
    lv.i4SetValFsVpnRaAddressPoolAddrType = i4SetValFsVpnRaAddressPoolAddrType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnRaAddressPoolAddrType, u4SeqNum,
                          FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnRaAddressPoolName,
                      i4SetValFsVpnRaAddressPoolAddrType));
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnRaAddressPoolStart
 Input       :  The Indices
                FsVpnRaAddressPoolName

                The Object 
                setValFsVpnRaAddressPoolStart
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnRaAddressPoolStart (tSNMP_OCTET_STRING_TYPE *
                               pFsVpnRaAddressPoolName,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsVpnRaAddressPoolStart)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;

    pVpnRaAddressPoolNode =
        VpnGetRaAddrPool (pFsVpnRaAddressPoolName->pu1_OctetList,
                          pFsVpnRaAddressPoolName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (pVpnRaAddressPoolNode == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pVpnRaAddressPoolNode->VpnRaAddressPoolStart.u4AddrType
        == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&(pVpnRaAddressPoolNode->VpnRaAddressPoolStart.uIpAddr.Ip4Addr),
                pSetValFsVpnRaAddressPoolStart->pu1_OctetList,
                pSetValFsVpnRaAddressPoolStart->i4_Length);
    }

    if (pVpnRaAddressPoolNode->VpnRaAddressPoolStart.u4AddrType
        == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (&(pVpnRaAddressPoolNode->VpnRaAddressPoolStart.uIpAddr.Ip6Addr),
                pSetValFsVpnRaAddressPoolStart->pu1_OctetList,
                pSetValFsVpnRaAddressPoolStart->i4_Length);
    }

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnRaAddressPoolStart lv;

    lv.cmd = NMH_SET_FS_VPN_RA_ADDRESS_POOL_START;
    lv.pFsVpnRaAddressPoolName = pFsVpnRaAddressPoolName;
    lv.pSetValFsVpnRaAddressPoolStart = pSetValFsVpnRaAddressPoolStart;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnRaAddressPoolStart, u4SeqNum,
                          FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s", pFsVpnRaAddressPoolName,
                      pSetValFsVpnRaAddressPoolStart));
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnRaAddressPoolEnd
 Input       :  The Indices
                FsVpnRaAddressPoolName

                The Object 
                setValFsVpnRaAddressPoolEnd
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnRaAddressPoolEnd (tSNMP_OCTET_STRING_TYPE * pFsVpnRaAddressPoolName,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsVpnRaAddressPoolEnd)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;

    pVpnRaAddressPoolNode =
        VpnGetRaAddrPool (pFsVpnRaAddressPoolName->pu1_OctetList,
                          pFsVpnRaAddressPoolName->i4_Length);

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (pVpnRaAddressPoolNode == NULL)
    {
        return (SNMP_FAILURE);
    }

    if (pVpnRaAddressPoolNode->VpnRaAddressPoolEnd.u4AddrType
        == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&(pVpnRaAddressPoolNode->VpnRaAddressPoolEnd.uIpAddr.Ip4Addr),
                pSetValFsVpnRaAddressPoolEnd->pu1_OctetList,
                pSetValFsVpnRaAddressPoolEnd->i4_Length);
    }

    if (pVpnRaAddressPoolNode->VpnRaAddressPoolEnd.u4AddrType
        == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (&(pVpnRaAddressPoolNode->VpnRaAddressPoolEnd.uIpAddr.Ip6Addr),
                pSetValFsVpnRaAddressPoolEnd->pu1_OctetList,
                pSetValFsVpnRaAddressPoolEnd->i4_Length);
    }

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnRaAddressPoolEnd lv;

    lv.cmd = NMH_SET_FS_VPN_RA_ADDRESS_POOL_END;
    lv.pFsVpnRaAddressPoolName = pFsVpnRaAddressPoolName;
    lv.pSetValFsVpnRaAddressPoolEnd = pSetValFsVpnRaAddressPoolEnd;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnRaAddressPoolEnd, u4SeqNum,
                          FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s", pFsVpnRaAddressPoolName,
                      pSetValFsVpnRaAddressPoolEnd));
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnRaAddressPoolPrefixLen
 Input       :  The Indices
                FsVpnRaAddressPoolName

                The Object 
                setValFsVpnRaAddressPoolPrefixLen
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnRaAddressPoolPrefixLen (tSNMP_OCTET_STRING_TYPE *
                                   pFsVpnRaAddressPoolName,
                                   UINT4 u4SetValFsVpnRaAddressPoolPrefixLen)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;

    pVpnRaAddressPoolNode =
        VpnGetRaAddrPool (pFsVpnRaAddressPoolName->pu1_OctetList,
                          pFsVpnRaAddressPoolName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (pVpnRaAddressPoolNode == NULL)
    {
        return (SNMP_FAILURE);
    }

    pVpnRaAddressPoolNode->u4VpnRaAddressPoolPrefixLen =
        u4SetValFsVpnRaAddressPoolPrefixLen;

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnRaAddressPoolPrefixLen lv;

    lv.cmd = NMH_SET_FS_VPN_RA_ADDRESS_POOL_PREFIX_LEN;
    lv.pFsVpnRaAddressPoolName = pFsVpnRaAddressPoolName;
    lv.u4SetValFsVpnRaAddressPoolPrefixLen =
        u4SetValFsVpnRaAddressPoolPrefixLen;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnRaAddressPoolPrefixLen, u4SeqNum,
                          FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %p", pFsVpnRaAddressPoolName,
                      u4SetValFsVpnRaAddressPoolPrefixLen));
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnRaAddressPoolRowStatus
 Input       :  The Indices
                FsVpnRaAddressPoolName

                The Object 
                setValFsVpnRaAddressPoolRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnRaAddressPoolRowStatus (tSNMP_OCTET_STRING_TYPE *
                                   pFsVpnRaAddressPoolName,
                                   INT4 i4SetValFsVpnRaAddressPoolRowStatus)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;
    INT1                i1Status = SNMP_SUCCESS;

    /* To avoid Warning */
    i1Status = SNMP_SUCCESS;

    pVpnRaAddressPoolNode =
        VpnGetRaAddrPool (pFsVpnRaAddressPoolName->pu1_OctetList,
                          pFsVpnRaAddressPoolName->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    switch (i4SetValFsVpnRaAddressPoolRowStatus)
    {
        case VPN_STATUS_ACTIVE:

            if (pVpnRaAddressPoolNode == NULL)
            {
                return (SNMP_FAILURE);
            }

            if (pVpnRaAddressPoolNode->i4RowStatus == VPN_STATUS_ACTIVE)
            {
                return SNMP_SUCCESS;
            }
            pVpnRaAddressPoolNode->i4RowStatus =
                i4SetValFsVpnRaAddressPoolRowStatus;
            break;

        case VPN_STATUS_NOT_IN_SERVICE:

            if (pVpnRaAddressPoolNode == NULL)
            {
                return (SNMP_FAILURE);
            }

            if (pVpnRaAddressPoolNode->i4RowStatus == VPN_STATUS_NOT_IN_SERVICE)
            {
                return SNMP_SUCCESS;
            }
            pVpnRaAddressPoolNode->i4RowStatus =
                i4SetValFsVpnRaAddressPoolRowStatus;
            break;

        case VPN_STATUS_CREATE_AND_WAIT:

            /*Allocating the Memory for Ra AdressPool for storing in CAS DB  */
            if (pVpnRaAddressPoolNode == NULL)
            {
                if (MemAllocateMemBlock (VPN_RA_ADDRESS_POOL_PID,
                                         (UINT1 **) (VOID *)
                                         &pVpnRaAddressPoolNode) != MEM_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
            VpnRaDBAddAddressPool (pVpnRaAddressPoolNode,
                                   pFsVpnRaAddressPoolName);

            break;

        case VPN_STATUS_DESTROY:

            if (pVpnRaAddressPoolNode == NULL)
            {
                return (SNMP_FAILURE);
            }

            TMO_SLL_Delete (&gVpnRaAddressPoolList,
                            (tTMO_SLL_NODE *) pVpnRaAddressPoolNode);
            MemReleaseMemBlock (VPN_RA_ADDRESS_POOL_PID,
                                (UINT1 *) pVpnRaAddressPoolNode);

            break;
        default:
            break;
    }
#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnRaAddressPoolRowStatus lv;

    lv.cmd = NMH_SET_FS_VPN_RA_ADDRESS_POOL_ROW_STATUS;
    lv.pFsVpnRaAddressPoolName = pFsVpnRaAddressPoolName;
    lv.i4SetValFsVpnRaAddressPoolRowStatus =
        i4SetValFsVpnRaAddressPoolRowStatus;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#else
    if (i1Status == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnRaAddressPoolRowStatus,
                              u4SeqNum, TRUE, VpnDsLock, VpnDsUnLock, VPN_ONE,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnRaAddressPoolName,
                          i4SetValFsVpnRaAddressPoolRowStatus));
    }
    return i1Status;
#endif
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVpnRaAddressPoolAddrType
 Input       :  The Indices
                FsVpnRaAddressPoolName

                The Object 
                testValFsVpnRaAddressPoolAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnRaAddressPoolAddrType (UINT4 *pu4ErrorCode,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsVpnRaAddressPoolName,
                                     INT4 i4TestValFsVpnRaAddressPoolAddrType)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;
    INT4                i4Found = OSIX_FALSE;

    if ((i4TestValFsVpnRaAddressPoolAddrType != IPVX_ADDR_FMLY_IPV4) &&
        (i4TestValFsVpnRaAddressPoolAddrType != IPVX_ADDR_FMLY_IPV6))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    TMO_SLL_Scan (&gVpnRaAddressPoolList, pVpnRaAddressPoolNode,
                  tVpnRaAddressPool *)
    {
        if (STRNCASECMP (pVpnRaAddressPoolNode->au1VpnRaAddressPoolName,
                         pFsVpnRaAddressPoolName->pu1_OctetList,
                         pFsVpnRaAddressPoolName->i4_Length) == VPN_ZERO)
        {
            if (pVpnRaAddressPoolNode->i4RowStatus == VPN_STATUS_ACTIVE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            i4Found = OSIX_TRUE;
        }
    }

    if (i4Found == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnRaAddressPoolAddrType lv;

    lv.cmd = NMH_TESTV2_FS_VPN_RA_ADDRESS_POOL_ADDR_TYPE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnRaAddressPoolName = pFsVpnRaAddressPoolName;
    lv.i4TestValFsVpnRaAddressPoolAddrType =
        i4TestValFsVpnRaAddressPoolAddrType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnRaAddressPoolStart
 Input       :  The Indices
                FsVpnRaAddressPoolName

                The Object 
                testValFsVpnRaAddressPoolStart
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnRaAddressPoolStart (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsVpnRaAddressPoolName,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsVpnRaAddressPoolStart)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;
    INT4                i4Found = VPN_ZERO;
    UINT4               u4IpAddr = VPN_ZERO;
    tIp6Addr            Ip6Addr;

    if (pTestValFsVpnRaAddressPoolStart->i4_Length == IPVX_IPV4_ADDR_LEN)
    {
        MEMCPY (&u4IpAddr, pTestValFsVpnRaAddressPoolStart->pu1_OctetList,
                sizeof (UINT4));
        if (VpnUtilValidateIpAddress (u4IpAddr) != VPN_UCAST_ADDR)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (pTestValFsVpnRaAddressPoolStart->i4_Length == IPVX_IPV6_ADDR_LEN)
    {
        MEMCPY (&Ip6Addr, pTestValFsVpnRaAddressPoolStart->pu1_OctetList,
                sizeof (UINT4));
        if (Ip6AddrType (&Ip6Addr) != ADDR6_UNICAST)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&gVpnRaAddressPoolList, pVpnRaAddressPoolNode,
                  tVpnRaAddressPool *)
    {
        if (STRNCASECMP (pVpnRaAddressPoolNode->au1VpnRaAddressPoolName,
                         pFsVpnRaAddressPoolName->pu1_OctetList,
                         pFsVpnRaAddressPoolName->i4_Length) == VPN_ZERO)
        {
            if (pVpnRaAddressPoolNode->i4RowStatus == VPN_STATUS_ACTIVE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            i4Found = VPN_ONE;
            continue;
        }
        /* Some other pool should not contain the same range of addresses
         * if yes return failure */
        if ((pVpnRaAddressPoolNode->VpnRaAddressPoolStart.u4AddrType
             == IPVX_ADDR_FMLY_IPV4) &&
            (pTestValFsVpnRaAddressPoolStart->i4_Length == IPVX_IPV4_ADDR_LEN))
        {
            if (((MEMCMP (pTestValFsVpnRaAddressPoolStart->pu1_OctetList,
                          &(pVpnRaAddressPoolNode->VpnRaAddressPoolStart.
                            uIpAddr.Ip4Addr), sizeof (tIp4Addr))) > VPN_ZERO) &&
                ((MEMCMP (pTestValFsVpnRaAddressPoolStart->pu1_OctetList,
                          &(pVpnRaAddressPoolNode->VpnRaAddressPoolEnd.
                            uIpAddr.Ip4Addr), sizeof (tIp4Addr))) < VPN_ZERO))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
        else if ((pVpnRaAddressPoolNode->VpnRaAddressPoolStart.
                  u4AddrType == IPVX_ADDR_FMLY_IPV6) &&
                 (pTestValFsVpnRaAddressPoolStart->i4_Length
                  == IPVX_IPV6_ADDR_LEN))
        {
            if ((Ip6AddrCompare (Ip6Addr,
                                 pVpnRaAddressPoolNode->VpnRaAddressPoolStart.
                                 uIpAddr.Ip6Addr) > IP6_ZERO) &&
                (Ip6AddrCompare (Ip6Addr,
                                 pVpnRaAddressPoolNode->VpnRaAddressPoolEnd.
                                 uIpAddr.Ip6Addr) < IP6_ZERO))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (i4Found == VPN_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnRaAddressPoolStart lv;

    lv.cmd = NMH_TESTV2_FS_VPN_RA_ADDRESS_POOL_START;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnRaAddressPoolName = pFsVpnRaAddressPoolName;
    lv.pTestValFsVpnRaAddressPoolStart = pTestValFsVpnRaAddressPoolStart;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnRaAddressPoolEnd
 Input       :  The Indices
                FsVpnRaAddressPoolName

                The Object 
                testValFsVpnRaAddressPoolEnd
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnRaAddressPoolEnd (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsVpnRaAddressPoolName,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsVpnRaAddressPoolEnd)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;
    INT4                i4Found = VPN_ZERO;
    UINT4               u4IpAddr = VPN_ZERO;
    tIp6Addr            Ip6Addr;

    if (pTestValFsVpnRaAddressPoolEnd->i4_Length == IPVX_IPV4_ADDR_LEN)
    {
        MEMCPY (&u4IpAddr, pTestValFsVpnRaAddressPoolEnd->pu1_OctetList,
                sizeof (UINT4));
        if (VpnUtilValidateIpAddress (u4IpAddr) != VPN_UCAST_ADDR)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (pTestValFsVpnRaAddressPoolEnd->i4_Length == IPVX_IPV6_ADDR_LEN)
    {
        MEMCPY (&Ip6Addr, pTestValFsVpnRaAddressPoolEnd->pu1_OctetList,
                sizeof (UINT4));
        if (Ip6AddrType (&Ip6Addr) != ADDR6_UNICAST)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&gVpnRaAddressPoolList, pVpnRaAddressPoolNode,
                  tVpnRaAddressPool *)
    {
        if (STRNCASECMP (pVpnRaAddressPoolNode->au1VpnRaAddressPoolName,
                         pFsVpnRaAddressPoolName->pu1_OctetList,
                         pFsVpnRaAddressPoolName->i4_Length) == VPN_ZERO)
        {
            if (pVpnRaAddressPoolNode->i4RowStatus == VPN_STATUS_ACTIVE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            i4Found = VPN_ONE;
            continue;
        }

        /* Some other pool should not contain the same range of addresses
         * if yes return failure */
        if ((pVpnRaAddressPoolNode->VpnRaAddressPoolEnd.u4AddrType
             == IPVX_ADDR_FMLY_IPV4) &&
            (pTestValFsVpnRaAddressPoolEnd->i4_Length == IPVX_IPV4_ADDR_LEN))
        {
            if (((MEMCMP (pTestValFsVpnRaAddressPoolEnd->pu1_OctetList,
                          &(pVpnRaAddressPoolNode->VpnRaAddressPoolEnd.
                            uIpAddr.Ip4Addr), sizeof (tIp4Addr))) > VPN_ZERO) &&
                ((MEMCMP (pTestValFsVpnRaAddressPoolEnd->pu1_OctetList,
                          &(pVpnRaAddressPoolNode->VpnRaAddressPoolEnd.
                            uIpAddr.Ip4Addr), sizeof (tIp4Addr))) < VPN_ZERO))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
        else if ((pVpnRaAddressPoolNode->VpnRaAddressPoolEnd.u4AddrType
                  == IPVX_ADDR_FMLY_IPV6) &&
                 (pTestValFsVpnRaAddressPoolEnd->i4_Length
                  == IPVX_IPV6_ADDR_LEN))
        {
            if ((Ip6AddrCompare (Ip6Addr,
                                 pVpnRaAddressPoolNode->VpnRaAddressPoolEnd.
                                 uIpAddr.Ip6Addr) > IP6_ZERO) &&
                (Ip6AddrCompare (Ip6Addr,
                                 pVpnRaAddressPoolNode->VpnRaAddressPoolEnd.
                                 uIpAddr.Ip6Addr) < IP6_ZERO))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (i4Found == VPN_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

#else
    int                 rc;
    tNpwnmhTestv2FsVpnRaAddressPoolEnd lv;

    lv.cmd = NMH_TESTV2_FS_VPN_RA_ADDRESS_POOL_END;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnRaAddressPoolName = pFsVpnRaAddressPoolName;
    lv.pTestValFsVpnRaAddressPoolEnd = pTestValFsVpnRaAddressPoolEnd;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnRaAddressPoolPrefixLen
 Input       :  The Indices
                FsVpnRaAddressPoolName

                The Object 
                testValFsVpnRaAddressPoolPrefixLen
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnRaAddressPoolPrefixLen (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsVpnRaAddressPoolName,
                                      UINT4
                                      u4TestValFsVpnRaAddressPoolPrefixLen)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;
    INT4                i4Found = OSIX_FALSE;

    TMO_SLL_Scan (&gVpnRaAddressPoolList, pVpnRaAddressPoolNode,
                  tVpnRaAddressPool *)
    {
        if (STRNCASECMP (pVpnRaAddressPoolNode->au1VpnRaAddressPoolName,
                         pFsVpnRaAddressPoolName->pu1_OctetList,
                         pFsVpnRaAddressPoolName->i4_Length) == VPN_ZERO)
        {
            if (pVpnRaAddressPoolNode->i4RowStatus == VPN_STATUS_ACTIVE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            else
            {
                if (((pVpnRaAddressPoolNode->VpnRaAddressPoolEnd.u4AddrType
                      == IPVX_ADDR_FMLY_IPV4) &&
                     (u4TestValFsVpnRaAddressPoolPrefixLen
                      > IPVX_IPV4_MAX_MASK_LEN)) ||
                    ((pVpnRaAddressPoolNode->VpnRaAddressPoolEnd.u4AddrType
                      == IPVX_ADDR_FMLY_IPV6) &&
                     (u4TestValFsVpnRaAddressPoolPrefixLen
                      > IP6_ADDR_MAX_PREFIX)))
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
            }

            i4Found = OSIX_TRUE;
        }
    }

    if (i4Found == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

#else

    int                 rc;
    tNpwnmhTestv2FsVpnRaAddressPoolPrefixLen lv;

    lv.cmd = NMH_TESTV2_FS_VPN_RA_ADDRESS_POOL_PREFIX_LEN;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnRaAddressPoolName = pFsVpnRaAddressPoolName;
    lv.u4TestValFsVpnRaAddressPoolPrefixLen =
        u4TestValFsVpnRaAddressPoolPrefixLen;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnRaAddressPoolRowStatus
 Input       :  The Indices
                FsVpnRaAddressPoolName

                The Object 
                testValFsVpnRaAddressPoolRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnRaAddressPoolRowStatus (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsVpnRaAddressPoolName,
                                      INT4 i4TestValFsVpnRaAddressPoolRowStatus)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;
    UINT4               u4IfIndex = VPN_ZERO;
    tIp6Addr            NullIp6Addr;

    MEMSET (&NullIp6Addr, VPN_ZERO, sizeof (tIp6Addr));

    pVpnRaAddressPoolNode =
        VpnGetRaAddrPool (pFsVpnRaAddressPoolName->pu1_OctetList,
                          pFsVpnRaAddressPoolName->i4_Length);
    switch (i4TestValFsVpnRaAddressPoolRowStatus)
    {
        case VPN_STATUS_CREATE_AND_WAIT:
            if (pVpnRaAddressPoolNode != NULL)
            {
                /* Entry already exists */
                CLI_SET_ERR (CLI_VPN_ERR_DUP_ENTRY);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }

            if (TMO_SLL_Count (&gVpnRaAddressPoolList) ==
                RA_VPN_MAX_ADDRESS_POOL)
            {
                CLI_SET_ERR (CLI_VPN_ERR_RAVPN_MAX_ADDR_POOLS);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if ((pFsVpnRaAddressPoolName->i4_Length >
                 RA_ADDRESS_POOL_NAME_LENGTH) ||
                (pFsVpnRaAddressPoolName->i4_Length <
                 VPN_RA_MIN_ADDR_POOL_NAME_LEN))
            {
                CLI_SET_ERR (CLI_VPN_ERR_RAVPN_ADDR_POOL_NAME_LEN);
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return SNMP_FAILURE;
            }

            break;

        case VPN_STATUS_ACTIVE:
            if (pVpnRaAddressPoolNode == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }

            if (pVpnRaAddressPoolNode->VpnRaAddressPoolStart.u4AddrType
                == IPVX_ADDR_FMLY_IPV4)
            {
                if ((pVpnRaAddressPoolNode->VpnRaAddressPoolStart.
                     uIpAddr.Ip4Addr == VPN_ZERO) ||
                    (pVpnRaAddressPoolNode->VpnRaAddressPoolEnd.
                     uIpAddr.Ip4Addr == VPN_ZERO))
                {
                    /* Both the values have to be configured . */
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

                if (SecUtilIpIsLocalNet (pVpnRaAddressPoolNode->
                                         VpnRaAddressPoolStart.uIpAddr.
                                         Ip4Addr,
                                         &u4IfIndex) == NETIPV4_SUCCESS)
                {
                    /* Local n/w addresses can't be assigned to remote
                     * persons */
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            else if (pVpnRaAddressPoolNode->VpnRaAddressPoolStart.u4AddrType
                     == IPVX_ADDR_FMLY_IPV6)
            {
                if ((Ip6AddrCompare (pVpnRaAddressPoolNode->
                                     VpnRaAddressPoolStart.uIpAddr.Ip6Addr,
                                     NullIp6Addr) == IP6_ZERO) ||
                    (Ip6AddrCompare (pVpnRaAddressPoolNode->
                                     VpnRaAddressPoolEnd.uIpAddr.Ip6Addr,
                                     NullIp6Addr) == IP6_ZERO))
                {
                    /* Both the values have to be configured . */
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

                if (Sec6UtilIpIfIsOurAddress (&(pVpnRaAddressPoolNode->
                                                VpnRaAddressPoolStart.uIpAddr.
                                                Ip6Addr),
                                              &u4IfIndex) == NETIPV4_SUCCESS)
                {
                    /* Local n/w addresses can't be assigned to remote
                     * persons */
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            break;

        case VPN_STATUS_NOT_IN_SERVICE:
        case VPN_STATUS_DESTROY:
            if (pVpnRaAddressPoolNode == NULL)
            {
                CLI_SET_ERR (CLI_VPN_ERR_NO_RAVPN_POOL);
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            if (pVpnRaAddressPoolNode->i4RefCnt > VPN_ZERO)
            {
                CLI_SET_ERR (CLI_VPN_ERR_RAVPN_POOL_ACTIVE);
                return SNMP_FAILURE;
            }

            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnRaAddressPoolRowStatus lv;

    lv.cmd = NMH_TESTV2_FS_VPN_RA_ADDRESS_POOL_ROW_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnRaAddressPoolName = pFsVpnRaAddressPoolName;
    lv.i4TestValFsVpnRaAddressPoolRowStatus =
        i4TestValFsVpnRaAddressPoolRowStatus;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVpnRaAddressPoolTable
 Input       :  The Indices
                FsVpnRaAddressPoolName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVpnRaAddressPoolTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsVpnRemoteIdTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVpnRemoteIdTable
 Input       :  The Indices
                FsVpnRemoteIdType
                FsVpnRemoteIdValue
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVpnRemoteIdTable (INT4 i4FsVpnRemoteIdType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsVpnRemoteIdValue)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnIdInfo         *pVpnIdInfo = NULL;

    pVpnIdInfo = VpnGetRemoteIdInfo (i4FsVpnRemoteIdType,
                                     pFsVpnRemoteIdValue->pu1_OctetList,
                                     pFsVpnRemoteIdValue->i4_Length);
    if (pVpnIdInfo == NULL)
    {
        return (SNMP_FAILURE);
    }
    else
    {
        return (SNMP_SUCCESS);
    }
#else
    int                 rc;
    tNpwnmhValidateIndexInstanceFsVpnRemoteIdTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_FS_VPN_REMOTE_ID_TABLE;
    lv.i4FsVpnRemoteIdType = i4FsVpnRemoteIdType;
    lv.pFsVpnRemoteIdValue = pFsVpnRemoteIdValue;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVpnRemoteIdTable
 Input       :  The Indices
                FsVpnRemoteIdType
                FsVpnRemoteIdValue
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVpnRemoteIdTable (INT4 *pi4FsVpnRemoteIdType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsVpnRemoteIdValue)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnIdInfo         *pVpnIdInfo = NULL;

    pVpnIdInfo = (tVpnIdInfo *) TMO_SLL_First (&gVpnRemoteIdList);
    if (pVpnIdInfo == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4FsVpnRemoteIdType = VPN_ID_TYPE (pVpnIdInfo);
    pFsVpnRemoteIdValue->i4_Length = (INT4) STRLEN (VPN_ID_VALUE (pVpnIdInfo));
    STRNCPY (pFsVpnRemoteIdValue->pu1_OctetList, VPN_ID_VALUE (pVpnIdInfo),
             (size_t) pFsVpnRemoteIdValue->i4_Length);

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFirstIndexFsVpnRemoteIdTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_FS_VPN_REMOTE_ID_TABLE;
    lv.pi4FsVpnRemoteIdType = pi4FsVpnRemoteIdType;
    lv.pFsVpnRemoteIdValue = pFsVpnRemoteIdValue;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVpnRemoteIdTable
 Input       :  The Indices
                FsVpnRemoteIdType
                nextFsVpnRemoteIdType
                FsVpnRemoteIdValue
                nextFsVpnRemoteIdValue
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsVpnRemoteIdTable (INT4 i4FsVpnRemoteIdType,
                                   INT4 *pi4NextFsVpnRemoteIdType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsVpnRemoteIdValue,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextFsVpnRemoteIdValue)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnIdInfo         *pVpnIdInfo = NULL;

    pVpnIdInfo = VpnGetRemoteIdInfo (i4FsVpnRemoteIdType,
                                     pFsVpnRemoteIdValue->pu1_OctetList,
                                     pFsVpnRemoteIdValue->i4_Length);
    pVpnIdInfo = (tVpnIdInfo *) TMO_SLL_Next ((tTMO_SLL *) & gVpnRemoteIdList,
                                              (tTMO_SLL_NODE *) pVpnIdInfo);
    if (pVpnIdInfo == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4NextFsVpnRemoteIdType = VPN_ID_TYPE (pVpnIdInfo);
    pNextFsVpnRemoteIdValue->i4_Length =
        (INT4) STRLEN (VPN_ID_VALUE (pVpnIdInfo));
    STRNCPY (pNextFsVpnRemoteIdValue->pu1_OctetList, VPN_ID_VALUE (pVpnIdInfo),
             (size_t) pNextFsVpnRemoteIdValue->i4_Length);

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetNextIndexFsVpnRemoteIdTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_FS_VPN_REMOTE_ID_TABLE;
    lv.i4FsVpnRemoteIdType = i4FsVpnRemoteIdType;
    lv.pi4NextFsVpnRemoteIdType = pi4NextFsVpnRemoteIdType;
    lv.pFsVpnRemoteIdValue = pFsVpnRemoteIdValue;
    lv.pNextFsVpnRemoteIdValue = pNextFsVpnRemoteIdValue;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVpnRemoteIdKey
 Input       :  The Indices
                FsVpnRemoteIdType
                FsVpnRemoteIdValue

                The Object 
                retValFsVpnRemoteIdKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnRemoteIdKey (INT4 i4FsVpnRemoteIdType,
                        tSNMP_OCTET_STRING_TYPE * pFsVpnRemoteIdValue,
                        tSNMP_OCTET_STRING_TYPE * pRetValFsVpnRemoteIdKey)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnIdInfo         *pVpnIdInfo = NULL;
    pVpnIdInfo = VpnGetRemoteIdInfo (i4FsVpnRemoteIdType,
                                     pFsVpnRemoteIdValue->pu1_OctetList,
                                     pFsVpnRemoteIdValue->i4_Length);

    if (pVpnIdInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRetValFsVpnRemoteIdKey->i4_Length =
        (INT4) STRLEN (VPN_ID_KEY (pVpnIdInfo));
    STRNCPY (pRetValFsVpnRemoteIdKey->pu1_OctetList, VPN_ID_KEY (pVpnIdInfo),
             (size_t) pRetValFsVpnRemoteIdKey->i4_Length);
    /* Added to satisfy both the requirements
     * (1) Not displaying the secret keys in CLI/WEB/SNMP
     * (2) Secret keys should be saved in secured memory during MSR
     */

    if (VpnGetMsrSaveStatus () != ISS_TRUE)
    {
        pRetValFsVpnRemoteIdKey->i4_Length = VPN_ZERO;
    }
    return SNMP_SUCCESS;

#else
    int                 rc;
    tNpwnmhGetFsVpnRemoteIdKey lv;

    lv.cmd = NMH_GET_FS_VPN_REMOTE_ID_KEY;
    lv.i4FsVpnRemoteIdType = i4FsVpnRemoteIdType;
    lv.pFsVpnRemoteIdValue = pFsVpnRemoteIdValue;
    lv.pRetValFsVpnRemoteIdKey = pRetValFsVpnRemoteIdKey;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);

    if (VpnGetMsrSaveStatus () != ISS_TRUE)
    {
        pRetValFsVpnRemoteIdKey->i4_Length = VPN_ZERO;
    }

    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnRemoteIdAuthType
 Input       :  The Indices
                FsVpnRemoteIdType
                FsVpnRemoteIdValue

                The Object 
                retValFsVpnRemoteIdAuthType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnRemoteIdAuthType (INT4 i4FsVpnRemoteIdType,
                             tSNMP_OCTET_STRING_TYPE * pFsVpnRemoteIdValue,
                             INT4 *pi4RetValFsVpnRemoteIdAuthType)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnIdInfo         *pVpnIdInfo = NULL;

    pVpnIdInfo = VpnGetRemoteIdInfo (i4FsVpnRemoteIdType,
                                     pFsVpnRemoteIdValue->pu1_OctetList,
                                     pFsVpnRemoteIdValue->i4_Length);
    if (pVpnIdInfo == NULL)
    {
        return (SNMP_FAILURE);
    }
    *pi4RetValFsVpnRemoteIdAuthType = (INT4) pVpnIdInfo->u4AuthType;
    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsVpnRemoteIdAuthType lv;

    lv.cmd = NMH_GET_FS_VPN_REMOTE_ID_AUTH_TYPE;
    lv.i4FsVpnRemoteIdType = i4FsVpnRemoteIdType;
    lv.pFsVpnRemoteIdValue = pFsVpnRemoteIdValue;
    lv.pi4RetValFsVpnRemoteIdAuthType = pi4RetValFsVpnRemoteIdAuthType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnRemoteIdStatus
 Input       :  The Indices
                FsVpnRemoteIdType
                FsVpnRemoteIdValue

                The Object 
                retValFsVpnRemoteIdStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnRemoteIdStatus (INT4 i4FsVpnRemoteIdType,
                           tSNMP_OCTET_STRING_TYPE * pFsVpnRemoteIdValue,
                           INT4 *pi4RetValFsVpnRemoteIdStatus)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnIdInfo         *pVpnIdInfo = NULL;

    pVpnIdInfo = VpnGetRemoteIdInfo (i4FsVpnRemoteIdType,
                                     pFsVpnRemoteIdValue->pu1_OctetList,
                                     pFsVpnRemoteIdValue->i4_Length);
    if (pVpnIdInfo == NULL)
    {
        return (SNMP_FAILURE);
    }

    *pi4RetValFsVpnRemoteIdStatus = VPN_ID_STATUS (pVpnIdInfo);

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhGetFsVpnRemoteIdStatus lv;

    lv.cmd = NMH_GET_FS_VPN_REMOTE_ID_STATUS;
    lv.i4FsVpnRemoteIdType = i4FsVpnRemoteIdType;
    lv.pFsVpnRemoteIdValue = pFsVpnRemoteIdValue;
    lv.pi4RetValFsVpnRemoteIdStatus = pi4RetValFsVpnRemoteIdStatus;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVpnRemoteIdKey
 Input       :  The Indices
                FsVpnRemoteIdType
                FsVpnRemoteIdValue

                The Object 
                setValFsVpnRemoteIdKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnRemoteIdKey (INT4 i4FsVpnRemoteIdType,
                        tSNMP_OCTET_STRING_TYPE * pFsVpnRemoteIdValue,
                        tSNMP_OCTET_STRING_TYPE * pSetValFsVpnRemoteIdKey)
{
    tVpnIdInfo         *pVpnIdInfo = NULL;
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pVpnIdInfo = VpnGetRemoteIdInfo (i4FsVpnRemoteIdType,
                                     pFsVpnRemoteIdValue->pu1_OctetList,
                                     pFsVpnRemoteIdValue->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnIdInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    STRNCPY (VPN_ID_KEY (pVpnIdInfo), pSetValFsVpnRemoteIdKey->pu1_OctetList,
             (size_t) pSetValFsVpnRemoteIdKey->i4_Length);

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnRemoteIdKey lv;

    lv.cmd = NMH_SET_FS_VPN_REMOTE_ID_KEY;
    lv.i4FsVpnRemoteIdType = i4FsVpnRemoteIdType;
    lv.pFsVpnRemoteIdValue = pFsVpnRemoteIdValue;
    lv.pSetValFsVpnRemoteIdKey = pSetValFsVpnRemoteIdKey;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnRemoteIdKey, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_TWO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %s", i4FsVpnRemoteIdType,
                      pFsVpnRemoteIdValue, pSetValFsVpnRemoteIdKey));
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnRemoteIdAuthType
 Input       :  The Indices
                FsVpnRemoteIdType
                FsVpnRemoteIdValue
                                                                                                                             
                The Object
                setValFsVpnRemoteIdAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnRemoteIdAuthType (INT4 i4FsVpnRemoteIdType,
                             tSNMP_OCTET_STRING_TYPE * pFsVpnRemoteIdValue,
                             INT4 i4SetValFsVpnRemoteIdAuthType)
{
    tVpnIdInfo         *pVpnIdInfo = NULL;
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    pVpnIdInfo = VpnGetRemoteIdInfo (i4FsVpnRemoteIdType,
                                     pFsVpnRemoteIdValue->pu1_OctetList,
                                     pFsVpnRemoteIdValue->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnIdInfo == NULL)
    {
        return (SNMP_FAILURE);
    }
    pVpnIdInfo->u4AuthType = (UINT4) i4SetValFsVpnRemoteIdAuthType;
#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnRemoteIdAuthType lv;

    lv.cmd = NMH_SET_FS_VPN_REMOTE_ID_AUTH_TYPE;
    lv.i4FsVpnRemoteIdType = i4FsVpnRemoteIdType;
    lv.pFsVpnRemoteIdValue = pFsVpnRemoteIdValue;
    lv.i4SetValFsVpnRemoteIdAuthType = i4SetValFsVpnRemoteIdAuthType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnRemoteIdAuthType, u4SeqNum,
                          FALSE, VpnDsLock, VpnDsUnLock, VPN_TWO, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i", i4FsVpnRemoteIdType,
                      pFsVpnRemoteIdValue, i4SetValFsVpnRemoteIdAuthType));
    return (SNMP_SUCCESS);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnRemoteIdStatus
 Input       :  The Indices
                FsVpnRemoteIdType
                FsVpnRemoteIdValue

                The Object 
                setValFsVpnRemoteIdStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnRemoteIdStatus (INT4 i4FsVpnRemoteIdType,
                           tSNMP_OCTET_STRING_TYPE * pFsVpnRemoteIdValue,
                           INT4 i4SetValFsVpnRemoteIdStatus)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnIdInfo         *pVpnIdInfo = NULL;
    INT1                i1RetVal = SNMP_SUCCESS;
    UINT4               u4TempLen = VPN_ZERO;
#ifdef IKE_WANTED
    UINT1               au1IdType[VPN_ID_MAX_LEN] = { VPN_ZERO };
#endif
    RM_GET_SEQ_NUM (&u4SeqNum);
    switch (i4SetValFsVpnRemoteIdStatus)
    {
        case ACTIVE:
            pVpnIdInfo = VpnGetRemoteIdInfo (i4FsVpnRemoteIdType,
                                             pFsVpnRemoteIdValue->pu1_OctetList,
                                             pFsVpnRemoteIdValue->i4_Length);
            if (pVpnIdInfo == NULL)
            {
                return (SNMP_FAILURE);
            }

            if (VPN_ID_STATUS (pVpnIdInfo) == ACTIVE)
            {
                /* Do nothig;  Already active */
                break;
            }
            if (i1RetVal == SNMP_SUCCESS)
            {
                VPN_ID_STATUS (pVpnIdInfo) = ACTIVE;
            }

            break;

        case NOT_IN_SERVICE:
            pVpnIdInfo = VpnGetRemoteIdInfo (i4FsVpnRemoteIdType,
                                             pFsVpnRemoteIdValue->pu1_OctetList,
                                             pFsVpnRemoteIdValue->i4_Length);
            if (pVpnIdInfo == NULL)
            {
                return (SNMP_FAILURE);
            }

            if (VPN_ID_STATUS (pVpnIdInfo) == ACTIVE)
            {
#ifdef IKE_WANTED
                i1RetVal = VpnUtilDeleteKeyDB (pVpnIdInfo);
#endif
            }

            if (i1RetVal == SNMP_SUCCESS)
            {
                VPN_ID_STATUS (pVpnIdInfo) = NOT_IN_SERVICE;
            }

            break;

        case CREATE_AND_WAIT:

            if (MemAllocateMemBlock
                (VPN_ID_PID, (UINT1 **) (VOID *) &pVpnIdInfo) != MEM_SUCCESS)
            {
                return (SNMP_FAILURE);
            }

            VPN_MEMSET (pVpnIdInfo, VPN_ZERO, sizeof (tVpnIdInfo));

            VPN_ID_TYPE (pVpnIdInfo) = (INT2) i4FsVpnRemoteIdType;
            u4TempLen =
                MEM_MAX_BYTES ((UINT4) pFsVpnRemoteIdValue->i4_Length,
                               STRLEN (pFsVpnRemoteIdValue->pu1_OctetList));
            MEMCPY (VPN_ID_VALUE (pVpnIdInfo),
                    pFsVpnRemoteIdValue->pu1_OctetList, u4TempLen);
            VPN_ID_VALUE (pVpnIdInfo)[u4TempLen] = '\0';
            VPN_ID_STATUS (pVpnIdInfo) = NOT_READY;

            TMO_SLL_Add (&gVpnRemoteIdList, (tTMO_SLL_NODE *) pVpnIdInfo);

            break;

        case DESTROY:
            pVpnIdInfo = VpnGetRemoteIdInfo (i4FsVpnRemoteIdType,
                                             pFsVpnRemoteIdValue->pu1_OctetList,
                                             pFsVpnRemoteIdValue->i4_Length);

            if (pVpnIdInfo == NULL)
            {
                return (SNMP_FAILURE);
            }

            if (VPN_ID_STATUS (pVpnIdInfo) == ACTIVE)
            {
#ifdef IKE_WANTED
                if (pVpnIdInfo->u4AuthType == VPN_AUTH_PRESHARED)
                {
                    i1RetVal = VpnUtilDeleteKeyDB (pVpnIdInfo);
                }
                else if (pVpnIdInfo->u4AuthType == VPN_AUTH_CERT)
                {
                    if (pVpnIdInfo->i2IdType == CLI_VPN_IKE_KEY_IPV4)
                    {
                        STRCPY (au1IdType, "ipv4");
                    }
                    else if (pVpnIdInfo->i2IdType == CLI_VPN_IKE_KEY_EMAIL)
                    {
                        STRCPY (au1IdType, "email");
                    }
                    else if (pVpnIdInfo->i2IdType == CLI_VPN_IKE_KEY_FQDN)
                    {
                        STRCPY (au1IdType, "fqdn");
                    }
                    else if (pVpnIdInfo->i2IdType == CLI_VPN_IKE_KEY_IPV6)
                    {
                        STRCPY (au1IdType, "ipv6");
                    }
                    else if (pVpnIdInfo->i2IdType == CLI_VPN_IKE_KEY_DN)
                    {
                        STRCPY (au1IdType, "dn");
                    }
                    else if (pVpnIdInfo->i2IdType == CLI_VPN_IKE_KEY_KEYID)
                    {
                        STRCPY (au1IdType, "keyid");
                    }
                    i1RetVal = (INT1) VpnIkeDelCertMap ((UINT1 *)
                                                        pVpnIdInfo->ac1IdKey,
                                                        au1IdType,
                                                        (UINT1 *) pVpnIdInfo->
                                                        ac1IdValue);
                }
#endif
            }

            if (i1RetVal == SNMP_SUCCESS)
            {
                TMO_SLL_Delete (&gVpnRemoteIdList,
                                (tTMO_SLL_NODE *) pVpnIdInfo);
                MemReleaseMemBlock (VPN_ID_PID, (UINT1 *) pVpnIdInfo);
            }

            break;
        default:
            break;
    }

#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnRemoteIdStatus lv;

    lv.cmd = NMH_SET_FS_VPN_REMOTE_ID_STATUS;
    lv.i4FsVpnRemoteIdType = i4FsVpnRemoteIdType;
    lv.pFsVpnRemoteIdValue = pFsVpnRemoteIdValue;
    lv.i4SetValFsVpnRemoteIdStatus = i4SetValFsVpnRemoteIdStatus;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    if (i1RetVal == SNMP_SUCCESS)
    {
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnRemoteIdStatus, u4SeqNum,
                              TRUE, VpnDsLock, VpnDsUnLock, VPN_TWO,
                              SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4FsVpnRemoteIdType,
                          pFsVpnRemoteIdValue, i4SetValFsVpnRemoteIdStatus));
    }
    return (i1RetVal);
#endif
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVpnRemoteIdKey
 Input       :  The Indices
                FsVpnRemoteIdType
                FsVpnRemoteIdValue

                The Object 
                testValFsVpnRemoteIdKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnRemoteIdKey (UINT4 *pu4ErrorCode, INT4 i4FsVpnRemoteIdType,
                           tSNMP_OCTET_STRING_TYPE * pFsVpnRemoteIdValue,
                           tSNMP_OCTET_STRING_TYPE * pTestValFsVpnRemoteIdKey)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnIdInfo         *pVpnIdInfo = NULL;

    pVpnIdInfo = VpnGetRemoteIdInfo (i4FsVpnRemoteIdType,
                                     pFsVpnRemoteIdValue->pu1_OctetList,
                                     pFsVpnRemoteIdValue->i4_Length);
    if (pVpnIdInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    if (pVpnIdInfo->u4AuthType == VPN_AUTH_PRESHARED)
    {
        if ((pTestValFsVpnRemoteIdKey->i4_Length < VPN_MIN_PRESHARED_KEY_LEN) ||
            (pTestValFsVpnRemoteIdKey->i4_Length > VPN_MAX_PRESHARED_KEY_LEN))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            return (SNMP_FAILURE);
        }
    }
    else
    {
        if (pTestValFsVpnRemoteIdKey->i4_Length > VPN_MAX_KEY_NAME_LEN)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            return (SNMP_FAILURE);
        }
    }
    return (SNMP_SUCCESS);
#else

    int                 rc;
    tNpwnmhTestv2FsVpnRemoteIdKey lv;

    lv.cmd = NMH_TESTV2_FS_VPN_REMOTE_ID_KEY;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4FsVpnRemoteIdType = i4FsVpnRemoteIdType;
    lv.pFsVpnRemoteIdValue = pFsVpnRemoteIdValue;
    lv.pTestValFsVpnRemoteIdKey = pTestValFsVpnRemoteIdKey;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnRemoteIdAuthType
 Input       :  The Indices
                FsVpnRemoteIdType
                FsVpnRemoteIdValue

                The Object 
                testValFsVpnRemoteIdStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnRemoteIdAuthType (UINT4 *pu4ErrorCode, INT4 i4FsVpnRemoteIdType,
                                tSNMP_OCTET_STRING_TYPE * pFsVpnRemoteIdValue,
                                INT4 i4TestValFsVpnRemoteIdAuthType)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnIdInfo         *pVpnIdInfo = NULL;

    pVpnIdInfo = VpnGetRemoteIdInfo (i4FsVpnRemoteIdType,
                                     pFsVpnRemoteIdValue->pu1_OctetList,
                                     pFsVpnRemoteIdValue->i4_Length);
    if (pVpnIdInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    if ((i4TestValFsVpnRemoteIdAuthType != VPN_AUTH_PRESHARED) &&
        (i4TestValFsVpnRemoteIdAuthType != VPN_AUTH_CERT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhTestv2FsVpnRemoteIdAuthType lv;

    lv.cmd = NMH_TESTV2_FS_VPN_REMOTE_ID_AUTH_TYPE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4FsVpnRemoteIdType = i4FsVpnRemoteIdType;
    lv.pFsVpnRemoteIdValue = pFsVpnRemoteIdValue;
    lv.i4TestValFsVpnRemoteIdAuthType = i4TestValFsVpnRemoteIdAuthType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnRemoteIdStatus
 Input       :  The Indices
                FsVpnRemoteIdType
                FsVpnRemoteIdValue

                The Object 
                testValFsVpnRemoteIdStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnRemoteIdStatus (UINT4 *pu4ErrorCode, INT4 i4FsVpnRemoteIdType,
                              tSNMP_OCTET_STRING_TYPE * pFsVpnRemoteIdValue,
                              INT4 i4TestValFsVpnRemoteIdStatus)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tIp6Addr            Ip6Addr;
    tVpnIdInfo         *pVpnIdInfo = NULL;
    UINT4               u4TempIpAddr = VPN_ZERO;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE] = { VPN_ZERO };

    if (i4TestValFsVpnRemoteIdStatus != DESTROY)
    {
        /* Tweak the code to allow deletion of invalid restored items. */
        switch (i4FsVpnRemoteIdType)
        {
            case VPN_ID_TYPE_IPV4:
                MEMSET (au1OctetListIdx, VPN_ZERO, MAX_OCTETSTRING_SIZE);
                MEMCPY (au1OctetListIdx, pFsVpnRemoteIdValue->pu1_OctetList,
                        pFsVpnRemoteIdValue->i4_Length);
                u4TempIpAddr = OSIX_NTOHL (INET_ADDR (au1OctetListIdx));
                if (VpnUtilValidateIpAddress (u4TempIpAddr) != VPN_UCAST_ADDR)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    return SNMP_FAILURE;
                }
                break;

            case VPN_ID_TYPE_IPV6:

                if (INET_ATON6 (pFsVpnRemoteIdValue->pu1_OctetList,
                                &Ip6Addr) == VPN_ZERO)
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }

                if (Ip6AddrType (&Ip6Addr) != ADDR6_UNICAST)
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
                break;

            case VPN_ID_TYPE_FQDN:
                if (VpnUtilsIsFqdn ((CHR1 *) pFsVpnRemoteIdValue->pu1_OctetList,
                                    pFsVpnRemoteIdValue->i4_Length) == FALSE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    return (SNMP_FAILURE);
                }
                break;

            case VPN_ID_TYPE_EMAIL:
                if (VpnUtilValidateEmailAddr (pFsVpnRemoteIdValue) ==
                    SNMP_FAILURE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    return (SNMP_FAILURE);
                }
                break;

            case VPN_ID_TYPE_KEYID:
                break;

            case VPN_ID_TYPE_DN:
                if (VpnUtilsIsDn ((CHR1 *) pFsVpnRemoteIdValue->pu1_OctetList,
                                  pFsVpnRemoteIdValue->i4_Length) == FALSE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    return (SNMP_FAILURE);
                }
                break;
            default:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return (SNMP_FAILURE);
        }
    }

    if (pFsVpnRemoteIdValue->i4_Length > VPN_MAX_NAME_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return (SNMP_FAILURE);
    }

    pVpnIdInfo = VpnGetRemoteIdInfo (i4FsVpnRemoteIdType,
                                     pFsVpnRemoteIdValue->pu1_OctetList,
                                     pFsVpnRemoteIdValue->i4_Length);
    switch (i4TestValFsVpnRemoteIdStatus)
    {
        case ACTIVE:
            if (pVpnIdInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return (SNMP_FAILURE);
            }

            if (STRLEN (VPN_ID_KEY (pVpnIdInfo)) < VPN_MIN_PRESHARED_KEY_LEN)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
            }

            break;

        case NOT_IN_SERVICE:
            if (pVpnIdInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return (SNMP_FAILURE);
            }
            break;

        case CREATE_AND_GO:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);

        case CREATE_AND_WAIT:
            if (pVpnIdInfo != NULL)
            {
                CLI_SET_ERR (CLI_VPN_ERR_DUP_ENTRY);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return (SNMP_FAILURE);
            }

            if (TMO_SLL_Count (&gVpnRemoteIdList) == VPN_SYS_MAX_REMOTE_IDS)
            {
                CLI_SET_ERR (CLI_VPN_ERR_MAX_REMOTE_IDS);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return (SNMP_FAILURE);
            }
            break;

        case DESTROY:
            if (pVpnIdInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (CLI_VPN_ERR_REMOTE_ID_NOT_EXIST);
                return (SNMP_FAILURE);
            }

            if (pVpnIdInfo->i4RefCnt > VPN_ZERO)
            {
                /* Remote-id is in use */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_VPN_ERR_REMOTE_ID_ACTIVE);
                return (SNMP_FAILURE);
            }
            break;
        default:
            break;
    }

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhTestv2FsVpnRemoteIdStatus lv;

    lv.cmd = NMH_TESTV2_FS_VPN_REMOTE_ID_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.i4FsVpnRemoteIdType = i4FsVpnRemoteIdType;
    lv.pFsVpnRemoteIdValue = pFsVpnRemoteIdValue;
    lv.i4TestValFsVpnRemoteIdStatus = i4TestValFsVpnRemoteIdStatus;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVpnRemoteIdTable
 Input       :  The Indices
                FsVpnRemoteIdType
                FsVpnRemoteIdValue
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVpnRemoteIdTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsVpnCertInfoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVpnCertInfoTable
 Input       :  The Indices
                FsVpnCertKeyString
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVpnCertInfoTable (tSNMP_OCTET_STRING_TYPE *
                                            pFsVpnCertKeyString)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnCertInfo       *pVpnCertInfo = NULL;

    pVpnCertInfo = VpnGetCertInfoFromKey (pFsVpnCertKeyString->pu1_OctetList,
                                          pFsVpnCertKeyString->i4_Length);
    if (pVpnCertInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
#else
    int                 rc;
    tNpwnmhValidateIndexInstanceFsVpnCertInfoTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_FS_VPN_CERT_INFO_TABLE;
    lv.pFsVpnCertKeyString = pFsVpnCertKeyString;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVpnCertInfoTable
 Input       :  The Indices
                FsVpnCertKeyString
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVpnCertInfoTable (tSNMP_OCTET_STRING_TYPE *
                                    pFsVpnCertKeyString)
{

#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnCertInfo       *pVpnCertInfo = NULL;

    pVpnCertInfo = (tVpnCertInfo *) TMO_SLL_First (&gVpnCertInfoList);
    if (pVpnCertInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pFsVpnCertKeyString->i4_Length = (INT4) STRLEN (pVpnCertInfo->ac1CertKeyId);
    MEMCPY (pFsVpnCertKeyString->pu1_OctetList,
            pVpnCertInfo->ac1CertKeyId, pFsVpnCertKeyString->i4_Length);
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFirstIndexFsVpnCertInfoTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_FS_VPN_CERT_INFO_TABLE;
    lv.pFsVpnCertKeyString = pFsVpnCertKeyString;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVpnCertInfoTable
 Input       :  The Indices
                FsVpnCertKeyString
                nextFsVpnCertKeyString
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsVpnCertInfoTable (tSNMP_OCTET_STRING_TYPE *
                                   pFsVpnCertKeyString,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextFsVpnCertKeyString)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnCertInfo       *pVpnCertInfo = NULL;

    pVpnCertInfo = VpnGetCertInfoFromKey (pFsVpnCertKeyString->pu1_OctetList,
                                          pFsVpnCertKeyString->i4_Length);

    pVpnCertInfo =
        (tVpnCertInfo *) TMO_SLL_Next ((tTMO_SLL *) & gVpnCertInfoList,
                                       (tTMO_SLL_NODE *) pVpnCertInfo);
    if (pVpnCertInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pNextFsVpnCertKeyString->i4_Length =
        (INT4) STRLEN (pVpnCertInfo->ac1CertKeyId);
    MEMCPY (pNextFsVpnCertKeyString->pu1_OctetList, pVpnCertInfo->ac1CertKeyId,
            pNextFsVpnCertKeyString->i4_Length);
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetNextIndexFsVpnCertInfoTable lv;
    lv.cmd = NMH_GET_NEXT_INDEX_FS_VPN_CERT_INFO_TABLE;
    lv.pFsVpnCertKeyString = pFsVpnCertKeyString;
    lv.pNextFsVpnCertKeyString = pNextFsVpnCertKeyString;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVpnCertKeyType
 Input       :  The Indices
                FsVpnCertKeyString

                The Object 
                retValFsVpnCertKeyType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnCertKeyType (tSNMP_OCTET_STRING_TYPE * pFsVpnCertKeyString,
                        INT4 *pi4RetValFsVpnCertKeyType)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnCertInfo       *pVpnCertInfo = NULL;

    pVpnCertInfo = VpnGetCertInfoFromKey (pFsVpnCertKeyString->pu1_OctetList,
                                          pFsVpnCertKeyString->i4_Length);
    if (pVpnCertInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsVpnCertKeyType = pVpnCertInfo->i4CertKeyType;
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnCertKeyType lv;
    lv.cmd = NMH_GET_FS_VPN_CERT_KEY_TYPE;
    lv.pFsVpnCertKeyString = pFsVpnCertKeyString;

    lv.pi4RetValFsVpnCertKeyType = pi4RetValFsVpnCertKeyType;
    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnCertKeyFileName
 Input       :  The Indices
                FsVpnCertKeyString

                The Object 
                retValFsVpnCertKeyFileName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnCertKeyFileName (tSNMP_OCTET_STRING_TYPE * pFsVpnCertKeyString,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValFsVpnCertKeyFileName)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnCertInfo       *pVpnCertInfo = NULL;

    pVpnCertInfo = VpnGetCertInfoFromKey (pFsVpnCertKeyString->pu1_OctetList,
                                          pFsVpnCertKeyString->i4_Length);
    if (pVpnCertInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRetValFsVpnCertKeyFileName->i4_Length =
        (INT4) STRLEN (pVpnCertInfo->ac1CertKeyFileName);
    MEMCPY (pRetValFsVpnCertKeyFileName->pu1_OctetList,
            pVpnCertInfo->ac1CertKeyFileName,
            pRetValFsVpnCertKeyFileName->i4_Length);

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnCertKeyFileName lv;

    lv.cmd = NMH_GET_FS_VPN_CERT_KEY_FILE_NAME;
    lv.pFsVpnCertKeyString = pFsVpnCertKeyString;
    lv.pRetValFsVpnCertKeyFileName = pRetValFsVpnCertKeyFileName;
    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnCertFileName
 Input       :  The Indices
                FsVpnCertKeyString

                The Object 
                retValFsVpnCertFileName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnCertFileName (tSNMP_OCTET_STRING_TYPE * pFsVpnCertKeyString,
                         tSNMP_OCTET_STRING_TYPE * pRetValFsVpnCertFileName)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnCertInfo       *pVpnCertInfo = NULL;

    pVpnCertInfo = VpnGetCertInfoFromKey (pFsVpnCertKeyString->pu1_OctetList,
                                          pFsVpnCertKeyString->i4_Length);
    if (pVpnCertInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRetValFsVpnCertFileName->i4_Length =
        (INT4) STRLEN (pVpnCertInfo->ac1CertFileName);
    MEMCPY (pRetValFsVpnCertFileName->pu1_OctetList,
            pVpnCertInfo->ac1CertFileName, pRetValFsVpnCertFileName->i4_Length);

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnCertFileName lv;

    lv.cmd = NMH_GET_FS_VPN_CERT_FILE_NAME;
    lv.pFsVpnCertKeyString = pFsVpnCertKeyString;
    lv.pRetValFsVpnCertFileName = pRetValFsVpnCertFileName;
    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);

#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnCertEncodeType
 Input       :  The Indices
                FsVpnCertKeyString

                The Object 
                retValFsVpnCertEncodeType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnCertEncodeType (tSNMP_OCTET_STRING_TYPE * pFsVpnCertKeyString,
                           INT4 *pi4RetValFsVpnCertEncodeType)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnCertInfo       *pVpnCertInfo = NULL;

    pVpnCertInfo = VpnGetCertInfoFromKey (pFsVpnCertKeyString->pu1_OctetList,
                                          pFsVpnCertKeyString->i4_Length);
    if (pVpnCertInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsVpnCertEncodeType = pVpnCertInfo->i4CertEncType;
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnCertEncodeType lv;
    lv.cmd = NMH_GET_FS_VPN_CERT_ENCODE_TYPE;
    lv.pFsVpnCertKeyString = pFsVpnCertKeyString;
    lv.pi4RetValFsVpnCertEncodeType = pi4RetValFsVpnCertEncodeType;
    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnCertStatus
 Input       :  The Indices
                FsVpnCertKeyString

                The Object 
                retValFsVpnCertStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnCertStatus (tSNMP_OCTET_STRING_TYPE * pFsVpnCertKeyString,
                       INT4 *pi4RetValFsVpnCertStatus)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnCertInfo       *pVpnCertInfo = NULL;

    pVpnCertInfo = VpnGetCertInfoFromKey (pFsVpnCertKeyString->pu1_OctetList,
                                          pFsVpnCertKeyString->i4_Length);
    if (pVpnCertInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsVpnCertStatus = pVpnCertInfo->i1IdStatus;
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnCertStatus lv;

    lv.cmd = NMH_GET_FS_VPN_CERT_STATUS;
    lv.pFsVpnCertKeyString = pFsVpnCertKeyString;
    lv.pi4RetValFsVpnCertStatus = pi4RetValFsVpnCertStatus;
    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVpnCertKeyType
 Input       :  The Indices
                FsVpnCertKeyString

                The Object 
                setValFsVpnCertKeyType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnCertKeyType (tSNMP_OCTET_STRING_TYPE * pFsVpnCertKeyString,
                        INT4 i4SetValFsVpnCertKeyType)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnCertInfo       *pVpnCertInfo = NULL;

    pVpnCertInfo = VpnGetCertInfoFromKey (pFsVpnCertKeyString->pu1_OctetList,
                                          pFsVpnCertKeyString->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnCertInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pVpnCertInfo->i4CertKeyType = i4SetValFsVpnCertKeyType;
#ifndef SECURITY_KERNEL_MAKE_WANTED
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnCertKeyType, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnCertKeyString,
                      i4SetValFsVpnCertKeyType));
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhSetFsVpnCertKeyType lv;

    lv.cmd = NMH_SET_FS_VPN_CERT_KEY_TYPE;
    lv.pFsVpnCertKeyString = pFsVpnCertKeyString;
    lv.i4SetValFsVpnCertKeyType = i4SetValFsVpnCertKeyType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnCertKeyFileName
 Input       :  The Indices
                FsVpnCertKeyString

                The Object 
                setValFsVpnCertKeyFileName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnCertKeyFileName (tSNMP_OCTET_STRING_TYPE * pFsVpnCertKeyString,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValFsVpnCertKeyFileName)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnCertInfo       *pVpnCertInfo = NULL;

    pVpnCertInfo = VpnGetCertInfoFromKey (pFsVpnCertKeyString->pu1_OctetList,
                                          pFsVpnCertKeyString->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (pVpnCertInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (pVpnCertInfo->ac1CertKeyFileName,
            pSetValFsVpnCertKeyFileName->pu1_OctetList,
            pSetValFsVpnCertKeyFileName->i4_Length);

#ifndef SECURITY_KERNEL_MAKE_WANTED
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnCertKeyFileName, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s", pFsVpnCertKeyString,
                      pSetValFsVpnCertKeyFileName));
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhSetFsVpnCertKeyFileName lv;
    lv.cmd = NMH_SET_FS_VPN_CERT_KEY_FILE_NAME;
    lv.pFsVpnCertKeyString = pFsVpnCertKeyString;
    lv.pSetValFsVpnCertKeyFileName = pSetValFsVpnCertKeyFileName;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnCertFileName
 Input       :  The Indices
                FsVpnCertKeyString

                The Object 
                setValFsVpnCertFileName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnCertFileName (tSNMP_OCTET_STRING_TYPE * pFsVpnCertKeyString,
                         tSNMP_OCTET_STRING_TYPE * pSetValFsVpnCertFileName)
{
    tVpnCertInfo       *pVpnCertInfo = NULL;
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    pVpnCertInfo = VpnGetCertInfoFromKey (pFsVpnCertKeyString->pu1_OctetList,
                                          pFsVpnCertKeyString->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnCertInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (pVpnCertInfo->ac1CertFileName,
            pSetValFsVpnCertFileName->pu1_OctetList,
            pSetValFsVpnCertFileName->i4_Length);

#ifndef SECURITY_KERNEL_MAKE_WANTED
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnCertFileName, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s", pFsVpnCertKeyString,
                      pSetValFsVpnCertFileName));
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhSetFsVpnCertFileName lv;
    lv.cmd = NMH_SET_FS_VPN_CERT_FILE_NAME;
    lv.pFsVpnCertKeyString = pFsVpnCertKeyString;
    lv.pSetValFsVpnCertFileName = pSetValFsVpnCertFileName;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnCertEncodeType
 Input       :  The Indices
                FsVpnCertKeyString

                The Object 
                setValFsVpnCertEncodeType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnCertEncodeType (tSNMP_OCTET_STRING_TYPE * pFsVpnCertKeyString,
                           INT4 i4SetValFsVpnCertEncodeType)
{
    tVpnCertInfo       *pVpnCertInfo = NULL;
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    pVpnCertInfo = VpnGetCertInfoFromKey (pFsVpnCertKeyString->pu1_OctetList,
                                          pFsVpnCertKeyString->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnCertInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pVpnCertInfo->i4CertEncType = i4SetValFsVpnCertEncodeType;
#ifndef SECURITY_KERNEL_MAKE_WANTED
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnCertEncodeType, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnCertKeyString,
                      i4SetValFsVpnCertEncodeType));
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhSetFsVpnCertEncodeType lv;

    lv.cmd = NMH_SET_FS_VPN_CERT_ENCODE_TYPE;
    lv.pFsVpnCertKeyString = pFsVpnCertKeyString;
    lv.i4SetValFsVpnCertEncodeType = i4SetValFsVpnCertEncodeType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnCertStatus
 Input       :  The Indices
                FsVpnCertKeyString

                The Object 
                setValFsVpnCertStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnCertStatus (tSNMP_OCTET_STRING_TYPE * pFsVpnCertKeyString,
                       INT4 i4SetValFsVpnCertStatus)
{
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tVpnCertInfo       *pVpnCertInfo = NULL;
    UINT4               u4TempLen = VPN_ZERO;
    INT1                i1RetVal = VPN_SUCCESS;

    RM_GET_SEQ_NUM (&u4SeqNum);

    switch (i4SetValFsVpnCertStatus)
    {
        case ACTIVE:
            pVpnCertInfo =
                VpnGetCertInfoFromKey (pFsVpnCertKeyString->pu1_OctetList,
                                       pFsVpnCertKeyString->i4_Length);
            if (pVpnCertInfo == NULL)
            {
                return (SNMP_FAILURE);
            }

            if (pVpnCertInfo->i1IdStatus == ACTIVE)
            {
                /* Do nothig;  Already active */
                break;
            }
#ifdef IKE_WANTED
            /* Updating both keys and certificate info in IKE database */
            i1RetVal = VpnUtilUpdateCertDB (pVpnCertInfo);
#endif
            if (i1RetVal == VPN_SUCCESS)
            {
                pVpnCertInfo->i1IdStatus = ACTIVE;
            }
            else
            {
                return SNMP_FAILURE;
            }
            break;

        case NOT_IN_SERVICE:
            pVpnCertInfo =
                VpnGetCertInfoFromKey (pFsVpnCertKeyString->pu1_OctetList,
                                       pFsVpnCertKeyString->i4_Length);
            if (pVpnCertInfo == NULL)
            {
                return (SNMP_FAILURE);
            }

            if (pVpnCertInfo->i1IdStatus == ACTIVE)
            {
#ifdef IKE_WANTED
                /* Deleting certificate file name info from IKE database */
                i1RetVal = VpnUtilDeleteCertDB (pVpnCertInfo);
#endif
            }

            /* resetting certificate file name and certificate encode type */
            MEMSET (pVpnCertInfo->ac1CertFileName, VPN_ZERO,
                    VPN_MAX_PRESHARED_KEY_LEN + VPN_ONE);
            pVpnCertInfo->i4CertEncType = VPN_ZERO;
            pVpnCertInfo->i1IdStatus = NOT_IN_SERVICE;
            break;

        case CREATE_AND_WAIT:

            if (MemAllocateMemBlock
                (VPN_CERT_PID,
                 (UINT1 **) (VOID *) &pVpnCertInfo) != MEM_SUCCESS)
            {
                return (SNMP_FAILURE);
            }

            MEMSET (pVpnCertInfo, VPN_ZERO, sizeof (tVpnCertInfo));

            u4TempLen =
                MEM_MAX_BYTES ((UINT4) pFsVpnCertKeyString->i4_Length,
                               STRLEN (pFsVpnCertKeyString->pu1_OctetList));
            MEMCPY (pVpnCertInfo->ac1CertKeyId,
                    pFsVpnCertKeyString->pu1_OctetList, u4TempLen);
            pVpnCertInfo->ac1CertKeyId[u4TempLen] = '\0';
            pVpnCertInfo->i4CertEncType = VPN_CERT_ENCODE_PEM;
            pVpnCertInfo->i1IdStatus = NOT_READY;

            TMO_SLL_Add (&gVpnCertInfoList, (tTMO_SLL_NODE *) pVpnCertInfo);
            break;

        case DESTROY:
            pVpnCertInfo =
                VpnGetCertInfoFromKey (pFsVpnCertKeyString->pu1_OctetList,
                                       pFsVpnCertKeyString->i4_Length);
            if (pVpnCertInfo == NULL)
            {
                return (SNMP_FAILURE);
            }
            if (pVpnCertInfo->i1IdStatus == ACTIVE)
            {
#ifdef IKE_WANTED
                /* Deleting key info from IKE DB */
                i1RetVal = VpnUtilDeleteCertKeys (pVpnCertInfo);
#endif
            }
            if (i1RetVal == VPN_SUCCESS)
            {
#ifdef IKE_WANTED
                /* Deleting Certificate file info from IKE DB */
                VpnUtilDeleteCertDB (pVpnCertInfo);
#endif
                TMO_SLL_Delete (&gVpnCertInfoList,
                                (tTMO_SLL_NODE *) pVpnCertInfo);
                MemReleaseMemBlock (VPN_CERT_PID, (UINT1 *) pVpnCertInfo);
            }
            else
            {
                return SNMP_FAILURE;
            }

            break;
        default:
            break;
    }
#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnCertStatus lv;

    lv.cmd = NMH_SET_FS_VPN_CERT_STATUS;
    lv.pFsVpnCertKeyString = pFsVpnCertKeyString;
    lv.i4SetValFsVpnCertStatus = i4SetValFsVpnCertStatus;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnCertStatus, u4SeqNum, TRUE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnCertKeyString,
                      i4SetValFsVpnCertStatus));
    return SNMP_SUCCESS;
#endif
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVpnCertKeyType
 Input       :  The Indices
                FsVpnCertKeyString

                The Object 
                testValFsVpnCertKeyType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnCertKeyType (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE * pFsVpnCertKeyString,
                           INT4 i4TestValFsVpnCertKeyType)
{
    tVpnCertInfo       *pVpnCertInfo = NULL;

    pVpnCertInfo =
        VpnGetCertInfoFromKey (pFsVpnCertKeyString->pu1_OctetList,
                               pFsVpnCertKeyString->i4_Length);
    if (pVpnCertInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    if ((i4TestValFsVpnCertKeyType == VPN_AUTH_RSA) ||
        (i4TestValFsVpnCertKeyType == VPN_AUTH_DSA))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
#ifndef SECURITY_KERNEL_MAKE_WANTED
    return SNMP_FAILURE;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnCertKeyType lv;
    lv.cmd = NMH_TESTV2_FS_VPN_CERT_KEY_TYPE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnCertKeyString = pFsVpnCertKeyString;
    lv.i4TestValFsVpnCertKeyType = i4TestValFsVpnCertKeyType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnCertKeyFileName
 Input       :  The Indices
                FsVpnCertKeyString

                The Object 
                testValFsVpnCertKeyFileName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnCertKeyFileName (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE * pFsVpnCertKeyString,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValFsVpnCertKeyFileName)
{
    tVpnCertInfo       *pVpnCertInfo = NULL;
    INT4                i4FileDesc = -1;

    pVpnCertInfo =
        VpnGetCertInfoFromKey (pFsVpnCertKeyString->pu1_OctetList,
                               pFsVpnCertKeyString->i4_Length);
    if (pVpnCertInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    if ((pTestValFsVpnCertKeyFileName->i4_Length <= VPN_ZERO) ||
        (pTestValFsVpnCertKeyFileName->i4_Length > VPN_CERT_KEY_FILE_NAME_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_NO_SUCH_NAME;
        return SNMP_FAILURE;
    }

    /* To confirm if file exists */
    if ((i4FileDesc = FileOpen (pTestValFsVpnCertKeyFileName->pu1_OctetList,
                                OSIX_FILE_RO)) <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    FileClose (i4FileDesc);
    /*Removing the Kernel portion from here As File existence check is not required in kernel space */

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnCertFileName
 Input       :  The Indices
                FsVpnCertKeyString

                The Object 
                testValFsVpnCertFileName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnCertFileName (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pFsVpnCertKeyString,
                            tSNMP_OCTET_STRING_TYPE * pTestValFsVpnCertFileName)
{
    tVpnCertInfo       *pVpnCertInfo = NULL;

    pVpnCertInfo =
        VpnGetCertInfoFromKey (pFsVpnCertKeyString->pu1_OctetList,
                               pFsVpnCertKeyString->i4_Length);
    if (pVpnCertInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    if ((pTestValFsVpnCertFileName->i4_Length <= VPN_ZERO) ||
        (pTestValFsVpnCertFileName->i4_Length > VPN_CERT_FILE_NAME_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
#ifndef SECURITY_KERNEL_MAKE_WANTED
    return SNMP_SUCCESS;
#else

    int                 rc;
    tNpwnmhTestv2FsVpnCertFileName lv;

    lv.cmd = NMH_TESTV2_FS_VPN_CERT_FILE_NAME;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnCertKeyString = pFsVpnCertKeyString;
    lv.pTestValFsVpnCertFileName = pTestValFsVpnCertFileName;
    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnCertEncodeType
 Input       :  The Indices
                FsVpnCertKeyString

                The Object 
                testValFsVpnCertEncodeType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnCertEncodeType (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pFsVpnCertKeyString,
                              INT4 i4TestValFsVpnCertEncodeType)
{
    tVpnCertInfo       *pVpnCertInfo = NULL;

    pVpnCertInfo =
        VpnGetCertInfoFromKey (pFsVpnCertKeyString->pu1_OctetList,
                               pFsVpnCertKeyString->i4_Length);
    if (pVpnCertInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    if ((i4TestValFsVpnCertEncodeType != VPN_CERT_ENCODE_PEM) &&
        (i4TestValFsVpnCertEncodeType != VPN_CERT_ENCODE_DER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#ifndef SECURITY_KERNEL_MAKE_WANTED
    return SNMP_SUCCESS;
#else

    int                 rc;
    tNpwnmhTestv2FsVpnCertEncodeType lv;

    lv.cmd = NMH_TESTV2_FS_VPN_CERT_ENCODE_TYPE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnCertKeyString = pFsVpnCertKeyString;
    lv.i4TestValFsVpnCertEncodeType = i4TestValFsVpnCertEncodeType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnCertStatus
 Input       :  The Indices
                FsVpnCertKeyString

                The Object 
                testValFsVpnCertStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnCertStatus (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pFsVpnCertKeyString,
                          INT4 i4TestValFsVpnCertStatus)
{
    tVpnCertInfo       *pVpnCertInfo = NULL;

    if (pFsVpnCertKeyString->i4_Length > VPN_MAX_KEY_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    pVpnCertInfo =
        VpnGetCertInfoFromKey (pFsVpnCertKeyString->pu1_OctetList,
                               pFsVpnCertKeyString->i4_Length);

    switch (i4TestValFsVpnCertStatus)
    {
        case ACTIVE:
            if (pVpnCertInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return (SNMP_FAILURE);
            }

            break;

        case NOT_IN_SERVICE:
            if (pVpnCertInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return (SNMP_FAILURE);
            }
            break;
        case CREATE_AND_GO:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);

        case CREATE_AND_WAIT:
            if (pVpnCertInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return (SNMP_FAILURE);
            }

            if (TMO_SLL_Count (&gVpnCertInfoList) == VPN_SYS_MAX_CERTS)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return (SNMP_FAILURE);
            }
            break;

        case DESTROY:
            if (pVpnCertInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return (SNMP_FAILURE);
            }

            if (pVpnCertInfo->i4RefCnt > VPN_ZERO)
            {
                /* Remote-id is in use */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
            }
            break;
        default:
            break;
    }

#ifndef SECURITY_KERNEL_MAKE_WANTED
    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhTestv2FsVpnCertStatus lv;

    lv.cmd = NMH_TESTV2_FS_VPN_CERT_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnCertKeyString = pFsVpnCertKeyString;
    lv.i4TestValFsVpnCertStatus = i4TestValFsVpnCertStatus;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVpnCertInfoTable
 Input       :  The Indices
                FsVpnCertKeyString
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVpnCertInfoTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsVpnCaCertInfoTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsVpnCaCertInfoTable
 Input       :  The Indices
                FsVpnCaCertKeyString
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsVpnCaCertInfoTable (tSNMP_OCTET_STRING_TYPE *
                                              pFsVpnCaCertKeyString)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnCaCertInfo     *pVpnCaCertInfo = NULL;

    pVpnCaCertInfo =
        VpnGetCaCertInfoFromKey (pFsVpnCaCertKeyString->pu1_OctetList,
                                 pFsVpnCaCertKeyString->i4_Length);
    if (pVpnCaCertInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
#else
    int                 rc;
    tNpwnmhValidateIndexInstanceFsVpnCaCertInfoTable lv;

    lv.cmd = NMH_VALIDATE_INDEX_INSTANCE_FS_VPN_CA_CERT_INFO_TABLE;
    lv.pFsVpnCaCertKeyString = pFsVpnCaCertKeyString;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsVpnCaCertInfoTable
 Input       :  The Indices
                FsVpnCaCertKeyString
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsVpnCaCertInfoTable (tSNMP_OCTET_STRING_TYPE *
                                      pFsVpnCaCertKeyString)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnCaCertInfo     *pVpnCaCertInfo = NULL;

    pVpnCaCertInfo = (tVpnCaCertInfo *) TMO_SLL_First (&gVpnCaCertInfoList);
    if (pVpnCaCertInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pFsVpnCaCertKeyString->i4_Length =
        (INT4) STRLEN (pVpnCaCertInfo->ac1CaCertKeyId);
    MEMCPY (pFsVpnCaCertKeyString->pu1_OctetList,
            pVpnCaCertInfo->ac1CaCertKeyId, pFsVpnCaCertKeyString->i4_Length);
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFirstIndexFsVpnCaCertInfoTable lv;

    lv.cmd = NMH_GET_FIRST_INDEX_FS_VPN_CA_CERT_INFO_TABLE;
    lv.pFsVpnCaCertKeyString = pFsVpnCaCertKeyString;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsVpnCaCertInfoTable
 Input       :  The Indices
                FsVpnCaCertKeyString
                nextFsVpnCaCertKeyString
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsVpnCaCertInfoTable (tSNMP_OCTET_STRING_TYPE *
                                     pFsVpnCaCertKeyString,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextFsVpnCaCertKeyString)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnCaCertInfo     *pVpnCaCertInfo = NULL;

    pVpnCaCertInfo =
        VpnGetCaCertInfoFromKey (pFsVpnCaCertKeyString->pu1_OctetList,
                                 pFsVpnCaCertKeyString->i4_Length);

    pVpnCaCertInfo =
        (tVpnCaCertInfo *) TMO_SLL_Next ((tTMO_SLL *) & gVpnCaCertInfoList,
                                         (tTMO_SLL_NODE *) pVpnCaCertInfo);
    if (pVpnCaCertInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pNextFsVpnCaCertKeyString->i4_Length =
        (INT4) STRLEN (pVpnCaCertInfo->ac1CaCertKeyId);
    MEMCPY (pNextFsVpnCaCertKeyString->pu1_OctetList,
            pVpnCaCertInfo->ac1CaCertKeyId,
            pNextFsVpnCaCertKeyString->i4_Length);
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetNextIndexFsVpnCaCertInfoTable lv;

    lv.cmd = NMH_GET_NEXT_INDEX_FS_VPN_CA_CERT_INFO_TABLE;
    lv.pFsVpnCaCertKeyString = pFsVpnCaCertKeyString;
    lv.pNextFsVpnCaCertKeyString = pNextFsVpnCaCertKeyString;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsVpnCaCertFileName
 Input       :  The Indices
                FsVpnCaCertKeyString

                The Object 
                retValFsVpnCaCertFileName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnCaCertFileName (tSNMP_OCTET_STRING_TYPE * pFsVpnCaCertKeyString,
                           tSNMP_OCTET_STRING_TYPE * pRetValFsVpnCaCertFileName)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnCaCertInfo     *pVpnCaCertInfo = NULL;

    pVpnCaCertInfo =
        VpnGetCaCertInfoFromKey (pFsVpnCaCertKeyString->pu1_OctetList,
                                 pFsVpnCaCertKeyString->i4_Length);
    if (pVpnCaCertInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRetValFsVpnCaCertFileName->i4_Length =
        (INT4) STRLEN (pVpnCaCertInfo->ac1CaCertFileName);
    MEMCPY (pRetValFsVpnCaCertFileName->pu1_OctetList,
            pVpnCaCertInfo->ac1CaCertFileName,
            pRetValFsVpnCaCertFileName->i4_Length);

    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnCaCertFileName lv;

    lv.cmd = NMH_GET_FS_VPN_CA_CERT_FILE_NAME;
    lv.pFsVpnCaCertKeyString = pFsVpnCaCertKeyString;
    lv.pRetValFsVpnCaCertFileName = pRetValFsVpnCaCertFileName;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnCaCertEncodeType
 Input       :  The Indices
                FsVpnCaCertKeyString

                The Object 
                retValFsVpnCaCertEncodeType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnCaCertEncodeType (tSNMP_OCTET_STRING_TYPE * pFsVpnCaCertKeyString,
                             INT4 *pi4RetValFsVpnCaCertEncodeType)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnCaCertInfo     *pVpnCaCertInfo = NULL;

    pVpnCaCertInfo =
        VpnGetCaCertInfoFromKey (pFsVpnCaCertKeyString->pu1_OctetList,
                                 pFsVpnCaCertKeyString->i4_Length);
    if (pVpnCaCertInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsVpnCaCertEncodeType = pVpnCaCertInfo->i4CaCertEncType;
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnCaCertEncodeType lv;

    lv.cmd = NMH_GET_FS_VPN_CA_CERT_ENCODE_TYPE;
    lv.pFsVpnCaCertKeyString = pFsVpnCaCertKeyString;
    lv.pi4RetValFsVpnCaCertEncodeType = pi4RetValFsVpnCaCertEncodeType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhGetFsVpnCaCertStatus
 Input       :  The Indices
                FsVpnCaCertKeyString

                The Object 
                retValFsVpnCaCertStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsVpnCaCertStatus (tSNMP_OCTET_STRING_TYPE * pFsVpnCaCertKeyString,
                         INT4 *pi4RetValFsVpnCaCertStatus)
{
#ifndef SECURITY_KERNEL_MAKE_WANTED
    tVpnCaCertInfo     *pVpnCaCertInfo = NULL;

    pVpnCaCertInfo =
        VpnGetCaCertInfoFromKey (pFsVpnCaCertKeyString->pu1_OctetList,
                                 pFsVpnCaCertKeyString->i4_Length);
    if (pVpnCaCertInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsVpnCaCertStatus = pVpnCaCertInfo->i1IdStatus;
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhGetFsVpnCaCertStatus lv;

    lv.cmd = NMH_GET_FS_VPN_CA_CERT_STATUS;
    lv.pFsVpnCaCertKeyString = pFsVpnCaCertKeyString;
    lv.pi4RetValFsVpnCaCertStatus = pi4RetValFsVpnCaCertStatus;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsVpnCaCertFileName
 Input       :  The Indices
                FsVpnCaCertKeyString

                The Object 
                setValFsVpnCaCertFileName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnCaCertFileName (tSNMP_OCTET_STRING_TYPE * pFsVpnCaCertKeyString,
                           tSNMP_OCTET_STRING_TYPE * pSetValFsVpnCaCertFileName)
{
    tVpnCaCertInfo     *pVpnCaCertInfo = NULL;
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    pVpnCaCertInfo =
        VpnGetCaCertInfoFromKey (pFsVpnCaCertKeyString->pu1_OctetList,
                                 pFsVpnCaCertKeyString->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnCaCertInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (pVpnCaCertInfo->ac1CaCertFileName,
            pSetValFsVpnCaCertFileName->pu1_OctetList,
            pSetValFsVpnCaCertFileName->i4_Length);

#ifndef SECURITY_KERNEL_MAKE_WANTED
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnCaCertFileName, u4SeqNum, FALSE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %s", pFsVpnCaCertKeyString,
                      pSetValFsVpnCaCertFileName));
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhSetFsVpnCaCertFileName lv;

    lv.cmd = NMH_SET_FS_VPN_CA_CERT_FILE_NAME;
    lv.pFsVpnCaCertKeyString = pFsVpnCaCertKeyString;
    lv.pSetValFsVpnCaCertFileName = pSetValFsVpnCaCertFileName;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnCaCertEncodeType
 Input       :  The Indices
                FsVpnCaCertKeyString

                The Object 
                setValFsVpnCaCertEncodeType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnCaCertEncodeType (tSNMP_OCTET_STRING_TYPE * pFsVpnCaCertKeyString,
                             INT4 i4SetValFsVpnCaCertEncodeType)
{
    tVpnCaCertInfo     *pVpnCaCertInfo = NULL;
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    pVpnCaCertInfo =
        VpnGetCaCertInfoFromKey (pFsVpnCaCertKeyString->pu1_OctetList,
                                 pFsVpnCaCertKeyString->i4_Length);
    RM_GET_SEQ_NUM (&u4SeqNum);
    if (pVpnCaCertInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pVpnCaCertInfo->i4CaCertEncType = i4SetValFsVpnCaCertEncodeType;
#ifndef SECURITY_KERNEL_MAKE_WANTED
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnCaCertEncodeType, u4SeqNum,
                          FALSE, VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnCaCertKeyString,
                      i4SetValFsVpnCaCertEncodeType));
    return SNMP_SUCCESS;
#else

    int                 rc;
    tNpwnmhSetFsVpnCaCertEncodeType lv;

    lv.cmd = NMH_SET_FS_VPN_CA_CERT_ENCODE_TYPE;
    lv.pFsVpnCaCertKeyString = pFsVpnCaCertKeyString;
    lv.i4SetValFsVpnCaCertEncodeType = i4SetValFsVpnCaCertEncodeType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhSetFsVpnCaCertStatus
 Input       :  The Indices
                FsVpnCaCertKeyString

                The Object 
                setValFsVpnCaCertStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsVpnCaCertStatus (tSNMP_OCTET_STRING_TYPE * pFsVpnCaCertKeyString,
                         INT4 i4SetValFsVpnCaCertStatus)
{
    tVpnCaCertInfo     *pVpnCaCertInfo = NULL;
    UINT4               u4TempLen = VPN_ZERO;
    INT1                i1RetVal = VPN_SUCCESS;
    UINT4               u4SeqNum = VPN_ZERO;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    RM_GET_SEQ_NUM (&u4SeqNum);

    switch (i4SetValFsVpnCaCertStatus)
    {
        case ACTIVE:
            pVpnCaCertInfo =
                VpnGetCaCertInfoFromKey (pFsVpnCaCertKeyString->pu1_OctetList,
                                         pFsVpnCaCertKeyString->i4_Length);
            if (pVpnCaCertInfo == NULL)
            {
                return (SNMP_FAILURE);
            }

            if (pVpnCaCertInfo->i1IdStatus == ACTIVE)
            {
                /* Do nothig;  Already active */
                break;
            }
#ifdef IKE_WANTED
            i1RetVal = VpnUtilUpdateCaCertDB (pVpnCaCertInfo);
#endif
            if (i1RetVal == VPN_SUCCESS)
            {
                pVpnCaCertInfo->i1IdStatus = ACTIVE;
            }
            else
            {
                return SNMP_FAILURE;
            }
            break;

        case NOT_IN_SERVICE:
            pVpnCaCertInfo =
                VpnGetCaCertInfoFromKey (pFsVpnCaCertKeyString->pu1_OctetList,
                                         pFsVpnCaCertKeyString->i4_Length);
            if (pVpnCaCertInfo == NULL)
            {
                return (SNMP_FAILURE);
            }

            if (pVpnCaCertInfo->i1IdStatus == ACTIVE)
            {
#ifdef IKE_WANTED
                i1RetVal = VpnUtilDeleteCaCertDB (pVpnCaCertInfo);
#endif
            }

            pVpnCaCertInfo->i1IdStatus = NOT_IN_SERVICE;
            break;

        case CREATE_AND_WAIT:
            if (MemAllocateMemBlock
                (VPN_CA_CERT_PID,
                 (UINT1 **) (VOID *) &pVpnCaCertInfo) != MEM_SUCCESS)
            {
                return (SNMP_FAILURE);
            }

            MEMSET (pVpnCaCertInfo, VPN_ZERO, sizeof (tVpnCaCertInfo));

            u4TempLen =
                MEM_MAX_BYTES ((UINT4) pFsVpnCaCertKeyString->i4_Length,
                               STRLEN (pFsVpnCaCertKeyString->pu1_OctetList));
            MEMCPY (pVpnCaCertInfo->ac1CaCertKeyId,
                    pFsVpnCaCertKeyString->pu1_OctetList, u4TempLen);
            pVpnCaCertInfo->ac1CaCertKeyId[u4TempLen] = '\0';
            pVpnCaCertInfo->i4CaCertEncType = VPN_CERT_ENCODE_PEM;
            pVpnCaCertInfo->i1IdStatus = NOT_READY;

            TMO_SLL_Add (&gVpnCaCertInfoList, (tTMO_SLL_NODE *) pVpnCaCertInfo);
            break;

        case DESTROY:
            pVpnCaCertInfo =
                VpnGetCaCertInfoFromKey (pFsVpnCaCertKeyString->pu1_OctetList,
                                         pFsVpnCaCertKeyString->i4_Length);
            if (pVpnCaCertInfo == NULL)
            {
                return (SNMP_FAILURE);
            }
            if (pVpnCaCertInfo->i1IdStatus == ACTIVE)
            {
#ifdef IKE_WANTED
                i1RetVal = VpnUtilDeleteCaCertDB (pVpnCaCertInfo);
#endif
            }
            if (i1RetVal == VPN_SUCCESS)
            {
                TMO_SLL_Delete (&gVpnCaCertInfoList,
                                (tTMO_SLL_NODE *) pVpnCaCertInfo);
                MemReleaseMemBlock (VPN_CA_CERT_PID, (UINT1 *) pVpnCaCertInfo);
            }
            else
            {
                return SNMP_FAILURE;
            }

            break;
        default:
            break;
    }
#ifdef SECURITY_KERNEL_MAKE_WANTED
    int                 rc;
    tNpwnmhSetFsVpnCaCertStatus lv;

    lv.cmd = NMH_SET_FS_VPN_CA_CERT_STATUS;
    lv.pFsVpnCaCertKeyString = pFsVpnCaCertKeyString;
    lv.i4SetValFsVpnCaCertStatus = i4SetValFsVpnCaCertStatus;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#else
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsVpnCaCertStatus, u4SeqNum, TRUE,
                          VpnDsLock, VpnDsUnLock, VPN_ONE, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsVpnCaCertKeyString,
                      i4SetValFsVpnCaCertStatus));
    return SNMP_SUCCESS;
#endif
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsVpnCaCertFileName
 Input       :  The Indices
                FsVpnCaCertKeyString

                The Object 
                testValFsVpnCaCertFileName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnCaCertFileName (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pFsVpnCaCertKeyString,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValFsVpnCaCertFileName)
{
    tVpnCaCertInfo     *pVpnCaCertInfo = NULL;

    pVpnCaCertInfo =
        VpnGetCaCertInfoFromKey (pFsVpnCaCertKeyString->pu1_OctetList,
                                 pFsVpnCaCertKeyString->i4_Length);
    if (pVpnCaCertInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    if ((pTestValFsVpnCaCertFileName->i4_Length <= VPN_ZERO) ||
        (pTestValFsVpnCaCertFileName->i4_Length > VPN_CERT_FILE_NAME_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
#ifndef SECURITY_KERNEL_MAKE_WANTED
    return SNMP_SUCCESS;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnCaCertFileName lv;

    lv.cmd = NMH_TESTV2_FS_VPN_CA_CERT_FILE_NAME;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnCaCertKeyString = pFsVpnCaCertKeyString;
    lv.pTestValFsVpnCaCertFileName = pTestValFsVpnCaCertFileName;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnCaCertEncodeType
 Input       :  The Indices
                FsVpnCaCertKeyString

                The Object 
                testValFsVpnCaCertEncodeType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnCaCertEncodeType (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pFsVpnCaCertKeyString,
                                INT4 i4TestValFsVpnCaCertEncodeType)
{
    tVpnCaCertInfo     *pVpnCaCertInfo = NULL;

    pVpnCaCertInfo =
        VpnGetCaCertInfoFromKey (pFsVpnCaCertKeyString->pu1_OctetList,
                                 pFsVpnCaCertKeyString->i4_Length);
    if (pVpnCaCertInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    if ((i4TestValFsVpnCaCertEncodeType == VPN_CERT_ENCODE_PEM) ||
        (i4TestValFsVpnCaCertEncodeType == VPN_CERT_ENCODE_DER))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
#ifndef SECURITY_KERNEL_MAKE_WANTED
    return SNMP_FAILURE;
#else
    int                 rc;
    tNpwnmhTestv2FsVpnCaCertEncodeType lv;

    lv.cmd = NMH_TESTV2_FS_VPN_CA_CERT_ENCODE_TYPE;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnCaCertKeyString = pFsVpnCaCertKeyString;
    lv.i4TestValFsVpnCaCertEncodeType = i4TestValFsVpnCaCertEncodeType;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/****************************************************************************
 Function    :  nmhTestv2FsVpnCaCertStatus
 Input       :  The Indices
                FsVpnCaCertKeyString

                The Object 
                testValFsVpnCaCertStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsVpnCaCertStatus (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pFsVpnCaCertKeyString,
                            INT4 i4TestValFsVpnCaCertStatus)
{
    tVpnCaCertInfo     *pVpnCaCertInfo = NULL;

    if (pFsVpnCaCertKeyString->i4_Length > VPN_MAX_KEY_NAME_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }

    pVpnCaCertInfo =
        VpnGetCaCertInfoFromKey (pFsVpnCaCertKeyString->pu1_OctetList,
                                 pFsVpnCaCertKeyString->i4_Length);

    switch (i4TestValFsVpnCaCertStatus)
    {
        case ACTIVE:
            if (pVpnCaCertInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return (SNMP_FAILURE);
            }

            break;

        case NOT_IN_SERVICE:
            if (pVpnCaCertInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return (SNMP_FAILURE);
            }
            break;
        case CREATE_AND_GO:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return (SNMP_FAILURE);

        case CREATE_AND_WAIT:
            if (pVpnCaCertInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return (SNMP_FAILURE);
            }

            if (TMO_SLL_Count (&gVpnCaCertInfoList) == VPN_SYS_MAX_CERTS)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return (SNMP_FAILURE);
            }
            break;

        case DESTROY:
            if (pVpnCaCertInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return (SNMP_FAILURE);
            }

            if (pVpnCaCertInfo->i4CaRefCnt > VPN_ZERO)
            {
                /* Remote-id is in use */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return (SNMP_FAILURE);
            }
            break;
        default:
            break;
    }
#ifndef SECURITY_KERNEL_MAKE_WANTED

    return (SNMP_SUCCESS);
#else
    int                 rc;
    tNpwnmhTestv2FsVpnCaCertStatus lv;

    lv.cmd = NMH_TESTV2_FS_VPN_CA_CERT_STATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.pFsVpnCaCertKeyString = pFsVpnCaCertKeyString;
    lv.i4TestValFsVpnCaCertStatus = i4TestValFsVpnCaCertStatus;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);
    return (rc < VPN_ZERO ? (INT1) SNMP_FAILURE : lv.rval);
#endif
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsVpnCaCertInfoTable
 Input       :  The Indices
                FsVpnCaCertKeyString
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsVpnCaCertInfoTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

#ifdef SECURITY_KERNEL_MAKE_WANTED

/***********************************************************************/
/*  Function Name : Secv4ProcessIKEMsg                                 */
/*  Description   : This function invokes the appropraite function to  */
/*                : process the IKE Message based on the Command type  */
/*                : of the IKE Message                                 */
/*                :                                                    */
/*  Input(s)      : pMsg - Points to the Message rcvd from IKE         */
/*                :                                                    */
/*  Output(s)     : None                                               */
/*  Return        : None                                               */
/***********************************************************************/

VOID
Secv4ProcessIKEMsg (tIkeIPSecQMsg * pMsg)
{
    tIkeIoctlRW         IoRW;

    IoRW.pBuf = (UINT1 *) pMsg;
    IoRW.wLen = sizeof (tIkeIPSecQMsg);

    if (ioctl (gi4SecDevFd, IKE_IPSEC_IOCTL, &IoRW) != IOCTL_SUCCESS)
    {
        perror ("ioctl write ike msg failed");
        return;
    }

    return;
}

/*************************************************************************/
/*  Function Name : Secv4IPCalcChkSum                                    */
/*  Description   : This function calculates teh IP checksum             */
/*                :                                                      */
/*  Input(s)      : pBuf   -  Pointer to Pkt Rcvd                        */
/*                : u4Size -  Size of the Pkt Rcvd                       */
/*  Output(s)     : None                                                 */
/*  Return Values : Calculated Check Sum                                 */
/*************************************************************************/
UINT2
Secv4IPCalcChkSum (UINT1 *pBuf, UINT4 u4Size)
{

    UINT4               u4Sum = VPN_ZERO;
    UINT2               u2Tmp = VPN_ZERO;

    while (u4Size > VPN_ONE)
    {
        u2Tmp = *((UINT2 *) pBuf);
        u4Sum += u2Tmp;
        pBuf += sizeof (UINT2);
        u4Size -= sizeof (UINT2);
    }
    if (u4Size == VPN_ONE)
    {
        u2Tmp = VPN_ZERO;
        *((UINT1 *) &u2Tmp) = *pBuf;
        u4Sum += u2Tmp;
    }

    u4Sum = (u4Sum >> VPN_MAX_ROW_LEN) + (u4Sum & VPN_BIT_MASK_ffff);
    u4Sum += (u4Sum >> VPN_MAX_ROW_LEN);
    u2Tmp = (UINT2) ~((UINT2) u4Sum);

    return (IPSEC_NTOHS (u2Tmp));
}
#endif
/******************************************************************************/
/*  Function Name : Secv4SendIcmpErrMsg                                       */
/*  Description   : This fuction Sends out the ICMP error message             */
/*  Input(s)      : pBuf  -  Actual Pkt                                       */
/*                : pIcmp - Pointer to the ICMP Sturcture                     */
/*                : i1flag - Flag to indicate the presence of IP options      */
/*  Output(s)     : None                                                      */
/*  Return Values : None                                                      */
/******************************************************************************/
VOID
Secv4SendIcmpErrMsg (tCRU_BUF_CHAIN_HEADER * pBuf, t_ICMP * pIcmp)
{
#if !defined (LNXIP4_WANTED) && !defined (LNXIP6_WANTED)
    if (SEC_KERN != gi4SysOperMode)
    {
        icmp_error_msg (pBuf, pIcmp);
    }
#endif

#ifdef SECURITY_KERNEL_WANTED
    else if (SEC_KERN == gi4SysOperMode)
    {
        Secv4SendIcmpErrMsgToUser (pBuf, pIcmp);
    }
#endif
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pIcmp);
    return;
}

#ifdef SECURITY_KERNEL_MAKE_WANTED
/***********************************************************************/
/*  Function Name : Secv4DeleteSecAssocEntries                         */
/*  Description   : This function deletes security assoc Database      */
/*                :                                                    */
/*  Input(s)      : None                                               */
/*                :                                                    */
/*  Output(s)     : Deletes the security association data base         */
/*  Return        : None                                               */
/***********************************************************************/
VOID
Secv4DeleteSecAssocEntries (VOID)
{
    if (ioctl (gi4SecDevFd, FIPS_IPSEC_IOCTL, NULL) != IOCTL_SUCCESS)
    {
        perror ("ioctl secv4 delete sec assoc entries failed");
    }
    return;
}

/***********************************************************************/
/*  Function Name : Secv4SetBypassCapability                           */
/*  Description   : This function deletes security assoc Database      */
/*                :                                                    */
/*  Input(s)      : None                                               */
/*                :                                                    */
/*  Output(s)     : Deletes the security association data base         */
/*  Return        : None                                               */
/***********************************************************************/
INT1
Secv4SetBypassCapability (INT4 pi4SetValSecv4BypassCapability)
{
    int                 rc = SNMP_SUCCESS;
    tNpwnmhSetSecv4BypassCapability lv;

    lv.cmd = NMH_SET_FS_SECV4_BYPASS_CAPABILITY;
    lv.pi4SetValSecv4BypassCapability = pi4SetValSecv4BypassCapability;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    if (rc < VPN_ZERO)
    {
        return ((INT1) SNMP_FAILURE);
    }
    else
    {
        return (lv.rval);
    }
}

/***********************************************************************/
/*  Function Name : Secv4GetBypassCapability                           */
/*  Description   : This function deletes security assoc Database      */
/*                :                                                    */
/*  Input(s)      : None                                               */
/*                :                                                    */
/*  Output(s)     : Deletes the security association data base         */
/*  Return        : None                                               */
/***********************************************************************/
INT1
Secv4GetBypassCapability (INT4 *pi4RetValSecv4BypassCapability)
{
    int                 rc = SNMP_SUCCESS;
    tNpwnmhGetSecv4BypassCapability lv;

    lv.cmd = NMH_GET_FS_SECV4_BYPASS_CAPABILITY;
    lv.pi4RetValSecv4BypassCapability = pi4RetValSecv4BypassCapability;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    if (rc < VPN_ZERO)
    {
        return ((INT1) SNMP_FAILURE);
    }
    else
    {
        return (lv.rval);
    }
}

/***********************************************************************/
/*  Function Name : Secv6SetBypassCapability                           */
/*  Description   : This function deletes security assoc Database      */
/*                :                                                    */
/*  Input(s)      : None                                               */
/*                :                                                    */
/*  Output(s)     : Deletes the security association data base         */
/*  Return        : None                                               */
/***********************************************************************/
INT1
Secv6SetBypassCapability (INT4 pi4SetValSecv6BypassCapability)
{
    int                 rc = SNMP_SUCCESS;
    tNpwnmhSetSecv6BypassCapability lv;

    lv.cmd = NMH_SET_FS_SECV6_BYPASS_CAPABILITY;
    lv.pi4SetValSecv6BypassCapability = pi4SetValSecv6BypassCapability;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    if (rc < VPN_ZERO)
    {
        return ((INT1) SNMP_FAILURE);
    }
    else
    {
        return (lv.rval);
    }
}

/***********************************************************************/
/*  Function Name : Secv6GetBypassCapability                           */
/*  Description   : This function deletes security assoc Database      */
/*                :                                                    */
/*  Input(s)      : None                                               */
/*                :                                                    */
/*  Output(s)     : Deletes the security association data base         */
/*  Return        : None                                               */
/***********************************************************************/
INT1
Secv6GetBypassCapability (INT4 *pi4RetValSecv6BypassCapability)
{
    int                 rc = SNMP_SUCCESS;
    tNpwnmhGetSecv6BypassCapability lv;

    lv.cmd = NMH_GET_FS_SECV6_BYPASS_CAPABILITY;
    lv.pi4RetValSecv6BypassCapability = pi4RetValSecv6BypassCapability;

    rc = ioctl (gi4SecDevFd, VPN_NMH_IOCTL, &lv);
    CLI_SET_ERR (lv.cliWebSetError);

    if (rc < VPN_ZERO)
    {
        return ((INT1) SNMP_FAILURE);
    }
    else
    {
        return (lv.rval);
    }
}
#endif
/****************************************************************************
 Function    :  nmhDepv2FsIkeTraceOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIkeTraceOption (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsIpsecTraceOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsIpsecTraceOption (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
