/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vpnsfnt.c,v 1.5 2013/10/24 09:55:53 siva Exp $
 *
 * Description: File containing apis for safenet porting
 * *******************************************************************/
# include "lr.h"
# include "cfa.h"
# include "fssnmp.h"
# include "fsvpnlw.h"
# include "vpninc.h"
#include "cli.h"
#include "seccli.h"
#include "vpncli.h"
#include "secv4.h"
#include "fsvpnwr.h"

#ifdef VPN_FP_WANTED
#include "secv4np.h"
#include "flowmgr.h"
#include "subsys.h"
#endif

CHR1                gVpnLogBuff[(VPN_LOGBUFSIZ * VPN_MAXPAGECNT) + 1];
#ifdef VPN_FP_WANTED
INT4                Secv4FormNpLdCfgMsgfromVpn (tVpnPolicy * pVpnPolicy,
                                                tRule * iRule);
VOID                VpnFillRaAddressPoolDetails (tVpnRaAddressPool *
                                                 pVpnRaAddressPoolNode,
                                                 tRaAddressPool *
                                                 pRaAddressPool);
#endif

#ifdef VPN_FP_WANTED
/******************************************************************/
INT4
Secv4FormNpLdCfgMsgfromVpn (tVpnPolicy * pVpnPolicy, tRule * iRule)
{
    CHR1               *pu1DStr = NULL;
    CHR1               *pu1SStr = NULL;
    tRule               rule;
    UINT4               tKeyLen = 0, tFlag = 0, teFlag = 0, u4CidrVal = 0;
    UINT1               au1AHOutKey[300];
    UINT1               au1ESPOutKey[300];
    UINT4               i = 0;
    UINT4               u4HashAlgo = 0;
    UINT1               au1ESPKey[60];
    UINT1              *pu1IdVal = NULL;
    UINT1               au1TempIp[16];
    UINT4               u4TempIpAddr = 0;

    MEMSET (au1AHOutKey, 0, 300);
    MEMSET (au1ESPOutKey, 0, 300);
    MEMSET (au1ESPKey, 0, 60);
    MEMSET (&rule, 0x0, sizeof (tRule));

    rule.r_cid = OSIX_HTONS (pVpnPolicy->u4VpnPolicyIndex);

    if (pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL)
    {
        rule.r_kmode = OSIX_HTONS (IPS_MODE_MANUAL);
    }
    else if (pVpnPolicy->u4VpnPolicyType == VPN_IKE_PRESHAREDKEY)
    {
        rule.r_kmode = OSIX_HTONS (IPS_MODE_IKE);
        rule.r_mode.ike.i_amode = OSIX_HTONL (IPS_IKE_PRESHARED);
    }
    else if (pVpnPolicy->u4VpnPolicyType == VPN_XAUTH)
    {
        rule.r_kmode = OSIX_HTONS (IPS_MODE_IKE);
        rule.r_mode.ike.i_amode = OSIX_HTONL (IPS_IKE_XAUTH);
    }
    else if (pVpnPolicy->u4VpnPolicyType == VPN_IKE_RA_PRESHAREDKEY)
    {
        rule.r_kmode = OSIX_HTONS (IPS_MODE_IKE);
        rule.r_mode.ike.i_amode = OSIX_HTONL (IPS_IKE_RA_PRESHARED);
    }

    rule.r_priority = OSIX_HTONS (100);

    MEMSET (&rule.r_sip, 0x0, 32);
    MEMSET (&rule.r_dip, 0x0, 32);

    /* Local Acces list address */
    u4CidrVal =
        VpnUtilConvMask2Prefix (pVpnPolicy->LocalProtectNetwork.Ip4Subnet);
    CLI_CONVERT_IPADDR_TO_STR (pu1SStr,
                               pVpnPolicy->LocalProtectNetwork.Ip4Addr);
    switch (pVpnPolicy->u4VpnProtocol)
    {
        case VPN_TCP:
            pu1IdVal = "tcp";
            break;
        case VPN_UDP:
            pu1IdVal = "udp";
            break;
        case VPN_ICMP:
            pu1IdVal = "icmp";
            break;
        default:
            pu1IdVal = "";        /* any */
            break;
    }
    SPRINTF (rule.r_sip, "ipv4(%s,%s/%lu)", pu1IdVal, pu1SStr, u4CidrVal);

    /* Remote Access list address */
    u4CidrVal =
        VpnUtilConvMask2Prefix (pVpnPolicy->RemoteProtectNetwork.Ip4Subnet);
    CLI_CONVERT_IPADDR_TO_STR (pu1DStr,
                               pVpnPolicy->RemoteProtectNetwork.Ip4Addr);
    SPRINTF (rule.r_dip, "ipv4(%s,%s/%lu)", pu1IdVal, pu1DStr, u4CidrVal);

    SPRINTF ((CHR1 *) & rule.r_tunnel.t_name[0], "Tunnel #%ld",
             pVpnPolicy->u4VpnPolicyIndex);

    if (pVpnPolicy->u1VpnMode == VPN_TRANSPORT)
    {
        rule.r_tunnel.t_trans = OSIX_HTONS (IPS_TRANSPORT);
    }
    else
    {
        rule.r_tunnel.t_trans = OSIX_HTONS (IPS_TUNNEL);
    }

    if (pVpnPolicy->u4VpnSecurityProtocol == SEC_AH)
    {
        tFlag |= IPS_PM_IPSEC_AH;
    }
    else
    {
        tFlag |= IPS_PM_IPSEC_ESP;
    }

    /* Fill the tunnel termination points */
    MEMSET (&rule.r_tunnel.t_lip, 0x0, 16);
    MEMSET (&rule.r_tunnel.t_pip, 0x0, 16);
    pu1DStr = NULL;

    CLI_CONVERT_IPADDR_TO_STR (pu1DStr,
                               (UINT4) pVpnPolicy->RemoteTunnTermAddr.Ip4Addr);
    SNPRINTF (rule.r_tunnel.t_pip, STRLEN (pu1DStr) + 1, "%s", pu1DStr);
    pu1DStr = NULL;
    CLI_CONVERT_IPADDR_TO_STR (pu1SStr,
                               (UINT4) pVpnPolicy->LocalTunnTermAddr.Ip4Addr);
    SNPRINTF (rule.r_tunnel.t_lip, STRLEN (pu1SStr) + 1, "%s", pu1SStr);

    if (VPN_POLICY_ANTI_REPLAY_STATUS (pVpnPolicy) == VPN_ANTI_REPLAY_ENABLE)
    {
        rule.r_tunnel.t_replay = OSIX_HTONS (IPS_ANTIREPLAY_ENABLE);
    }
    else
    {
        rule.r_tunnel.t_replay = OSIX_HTONS (IPS_ANTIREPLAY_DISABLE);
    }

    if (pVpnPolicy->u4VpnPolicyType == CLI_VPN_IPSEC_MANUAL)
    {
        UINT1               au1kstr[128];

        MEMSET (&au1kstr, 0x0, 128);

        if (pVpnPolicy->uVpnKeyMode.IpsecManualKey.u1VpnEncryptionAlgo ==
            VPN_3DES_CBC)
        {
            for (i = 0; i < 16; i++)
            {
                au1ESPKey[i] =
                    pVpnPolicy->uVpnKeyMode.IpsecManualKey.au2VpnEspKey[i];
            }
            for (i = 17; i < 33; i++)
            {
                au1ESPKey[i - 1] =
                    pVpnPolicy->uVpnKeyMode.IpsecManualKey.au2VpnEspKey[i];
            }
            for (i = 34; i < 50; i++)
            {
                au1ESPKey[i - 2] =
                    pVpnPolicy->uVpnKeyMode.IpsecManualKey.au2VpnEspKey[i];
            }
        }
        else                    /* DES/AES-128/192/256 */
        {
            STRCPY (au1ESPKey,
                    pVpnPolicy->uVpnKeyMode.IpsecManualKey.au2VpnEspKey);
        }

        if ((pVpnPolicy->uVpnKeyMode.IpsecManualKey.u1VpnEncryptionAlgo != 0)
            && (pVpnPolicy->uVpnKeyMode.IpsecManualKey.u1VpnEncryptionAlgo !=
                VPN_NULLESPALGO))
        {
            if (Secv4ConstructKey (au1ESPKey, STRLEN (au1ESPKey),
                                   au1ESPOutKey) == SNMP_FAILURE)
            {
                return (SNMP_FAILURE);
            }
        }

        rule.r_tunnel.t_aflg = 0x0;
        rule.r_tunnel.t_eflg = 0x0;
        switch (pVpnPolicy->uVpnKeyMode.IpsecManualKey.u1VpnEncryptionAlgo)
        {
            case VPN_DES_CBC:
                teFlag |= (IPS_PM_IPSEC_ESP | IPS_PM_CRYPT_DES);
                break;

            case VPN_3DES_CBC:
                teFlag |= (IPS_PM_IPSEC_ESP | IPS_PM_CRYPT_3DES);
                break;

            case VPN_AES_128:
                teFlag |= (IPS_PM_IPSEC_ESP | IPS_PM_CRYPT_AES);
                rule.r_tunnel.t_aesalgtype |= OSIX_HTONL (IPS_PM_CRYPT_AES128);
                break;

            case VPN_AES_192:
                teFlag |= (IPS_PM_IPSEC_ESP | IPS_PM_CRYPT_AES);
                rule.r_tunnel.t_aesalgtype |= OSIX_HTONL (IPS_PM_CRYPT_AES192);
                break;

            case VPN_AES_256:
                teFlag |= (IPS_PM_IPSEC_ESP | IPS_PM_CRYPT_AES);
                rule.r_tunnel.t_aesalgtype |= OSIX_HTONL (IPS_PM_CRYPT_AES256);
                break;
            case VPN_NULLESPALGO:
                teFlag |= (IPS_PM_IPSEC_ESP | IPS_PM_CRYPT_NULL);
                break;
        }

        /* Fill the SPI parameters, No different SPI's for esp and ah */
        rule.r_mode.man.m_ain = rule.r_mode.man.m_ein =
            OSIX_HTONL (pVpnPolicy->uVpnKeyMode.IpsecManualKey.
                        u4VpnAhInboundSpi);
        rule.r_mode.man.m_aout = rule.r_mode.man.m_eout =
            OSIX_HTONL (pVpnPolicy->uVpnKeyMode.IpsecManualKey.
                        u4VpnAhOutboundSpi);

        /* Fill the authentication parameters */
        if (pVpnPolicy->uVpnKeyMode.IpsecManualKey.u1VpnAuthAlgo)
        {
            if (Secv4ConstructKey
                (pVpnPolicy->uVpnKeyMode.IpsecManualKey.au1VpnAhKey,
                 STRLEN (pVpnPolicy->uVpnKeyMode.IpsecManualKey.au1VpnAhKey),
                 au1AHOutKey) == SNMP_FAILURE)
            {
                return (SNMP_FAILURE);
            }
        }
        switch (pVpnPolicy->uVpnKeyMode.IpsecManualKey.u1VpnAuthAlgo)
        {
            case VPN_HMACMD5:
                tFlag |= IPS_PM_MAC_HMAC_MD5;
                break;
            case VPN_HMACSHA1:
                tFlag |= IPS_PM_MAC_HMAC_SHA1;
                break;
        }

        /* Key should consist the below pairs and in the order specified
         * (Encryption key in, Authentication key in),
         * (Encryption key out, Authentication key out)
         * In our case in and out keys are same so just copy twice !
         */
        tKeyLen = STRLEN (au1ESPOutKey);
        MEMCPY (au1kstr, au1ESPOutKey, tKeyLen);
        MEMCPY ((au1kstr + tKeyLen), au1AHOutKey, (STRLEN (au1AHOutKey)));
        tKeyLen += STRLEN (au1AHOutKey);
        MEMCPY ((au1kstr + tKeyLen), au1kstr, tKeyLen);
        tKeyLen += tKeyLen;

        MEMSET (&rule.r_kinfo.mkey.key, 0x0, 128);
        MEMCPY (&rule.r_kinfo.mkey.key, au1kstr, tKeyLen);
        rule.r_kinfo.mkey.key[tKeyLen + 1] = '\0';
        rule.r_kinfo.mkey.klen = OSIX_HTONL (tKeyLen);

        rule.r_tunnel.t_eflg = OSIX_HTONL (teFlag);
        rule.r_tunnel.t_aflg = OSIX_HTONL (tFlag);
    }
    else                        /* IKE */
    {
        tVpnIdInfo         *pVpnIdInfo = NULL;
        UINT2               u2Length;

        /* Fill the life time in seconds */
        rule.r_mode.ike.i_life =
            pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4LifeTime;
        VPN_CONVERT_LIFETIME_TO_SECS (pVpnPolicy->uVpnKeyMode.VpnIkeDb.
                                      IkePhase1.u4LifeTimeType,
                                      rule.r_mode.ike.i_life);
        rule.r_mode.ike.i_life = OSIX_HTONL (rule.r_mode.ike.i_life);

        rule.r_mode.ike.i_dhgrp =
            OSIX_HTONL (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4DHGroup);

        teFlag = 0x0;
        tFlag = 0x0;
        switch (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4EncryptionAlgo)
        {
            case VPN_DES_CBC:
                teFlag |= IPS_PM_CRYPT_DES;
                break;

            case VPN_3DES_CBC:
                teFlag |= IPS_PM_CRYPT_3DES;
                break;

            case VPN_AES_128:
                teFlag |= IPS_PM_CRYPT_AES;
                rule.r_mode.ike.i_aesalgtype = OSIX_HTONL (IPS_PM_CRYPT_AES128);
                break;
            case VPN_AES_192:
                teFlag |= IPS_PM_CRYPT_AES;
                rule.r_mode.ike.i_aesalgtype = OSIX_HTONL (IPS_PM_CRYPT_AES192);
                break;

            case VPN_AES_256:
                teFlag |= IPS_PM_CRYPT_AES;
                rule.r_mode.ike.i_aesalgtype = OSIX_HTONL (IPS_PM_CRYPT_AES256);
                break;

            default:
                /* Invalid Algo */
                break;
        }

        switch (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4HashAlgo)
        {
            case VPN_HMACMD5:
                tFlag |= IPS_PM_MAC_HMAC_MD5;
                break;

            case VPN_HMACSHA1:
                tFlag |= IPS_PM_MAC_HMAC_SHA1;
                break;
        }

        rule.r_mode.ike.i_eflags = OSIX_HTONL (teFlag);
        rule.r_mode.ike.i_aflags = OSIX_HTONL (tFlag);

        teFlag = 0x0;
        tFlag = 0x0;
        switch (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u1EncryptionAlgo)
        {
            case VPN_DES_CBC:
                teFlag |= IPS_PM_CRYPT_DES;
                teFlag |= IPS_PM_IPSEC_ESP;
                break;

            case VPN_3DES_CBC:
                teFlag |= IPS_PM_CRYPT_3DES;
                teFlag |= IPS_PM_IPSEC_ESP;
                break;

            case VPN_AES_128:
                teFlag |= IPS_PM_CRYPT_AES;
                teFlag |= IPS_PM_IPSEC_ESP;
                rule.r_tunnel.t_aesalgtype = OSIX_HTONL (IPS_PM_CRYPT_AES128);
                break;

            case VPN_AES_192:
                teFlag |= IPS_PM_CRYPT_AES;
                teFlag |= IPS_PM_IPSEC_ESP;
                rule.r_tunnel.t_aesalgtype = OSIX_HTONL (IPS_PM_CRYPT_AES192);
                break;

            case VPN_AES_256:
                teFlag |= IPS_PM_CRYPT_AES;
                teFlag |= IPS_PM_IPSEC_ESP;
                rule.r_tunnel.t_aesalgtype = OSIX_HTONL (IPS_PM_CRYPT_AES256);
                break;

            case VPN_NULLESPALGO:
                teFlag |= IPS_PM_IPSEC_ESP;
                teFlag |= IPS_PM_CRYPT_NULL;
                break;

            default:
                /* Invalid Algo */
                break;
        }

        /* Set Phase-II authentication parameters */
        if (pVpnPolicy->u4VpnSecurityProtocol == SEC_AH)
        {
            tFlag |= IPS_PM_IPSEC_AH;
        }
        else
        {
            tFlag |= IPS_PM_IPSEC_ESP;
        }
        u4HashAlgo = pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u1AuthAlgo;
        switch (u4HashAlgo)
        {
            case VPN_HMACMD5:
                tFlag |= IPS_PM_MAC_HMAC_MD5;
                break;

            case VPN_HMACSHA1:
                tFlag |= IPS_PM_MAC_HMAC_SHA1;
                break;

            default:
                /* Invalid Algo */
                break;
        }

        rule.r_tunnel.t_eflg = OSIX_HTONL (teFlag);
        rule.r_tunnel.t_aflg = OSIX_HTONL (tFlag);

        /* Convert the given time units into seconds and send */
        rule.r_tunnel.t_life =
            pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u4LifeTime;
        VPN_CONVERT_LIFETIME_TO_SECS (pVpnPolicy->uVpnKeyMode.VpnIkeDb.
                                      IkePhase2.u1LifeTimeType,
                                      rule.r_tunnel.t_life);
        rule.r_tunnel.t_life = OSIX_HTONL (rule.r_tunnel.t_life);

        rule.r_tunnel.t_dhgrp =
            OSIX_HTONL (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase2.u1DHGroup);

        rule.r_mode.ike.i_exchmode =
            OSIX_HTONL (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4Mode);

        rule.r_mode.ike.i_dhgrp =
            OSIX_HTONL (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.u4DHGroup);

        /* Setting  ISAKMP Peer ID */
        switch (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.
                i2IdType)
        {
            case VPN_ID_TYPE_IPV4:
                MEMSET (au1TempIp, 0, sizeof (au1TempIp));
                u4TempIpAddr =
                    pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.uID.
                    Ip4Addr;
                VPN_INET_NTOA (au1TempIp, u4TempIpAddr);
                pu1IdVal = au1TempIp;
                pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.
                    u2Length = STRLEN (pu1IdVal);

                break;

            case VPN_ID_TYPE_EMAIL:
                pu1IdVal =
                    pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.uID.
                    au1Email;
                break;

            case VPN_ID_TYPE_FQDN:
                pu1IdVal =
                    pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.uID.
                    au1Fqdn;
                break;

            case VPN_ID_TYPE_KEYID:
                pu1IdVal =
                    pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.uID.
                    au1KeyId;
                break;
        }

        u2Length = STRLEN (pu1IdVal);
        pVpnIdInfo = VpnGetRemoteIdInfo (pVpnPolicy->uVpnKeyMode.VpnIkeDb.
                                         IkePhase1.PolicyPeerID.i2IdType,
                                         pu1IdVal, u2Length);
        /* Set local & remote pre-shared keys */
        STRNCPY ((char *) &rule.r_kinfo.ikey.ik_locl.key,
                 VPN_ID_KEY (pVpnIdInfo), STRLEN (VPN_ID_KEY (pVpnIdInfo)));
        rule.r_kinfo.ikey.ik_locl.klen =
            OSIX_HTONL (strlen (rule.r_kinfo.ikey.ik_locl.key));

        rule.r_mode.ike.i_PeerID.IdType =
            OSIX_HTONL (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.
                        i2IdType);
        rule.r_mode.ike.i_PeerID.IdLen =
            OSIX_HTONL (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.
                        u2Length);
        MEMCPY ((char *) rule.r_mode.ike.i_PeerID.Id, pu1IdVal,
                pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyPeerID.
                u2Length);

        /* Setting  ISAKMP Local  ID */
        switch (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyLocalID.
                i2IdType)
        {

            case VPN_ID_TYPE_IPV4:
                MEMSET (au1TempIp, 0, sizeof (au1TempIp));
                u4TempIpAddr =
                    pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyLocalID.
                    uID.Ip4Addr;
                VPN_INET_NTOA (au1TempIp, u4TempIpAddr);
                pu1IdVal = au1TempIp;
                pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyLocalID.
                    u2Length = STRLEN (pu1IdVal);
                break;

            case VPN_ID_TYPE_EMAIL:
                pu1IdVal =
                    pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyLocalID.
                    uID.au1Email;
                break;

            case VPN_ID_TYPE_FQDN:
                pu1IdVal =
                    pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyLocalID.
                    uID.au1Fqdn;
                break;

            case VPN_ID_TYPE_KEYID:
                pu1IdVal =
                    pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyLocalID.
                    uID.au1KeyId;
                break;
        }

        rule.r_mode.ike.i_LocalID.IdType =
            OSIX_HTONL (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.
                        PolicyLocalID.i2IdType);

        rule.r_mode.ike.i_LocalID.IdLen =
            OSIX_HTONL (pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.
                        PolicyLocalID.u2Length);

        MEMCPY ((char *) rule.r_mode.ike.i_LocalID.Id, pu1IdVal,
                pVpnPolicy->uVpnKeyMode.VpnIkeDb.IkePhase1.PolicyLocalID.
                u2Length);
    }

    MEMCPY (iRule, &rule, sizeof (tRule));

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  VpnNpUpdateRaUserDetails
 Input       :  RAVPN user information and action (ADD/DEL/UPDATE) to be
                performed.
 Description :  Fills the Remote access User name datastructure 
                used by Aricent policy manager. 
 Returns     :  SNMP_SUCCESS/SNMP_FAILURE
****************************************************************************/
INT4
VpnNpUpdateRaUserDetails (tVpnRaUserInfo * pVpnRaUserName, INT4 i4UpdateFlag)
{
    tRaUser            *pRaUser = NULL;
    INT4                i4RetVal = SNMP_SUCCESS;

    /* allocating memory for the RA User structure used by APM */
    pRaUser = MEM_MALLOC (sizeof (tRaUser), tRaUser);
    if ((pVpnRaUserName == NULL) || (pRaUser == NULL))
    {
        return (SNMP_FAILURE);
    }

    STRCPY (pRaUser->au1VpnRaUserName, pVpnRaUserName->au1VpnRaUserName);
    STRCPY (pRaUser->au1VpnRaUserSecret, pVpnRaUserName->au1VpnRaUserSecret);

    VpnDsUnLock ();
    if (i4UpdateFlag == VPN_ADD_RA_USER_INFO)
    {
        /* Configuring the RA User parameters in safenet */
        if (FsVpnNpFormAndSendAddUserCfgMessage (pRaUser) != FNP_SUCCESS)
        {
            i4RetVal = SNMP_FAILURE;
        }
    }
    else if (i4UpdateFlag == VPN_DEL_RA_USER_INFO)
    {
        /* Deleting the RA User parameters in safenet */
        if (FsVpnNpFormAndSendDelUserCfgMessage (pRaUser) != FNP_SUCCESS)
        {
            i4RetVal = SNMP_FAILURE;
        }
    }
    else
    {
        /* update */
    }
    VpnDsLock ();

    MEM_FREE (pRaUser);
    return (i4RetVal);
}

/****************************************************************************
 Function    :  VpnFillRaAddressPoolDetails
 Input       :  pVpnRaAddressPoolNode
                pRaAddressPoolpRaUser  
 Description :  Fills the Remote access Address Pool datastructure 
                used by Aricent policy manager. 
 Returns     :  NONE
****************************************************************************/
VOID
VpnFillRaAddressPoolDetails (tVpnRaAddressPool * pVpnRaAddressPoolNode,
                             tRaAddressPool * pRaAddressPool)
{

    UINT1               au1StartIp[16];
    UINT1               au1EndIp[16];
    UINT4               u4TempIpAddr = 0;

    MEMSET (au1StartIp, 0, sizeof (au1StartIp));
    MEMSET (au1EndIp, 0, sizeof (au1EndIp));

    MEMSET (pRaAddressPool->au1VpnRaAddressPoolRange, 0,
            sizeof (pRaAddressPool->au1VpnRaAddressPoolRange));

    u4TempIpAddr = pVpnRaAddressPoolNode->VpnRaAddressPoolStart;
    VPN_INET_NTOA (au1StartIp, u4TempIpAddr);

    u4TempIpAddr = pVpnRaAddressPoolNode->VpnRaAddressPoolEnd;
    VPN_INET_NTOA (au1EndIp, u4TempIpAddr);

    SPRINTF (pRaAddressPool->au1VpnRaAddressPoolRange, "%s-%s",
             au1StartIp, au1EndIp);

    MEMSET (au1EndIp, 0, sizeof (au1EndIp));
    MEMSET (pRaAddressPool->au1VpnRaAddressNetmask, 0,
            sizeof (pRaAddressPool->au1VpnRaAddressNetmask));

    u4TempIpAddr = pVpnRaAddressPoolNode->VpnRaAddressPoolNetMask;
    VPN_INET_NTOA (au1EndIp, u4TempIpAddr);
    SPRINTF (pRaAddressPool->au1VpnRaAddressNetmask, "%s", au1EndIp);

}
#endif

/**************************************************************************/
/*  Function Name   : VpnUtilGetLogBuffer                                 */
/*  Description     : This function fetches the log file contents from    */
/*                    processor.                                          */
/*  Input (s)       : LogBuffer, Number of Bytes returned                 */
/*  Output(s)       : None.                                               */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS.                          */
/**************************************************************************/
PUBLIC INT4
VpnUtilGetLogBuffer (CHR1 * puVpnLogBuff, UINT4 *u4RetNumBytes, UINT4 u4PgCnt)
{
    INT4                i4RetStatus = OSIX_SUCCESS, i4RetVal = 0;
    UINT4               u4tPgCnt = u4PgCnt, u4Cnt = 0, u4TotBytes = 0;
    tSNMP_OCTET_STRING_TYPE VpnULog;

#ifdef VPN_FP_WANTED

    VpnULog.pu1_OctetList = CLI_ALLOC (VPN_LOGBUFSIZ + 1, UINT1);
    if (VpnULog.pu1_OctetList == NULL)
    {
        return (OSIX_FAILURE);
    }

    VpnULog.i4_Length = VPN_LOGBUFSIZ + 1;
    MEMSET (VpnULog.pu1_OctetList, 0x0, VpnULog.i4_Length);

    while (u4tPgCnt > 0)
    {
        VpnDsUnLock ();
        i4RetVal = FsSecv4NpFormAndSendLogBuffReqMessage (u4RetNumBytes,
                                                          (VOID *) VpnULog.
                                                          pu1_OctetList);
        VpnDsLock ();
        if ((*u4RetNumBytes > 0) && (i4RetVal == FNP_SUCCESS))
        {
            for (u4Cnt = 0; u4Cnt < (VPN_MAXPAGECNT - 1); u4Cnt++)
            {
                MEMSET (gVpnLogBuff + (u4Cnt * VPN_LOGBUFSIZ), 0x0,
                        VPN_LOGBUFSIZ);
                MEMCPY (gVpnLogBuff + (u4Cnt * VPN_LOGBUFSIZ),
                        gVpnLogBuff + ((u4Cnt + 1) * VPN_LOGBUFSIZ),
                        VPN_LOGBUFSIZ);
            }
            MEMSET (gVpnLogBuff + ((VPN_MAXPAGECNT - 1) * VPN_LOGBUFSIZ), 0x0,
                    VPN_LOGBUFSIZ);
            MEMCPY (gVpnLogBuff + ((VPN_MAXPAGECNT - 1) * VPN_LOGBUFSIZ),
                    VpnULog.pu1_OctetList, *u4RetNumBytes);
            u4TotBytes += *u4RetNumBytes;
            gu4VpnClearedLogBuff = FALSE;
        }
        else
        {
            break;
        }
        u4tPgCnt--;
    }

    if (gu4VpnClearedLogBuff == FALSE)
    {
        for (u4Cnt = 0, u4tPgCnt = (VPN_MAXPAGECNT - u4PgCnt);
             u4Cnt < u4PgCnt; u4Cnt++, u4tPgCnt++)
        {
            MEMCPY (puVpnLogBuff + (VPN_LOGBUFSIZ * u4Cnt),
                    gVpnLogBuff + (VPN_LOGBUFSIZ * u4tPgCnt), VPN_LOGBUFSIZ);

        }
    }

    CLI_FREE (VpnULog.pu1_OctetList);

    i4RetStatus = ((i4RetVal == FNP_SUCCESS) ? OSIX_SUCCESS : OSIX_FAILURE);
    *u4RetNumBytes = u4TotBytes;
#else
    UNUSED_PARAM (i4RetVal);
    UNUSED_PARAM (u4tPgCnt);
    UNUSED_PARAM (u4Cnt);
    UNUSED_PARAM (u4TotBytes);
    UNUSED_PARAM (VpnULog);
    UNUSED_PARAM (puVpnLogBuff);
    UNUSED_PARAM (u4RetNumBytes);
#endif
    return i4RetStatus;
}

#ifdef VPN_FP_WANTED
INT4
VpnNpUpdateRemoteIdDb (tVpnIdInfo * pVpnIdInfo, INT4 i4UpdateFlag)
{
    tVpnIpcIdInfo       IpcIdInfo;
    INT4                i4RetVal = SNMP_SUCCESS;

    /* NOTE: Copy the values to IPC data structure; Don't assign pointers.
     * because the memory is different for CAS and the subsystem.
     * Both will be running in their individual memories
     */
    VPN_IPC_ID_TYPE (&IpcIdInfo) = VPN_ID_TYPE (pVpnIdInfo);
    STRCPY (VPN_IPC_ID_VALUE (&IpcIdInfo), VPN_ID_VALUE (pVpnIdInfo));
    STRCPY (VPN_IPC_ID_KEY (&IpcIdInfo), VPN_ID_KEY (pVpnIdInfo));

    VpnDsUnLock ();
    if (i4UpdateFlag == VPN_ADD_REMOTE_ID_INFO)
    {
        if (Secv4IpcFormAndSendRequestMessage (CFA_SFNT,
                                               1,
                                               "VPN_add_remote_id_info",
                                               &IpcIdInfo) != FNP_SUCCESS)
        {
            i4RetVal = SNMP_FAILURE;
        }
    }
    else if (i4UpdateFlag == VPN_DEL_REMOTE_ID_INFO)
    {
        /* Check if ref cnt is zero and no active policies are using it */
        if (pVpnIdInfo->i4RefCnt > 0)
        {
            i4RetVal = SNMP_FAILURE;
        }
        else
        {

            if (Secv4IpcFormAndSendRequestMessage (CFA_SFNT,
                                                   1,
                                                   "VPN_del_remote_id_info",
                                                   &IpcIdInfo) != FNP_SUCCESS)
            {
                i4RetVal = SNMP_FAILURE;
            }
        }
    }
    else
    {
        /* modify */
    }
    VpnDsLock ();

    return (i4RetVal);
}
#endif

#ifdef VPN_FP_WANTED
/**************************************************************************/
/*  Function Name   : VpnUtilClearLogBuffer                               */
/*  Description     : This function clears the log file contents in the   */
/*                    vpn subsystem.                                      */
/*  Input (s)       : None.                                               */
/*  Output(s)       : None.                                               */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS.                          */
/**************************************************************************/
INT4
VpnUtilClearLogBuffer ()
{
    INT4                i4RetVal = FNP_FAILURE;

    VpnDsUnLock ();
    i4RetVal = FsSecv4NpFormAndSendClearLogReqMessage ();

    if (i4RetVal == FNP_SUCCESS)
    {
        MEMSET (gVpnLogBuff, 0x0, ((VPN_LOGBUFSIZ * VPN_MAXPAGECNT) + 1));
        gu4VpnClearedLogBuff = TRUE;
        VpnDsLock ();
        return (OSIX_SUCCESS);
    }
    else
    {
        VpnDsLock ();
        return (OSIX_FAILURE);
    }
}
#endif

/**************************************************************************/
/*  Function Name   : VpnUtilGetSafnetVersion                             */
/*  Description     : This function fetches the safenet version contents  */
/*                    from the processor.                                 */
/*  Input (s)       : SafnetVersionBuff                                  */
/*  Output(s)       : None.                                               */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS.                          */
/**************************************************************************/
PUBLIC INT4
VpnUtilGetSafnetVersion (CHR1 * puVpnSafnetVersion)
{
    INT4                i4RetStatus = OSIX_FAILURE;

#ifdef VPN_FP_WANTED

    tVpnFwVerResp       VpnVerInfo;

    MEMSET (&VpnVerInfo, 0x0, sizeof (tVpnFwVerResp));
    VpnVerInfo.iVpnVerBuf = puVpnSafnetVersion;

    /* Forming the message */
    if (Secv4IpcFormAndSendRequestMessage (CFA_SFNT,
                                           1,
                                           "VPN_get_safnet_version",
                                           (VOID *) &VpnVerInfo) == FNP_SUCCESS)

    {
        i4RetStatus = OSIX_SUCCESS;
    }
    else
    {
        i4RetStatus = OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (puVpnSafnetVersion);
#endif
    return i4RetStatus;

}

/**************************************************************************/
/*  Function Name   : VpnUtilSetSafnetSysTime                             */
/*  Description     : This function fetches the safenet version contents  */
/*                    from the processor.                                 */
/*  Input (s)       : None                                  */
/*  Output(s)       : None.                                               */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS.                          */
/**************************************************************************/
PUBLIC INT4
VpnUtilSetSafnetSysTime ()
{
    INT4                i4RetStatus = OSIX_FAILURE;

#ifdef VPN_FP_WANTED
    UINT4               u4SecsSinceEpoch = 0;

    u4SecsSinceEpoch = UtlGetTimeSinceEpoch ();

    /* Forming the message */
    if (Secv4IpcFormAndSendRequestMessage (CFA_SFNT,
                                           1,
                                           "VPN_set_safnet_systime",
                                           u4SecsSinceEpoch) == FNP_SUCCESS)

    {
        i4RetStatus = OSIX_SUCCESS;
    }
    else
    {
        i4RetStatus = OSIX_FAILURE;
    }
#endif
    return i4RetStatus;

}

/**************************************************************************/
/*  Function Name   : VpnUtilGetVpnHealthStatus                           */
/*  Description     : This function checks the health status of the vpn   */
/*                    subsystem                                           */
/*  Input (s)       : None                                                */
/*  Output(s)       : None.                                               */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS.                          */
/**************************************************************************/
PUBLIC INT4
VpnUtilGetVpnHealthStatus ()
{
    INT4                i4RetStatus = OSIX_FAILURE;
    UINT4               u4SubSysStatus = 0;

#ifdef VPN_FP_WANTED
    if (ISS_FAILURE ==
        IssBoardGetHealthStatusFromIntfType (CFA_SFNT, 1, &u4SubSysStatus))
    {
        return OSIX_FAILURE;
    }

    if (u4SubSysStatus == SUBSYS_NOT_PRESENT)
    {
        return OSIX_FAILURE;
    }

    if (VpnUtilSetSafnetSysTime () == OSIX_FAILURE)
    {
        /* Subsystem Down */
        IssBoardSetHealthStatus (CFA_SFNT, 1, SUBSYS_HEALTH_DOWN);
        SubsysGenerateSubsystemStatusTrap (CFA_SFNT, 1);

        if (u4SubSysStatus == SUBSYS_HEALTH_UP)
        {
            IssBoardIncrementHealthStatusChangeCount (CFA_SFNT, 1);
        }
        return OSIX_FAILURE;
    }
    else
    {
        /* Subsystem UP */
        IssBoardSetHealthStatus (CFA_SFNT, 1, SUBSYS_HEALTH_UP);

        if (u4SubSysStatus == SUBSYS_HEALTH_DOWN)
        {
            IssBoardIncrementHealthStatusChangeCount (CFA_SFNT, 1);
            SubsysGenerateSubsystemStatusTrap (CFA_SFNT, 1);
        }
        return OSIX_SUCCESS;
    }
#else
    UNUSED_PARAM (u4SubSysStatus);
#endif
    return i4RetStatus;
}

/****************************************************************************/
/*  Function Name   : VpnUtilGetPort7LinkStatus                             */
/*  Description     : This function fetches the link status of port 7       */
/*                    residing in the safenet subsystem.                    */
/*  Input (s)       : None                                                  */
/*  Output(s)       : Link status stored in pi4P7LinkStatus.                */
/*  Returns         : FNP_SUCCESS and FNP_FAILURE if IPC fails.             */
/****************************************************************************/
PUBLIC INT4
VpnUtilGetPort7LinkStatus (INT4 *pi4P7LinkStatus)
{
    INT4                i4RetStatus = OSIX_FAILURE;

#ifdef VPN_FP_WANTED
    INT4                i4LinkStatus = CFA_IF_DOWN;

    /* Forming the message */
    if (Secv4IpcFormAndSendRequestMessage (CFA_SFNT,
                                           1,
                                           "VPN_get_port7_LinkStatus",
                                           &i4LinkStatus) == FNP_SUCCESS)
    {
        i4RetStatus = OSIX_SUCCESS;
    }
    else
    {
        i4RetStatus = OSIX_FAILURE;
    }

    *pi4P7LinkStatus = i4LinkStatus;
#else
    UNUSED_PARAM (pi4P7LinkStatus);
#endif
    return i4RetStatus;

}

/* **************************************************************************
 * Function Name : VpnUtilConvMask2Prefix
 * Description   : Converts the given network mask into equivalent prefix
 *                 length.
 * Input(s)      : u4NetMask - Subnet mask to be converted
 * Output(s)     : None
 * Return value  : Returns the calculated prefix length
 * **************************************************************************/
UINT2
VpnUtilConvMask2Prefix (UINT4 u4NetMask)
{
    UINT2               u2CidrVal = 0;

    while (gau4VpnCidrSubnetMask[u2CidrVal] != u4NetMask)
    {
        ++u2CidrVal;
        if (u2CidrVal > VPN_MAX_CIDR)
            break;
    }

    return (u2CidrVal);
}

/* EOF */
