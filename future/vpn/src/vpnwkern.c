/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vpnwkern.c,v 1.17 2015/04/11 12:51:02 siva Exp $
 *
 * Description: File containing apis for user to kernel communication
 *******************************************************************/
#include "vpnwincs.h"
#include "secv4.h"
#include "vpninc.h"
#include "npapi.h"

#define KERN_INFO_TRC "<6>"
#define IOCTL_SUCCESS FNP_SUCCESS

/* -------------------------------------------------------------
 *
 * Function: VpnNmhIoctl
 *
 * -------------------------------------------------------------
 */
int
VpnNmhIoctl (unsigned long p)
{
    int                 rc = 0;
    int                 npiocnr;
    union unNpapi       lv;
    UINT4               u4Secv4Flags = 0;
    tOsixKernUserInfo   OsixKerUseInfo;

    gi4CliWebSetError = 0;

    /* Copy the ioctl command number */

    OsixKerUseInfo.pDest = &npiocnr;
    OsixKerUseInfo.pSrc = (int *) p;

    if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo, 4, 0) ==
        OSIX_FAILURE)
    {
        return (-EFAULT);
    }

    MEMSET (&lv, 0, sizeof (union unNpapi));

    switch (npiocnr)
    {

        case NMH_GET_FS_VPN_GLOBAL_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnGlobalStatus;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnGlobalStatus *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnGlobalStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnGlobalStatus.rval =
                nmhGetFsVpnGlobalStatus (lv.nmhGetFsVpnGlobalStatus.
                                         pi4RetValFsVpnGlobalStatus);
            lv.nmhGetFsVpnGlobalStatus.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnGlobalStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnGlobalStatus;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnGlobalStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_RA_SERVER:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnRaServer;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnRaServer *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRaServer),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnRaServer.rval =
                nmhGetFsVpnRaServer (lv.nmhGetFsVpnRaServer.
                                     pi4RetValFsVpnRaServer);
            lv.nmhGetFsVpnRaServer.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnRaServer *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnRaServer;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRaServer)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_MAX_TUNNELS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnMaxTunnels;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnMaxTunnels *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnMaxTunnels),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnMaxTunnels.rval =
                nmhGetFsVpnMaxTunnels (lv.nmhGetFsVpnMaxTunnels.
                                       pi4RetValFsVpnMaxTunnels);
            lv.nmhGetFsVpnMaxTunnels.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnMaxTunnels *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnMaxTunnels;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnMaxTunnels)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_IP_PKTS_IN:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIpPktsIn;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIpPktsIn *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIpPktsIn),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIpPktsIn.rval =
                nmhGetFsVpnIpPktsIn (lv.nmhGetFsVpnIpPktsIn.
                                     pu4RetValFsVpnIpPktsIn);
            lv.nmhGetFsVpnIpPktsIn.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIpPktsIn *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIpPktsIn;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIpPktsIn)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_IP_PKTS_OUT:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIpPktsOut;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIpPktsOut *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIpPktsOut),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIpPktsOut.rval =
                nmhGetFsVpnIpPktsOut (lv.nmhGetFsVpnIpPktsOut.
                                      pu4RetValFsVpnIpPktsOut);
            lv.nmhGetFsVpnIpPktsOut.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIpPktsOut *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIpPktsOut;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIpPktsOut)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_PKTS_SECURED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnPktsSecured;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnPktsSecured *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnPktsSecured),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnPktsSecured.rval =
                nmhGetFsVpnPktsSecured (lv.nmhGetFsVpnPktsSecured.
                                        pu4RetValFsVpnPktsSecured);
            lv.nmhGetFsVpnPktsSecured.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnPktsSecured *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnPktsSecured;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnPktsSecured)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_PKTS_DROPPED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnPktsDropped;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnPktsDropped *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnPktsDropped),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnPktsDropped.rval =
                nmhGetFsVpnPktsDropped (lv.nmhGetFsVpnPktsDropped.
                                        pu4RetValFsVpnPktsDropped);
            lv.nmhGetFsVpnPktsDropped.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnPktsDropped *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnPktsDropped;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnPktsDropped)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_IKE_S_AS_ACTIVE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIkeSAsActive;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIkeSAsActive *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkeSAsActive),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIkeSAsActive.rval =
                nmhGetFsVpnIkeSAsActive (lv.nmhGetFsVpnIkeSAsActive.
                                         pu4RetValFsVpnIkeSAsActive);
            lv.nmhGetFsVpnIkeSAsActive.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIkeSAsActive *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIkeSAsActive;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkeSAsActive)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_IKE_NEGOTIATIONS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIkeNegotiations;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIkeNegotiations *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkeNegotiations),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIkeNegotiations.rval =
                nmhGetFsVpnIkeNegotiations (lv.nmhGetFsVpnIkeNegotiations.
                                            pu4RetValFsVpnIkeNegotiations);
            lv.nmhGetFsVpnIkeNegotiations.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIkeNegotiations *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIkeNegotiations;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkeNegotiations)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_IKE_REKEYS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIkeRekeys;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIkeRekeys *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkeRekeys),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIkeRekeys.rval =
                nmhGetFsVpnIkeRekeys (lv.nmhGetFsVpnIkeRekeys.
                                      pu4RetValFsVpnIkeRekeys);
            lv.nmhGetFsVpnIkeRekeys.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIkeRekeys *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIkeRekeys;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkeRekeys)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_IKE_NEGO_FAILED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIkeNegoFailed;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIkeNegoFailed *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkeNegoFailed),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIkeNegoFailed.rval =
                nmhGetFsVpnIkeNegoFailed (lv.nmhGetFsVpnIkeNegoFailed.
                                          pu4RetValFsVpnIkeNegoFailed);
            lv.nmhGetFsVpnIkeNegoFailed.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIkeNegoFailed *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIkeNegoFailed;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkeNegoFailed)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_I_P_SEC_S_AS_ACTIVE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIPSecSAsActive;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIPSecSAsActive *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIPSecSAsActive),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIPSecSAsActive.rval =
                nmhGetFsVpnIPSecSAsActive (lv.nmhGetFsVpnIPSecSAsActive.
                                           pu4RetValFsVpnIPSecSAsActive);
            lv.nmhGetFsVpnIPSecSAsActive.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIPSecSAsActive *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIPSecSAsActive;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIPSecSAsActive)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_I_P_SEC_NEGOTIATIONS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIPSecNegotiations;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIPSecNegotiations *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIPSecNegotiations),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIPSecNegotiations.rval =
                nmhGetFsVpnIPSecNegotiations (lv.nmhGetFsVpnIPSecNegotiations.
                                              pu4RetValFsVpnIPSecNegotiations);
            lv.nmhGetFsVpnIPSecNegotiations.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIPSecNegotiations *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIPSecNegotiations;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIPSecNegotiations)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_I_P_SEC_NEGO_FAILED:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIPSecNegoFailed;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIPSecNegoFailed *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIPSecNegoFailed),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIPSecNegoFailed.rval =
                nmhGetFsVpnIPSecNegoFailed (lv.nmhGetFsVpnIPSecNegoFailed.
                                            pu4RetValFsVpnIPSecNegoFailed);
            lv.nmhGetFsVpnIPSecNegoFailed.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIPSecNegoFailed *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIPSecNegoFailed;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIPSecNegoFailed)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_TOTAL_REKEYS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnTotalRekeys;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnTotalRekeys *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnTotalRekeys),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnTotalRekeys.rval =
                nmhGetFsVpnTotalRekeys (lv.nmhGetFsVpnTotalRekeys.
                                        pu4RetValFsVpnTotalRekeys);
            lv.nmhGetFsVpnTotalRekeys.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnTotalRekeys *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnTotalRekeys;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnTotalRekeys)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_GLOBAL_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnGlobalStatus;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnGlobalStatus *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnGlobalStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnGlobalStatus.rval =
                nmhSetFsVpnGlobalStatus (lv.nmhSetFsVpnGlobalStatus.
                                         i4SetValFsVpnGlobalStatus);
            lv.nmhSetFsVpnGlobalStatus.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnGlobalStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnGlobalStatus;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnGlobalStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_RA_SERVER:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnRaServer;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnRaServer *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRaServer),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnRaServer.rval =
                nmhSetFsVpnRaServer (lv.nmhSetFsVpnRaServer.
                                     i4SetValFsVpnRaServer);
            lv.nmhSetFsVpnRaServer.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnRaServer *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnRaServer;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRaServer)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_GLOBAL_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnGlobalStatus;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnGlobalStatus *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnGlobalStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnGlobalStatus.rval =
                nmhTestv2FsVpnGlobalStatus (lv.nmhTestv2FsVpnGlobalStatus.
                                            pu4ErrorCode,
                                            lv.nmhTestv2FsVpnGlobalStatus.
                                            i4TestValFsVpnGlobalStatus);
            lv.nmhTestv2FsVpnGlobalStatus.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnGlobalStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnGlobalStatus;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnGlobalStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_RA_SERVER:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnRaServer;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnRaServer *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnRaServer),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnRaServer.rval =
                nmhTestv2FsVpnRaServer (lv.nmhTestv2FsVpnRaServer.pu4ErrorCode,
                                        lv.nmhTestv2FsVpnRaServer.
                                        i4TestValFsVpnRaServer);
            lv.nmhTestv2FsVpnRaServer.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnRaServer *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnRaServer;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnRaServer)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_FS_VPN_GLOBAL_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2FsVpnGlobalStatus;
            OsixKerUseInfo.pSrc = (tNpwnmhDepv2FsVpnGlobalStatus *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhDepv2FsVpnGlobalStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhDepv2FsVpnGlobalStatus.rval =
                nmhDepv2FsVpnGlobalStatus (lv.nmhDepv2FsVpnGlobalStatus.
                                           pu4ErrorCode,
                                           lv.nmhDepv2FsVpnGlobalStatus.
                                           pSnmpIndexList,
                                           lv.nmhDepv2FsVpnGlobalStatus.
                                           pSnmpVarBind);
            lv.nmhDepv2FsVpnGlobalStatus.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhDepv2FsVpnGlobalStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhDepv2FsVpnGlobalStatus;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhDepv2FsVpnGlobalStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_FS_VPN_RA_SERVER:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2FsVpnRaServer;
            OsixKerUseInfo.pSrc = (tNpwnmhDepv2FsVpnRaServer *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhDepv2FsVpnRaServer),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhDepv2FsVpnRaServer.rval =
                nmhDepv2FsVpnRaServer (lv.nmhDepv2FsVpnRaServer.pu4ErrorCode,
                                       lv.nmhDepv2FsVpnRaServer.pSnmpIndexList,
                                       lv.nmhDepv2FsVpnRaServer.pSnmpVarBind);
            lv.nmhDepv2FsVpnRaServer.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhDepv2FsVpnRaServer *) p;
            OsixKerUseInfo.pSrc = &lv.nmhDepv2FsVpnRaServer;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhDepv2FsVpnRaServer)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_FS_VPN_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhValidateIndexInstanceFsVpnTable;
            OsixKerUseInfo.pSrc = (tNpwnmhValidateIndexInstanceFsVpnTable *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhValidateIndexInstanceFsVpnTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhValidateIndexInstanceFsVpnTable.rval =
                nmhValidateIndexInstanceFsVpnTable (lv.
                                                    nmhValidateIndexInstanceFsVpnTable.
                                                    pFsVpnPolicyName);
            lv.nmhValidateIndexInstanceFsVpnTable.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhValidateIndexInstanceFsVpnTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhValidateIndexInstanceFsVpnTable;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhValidateIndexInstanceFsVpnTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_FS_VPN_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexFsVpnTable;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFirstIndexFsVpnTable *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFirstIndexFsVpnTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFirstIndexFsVpnTable.rval =
                nmhGetFirstIndexFsVpnTable (lv.nmhGetFirstIndexFsVpnTable.
                                            pFsVpnPolicyName);
            lv.nmhGetFirstIndexFsVpnTable.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFirstIndexFsVpnTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexFsVpnTable;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFirstIndexFsVpnTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_FS_VPN_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexFsVpnTable;
            OsixKerUseInfo.pSrc = (tNpwnmhGetNextIndexFsVpnTable *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetNextIndexFsVpnTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetNextIndexFsVpnTable.rval =
                nmhGetNextIndexFsVpnTable (lv.nmhGetNextIndexFsVpnTable.
                                           pFsVpnPolicyName,
                                           lv.nmhGetNextIndexFsVpnTable.
                                           pNextFsVpnPolicyName);
            lv.nmhGetNextIndexFsVpnTable.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetNextIndexFsVpnTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexFsVpnTable;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetNextIndexFsVpnTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_POLICY_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnPolicyType;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnPolicyType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnPolicyType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnPolicyType.rval =
                nmhGetFsVpnPolicyType (lv.nmhGetFsVpnPolicyType.
                                       pFsVpnPolicyName,
                                       lv.nmhGetFsVpnPolicyType.
                                       pi4RetValFsVpnPolicyType);
            lv.nmhGetFsVpnPolicyType.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnPolicyType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnPolicyType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnPolicyType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_CERT_ALGO_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnCertAlgoType;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnCertAlgoType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnCertAlgoType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnCertAlgoType.rval =
                nmhGetFsVpnCertAlgoType (lv.nmhGetFsVpnCertAlgoType.
                                         pFsVpnPolicyName,
                                         lv.nmhGetFsVpnCertAlgoType.
                                         pi4RetValFsVpnCertAlgoType);
            lv.nmhGetFsVpnCertAlgoType.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnCertAlgoType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnCertAlgoType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnCertAlgoType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_POLICY_PRIORITY:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnPolicyPriority;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnPolicyPriority *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnPolicyPriority),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnPolicyPriority.rval =
                nmhGetFsVpnPolicyPriority (lv.nmhGetFsVpnPolicyPriority.
                                           pFsVpnPolicyName,
                                           lv.nmhGetFsVpnPolicyPriority.
                                           pi4RetValFsVpnPolicyPriority);
            lv.nmhGetFsVpnPolicyPriority.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnPolicyPriority *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnPolicyPriority;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnPolicyPriority)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_TUN_TERM_ADDR_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnTunTermAddrType;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnTunTermAddrType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnTunTermAddrType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnTunTermAddrType.rval =
                nmhGetFsVpnTunTermAddrType (lv.nmhGetFsVpnTunTermAddrType.
                                            pFsVpnPolicyName,
                                            lv.nmhGetFsVpnTunTermAddrType.
                                            pi4RetValFsVpnTunTermAddrType);
            lv.nmhGetFsVpnTunTermAddrType.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnTunTermAddrType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnTunTermAddrType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnTunTermAddrType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_LOCAL_TUN_TERM_ADDR:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnLocalTunTermAddr;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnLocalTunTermAddr *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnLocalTunTermAddr),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnLocalTunTermAddr.rval =
                nmhGetFsVpnLocalTunTermAddr (lv.nmhGetFsVpnLocalTunTermAddr.
                                             pFsVpnPolicyName,
                                             lv.nmhGetFsVpnLocalTunTermAddr.
                                             pRetValFsVpnLocalTunTermAddr);
            lv.nmhGetFsVpnLocalTunTermAddr.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnLocalTunTermAddr *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnLocalTunTermAddr;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnLocalTunTermAddr)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_REMOTE_TUN_TERM_ADDR:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnRemoteTunTermAddr;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnRemoteTunTermAddr *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRemoteTunTermAddr),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnRemoteTunTermAddr.rval =
                nmhGetFsVpnRemoteTunTermAddr (lv.nmhGetFsVpnRemoteTunTermAddr.
                                              pFsVpnPolicyName,
                                              lv.nmhGetFsVpnRemoteTunTermAddr.
                                              pRetValFsVpnRemoteTunTermAddr);
            lv.nmhGetFsVpnRemoteTunTermAddr.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnRemoteTunTermAddr *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnRemoteTunTermAddr;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRemoteTunTermAddr)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_PROTECT_NETWORK_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnProtectNetworkType;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnProtectNetworkType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnProtectNetworkType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnProtectNetworkType.rval =
                nmhGetFsVpnProtectNetworkType (lv.nmhGetFsVpnProtectNetworkType.
                                               pFsVpnPolicyName,
                                               lv.nmhGetFsVpnProtectNetworkType.
                                               pi4RetValFsVpnProtectNetworkType);
            lv.nmhGetFsVpnProtectNetworkType.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnProtectNetworkType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnProtectNetworkType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnProtectNetworkType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_LOCAL_PROTECT_NETWORK:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnLocalProtectNetwork;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnLocalProtectNetwork *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnLocalProtectNetwork),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnLocalProtectNetwork.rval =
                nmhGetFsVpnLocalProtectNetwork (lv.
                                                nmhGetFsVpnLocalProtectNetwork.
                                                pFsVpnPolicyName,
                                                lv.
                                                nmhGetFsVpnLocalProtectNetwork.
                                                pRetValFsVpnLocalProtectNetwork);
            lv.nmhGetFsVpnLocalProtectNetwork.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnLocalProtectNetwork *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnLocalProtectNetwork;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnLocalProtectNetwork)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_LOCAL_PROTECT_NETWORK_PREFIX_LEN:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnLocalProtectNetworkPrefixLen;
            OsixKerUseInfo.pSrc =
                (tNpwnmhGetFsVpnLocalProtectNetworkPrefixLen *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhGetFsVpnLocalProtectNetworkPrefixLen),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnLocalProtectNetworkPrefixLen.rval =
                nmhGetFsVpnLocalProtectNetworkPrefixLen (lv.
                                                         nmhGetFsVpnLocalProtectNetworkPrefixLen.
                                                         pFsVpnPolicyName,
                                                         lv.
                                                         nmhGetFsVpnLocalProtectNetworkPrefixLen.
                                                         pu4RetValFsVpnLocalProtectNetworkPrefixLen);
            lv.nmhGetFsVpnLocalProtectNetworkPrefixLen.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhGetFsVpnLocalProtectNetworkPrefixLen *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnLocalProtectNetworkPrefixLen;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tNpwnmhGetFsVpnLocalProtectNetworkPrefixLen)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_REMOTE_PROTECT_NETWORK:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnRemoteProtectNetwork;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnRemoteProtectNetwork *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRemoteProtectNetwork),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnRemoteProtectNetwork.rval =
                nmhGetFsVpnRemoteProtectNetwork (lv.
                                                 nmhGetFsVpnRemoteProtectNetwork.
                                                 pFsVpnPolicyName,
                                                 lv.
                                                 nmhGetFsVpnRemoteProtectNetwork.
                                                 pRetValFsVpnRemoteProtectNetwork);
            lv.nmhGetFsVpnRemoteProtectNetwork.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnRemoteProtectNetwork *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnRemoteProtectNetwork;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRemoteProtectNetwork)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_REMOTE_PROTECT_NETWORK_PREFIX_LEN:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnRemoteProtectNetworkPrefixLen;
            OsixKerUseInfo.pSrc =
                (tNpwnmhGetFsVpnRemoteProtectNetworkPrefixLen *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhGetFsVpnRemoteProtectNetworkPrefixLen),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnRemoteProtectNetworkPrefixLen.rval =
                nmhGetFsVpnRemoteProtectNetworkPrefixLen (lv.
                                                          nmhGetFsVpnRemoteProtectNetworkPrefixLen.
                                                          pFsVpnPolicyName,
                                                          lv.
                                                          nmhGetFsVpnRemoteProtectNetworkPrefixLen.
                                                          pu4RetValFsVpnRemoteProtectNetworkPrefixLen);
            lv.nmhGetFsVpnRemoteProtectNetworkPrefixLen.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhGetFsVpnRemoteProtectNetworkPrefixLen *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnRemoteProtectNetworkPrefixLen;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tNpwnmhGetFsVpnRemoteProtectNetworkPrefixLen)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_IKE_SRC_PORT_RANGE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIkeSrcPortRange;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIkeSrcPortRange *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkeSrcPortRange),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIkeSrcPortRange.rval =
                nmhGetFsVpnIkeSrcPortRange (lv.nmhGetFsVpnIkeSrcPortRange.
                                            pFsVpnPolicyName,
                                            lv.nmhGetFsVpnIkeSrcPortRange.
                                            pRetValFsVpnIkeSrcPortRange);
            lv.nmhGetFsVpnIkeSrcPortRange.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIkeSrcPortRange *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIkeSrcPortRange;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkeSrcPortRange)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_IKE_DST_PORT_RANGE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIkeDstPortRange;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIkeDstPortRange *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkeDstPortRange),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIkeDstPortRange.rval =
                nmhGetFsVpnIkeDstPortRange (lv.nmhGetFsVpnIkeDstPortRange.
                                            pFsVpnPolicyName,
                                            lv.nmhGetFsVpnIkeDstPortRange.
                                            pRetValFsVpnIkeDstPortRange);
            lv.nmhGetFsVpnIkeDstPortRange.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIkeDstPortRange *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIkeDstPortRange;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkeDstPortRange)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_SECURITY_PROTOCOL:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnSecurityProtocol;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnSecurityProtocol *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnSecurityProtocol),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnSecurityProtocol.rval =
                nmhGetFsVpnSecurityProtocol (lv.nmhGetFsVpnSecurityProtocol.
                                             pFsVpnPolicyName,
                                             lv.nmhGetFsVpnSecurityProtocol.
                                             pi4RetValFsVpnSecurityProtocol);
            lv.nmhGetFsVpnSecurityProtocol.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnSecurityProtocol *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnSecurityProtocol;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnSecurityProtocol)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_INBOUND_SPI:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnInboundSpi;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnInboundSpi *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnInboundSpi),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnInboundSpi.rval =
                nmhGetFsVpnInboundSpi (lv.nmhGetFsVpnInboundSpi.
                                       pFsVpnPolicyName,
                                       lv.nmhGetFsVpnInboundSpi.
                                       pi4RetValFsVpnInboundSpi);
            lv.nmhGetFsVpnInboundSpi.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnInboundSpi *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnInboundSpi;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnInboundSpi)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_OUTBOUND_SPI:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnOutboundSpi;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnOutboundSpi *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnOutboundSpi),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnOutboundSpi.rval =
                nmhGetFsVpnOutboundSpi (lv.nmhGetFsVpnOutboundSpi.
                                        pFsVpnPolicyName,
                                        lv.nmhGetFsVpnOutboundSpi.
                                        pi4RetValFsVpnOutboundSpi);
            lv.nmhGetFsVpnOutboundSpi.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnOutboundSpi *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnOutboundSpi;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnOutboundSpi)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_MODE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnMode;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnMode *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnMode), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnMode.rval =
                nmhGetFsVpnMode (lv.nmhGetFsVpnMode.pFsVpnPolicyName,
                                 lv.nmhGetFsVpnMode.pi4RetValFsVpnMode);
            lv.nmhGetFsVpnMode.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnMode *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnMode;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnMode)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_AUTH_ALGO:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnAuthAlgo;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnAuthAlgo *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnAuthAlgo),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnAuthAlgo.rval =
                nmhGetFsVpnAuthAlgo (lv.nmhGetFsVpnAuthAlgo.pFsVpnPolicyName,
                                     lv.nmhGetFsVpnAuthAlgo.
                                     pi4RetValFsVpnAuthAlgo);
            lv.nmhGetFsVpnAuthAlgo.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnAuthAlgo *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnAuthAlgo;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnAuthAlgo)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_AH_KEY:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnAhKey;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnAhKey *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnAhKey), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnAhKey.rval =
                nmhGetFsVpnAhKey (lv.nmhGetFsVpnAhKey.pFsVpnPolicyName,
                                  lv.nmhGetFsVpnAhKey.pRetValFsVpnAhKey);
            lv.nmhGetFsVpnAhKey.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnAhKey *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnAhKey;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnAhKey)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_ENCR_ALGO:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnEncrAlgo;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnEncrAlgo *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnEncrAlgo),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnEncrAlgo.rval =
                nmhGetFsVpnEncrAlgo (lv.nmhGetFsVpnEncrAlgo.pFsVpnPolicyName,
                                     lv.nmhGetFsVpnEncrAlgo.
                                     pi4RetValFsVpnEncrAlgo);
            lv.nmhGetFsVpnEncrAlgo.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnEncrAlgo *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnEncrAlgo;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnEncrAlgo)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_ESP_KEY:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnEspKey;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnEspKey *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnEspKey), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnEspKey.rval =
                nmhGetFsVpnEspKey (lv.nmhGetFsVpnEspKey.pFsVpnPolicyName,
                                   lv.nmhGetFsVpnEspKey.pRetValFsVpnEspKey);
            lv.nmhGetFsVpnEspKey.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnEspKey *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnEspKey;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnEspKey)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_ANTI_REPLAY:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnAntiReplay;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnAntiReplay *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnAntiReplay),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnAntiReplay.rval =
                nmhGetFsVpnAntiReplay (lv.nmhGetFsVpnAntiReplay.
                                       pFsVpnPolicyName,
                                       lv.nmhGetFsVpnAntiReplay.
                                       pi4RetValFsVpnAntiReplay);
            lv.nmhGetFsVpnAntiReplay.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnAntiReplay *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnAntiReplay;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnAntiReplay)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_POLICY_FLAG:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnPolicyFlag;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnPolicyFlag *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnPolicyFlag),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnPolicyFlag.rval =
                nmhGetFsVpnPolicyFlag (lv.nmhGetFsVpnPolicyFlag.
                                       pFsVpnPolicyName,
                                       lv.nmhGetFsVpnPolicyFlag.
                                       pi4RetValFsVpnPolicyFlag);
            lv.nmhGetFsVpnPolicyFlag.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnPolicyFlag *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnPolicyFlag;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnPolicyFlag)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_PROTOCOL:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnProtocol;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnProtocol *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnProtocol),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnProtocol.rval =
                nmhGetFsVpnProtocol (lv.nmhGetFsVpnProtocol.pFsVpnPolicyName,
                                     lv.nmhGetFsVpnProtocol.
                                     pi4RetValFsVpnProtocol);
            lv.nmhGetFsVpnProtocol.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnProtocol *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnProtocol;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnProtocol)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_POLICY_INTF_INDEX:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnPolicyIntfIndex;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnPolicyIntfIndex *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnPolicyIntfIndex),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnPolicyIntfIndex.rval =
                nmhGetFsVpnPolicyIntfIndex (lv.nmhGetFsVpnPolicyIntfIndex.
                                            pFsVpnPolicyName,
                                            lv.nmhGetFsVpnPolicyIntfIndex.
                                            pi4RetValFsVpnPolicyIntfIndex);
            lv.nmhGetFsVpnPolicyIntfIndex.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnPolicyIntfIndex *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnPolicyIntfIndex;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnPolicyIntfIndex)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_IKE_PHASE1_HASH_ALGO:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIkePhase1HashAlgo;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIkePhase1HashAlgo *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase1HashAlgo),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIkePhase1HashAlgo.rval =
                nmhGetFsVpnIkePhase1HashAlgo (lv.nmhGetFsVpnIkePhase1HashAlgo.
                                              pFsVpnPolicyName,
                                              lv.nmhGetFsVpnIkePhase1HashAlgo.
                                              pi4RetValFsVpnIkePhase1HashAlgo);
            lv.nmhGetFsVpnIkePhase1HashAlgo.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIkePhase1HashAlgo *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIkePhase1HashAlgo;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase1HashAlgo)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_IKE_PHASE1_ENCRYPTION_ALGO:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIkePhase1EncryptionAlgo;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIkePhase1EncryptionAlgo *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase1EncryptionAlgo),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIkePhase1EncryptionAlgo.rval =
                nmhGetFsVpnIkePhase1EncryptionAlgo (lv.
                                                    nmhGetFsVpnIkePhase1EncryptionAlgo.
                                                    pFsVpnPolicyName,
                                                    lv.
                                                    nmhGetFsVpnIkePhase1EncryptionAlgo.
                                                    pi4RetValFsVpnIkePhase1EncryptionAlgo);
            lv.nmhGetFsVpnIkePhase1EncryptionAlgo.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIkePhase1EncryptionAlgo *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIkePhase1EncryptionAlgo;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase1EncryptionAlgo)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_IKE_PHASE1_D_H_GROUP:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIkePhase1DHGroup;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIkePhase1DHGroup *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase1DHGroup),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIkePhase1DHGroup.rval =
                nmhGetFsVpnIkePhase1DHGroup (lv.nmhGetFsVpnIkePhase1DHGroup.
                                             pFsVpnPolicyName,
                                             lv.nmhGetFsVpnIkePhase1DHGroup.
                                             pi4RetValFsVpnIkePhase1DHGroup);
            lv.nmhGetFsVpnIkePhase1DHGroup.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIkePhase1DHGroup *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIkePhase1DHGroup;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase1DHGroup)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_IKE_PHASE1_LOCAL_ID_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIkePhase1LocalIdType;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIkePhase1LocalIdType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase1LocalIdType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIkePhase1LocalIdType.rval =
                nmhGetFsVpnIkePhase1LocalIdType (lv.
                                                 nmhGetFsVpnIkePhase1LocalIdType.
                                                 pFsVpnPolicyName,
                                                 lv.
                                                 nmhGetFsVpnIkePhase1LocalIdType.
                                                 pi4RetValFsVpnIkePhase1LocalIdType);
            lv.nmhGetFsVpnIkePhase1LocalIdType.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIkePhase1LocalIdType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIkePhase1LocalIdType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase1LocalIdType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_IKE_PHASE1_LOCAL_ID_VALUE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIkePhase1LocalIdValue;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIkePhase1LocalIdValue *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase1LocalIdValue),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIkePhase1LocalIdValue.rval =
                nmhGetFsVpnIkePhase1LocalIdValue (lv.
                                                  nmhGetFsVpnIkePhase1LocalIdValue.
                                                  pFsVpnPolicyName,
                                                  lv.
                                                  nmhGetFsVpnIkePhase1LocalIdValue.
                                                  pRetValFsVpnIkePhase1LocalIdValue);
            lv.nmhGetFsVpnIkePhase1LocalIdValue.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIkePhase1LocalIdValue *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIkePhase1LocalIdValue;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase1LocalIdValue)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_IKE_PHASE1_PEER_ID_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIkePhase1PeerIdType;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIkePhase1PeerIdType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase1PeerIdType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIkePhase1PeerIdType.rval =
                nmhGetFsVpnIkePhase1PeerIdType (lv.
                                                nmhGetFsVpnIkePhase1PeerIdType.
                                                pFsVpnPolicyName,
                                                lv.
                                                nmhGetFsVpnIkePhase1PeerIdType.
                                                pi4RetValFsVpnIkePhase1PeerIdType);
            lv.nmhGetFsVpnIkePhase1PeerIdType.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIkePhase1PeerIdType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIkePhase1PeerIdType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase1PeerIdType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_IKE_PHASE1_PEER_ID_VALUE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIkePhase1PeerIdValue;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIkePhase1PeerIdValue *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase1PeerIdValue),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIkePhase1PeerIdValue.rval =
                nmhGetFsVpnIkePhase1PeerIdValue (lv.
                                                 nmhGetFsVpnIkePhase1PeerIdValue.
                                                 pFsVpnPolicyName,
                                                 lv.
                                                 nmhGetFsVpnIkePhase1PeerIdValue.
                                                 pRetValFsVpnIkePhase1PeerIdValue);
            lv.nmhGetFsVpnIkePhase1PeerIdValue.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIkePhase1PeerIdValue *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIkePhase1PeerIdValue;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase1PeerIdValue)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_IKE_PHASE1_LIFE_TIME_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIkePhase1LifeTimeType;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIkePhase1LifeTimeType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase1LifeTimeType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIkePhase1LifeTimeType.rval =
                nmhGetFsVpnIkePhase1LifeTimeType (lv.
                                                  nmhGetFsVpnIkePhase1LifeTimeType.
                                                  pFsVpnPolicyName,
                                                  lv.
                                                  nmhGetFsVpnIkePhase1LifeTimeType.
                                                  pi4RetValFsVpnIkePhase1LifeTimeType);
            lv.nmhGetFsVpnIkePhase1LifeTimeType.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIkePhase1LifeTimeType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIkePhase1LifeTimeType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase1LifeTimeType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_IKE_PHASE1_LIFE_TIME:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIkePhase1LifeTime;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIkePhase1LifeTime *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase1LifeTime),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIkePhase1LifeTime.rval =
                nmhGetFsVpnIkePhase1LifeTime (lv.nmhGetFsVpnIkePhase1LifeTime.
                                              pFsVpnPolicyName,
                                              lv.nmhGetFsVpnIkePhase1LifeTime.
                                              pi4RetValFsVpnIkePhase1LifeTime);
            lv.nmhGetFsVpnIkePhase1LifeTime.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIkePhase1LifeTime *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIkePhase1LifeTime;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase1LifeTime)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_IKE_PHASE1_MODE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIkePhase1Mode;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIkePhase1Mode *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase1Mode),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIkePhase1Mode.rval =
                nmhGetFsVpnIkePhase1Mode (lv.nmhGetFsVpnIkePhase1Mode.
                                          pFsVpnPolicyName,
                                          lv.nmhGetFsVpnIkePhase1Mode.
                                          pi4RetValFsVpnIkePhase1Mode);
            lv.nmhGetFsVpnIkePhase1Mode.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIkePhase1Mode *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIkePhase1Mode;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase1Mode)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_IKE_PHASE2_AUTH_ALGO:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIkePhase2AuthAlgo;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIkePhase2AuthAlgo *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase2AuthAlgo),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIkePhase2AuthAlgo.rval =
                nmhGetFsVpnIkePhase2AuthAlgo (lv.nmhGetFsVpnIkePhase2AuthAlgo.
                                              pFsVpnPolicyName,
                                              lv.nmhGetFsVpnIkePhase2AuthAlgo.
                                              pi4RetValFsVpnIkePhase2AuthAlgo);
            lv.nmhGetFsVpnIkePhase2AuthAlgo.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIkePhase2AuthAlgo *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIkePhase2AuthAlgo;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase2AuthAlgo)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_IKE_PHASE2_ESP_ENCRYPTION_ALGO:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIkePhase2EspEncryptionAlgo;
            OsixKerUseInfo.pSrc =
                (tNpwnmhGetFsVpnIkePhase2EspEncryptionAlgo *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhGetFsVpnIkePhase2EspEncryptionAlgo),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIkePhase2EspEncryptionAlgo.rval =
                nmhGetFsVpnIkePhase2EspEncryptionAlgo (lv.
                                                       nmhGetFsVpnIkePhase2EspEncryptionAlgo.
                                                       pFsVpnPolicyName,
                                                       lv.
                                                       nmhGetFsVpnIkePhase2EspEncryptionAlgo.
                                                       pi4RetValFsVpnIkePhase2EspEncryptionAlgo);
            lv.nmhGetFsVpnIkePhase2EspEncryptionAlgo.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhGetFsVpnIkePhase2EspEncryptionAlgo *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIkePhase2EspEncryptionAlgo;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase2EspEncryptionAlgo))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_IKE_PHASE2_LIFE_TIME_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIkePhase2LifeTimeType;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIkePhase2LifeTimeType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase2LifeTimeType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIkePhase2LifeTimeType.rval =
                nmhGetFsVpnIkePhase2LifeTimeType (lv.
                                                  nmhGetFsVpnIkePhase2LifeTimeType.
                                                  pFsVpnPolicyName,
                                                  lv.
                                                  nmhGetFsVpnIkePhase2LifeTimeType.
                                                  pi4RetValFsVpnIkePhase2LifeTimeType);
            lv.nmhGetFsVpnIkePhase2LifeTimeType.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIkePhase2LifeTimeType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIkePhase2LifeTimeType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase2LifeTimeType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_IKE_PHASE2_LIFE_TIME:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIkePhase2LifeTime;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIkePhase2LifeTime *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase2LifeTime),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIkePhase2LifeTime.rval =
                nmhGetFsVpnIkePhase2LifeTime (lv.nmhGetFsVpnIkePhase2LifeTime.
                                              pFsVpnPolicyName,
                                              lv.nmhGetFsVpnIkePhase2LifeTime.
                                              pi4RetValFsVpnIkePhase2LifeTime);
            lv.nmhGetFsVpnIkePhase2LifeTime.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIkePhase2LifeTime *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIkePhase2LifeTime;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase2LifeTime)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_IKE_PHASE2_D_H_GROUP:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIkePhase2DHGroup;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIkePhase2DHGroup *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase2DHGroup),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIkePhase2DHGroup.rval =
                nmhGetFsVpnIkePhase2DHGroup (lv.nmhGetFsVpnIkePhase2DHGroup.
                                             pFsVpnPolicyName,
                                             lv.nmhGetFsVpnIkePhase2DHGroup.
                                             pi4RetValFsVpnIkePhase2DHGroup);
            lv.nmhGetFsVpnIkePhase2DHGroup.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIkePhase2DHGroup *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIkePhase2DHGroup;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkePhase2DHGroup)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_IKE_VERSION:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnIkeVersion;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnIkeVersion *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkeVersion),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnIkeVersion.rval =
                nmhGetFsVpnIkeVersion (lv.nmhGetFsVpnIkeVersion.
                                       pFsVpnPolicyName,
                                       lv.nmhGetFsVpnIkeVersion.
                                       pi4RetValFsVpnIkeVersion);
            lv.nmhGetFsVpnIkeVersion.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnIkeVersion *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnIkeVersion;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnIkeVersion)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_POLICY_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnPolicyRowStatus;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnPolicyRowStatus *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnPolicyRowStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnPolicyRowStatus.rval =
                nmhGetFsVpnPolicyRowStatus (lv.nmhGetFsVpnPolicyRowStatus.
                                            pFsVpnPolicyName,
                                            lv.nmhGetFsVpnPolicyRowStatus.
                                            pi4RetValFsVpnPolicyRowStatus);
            lv.nmhGetFsVpnPolicyRowStatus.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnPolicyRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnPolicyRowStatus;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnPolicyRowStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_POLICY_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnPolicyType;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnPolicyType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnPolicyType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnPolicyType.rval =
                nmhSetFsVpnPolicyType (lv.nmhSetFsVpnPolicyType.
                                       pFsVpnPolicyName,
                                       lv.nmhSetFsVpnPolicyType.
                                       i4SetValFsVpnPolicyType);
            lv.nmhSetFsVpnPolicyType.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnPolicyType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnPolicyType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnPolicyType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_CERT_ALGO_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnCertAlgoType;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnCertAlgoType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnCertAlgoType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnCertAlgoType.rval =
                nmhSetFsVpnCertAlgoType (lv.nmhSetFsVpnCertAlgoType.
                                         pFsVpnPolicyName,
                                         lv.nmhSetFsVpnCertAlgoType.
                                         i4SetValFsVpnCertAlgoType);
            lv.nmhSetFsVpnCertAlgoType.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnCertAlgoType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnCertAlgoType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnCertAlgoType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_POLICY_PRIORITY:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnPolicyPriority;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnPolicyPriority *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnPolicyPriority),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnPolicyPriority.rval =
                nmhSetFsVpnPolicyPriority (lv.nmhSetFsVpnPolicyPriority.
                                           pFsVpnPolicyName,
                                           lv.nmhSetFsVpnPolicyPriority.
                                           i4SetValFsVpnPolicyPriority);
            lv.nmhSetFsVpnPolicyPriority.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnPolicyPriority *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnPolicyPriority;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnPolicyPriority)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_TUN_TERM_ADDR_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnTunTermAddrType;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnTunTermAddrType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnTunTermAddrType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnTunTermAddrType.rval =
                nmhSetFsVpnTunTermAddrType (lv.nmhSetFsVpnTunTermAddrType.
                                            pFsVpnPolicyName,
                                            lv.nmhSetFsVpnTunTermAddrType.
                                            i4SetValFsVpnTunTermAddrType);
            lv.nmhSetFsVpnTunTermAddrType.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnTunTermAddrType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnTunTermAddrType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnTunTermAddrType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_LOCAL_TUN_TERM_ADDR:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnLocalTunTermAddr;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnLocalTunTermAddr *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnLocalTunTermAddr),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnLocalTunTermAddr.rval =
                nmhSetFsVpnLocalTunTermAddr (lv.nmhSetFsVpnLocalTunTermAddr.
                                             pFsVpnPolicyName,
                                             lv.nmhSetFsVpnLocalTunTermAddr.
                                             pSetValFsVpnLocalTunTermAddr);
            lv.nmhSetFsVpnLocalTunTermAddr.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnLocalTunTermAddr *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnLocalTunTermAddr;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnLocalTunTermAddr)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_REMOTE_TUN_TERM_ADDR:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnRemoteTunTermAddr;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnRemoteTunTermAddr *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRemoteTunTermAddr),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnRemoteTunTermAddr.rval =
                nmhSetFsVpnRemoteTunTermAddr (lv.nmhSetFsVpnRemoteTunTermAddr.
                                              pFsVpnPolicyName,
                                              lv.nmhSetFsVpnRemoteTunTermAddr.
                                              pSetValFsVpnRemoteTunTermAddr);
            lv.nmhSetFsVpnRemoteTunTermAddr.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnRemoteTunTermAddr *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnRemoteTunTermAddr;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRemoteTunTermAddr)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_PROTECT_NETWORK_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnProtectNetworkType;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnProtectNetworkType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnProtectNetworkType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnProtectNetworkType.rval =
                nmhSetFsVpnProtectNetworkType (lv.nmhSetFsVpnProtectNetworkType.
                                               pFsVpnPolicyName,
                                               lv.nmhSetFsVpnProtectNetworkType.
                                               i4SetValFsVpnProtectNetworkType);
            lv.nmhSetFsVpnProtectNetworkType.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnProtectNetworkType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnProtectNetworkType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnProtectNetworkType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_LOCAL_PROTECT_NETWORK:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnLocalProtectNetwork;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnLocalProtectNetwork *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnLocalProtectNetwork),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnLocalProtectNetwork.rval =
                nmhSetFsVpnLocalProtectNetwork (lv.
                                                nmhSetFsVpnLocalProtectNetwork.
                                                pFsVpnPolicyName,
                                                lv.
                                                nmhSetFsVpnLocalProtectNetwork.
                                                pSetValFsVpnLocalProtectNetwork);
            lv.nmhSetFsVpnLocalProtectNetwork.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnLocalProtectNetwork *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnLocalProtectNetwork;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnLocalProtectNetwork)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_LOCAL_PROTECT_NETWORK_PREFIX_LEN:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnLocalProtectNetworkPrefixLen;
            OsixKerUseInfo.pSrc =
                (tNpwnmhSetFsVpnLocalProtectNetworkPrefixLen *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhSetFsVpnLocalProtectNetworkPrefixLen),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnLocalProtectNetworkPrefixLen.rval =
                nmhSetFsVpnLocalProtectNetworkPrefixLen (lv.
                                                         nmhSetFsVpnLocalProtectNetworkPrefixLen.
                                                         pFsVpnPolicyName,
                                                         lv.
                                                         nmhSetFsVpnLocalProtectNetworkPrefixLen.
                                                         u4SetValFsVpnLocalProtectNetworkPrefixLen);
            lv.nmhSetFsVpnLocalProtectNetworkPrefixLen.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhSetFsVpnLocalProtectNetworkPrefixLen *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnLocalProtectNetworkPrefixLen;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tNpwnmhSetFsVpnLocalProtectNetworkPrefixLen)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_REMOTE_PROTECT_NETWORK:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnRemoteProtectNetwork;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnRemoteProtectNetwork *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRemoteProtectNetwork),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnRemoteProtectNetwork.rval =
                nmhSetFsVpnRemoteProtectNetwork (lv.
                                                 nmhSetFsVpnRemoteProtectNetwork.
                                                 pFsVpnPolicyName,
                                                 lv.
                                                 nmhSetFsVpnRemoteProtectNetwork.
                                                 pSetValFsVpnRemoteProtectNetwork);
            lv.nmhSetFsVpnRemoteProtectNetwork.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnRemoteProtectNetwork *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnRemoteProtectNetwork;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRemoteProtectNetwork)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_REMOTE_PROTECT_NETWORK_PREFIX_LEN:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnRemoteProtectNetworkPrefixLen;
            OsixKerUseInfo.pSrc =
                (tNpwnmhSetFsVpnRemoteProtectNetworkPrefixLen *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhSetFsVpnRemoteProtectNetworkPrefixLen),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnRemoteProtectNetworkPrefixLen.rval =
                nmhSetFsVpnRemoteProtectNetworkPrefixLen (lv.
                                                          nmhSetFsVpnRemoteProtectNetworkPrefixLen.
                                                          pFsVpnPolicyName,
                                                          lv.
                                                          nmhSetFsVpnRemoteProtectNetworkPrefixLen.
                                                          u4SetValFsVpnRemoteProtectNetworkPrefixLen);
            lv.nmhSetFsVpnRemoteProtectNetworkPrefixLen.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhSetFsVpnRemoteProtectNetworkPrefixLen *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnRemoteProtectNetworkPrefixLen;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tNpwnmhSetFsVpnRemoteProtectNetworkPrefixLen)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_IKE_SRC_PORT_RANGE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnIkeSrcPortRange;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnIkeSrcPortRange *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkeSrcPortRange),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnIkeSrcPortRange.rval =
                nmhSetFsVpnIkeSrcPortRange (lv.nmhSetFsVpnIkeSrcPortRange.
                                            pFsVpnPolicyName,
                                            lv.nmhSetFsVpnIkeSrcPortRange.
                                            pSetValFsVpnIkeSrcPortRange);
            lv.nmhSetFsVpnIkeSrcPortRange.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnIkeSrcPortRange *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnIkeSrcPortRange;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkeSrcPortRange)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_IKE_DST_PORT_RANGE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnIkeDstPortRange;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnIkeDstPortRange *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkeDstPortRange),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnIkeDstPortRange.rval =
                nmhSetFsVpnIkeDstPortRange (lv.nmhSetFsVpnIkeDstPortRange.
                                            pFsVpnPolicyName,
                                            lv.nmhSetFsVpnIkeDstPortRange.
                                            pSetValFsVpnIkeDstPortRange);
            lv.nmhSetFsVpnIkeDstPortRange.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnIkeDstPortRange *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnIkeDstPortRange;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkeDstPortRange)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_SECURITY_PROTOCOL:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnSecurityProtocol;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnSecurityProtocol *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnSecurityProtocol),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnSecurityProtocol.rval =
                nmhSetFsVpnSecurityProtocol (lv.nmhSetFsVpnSecurityProtocol.
                                             pFsVpnPolicyName,
                                             lv.nmhSetFsVpnSecurityProtocol.
                                             i4SetValFsVpnSecurityProtocol);
            lv.nmhSetFsVpnSecurityProtocol.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnSecurityProtocol *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnSecurityProtocol;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnSecurityProtocol)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_INBOUND_SPI:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnInboundSpi;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnInboundSpi *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnInboundSpi),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnInboundSpi.rval =
                nmhSetFsVpnInboundSpi (lv.nmhSetFsVpnInboundSpi.
                                       pFsVpnPolicyName,
                                       lv.nmhSetFsVpnInboundSpi.
                                       i4SetValFsVpnInboundSpi);
            lv.nmhSetFsVpnInboundSpi.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnInboundSpi *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnInboundSpi;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnInboundSpi)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_OUTBOUND_SPI:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnOutboundSpi;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnOutboundSpi *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnOutboundSpi),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnOutboundSpi.rval =
                nmhSetFsVpnOutboundSpi (lv.nmhSetFsVpnOutboundSpi.
                                        pFsVpnPolicyName,
                                        lv.nmhSetFsVpnOutboundSpi.
                                        i4SetValFsVpnOutboundSpi);
            lv.nmhSetFsVpnOutboundSpi.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnOutboundSpi *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnOutboundSpi;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnOutboundSpi)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_MODE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnMode;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnMode *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnMode), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnMode.rval =
                nmhSetFsVpnMode (lv.nmhSetFsVpnMode.pFsVpnPolicyName,
                                 lv.nmhSetFsVpnMode.i4SetValFsVpnMode);
            lv.nmhSetFsVpnMode.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnMode *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnMode;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnMode)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_AUTH_ALGO:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnAuthAlgo;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnAuthAlgo *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnAuthAlgo),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnAuthAlgo.rval =
                nmhSetFsVpnAuthAlgo (lv.nmhSetFsVpnAuthAlgo.pFsVpnPolicyName,
                                     lv.nmhSetFsVpnAuthAlgo.
                                     i4SetValFsVpnAuthAlgo);
            lv.nmhSetFsVpnAuthAlgo.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnAuthAlgo *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnAuthAlgo;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnAuthAlgo)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_AH_KEY:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnAhKey;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnAhKey *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnAhKey), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnAhKey.rval =
                nmhSetFsVpnAhKey (lv.nmhSetFsVpnAhKey.pFsVpnPolicyName,
                                  lv.nmhSetFsVpnAhKey.pSetValFsVpnAhKey);
            lv.nmhSetFsVpnAhKey.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnAhKey *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnAhKey;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnAhKey)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_ENCR_ALGO:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnEncrAlgo;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnEncrAlgo *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnEncrAlgo),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnEncrAlgo.rval =
                nmhSetFsVpnEncrAlgo (lv.nmhSetFsVpnEncrAlgo.pFsVpnPolicyName,
                                     lv.nmhSetFsVpnEncrAlgo.
                                     i4SetValFsVpnEncrAlgo);
            lv.nmhSetFsVpnEncrAlgo.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnEncrAlgo *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnEncrAlgo;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnEncrAlgo)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_ESP_KEY:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnEspKey;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnEspKey *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnEspKey), 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnEspKey.rval =
                nmhSetFsVpnEspKey (lv.nmhSetFsVpnEspKey.pFsVpnPolicyName,
                                   lv.nmhSetFsVpnEspKey.pSetValFsVpnEspKey);
            lv.nmhSetFsVpnEspKey.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnEspKey *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnEspKey;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnEspKey)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_ANTI_REPLAY:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnAntiReplay;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnAntiReplay *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnAntiReplay),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnAntiReplay.rval =
                nmhSetFsVpnAntiReplay (lv.nmhSetFsVpnAntiReplay.
                                       pFsVpnPolicyName,
                                       lv.nmhSetFsVpnAntiReplay.
                                       i4SetValFsVpnAntiReplay);
            lv.nmhSetFsVpnAntiReplay.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnAntiReplay *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnAntiReplay;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnAntiReplay)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_POLICY_FLAG:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnPolicyFlag;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnPolicyFlag *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnPolicyFlag),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnPolicyFlag.rval =
                nmhSetFsVpnPolicyFlag (lv.nmhSetFsVpnPolicyFlag.
                                       pFsVpnPolicyName,
                                       lv.nmhSetFsVpnPolicyFlag.
                                       i4SetValFsVpnPolicyFlag);
            lv.nmhSetFsVpnPolicyFlag.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnPolicyFlag *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnPolicyFlag;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnPolicyFlag)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_PROTOCOL:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnProtocol;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnProtocol *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnProtocol),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnProtocol.rval =
                nmhSetFsVpnProtocol (lv.nmhSetFsVpnProtocol.pFsVpnPolicyName,
                                     lv.nmhSetFsVpnProtocol.
                                     i4SetValFsVpnProtocol);
            lv.nmhSetFsVpnProtocol.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnProtocol *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnProtocol;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnProtocol)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_POLICY_INTF_INDEX:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnPolicyIntfIndex;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnPolicyIntfIndex *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnPolicyIntfIndex),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnPolicyIntfIndex.rval =
                nmhSetFsVpnPolicyIntfIndex (lv.nmhSetFsVpnPolicyIntfIndex.
                                            pFsVpnPolicyName,
                                            lv.nmhSetFsVpnPolicyIntfIndex.
                                            i4SetValFsVpnPolicyIntfIndex);
            lv.nmhSetFsVpnPolicyIntfIndex.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnPolicyIntfIndex *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnPolicyIntfIndex;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnPolicyIntfIndex)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_IKE_PHASE1_HASH_ALGO:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnIkePhase1HashAlgo;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnIkePhase1HashAlgo *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase1HashAlgo),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnIkePhase1HashAlgo.rval =
                nmhSetFsVpnIkePhase1HashAlgo (lv.nmhSetFsVpnIkePhase1HashAlgo.
                                              pFsVpnPolicyName,
                                              lv.nmhSetFsVpnIkePhase1HashAlgo.
                                              i4SetValFsVpnIkePhase1HashAlgo);
            lv.nmhSetFsVpnIkePhase1HashAlgo.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnIkePhase1HashAlgo *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnIkePhase1HashAlgo;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase1HashAlgo)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_IKE_PHASE1_ENCRYPTION_ALGO:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnIkePhase1EncryptionAlgo;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnIkePhase1EncryptionAlgo *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase1EncryptionAlgo),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnIkePhase1EncryptionAlgo.rval =
                nmhSetFsVpnIkePhase1EncryptionAlgo (lv.
                                                    nmhSetFsVpnIkePhase1EncryptionAlgo.
                                                    pFsVpnPolicyName,
                                                    lv.
                                                    nmhSetFsVpnIkePhase1EncryptionAlgo.
                                                    i4SetValFsVpnIkePhase1EncryptionAlgo);
            lv.nmhSetFsVpnIkePhase1EncryptionAlgo.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnIkePhase1EncryptionAlgo *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnIkePhase1EncryptionAlgo;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase1EncryptionAlgo)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_IKE_PHASE1_D_H_GROUP:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnIkePhase1DHGroup;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnIkePhase1DHGroup *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase1DHGroup),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnIkePhase1DHGroup.rval =
                nmhSetFsVpnIkePhase1DHGroup (lv.nmhSetFsVpnIkePhase1DHGroup.
                                             pFsVpnPolicyName,
                                             lv.nmhSetFsVpnIkePhase1DHGroup.
                                             i4SetValFsVpnIkePhase1DHGroup);
            lv.nmhSetFsVpnIkePhase1DHGroup.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnIkePhase1DHGroup *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnIkePhase1DHGroup;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase1DHGroup)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_IKE_PHASE1_LOCAL_ID_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnIkePhase1LocalIdType;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnIkePhase1LocalIdType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase1LocalIdType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnIkePhase1LocalIdType.rval =
                nmhSetFsVpnIkePhase1LocalIdType (lv.
                                                 nmhSetFsVpnIkePhase1LocalIdType.
                                                 pFsVpnPolicyName,
                                                 lv.
                                                 nmhSetFsVpnIkePhase1LocalIdType.
                                                 i4SetValFsVpnIkePhase1LocalIdType);
            lv.nmhSetFsVpnIkePhase1LocalIdType.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnIkePhase1LocalIdType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnIkePhase1LocalIdType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase1LocalIdType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_IKE_PHASE1_LOCAL_ID_VALUE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnIkePhase1LocalIdValue;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnIkePhase1LocalIdValue *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase1LocalIdValue),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnIkePhase1LocalIdValue.rval =
                nmhSetFsVpnIkePhase1LocalIdValue (lv.
                                                  nmhSetFsVpnIkePhase1LocalIdValue.
                                                  pFsVpnPolicyName,
                                                  lv.
                                                  nmhSetFsVpnIkePhase1LocalIdValue.
                                                  pSetValFsVpnIkePhase1LocalIdValue);
            lv.nmhSetFsVpnIkePhase1LocalIdValue.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnIkePhase1LocalIdValue *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnIkePhase1LocalIdValue;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase1LocalIdValue)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_IKE_PHASE1_PEER_ID_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnIkePhase1PeerIdType;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnIkePhase1PeerIdType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase1PeerIdType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnIkePhase1PeerIdType.rval =
                nmhSetFsVpnIkePhase1PeerIdType (lv.
                                                nmhSetFsVpnIkePhase1PeerIdType.
                                                pFsVpnPolicyName,
                                                lv.
                                                nmhSetFsVpnIkePhase1PeerIdType.
                                                i4SetValFsVpnIkePhase1PeerIdType);
            lv.nmhSetFsVpnIkePhase1PeerIdType.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnIkePhase1PeerIdType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnIkePhase1PeerIdType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase1PeerIdType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_IKE_PHASE1_PEER_ID_VALUE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnIkePhase1PeerIdValue;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnIkePhase1PeerIdValue *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase1PeerIdValue),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnIkePhase1PeerIdValue.rval =
                nmhSetFsVpnIkePhase1PeerIdValue (lv.
                                                 nmhSetFsVpnIkePhase1PeerIdValue.
                                                 pFsVpnPolicyName,
                                                 lv.
                                                 nmhSetFsVpnIkePhase1PeerIdValue.
                                                 pSetValFsVpnIkePhase1PeerIdValue);
            lv.nmhSetFsVpnIkePhase1PeerIdValue.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnIkePhase1PeerIdValue *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnIkePhase1PeerIdValue;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase1PeerIdValue)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_IKE_PHASE1_LIFE_TIME_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnIkePhase1LifeTimeType;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnIkePhase1LifeTimeType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase1LifeTimeType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnIkePhase1LifeTimeType.rval =
                nmhSetFsVpnIkePhase1LifeTimeType (lv.
                                                  nmhSetFsVpnIkePhase1LifeTimeType.
                                                  pFsVpnPolicyName,
                                                  lv.
                                                  nmhSetFsVpnIkePhase1LifeTimeType.
                                                  i4SetValFsVpnIkePhase1LifeTimeType);
            lv.nmhSetFsVpnIkePhase1LifeTimeType.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnIkePhase1LifeTimeType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnIkePhase1LifeTimeType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase1LifeTimeType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_IKE_PHASE1_LIFE_TIME:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnIkePhase1LifeTime;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnIkePhase1LifeTime *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase1LifeTime),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnIkePhase1LifeTime.rval =
                nmhSetFsVpnIkePhase1LifeTime (lv.nmhSetFsVpnIkePhase1LifeTime.
                                              pFsVpnPolicyName,
                                              lv.nmhSetFsVpnIkePhase1LifeTime.
                                              i4SetValFsVpnIkePhase1LifeTime);
            lv.nmhSetFsVpnIkePhase1LifeTime.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnIkePhase1LifeTime *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnIkePhase1LifeTime;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase1LifeTime)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_IKE_PHASE1_MODE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnIkePhase1Mode;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnIkePhase1Mode *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase1Mode),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnIkePhase1Mode.rval =
                nmhSetFsVpnIkePhase1Mode (lv.nmhSetFsVpnIkePhase1Mode.
                                          pFsVpnPolicyName,
                                          lv.nmhSetFsVpnIkePhase1Mode.
                                          i4SetValFsVpnIkePhase1Mode);
            lv.nmhSetFsVpnIkePhase1Mode.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnIkePhase1Mode *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnIkePhase1Mode;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase1Mode)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_IKE_PHASE2_AUTH_ALGO:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnIkePhase2AuthAlgo;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnIkePhase2AuthAlgo *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase2AuthAlgo),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnIkePhase2AuthAlgo.rval =
                nmhSetFsVpnIkePhase2AuthAlgo (lv.nmhSetFsVpnIkePhase2AuthAlgo.
                                              pFsVpnPolicyName,
                                              lv.nmhSetFsVpnIkePhase2AuthAlgo.
                                              i4SetValFsVpnIkePhase2AuthAlgo);
            lv.nmhSetFsVpnIkePhase2AuthAlgo.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnIkePhase2AuthAlgo *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnIkePhase2AuthAlgo;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase2AuthAlgo)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_IKE_PHASE2_ESP_ENCRYPTION_ALGO:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnIkePhase2EspEncryptionAlgo;
            OsixKerUseInfo.pSrc =
                (tNpwnmhSetFsVpnIkePhase2EspEncryptionAlgo *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhSetFsVpnIkePhase2EspEncryptionAlgo),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnIkePhase2EspEncryptionAlgo.rval =
                nmhSetFsVpnIkePhase2EspEncryptionAlgo (lv.
                                                       nmhSetFsVpnIkePhase2EspEncryptionAlgo.
                                                       pFsVpnPolicyName,
                                                       lv.
                                                       nmhSetFsVpnIkePhase2EspEncryptionAlgo.
                                                       i4SetValFsVpnIkePhase2EspEncryptionAlgo);
            lv.nmhSetFsVpnIkePhase2EspEncryptionAlgo.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhSetFsVpnIkePhase2EspEncryptionAlgo *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnIkePhase2EspEncryptionAlgo;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase2EspEncryptionAlgo))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_IKE_PHASE2_LIFE_TIME_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnIkePhase2LifeTimeType;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnIkePhase2LifeTimeType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase2LifeTimeType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnIkePhase2LifeTimeType.rval =
                nmhSetFsVpnIkePhase2LifeTimeType (lv.
                                                  nmhSetFsVpnIkePhase2LifeTimeType.
                                                  pFsVpnPolicyName,
                                                  lv.
                                                  nmhSetFsVpnIkePhase2LifeTimeType.
                                                  i4SetValFsVpnIkePhase2LifeTimeType);
            lv.nmhSetFsVpnIkePhase2LifeTimeType.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnIkePhase2LifeTimeType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnIkePhase2LifeTimeType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase2LifeTimeType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_IKE_PHASE2_LIFE_TIME:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnIkePhase2LifeTime;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnIkePhase2LifeTime *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase2LifeTime),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnIkePhase2LifeTime.rval =
                nmhSetFsVpnIkePhase2LifeTime (lv.nmhSetFsVpnIkePhase2LifeTime.
                                              pFsVpnPolicyName,
                                              lv.nmhSetFsVpnIkePhase2LifeTime.
                                              i4SetValFsVpnIkePhase2LifeTime);
            lv.nmhSetFsVpnIkePhase2LifeTime.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnIkePhase2LifeTime *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnIkePhase2LifeTime;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase2LifeTime)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_IKE_PHASE2_D_H_GROUP:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnIkePhase2DHGroup;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnIkePhase2DHGroup *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase2DHGroup),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnIkePhase2DHGroup.rval =
                nmhSetFsVpnIkePhase2DHGroup (lv.nmhSetFsVpnIkePhase2DHGroup.
                                             pFsVpnPolicyName,
                                             lv.nmhSetFsVpnIkePhase2DHGroup.
                                             i4SetValFsVpnIkePhase2DHGroup);
            lv.nmhSetFsVpnIkePhase2DHGroup.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnIkePhase2DHGroup *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnIkePhase2DHGroup;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkePhase2DHGroup)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_IKE_VERSION:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnIkeVersion;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnIkeVersion *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkeVersion),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnIkeVersion.rval =
                nmhSetFsVpnIkeVersion (lv.nmhSetFsVpnIkeVersion.
                                       pFsVpnPolicyName,
                                       lv.nmhSetFsVpnIkeVersion.
                                       i4SetValFsVpnIkeVersion);
            lv.nmhSetFsVpnIkeVersion.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnIkeVersion *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnIkeVersion;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnIkeVersion)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_POLICY_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnPolicyRowStatus;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnPolicyRowStatus *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnPolicyRowStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnPolicyRowStatus.rval =
                nmhSetFsVpnPolicyRowStatus (lv.nmhSetFsVpnPolicyRowStatus.
                                            pFsVpnPolicyName,
                                            lv.nmhSetFsVpnPolicyRowStatus.
                                            i4SetValFsVpnPolicyRowStatus);
            lv.nmhSetFsVpnPolicyRowStatus.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnPolicyRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnPolicyRowStatus;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnPolicyRowStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_POLICY_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnPolicyType;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnPolicyType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnPolicyType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnPolicyType.rval =
                nmhTestv2FsVpnPolicyType (lv.nmhTestv2FsVpnPolicyType.
                                          pu4ErrorCode,
                                          lv.nmhTestv2FsVpnPolicyType.
                                          pFsVpnPolicyName,
                                          lv.nmhTestv2FsVpnPolicyType.
                                          i4TestValFsVpnPolicyType);
            lv.nmhTestv2FsVpnPolicyType.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnPolicyType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnPolicyType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnPolicyType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_CERT_ALGO_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnCertAlgoType;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnCertAlgoType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnCertAlgoType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnCertAlgoType.rval =
                nmhTestv2FsVpnCertAlgoType (lv.nmhTestv2FsVpnCertAlgoType.
                                            pu4ErrorCode,
                                            lv.nmhTestv2FsVpnCertAlgoType.
                                            pFsVpnPolicyName,
                                            lv.nmhTestv2FsVpnCertAlgoType.
                                            i4TestValFsVpnCertAlgoType);
            lv.nmhTestv2FsVpnCertAlgoType.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnCertAlgoType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnCertAlgoType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnCertAlgoType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_POLICY_PRIORITY:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnPolicyPriority;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnPolicyPriority *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnPolicyPriority),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnPolicyPriority.rval =
                nmhTestv2FsVpnPolicyPriority (lv.nmhTestv2FsVpnPolicyPriority.
                                              pu4ErrorCode,
                                              lv.nmhTestv2FsVpnPolicyPriority.
                                              pFsVpnPolicyName,
                                              lv.nmhTestv2FsVpnPolicyPriority.
                                              i4TestValFsVpnPolicyPriority);
            lv.nmhTestv2FsVpnPolicyPriority.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnPolicyPriority *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnPolicyPriority;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnPolicyPriority)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_TUN_TERM_ADDR_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnTunTermAddrType;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnTunTermAddrType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnTunTermAddrType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnTunTermAddrType.rval =
                nmhTestv2FsVpnTunTermAddrType (lv.
                                               nmhTestv2FsVpnTunTermAddrType.
                                               pu4ErrorCode,
                                               lv.
                                               nmhTestv2FsVpnTunTermAddrType.
                                               pFsVpnPolicyName,
                                               lv.
                                               nmhTestv2FsVpnTunTermAddrType.
                                               i4TestValFsVpnTunTermAddrType);
            lv.nmhTestv2FsVpnTunTermAddrType.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnTunTermAddrType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnTunTermAddrType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnTunTermAddrType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_LOCAL_TUN_TERM_ADDR:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnLocalTunTermAddr;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnLocalTunTermAddr *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnLocalTunTermAddr),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnLocalTunTermAddr.rval =
                nmhTestv2FsVpnLocalTunTermAddr (lv.
                                                nmhTestv2FsVpnLocalTunTermAddr.
                                                pu4ErrorCode,
                                                lv.
                                                nmhTestv2FsVpnLocalTunTermAddr.
                                                pFsVpnPolicyName,
                                                lv.
                                                nmhTestv2FsVpnLocalTunTermAddr.
                                                pTestValFsVpnLocalTunTermAddr);
            lv.nmhTestv2FsVpnLocalTunTermAddr.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnLocalTunTermAddr *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnLocalTunTermAddr;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnLocalTunTermAddr)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_REMOTE_TUN_TERM_ADDR:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnRemoteTunTermAddr;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnRemoteTunTermAddr *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnRemoteTunTermAddr),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnRemoteTunTermAddr.rval =
                nmhTestv2FsVpnRemoteTunTermAddr (lv.
                                                 nmhTestv2FsVpnRemoteTunTermAddr.
                                                 pu4ErrorCode,
                                                 lv.
                                                 nmhTestv2FsVpnRemoteTunTermAddr.
                                                 pFsVpnPolicyName,
                                                 lv.
                                                 nmhTestv2FsVpnRemoteTunTermAddr.
                                                 pTestValFsVpnRemoteTunTermAddr);
            lv.nmhTestv2FsVpnRemoteTunTermAddr.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnRemoteTunTermAddr *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnRemoteTunTermAddr;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnRemoteTunTermAddr)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_PROTECT_NETWORK_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnProtectNetworkType;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnProtectNetworkType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnProtectNetworkType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnProtectNetworkType.rval =
                nmhTestv2FsVpnProtectNetworkType (lv.
                                                  nmhTestv2FsVpnProtectNetworkType.
                                                  pu4ErrorCode,
                                                  lv.
                                                  nmhTestv2FsVpnProtectNetworkType.
                                                  pFsVpnPolicyName,
                                                  lv.
                                                  nmhTestv2FsVpnProtectNetworkType.
                                                  i4TestValFsVpnProtectNetworkType);
            lv.nmhTestv2FsVpnProtectNetworkType.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnProtectNetworkType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnProtectNetworkType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnProtectNetworkType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_LOCAL_PROTECT_NETWORK:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnLocalProtectNetwork;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnLocalProtectNetwork *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnLocalProtectNetwork),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnLocalProtectNetwork.rval =
                nmhTestv2FsVpnLocalProtectNetwork (lv.
                                                   nmhTestv2FsVpnLocalProtectNetwork.
                                                   pu4ErrorCode,
                                                   lv.
                                                   nmhTestv2FsVpnLocalProtectNetwork.
                                                   pFsVpnPolicyName,
                                                   lv.
                                                   nmhTestv2FsVpnLocalProtectNetwork.
                                                   pTestValFsVpnLocalProtectNetwork);
            lv.nmhTestv2FsVpnLocalProtectNetwork.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnLocalProtectNetwork *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnLocalProtectNetwork;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnLocalProtectNetwork)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_LOCAL_PROTECT_NETWORK_PREFIX_LEN:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhTestv2FsVpnLocalProtectNetworkPrefixLen;
            OsixKerUseInfo.pSrc =
                (tNpwnmhTestv2FsVpnLocalProtectNetworkPrefixLen *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhTestv2FsVpnLocalProtectNetworkPrefixLen),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnLocalProtectNetworkPrefixLen.rval =
                nmhTestv2FsVpnLocalProtectNetworkPrefixLen (lv.
                                                            nmhTestv2FsVpnLocalProtectNetworkPrefixLen.
                                                            pu4ErrorCode,
                                                            lv.
                                                            nmhTestv2FsVpnLocalProtectNetworkPrefixLen.
                                                            pFsVpnPolicyName,
                                                            lv.
                                                            nmhTestv2FsVpnLocalProtectNetworkPrefixLen.
                                                            u4TestValFsVpnLocalProtectNetworkPrefixLen);
            lv.nmhTestv2FsVpnLocalProtectNetworkPrefixLen.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhTestv2FsVpnLocalProtectNetworkPrefixLen *) p;
            OsixKerUseInfo.pSrc =
                &lv.nmhTestv2FsVpnLocalProtectNetworkPrefixLen;
            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhTestv2FsVpnLocalProtectNetworkPrefixLen)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_REMOTE_PROTECT_NETWORK:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnRemoteProtectNetwork;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnRemoteProtectNetwork *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnRemoteProtectNetwork),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnRemoteProtectNetwork.rval =
                nmhTestv2FsVpnRemoteProtectNetwork (lv.
                                                    nmhTestv2FsVpnRemoteProtectNetwork.
                                                    pu4ErrorCode,
                                                    lv.
                                                    nmhTestv2FsVpnRemoteProtectNetwork.
                                                    pFsVpnPolicyName,
                                                    lv.
                                                    nmhTestv2FsVpnRemoteProtectNetwork.
                                                    pTestValFsVpnRemoteProtectNetwork);
            lv.nmhTestv2FsVpnRemoteProtectNetwork.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnRemoteProtectNetwork *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnRemoteProtectNetwork;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnRemoteProtectNetwork)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_REMOTE_PROTECT_NETWORK_PREFIX_LEN:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhTestv2FsVpnRemoteProtectNetworkPrefixLen;
            OsixKerUseInfo.pSrc =
                (tNpwnmhTestv2FsVpnRemoteProtectNetworkPrefixLen *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhTestv2FsVpnRemoteProtectNetworkPrefixLen),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnRemoteProtectNetworkPrefixLen.rval =
                nmhTestv2FsVpnRemoteProtectNetworkPrefixLen (lv.
                                                             nmhTestv2FsVpnRemoteProtectNetworkPrefixLen.
                                                             pu4ErrorCode,
                                                             lv.
                                                             nmhTestv2FsVpnRemoteProtectNetworkPrefixLen.
                                                             pFsVpnPolicyName,
                                                             lv.
                                                             nmhTestv2FsVpnRemoteProtectNetworkPrefixLen.
                                                             u4TestValFsVpnRemoteProtectNetworkPrefixLen);
            lv.nmhTestv2FsVpnRemoteProtectNetworkPrefixLen.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhTestv2FsVpnRemoteProtectNetworkPrefixLen *) p;
            OsixKerUseInfo.pSrc =
                &lv.nmhTestv2FsVpnRemoteProtectNetworkPrefixLen;
            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhTestv2FsVpnRemoteProtectNetworkPrefixLen)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_IKE_SRC_PORT_RANGE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnIkeSrcPortRange;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnIkeSrcPortRange *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkeSrcPortRange),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnIkeSrcPortRange.rval =
                nmhTestv2FsVpnIkeSrcPortRange (lv.nmhTestv2FsVpnIkeSrcPortRange.
                                               pu4ErrorCode,
                                               lv.nmhTestv2FsVpnIkeSrcPortRange.
                                               pFsVpnPolicyName,
                                               lv.nmhTestv2FsVpnIkeSrcPortRange.
                                               pTestValFsVpnIkeSrcPortRange);
            lv.nmhTestv2FsVpnIkeSrcPortRange.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnIkeSrcPortRange *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnIkeSrcPortRange;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkeSrcPortRange)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_IKE_DST_PORT_RANGE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnIkeDstPortRange;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnIkeDstPortRange *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkeDstPortRange),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnIkeDstPortRange.rval =
                nmhTestv2FsVpnIkeDstPortRange (lv.nmhTestv2FsVpnIkeDstPortRange.
                                               pu4ErrorCode,
                                               lv.nmhTestv2FsVpnIkeDstPortRange.
                                               pFsVpnPolicyName,
                                               lv.nmhTestv2FsVpnIkeDstPortRange.
                                               pTestValFsVpnIkeDstPortRange);
            lv.nmhTestv2FsVpnIkeDstPortRange.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnIkeDstPortRange *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnIkeDstPortRange;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkeDstPortRange)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_SECURITY_PROTOCOL:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnSecurityProtocol;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnSecurityProtocol *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnSecurityProtocol),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnSecurityProtocol.rval =
                nmhTestv2FsVpnSecurityProtocol (lv.
                                                nmhTestv2FsVpnSecurityProtocol.
                                                pu4ErrorCode,
                                                lv.
                                                nmhTestv2FsVpnSecurityProtocol.
                                                pFsVpnPolicyName,
                                                lv.
                                                nmhTestv2FsVpnSecurityProtocol.
                                                i4TestValFsVpnSecurityProtocol);
            lv.nmhTestv2FsVpnSecurityProtocol.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnSecurityProtocol *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnSecurityProtocol;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnSecurityProtocol)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_INBOUND_SPI:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnInboundSpi;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnInboundSpi *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnInboundSpi),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnInboundSpi.rval =
                nmhTestv2FsVpnInboundSpi (lv.nmhTestv2FsVpnInboundSpi.
                                          pu4ErrorCode,
                                          lv.nmhTestv2FsVpnInboundSpi.
                                          pFsVpnPolicyName,
                                          lv.nmhTestv2FsVpnInboundSpi.
                                          i4TestValFsVpnInboundSpi);
            lv.nmhTestv2FsVpnInboundSpi.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnInboundSpi *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnInboundSpi;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnInboundSpi)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_OUTBOUND_SPI:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnOutboundSpi;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnOutboundSpi *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnOutboundSpi),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnOutboundSpi.rval =
                nmhTestv2FsVpnOutboundSpi (lv.nmhTestv2FsVpnOutboundSpi.
                                           pu4ErrorCode,
                                           lv.nmhTestv2FsVpnOutboundSpi.
                                           pFsVpnPolicyName,
                                           lv.nmhTestv2FsVpnOutboundSpi.
                                           i4TestValFsVpnOutboundSpi);
            lv.nmhTestv2FsVpnOutboundSpi.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnOutboundSpi *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnOutboundSpi;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnOutboundSpi)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_MODE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnMode;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnMode *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnMode),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnMode.rval =
                nmhTestv2FsVpnMode (lv.nmhTestv2FsVpnMode.pu4ErrorCode,
                                    lv.nmhTestv2FsVpnMode.pFsVpnPolicyName,
                                    lv.nmhTestv2FsVpnMode.i4TestValFsVpnMode);
            lv.nmhTestv2FsVpnMode.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnMode *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnMode;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnMode)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_AUTH_ALGO:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnAuthAlgo;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnAuthAlgo *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnAuthAlgo),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnAuthAlgo.rval =
                nmhTestv2FsVpnAuthAlgo (lv.nmhTestv2FsVpnAuthAlgo.pu4ErrorCode,
                                        lv.nmhTestv2FsVpnAuthAlgo.
                                        pFsVpnPolicyName,
                                        lv.nmhTestv2FsVpnAuthAlgo.
                                        i4TestValFsVpnAuthAlgo);
            lv.nmhTestv2FsVpnAuthAlgo.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnAuthAlgo *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnAuthAlgo;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnAuthAlgo)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_AH_KEY:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnAhKey;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnAhKey *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnAhKey),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnAhKey.rval =
                nmhTestv2FsVpnAhKey (lv.nmhTestv2FsVpnAhKey.pu4ErrorCode,
                                     lv.nmhTestv2FsVpnAhKey.pFsVpnPolicyName,
                                     lv.nmhTestv2FsVpnAhKey.pTestValFsVpnAhKey);
            lv.nmhTestv2FsVpnAhKey.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnAhKey *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnAhKey;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnAhKey)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_ENCR_ALGO:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnEncrAlgo;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnEncrAlgo *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnEncrAlgo),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnEncrAlgo.rval =
                nmhTestv2FsVpnEncrAlgo (lv.nmhTestv2FsVpnEncrAlgo.pu4ErrorCode,
                                        lv.nmhTestv2FsVpnEncrAlgo.
                                        pFsVpnPolicyName,
                                        lv.nmhTestv2FsVpnEncrAlgo.
                                        i4TestValFsVpnEncrAlgo);
            lv.nmhTestv2FsVpnEncrAlgo.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnEncrAlgo *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnEncrAlgo;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnEncrAlgo)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_ESP_KEY:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnEspKey;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnEspKey *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnEspKey),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnEspKey.rval =
                nmhTestv2FsVpnEspKey (lv.nmhTestv2FsVpnEspKey.pu4ErrorCode,
                                      lv.nmhTestv2FsVpnEspKey.pFsVpnPolicyName,
                                      lv.nmhTestv2FsVpnEspKey.
                                      pTestValFsVpnEspKey);
            lv.nmhTestv2FsVpnEspKey.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnEspKey *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnEspKey;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnEspKey)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_ANTI_REPLAY:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnAntiReplay;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnAntiReplay *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnAntiReplay),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnAntiReplay.rval =
                nmhTestv2FsVpnAntiReplay (lv.nmhTestv2FsVpnAntiReplay.
                                          pu4ErrorCode,
                                          lv.nmhTestv2FsVpnAntiReplay.
                                          pFsVpnPolicyName,
                                          lv.nmhTestv2FsVpnAntiReplay.
                                          i4TestValFsVpnAntiReplay);
            lv.nmhTestv2FsVpnAntiReplay.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnAntiReplay *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnAntiReplay;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnAntiReplay)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_POLICY_FLAG:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnPolicyFlag;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnPolicyFlag *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnPolicyFlag),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnPolicyFlag.rval =
                nmhTestv2FsVpnPolicyFlag (lv.nmhTestv2FsVpnPolicyFlag.
                                          pu4ErrorCode,
                                          lv.nmhTestv2FsVpnPolicyFlag.
                                          pFsVpnPolicyName,
                                          lv.nmhTestv2FsVpnPolicyFlag.
                                          i4TestValFsVpnPolicyFlag);
            lv.nmhTestv2FsVpnPolicyFlag.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnPolicyFlag *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnPolicyFlag;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnPolicyFlag)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_PROTOCOL:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnProtocol;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnProtocol *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnProtocol),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnProtocol.rval =
                nmhTestv2FsVpnProtocol (lv.nmhTestv2FsVpnProtocol.pu4ErrorCode,
                                        lv.nmhTestv2FsVpnProtocol.
                                        pFsVpnPolicyName,
                                        lv.nmhTestv2FsVpnProtocol.
                                        i4TestValFsVpnProtocol);
            lv.nmhTestv2FsVpnProtocol.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnProtocol *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnProtocol;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnProtocol)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_POLICY_INTF_INDEX:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnPolicyIntfIndex;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnPolicyIntfIndex *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnPolicyIntfIndex),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnPolicyIntfIndex.rval =
                nmhTestv2FsVpnPolicyIntfIndex (lv.nmhTestv2FsVpnPolicyIntfIndex.
                                               pu4ErrorCode,
                                               lv.nmhTestv2FsVpnPolicyIntfIndex.
                                               pFsVpnPolicyName,
                                               lv.nmhTestv2FsVpnPolicyIntfIndex.
                                               i4TestValFsVpnPolicyIntfIndex);
            lv.nmhTestv2FsVpnPolicyIntfIndex.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnPolicyIntfIndex *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnPolicyIntfIndex;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnPolicyIntfIndex)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_IKE_PHASE1_HASH_ALGO:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnIkePhase1HashAlgo;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnIkePhase1HashAlgo *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase1HashAlgo),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnIkePhase1HashAlgo.rval =
                nmhTestv2FsVpnIkePhase1HashAlgo (lv.
                                                 nmhTestv2FsVpnIkePhase1HashAlgo.
                                                 pu4ErrorCode,
                                                 lv.
                                                 nmhTestv2FsVpnIkePhase1HashAlgo.
                                                 pFsVpnPolicyName,
                                                 lv.
                                                 nmhTestv2FsVpnIkePhase1HashAlgo.
                                                 i4TestValFsVpnIkePhase1HashAlgo);
            lv.nmhTestv2FsVpnIkePhase1HashAlgo.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnIkePhase1HashAlgo *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnIkePhase1HashAlgo;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase1HashAlgo)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_IKE_PHASE1_ENCRYPTION_ALGO:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnIkePhase1EncryptionAlgo;
            OsixKerUseInfo.pSrc =
                (tNpwnmhTestv2FsVpnIkePhase1EncryptionAlgo *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhTestv2FsVpnIkePhase1EncryptionAlgo),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnIkePhase1EncryptionAlgo.rval =
                nmhTestv2FsVpnIkePhase1EncryptionAlgo (lv.
                                                       nmhTestv2FsVpnIkePhase1EncryptionAlgo.
                                                       pu4ErrorCode,
                                                       lv.
                                                       nmhTestv2FsVpnIkePhase1EncryptionAlgo.
                                                       pFsVpnPolicyName,
                                                       lv.
                                                       nmhTestv2FsVpnIkePhase1EncryptionAlgo.
                                                       i4TestValFsVpnIkePhase1EncryptionAlgo);
            lv.nmhTestv2FsVpnIkePhase1EncryptionAlgo.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhTestv2FsVpnIkePhase1EncryptionAlgo *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnIkePhase1EncryptionAlgo;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase1EncryptionAlgo))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_IKE_PHASE1_D_H_GROUP:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnIkePhase1DHGroup;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnIkePhase1DHGroup *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase1DHGroup),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnIkePhase1DHGroup.rval =
                nmhTestv2FsVpnIkePhase1DHGroup (lv.
                                                nmhTestv2FsVpnIkePhase1DHGroup.
                                                pu4ErrorCode,
                                                lv.
                                                nmhTestv2FsVpnIkePhase1DHGroup.
                                                pFsVpnPolicyName,
                                                lv.
                                                nmhTestv2FsVpnIkePhase1DHGroup.
                                                i4TestValFsVpnIkePhase1DHGroup);
            lv.nmhTestv2FsVpnIkePhase1DHGroup.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnIkePhase1DHGroup *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnIkePhase1DHGroup;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase1DHGroup)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_IKE_PHASE1_LOCAL_ID_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnIkePhase1LocalIdType;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnIkePhase1LocalIdType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase1LocalIdType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnIkePhase1LocalIdType.rval =
                nmhTestv2FsVpnIkePhase1LocalIdType (lv.
                                                    nmhTestv2FsVpnIkePhase1LocalIdType.
                                                    pu4ErrorCode,
                                                    lv.
                                                    nmhTestv2FsVpnIkePhase1LocalIdType.
                                                    pFsVpnPolicyName,
                                                    lv.
                                                    nmhTestv2FsVpnIkePhase1LocalIdType.
                                                    i4TestValFsVpnIkePhase1LocalIdType);
            lv.nmhTestv2FsVpnIkePhase1LocalIdType.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnIkePhase1LocalIdType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnIkePhase1LocalIdType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase1LocalIdType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_IKE_PHASE1_LOCAL_ID_VALUE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnIkePhase1LocalIdValue;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnIkePhase1LocalIdValue *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase1LocalIdValue),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnIkePhase1LocalIdValue.rval =
                nmhTestv2FsVpnIkePhase1LocalIdValue (lv.
                                                     nmhTestv2FsVpnIkePhase1LocalIdValue.
                                                     pu4ErrorCode,
                                                     lv.
                                                     nmhTestv2FsVpnIkePhase1LocalIdValue.
                                                     pFsVpnPolicyName,
                                                     lv.
                                                     nmhTestv2FsVpnIkePhase1LocalIdValue.
                                                     pTestValFsVpnIkePhase1LocalIdValue);
            lv.nmhTestv2FsVpnIkePhase1LocalIdValue.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhTestv2FsVpnIkePhase1LocalIdValue *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnIkePhase1LocalIdValue;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase1LocalIdValue))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_IKE_PHASE1_PEER_ID_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnIkePhase1PeerIdType;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnIkePhase1PeerIdType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase1PeerIdType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnIkePhase1PeerIdType.rval =
                nmhTestv2FsVpnIkePhase1PeerIdType (lv.
                                                   nmhTestv2FsVpnIkePhase1PeerIdType.
                                                   pu4ErrorCode,
                                                   lv.
                                                   nmhTestv2FsVpnIkePhase1PeerIdType.
                                                   pFsVpnPolicyName,
                                                   lv.
                                                   nmhTestv2FsVpnIkePhase1PeerIdType.
                                                   i4TestValFsVpnIkePhase1PeerIdType);
            lv.nmhTestv2FsVpnIkePhase1PeerIdType.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnIkePhase1PeerIdType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnIkePhase1PeerIdType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase1PeerIdType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_IKE_PHASE1_PEER_ID_VALUE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnIkePhase1PeerIdValue;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnIkePhase1PeerIdValue *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase1PeerIdValue),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnIkePhase1PeerIdValue.rval =
                nmhTestv2FsVpnIkePhase1PeerIdValue (lv.
                                                    nmhTestv2FsVpnIkePhase1PeerIdValue.
                                                    pu4ErrorCode,
                                                    lv.
                                                    nmhTestv2FsVpnIkePhase1PeerIdValue.
                                                    pFsVpnPolicyName,
                                                    lv.
                                                    nmhTestv2FsVpnIkePhase1PeerIdValue.
                                                    pTestValFsVpnIkePhase1PeerIdValue);
            lv.nmhTestv2FsVpnIkePhase1PeerIdValue.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnIkePhase1PeerIdValue *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnIkePhase1PeerIdValue;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase1PeerIdValue)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_IKE_PHASE1_LIFE_TIME_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnIkePhase1LifeTimeType;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnIkePhase1LifeTimeType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase1LifeTimeType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnIkePhase1LifeTimeType.rval =
                nmhTestv2FsVpnIkePhase1LifeTimeType (lv.
                                                     nmhTestv2FsVpnIkePhase1LifeTimeType.
                                                     pu4ErrorCode,
                                                     lv.
                                                     nmhTestv2FsVpnIkePhase1LifeTimeType.
                                                     pFsVpnPolicyName,
                                                     lv.
                                                     nmhTestv2FsVpnIkePhase1LifeTimeType.
                                                     i4TestValFsVpnIkePhase1LifeTimeType);
            lv.nmhTestv2FsVpnIkePhase1LifeTimeType.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhTestv2FsVpnIkePhase1LifeTimeType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnIkePhase1LifeTimeType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase1LifeTimeType))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_IKE_PHASE1_LIFE_TIME:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnIkePhase1LifeTime;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnIkePhase1LifeTime *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase1LifeTime),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnIkePhase1LifeTime.rval =
                nmhTestv2FsVpnIkePhase1LifeTime (lv.
                                                 nmhTestv2FsVpnIkePhase1LifeTime.
                                                 pu4ErrorCode,
                                                 lv.
                                                 nmhTestv2FsVpnIkePhase1LifeTime.
                                                 pFsVpnPolicyName,
                                                 lv.
                                                 nmhTestv2FsVpnIkePhase1LifeTime.
                                                 i4TestValFsVpnIkePhase1LifeTime);
            lv.nmhTestv2FsVpnIkePhase1LifeTime.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnIkePhase1LifeTime *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnIkePhase1LifeTime;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase1LifeTime)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_IKE_PHASE1_MODE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnIkePhase1Mode;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnIkePhase1Mode *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase1Mode),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnIkePhase1Mode.rval =
                nmhTestv2FsVpnIkePhase1Mode (lv.nmhTestv2FsVpnIkePhase1Mode.
                                             pu4ErrorCode,
                                             lv.nmhTestv2FsVpnIkePhase1Mode.
                                             pFsVpnPolicyName,
                                             lv.nmhTestv2FsVpnIkePhase1Mode.
                                             i4TestValFsVpnIkePhase1Mode);
            lv.nmhTestv2FsVpnIkePhase1Mode.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnIkePhase1Mode *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnIkePhase1Mode;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase1Mode)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_IKE_PHASE2_AUTH_ALGO:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnIkePhase2AuthAlgo;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnIkePhase2AuthAlgo *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase2AuthAlgo),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnIkePhase2AuthAlgo.rval =
                nmhTestv2FsVpnIkePhase2AuthAlgo (lv.
                                                 nmhTestv2FsVpnIkePhase2AuthAlgo.
                                                 pu4ErrorCode,
                                                 lv.
                                                 nmhTestv2FsVpnIkePhase2AuthAlgo.
                                                 pFsVpnPolicyName,
                                                 lv.
                                                 nmhTestv2FsVpnIkePhase2AuthAlgo.
                                                 i4TestValFsVpnIkePhase2AuthAlgo);
            lv.nmhTestv2FsVpnIkePhase2AuthAlgo.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnIkePhase2AuthAlgo *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnIkePhase2AuthAlgo;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase2AuthAlgo)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_IKE_PHASE2_ESP_ENCRYPTION_ALGO:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnIkePhase2EspEncryptionAlgo;
            OsixKerUseInfo.pSrc =
                (tNpwnmhTestv2FsVpnIkePhase2EspEncryptionAlgo *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhTestv2FsVpnIkePhase2EspEncryptionAlgo),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnIkePhase2EspEncryptionAlgo.rval =
                nmhTestv2FsVpnIkePhase2EspEncryptionAlgo (lv.
                                                          nmhTestv2FsVpnIkePhase2EspEncryptionAlgo.
                                                          pu4ErrorCode,
                                                          lv.
                                                          nmhTestv2FsVpnIkePhase2EspEncryptionAlgo.
                                                          pFsVpnPolicyName,
                                                          lv.
                                                          nmhTestv2FsVpnIkePhase2EspEncryptionAlgo.
                                                          i4TestValFsVpnIkePhase2EspEncryptionAlgo);
            lv.nmhTestv2FsVpnIkePhase2EspEncryptionAlgo.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhTestv2FsVpnIkePhase2EspEncryptionAlgo *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnIkePhase2EspEncryptionAlgo;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tNpwnmhTestv2FsVpnIkePhase2EspEncryptionAlgo)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_IKE_PHASE2_LIFE_TIME_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnIkePhase2LifeTimeType;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnIkePhase2LifeTimeType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase2LifeTimeType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnIkePhase2LifeTimeType.rval =
                nmhTestv2FsVpnIkePhase2LifeTimeType (lv.
                                                     nmhTestv2FsVpnIkePhase2LifeTimeType.
                                                     pu4ErrorCode,
                                                     lv.
                                                     nmhTestv2FsVpnIkePhase2LifeTimeType.
                                                     pFsVpnPolicyName,
                                                     lv.
                                                     nmhTestv2FsVpnIkePhase2LifeTimeType.
                                                     i4TestValFsVpnIkePhase2LifeTimeType);
            lv.nmhTestv2FsVpnIkePhase2LifeTimeType.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhTestv2FsVpnIkePhase2LifeTimeType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnIkePhase2LifeTimeType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase2LifeTimeType))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_IKE_PHASE2_LIFE_TIME:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnIkePhase2LifeTime;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnIkePhase2LifeTime *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase2LifeTime),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnIkePhase2LifeTime.rval =
                nmhTestv2FsVpnIkePhase2LifeTime (lv.
                                                 nmhTestv2FsVpnIkePhase2LifeTime.
                                                 pu4ErrorCode,
                                                 lv.
                                                 nmhTestv2FsVpnIkePhase2LifeTime.
                                                 pFsVpnPolicyName,
                                                 lv.
                                                 nmhTestv2FsVpnIkePhase2LifeTime.
                                                 i4TestValFsVpnIkePhase2LifeTime);
            lv.nmhTestv2FsVpnIkePhase2LifeTime.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnIkePhase2LifeTime *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnIkePhase2LifeTime;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase2LifeTime)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_IKE_PHASE2_D_H_GROUP:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnIkePhase2DHGroup;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnIkePhase2DHGroup *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase2DHGroup),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnIkePhase2DHGroup.rval =
                nmhTestv2FsVpnIkePhase2DHGroup (lv.
                                                nmhTestv2FsVpnIkePhase2DHGroup.
                                                pu4ErrorCode,
                                                lv.
                                                nmhTestv2FsVpnIkePhase2DHGroup.
                                                pFsVpnPolicyName,
                                                lv.
                                                nmhTestv2FsVpnIkePhase2DHGroup.
                                                i4TestValFsVpnIkePhase2DHGroup);
            lv.nmhTestv2FsVpnIkePhase2DHGroup.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnIkePhase2DHGroup *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnIkePhase2DHGroup;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkePhase2DHGroup)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_IKE_VERSION:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnIkeVersion;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnIkeVersion *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkeVersion),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnIkeVersion.rval =
                nmhTestv2FsVpnIkeVersion (lv.nmhTestv2FsVpnIkeVersion.
                                          pu4ErrorCode,
                                          lv.nmhTestv2FsVpnIkeVersion.
                                          pFsVpnPolicyName,
                                          lv.nmhTestv2FsVpnIkeVersion.
                                          i4TestValFsVpnIkeVersion);
            lv.nmhTestv2FsVpnIkeVersion.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnIkeVersion *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnIkeVersion;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnIkeVersion)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_POLICY_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnPolicyRowStatus;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnPolicyRowStatus *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnPolicyRowStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnPolicyRowStatus.rval =
                nmhTestv2FsVpnPolicyRowStatus (lv.nmhTestv2FsVpnPolicyRowStatus.
                                               pu4ErrorCode,
                                               lv.nmhTestv2FsVpnPolicyRowStatus.
                                               pFsVpnPolicyName,
                                               lv.nmhTestv2FsVpnPolicyRowStatus.
                                               i4TestValFsVpnPolicyRowStatus);
            lv.nmhTestv2FsVpnPolicyRowStatus.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnPolicyRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnPolicyRowStatus;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnPolicyRowStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_FS_VPN_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2FsVpnTable;
            OsixKerUseInfo.pSrc = (tNpwnmhDepv2FsVpnTable *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhDepv2FsVpnTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhDepv2FsVpnTable.rval =
                nmhDepv2FsVpnTable (lv.nmhDepv2FsVpnTable.pu4ErrorCode,
                                    lv.nmhDepv2FsVpnTable.pSnmpIndexList,
                                    lv.nmhDepv2FsVpnTable.pSnmpVarBind);
            lv.nmhDepv2FsVpnTable.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhDepv2FsVpnTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhDepv2FsVpnTable;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhDepv2FsVpnTable)) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_FS_VPN_RA_USERS_TABLE:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhValidateIndexInstanceFsVpnRaUsersTable;
            OsixKerUseInfo.pSrc =
                (tNpwnmhValidateIndexInstanceFsVpnRaUsersTable *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhValidateIndexInstanceFsVpnRaUsersTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhValidateIndexInstanceFsVpnRaUsersTable.rval =
                nmhValidateIndexInstanceFsVpnRaUsersTable (lv.
                                                           nmhValidateIndexInstanceFsVpnRaUsersTable.
                                                           pFsVpnRaUserName);
            lv.nmhValidateIndexInstanceFsVpnRaUsersTable.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhValidateIndexInstanceFsVpnRaUsersTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhValidateIndexInstanceFsVpnRaUsersTable;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tNpwnmhValidateIndexInstanceFsVpnRaUsersTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_FS_VPN_RA_USERS_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexFsVpnRaUsersTable;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFirstIndexFsVpnRaUsersTable *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFirstIndexFsVpnRaUsersTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFirstIndexFsVpnRaUsersTable.rval =
                nmhGetFirstIndexFsVpnRaUsersTable (lv.
                                                   nmhGetFirstIndexFsVpnRaUsersTable.
                                                   pFsVpnRaUserName);
            lv.nmhGetFirstIndexFsVpnRaUsersTable.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFirstIndexFsVpnRaUsersTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexFsVpnRaUsersTable;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFirstIndexFsVpnRaUsersTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_FS_VPN_RA_USERS_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexFsVpnRaUsersTable;
            OsixKerUseInfo.pSrc = (tNpwnmhGetNextIndexFsVpnRaUsersTable *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetNextIndexFsVpnRaUsersTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetNextIndexFsVpnRaUsersTable.rval =
                nmhGetNextIndexFsVpnRaUsersTable (lv.
                                                  nmhGetNextIndexFsVpnRaUsersTable.
                                                  pFsVpnRaUserName,
                                                  lv.
                                                  nmhGetNextIndexFsVpnRaUsersTable.
                                                  pNextFsVpnRaUserName);
            lv.nmhGetNextIndexFsVpnRaUsersTable.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetNextIndexFsVpnRaUsersTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexFsVpnRaUsersTable;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetNextIndexFsVpnRaUsersTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_RA_USER_SECRET:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnRaUserSecret;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnRaUserSecret *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRaUserSecret),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnRaUserSecret.rval =
                nmhGetFsVpnRaUserSecret (lv.nmhGetFsVpnRaUserSecret.
                                         pFsVpnRaUserName,
                                         lv.nmhGetFsVpnRaUserSecret.
                                         pRetValFsVpnRaUserSecret);
            lv.nmhGetFsVpnRaUserSecret.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnRaUserSecret *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnRaUserSecret;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRaUserSecret)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_RA_USER_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnRaUserRowStatus;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnRaUserRowStatus *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRaUserRowStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnRaUserRowStatus.rval =
                nmhGetFsVpnRaUserRowStatus (lv.nmhGetFsVpnRaUserRowStatus.
                                            pFsVpnRaUserName,
                                            lv.nmhGetFsVpnRaUserRowStatus.
                                            pi4RetValFsVpnRaUserRowStatus);
            lv.nmhGetFsVpnRaUserRowStatus.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnRaUserRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnRaUserRowStatus;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRaUserRowStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_RA_USER_SECRET:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnRaUserSecret;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnRaUserSecret *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRaUserSecret),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnRaUserSecret.rval =
                nmhSetFsVpnRaUserSecret (lv.nmhSetFsVpnRaUserSecret.
                                         pFsVpnRaUserName,
                                         lv.nmhSetFsVpnRaUserSecret.
                                         pSetValFsVpnRaUserSecret);
            lv.nmhSetFsVpnRaUserSecret.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnRaUserSecret *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnRaUserSecret;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRaUserSecret)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_RA_USER_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnRaUserRowStatus;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnRaUserRowStatus *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRaUserRowStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnRaUserRowStatus.rval =
                nmhSetFsVpnRaUserRowStatus (lv.nmhSetFsVpnRaUserRowStatus.
                                            pFsVpnRaUserName,
                                            lv.nmhSetFsVpnRaUserRowStatus.
                                            i4SetValFsVpnRaUserRowStatus);
            lv.nmhSetFsVpnRaUserRowStatus.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnRaUserRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnRaUserRowStatus;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRaUserRowStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_RA_USER_SECRET:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnRaUserSecret;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnRaUserSecret *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnRaUserSecret),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnRaUserSecret.rval =
                nmhTestv2FsVpnRaUserSecret (lv.nmhTestv2FsVpnRaUserSecret.
                                            pu4ErrorCode,
                                            lv.nmhTestv2FsVpnRaUserSecret.
                                            pFsVpnRaUserName,
                                            lv.nmhTestv2FsVpnRaUserSecret.
                                            pTestValFsVpnRaUserSecret);
            lv.nmhTestv2FsVpnRaUserSecret.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnRaUserSecret *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnRaUserSecret;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnRaUserSecret)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_RA_USER_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnRaUserRowStatus;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnRaUserRowStatus *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnRaUserRowStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnRaUserRowStatus.rval =
                nmhTestv2FsVpnRaUserRowStatus (lv.nmhTestv2FsVpnRaUserRowStatus.
                                               pu4ErrorCode,
                                               lv.nmhTestv2FsVpnRaUserRowStatus.
                                               pFsVpnRaUserName,
                                               lv.nmhTestv2FsVpnRaUserRowStatus.
                                               i4TestValFsVpnRaUserRowStatus);
            lv.nmhTestv2FsVpnRaUserRowStatus.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnRaUserRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnRaUserRowStatus;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnRaUserRowStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_FS_VPN_RA_USERS_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2FsVpnRaUsersTable;
            OsixKerUseInfo.pSrc = (tNpwnmhDepv2FsVpnRaUsersTable *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhDepv2FsVpnRaUsersTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhDepv2FsVpnRaUsersTable.rval =
                nmhDepv2FsVpnRaUsersTable (lv.nmhDepv2FsVpnRaUsersTable.
                                           pu4ErrorCode,
                                           lv.nmhDepv2FsVpnRaUsersTable.
                                           pSnmpIndexList,
                                           lv.nmhDepv2FsVpnRaUsersTable.
                                           pSnmpVarBind);
            lv.nmhDepv2FsVpnRaUsersTable.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhDepv2FsVpnRaUsersTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhDepv2FsVpnRaUsersTable;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhDepv2FsVpnRaUsersTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_FS_VPN_RA_ADDRESS_POOL_TABLE:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhValidateIndexInstanceFsVpnRaAddressPoolTable;
            OsixKerUseInfo.pSrc =
                (tNpwnmhValidateIndexInstanceFsVpnRaAddressPoolTable *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhValidateIndexInstanceFsVpnRaAddressPoolTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhValidateIndexInstanceFsVpnRaAddressPoolTable.rval =
                nmhValidateIndexInstanceFsVpnRaAddressPoolTable (lv.
                                                                 nmhValidateIndexInstanceFsVpnRaAddressPoolTable.
                                                                 pFsVpnRaAddressPoolName);
            lv.nmhValidateIndexInstanceFsVpnRaAddressPoolTable.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhValidateIndexInstanceFsVpnRaAddressPoolTable *) p;
            OsixKerUseInfo.pSrc =
                &lv.nmhValidateIndexInstanceFsVpnRaAddressPoolTable;
            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhValidateIndexInstanceFsVpnRaAddressPoolTable))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_FS_VPN_RA_ADDRESS_POOL_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexFsVpnRaAddressPoolTable;
            OsixKerUseInfo.pSrc =
                (tNpwnmhGetFirstIndexFsVpnRaAddressPoolTable *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhGetFirstIndexFsVpnRaAddressPoolTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFirstIndexFsVpnRaAddressPoolTable.rval =
                nmhGetFirstIndexFsVpnRaAddressPoolTable (lv.
                                                         nmhGetFirstIndexFsVpnRaAddressPoolTable.
                                                         pFsVpnRaAddressPoolName);
            lv.nmhGetFirstIndexFsVpnRaAddressPoolTable.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhGetFirstIndexFsVpnRaAddressPoolTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexFsVpnRaAddressPoolTable;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tNpwnmhGetFirstIndexFsVpnRaAddressPoolTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_FS_VPN_RA_ADDRESS_POOL_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexFsVpnRaAddressPoolTable;
            OsixKerUseInfo.pSrc =
                (tNpwnmhGetNextIndexFsVpnRaAddressPoolTable *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhGetNextIndexFsVpnRaAddressPoolTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetNextIndexFsVpnRaAddressPoolTable.rval =
                nmhGetNextIndexFsVpnRaAddressPoolTable (lv.
                                                        nmhGetNextIndexFsVpnRaAddressPoolTable.
                                                        pFsVpnRaAddressPoolName,
                                                        lv.
                                                        nmhGetNextIndexFsVpnRaAddressPoolTable.
                                                        pNextFsVpnRaAddressPoolName);
            lv.nmhGetNextIndexFsVpnRaAddressPoolTable.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhGetNextIndexFsVpnRaAddressPoolTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexFsVpnRaAddressPoolTable;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof
                             (tNpwnmhGetNextIndexFsVpnRaAddressPoolTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_RA_ADDRESS_POOL_ADDR_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnRaAddressPoolAddrType;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnRaAddressPoolAddrType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRaAddressPoolAddrType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnRaAddressPoolAddrType.rval =
                nmhGetFsVpnRaAddressPoolAddrType (lv.
                                                  nmhGetFsVpnRaAddressPoolAddrType.
                                                  pFsVpnRaAddressPoolName,
                                                  lv.
                                                  nmhGetFsVpnRaAddressPoolAddrType.
                                                  pi4RetValFsVpnRaAddressPoolAddrType);
            lv.nmhGetFsVpnRaAddressPoolAddrType.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnRaAddressPoolAddrType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnRaAddressPoolAddrType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRaAddressPoolAddrType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_RA_ADDRESS_POOL_START:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnRaAddressPoolStart;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnRaAddressPoolStart *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRaAddressPoolStart),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnRaAddressPoolStart.rval =
                nmhGetFsVpnRaAddressPoolStart (lv.nmhGetFsVpnRaAddressPoolStart.
                                               pFsVpnRaAddressPoolName,
                                               lv.nmhGetFsVpnRaAddressPoolStart.
                                               pRetValFsVpnRaAddressPoolStart);
            lv.nmhGetFsVpnRaAddressPoolStart.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnRaAddressPoolStart *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnRaAddressPoolStart;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRaAddressPoolStart)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_RA_ADDRESS_POOL_END:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnRaAddressPoolEnd;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnRaAddressPoolEnd *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRaAddressPoolEnd),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnRaAddressPoolEnd.rval =
                nmhGetFsVpnRaAddressPoolEnd (lv.nmhGetFsVpnRaAddressPoolEnd.
                                             pFsVpnRaAddressPoolName,
                                             lv.nmhGetFsVpnRaAddressPoolEnd.
                                             pRetValFsVpnRaAddressPoolEnd);
            lv.nmhGetFsVpnRaAddressPoolEnd.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnRaAddressPoolEnd *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnRaAddressPoolEnd;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRaAddressPoolEnd)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_RA_ADDRESS_POOL_PREFIX_LEN:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnRaAddressPoolPrefixLen;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnRaAddressPoolPrefixLen *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRaAddressPoolPrefixLen),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnRaAddressPoolPrefixLen.rval =
                nmhGetFsVpnRaAddressPoolPrefixLen (lv.
                                                   nmhGetFsVpnRaAddressPoolPrefixLen.
                                                   pFsVpnRaAddressPoolName,
                                                   lv.
                                                   nmhGetFsVpnRaAddressPoolPrefixLen.
                                                   pu4RetValFsVpnRaAddressPoolPrefixLen);
            lv.nmhGetFsVpnRaAddressPoolPrefixLen.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnRaAddressPoolPrefixLen *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnRaAddressPoolPrefixLen;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRaAddressPoolPrefixLen)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_RA_ADDRESS_POOL_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnRaAddressPoolRowStatus;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnRaAddressPoolRowStatus *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRaAddressPoolRowStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnRaAddressPoolRowStatus.rval =
                nmhGetFsVpnRaAddressPoolRowStatus (lv.
                                                   nmhGetFsVpnRaAddressPoolRowStatus.
                                                   pFsVpnRaAddressPoolName,
                                                   lv.
                                                   nmhGetFsVpnRaAddressPoolRowStatus.
                                                   pi4RetValFsVpnRaAddressPoolRowStatus);
            lv.nmhGetFsVpnRaAddressPoolRowStatus.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnRaAddressPoolRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnRaAddressPoolRowStatus;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRaAddressPoolRowStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_RA_ADDRESS_POOL_ADDR_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnRaAddressPoolAddrType;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnRaAddressPoolAddrType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRaAddressPoolAddrType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnRaAddressPoolAddrType.rval =
                nmhSetFsVpnRaAddressPoolAddrType (lv.
                                                  nmhSetFsVpnRaAddressPoolAddrType.
                                                  pFsVpnRaAddressPoolName,
                                                  lv.
                                                  nmhSetFsVpnRaAddressPoolAddrType.
                                                  i4SetValFsVpnRaAddressPoolAddrType);
            lv.nmhSetFsVpnRaAddressPoolAddrType.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnRaAddressPoolAddrType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnRaAddressPoolAddrType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRaAddressPoolAddrType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_RA_ADDRESS_POOL_START:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnRaAddressPoolStart;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnRaAddressPoolStart *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRaAddressPoolStart),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnRaAddressPoolStart.rval =
                nmhSetFsVpnRaAddressPoolStart (lv.nmhSetFsVpnRaAddressPoolStart.
                                               pFsVpnRaAddressPoolName,
                                               lv.nmhSetFsVpnRaAddressPoolStart.
                                               pSetValFsVpnRaAddressPoolStart);
            lv.nmhSetFsVpnRaAddressPoolStart.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnRaAddressPoolStart *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnRaAddressPoolStart;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRaAddressPoolStart)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_RA_ADDRESS_POOL_END:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnRaAddressPoolEnd;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnRaAddressPoolEnd *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRaAddressPoolEnd),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnRaAddressPoolEnd.rval =
                nmhSetFsVpnRaAddressPoolEnd (lv.nmhSetFsVpnRaAddressPoolEnd.
                                             pFsVpnRaAddressPoolName,
                                             lv.nmhSetFsVpnRaAddressPoolEnd.
                                             pSetValFsVpnRaAddressPoolEnd);
            lv.nmhSetFsVpnRaAddressPoolEnd.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnRaAddressPoolEnd *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnRaAddressPoolEnd;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRaAddressPoolEnd)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_RA_ADDRESS_POOL_PREFIX_LEN:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnRaAddressPoolPrefixLen;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnRaAddressPoolPrefixLen *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRaAddressPoolPrefixLen),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnRaAddressPoolPrefixLen.rval =
                nmhSetFsVpnRaAddressPoolPrefixLen (lv.
                                                   nmhSetFsVpnRaAddressPoolPrefixLen.
                                                   pFsVpnRaAddressPoolName,
                                                   lv.
                                                   nmhSetFsVpnRaAddressPoolPrefixLen.
                                                   u4SetValFsVpnRaAddressPoolPrefixLen);
            lv.nmhSetFsVpnRaAddressPoolPrefixLen.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnRaAddressPoolPrefixLen *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnRaAddressPoolPrefixLen;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRaAddressPoolPrefixLen)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_RA_ADDRESS_POOL_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnRaAddressPoolRowStatus;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnRaAddressPoolRowStatus *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRaAddressPoolRowStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnRaAddressPoolRowStatus.rval =
                nmhSetFsVpnRaAddressPoolRowStatus (lv.
                                                   nmhSetFsVpnRaAddressPoolRowStatus.
                                                   pFsVpnRaAddressPoolName,
                                                   lv.
                                                   nmhSetFsVpnRaAddressPoolRowStatus.
                                                   i4SetValFsVpnRaAddressPoolRowStatus);
            lv.nmhSetFsVpnRaAddressPoolRowStatus.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnRaAddressPoolRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnRaAddressPoolRowStatus;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRaAddressPoolRowStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_RA_ADDRESS_POOL_ADDR_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnRaAddressPoolAddrType;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnRaAddressPoolAddrType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnRaAddressPoolAddrType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnRaAddressPoolAddrType.rval =
                nmhTestv2FsVpnRaAddressPoolAddrType (lv.
                                                     nmhTestv2FsVpnRaAddressPoolAddrType.
                                                     pu4ErrorCode,
                                                     lv.
                                                     nmhTestv2FsVpnRaAddressPoolAddrType.
                                                     pFsVpnRaAddressPoolName,
                                                     lv.
                                                     nmhTestv2FsVpnRaAddressPoolAddrType.
                                                     i4TestValFsVpnRaAddressPoolAddrType);
            lv.nmhTestv2FsVpnRaAddressPoolAddrType.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhTestv2FsVpnRaAddressPoolAddrType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnRaAddressPoolAddrType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnRaAddressPoolAddrType))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_RA_ADDRESS_POOL_START:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnRaAddressPoolStart;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnRaAddressPoolStart *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnRaAddressPoolStart),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnRaAddressPoolStart.rval =
                nmhTestv2FsVpnRaAddressPoolStart (lv.
                                                  nmhTestv2FsVpnRaAddressPoolStart.
                                                  pu4ErrorCode,
                                                  lv.
                                                  nmhTestv2FsVpnRaAddressPoolStart.
                                                  pFsVpnRaAddressPoolName,
                                                  lv.
                                                  nmhTestv2FsVpnRaAddressPoolStart.
                                                  pTestValFsVpnRaAddressPoolStart);
            lv.nmhTestv2FsVpnRaAddressPoolStart.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnRaAddressPoolStart *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnRaAddressPoolStart;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnRaAddressPoolStart)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_RA_ADDRESS_POOL_END:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnRaAddressPoolEnd;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnRaAddressPoolEnd *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnRaAddressPoolEnd),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnRaAddressPoolEnd.rval =
                nmhTestv2FsVpnRaAddressPoolEnd (lv.
                                                nmhTestv2FsVpnRaAddressPoolEnd.
                                                pu4ErrorCode,
                                                lv.
                                                nmhTestv2FsVpnRaAddressPoolEnd.
                                                pFsVpnRaAddressPoolName,
                                                lv.
                                                nmhTestv2FsVpnRaAddressPoolEnd.
                                                pTestValFsVpnRaAddressPoolEnd);
            lv.nmhTestv2FsVpnRaAddressPoolEnd.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnRaAddressPoolEnd *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnRaAddressPoolEnd;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnRaAddressPoolEnd)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_RA_ADDRESS_POOL_PREFIX_LEN:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnRaAddressPoolPrefixLen;
            OsixKerUseInfo.pSrc =
                (tNpwnmhTestv2FsVpnRaAddressPoolPrefixLen *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhTestv2FsVpnRaAddressPoolPrefixLen),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnRaAddressPoolPrefixLen.rval =
                nmhTestv2FsVpnRaAddressPoolPrefixLen (lv.
                                                      nmhTestv2FsVpnRaAddressPoolPrefixLen.
                                                      pu4ErrorCode,
                                                      lv.
                                                      nmhTestv2FsVpnRaAddressPoolPrefixLen.
                                                      pFsVpnRaAddressPoolName,
                                                      lv.
                                                      nmhTestv2FsVpnRaAddressPoolPrefixLen.
                                                      u4TestValFsVpnRaAddressPoolPrefixLen);
            lv.nmhTestv2FsVpnRaAddressPoolPrefixLen.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhTestv2FsVpnRaAddressPoolPrefixLen *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnRaAddressPoolPrefixLen;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnRaAddressPoolPrefixLen))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_RA_ADDRESS_POOL_ROW_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnRaAddressPoolRowStatus;
            OsixKerUseInfo.pSrc =
                (tNpwnmhTestv2FsVpnRaAddressPoolRowStatus *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhTestv2FsVpnRaAddressPoolRowStatus),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnRaAddressPoolRowStatus.rval =
                nmhTestv2FsVpnRaAddressPoolRowStatus (lv.
                                                      nmhTestv2FsVpnRaAddressPoolRowStatus.
                                                      pu4ErrorCode,
                                                      lv.
                                                      nmhTestv2FsVpnRaAddressPoolRowStatus.
                                                      pFsVpnRaAddressPoolName,
                                                      lv.
                                                      nmhTestv2FsVpnRaAddressPoolRowStatus.
                                                      i4TestValFsVpnRaAddressPoolRowStatus);
            lv.nmhTestv2FsVpnRaAddressPoolRowStatus.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhTestv2FsVpnRaAddressPoolRowStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnRaAddressPoolRowStatus;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnRaAddressPoolRowStatus))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_FS_VPN_RA_ADDRESS_POOL_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2FsVpnRaAddressPoolTable;
            OsixKerUseInfo.pSrc = (tNpwnmhDepv2FsVpnRaAddressPoolTable *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhDepv2FsVpnRaAddressPoolTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhDepv2FsVpnRaAddressPoolTable.rval =
                nmhDepv2FsVpnRaAddressPoolTable (lv.
                                                 nmhDepv2FsVpnRaAddressPoolTable.
                                                 pu4ErrorCode,
                                                 lv.
                                                 nmhDepv2FsVpnRaAddressPoolTable.
                                                 pSnmpIndexList,
                                                 lv.
                                                 nmhDepv2FsVpnRaAddressPoolTable.
                                                 pSnmpVarBind);
            lv.nmhDepv2FsVpnRaAddressPoolTable.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhDepv2FsVpnRaAddressPoolTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhDepv2FsVpnRaAddressPoolTable;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhDepv2FsVpnRaAddressPoolTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_FS_VPN_REMOTE_ID_TABLE:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhValidateIndexInstanceFsVpnRemoteIdTable;
            OsixKerUseInfo.pSrc =
                (tNpwnmhValidateIndexInstanceFsVpnRemoteIdTable *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhValidateIndexInstanceFsVpnRemoteIdTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhValidateIndexInstanceFsVpnRemoteIdTable.rval =
                nmhValidateIndexInstanceFsVpnRemoteIdTable (lv.
                                                            nmhValidateIndexInstanceFsVpnRemoteIdTable.
                                                            i4FsVpnRemoteIdType,
                                                            lv.
                                                            nmhValidateIndexInstanceFsVpnRemoteIdTable.
                                                            pFsVpnRemoteIdValue);
            lv.nmhValidateIndexInstanceFsVpnRemoteIdTable.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhValidateIndexInstanceFsVpnRemoteIdTable *) p;
            OsixKerUseInfo.pSrc =
                &lv.nmhValidateIndexInstanceFsVpnRemoteIdTable;
            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhValidateIndexInstanceFsVpnRemoteIdTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_FS_VPN_REMOTE_ID_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexFsVpnRemoteIdTable;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFirstIndexFsVpnRemoteIdTable *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFirstIndexFsVpnRemoteIdTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFirstIndexFsVpnRemoteIdTable.rval =
                nmhGetFirstIndexFsVpnRemoteIdTable (lv.
                                                    nmhGetFirstIndexFsVpnRemoteIdTable.
                                                    pi4FsVpnRemoteIdType,
                                                    lv.
                                                    nmhGetFirstIndexFsVpnRemoteIdTable.
                                                    pFsVpnRemoteIdValue);
            lv.nmhGetFirstIndexFsVpnRemoteIdTable.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFirstIndexFsVpnRemoteIdTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexFsVpnRemoteIdTable;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFirstIndexFsVpnRemoteIdTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_FS_VPN_REMOTE_ID_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexFsVpnRemoteIdTable;
            OsixKerUseInfo.pSrc = (tNpwnmhGetNextIndexFsVpnRemoteIdTable *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetNextIndexFsVpnRemoteIdTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetNextIndexFsVpnRemoteIdTable.rval =
                nmhGetNextIndexFsVpnRemoteIdTable (lv.
                                                   nmhGetNextIndexFsVpnRemoteIdTable.
                                                   i4FsVpnRemoteIdType,
                                                   lv.
                                                   nmhGetNextIndexFsVpnRemoteIdTable.
                                                   pi4NextFsVpnRemoteIdType,
                                                   lv.
                                                   nmhGetNextIndexFsVpnRemoteIdTable.
                                                   pFsVpnRemoteIdValue,
                                                   lv.
                                                   nmhGetNextIndexFsVpnRemoteIdTable.
                                                   pNextFsVpnRemoteIdValue);
            lv.nmhGetNextIndexFsVpnRemoteIdTable.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetNextIndexFsVpnRemoteIdTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexFsVpnRemoteIdTable;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetNextIndexFsVpnRemoteIdTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_REMOTE_ID_KEY:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnRemoteIdKey;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnRemoteIdKey *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRemoteIdKey),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnRemoteIdKey.rval =
                nmhGetFsVpnRemoteIdKey (lv.nmhGetFsVpnRemoteIdKey.
                                        i4FsVpnRemoteIdType,
                                        lv.nmhGetFsVpnRemoteIdKey.
                                        pFsVpnRemoteIdValue,
                                        lv.nmhGetFsVpnRemoteIdKey.
                                        pRetValFsVpnRemoteIdKey);
            lv.nmhGetFsVpnRemoteIdKey.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnRemoteIdKey *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnRemoteIdKey;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRemoteIdKey)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_REMOTE_ID_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnRemoteIdStatus;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnRemoteIdStatus *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRemoteIdStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnRemoteIdStatus.rval =
                nmhGetFsVpnRemoteIdStatus (lv.nmhGetFsVpnRemoteIdStatus.
                                           i4FsVpnRemoteIdType,
                                           lv.nmhGetFsVpnRemoteIdStatus.
                                           pFsVpnRemoteIdValue,
                                           lv.nmhGetFsVpnRemoteIdStatus.
                                           pi4RetValFsVpnRemoteIdStatus);
            lv.nmhGetFsVpnRemoteIdStatus.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnRemoteIdStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnRemoteIdStatus;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRemoteIdStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_REMOTE_ID_KEY:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnRemoteIdKey;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnRemoteIdKey *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRemoteIdKey),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnRemoteIdKey.rval =
                nmhSetFsVpnRemoteIdKey (lv.nmhSetFsVpnRemoteIdKey.
                                        i4FsVpnRemoteIdType,
                                        lv.nmhSetFsVpnRemoteIdKey.
                                        pFsVpnRemoteIdValue,
                                        lv.nmhSetFsVpnRemoteIdKey.
                                        pSetValFsVpnRemoteIdKey);
            lv.nmhSetFsVpnRemoteIdKey.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnRemoteIdKey *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnRemoteIdKey;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRemoteIdKey)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_REMOTE_ID_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnRemoteIdStatus;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnRemoteIdStatus *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRemoteIdStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnRemoteIdStatus.rval =
                nmhSetFsVpnRemoteIdStatus (lv.nmhSetFsVpnRemoteIdStatus.
                                           i4FsVpnRemoteIdType,
                                           lv.nmhSetFsVpnRemoteIdStatus.
                                           pFsVpnRemoteIdValue,
                                           lv.nmhSetFsVpnRemoteIdStatus.
                                           i4SetValFsVpnRemoteIdStatus);
            lv.nmhSetFsVpnRemoteIdStatus.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnRemoteIdStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnRemoteIdStatus;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRemoteIdStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_REMOTE_ID_KEY:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnRemoteIdKey;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnRemoteIdKey *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnRemoteIdKey),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnRemoteIdKey.rval =
                nmhTestv2FsVpnRemoteIdKey (lv.nmhTestv2FsVpnRemoteIdKey.
                                           pu4ErrorCode,
                                           lv.nmhTestv2FsVpnRemoteIdKey.
                                           i4FsVpnRemoteIdType,
                                           lv.nmhTestv2FsVpnRemoteIdKey.
                                           pFsVpnRemoteIdValue,
                                           lv.nmhTestv2FsVpnRemoteIdKey.
                                           pTestValFsVpnRemoteIdKey);
            lv.nmhTestv2FsVpnRemoteIdKey.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnRemoteIdKey *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnRemoteIdKey;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnRemoteIdKey)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_REMOTE_ID_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnRemoteIdStatus;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnRemoteIdStatus *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnRemoteIdStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnRemoteIdStatus.rval =
                nmhTestv2FsVpnRemoteIdStatus (lv.nmhTestv2FsVpnRemoteIdStatus.
                                              pu4ErrorCode,
                                              lv.nmhTestv2FsVpnRemoteIdStatus.
                                              i4FsVpnRemoteIdType,
                                              lv.nmhTestv2FsVpnRemoteIdStatus.
                                              pFsVpnRemoteIdValue,
                                              lv.nmhTestv2FsVpnRemoteIdStatus.
                                              i4TestValFsVpnRemoteIdStatus);
            lv.nmhTestv2FsVpnRemoteIdStatus.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnRemoteIdStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnRemoteIdStatus;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnRemoteIdStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_DEPV2_FS_VPN_REMOTE_ID_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhDepv2FsVpnRemoteIdTable;
            OsixKerUseInfo.pSrc = (tNpwnmhDepv2FsVpnRemoteIdTable *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhDepv2FsVpnRemoteIdTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhDepv2FsVpnRemoteIdTable.rval =
                nmhDepv2FsVpnRemoteIdTable (lv.nmhDepv2FsVpnRemoteIdTable.
                                            pu4ErrorCode,
                                            lv.nmhDepv2FsVpnRemoteIdTable.
                                            pSnmpIndexList,
                                            lv.nmhDepv2FsVpnRemoteIdTable.
                                            pSnmpVarBind);
            lv.nmhDepv2FsVpnRemoteIdTable.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhDepv2FsVpnRemoteIdTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhDepv2FsVpnRemoteIdTable;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhDepv2FsVpnRemoteIdTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_GET_FS_VPN_REMOTE_ID_AUTH_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnRemoteIdAuthType;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnRemoteIdAuthType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRemoteIdAuthType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhGetFsVpnRemoteIdAuthType.rval =
                nmhGetFsVpnRemoteIdAuthType (lv.nmhGetFsVpnRemoteIdAuthType.
                                             i4FsVpnRemoteIdType,
                                             lv.nmhGetFsVpnRemoteIdAuthType.
                                             pFsVpnRemoteIdValue,
                                             lv.nmhGetFsVpnRemoteIdAuthType.
                                             pi4RetValFsVpnRemoteIdAuthType);
            lv.nmhGetFsVpnRemoteIdAuthType.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnRemoteIdAuthType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnRemoteIdAuthType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnRemoteIdAuthType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_REMOTE_ID_AUTH_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnRemoteIdAuthType;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnRemoteIdAuthType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRemoteIdAuthType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhSetFsVpnRemoteIdAuthType.rval =
                nmhSetFsVpnRemoteIdAuthType (lv.nmhSetFsVpnRemoteIdAuthType.
                                             i4FsVpnRemoteIdType,
                                             lv.nmhSetFsVpnRemoteIdAuthType.
                                             pFsVpnRemoteIdValue,
                                             lv.nmhSetFsVpnRemoteIdAuthType.
                                             i4SetValFsVpnRemoteIdAuthType);
            lv.nmhSetFsVpnRemoteIdAuthType.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnRemoteIdAuthType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnRemoteIdAuthType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnRemoteIdAuthType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_REMOTE_ID_AUTH_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnRemoteIdAuthType;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnRemoteIdAuthType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnRemoteIdAuthType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FsVpnRemoteIdAuthType.rval =
                nmhTestv2FsVpnRemoteIdAuthType (lv.
                                                nmhTestv2FsVpnRemoteIdAuthType.
                                                pu4ErrorCode,
                                                lv.
                                                nmhTestv2FsVpnRemoteIdAuthType.
                                                i4FsVpnRemoteIdType,
                                                lv.
                                                nmhTestv2FsVpnRemoteIdAuthType.
                                                pFsVpnRemoteIdValue,
                                                lv.
                                                nmhTestv2FsVpnRemoteIdAuthType.
                                                i4TestValFsVpnRemoteIdAuthType);
            lv.nmhTestv2FsVpnRemoteIdAuthType.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnRemoteIdAuthType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnRemoteIdAuthType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnRemoteIdAuthType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_FS_VPN_CERT_INFO_TABLE:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhValidateIndexInstanceFsVpnCertInfoTable;
            OsixKerUseInfo.pSrc =
                (tNpwnmhValidateIndexInstanceFsVpnCertInfoTable *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhValidateIndexInstanceFsVpnCertInfoTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhValidateIndexInstanceFsVpnCertInfoTable.rval =
                nmhValidateIndexInstanceFsVpnCertInfoTable (lv.
                                                            nmhValidateIndexInstanceFsVpnCertInfoTable.
                                                            pFsVpnCertKeyString);
            lv.nmhValidateIndexInstanceFsVpnCertInfoTable.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhValidateIndexInstanceFsVpnCertInfoTable *) p;
            OsixKerUseInfo.pSrc =
                &lv.nmhValidateIndexInstanceFsVpnCertInfoTable;
            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhValidateIndexInstanceFsVpnCertInfoTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_FS_VPN_CERT_INFO_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexFsVpnCertInfoTable;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFirstIndexFsVpnCertInfoTable *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFirstIndexFsVpnCertInfoTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFirstIndexFsVpnCertInfoTable.rval =
                nmhGetFirstIndexFsVpnCertInfoTable (lv.
                                                    nmhGetFirstIndexFsVpnCertInfoTable.
                                                    pFsVpnCertKeyString);
            lv.nmhGetFirstIndexFsVpnCertInfoTable.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFirstIndexFsVpnCertInfoTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexFsVpnCertInfoTable;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFirstIndexFsVpnCertInfoTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_FS_VPN_CERT_INFO_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexFsVpnCertInfoTable;
            OsixKerUseInfo.pSrc = (tNpwnmhGetNextIndexFsVpnCertInfoTable *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetNextIndexFsVpnCertInfoTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetNextIndexFsVpnCertInfoTable.rval =
                nmhGetNextIndexFsVpnCertInfoTable (lv.
                                                   nmhGetNextIndexFsVpnCertInfoTable.
                                                   pFsVpnCertKeyString,
                                                   lv.
                                                   nmhGetNextIndexFsVpnCertInfoTable.
                                                   pNextFsVpnCertKeyString);
            lv.nmhGetNextIndexFsVpnCertInfoTable.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetNextIndexFsVpnCertInfoTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexFsVpnCertInfoTable;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetNextIndexFsVpnCertInfoTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_CERT_KEY_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnCertKeyType;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnCertKeyType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnCertKeyType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhGetFsVpnCertKeyType.rval =
                nmhGetFsVpnCertKeyType (lv.nmhGetFsVpnCertKeyType.
                                        pFsVpnCertKeyString,
                                        lv.nmhGetFsVpnCertKeyType.
                                        pi4RetValFsVpnCertKeyType);
            lv.nmhGetFsVpnCertKeyType.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnCertKeyType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnCertKeyType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnCertKeyType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_CERT_KEY_FILE_NAME:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnCertKeyFileName;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnCertKeyFileName *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnCertKeyFileName),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhGetFsVpnCertKeyFileName.rval =
                nmhGetFsVpnCertKeyFileName (lv.nmhGetFsVpnCertKeyFileName.
                                            pFsVpnCertKeyString,
                                            lv.nmhGetFsVpnCertKeyFileName.
                                            pRetValFsVpnCertKeyFileName);
            lv.nmhGetFsVpnCertKeyFileName.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnCertKeyFileName *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnCertKeyFileName;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnCertKeyFileName)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_CERT_FILE_NAME:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnCertKeyFileName;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnCertKeyFileName *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnCertKeyFileName),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhGetFsVpnCertFileName.rval =
                nmhGetFsVpnCertFileName (lv.nmhGetFsVpnCertFileName.
                                         pFsVpnCertKeyString,
                                         lv.nmhGetFsVpnCertFileName.
                                         pRetValFsVpnCertFileName);
            lv.nmhGetFsVpnCertKeyFileName.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnCertKeyFileName *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnCertKeyFileName;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnCertKeyFileName)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_CERT_ENCODE_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnCertEncodeType;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnCertEncodeType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnCertEncodeType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhGetFsVpnCertEncodeType.rval =
                nmhGetFsVpnCertEncodeType (lv.nmhGetFsVpnCertEncodeType.
                                           pFsVpnCertKeyString,
                                           lv.nmhGetFsVpnCertEncodeType.
                                           pi4RetValFsVpnCertEncodeType);
            lv.nmhGetFsVpnCertEncodeType.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnCertEncodeType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnCertEncodeType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnCertEncodeType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_CERT_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnCertStatus;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnCertStatus *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnCertStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhGetFsVpnCertStatus.rval =
                nmhGetFsVpnCertStatus (lv.nmhGetFsVpnCertStatus.
                                       pFsVpnCertKeyString,
                                       lv.nmhGetFsVpnCertStatus.
                                       pi4RetValFsVpnCertStatus);
            lv.nmhGetFsVpnCertStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnCertStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnCertStatus;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnCertStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_CERT_KEY_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnCertKeyType;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnCertKeyType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnCertKeyType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhSetFsVpnCertKeyType.rval =
                nmhSetFsVpnCertKeyType (lv.nmhSetFsVpnCertKeyType.
                                        pFsVpnCertKeyString,
                                        lv.nmhSetFsVpnCertKeyType.
                                        i4SetValFsVpnCertKeyType);
            lv.nmhSetFsVpnCertKeyType.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnCertKeyType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnCertKeyType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnCertKeyType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_CERT_KEY_FILE_NAME:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnCertKeyFileName;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnCertKeyFileName *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnCertKeyFileName),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhSetFsVpnCertKeyFileName.rval =
                nmhSetFsVpnCertKeyFileName (lv.nmhSetFsVpnCertKeyFileName.
                                            pFsVpnCertKeyString,
                                            lv.nmhSetFsVpnCertKeyFileName.
                                            pSetValFsVpnCertKeyFileName);
            lv.nmhSetFsVpnCertKeyFileName.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnCertKeyFileName *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnCertKeyFileName;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnCertKeyFileName)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_CERT_FILE_NAME:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnCertFileName;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnCertFileName *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnCertFileName),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhSetFsVpnCertFileName.rval =
                nmhSetFsVpnCertFileName (lv.nmhSetFsVpnCertFileName.
                                         pFsVpnCertKeyString,
                                         lv.nmhSetFsVpnCertFileName.
                                         pSetValFsVpnCertFileName);
            lv.nmhSetFsVpnCertKeyFileName.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnCertFileName *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnCertFileName;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnCertFileName)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_CERT_ENCODE_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnCertEncodeType;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnCertEncodeType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnCertEncodeType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhSetFsVpnCertEncodeType.rval =
                nmhSetFsVpnCertEncodeType (lv.nmhSetFsVpnCertEncodeType.
                                           pFsVpnCertKeyString,
                                           lv.nmhSetFsVpnCertEncodeType.
                                           i4SetValFsVpnCertEncodeType);
            lv.nmhSetFsVpnCertKeyFileName.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnCertEncodeType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnCertEncodeType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnCertEncodeType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_CERT_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnCertStatus;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnCertStatus *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnCertStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhSetFsVpnCertStatus.rval =
                nmhSetFsVpnCertStatus (lv.nmhSetFsVpnCertStatus.
                                       pFsVpnCertKeyString,
                                       lv.nmhSetFsVpnCertStatus.
                                       i4SetValFsVpnCertStatus);
            lv.nmhSetFsVpnCertKeyFileName.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnCertStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnCertStatus;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnCertStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_CERT_KEY_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnCertKeyType;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnCertKeyType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnCertKeyType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FsVpnCertKeyType.rval =
                nmhTestv2FsVpnCertKeyType (lv.nmhTestv2FsVpnCertKeyType.
                                           pu4ErrorCode,
                                           lv.nmhTestv2FsVpnCertKeyType.
                                           pFsVpnCertKeyString,
                                           lv.nmhTestv2FsVpnCertKeyType.
                                           i4TestValFsVpnCertKeyType);
            lv.nmhTestv2FsVpnCertKeyType.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnCertKeyType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnCertKeyType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnCertKeyType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_CERT_KEY_FILE_NAME:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnCertKeyFileName;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnCertKeyFileName *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnCertKeyFileName),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FsVpnCertKeyFileName.rval =
                nmhTestv2FsVpnCertKeyFileName (lv.nmhTestv2FsVpnCertKeyFileName.
                                               pu4ErrorCode,
                                               lv.nmhTestv2FsVpnCertKeyFileName.
                                               pFsVpnCertKeyString,
                                               lv.nmhTestv2FsVpnCertKeyFileName.
                                               pTestValFsVpnCertKeyFileName);
            lv.nmhTestv2FsVpnCertKeyFileName.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnCertKeyFileName *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnCertKeyFileName;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnCertKeyFileName)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_CERT_FILE_NAME:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnCertFileName;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnCertFileName *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnCertFileName),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FsVpnCertFileName.rval =
                nmhTestv2FsVpnCertFileName (lv.nmhTestv2FsVpnCertFileName.
                                            pu4ErrorCode,
                                            lv.nmhTestv2FsVpnCertFileName.
                                            pFsVpnCertKeyString,
                                            lv.nmhTestv2FsVpnCertFileName.
                                            pTestValFsVpnCertFileName);
            lv.nmhTestv2FsVpnCertFileName.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnCertFileName *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnCertFileName;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnCertFileName)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_CERT_ENCODE_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnCertEncodeType;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnCertEncodeType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnCertEncodeType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FsVpnCertEncodeType.rval =
                nmhTestv2FsVpnCertEncodeType (lv.nmhTestv2FsVpnCertEncodeType.
                                              pu4ErrorCode,
                                              lv.nmhTestv2FsVpnCertEncodeType.
                                              pFsVpnCertKeyString,
                                              lv.nmhTestv2FsVpnCertEncodeType.
                                              i4TestValFsVpnCertEncodeType);
            lv.nmhTestv2FsVpnCertEncodeType.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnCertEncodeType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnCertEncodeType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnCertEncodeType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_CERT_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnCertStatus;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnCertStatus *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnCertStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FsVpnCertStatus.rval =
                nmhTestv2FsVpnCertStatus (lv.nmhTestv2FsVpnCertStatus.
                                          pu4ErrorCode,
                                          lv.nmhTestv2FsVpnCertStatus.
                                          pFsVpnCertKeyString,
                                          lv.nmhTestv2FsVpnCertStatus.
                                          i4TestValFsVpnCertStatus);
            lv.nmhTestv2FsVpnCertStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnCertStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnCertStatus;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnCertStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_VALIDATE_INDEX_INSTANCE_FS_VPN_CA_CERT_INFO_TABLE:
        {
            OsixKerUseInfo.pDest =
                &lv.nmhValidateIndexInstanceFsVpnCaCertInfoTable;
            OsixKerUseInfo.pSrc =
                (tNpwnmhValidateIndexInstanceFsVpnCaCertInfoTable *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhValidateIndexInstanceFsVpnCaCertInfoTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhValidateIndexInstanceFsVpnCaCertInfoTable.rval =
                nmhValidateIndexInstanceFsVpnCaCertInfoTable (lv.
                                                              nmhValidateIndexInstanceFsVpnCaCertInfoTable.
                                                              pFsVpnCaCertKeyString);
            lv.nmhValidateIndexInstanceFsVpnCaCertInfoTable.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhValidateIndexInstanceFsVpnCaCertInfoTable *) p;
            OsixKerUseInfo.pSrc =
                &lv.nmhValidateIndexInstanceFsVpnCaCertInfoTable;
            if (OsixQueSend
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhValidateIndexInstanceFsVpnCaCertInfoTable)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FIRST_INDEX_FS_VPN_CA_CERT_INFO_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFirstIndexFsVpnCaCertInfoTable;
            OsixKerUseInfo.pSrc =
                (tNpwnmhGetFirstIndexFsVpnCaCertInfoTable *) p;
            if (OsixQueRecv
                (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                 sizeof (tNpwnmhGetFirstIndexFsVpnCaCertInfoTable),
                 0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFirstIndexFsVpnCaCertInfoTable.rval =
                nmhGetFirstIndexFsVpnCaCertInfoTable (lv.
                                                      nmhGetFirstIndexFsVpnCaCertInfoTable.
                                                      pFsVpnCaCertKeyString);
            lv.nmhGetFirstIndexFsVpnCaCertInfoTable.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhGetFirstIndexFsVpnCaCertInfoTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFirstIndexFsVpnCaCertInfoTable;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFirstIndexFsVpnCaCertInfoTable))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_NEXT_INDEX_FS_VPN_CA_CERT_INFO_TABLE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetNextIndexFsVpnCaCertInfoTable;
            OsixKerUseInfo.pSrc = (tNpwnmhGetNextIndexFsVpnCaCertInfoTable *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetNextIndexFsVpnCaCertInfoTable),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetNextIndexFsVpnCaCertInfoTable.rval =
                nmhGetNextIndexFsVpnCaCertInfoTable (lv.
                                                     nmhGetNextIndexFsVpnCaCertInfoTable.
                                                     pFsVpnCaCertKeyString,
                                                     lv.
                                                     nmhGetNextIndexFsVpnCaCertInfoTable.
                                                     pNextFsVpnCaCertKeyString);
            lv.nmhGetNextIndexFsVpnCaCertInfoTable.cliWebSetError =
                gi4CliWebSetError;

            OsixKerUseInfo.pDest =
                (tNpwnmhGetNextIndexFsVpnCaCertInfoTable *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetNextIndexFsVpnCaCertInfoTable;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetNextIndexFsVpnCaCertInfoTable))
                == OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_CA_CERT_FILE_NAME:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnCaCertFileName;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnCaCertFileName *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnCaCertFileName),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhGetFsVpnCaCertFileName.rval =
                nmhGetFsVpnCaCertFileName (lv.nmhGetFsVpnCaCertFileName.
                                           pFsVpnCaCertKeyString,
                                           lv.nmhGetFsVpnCaCertFileName.
                                           pRetValFsVpnCaCertFileName);
            lv.nmhGetFsVpnCaCertFileName.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnCaCertFileName *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnCaCertFileName;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnCaCertFileName)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_CA_CERT_ENCODE_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnCaCertEncodeType;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnCaCertEncodeType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnCaCertEncodeType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhGetFsVpnCaCertEncodeType.rval =
                nmhGetFsVpnCaCertEncodeType (lv.nmhGetFsVpnCaCertEncodeType.
                                             pFsVpnCaCertKeyString,
                                             lv.nmhGetFsVpnCaCertEncodeType.
                                             pi4RetValFsVpnCaCertEncodeType);
            lv.nmhGetFsVpnCaCertEncodeType.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnCaCertEncodeType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnCaCertEncodeType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnCaCertEncodeType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_CA_CERT_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnCaCertStatus;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnCaCertStatus *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnCaCertStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhGetFsVpnCaCertStatus.rval =
                nmhGetFsVpnCaCertStatus (lv.nmhGetFsVpnCaCertStatus.
                                         pFsVpnCaCertKeyString,
                                         lv.nmhGetFsVpnCaCertStatus.
                                         pi4RetValFsVpnCaCertStatus);
            lv.nmhGetFsVpnCaCertStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnCaCertStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnCaCertStatus;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnCaCertStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_CA_CERT_FILE_NAME:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnCaCertFileName;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnCaCertFileName *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnCaCertFileName),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhSetFsVpnCaCertFileName.rval =
                nmhSetFsVpnCaCertFileName (lv.nmhSetFsVpnCaCertFileName.
                                           pFsVpnCaCertKeyString,
                                           lv.nmhSetFsVpnCaCertFileName.
                                           pSetValFsVpnCaCertFileName);
            lv.nmhSetFsVpnCertKeyFileName.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnCaCertFileName *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnCaCertFileName;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnCaCertFileName)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_CA_CERT_ENCODE_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnCaCertEncodeType;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnCaCertEncodeType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnCaCertEncodeType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhSetFsVpnCaCertEncodeType.rval =
                nmhSetFsVpnCaCertEncodeType (lv.nmhSetFsVpnCaCertEncodeType.
                                             pFsVpnCaCertKeyString,
                                             lv.nmhSetFsVpnCaCertEncodeType.
                                             i4SetValFsVpnCaCertEncodeType);
            lv.nmhSetFsVpnCertKeyFileName.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnCaCertEncodeType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnCaCertEncodeType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnCaCertEncodeType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_CA_CERT_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnCaCertStatus;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnCaCertStatus *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnCaCertStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhSetFsVpnCaCertStatus.rval =
                nmhSetFsVpnCaCertStatus (lv.nmhSetFsVpnCaCertStatus.
                                         pFsVpnCaCertKeyString,
                                         lv.nmhSetFsVpnCaCertStatus.
                                         i4SetValFsVpnCaCertStatus);
            lv.nmhSetFsVpnCertKeyFileName.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnCaCertStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnCaCertStatus;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnCaCertStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_CA_CERT_FILE_NAME:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnCaCertFileName;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnCaCertFileName *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnCaCertFileName),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FsVpnCaCertFileName.rval =
                nmhTestv2FsVpnCaCertFileName (lv.nmhTestv2FsVpnCaCertFileName.
                                              pu4ErrorCode,
                                              lv.nmhTestv2FsVpnCaCertFileName.
                                              pFsVpnCaCertKeyString,
                                              lv.nmhTestv2FsVpnCaCertFileName.
                                              pTestValFsVpnCaCertFileName);
            lv.nmhTestv2FsVpnCaCertFileName.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnCaCertFileName *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnCaCertFileName;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnCaCertFileName)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_CA_CERT_ENCODE_TYPE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnCaCertEncodeType;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnCaCertEncodeType *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnCaCertEncodeType),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FsVpnCaCertEncodeType.rval =
                nmhTestv2FsVpnCaCertEncodeType (lv.
                                                nmhTestv2FsVpnCaCertEncodeType.
                                                pu4ErrorCode,
                                                lv.
                                                nmhTestv2FsVpnCaCertEncodeType.
                                                pFsVpnCaCertKeyString,
                                                lv.
                                                nmhTestv2FsVpnCaCertEncodeType.
                                                i4TestValFsVpnCaCertEncodeType);
            lv.nmhTestv2FsVpnCaCertEncodeType.cliWebSetError =
                gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnCaCertEncodeType *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnCaCertEncodeType;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnCaCertEncodeType)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_TESTV2_FS_VPN_CA_CERT_STATUS:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnCaCertStatus;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnCaCertStatus *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnCaCertStatus),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            /* Invoke NPAPI */
            lv.nmhTestv2FsVpnCaCertStatus.rval =
                nmhTestv2FsVpnCaCertStatus (lv.nmhTestv2FsVpnCaCertStatus.
                                            pu4ErrorCode,
                                            lv.nmhTestv2FsVpnCaCertStatus.
                                            pFsVpnCaCertKeyString,
                                            lv.nmhTestv2FsVpnCaCertStatus.
                                            i4TestValFsVpnCaCertStatus);
            lv.nmhTestv2FsVpnCaCertStatus.cliWebSetError = gi4CliWebSetError;

            /* Copy NPAPI return value(s) back to user */
            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnCaCertStatus *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnCaCertStatus;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnCaCertStatus)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_SECV4_BYPASS_CAPABILITY:
        {
            OsixKerUseInfo.pDest = &lv.Secv4GetBypassCapability;
            OsixKerUseInfo.pSrc = (tNpwnmhGetSecv4BypassCapability *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetSecv4BypassCapability),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.Secv4GetBypassCapability.rval =
                Secv4GetBypassCapability (lv.Secv4GetBypassCapability.
                                          pi4RetValSecv4BypassCapability);
            lv.Secv4GetBypassCapability.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetSecv4BypassCapability *) p;
            OsixKerUseInfo.pSrc = &lv.Secv4GetBypassCapability;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetSecv4BypassCapability)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_SECV4_BYPASS_CAPABILITY:
        {
            OsixKerUseInfo.pDest = &lv.Secv4SetBypassCapability;
            OsixKerUseInfo.pSrc = (tNpwnmhSetSecv4BypassCapability *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetSecv4BypassCapability),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.Secv4SetBypassCapability.rval =
                Secv4SetBypassCapability (lv.Secv4SetBypassCapability.
                                          pi4SetValSecv4BypassCapability);
            lv.Secv4SetBypassCapability.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetSecv4BypassCapability *) p;
            OsixKerUseInfo.pSrc = &lv.Secv4SetBypassCapability;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetSecv4BypassCapability)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_GET_FS_VPN_DUMMY_PKT_GEN:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnDummyPktGen;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnDummyPktGen *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnDummyPktGen),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnDummyPktGen.rval =
                nmhGetFsVpnDummyPktGen (lv.nmhGetFsVpnDummyPktGen.
                                        pi4RetValFsVpnDummyPktGen);
            lv.nmhGetFsVpnDummyPktGen.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnDummyPktGen *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnDummyPktGen;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnDummyPktGen)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_SET_FS_VPN_DUMMY_PKT_GEN:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnDummyPktGen;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnDummyPktGen *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnDummyPktGen),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnDummyPktGen.rval =
                nmhSetFsVpnDummyPktGen (lv.nmhSetFsVpnDummyPktGen.
                                        i4SetValFsVpnDummyPktGen);
            lv.nmhSetFsVpnDummyPktGen.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnDummyPktGen *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnDummyPktGen;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnDummyPktGen)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_TESTV2_FS_VPN_DUMMY_PKT_GEN:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnDummyPktGen;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnDummyPktGen *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnDummyPktGen),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnDummyPktGen.rval =
                nmhTestv2FsVpnDummyPktGen (lv.nmhTestv2FsVpnDummyPktGen.
                                           pu4ErrorCode,
                                           lv.nmhTestv2FsVpnDummyPktGen.
                                           i4TestValFsVpnDummyPktGen);
            lv.nmhTestv2FsVpnDummyPktGen.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnDummyPktGen *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnDummyPktGen;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnDummyPktGen)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_GET_FS_VPN_DUMMY_PKT_PARAM:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsVpnDummyPktParam;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsVpnDummyPktParam *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnDummyPktParam),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsVpnDummyPktParam.rval =
                nmhGetFsVpnDummyPktParam (lv.nmhGetFsVpnDummyPktParam.
                                          pi4RetValFsVpnDummyPktParam);
            lv.nmhGetFsVpnDummyPktParam.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsVpnDummyPktParam *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsVpnDummyPktParam;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsVpnDummyPktParam)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_SET_FS_VPN_DUMMY_PKT_PARAM:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsVpnDummyPktParam;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsVpnDummyPktParam *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnDummyPktParam),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsVpnDummyPktParam.rval =
                nmhSetFsVpnDummyPktParam (lv.nmhSetFsVpnDummyPktParam.
                                          i4SetValFsVpnDummyPktParam);
            lv.nmhSetFsVpnDummyPktParam.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsVpnDummyPktParam *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsVpnDummyPktParam;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsVpnDummyPktParam)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_TESTV2_FS_VPN_DUMMY_PKT_PARAM:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsVpnDummyPktParam;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsVpnDummyPktParam *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnDummyPktParam),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsVpnDummyPktParam.rval =
                nmhTestv2FsVpnDummyPktParam (lv.nmhTestv2FsVpnDummyPktParam.
                                             pu4ErrorCode,
                                             lv.nmhTestv2FsVpnDummyPktParam.
                                             i4TestValFsVpnDummyPktParam);
            lv.nmhTestv2FsVpnDummyPktParam.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsVpnDummyPktParam *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsVpnDummyPktParam;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsVpnDummyPktParam)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_GET_FS_VPN_IPSEC_TRACE:
        {
            OsixKerUseInfo.pDest = &lv.nmhGetFsIpsecTraceOption;
            OsixKerUseInfo.pSrc = (tNpwnmhGetFsIpsecTraceOption *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsIpsecTraceOption),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhGetFsIpsecTraceOption.rval =
                nmhGetFsIpsecTraceOption (lv.nmhGetFsIpsecTraceOption.
                                          pi4RetValFsIpsecTraceOption);
            lv.nmhGetFsIpsecTraceOption.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetFsIpsecTraceOption *) p;
            OsixKerUseInfo.pSrc = &lv.nmhGetFsIpsecTraceOption;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetFsIpsecTraceOption)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_VPN_IPSEC_TRACE:
        {
            OsixKerUseInfo.pDest = &lv.nmhSetFsIpsecTraceOption;
            OsixKerUseInfo.pSrc = (tNpwnmhSetFsIpsecTraceOption *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsIpsecTraceOption),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhSetFsIpsecTraceOption.rval =
                nmhSetFsIpsecTraceOption (lv.nmhSetFsIpsecTraceOption.
                                          i4SetValFsIpsecTraceOption);
            lv.nmhSetFsIpsecTraceOption.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetFsIpsecTraceOption *) p;
            OsixKerUseInfo.pSrc = &lv.nmhSetFsIpsecTraceOption;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetFsIpsecTraceOption)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_TESTV2_FS_VPN_IPSEC_TRACE:
        {
            OsixKerUseInfo.pDest = &lv.nmhTestv2FsIpsecTraceOption;
            OsixKerUseInfo.pSrc = (tNpwnmhTestv2FsIpsecTraceOption *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsIpsecTraceOption),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.nmhTestv2FsIpsecTraceOption.rval =
                nmhTestv2FsIpsecTraceOption (lv.nmhTestv2FsIpsecTraceOption.
                                             pu4ErrorCode,
                                             lv.nmhTestv2FsIpsecTraceOption.
                                             i4TestValFsIpsecTraceOption);
            lv.nmhTestv2FsIpsecTraceOption.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhTestv2FsIpsecTraceOption *) p;
            OsixKerUseInfo.pSrc = &lv.nmhTestv2FsIpsecTraceOption;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhTestv2FsIpsecTraceOption)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

#ifdef IPSECv6_WANTED
        case NMH_GET_FS_SECV6_BYPASS_CAPABILITY:
        {
            OsixKerUseInfo.pDest = &lv.Secv6GetBypassCapability;
            OsixKerUseInfo.pSrc = (tNpwnmhGetSecv6BypassCapability *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetSecv6BypassCapability),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.Secv6GetBypassCapability.rval =
                Secv6GetBypassCapability (lv.Secv6GetBypassCapability.
                                          pi4RetValSecv6BypassCapability);
            lv.Secv6GetBypassCapability.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhGetSecv6BypassCapability *) p;
            OsixKerUseInfo.pSrc = &lv.Secv6GetBypassCapability;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhGetSecv6BypassCapability)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }

        case NMH_SET_FS_SECV6_BYPASS_CAPABILITY:
        {
            OsixKerUseInfo.pDest = &lv.Secv6SetBypassCapability;
            OsixKerUseInfo.pSrc = (tNpwnmhSetSecv6BypassCapability *) p;
            if (OsixQueRecv (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetSecv6BypassCapability),
                             0) == OSIX_FAILURE)
            {
                return (-EFAULT);
            }

            lv.Secv6SetBypassCapability.rval =
                Secv6SetBypassCapability (lv.Secv6SetBypassCapability.
                                          pi4SetValSecv6BypassCapability);
            lv.Secv6SetBypassCapability.cliWebSetError = gi4CliWebSetError;

            OsixKerUseInfo.pDest = (tNpwnmhSetSecv6BypassCapability *) p;
            OsixKerUseInfo.pSrc = &lv.Secv6SetBypassCapability;
            if (OsixQueSend (gu4CopyModIdx, (UINT1 *) &OsixKerUseInfo,
                             sizeof (tNpwnmhSetSecv6BypassCapability)) ==
                OSIX_FAILURE)
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
#endif

        default:
            rc = -EFAULT;
            break;

    }                            /* switch */

    return (rc);
}

/****************************************************************************
 *  Function    :  allocmem_octetstring
 *  Description :  This Function Allocates Memory for Octet String Type.
 *  Input       :  i4_size : The Size of Octet String.
 *  Output      :  Allocated Octet String Pointer.
 *  Returns     :  Pointer to an Octet String Variable.
 *  ****************************************************************************/
tSNMP_OCTET_STRING_TYPE *
allocmem_octetstring (INT4 i4_size)
{
    tSNMP_OCTET_STRING_TYPE *temp;

    /*  This Fn Allocates Memory for the Octet String. */
    temp =
        (tSNMP_OCTET_STRING_TYPE *)
        MEM_MALLOC (sizeof (tSNMP_OCTET_STRING_TYPE), tSNMP_OCTET_STRING_TYPE);
    if (temp == NULL)
    {
        return (NULL);
    }
    temp->pu1_OctetList = (UINT1 *) MEM_MALLOC (i4_size, INT4);

    if (temp->pu1_OctetList == NULL)
    {
        MEM_FREE (temp);
        return (NULL);
    }
    temp->i4_Length = i4_size;
    return temp;
}

/****************************************************************************
 Function    :  free_octetstring
 Description :  This Function Frees Memory for Octet String Type Var.
 Input       :  temp : The Pointer to the Octet String.
 Output      :  Frees the Octet String Pointer.
 Returns     :  None.
****************************************************************************/
void
free_octetstring (tSNMP_OCTET_STRING_TYPE * temp)
{
    if (temp == NULL)
    {
        return;
    }
    MEM_FREE (temp->pu1_OctetList);
    MEM_FREE (temp);
    return;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6IsOurAddressInCxt
 * Description      :   Check wether the given address is our addess and
 *                      fetch the interface index of that address
 * Inputs           :   pAddr          - IPv6 Address
 * Outputs          :   pu4IfIndex     - Interface Index
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
NetIpv6IsOurAddress (tIp6Addr * pAddr, UINT4 *pu4IfIndex)
{
    /* Need to be implemented while IPV6 support is provided for security module */
    UNUSED_PARAM (pAddr);
    UNUSED_PARAM (pu4IfIndex);
    return 0;
}

/*
 ******************************************************************************
 * Function Name    :   NetIpv6GetFirstIfAddr
 * Description      :   This function provides the first address of the
 *                      interface
 * Inputs           :   u4IfIndex - Interface index
 * Outputs          :   pNetIpv6AddrInfo - First address information
 * Return Value     :   NETIPV6_SUCCESS | NETIPV6_FAILURE
 *******************************************************************************
 */

INT4
NetIpv6GetFirstIfAddr (UINT4 u4IfIndex, tNetIpv6AddrInfo * pNetIpv6AddrInfo)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pNetIpv6AddrInfo);
    return IP6_FAILURE;
}

/*****************************************************************************/
/* Function     : VpnMain                                                    */
/* Description  : This function initialises the VPN module memory pools,     */
/*                queues and the semaphores. It also receives and processes  */
/*                events from the other modules.                             */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : Nothing                                                    */
/*****************************************************************************/

VOID
VpnMain (VOID)
{
    lrInitComplete (OSIX_SUCCESS);
    return;
}

/******************************************************************************/
/*  Function Name : Secv4SendIcmpErrMsg                                       */
/*  Description   : This fuction Sends out the ICMP error message             */
/*  Input(s)      : pBuf  -  Actual Pkt                                       */
/*                : pIcmp - Pointer to the ICMP Sturcture                     */
/*                : i1flag - Flag to indicate the presence of IP options      */
/*  Output(s)     : None                                                      */
/*  Return Values : None                                                      */
/******************************************************************************/
VOID
Secv4SendIcmpErrMsgToUser (tCRU_BUF_CHAIN_HEADER * pBuf, t_ICMP * pIcmp)
{
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
    tSecQueMsg          SecQueMsg;
    tSecv4ErrMsg       *pSecv4ErrMsg = NULL;
    INT1                i1RetVal = 0;

    pSecv4ErrMsg = &SecQueMsg.ModuleParam.Secv4ErrMsg;
    pSecv4ErrMsg->i1Type = pIcmp->i1Type;
    pSecv4ErrMsg->i1Code = pIcmp->i1Code;
    pSecv4ErrMsg->u4Mtu = pIcmp->u4Mtu;
    SecQueMsg.u1MsgType = SEC_MSG_TO_SECV4_FROM_KERNEL;

    pDupBuf = SEC_CRU_BUF_Duplicate_BufChain (pBuf);
    if (NULL == pDupBuf)
    {
        return;
    }

    if (CRU_BUF_Prepend_BufChain (pDupBuf, (UINT1 *)
                                  SEC_GET_MODULE_DATA_PTR (pBuf),
                                  sizeof (tSecModuleData)) == CRU_FAILURE)
    {
        SEC_CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
        return;
    }

    if (CRU_BUF_Prepend_BufChain (pDupBuf, (UINT1 *) &SecQueMsg,
                                  sizeof (tSecQueMsg)) == CRU_FAILURE)
    {
        SEC_CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
        return;
    }

    /* post the buffer to CFA queue */
    i1RetVal = OsixQueSend (gu4SecReadModIdx,
                            (UINT1 *) &pDupBuf, OSIX_DEF_MSG_LEN);

    if (i1RetVal != OSIX_SUCCESS)
    {
        SEC_CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
        return;
    }
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    return;
}
