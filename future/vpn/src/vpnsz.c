#define _VPNSZ_C
#include "vpninc.h"
extern INT4  IssSzRegisterModuleSizingParams( CHR1 *pu1ModName, tFsModSizingParams *pModSizingParams); 
extern INT4  IssSzRegisterModulePoolId( CHR1 *pu1ModName, tMemPoolId *pModPoolId); 
INT4  VpnSizingMemCreateMemPools()
{
    INT4 i4RetVal;
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < VPN_MAX_SIZING_ID; i4SizingId++) {
        i4RetVal = (INT4) MemCreateMemPool( 
                          FsVPNSizingParams[i4SizingId].u4StructSize,
                          FsVPNSizingParams[i4SizingId].u4PreAllocatedUnits,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(VPNMemPoolIds[ i4SizingId]));
        if( i4RetVal == (INT4) MEM_FAILURE) {
            VpnSizingMemDeleteMemPools();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}


INT4   VpnSzRegisterModuleSizingParams( CHR1 *pu1ModName)
{
      /* Copy the Module Name */ 
       IssSzRegisterModuleSizingParams( pu1ModName, FsVPNSizingParams); 
      IssSzRegisterModulePoolId( pu1ModName, VPNMemPoolIds); 
      return OSIX_SUCCESS; 
}


VOID  VpnSizingMemDeleteMemPools()
{
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < VPN_MAX_SIZING_ID; i4SizingId++) {
        if(VPNMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool( VPNMemPoolIds[ i4SizingId] );
            VPNMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
