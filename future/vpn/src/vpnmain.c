/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vpnmain.c,v 1.13 2014/08/21 12:19:13 siva Exp $
 *
 * Description: File containing main apis for module initialization
 * *******************************************************************/

#ifndef _VPN_MAIN_C__
#define _VPN_MAIN_C__
#include "vpninc.h"
#include "cfa.h"
#include "fsvpnwr.h"

PRIVATE INT4        VpnPortCfaRegLL (VOID);
#ifdef IP6_WANTED
PRIVATE VOID        VpnIpv6IfStateChangeCallBack (tNetIpv6HliParams *
                                                  pIp6HliParams);
#endif

/*****************************************************************************/
/* Function     : VpnInit                                                    */
/* Description  : Initialization for VPN                                     */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
VpnInit (INT1 *pi1Param)
{

#ifdef IP6_WANTED
    INT4                i4RetVal = OSIX_SUCCESS;
#endif

    UNUSED_PARAM (pi1Param);

    gu4VPNMaxTunnels = VPN_SYS_MAX_NUM_TUNNELS;
    gu4VpnRaServer = RAVPN_SERVER;
    gu4RaRefCount = VPN_ZERO;
    gu4VpnRestoredConfig = OSIX_FALSE;

    if (OsixCreateSem (VPN_DS_LOCK,
                       VPN_ONE, VPN_ZERO, &gVpnDsSemId) != OSIX_SUCCESS)
    {
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    if (VpnSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        lrInitComplete (OSIX_FAILURE);
    }
    /* Initialising the Linked list for Address Pool */
    TMO_SLL_Init (&gVpnRaUserList);
    TMO_SLL_Init (&gVpnRaAddressPoolList);
    TMO_SLL_Init (&gVpnRemoteIdList);
    TMO_SLL_Init (&gVpnCertInfoList);
    TMO_SLL_Init (&gVpnCaCertInfoList);
    TMO_SLL_Init (&VpnPolicyList);

    /* Register the VPN mib */
    RegisterFSVPN ();

    if (VpnPortCfaRegLL () == OSIX_FAILURE)
    {
        lrInitComplete (OSIX_FAILURE);
    }

#ifdef IP6_WANTED
    i4RetVal = NetIpv6RegisterHigherLayerProtocol (VPN_ID,
                                                   (NETIPV6_ADDRESS_CHANGE |
                                                    NETIPV6_INTERFACE_PARAMETER_CHANGE),
                                                   (void *)
                                                   VpnIpv6IfStateChangeCallBack);
    if (i4RetVal == NETIPV6_FAILURE)
    {
        lrInitComplete (OSIX_FAILURE);
    }
#endif

    if (OsixCreateQ (VPN_EVENT_QUEUE, VPN_EVENT_QUEUE_DEPTH,
                     SELF, &gVpnQId) != OSIX_SUCCESS)
    {
        lrInitComplete (OSIX_FAILURE);
    }

    lrInitComplete (OSIX_SUCCESS);
}

/*****************************************************************************/
/* Function     : VpnDsLock                                                  */
/* Description  : This semaphore is used to protect the VPN data structures. */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*****************************************************************************/

INT4
VpnDsLock (VOID)
{
    if (OsixSemTake (gVpnDsSemId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : VpnDsUnLock                                                */
/* Description  : Releases the semaphore lock acquired by VpnDsLock ().      */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*****************************************************************************/

INT4
VpnDsUnLock (VOID)
{
    OsixSemGive (gVpnDsSemId);

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : VpnMain                                                    */
/* Description  : This function initialises the VPN module memory pools,     */
/*                queues and the semaphores. It also receives and processes  */
/*                events from the other modules.                             */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : Nothing                                                    */
/*****************************************************************************/

VOID
VpnMain (VOID)
{
    UINT4               u4EventMask = VPN_ZERO;
    tVpnIntfInfoChgEvtMsg *pIfInfoChgMsg = NULL;

    VpnInit (NULL);

    while (VPN_TRUE)
    {
        OsixReceiveEvent (VPN_INTF_INFO_CHANGE_EVENT,
                          (OSIX_WAIT | OSIX_EV_ANY), VPN_ZERO, &u4EventMask);
        {
            if (u4EventMask & VPN_INTF_INFO_CHANGE_EVENT)
            {
                while (OsixReceiveFromQ (SELF, VPN_EVENT_QUEUE, OSIX_NO_WAIT,
                                         VPN_ZERO,
                                         (tOsixMsg **) (VOID *) &pIfInfoChgMsg)
                       == OSIX_SUCCESS)
                {
                    if (pIfInfoChgMsg->u4IfAddrType == IPVX_ADDR_FMLY_IPV4)
                    {
                        VpnIntfStateChngTrigger (pIfInfoChgMsg->u1Status,
                                                 pIfInfoChgMsg->u4IfIndex,
                                                 pIfInfoChgMsg->u4IpAddr,
                                                 pIfInfoChgMsg->u1NwType,
                                                 pIfInfoChgMsg->u1WanType);
                    }

                    if (pIfInfoChgMsg->u4IfAddrType == IPVX_ADDR_FMLY_IPV6)
                    {
                        VpnIpv6IntfStateChngTrigger (pIfInfoChgMsg->u1Status,
                                                     pIfInfoChgMsg->u4IfIndex,
                                                     pIfInfoChgMsg->u1NwType,
                                                     pIfInfoChgMsg->u1WanType);
                    }
                    /* Release the memory allocated at the caller context */
                    MemReleaseMemBlock (VPN_Q_PID, (UINT1 *) pIfInfoChgMsg);
                }
            }
        }
    }
}

/*****************************************************************************/
/* Function     : VpnPostMsgToQAndNotify                                     */
/* Description  : This function posts the given message to VPN message queue */
/*                and sends the corresponding event to VPN task to process it*/
/* Input        : Typecasted message to tOsixMsg and the event to be posted. */
/* Output       : None                                                       */
/* Returns      : VPN_SUCCESS/VPN_FAILURE                                    */
/*****************************************************************************/
INT4
VpnPostMsgToQAndNotify (tOsixMsg * pQMsg, UINT4 u4Event)
{

    if (OsixSendToQ (SELF, VPN_EVENT_QUEUE,
                     pQMsg, OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        return (VPN_FAILURE);
    }
    else
    {
        if (OsixSendEvent (SELF, (const UINT1 *) VPN_TASK_NAME, u4Event)
            != OSIX_SUCCESS)
        {
            return (VPN_FAILURE);
        }
    }

    return (VPN_SUCCESS);
}

/*****************************************************************************/
/* Function     : VpnPortCfaRegLL                                            */
/*                                                                           */
/* Description  : VPN module during initialization will invoke this API after*/
/*                loading the structure CfaRegParams with necessary callback */
/*                functions. On interface status changes, CFA module should  */
/*                invoke the mentioned call back functions appropriately.    */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
VpnPortCfaRegLL (VOID)
{
    tCfaRegParams       CfaRegParams;
    INT4                i4RetVal = OSIX_SUCCESS;
    MEMSET (&CfaRegParams, VPN_ZERO, sizeof (tCfaRegParams));

    CfaRegParams.u2RegMask = CFA_IF_DELETE | CFA_IF_OPER_ST_CHG | CFA_IF_UPDATE;
    CfaRegParams.pIfCreate = NULL;
    CfaRegParams.pIfDelete = &VpnIntfChangeCallBack;
    CfaRegParams.pIfUpdate = &VpnIntfChangeCallBack;
    CfaRegParams.pIfOperStChg = &VpnIntfChangeCallBack;
    CfaRegParams.pIfRcvPkt = NULL;

    if (CfaRegisterHL (&CfaRegParams) != CFA_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }

    return i4RetVal;
}

#ifdef IP6_WANTED
/***************************************************************************/
/*  Function Name    : VpnIpv6IfStateChangeCallBack                        */
/*                                                                         */
/*  Description      : This function is provided to IPv6 as a callback     */
/*                     function. This is called by IPv6 when IPv6 Address  */
/*                     status chenged or Interface status changed.         */
/*                                                                         */
/*  Input(s)         : pIp6HliParams : Pointer to tIp6HliParams            */
/*                                                                         */
/*  Output(s)        : None.                                               */
/*                                                                         */
/*  Returns          : VOID.                                               */
/***************************************************************************/
PUBLIC VOID
VpnIpv6IfStateChangeCallBack (tNetIpv6HliParams * pIp6HliParams)
{
    tCfaIfInfo          IfInfo;
    tVpnIntfInfoChgEvtMsg *pIfInfoChgMsg = NULL;

    MEMSET (&IfInfo, VPN_ZERO, sizeof (tCfaIfInfo));

    pIfInfoChgMsg = (tVpnIntfInfoChgEvtMsg *) MemAllocMemBlk (VPN_Q_PID);
    if (pIfInfoChgMsg == NULL)
    {
        return;
    }

    pIfInfoChgMsg->u4IfAddrType = IPVX_ADDR_FMLY_IPV6;

    switch (pIp6HliParams->u4Command)
    {
        case NETIPV6_ADDRESS_CHANGE:

            CfaGetIfInfo (pIp6HliParams->unIpv6HlCmdType.AddrChange.u4Index,
                          &IfInfo);
            pIfInfoChgMsg->u4IfIndex = pIp6HliParams->unIpv6HlCmdType.
                AddrChange.u4Index;
            pIfInfoChgMsg->u1WanType = IfInfo.u1WanType;
            pIfInfoChgMsg->u1NwType = IfInfo.u1NwType;

            if (pIp6HliParams->unIpv6HlCmdType.AddrChange.u4Mask
                == NETIPV6_ADDRESS_ADD)
            {
                pIfInfoChgMsg->u1Status = VPN_ADDR_ADD;
            }
            else
            {
                pIfInfoChgMsg->u1Status = VPN_ADDR_DEL;
            }
            break;

        case NETIPV6_INTERFACE_PARAMETER_CHANGE:

            CfaGetIfInfo (pIp6HliParams->unIpv6HlCmdType.
                          IfStatChange.u4Index, &IfInfo);

            if ((pIp6HliParams->unIpv6HlCmdType.IfStatChange.u4Mask ==
                 NETIPV6_INTERFACE_STATUS_CHANGE) &&
                (pIp6HliParams->unIpv6HlCmdType.IfStatChange.u4IfStat
                 == NETIPV6_IF_DELETE))
            {
                pIfInfoChgMsg->u4IfIndex = pIp6HliParams->unIpv6HlCmdType.
                    AddrChange.u4Index;
                pIfInfoChgMsg->u1NwType = IfInfo.u1NwType;
                pIfInfoChgMsg->u1WanType = IfInfo.u1WanType;
                pIfInfoChgMsg->u1Status = VPN_INTF_DELETE;
            }
            break;

        default:
            MemReleaseMemBlock (VPN_Q_PID, (UINT1 *) pIfInfoChgMsg);
            return;
    }

    if (VpnPostMsgToQAndNotify ((tOsixMsg *) pIfInfoChgMsg,
                                VPN_INTF_INFO_CHANGE_EVENT) == VPN_FAILURE)
    {
        MemReleaseMemBlock (VPN_Q_PID, (UINT1 *) pIfInfoChgMsg);
        return;
    }
}
#endif
#endif /* _VPN_MAIN_C__  */
