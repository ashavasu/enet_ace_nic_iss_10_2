/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vpnkmain.c,v 1.9 2014/03/16 11:28:59 siva Exp $
 *
 * Description: File containing main apis for kernel initialization
 * *******************************************************************/

#ifndef _VPN_KMAIN_C__
#define _VPN_KMAIN_C__

#include "vpnwincs.h"
#include "vpninc.h"
#include "cfa.h"

/*****************************************************************************/
/* Function     : VpnKernelInit                                              */
/* Description  : Initialization for VPN                                     */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
INT1
VpnKernelInit (VOID)
{
    gu4VPNMaxTunnels = VPN_SYS_MAX_NUM_TUNNELS;
    gu4VpnRaServer = RAVPN_SERVER;

    if (VpnSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        VpnKernelDeInit ();
        return (VPN_FAILURE);
    }

    /* Initialising the Linked list for Address Pool */
    TMO_SLL_Init (&gVpnRaUserList);
    TMO_SLL_Init (&gVpnRaAddressPoolList);
    TMO_SLL_Init (&gVpnRemoteIdList);
    TMO_SLL_Init (&gVpnCertInfoList);
    TMO_SLL_Init (&gVpnCaCertInfoList);
    TMO_SLL_Init (&VpnPolicyList);
    lrInitComplete (OSIX_SUCCESS);
    return (VPN_SUCCESS);

}

/*****************************************************************************/
/* Function     : VpnKernelMain                                              */
/* Description  : This function initialises the VPN module memory pools,     */
/*                It also receives and processes events from the other       */
/*                modules.                                                   */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : Nothing                                                    */
/*****************************************************************************/

VOID
VpnKernelDeInit (VOID)
{
    VpnSizingMemDeleteMemPools ();
    return;
}

/*****************************************************************************/
/* Function     : VpnDsLock                                                  */
/* Description  : This semaphore is used to protect the VPN data structures. */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*****************************************************************************/

INT4
VpnDsLock (VOID)
{
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : VpnDsUnLock                                                */
/* Description  : Releases the semaphore lock acquired by VpnDsLock ().      */
/* Input        : None                                                       */
/* Output       : None                                                       */
/* Returns      : SNMP_SUCCESS/SNMP_FAILURE                                  */
/*****************************************************************************/

INT4
VpnDsUnLock (VOID)
{
    return SNMP_SUCCESS;
}

#endif /* _VPN_KMAIN_C__  */
