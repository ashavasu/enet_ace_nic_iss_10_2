/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vpncert.c,v 1.5 2013/10/24 09:55:53 siva Exp $
 *
 * Description: File containing apis for certificate related configuration
 * *******************************************************************/
#include "fsike.h"
#include "vpninc.h"
#include "vpn.h"
#include "vpnclip.h"

/* Certificate related wrapper functions*/

/**************************************************************************/
/*  Function Name   : VpnIkeGenKeyPair                                    */
/*  Description     : This function triggers the RSA/DSA key generation   */
/*                    with the specified bit length                       */
/*  Input (s)       : pu1KeyName - keyname and                            */
/*                    u4Bits - bit Length of the RSA/DSA key              */
/*                    u4Type - Type of algorithm whether RSA/DSA          */
/*  Output(s)       : None.                                               */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS.                          */
/**************************************************************************/

INT4
VpnIkeGenKeyPair (UINT1 *pu1KeyName, UINT4 u4Bits, UINT4 u4Type)
{
    UINT1               au1EngineName[VPN_MAX_NAME_LENGTH + VPN_ONE] =
        { VPN_ZERO };

    MEMSET (au1EngineName, VPN_ZERO, sizeof (au1EngineName));
    STRNCPY (au1EngineName, IKE_DUMMY_ENGINE, sizeof (au1EngineName));

    if (IkeVpnGenKeyPair (au1EngineName, pu1KeyName, u4Bits, u4Type) ==
        OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/**************************************************************************/
/*  Function Name   : VpnIkeImportKey                                     */
/*  Description     : This function import the RSA/DSA key and            */
/*                    stores the key with the specified name in database  */
/*                    u4Type - Type of algorithm whether RSA/DSA          */
/*  Input (s)       : pu1KeyName - keyname and                            */
/*  Output(s)       : None.                                               */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS.                          */
/**************************************************************************/

INT4
VpnIkeImportKey (UINT1 *pu1KeyName, UINT1 *pu1Filename, UINT4 u4Type)
{
    UINT1               au1EngineName[VPN_MAX_NAME_LENGTH + VPN_ONE] =
        { VPN_ZERO };

    MEMSET (au1EngineName, VPN_ZERO, sizeof (au1EngineName));
    STRNCPY (au1EngineName, IKE_DUMMY_ENGINE, sizeof (au1EngineName));

    if (IkeVpnImportKey (au1EngineName, pu1KeyName, pu1Filename, u4Type) ==
        OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/**************************************************************************/
/*  Function Name   : VpnIkeShowKey                                       */
/*  Description     : This function displays all the RSA/DSA Keys         */
/*  Input (s)       : u4Type - Type of algorithm whether RSA/DSA          */
/*  Output(s)       : None.                                               */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS.                          */
/**************************************************************************/

INT4
VpnIkeShowKeys (UINT4 u4Type)
{
    UINT1               au1EngineName[VPN_MAX_NAME_LENGTH + VPN_ONE]
        = { VPN_ZERO };

    MEMSET (au1EngineName, VPN_ZERO, sizeof (au1EngineName));
    STRNCPY (au1EngineName, IKE_DUMMY_ENGINE, sizeof (au1EngineName));

    if (IkeVpnShowKeys (au1EngineName, u4Type) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }
    return (OSIX_SUCCESS);
}

/**************************************************************************/
/*  Function Name   : VpnIkeDelKeys                                       */
/*  Description     : This function deletes the specified key or          */
/*                    deletes all the keys depending on input             */
/*  Input (s)       : pu1KeyID - keyname                                  */
/*                    u4Type - Type of algorithm whether RSA/DSA          */
/*  Output(s)       : None.                                               */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS.                          */
/**************************************************************************/

INT4
VpnIkeDelKeys (UINT1 *pu1KeyID, UINT4 u4Type)
{
    UINT1               au1EngineName[VPN_MAX_NAME_LENGTH + VPN_ONE] =
        { VPN_ZERO };

    MEMSET (au1EngineName, VPN_ZERO, sizeof (au1EngineName));
    STRNCPY (au1EngineName, IKE_DUMMY_ENGINE, sizeof (au1EngineName));

    if (IkeVpnDelKeys (au1EngineName, pu1KeyID, u4Type) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    return (OSIX_SUCCESS);
}

/**************************************************************************/
/*  Function Name   : VpnIkeGenCertReq                                    */
/*  Description     : This function generates a Certificate request       */
/*  Input (s)       : pu1KeyID - keyname                                  */
/*                    pu1SubName - Subject Name                           */
/*                    pu1SubAltName - Subject Alt Name                    */
/*                    u4Type - Type of algorithm whether RSA/DSA          */
/*  Output(s)       : None.                                               */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS.                          */
/**************************************************************************/

INT4
VpnIkeGenCertReq (UINT1 *pu1KeyID, UINT1 *pu1SubName,
                  UINT1 *pu1SubAltName, UINT4 u4Type)
{
    UINT1               au1EngineName[VPN_MAX_NAME_LENGTH + VPN_ONE] =
        { VPN_ZERO };

    MEMSET (au1EngineName, VPN_ZERO, sizeof (au1EngineName));
    STRNCPY (au1EngineName, IKE_DUMMY_ENGINE, sizeof (au1EngineName));

    if (IkeVpnGenCertReq (au1EngineName, pu1KeyID, pu1SubName,
                          pu1SubAltName, u4Type) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    return (OSIX_SUCCESS);
}

/**************************************************************************/
/*  Function Name   : VpnIkeImportCert                                    */
/*  Description     : This function imports a Signed Certificate          */
/*  Input (s)       : pu1Filename   - Name of the certificate file        */
/*                    u1EncodeType  - Type (PEM/DER)                      */
/*                    pu1Key        - RSA Key Index to which this         */
/*                                    certificate should be linked        */
/*  Output(s)       : None.                                               */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS.                          */
/**************************************************************************/

INT4
VpnIkeImportCert (UINT1 *pu1Filename, UINT1 u1EncodeType, UINT1 *pu1Key)
{
    UINT1               au1EngineName[VPN_MAX_NAME_LENGTH + VPN_ONE] =
        { VPN_ZERO };

    MEMSET (au1EngineName, VPN_ZERO, sizeof (au1EngineName));
    STRNCPY (au1EngineName, IKE_DUMMY_ENGINE, sizeof (au1EngineName));

    if (IkeVpnImportCert (au1EngineName, pu1Filename,
                          u1EncodeType, pu1Key) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    return (OSIX_SUCCESS);
}

/**************************************************************************/
/*  Function Name   : VpnIkeImportPeerCert                                */
/*  Description     : This function imports Peer's Certificate            */
/*  Input (s)       : pu1PeerCertID - Pert Certificate name               */
/*                    pu1Filename   - Name of the certificate file        */
/*                    u1EncodeType  - Type (PEM/DER)                      */
/*                    u1TrustFlag   -  Trusted/Untrusted                  */
/*  Output(s)       : None.                                               */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS.                          */
/**************************************************************************/
INT4
VpnIkeImportPeerCert (UINT1 *pu1PeerCertID, UINT1 *pu1FileName,
                      UINT1 u1EncodeType, UINT1 u1TrustFlag)
{

    UINT1               au1EngineName[VPN_MAX_NAME_LENGTH + VPN_ONE] =
        { VPN_ZERO };

    MEMSET (au1EngineName, VPN_ZERO, sizeof (au1EngineName));
    STRNCPY (au1EngineName, IKE_DUMMY_ENGINE, sizeof (au1EngineName));

    if (IkeVpnImportPeerCert (au1EngineName, pu1PeerCertID, pu1FileName,
                              u1EncodeType, u1TrustFlag) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    return (OSIX_SUCCESS);

}

/**************************************************************************/
/*  Function Name   : VpnIkeImportCACert                                  */
/*  Description     : This function imports CA Certificate                */
/*  Input (s)       : pu1CaCertName - CA Certificate Name                 */
/*                    pu1FileName   - Name of the Certifcate files        */
/*                    u1EncodeType  - Type (PEM/DER)                      */
/*  Output(s)       : None.                                               */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS.                          */
/**************************************************************************/

INT4
VpnIkeImportCACert (UINT1 *pu1CaCertName, UINT1 *pu1FileName,
                    UINT1 u1EncodeType)
{

    UINT1               au1EngineName[VPN_MAX_NAME_LENGTH + VPN_ONE] =
        { VPN_ZERO };

    MEMSET (au1EngineName, VPN_ZERO, sizeof (au1EngineName));
    STRNCPY (au1EngineName, IKE_DUMMY_ENGINE, sizeof (au1EngineName));

    if (IkeVpnImportCACert (au1EngineName, pu1CaCertName, pu1FileName,
                            u1EncodeType) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    return (OSIX_SUCCESS);

}

/**************************************************************************/
/*  Function Name   : VpnIkeShowCerts                                     */
/*  Description     : This function displays the Certs                    */
/*  Input (s)       : u4Option - Option states what certificates needs to */
/*                               be displayed (CA/My Cert/Peer Cert)      */
/*                  : pu1Key   - can be NULL or specific certificate name */
/*                               which needs to be displayed. IF it is    */
/*                               NULL then all the certificates will be   */
/*                               displayed                                */
/*  Output(s)       : Required certificate(s) is/are displayed            */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS.                          */
/**************************************************************************/

INT4
VpnIkeShowCerts (UINT4 u4Option, UINT1 *pu1Key)
{
    UINT1               au1EngineName[VPN_MAX_NAME_LENGTH + VPN_ONE] =
        { VPN_ZERO };

    MEMSET (au1EngineName, VPN_ZERO, sizeof (au1EngineName));
    STRNCPY (au1EngineName, IKE_DUMMY_ENGINE, sizeof (au1EngineName));

    if (IkeVpnShowCerts (au1EngineName, u4Option, pu1Key) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    return (OSIX_SUCCESS);

}

/**************************************************************************/
/*  Function Name   : VpnIkeDelCerts                                      */
/*  Description     : This function Deletes all or selected Certs         */
/*  Input (s)       : u4Option - states type of certificates which needs  */
/*                               to be deleted (CA/My Cert/Peer Cert)     */
/*                  : pu1Key   - can be NULL or specific certificate name */
/*                               which needs to be displayed. IF it is    */
/*                               NULL then all the certificates will be   */
/*                               displayed                                */
/*  Output(s)       : Required Certificate(s) is/are deleted              */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS.                          */
/**************************************************************************/

INT4
VpnIkeDelCerts (UINT4 u4Option, UINT1 *pu1Key)
{
    UINT1               au1EngineName[VPN_MAX_NAME_LENGTH + VPN_ONE] =
        { VPN_ZERO };

    MEMSET (au1EngineName, VPN_ZERO, sizeof (au1EngineName));
    STRNCPY (au1EngineName, IKE_DUMMY_ENGINE, sizeof (au1EngineName));

    if (IkeVpnDelCerts (au1EngineName, u4Option, pu1Key) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    return (OSIX_SUCCESS);
}

/**************************************************************************/
/*  Function Name   : VpnIkeCertMapPeer                                   */
/*  Description     : This function imports CA Certificate                */
/*  Input (s)       : pIkeEngineName - Engine name                        */
/*                    pu1KeyId     - RSA key ID                           */
/*                    pu1IdType    - Type of the ID (fqdn/IPv4/email)     */
/*                    u1PeerID     - Peer ID using which request          */
/*                                   will be identified                   */
/*  Output(s)       : None.                                               */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS.                          */
/**************************************************************************/

INT4
VpnIkeCertMapPeer (UINT1 *pu1KeyId, UINT1 *u1IdType, UINT1 *u1PeerId)
{
    UINT1               au1EngineName[VPN_MAX_NAME_LENGTH + VPN_ONE] =
        { VPN_ZERO };

    MEMSET (au1EngineName, VPN_ZERO, sizeof (au1EngineName));
    STRNCPY (au1EngineName, IKE_DUMMY_ENGINE, sizeof (au1EngineName));

    if (IkeVpnCertMapPeer (au1EngineName, pu1KeyId, u1IdType, u1PeerId) ==
        OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    return (OSIX_SUCCESS);
}

/**************************************************************************/
/*  Function Name   : VpnIkeDelCertMap                                   */
/*  Description     : This function deletes a particular mappping        */
/*  Input (s)       : pu1KeyId     - RSA key ID                           */
/*                    pu1IdType    - Type of the ID (fqdn/IPv4/email)     */
/*                    u1PeerID     - Peer ID using which request          */
/*                                   will be identified                   */
/*  Output(s)       : None.                                               */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS.                          */
/**************************************************************************/

INT4
VpnIkeDelCertMap (UINT1 *pu1KeyId, UINT1 *u1IdType, UINT1 *u1PeerId)
{
    UINT1               au1EngineName[VPN_MAX_NAME_LENGTH + VPN_ONE] =
        { VPN_ZERO };

    MEMSET (au1EngineName, VPN_ZERO, sizeof (au1EngineName));
    STRNCPY (au1EngineName, IKE_DUMMY_ENGINE, sizeof (au1EngineName));

    if (IkeVpnDelCertMap (au1EngineName, pu1KeyId,
                          u1IdType, u1PeerId) == OSIX_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/**************************************************************************/
/*  Function Name   : VpnIkeSaveCerts                                     */
/*  Description     : This function Saves the certificates to flash       */
/*  Input (s)       : None                                                */
/*  Output(s)       : None.                                               */
/*  Returns         : OSIX_FAILURE/OSIX_SUCCESS.                          */
/**************************************************************************/

INT4
VpnIkeSaveCerts (VOID)
{
    UINT1               au1EngineName[VPN_MAX_NAME_LENGTH + VPN_ONE] =
        { VPN_ZERO };

    MEMSET (au1EngineName, VPN_ZERO, sizeof (au1EngineName));
    STRNCPY (au1EngineName, IKE_DUMMY_ENGINE, sizeof (au1EngineName));

    if (IkeVpnSaveCerts (au1EngineName) == OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    return (OSIX_SUCCESS);
}

/* EOF */
