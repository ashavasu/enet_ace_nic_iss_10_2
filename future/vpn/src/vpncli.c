/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vpncli.c,v 1.45 2017/12/05 09:38:01 siva Exp $
 *
 * Description: File containing cli functions
 * *******************************************************************/

#ifndef __VPNCLI_C__
#define __VPNCLI_C__

#ifdef VPN_WANTED

#include "lr.h"
#include "cfa.h"
#include "cli.h"
#include "ip.h"
#include "snmcdefn.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "vpnclip.h"
#include "vpninc.h"
#include "fsvpnwr.h"
#include "ip6util.h"

INT4
cli_process_vpn_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[CLI_MAX_ARGS] = { NULL };
    UINT1               u1Argno = VPN_ZERO;
    INT4                i4RetVal = CLI_SUCCESS;
    UINT1              *pu1Inst = NULL;
    INT4                i4Len = VPN_ZERO;
    INT4                i4Value = VPN_ZERO;
    /* configured ah key */
    UINT1              *pu1SecAssocAhKey = NULL;
    UINT1               au1ModeStr[VPN_MAX_NAME_LENGTH + VPN_ONE]
        = { VPN_ZERO };
    UINT4               u4Index = VPN_ZERO;
    UINT4               u4PeerAddress = VPN_ZERO;
    UINT4               u4Action = VPN_ZERO;
    UINT4               u4Mode = VPN_ZERO;
    UINT4               u4AuthProto = VPN_ZERO;
    UINT4               u4InBoundSpi = VPN_ZERO;
    UINT4               u4OutBoundSpi = VPN_ZERO;
    UINT4               u4AntiReplay = VPN_ZERO;
    INT4                i4EspProto = VPN_ZERO;
    UINT1               au1SecAssocEspKey[IPSEC_MANUAL_KEY_LEN] = { VPN_ZERO };
    UINT1               au1TempEspKey[VPN_MAX_SIZE] = { VPN_ZERO };
    INT4                i4Length = VPN_ZERO;
    UINT4               u4SnmpErrorStatus = VPN_ZERO;
    UINT4               u4EncrAlgo = VPN_ZERO;
    UINT4               u4HashAlgo = VPN_ZERO;
    UINT4               u4DHGrp = VPN_ZERO;
    UINT4               u4ExchMode = VPN_ZERO;
    UINT4               u4LifeTime = VPN_ZERO;
    UINT4               u4LifeTimeType = VPN_ZERO;
    UINT4               u4Protocol = VPN_ZERO;
    UINT4               u4PeerIdType = VPN_ZERO;
    UINT1               au1PeerIdValue[VPN_MAX_NAME_LENGTH + VPN_ONE]
        = { VPN_ZERO };
    UINT4               u4LocalIdType = VPN_ZERO;
    UINT1               au1LocalIdValue[VPN_MAX_NAME_LENGTH + VPN_ONE]
        = { VPN_ZERO };
    UINT4               u4ErrCode = VPN_ZERO;
    tSNMP_OCTET_STRING_TYPE *pPolicyName = NULL;
    tSNMP_OCTET_STRING_TYPE *pVpnRaUserName = NULL;
    tSNMP_OCTET_STRING_TYPE *pVpnRaUserSecret = NULL;
    tSNMP_OCTET_STRING_TYPE *pVpnRaAddressPoolName = NULL;
    tSNMP_OCTET_STRING_TYPE *pSrcPortRange = NULL;    /* Source Port Range For TS */
    /* Destination Port Range For TS */
    tSNMP_OCTET_STRING_TYPE *pDstPortRange = NULL;
    UINT4               u4VpnRaStartIp = VPN_ZERO;
    UINT4               u4VpnRaEndIp = VPN_ZERO;
    UINT4               u4EncType = VPN_ZERO;
    UINT4               u4TrustFlag = VPN_ZERO;
    UINT4               u4Keylen = VPN_ZERO;
    UINT4               u4Type = VPN_ZERO;
    UINT4               u4VpnIkeVersion = VPN_ZERO;
    tVpnNetwork         LocalNet;
    tVpnNetwork         RemoteNet;
    INT4                i4XAuthType = VPN_INVALID;
    INT4                i4RAVPNServer = VPN_INVALID;
    UINT4               u4PrefixLen = VPN_ZERO;
    tIp6Addr            Ip6Addr;
    tIp6Addr            StartIpv6Addr;
    tIp6Addr            EndIpv6Addr;
    tUtlIn6Addr         In6Addr;
    UINT4               u4PolicyIndex = VPN_ZERO;
    tVpnPolicy         *pVpnPolicy = NULL;
    UINT1               au1IpAddr[VPN_IPV6_ADDR_LEN + VPN_ONE] = { VPN_ZERO };

    va_start (ap, u4Command);

    /* first arguement is always command type */
    pu1Inst = va_arg (ap, UINT1 *);

    /*  Walk through the rest of the arguements and store in args array. 
     ** Store 17 arguements at the max. This is because ipsec commands do not
     ** take more than 17 inputs from the command line. Another reason to
     ** store is in some cases first input may be optional but user may give 
     ** second input. In that case first arg will be null and second arg only 
     ** has value */
    while (VPN_TRUE)
    {
        if (u1Argno >= VPN_TEN)
        {
            break;
        }
        args[u1Argno++] = va_arg (ap, UINT1 *);
    }
    va_end (ap);

    CliRegisterLock (CliHandle, VpnDsLock, VpnDsUnLock);
    VpnDsLock ();

    if ((pPolicyName =
         allocmem_octetstring (VPN_MAX_POLICY_NAME_LENGTH + VPN_ONE)) == NULL)
    {
        i4RetVal = CLI_FAILURE;
        CLI_SET_CMD_STATUS ((UINT4) i4RetVal);
        VpnDsUnLock ();
        CliUnRegisterLock (CliHandle);

        return i4RetVal;
    }

    switch (u4Command)
    {
        case CLI_VPN_SET_VPN_STATUS:
            i4Value =
                ((CLI_PTR_TO_I4 (args[VPN_INDEX_0]) ==
                  CLI_ENABLE) ? VPN_ENABLE : VPN_DISABLE);
            if (nmhSetFsVpnGlobalStatus (i4Value) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "%% -E- Failed to set the module status\r\n");
                i4RetVal = CLI_FAILURE;
            }

            break;
        case CLI_VPN_CREATE_NEW_CRYPTO_MAP:

            CLI_STRCPY (pPolicyName->pu1_OctetList, args[VPN_INDEX_0]);
            pPolicyName->i4_Length = (INT4) STRLEN (args[VPN_INDEX_0]);
            if (STRLEN (pPolicyName->pu1_OctetList) >
                VPN_MAX_POLICY_NAME_LENGTH)
            {
                CliPrintf (CliHandle,
                           "%% -E- Policy name length is Invalid\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }
            i4RetVal = VpnCreatePolicy (CliHandle, pPolicyName);
            if (i4RetVal == CLI_FAILURE)
            {
                break;
            }
            pVpnPolicy = VpnSnmpGetPolicy (pPolicyName->pu1_OctetList,
                                           pPolicyName->i4_Length);
            if (pVpnPolicy == NULL)
            {
                break;
            }
            SPRINTF ((CHR1 *) au1ModeStr, "%s%u", VPN_CRYPTO_MODE,
                     pVpnPolicy->u4VpnPolicyIndex);
            if (CliChangePath ((CHR1 *) au1ModeStr) == CLI_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%% Could not change the mode to CRYPTO\r\n");
                i4Value = VpnDeletePolicy (CliHandle, pPolicyName);
                i4RetVal = CLI_FAILURE;
            }
            break;

        case CLI_VPN_DEL_CRYPTOMAP:

            if (args[VPN_INDEX_0] == NULL)
            {
                /* Delete all the policies exist in the system */
                i4RetVal = VpnDeletePolicyAll (CliHandle);
                break;
            }

            CLI_STRCPY (pPolicyName->pu1_OctetList, args[VPN_INDEX_0]);
            pPolicyName->i4_Length = (INT4) STRLEN (args[VPN_INDEX_0]);
            if (pPolicyName->i4_Length > (VPN_MAX_POLICY_NAME_LENGTH + VPN_ONE))
            {
                CliPrintf (CliHandle,
                           "%% -E- Policy name length is Invalid\r\n");
                i4RetVal = CLI_FAILURE;
            }

            i4RetVal = VpnDeletePolicy (CliHandle, pPolicyName);
            break;

        case CLI_VPN_SET_MODE:

            u4Mode = CLI_PTR_TO_U4 (args[VPN_INDEX_0]);

            u4PolicyIndex = (UINT4) CLI_GET_VPN_POLICY_ID ();
            VpnGetPolicyByIndex (u4PolicyIndex, pPolicyName);
            i4RetVal = VpnSetPolicyMode (CliHandle, pPolicyName, u4Mode);
            break;

        case CLI_VPN_SET_ENCRYPT_TYPE:
        case CLI_VPN_SET_DECRYPT_TYPE:
            u4Type = CLI_PTR_TO_U4 (args[VPN_INDEX_0]);
            if (u4Type == CLI_VPN_AUTH_RSA)
            {
                u4Type = VPN_AUTH_RSA;
            }
            else if (u4Type == CLI_VPN_AUTH_DSA)
            {
                u4Type = VPN_AUTH_DSA;
            }
            u4PolicyIndex = (UINT4) CLI_GET_VPN_POLICY_ID ();
            VpnGetPolicyByIndex (u4PolicyIndex, pPolicyName);
            i4RetVal = VpnSetEncryptType (CliHandle, pPolicyName, u4Type);
            break;

        case CLI_VPN_SET_POLICY_TYPE:

            u4Index = CLI_PTR_TO_U4 (args[VPN_INDEX_0]);
            u4PolicyIndex = (UINT4) CLI_GET_VPN_POLICY_ID ();
            VpnGetPolicyByIndex (u4PolicyIndex, pPolicyName);
            i4RetVal = VpnSetPolicyType (CliHandle, pPolicyName, u4Index);
            break;

        case CLI_VPN_SET_PEER_IP:

            u4PeerAddress = *((UINT4 *) (VOID *) args[VPN_INDEX_0]);

            u4PolicyIndex = (UINT4) CLI_GET_VPN_POLICY_ID ();
            VpnGetPolicyByIndex (u4PolicyIndex, pPolicyName);
            i4RetVal = VpnSetPeerIp (CliHandle, pPolicyName, u4PeerAddress);
            break;

        case CLI_VPN_SET_IPV6_PEER:

            MEMSET (&Ip6Addr, VPN_ZERO, sizeof (tIp6Addr));

            if (INET_ATON6 (args[VPN_INDEX_0], &Ip6Addr) == VPN_ZERO)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }

            u4PolicyIndex = (UINT4) CLI_GET_VPN_POLICY_ID ();
            VpnGetPolicyByIndex (u4PolicyIndex, pPolicyName);
            i4RetVal = VpnSetIpv6Peer (CliHandle, pPolicyName, &Ip6Addr);
            break;

        case CLI_VPN_SET_ACCESS_PARAMS:

            MEMSET (&LocalNet, VPN_ZERO, sizeof (tVpnNetwork));
            MEMSET (&RemoteNet, VPN_ZERO, sizeof (tVpnNetwork));

            u4Action = CLI_PTR_TO_U4 (args[VPN_INDEX_0]);
            u4Protocol = CLI_PTR_TO_U4 (args[VPN_INDEX_1]);
            LocalNet.IpAddr.uIpAddr.Ip4Addr =
                *((UINT4 *) (VOID *) args[VPN_INDEX_2]);
            IPV4_MASK_TO_MASKLEN (LocalNet.u4AddrPrefixLen,
                                  (*((UINT4 *) (VOID *) args[VPN_INDEX_3])));
            RemoteNet.IpAddr.uIpAddr.Ip4Addr =
                *((UINT4 *) (VOID *) args[VPN_INDEX_4]);
            IPV4_MASK_TO_MASKLEN (RemoteNet.u4AddrPrefixLen,
                                  (*((UINT4 *) (VOID *) args[VPN_INDEX_5])));

            if ((pSrcPortRange =
                 allocmem_octetstring ((INT4) STRLEN (args[VPN_INDEX_6]) +
                                       VPN_ONE)) == NULL)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }
            CLI_STRCPY (pSrcPortRange->pu1_OctetList, args[VPN_INDEX_6]);
            pSrcPortRange->i4_Length = (INT4) STRLEN (args[VPN_INDEX_6]);

            if ((pDstPortRange =
                 allocmem_octetstring ((INT4) STRLEN (args[VPN_INDEX_7]) +
                                       VPN_ONE)) == NULL)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }
            CLI_STRCPY (pDstPortRange->pu1_OctetList, args[VPN_INDEX_7]);
            pDstPortRange->i4_Length = (INT4) STRLEN (args[VPN_INDEX_7]);

            u4PolicyIndex = (UINT4) CLI_GET_VPN_POLICY_ID ();
            VpnGetPolicyByIndex (u4PolicyIndex, pPolicyName);
            i4RetVal =
                VpnSetAccessParams (pPolicyName, &LocalNet,
                                    &RemoteNet, u4Action, u4Protocol,
                                    pSrcPortRange, pDstPortRange);

            break;

        case CLI_VPN_SET_IPV6_ACCESS_PARAMS:
            /* args[0] - Action (permit/deny/apply)
               args[1] - Protocol (any/tcp/udp/icmpv4/ah/esp)
               args[2] - LocalNetwork IPv6Address
               args[3] - LocalNetwork Prefix Length
               args[4] - RemoteNetwork IPv6Address
               args[5] - RemoteNetwork PrefixLength
               args[6] - Source Port range
               args[7] - Destination Port range */

            MEMSET (&LocalNet, VPN_ZERO, sizeof (tVpnNetwork));
            MEMSET (&RemoteNet, VPN_ZERO, sizeof (tVpnNetwork));

            u4Action = CLI_PTR_TO_U4 (args[VPN_INDEX_0]);
            u4Protocol = CLI_PTR_TO_U4 (args[VPN_INDEX_1]);

            MEMSET (&(LocalNet.IpAddr.uIpAddr.Ip6Addr),
                    VPN_ZERO, sizeof (tIp6Addr));
            if (INET_ATON6 (args[VPN_INDEX_2],
                            &LocalNet.IpAddr.uIpAddr.Ip6Addr) == VPN_ZERO)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }
            LocalNet.u4AddrPrefixLen = *((UINT4 *) (VOID *) args[VPN_INDEX_3]);

            if (INET_ATON6 (args[VPN_INDEX_4],
                            &RemoteNet.IpAddr.uIpAddr.Ip6Addr) == VPN_ZERO)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }
            RemoteNet.u4AddrPrefixLen = *((UINT4 *) (VOID *) args[VPN_INDEX_5]);

            if ((pSrcPortRange =
                 allocmem_octetstring ((INT4) STRLEN (args[VPN_INDEX_6])
                                       + VPN_ONE)) == NULL)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }
            CLI_STRCPY (pSrcPortRange->pu1_OctetList, args[VPN_INDEX_6]);
            pSrcPortRange->i4_Length = (INT4) STRLEN (args[VPN_INDEX_6]);

            if ((pDstPortRange =
                 allocmem_octetstring ((INT4) STRLEN (args[VPN_INDEX_7])
                                       + VPN_ONE)) == NULL)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }
            CLI_STRCPY (pDstPortRange->pu1_OctetList, args[VPN_INDEX_7]);
            pDstPortRange->i4_Length = (INT4) STRLEN (args[VPN_INDEX_7]);

            u4PolicyIndex = (UINT4) CLI_GET_VPN_POLICY_ID ();
            VpnGetPolicyByIndex (u4PolicyIndex, pPolicyName);
            i4RetVal =
                VpnSetIpv6AccessParams (pPolicyName, &LocalNet,
                                        &RemoteNet, u4Action, u4Protocol,
                                        pSrcPortRange, pDstPortRange);

            break;

        case CLI_VPN_IKE_VERSION:

            u4VpnIkeVersion = CLI_PTR_TO_U4 (args[VPN_INDEX_0]);
            u4PolicyIndex = (UINT4) CLI_GET_VPN_POLICY_ID ();
            VpnGetPolicyByIndex (u4PolicyIndex, pPolicyName);
            i4RetVal =
                VpnSetIkeVersion (CliHandle, pPolicyName, u4VpnIkeVersion);
            break;

        case CLI_VPN_SET_CRYPTO_PARAMS:

            if ((MemAllocateMemBlock
                 (VPN_WEB_POLL_BUFF_PID,
                  (UINT1 **) (VOID *) &pu1SecAssocAhKey)) == MEM_FAILURE)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }
            /* args[0] - Authentication Protocol
             * args[1] - Authentication Algorithm
             * args[2] - Authentication Key
             * args[3] - Encryption Algorithm
             * args[4] - Cipher Key
             * args[5] - Cipher Key2
             * args[6] - Cipher Key3
             * args[7] - Outbound SPI
             * args[8] - Inbound SPI
             * args[9] - Anti-Reply Status
             */

            u4AuthProto = CLI_PTR_TO_U4 (args[VPN_INDEX_0]);
            if (u4AuthProto != VPN_ZERO)
            {
                u4Protocol = CLI_PTR_TO_U4 (args[VPN_INDEX_1]);
                CLI_MEMSET (pu1SecAssocAhKey, VPN_ZERO, VPN_MAX_LEN);
                CLI_STRNCPY (pu1SecAssocAhKey, args[VPN_INDEX_2],
                             VPN_MAX_LEN - VPN_ONE);
            }

            i4EspProto = CLI_PTR_TO_I4 (args[VPN_INDEX_3]);
            if ((i4EspProto != VPN_ZERO) && (u4AuthProto == CLI_VPN_AH))
            {
                CliPrintf (CliHandle, "%% The protocol for encryption"
                           " and authentication  must be ESP, if both"
                           " are specified\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }
            if (i4EspProto != VPN_ZERO)
            {
                i4Length = (INT4) STRLEN (args[VPN_INDEX_4]);
                switch (i4EspProto)
                {
                    case CLI_VPN_DES:
                    case CLI_VPN_3DES:
                        i4Len = VPN_ESP_DES_KEY_LEN;
                        break;
                    case VPN_AES_128:
                        i4Len = VPN_ESP_AES_KEY1_LEN;
                        break;
                    case VPN_AES_192:
                        i4Len = VPN_ESP_AES_KEY2_LEN;
                        break;
                    case VPN_AES_256:
                        i4Len = VPN_ESP_AES_KEY3_LEN;
                        break;
                    default:
                        i4Len = VPN_INVALID;
                }
                if (i4Len != i4Length)
                {
                    CLI_SET_ERR (CLI_VPN_ERR_MAN_ENCRY_KEY_LEN);
                    i4RetVal = CLI_FAILURE;
                    break;
                }
                CLI_STRCPY (au1SecAssocEspKey, args[VPN_INDEX_4]);

                if (i4EspProto == CLI_VPN_3DES)    /* 3des - so read 2 more keys */
                {
                    i4Len = (INT4) (i4Len + (INT4) STRLEN (args[VPN_INDEX_5]));
                    i4Len = (INT4) (i4Len + (INT4) STRLEN (args[VPN_INDEX_6]));
                    if (i4Len != VPN_ESP_3DES_KEY_LEN)
                    {
                        CLI_SET_ERR (CLI_VPN_ERR_MAN_ENCRY_KEY_LEN);
                        i4RetVal = CLI_FAILURE;
                        break;
                    }

                    au1SecAssocEspKey[i4Length++] = '.';
                    CLI_STRCPY (au1TempEspKey, args[VPN_INDEX_5]);
                    for (i4Len = VPN_ZERO; i4Len < VPN_ESP_DES_KEY_LEN; i4Len++)
                    {
                        au1SecAssocEspKey[i4Length++] = au1TempEspKey[i4Len];
                    }

                    au1SecAssocEspKey[i4Length++] = '.';
                    CLI_STRCPY (au1TempEspKey, args[VPN_INDEX_6]);
                    for (i4Len = VPN_ZERO; i4Len < VPN_ESP_DES_KEY_LEN; i4Len++)
                    {
                        au1SecAssocEspKey[i4Length++] = au1TempEspKey[i4Len];
                    }
                    au1SecAssocEspKey[i4Length] = '\0';
                }
            }

            u4OutBoundSpi = *((UINT4 *) (VOID *) args[VPN_INDEX_7]);
            u4InBoundSpi = *((UINT4 *) (VOID *) args[VPN_INDEX_8]);

            u4AntiReplay = CLI_PTR_TO_U4 (args[VPN_INDEX_9]);

            u4PolicyIndex = (UINT4) CLI_GET_VPN_POLICY_ID ();
            VpnGetPolicyByIndex (u4PolicyIndex, pPolicyName);
            i4RetVal = VpnSetCryptoParams (CliHandle, pPolicyName,
                                           u4AuthProto, u4Protocol,
                                           pu1SecAssocAhKey,
                                           u4InBoundSpi, u4OutBoundSpi,
                                           i4EspProto, au1SecAssocEspKey,
                                           u4AntiReplay);
            break;

        case CLI_VPN_SET_POLICY_TO_INTERFACE:

            CLI_STRCPY (pPolicyName->pu1_OctetList, args[VPN_INDEX_0]);
            pPolicyName->i4_Length = (INT4) STRLEN (args[VPN_INDEX_0]);
            u4Index = (UINT4) CLI_GET_IFINDEX ();

            i4RetVal = VpnSetPolicyInterface (CliHandle, pPolicyName, u4Index);
            break;

        case CLI_VPN_DEL_POLICY_FROM_INTERFACE:

            CLI_STRCPY (pPolicyName->pu1_OctetList, args[VPN_INDEX_0]);
            pPolicyName->i4_Length = (INT4) STRLEN (args[VPN_INDEX_0]);

            if (nmhTestv2FsVpnPolicyRowStatus (&u4SnmpErrorStatus, pPolicyName,
                                               NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_VPN_ERR_NO_POLICY);
                i4RetVal = CLI_FAILURE;
                break;
            }

            nmhGetFsVpnPolicyIntfIndex (pPolicyName, (INT4 *) &u4Index);
            if (u4Index != (UINT4) CLI_GET_IFINDEX ())
            {
                CliPrintf (CliHandle, "%% Specified policy is not configured "
                           "on this interface\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }
            if (nmhSetFsVpnPolicyRowStatus (pPolicyName, NOT_IN_SERVICE) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "%% Unable to delete the policy "
                           "from interface.\r\n");
                i4RetVal = CLI_FAILURE;
            }

            break;

        case CLI_VPN_SHOW_VPN_CONFIG:
            i4RetVal = VpnShowConfiguration (CliHandle);
            break;

        case CLI_VPN_SHOW_VPN_POLICY:

            if (args[VPN_INDEX_0] == VPN_ZERO)
            {
                i4RetVal = VpnShowPolicy (CliHandle, NULL);
                break;
            }

            if (CLI_STRLEN (args[VPN_INDEX_0]) > VPN_MAX_POLICY_NAME_LENGTH)
            {
                CLI_SET_ERR (CLI_VPN_ERR_POLICY_NAME_LENGTH);
                i4RetVal = CLI_FAILURE;
                break;
            }

            CLI_STRCPY (pPolicyName->pu1_OctetList, args[VPN_INDEX_0]);
            pPolicyName->i4_Length = (INT4) STRLEN (args[VPN_INDEX_0]);

            i4RetVal = VpnShowPolicy (CliHandle, pPolicyName);
            break;

        case CLI_VPN_ADD_REMOTE_ID_INFO:
            /* args[0] - Identity type
             * args[1] - Identity value
             * args[2] - Identifier whether PSK/CERT.
             * args[3] - Pre-shared key / RSA Key Id
             */
            i4RetVal = VpnAddRemoteIdInfo (CliHandle,
                                           CLI_PTR_TO_I4 (args[VPN_INDEX_0]),
                                           ((UINT1 *) args[VPN_INDEX_1]),
                                           (CLI_PTR_TO_U4 (args[VPN_INDEX_2])),
                                           ((UINT1 *) args[VPN_INDEX_3]));
            break;

        case CLI_VPN_DEL_REMOTE_ID_INFO:
            /* args[0] - Identity type
             * args[1] - Identity value
             */

            if ((CLI_PTR_TO_I4 (args[VPN_INDEX_0])) == CLI_VPN_IKE_KEY_IPV6)
            {
                /* Allways store the IPv6 identity in the full address
                 * format. So convert the ATON and NTOA to get the full
                 * address in string */
                MEMSET (&In6Addr, VPN_ZERO, sizeof (tUtlIn6Addr));
                MEMSET (au1IpAddr, VPN_ZERO, (VPN_IPV6_ADDR_LEN + VPN_ONE));

                INET_ATON6 (args[VPN_INDEX_1], &In6Addr);
                MEMCPY (au1IpAddr, INET_NTOA6 (In6Addr), (VPN_IPV6_ADDR_LEN));

                pPolicyName->i4_Length = (INT4) STRLEN (au1IpAddr);
                CLI_STRCPY (pPolicyName->pu1_OctetList, au1IpAddr);
            }
            else
            {
                CLI_STRCPY (pPolicyName->pu1_OctetList, args[VPN_INDEX_1]);
                pPolicyName->i4_Length = (INT4) STRLEN (args[VPN_INDEX_1]);
            }

            if (nmhTestv2FsVpnRemoteIdStatus (&u4SnmpErrorStatus,
                                              CLI_PTR_TO_I4 (args[VPN_INDEX_0]),
                                              pPolicyName,
                                              DESTROY) == SNMP_FAILURE)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }
            if (nmhSetFsVpnRemoteIdStatus (CLI_PTR_TO_I4 (args[VPN_INDEX_0]),
                                           pPolicyName,
                                           DESTROY) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "%% Failed to delete the Id. There"
                           " could be active policies using this Remote Id "
                           "or could not communicate to Vpn subsystem\r\n");
                i4RetVal = CLI_FAILURE;
            }

            break;

        case CLI_VPN_SHOW_REMOTE_ID_INFO:
            i4RetVal = VpnShowRemoteIdInfo (CliHandle,
                                            CLI_PTR_TO_I4 (args[VPN_INDEX_0]));
            break;

        case CLI_VPN_ISAKMP_PEER_IDENTITY:

            u4PeerIdType = CLI_PTR_TO_U4 (args[VPN_INDEX_0]);

            MEMSET (au1PeerIdValue, VPN_ZERO, (VPN_MAX_NAME_LENGTH + VPN_ONE));
            if (STRLEN (args[VPN_INDEX_1]) > VPN_MAX_NAME_LENGTH)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }
            MEMCPY (au1PeerIdValue, args[VPN_INDEX_1],
                    STRLEN (args[VPN_INDEX_1]));
            au1PeerIdValue[STRLEN (args[VPN_INDEX_1])] = '\0';
            u4PolicyIndex = (UINT4) CLI_GET_VPN_POLICY_ID ();
            VpnGetPolicyByIndex (u4PolicyIndex, pPolicyName);
            i4RetVal =
                VpnSetPeerType (CliHandle, pPolicyName, u4PeerIdType,
                                au1PeerIdValue);

            break;

        case CLI_VPN_ISAKMP_LOCAL_IDENTITY:

            u4LocalIdType = CLI_PTR_TO_U4 (args[VPN_INDEX_0]);
            MEMSET (au1LocalIdValue, VPN_ZERO, sizeof (au1LocalIdValue));
            if (STRLEN (args[VPN_INDEX_1]) > VPN_MAX_NAME_LENGTH)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }
            MEMCPY (au1LocalIdValue, args[VPN_INDEX_1],
                    STRLEN (args[VPN_INDEX_1]));
            au1LocalIdValue[(STRLEN (args[VPN_INDEX_1]) + VPN_ONE)] = '\0';
            u4PolicyIndex = (UINT4) CLI_GET_VPN_POLICY_ID ();
            VpnGetPolicyByIndex (u4PolicyIndex, pPolicyName);
            i4RetVal =
                VpnSetLocalIdType (CliHandle, pPolicyName, u4LocalIdType,
                                   au1LocalIdValue);
            break;

        case CLI_VPN_IKE_PROPOSAL:

            /* 
             * args[0] = Encryption Algorithm (des/3des/aes-128/192/256)
             * args[1] = Hash Algorithm
             * args[2] = Diffie-Hellman Group
             * args[3] = IKE Phase I exchange mode (main/aggressive)
             * args[4] = Lifetime Type (secs/mins/hrs)
             * args[5] = Lifetime value
             */

            u4EncrAlgo = CLI_PTR_TO_U4 (args[VPN_INDEX_0]);
            u4HashAlgo = CLI_PTR_TO_U4 (args[VPN_INDEX_1]);
            u4DHGrp = CLI_PTR_TO_U4 (args[VPN_INDEX_2]);
            u4ExchMode = CLI_PTR_TO_U4 (args[VPN_INDEX_3]);
            u4LifeTimeType = CLI_PTR_TO_U4 (args[VPN_INDEX_4]);
            u4LifeTime = CLI_PTR_TO_U4 (args[VPN_INDEX_5]);

            u4PolicyIndex = (UINT4) CLI_GET_VPN_POLICY_ID ();
            VpnGetPolicyByIndex (u4PolicyIndex, pPolicyName);
            i4RetVal = VpnCliSetVpnPolicyIkeProposal (CliHandle,
                                                      pPolicyName, u4EncrAlgo,
                                                      u4HashAlgo, u4DHGrp,
                                                      u4ExchMode,
                                                      u4LifeTimeType,
                                                      u4LifeTime);

            break;

        case CLI_VPN_IPSEC_PROPOSAL:

            /* args[0] = Encryption Algorithm 
             * (des/3-des/aes-128/192/256/aes-ctr-128/192/256)
             * args[1] = Protocol to identify ESP/AH
             * args[2] = Hash Algorithm
             * args[3] = Diffie-Hellman Group
             * args[4] = Lifetime Type (secs/mins/hrs)
             * args[5] = Lifetime value
             */

            u4EncrAlgo = CLI_PTR_TO_U4 (args[VPN_INDEX_0]);
            u4Protocol = CLI_PTR_TO_U4 (args[VPN_INDEX_1]);
            u4HashAlgo = CLI_PTR_TO_U4 (args[VPN_INDEX_2]);
            u4DHGrp = CLI_PTR_TO_U4 (args[VPN_INDEX_3]);
            u4LifeTimeType = CLI_PTR_TO_U4 (args[VPN_INDEX_4]);
            u4LifeTime = CLI_PTR_TO_U4 (args[VPN_INDEX_5]);
            if ((u4EncrAlgo != VPN_ZERO) && (u4Protocol == CLI_VPN_AH))
            {
                CliPrintf (CliHandle, "%% The protocol for encryption"
                           " and authentication must be ESP, if both"
                           " are specified.\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }
            else if ((u4EncrAlgo == CLI_VPN_ESP_NULL) && (u4Protocol
                                                          == VPN_ZERO))
            {
                CliPrintf (CliHandle, "%% NULL encryption alone can NOT "
                           "be supported. Choose a authentication algorithm "
                           "too.\r\n");
                i4RetVal = CLI_FAILURE;
                break;
            }
            if ((u4EncrAlgo == SEC_AESCTR) || (u4EncrAlgo == SEC_AESCTR192)
                || (u4EncrAlgo == SEC_AESCTR256))
            {
                if (u4HashAlgo == VPN_ZERO)
                {
                    CliPrintf (CliHandle,
                               "%% AES-CTR mode should be used with a"
                               " Authentication function\r\n");
                    i4RetVal = CLI_FAILURE;
                    break;

                }
            }
            u4PolicyIndex = (UINT4) CLI_GET_VPN_POLICY_ID ();
            VpnGetPolicyByIndex (u4PolicyIndex, pPolicyName);
            i4RetVal = VpnCliSetVpnPolicyIpsecProposal (CliHandle, pPolicyName,
                                                        u4EncrAlgo, u4Protocol,
                                                        u4HashAlgo, u4DHGrp,
                                                        u4LifeTimeType,
                                                        u4LifeTime);

            break;

        case CLI_VPN_ADD_RA_USER:

            /* args[0] = User Name 
             * args[1] = User Password
             */
            i4Len = (INT4) STRLEN (args[VPN_INDEX_0]);
            if ((pVpnRaUserName = allocmem_octetstring (i4Len + VPN_ONE))
                == NULL)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }
            STRCPY (pVpnRaUserName->pu1_OctetList, args[VPN_INDEX_0]);
            pVpnRaUserName->i4_Length = i4Len;

            i4Len = (INT4) STRLEN (args[VPN_INDEX_1]);
            if ((pVpnRaUserSecret = allocmem_octetstring (i4Len + VPN_ONE))
                == NULL)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }
            STRCPY (pVpnRaUserSecret->pu1_OctetList, args[VPN_INDEX_1]);
            pVpnRaUserSecret->i4_Length = i4Len;

            i4RetVal =
                VpnAddRaUserInfo (CliHandle, pVpnRaUserName, pVpnRaUserSecret);

            break;

        case CLI_VPN_DEL_RA_USER:

            /* args[0] = User Name */

            i4Len = (INT4) STRLEN (args[VPN_INDEX_0]);
            if ((pVpnRaUserName = allocmem_octetstring (i4Len + VPN_ONE))
                == NULL)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }
            MEMSET (pVpnRaUserName->pu1_OctetList, VPN_ZERO,
                    (size_t) (i4Len + VPN_ONE));
            MEMCPY (pVpnRaUserName->pu1_OctetList, args[VPN_INDEX_0], i4Len);
            pVpnRaUserName->i4_Length = i4Len;

            i4RetVal = VpnDelRaUserInfo (CliHandle, pVpnRaUserName);

            break;

        case CLI_VPN_ADD_RA_ADDRESS_POOL:

            /* args[0] = Address Pool Name 
             * args[1] = Starting ipaddress of the pool
             * args[2] = end ipaddress of the pool
             */

            /* Address Pool configuration is blocked for RAVPN Client */
            nmhGetFsVpnRaServer (&i4RAVPNServer);
            if (i4RAVPNServer == FALSE)
            {
                CliPrintf (CliHandle, "% RAVPN Address Pool\
                        cannot be configured for RAVPN Client \r\n");
                i4RetVal = CLI_FAILURE;
                break;

            }

            i4Len = (INT4) STRLEN (args[VPN_INDEX_0]);
            if (i4Len > RA_ADDRESS_POOL_NAME_LENGTH)
            {
                CliPrintf (CliHandle, "%% Address Pool Name length should be "
                           "less than or equal to %d characters\r\n",
                           RA_ADDRESS_POOL_NAME_LENGTH);
                i4RetVal = CLI_FAILURE;
                break;
            }

            if ((pVpnRaAddressPoolName =
                 allocmem_octetstring (i4Len + VPN_ONE)) == NULL)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }

            MEMSET (pVpnRaAddressPoolName->pu1_OctetList,
                    VPN_ZERO, (size_t) (i4Len + VPN_ONE));
            MEMCPY (pVpnRaAddressPoolName->pu1_OctetList,
                    args[VPN_INDEX_0], i4Len);
            pVpnRaAddressPoolName->i4_Length = i4Len;

            u4VpnRaStartIp = *((UINT4 *) (VOID *) args[VPN_INDEX_1]);
            u4VpnRaEndIp = *((UINT4 *) (VOID *) args[VPN_INDEX_2]);

            i4RetVal =
                VpnAddAddressPool (CliHandle, pVpnRaAddressPoolName,
                                   u4VpnRaStartIp, u4VpnRaEndIp);

            break;

        case CLI_VPN_ADD_IPV6_RA_ADDRESS_POOL:

            /* args[0] = Address Pool Name                 * 
             * args[1] = Starting ipv6 address of the pool *
             * args[2] = end ipv6 address of the pool      *
             * args[3] = Prefix length of the ipv6 address */

            /* Address Pool configuration is blocked for RAVPN Client */
            nmhGetFsVpnRaServer (&i4RAVPNServer);
            if (i4RAVPNServer == FALSE)
            {
                CliPrintf (CliHandle, "% RAVPN Address Pool \
                           cannot be configured for RAVPN Client \r\n");
                i4RetVal = CLI_FAILURE;
                break;

            }

            i4Len = (INT4) STRLEN (args[VPN_INDEX_0]);
            if (i4Len > RA_ADDRESS_POOL_NAME_LENGTH)
            {
                CliPrintf (CliHandle, "%% Address Pool Name length should be "
                           "less than or equal to %d characters\r\n",
                           RA_ADDRESS_POOL_NAME_LENGTH);
                i4RetVal = CLI_FAILURE;
                break;
            }

            if ((pVpnRaAddressPoolName =
                 allocmem_octetstring (i4Len + VPN_ONE)) == NULL)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }

            MEMSET (pVpnRaAddressPoolName->pu1_OctetList,
                    VPN_ZERO, (size_t) (i4Len + VPN_ONE));
            MEMCPY (pVpnRaAddressPoolName->pu1_OctetList,
                    args[VPN_INDEX_0], i4Len);
            pVpnRaAddressPoolName->i4_Length = i4Len;

            MEMSET (&StartIpv6Addr, VPN_ZERO, sizeof (tIp6Addr));
            if (INET_ATON6 (args[VPN_INDEX_1], &StartIpv6Addr) == VPN_ZERO)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }

            MEMSET (&EndIpv6Addr, VPN_ZERO, sizeof (tIp6Addr));
            if (INET_ATON6 (args[VPN_INDEX_2], &EndIpv6Addr) == VPN_ZERO)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }

            u4PrefixLen = *((UINT4 *) (VOID *) args[VPN_INDEX_3]);

            i4RetVal = VpnIpv6AddAddressPool (CliHandle, pVpnRaAddressPoolName,
                                              &StartIpv6Addr,
                                              &EndIpv6Addr, u4PrefixLen);

            break;

        case CLI_VPN_DEL_RA_ADDRESS_POOL:

            /* args[0] = Address Pool Name */

            i4Len = (INT4) STRLEN (args[VPN_INDEX_0]);
            if ((pVpnRaAddressPoolName =
                 allocmem_octetstring (i4Len + VPN_ONE)) == NULL)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }
            MEMSET (pVpnRaAddressPoolName->pu1_OctetList,
                    VPN_ZERO, (size_t) (i4Len + VPN_ONE));
            MEMCPY (pVpnRaAddressPoolName->pu1_OctetList,
                    args[VPN_INDEX_0], i4Len);
            pVpnRaAddressPoolName->i4_Length = i4Len;

            i4RetVal = VpnDelAddressPool (CliHandle, pVpnRaAddressPoolName);
            break;

        case CLI_VPN_RA_GLOBAL:

            if (STRCMP (args[VPN_INDEX_0], CLI_RAVPN_SERVER) == VPN_ZERO)
            {
                i4XAuthType = TRUE;
            }
            else if (STRCMP (args[VPN_INDEX_0], CLI_RAVPN_CLIENT) == VPN_ZERO)
            {
                i4XAuthType = FALSE;
            }
            else
            {
                CliPrintf (CliHandle,
                           "Unknown Value %s\r\n", args[VPN_INDEX_0]);
                if (pPolicyName != NULL)
                {
                    free_octetstring (pPolicyName);
                }

                return (CLI_FAILURE);
            }
            i4RetVal = VpnRAGlobalConfig (CliHandle, i4XAuthType);
            break;

        case CLI_VPN_IKE_TRIGGER:

            u4PeerAddress = *((UINT4 *) (VOID *) args[VPN_INDEX_0]);
            i4RetVal = VpnIkeCliTrigger (CliHandle, u4PeerAddress);
            break;

        case CLI_VPN_IKE_IPV6_TRIGGER:

            MEMSET (&Ip6Addr, VPN_ZERO, sizeof (tIp6Addr));
            if (INET_ATON6 (args[VPN_INDEX_0], &Ip6Addr) == VPN_ZERO)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }

            i4RetVal = VpnIkeIpv6CliTrigger (CliHandle, &Ip6Addr);
            break;

        case CLI_VPN_SHOW_VPN_RA_USERS:

            i4RetVal = VpnShowRaUsers (CliHandle);
            break;

        case CLI_VPN_SHOW_VPN_RA_ADDRESSPOOL:

            i4RetVal = VpnShowRaAddressPool (CliHandle);
            break;

        case CLI_VPN_SHOW_GLOB_PKTS_STATS:
            i4RetVal = VpnShowGlobPktsStats (CliHandle);
            break;

        case CLI_VPN_SHOW_GLOB_IKE_STATS:
            i4RetVal = VpnShowGlobIkeStats (CliHandle);
            break;

            /* Certificate Related Changes - Start */
        case CLI_VPN_GEN_KEYPAIR:

            if (STRLEN (args[VPN_INDEX_0]) > VPN_MAX_KEY_NAME_LEN)
            {
                CliPrintf (CliHandle, "%% Max allowed length for RSA/DSA keyid"
                           " is  %d\r\n", VPN_MAX_KEY_NAME_LEN);
                CLI_SET_ERR (VPN_ZERO);
                break;
            }
            u4Keylen = CLI_PTR_TO_U4 (args[VPN_INDEX_1]);
            u4Type = CLI_PTR_TO_U4 (args[VPN_INDEX_2]);
            i4RetVal = VpnIkeGenKeyPair (args[VPN_INDEX_0], u4Keylen, u4Type);
            break;

        case CLI_VPN_IMPORT_KEY:
            /* args[0] - KeyName
             * args[1] - FileName
             * args[2] - Key Type
             */
            i4RetVal = VpnCliIkeImportKey (CliHandle,
                                           (UINT1 *) args[VPN_INDEX_0],
                                           (UINT1 *) args[VPN_INDEX_1],
                                           (INT4)
                                           CLI_PTR_TO_U4 (args[VPN_INDEX_2]));
            break;

        case CLI_VPN_SHOW_KEYS:
            u4Type = CLI_PTR_TO_U4 (args[VPN_INDEX_0]);
            i4RetVal = VpnIkeShowKeys (u4Type);
            break;

        case CLI_VPN_DEL_KEYPAIR:

            i4RetVal = VpnCliIkeDelKeys (CliHandle, args[VPN_INDEX_0]);
            break;

        case CLI_VPN_GEN_CERT_REQ:
            u4Type = CLI_PTR_TO_U4 (args[VPN_INDEX_3]);
            i4RetVal = VpnIkeGenCertReq (args[VPN_INDEX_0],
                                         args[VPN_INDEX_1],
                                         args[VPN_INDEX_2], u4Type);
            break;

        case CLI_VPN_IMPORT_CERT:
            u4EncType = CLI_PTR_TO_U4 (args[VPN_INDEX_1]);
            i4RetVal = VpnCliIkeImportCert (CliHandle,
                                            args[VPN_INDEX_0],
                                            args[VPN_INDEX_2],
                                            (UINT1) u4EncType);
            break;

        case CLI_VPN_SHOW_CERTS:
            i4RetVal = VpnIkeShowCerts (CLI_VPN_CERT_SHOW, args[VPN_INDEX_0]);
            break;

        case CLI_VPN_DEL_CERT:
            i4RetVal =
                VpnCliIkeDelCerts (CliHandle,
                                   CLI_VPN_ERASE_CERT, args[VPN_INDEX_0]);
            break;

        case CLI_VPN_IMPORT_PEER_CERT:
            u4EncType = CLI_PTR_TO_U4 (args[VPN_INDEX_2]);
            u4TrustFlag = CLI_PTR_TO_U4 (args[VPN_INDEX_3]);
            i4RetVal =
                VpnIkeImportPeerCert (args[VPN_INDEX_0],
                                      args[VPN_INDEX_1],
                                      (UINT1) u4EncType, (UINT1) u4TrustFlag);
            break;

        case CLI_VPN_SHOW_PEER_CERTS:
            i4RetVal = VpnIkeShowCerts (CLI_VPN_PEER_CERT_SHOW,
                                        args[VPN_INDEX_0]);
            break;

        case CLI_VPN_DEL_PEER_CERT:
            i4RetVal = VpnIkeDelCerts (CLI_VPN_ERASE_PEER_CERT,
                                       args[VPN_INDEX_0]);
            break;

        case CLI_VPN_IMPORT_CA_CERT:
            u4EncType = CLI_PTR_TO_U4 (args[VPN_INDEX_2]);
            i4RetVal =
                VpnCliIkeImportCACert (CliHandle,
                                       args[VPN_INDEX_0],
                                       args[VPN_INDEX_1], (UINT1) u4EncType);
            break;

        case CLI_VPN_SHOW_CA_CERTS:
            i4RetVal = VpnIkeShowCerts (CLI_VPN_CA_CERT_SHOW,
                                        args[VPN_INDEX_0]);
            break;

        case CLI_VPN_DEL_CA_CERT:
            i4RetVal =
                VpnCliIkeDelCerts (CliHandle,
                                   CLI_VPN_ERASE_CA_CERT, args[VPN_INDEX_0]);
            break;

        case CLI_VPN_CERT_MAP_PEER:
            i4RetVal = VpnIkeCertMapPeer (args[VPN_INDEX_0],
                                          args[VPN_INDEX_1], args[VPN_INDEX_2]);
            break;

        case CLI_VPN_SHOW_CERT_MAP:
            i4RetVal = VpnIkeShowCerts (CLI_VPN_CERT_MAP_SHOW, NULL);
            break;

        case CLI_VPN_DEL_CERT_MAP:
            i4RetVal = VpnIkeDelCerts (CLI_VPN_ERASE_CERT_MAP,
                                       args[VPN_INDEX_0]);
            break;

        case CLI_VPN_SAVE_CERT:
            i4RetVal = VpnIkeSaveCerts ();
            break;
            /* Certificate Related Changes - End */

        case CLI_VPN_IKE_DEBUG:
            i4RetVal =
                VpnCliIkeTrace (CliHandle,
                                CLI_ENABLE, CLI_PTR_TO_I4 (args[VPN_INDEX_0]));
            break;

        case CLI_VPN_IKE_NO_DEBUG:
            i4RetVal =
                VpnCliIkeTrace (CliHandle, CLI_DISABLE,
                                CLI_PTR_TO_I4 (args[VPN_INDEX_0]));
            break;

        case CLI_VPN_IPSEC_DEBUG:
            i4RetVal =
                VpnCliIpsecTrace (CliHandle, CLI_ENABLE,
                                  CLI_PTR_TO_I4 (args[VPN_INDEX_0]));
            break;

        case CLI_VPN_IPSEC_NO_DEBUG:
            i4RetVal =
                VpnCliIpsecTrace (CliHandle, CLI_DISABLE,
                                  CLI_PTR_TO_I4 (args[VPN_INDEX_0]));
            break;
        default:
            break;

    }

    if (pPolicyName != NULL)
    {
        free_octetstring (pPolicyName);
    }

    if (pVpnRaUserName != NULL)
    {
        free_octetstring (pVpnRaUserName);
    }

    if (pVpnRaUserSecret != NULL)
    {
        free_octetstring (pVpnRaUserSecret);
    }

    if (pVpnRaAddressPoolName != NULL)
    {
        free_octetstring (pVpnRaAddressPoolName);
    }

    if (pSrcPortRange != NULL)
    {
        free_octetstring (pSrcPortRange);
    }

    if (pDstPortRange != NULL)
    {
        free_octetstring (pDstPortRange);
    }

    if (pu1SecAssocAhKey != NULL)
    {
        MemReleaseMemBlock (VPN_WEB_POLL_BUFF_PID, (UINT1 *) pu1SecAssocAhKey);
    }
    if (i4RetVal == CLI_FAILURE)
    {
        /* Display the error message */
        if (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS)
        {
            VpnDisplayError (CliHandle, (INT4) u4ErrCode);
        }
        CLI_SET_ERR (VPN_ERROR);
    }
    UNUSED_PARAM (pu1Inst);
    CLI_SET_CMD_STATUS ((UINT4) i4RetVal);
    VpnDsUnLock ();
    CliUnRegisterLock (CliHandle);

    return i4RetVal;
}

/*****************************************************************************
 * VpnCreatePolicy : Creates a VPN policy with the specified policy name     *
 *****************************************************************************/
INT4
VpnCreatePolicy (tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE * pPolicyName)
{
    UINT4               u4SnmpErrorStatus = VPN_ZERO;
    INT4                i4RowStatus = VPN_ZERO;
    if (nmhTestv2FsVpnPolicyRowStatus (&u4SnmpErrorStatus, pPolicyName,
                                       CREATE_AND_GO) == SNMP_FAILURE)
    {
        switch (u4SnmpErrorStatus)
        {
            case SNMP_ERR_WRONG_LENGTH:
                CliPrintf (CliHandle,
                           "%% Policy name must be of minimum length %d "
                           "and maximum length %d\r\n",
                           VPN_MIN_POLICY_NAME_LENGTH,
                           VPN_MAX_POLICY_NAME_LENGTH);
                break;

            case SNMP_ERR_INCONSISTENT_NAME:
                if (nmhGetFsVpnPolicyRowStatus (pPolicyName, &i4RowStatus) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "%% Invalid policy name. NOTE: Policy name "
                               "\"all\" is reserved.\r\n");
                    break;
                }
                if (i4RowStatus != ACTIVE)
                {
                    /* Entry already exists.. just change the mode */
                    return CLI_SUCCESS;
                }
                else
                {
                    /* Policy is already applied so can not be changed now */
                    CliPrintf (CliHandle, "%% Updation of ACTIVE"
                               " policies is NOT allowed !\r\n ");
                    break;
                }
            default:
                break;
        }

        return CLI_FAILURE;
    }

    if (nmhSetFsVpnPolicyRowStatus (pPolicyName, CREATE_AND_GO) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_MEM_ALLOC_FAIL);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
** VpnDeletePolicy : Deletes a VPN policy with the specified policy name      *
*******************************************************************************/

INT4
VpnDeletePolicy (tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE * pPolicyName)
{
    UINT4               u4SnmpErrorStatus = VPN_ZERO;
    if (pPolicyName->i4_Length > (VPN_MAX_POLICY_NAME_LENGTH + VPN_ONE))
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsVpnPolicyRowStatus (&u4SnmpErrorStatus, pPolicyName,
                                       DESTROY) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_NO_POLICY);
        return CLI_FAILURE;
    }
    if (nmhSetFsVpnPolicyRowStatus (pPolicyName, DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Unable to delete the policy\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function    : VpnDeletePolicyAll
 * Description : Deletes all vpn policies exist in the system
 * Input(s)    : CliHandle
 * OutPut(s)   : None
 * Returns     : CLI_SUCCESS / CLI_FAILURE
 *****************************************************************************/
PRIVATE INT4
VpnDeletePolicyAll (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE *pPolicyName = NULL;

    if (((pPolicyName =
          allocmem_octetstring (VPN_MAX_POLICY_NAME_LENGTH + VPN_ONE)) == NULL))
    {
        CLI_SET_ERR (CLI_VPN_MEM_ALLOC_FAIL);
        return (CLI_FAILURE);
    }

    while (nmhGetFirstIndexFsVpnTable (pPolicyName) == SNMP_SUCCESS)
    {
        if (nmhSetFsVpnPolicyRowStatus (pPolicyName, DESTROY) == SNMP_FAILURE)
        {
            pPolicyName->pu1_OctetList[pPolicyName->i4_Length] = '\0';
            CliPrintf (CliHandle,
                       "\r\n%% Unable to delete the policy: %s "
                       "and the policies created after the same.\r\n",
                       pPolicyName->pu1_OctetList);
            break;
        }
    }
    if (pPolicyName != NULL)
    {
        free_octetstring (pPolicyName);
    }

    return (CLI_SUCCESS);
}

/**********************************************************************
 * VpnSetEncryptType: Sets the Type of algorithm to used for encryption
 *                    and decryption
 *  Input :Policy Name and the Type of algorithm to be used
 **********************************************************************/
INT4
VpnSetEncryptType (tCliHandle CliHandle,
                   tSNMP_OCTET_STRING_TYPE * pPolicyName, UINT4 u4Type)
{
    UINT4               u4SnmpErrorStatus = VPN_ZERO;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsVpnCertAlgoType (&u4SnmpErrorStatus,
                                    pPolicyName, (INT4) u4Type) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_VPN_MODE_ALGO);
        return CLI_FAILURE;
    }
    nmhSetFsVpnCertAlgoType (pPolicyName, (INT4) u4Type);

    return CLI_SUCCESS;
}

/*******************************************************************
 * VpnSetPolicyType: Sets the Type of policy for the corresponding
 *                   VPN Policy
 *  Input :Policy Name and the corresponding mode of policy
 *******************************************************************/
INT4
VpnSetPolicyType (tCliHandle CliHandle,
                  tSNMP_OCTET_STRING_TYPE * pPolicyName, UINT4 u4Index)
{
    UINT4               u4SnmpErrorStatus = VPN_ZERO;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsVpnPolicyType
        (&u4SnmpErrorStatus, pPolicyName, (INT4) u4Index) == SNMP_FAILURE)
    {
        if (u4SnmpErrorStatus == SNMP_ERR_GEN_ERR)
        {
            CLI_SET_ERR (CLI_VPN_ERR_VPN_MODE_CONFIG_ALGO);
            return CLI_FAILURE;
        }
        else
        {
            CLI_SET_ERR (CLI_VPN_ERR_VPN_MODE);
            return CLI_FAILURE;
        }
    }
    nmhSetFsVpnPolicyType (pPolicyName, (INT4) u4Index);

    return CLI_SUCCESS;
}

/****************************************************************/
INT4
VpnSetPolicyMode (tCliHandle CliHandle,
                  tSNMP_OCTET_STRING_TYPE * pPolicyName, UINT4 u4Mode)
{
    UINT4               u4SnmpErrorStatus = VPN_ZERO;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsVpnMode (&u4SnmpErrorStatus, pPolicyName, (INT4) u4Mode)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_IPSEC_MODE);
        return CLI_FAILURE;
    }
    nmhSetFsVpnMode (pPolicyName, (INT4) u4Mode);

    return CLI_SUCCESS;
}

/*******************************************************************************
***  VpnSetPeerIp : Sets the Peer Ip  for the corresponding VPN Policy         *
***  Input :  Policy Name and the corresponding PeerIp                         *
*******************************************************************************/
INT4
VpnSetPeerIp (tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE * pPolicyName,
              UINT4 u4PeerAddress)
{
    UINT4               u4SnmpErrorStatus = VPN_ZERO;
    UINT1               au1Addr[VPN_IP_ADDR_LEN] = { VPN_ZERO };
    tSNMP_OCTET_STRING_TYPE RemoteTunTermAddr;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsVpnTunTermAddrType (&u4SnmpErrorStatus, pPolicyName,
                                       IPVX_ADDR_FMLY_IPV4) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_PEER_INFO);
        return CLI_FAILURE;
    }

    nmhSetFsVpnTunTermAddrType (pPolicyName, IPVX_ADDR_FMLY_IPV4);

    MEMSET (au1Addr, VPN_ZERO, VPN_IP_ADDR_LEN);
    MEMCPY (au1Addr, &u4PeerAddress, sizeof (UINT4));
    RemoteTunTermAddr.pu1_OctetList = au1Addr;
    RemoteTunTermAddr.i4_Length = sizeof (UINT4);

    if (nmhTestv2FsVpnRemoteTunTermAddr (&u4SnmpErrorStatus, pPolicyName,
                                         &RemoteTunTermAddr) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_PEER_INFO);
        return CLI_FAILURE;
    }
    nmhSetFsVpnRemoteTunTermAddr (pPolicyName, &RemoteTunTermAddr);
    return CLI_SUCCESS;
}

/*******************************************************************************
***  VpnSetIkeVersion : Sets the IKE version for the corresponding policy      *
***  Input : Policy Name and IKE version                                       *
*******************************************************************************/
INT4
VpnSetIkeVersion (tCliHandle CliHandle,
                  tSNMP_OCTET_STRING_TYPE * pPolicyName, UINT4 u4VpnIkeVersion)
{
    UINT4               u4ErrCode = VPN_ZERO;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsVpnIkeVersion
        (&u4ErrCode, pPolicyName, (INT4) u4VpnIkeVersion) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_IKE_VERSION_SET_FAILED);
        return CLI_FAILURE;
    }
    if (nmhSetFsVpnIkeVersion (pPolicyName, (INT4) u4VpnIkeVersion) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_IKE_VERSION_SET_FAILED);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
  VpnSetAccessParams  : Sets the Access List params
        for the corresponding VPN Policy 
  Input :  Policy Name and the corresponding access list params 
 ******************************************************************************/
INT4
VpnSetAccessParams (tSNMP_OCTET_STRING_TYPE * pPolicyName,
                    tVpnNetwork * pLocalNet, tVpnNetwork * pRemoteNet,
                    UINT4 u4Action, UINT4 u4Protocol,
                    tSNMP_OCTET_STRING_TYPE * pSrcPortRange,
                    tSNMP_OCTET_STRING_TYPE * pDstPortRange)
{
    UINT4               u4SnmpErrorStatus = VPN_ZERO;
    INT4                i4AddrType = IPVX_ADDR_FMLY_IPV4;
    UINT1               au1IpAddr[VPN_IP_ADDR_LEN] = { VPN_ZERO };
    tSNMP_OCTET_STRING_TYPE Addr;

    if (VpnAccessListCheckIpMask (pLocalNet->u4AddrPrefixLen,
                                  pLocalNet->IpAddr.uIpAddr.Ip4Addr,
                                  pRemoteNet->u4AddrPrefixLen,
                                  pRemoteNet->IpAddr.uIpAddr.Ip4Addr) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsVpnProtectNetworkType (&u4SnmpErrorStatus, pPolicyName,
                                          i4AddrType) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_ACL_PARAM);
        return CLI_FAILURE;
    }
    nmhSetFsVpnProtectNetworkType (pPolicyName, IPVX_ADDR_FMLY_IPV4);

    MEMSET (au1IpAddr, VPN_ZERO, sizeof (tIp4Addr));
    MEMCPY (au1IpAddr, &(pLocalNet->IpAddr.uIpAddr.Ip4Addr), sizeof (tIp4Addr));
    Addr.pu1_OctetList = au1IpAddr;
    Addr.i4_Length = sizeof (tIp4Addr);

    if (nmhTestv2FsVpnLocalProtectNetwork (&u4SnmpErrorStatus, pPolicyName,
                                           &Addr) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_ACL_PARAM);
        return CLI_FAILURE;
    }
    nmhSetFsVpnLocalProtectNetwork (pPolicyName, &Addr);

    if (nmhTestv2FsVpnLocalProtectNetworkPrefixLen (&u4SnmpErrorStatus,
                                                    pPolicyName,
                                                    pLocalNet->
                                                    u4AddrPrefixLen) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_ACL_PARAM);
        return CLI_FAILURE;
    }
    nmhSetFsVpnLocalProtectNetworkPrefixLen (pPolicyName,
                                             pLocalNet->u4AddrPrefixLen);

    MEMSET (au1IpAddr, VPN_ZERO, sizeof (tIp4Addr));
    MEMCPY (au1IpAddr, &(pRemoteNet->IpAddr.uIpAddr.Ip4Addr),
            sizeof (tIp4Addr));
    Addr.pu1_OctetList = au1IpAddr;
    Addr.i4_Length = sizeof (tIp4Addr);

    if (nmhTestv2FsVpnRemoteProtectNetwork
        (&u4SnmpErrorStatus, pPolicyName, &Addr) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_ACL_PARAM);
        return CLI_FAILURE;
    }
    nmhSetFsVpnRemoteProtectNetwork (pPolicyName, &Addr);

    if (nmhTestv2FsVpnRemoteProtectNetworkPrefixLen (&u4SnmpErrorStatus,
                                                     pPolicyName,
                                                     pRemoteNet->
                                                     u4AddrPrefixLen) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_ACL_PARAM);
        return CLI_FAILURE;
    }

    if (nmhSetFsVpnRemoteProtectNetworkPrefixLen
        (pPolicyName, pRemoteNet->u4AddrPrefixLen) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_ACL_PARAM);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsVpnPolicyFlag (&u4SnmpErrorStatus, pPolicyName,
                                  (INT4) u4Action) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_ACL_PARAM);
        return CLI_FAILURE;
    }
    nmhSetFsVpnPolicyFlag (pPolicyName, (INT4) u4Action);

    if (nmhTestv2FsVpnProtocol (&u4SnmpErrorStatus, pPolicyName,
                                (INT4) u4Protocol) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_ACL_PARAM);
        return CLI_FAILURE;
    }
    nmhSetFsVpnProtocol (pPolicyName, (INT4) u4Protocol);

    if (pSrcPortRange != NULL)
    {
        if (nmhTestv2FsVpnIkeSrcPortRange (&u4SnmpErrorStatus, pPolicyName,
                                           pSrcPortRange) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_VPN_ERR_INVALID_PORT_RANGE);
            return CLI_FAILURE;
        }

        if (nmhSetFsVpnIkeSrcPortRange (pPolicyName, pSrcPortRange) ==
            SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_VPN_ERR_INVALID_PORT_RANGE);
            return CLI_FAILURE;
        }
    }

    if (pDstPortRange != NULL)
    {
        if (nmhTestv2FsVpnIkeDstPortRange (&u4SnmpErrorStatus, pPolicyName,
                                           pDstPortRange) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_VPN_ERR_INVALID_PORT_RANGE);
            return CLI_FAILURE;
        }
        if (nmhSetFsVpnIkeDstPortRange (pPolicyName, pDstPortRange) ==
            SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_VPN_ERR_INVALID_PORT_RANGE);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/******************************************************************************
**  VpnSetCryptoParams  : Sets the Crypto  List params  for the               *
**                        corresponding VPN Policy                            *
**  Input :  Policy Name and the corresponding crypto  list params            *
*******************************************************************************/
INT4
VpnSetCryptoParams (tCliHandle CliHandle,
                    tSNMP_OCTET_STRING_TYPE * pPolicyName,
                    UINT4 u4AuthProto, UINT4 u4AuthAlgo,
                    UINT1 *au1SecAssocAhKey, UINT4 u4InBoundSpi,
                    UINT4 u4OutBoundSpi, INT4 i4EspProto,
                    UINT1 *au1SecAssocEspKey, UINT4 u4AntiReplay)
{
    UINT4               u4SnmpErrorStatus = VPN_ZERO;
    INT4                i4VpnPolicyType = VPN_ZERO;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    INT4                i4RetVal = VPN_ZERO;

    UNUSED_PARAM (CliHandle);

    do
    {
        nmhGetFsVpnPolicyType (pPolicyName, &i4VpnPolicyType);
        if (i4VpnPolicyType != VPN_IPSEC_MANUAL)
        {
            CLI_SET_ERR (CLI_VPN_ERR_POLICY_NOT_MANUAL);
            break;
        }

        if (u4AuthProto != VPN_ZERO)
        {
            if (u4AuthProto == CLI_VPN_AH)
            {
                nmhSetFsVpnSecurityProtocol (pPolicyName, SEC_AH);
            }
            else
            {
                nmhSetFsVpnSecurityProtocol (pPolicyName, SEC_ESP);
            }

            if (nmhTestv2FsVpnAuthAlgo
                (&u4SnmpErrorStatus, pPolicyName,
                 (INT4) u4AuthAlgo) == SNMP_FAILURE)
            {
                i4RetVal = CLI_GET_ERR (&u4SnmpErrorStatus);
                if (VPN_ZERO == u4SnmpErrorStatus)
                {
                    CLI_SET_ERR (CLI_VPN_ERR_MAN_AUTH_PARAM);
                }
                break;
            }
            nmhSetFsVpnAuthAlgo (pPolicyName, (INT4) u4AuthAlgo);

            OctetStr.i4_Length = (INT4) STRLEN (au1SecAssocAhKey);
            OctetStr.pu1_OctetList = au1SecAssocAhKey;
            if (nmhTestv2FsVpnAhKey
                (&u4SnmpErrorStatus, pPolicyName, &OctetStr) == SNMP_FAILURE)
            {
                if (u4SnmpErrorStatus == SNMP_ERR_WRONG_LENGTH)
                {
                    CLI_SET_ERR (CLI_VPN_ERR_MAN_AUTH_KEY_LEN);
                }
                else
                {
                    CLI_SET_ERR (CLI_VPN_ERR_MAN_KEY);
                }
                break;
            }
            nmhSetFsVpnAhKey (pPolicyName, &OctetStr);
        }
        else
        {
            /* Set the auth params to 0 if they are not configured */
            nmhSetFsVpnSecurityProtocol (pPolicyName, VPN_ZERO);
            nmhSetFsVpnAuthAlgo (pPolicyName, VPN_ZERO);
        }

        if (i4EspProto != VPN_ZERO)
        {
            if (nmhTestv2FsVpnEncrAlgo
                (&u4SnmpErrorStatus, pPolicyName, i4EspProto) == SNMP_FAILURE)
            {
                i4RetVal = CLI_GET_ERR (&u4SnmpErrorStatus);
                UNUSED_PARAM (i4RetVal);
                if (VPN_ZERO == u4SnmpErrorStatus)
                {
                    CLI_SET_ERR (CLI_VPN_ERR_MAN_ENCRY_PARAM);
                }
                break;
            }
            nmhSetFsVpnEncrAlgo (pPolicyName, i4EspProto);

            OctetStr.i4_Length = (INT4) STRLEN (au1SecAssocEspKey);
            OctetStr.pu1_OctetList = au1SecAssocEspKey;
            if (nmhTestv2FsVpnEspKey
                (&u4SnmpErrorStatus, pPolicyName, &OctetStr) == SNMP_FAILURE)
            {
                if (SNMP_ERR_WRONG_LENGTH == u4SnmpErrorStatus)
                {
                    CLI_SET_ERR (CLI_VPN_ERR_MAN_ENCRY_KEY_LEN);
                }
                else if (SNMP_ERR_WRONG_VALUE == u4SnmpErrorStatus)
                {
                    CLI_SET_ERR (CLI_VPN_ERR_MAN_TDES_KEY);
                }
                else
                {
                    CLI_SET_ERR (CLI_VPN_ERR_MAN_KEY);
                }
                break;
            }
            nmhSetFsVpnEspKey (pPolicyName, &OctetStr);
        }
        else if (u4AuthProto == CLI_VPN_ESP)
        {
            /* Set the encryption to NULL if it is not configured and
             * the ESP authentication is choosen.
             */
            nmhSetFsVpnEncrAlgo (pPolicyName, VPN_NULLESPALGO);
        }
        else
        {
            /* Reset to 0 if existing policy is updated as auth alone */
            nmhSetFsVpnEncrAlgo (pPolicyName, VPN_ZERO);
        }

        if (nmhTestv2FsVpnInboundSpi
            (&u4SnmpErrorStatus, pPolicyName,
             (INT4) u4InBoundSpi) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_VPN_ERR_MAN_SPI_PARAM);
            break;
        }
        nmhSetFsVpnInboundSpi (pPolicyName, (INT4) u4InBoundSpi);

        if (nmhTestv2FsVpnOutboundSpi
            (&u4SnmpErrorStatus, pPolicyName,
             (INT4) u4OutBoundSpi) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_VPN_ERR_MAN_SPI_PARAM);
            break;
        }
        nmhSetFsVpnOutboundSpi (pPolicyName, (INT4) u4OutBoundSpi);

        nmhSetFsVpnAntiReplay (pPolicyName, (INT4) u4AntiReplay);

        return CLI_SUCCESS;
    }
    while (VPN_FALSE);

    return CLI_FAILURE;
}

/*****************************************************************************
 *  VpnSetPolicyInterface  : Sets the Interface params for the corresponding
 *                           VPN Policy
 *  Input :  Policy Name and the corresponding Interface  params
 *****************************************************************************/
INT4
VpnSetPolicyInterface (tCliHandle CliHandle,
                       tSNMP_OCTET_STRING_TYPE * pPolicyName, UINT4 u4IfIndex)
{
    UINT4               u4ErrorStatus = VPN_ZERO;
    INT4                i4Value = VPN_ZERO;

    if (nmhGetFsVpnPolicyRowStatus (pPolicyName, &i4Value) == SNMP_SUCCESS)
    {
        if (i4Value == ACTIVE)
        {
            nmhGetFsVpnPolicyIntfIndex (pPolicyName, &i4Value);
            if (i4Value != (INT4) u4IfIndex)
            {
                CliPrintf (CliHandle, "%% Policy already active, One "
                           "policy can be bound to one interface only\r\n");
            }
            /* Policy is already active, nothing to do!! */
            return (CLI_SUCCESS);
        }

        if (nmhTestv2FsVpnPolicyIntfIndex
            (&u4ErrorStatus, pPolicyName, (INT4) u4IfIndex) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }
    else
    {
        CLI_SET_ERR (CLI_VPN_ERR_NO_POLICY);
        return (CLI_FAILURE);
    }
    nmhSetFsVpnPolicyIntfIndex (pPolicyName, (INT4) u4IfIndex);

    if (nmhTestv2FsVpnPolicyRowStatus (&u4ErrorStatus, pPolicyName,
                                       ACTIVE) == SNMP_FAILURE)
    {
        UINT4               u4Errcode = VPN_ZERO;
        i4Value = CLI_GET_ERR (&u4Errcode);
        nmhSetFsVpnPolicyIntfIndex (pPolicyName, VPN_ZERO);
        CLI_SET_ERR (u4Errcode);
        return (CLI_FAILURE);
    }

    if (nmhSetFsVpnPolicyRowStatus (pPolicyName, ACTIVE) == SNMP_FAILURE)
    {
        i4Value = CLI_GET_ERR (&u4ErrorStatus);
        if (u4ErrorStatus == VPN_ZERO)
        {
            CliPrintf (CliHandle, "%% Unable to make the policy active\r\n");
        }
        nmhSetFsVpnPolicyIntfIndex (pPolicyName, VPN_ZERO);
        CLI_SET_ERR (u4ErrorStatus);
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * VpnShowPolicy : Displays the vpn parameters based on policy 
 ***************************************************************************/
INT4
VpnShowPolicy (tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE * pPolicyName)
{
    tVpnPolicy         *pVpnPolicy = NULL;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    INT4                i4RetVal = CLI_SUCCESS;
    INT4                i4Count = VPN_ZERO;
    INT4                i4ActiveCount = VPN_ZERO;

    if (pPolicyName == NULL)
    {
        TMO_SLL_Scan (&VpnPolicyList, pVpnPolicy, tVpnPolicy *)
        {
            i4Count++;
            OctetStr.i4_Length = (INT4) STRLEN (pVpnPolicy->au1VpnPolicyName);
            OctetStr.pu1_OctetList = pVpnPolicy->au1VpnPolicyName;
            i4RetVal = VpnShowPolicyParms (CliHandle, &OctetStr);
            if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
            {
                i4ActiveCount++;
            }
        }
        CliPrintf (CliHandle, "\r\nNo.of ACTIVE VPN policies      = %d\r\n",
                   i4ActiveCount);
        CliPrintf (CliHandle, "No.of VPN policies configured  = %d\r\n",
                   i4Count);
    }
    else
    {
        i4RetVal = VpnShowPolicyParms (CliHandle, pPolicyName);
        return i4RetVal;
    }

    return i4RetVal;
}

/******************************************************************************
 * VpnShowPolicyParms : Displays all the parameters specific to the particular
 *                      interface
 * Input : Policy name for which the parameters has to be displayed
 * ****************************************************************************/

INT4
VpnShowPolicyParms (tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE * pPolicyName)
{
    CHR1               *pu1String = NULL;
    tVpnPolicy         *pVpnPolicy = NULL;
    INT1                ai1IfName[CFA_MAX_PORT_NAME_LENGTH] = { VPN_ZERO };
    UINT1               au1TempStr[VPN_MAX_NAME_LENGTH + VPN_ONE]
        = { VPN_ZERO };
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT4               u4RetVal = VPN_ZERO;
    INT4                i4Value = VPN_ZERO;
    UINT1               u1AdminStatus = VPN_ZERO;
    INT4                i4PolicyType = VPN_ZERO;
    UINT4               u4IpAddr = VPN_ZERO;
    INT4                i4RetVal = VPN_ZERO;
    UINT1               au1IpAddr[VPN_IPV6_ADDR_LEN + VPN_ONE] = { VPN_ZERO };
    INT4                i4AddrType = VPN_ZERO;
    tIp6Addr            Ip6Addr;

    pVpnPolicy =
        VpnSnmpGetPolicy (pPolicyName->pu1_OctetList, pPolicyName->i4_Length);
    if (pVpnPolicy == NULL)
    {
        CLI_SET_ERR (CLI_VPN_ERR_NO_POLICY);
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\n\t\t\t   VPN Policy Parameters");
    CliPrintf (CliHandle, "\r\n\t\t\t -------------------------");
    CliPrintf (CliHandle, "\r\n Policy Name                      : %s",
               pVpnPolicy->au1VpnPolicyName);
    CliPrintf (CliHandle, "\r\n Policy Status                    : ");
    if (pVpnPolicy->u1VpnPolicyRowStatus == ACTIVE)
    {
        CliPrintf (CliHandle, "Active");
    }
    else
    {
        CliPrintf (CliHandle, "Inactive");
    }

    CliPrintf (CliHandle, "\r\n Policy Type                      : ");
    nmhGetFsVpnPolicyType (pPolicyName, &i4PolicyType);
    if (i4PolicyType == VPN_IPSEC_MANUAL)
    {
        CliPrintf (CliHandle, "IPSec Manual");
    }
    else if (i4PolicyType == VPN_IKE_PRESHAREDKEY)
    {
        CliPrintf (CliHandle, "IKE Pre-shared");
    }
    else if (i4PolicyType == VPN_XAUTH)
    {
        CliPrintf (CliHandle, "IKE XAUTH");
    }
    else if (i4PolicyType == VPN_IKE_RA_PRESHAREDKEY)
    {
        CliPrintf (CliHandle, "IKE RA Pre-Shared");
    }
    else if (i4PolicyType == VPN_IKE_CERTIFICATE)
    {
        CliPrintf (CliHandle, "IKE x509 Certificate");
    }
    else if (i4PolicyType == VPN_IKE_RA_CERT)
    {
        CliPrintf (CliHandle, "IKE RA x509 Certificate");
    }
    else if (i4PolicyType == VPN_IKE_XAUTH_CERT)
    {
        CliPrintf (CliHandle, "IKE Xauth x509 Certificate");
    }
    else
    {
        CliPrintf (CliHandle, "Unknown");
    }

    /* Manual keying doesnt have IKE */
    if (i4PolicyType != VPN_IPSEC_MANUAL)
    {
        CliPrintf (CliHandle, "\r\n Ike Version                      : ");

        if (pVpnPolicy->u1VpnIkeVer == CLI_VPN_IKE_V1)
        {
            CliPrintf (CliHandle, "v1");
        }
        else
        {
            CliPrintf (CliHandle, "v2");
        }
    }

    MEMSET (au1IpAddr, VPN_ZERO, (VPN_IPV6_ADDR_LEN + VPN_ONE));
    OctetStr.pu1_OctetList = au1IpAddr;
    OctetStr.i4_Length = (VPN_IPV6_ADDR_LEN + VPN_ONE);
    nmhGetFsVpnLocalProtectNetwork (pPolicyName, &OctetStr);

    nmhGetFsVpnProtectNetworkType (pPolicyName, &i4AddrType);
    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&u4IpAddr, OctetStr.pu1_OctetList, sizeof (UINT4));
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr);
        CliPrintf (CliHandle, "\r\n Local & Remote Protected N/W's   : %s",
                   pu1String);
        nmhGetFsVpnLocalProtectNetworkPrefixLen (pPolicyName, &u4RetVal);
        CliPrintf (CliHandle, "/%d", u4RetVal);
    }
    else if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (&Ip6Addr, OctetStr.pu1_OctetList, sizeof (tIp6Addr));
        CliPrintf (CliHandle, "\r\n Local & Remote Protected N/W's   : %s",
                   Ip6PrintAddr (&Ip6Addr));
        nmhGetFsVpnLocalProtectNetworkPrefixLen (pPolicyName, &u4RetVal);
        CliPrintf (CliHandle, "/%d", u4RetVal);
    }
    else
    {
        CliPrintf (CliHandle, "\r\n Local & Remote Protected N/W's   : None");
    }

    MEMSET (au1IpAddr, VPN_ZERO, (VPN_IPV6_ADDR_LEN + VPN_ONE));
    OctetStr.i4_Length = (VPN_IPV6_ADDR_LEN + VPN_ONE);
    nmhGetFsVpnRemoteProtectNetwork (pPolicyName, &OctetStr);

    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&u4IpAddr, OctetStr.pu1_OctetList, sizeof (UINT4));
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr);
        CliPrintf (CliHandle, " <-- --> %s", pu1String);
        nmhGetFsVpnRemoteProtectNetworkPrefixLen (pPolicyName, &u4RetVal);
        CliPrintf (CliHandle, "/%d", u4RetVal);
    }
    else if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (&Ip6Addr, OctetStr.pu1_OctetList, sizeof (tIp6Addr));
        CliPrintf (CliHandle, " <-- --> %s", Ip6PrintAddr (&Ip6Addr));
        nmhGetFsVpnRemoteProtectNetworkPrefixLen (pPolicyName, &u4RetVal);
        CliPrintf (CliHandle, "/%d", u4RetVal);
    }
    else
    {
        CliPrintf (CliHandle, " <-- --> None");
    }

    CliPrintf (CliHandle, "\r\n Local & Remote Port Range        : ");

    CliPrintf (CliHandle, "%d-%d",
               pVpnPolicy->LocalProtectNetwork.u2StartPort,
               pVpnPolicy->LocalProtectNetwork.u2EndPort);
    CliPrintf (CliHandle, " <-- --> ");
    CliPrintf (CliHandle, "%d-%d",
               pVpnPolicy->RemoteProtectNetwork.u2StartPort,
               pVpnPolicy->RemoteProtectNetwork.u2EndPort);

    CliPrintf (CliHandle, "\r\n Security Mode                    : ");
    nmhGetFsVpnMode (pPolicyName, &i4Value);
    if (i4Value == CLI_VPN_TRANSPORT)
    {
        CliPrintf (CliHandle, "Transport");
    }
    else if (i4Value == CLI_VPN_TUNNEL)
    {
        CliPrintf (CliHandle, "Tunnel");
    }
    else
    {
        CliPrintf (CliHandle, "Unknown");
    }

    MEMSET (au1IpAddr, VPN_ZERO, (VPN_IPV6_ADDR_LEN + VPN_ONE));
    OctetStr.pu1_OctetList = au1IpAddr;
    OctetStr.i4_Length = (VPN_IPV6_ADDR_LEN + VPN_ONE);
    nmhGetFsVpnLocalTunTermAddr (pPolicyName, &OctetStr);

    nmhGetFsVpnTunTermAddrType (pPolicyName, &i4AddrType);
    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&u4IpAddr, OctetStr.pu1_OctetList, sizeof (UINT4));
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr);
        CliPrintf (CliHandle, "\r\n Local & Remote Tunnel Term Addr  : %s",
                   pu1String);
    }
    else if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (&Ip6Addr, OctetStr.pu1_OctetList, sizeof (tIp6Addr));
        CliPrintf (CliHandle, "\r\n Local & Remote Tunnel Term Addr  : %s",
                   Ip6PrintAddr (&Ip6Addr));
    }
    else
    {
        CliPrintf (CliHandle, "\r\n Local & Remote Tunnel Term Addr  : None");
    }

    MEMSET (au1IpAddr, VPN_ZERO, (VPN_IPV6_ADDR_LEN + VPN_ONE));
    OctetStr.pu1_OctetList = au1IpAddr;
    OctetStr.i4_Length = (VPN_IPV6_ADDR_LEN + VPN_ONE);
    nmhGetFsVpnRemoteTunTermAddr (pPolicyName, &OctetStr);

    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&u4IpAddr, OctetStr.pu1_OctetList, sizeof (UINT4));
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr);
        CliPrintf (CliHandle, " <== ==> %s", pu1String);
    }
    else if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (&Ip6Addr, OctetStr.pu1_OctetList, sizeof (tIp6Addr));
        CliPrintf (CliHandle, " <== ==> %s", Ip6PrintAddr (&Ip6Addr));
    }
    else
    {
        CliPrintf (CliHandle, " <== ==> None");
    }

    nmhGetFsVpnPolicyIntfIndex (pPolicyName, (INT4 *) &u4RetVal);
    if (u4RetVal == VPN_ZERO)
    {
        CliPrintf (CliHandle, "\r\n Interface Name                   : %s",
                   "Not Configured");
    }
    else
    {
        CfaCliGetIfName (u4RetVal, ai1IfName);
        CliPrintf (CliHandle, "\r\n Interface Name                   : %s",
                   ai1IfName);

        /* To display the VPN interface UP/DOWN */
        CfaApiGetIfAdminStatus ((INT4) u4RetVal, &u1AdminStatus);
        if (u1AdminStatus == CFA_IF_UP)
        {
            CliPrintf (CliHandle, "\r\n Tunnel Status                    : UP");
        }
        else if (u1AdminStatus == CFA_IF_DOWN)
        {
            CliPrintf (CliHandle,
                       "\r\n Tunnel Status                    : DOWN");
        }
    }

    CliPrintf (CliHandle, "\r\n Policy Protocol                  : ");
    nmhGetFsVpnProtocol (pPolicyName, &i4Value);
    switch (i4Value)
    {
        case VPN_TCP:
            CliPrintf (CliHandle, "TCP");
            break;
        case VPN_UDP:
            CliPrintf (CliHandle, "UDP");
            break;
        case VPN_AH:
            CliPrintf (CliHandle, "AH");
            break;
        case VPN_ESP:
            CliPrintf (CliHandle, "ESP");
            break;
        case VPN_ICMP:
            CliPrintf (CliHandle, "ICMP");
            break;
        case VPN_ANY_PROTOCOL:
            CliPrintf (CliHandle, "any");
            break;
        default:
            CliPrintf (CliHandle, "Unknown");
            break;
    }

    CliPrintf (CliHandle, "\r\n Policy Action                    : ");
    nmhGetFsVpnPolicyFlag (pPolicyName, &i4Value);
    if (i4Value == SEC_APPLY)
    {
        CliPrintf (CliHandle, "Apply");
    }
    else if (i4Value == SEC_BYPASS)
    {
        CliPrintf (CliHandle, "Bypass");
    }
    else
    {
        CliPrintf (CliHandle, "Filter");
    }

    if (i4PolicyType == VPN_IPSEC_MANUAL)
    {
        nmhGetFsVpnInboundSpi (pPolicyName, &i4Value);
        CliPrintf (CliHandle, "\r\n In/Out bound SPI                 : %d / ",
                   i4Value);
        nmhGetFsVpnOutboundSpi (pPolicyName, &i4Value);
        CliPrintf (CliHandle, "%d ", i4Value);

        CliPrintf (CliHandle, "\r\n Security Protocol                : ");
        nmhGetFsVpnSecurityProtocol (pPolicyName, &i4Value);
        if (i4Value == SEC_AH)
        {
            CliPrintf (CliHandle, "AH ");
        }
        else
        {
            CliPrintf (CliHandle, "ESP ");
        }

        nmhGetFsVpnAuthAlgo (pPolicyName, &i4Value);
        if (i4Value == CLI_HMAC_MD5)
        {
            CliPrintf (CliHandle, "\r\n Authentication Algorithm         : "
                       "HMAC-MD5");
        }
        if (i4Value == CLI_HMAC_SHA1)
        {
            CliPrintf (CliHandle, "\r\n Authentication Algorithm         : "
                       "HMAC-SHA1");
        }
        if (i4Value == SEC_XCBCMAC)
        {
            CliPrintf (CliHandle, "\r\n Authentication Algorithm         : "
                       "AES-XCBC-MAC-96");
        }
        if (i4Value == HMAC_SHA_256)
        {
            CliPrintf (CliHandle, "\r\n Authentication Algorithm         : "
                       "HMAC_SHA_256");
        }
        if (i4Value == HMAC_SHA_384)
        {
            CliPrintf (CliHandle, "\r\n Authentication Algorithm         : "
                       "HMAC_SHA_384");
        }
        if (i4Value == HMAC_SHA_512)
        {
            CliPrintf (CliHandle, "\r\n Authentication Algorithm         : "
                       "HMAC_SHA_512");
        }

        CliPrintf (CliHandle, "\r\n Encryption  Algo                 : ");
        nmhGetFsVpnEncrAlgo (pPolicyName, &i4Value);
        switch (i4Value)
        {
            case CLI_VPN_DES:
                CliPrintf (CliHandle, "DES ");
                break;
            case CLI_VPN_3DES:
                CliPrintf (CliHandle, "3DES ");
                break;
            case CLI_VPN_AES_128:
                CliPrintf (CliHandle, "AES-128 ");
                break;
            case CLI_VPN_AES_192:
                CliPrintf (CliHandle, "AES-192 ");
                break;
            case CLI_VPN_AES_256:
                CliPrintf (CliHandle, "AES-256 ");
                break;
            case CLI_VPN_ESP_NULL:
                CliPrintf (CliHandle, "NULL ");
                break;
            default:
                CliPrintf (CliHandle, "Not Configured ");
        }

        nmhGetFsVpnAntiReplay (pPolicyName, &i4Value);
        if (i4Value == CLI_AR_ENABLE)
        {
            CliPrintf (CliHandle, "\r\n Anti Replay                      : "
                       "Enable");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n Anti Replay                      : "
                       "Disable");
        }
    }
    else if ((i4PolicyType == VPN_IKE_PRESHAREDKEY) ||
             (i4PolicyType == VPN_XAUTH) ||
             (i4PolicyType == VPN_IKE_CERTIFICATE) ||
             (i4PolicyType == VPN_IKE_RA_PRESHAREDKEY) ||
             (i4PolicyType == VPN_IKE_RA_CERT) ||
             (i4PolicyType == VPN_IKE_XAUTH_CERT))

    {
        nmhGetFsVpnIkePhase2AuthAlgo (pPolicyName, &i4Value);
        if ((i4Value == VPN_HMACMD5) || (i4Value == VPN_HMACSHA1) ||
            (i4Value == SEC_XCBCMAC) || (i4Value == HMAC_SHA_256) ||
            (i4Value == HMAC_SHA_384) || (i4Value == HMAC_SHA_512))
        {
            CliPrintf (CliHandle, "\r\n Anti Replay                      : "
                       "Enable");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n Anti Replay                      : "
                       "Disable");
        }

        CliPrintf (CliHandle, "\r\n IKE suite Info [PHASE I] : ");

        CliPrintf (CliHandle, " \r\n\t  Encryption  Algo        : ");
        nmhGetFsVpnIkePhase1EncryptionAlgo (pPolicyName, &i4Value);
        if (i4Value == CLI_VPN_DES)
        {
            CliPrintf (CliHandle, "DES ");
        }
        else if (i4Value == CLI_VPN_3DES)
        {
            CliPrintf (CliHandle, "3DES ");
        }
        else if (i4Value == CLI_VPN_AES_128)
        {
            CliPrintf (CliHandle, "AES-128 ");
        }
        else if (i4Value == CLI_VPN_AES_192)
        {
            CliPrintf (CliHandle, "AES-192 ");
        }
        else if (i4Value == CLI_VPN_AES_256)
        {
            CliPrintf (CliHandle, "AES-256 ");
        }

        nmhGetFsVpnIkePhase1HashAlgo (pPolicyName, &i4Value);
        if (i4Value == CLI_HMAC_MD5)
        {
            CliPrintf (CliHandle, "\r\n\t  Hash Algorithm          : HMAC MD5");
        }
        else if (i4Value == CLI_HMAC_SHA1)
        {
            CliPrintf (CliHandle,
                       "\r\n\t  Hash Algorithm          : HMAC SHA1");
        }
        else if (i4Value == HMAC_SHA_256)
        {
            CliPrintf (CliHandle,
                       "\r\n\t  Hash Algorithm          : HMAC SHA256");
        }
        else if (i4Value == HMAC_SHA_384)
        {
            CliPrintf (CliHandle,
                       "\r\n\t  Hash Algorithm          : HMAC SHA384");
        }
        else if (i4Value == HMAC_SHA_512)
        {
            CliPrintf (CliHandle,
                       "\r\n\t  Hash Algorithm          : HMAC SHA512");
        }

        CliPrintf (CliHandle, "\r\n\t  Diffie-Hellman Group    : ");
        nmhGetFsVpnIkePhase1DHGroup (pPolicyName, &i4Value);
        if (i4Value == CLI_VPN_IKE_DH_GROUP_1)
        {
            CliPrintf (CliHandle, "DH Group 1");
        }
        else if (i4Value == CLI_VPN_IKE_DH_GROUP_2)
        {
            CliPrintf (CliHandle, "DH Group 2 ");
        }
        else if (i4Value == CLI_VPN_IKE_DH_GROUP_5)
        {
            CliPrintf (CliHandle, "DH Group 5");
        }
        else if (i4Value == SEC_DH_GROUP_14)
        {
            CliPrintf (CliHandle, "DH Group 14");
        }

        if (pVpnPolicy->u1VpnIkeVer == CLI_VPN_IKE_V1)
        {
            if (nmhGetFsVpnIkePhase1Mode (pPolicyName, &i4Value) !=
                SNMP_FAILURE)
            {
                if (i4Value == CLI_VPN_IKE_MAIN_MODE)
                {
                    CliPrintf (CliHandle,
                               "\r\n\t  IKE Exchange Mode       : Main");
                }
                else if (i4Value == CLI_VPN_IKE_AGGRESSIVE_MODE)
                {
                    CliPrintf (CliHandle,
                               "\r\n\t  IKE Exchange Mode       : Aggressive ");
                }
            }
        }

        if (nmhGetFsVpnIkePhase1LifeTime (pPolicyName, &i4Value) !=
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n\t  Life Time               : %d ",
                       i4Value);
        }

        if (nmhGetFsVpnIkePhase1LifeTimeType (pPolicyName, &i4Value) !=
            SNMP_FAILURE)
        {
            if (i4Value == FSIKE_LIFETIME_SECS)
            {
                CliPrintf (CliHandle, "Secs");
            }
            else if (i4Value == FSIKE_LIFETIME_KB)
            {
                CliPrintf (CliHandle, "KB");
            }
            else if (i4Value == FSIKE_LIFETIME_MINS)
            {
                CliPrintf (CliHandle, "Minutes");
            }
            else if (i4Value == FSIKE_LIFETIME_HRS)
            {
                CliPrintf (CliHandle, "Hours");
            }
            else if (i4Value == FSIKE_LIFETIME_DAYS)
            {
                CliPrintf (CliHandle, "Days");
            }
        }

        CliPrintf (CliHandle, "\r\n Identity Information :");

        MEMSET (au1TempStr, VPN_ZERO, sizeof (au1TempStr));
        OctetStr.pu1_OctetList = au1TempStr;

        if (nmhGetFsVpnIkePhase1LocalIdType (pPolicyName, &i4RetVal) !=
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, " \n\t  Local Identity Type     : ");
            switch (i4RetVal)
            {
                case VPN_ID_TYPE_IPV4:
                    CliPrintf (CliHandle, "IPv4 ");
                    break;

                case VPN_ID_TYPE_IPV6:
                    CliPrintf (CliHandle, "IPv6 ");
                    break;

                case VPN_ID_TYPE_EMAIL:
                    CliPrintf (CliHandle, "email ");
                    break;

                case VPN_ID_TYPE_FQDN:
                    CliPrintf (CliHandle, "fqdn ");
                    break;

                case VPN_ID_TYPE_DN:
                    CliPrintf (CliHandle, "dn ");
                    break;
                case VPN_ID_TYPE_KEYID:
                    CliPrintf (CliHandle, "keyid ");
                    break;

                default:
                    CliPrintf (CliHandle, "Default ");
                    break;
            }
            nmhGetFsVpnIkePhase1LocalIdValue (pPolicyName, &OctetStr);
            CliPrintf (CliHandle, " \n\t  Local Identity value    : %s ",
                       OctetStr.pu1_OctetList);
        }

        MEMSET (au1TempStr, VPN_ZERO, sizeof (au1TempStr));
        OctetStr.pu1_OctetList = au1TempStr;

        if (nmhGetFsVpnIkePhase1PeerIdType (pPolicyName, &i4RetVal) !=
            SNMP_FAILURE)
        {
            nmhGetFsVpnIkePhase1PeerIdValue (pPolicyName, &OctetStr);
            switch (i4RetVal)
            {
                case VPN_ID_TYPE_IPV4:
                    CliPrintf (CliHandle,
                               " \n\t  Peer Identity Type      : IPv4 ");
                    break;

                case VPN_ID_TYPE_IPV6:
                    CliPrintf (CliHandle,
                               " \n\t  Peer Identity Type      : IPv6 ");
                    break;

                case VPN_ID_TYPE_EMAIL:
                    CliPrintf (CliHandle,
                               " \n\t  Peer Identity Type      : email ");
                    break;

                case VPN_ID_TYPE_FQDN:
                    CliPrintf (CliHandle,
                               " \n\t  Peer Identity Type      : fqdn ");
                    break;
                case VPN_ID_TYPE_DN:
                    CliPrintf (CliHandle,
                               " \n\t  Peer Identity Type      : dn ");
                    break;

                case VPN_ID_TYPE_KEYID:
                    CliPrintf (CliHandle,
                               " \n\t  Peer Identity Type      : keyid ");
                    break;

                default:
                    CliPrintf (CliHandle,
                               " \n\t  Peer Identity Type      : Default");
                    break;
            }
            CliPrintf (CliHandle, " \n\t  Peer Identity value     : %s ",
                       OctetStr.pu1_OctetList);
        }

        CliPrintf (CliHandle, "\r\n IPSEC suite Info [Phase II] : ");
        nmhGetFsVpnSecurityProtocol (pPolicyName, &i4Value);
        if (i4Value == SEC_AH)
        {
            CliPrintf (CliHandle, " \n\t  Protocol                : AH ");
        }
        else if (i4Value == SEC_ESP)
        {
            CliPrintf (CliHandle, " \n\t  Protocol                : ESP ");
        }

        nmhGetFsVpnIkePhase2EspEncryptionAlgo (pPolicyName, &i4Value);
        CliPrintf (CliHandle, "\r\n\t  Encryption  Algo        : ");
        switch (i4Value)
        {
            case CLI_VPN_DES:
                CliPrintf (CliHandle, "DES ");
                break;
            case CLI_VPN_3DES:
                CliPrintf (CliHandle, "3DES ");
                break;
            case CLI_VPN_AES_128:
                CliPrintf (CliHandle, "AES - 128 ");
                break;
            case CLI_VPN_AES_192:
                CliPrintf (CliHandle, "AES - 192 ");
                break;
            case CLI_VPN_AES_256:
                CliPrintf (CliHandle, "AES - 256 ");
                break;
            case CLI_VPN_ESP_NULL:
                CliPrintf (CliHandle, "NULL ");
                break;
            case SEC_AESCTR:
                CliPrintf (CliHandle, "AES-CTR-128 ");
                break;
            case SEC_AESCTR192:
                CliPrintf (CliHandle, "AES-CTR-192 ");
                break;
            case SEC_AESCTR256:
                CliPrintf (CliHandle, "AES-CTR-256 ");
                break;
            default:
                CliPrintf (CliHandle, "Not Configured");
        }

        nmhGetFsVpnIkePhase2AuthAlgo (pPolicyName, &i4Value);
        if (i4Value == CLI_HMAC_MD5)
        {
            CliPrintf (CliHandle, "\r\n\t  Authenticator           : HMAC-MD5");
        }
        else if (i4Value == CLI_HMAC_SHA1)
        {
            CliPrintf (CliHandle,
                       "\r\n\t  Authenticator           : HMAC-SHA1");
        }
        else if (i4Value == SEC_XCBCMAC)
        {
            CliPrintf (CliHandle,
                       "\r\n\t  Authenticator           : AES-XCBC-MAC-96");
        }
        else if (i4Value == HMAC_SHA_256)
        {
            CliPrintf (CliHandle, "\r\n\t  Authenticator         : "
                       "HMAC_SHA_256");
        }
        else if (i4Value == HMAC_SHA_384)
        {
            CliPrintf (CliHandle, "\r\n\t  Authenticator        : "
                       "HMAC_SHA_384");
        }
        else if (i4Value == HMAC_SHA_512)
        {
            CliPrintf (CliHandle, "\r\n\t  Authenticator         : "
                       "HMAC_SHA_512");
        }

        CliPrintf (CliHandle, "\r\n\t  Perfect Forward Secrecy : ");
        nmhGetFsVpnIkePhase2DHGroup (pPolicyName, &i4Value);
        switch (i4Value)
        {
            case CLI_VPN_IKE_DH_GROUP_1:
                CliPrintf (CliHandle, "PFS Group 1");
                break;
            case CLI_VPN_IKE_DH_GROUP_2:
                CliPrintf (CliHandle, "PFS Group 2");
                break;
            case CLI_VPN_IKE_DH_GROUP_5:
                CliPrintf (CliHandle, "PFS Group 5");
                break;
            case SEC_DH_GROUP_14:
                CliPrintf (CliHandle, "PFS Group 14");
                break;
            default:
                CliPrintf (CliHandle, "Not Configured");
        }

        nmhGetFsVpnIkePhase2LifeTime (pPolicyName, &i4Value);
        CliPrintf (CliHandle, "\r\n\t  Life Time               : %d ", i4Value);
        nmhGetFsVpnIkePhase2LifeTimeType (pPolicyName, &i4Value);
        switch (i4Value)
        {
            case FSIKE_LIFETIME_SECS:
                CliPrintf (CliHandle, "Secs");
                break;
            case FSIKE_LIFETIME_KB:
                CliPrintf (CliHandle, "KBs ");
                break;
            case FSIKE_LIFETIME_MINS:
                CliPrintf (CliHandle, "Minutes");
                break;
            case FSIKE_LIFETIME_HRS:
                CliPrintf (CliHandle, "Hours");
                break;
            case FSIKE_LIFETIME_DAYS:
                CliPrintf (CliHandle, "Days");
                break;
            default:
                CliPrintf (CliHandle, "Not Configured");
        }
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
INT1
VpnGetCryptoMapPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    INT4                i4Len;
    UINT4               u4Index;

    i4Len = (INT4) STRLEN (VPN_CRYPTO_MODE);

    if (STRNCMP (pi1ModeName, VPN_CRYPTO_MODE, (size_t) i4Len) != VPN_ZERO)
    {
        return FALSE;
    }
    u4Index = (UINT4) CLI_ATOI (pi1ModeName + i4Len);

    if (((UINT4) CLI_SET_VPN_POLICY_ID ((INT4) u4Index)) != (UINT4) u4Index)
        return (FALSE);

    STRCPY (pi1DispStr, "(config-crypto-map)#");
    return (TRUE);
}

/**************************************************************************/
/*  Function Name   : VpnCliSetVpnPolicyIkeProposal                       */
/*  Description     : This function does the following:                   */
/*                  : Set the following IKE Phase I paramaters in the vpn */
/*                    policy database:                                    */
/*                      Mode - main/aggressive                            */
/*                      Hash Algo - md5/sha1/aes-xcbc-mac                 */
/*                      Encryption Algo - des/triple-des/aes              */
/*                      DH Group - Deffie-Hellman Group 1/2/5/14          */
/*                      Lifetime Type - secs/minutes/hours                */
/*                      Lifetime value                                    */
/*                                                                        */
/*  Input(s)        : Parameters required for the CLI cmd                 */
/*  Output(s)       : Any reply message to the user whether success or    */
/*                      failure                                           */
/*  Returns         : None                                                */
/**************************************************************************/

INT4
VpnCliSetVpnPolicyIkeProposal (tCliHandle CliHandle,
                               tSNMP_OCTET_STRING_TYPE * pVpnPolicy,
                               UINT4 u4EncrAlgo, UINT4 u4HashAlgo,
                               UINT4 u4DHGrp, UINT4 u4ExchMode,
                               UINT4 u4LifetimeType, UINT4 u4Lifetime)
{
    UINT4               u4SnmpErrorStatus = VPN_ZERO;
    INT4                i4PrevIkeLTimeType = VPN_ZERO;
    INT4                i4VpnPolicyType = VPN_ZERO;
    INT4                i4VpnIkeVersion = VPN_ZERO;
    INT4                i4RetVal = VPN_ZERO;

    UNUSED_PARAM (CliHandle);

    nmhGetFsVpnPolicyType (pVpnPolicy, &i4VpnPolicyType);
    if (i4VpnPolicyType == VPN_IPSEC_MANUAL)
    {
        CLI_SET_ERR (CLI_VPN_ERR_POLICY_NOT_IKE);
        return (CLI_FAILURE);
    }

    nmhGetFsVpnIkeVersion (pVpnPolicy, &i4VpnIkeVersion);
    if (i4VpnIkeVersion == VPN_ZERO)
    {
        CLI_SET_ERR (CLI_VPN_ERR_IKE_VERSION_NOT_SET);
        return (CLI_FAILURE);
    }

    if (nmhTestv2FsVpnIkePhase1EncryptionAlgo
        (&u4SnmpErrorStatus, pVpnPolicy, (INT4) u4EncrAlgo) == SNMP_FAILURE)
    {
        i4RetVal = CLI_GET_ERR (&u4SnmpErrorStatus);
        UNUSED_PARAM (i4RetVal);
        if (VPN_ZERO == u4SnmpErrorStatus)
        {
            CLI_SET_ERR (CLI_VPN_ERR_PHASEI_INFO);
        }
        return (CLI_FAILURE);
    }
    nmhSetFsVpnIkePhase1EncryptionAlgo (pVpnPolicy, (INT4) u4EncrAlgo);

    if (nmhTestv2FsVpnIkePhase1HashAlgo (&u4SnmpErrorStatus,
                                         pVpnPolicy,
                                         (INT4) u4HashAlgo) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_PHASEI_INFO);
        return (CLI_FAILURE);
    }
    nmhSetFsVpnIkePhase1HashAlgo (pVpnPolicy, (INT4) u4HashAlgo);

    if (nmhTestv2FsVpnIkePhase1DHGroup (&u4SnmpErrorStatus, pVpnPolicy,
                                        (INT4) u4DHGrp) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_PHASEI_INFO);
        return (CLI_FAILURE);
    }
    nmhSetFsVpnIkePhase1DHGroup (pVpnPolicy, (INT4) u4DHGrp);

    if (u4ExchMode != VPN_ZERO)
    {
        if (nmhTestv2FsVpnIkePhase1Mode (&u4SnmpErrorStatus, pVpnPolicy,
                                         (INT4) u4ExchMode) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        nmhSetFsVpnIkePhase1Mode (pVpnPolicy, (INT4) u4ExchMode);
    }
    if (nmhTestv2FsVpnIkePhase1LifeTimeType (&u4SnmpErrorStatus, pVpnPolicy,
                                             (INT4) u4LifetimeType) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_IKE_LTIME_INFO);
        return (CLI_FAILURE);
    }
    nmhGetFsVpnIkePhase1LifeTimeType (pVpnPolicy, &i4PrevIkeLTimeType);
    nmhSetFsVpnIkePhase1LifeTimeType (pVpnPolicy, (INT4) u4LifetimeType);
    if (nmhTestv2FsVpnIkePhase1LifeTime (&u4SnmpErrorStatus, pVpnPolicy,
                                         (INT4) u4Lifetime) == SNMP_FAILURE)
    {
        nmhSetFsVpnIkePhase1LifeTimeType (pVpnPolicy, i4PrevIkeLTimeType);
        CLI_SET_ERR (CLI_VPN_ERR_IKE_LTIME_INFO);
        return (CLI_FAILURE);
    }
    nmhSetFsVpnIkePhase1LifeTime (pVpnPolicy, (INT4) u4Lifetime);

    return CLI_SUCCESS;
}

/****************************************************************************/
/*  Function Name   : VpnCliSetVpnPolicyIpsecProposal                       */
/*  Description     : This function does the following:                     */
/*                  : Set the following IKE Phase II paramaters in the vpn  */
/*                    policy database:                                      */
/*                      Mode - Tunnel/Transport                             */
/*                      Hash Algo - md5/sha1/aes-xcbc-mac                   */
/*                      Encryption Algo - des/triple-des/aes                */
/*                      DH Group - Deffie-Hellman Group 1/2/5/14            */
/*                      Lifetime Type - secs/minutes/hours                  */
/*                      Lifetime value                                      */
/*                                                                          */
/*  Input(s)        : Parameters required for the CLI cmd                   */
/*  Output(s)       : Any reply message to the user whether success or      */
/*                      failure                                             */
/*  Returns         : None                                                  */
/****************************************************************************/

INT4
VpnCliSetVpnPolicyIpsecProposal (tCliHandle CliHandle,
                                 tSNMP_OCTET_STRING_TYPE * pVpnPolicy,
                                 UINT4 u4EncrAlgo, UINT4 u4Protocol,
                                 UINT4 u4HashAlgo, UINT4 u4DHGrp,
                                 UINT4 u4LifeTimeType, UINT4 u4LifeTime)
{
    UINT4               u4SnmpErrorStatus = VPN_ZERO;
    INT4                i4PrevIPSecLTimeType = VPN_ZERO;
    INT4                i4VpnPolicyType = VPN_ZERO;
    INT4                i4VpnIkeVersion = VPN_ZERO;
    INT4                i4RetVal = VPN_ZERO;

    UNUSED_PARAM (CliHandle);

    nmhGetFsVpnPolicyType (pVpnPolicy, &i4VpnPolicyType);
    if (i4VpnPolicyType == VPN_IPSEC_MANUAL)
    {
        CLI_SET_ERR (CLI_VPN_ERR_POLICY_NOT_IKE);
        return (CLI_FAILURE);
    }

    nmhGetFsVpnIkeVersion (pVpnPolicy, &i4VpnIkeVersion);
    if (i4VpnIkeVersion == VPN_ZERO)
    {
        CLI_SET_ERR (CLI_VPN_ERR_IKE_VERSION_NOT_SET);
        return (CLI_FAILURE);
    }

    if (u4EncrAlgo != VPN_ZERO)
    {
        if (nmhTestv2FsVpnIkePhase2EspEncryptionAlgo (&u4SnmpErrorStatus,
                                                      pVpnPolicy,
                                                      (INT4) u4EncrAlgo) ==
            SNMP_FAILURE)
        {
            i4RetVal = CLI_GET_ERR (&u4SnmpErrorStatus);
            if (VPN_ZERO == u4SnmpErrorStatus)
            {
                CLI_SET_ERR (CLI_VPN_ERR_PHASEII_INFO);
            }
            return CLI_FAILURE;
        }
    }
    else if (u4Protocol == CLI_VPN_ESP)
    {
        /* If no encryption is choosen treat that as NULL encryption
         * beacuse it is mandatory to provide encryption algo to SafeNet.
         */
        u4EncrAlgo = VPN_NULLESPALGO;
    }
    nmhSetFsVpnIkePhase2EspEncryptionAlgo (pVpnPolicy, (INT4) u4EncrAlgo);

    nmhSetFsVpnSecurityProtocol (pVpnPolicy, (INT4) u4Protocol);
    if (u4HashAlgo != VPN_ZERO)
    {
        if (nmhTestv2FsVpnIkePhase2AuthAlgo (&u4SnmpErrorStatus,
                                             pVpnPolicy,
                                             (INT4) u4HashAlgo) == SNMP_FAILURE)
        {
            i4RetVal = CLI_GET_ERR (&u4SnmpErrorStatus);
            UNUSED_PARAM (i4RetVal);
            if (VPN_ZERO == u4SnmpErrorStatus)
            {
                CLI_SET_ERR (CLI_VPN_ERR_PHASEII_INFO);
            }
            return CLI_FAILURE;
        }
    }
    nmhSetFsVpnIkePhase2AuthAlgo (pVpnPolicy, (INT4) u4HashAlgo);

    if (u4DHGrp != VPN_ZERO)
    {
        if (nmhTestv2FsVpnIkePhase2DHGroup (&u4SnmpErrorStatus,
                                            pVpnPolicy,
                                            (INT4) u4DHGrp) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_VPN_ERR_PHASEII_INFO);
            return CLI_FAILURE;
        }
    }
    nmhSetFsVpnIkePhase2DHGroup (pVpnPolicy, (INT4) u4DHGrp);

    if ((u4LifeTimeType != VPN_ZERO) && (u4LifeTime != VPN_ZERO))
    {
        if (nmhTestv2FsVpnIkePhase2LifeTimeType (&u4SnmpErrorStatus,
                                                 pVpnPolicy,
                                                 (INT4) u4LifeTimeType) ==
            SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_VPN_ERR_IPSEC_LTIME_INFO);
            return CLI_FAILURE;
        }
        nmhGetFsVpnIkePhase2LifeTimeType (pVpnPolicy, &i4PrevIPSecLTimeType);
        nmhSetFsVpnIkePhase2LifeTimeType (pVpnPolicy, (INT4) u4LifeTimeType);
        if (nmhTestv2FsVpnIkePhase2LifeTime (&u4SnmpErrorStatus,
                                             pVpnPolicy,
                                             (INT4) u4LifeTime) == SNMP_FAILURE)
        {
            /* Reset the lifetime type since the new info is not valid */
            CLI_SET_ERR (CLI_VPN_ERR_IPSEC_LTIME_INFO);
            nmhSetFsVpnIkePhase2LifeTimeType (pVpnPolicy, i4PrevIPSecLTimeType);
            return CLI_FAILURE;
        }
        nmhSetFsVpnIkePhase2LifeTime (pVpnPolicy, (INT4) u4LifeTime);
    }
    else
    {
        CLI_SET_ERR (CLI_VPN_ERR_IPSEC_LTIME_INFO);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
VpnSetPeerType (tCliHandle CliHandle,
                tSNMP_OCTET_STRING_TYPE * pPolicyName, UINT4 u4PeerType,
                UINT1 *au1PeerValue)
{
    UINT4               u4SnmpErrorStatus = VPN_ZERO;
    tSNMP_OCTET_STRING_TYPE OctetStr;

    UNUSED_PARAM (CliHandle);

    OctetStr.i4_Length = (INT4) STRLEN (au1PeerValue);
    OctetStr.pu1_OctetList = au1PeerValue;

    if (nmhTestv2FsVpnIkePhase1PeerIdType
        (&u4SnmpErrorStatus, pPolicyName, (INT4) u4PeerType) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_POLICY_NOT_IKE);
        return CLI_FAILURE;
    }
    nmhSetFsVpnIkePhase1PeerIdType (pPolicyName, (INT4) u4PeerType);

    if (nmhTestv2FsVpnIkePhase1PeerIdValue (&u4SnmpErrorStatus, pPolicyName,
                                            &OctetStr) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_ID_INFO);
        return CLI_FAILURE;
    }
    nmhSetFsVpnIkePhase1PeerIdValue (pPolicyName, &OctetStr);

    return CLI_SUCCESS;
}

INT4
VpnSetLocalIdType (tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE * pPolicyName,
                   UINT4 u4LocalIdType, UINT1 *au1LocalIdValue)
{
    UINT4               u4SnmpErrorStatus = VPN_ZERO;
    tSNMP_OCTET_STRING_TYPE OctetStr;

    UNUSED_PARAM (CliHandle);

    OctetStr.i4_Length = (INT4) STRLEN (au1LocalIdValue);
    OctetStr.pu1_OctetList = au1LocalIdValue;

    if (nmhTestv2FsVpnIkePhase1LocalIdType
        (&u4SnmpErrorStatus, pPolicyName, (INT4) u4LocalIdType) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_POLICY_NOT_IKE);
        return CLI_FAILURE;
    }
    nmhSetFsVpnIkePhase1LocalIdType (pPolicyName, (INT4) u4LocalIdType);

    if (nmhTestv2FsVpnIkePhase1LocalIdValue (&u4SnmpErrorStatus, pPolicyName,
                                             &OctetStr) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_ID_INFO);
        return CLI_FAILURE;
    }
    nmhSetFsVpnIkePhase1LocalIdValue (pPolicyName, &OctetStr);

    return CLI_SUCCESS;
}

INT4
VpnAddRemoteIdInfo (tCliHandle CliHandle, INT4 i4IdType, UINT1 *pu1IdValue,
                    UINT4 u4AuthType, UINT1 *pu1PresharedKey)
{
    UINT4               u4SnmpErrorStatus = VPN_ZERO;
    tSNMP_OCTET_STRING_TYPE IdValue;
    tSNMP_OCTET_STRING_TYPE IdKey;
    tUtlIn6Addr         In6Addr;
    UINT1               au1IpAddr[VPN_IPV6_ADDR_LEN + VPN_ONE] = { VPN_ZERO };
    INT4                i4RetVal = VPN_ZERO;

    if (i4IdType == CLI_VPN_IKE_KEY_IPV6)
    {
        /* Allways store the IPv6 identity in the full address
         * format. So convert the ATON and NTOA to get the full
         * address in string */
        MEMSET (&In6Addr, VPN_ZERO, sizeof (tUtlIn6Addr));
        MEMSET (au1IpAddr, VPN_ZERO, (VPN_IPV6_ADDR_LEN + VPN_ONE));

        INET_ATON6 (pu1IdValue, &In6Addr);
        MEMCPY (au1IpAddr, INET_NTOA6 (In6Addr), (VPN_IPV6_ADDR_LEN));

        IdValue.i4_Length = (INT4) STRLEN (au1IpAddr);
        IdValue.pu1_OctetList = au1IpAddr;
    }
    else
    {
        IdValue.i4_Length = (INT4) STRLEN (pu1IdValue);
        IdValue.pu1_OctetList = pu1IdValue;
    }

    if (nmhTestv2FsVpnRemoteIdStatus (&u4SnmpErrorStatus, i4IdType, &IdValue,
                                      CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        i4RetVal = CLI_GET_ERR (&u4SnmpErrorStatus);
        UNUSED_PARAM (i4RetVal);
        if (u4SnmpErrorStatus == VPN_ZERO)
        {
            CliPrintf (CliHandle, "%% Remote id value should match the "
                       "given type and should not exceed max len %d\r\n",
                       VPN_MAX_NAME_LENGTH);
            CLI_SET_ERR (VPN_ZERO);
        }
        return (CLI_FAILURE);
    }
    if (nmhSetFsVpnRemoteIdStatus (i4IdType, &IdValue, CREATE_AND_WAIT) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_MEM_ALLOC_FAIL);
        return (CLI_FAILURE);
    }
    if (nmhTestv2FsVpnRemoteIdAuthType (&u4SnmpErrorStatus, i4IdType, &IdValue,
                                        (INT4) u4AuthType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Invalid Auth Type \r\n");
        return (CLI_FAILURE);
    }
    nmhSetFsVpnRemoteIdAuthType (i4IdType, &IdValue, (INT4) u4AuthType);
    IdKey.i4_Length = (INT4) STRLEN (pu1PresharedKey);
    IdKey.pu1_OctetList = pu1PresharedKey;
    if (nmhTestv2FsVpnRemoteIdKey (&u4SnmpErrorStatus, i4IdType, &IdValue,
                                   &IdKey) == SNMP_FAILURE)
    {
        nmhSetFsVpnRemoteIdStatus (i4IdType, &IdValue, DESTROY);
        if (u4AuthType == VPN_AUTH_PRESHARED)
        {
            CliPrintf (CliHandle, "%% Preshared Key must be of "
                       "minimum length %d and maximum %d\r\n",
                       VPN_MIN_PRESHARED_KEY_LEN, VPN_MAX_PRESHARED_KEY_LEN);
        }
        else
        {
            CliPrintf (CliHandle, "%% Certficate keyId must be of "
                       "maximum length %d\r\n", VPN_MAX_KEY_NAME_LEN);
        }
        CLI_SET_ERR (VPN_ERROR);
        return (CLI_FAILURE);
    }
    nmhSetFsVpnRemoteIdKey (i4IdType, &IdValue, &IdKey);
    if (nmhSetFsVpnRemoteIdStatus (i4IdType, &IdValue, ACTIVE) == SNMP_FAILURE)
    {
        nmhSetFsVpnRemoteIdStatus (i4IdType, &IdValue, DESTROY);
        CliPrintf (CliHandle, "%% Unable to add the new id due to "
                   "communication failure with VPN subsystem.\r\n");
        CLI_SET_ERR (VPN_ERROR);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************
 * Function    : VpnDisplayError
 * Description : Prints the error corresponding to the given error code
 * Input(s)    : CliHandle, Error code
 * OutPut(s)   : None
 * Returns     : CLI_SUCCESS / CLI_FAILURE
 *****************************************************************************/
PRIVATE INT4
VpnDisplayError (tCliHandle CliHandle, INT4 i4Error)
{
    switch (i4Error)
    {
        case CLI_VPN_ERR_PEER_INFO:
            CliPrintf (CliHandle,
                       "%% Invalid peer ip; Peer ip should not be any of the "
                       "local addresses.\r\n");
            break;
        case CLI_VPN_ERR_ACL_PARAM:
            CliPrintf (CliHandle,
                       "%% Set Access List parameters properly! "
                       "Use proper ipaddr and the mask.\r\n");
            break;
        case CLI_VPN_ERR_IPV6_ACL_PARAM:
            CliPrintf (CliHandle,
                       "%% Set IPv6 Access List parameters properly! \r\n");
            break;
        case CLI_VPN_ERR_IPSEC_MODE:
            CliPrintf (CliHandle, "%% Set IPSEC mode to Transport/Tunnel!\r\n");
            break;
        case CLI_VPN_ERR_PHASEI_INFO:
            CliPrintf (CliHandle,
                       "%% Set ISAKMP (IKE Phase I) Parameters properly!\r\n");
            break;
        case CLI_VPN_ERR_PHASEII_INFO:
            CliPrintf (CliHandle,
                       "%% Set IPSEC (IKE Phase II) Parameters properly!\r\n");
            break;
        case CLI_VPN_ERR_VPN_MODE:
            CliPrintf (CliHandle,
                       "%% Choose the proper policy type (Manual/IKE).\r\n");
            break;
        case CLI_VPN_IPC_SFNT_FAIL:
            CliPrintf (CliHandle, "%% Unable to communicate with "
                       "VPN subsystem.\r\n");
            break;
        case CLI_VPN_FILL_IPSEC_ACCESS_FAIL:
            CliPrintf (CliHandle, "%% Access List Overlaps with "
                       "an existing configuration.\r\n");
            break;
        case CLI_VPN_BUILD_SFNT_MSG_FAIL:
            CliPrintf (CliHandle,
                       "%% SafeNet config message building failed.\r\n");
            break;
        case CLI_VPN_MEM_ALLOC_FAIL:
            CliPrintf (CliHandle, "%% Fatal error! "
                       "Unable to allocate resources. \r\n");
            break;
        case CLI_VPN_INVLAID_INTF_PARAMS:
            CliPrintf (CliHandle, "%% Ip address and interface index "
                       "are not set properly.\r\n");
            break;
        case CLI_VPN_ERR_MAN_ENCRY_KEY_LEN:
            CliPrintf (CliHandle,
                       "%% Esp keys should have exact len of %d, %d, %d, %d, "
                       "%d characters for DES, 3DES, AES 128, 192, 256 "
                       "respectively\r\n",
                       VPN_ESP_DES_KEY_LEN, VPN_ESP_3DES_KEY_LEN,
                       VPN_ESP_AES_KEY1_LEN, VPN_ESP_AES_KEY2_LEN,
                       VPN_ESP_AES_KEY3_LEN);
            break;
        case CLI_VPN_ERR_MAN_SPI_PARAM:
            CliPrintf (CliHandle, "%% ERROR: Security Parameter Index(SPI) "
                       "value should be > %d\r\n", VPN_MIN_SPI);
            break;
        case CLI_VPN_ERR_NO_POLICY:
            CliPrintf (CliHandle, "%% Specified policy does not exist \r\n");
            break;
        case CLI_VPN_ERR_MAN_KEY:
            CliPrintf (CliHandle, "%% Invalid characters in auth/encr key, "
                       "Should contain only hex characters\r\n");
            break;
        case CLI_VPN_ERR_MAN_AUTH_KEY_LEN:
            CliPrintf (CliHandle, "%% Invalid key length! MD5,SHA1,XCBC-MAC"
                       "SHA256,SHA384,SHA512 must have keys of %d,%d,%d,%d"
                       "%d,%d chars resp.\r\n",
                       VPN_HMACMD5_LEN, VPN_HMACSHA1_LEN, VPN_XCBCMAC_LEN,
                       VPN_SHA2_256_LEN, VPN_SHA2_384_LEN, VPN_SHA2_512_LEN);

            break;
        case CLI_VPN_ERR_MAN_AUTH_PARAM:
        case CLI_VPN_ERR_MAN_ENCRY_PARAM:
            CliPrintf (CliHandle,
                       "%% Invalid authentication/encryption parameters \r\n");
            break;
        case CLI_VPN_ERR_ID_INFO:
            CliPrintf (CliHandle, "%% Set the Local and Remote identity "
                       "parameters properly\r\n");
            break;
        case CLI_VPN_ERR_REMOTE_ID_NOT_EXIST:
            CliPrintf (CliHandle, "%% Remote identity does not exist\r\n");
            break;

        case CLI_VPN_ERR_IKE_LTIME_INFO:
            CliPrintf (CliHandle, "%% Invalid lifetime info. "
                       "Valid range is %d to %d secs or %d to %d KB\r\n",
                       VPN_MIN_LIFETIME_SECS, VPN_PHASE1_MAX_LIFETIME_SECS,
                       VPN_MIN_LIFETIME_KB, VPN_MAX_LIFETIME_KB);
            break;

        case CLI_VPN_ERR_IPSEC_LTIME_INFO:
            CliPrintf (CliHandle, "%% Invalid lifetime info. "
                       "Valid range is %d to %d secs or %d to %d KB\r\n",
                       VPN_MIN_LIFETIME_SECS, VPN_PHASE2_MAX_LIFETIME_SECS,
                       VPN_MIN_LIFETIME_KB, VPN_MAX_LIFETIME_KB);
            break;

        case CLI_VPN_ERR_POLICY_NOT_IKE:
            CliPrintf (CliHandle, "%% IKE parameters can not be set for MANUAL"
                       " policy.\r\n");
            break;

        case CLI_VPN_ERR_POLICY_NOT_MANUAL:
            CliPrintf (CliHandle, "%% MANUAL mode parameters can not be set "
                       "for IKE policy.\r\n");
            break;

        case CLI_VPN_ERR_USER_SECRET_LENGTH:
            CliPrintf (CliHandle,
                       "%% Password should be "
                       "greater than or equal to %d characters and "
                       "less than or equal to %d characters\r\n",
                       VPN_MIN_RA_USER_SECRET_LEN, RA_USER_SECRET_LENGTH);
            break;

        case CLI_VPN_ERR_USER_NAME_LENGTH:
            CliPrintf (CliHandle,
                       "%% User name should be "
                       "less than or equal to %d characters\r\n",
                       RA_USER_NAME_LENGTH);
            break;

        case CLI_VPN_ERR_DUP_ENTRY:
            CliPrintf (CliHandle, "%% Entry already exists with the "
                       "given name.\r\n");
            break;

        case CLI_VPN_ERR_MAX_VPN_POLICIES:
            CliPrintf (CliHandle,
                       "%% Maximum no.of vpn policies should not exceed "
                       "%d \r\n", VPN_SYS_MAX_NUM_TUNNELS);
            break;

        case CLI_VPN_ERR_RAVPN_MAX_USERS:
            CliPrintf (CliHandle,
                       "%% Maximum no.of ravpn users should not exceed "
                       "%d \r\n", RA_VPN_MAX_USERS_DB);
            break;

        case CLI_VPN_ERR_RAVPN_MAX_ADDR_POOLS:
            CliPrintf (CliHandle,
                       "%% Maximum no.of ravpn address pools should not "
                       "exceed %d \r\n", RA_VPN_MAX_ADDRESS_POOL);
            break;

        case CLI_VPN_ERR_RAVPN_ADDR_POOL_NAME_LEN:
            CliPrintf (CliHandle, "%% Ravpn address pool name should contain "
                       "minimum %d characters and maximum %d characters.",
                       VPN_RA_MIN_ADDR_POOL_NAME_LEN,
                       RA_ADDRESS_POOL_NAME_LENGTH);
            break;

        case CLI_VPN_ERR_MAX_REMOTE_IDS:
            CliPrintf (CliHandle,
                       "%% Maximum no.of vpn remote-ids should not exceed "
                       "%d \r\n", VPN_SYS_MAX_REMOTE_IDS);
            break;

        case CLI_VPN_ERR_XAUTH_EXCHG_MODE:
            CliPrintf (CliHandle, "%% RAVPN with PSK works only in "
                       "aggressive mode.\r\n");
            break;

        case CLI_VPN_ERR_REMOTE_ID_ACTIVE:
            CliPrintf (CliHandle, "%% Failed to delete the Id since there"
                       " is an active policy using this Remote Id.\r\n");
            break;

        case CLI_VPN_ERR_PHASEI_RAVPN_MODE:
            CliPrintf (CliHandle,
                       "%% Unable to configure VPN client using xauth "
                       "in main mode\r\n");
            break;
        case CLI_VPN_ERR_WAN_TYPE:
            CliPrintf (CliHandle, "%% VPN policies can be applied only on "
                       "untrusted/Public L3 interfaces(PPP,Ethernet etc)."
                       "\r\n");
            break;
        case CLI_VPN_ERR_MP_TYPE:
            CliPrintf (CliHandle, "%% VPN policies cannot be applied on "
                       "bundled PPP Interfaces." "\r\n");
            break;
        case CLI_VPN_ERR_WAN_IP:
            CliPrintf (CliHandle, "%% Invalid Ip address is assigned to "
                       "Interface.\r\n");
            break;
        case CLI_VPN_ERR_NO_RAVPN_POOL:
            CliPrintf (CliHandle, "%% Address Pool is not configured"
                       " for remote users.\r\n");
            break;
        case CLI_VPN_ERR_RAVPN_POOL_ACTIVE:
            CliPrintf (CliHandle, "%% Can't delete the Address Pool. There are"
                       " active ra-vpn policies using this "
                       "address pool. Deactivate/Delete active "
                       "ra-vpn policies first.\r\n");
            break;
        case CLI_VPN_ERR_INVALID_RAVPN_ACL:
            CliPrintf (CliHandle, "%% Ra-vpn remote network can be either"
                       " 0.0.0.0/0.0.0.0 or ravpn-pool.\r\n");
            break;
        case CLI_VPN_ERR_POLICY_NAME_LENGTH:
            CliPrintf (CliHandle, "%% Invalid policy name length.\
                       Policy name should contain minimum\
                       %d and maximum %d characters\r\n", VPN_MIN_POLICY_NAME_LENGTH, VPN_MAX_POLICY_NAME_LENGTH);
            break;
        case CLI_VPN_ERR_IKE_VERSION_NOT_SET:
            CliPrintf (CliHandle, "%% Please Select IKE version.\r\n");
            break;
        case CLI_VPN_ERR_IKE_VERSION_SET_FAILED:
            CliPrintf (CliHandle, "%% Unable to set IKE Version.\r\n");
            break;
        case CLI_VPN_ERR_RAVPN_USER_NOT_EXIST:
            CliPrintf (CliHandle, "%% Ravpn users are not configured \r\n");
            break;
        case CLI_VPN_ERR_INVALID_PORT_RANGE:
            CliPrintf (CliHandle, "%% Invalid Port Range Value. \r\n");
            break;
        case CLI_VPN_ERR_REMOTE_ID_DIFF_AUTH:
            CliPrintf (CliHandle, "%% Identity exists for different "
                       "authentication method. \r\n");
            break;
        case CLI_VPN_ERR_V2_XAUTH_FAILED:
            CliPrintf (CliHandle,
                       "%% XAUTH mode is not allowed for IKEv2 "
                       "policies. \r\n");
            break;
        case CLI_VPN_ERR_VPN_MODE_CONFIG_ALGO:
            CliPrintf (CliHandle,
                       "%% Configure the Algorithm Type before set "
                       "VPN Policy as Cert. \r\n");
            break;
        case CLI_VPN_ERR_VPN_MODE_ALGO:
            CliPrintf (CliHandle,
                       "%% Configure the proper Algorithm Type. \r\n");
            break;
        case CLI_VPN_ERR_MAN_TDES_KEY:
            CliPrintf (CliHandle,
                       "%% 3DES keys can not be identical, "
                       "Configure all the 3 keys different. \r\n");
            break;

        case CLI_VPN_ERR_FIPS_AGGRESSIVE_MODE:
            CliPrintf (CliHandle, "%% Cannot use Aggressive mode in "
                       "FIPS mode\r\n");
            break;

        case CLI_VPN_ERR_ACL_SRC_PARAM:
            CliPrintf (CliHandle,
                       "%% Set Access List parameters properly! "
                       "Use proper source ipaddr and the mask.\r\n");
            break;

        case CLI_VPN_ERR_ACL_DST_PARAM:
            CliPrintf (CliHandle,
                       "%% Set Access List parameters properly! "
                       "Use proper destination ipaddr and the mask.\r\n");
            break;

        case CLI_VPN_ERR_ACL_SRC_DST_PARAM:
            CliPrintf (CliHandle,
                       "%% Set Access List parameters properly! "
                       "Use proper source, destination ipaddr and the mask.\r\n");
            break;
        case CLI_VPN_ERR_REDUNDANT_POLICY_TO_PEER:
            CliPrintf (CliHandle,
                       "%% Another policy with same peer is bind "
                       "in this interface\r\n");
            break;

        default:
            break;
    }

    return (CLI_SUCCESS);
}

INT4
VpnAddRaUserInfo (tCliHandle CliHandle,
                  tSNMP_OCTET_STRING_TYPE * pUserName,
                  tSNMP_OCTET_STRING_TYPE * pUserSecret)
{
    UINT4               u4SnmpErrorStatus = VPN_ZERO;

    if (nmhTestv2FsVpnRaUserRowStatus
        (&u4SnmpErrorStatus, pUserName, VPN_STATUS_CREATE_AND_WAIT)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhSetFsVpnRaUserRowStatus (pUserName, VPN_STATUS_CREATE_AND_WAIT);

    do
    {
        if (nmhTestv2FsVpnRaUserSecret
            (&u4SnmpErrorStatus, pUserName, pUserSecret) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_VPN_ERR_USER_SECRET_LENGTH);
            break;
        }
        nmhSetFsVpnRaUserSecret (pUserName, pUserSecret);

        if (nmhSetFsVpnRaUserRowStatus
            (pUserName, VPN_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%% Failed to add the User \r\n");
            break;
        }

        return CLI_SUCCESS;
    }
    while (VPN_FALSE);

    nmhSetFsVpnRaUserRowStatus (pUserName, VPN_STATUS_DESTROY);
    return CLI_FAILURE;
}

INT4
VpnDelRaUserInfo (tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE * pUserName)
{
    UINT4               u4SnmpErrorStatus = VPN_ZERO;

    if (nmhTestv2FsVpnRaUserRowStatus
        (&u4SnmpErrorStatus, pUserName, VPN_STATUS_DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% User id doesn't exists \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsVpnRaUserRowStatus
        (pUserName, VPN_STATUS_DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Unable to delete the user \r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
VpnAddAddressPool (tCliHandle CliHandle,
                   tSNMP_OCTET_STRING_TYPE * pVpnRaAddressPoolName,
                   UINT4 u4VpnRaStartIp, UINT4 u4VpnRaEndIp)
{
    UINT4               u4SnmpErrorStatus = VPN_ZERO;
    UINT4               u4PrefixLength = VPN_ZERO;
    INT4                i4AddrType = IPVX_ADDR_FMLY_IPV4;
    UINT1               au1IpAddr[IPVX_IPV4_ADDR_LEN] = { VPN_ZERO };
    tSNMP_OCTET_STRING_TYPE Addr;

    if (nmhTestv2FsVpnRaAddressPoolRowStatus (&u4SnmpErrorStatus,
                                              pVpnRaAddressPoolName,
                                              VPN_STATUS_CREATE_AND_WAIT) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhSetFsVpnRaAddressPoolRowStatus (pVpnRaAddressPoolName,
                                       VPN_STATUS_CREATE_AND_WAIT);

    do
    {

        if (nmhTestv2FsVpnRaAddressPoolAddrType (&u4SnmpErrorStatus,
                                                 pVpnRaAddressPoolName,
                                                 i4AddrType) == SNMP_FAILURE)
        {
            break;
        }

        nmhSetFsVpnRaAddressPoolAddrType (pVpnRaAddressPoolName, i4AddrType);

        MEMSET (au1IpAddr, VPN_ZERO, IPVX_IPV4_ADDR_LEN);
        MEMCPY (au1IpAddr, &u4VpnRaStartIp, sizeof (UINT4));
        Addr.pu1_OctetList = au1IpAddr;
        Addr.i4_Length = IPVX_IPV4_ADDR_LEN;

        if (nmhTestv2FsVpnRaAddressPoolStart (&u4SnmpErrorStatus,
                                              pVpnRaAddressPoolName,
                                              &Addr) == SNMP_FAILURE)
        {
            break;
        }

        nmhSetFsVpnRaAddressPoolStart (pVpnRaAddressPoolName, &Addr);

        MEMSET (au1IpAddr, VPN_ZERO, IPVX_IPV4_ADDR_LEN);
        MEMCPY (au1IpAddr, &u4VpnRaEndIp, sizeof (UINT4));
        Addr.pu1_OctetList = au1IpAddr;
        Addr.i4_Length = IPVX_IPV4_ADDR_LEN;

        if (nmhTestv2FsVpnRaAddressPoolEnd (&u4SnmpErrorStatus,
                                            pVpnRaAddressPoolName,
                                            &Addr) == SNMP_FAILURE)
        {
            break;
        }
        nmhSetFsVpnRaAddressPoolEnd (pVpnRaAddressPoolName, &Addr);

        if ((VPN_IS_ADDR_CLASS_A (u4VpnRaStartIp)))
        {
            u4PrefixLength = VPN_PREFIX_LENGTH_8;
        }
        else if (VPN_IS_ADDR_CLASS_B (u4VpnRaStartIp))
        {
            u4PrefixLength = VPN_PREFIX_LENGTH_16;
        }
        else if (VPN_IS_ADDR_CLASS_C (u4VpnRaStartIp))
        {
            u4PrefixLength = VPN_PREFIX_LENGTH_24;
        }

        if (nmhTestv2FsVpnRaAddressPoolPrefixLen (&u4SnmpErrorStatus,
                                                  pVpnRaAddressPoolName,
                                                  u4PrefixLength) ==
            SNMP_FAILURE)
        {
            break;
        }
        nmhSetFsVpnRaAddressPoolPrefixLen (pVpnRaAddressPoolName,
                                           u4PrefixLength);

        if (nmhTestv2FsVpnRaAddressPoolRowStatus
            (&u4SnmpErrorStatus, pVpnRaAddressPoolName, ACTIVE) == SNMP_FAILURE)
        {
            break;
        }
        if (nmhSetFsVpnRaAddressPoolRowStatus
            (pVpnRaAddressPoolName, VPN_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            break;
        }

        return CLI_SUCCESS;
    }
    while (VPN_FALSE);

    CliPrintf (CliHandle,
               "%% Invalid Address pool range (or) Pool overlaps "
               "with existing network's.\r\n");
    nmhSetFsVpnRaAddressPoolRowStatus (pVpnRaAddressPoolName,
                                       VPN_STATUS_DESTROY);
    CLI_SET_ERR (VPN_ERROR);
    return CLI_FAILURE;
}

INT4
VpnDelAddressPool (tCliHandle CliHandle,
                   tSNMP_OCTET_STRING_TYPE * pVpnRaAddressPoolName)
{
    UINT4               u4SnmpErrorStatus = VPN_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsVpnRaAddressPoolRowStatus
        (&u4SnmpErrorStatus, pVpnRaAddressPoolName, VPN_STATUS_DESTROY)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsVpnRaAddressPoolRowStatus
        (pVpnRaAddressPoolName, VPN_STATUS_DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "%% Unable to delete! Address Pool is IN USE\r\n");
        CLI_SET_ERR (VPN_ERROR);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VpnRAGlobalConfig                                   */
/*  Description     : This function  enables or disables the RaVPN Server */
/*  Input (s)       : i4XAuthType                                         */
/*  Output(s)       : None.                                               */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS.                            */
/**************************************************************************/
INT4
VpnRAGlobalConfig (tCliHandle CliHandle, INT4 i4XAuthType)
{
    UINT4               u4ErrCode = VPN_ZERO;

    if (nmhTestv2FsVpnRaServer (&u4ErrCode, i4XAuthType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Unable to Set ravpn type. There may"
                   "be active Ravpn policy configured\r\r\n");
        return CLI_FAILURE;
    }
    nmhSetFsVpnRaServer (i4XAuthType);
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VpnIkeCliTrigger                                    */
/*  Description     : This function triggers the Ike                      */
/*  Input (s)       : u4PeerIp                                            */
/*  Output(s)       : None.                                               */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS.                            */
/**************************************************************************/

INT4
VpnIkeCliTrigger (tCliHandle CliHandle, UINT4 u4PeerIp)
{
    UINT1               u1IkeVersion = VPN_ZERO;
    tVpnIpAddr          PeerIpAddr;
    tIkeIpAddr          IkePeer;

    if (u4PeerIp == VPN_ZERO)
    {
        CliPrintf (CliHandle, "Unable to Set Trigger IP Addr\r\n");
        return CLI_FAILURE;
    }

    MEMSET (&PeerIpAddr, VPN_ZERO, sizeof (tVpnIpAddr));
    MEMCPY (&PeerIpAddr.uIpAddr.Ip4Addr, &u4PeerIp, sizeof (UINT4));
    PeerIpAddr.u4AddrType = IPVX_ADDR_FMLY_IPV4;

    if (VpnUtilGetIkeVersion (&PeerIpAddr, &u1IkeVersion) == VPN_FAILURE)
    {
        CliPrintf (CliHandle, "Unable to Trigger the negotiation \r\n");
        return CLI_FAILURE;
    }

    MEMCPY (&IkePeer.Ipv4Addr, &PeerIpAddr.uIpAddr.Ip4Addr, sizeof (tIp4Addr));
    IkePeer.u4AddrType = IKE_IPSEC_ID_IPV4_ADDR;
#ifdef IKE_WANTED
    TriggerIke (&IkePeer, u1IkeVersion);
#endif
    return CLI_SUCCESS;
}

INT4
VpnShowRaUsers (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE *pVpnRaUserName = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextVpnRaUserName = NULL;
    INT4                i4RetVal = CLI_SUCCESS;
    INT4                i4Count = VPN_ZERO;
    CHR1               *pc1Status = NULL;
    INT4                i4UsrStatus = VPN_ZERO;

    do
    {
        if (((pVpnRaUserName =
              allocmem_octetstring (RA_USER_NAME_LENGTH + VPN_ONE)) == NULL)
            ||
            ((pNextVpnRaUserName =
              allocmem_octetstring (RA_USER_NAME_LENGTH + VPN_ONE)) == NULL))
        {
            i4RetVal = CLI_FAILURE;
            break;
        }
        MEMSET (pVpnRaUserName->pu1_OctetList,
                VPN_ZERO, RA_USER_NAME_LENGTH + VPN_ONE);
        MEMSET (pNextVpnRaUserName->pu1_OctetList,
                VPN_ZERO, RA_USER_NAME_LENGTH + VPN_ONE);
        i4Count = VPN_ZERO;

        if (nmhGetFirstIndexFsVpnRaUsersTable (pVpnRaUserName) == SNMP_FAILURE)
        {
            break;
        }
        MEMCPY (pNextVpnRaUserName->pu1_OctetList,
                pVpnRaUserName->pu1_OctetList, pVpnRaUserName->i4_Length);
        pNextVpnRaUserName->i4_Length = pVpnRaUserName->i4_Length;

        CliPrintf (CliHandle, "\r\nSl.No          List of RA VPN users       "
                   "\t  Status \r\n");
        CliPrintf (CliHandle, "-----     ------------------------------- "
                   "\t --------\r\n");
        do
        {
            nmhGetFsVpnRaUserRowStatus (pNextVpnRaUserName, &i4UsrStatus);
            if (i4UsrStatus == ACTIVE)
            {
                pc1Status = "Enabled";
            }
            else
            {
                pc1Status = "Disabled";
            }
            CliPrintf (CliHandle, "%4d)     %-32s \t %s\r\n", ++i4Count,
                       pNextVpnRaUserName->pu1_OctetList, pc1Status);

            STRNCPY (pVpnRaUserName->pu1_OctetList,
                     pNextVpnRaUserName->pu1_OctetList,
                     (size_t) pNextVpnRaUserName->i4_Length);
            pVpnRaUserName->i4_Length = pNextVpnRaUserName->i4_Length;
            MEMSET (pNextVpnRaUserName->pu1_OctetList,
                    VPN_ZERO, RA_USER_NAME_LENGTH + VPN_ONE);
        }
        while (nmhGetNextIndexFsVpnRaUsersTable
               (pVpnRaUserName, pNextVpnRaUserName) != SNMP_FAILURE);
    }
    while (VPN_FALSE);

    if (pVpnRaUserName != NULL)
    {
        free_octetstring (pVpnRaUserName);
    }
    if (pNextVpnRaUserName != NULL)
    {
        free_octetstring (pNextVpnRaUserName);
    }

    return i4RetVal;
}

INT4
VpnShowRaAddressPool (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE *pVpnRaAddressPoolName = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextVpnRaAddressPoolName = NULL;
    UINT1               au1IpAddr[VPN_MAX_ROW_LEN] = { VPN_ZERO };
    UINT4               u4IpAddr = VPN_ZERO;
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;

    do
    {
        if (((pVpnRaAddressPoolName =
              allocmem_octetstring (RA_ADDRESS_POOL_NAME_LENGTH + VPN_ONE))
             == NULL)
            ||
            ((pNextVpnRaAddressPoolName =
              allocmem_octetstring (RA_ADDRESS_POOL_NAME_LENGTH + VPN_ONE))
             == NULL))
        {
            break;
        }

        if (nmhGetFirstIndexFsVpnRaAddressPoolTable (pVpnRaAddressPoolName) ==
            SNMP_FAILURE)
        {
            break;
        }
        MEMCPY (pNextVpnRaAddressPoolName->pu1_OctetList,
                pVpnRaAddressPoolName->pu1_OctetList,
                pVpnRaAddressPoolName->i4_Length);
        pNextVpnRaAddressPoolName->pu1_OctetList[pVpnRaAddressPoolName->
                                                 i4_Length] = '\0';
        pNextVpnRaAddressPoolName->i4_Length = pVpnRaAddressPoolName->i4_Length;

        CliPrintf (CliHandle,
                   "\r\nAddress PoolName                Start Ip        ");
        CliPrintf (CliHandle, "End Ip          Prefix length");
        CliPrintf (CliHandle,
                   "\r\n------------------------------- --------------- ");
        CliPrintf (CliHandle, "--------------- ---------------\r\n");
        do
        {
            CliPrintf (CliHandle, "%-31s ",
                       pNextVpnRaAddressPoolName->pu1_OctetList);

            pVpnRaAddressPoolNode =
                VpnGetRaAddrPool (pNextVpnRaAddressPoolName->pu1_OctetList,
                                  pNextVpnRaAddressPoolName->i4_Length);
            if (pVpnRaAddressPoolNode == NULL)
            {
                break;
            }
            /* CAUTION: Don't pass the DS directly to VPN_INET_NTOA
             * as it changes the u4Value
             */
            if (pVpnRaAddressPoolNode->VpnRaAddressPoolStart.u4AddrType
                == IPVX_ADDR_FMLY_IPV4)
            {
                u4IpAddr = pVpnRaAddressPoolNode->VpnRaAddressPoolStart.
                    uIpAddr.Ip4Addr;
                VPN_INET_NTOA (au1IpAddr, u4IpAddr)
                    CliPrintf (CliHandle, "%-15s ", au1IpAddr);
                u4IpAddr = pVpnRaAddressPoolNode->VpnRaAddressPoolEnd.
                    uIpAddr.Ip4Addr;
                VPN_INET_NTOA (au1IpAddr, u4IpAddr)
                    CliPrintf (CliHandle, "%-15s ", au1IpAddr);

            }
            else if (pVpnRaAddressPoolNode->VpnRaAddressPoolStart.u4AddrType
                     == IPVX_ADDR_FMLY_IPV6)
            {
                CliPrintf (CliHandle, "%-15s ",
                           Ip6PrintAddr (&
                                         (pVpnRaAddressPoolNode->
                                          VpnRaAddressPoolStart.uIpAddr.
                                          Ip6Addr)));
                CliPrintf (CliHandle, "%-15s ",
                           Ip6PrintAddr (&
                                         (pVpnRaAddressPoolNode->
                                          VpnRaAddressPoolEnd.uIpAddr.
                                          Ip6Addr)));
            }

            CliPrintf (CliHandle, "%-4d\r\n",
                       pVpnRaAddressPoolNode->u4VpnRaAddressPoolPrefixLen);

            STRCPY (pVpnRaAddressPoolName->pu1_OctetList,
                    pNextVpnRaAddressPoolName->pu1_OctetList);
            pVpnRaAddressPoolName->i4_Length =
                pNextVpnRaAddressPoolName->i4_Length;
            MEMSET (pNextVpnRaAddressPoolName->pu1_OctetList, VPN_ZERO,
                    RA_ADDRESS_POOL_NAME_LENGTH + VPN_ONE);
        }
        while (nmhGetNextIndexFsVpnRaAddressPoolTable
               (pVpnRaAddressPoolName,
                pNextVpnRaAddressPoolName) != SNMP_FAILURE);

    }
    while (VPN_FALSE);

    if (pVpnRaAddressPoolName != NULL)
    {
        free_octetstring (pVpnRaAddressPoolName);
    }
    if (pNextVpnRaAddressPoolName != NULL)
    {
        free_octetstring (pNextVpnRaAddressPoolName);
    }
    return (CLI_SUCCESS);
}

/**************************************************************************/
/*  Function Name   : VpnShowGlobPktsStats                                */
/*  Description     : This function Get the In, Out, Secured and Dropped  */
/*                    Packets in the VPN module.                          */
/*  Input (s)       : CliHandle    - Handle to CLI session.               */
/*  Output(s)       : Displayes the In, Out, Secured, Dropped Packet      */
/*                    in the VPN module.                                  */
/* Returns          : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/
INT4
VpnShowGlobPktsStats (tCliHandle CliHandle)
{
    UINT4               u4InPkts = VPN_ZERO;
    UINT4               u4OutPkts = VPN_ZERO;
    UINT4               u4SecPkts = VPN_ZERO;
    UINT4               u4DropedPkts = VPN_ZERO;

    nmhGetFsVpnIpPktsIn (&u4InPkts);
    nmhGetFsVpnIpPktsOut (&u4OutPkts);
    nmhGetFsVpnPktsSecured (&u4SecPkts);
    nmhGetFsVpnPktsDropped (&u4DropedPkts);

    CliPrintf (CliHandle, "\r\n VPN Global Statistics:");
    CliPrintf (CliHandle, "\r\n ----------------------");
    CliPrintf (CliHandle, "\r\n Packets In             : %ld", u4InPkts);
    CliPrintf (CliHandle, "\r\n Packets Out            : %ld", u4OutPkts);
    CliPrintf (CliHandle, "\r\n Packets Secured        : %ld", u4SecPkts);
    CliPrintf (CliHandle, "\r\n Packets Dropped        : %ld", u4DropedPkts);
    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/**************************************************************************/
/*  Function Name   : VpnShowGlobIkeStats                                 */
/*  Description     : This function Get the Acitve, Negotiated, Rekeyed   */
/*                    and Failed Negotiations of IKE and IPSec SAs.       */
/*  Input (s)       : CliHandle    - Handle to CLI session.               */
/*  Output(s)       : Displayes the IKE and IPSec Security Associatios    */
/*                    (SA) statistics.                                    */
/* Returns          : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/
INT4
VpnShowGlobIkeStats (tCliHandle CliHandle)
{
    UINT4               u4IkeActSAs = VPN_ZERO;
    UINT4               u4IkeNegSAs = VPN_ZERO;
    UINT4               u4IkeRekeyedSAs = VPN_ZERO;
    UINT4               u4IkeNegFailedSAs = VPN_ZERO;
    UINT4               u4IPSecActSAs = VPN_ZERO;
    UINT4               u4IPSecNegSAs = VPN_ZERO;
    UINT4               u4IPSecNegFailedSAs = VPN_ZERO;
    UINT4               u4TotalRekeyedSAs = VPN_ZERO;

    nmhGetFsVpnIkeSAsActive (&u4IkeActSAs);
    nmhGetFsVpnIkeNegotiations (&u4IkeNegSAs);
    nmhGetFsVpnIkeRekeys (&u4IkeRekeyedSAs);
    nmhGetFsVpnIkeNegoFailed (&u4IkeNegFailedSAs);
    nmhGetFsVpnIPSecSAsActive (&u4IPSecActSAs);
    nmhGetFsVpnIPSecNegotiations (&u4IPSecNegSAs);
    nmhGetFsVpnTotalRekeys (&u4TotalRekeyedSAs);
    nmhGetFsVpnIPSecNegoFailed (&u4IPSecNegFailedSAs);

    CliPrintf (CliHandle, "\r\n VPN IKE-IPSec SA Statistics:");
    CliPrintf (CliHandle, "\r\n ----------------------------");
    CliPrintf (CliHandle, "\r\n IKE SAs");
    CliPrintf (CliHandle, "\r\n     Active             : %ld", u4IkeActSAs);
    CliPrintf (CliHandle, "\r\n     Negotiated         : %ld", u4IkeNegSAs);
    CliPrintf (CliHandle, "\r\n     ReKeyed            : %ld", u4IkeRekeyedSAs);
    CliPrintf (CliHandle, "\r\n     Negotiation Failed : %ld",
               u4IkeNegFailedSAs);
    CliPrintf (CliHandle, "\r\n IPSec SAs");
    CliPrintf (CliHandle, "\r\n     Active             : %ld", u4IPSecActSAs);
    CliPrintf (CliHandle, "\r\n     Negotiated         : %ld", u4IPSecNegSAs);
    CliPrintf (CliHandle, "\r\n     Negotiation Failed : %ld",
               u4IPSecNegFailedSAs);
    CliPrintf (CliHandle, "\r\n     ReKeyed            : %ld",
               u4TotalRekeyedSAs);
    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/**************************************************************************/
/*  Function Name   : VpnShowConfiguration                                */
/*  Description     : This function Show Vpn Module Configurations        */
/*  Input (s)       : CliHandle    - Handle to CLI session.               */
/*  Output(s)       : This function Display Vpn Module Configurations     */
/* Returns          : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/
INT4
VpnShowConfiguration (tCliHandle CliHandle)
{
    INT4                i4MaxTlns = VPN_ZERO;
    INT4                i4Status = VPN_ZERO;
    UINT4               u4ConfgTnls = VPN_ZERO;
    INT4                i4RaMode = VPN_INVALID;

    nmhGetFsVpnGlobalStatus (&i4Status);
    nmhGetFsVpnMaxTunnels (&i4MaxTlns);
    VpnUtilGetConfigTunnels (&u4ConfgTnls);
    nmhGetFsVpnRaServer (&i4RaMode);

    CliPrintf (CliHandle, "\r\nVPN Global Configuration\r\n");
    CliPrintf (CliHandle, "------------------------\r\n");
    if (i4Status == VPN_ENABLE)
    {
        CliPrintf (CliHandle, "IPSecurity Status            : Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "IPSecurity Status            : Disabled");
    }

    if (i4RaMode == (INT4) RAVPN_SERVER)
    {
        CliPrintf (CliHandle, "\r\nRa-Vpn Mode                  : Server");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nRa-Vpn Mode                  : Client");
    }

    CliPrintf (CliHandle, "\r\nMaximum Number of Tunnels    : %ld", i4MaxTlns);
    CliPrintf (CliHandle, "\r\nNumber of Tunnels configured : %ld",
               u4ConfgTnls);
    CliPrintf (CliHandle, "\r\nMaximum no.of Address pools  : %d",
               RA_VPN_MAX_ADDRESS_POOL);
    CliPrintf (CliHandle, "\r\nNo.of Address pools utilised : %d",
               TMO_SLL_Count (&gVpnRaAddressPoolList));
    CliPrintf (CliHandle, "\r\nMaximum RA users allowed     : %d",
               RA_VPN_MAX_USERS_DB);
    CliPrintf (CliHandle, "\r\nNumber of RA users exist     : %d",
               TMO_SLL_Count (&gVpnRaUserList));
    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

INT4
VpnShowRemoteIdInfo (tCliHandle CliHandle, INT4 i4IdType)
{
    tVpnIdInfo         *pVpnIdInfoNode = NULL;
    UINT1              *pu1IdType = NULL;
    INT4                i4Count = VPN_ZERO;

    CliPrintf (CliHandle, "\r\nRemote identity information table:\r\n");
    CliPrintf (CliHandle, "Identity type \t Identity Value\r\n");
    CliPrintf (CliHandle, "------------- \t --------------\r\n");
    TMO_SLL_Scan (&gVpnRemoteIdList, pVpnIdInfoNode, tVpnIdInfo *)
    {
        if ((i4IdType != VPN_ZERO)
            && (i4IdType != (INT4) VPN_ID_TYPE (pVpnIdInfoNode)))
        {
            continue;
        }

        switch (VPN_ID_TYPE (pVpnIdInfoNode))
        {
            case VPN_ID_TYPE_IPV4:
                pu1IdType = (UINT1 *) "IPv4";
                break;
            case VPN_ID_TYPE_IPV6:
                pu1IdType = (UINT1 *) "IPv6";
                break;
            case VPN_ID_TYPE_FQDN:
                pu1IdType = (UINT1 *) "Fqdn";
                break;
            case VPN_ID_TYPE_DN:
                pu1IdType = (UINT1 *) "dn";
                break;
            case VPN_ID_TYPE_EMAIL:
                pu1IdType = (UINT1 *) "E-Mail";
                break;
            case VPN_ID_TYPE_KEYID:
                pu1IdType = (UINT1 *) "Key-Id";
                break;
            default:
                pu1IdType = (UINT1 *) "Unknown";
                break;
        }

        CliPrintf (CliHandle, "%-13s \t %s\r\n", pu1IdType,
                   VPN_ID_VALUE (pVpnIdInfoNode));
        ++i4Count;
    }

    CliPrintf (CliHandle, "\r\nNo.of remote-id's displayed: %d\r\n", i4Count);

    return (CLI_SUCCESS);
}

/*****************************************************************************
 VpnSetIpv6Peer : Sets the Peer with Ipv6 address  for the corresponding
                   VPN Policy 
Input          : Policy Name and the corresponding Peer IpV6- address         *
 *****************************************************************************/
INT4
VpnSetIpv6Peer (tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE * pPolicyName,
                tIp6Addr * pIp6Addr)
{
    UINT4               u4SnmpErrorStatus = CLI_FAILURE;
    INT4                i4AddrType = IPVX_ADDR_FMLY_IPV6;
    tSNMP_OCTET_STRING_TYPE Ipv6PeerAddr;

    UNUSED_PARAM (CliHandle);

    Ipv6PeerAddr.pu1_OctetList = pIp6Addr->u1_addr;
    Ipv6PeerAddr.i4_Length = sizeof (tIp6Addr);

    if (nmhTestv2FsVpnTunTermAddrType (&u4SnmpErrorStatus, pPolicyName,
                                       i4AddrType) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_PEER_INFO);
        return CLI_FAILURE;
    }
    nmhSetFsVpnTunTermAddrType (pPolicyName, i4AddrType);

    if (nmhTestv2FsVpnRemoteTunTermAddr (&u4SnmpErrorStatus, pPolicyName,
                                         &Ipv6PeerAddr) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_PEER_INFO);
        return CLI_FAILURE;
    }
    nmhSetFsVpnRemoteTunTermAddr (pPolicyName, &Ipv6PeerAddr);
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VpnIkeIpv6CliTrigger                                */
/*  Description     : This function triggers the Ike                      */
/*  Input (s)       : Peer IPv6 Address                                   */
/*  Output(s)       : None.                                               */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS.                            */
/**************************************************************************/

INT4
VpnIkeIpv6CliTrigger (tCliHandle CliHandle, tIp6Addr * pPeerAddress)
{
    tVpnIpAddr          PeerIpAddr;
    tIkeIpAddr          IkePeer;
    UINT1               u1IkeVersion = VPN_ZERO;

    MEMSET (&IkePeer, VPN_ZERO, sizeof (tVpnIpAddr));
    MEMSET (&PeerIpAddr, VPN_ZERO, sizeof (tVpnIpAddr));

    MEMCPY (&PeerIpAddr.Ipv6Addr, pPeerAddress, sizeof (tIp6Addr));
    PeerIpAddr.u4AddrType = IPVX_ADDR_FMLY_IPV6;

    if (VpnUtilGetIkeVersion (&PeerIpAddr, &u1IkeVersion) == VPN_FAILURE)
    {
        CliPrintf (CliHandle, "Unable to Trigger the negotiation \r\n");
        return CLI_FAILURE;
    }

    MEMCPY (&IkePeer.Ipv6Addr, &PeerIpAddr.Ipv6Addr, sizeof (tIp6Addr));
    IkePeer.u4AddrType = IKE_IPSEC_ID_IPV6_ADDR;
#ifdef IKE_WANTED
    TriggerIke (&IkePeer, u1IkeVersion);
#endif
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VpnIpv6AddAddressPool                               */
/*  Description     : Configures the IPv6 address pool for remote users   */
/*                    connected via VPN tunnel.                           */
/*  Input (s)       : Start RA VPN IPv6 Address, End RA VPN IPv6 Address  */
/*                    Prefix length.                                       */
/*  Output(s)       : None.                                               */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS.                            */
/**************************************************************************/

INT4
VpnIpv6AddAddressPool (tCliHandle CliHandle,
                       tSNMP_OCTET_STRING_TYPE * pVpnRaAddressPoolName,
                       tIp6Addr * StartIpv6Address,
                       tIp6Addr * EndIpv6Address, UINT4 u4PrefixLength)
{
    UINT4               u4SnmpErrorStatus = CLI_FAILURE;
    INT4                i4AddrType = IPVX_ADDR_FMLY_IPV6;
    tSNMP_OCTET_STRING_TYPE Ipv6PeerAddr;

    if (nmhTestv2FsVpnRaAddressPoolRowStatus (&u4SnmpErrorStatus,
                                              pVpnRaAddressPoolName,
                                              VPN_STATUS_CREATE_AND_WAIT) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhSetFsVpnRaAddressPoolRowStatus (pVpnRaAddressPoolName,
                                       VPN_STATUS_CREATE_AND_WAIT);

    do
    {
        if (nmhTestv2FsVpnRaAddressPoolAddrType (&u4SnmpErrorStatus,
                                                 pVpnRaAddressPoolName,
                                                 i4AddrType) == SNMP_FAILURE)
        {
            break;
        }
        nmhSetFsVpnRaAddressPoolAddrType (pVpnRaAddressPoolName, i4AddrType);

        Ipv6PeerAddr.pu1_OctetList = StartIpv6Address->u1_addr;
        Ipv6PeerAddr.i4_Length = sizeof (tIp6Addr);
        if (nmhTestv2FsVpnRaAddressPoolStart (&u4SnmpErrorStatus,
                                              pVpnRaAddressPoolName,
                                              &Ipv6PeerAddr) == SNMP_FAILURE)
        {
            break;
        }
        nmhSetFsVpnRaAddressPoolStart (pVpnRaAddressPoolName, &Ipv6PeerAddr);

        Ipv6PeerAddr.pu1_OctetList = EndIpv6Address->u1_addr;
        Ipv6PeerAddr.i4_Length = sizeof (tIp6Addr);
        if (nmhTestv2FsVpnRaAddressPoolEnd (&u4SnmpErrorStatus,
                                            pVpnRaAddressPoolName,
                                            &Ipv6PeerAddr) == SNMP_FAILURE)
        {
            break;
        }
        nmhSetFsVpnRaAddressPoolEnd (pVpnRaAddressPoolName, &Ipv6PeerAddr);

        if (nmhTestv2FsVpnRaAddressPoolPrefixLen (&u4SnmpErrorStatus,
                                                  pVpnRaAddressPoolName,
                                                  u4PrefixLength) ==
            SNMP_FAILURE)
        {
            break;
        }
        nmhSetFsVpnRaAddressPoolPrefixLen (pVpnRaAddressPoolName,
                                           u4PrefixLength);

        if (nmhTestv2FsVpnRaAddressPoolRowStatus (&u4SnmpErrorStatus,
                                                  pVpnRaAddressPoolName,
                                                  ACTIVE) == SNMP_FAILURE)
        {
            break;
        }
        if (nmhSetFsVpnRaAddressPoolRowStatus (pVpnRaAddressPoolName,
                                               VPN_STATUS_ACTIVE) ==
            SNMP_FAILURE)
        {
            break;
        }

        return CLI_SUCCESS;
    }
    while (VPN_FALSE);

    CliPrintf (CliHandle,
               "%% Invalid Address pool range (or) Pool overlaps "
               "with existing network's.\r\n");
    nmhSetFsVpnRaAddressPoolRowStatus (pVpnRaAddressPoolName,
                                       VPN_STATUS_DESTROY);
    CLI_SET_ERR (VPN_ERROR);
    return CLI_FAILURE;
}

INT4
VpnSetIpv6AccessParams (tSNMP_OCTET_STRING_TYPE * pPolicyName,
                        tVpnNetwork * pLocalNet, tVpnNetwork * pRemoteNet,
                        UINT4 u4Action, UINT4 u4Protocol,
                        tSNMP_OCTET_STRING_TYPE * pSrcPortRange,
                        tSNMP_OCTET_STRING_TYPE * pDstPortRange)
{
    tSNMP_OCTET_STRING_TYPE TempIpv6Addr;
    UINT4               u4SnmpErrorStatus = CLI_FAILURE;
    INT4                i4AddrType = IPVX_ADDR_FMLY_IPV6;

    if (nmhTestv2FsVpnProtectNetworkType (&u4SnmpErrorStatus, pPolicyName,
                                          i4AddrType) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_IPV6_ACL_PARAM);
        return CLI_FAILURE;
    }
    nmhSetFsVpnProtectNetworkType (pPolicyName, i4AddrType);

    TempIpv6Addr.pu1_OctetList = pLocalNet->IpAddr.uIpAddr.Ip6Addr.u1_addr;
    TempIpv6Addr.i4_Length = sizeof (tIp6Addr);

    if (nmhTestv2FsVpnLocalProtectNetwork (&u4SnmpErrorStatus, pPolicyName,
                                           &TempIpv6Addr) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_IPV6_ACL_PARAM);
        return CLI_FAILURE;
    }
    nmhSetFsVpnLocalProtectNetwork (pPolicyName, &TempIpv6Addr);

    if (nmhTestv2FsVpnLocalProtectNetworkPrefixLen (&u4SnmpErrorStatus,
                                                    pPolicyName,
                                                    pLocalNet->
                                                    u4AddrPrefixLen) ==
        SNMP_FAILURE)

    {
        CLI_SET_ERR (CLI_VPN_ERR_IPV6_ACL_PARAM);
        return CLI_FAILURE;
    }
    nmhSetFsVpnLocalProtectNetworkPrefixLen (pPolicyName,
                                             pLocalNet->u4AddrPrefixLen);

    TempIpv6Addr.pu1_OctetList = pRemoteNet->IpAddr.Ipv6Addr.u1_addr;
    TempIpv6Addr.i4_Length = sizeof (tIp6Addr);

    if (nmhTestv2FsVpnRemoteProtectNetwork (&u4SnmpErrorStatus, pPolicyName,
                                            &TempIpv6Addr) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_IPV6_ACL_PARAM);
        return CLI_FAILURE;
    }
    nmhSetFsVpnRemoteProtectNetwork (pPolicyName, &TempIpv6Addr);

    if (nmhTestv2FsVpnRemoteProtectNetworkPrefixLen (&u4SnmpErrorStatus,
                                                     pPolicyName,
                                                     pRemoteNet->
                                                     u4AddrPrefixLen) ==
        SNMP_FAILURE)

    {
        CLI_SET_ERR (CLI_VPN_ERR_IPV6_ACL_PARAM);
        return CLI_FAILURE;
    }
    nmhSetFsVpnRemoteProtectNetworkPrefixLen (pPolicyName,
                                              pRemoteNet->u4AddrPrefixLen);

    if (nmhTestv2FsVpnPolicyFlag
        (&u4SnmpErrorStatus, pPolicyName, (INT4) u4Action) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_IPV6_ACL_PARAM);
        return CLI_FAILURE;
    }
    nmhSetFsVpnPolicyFlag (pPolicyName, (INT4) u4Action);

    if (nmhTestv2FsVpnProtocol (&u4SnmpErrorStatus, pPolicyName,
                                (INT4) u4Protocol) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_ERR_IPV6_ACL_PARAM);
        return CLI_FAILURE;
    }
    nmhSetFsVpnProtocol (pPolicyName, (INT4) u4Protocol);

    if (pSrcPortRange != NULL)
    {
        if (nmhTestv2FsVpnIkeSrcPortRange (&u4SnmpErrorStatus, pPolicyName,
                                           pSrcPortRange) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_VPN_ERR_INVALID_PORT_RANGE);
            return CLI_FAILURE;
        }

        if (nmhSetFsVpnIkeSrcPortRange (pPolicyName, pSrcPortRange)
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_VPN_ERR_INVALID_PORT_RANGE);
            return CLI_FAILURE;
        }
    }

    if (pDstPortRange != NULL)
    {
        if (nmhTestv2FsVpnIkeDstPortRange (&u4SnmpErrorStatus, pPolicyName,
                                           pDstPortRange) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_VPN_ERR_INVALID_PORT_RANGE);
            return CLI_FAILURE;
        }
        if (nmhSetFsVpnIkeDstPortRange (pPolicyName, pDstPortRange) ==
            SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_VPN_ERR_INVALID_PORT_RANGE);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VpnCliIkeTrace                                      */
/*  Description     : This function sets/resets the IKE trace level       */
/*  Input (s)       : CliHandle, u1TraceFlag, i4TraceLevel                */
/*  Output(s)       : None.                                               */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS.                            */
/**************************************************************************/
INT4
VpnCliIkeTrace (tCliHandle CliHandle, UINT1 u1TraceFlag, INT4 i4TraceLevel)
{

    INT4                i4TraceVal = VPN_ZERO;
    UINT4               u4ErrCode = VPN_ZERO;

    nmhGetFsIkeTraceOption (&i4TraceVal);

    if (u1TraceFlag == CLI_ENABLE)
    {
        i4TraceVal |= i4TraceLevel;
    }
    else
    {
        i4TraceVal &= (~(i4TraceLevel));
        if (i4TraceLevel == VPN_ZERO)
        {
            i4TraceVal = VPN_ZERO;
        }
    }
    if (nmhTestv2FsIkeTraceOption (&u4ErrCode, i4TraceVal) == SNMP_SUCCESS)
    {

        if (nmhSetFsIkeTraceOption (i4TraceVal) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to set trace messages\r\n");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;

}

/**************************************************************************/
/*  Function Name   : VpnCliIpsecTrace                                    */
/*  Description     : This function sets/resets the Ipsec trace level     */
/*  Input (s)       : CliHandle, u1TraceFlag, i4TraceLevel                */
/*  Output(s)       : None.                                               */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS.                            */
/**************************************************************************/

INT4
VpnCliIpsecTrace (tCliHandle CliHandle, UINT1 u1TraceFlag, INT4 i4TraceLevel)
{

    INT4                i4TraceVal = VPN_ZERO;
    UINT4               u4ErrCode = VPN_ZERO;
    nmhGetFsIpsecTraceOption (&i4TraceVal);

    if (u1TraceFlag == CLI_ENABLE)
    {
        i4TraceVal |= i4TraceLevel;
    }
    else
    {
        i4TraceVal &= (~(i4TraceLevel));
        if (i4TraceLevel == VPN_ZERO)
        {
            i4TraceVal = VPN_ZERO;
        }
    }
    if (nmhTestv2FsIpsecTraceOption (&u4ErrCode, i4TraceVal) == SNMP_SUCCESS)
    {

        if (nmhSetFsIpsecTraceOption (i4TraceVal) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to set trace messages\r\n");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;

}

/**************************************************************************/
/*  Function Name   : VpnCliIkeImportKey                                  */
/*  Description     : This function adds an entry in the certificate      */
/*                    information table                                   */
/*  Input (s)       : CliHandle, pu1KeyName - pointer to Key identifier   */
/*                    pu1FileName - pointer to key filename               */
/*                    u1Type - indicates the algorithm of the key         */
/*  Output(s)       : None.                                               */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS.                            */
/**************************************************************************/
UINT1
VpnCliIkeImportKey (tCliHandle CliHandle,
                    UINT1 *pu1KeyName, UINT1 *pu1FileName, INT4 i4Type)
{
    UINT4               u4SnmpErrorStatus = VPN_ZERO;
    tSNMP_OCTET_STRING_TYPE VpnCertKeyString;
    tSNMP_OCTET_STRING_TYPE VpnCertKeyFileNameString;
    INT4                i4RetVal = VPN_ZERO;

    MEMSET (&VpnCertKeyString, VPN_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    VpnCertKeyString.pu1_OctetList = pu1KeyName;
    VpnCertKeyString.i4_Length = (INT4) STRLEN (pu1KeyName);

    if (nmhTestv2FsVpnCertStatus (&u4SnmpErrorStatus, &VpnCertKeyString,
                                  CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        i4RetVal = CLI_GET_ERR (&u4SnmpErrorStatus);
        UNUSED_PARAM (i4RetVal);
        if (u4SnmpErrorStatus == VPN_ZERO)
        {
            CliPrintf (CliHandle, "%% Failed to import Key\r\n");
            CLI_SET_ERR (VPN_ZERO);
        }
        return CLI_FAILURE;
    }

    if (nmhSetFsVpnCertStatus (&VpnCertKeyString,
                               CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_MEM_ALLOC_FAIL);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsVpnCertKeyType (&u4SnmpErrorStatus, &VpnCertKeyString,
                                   i4Type) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Invalid Key Type\r\n");
        CLI_SET_ERR (VPN_ZERO);
        /* Destroying the incomplete entry. Incase of failure */
        nmhSetFsVpnCertStatus (&VpnCertKeyString, DESTROY);
        return CLI_FAILURE;
    }

    MEMSET (&VpnCertKeyFileNameString,
            VPN_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    VpnCertKeyFileNameString.pu1_OctetList = pu1FileName;
    VpnCertKeyFileNameString.i4_Length = (INT4) STRLEN (pu1FileName);

    if (nmhTestv2FsVpnCertKeyFileName (&u4SnmpErrorStatus,
                                       &VpnCertKeyString,
                                       &VpnCertKeyFileNameString)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Invalid Key Filename \r\n");
        CLI_SET_ERR (VPN_ZERO);
        /* Destroying the incomplete entry. Incase of failure */
        nmhSetFsVpnCertStatus (&VpnCertKeyString, DESTROY);
        return CLI_FAILURE;
    }

    if (nmhSetFsVpnCertKeyType (&VpnCertKeyString, i4Type) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        /* Destroying the incomplete entry. Incase of failure */
        nmhSetFsVpnCertStatus (&VpnCertKeyString, DESTROY);
        return CLI_FAILURE;
    }

    if (nmhSetFsVpnCertKeyFileName (&VpnCertKeyString,
                                    &VpnCertKeyFileNameString) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        /* Destroying the incomplete entry. Incase of failure */
        nmhSetFsVpnCertStatus (&VpnCertKeyString, DESTROY);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VpnCliIkeImportCert                                 */
/*  Description     : This function adds an entry in the certificate      */
/*                    information table                                   */
/*  Input (s)       : CliHandle, pu1KeyName - pointer to Key identifier   */
/*                    pu1FileName - pointer to certificate filename       */
/*                    u1EncodeType - indicates the encode type used       */
/*  Output(s)       : None.                                               */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS.                            */
/**************************************************************************/
UINT1
VpnCliIkeImportCert (tCliHandle CliHandle,
                     UINT1 *pu1FileName, UINT1 *pu1KeyName, UINT1 u1EncodeType)
{
    UINT4               u4SnmpErrorStatus = VPN_ZERO;
    tSNMP_OCTET_STRING_TYPE VpnCertKeyString;
    tSNMP_OCTET_STRING_TYPE VpnCertFileNameString;
    INT4                i4RetVal = VPN_ZERO;

    MEMSET (&VpnCertKeyString, VPN_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    VpnCertKeyString.pu1_OctetList = pu1KeyName;
    VpnCertKeyString.i4_Length = (INT4) STRLEN (pu1KeyName);

    if (VpnGetCertInfoFromKey (VpnCertKeyString.pu1_OctetList,
                               VpnCertKeyString.i4_Length) == NULL)
    {
        CliPrintf (CliHandle, "%% Certificate Key entry is not present\r\n");
        CLI_SET_ERR (VPN_ZERO);
        return CLI_FAILURE;
    }

    MEMSET (&VpnCertFileNameString, VPN_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    VpnCertFileNameString.pu1_OctetList = pu1FileName;
    VpnCertFileNameString.i4_Length = (INT4) STRLEN (pu1FileName);

    if (nmhTestv2FsVpnCertEncodeType (&u4SnmpErrorStatus,
                                      &VpnCertKeyString,
                                      u1EncodeType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Not a valid Encode type\r\n");
        CLI_SET_ERR (VPN_ZERO);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsVpnCertFileName (&u4SnmpErrorStatus,
                                    &VpnCertKeyString,
                                    &VpnCertFileNameString) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Max allowed file name length is %d\r\n",
                   VPN_CERT_KEY_FILE_NAME_LEN);
        CLI_SET_ERR (VPN_ERROR);
        return CLI_FAILURE;
    }

    if (nmhSetFsVpnCertEncodeType (&VpnCertKeyString,
                                   u1EncodeType) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsVpnCertFileName (&VpnCertKeyString,
                                 &VpnCertFileNameString) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsVpnCertStatus (&u4SnmpErrorStatus,
                                  &VpnCertKeyString, ACTIVE) == SNMP_FAILURE)
    {
        i4RetVal = CLI_GET_ERR (&u4SnmpErrorStatus);
        UNUSED_PARAM (i4RetVal);
        if (u4SnmpErrorStatus == VPN_ZERO)
        {
            CliPrintf (CliHandle,
                       "%% Failed to add the Certificate information\r\n");
            CLI_SET_ERR (VPN_ERROR);
        }
        return CLI_FAILURE;
    }

    if (nmhSetFsVpnCertStatus (&VpnCertKeyString, ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VpnCliIkeDelKeys                                    */
/*  Description     : This function deletes entry in the certificate      */
/*                    information table                                   */
/*  Input (s)       : CliHandle, pu1KeyName - pointer to Key identifier   */
/*  Output(s)       : None.                                               */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS.                            */
/**************************************************************************/
UINT1
VpnCliIkeDelKeys (tCliHandle CliHandle, UINT1 *pu1KeyName)
{
    UINT4               u4SnmpErrorStatus = VPN_ZERO;
    UINT1               u1FailFlag = CLI_SUCCESS;
    tSNMP_OCTET_STRING_TYPE VpnCertKeyString;
    tSNMP_OCTET_STRING_TYPE VpnNextCertKeyString;
    UINT1               au1CertKeyId[VPN_MAX_PRESHARED_KEY_LEN + VPN_ONE]
        = { VPN_ZERO };
    UINT1
         
         
         
         
         
         
          au1NextCertKeyId[VPN_MAX_PRESHARED_KEY_LEN + VPN_ONE] = { VPN_ZERO };
    INT4                i4RetVal = VPN_ZERO;

    MEMSET (&VpnCertKeyString, VPN_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1CertKeyId, VPN_ZERO, VPN_MAX_PRESHARED_KEY_LEN + VPN_ONE);
    VpnCertKeyString.pu1_OctetList = au1CertKeyId;
    VpnCertKeyString.i4_Length = VPN_ZERO;

    MEMSET (&VpnNextCertKeyString, VPN_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1NextCertKeyId, VPN_ZERO, VPN_MAX_PRESHARED_KEY_LEN + VPN_ONE);
    VpnNextCertKeyString.pu1_OctetList = au1CertKeyId;
    VpnNextCertKeyString.i4_Length = VPN_ZERO;

    /* pu1KeyName = CLI_VPN_DEL_ALL, if del all option is selected,
     * in CLI */
    if (STRCMP (pu1KeyName, CLI_VPN_DEL_ALL) == VPN_ZERO)
    {
        if (nmhGetFirstIndexFsVpnCertInfoTable (&VpnNextCertKeyString)
            == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }

        do
        {
            MEMCPY (VpnCertKeyString.pu1_OctetList,
                    VpnNextCertKeyString.pu1_OctetList,
                    VPN_MAX_PRESHARED_KEY_LEN + VPN_ONE);
            VpnCertKeyString.i4_Length = VpnNextCertKeyString.i4_Length;
            if (nmhTestv2FsVpnCertStatus (&u4SnmpErrorStatus, &VpnCertKeyString,
                                          DESTROY) == SNMP_FAILURE)
            {
                u1FailFlag = CLI_FAILURE;
                continue;
            }

            if (nmhSetFsVpnCertStatus (&VpnCertKeyString,
                                       DESTROY) == SNMP_FAILURE)
            {
                u1FailFlag = CLI_FAILURE;
                continue;
            }

            MEMSET (VpnNextCertKeyString.pu1_OctetList, VPN_ZERO,
                    VPN_MAX_PRESHARED_KEY_LEN + VPN_ONE);
            VpnNextCertKeyString.i4_Length = VPN_ZERO;
        }
        while (nmhGetNextIndexFsVpnCertInfoTable (&VpnCertKeyString,
                                                  &VpnNextCertKeyString) !=
               SNMP_FAILURE);

        if (u1FailFlag == CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%% Failed to remove all the certificates\r\n");
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }
    else
    {
        VpnCertKeyString.pu1_OctetList = pu1KeyName;
        VpnCertKeyString.i4_Length = (INT4) STRLEN (pu1KeyName);

        if (nmhTestv2FsVpnCertStatus (&u4SnmpErrorStatus, &VpnCertKeyString,
                                      DESTROY) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%% Failed to remove the certificate\r\n");
            i4RetVal = CLI_GET_ERR (&u4SnmpErrorStatus);
            UNUSED_PARAM (i4RetVal);
            return CLI_FAILURE;
        }

        if (nmhSetFsVpnCertStatus (&VpnCertKeyString, DESTROY) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VpnCliIkeDelCerts                                   */
/*  Description     : This function deletes entry in the certificate      */
/*                    information table                                   */
/*  Input (s)       : CliHandle, pu1KeyName - pointer to Key identifier   */
/*  Output(s)       : None.                                               */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS.                            */
/**************************************************************************/
UINT1
VpnCliIkeDelCerts (tCliHandle CliHandle, UINT1 u1Option, UINT1 *pu1KeyName)
{
    tSNMP_OCTET_STRING_TYPE VpnCertKeyString;
    tSNMP_OCTET_STRING_TYPE VpnNextCertKeyString;

    UINT1               au1CertKeyId[VPN_MAX_PRESHARED_KEY_LEN + VPN_ONE]
        = { VPN_ZERO };
    UINT1
         
         
         
         
         
         
          au1NextCertKeyId[VPN_MAX_PRESHARED_KEY_LEN + VPN_ONE] = { VPN_ZERO };
    UINT1               u1FailFlag = CLI_SUCCESS;
    UINT4               u4SnmpErrorStatus = VPN_ZERO;

    MEMSET (&VpnCertKeyString, VPN_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1CertKeyId, VPN_ZERO, VPN_MAX_PRESHARED_KEY_LEN + VPN_ONE);
    VpnCertKeyString.pu1_OctetList = au1CertKeyId;
    VpnCertKeyString.i4_Length = VPN_ZERO;

    MEMSET (&VpnNextCertKeyString, VPN_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1NextCertKeyId, VPN_ZERO, VPN_MAX_PRESHARED_KEY_LEN + VPN_ONE);
    VpnNextCertKeyString.pu1_OctetList = au1NextCertKeyId;
    VpnNextCertKeyString.i4_Length = VPN_ZERO;

    switch (u1Option)
    {
        case CLI_VPN_ERASE_CERT:
            /* pu1KeyName = CLI_VPN_DEL_ALL, if del all option is selected,
             * in CLI */
            if (STRCMP (pu1KeyName, CLI_VPN_DEL_ALL) == VPN_ZERO)
            {
                if (nmhGetFirstIndexFsVpnCertInfoTable (&VpnNextCertKeyString)
                    == SNMP_FAILURE)
                {
                    break;
                }

                do
                {
                    MEMCPY (VpnCertKeyString.pu1_OctetList,
                            VpnNextCertKeyString.pu1_OctetList,
                            VPN_MAX_PRESHARED_KEY_LEN + VPN_ONE);
                    VpnCertKeyString.i4_Length = VpnNextCertKeyString.i4_Length;
                    if (nmhTestv2FsVpnCertStatus (&u4SnmpErrorStatus,
                                                  &VpnCertKeyString,
                                                  NOT_IN_SERVICE) ==
                        SNMP_FAILURE)
                    {
                        u1FailFlag = CLI_FAILURE;
                        continue;
                    }

                    if (nmhSetFsVpnCertStatus (&VpnCertKeyString,
                                               NOT_IN_SERVICE) == SNMP_FAILURE)
                    {
                        u1FailFlag = CLI_FAILURE;
                        continue;
                    }
                    MEMSET (VpnNextCertKeyString.pu1_OctetList, VPN_ZERO,
                            VPN_MAX_PRESHARED_KEY_LEN + VPN_ONE);
                    VpnNextCertKeyString.i4_Length = VPN_ZERO;
                }
                while (nmhGetNextIndexFsVpnCertInfoTable (&VpnCertKeyString,
                                                          &VpnNextCertKeyString)
                       != SNMP_FAILURE);

                if (u1FailFlag == CLI_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "%% Unable to remove all certificate info\r\n");
                    return CLI_FAILURE;
                }
            }
            else
            {
                VpnCertKeyString.pu1_OctetList = pu1KeyName;
                VpnCertKeyString.i4_Length = (INT4) STRLEN (pu1KeyName);

                /* Test if the Certificate entry exist... */
                if (nmhTestv2FsVpnCertStatus
                    (&u4SnmpErrorStatus, &VpnCertKeyString,
                     NOT_IN_SERVICE) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "%% Certificate Info doesn't exist\r\n");
                    return CLI_FAILURE;
                }

                if (nmhSetFsVpnCertStatus (&VpnCertKeyString, NOT_IN_SERVICE)
                    == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
            }
            break;

        case CLI_VPN_ERASE_CA_CERT:

            if (STRCMP (pu1KeyName, CLI_VPN_DEL_ALL) == VPN_ZERO)
            {
                if (nmhGetFirstIndexFsVpnCaCertInfoTable (&VpnNextCertKeyString)
                    == SNMP_FAILURE)
                {
                    break;
                }

                do
                {
                    MEMCPY (VpnCertKeyString.pu1_OctetList,
                            VpnNextCertKeyString.pu1_OctetList,
                            VPN_MAX_PRESHARED_KEY_LEN + VPN_ONE);
                    VpnCertKeyString.i4_Length = VpnNextCertKeyString.i4_Length;
                    if (nmhTestv2FsVpnCaCertStatus (&u4SnmpErrorStatus,
                                                    &VpnCertKeyString,
                                                    DESTROY) == SNMP_FAILURE)
                    {
                        u1FailFlag = CLI_FAILURE;
                        continue;
                    }

                    if (nmhSetFsVpnCaCertStatus (&VpnCertKeyString,
                                                 DESTROY) == SNMP_FAILURE)
                    {
                        u1FailFlag = CLI_FAILURE;
                        continue;
                    }
                    MEMSET (VpnNextCertKeyString.pu1_OctetList, VPN_ZERO,
                            VPN_MAX_PRESHARED_KEY_LEN + VPN_ONE);
                    VpnNextCertKeyString.i4_Length = VPN_ZERO;

                }
                while (nmhGetNextIndexFsVpnCaCertInfoTable (&VpnCertKeyString,
                                                            &VpnNextCertKeyString)
                       != SNMP_FAILURE);

                if (u1FailFlag == CLI_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "%% Unable to remove all certificate info\r\n");
                    return CLI_FAILURE;
                }

            }
            else
            {
                MEMSET (&VpnCertKeyString,
                        VPN_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
                VpnCertKeyString.pu1_OctetList = pu1KeyName;
                VpnCertKeyString.i4_Length = (INT4) STRLEN (pu1KeyName);

                /* Test if the Certificate entry exist... */
                if (nmhTestv2FsVpnCaCertStatus (&u4SnmpErrorStatus,
                                                &VpnCertKeyString,
                                                DESTROY) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "%% Certificate Info doesn't exist\r\n");
                    return CLI_FAILURE;
                }

                if (nmhSetFsVpnCaCertStatus (&VpnCertKeyString,
                                             DESTROY) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
            }
            break;
        default:
            break;
    }
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VpnCliIkeImportCACert                               */
/*  Description     : This function adds an entry in the certificate      */
/*                    information table                                   */
/*  Input (s)       : CliHandle,                                          */
/*                    pu1CaCertKeyString - pointer to CA Key identifier   */
/*                    pu1CaFileName - pointer to CA certificate filename  */
/*                    u1EncodeType - indicates the encode type used       */
/*  Output(s)       : None.                                               */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS.                            */
/**************************************************************************/

UINT1
VpnCliIkeImportCACert (tCliHandle CliHandle,
                       UINT1 *pu1CaCertKeyString,
                       UINT1 *pu1CaFileName, UINT1 u1EncodeType)
{
    UINT4               u4SnmpErrorStatus = VPN_ZERO;
    tSNMP_OCTET_STRING_TYPE VpnCaCertKeyString;
    tSNMP_OCTET_STRING_TYPE VpnCaFileName;
    INT4                i4RetVal = VPN_ZERO;

    MEMSET (&VpnCaCertKeyString, VPN_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    VpnCaCertKeyString.pu1_OctetList = pu1CaCertKeyString;
    VpnCaCertKeyString.i4_Length = (INT4) STRLEN (pu1CaCertKeyString);

    MEMSET (&VpnCaFileName, VPN_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    VpnCaFileName.pu1_OctetList = pu1CaFileName;
    VpnCaFileName.i4_Length = (INT4) STRLEN (pu1CaFileName);

    if (nmhTestv2FsVpnCaCertStatus (&u4SnmpErrorStatus, &VpnCaCertKeyString,
                                    CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        i4RetVal = CLI_GET_ERR (&u4SnmpErrorStatus);
        if (u4SnmpErrorStatus == VPN_ZERO)
        {
            CliPrintf (CliHandle, "%% Failed to add CA certificate\r\n");
            CLI_SET_ERR (VPN_ZERO);
        }
        return CLI_FAILURE;
    }

    if (nmhSetFsVpnCaCertStatus (&VpnCaCertKeyString,
                                 CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_MEM_ALLOC_FAIL);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsVpnCaCertEncodeType (&u4SnmpErrorStatus,
                                        &VpnCaCertKeyString,
                                        u1EncodeType) == SNMP_FAILURE)
    {
        /* Destroying the incomplete entry. Incase of failure */
        nmhSetFsVpnCaCertStatus (&VpnCaCertKeyString, DESTROY);
        CliPrintf (CliHandle, "%% Invalid Encode type\r\n");
        CLI_SET_ERR (VPN_ZERO);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsVpnCaCertFileName (&u4SnmpErrorStatus, &VpnCaCertKeyString,
                                      &VpnCaFileName) == SNMP_FAILURE)
    {
        /* Destroying the incomplete entry. Incase of failure */
        nmhSetFsVpnCaCertStatus (&VpnCaCertKeyString, DESTROY);
        CliPrintf (CliHandle, "%% Invalid CA certificate Filename\r\n");
        CLI_SET_ERR (VPN_ZERO);
        return CLI_FAILURE;
    }

    if (nmhSetFsVpnCaCertEncodeType (&VpnCaCertKeyString,
                                     u1EncodeType) == SNMP_FAILURE)
    {
        /* Destroying the incomplete entry. Incase of failure */
        nmhSetFsVpnCaCertStatus (&VpnCaCertKeyString, DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsVpnCaCertFileName (&VpnCaCertKeyString,
                                   &VpnCaFileName) == SNMP_FAILURE)
    {
        /* Destroying the incomplete entry. Incase of failure */
        nmhSetFsVpnCaCertStatus (&VpnCaCertKeyString, DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsVpnCaCertStatus (&u4SnmpErrorStatus,
                                    &VpnCaCertKeyString,
                                    ACTIVE) == SNMP_FAILURE)
    {
        i4RetVal = CLI_GET_ERR (&u4SnmpErrorStatus);
        UNUSED_PARAM (i4RetVal);
        /* Destroying the incomplete entry. Incase of failure */
        nmhSetFsVpnCaCertStatus (&VpnCaCertKeyString, DESTROY);
        if (u4SnmpErrorStatus == VPN_ZERO)
        {
            CliPrintf (CliHandle,
                       "%% Failed to create CA certificate Entry\r\n");
            CLI_SET_ERR (VPN_ZERO);
        }
        return CLI_FAILURE;
    }

    if (nmhSetFsVpnCaCertStatus (&VpnCaCertKeyString, ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        /* Destroying the incomplete entry. Incase of failure */
        nmhSetFsVpnCaCertStatus (&VpnCaCertKeyString, DESTROY);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VpnShowRunningConfig                               */
/*  Description     : This function display the current VPN configuration*/
/*  Input (s)       : CliHandle,                                          */
/*  Output(s)       : None.                                               */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS.                            */
/**************************************************************************/

INT4
VpnShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4Status = VPN_ZERO;
    INT4                i4RaMode = RAVPN_SERVER;
    INT4                i4IkeTraceOption = VPN_ZERO;
    INT4                i4IpsecTraceOption = VPN_ZERO;

    CliRegisterLock (CliHandle, VpnDsLock, VpnDsUnLock);
    VpnDsLock ();
    nmhGetFsVpnGlobalStatus (&i4Status);
    if (i4Status == VPN_ENABLE)
    {
        CliPrintf (CliHandle, "set vpn enable\r\n");
    }

    if (nmhGetFsVpnRaServer (&i4RaMode) != SNMP_FAILURE)
    {
        if (i4RaMode == (INT4) RAVPN_CLIENT)
        {
            CliPrintf (CliHandle, "vpn remote-access client\r\n");
        }
    }
    if (nmhGetFsIkeTraceOption (&i4IkeTraceOption) != SNMP_FAILURE)
    {
        switch (i4IkeTraceOption)
        {
            case (INT4) CLI_ALL_TRC:
                CliPrintf (CliHandle, "debug crypto ike all\r\n");
                break;
            case CLI_INIT_SHUT_TRC:
                CliPrintf (CliHandle, "debug crypto ike shut\r\n");
                break;
            case CLI_MGMT_TRC:
                CliPrintf (CliHandle, "debug crypto ike mgmt\r\n");
                break;
            case CLI_CONTROL_PLANE_TRC:
                CliPrintf (CliHandle, "debug crypto ike ctrl\r\n");
                break;
            case CLI_DUMP_TRC:
                CliPrintf (CliHandle, "debug crypto ike dump\r\n");
                break;
            case CLI_OS_RESOURCE_TRC:
                CliPrintf (CliHandle, "debug crypto ike resource\r\n");
                break;
            case CLI_BUFFER_TRC:
                CliPrintf (CliHandle, "debug crypto ike buffer\r\n");
                break;
            case CLI_DATA_PATH_TRC:
                CliPrintf (CliHandle, "debug crypto ike data\r\n");
                break;
            default:
                CliPrintf (CliHandle, "\r\n");
                break;
        }
    }
    if (nmhGetFsIpsecTraceOption (&i4IpsecTraceOption) != SNMP_FAILURE)
    {
        switch (i4IpsecTraceOption)
        {
            case (INT4) CLI_ALL_TRC:
                CliPrintf (CliHandle, "debug crypto ipsec all\r\n");
                break;
            case CLI_INIT_SHUT_TRC:
                CliPrintf (CliHandle, "debug crypto ipsec shut\r\n");
                break;
            case CLI_MGMT_TRC:
                CliPrintf (CliHandle, "debug crypto ipsec mgmt\r\n");
                break;
            case CLI_CONTROL_PLANE_TRC:
                CliPrintf (CliHandle, "debug crypto ipsec ctrl\r\n");
                break;
            case CLI_DUMP_TRC:
                CliPrintf (CliHandle, "debug crypto ipsec dump\r\n");
                break;
            case CLI_OS_RESOURCE_TRC:
                CliPrintf (CliHandle, "debug crypto ipsec resource\r\n");
                break;
            case CLI_BUFFER_TRC:
                CliPrintf (CliHandle, "debug crypto ipsec buffer\r\n");
                break;
            case CLI_DATA_PATH_TRC:
                CliPrintf (CliHandle, "debug crypto ipsec data\r\n");
                break;
            default:
                CliPrintf (CliHandle, "\r\n");
                break;
        }
    }
    ShowRunningKeyInfo (CliHandle);
    ShowRunningCaCertInfo (CliHandle);
    ShowRunningCertInfo (CliHandle);
    VpnShowRunningRemoteIds (CliHandle);
    VpnShowRunningRaUserTable (CliHandle);
    VpnShowRunningRaVpnPoolTable (CliHandle);
    VpnShowRunningPolicyTables (CliHandle);
    VpnDsUnLock ();
    CliUnRegisterLock (CliHandle);
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VpnShowRunningRemoteIds                             */
/*  Description     : This function display the current VPN remote       */
/*              identity info.                      */
/*  Input (s)       : CliHandle,                                          */
/*  Output(s)       : None.                                               */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS.                            */
/**************************************************************************/
INT4
VpnShowRunningRemoteIds (tCliHandle CliHandle)
{
    UINT4               u4AuthType = VPN_ZERO;
    UINT1               au1Temp[MAX_LEN_ARRAY] = { VPN_ZERO };
    INT2                i2IdType = VPN_ZERO;
    tVpnIdInfo         *pVpnIdInfoNode = NULL;
    TMO_SLL_Scan (&gVpnRemoteIdList, pVpnIdInfoNode, tVpnIdInfo *)
    {
        MEMSET (au1Temp, VPN_ZERO, sizeof (au1Temp));
        i2IdType = VPN_ID_TYPE (pVpnIdInfoNode);
        switch (i2IdType)
        {
            case VPN_ID_TYPE_IPV4:
                STRCAT (au1Temp, "IPv4 ");
                break;
            case VPN_ID_TYPE_IPV6:
                STRCAT (au1Temp, "IPv6 ");
                break;
            case VPN_ID_TYPE_FQDN:
                STRCAT (au1Temp, "Fqdn ");
                break;
            case VPN_ID_TYPE_DN:
                STRCAT (au1Temp, "dn ");
                break;
            case VPN_ID_TYPE_EMAIL:
                STRCAT (au1Temp, "E-Mail ");
                break;
            case VPN_ID_TYPE_KEYID:
                STRCAT (au1Temp, "Key-Id ");
                break;
            default:
                return CLI_FAILURE;
        }

        STRCAT (au1Temp, VPN_ID_VALUE (pVpnIdInfoNode));
        STRCAT (au1Temp, " ");
        u4AuthType = (UINT4) pVpnIdInfoNode->u4AuthType;
        if (u4AuthType == VPN_AUTH_CERT)
        {
            STRCAT (au1Temp, "cert ");
        }
        else
        {
            STRCAT (au1Temp, "psk ");
        }
        STRCAT (au1Temp, VPN_ID_KEY (pVpnIdInfoNode));
        CliPrintf (CliHandle, "vpn remote identity %s\r\n", au1Temp);

    }
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VpnShowRunningRaUserTable                           */
/*  Description     : This function display the current VPN User Info       */
/*  Input (s)       : CliHandle,                                          */
/*  Output(s)       : None.                                               */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS.                            */
/**************************************************************************/
INT4
VpnShowRunningRaUserTable (tCliHandle CliHandle)
{

    tSNMP_OCTET_STRING_TYPE *pVpnRaUserName = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextVpnRaUserName = NULL;
    tSNMP_OCTET_STRING_TYPE *pVpnRaUserSecret = NULL;

    if (((pVpnRaUserName =
          allocmem_octetstring (RA_USER_NAME_LENGTH + VPN_ONE)) == NULL))
    {
        return CLI_FAILURE;
    }
    if (((pNextVpnRaUserName =
          allocmem_octetstring (RA_USER_NAME_LENGTH + VPN_ONE)) == NULL))
    {
        free_octetstring (pVpnRaUserName);
        return CLI_FAILURE;
    }
    if ((pVpnRaUserSecret =
         allocmem_octetstring (RA_USER_SECRET_LENGTH + VPN_ONE)) == NULL)
    {
        free_octetstring (pVpnRaUserName);
        free_octetstring (pNextVpnRaUserName);
        return CLI_FAILURE;
    }
    MEMSET (pVpnRaUserName->pu1_OctetList, VPN_ZERO,
            RA_USER_NAME_LENGTH + VPN_ONE);
    MEMSET (pNextVpnRaUserName->pu1_OctetList, VPN_ZERO,
            RA_USER_NAME_LENGTH + VPN_ONE);
    MEMSET (pVpnRaUserSecret->pu1_OctetList, VPN_ZERO,
            RA_USER_SECRET_LENGTH + VPN_ONE);
    if (nmhGetFirstIndexFsVpnRaUsersTable (pNextVpnRaUserName) == SNMP_FAILURE)
    {
        free_octetstring (pVpnRaUserName);
        free_octetstring (pNextVpnRaUserName);
        free_octetstring (pVpnRaUserSecret);
        return CLI_FAILURE;
    }
    do
    {
        MEMSET (pVpnRaUserName->pu1_OctetList, VPN_ZERO,
                RA_USER_NAME_LENGTH + VPN_ONE);

        STRNCPY (pVpnRaUserName->pu1_OctetList,
                 pNextVpnRaUserName->pu1_OctetList,
                 (size_t) pNextVpnRaUserName->i4_Length);

        pVpnRaUserName->i4_Length = pNextVpnRaUserName->i4_Length;
        nmhGetFsVpnRaUserSecret (pVpnRaUserName, pVpnRaUserSecret);

        MEMSET (pNextVpnRaUserName->pu1_OctetList, VPN_ZERO,
                RA_USER_NAME_LENGTH + VPN_ONE);

        CliPrintf (CliHandle, "ra-vpn username %s password %s\r\n",
                   pVpnRaUserName->pu1_OctetList,
                   pVpnRaUserSecret->pu1_OctetList);
    }
    while (nmhGetNextIndexFsVpnRaUsersTable
           (pVpnRaUserName, pNextVpnRaUserName) != SNMP_FAILURE);

    free_octetstring (pVpnRaUserName);
    free_octetstring (pNextVpnRaUserName);
    free_octetstring (pVpnRaUserSecret);
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VpnShowRunningRaVpnPoolTable                        */
/*  Description     : This function display the current VPN RA-Pool Info       */
/*  Input (s)       : CliHandle,                                          */
/*  Output(s)       : None.                                               */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS.                            */
/**************************************************************************/
INT4
VpnShowRunningRaVpnPoolTable (tCliHandle CliHandle)
{

    tSNMP_OCTET_STRING_TYPE *pVpnRaAddressPoolName = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextVpnRaAddressPoolName = NULL;
    UINT1               au1StartIpAddr[VPN_IP_ADDR_LEN];
    UINT1               au1EndIpAddr[VPN_IP_ADDR_LEN];
    UINT4               u4IpAddr = VPN_ZERO;
    tVpnRaAddressPool  *pVpnRaAddressPoolNode = NULL;

    if (((pVpnRaAddressPoolName =
          allocmem_octetstring (RA_ADDRESS_POOL_NAME_LENGTH + VPN_ONE)) ==
         NULL))
    {
        return CLI_FAILURE;
    }
    if (((pNextVpnRaAddressPoolName =
          allocmem_octetstring (RA_ADDRESS_POOL_NAME_LENGTH + VPN_ONE)) ==
         NULL))
    {
        free_octetstring (pVpnRaAddressPoolName);
        return CLI_FAILURE;
    }

    if (nmhGetFirstIndexFsVpnRaAddressPoolTable (pNextVpnRaAddressPoolName) ==
        SNMP_FAILURE)
    {
        free_octetstring (pVpnRaAddressPoolName);
        free_octetstring (pNextVpnRaAddressPoolName);
        return CLI_FAILURE;
    }
    do
    {
        pVpnRaAddressPoolNode =
            VpnGetRaAddrPool (pNextVpnRaAddressPoolName->pu1_OctetList,
                              pNextVpnRaAddressPoolName->i4_Length);
        if (pVpnRaAddressPoolNode == NULL)
        {
            free_octetstring (pVpnRaAddressPoolName);
            free_octetstring (pNextVpnRaAddressPoolName);
            return CLI_FAILURE;
        }
        if (pVpnRaAddressPoolNode->VpnRaAddressPoolStart.u4AddrType
            == IPVX_ADDR_FMLY_IPV4)
        {
            u4IpAddr = pVpnRaAddressPoolNode->VpnRaAddressPoolStart.
                uIpAddr.Ip4Addr;
            VPN_INET_NTOA (au1StartIpAddr, u4IpAddr)
                u4IpAddr = pVpnRaAddressPoolNode->VpnRaAddressPoolEnd.
                uIpAddr.Ip4Addr;
            VPN_INET_NTOA (au1EndIpAddr, u4IpAddr)
                CliPrintf (CliHandle, "ip ra-vpn pool %s %s - %s\r\n",
                           pNextVpnRaAddressPoolName->pu1_OctetList,
                           au1StartIpAddr, au1EndIpAddr);

        }
        else if (pVpnRaAddressPoolNode->VpnRaAddressPoolStart.u4AddrType
                 == IPVX_ADDR_FMLY_IPV6)
        {
            CliPrintf (CliHandle, "ipv6 ra-vpn pool %s %s %s %d\r\n",
                       pNextVpnRaAddressPoolName->pu1_OctetList,
                       Ip6PrintAddr (&(pVpnRaAddressPoolNode->
                                       VpnRaAddressPoolStart.uIpAddr.Ip6Addr)),
                       Ip6PrintAddr (&(pVpnRaAddressPoolNode->
                                       VpnRaAddressPoolEnd.uIpAddr.Ip6Addr)),
                       pVpnRaAddressPoolNode->u4VpnRaAddressPoolPrefixLen);
        }

        STRCPY (pVpnRaAddressPoolName->pu1_OctetList,
                pNextVpnRaAddressPoolName->pu1_OctetList);
        pVpnRaAddressPoolName->i4_Length = pNextVpnRaAddressPoolName->i4_Length;
        MEMSET (pNextVpnRaAddressPoolName->pu1_OctetList, VPN_ZERO,
                RA_ADDRESS_POOL_NAME_LENGTH + VPN_ONE);
    }
    while (nmhGetNextIndexFsVpnRaAddressPoolTable
           (pVpnRaAddressPoolName, pNextVpnRaAddressPoolName) != SNMP_FAILURE);

    free_octetstring (pVpnRaAddressPoolName);
    free_octetstring (pNextVpnRaAddressPoolName);
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VpnShowRunningPhase1PolicyEncryption                */
/*  Description     : This function display the Phase1 configuration.       */
/*  Input (s)       : CliHandle,                      */
/*                    pPolicy,                          */
/*                    pu1PolicyName                           */
/*  Output(s)       : None.                                               */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS.                            */
/**************************************************************************/
INT4
VpnShowRunningPhase1PolicyEncryption (tCliHandle CliHandle,
                                      tVpnPolicy * pPolicy,
                                      tSNMP_OCTET_STRING_TYPE * pu1PolicyName)
{
    INT4                i4Value = VPN_ZERO;
    INT4                i4Flag = VPN_ZERO;
    UINT1               au1Temp[MAX_LEN_ARRAY] = { VPN_ZERO };
    MEMSET (au1Temp, VPN_ZERO, sizeof (au1Temp));

    nmhGetFsVpnIkePhase1EncryptionAlgo (pu1PolicyName, &i4Value);
    if (i4Value != CLI_VPN_DES)
    {
        i4Flag = VPN_ONE;
    }
    switch (i4Value)
    {
        case CLI_VPN_DES:
            CLI_STRCAT (au1Temp, "DES ");
            break;
        case CLI_VPN_3DES:
            CLI_STRCAT (au1Temp, "triple-des ");
            break;
        case CLI_VPN_AES_128:
            CLI_STRCAT (au1Temp, "AES ");
            break;
        case CLI_VPN_AES_192:
            CLI_STRCAT (au1Temp, "AES-192 ");
            break;
        case CLI_VPN_AES_256:
            CLI_STRCAT (au1Temp, "AES-256 ");
            break;
        default:
            return CLI_FAILURE;
    }

    nmhGetFsVpnIkePhase1HashAlgo (pu1PolicyName, &i4Value);
    if (i4Value != CLI_HMAC_SHA1)
    {
        i4Flag = VPN_ONE;
    }
    switch (i4Value)
    {
        case CLI_HMAC_MD5:
            CLI_STRCAT (au1Temp, "hash MD5 ");
            break;
        case CLI_HMAC_SHA1:
            CLI_STRCAT (au1Temp, "hash SHA1 ");
            break;
        case HMAC_SHA_256:
            CLI_STRCAT (au1Temp, "hash SHA256 ");
            break;
        case HMAC_SHA_384:
            CLI_STRCAT (au1Temp, "hash SHA384 ");
            break;
        case HMAC_SHA_512:
            CLI_STRCAT (au1Temp, "hash SHA512 ");
            break;
        default:
            return CLI_FAILURE;
    }
    nmhGetFsVpnIkePhase1DHGroup (pu1PolicyName, &i4Value);
    if (i4Value != CLI_VPN_IKE_DH_GROUP_2)
    {
        i4Flag = VPN_ONE;
    }
    switch (i4Value)
    {
        case CLI_VPN_IKE_DH_GROUP_1:
            CLI_STRCAT (au1Temp, "dh Group1 ");
            break;
        case CLI_VPN_IKE_DH_GROUP_2:
            CLI_STRCAT (au1Temp, "dh Group2 ");
            break;
        case CLI_VPN_IKE_DH_GROUP_5:
            CLI_STRCAT (au1Temp, "dh Group5 ");
            break;
        case SEC_DH_GROUP_14:
            CLI_STRCAT (au1Temp, "dh Group14 ");
            break;
        default:
            return CLI_FAILURE;
    }

    if (pPolicy->u1VpnIkeVer == CLI_VPN_IKE_V1)
    {
        nmhGetFsVpnIkePhase1Mode (pu1PolicyName, &i4Value);
        if (i4Value == CLI_VPN_IKE_MAIN_MODE)
            CLI_STRCAT (au1Temp, "exch Main ");
        else if (i4Value == CLI_VPN_IKE_AGGRESSIVE_MODE)
        {
            CLI_STRCAT (au1Temp, "exch Aggressive ");
            i4Flag = VPN_ONE;
        }
        else
            return CLI_FAILURE;
    }
    nmhGetFsVpnIkePhase1LifeTimeType (pu1PolicyName, &i4Value);
    switch (i4Value)
    {
        case FSIKE_LIFETIME_SECS:
            CLI_STRCAT (au1Temp, "lifetime Secs ");
            break;
        case FSIKE_LIFETIME_KB:
            CLI_STRCAT (au1Temp, "lifetime KBs ");
            break;
        case FSIKE_LIFETIME_MINS:
            CLI_STRCAT (au1Temp, "lifetime Mins ");
            break;
        case FSIKE_LIFETIME_HRS:
            CLI_STRCAT (au1Temp, "lifetime Hrs ");
            break;
        case FSIKE_LIFETIME_DAYS:
            CLI_STRCAT (au1Temp, "lifetime Days ");
            break;
        default:
            return CLI_FAILURE;
    }
    if (i4Value == FSIKE_LIFETIME_SECS)
    {
        nmhGetFsVpnIkePhase1LifeTime (pu1PolicyName, &i4Value);
        if (i4Value != VPN_DEFAULT_IKE_LTIME_VALUE)
        {
            i4Flag = VPN_ONE;
        }
    }
    else
    {
        nmhGetFsVpnIkePhase1LifeTime (pu1PolicyName, &i4Value);
    }
    if (i4Flag == VPN_ONE)
    {
        if (pPolicy->u1VpnIkeVer == CLI_VPN_IKE_V1)
        {
            CliPrintf (CliHandle, "isakmp policy encryption %s %d\r\n", au1Temp,
                       i4Value);
        }
        else
        {
            CliPrintf (CliHandle, "isakmp policy encryption %s %d\r\n", au1Temp,
                       i4Value);
        }
    }
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VpnShowRunningPhase2Policy                      */
/*  Description     : This function display the Phase2 configuration.       */
/*  Input (s)       : CliHandle,                                          */
/*                    pu1PolicyName,                      */
/*  Output(s)       : None.                                               */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS.                            */
/**************************************************************************/
INT4
VpnShowRunningPhase2Policy (tCliHandle CliHandle,
                            tSNMP_OCTET_STRING_TYPE * pu1PolicyName)
{
    INT4                i4Value = VPN_ZERO;
    UINT1               au1Temp[MAX_LEN_ARRAY] = { VPN_ZERO };
    MEMSET (au1Temp, VPN_ZERO, sizeof (au1Temp));
    nmhGetFsVpnIkePhase2EspEncryptionAlgo (pu1PolicyName, &i4Value);
    switch (i4Value)
    {
        case CLI_VPN_DES:
            CLI_STRCAT (au1Temp, "encryption esp DES ");
            break;
        case CLI_VPN_3DES:
            CLI_STRCAT (au1Temp, "encryption esp triple-des ");
            break;
        case CLI_VPN_AES_128:
            CLI_STRCAT (au1Temp, "encryption esp AES ");
            break;
        case CLI_VPN_AES_192:
            CLI_STRCAT (au1Temp, "encryption esp AES-192 ");
            break;
        case CLI_VPN_AES_256:
            CLI_STRCAT (au1Temp, "encryption esp AES-256 ");
            break;
        case CLI_VPN_ESP_NULL:
            CLI_STRCAT (au1Temp, "encryption esp NULL ");
            break;
        case SEC_AESCTR:
            CLI_STRCAT (au1Temp, "encryption esp AES-CTR ");
            break;
        case SEC_AESCTR192:
            CLI_STRCAT (au1Temp, "encryption esp AES-CTR-192 ");
            break;
        case SEC_AESCTR256:
            CLI_STRCAT (au1Temp, "encryption esp AES-CTR-256 ");
            break;
        default:
            return CLI_FAILURE;
    }

    nmhGetFsVpnIkePhase2AuthAlgo (pu1PolicyName, &i4Value);
    switch (i4Value)
    {
        case CLI_HMAC_MD5:
            CLI_STRCAT (au1Temp, "authentication esp md5 ");
            break;
        case CLI_HMAC_SHA1:
            CLI_STRCAT (au1Temp, "authentication esp sha1 ");
            break;
        case SEC_XCBCMAC:
            CLI_STRCAT (au1Temp, "authentication esp xcbc-mac ");
            break;
        case HMAC_SHA_256:
            CLI_STRCAT (au1Temp, "authentication esp hmac-sha-256 ");
            break;
        case HMAC_SHA_384:
            CLI_STRCAT (au1Temp, "authentication esp hmac-sha-384 ");
            break;
        case HMAC_SHA_512:
            CLI_STRCAT (au1Temp, "authentication esp hmac-sha-512 ");
            break;
        default:
            return CLI_FAILURE;
    }

    nmhGetFsVpnIkePhase2DHGroup (pu1PolicyName, &i4Value);
    switch (i4Value)
    {
        case CLI_VPN_IKE_DH_GROUP_1:
            CLI_STRCAT (au1Temp, "pfs Group1 ");
            break;
        case CLI_VPN_IKE_DH_GROUP_2:
            CLI_STRCAT (au1Temp, "pfs Group2 ");
            break;
        case CLI_VPN_IKE_DH_GROUP_5:
            CLI_STRCAT (au1Temp, "pfs Group5 ");
            break;
        case SEC_DH_GROUP_14:
            CLI_STRCAT (au1Temp, "pfs Group14 ");
            break;
        default:
            return CLI_FAILURE;
    }

    nmhGetFsVpnIkePhase2LifeTimeType (pu1PolicyName, &i4Value);
    switch (i4Value)
    {
        case FSIKE_LIFETIME_SECS:
            CLI_STRCAT (au1Temp, "lifetime Secs ");
            break;
        case FSIKE_LIFETIME_KB:
            CLI_STRCAT (au1Temp, "lifetime KB ");
            break;
        case FSIKE_LIFETIME_MINS:
            CLI_STRCAT (au1Temp, "lifetime Mins ");
            break;
        case FSIKE_LIFETIME_HRS:
            CLI_STRCAT (au1Temp, "lifetime Hrs ");
            break;
        case FSIKE_LIFETIME_DAYS:
            CLI_STRCAT (au1Temp, "lifetime Days ");
            break;
        default:
            return CLI_FAILURE;
    }
    nmhGetFsVpnIkePhase2LifeTime (pu1PolicyName, &i4Value);

    CliPrintf (CliHandle, "crypto map ipsec %s %d\r\n", au1Temp, i4Value);

    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VpnShowRunningAccessList                      */
/*  Description     : This function display the Access List configuration.*/
/*  Input (s)       : CliHandle,                                          */
/*            pPolicy,                      */
/*            pu1PolicyName                          */
/*  Output(s)       : None.                                               */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS.                            */
/**************************************************************************/
INT4
VpnShowRunningAccessList (tCliHandle CliHandle, tVpnPolicy * pPolicy,
                          tSNMP_OCTET_STRING_TYPE * pu1PolicyName)
{
    INT4                i4Value = VPN_ZERO;
    INT4                i4RetVal = VPN_ZERO;
    INT4                i4AddrType = VPN_ZERO;
    UINT4               u4LocMask = VPN_ZERO;
    UINT4               u4IpAddr = VPN_ZERO;
    UINT4               u4RemMask = VPN_ZERO;
    UINT4               u4RetPreFixLen = VPN_ZERO;
    UINT4               u4LocPreFixLen = VPN_ZERO;
    UINT4               u4RemPreFixLen = VPN_ZERO;
    CHR1               *pu1String = NULL;
    CHR1               *pu1MaskString = NULL;
    INT1                au1Temp[MAX_LEN_ARRAY];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT1               au1TempStr[VPN_MAX_NAME_LENGTH + VPN_ONE];
    tIp6Addr            Ip6Addr;
    MEMSET (au1Temp, VPN_ZERO, sizeof (au1Temp));
    MEMSET (au1TempStr, VPN_ZERO, sizeof (au1TempStr));
    nmhGetFsVpnPolicyFlag (pu1PolicyName, &i4Value);
    switch (i4Value)
    {
        case SEC_APPLY:
            CLI_STRCAT (au1Temp, "Apply ");
            break;
        case SEC_BYPASS:
            CLI_STRCAT (au1Temp, "Bypass ");
            break;
        case SEC_FILTER:
            CLI_STRCAT (au1Temp, "Filter ");
            break;
        default:
            return CLI_FAILURE;
    }
    nmhGetFsVpnProtocol (pu1PolicyName, &i4Value);
    switch (i4Value)
    {
        case VPN_TCP:
            CLI_STRCAT (au1Temp, "TCP ");
            break;
        case VPN_UDP:
            CLI_STRCAT (au1Temp, "UDP ");
            break;
        case VPN_AH:
            CLI_STRCAT (au1Temp, "AH ");
            break;
        case VPN_ESP:
            CLI_STRCAT (au1Temp, "ESP ");
            break;
        case VPN_ICMP:
            CLI_STRCAT (au1Temp, "ICMP ");
            break;
        case VPN_ANY_PROTOCOL:
            CLI_STRCAT (au1Temp, "any ");
            break;
        default:
            return CLI_FAILURE;
    }

    MEMSET (au1TempStr, VPN_ZERO, sizeof (au1TempStr));
    OctetStr.pu1_OctetList = au1TempStr;
    i4RetVal = nmhGetFsVpnLocalProtectNetwork (pu1PolicyName, &OctetStr);
    if (i4RetVal != SNMP_FAILURE)
    {
        nmhGetFsVpnProtectNetworkType (pu1PolicyName, &i4AddrType);
        if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (&u4IpAddr, OctetStr.pu1_OctetList, sizeof (UINT4));
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr);
            CLI_STRCAT (au1Temp, "source ");
            CLI_STRCAT (au1Temp, pu1String);
            CLI_STRCAT (au1Temp, " ");

            nmhGetFsVpnLocalProtectNetworkPrefixLen (pu1PolicyName,
                                                     &u4RetPreFixLen);
            IPV4_MASKLEN_TO_MASK (u4LocMask, u4RetPreFixLen);
            CLI_CONVERT_IPADDR_TO_STR (pu1MaskString, u4LocMask);
            CLI_STRCAT (au1Temp, pu1MaskString);
            CLI_STRCAT (au1Temp, " ");

        }
        else if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            MEMCPY (&Ip6Addr, OctetStr.pu1_OctetList, sizeof (tIp6Addr));
            pu1String = (CHR1 *) Ip6PrintAddr (&Ip6Addr);
            CLI_STRCAT (au1Temp, "source ");
            CLI_STRCAT (au1Temp, pu1String);
            CLI_STRCAT (au1Temp, " ");
            nmhGetFsVpnLocalProtectNetworkPrefixLen (pu1PolicyName,
                                                     &u4LocPreFixLen);
        }
    }
    MEMSET (au1TempStr, VPN_ZERO, sizeof (au1TempStr));
    OctetStr.pu1_OctetList = au1TempStr;
    i4RetVal = nmhGetFsVpnRemoteProtectNetwork (pu1PolicyName, &OctetStr);
    if (i4RetVal != SNMP_FAILURE)
    {
        if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (&u4IpAddr, OctetStr.pu1_OctetList, sizeof (UINT4));
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr);
            CLI_STRCAT (au1Temp, "destination ");
            CLI_STRCAT (au1Temp, pu1String);
            CLI_STRCAT (au1Temp, " ");
            nmhGetFsVpnRemoteProtectNetworkPrefixLen (pu1PolicyName,
                                                      &u4RetPreFixLen);
            IPV4_MASKLEN_TO_MASK (u4RemMask, u4RetPreFixLen);
            CLI_CONVERT_IPADDR_TO_STR (pu1MaskString, u4RemMask);
            CLI_STRCAT (au1Temp, pu1MaskString);
            CLI_STRCAT (au1Temp, " ");
        }
        else if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
        {
            MEMCPY (&Ip6Addr, OctetStr.pu1_OctetList, sizeof (tIp6Addr));
            pu1String = (CHR1 *) Ip6PrintAddr (&Ip6Addr);
            nmhGetFsVpnRemoteProtectNetworkPrefixLen (pu1PolicyName,
                                                      &u4RemPreFixLen);
        }
    }

    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        CliPrintf (CliHandle, "access-list %s srcport %d-%d destport %d-%d\r\n",
                   au1Temp, pPolicy->LocalProtectNetwork.u2StartPort,
                   pPolicy->LocalProtectNetwork.u2EndPort,
                   pPolicy->RemoteProtectNetwork.u2StartPort,
                   pPolicy->RemoteProtectNetwork.u2EndPort);
    }
    else if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        CliPrintf (CliHandle,
                   "access-list ipv6 %s %d destination %s %d srcport %d-%d destport %d-%d\r\n",
                   au1Temp, u4LocPreFixLen, pu1String, u4RemPreFixLen,
                   pPolicy->LocalProtectNetwork.u2StartPort,
                   pPolicy->LocalProtectNetwork.u2EndPort,
                   pPolicy->RemoteProtectNetwork.u2StartPort,
                   pPolicy->RemoteProtectNetwork.u2EndPort);
    }
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VpnShowRunningInterfacePolicyTable                  */
/*  Description     : This function display the Interface Policy map conf */
/*  Input (s)       : CliHandle,                                          */
/*            pu1PolicyName                          */
/*  Output(s)       : None.                                               */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS.                            */
/**************************************************************************/
INT4
VpnShowRunningInterfacePolicyTable (tCliHandle CliHandle,
                                    tSNMP_OCTET_STRING_TYPE * pu1PolicyName)
{
    UINT4               u4RetVal = VPN_ZERO;
    INT1                ai1IfName[CFA_MAX_PORT_NAME_LENGTH] = { VPN_ZERO };
    INT1               *piIfName;
    nmhGetFsVpnPolicyIntfIndex (pu1PolicyName, (INT4 *) &u4RetVal);
    if (u4RetVal != VPN_ZERO)
    {
        piIfName = (INT1 *) &ai1IfName[0];
        CfaCliConfGetIfName (u4RetVal, piIfName);
        CliPrintf (CliHandle, "!\r\n");
        CliPrintf (CliHandle, "interface %s\r\n", piIfName);
        CliPrintf (CliHandle, "crypto map %s\r\n",
                   pu1PolicyName->pu1_OctetList);
        CliPrintf (CliHandle, "!\r\n");
    }
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VpnShowRunningSessionKey                      */
/*  Description     : This function display the Session Key configuration.*/
/*  Input (s)       : CliHandle,                                          */
/*            pu1PolicyName                          */
/*  Output(s)       : None.                                               */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS.                            */
/**************************************************************************/
INT4
VpnShowRunningSessionKey (tCliHandle CliHandle,
                          tSNMP_OCTET_STRING_TYPE * pu1PolicyName)
{
    INT4                i4Value;
    INT4                i4PolicyType;
    INT4                i4InSpi;
    INT4                i4OutSpi;
    INT1                au1Temp[MAX_LEN_ARRAY];
    tSNMP_OCTET_STRING_TYPE *pu1FsVpnAhKey = NULL;
    tSNMP_OCTET_STRING_TYPE *pu1FsVpnEspKey = NULL;

    if ((pu1FsVpnAhKey =
         allocmem_octetstring (IPSEC_AH_KEY_LEN + VPN_ONE)) == NULL)
    {
        return CLI_FAILURE;
    }
    if ((pu1FsVpnEspKey =
         allocmem_octetstring (IPSEC_ESP_KEY_LEN + VPN_ONE)) == NULL)
    {
        free_octetstring (pu1FsVpnAhKey);
        return CLI_FAILURE;
    }

    MEMSET (pu1FsVpnAhKey->pu1_OctetList, VPN_ZERO, IPSEC_AH_KEY_LEN + VPN_ONE);
    MEMSET (pu1FsVpnEspKey->pu1_OctetList, VPN_ZERO,
            IPSEC_ESP_KEY_LEN + VPN_ONE);
    MEMSET (au1Temp, VPN_ZERO, sizeof (au1Temp));

    nmhGetFsVpnSecurityProtocol (pu1PolicyName, &i4Value);
    if (i4Value == SEC_AH)
        CLI_STRCAT (au1Temp, "AH ");
    else if (i4Value == SEC_ESP)
        CLI_STRCAT (au1Temp, "ESP ");

    nmhGetFsVpnAuthAlgo (pu1PolicyName, &i4Value);
    switch (i4Value)
    {
        case CLI_HMAC_MD5:
            CLI_STRCAT (au1Temp, "HMAC-MD5 ");
            break;
        case CLI_HMAC_SHA1:
            CLI_STRCAT (au1Temp, "HMAC-SHA1 ");
            break;
        case SEC_XCBCMAC:
            CLI_STRCAT (au1Temp, "AES-XCBC-MAC-96 ");
            break;
        case HMAC_SHA_256:
            CLI_STRCAT (au1Temp, "HMAC_SHA_256 ");
            break;
        case HMAC_SHA_384:
            CLI_STRCAT (au1Temp, "HMAC_SHA_384 ");
            break;
        case HMAC_SHA_512:
            CLI_STRCAT (au1Temp, "HMAC_SHA_512 ");
            break;
        default:
            break;
    }

    nmhGetFsVpnAhKey (pu1PolicyName, pu1FsVpnAhKey);
    if (pu1FsVpnAhKey->pu1_OctetList[VPN_INDEX_0] != '\0')
    {
        STRNCAT (au1Temp, pu1FsVpnAhKey->pu1_OctetList,
                 pu1FsVpnAhKey->i4_Length);
        CLI_STRCAT (au1Temp, " ");
    }
    nmhGetFsVpnEncrAlgo (pu1PolicyName, &i4Value);
    switch (i4Value)
    {
        case CLI_VPN_DES:
            CLI_STRCAT (au1Temp, " esp DES ");
            break;
        case CLI_VPN_3DES:
            CLI_STRCAT (au1Temp, "esp triple-des ");
            break;
        case CLI_VPN_AES_128:
            CLI_STRCAT (au1Temp, "esp AES ");
            break;
        case CLI_VPN_AES_192:
            CLI_STRCAT (au1Temp, "esp AES-192 ");
            break;
        case CLI_VPN_AES_256:
            CLI_STRCAT (au1Temp, "esp AES-256 ");
            break;
        case CLI_VPN_ESP_NULL:
            break;
        default:
            break;
    }

    nmhGetFsVpnEspKey (pu1PolicyName, pu1FsVpnEspKey);
    if (pu1FsVpnEspKey->pu1_OctetList[VPN_INDEX_0] != '\0')
    {
        CLI_STRCAT (au1Temp, "cipher ");
        STRNCAT (au1Temp, pu1FsVpnEspKey->pu1_OctetList,
                 pu1FsVpnEspKey->i4_Length);
        CLI_STRCAT (au1Temp, " ");
    }

    nmhGetFsVpnInboundSpi (pu1PolicyName, &i4InSpi);
    nmhGetFsVpnOutboundSpi (pu1PolicyName, &i4OutSpi);

    nmhGetFsVpnAntiReplay (pu1PolicyName, &i4Value);
    nmhGetFsVpnPolicyType (pu1PolicyName, &i4PolicyType);
    if (i4PolicyType == VPN_IPSEC_MANUAL)
    {
        if (i4Value == CLI_AR_ENABLE)
        {
            CliPrintf (CliHandle,
                       "set session-key authenticator %s outbound %d inbound %d anti-replay\n\r",
                       au1Temp, i4OutSpi, i4InSpi);
        }
        else
        {
            CliPrintf (CliHandle,
                       "set session-key authenticator %s outbound %d inbound %d \n\r",
                       au1Temp, i4OutSpi, i4InSpi);
        }
    }
    free_octetstring (pu1FsVpnAhKey);
    free_octetstring (pu1FsVpnEspKey);
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : VpnShowRunningPolicyTables                  */
/*  Description     : This function display the VPN Policy Table info.    */
/*  Input (s)       : CliHandle,                                          */
/*  Output(s)       : None.                                               */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS.                            */
/**************************************************************************/
INT4
VpnShowRunningPolicyTables (tCliHandle CliHandle)
{
    tVpnPolicy         *pPolicy = NULL;
    INT4                i4VpnCertAlgoType;
    UINT4               u4IpAddr = VPN_ZERO;
    INT4                i4Value;
    CHR1               *pu1String = NULL;
    UINT1               au1TempStr[VPN_MAX_NAME_LENGTH + 1];

    tIp6Addr            Ip6Addr;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    tSNMP_OCTET_STRING_TYPE pu1PolicyName;
    INT4                i4VpnMode;
    INT4                i4AddrType;
    INT4                i4PolicyType;
    TMO_SLL_Scan (&VpnPolicyList, pPolicy, tVpnPolicy *)
    {

        pu1PolicyName.i4_Length = (INT4) STRLEN (pPolicy->au1VpnPolicyName);

        pu1PolicyName.pu1_OctetList = pPolicy->au1VpnPolicyName;

        CliPrintf (CliHandle, "crypto map %s\r\n", pu1PolicyName.pu1_OctetList);

        if (pPolicy->u1VpnIkeVer == CLI_VPN_IKE_V2)
            CliPrintf (CliHandle, "set ike version v2\r\n");

        if (nmhGetFsVpnMode (&pu1PolicyName, &i4VpnMode) != SNMP_FAILURE)
        {
            if (i4VpnMode == VPN_TRANSPORT)
            {
                CliPrintf (CliHandle, "crypto ipsec mode transport\r\n");
            }
        }

        if (nmhGetFsVpnCertAlgoType (&pu1PolicyName, &i4VpnCertAlgoType) !=
            SNMP_FAILURE)
        {
            if (i4VpnCertAlgoType == VPN_AUTH_DSA)
            {
                CliPrintf (CliHandle, "crypto key encrypt dsa\r\n");
            }
            else if (i4VpnCertAlgoType == VPN_AUTH_RSA)
            {
                CliPrintf (CliHandle, "crypto key encrypt rsa\r\n");

            }
        }
        if (nmhGetFsVpnPolicyType (&pu1PolicyName, &i4PolicyType) !=
            SNMP_FAILURE)
        {
            switch (i4PolicyType)
            {
                case VPN_IPSEC_MANUAL:
                    CliPrintf (CliHandle, "crypto key mode ipsec-manual\r\n");
                    break;

                case VPN_IKE_PRESHAREDKEY:
                    /* This is the default value.
                     * so it should'nt be printed in SRC */
                    break;

                case VPN_XAUTH:
                    CliPrintf (CliHandle, "crypto key mode xauth\r\n");
                    break;

                case VPN_IKE_RA_PRESHAREDKEY:
                    CliPrintf (CliHandle,
                               "crypto key mode ravpn-preshared-key\r\n");
                    break;

                case VPN_IKE_CERTIFICATE:
                    CliPrintf (CliHandle, "crypto key mode cert\r\n");
                    break;

                case VPN_IKE_RA_CERT:
                    CliPrintf (CliHandle, "crypto key mode ra-cert\r\n");
                    break;

                case VPN_IKE_XAUTH_CERT:
                    CliPrintf (CliHandle, "crypto key mode xruth-cert\n");
                    break;
                default:
                    CliPrintf (CliHandle, "\r\n");
                    break;
            }
        }

        MEMSET (au1TempStr, VPN_ZERO, sizeof (au1TempStr));
        OctetStr.pu1_OctetList = au1TempStr;
        if (nmhGetFsVpnTunTermAddrType (&pu1PolicyName, &i4AddrType) !=
            SNMP_FAILURE)
        {
            if (nmhGetFsVpnRemoteTunTermAddr (&pu1PolicyName, &OctetStr) !=
                SNMP_FAILURE)
            {
                if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
                {
                    MEMCPY (&u4IpAddr, OctetStr.pu1_OctetList, sizeof (UINT4));
                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4IpAddr);
                    CliPrintf (CliHandle, "set peer %s\r\n", pu1String);
                }
                else if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
                {
                    MEMCPY (&Ip6Addr, OctetStr.pu1_OctetList,
                            sizeof (tIp6Addr));
                    CliPrintf (CliHandle, "set ipv6 peer %s\r\n",
                               Ip6PrintAddr (&Ip6Addr));
                }
            }
        }
        MEMSET (au1TempStr, VPN_ZERO, sizeof (au1TempStr));
        OctetStr.pu1_OctetList = au1TempStr;
        if (nmhGetFsVpnIkePhase1LocalIdType (&pu1PolicyName, &i4Value) !=
            SNMP_FAILURE)
        {
            if (nmhGetFsVpnIkePhase1LocalIdValue (&pu1PolicyName, &OctetStr) !=
                SNMP_FAILURE)
            {

                switch (i4Value)
                {
                    case VPN_ID_TYPE_IPV4:
                        CliPrintf (CliHandle,
                                   "isakmp local identity ipv4 %s\r\n",
                                   OctetStr.pu1_OctetList);
                        break;

                    case VPN_ID_TYPE_IPV6:
                        CliPrintf (CliHandle,
                                   "isakmp local identity ipv6 %s\r\n",
                                   OctetStr.pu1_OctetList);
                        break;

                    case VPN_ID_TYPE_EMAIL:
                        CliPrintf (CliHandle,
                                   "isakmp local identity email %s\r\n",
                                   OctetStr.pu1_OctetList);
                        break;

                    case VPN_ID_TYPE_FQDN:
                        CliPrintf (CliHandle,
                                   "isakmp local identity fqdn %s\r\n",
                                   OctetStr.pu1_OctetList);
                        break;

                    case VPN_ID_TYPE_DN:
                        CliPrintf (CliHandle, "isakmp local identity dn %s\r\n",
                                   OctetStr.pu1_OctetList);
                        break;

                    case VPN_ID_TYPE_KEYID:
                        CliPrintf (CliHandle,
                                   "isakmp local identity keyid %s\r\n",
                                   OctetStr.pu1_OctetList);
                        break;
                    default:
                        CliPrintf (CliHandle, "\r\n");
                        break;
                }
            }
        }

        MEMSET (au1TempStr, VPN_ZERO, sizeof (au1TempStr));
        OctetStr.pu1_OctetList = au1TempStr;
        if (nmhGetFsVpnIkePhase1PeerIdType (&pu1PolicyName, &i4Value) !=
            SNMP_FAILURE)
        {
            if (nmhGetFsVpnIkePhase1PeerIdValue (&pu1PolicyName, &OctetStr) !=
                SNMP_FAILURE)
            {
                switch (i4Value)
                {
                    case VPN_ID_TYPE_IPV4:
                        CliPrintf (CliHandle,
                                   "isakmp peer identity ipv4 %s\r\n",
                                   OctetStr.pu1_OctetList);
                        break;

                    case VPN_ID_TYPE_IPV6:
                        CliPrintf (CliHandle,
                                   "isakmp peer identity ipv6 %s\r\n",
                                   OctetStr.pu1_OctetList);
                        break;

                    case VPN_ID_TYPE_EMAIL:
                        CliPrintf (CliHandle,
                                   "isakmp peer identity email %s\r\n",
                                   OctetStr.pu1_OctetList);
                        break;

                    case VPN_ID_TYPE_FQDN:
                        CliPrintf (CliHandle,
                                   "isakmp peer identity fqdn %s\r\n",
                                   OctetStr.pu1_OctetList);
                        break;

                    case VPN_ID_TYPE_DN:
                        CliPrintf (CliHandle, "isakmp peer identity dn %s\r\n",
                                   OctetStr.pu1_OctetList);
                        break;

                    case VPN_ID_TYPE_KEYID:
                        CliPrintf (CliHandle,
                                   "isakmp peer identity keyid %s\r\n",
                                   OctetStr.pu1_OctetList);
                        break;
                    default:
                        CliPrintf (CliHandle, "\r\n");
                        break;
                }
            }
        }
        VpnShowRunningPhase1PolicyEncryption (CliHandle, pPolicy,
                                              &pu1PolicyName);
        VpnShowRunningPhase2Policy (CliHandle, &pu1PolicyName);
        VpnShowRunningAccessList (CliHandle, pPolicy, &pu1PolicyName);
        VpnShowRunningSessionKey (CliHandle, &pu1PolicyName);
        VpnShowRunningInterfacePolicyTable (CliHandle, &pu1PolicyName);
    }
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : ShowRunningCaCertInfo                        */
/*  Description     : This function display the CACert Table info.        */
/*  Input (s)       : CliHandle,                                          */
/*  Output(s)       : None.                                               */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS.                            */
/**************************************************************************/

INT4
ShowRunningCaCertInfo (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE *pFirstVpnCaCertKeyString = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextVpnCaCertKeyString = NULL;
    INT4                i4CaCertEncodeType;
    tSNMP_OCTET_STRING_TYPE *pCaCertFileName = NULL;
    if ((pFirstVpnCaCertKeyString =
         allocmem_octetstring (VPN_MAX_KEY_NAME_LEN + VPN_ONE)) == NULL)
    {
        return CLI_FAILURE;
    }
    if ((pNextVpnCaCertKeyString =
         allocmem_octetstring (VPN_MAX_KEY_NAME_LEN + VPN_ONE)) == NULL)
    {
        free_octetstring (pFirstVpnCaCertKeyString);
        return CLI_FAILURE;
    }
    if ((pCaCertFileName =
         allocmem_octetstring (VPN_CERT_FILE_NAME_LEN + VPN_ONE)) == NULL)
    {
        free_octetstring (pFirstVpnCaCertKeyString);
        free_octetstring (pNextVpnCaCertKeyString);
        return CLI_FAILURE;
    }
    MEMSET (pNextVpnCaCertKeyString->pu1_OctetList, VPN_ZERO,
            VPN_MAX_KEY_NAME_LEN);
    if (nmhGetFirstIndexFsVpnCaCertInfoTable (pNextVpnCaCertKeyString) ==
        SNMP_FAILURE)
    {
        free_octetstring (pFirstVpnCaCertKeyString);
        free_octetstring (pNextVpnCaCertKeyString);
        free_octetstring (pCaCertFileName);
        return CLI_FAILURE;
    }
    do
    {
        MEMSET (pFirstVpnCaCertKeyString->pu1_OctetList, VPN_ZERO,
                VPN_MAX_KEY_NAME_LEN);
        MEMSET (pCaCertFileName->pu1_OctetList, VPN_ZERO,
                VPN_CERT_FILE_NAME_LEN + VPN_ONE);

        STRCPY (pFirstVpnCaCertKeyString->pu1_OctetList,
                pNextVpnCaCertKeyString->pu1_OctetList);
        pFirstVpnCaCertKeyString->i4_Length =
            pNextVpnCaCertKeyString->i4_Length;
        nmhGetFsVpnCaCertFileName (pFirstVpnCaCertKeyString, pCaCertFileName);
        if (pFirstVpnCaCertKeyString->pu1_OctetList[VPN_INDEX_0] != '\0' &&
            pCaCertFileName->pu1_OctetList[VPN_INDEX_0] != '\0')
        {
            CliPrintf (CliHandle, "vpn import ca-cert %s file %s ",
                       pFirstVpnCaCertKeyString->pu1_OctetList,
                       pCaCertFileName->pu1_OctetList);
        }
        nmhGetFsVpnCaCertEncodeType (pFirstVpnCaCertKeyString,
                                     &i4CaCertEncodeType);
        if (i4CaCertEncodeType == CLI_VPN_CERT_ENCODE_PEM)
            CliPrintf (CliHandle, "encode-type PEM\r\n");
        else if (i4CaCertEncodeType == CLI_VPN_CERT_ENCODE_DER)
            CliPrintf (CliHandle, "encode-type DER\r\n");

        MEMSET (pNextVpnCaCertKeyString->pu1_OctetList, VPN_ZERO,
                VPN_MAX_KEY_NAME_LEN);
    }
    while (nmhGetNextIndexFsVpnCaCertInfoTable
           (pFirstVpnCaCertKeyString, pNextVpnCaCertKeyString) != SNMP_FAILURE);

    free_octetstring (pFirstVpnCaCertKeyString);
    free_octetstring (pNextVpnCaCertKeyString);
    free_octetstring (pCaCertFileName);
    return SUCCESS;
}

/**************************************************************************/
/*  Function Name   : ShowRunningCertInfo                        */
/*  Description     : This function display the Cert Table info.        */
/*  Input (s)       : CliHandle,                                          */
/*  Output(s)       : None.                                               */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS.                            */
/**************************************************************************/
INT4
ShowRunningCertInfo (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE *pFirstVpnCertKeyString = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextVpnCertKeyString = NULL;
    INT4                i4CertEncodeType;
    tSNMP_OCTET_STRING_TYPE *pCertFileName = NULL;

    if ((pFirstVpnCertKeyString =
         allocmem_octetstring (VPN_MAX_KEY_NAME_LEN + VPN_ONE)) == NULL)
    {
        return CLI_FAILURE;
    }
    if ((pNextVpnCertKeyString =
         allocmem_octetstring (VPN_MAX_KEY_NAME_LEN + VPN_ONE)) == NULL)
    {
        free_octetstring (pFirstVpnCertKeyString);
        return CLI_FAILURE;
    }
    if ((pCertFileName =
         allocmem_octetstring (VPN_CERT_FILE_NAME_LEN + VPN_ONE)) == NULL)
    {
        free_octetstring (pFirstVpnCertKeyString);
        free_octetstring (pNextVpnCertKeyString);
        return CLI_FAILURE;
    }
    MEMSET (pNextVpnCertKeyString->pu1_OctetList, VPN_ZERO,
            VPN_MAX_KEY_NAME_LEN + VPN_ONE);

    if (nmhGetFirstIndexFsVpnCertInfoTable (pNextVpnCertKeyString) ==
        SNMP_FAILURE)
    {
        free_octetstring (pFirstVpnCertKeyString);
        free_octetstring (pNextVpnCertKeyString);
        free_octetstring (pCertFileName);
        return CLI_FAILURE;
    }
    do
    {
        MEMSET (pFirstVpnCertKeyString->pu1_OctetList, VPN_ZERO,
                VPN_MAX_KEY_NAME_LEN + VPN_ONE);
        MEMSET (pCertFileName->pu1_OctetList, VPN_ZERO,
                VPN_CERT_FILE_NAME_LEN + VPN_ONE);

        STRCPY (pFirstVpnCertKeyString->pu1_OctetList,
                pNextVpnCertKeyString->pu1_OctetList);
        pFirstVpnCertKeyString->i4_Length = pNextVpnCertKeyString->i4_Length;

        nmhGetFsVpnCertFileName (pFirstVpnCertKeyString, pCertFileName);
        if (pCertFileName->pu1_OctetList[VPN_INDEX_0] != '\0' &&
            pFirstVpnCertKeyString->pu1_OctetList[VPN_INDEX_0] != '\0')
        {
            CliPrintf (CliHandle, "vpn import cert file %s ",
                       pCertFileName->pu1_OctetList);
        }
        else
        {
            free_octetstring (pFirstVpnCertKeyString);
            free_octetstring (pNextVpnCertKeyString);
            free_octetstring (pCertFileName);
            return CLI_FAILURE;
        }
        nmhGetFsVpnCertEncodeType (pFirstVpnCertKeyString, &i4CertEncodeType);

        if (i4CertEncodeType == CLI_VPN_CERT_ENCODE_PEM)
            CliPrintf (CliHandle, "encode-type PEM ");
        else if (i4CertEncodeType == CLI_VPN_CERT_ENCODE_DER)
            CliPrintf (CliHandle, "encode-type DER ");

        CliPrintf (CliHandle, "key %s\r\n",
                   pFirstVpnCertKeyString->pu1_OctetList);

        MEMSET (pNextVpnCertKeyString->pu1_OctetList, VPN_ZERO,
                VPN_MAX_KEY_NAME_LEN + VPN_ONE);

    }
    while (nmhGetNextIndexFsVpnCertInfoTable
           (pFirstVpnCertKeyString, pNextVpnCertKeyString) != SNMP_FAILURE);

    free_octetstring (pFirstVpnCertKeyString);
    free_octetstring (pNextVpnCertKeyString);
    free_octetstring (pCertFileName);
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : ShowRunningKeyInfo                        */
/*  Description     : This function display the Import key info.        */
/*  Input (s)       : CliHandle,                                          */
/*  Output(s)       : None.                                               */
/*  Returns         : CLI_FAILURE/CLI_SUCCESS.                            */
/**************************************************************************/
INT4
ShowRunningKeyInfo (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE *pFirstVpnCertKeyString = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextVpnCertKeyString = NULL;
    INT4                i4KeyType;
    tSNMP_OCTET_STRING_TYPE *pCertKeyFileName = NULL;
    if ((pFirstVpnCertKeyString =
         allocmem_octetstring (VPN_MAX_KEY_NAME_LEN + VPN_ONE)) == NULL)
    {
        return CLI_FAILURE;
    }
    if ((pNextVpnCertKeyString =
         allocmem_octetstring (VPN_MAX_KEY_NAME_LEN + VPN_ONE)) == NULL)
    {
        free_octetstring (pFirstVpnCertKeyString);
        return CLI_FAILURE;
    }
    if ((pCertKeyFileName =
         allocmem_octetstring (VPN_CERT_KEY_FILE_NAME_LEN + VPN_ONE)) == NULL)
    {
        free_octetstring (pFirstVpnCertKeyString);
        free_octetstring (pNextVpnCertKeyString);
        return CLI_FAILURE;
    }
    MEMSET (pNextVpnCertKeyString->pu1_OctetList, VPN_ZERO,
            VPN_MAX_KEY_NAME_LEN + VPN_ONE);

    if (nmhGetFirstIndexFsVpnCertInfoTable (pNextVpnCertKeyString) ==
        SNMP_FAILURE)
    {
        free_octetstring (pFirstVpnCertKeyString);
        free_octetstring (pNextVpnCertKeyString);
        free_octetstring (pCertKeyFileName);
        return CLI_FAILURE;
    }
    do
    {
        MEMSET (pFirstVpnCertKeyString->pu1_OctetList, VPN_ZERO,
                VPN_MAX_KEY_NAME_LEN + VPN_ONE);
        MEMSET (pCertKeyFileName->pu1_OctetList, VPN_ZERO,
                VPN_CERT_KEY_FILE_NAME_LEN + VPN_ONE);

        STRCPY (pFirstVpnCertKeyString->pu1_OctetList,
                pNextVpnCertKeyString->pu1_OctetList);
        pFirstVpnCertKeyString->i4_Length = pNextVpnCertKeyString->i4_Length;

        nmhGetFsVpnCertKeyFileName (pFirstVpnCertKeyString, pCertKeyFileName);
        nmhGetFsVpnCertKeyType (pFirstVpnCertKeyString, &i4KeyType);

        if (i4KeyType == CLI_VPN_AUTH_DSA)
            CliPrintf (CliHandle, "vpn import DSA ");
        else if (i4KeyType == CLI_VPN_AUTH_RSA)
            CliPrintf (CliHandle, "vpn import RSA ");
        else
        {
            free_octetstring (pFirstVpnCertKeyString);
            free_octetstring (pNextVpnCertKeyString);
            free_octetstring (pCertKeyFileName);
            return CLI_FAILURE;
        }

        CliPrintf (CliHandle, "key %s file %s\r\n",
                   pFirstVpnCertKeyString->pu1_OctetList,
                   pCertKeyFileName->pu1_OctetList);

        MEMSET (pNextVpnCertKeyString->pu1_OctetList, VPN_ZERO,
                VPN_MAX_KEY_NAME_LEN + VPN_ONE);

    }
    while (nmhGetNextIndexFsVpnCertInfoTable
           (pFirstVpnCertKeyString, pNextVpnCertKeyString) != SNMP_FAILURE);

    free_octetstring (pFirstVpnCertKeyString);
    free_octetstring (pNextVpnCertKeyString);
    free_octetstring (pCertKeyFileName);
    return CLI_SUCCESS;
}
#endif /* VPN_WANTED */
#endif /* __VPNCLI_C__ */
