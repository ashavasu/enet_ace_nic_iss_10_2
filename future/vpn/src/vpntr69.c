/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vpntr69.c,v 1.5 2013/12/18 12:47:58 siva Exp $
 *
 * Description: File containing apis for TR69 related configuration
 *******************************************************************/
#define _VPNTR69_C_
#include "cfa.h"
#include "vpninc.h"
#include "tr.h"
#include "tr69dim.h"
#include "vpntr69.h"
#include "fsvpnlw.h"
#include "vpntrc.h"

/*****************************************************************************/
/*    Function Name             : VpnTr69InitTrList                          */
/*    Description               : Initialises VPN Tr069 List                 */
/*    Input(s)                  : None.                                      */
/*    Output(s)                 : None.                                      */
/*    Returns                   : None                                       */
/*****************************************************************************/
VOID
VpnTr69InitTrList (VOID)
{
    UTL_SLL_Init (&VPNList, 0);

    /* RAVPN Table List */
    UTL_SLL_Init (&UserList, 0);
    UTL_SLL_Init (&AddressPoolList, 0);
    UTL_SLL_Init (&VpnRemoteIdList, 0);

    return;
}

/************************************************************************
 *  Function Name   : VpnTr69ScanAndUpdateTrList
 *  Description     : Scans and Updates VPN TR069 Database
 *
 *  Input           : None
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
VpnTr69ScanAndUpdateTrList ()
{
    TrScan_User ();
    TrScan_AddressPool ();
    TrScan_VpnRemoteId ();

    return;
}

/******************************************************************************
 *  Function Name   : TrAdd_VPN
 *  Description     : Adds new VPN node.
 *
 *  Input           : u4TrIdx          - TR index of the new node
 *                    pOctetStrIdx     - Snmp Index of the new node
 *                    u4WANDeviceIfIdx - Interface index to which the new node
 *                                       is applied to.
 *  Output          :
 *  Returns         : Pointer to the new VPN Node 
 *****************************************************************************/
tVPN               *
TrAdd_VPN (UINT4 u4TrIdx, tSNMP_OCTET_STRING_TYPE * pOctetStrIdx,
           UINT4 u4WANDeviceIfIdx)
{
    tVPN               *pVPN = NULL;
    tVPN               *pNextVPN = NULL;
    UINT4               u4val = 0;
    UINT4               u4WanIpAddr = 0;
    CHR1               *pu1TempIp = NULL;
    tUtlInAddr          IpAddr;

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Entry: TrAdd_VPN\n");
    if (pOctetStrIdx == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "Creating an Empty Node for AddObject\n");
        pVPN = MEM_MALLOC (sizeof (tVPN), tVPN);
        if (pVPN == NULL)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Unable to Allocate Memory for New User Node\n");
            return (NULL);
        }

        MEMSET (pVPN, 0, sizeof (tVPN));

        if (SecUtilValidateIfIndex (u4WANDeviceIfIdx) != CFA_SUCCESS)
        {
            TR69_TRC (TR69_DBG_TRC, "Invalid Interface Index\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_VPN\n");
            MEM_FREE (pVPN);
            return SNMP_FAILURE;
        }
        pVPN->u4TrInstance = u4TrIdx;
        pVPN->i4PolicyIntfIndex = u4WANDeviceIfIdx;
        if (CfaGetWanIpfromIndex (&u4WANDeviceIfIdx, &u4WanIpAddr) !=
            CFA_SUCCESS)
        {
            TR69_TRC (TR69_DBG_TRC, "Invalid Interface Index\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_VPN\n");
            MEM_FREE (pVPN);
            return SNMP_FAILURE;
        }
        IpAddr.u4Addr = OSIX_NTOHL (u4WanIpAddr);
        MEMSET (pVPN->au1LocalTunTermAddr, '\0', 16);
        pu1TempIp = UtlInetNtoa (IpAddr);
        SNPRINTF ((CHR1 *) pVPN->au1LocalTunTermAddr,
                  sizeof (pVPN->au1LocalTunTermAddr), "%s", pu1TempIp);
        pVPN->u4GlobalValMask = VPN_VAL_MASK;
        pVPN->u4ValMask = 0;
        pVPN->u4Visited = 1;
        pVPN->u4PolicyRowStatus = NOT_IN_SERVICE;

        TMO_SLL_Add (&VPNList, &pVPN->Link);
        TR69_TRC (TR69_DBG_TRC, "Empty Node added to the global VPN List\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_VPN\n");

        return pVPN;
    }

    UTL_SLL_OFFSET_SCAN (&VPNList, pVPN, pNextVPN, tVPN *)
    {
        if ((pVPN->u4TrInstance == u4TrIdx)
            && (pVPN->i4PolicyIntfIndex == (INT4) u4WANDeviceIfIdx))
        {
            MynmhGet (*Join
                      (FsVpnPolicyType, 12, FsVpnTableINDEX, 1, pOctetStrIdx),
                      SNMP_DATA_TYPE_INTEGER, &u4val);

            if (u4val == 1)
            {
                /* Ipsec Manual Tyep */
                pVPN->u4GlobalValMask = (VPN_VAL_MASK | VPN_IPSEC_VAL_MASK);
                pVPN->u4ValMask = (VPN_VAL_MASK | VPN_IPSEC_VAL_MASK);
            }
            else if ((u4val == 2) || (u4val == 4))
            {
                /* psecIkePreshearedType */
                pVPN->u4GlobalValMask = (VPN_VAL_MASK | VPN_IKE_VAL_MASK);
                pVPN->u4ValMask = (VPN_VAL_MASK | VPN_IKE_VAL_MASK);
            }
            else
            {
                pVPN->u4GlobalValMask = VPN_VAL_MASK;
            }

            MynmhGet (*Join
                      (FsVpnPolicyRowStatus, 12, FsVpnTableINDEX, 1,
                       pOctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (u4val != ACTIVE)
            {
                pVPN->u4PolicyRowStatus = NOT_IN_SERVICE;
            }

            pVPN->u4TrInstance = u4TrIdx;
            pVPN->i4PolicyIntfIndex = u4WANDeviceIfIdx;
            MEMCPY (pVPN->au1PolicyName, pOctetStrIdx->pu1_OctetList,
                    pOctetStrIdx->i4_Length);
            pVPN->u4Visited = 1;
            TR69_TRC_ARG2 (TR69_DBG_TRC, "+  CRT %s.%d \r\n",
                           pVPN->au1PolicyName, u4TrIdx);
            break;
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_VPN\n");

    return pVPN;
}

/************************************************************************
 *  Function Name   : TrScan_VPN
 *  Description     : Scans and detects available VPNs.
 *
 *  Input           : none.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
UINT4
TrScan_VPN (tWANDevice * pDev, tWANConnDev * pWANConnDev,
            tWANIPConnection * pWANIPConnection)
{
    tSNMP_OCTET_STRING_TYPE PolicyName;
    tSNMP_OCTET_STRING_TYPE NextPolicyName;
    UINT1               au1PolicyName[MAX_OCTETSTRING_SIZE];
    UINT1               au1NextPolicyName[MAX_OCTETSTRING_SIZE];
    tVPN               *pVPN = NULL;
    tVPN               *pNextVPN = NULL;
    UINT4               u4NewNode = 0;
    INT4                i4rc = 0;
    INT4                i4PolicyIntfIndex = 0;
    UINT4               u4TrIdx = 0;
    tTMO_SLL           *pVPNList = NULL;
    CHR1                ac1ObjName[256];
    UINT4              *ptr = NULL;
    UINT4               u4Size = 0;

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Entry: TrScan_VPN\n");

    MEMSET (ac1ObjName, '\0', 256);
    MEMSET (&PolicyName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextPolicyName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MEMSET (au1PolicyName, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (au1NextPolicyName, 0, MAX_OCTETSTRING_SIZE);

    PolicyName.pu1_OctetList = au1PolicyName;
    NextPolicyName.pu1_OctetList = au1NextPolicyName;

    i4rc = nmhGetFirstIndexFsVpnTable (&PolicyName);
    if (i4rc == SNMP_FAILURE)
    {
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: nmhGetFirstIndexFsVpnTable Returned - Failure\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrScan_VPN\n");
        return (OSIX_FAILURE);
    }
    STRNCPY (NextPolicyName.pu1_OctetList, PolicyName.pu1_OctetList,
             PolicyName.i4_Length);
    NextPolicyName.i4_Length = PolicyName.i4_Length;

    pVPNList = &VPNList;

    UTL_SLL_OFFSET_SCAN (pVPNList, pVPN, pNextVPN, tVPN *)
    {
        pVPN->u4Visited = 0;
    }
    while (1)
    {
        pVPN = NULL;
        u4NewNode = 1;

        /* Get the interface index to which the policy is
         * attached. This policy node is to be associated
         * to the TR instances of only those interface nodes.
         */
        i4rc = nmhGetFsVpnPolicyIntfIndex (&NextPolicyName, &i4PolicyIntfIndex);
        if (SNMP_FAILURE == i4rc)
        {
            u4NewNode = 0;
        }
        else
        {
            if (pWANConnDev->u4IfIdx == (UINT4) (i4PolicyIntfIndex))
            {
                /* Do not instantiate if already present */
                UTL_SLL_OFFSET_SCAN (pVPNList, pVPN, pNextVPN, tVPN *)
                {
                    if (STRCMP (pVPN->au1PolicyName,
                                NextPolicyName.pu1_OctetList) == 0)
                    {
                        TR69_TRC_ARG1 (TR69_DBG_TRC,
                                       "Policy %s Node already present \n",
                                       NextPolicyName.pu1_OctetList);
                        u4NewNode = 0;
                        pVPN->u4Visited = 1;
                        break;
                    }
                }
            }
            else
            {
                /* The policy does not belong to this WAN Interface. 
                 * So no need to add.
                 */
                u4NewNode = 0;
            }
        }

        /* Instantiate if this is a new instance */
        if (u4NewNode != 0)
        {
            u4TrIdx = 0;
            SNPRINTF (ac1ObjName, sizeof (ac1ObjName), "%s%lu%s%lu%s%lu%s",
                      "InternetGatewayDevice.WANDevice.", pDev->u4TrInstance,
                      ".WANConnectionDevice.", pWANConnDev->u4TrInstance,
                      ".WANIPConnection.", pWANIPConnection->u4TrInstance,
                      ".vpn");

            i4rc = addObjectIntern (ac1ObjName, (int *) &u4TrIdx);
            if (i4rc == 0)
            {
                pVPN =
                    TrAdd_VPN (u4TrIdx, &NextPolicyName, pWANConnDev->u4IfIdx);
                if (pVPN == NULL)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "Creating a New Node for VPN Table - Failed\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrScan_VPN\n");
                    return SNMP_FAILURE;
                }
                pVPN->u4Visited = 1;
            }
            else
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "addObjectIntern failed, rc = %d\n", i4rc);
            }
        }

        STRNCPY (PolicyName.pu1_OctetList, NextPolicyName.pu1_OctetList,
                 NextPolicyName.i4_Length);
        PolicyName.i4_Length = NextPolicyName.i4_Length;

        MEMSET (NextPolicyName.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
        i4rc = nmhGetNextIndexFsVpnTable (&PolicyName, &NextPolicyName);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "BREAKING from VPN scan\n");
            break;
        }
    }

    UTL_SLL_OFFSET_SCAN (pVPNList, pVPN, pNextVPN, tVPN *)
    {
        /* Before initializing  the structure elements to zero, check 
           is made against both visited flag and the table mask in order to make 
           sure that this does not intervene the row creation process */

        if ((pVPN->u4Visited == 0) &&
            ((pVPN->u4ValMask & pVPN->u4GlobalValMask) ==
             pVPN->u4GlobalValMask)
            && (pWANConnDev->u4IfIdx == (UINT4) (pVPN->i4PolicyIntfIndex)))
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Deleting the node as it is not present in SNMP\n");

            ptr = &(pVPN->u4ValMask);
            u4Size =
                sizeof (tVPN) - (3 * sizeof (UINT4) + sizeof (tTMO_SLL_NODE));
            MEMSET (ptr, 0, u4Size);
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrScan_VPN\n");
    return 0;
}

INT4
TrScan_User ()
{
    tSNMP_OCTET_STRING_TYPE FsVpnRaUsrName;
    tSNMP_OCTET_STRING_TYPE NextFsVpnRaUsrName;
    UINT1               au1FsVpnRaUsrName[MAX_OCTETSTRING_SIZE];
    UINT1               au1NextFsVpnRaUsrName[MAX_OCTETSTRING_SIZE];

    tUser              *pUser = NULL;
    tUser              *pNextUser = NULL;

    UINT4               u4NewNode = 0;
    INT4                i4rc = 0;
    UINT4               u4TrIdx = 0;

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Entry: TrScan_User\n");

    MEMSET (&FsVpnRaUsrName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextFsVpnRaUsrName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MEMSET (au1FsVpnRaUsrName, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (au1NextFsVpnRaUsrName, 0, MAX_OCTETSTRING_SIZE);

    FsVpnRaUsrName.pu1_OctetList = au1FsVpnRaUsrName;
    FsVpnRaUsrName.i4_Length = STRLEN (au1FsVpnRaUsrName);
    NextFsVpnRaUsrName.pu1_OctetList = au1NextFsVpnRaUsrName;
    NextFsVpnRaUsrName.i4_Length = STRLEN (au1NextFsVpnRaUsrName);

    i4rc = nmhGetFirstIndexFsVpnRaUsersTable (&FsVpnRaUsrName);
    if (i4rc == SNMP_FAILURE)
    {
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: nmhGetFirstIndexFsVpnRaUsersTable returned failure\n");
        return (OSIX_FAILURE);
    }
    STRNCPY (NextFsVpnRaUsrName.pu1_OctetList,
             FsVpnRaUsrName.pu1_OctetList, FsVpnRaUsrName.i4_Length);
    NextFsVpnRaUsrName.i4_Length = FsVpnRaUsrName.i4_Length;

    UTL_SLL_OFFSET_SCAN (&UserList, pUser, pNextUser, tUser *)
    {
        pUser->u4Visited = 0;
    }
    while (1)
    {
        pUser = NULL;
        u4NewNode = 1;

        /* Do not instantiate if already present */

        UTL_SLL_OFFSET_SCAN (&UserList, pUser, pNextUser, tUser *)
        {
            if (STRCMP (pUser->au1Name, NextFsVpnRaUsrName.pu1_OctetList) == 0)
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "OctetStrIdx %s Node already present \n",
                               NextFsVpnRaUsrName.pu1_OctetList);
                u4NewNode = 0;
                pUser->u4Visited = 1;
                break;
            }
        }

        /* Instantiate if this is a new instance */
        if (u4NewNode)
        {
            u4TrIdx = 0;
            TR69_TRC (TR69_DBG_TRC, "Creating a New Node for User Table\n");

            i4rc =
                addObjectIntern ("InternetGatewayDevice.VPN.RAVPN.User",
                                 (int *) &u4TrIdx);

            if (i4rc == 0)
            {
                pUser = TrAdd_User (u4TrIdx, &NextFsVpnRaUsrName);
                if (pUser == NULL)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "Creating a New Node for User Table - Failed\n");
                    continue;
                }
                TR69_TRC (TR69_DBG_TRC,
                          "Creating a New Node for User Table - Success\n");
                pUser->u4Visited = 1;
            }
            else
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "addObjectIntern failed, rc = %d for User Table\n",
                               i4rc);
            }
        }

        STRNCPY (FsVpnRaUsrName.pu1_OctetList, NextFsVpnRaUsrName.pu1_OctetList,
                 NextFsVpnRaUsrName.i4_Length);
        FsVpnRaUsrName.i4_Length = NextFsVpnRaUsrName.i4_Length;

        MEMSET (NextFsVpnRaUsrName.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
        i4rc =
            nmhGetNextIndexFsVpnRaUsersTable (&FsVpnRaUsrName,
                                              &NextFsVpnRaUsrName);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "BREAKING from RavpnUser scan\n");
            break;
        }
    }
    /* Incase if an entry is deleted via CLI or SNMP, in order 
       to synchronize TR database locate the node and initialize 
       the structure  elements to zero */

    UTL_SLL_OFFSET_SCAN (&UserList, pUser, pNextUser, tUser *)
    {
        /* Before initializing  the structure elements to zero, check 
           is made against both visited flag and the table mask in order to make 
           sure that this does not intervene the row creation process */

        if ((pUser->u4Visited == 0) &&
            (((pUser->u4ValMask) & (USER_VAL_MASK)) == (USER_VAL_MASK)))
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Deleting the node as it is not present in SNMP\n");
            pUser->u4RowStatus = 0;
            pUser->u4ValMask = 0;
            MEMSET (pUser->au1Name, '\0', USER_NAME);
            MEMSET (pUser->au1Secret, '\0', USER_SECRET);
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrScan_User\n");

    return OSIX_SUCCESS;
}

tUser              *
TrAdd_User (UINT4 u4TrIdx, tSNMP_OCTET_STRING_TYPE * pOctetStrIdx)
{
    tUser              *pUser = NULL;
    tUser              *pNextUser = NULL;

    /* If TrAdd is called via init function, then 
       allocate the memory and initalize structure members.
       If TrAdd is called via Scan function, 
       then locate the node, assign the SNMP index 
       and table mask. */

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Entry: TrAdd_User\n");
    if (pOctetStrIdx == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "Creating an Empty Node for AddObject\n");
        pUser = MEM_MALLOC (sizeof (tUser), tUser);
        if (pUser == NULL)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Unable to Allocate Memory for New User Node\n");
            return (NULL);

        }

        MEMSET (pUser, 0, sizeof (tUser));
        pUser->u4TrInstance = u4TrIdx;
        pUser->u4ValMask = 0;
        pUser->u4RowStatus = NOT_IN_SERVICE;

        TMO_SLL_Add (&UserList, &pUser->Link);

        TR69_TRC (TR69_DBG_TRC, "Empty Node added to the global User List\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_User\n");
        return pUser;
    }
    UTL_SLL_OFFSET_SCAN (&UserList, pUser, pNextUser, tUser *)
    {
        if (pUser->u4TrInstance == u4TrIdx)
        {
            MEMCPY (pUser->au1Name, pOctetStrIdx->pu1_OctetList,
                    pOctetStrIdx->i4_Length);
            pUser->u4ValMask = USER_VAL_MASK;
            break;
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_User\n");
    return pUser;
}

INT4
TrScan_AddressPool ()
{
    tAddressPool       *pAddressPoolTable = NULL;
    tAddressPool       *pNextAddressPoolTable = NULL;
    tSNMP_OCTET_STRING_TYPE AddrPoolName;
    tSNMP_OCTET_STRING_TYPE NextAddrPoolName;
    UINT1               au1AddrPoolName[MAX_OCTETSTRING_SIZE];
    UINT1               au1NextAddrPoolName[MAX_OCTETSTRING_SIZE];
    UINT4               u4NewNode = 0;
    INT4                i4rc = 0;
    UINT4               u4TrIdx = 0;

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Entry: TrScan_AddressPool\n");
    MEMSET (&AddrPoolName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextAddrPoolName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MEMSET (au1AddrPoolName, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (au1NextAddrPoolName, 0, MAX_OCTETSTRING_SIZE);

    AddrPoolName.pu1_OctetList = au1AddrPoolName;
    AddrPoolName.i4_Length = STRLEN (au1AddrPoolName);
    NextAddrPoolName.pu1_OctetList = au1NextAddrPoolName;
    NextAddrPoolName.i4_Length = STRLEN (au1NextAddrPoolName);

    i4rc = nmhGetFirstIndexFsVpnRaAddressPoolTable (&AddrPoolName);
    if (i4rc == SNMP_FAILURE)
    {
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: nmhGetFirstIndexFsVpnRaAddressPoolTable Returned - Failure\n");

        return (OSIX_FAILURE);
    }
    STRNCPY (NextAddrPoolName.pu1_OctetList,
             AddrPoolName.pu1_OctetList, AddrPoolName.i4_Length);
    NextAddrPoolName.i4_Length = AddrPoolName.i4_Length;

    UTL_SLL_OFFSET_SCAN (&AddressPoolList, pAddressPoolTable,
                         pNextAddressPoolTable, tAddressPool *)
    {
        pAddressPoolTable->u4Visited = 0;
    }
    while (1)
    {
        pAddressPoolTable = NULL;
        u4NewNode = 1;

        /* Do not instantiate if already present */

        UTL_SLL_OFFSET_SCAN (&AddressPoolList, pAddressPoolTable,
                             pNextAddressPoolTable, tAddressPool *)
        {
            if (STRCMP
                (pAddressPoolTable->au1Name,
                 NextAddrPoolName.pu1_OctetList) == 0)
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "OctetStrIdx %s Node already present \n",
                               NextAddrPoolName.pu1_OctetList);
                u4NewNode = 0;
                pAddressPoolTable->u4Visited = 1;
                break;
            }
        }

        /* Instantiate if this is a new instance */
        if (u4NewNode)
        {
            u4TrIdx = 0;
            TR69_TRC (TR69_DBG_TRC, "Creating a New Node for User Table\n");

            i4rc =
                addObjectIntern
                ("InternetGatewayDevice.VPN.RAVPN.AddressPool",
                 (int *) &u4TrIdx);

            if (i4rc == 0)
            {
                pAddressPoolTable =
                    TrAdd_AddressPool (u4TrIdx, &NextAddrPoolName);
                if (pAddressPoolTable == NULL)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "Creating a New Node for User Table - Failed\n");
                    continue;
                }
                TR69_TRC (TR69_DBG_TRC,
                          "Creating a New Node for User Table - Success\n");
                pAddressPoolTable->u4Visited = 1;
            }
            else
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "addObjectIntern failed, rc = %d\n", i4rc);
            }
        }

        STRNCPY (AddrPoolName.pu1_OctetList, NextAddrPoolName.pu1_OctetList,
                 NextAddrPoolName.i4_Length);
        AddrPoolName.i4_Length = NextAddrPoolName.i4_Length;
        MEMSET (NextAddrPoolName.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);

        i4rc =
            nmhGetNextIndexFsVpnRaAddressPoolTable (&AddrPoolName,
                                                    &NextAddrPoolName);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "BREAKING from AddressPool Table scan\n");
            break;
        }
    }
    /* Incase if an entry is deleted via CLI or SNMP, in order 
       to synchronize TR database locate the node and initialize 
       the structure  elements to zero */

    UTL_SLL_OFFSET_SCAN (&AddressPoolList, pAddressPoolTable,
                         pNextAddressPoolTable, tAddressPool *)
    {
        /* Before initializing  the structure elements to zero, check 
           is made against both visited flag and the table mask in order to make 
           sure that this does not intervene the row creation process */

        if ((pAddressPoolTable->u4Visited == 0) &&
            (((pAddressPoolTable->u4ValMask) & (ADDRESS_POOL_VAL_MASK)) ==
             (ADDRESS_POOL_VAL_MASK)))
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Deleting the node as it is not present in SNMP\n");
            pAddressPoolTable->u4RowStatus = 0;
            pAddressPoolTable->u4ValMask = 0;
            MEMSET (pAddressPoolTable->au1Name, '\0', ADDRESSPOOL_NAME);
            MEMSET (pAddressPoolTable->au1EndAddress, '\0',
                    ADDRESSPOOL_END_ADDRESS);
            MEMSET (pAddressPoolTable->au1StartAddress, '\0',
                    ADDRESSPOOL_START_ADDRESS);
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrScan_AddressPool\n");
    return OSIX_SUCCESS;

}

tAddressPool       *
TrAdd_AddressPool (UINT4 u4TrIdx, tSNMP_OCTET_STRING_TYPE * pOctetStrIdx)
{

    tAddressPool       *pAddressPool = NULL;
    tAddressPool       *pNextAddressPool = NULL;

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Entry: TrAdd_AddressPool\n");
    /* If TrAdd is called via init function, then 
       allocate the memory and initalize structure members.
       If TrAdd is called via Scan function, 
       then locate the node, assign the SNMP index 
       and table mask. */

    if (pOctetStrIdx == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "Creating an Empty Node for AddObject\n");
        pAddressPool = MEM_MALLOC (sizeof (tAddressPool), tAddressPool);
        if (pAddressPool == NULL)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Unable to Allocate Memory for New AddressPool Node\n");
            return (NULL);

        }
        MEMSET (pAddressPool, 0, sizeof (tAddressPool));
        pAddressPool->u4TrInstance = u4TrIdx;
        pAddressPool->u4ValMask = 0;

        pAddressPool->u4RowStatus = NOT_IN_SERVICE;
        TMO_SLL_Add (&AddressPoolList, &pAddressPool->Link);

        TR69_TRC (TR69_DBG_TRC,
                  "Empty Node added to the global AddressPool List\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_AddressPool\n");

        return pAddressPool;
    }

    UTL_SLL_OFFSET_SCAN (&AddressPoolList, pAddressPool,
                         pNextAddressPool, tAddressPool *)
    {
        if (pAddressPool->u4TrInstance == u4TrIdx)
        {
            MEMCPY (pAddressPool->au1Name, pOctetStrIdx->pu1_OctetList,
                    pOctetStrIdx->i4_Length);
            pAddressPool->u4ValMask = ADDRESS_POOL_VAL_MASK;
            break;
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_AddressPool\n");
    return pAddressPool;
}

INT4
TrScan_VpnRemoteId ()
{
    tSNMP_OCTET_STRING_TYPE RemoteIdValue;
    tSNMP_OCTET_STRING_TYPE NextRemoteIdValue;
    UINT1               au1RemoteIdValue[MAX_OCTETSTRING_SIZE];
    UINT1               au1NextRemoteIdValue[MAX_OCTETSTRING_SIZE];
    tVpnRemoteId       *pRemoteIdTable = NULL;
    tVpnRemoteId       *pNextRemoteIdTable = NULL;
    INT4                i4RemoteIdType = 0;
    INT4                i4NextRemoteIdType = 0;

    UINT4               u4NewNode = 0;
    INT4                i4rc = 0;
    UINT4               u4TrIdx = 0;

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Entry: TrScan_VpnRemoteId\n");
    MEMSET (&RemoteIdValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextRemoteIdValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MEMSET (au1RemoteIdValue, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (au1NextRemoteIdValue, 0, MAX_OCTETSTRING_SIZE);

    RemoteIdValue.pu1_OctetList = au1RemoteIdValue;
    NextRemoteIdValue.pu1_OctetList = au1NextRemoteIdValue;

    i4rc = nmhGetFirstIndexFsVpnRemoteIdTable (&i4RemoteIdType, &RemoteIdValue);

    if (i4rc == SNMP_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    MEMCPY (NextRemoteIdValue.pu1_OctetList, RemoteIdValue.pu1_OctetList,
            RemoteIdValue.i4_Length);
    NextRemoteIdValue.i4_Length = RemoteIdValue.i4_Length;

    i4NextRemoteIdType = i4RemoteIdType;

    UTL_SLL_OFFSET_SCAN (&VpnRemoteIdList, pRemoteIdTable,
                         pNextRemoteIdTable, tVpnRemoteId *)
    {
        pRemoteIdTable->u4Visited = 0;
    }
    while (1)
    {
        pRemoteIdTable = NULL;
        u4NewNode = 1;
        /* Do not instantiate if already present */
        UTL_SLL_OFFSET_SCAN (&VpnRemoteIdList, pRemoteIdTable,
                             pNextRemoteIdTable, tVpnRemoteId *)
        {
            if (STRCASECMP (pRemoteIdTable->au1Type, "ipv4") == 0)
            {
                pRemoteIdTable->i4Type = 1;
            }
            else if (STRCASECMP (pRemoteIdTable->au1Type, "email") == 0)
            {
                pRemoteIdTable->i4Type = 3;
            }
            else if (STRCASECMP (pRemoteIdTable->au1Type, "fqdn") == 0)
            {
                pRemoteIdTable->i4Type = 2;
            }
            else if (STRCASECMP (pRemoteIdTable->au1Type, "keyId") == 0)
            {
                pRemoteIdTable->i4Type = 11;
            }
            if ((STRCMP
                 (pRemoteIdTable->au1Value,
                  NextRemoteIdValue.pu1_OctetList) == 0)
                && ((pRemoteIdTable->i4Type) == i4NextRemoteIdType))
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "Remote Id Value %s Node already present \n",
                               NextRemoteIdValue.pu1_OctetList);
                u4NewNode = 0;
                pRemoteIdTable->u4Visited = 1;
                break;
            }
        }

        /* Instantiate if this is a new instance */
        if (u4NewNode)
        {
            u4TrIdx = 0;
            i4rc =
                addObjectIntern ("InternetGatewayDevice.VPN.RemoteId",
                                 (int *) &u4TrIdx);

            if (i4rc == 0)
            {
                pRemoteIdTable =
                    TrAdd_VpnRemoteId (u4TrIdx, &NextRemoteIdValue,
                                       i4NextRemoteIdType);
                if (pRemoteIdTable == NULL)
                {
                    continue;
                }
                pRemoteIdTable->u4Visited = 1;
            }
            else
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "addObjectIntern failed, rc = %d\n", i4rc);
            }
        }

        /* get the next elem */
        MEMCPY (RemoteIdValue.pu1_OctetList, NextRemoteIdValue.pu1_OctetList,
                NextRemoteIdValue.i4_Length);
        RemoteIdValue.i4_Length = NextRemoteIdValue.i4_Length;

        i4RemoteIdType = i4NextRemoteIdType;
        MEMSET (NextRemoteIdValue.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
        i4rc =
            nmhGetNextIndexFsVpnRemoteIdTable (i4RemoteIdType,
                                               &i4NextRemoteIdType,
                                               &RemoteIdValue,
                                               &NextRemoteIdValue);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "BREAKING from VpnRemoteId Table scan\n");
            break;
        }
    }
    /* Incase if an entry is deleted via CLI or SNMP, in order 
       to synchronize TR database locate the node and initialize 
       the structure  elements to zero */

    UTL_SLL_OFFSET_SCAN (&VpnRemoteIdList, pRemoteIdTable,
                         pNextRemoteIdTable, tVpnRemoteId *)
    {
        /* Before initializing  the structure elements to zero, check 
           is made against both visited flag and the table mask in order to make 
           sure that this does not intervene the row creation process */

        if ((pRemoteIdTable->u4Visited == 0) &&
            (((pRemoteIdTable->u4ValMask) & (VPN_REMOTE_ID_VAL_MASK)) ==
             (VPN_REMOTE_ID_VAL_MASK)))
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Deleting the node as it is not present in SNMP\n");
            pRemoteIdTable->u4Status = 0;
            pRemoteIdTable->u4ValMask = 0;
            pRemoteIdTable->i4Type = 0;
            MEMSET (pRemoteIdTable->au1Value, '\0', VPNREMOTEID_VALUE);
            MEMSET (pRemoteIdTable->au1Type, '\0', VPNREMOTEID_TYPE);
            MEMSET (pRemoteIdTable->au1Key, '\0', VPNREMOTEID_KEY);
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrScan_VpnRemoteId\n");

    return OSIX_SUCCESS;
}

tVpnRemoteId       *
TrAdd_VpnRemoteId (UINT4 u4TrIdx, tSNMP_OCTET_STRING_TYPE * pOctetStrIdx,
                   INT4 i4Idx0)
{
    tVpnRemoteId       *pVpnRemoteId = NULL;
    tVpnRemoteId       *pNextVpnRemoteId = NULL;

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Entry: TrAdd_VpnRemoteId\n");

    if (pOctetStrIdx == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "Creating an Empty Node for AddObject\n");
        pVpnRemoteId = MEM_MALLOC (sizeof (tVpnRemoteId), tVpnRemoteId);
        if (pVpnRemoteId == NULL)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Unable to Allocate Memory for New VPNRemoteID Node\n");
            return (NULL);

        }
        MEMSET (pVpnRemoteId, 0, sizeof (tVpnRemoteId));

        pVpnRemoteId->u4TrInstance = u4TrIdx;

        pVpnRemoteId->u4ValMask = 0;

        pVpnRemoteId->u4Status = NOT_IN_SERVICE;

        TMO_SLL_Add (&VpnRemoteIdList, &pVpnRemoteId->Link);
        TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_VPNRemoteId\n");

        return pVpnRemoteId;
    }

    UTL_SLL_OFFSET_SCAN (&VpnRemoteIdList, pVpnRemoteId,
                         pNextVpnRemoteId, tVpnRemoteId *)
    {
        if (pVpnRemoteId->u4TrInstance == u4TrIdx)
        {
            MEMSET (pVpnRemoteId->au1Value, '\0', VPNREMOTEID_VALUE);
            MEMCPY (pVpnRemoteId->au1Value, pOctetStrIdx->pu1_OctetList,
                    pOctetStrIdx->i4_Length);
            pVpnRemoteId->u4ValMask = VPN_REMOTE_ID_VAL_MASK;

            pVpnRemoteId->i4Type = i4Idx0;
            break;
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrAdd_VPNRemoteId\n");
    return pVpnRemoteId;
}

/************************************************************************
 *  Function Name   : InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
 *  Description     : Get function for InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int  
     
     
     
     
     
     
     
    TrGetInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
    (char *name, int idx1, int idx2, int idx3, int idx4, ParameterValue * value)
{
    UINT4               u4val = 0;
    UINT4              *ptr = NULL;
    UINT4               u4Size = 0;
    INT4                i4rc = 0;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    /* INDICES: */
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];

    tVPN               *pVPN = NULL;
    tVPN               *pNextVPN = NULL;
    tVPN               *pNode = NULL;
    tWANDevice         *pWANDevice = NULL;
    tWANDevice         *pNextWANDevice = NULL;
    CHR1               *pu1TempIp = NULL;
    tUtlInAddr          IpAddr;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrGetInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN\n");
    UNUSED_PARAM (idx2);
    UNUSED_PARAM (idx3);

    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    OctetStr.pu1_OctetList = au1OctetList;
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;

    UTL_SLL_OFFSET_SCAN (&WANDeviceList, pWANDevice, pNextWANDevice,
                         tWANDevice *)
    {
        if (pWANDevice->u4TrInstance == (UINT4) idx1)
        {
            break;
        }
    }

    if (pWANDevice == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "-E- Invalid WanDevice - Failed \r\n");

        return -1;
    }

    UTL_SLL_OFFSET_SCAN (&VPNList, pVPN, pNextVPN, tVPN *)
    {
        if (pVPN->u4TrInstance == (UINT4) idx4)
        {
            if ((UINT4) (pVPN->i4PolicyIntfIndex) == pWANDevice->u4IfIdx)
            {
                OctetStrIdx.i4_Length = STRLEN (pVPN->au1PolicyName);
                MEMCPY (OctetStrIdx.pu1_OctetList, pVPN->au1PolicyName,
                        OctetStrIdx.i4_Length);
                if (((pVPN->u4ValMask & pVPN->u4GlobalValMask) ==
                     pVPN->u4GlobalValMask))
                {
                    if (nmhValidateIndexInstanceFsVpnTable (&OctetStrIdx) !=
                        SNMP_SUCCESS)
                    {
                        TR69_TRC (TR69_DBG_TRC,
                                  "-E- nmhValidateIndexInstanceFsVpnTable - Failed \r\n");
                        /* set all fields in pNode to 0 */

                        ptr = &(pVPN->u4ValMask);
                        u4Size =
                            sizeof (tVPN) - (3 * sizeof (UINT4) +
                                             sizeof (tTMO_SLL_NODE));
                        MEMSET (ptr, 0, u4Size);

                    }
                }
                break;
            }
        }
    }

    if (pVPN == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "VPN Node not found in VPN List\n");
        return -1;
    }

    pNode = pVPN;

    if (STRCMP (name, "IkePhase1PeerIdType") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnIkePhase1PeerIdType, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);

            if (u4val == 1)
            {
                MEMSET (pNode->au1IkePhase1PeerIdType, '\0',
                        VPN_IKE_PHASE1_PEER_ID_TYPE);
                MEMCPY (pNode->au1IkePhase1PeerIdType, "IPV4", 4);
                pNode->i4IkePhase1PeerIdType = 1;
            }
            else if (u4val == 2)
            {
                MEMSET (pNode->au1IkePhase1PeerIdType, '\0',
                        VPN_IKE_PHASE1_PEER_ID_TYPE);
                MEMCPY (pNode->au1IkePhase1PeerIdType, "FQDN", 4);
                pNode->i4IkePhase1PeerIdType = 2;
            }
            else if (u4val == 3)
            {
                MEMSET (pNode->au1IkePhase1PeerIdType, '\0',
                        VPN_IKE_PHASE1_PEER_ID_TYPE);
                MEMCPY (pNode->au1IkePhase1PeerIdType, "EMAIL", 5);
                pNode->i4IkePhase1PeerIdType = 3;
            }
            else if (u4val == 11)
            {
                MEMSET (pNode->au1IkePhase1PeerIdType, '\0',
                        VPN_IKE_PHASE1_PEER_ID_TYPE);
                MEMCPY (pNode->au1IkePhase1PeerIdType, "KEYID", 5);
                pNode->i4IkePhase1PeerIdType = 11;
            }

            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnIkePhase1PeerIdType\n");
                return -1;
            }
            value->out_cval = pNode->au1IkePhase1PeerIdType;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase1PeerIdType ret %s\n",
                           pNode->au1IkePhase1PeerIdType);
            return 0;
        }
        else
        {
            value->out_cval = pNode->au1IkePhase1PeerIdType;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase1PeerIdType Tr ret %s\n",
                           pNode->au1IkePhase1PeerIdType);
            return 0;
        }
    }
    else if (STRCMP (name, "IkePhase1DHGroup") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnIkePhase1DHGroup, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnIkePhase1DHGroup\n");
                return -1;
            }
            value->out_int = u4val;
            pNode->i4IkePhase1DHGroup = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase1DHGroup ret %d\n", u4val);
            return 0;
        }
        else
        {
            value->out_int = pNode->i4IkePhase1DHGroup;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase1DHGroup tr ret %d\n",
                           value->out_int);
            return 0;
        }
    }
    else if (STRCMP (name, "IkePhase1HashAlgo") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnIkePhase1HashAlgo, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnIkePhase1HashAlgo\n");
                return -1;
            }
            value->out_int = u4val;
            pNode->i4IkePhase1HashAlgo = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FsVpnIkePhase1HashAlgo ret %d\n",
                           u4val);
            return 0;
        }
        else
        {
            value->out_int = pNode->i4IkePhase1HashAlgo;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase1HashAlgo tr ret %d\n",
                           value->out_int);
            return 0;
        }
    }
    else if (STRCMP (name, "PolicyIntfIndex") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnPolicyIntfIndex, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnPolicyIntfIndex\n");
                return -1;
            }
            value->out_int = u4val;
            pNode->i4PolicyIntfIndex = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnPolicyIntfIndex ret %d\n", u4val);
            return 0;
        }
        else
        {
            value->out_int = pNode->i4PolicyIntfIndex;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnPolicyIntfIndex tr ret %d\n",
                           value->out_int);
            return 0;
        }
    }
    else if (STRCMP (name, "LocalProtectNetwork") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnLocalProtectNetwork, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_IP_ADDR_PRIM, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnLocalProtectNetwork\n");
                return -1;
            }
            IpAddr.u4Addr = OSIX_NTOHL (u4val);
            MEMSET (pNode->au1LocalProtectNetwork, '\0', 16);
            pu1TempIp = UtlInetNtoa (IpAddr);
            SNPRINTF ((CHR1 *) pNode->au1LocalProtectNetwork,
                      sizeof (pNode->au1LocalProtectNetwork), "%s", pu1TempIp);
            value->out_cval = pNode->au1LocalProtectNetwork;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnLocalProtectNetwork ret %s\n",
                           value->out_cval);
            return 0;
        }
        else
        {
            value->out_cval = pNode->au1LocalProtectNetwork;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnLocalProtectNetwork tr ret %s\n",
                           value->out_cval);
            return 0;
        }
    }
    else if (STRCMP (name, "AntiReplay") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnAntiReplay, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC, "-E- Get failed for FsVpnAntiReplay\n");
                return -1;
            }
            value->out_int = u4val;
            pNode->i4AntiReplay = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FsVpnAntiReplay ret %d\n",
                           u4val);
            return 0;
        }
        else
        {
            value->out_int = pNode->i4AntiReplay;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnAntiReplay tr ret %d\n", value->out_int);
            return 0;
        }
    }

    else if (STRCMP (name, "PolicyType") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnPolicyType, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC, "-E- Get failed for FsVpnPolicyType\n");
                return -1;
            }
            value->out_int = u4val;
            pNode->i4PolicyType = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FsVpnPolicyType ret %d\n",
                           u4val);
            return 0;
        }
        else
        {
            value->out_int = pNode->i4PolicyType;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnPolicyType tr ret %d\n", value->out_int);
            return 0;
        }
    }
    else if (STRCMP (name, "IkePhase2LifeTimeType") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnIkePhase2LifeTimeType, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnIkePhase2LifeTimeType\n");
                return -1;
            }

            if (u4val == LIFE_TIME_MINS)
            {
                value->out_int = TR_LIFE_TIME_MINS;
            }
            else if (u4val == LIFE_TIME_HOURS)
            {
                value->out_int = TR_LIFE_TIME_HOURS;
            }
            else if (u4val == LIFE_TIME_DAYS)
            {
                value->out_int = TR_LIFE_TIME_DAYS;
            }
            else
                value->out_int = u4val;

            pNode->i4IkePhase2LifeTimeType = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase2LifeTimeType ret %d\n", u4val);
            return 0;
        }
        else
        {
            value->out_int = pNode->i4IkePhase2LifeTimeType;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase2LifeTimeType tr ret %d\n",
                           value->out_int);
            return 0;
        }
    }
    else if (STRCMP (name, "LocalTunTermAddr") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnLocalTunTermAddr, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_IP_ADDR_PRIM, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnLocalTunTermAddr\n");
                return -1;
            }
            IpAddr.u4Addr = OSIX_NTOHL (u4val);
            MEMSET (pNode->au1LocalTunTermAddr, '\0', 16);
            pu1TempIp = UtlInetNtoa (IpAddr);
            SNPRINTF ((CHR1 *) pNode->au1LocalTunTermAddr,
                      sizeof (pNode->au1LocalTunTermAddr), "%s", pu1TempIp);
            value->out_cval = pNode->au1LocalTunTermAddr;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnLocalTunTermAddr ret %s\n",
                           value->out_cval);
            return 0;
        }
        else
        {
            value->out_cval = pNode->au1LocalTunTermAddr;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnLocalTunTermAddr tr ret %s\n",
                           value->out_cval);
            return 0;
        }
    }

    else if (STRCMP (name, "IkePhase1Mode") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnIkePhase1Mode, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnIkePhase1Mode\n");
                return -1;
            }
            value->out_int = u4val;
            pNode->i4IkePhase1Mode = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase1Mode ret %d\n", u4val);
            return 0;
        }
        else
        {
            value->out_int = pNode->i4IkePhase1Mode;
            TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FsVpnIkePhase1Mode tr ret %d\n",
                           value->out_int);
            return 0;
        }
    }
    else if (STRCMP (name, "IkePhase1LifeTimeType") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnIkePhase1LifeTimeType, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnIkePhase1LifeTimeType\n");
                return -1;
            }

            if (u4val == LIFE_TIME_MINS)
            {
                value->out_int = TR_LIFE_TIME_MINS;
            }
            else if (u4val == LIFE_TIME_HOURS)
            {
                value->out_int = TR_LIFE_TIME_HOURS;
            }
            else if (u4val == LIFE_TIME_DAYS)
            {
                value->out_int = TR_LIFE_TIME_DAYS;
            }
            else
                value->out_int = u4val;

            pNode->i4IkePhase1LifeTimeType = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase1LifeTimeType ret %d\n", u4val);
            return 0;
        }
        else
        {
            value->out_int = pNode->i4IkePhase1LifeTimeType;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase1LifeTimeType tr ret %d\n",
                           value->out_int);
            return 0;
        }
    }
    else if (STRCMP (name, "PolicyFlag") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnPolicyFlag, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC, "-E- Get failed for FsVpnPolicyFlag\n");
                return -1;
            }
            value->out_int = u4val;
            pNode->i4PolicyFlag = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FsVpnPolicyFlag ret %d\n",
                           u4val);
            return 0;
        }
        else
        {
            value->out_int = pNode->i4PolicyFlag;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnPolicyFlag tr ret %d\n", value->out_int);
            return 0;
        }
    }
    else if (STRCMP (name, "PolicyPriority") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnPolicyPriority, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER32, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnPolicyPriority\n");
                return -1;
            }
            value->out_int = u4val;
            pNode->i4PolicyPriority = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnPolicyPriority ret %d\n", u4val);
            return 0;
        }
        else
        {
            value->out_int = pNode->i4PolicyPriority;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnPolicyPriority tr ret %d\n",
                           value->out_int);
            return 0;
        }
    }
    else if (STRCMP (name, "PolicyRowStatus") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnPolicyRowStatus, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnPolicyRowStatus\n");
                return -1;
            }
            if (u4val == TR_ENABLE_ROWSTATUS)
            {
                value->out_uint = 1;
                pNode->u4PolicyRowStatus = TR_ENABLE_ROWSTATUS;
            }
            else
            {
                value->out_uint = 0;
                pNode->u4PolicyRowStatus = u4val = TR_DISABLE_ROWSTATUS;
            }
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnPolicyRowStatus ret %d\n",
                           value->out_uint);
            return 0;
        }
        else
        {
            if (pNode->u4PolicyRowStatus == TR_ENABLE_ROWSTATUS)
            {
                value->out_uint = 1;
            }
            else if (pNode->u4PolicyRowStatus == TR_DISABLE_ROWSTATUS)
            {
                value->out_uint = 0;
            }
            else
            {
                value->out_uint = 0;
            }
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnPolicyRowStatus tr ret %d\n",
                           value->out_uint);
            return 0;
        }
    }
    else if (STRCMP (name, "EncrAlgo") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnEncrAlgo, 12, FsVpnTableINDEX, 1, &OctetStrIdx),
                          SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC, "-E- Get failed for FsVpnEncrAlgo\n");
                return -1;
            }
            value->out_int = u4val;
            pNode->i4EncrAlgo = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FsVpnEncrAlgo ret %d\n", u4val);
            return 0;
        }
        else
        {
            value->out_int = pNode->i4EncrAlgo;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnEncrAlgo tr ret %d\n", value->out_int);
            return 0;
        }
    }
    else if (STRCMP (name, "IkePhase1LifeTime") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnIkePhase1LifeTime, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER32, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnIkePhase1LifeTime\n");
                return -1;
            }
            value->out_int = u4val;
            pNode->i4IkePhase1LifeTime = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase1LifeTime ret %d\n", u4val);
            return 0;
        }
        else
        {
            value->out_int = pNode->i4IkePhase1LifeTime;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase1LifeTime tr ret %d\n",
                           value->out_int);
            return 0;
        }
    }
    else if (STRCMP (name, "RemoteProtectNetwork") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnRemoteProtectNetwork, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_IP_ADDR_PRIM, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnRemoteProtectNetwork\n");
                return -1;
            }
            IpAddr.u4Addr = OSIX_NTOHL (u4val);
            MEMSET (pNode->au1RemoteProtectNetwork, '\0', 16);
            pu1TempIp = UtlInetNtoa (IpAddr);
            SNPRINTF ((CHR1 *) pNode->au1RemoteProtectNetwork,
                      sizeof (pNode->au1RemoteProtectNetwork), "%s", pu1TempIp);
            value->out_cval = pNode->au1RemoteProtectNetwork;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnRemoteProtectNetwork ret %s\n",
                           value->out_cval);
            return 0;
        }
        else
        {
            value->out_cval = pNode->au1RemoteProtectNetwork;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnRemoteProtectNetwork tr ret %s\n",
                           value->out_cval);
            return 0;
        }
    }
    else if (STRCMP (name, "IkePhase2DHGroup") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnIkePhase2DHGroup, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnIkePhase2DHGroup\n");
                return -1;
            }
            value->out_int = u4val;
            pNode->i4IkePhase2DHGroup = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase2DHGroup ret %d\n", u4val);
            return 0;
        }
        else
        {
            value->out_int = pNode->i4IkePhase2DHGroup;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase2DHGroup tr ret %d\n",
                           value->out_int);
            return 0;
        }
    }
    else if (STRCMP (name, "PolicyName") == 0)
    {
        value->out_cval = pNode->au1PolicyName;
        TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FsVpnPolicyName ret %s\n",
                       value->out_cval);
        return 0;
    }
    else if (STRCMP (name, "LocalProtectSubnetMask") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnLocalProtectSubnetMask, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_IP_ADDR_PRIM, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnLocalProtectSubnetMask\n");
                return -1;
            }
            IpAddr.u4Addr = OSIX_NTOHL (u4val);
            MEMSET (pNode->au1LocalProtectSubnetMask, '\0', 16);
            pu1TempIp = UtlInetNtoa (IpAddr);
            SNPRINTF ((CHR1 *) pNode->au1LocalProtectSubnetMask,
                      sizeof (pNode->au1LocalProtectSubnetMask), "%s",
                      pu1TempIp);
            value->out_cval = pNode->au1LocalProtectSubnetMask;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnLocalProtectSubnetMask ret %s\n",
                           value->out_cval);
            return 0;
        }
        else
        {
            value->out_cval = pNode->au1LocalProtectSubnetMask;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnLocalProtectSubnetMask tr ret %s\n",
                           value->out_cval);
            return 0;
        }
    }
    else if (STRCMP (name, "RemoteTunTermAddr") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnRemoteTunTermAddr, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_IP_ADDR_PRIM, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnRemoteTunTermAddr\n");
                return -1;
            }
            IpAddr.u4Addr = OSIX_NTOHL (u4val);
            MEMSET (pNode->au1RemoteTunTermAddr, '\0', 16);
            pu1TempIp = UtlInetNtoa (IpAddr);
            SNPRINTF ((CHR1 *) pNode->au1RemoteTunTermAddr,
                      sizeof (pNode->au1RemoteTunTermAddr), "%s", pu1TempIp);
            value->out_cval = pNode->au1RemoteTunTermAddr;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnRemoteTunTermAddr ret %s\n",
                           value->out_cval);
            return 0;
        }
        else
        {
            value->out_cval = pNode->au1RemoteTunTermAddr;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnRemoteTunTermAddr ret %s\n",
                           value->out_cval);
            return 0;
        }
    }
    else if (STRCMP (name, "IkePhase1EncryptionAlgo") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnIkePhase1EncryptionAlgo, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnIkePhase1EncryptionAlgo\n");
                return -1;
            }
            value->out_int = u4val;
            pNode->i4IkePhase1EncryptionAlgo = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase1EncryptionAlgo ret %d\n",
                           value->out_int);
            return 0;
        }
        else
        {
            value->out_int = pNode->i4IkePhase1EncryptionAlgo;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase1EncryptionAlgo ret %d\n",
                           value->out_int);
            return 0;
        }
    }

    else if (STRCMP (name, "SecurityProtocol") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnSecurityProtocol, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnSecurityProtocol\n");
                return -1;
            }
            value->out_int = u4val;
            pNode->i4SecurityProtocol = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnSecurityProtocol ret %d\n", u4val);
            return 0;
        }
        else
        {
            value->out_int = pNode->i4SecurityProtocol;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnSecurityProtocol tr ret %d\n",
                           value->out_int);
            return 0;
        }
    }
    else if (STRCMP (name, "AuthAlgo") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnAuthAlgo, 12, FsVpnTableINDEX, 1, &OctetStrIdx),
                          SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC, "-E- Get failed for FsVpnAuthAlgo\n");
                return -1;
            }
            value->out_int = u4val;
            pNode->i4AuthAlgo = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FsVpnAuthAlgo ret %d\n", u4val);
            return 0;
        }
        else
        {
            value->out_int = pNode->i4AuthAlgo;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnAuthAlgo tr ret %d\n", value->out_int);
            return 0;
        }
    }
    else if (STRCMP (name, "EspKey") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnEspKey, 12, FsVpnTableINDEX, 1, &OctetStrIdx),
                          SNMP_DATA_TYPE_OCTET_PRIM, &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC, "-E- Get failed for FsVpnEspKey\n");
                return -1;
            }
            MEMSET (pNode->au1EspKey, '\0', VPN_ESP_KEY);
            MEMCPY (pNode->au1EspKey, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);

            value->out_cval = pNode->au1EspKey;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnEspKey ret %s\n", value->out_cval);
            return 0;
        }
        else
        {
            value->out_cval = pNode->au1EspKey;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnEspKey tr ret %s\n", value->out_cval);
            return 0;
        }
    }
    else if (STRCMP (name, "Mode") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnMode, 12, FsVpnTableINDEX, 1, &OctetStrIdx),
                          SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC, "-E- Get failed for FsVpnMode\n");
                return -1;
            }
            value->out_int = u4val;
            pNode->i4Mode = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FsVpnMode ret %d\n", u4val);
            return 0;
        }
        else
        {
            value->out_int = pNode->i4Mode;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnMode tr ret %d\n", value->out_int);
            return 0;
        }
    }
    else if (STRCMP (name, "AhKey") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnAhKey, 12, FsVpnTableINDEX, 1, &OctetStrIdx),
                          SNMP_DATA_TYPE_OCTET_PRIM, &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC, "-E- Get failed for FsVpnAhKey\n");
                return -1;
            }
            MEMSET (pNode->au1AhKey, '\0', VPN_AH_KEY);
            MEMCPY (pNode->au1AhKey, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);

            value->out_cval = pNode->au1AhKey;
            TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FsVpnAhKey ret %s\n",
                           value->out_cval);
            return 0;
        }
        else
        {
            value->out_cval = pNode->au1AhKey;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnAhKey tr ret %s\n", value->out_cval);
            return 0;
        }
    }
    else if (STRCMP (name, "RemoteProtectSubnetMask") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnRemoteProtectSubnetMask, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_IP_ADDR_PRIM, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnRemoteProtectSubnetMask\n");
                return -1;
            }
            IpAddr.u4Addr = OSIX_NTOHL (u4val);
            MEMSET (pNode->au1RemoteProtectSubnetMask, '\0', 16);
            pu1TempIp = UtlInetNtoa (IpAddr);
            SNPRINTF ((CHR1 *) pNode->au1RemoteProtectSubnetMask,
                      sizeof (pNode->au1RemoteProtectSubnetMask), "%s",
                      pu1TempIp);
            value->out_cval = pNode->au1RemoteProtectSubnetMask;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnRemoteProtectSubnetMask ret %s\n",
                           value->out_cval);
            return 0;
        }
        else
        {
            value->out_cval = pNode->au1RemoteProtectSubnetMask;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnRemoteProtectSubnetMask tr ret %s\n",
                           value->out_cval);
            return 0;
        }
    }
    else if (STRCMP (name, "OutboundSpi") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnOutboundSpi, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER32, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnOutboundSpi\n");
                return -1;
            }
            value->out_int = u4val;
            pNode->i4OutboundSpi = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FsVpnOutboundSpi ret %d\n",
                           u4val);
            return 0;
        }
        else
        {
            value->out_int = pNode->i4OutboundSpi;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnOutboundSpi tr ret %d\n", value->out_int);
            return 0;
        }
    }
    else if (STRCMP (name, "IkePhase1PeerIdValue") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnIkePhase1PeerIdValue, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM, &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnIkePhase1PeerIdValue\n");
                return -1;
            }
            MEMSET (pNode->au1IkePhase1PeerIdValue, '\0',
                    VPN_IKE_PHASE1_PEER_ID_VALUE);
            MEMCPY (pNode->au1IkePhase1PeerIdValue, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);

            value->out_cval = pNode->au1IkePhase1PeerIdValue;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase1PeerIdValue ret %s\n",
                           value->out_cval);
            return 0;
        }
        else
        {
            value->out_cval = pNode->au1IkePhase1PeerIdValue;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase1PeerIdValue tr ret %s\n",
                           value->out_cval);
            return 0;
        }
    }
    else if (STRCMP (name, "InboundSpi") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnInboundSpi, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER32, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC, "-E- Get failed for FsVpnInboundSpi\n");
                return -1;
            }
            value->out_int = u4val;
            pNode->i4InboundSpi = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FsVpnInboundSpi ret %d\n",
                           u4val);
            return 0;
        }
        else
        {
            value->out_int = pNode->i4InboundSpi;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnInboundSpi tr ret %d\n", value->out_int);
            return 0;
        }
    }
    else if (STRCMP (name, "IkePhase2LifeTime") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnIkePhase2LifeTime, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER32, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnIkePhase2LifeTime\n");
                return -1;
            }
            value->out_int = u4val;
            pNode->i4IkePhase2LifeTime = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase2LifeTime ret %d\n", u4val);
            return 0;
        }
        else
        {
            value->out_int = pNode->i4IkePhase2LifeTime;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase2LifeTime tr ret %d\n",
                           value->out_int);
            return 0;
        }
    }
    else if (STRCMP (name, "Protocol") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnProtocol, 12, FsVpnTableINDEX, 1, &OctetStrIdx),
                          SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC, "-E- Get failed for FsVpnProtocol\n");
                return -1;
            }
            value->out_int = u4val;
            pNode->i4Protocol = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FsVpnProtocol ret %d\n", u4val);
            return 0;
        }
        else
        {
            value->out_int = pNode->i4Protocol;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnProtocol tr ret %d\n", value->out_int);
            return 0;
        }
    }
    else if (STRCMP (name, "IkePhase2AuthAlgo") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnIkePhase2AuthAlgo, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnIkePhase2AuthAlgo\n");
                return -1;
            }
            value->out_int = u4val;
            pNode->i4IkePhase2AuthAlgo = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FsVpnIkePhase2AuthAlgo ret %d\n",
                           u4val);
            return 0;
        }
        else
        {
            value->out_int = pNode->i4IkePhase2AuthAlgo;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase2AuthAlgo tr ret %d\n",
                           value->out_int);
            return 0;
        }
    }
    else if (STRCMP (name, "IkePhase2EspEncryptionAlgo") == 0)
    {
        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnIkePhase2EspEncryptionAlgo, 12, FsVpnTableINDEX,
                           1, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnIkePhase2EspEncryptionAlgo\n");
                return -1;
            }
            value->out_int = u4val;
            pNode->i4IkePhase2EspEncryptionAlgo = u4val;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase2EspEncryptionAlgo ret %d\n",
                           u4val);
            return 0;
        }
        else
        {
            value->out_int = pNode->i4IkePhase2EspEncryptionAlgo;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase2EspEncryptionAlgo tr ret %d\n",
                           value->out_int);
            return 0;
        }
    }
    else if (STRCMP (name, "IpComp") == 0)
    {
        value->out_int = pNode->i4IpComp = 0;
        TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FsVpnIpComp ret %d\n",
                       pNode->i4IpComp);
        return 0;
    }
    else if (STRCMP (name, "IkeRAInfoProtectedNetBundle") == 0)
    {

        STRCPY (pNode->au1IkeRAInfoProtectedNetBundle, "Not Supported");
        value->out_cval = pNode->au1IkeRAInfoProtectedNetBundle;
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "Get: FsVpnIkeRAInfoProtectedNetBundle ret %s\n",
                       pNode->au1IkeRAInfoProtectedNetBundle);
        return 0;
    }
    else if (STRCMP (name, "IkePhase1LocalIdType") == 0)
    {
        if ((pNode->u4ValMask & VPN_IKE_VAL_MASK) == VPN_IKE_VAL_MASK)
        {
            i4rc =
                MynmhGet (*Join
                          (FsVpnIkePhase1LocalIdType, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnIkePhase1LocalIdType\n");
                return -1;
            }
            if (u4val == 1)
            {
                MEMSET (pNode->au1IkePhase1LocalIdType, '\0',
                        VPN_IKE_PHASE1_LOCAL_ID_TYPE);
                MEMCPY (pNode->au1IkePhase1LocalIdType, "IPV4", 4);
            }
            else if (u4val == 2)
            {
                MEMSET (pNode->au1IkePhase1LocalIdType, '\0',
                        VPN_IKE_PHASE1_LOCAL_ID_TYPE);
                MEMCPY (pNode->au1IkePhase1LocalIdType, "FQDN", 4);
            }
            else if (u4val == 3)
            {
                MEMSET (pNode->au1IkePhase1LocalIdType, '\0',
                        VPN_IKE_PHASE1_LOCAL_ID_TYPE);
                MEMCPY (pNode->au1IkePhase1LocalIdType, "EMAIL", 5);
            }
            else if (u4val == 11)
            {
                MEMSET (pNode->au1IkePhase1LocalIdType, '\0',
                        VPN_IKE_PHASE1_LOCAL_ID_TYPE);
                MEMCPY (pNode->au1IkePhase1LocalIdType, "KEYID", 5);
            }

            value->out_cval = pNode->au1IkePhase1LocalIdType;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase1LocalIdType ret %s\n",
                           pNode->au1IkePhase1LocalIdType);
            return 0;
        }

        else
        {
            value->out_cval = pNode->au1IkePhase1LocalIdType;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase1LocalIdType ret %s\n",
                           pNode->au1IkePhase1LocalIdType);
            return 0;
        }

    }
    else if (STRCMP (name, "IkePhase1LocalIdValue") == 0)
    {
        if ((pNode->u4ValMask & VPN_IKE_VAL_MASK) == VPN_IKE_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnIkePhase1LocalIdValue, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM, &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnIkePhase1LocalIdValue\n");
                return -1;
            }
            MEMSET (pNode->au1IkePhase1LocalIdValue, '\0',
                    VPN_IKE_PHASE1_LOCAL_ID_VALUE);
            MEMCPY (pNode->au1IkePhase1LocalIdValue, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);

            value->out_cval = pNode->au1IkePhase1LocalIdValue;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase1LocalIdValue ret %s\n",
                           value->out_cval);
            return 0;
        }
        else
        {
            value->out_cval = pNode->au1IkePhase1LocalIdValue;
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "Get: FsVpnIkePhase1LocalIdValue ret %s\n",
                           value->out_cval);
            return 0;
        }

    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrGetInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - SUCCESS\n");
    return i4rc;
}

/************************************************************************
 *  Function Name   : InternetGatewayDevice_RAVPN_User
 *  Description     : Get function for InternetGatewayDevice_RAVPN_User
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int
TrGetInternetGatewayDevice_RAVPN_User (char *name, int idx1,
                                       ParameterValue * value)
{
    UINT4               u4val = 0;
    INT4                i4rc = 0;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    /* INDICES: */
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    UINT4              *ptr = NULL;
    UINT4               u4Size = 0;

    tUser              *pUser = NULL;
    tUser              *pNextUser = NULL;
    tUser              *pNode = NULL;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrGetInternetGatewayDevice_RAVPN_User\n");
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    OctetStr.pu1_OctetList = au1OctetList;
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;

    UTL_SLL_OFFSET_SCAN (&UserList, pUser, pNextUser, tUser *)
    {
        if (pUser->u4TrInstance == (UINT4) idx1)
        {
            /* TO CHECK */
            OctetStrIdx.i4_Length = STRLEN (pUser->au1Name);
            MEMCPY (OctetStrIdx.pu1_OctetList, pUser->au1Name,
                    OctetStrIdx.i4_Length);
            if (((pUser->u4ValMask) & (USER_VAL_MASK)) == (USER_VAL_MASK))
            {
                if (nmhValidateIndexInstanceFsVpnRaUsersTable (&OctetStrIdx) !=
                    SNMP_SUCCESS)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E- nmhValidateIndexInstanceFsVpnRaUsersTable - Failed \r\n");
                    ptr = &(pUser->u4ValMask);
                    u4Size =
                        sizeof (tUser) - (2 * sizeof (UINT4) +
                                          sizeof (tTMO_SLL_NODE));
                    MEMSET (ptr, 0, u4Size);
                }
            }
            break;
        }
    }

    if (pUser == NULL)
    {

        TR69_TRC (TR69_DBG_TRC, "UserNode not found in User List\n");
        return -1;
    }

    pNode = pUser;

    if (STRCMP (name, "RowStatus") == 0)
    {
        if (((pNode->u4ValMask) & (USER_VAL_MASK)) == (USER_VAL_MASK))
        {
            i4rc =
                MynmhGet (*Join
                          (FsVpnRaUserRowStatus, 12, FsVpnRaUsersTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "-E- Get failed for FsVpnRaUserRowStatus for Instance  %d\n",
                               idx1);
                value->out_uint = 0;
                return OK;
            }
            if (u4val == TR_DISABLE_ROWSTATUS)
            {
                value->out_uint = 0;
                pNode->u4RowStatus = TR_DISABLE_ROWSTATUS;
            }
            else
            {
                value->out_uint = 1;
                pNode->u4RowStatus = TR_ENABLE_ROWSTATUS;
            }
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "Get: FsVpnRaUserRowStatus for Instance %d - %d\n",
                           idx1, u4val);
            return 0;
        }
        else
        {
            value->out_uint = pNode->u4RowStatus;
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "Get: FsVpnRaUserRowStatus for Tr Instance %d - %d\n",
                           idx1, value->out_uint);
            return 0;
        }
    }
    else if (STRCMP (name, "Name") == 0)
    {
        value->out_cval = pNode->au1Name;
        TR69_TRC_ARG2 (TR69_DBG_TRC,
                       "Get: FsVpnRaUserName for Instance %d - %s\n", idx1,
                       value->out_cval);
        return 0;
    }
    else if (STRCMP (name, "Secret") == 0)
    {
        if (((pNode->u4ValMask) & (USER_VAL_MASK)) == (USER_VAL_MASK))
        {
            i4rc =
                MynmhGet (*Join
                          (FsVpnRaUserSecret, 12, FsVpnRaUsersTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM, &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "-E- Get failed for FsVpnRaUserSecret for Instance  %d\n",
                               idx1);
                MEMSET (pNode->au1Secret, '\0', USER_SECRET);
                value->out_cval = pNode->au1Secret;
                return OK;
            }

            MEMSET (pNode->au1Secret, '\0', USER_SECRET);
            MEMCPY (pNode->au1Secret, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);

            value->out_cval = pNode->au1Secret;
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "Get: FsVpnRaUserName for Instance %d - %s\n", idx1,
                           value->out_cval);
            return 0;
        }
        else
        {
            value->out_cval = pNode->au1Secret;
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "Get: FsVpnRaUserName for Tr Instance %d - %s\n",
                           idx1, value->out_cval);
            return 0;
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrGetInternetGatewayDevice_RAVPN_User\n");

    return OK;
}

/************************************************************************
 *  Function Name   : InternetGatewayDevice_RAVPN_AddressPool
 *  Description     : Get function for InternetGatewayDevice_RAVPN_AddressPool
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int
TrGetInternetGatewayDevice_RAVPN_AddressPool (char *name, int idx1,
                                              ParameterValue * value)
{

    UINT4               u4val = 0;
    INT4                i4rc = 0;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    /* INDICES: */
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    UINT4              *ptr = NULL;
    UINT4               u4Size = 0;

    tAddressPool       *pAddressPool = NULL;
    tAddressPool       *pNextAddressPool = NULL;
    tAddressPool       *pNode = NULL;
    CHR1               *pu1TempIp = NULL;
    tUtlInAddr          IpAddr;
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrGetInternetGatewayDevice_RAVPN_AddressPool\n");
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    OctetStr.pu1_OctetList = au1OctetList;
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;

    UTL_SLL_OFFSET_SCAN (&AddressPoolList, pAddressPool, pNextAddressPool,
                         tAddressPool *)
    {
        if (pAddressPool->u4TrInstance == (UINT4) idx1)
        {
            /* TO CHECK */
            OctetStrIdx.i4_Length = STRLEN (pAddressPool->au1Name);
            MEMCPY (OctetStrIdx.pu1_OctetList, pAddressPool->au1Name,
                    OctetStrIdx.i4_Length);
            if (((pAddressPool->u4ValMask) & (ADDRESS_POOL_VAL_MASK)) ==
                (ADDRESS_POOL_VAL_MASK))
            {
                if (nmhValidateIndexInstanceFsVpnRaAddressPoolTable
                    (&OctetStrIdx) != SNMP_SUCCESS)
                {

                    TR69_TRC (TR69_DBG_TRC,
                              "-E- nmhValidateIndexInstanceFsVpnRaAddressPoolTable - Failed \r\n");
                    ptr = &(pAddressPool->u4ValMask);
                    u4Size =
                        sizeof (tAddressPool) - (2 * sizeof (UINT4) +
                                                 sizeof (tTMO_SLL_NODE));
                    MEMSET (ptr, 0, u4Size);

                }
            }

            break;
        }
    }

    if (pAddressPool == NULL)
    {
        TR69_TRC (TR69_DBG_TRC,
                  "-E- Get: AddressPoolNode not found in AddressPool List\n");
        return -1;
    }

    pNode = pAddressPool;

    if (STRCMP (name, "EndAddress") == 0)
    {
        if ((pNode->u4ValMask & ADDRESS_POOL_VAL_MASK) == ADDRESS_POOL_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnRaAddressPoolEnd, 12,
                           FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                          SNMP_DATA_TYPE_IP_ADDR_PRIM, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "-E- Get failed for FsVpnRaAddressPoolEnd for Instance  %d\n",
                               idx1);
                MEMSET (pNode->au1EndAddress, '\0', ADDRESSPOOL_END_ADDRESS);
                value->out_cval = pNode->au1EndAddress;
                return OK;
            }

            IpAddr.u4Addr = OSIX_NTOHL (u4val);
            MEMSET (pNode->au1EndAddress, '\0', 16);
            pu1TempIp = UtlInetNtoa (IpAddr);
            SNPRINTF ((CHR1 *) pNode->au1EndAddress,
                      sizeof (pNode->au1EndAddress), "%s", pu1TempIp);
            value->out_cval = pNode->au1EndAddress;
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "Get: FsVpnRaAddressPoolEnd for Instance %d - %s\n",
                           idx1, value->out_cval);
            return 0;
        }
        else
        {
            value->out_cval = pNode->au1EndAddress;
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "Get: FsVpnRaAddressPoolEnd for Tr Instance %d - %s\n",
                           idx1, value->out_cval);
            return 0;
        }
    }
    else if (STRCMP (name, "StartAddress") == 0)
    {
        if ((pNode->u4ValMask & ADDRESS_POOL_VAL_MASK) == ADDRESS_POOL_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnRaAddressPoolStart, 12,
                           FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                          SNMP_DATA_TYPE_IP_ADDR_PRIM, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "-E- Get failed for FsVpnRaAddressPoolStart for Instance  %d\n",
                               idx1);
                MEMSET (pNode->au1StartAddress, '\0',
                        ADDRESSPOOL_START_ADDRESS);
                value->out_cval = pNode->au1StartAddress;
                return OK;
            }
            IpAddr.u4Addr = OSIX_NTOHL (u4val);
            MEMSET (pNode->au1StartAddress, '\0', 16);
            pu1TempIp = UtlInetNtoa (IpAddr);
            SNPRINTF ((CHR1 *) pNode->au1StartAddress,
                      sizeof (pNode->au1StartAddress), "%s", pu1TempIp);
            value->out_cval = pNode->au1StartAddress;
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "Get: FsVpnRaAddressPoolStart for Instance %d - %s\n",
                           idx1, value->out_cval);
            return 0;
        }
        else
        {
            value->out_cval = pNode->au1StartAddress;
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "Get: FsVpnRaAddressPoolStart for Tr Instance %d - %s\n",
                           idx1, value->out_cval);
            return 0;
        }
    }
    else if (STRCMP (name, "RowStatus") == 0)
    {
        if ((pNode->u4ValMask & ADDRESS_POOL_VAL_MASK) == ADDRESS_POOL_VAL_MASK)
        {

            i4rc =
                MynmhGet (*Join
                          (FsVpnRaAddressPoolRowStatus, 12,
                           FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                          SNMP_DATA_TYPE_INTEGER, &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "-E- Get failed for FsVpnRaAddressPoolRowStatus for Instance  %d\n",
                               idx1);
                value->out_uint = 0;
                return OK;
            }
            if (u4val == TR_DISABLE_ROWSTATUS)
            {
                value->out_uint = 0;
                pNode->u4RowStatus = TR_DISABLE_ROWSTATUS;
            }
            else
            {
                value->out_uint = 1;
                pNode->u4RowStatus = TR_ENABLE_ROWSTATUS;
            }
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "Get: FsVpnRaAddressPoolRowStatus for Instance %d - %d\n",
                           idx1, value->out_uint);
            return 0;
        }
        else
        {
            value->out_uint = pNode->u4RowStatus;
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "Get: FsVpnRaAddressPoolRowStatus for Tr Instance %d - %d\n",
                           idx1, value->out_uint);
            return 0;
        }
    }
    else if (STRCMP (name, "Name") == 0)
    {
        value->out_cval = pNode->au1Name;
        TR69_TRC_ARG2 (TR69_DBG_TRC,
                       "Get: FsVpnRaAddressPoolName for Instance %d - %s\n",
                       idx1, value->out_cval);
        return 0;
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrGetInternetGatewayDevice_RAVPN_AddressPool\n");
    return OK;
}

/************************************************************************
 *  Function Name   : InternetGatewayDevice_VpnRemoteId
 *  Description     : Get function for InternetGatewayDevice_VpnRemoteId
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int
TrGetInternetGatewayDevice_VpnRemoteId (char *name, int idx1,
                                        ParameterValue * value)
{

    UINT4               u4val = 0;
    INT4                i4rc = 0;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    /* INDICES: */
    UINT4               u4Idx0 = 0;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    UINT4              *ptr = NULL;
    UINT4               u4Size = 0;

    tVpnRemoteId       *pVpnRemoteId = NULL;
    tVpnRemoteId       *pNextVpnRemoteId = NULL;
    tVpnRemoteId       *pNode = NULL;

    TR69_TRC (TR69_DBG_TRC, "TrGetInternetGatewayDevice_RAVPN_VpnRemoteId\n");
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    OctetStr.pu1_OctetList = au1OctetList;
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;

    UTL_SLL_OFFSET_SCAN (&VpnRemoteIdList, pVpnRemoteId, pNextVpnRemoteId,
                         tVpnRemoteId *)
    {
        if (pVpnRemoteId->u4TrInstance == (UINT4) idx1)
        {
            OctetStrIdx.i4_Length = STRLEN (pVpnRemoteId->au1Value);
            MEMCPY (OctetStrIdx.pu1_OctetList, pVpnRemoteId->au1Value,
                    OctetStrIdx.i4_Length);
            u4Idx0 = (UINT4) pVpnRemoteId->i4Type;

            if (((pVpnRemoteId->u4ValMask) & (VPN_REMOTE_ID_VAL_MASK)) ==
                (VPN_REMOTE_ID_VAL_MASK))
            {
                if (nmhValidateIndexInstanceFsVpnRemoteIdTable
                    (u4Idx0, &OctetStrIdx) != SNMP_SUCCESS)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E- nmhValidateIndexInstanceFsVpnRemoteIdTable - Failed \r\n");
                    ptr = &(pVpnRemoteId->u4ValMask);
                    u4Size =
                        sizeof (tVpnRemoteId) - (2 * sizeof (UINT4) +
                                                 sizeof (tTMO_SLL_NODE));
                    MEMSET (ptr, 0, u4Size);
                }
            }
            break;
        }

    }

    if (pVpnRemoteId == NULL)
    {
        TR69_TRC (TR69_DBG_TRC,
                  "-E- Get: VpnRemoteID Node not found in AddressPool List\n");
        return -1;
    }

    pNode = pVpnRemoteId;

    if (STRCMP (name, "Value") == 0)
    {
        MEMCPY (pNode->au1Value, OctetStr.pu1_OctetList, OctetStr.i4_Length);

        value->out_cval = pNode->au1Value;
        TR69_TRC_ARG2 (TR69_DBG_TRC,
                       "Get: FsVpnRemoteIdValue for Tr Instance %d - %s\n",
                       idx1, value->out_cval);
        return 0;
    }
    else if (STRCMP (name, "Status") == 0)
    {
        if ((pNode->u4ValMask & VPN_REMOTE_ID_VAL_MASK) ==
            VPN_REMOTE_ID_VAL_MASK)
        {
            i4rc =
                MynmhGet (*Join
                          (FsVpnRemoteIdStatus, 12, FsVpnRemoteIdTableINDEX, 2,
                           u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          &u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnRemoteIdStatus\n");
                return -1;
            }
            if (u4val == TR_DISABLE_ROWSTATUS)
            {
                value->out_uint = 0;
                pNode->u4Status = TR_DISABLE_ROWSTATUS;
            }
            else
            {
                value->out_uint = 1;
                pNode->u4Status = TR_ENABLE_ROWSTATUS;
            }
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "Get: FsVpnRemoteIdStatus for Instance %d - %d\n",
                           idx1, value->out_uint);
            return 0;
        }
        else
        {
            value->out_uint = pNode->u4Status;
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "Get: FsVpnRemoteIdStatus for Tr Instance %d - %d\n",
                           idx1, value->out_uint);
            return 0;

        }

    }
    else if (STRCMP (name, "Type") == 0)
    {

        if (pNode->i4Type == 1)
        {
            MEMSET (pNode->au1Type, '\0', VPNREMOTEID_TYPE);
            MEMCPY (pNode->au1Type, "IPV4", 4);
        }
        else if (pNode->i4Type == 2)
        {
            MEMSET (pNode->au1Type, '\0', VPNREMOTEID_TYPE);
            MEMCPY (pNode->au1Type, "FQDN", 4);
        }
        else if (pNode->i4Type == 3)
        {
            MEMSET (pNode->au1Type, '\0', VPNREMOTEID_TYPE);
            MEMCPY (pNode->au1Type, "EMAIL", 5);
        }
        else if (pNode->i4Type == 11)
        {
            MEMSET (pNode->au1Type, '\0', VPNREMOTEID_TYPE);
            MEMCPY (pNode->au1Type, "KEYID", 5);
        }

        value->out_cval = pNode->au1Type;
        TR69_TRC_ARG2 (TR69_DBG_TRC,
                       "Get: FsVpnRemoteIdType for Instance %d - %s\n",
                       idx1, value->out_cval);
        return 0;
    }
    else if (STRCMP (name, "Key") == 0)
    {
        if ((pNode->u4ValMask & VPN_REMOTE_ID_VAL_MASK) ==
            VPN_REMOTE_ID_VAL_MASK)
        {
            i4rc =
                MynmhGet (*Join
                          (FsVpnRemoteIdKey, 12, FsVpnRemoteIdTableINDEX, 2,
                           u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                          &OctetStr);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- Get failed for FsVpnRemoteIdKey\n");
                return -1;
            }
            MEMSET (pNode->au1Key, '\0', VPNREMOTEID_KEY);
            MEMCPY (pNode->au1Key, OctetStr.pu1_OctetList, OctetStr.i4_Length);

            value->out_cval = pNode->au1Key;
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "Get: FsVpnRemoteIdKey for Instance %d - %s\n",
                           idx1, value->out_cval);
            return 0;
        }
        else
        {
            value->out_cval = pNode->au1Key;
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "Get: FsVpnRemoteIdKey for Tr Instance %d - %s\n",
                           idx1, value->out_cval);
            return 0;
        }
    }
    return i4rc;
}

/************************************************************************
 *  Function Name   : InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
 *  Description     : Get function for InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int  
     
     
     
     
     
     
     
    TrSetInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
    (char *name, int idx1, int idx2, int idx3, int idx4, ParameterValue * value)
{
    UINT4               u4val = 0;
    UINT4               u4MakeActive = 0;
    INT4                i4rc = 0;
    INT4                i4rc2 = 0;
    INT4                i4IfMainWanType = 0;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    /* INDICES: */
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];

    tVPN               *pVPN = NULL;
    tVPN               *pNextVPN = NULL;
    tVPN               *pNode = NULL;
    tWANDevice         *pWANDevice = NULL;
    tWANDevice         *pNextWANDevice = NULL;
    tTMO_SLL           *pVPNList = NULL;
#ifdef TR69MSR_WANTED
    UINT1               u1Tr69MsrStatus = 0;
#endif

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN\n");

    UNUSED_PARAM (idx2);
    UNUSED_PARAM (idx3);

    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    OctetStr.pu1_OctetList = au1OctetList;
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;

    /* Get the parent WAN Device entry */
    UTL_SLL_OFFSET_SCAN (&WANDeviceList, pWANDevice, pNextWANDevice,
                         tWANDevice *)
    {
        if (pWANDevice->u4TrInstance == (UINT4) idx1)
        {
            break;
        }
    }
    if (pWANDevice == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "-E- Set: WANDevice not found \r\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrSetInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
        return SNMP_FAILURE;
    }

    pVPNList = &VPNList;
    UTL_SLL_OFFSET_SCAN (pVPNList, pVPN, pNextVPN, tVPN *)
    {
        if (pVPN->u4TrInstance == (UINT4) idx4)
        {
            /* Check with the interface index of the Policy with
             * that of the parent WAN Device instatnce
             */
            if ((UINT4) (pVPN->i4PolicyIntfIndex) == pWANDevice->u4IfIdx)
            {
                OctetStrIdx.i4_Length = STRLEN (pVPN->au1PolicyName);
                MEMCPY (OctetStrIdx.pu1_OctetList, pVPN->au1PolicyName,
                        OctetStrIdx.i4_Length);
                break;
            }
            else
            {
                pVPN = NULL;
            }
        }
    }

    if (pVPN == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "-E- Set: VPN Policy not found \r\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrSetInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
        return SNMP_FAILURE;
    }

    if (SecUtilValidateIfIndex (pWANDevice->u4IfIdx) != CFA_SUCCESS)
    {
        i4rc = TrSetNonWANDevice_vpn (name);
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrSetInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
        return i4rc;
    }

    MynmhGet (*Join
              (IfMainWanType, 12, IfMainTableINDEX, 1, pWANDevice->u4IfIdx),
              SNMP_DATA_TYPE_INTEGER, &i4IfMainWanType);

    if (i4IfMainWanType != CFA_WAN_TYPE_PUBLIC)
    {
        i4rc = TrSetNonWANDevice_vpn (name);
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrSetInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
        return i4rc;
    }

    pNode = pVPN;

#ifdef TR69MSR_WANTED
    Tr69MsrGetRestoreStatus (&u1Tr69MsrStatus);
    if (u1Tr69MsrStatus == OSIX_TRUE)
    {
        /*During Restoration, when first time Set request comes,
           the policy type  will be zero and normal code flow will
           be followed. If policy type is non-zero depending upon 
           the parameter name and policy type, values will be 
           cached in pNode. */
        if (pNode->i4PolicyType != 0)
        {
            i4Retval = Tr69MsrSetVpn (name, pNode);
            if (i4Retval == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            else
            {
                /* Check is made aganist the ValMask to differentate aganist 
                   a fully created row and the row which is in the process of 
                   creation. When restoration happens via castr69walk.conf 
                   i.e row under creation, set of statements inside the if 
                   statement will be executed. When restoration happens via
                   castr69.conf i.e already created row, noraml code flow 
                   will be followed. */
                if ((pNode->u4ValMask & pNode->u4GlobalValMask) !=
                    pNode->u4GlobalValMask)
                {
                    i4Retval =
                        TrSetMand_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
                        (name, value, pNode);
                    return i4Retval;
                }
            }

        }
    }
#endif

    u4MakeActive = 0;
    if ((pNode->u4ValMask & pNode->u4GlobalValMask) == pNode->u4GlobalValMask)
    {
        u4val = NOT_IN_SERVICE;

        i4rc =
            MynmhTest (*Join
                       (FsVpnPolicyRowStatus, 12, FsVpnTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSet: Test failed for FsVpnPolicyRowStatus NOT_IN_SERVICE\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
            return SNMP_FAILURE;
        }

        i4rc = MynmhSet (*Join (FsVpnPolicyRowStatus, 12, FsVpnTableINDEX, 1,
                                &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                         (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSet: Set failed for FsVpnPolicyRowStatus NOT_IN_SERVICE\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
            return SNMP_FAILURE;
        }
        /* Setting the row status to NOT_IN_SERVICE */
        /*pNode->u4PolicyRowStatus = u4val; */
        u4MakeActive = 1;
    }
    i4rc = TR_NOT_MAND_PARAM;
    i4rc2 =
        TrSetMand_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
        (name, value, pNode);

    if ((u4MakeActive) && (strcmp (name, "PolicyRowStatus") != 0)
        && (pNode->u4PolicyRowStatus == ACTIVE))
    {
        u4val = ACTIVE;

        OctetStrIdx.i4_Length = STRLEN (pVPN->au1PolicyName);
        MEMCPY (OctetStrIdx.pu1_OctetList, pVPN->au1PolicyName,
                OctetStrIdx.i4_Length);

        i4rc =
            MynmhTest (*Join
                       (FsVpnPolicyRowStatus, 12, FsVpnTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSet: Test failed for FsVpnPolicyRowStatus ACTIVE\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
            return SNMP_FAILURE;
        }

        i4rc = MynmhSet (*Join (FsVpnPolicyRowStatus, 12, FsVpnTableINDEX, 1,
                                &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                         (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSet: Set failed for FsVpnPolicyRowStatus ACTIVE\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
            return SNMP_FAILURE;
        }

        /* Setting the rowstatus  back to ACTIVE */
        pNode->u4PolicyRowStatus = u4val;
    }
    if (i4rc2 == SNMP_FAILURE)
    {
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrSetInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
        return SNMP_FAILURE;
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - SUCCESS\n");

    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : InternetGatewayDevice_RAVPN_User
 *  Description     : Get function for InternetGatewayDevice_RAVPN_User
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int
TrSetInternetGatewayDevice_RAVPN_User (char *name, int idx1,
                                       ParameterValue * value)
{

    UINT4               u4val = 0;
    UINT4               u4MakeActive = 0;
    INT4                i4rc = 0;
    INT4                i4rc2 = 0;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    /* INDICES: */
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];

    tUser              *pUser = NULL;
    tUser              *pNextUser = NULL;
    tUser              *pNode = NULL;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetInternetGatewayDevice_RAVPN_User\n");
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    OctetStr.pu1_OctetList = au1OctetList;
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;

    UTL_SLL_OFFSET_SCAN (&UserList, pUser, pNextUser, tUser *)
    {
        if (pUser->u4TrInstance == (UINT4) idx1)
        {
            /* TO CHECK */
            OctetStrIdx.i4_Length = STRLEN (pUser->au1Name);
            MEMCPY (OctetStrIdx.pu1_OctetList, pUser->au1Name,
                    OctetStrIdx.i4_Length);
            if (pUser->u4ValMask == (USER_VAL_MASK))
            {
                if (nmhValidateIndexInstanceFsVpnRaUsersTable (&OctetStrIdx) !=
                    SNMP_SUCCESS)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E- Set: nmhValidateIndexInstanceFsVpnRaUsersTable in set - Failed \r\n");
                    value->out_int = 0;
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrSetInternetGatewayDevice_RAVPN_User - FAILURE\n");

                    return SNMP_FAILURE;
                }
            }
            break;
        }
    }

    if (pUser == NULL)
    {
        TR69_TRC (TR69_DBG_TRC, "-E- Set: UserNode not found in User List\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrSetInternetGatewayDevice_RAVPN_User - FAILURE\n");

        return SNMP_FAILURE;
    }

    pNode = pUser;

    u4MakeActive = 0;
    if (((pNode->u4ValMask & (USER_VAL_MASK)) ==
         (USER_VAL_MASK)) && (strcmp (name, "RowStatus") != 0))
    {
        u4val = NOT_IN_SERVICE;
        i4rc =
            MynmhTest (*Join
                       (FsVpnRaUserRowStatus, 12, FsVpnRaUsersTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);

        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSet: Test failed for RaVpnUserRowStatus NOT_IN_SERVICE\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetInternetGatewayDevice_RAVPN_User - FAILURE\n");

            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnRaUserRowStatus, 12, FsVpnRaUsersTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSet: Test failed for RaVpnUserRowStatus As NOT_IN_SERVICE\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetInternetGatewayDevice_RAVPN_User - FAILURE\n");

            return SNMP_FAILURE;
        }
        u4MakeActive = 1;
    }
    i4rc2 = TrSetNonMand_InternetGatewayDevice_RAVPN_User (name, value, pNode);
    if (i4rc2 == TR_NOT_MAND_PARAM)
    {
        i4rc2 = TrSetMand_InternetGatewayDevice_RAVPN_User (name, value, pNode);
    }

    if ((u4MakeActive) && (strcmp (name, "RowStatus") != 0)
        && (pNode->u4RowStatus == ACTIVE))
    {
        /* TOCHECK: Set RowStatus back to ACTIVE */
        u4val = ACTIVE;
        OctetStrIdx.i4_Length = STRLEN (pNode->au1Name);
        MEMCPY (OctetStrIdx.pu1_OctetList,
                pNode->au1Name, OctetStrIdx.i4_Length);
        i4rc =
            MynmhTest (*Join
                       (FsVpnRaUserRowStatus, 12, FsVpnRaUsersTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSet: Test failed for FsVpnRaUserRowStatus As ACTIVE\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetInternetGatewayDevice_RAVPN_User - FAILURE\n");

            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnRaUserRowStatus, 12, FsVpnRaUsersTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSet: Set failed for FsVpnRaUserRowStatus As ACTIVE\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetInternetGatewayDevice_RAVPN_User - FAILURE\n");

            return SNMP_FAILURE;
        }
    }
    if (i4rc2 == SNMP_FAILURE)
    {
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrSetInternetGatewayDevice_RAVPN_User - FAILURE\n");

        return SNMP_FAILURE;
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetInternetGatewayDevice_RAVPN_User - SUCCESS\n");

    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : InternetGatewayDevice_RAVPN_AddressPool
 *  Description     : Get function for InternetGatewayDevice_RAVPN_AddressPool
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int
TrSetInternetGatewayDevice_RAVPN_AddressPool (char *name, int idx1,
                                              ParameterValue * value)
{
    UINT4               u4val = 0;
    UINT4               u4MakeActive = 0;
    INT4                i4rc = 0;
    INT4                i4rc2 = 0;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    /* INDICES: */
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];

    tAddressPool       *pAddressPool = NULL;
    tAddressPool       *pNextAddressPool = NULL;
    tAddressPool       *pNode = NULL;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetInternetGatewayDevice_RAVPN_AddressPool\n");
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    OctetStr.pu1_OctetList = au1OctetList;
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;

    UTL_SLL_OFFSET_SCAN (&AddressPoolList, pAddressPool, pNextAddressPool,
                         tAddressPool *)
    {

        if (pAddressPool->u4TrInstance == (UINT4) idx1)
        {
            /* TO CHECK */
            OctetStrIdx.i4_Length = STRLEN (pAddressPool->au1Name);
            MEMCPY (OctetStrIdx.pu1_OctetList, pAddressPool->au1Name,
                    OctetStrIdx.i4_Length);
            if (pAddressPool->u4ValMask == (ADDRESS_POOL_VAL_MASK))
            {
                if (nmhValidateIndexInstanceFsVpnRaAddressPoolTable
                    (&OctetStrIdx) != SNMP_SUCCESS)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E- Set: nmhValidateIndexInstanceFsVpnRaAddressPoolTable in set - Failed \r\n");
                    value->out_int = 0;
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrSetInternetGatewayDevice_RAVPN_AddressPool - FAILURE\n");

                    return SNMP_FAILURE;
                }
            }
            break;
        }
    }

    if (pAddressPool == NULL)
    {
        return SNMP_FAILURE;
    }

    pNode = pAddressPool;

    u4MakeActive = 0;
    if (((pNode->u4ValMask & ADDRESS_POOL_VAL_MASK) == ADDRESS_POOL_VAL_MASK) &&
        (strcmp (name, "RowStatus") != 0))
    {
        /* TOCHECK: Set RowStatus to NOT_IN_SERVICE */
        u4val = NOT_IN_SERVICE;
        i4rc =
            MynmhTest (*Join
                       (FsVpnRaAddressPoolRowStatus, 12,
                        FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                       SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSet: Test failed for FsVpnRaAddressPoolRowStatus NOT_IN_SERVICE\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetInternetGatewayDevice_RAVPN_AddressPool - FAILURE\n");

            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnRaAddressPoolRowStatus, 12,
                       FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSet: Set failed for FsVpnRaAddressPoolRowStatus NOT_IN_SERVICE\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetInternetGatewayDevice_RAVPN_AddressPool - FAILURE\n");

            return SNMP_FAILURE;
        }

        u4MakeActive = 1;
    }
    i4rc2 =
        TrSetNonMand_InternetGatewayDevice_RAVPN_AddressPool (name, value,
                                                              pNode);
    if (i4rc2 == TR_NOT_MAND_PARAM)
    {
        i4rc2 =
            TrSetMand_InternetGatewayDevice_RAVPN_AddressPool (name, value,
                                                               pNode);
    }
    if ((u4MakeActive) && (strcmp (name, "RowStatus") != 0)
        && (pNode->u4RowStatus == ACTIVE))
    {
        /* TOCHECK: Set RowStatus to ACTIVE */
        u4val = ACTIVE;
        MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
        OctetStrIdx.i4_Length = STRLEN (pNode->au1Name);
        MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1Name,
                OctetStrIdx.i4_Length);
        i4rc =
            MynmhTest (*Join
                       (FsVpnRaAddressPoolRowStatus, 12,
                        FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                       SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSet: Test failed for FsVpnRaAddressPoolRowStatus ACTIVE\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetInternetGatewayDevice_RAVPN_AddressPool - FAILURE\n");

            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnRaAddressPoolRowStatus, 12,
                       FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSet: Set failed for FsVpnRaAddressPoolRowStatus ACTIVE\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetInternetGatewayDevice_RAVPN_AddressPool - FAILURE\n");

            return SNMP_FAILURE;
        }
    }
    if (i4rc2 == SNMP_FAILURE)
    {
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrSetInternetGatewayDevice_RAVPN_AddressPool - FAILURE\n");

        return SNMP_FAILURE;
    }

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetInternetGatewayDevice_RAVPN_AddressPool - SUCCESS\n");

    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : InternetGatewayDevice_VpnRemoteId
 *  Description     : Get function for InternetGatewayDevice_VpnRemoteId
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    idx1,... - list of TR indices.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
int
TrSetInternetGatewayDevice_VpnRemoteId (char *name, int idx1,
                                        ParameterValue * value)
{
    UINT4               u4val = 0;
    UINT4               u4MakeActive = 0;
    INT4                i4rc = 0;
    INT4                i4rc2 = 0;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    /* INDICES: */
    UINT4               u4Idx0 = 0;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];

    tVpnRemoteId       *pVpnRemoteId = NULL;
    tVpnRemoteId       *pNextVpnRemoteId = NULL;
    tVpnRemoteId       *pNode = NULL;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetInternetGatewayDevice_VpnRemoteId\n");
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    OctetStr.pu1_OctetList = au1OctetList;
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;

    UTL_SLL_OFFSET_SCAN (&VpnRemoteIdList, pVpnRemoteId, pNextVpnRemoteId,
                         tVpnRemoteId *)
    {
        if (pVpnRemoteId->u4TrInstance == (UINT4) idx1)
        {
            OctetStrIdx.i4_Length = STRLEN (pVpnRemoteId->au1Value);
            MEMCPY (OctetStrIdx.pu1_OctetList, pVpnRemoteId->au1Value,
                    OctetStrIdx.i4_Length);
            u4Idx0 = (UINT4) pVpnRemoteId->i4Type;

            if (pVpnRemoteId->u4ValMask == (VPN_REMOTE_ID_VAL_MASK))
            {
                if (nmhValidateIndexInstanceFsVpnRemoteIdTable (u4Idx0,
                                                                &OctetStrIdx) !=
                    SNMP_SUCCESS)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E- Set: nmhValidateIndexInstanceFsVpnRemoteIdTable in set - Failed \r\n");
                    value->out_int = 0;
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrSetInternetGatewayDevice_VpnRemoteId - FAILURE\n");

                    return SNMP_FAILURE;
                }
            }
            break;
        }
    }

    if (pVpnRemoteId == NULL)
    {
        TR69_TRC (TR69_DBG_TRC,
                  "-E- Set: VpnRemoteIdNode not found in RemoteId List\n");
        TR69_TRC (TR69_ENTRY_EXIT_TRC,
                  "Exit: TrSetInternetGatewayDevice_VpnRemoteId - FAILURE\n");

        return SNMP_FAILURE;
    }

    pNode = pVpnRemoteId;

    u4MakeActive = 0;
    if (((pNode->u4ValMask & (VPN_REMOTE_ID_VAL_MASK)) ==
         (VPN_REMOTE_ID_VAL_MASK)) && (strcmp (name, "Status") != 0))
    {
        u4val = NOT_IN_SERVICE;

        i4rc =
            MynmhTest (*Join
                       (FsVpnRemoteIdStatus, 12, FsVpnRemoteIdTableINDEX, 2,
                        u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                       (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSet: Test failed for FsVpnRemoteIdStatus NOT_IN_SERVICE\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetInternetGatewayDevice_VpnRemoteId - FAILURE\n");

            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetInternetGatewayDevice_RAVPN_User - FAILURE\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnRemoteIdStatus, 12, FsVpnRemoteIdTableINDEX, 2,
                       u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSet: Set failed for FsVpnRemoteIdStatus NOT_IN_SERVICE\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetInternetGatewayDevice_VpnRemoteId - FAILURE\n");

            return SNMP_FAILURE;
        }

        u4MakeActive = 1;
    }
    i4rc2 = TrSetNonMand_InternetGatewayDevice_VpnRemoteId (name, value, pNode);
    if (i4rc2 == TR_NOT_MAND_PARAM)
    {
        i4rc2 =
            TrSetMand_InternetGatewayDevice_VpnRemoteId (name, value, pNode);
    }
    if ((u4MakeActive) && (strcmp (name, "Status") != 0)
        && (pNode->u4Status == ACTIVE))
    {
        /* TOCHECK: Set RowStatus to ACTIVE */
        u4val = ACTIVE;
        i4rc =
            MynmhTest (*Join
                       (FsVpnRemoteIdStatus, 12, FsVpnRemoteIdTableINDEX, 2,
                        u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                       (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- Test failed for FsVpnRemoteIdStatus\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnRemoteIdStatus, 12, FsVpnRemoteIdTableINDEX, 2,
                       u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "-E- Set failed for FsVpnRemoteIdStatus\n");
            return SNMP_FAILURE;
        }
    }
    if (i4rc2 == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrSetNonMand_InternetGatewayDevice_RAVPN
 *  Description     : Set function for non-mandatory objects.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tRAVPN entry.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetNonMand_InternetGatewayDevice_RAVPN (char *name, ParameterValue * value,
                                          tRAVPN * pNode)
{
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    /* INDICES: */
    UNUSED_PARAM (name);
    UNUSED_PARAM (value);
    UNUSED_PARAM (pNode);

    /* Indices not mapped in .inp file */

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    OctetStr.pu1_OctetList = au1OctetList;

    return TR_NOT_MAND_PARAM;
}

/************************************************************************
 *  Function Name   : TrSetNonMand_InternetGatewayDevice_RAVPN_User
 *  Description     : Set function for non-mandatory objects.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tUser entry.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetNonMand_InternetGatewayDevice_RAVPN_User (char *name,
                                               ParameterValue * value,
                                               tUser * pNode)
{
    INT4                i4rc = 0;
    UINT4               u4val = 0;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    /* INDICES: */
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetNonMand_InternetGatewayDevice_RAVPN_User\n");
    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStr.pu1_OctetList = au1OctetList;

    OctetStrIdx.i4_Length = STRLEN (pNode->au1Name);
    MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1Name, OctetStrIdx.i4_Length);

    if (STRCMP (name, "RowStatus") == 0)
    {
        if ((pNode->u4ValMask & (USER_VAL_MASK)) == (USER_VAL_MASK))
        {
            if (value->in_uint == TR_TRUE)
            {
                pNode->u4RowStatus = u4val = TR_ENABLE_ROWSTATUS;
            }
            else if (value->in_uint == TR_FALSE)
            {
                pNode->u4RowStatus = u4val = TR_DISABLE_ROWSTATUS;
            }
            else
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Invalid value for FsVpnRaUserRowStatus \n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetNonMand_InternetGatewayDevice_RAVPN_User - FAILURE\n");

                return SNMP_FAILURE;
            }

            i4rc =
                MynmhTest (*Join
                           (FsVpnRaUserRowStatus, 12,
                            FsVpnRaUsersTableINDEX, 1, &OctetStrIdx),
                           SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "-E- TrSetNonMand: Test failed for FsVpnRaUserRowStatus %d\n",
                               u4val);
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetNonMand_InternetGatewayDevice_RAVPN_User - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnRaUserRowStatus, 12,
                           FsVpnRaUsersTableINDEX, 1, &OctetStrIdx),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "-E- TrSetNonMand: Set failed for FsVpnRaUserRowStatus %d\n",
                               u4val);
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetNonMand_InternetGatewayDevice_RAVPN_User - FAILURE\n");

                return SNMP_FAILURE;
            }
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_InternetGatewayDevice_RAVPN_User - SUCCESS\n");

            return SNMP_SUCCESS;
        }
        else
        {
            TrSetAll_InternetGatewayDevice_RAVPN_User (name, pNode,
                                                       value, !TR_DO_SNMP_SET);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_InternetGatewayDevice_RAVPN_User - SUCCESS\n");
            return SNMP_SUCCESS;
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetNonMand_InternetGatewayDevice_RAVPN_User - NON_MAND\n");
    return TR_NOT_MAND_PARAM;
}

/************************************************************************
 *  Function Name   : TrSetNonMand_InternetGatewayDevice_RAVPN_AddressPool
 *  Description     : Set function for non-mandatory objects.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tAddressPool entry.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetNonMand_InternetGatewayDevice_RAVPN_AddressPool (char *name,
                                                      ParameterValue * value,
                                                      tAddressPool * pNode)
{
    INT4                i4rc = 0;
    UINT4               u4val = 0;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    /* INDICES: */
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetNonMand_InternetGatewayDevice_RAVPN_AddressPool\n");
    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStr.pu1_OctetList = au1OctetList;

    OctetStrIdx.i4_Length = STRLEN (pNode->au1Name);
    MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1Name, OctetStrIdx.i4_Length);

    if (STRCMP (name, "RowStatus") == 0)
    {
        if ((pNode->u4ValMask & (ADDRESS_POOL_VAL_MASK)) ==
            (ADDRESS_POOL_VAL_MASK))
        {
            if (value->in_uint == TR_TRUE)
            {
                pNode->u4RowStatus = u4val = TR_ENABLE_ROWSTATUS;
            }
            else if (value->in_uint == TR_FALSE)
            {
                pNode->u4RowStatus = u4val = TR_DISABLE_ROWSTATUS;
            }
            else
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Invalid value for FsVpnRaAddressPoolRowStatus\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetNonMand_InternetGatewayDevice_RAVPN_AddressPool - FAILURE\n");

                return SNMP_FAILURE;
            }

            i4rc =
                MynmhTest (*Join
                           (FsVpnRaAddressPoolRowStatus, 12,
                            FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                           SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "-E- TrSetNonMand: Test failed for FsVpnRaAddressPoolRowStatus %d\n",
                               u4val);
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetNonMand_InternetGatewayDevice_RAVPN_AddressPool - FAILURE\n");

                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnRaAddressPoolRowStatus, 12,
                           FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "-E- TrSetNonMand: Set failed for FsVpnRaAddressPoolRowStatus %d\n",
                               u4val);
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetNonMand_InternetGatewayDevice_RAVPN_AddressPool - FAILURE\n");
                return SNMP_FAILURE;
            }
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_InternetGatewayDevice_RAVPN_AddressPool - SUCCESS\n");
            return SNMP_SUCCESS;
        }
        else
        {
            TrSetAll_InternetGatewayDevice_RAVPN_AddressPool (name, pNode,
                                                              value,
                                                              !TR_DO_SNMP_SET);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_InternetGatewayDevice_RAVPN_AddressPool - SUCCESS\n");
            return SNMP_SUCCESS;
        }

    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetNonMand_InternetGatewayDevice_RAVPN_AddressPool - NON_MAND_PARAM\n");
    return TR_NOT_MAND_PARAM;
}

/************************************************************************
 *  Function Name   : TrSetNonMand_InternetGatewayDevice_VpnRemoteId
 *  Description     : Set function for non-mandatory objects.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tVpnRemoteId entry.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetNonMand_InternetGatewayDevice_VpnRemoteId (char *name,
                                                ParameterValue * value,
                                                tVpnRemoteId * pNode)
{
    INT4                i4rc = 0;
    UINT4               u4val = 0;
    UINT4               u4Idx0 = 0;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    /* INDICES: */
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry:TrSetNonMand_InternetGatewayDevice_VpnRemoteId\n");
    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStr.pu1_OctetList = au1OctetList;

    OctetStrIdx.i4_Length = STRLEN (pNode->au1Value);
    MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1Value, OctetStrIdx.i4_Length);
    u4Idx0 = pNode->i4Type;

    if (STRCMP (name, "Status") == 0)
    {
        if ((pNode->u4ValMask & VPN_REMOTE_ID_VAL_MASK) ==
            VPN_REMOTE_ID_VAL_MASK)
        {
            if (value->in_uint == TR_TRUE)
            {
                pNode->u4Status = u4val = TR_ENABLE_ROWSTATUS;
            }
            else if (value->in_uint == TR_FALSE)
            {
                pNode->u4Status = u4val = TR_DISABLE_ROWSTATUS;
            }
            else
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Invalid value for FsVpnRemoteIdStatus \n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetNonMand_InternetGatewayDevice_VpnRemoteId - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhTest (*Join
                           (FsVpnRemoteIdStatus, 12, FsVpnRemoteIdTableINDEX, 2,
                            u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Test failed for FsVpnRemoteIdStatus \n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetNonMand_InternetGatewayDevice_VpnRemoteId - FAILURE\n");
                return SNMP_FAILURE;
            }
            i4rc =
                MynmhSet (*Join
                          (FsVpnRemoteIdStatus, 12, FsVpnRemoteIdTableINDEX, 2,
                           u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) u4val);

            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetNonMand: Set failed for FsVpnRemoteIdStatus \n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetNonMand_InternetGatewayDevice_VpnRemoteId - FAILURE\n");
                return SNMP_FAILURE;
            }
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_InternetGatewayDevice_VpnRemoteId - SUCCESS\n");
            return SNMP_SUCCESS;
        }
        else
        {
            TrSetAll_InternetGatewayDevice_VpnRemoteId (name, pNode,
                                                        value, !TR_DO_SNMP_SET);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetNonMand_InternetGatewayDevice_VpnRemoteId - SUCCESS\n");
            return SNMP_SUCCESS;
        }

    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetNonMand_InternetGatewayDevice_VpnRemoteId - NOT_NON_MAND\n");
    return TR_NOT_MAND_PARAM;
}

/************************************************************************
 *  Function Name   : TrSetMand_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
 *  Description     : Set function for mandatory objects.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tVPN entry.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4 
     
     
     
     
     
     
     
    TrSetMand_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
    (char *name, ParameterValue * value, tVPN * pNode)
{
    INT4                i4rc = 0;
    tVPN                NewNode;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    /* INDICES: */
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    UINT4              *ptr = NULL;
    UINT4               u4Size = 0;
#ifdef TR69MSR_WANTED
    UINT1               u1Tr69MsrStatus = 0;
#endif

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry:TrSetMand_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN\n");

    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStr.pu1_OctetList = au1OctetList;

    if ((pNode->u4ValMask & pNode->u4GlobalValMask) != pNode->u4GlobalValMask)
    {
        TR69_TRC (TR69_DBG_TRC,
                  "TrSetMand: Caching Node values until valMask becomes full\n");

        TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
            (name, pNode, value, !TR_DO_SNMP_SET);

        if ((pNode->u4ValMask & pNode->u4GlobalValMask) ==
            pNode->u4GlobalValMask)

        {
            TR69_TRC (TR69_DBG_TRC,
                      "TrSetMand: Calling low level routines to set all values\n");

            OctetStrIdx.i4_Length = STRLEN (pNode->au1PolicyName);
            MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1PolicyName,
                    OctetStrIdx.i4_Length);

            i4rc =
                MynmhTest (*Join
                           (FsVpnPolicyRowStatus, 12, FsVpnTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) CREATE_AND_WAIT);
            if (i4rc == SNMP_FAILURE)
            {
#ifdef TR69MSR_WANTED
                Tr69MsrGetRestoreStatus (&u1Tr69MsrStatus);
                if (u1Tr69MsrStatus == OSIX_TRUE)
                {
                    return SNMP_SUCCESS;
                }
#endif
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Test failed for FsVpnPolicyRowStatus CREATE_AND_WAIT\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit:TrSetMand_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;

            }
            i4rc =
                MynmhSet (*Join
                          (FsVpnPolicyRowStatus, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) CREATE_AND_WAIT);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Set failed for FsVpnPolicyRowStatus CREATE_AND_WAIT\n");

                ptr = &(pNode->u4ValMask);
                u4Size =
                    sizeof (tVPN) - (3 * sizeof (UINT4) +
                                     sizeof (tTMO_SLL_NODE));
                MEMSET (ptr, 0, u4Size);
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit:TrSetMand_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;

            }
            else
            {
                pNode->u4PolicyRowStatus = NOT_IN_SERVICE;
            }
            i4rc =
                TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
                (NULL, pNode, value, TR_DO_SNMP_SET);

            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: TrSetAll Failed DESTROYING Row\n");

                i4rc =
                    MynmhSet (*Join
                              (FsVpnPolicyRowStatus, 12, FsVpnTableINDEX, 1,
                               &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                              (VOID *) DESTROY);
                ptr = &(pNode->u4ValMask);
                u4Size =
                    sizeof (tVPN) - (3 * sizeof (UINT4) +
                                     sizeof (tTMO_SLL_NODE));
                MEMSET (ptr, 0, u4Size);
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit:TrSetMand_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnPolicyRowStatus, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) ACTIVE);
            if (i4rc == SNMP_SUCCESS)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "TrSetMand: Set Success for FsVpnPolicyRowStatus ACTIVE\n");
                pNode->u4PolicyRowStatus = ACTIVE;
            }
            else
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Set Failed FsVpnPolicyRowStatus ACTIVE \n");
                i4rc =
                    MynmhSet (*Join
                              (FsVpnPolicyRowStatus, 12, FsVpnTableINDEX, 1,
                               &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                              (VOID *) DESTROY);
                ptr = &(pNode->u4ValMask);
                u4Size =
                    sizeof (tVPN) - (3 * sizeof (UINT4) +
                                     sizeof (tTMO_SLL_NODE));
                MEMSET (ptr, 0, u4Size);
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit:TrSetMand_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

        }
    }
    else
    {

        MEMCPY (&NewNode, pNode, sizeof (*pNode));

        MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
        OctetStrIdx.i4_Length = STRLEN (pNode->au1PolicyName);
        MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1PolicyName,
                OctetStrIdx.i4_Length);

        if (STRCMP (name, "PolicyName") == 0)
        {

            i4rc =
                MynmhTest (*Join
                           (FsVpnPolicyRowStatus, 12, FsVpnTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) DESTROY);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "-E- TrSetMand: Test failed for %s FsVpnPolicyRowStatus DESTROY\n",
                               OctetStrIdx.pu1_OctetList);

                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit:TrSetMand_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnPolicyRowStatus, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) DESTROY);

            MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
            OctetStrIdx.i4_Length = STRLEN (value->in_cval);
            MEMCPY (OctetStrIdx.pu1_OctetList, value->in_cval,
                    OctetStrIdx.i4_Length);

            /* Create a New node if validation is success */
            i4rc =
                MynmhTest (*Join
                           (FsVpnPolicyRowStatus, 12, FsVpnTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) CREATE_AND_WAIT);

            if (i4rc == SNMP_FAILURE)
            {

                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "-E- TrSetMand: Test failed for %s FsVpnPolicyRowStatus CREATE_AND_WAIT\n",
                               OctetStrIdx.pu1_OctetList);
                MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
                OctetStrIdx.i4_Length = STRLEN (pNode->au1PolicyName);
                MEMCPY (OctetStrIdx.pu1_OctetList,
                        pNode->au1PolicyName, OctetStrIdx.i4_Length);

                i4rc =
                    MynmhSet (*Join
                              (FsVpnPolicyRowStatus, 12, FsVpnTableINDEX, 1,
                               &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                              (VOID *) CREATE_AND_WAIT);

                /*Update the Index of the Newnode */
                MEMSET (NewNode.au1PolicyName, 0, VPN_POLICY_NAME);
                MEMCPY (NewNode.au1PolicyName, OctetStrIdx.pu1_OctetList,
                        OctetStrIdx.i4_Length);

                /* Set all the values of the elements of the previous node */
                i4rc =
                    TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
                    (NULL, &NewNode, value, TR_DO_SNMP_SET);

                /*Copy into Old Node */
                MEMCPY (pNode, &NewNode, sizeof (*pNode));
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit:TrSetMand_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnPolicyRowStatus, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) CREATE_AND_WAIT);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "-E- TrSetMand: Set failed for %s FsVpnPolicyRowStatus CREATE_AND_WAIT\n",
                               OctetStrIdx.pu1_OctetList);

                MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
                OctetStrIdx.i4_Length = STRLEN (pNode->au1PolicyName);
                MEMCPY (OctetStrIdx.pu1_OctetList,
                        pNode->au1PolicyName, OctetStrIdx.i4_Length);

                i4rc =
                    MynmhSet (*Join
                              (FsVpnPolicyRowStatus, 12, FsVpnTableINDEX, 1,
                               &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                              (VOID *) CREATE_AND_WAIT);

                /*Update the Index of the Newnode */
                MEMSET (NewNode.au1PolicyName, 0, VPN_POLICY_NAME);
                MEMCPY (NewNode.au1PolicyName, OctetStrIdx.pu1_OctetList,
                        OctetStrIdx.i4_Length);

                /* Set all the values of the elements of the previous node */
                i4rc =
                    TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
                    (NULL, &NewNode, value, TR_DO_SNMP_SET);

                /*Copy into Old Node */
                MEMCPY (pNode, &NewNode, sizeof (*pNode));
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit:TrSetMand_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

        }

        /* First update NewNode and test the new value */
        i4rc =
            TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
            (name, &NewNode, value, !TR_DO_SNMP_SET);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit:TrSetMand_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
            return SNMP_FAILURE;
        }

        /* Set the new value */
        i4rc =
            TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
            (NULL, &NewNode, value, TR_DO_SNMP_SET);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetMand: Unable to set the new value\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit:TrSetMand_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
            return SNMP_FAILURE;
        }
        MEMCPY (pNode, &NewNode, sizeof (*pNode));

    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit:TrSetMand_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - SUCCESS\n");

    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrSetMand_InternetGatewayDevice_RAVPN_User
 *  Description     : Set function for mandatory objects.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tUser entry.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetMand_InternetGatewayDevice_RAVPN_User (char *name, ParameterValue * value,
                                            tUser * pNode)
{
    INT4                i4rc = 0;
    INT4                i4rc2 = SNMP_SUCCESS;
    tUser               NewNode;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    /* INDICES: */
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
#ifdef TR69MSR_WANTED
    UINT1               u1Tr69MsrStatus = 0;
#endif

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry:TrSetMand_InternetGatewayDevice_RAVPN_User\n");
    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStr.pu1_OctetList = au1OctetList;

    if (pNode->u4ValMask != (USER_VAL_MASK))
    {
        TR69_TRC (TR69_DBG_TRC,
                  "TrSetMand: Caching Node values until valMask becomes full\n");

        TrSetAll_InternetGatewayDevice_RAVPN_User (name, pNode, value,
                                                   !TR_DO_SNMP_SET);
        if (((pNode->u4ValMask) & (USER_VAL_MASK)) == (USER_VAL_MASK))
        {
            TR69_TRC (TR69_DBG_TRC,
                      "TrSetMand: Calling low level routines to set all values\n");
            /* TO CHECK */
            OctetStrIdx.i4_Length = STRLEN (pNode->au1Name);
            MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1Name,
                    OctetStrIdx.i4_Length);

            i4rc =
                MynmhTest (*Join
                           (FsVpnRaUserRowStatus, 12, FsVpnRaUsersTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) CREATE_AND_WAIT);

            if (i4rc == SNMP_FAILURE)
            {
#ifdef TR69MSR_WANTED
                Tr69MsrGetRestoreStatus (&u1Tr69MsrStatus);
                if (u1Tr69MsrStatus == OSIX_TRUE)
                {
                    return SNMP_SUCCESS;
                }
#endif
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Test failed for FsVpnRaUserRowStatus CREATE_AND_WAIT\n");
                pNode->u4RowStatus = 0;
                MEMSET (pNode->au1Name, '\0', USER_NAME);
                MEMSET (pNode->au1Secret, '\0', USER_SECRET);
                pNode->u4ValMask = 0;
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit:TrSetMand_InternetGatewayDevice_RAVPN_User - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnRaUserRowStatus, 12, FsVpnRaUsersTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) CREATE_AND_WAIT);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Set failed for FsVpnRaUserRowStatus CREATE_AND_WAIT\n");
                pNode->u4RowStatus = 0;
                MEMSET (pNode->au1Name, '\0', USER_NAME);
                MEMSET (pNode->au1Secret, '\0', USER_SECRET);
                pNode->u4ValMask = 0;
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit:TrSetMand_InternetGatewayDevice_RAVPN_User - FAILURE\n");
                return SNMP_FAILURE;
            }
            else
            {
                pNode->u4RowStatus = NOT_IN_SERVICE;
            }

            i4rc =
                TrSetAll_InternetGatewayDevice_RAVPN_User (NULL, pNode, value,
                                                           TR_DO_SNMP_SET);

            if (i4rc == SNMP_FAILURE)
            {

                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: TrSetAll Failed DESTROYING Row\n");
                MynmhSet (*Join
                          (FsVpnRaUserRowStatus, 12, FsVpnRaUsersTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) DESTROY);
                pNode->u4RowStatus = 0;
                MEMSET (pNode->au1Name, '\0', USER_NAME);
                MEMSET (pNode->au1Secret, '\0', USER_SECRET);
                pNode->u4ValMask = 0;
                pNode->u4RowStatus = 0;
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit:TrSetMand_InternetGatewayDevice_RAVPN_User - FAILURE\n");
                return SNMP_FAILURE;
            }
            i4rc =
                MynmhSet (*Join
                          (FsVpnRaUserRowStatus, 12, FsVpnRaUsersTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) ACTIVE);

            if (i4rc == SNMP_SUCCESS)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "TrSetMand: Set Success for FsVpnRaUserRowStatus ACTIVE\n");
                pNode->u4RowStatus = ACTIVE;
            }
            else
            {

                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Set failed for FsVpnRaUserRowStatus ACTIVE\n");
                MynmhSet (*Join
                          (FsVpnRaUserRowStatus, 12, FsVpnRaUsersTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) DESTROY);
                pNode->u4RowStatus = 0;
                MEMSET (pNode->au1Name, '\0', USER_NAME);
                MEMSET (pNode->au1Secret, '\0', USER_SECRET);
                pNode->u4ValMask = 0;
                pNode->u4RowStatus = 0;
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit:TrSetMand_InternetGatewayDevice_RAVPN_User - FAILURE\n");
                return SNMP_FAILURE;
            }

        }
    }
    else
    {
        /* TO CHECK */
        OctetStrIdx.i4_Length = STRLEN (pNode->au1Name);
        MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1Name,
                OctetStrIdx.i4_Length);

        MEMCPY (&NewNode, pNode, sizeof (*pNode));
        i4rc =
            MynmhTest (*Join
                       (FsVpnRaUserRowStatus, 12, FsVpnRaUsersTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                       (VOID *) DESTROY);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetMand: Test failed for %s FsVpnRaUserRowStatus DESTROY\n",
                           OctetStrIdx.pu1_OctetList);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit:TrSetMand_InternetGatewayDevice_RAVPN_User - FAILURE\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnRaUserRowStatus, 12, FsVpnRaUsersTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetMand: Set failed for %s FsVpnRaUserRowStatus DESTROY\n",
                           OctetStrIdx.pu1_OctetList);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit:TrSetMand_InternetGatewayDevice_RAVPN_User - FAILURE\n");
            return SNMP_FAILURE;
        }
        if (STRCMP (name, "Name") == 0)
        {
            OctetStrIdx.i4_Length = STRLEN (value->in_cval);
            MEMCPY (OctetStrIdx.pu1_OctetList, value->in_cval,
                    OctetStrIdx.i4_Length);
        }
        i4rc =
            MynmhTest (*Join
                       (FsVpnRaUserRowStatus, 12, FsVpnRaUsersTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                       (VOID *) CREATE_AND_WAIT);

        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetMand: Test failed for %s FsVpnRaUserRowStatus CREATE_AND_WAIT\n",
                           OctetStrIdx.pu1_OctetList);
            MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStrIdx.i4_Length = STRLEN (pNode->au1Name);
            MEMCPY (OctetStrIdx.pu1_OctetList,
                    pNode->au1Name, OctetStrIdx.i4_Length);

            MynmhSet (*Join
                      (FsVpnRaUserRowStatus, 12, FsVpnRaUsersTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) CREATE_AND_WAIT);

            TrSetAll_InternetGatewayDevice_RAVPN_User (NULL,
                                                       pNode,
                                                       value, TR_DO_SNMP_SET);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit:TrSetMand_InternetGatewayDevice_RAVPN_User - FAILURE\n");

            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnRaUserRowStatus, 12, FsVpnRaUsersTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) CREATE_AND_WAIT);

        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetMand: Set failed for %s FsVpnRaUserRowStatus CREATE_AND_WAIT\n",
                           OctetStrIdx.pu1_OctetList);
            MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStrIdx.i4_Length = STRLEN (pNode->au1Name);
            MEMCPY (OctetStrIdx.pu1_OctetList,
                    pNode->au1Name, OctetStrIdx.i4_Length);

            MynmhSet (*Join
                      (FsVpnRaUserRowStatus, 12, FsVpnRaUsersTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) CREATE_AND_WAIT);

            TrSetAll_InternetGatewayDevice_RAVPN_User (NULL,
                                                       pNode,
                                                       value, TR_DO_SNMP_SET);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit:TrSetMand_InternetGatewayDevice_RAVPN_User - FAILURE\n");

            return SNMP_FAILURE;
        }

        /* First update NewNode with the new value */
        /* Then SET all the values for the new row */
        i4rc2 =
            TrSetAll_InternetGatewayDevice_RAVPN_User (name, &NewNode, value,
                                                       !TR_DO_SNMP_SET);
        i4rc =
            TrSetAll_InternetGatewayDevice_RAVPN_User (NULL, &NewNode, value,
                                                       TR_DO_SNMP_SET);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetMand: Unable to set the new value\n");

            MynmhSet (*Join
                      (FsVpnRaUserRowStatus, 12, FsVpnRaUsersTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);

            MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStrIdx.i4_Length = STRLEN (pNode->au1Name);
            MEMCPY (OctetStrIdx.pu1_OctetList,
                    pNode->au1Name, OctetStrIdx.i4_Length);

            MynmhSet (*Join
                      (FsVpnRaUserRowStatus, 12, FsVpnRaUsersTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) CREATE_AND_WAIT);

            i4rc =
                TrSetAll_InternetGatewayDevice_RAVPN_User (NULL, pNode, value,
                                                           TR_DO_SNMP_SET);
            i4rc2 = SNMP_FAILURE;
        }
        if ((strcmp (name, "RowStatus") != 0) && (pNode->u4RowStatus == ACTIVE))
        {
            i4rc =
                MynmhSet (*Join
                          (FsVpnRaUserRowStatus, 12, FsVpnRaUsersTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) ACTIVE);
        }
        MEMCPY (pNode, &NewNode, sizeof (NewNode));
        if (i4rc2 == SNMP_FAILURE)
        {
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit:TrSetMand_InternetGatewayDevice_RAVPN_User - FAILURE\n");
            return SNMP_FAILURE;
        }

    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit:TrSetMand_InternetGatewayDevice_RAVPN_User - SUCCESS\n");

    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrSetMand_InternetGatewayDevice_RAVPN_AddressPool
 *  Description     : Set function for mandatory objects.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tAddressPool entry.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetMand_InternetGatewayDevice_RAVPN_AddressPool (char *name,
                                                   ParameterValue * value,
                                                   tAddressPool * pNode)
{

    INT4                i4rc = 0;
    INT4                i4rc2 = SNMP_SUCCESS;
    tAddressPool        NewNode;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    /* INDICES: */
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
#ifdef TR69MSR_WANTED
    UINT1               u1Tr69MsrStatus = 0;
#endif

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry:TrSetMand_InternetGatewayDevice_RAVPN_AddressPool\n");
    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStr.pu1_OctetList = au1OctetList;

    if (pNode->u4ValMask != (ADDRESS_POOL_VAL_MASK))
    {
        TR69_TRC (TR69_DBG_TRC,
                  "TrSetMand: Caching Node values until valMask becomes full\n");
        TrSetAll_InternetGatewayDevice_RAVPN_AddressPool (name, pNode, value,
                                                          !TR_DO_SNMP_SET);
        if ((pNode->u4ValMask & ADDRESS_POOL_VAL_MASK) == ADDRESS_POOL_VAL_MASK)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "TrSetMand: Calling low level routines to set all values\n");
            /* TO CHECK */
            OctetStrIdx.i4_Length = STRLEN (pNode->au1Name);
            MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1Name,
                    OctetStrIdx.i4_Length);
            i4rc = MynmhTest (*Join (FsVpnRaAddressPoolRowStatus, 12,
                                     FsVpnRaAddressPoolTableINDEX, 1,
                                     &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                              (VOID *) CREATE_AND_WAIT);
            if (i4rc == SNMP_FAILURE)
            {
#ifdef TR69MSR_WANTED
                Tr69MsrGetRestoreStatus (&u1Tr69MsrStatus);
                if (u1Tr69MsrStatus == OSIX_TRUE)
                {
                    return SNMP_SUCCESS;
                }
#endif
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Test failed for FsVpnRaAddressPoolRowStatus CREATE_AND_WAIT\n");
                pNode->u4RowStatus = 0;
                MEMSET (pNode->au1Name, '\0', ADDRESSPOOL_NAME);
                MEMSET (pNode->au1EndAddress, '\0', ADDRESSPOOL_END_ADDRESS);
                MEMSET (pNode->au1StartAddress, '\0',
                        ADDRESSPOOL_START_ADDRESS);
                pNode->u4ValMask = 0;
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_InternetGatewayDevice_RAVPN_AddressPool - FAILURE\n");

                return SNMP_FAILURE;
            }

            i4rc = MynmhSet (*Join (FsVpnRaAddressPoolRowStatus, 12,
                                    FsVpnRaAddressPoolTableINDEX, 1,
                                    &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                             (VOID *) CREATE_AND_WAIT);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Set failed for FsVpnRaAddressPoolRowStatus CREATE_AND_WAIT\n");
                pNode->u4RowStatus = 0;
                MEMSET (pNode->au1Name, '\0', ADDRESSPOOL_NAME);
                MEMSET (pNode->au1EndAddress, '\0', ADDRESSPOOL_END_ADDRESS);
                MEMSET (pNode->au1StartAddress, '\0',
                        ADDRESSPOOL_START_ADDRESS);
                pNode->u4ValMask = 0;

                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_InternetGatewayDevice_RAVPN_AddressPool - FAILURE\n");

                return SNMP_FAILURE;
            }
            else
            {
                pNode->u4RowStatus = NOT_IN_SERVICE;
            }
            i4rc =
                TrSetAll_InternetGatewayDevice_RAVPN_AddressPool (NULL, pNode,
                                                                  value,
                                                                  TR_DO_SNMP_SET);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: TrSetAll Failed DESTROYING Row\n");

                MynmhSet (*Join
                          (FsVpnRaAddressPoolRowStatus, 12,
                           FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);

                pNode->u4RowStatus = 0;
                MEMSET (pNode->au1Name, '\0', ADDRESSPOOL_NAME);
                MEMSET (pNode->au1EndAddress, '\0', ADDRESSPOOL_END_ADDRESS);
                MEMSET (pNode->au1StartAddress, '\0',
                        ADDRESSPOOL_START_ADDRESS);
                pNode->u4ValMask = 0;
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_InternetGatewayDevice_RAVPN_AddressPool - FAILURE\n");

                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnRaAddressPoolRowStatus, 12,
                           FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) ACTIVE);

            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Set failed for FsVpnRaAddressPoolRowStatus ACTIVE\n");
                MynmhSet (*Join
                          (FsVpnRaAddressPoolRowStatus, 12,
                           FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);

                pNode->u4RowStatus = 0;
                MEMSET (pNode->au1Name, '\0', ADDRESSPOOL_NAME);
                MEMSET (pNode->au1EndAddress, '\0', ADDRESSPOOL_END_ADDRESS);
                MEMSET (pNode->au1StartAddress, '\0',
                        ADDRESSPOOL_START_ADDRESS);
                pNode->u4ValMask = 0;

                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_InternetGatewayDevice_RAVPN_AddressPool - FAILURE\n");

                return SNMP_FAILURE;
            }
            else
            {
                pNode->u4RowStatus = ACTIVE;
            }

        }
    }
    else
    {
        /* TO CHECK */
        OctetStrIdx.i4_Length = STRLEN (pNode->au1Name);
        MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1Name,
                OctetStrIdx.i4_Length);

        MEMCPY (&NewNode, pNode, sizeof (*pNode));

        i4rc = MynmhTest (*Join (FsVpnRaAddressPoolRowStatus, 12,
                                 FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetMand: Test failed for FsVpnRaAddressPoolRowStatus DESTROY\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_InternetGatewayDevice_RAVPN_AddressPool - FAILURE\n");

            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnRaAddressPoolRowStatus, 12,
                       FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetMand: Set failed for FsVpnRaAddressPoolRowStatus DESTROY\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_InternetGatewayDevice_RAVPN_AddressPool - FAILURE\n");

            return SNMP_FAILURE;
        }
        if (STRCMP (name, "Name") == 0)
        {
            MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStrIdx.i4_Length = STRLEN (value->in_cval);
            MEMCPY (OctetStrIdx.pu1_OctetList, value->in_cval,
                    OctetStrIdx.i4_Length);
        }
        i4rc =
            MynmhTest (*Join
                       (FsVpnRaAddressPoolRowStatus, 12,
                        FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                       SNMP_DATA_TYPE_INTEGER, (VOID *) CREATE_AND_WAIT);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetMand: Test failed for FsVpnRaAddressPoolRowStatus CREATE_AND_WAIT\n");
            MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStrIdx.i4_Length = STRLEN (pNode->au1Name);
            MEMCPY (OctetStrIdx.pu1_OctetList,
                    pNode->au1Name, OctetStrIdx.i4_Length);

            MynmhSet (*Join
                      (FsVpnRaAddressPoolRowStatus, 12,
                       FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) CREATE_AND_WAIT);

            TrSetAll_InternetGatewayDevice_RAVPN_AddressPool (NULL,
                                                              pNode,
                                                              value,
                                                              TR_DO_SNMP_SET);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_InternetGatewayDevice_RAVPN_AddressPool - FAILURE\n");

            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnRaAddressPoolRowStatus, 12,
                       FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) CREATE_AND_WAIT);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetMand: Set failed for FsVpnRaAddressPoolRowStatus CREATE_AND_WAIT\n");
            MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStrIdx.i4_Length = STRLEN (pNode->au1Name);
            MEMCPY (OctetStrIdx.pu1_OctetList,
                    pNode->au1Name, OctetStrIdx.i4_Length);

            MynmhSet (*Join
                      (FsVpnRaAddressPoolRowStatus, 12,
                       FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) CREATE_AND_WAIT);

            TrSetAll_InternetGatewayDevice_RAVPN_AddressPool (NULL,
                                                              pNode,
                                                              value,
                                                              TR_DO_SNMP_SET);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_InternetGatewayDevice_RAVPN_AddressPool - FAILURE\n");

            return SNMP_FAILURE;
        }

        /* First update NewNode with the new value */
        /* Then SET all the values for the new row */
        i4rc2 =
            TrSetAll_InternetGatewayDevice_RAVPN_AddressPool (name, &NewNode,
                                                              value,
                                                              !TR_DO_SNMP_SET);

        i4rc =
            TrSetAll_InternetGatewayDevice_RAVPN_AddressPool (NULL, &NewNode,
                                                              value,
                                                              TR_DO_SNMP_SET);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetMand: Unable to set the new value\n");
            MynmhSet (*Join
                      (FsVpnRaAddressPoolRowStatus, 12,
                       FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);

            MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStrIdx.i4_Length = STRLEN (pNode->au1Name);
            MEMCPY (OctetStrIdx.pu1_OctetList,
                    pNode->au1Name, OctetStrIdx.i4_Length);

            MynmhSet (*Join (FsVpnRaAddressPoolRowStatus, 12,
                             FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) CREATE_AND_WAIT);

            i4rc = TrSetAll_InternetGatewayDevice_RAVPN_AddressPool (NULL,
                                                                     pNode,
                                                                     value,
                                                                     TR_DO_SNMP_SET);
            i4rc2 = SNMP_FAILURE;

        }

        if ((strcmp (name, "RowStatus") != 0) && (pNode->u4RowStatus == ACTIVE))
        {
            i4rc =
                MynmhSet (*Join
                          (FsVpnRaAddressPoolRowStatus, 12,
                           FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) ACTIVE);
        }

        MEMCPY (pNode, &NewNode, sizeof (NewNode));

        if (i4rc2 == SNMP_FAILURE)
        {
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_InternetGatewayDevice_RAVPN_AddressPool - FAILURE\n");
            return SNMP_FAILURE;
        }

    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit:TrSetMand_InternetGatewayDevice_RAVPN_AddressPool - SUCCESS\n");

    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrSetMand_InternetGatewayDevice_RAVPN_User
 *  Description     : Set function for mandatory objects.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tUser entry.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetMand_InternetGatewayDevice_VpnRemoteId (char *name, ParameterValue * value,
                                             tVpnRemoteId * pNode)
{
    INT4                i4rc = 0;
    INT4                i4rc2 = SNMP_SUCCESS;
    tVpnRemoteId        NewNode;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    /* INDICES: */
    UINT4               u4Idx0 = 0;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
#ifdef TR69MSR_WANTED
    UINT1               u1Tr69MsrStatus = 0;
#endif

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetMand_InternetGatewayDevice_VpnRemoteId\n");
    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStr.pu1_OctetList = au1OctetList;

    if (pNode->u4ValMask != (VPN_REMOTE_ID_VAL_MASK))
    {

        TrSetAll_InternetGatewayDevice_VpnRemoteId (name, pNode, value,
                                                    !TR_DO_SNMP_SET);
        if ((pNode->u4ValMask & VPN_REMOTE_ID_VAL_MASK) ==
            VPN_REMOTE_ID_VAL_MASK)
        {

            /* TO CHECK */
            OctetStrIdx.i4_Length = STRLEN (pNode->au1Value);
            MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1Value,
                    OctetStrIdx.i4_Length);
            u4Idx0 = (UINT4) pNode->i4Type;
            i4rc = MynmhTest (*Join
                              (FsVpnRemoteIdStatus, 12, FsVpnRemoteIdTableINDEX,
                               2, u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                              (VOID *) CREATE_AND_WAIT);
            if (i4rc == SNMP_FAILURE)
            {
#ifdef TR69MSR_WANTED
                Tr69MsrGetRestoreStatus (&u1Tr69MsrStatus);
                if (u1Tr69MsrStatus == OSIX_TRUE)
                {
                    return SNMP_SUCCESS;
                }
#endif
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Test failed for FsVpnRemoteIdStatus CREATE_AND_WAIT\n");
                pNode->u4Status = 0;
                pNode->u4ValMask = 0;
                pNode->i4Type = 0;
                MEMSET (pNode->au1Value, '\0', VPNREMOTEID_VALUE);
                MEMSET (pNode->au1Type, '\0', VPNREMOTEID_TYPE);
                MEMSET (pNode->au1Key, '\0', VPNREMOTEID_KEY);
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_InternetGatewayDevice_VpnRemoteId - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnRemoteIdStatus, 12, FsVpnRemoteIdTableINDEX, 2,
                           u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) CREATE_AND_WAIT);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Test failed for FsVpnRemoteIdStatus CREATE_AND_WAIT\n");
                pNode->u4Status = 0;
                pNode->u4ValMask = 0;
                pNode->i4Type = 0;
                MEMSET (pNode->au1Value, '\0', VPNREMOTEID_VALUE);
                MEMSET (pNode->au1Type, '\0', VPNREMOTEID_TYPE);
                MEMSET (pNode->au1Key, '\0', VPNREMOTEID_KEY);
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_InternetGatewayDevice_VpnRemoteId - FAILURE\n");
                return SNMP_FAILURE;
            }
            else
            {
                pNode->u4Status = NOT_IN_SERVICE;
            }

            i4rc =
                TrSetAll_InternetGatewayDevice_VpnRemoteId (NULL, pNode, value,
                                                            TR_DO_SNMP_SET);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: TrSetAll Failed DESTROYING Row\n");
                MynmhSet (*Join
                          (FsVpnRemoteIdStatus, 12, FsVpnRemoteIdTableINDEX, 2,
                           u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) DESTROY);
                pNode->u4Status = 0;
                pNode->u4ValMask = 0;
                pNode->i4Type = 0;
                MEMSET (pNode->au1Value, '\0', VPNREMOTEID_VALUE);
                MEMSET (pNode->au1Type, '\0', VPNREMOTEID_TYPE);
                MEMSET (pNode->au1Key, '\0', VPNREMOTEID_KEY);
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_InternetGatewayDevice_VpnRemoteId - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhTest (*Join
                           (FsVpnRemoteIdStatus, 12, FsVpnRemoteIdTableINDEX, 2,
                            u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) ACTIVE);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Test failed for FsVpnRemoteIdStatus ACTIVE\n");
                MynmhSet (*Join
                          (FsVpnRemoteIdStatus, 12, FsVpnRemoteIdTableINDEX, 2,
                           u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) DESTROY);
                pNode->u4Status = 0;
                pNode->u4ValMask = 0;
                pNode->i4Type = 0;
                MEMSET (pNode->au1Value, '\0', VPNREMOTEID_VALUE);
                MEMSET (pNode->au1Type, '\0', VPNREMOTEID_TYPE);
                MEMSET (pNode->au1Key, '\0', VPNREMOTEID_KEY);
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_InternetGatewayDevice_VpnRemoteId - FAILURE\n");
                return SNMP_FAILURE;
            }
            i4rc =
                MynmhSet (*Join
                          (FsVpnRemoteIdStatus, 12, FsVpnRemoteIdTableINDEX, 2,
                           u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) ACTIVE);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetMand: Set failed for FsVpnRemoteIdStatus ACTIVE\n");
                MynmhSet (*Join
                          (FsVpnRemoteIdStatus, 12, FsVpnRemoteIdTableINDEX, 2,
                           u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) DESTROY);
                pNode->u4Status = 0;
                pNode->u4ValMask = 0;
                pNode->i4Type = 0;
                MEMSET (pNode->au1Value, '\0', VPNREMOTEID_VALUE);
                MEMSET (pNode->au1Type, '\0', VPNREMOTEID_TYPE);
                MEMSET (pNode->au1Key, '\0', VPNREMOTEID_KEY);
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetMand_InternetGatewayDevice_VpnRemoteId - FAILURE\n");
                return SNMP_FAILURE;
            }
            else
            {
                TR69_TRC (TR69_DBG_TRC,
                          "TrSetMand: Set Success for FsVpnRemoteIdStatus ACTIVE\n");
                pNode->u4Status = ACTIVE;
            }

        }
    }
    else
    {
        /* TO CHECK */
        OctetStrIdx.i4_Length = STRLEN (pNode->au1Value);
        MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1Value,
                OctetStrIdx.i4_Length);
        u4Idx0 = (UINT4) pNode->i4Type;

        MEMCPY (&NewNode, pNode, sizeof (*pNode));

        if ((STRCMP (name, "Value") == 0) || (STRCMP (name, "Type") == 0))
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetMand: Cannot Modify the Index of VpnRemoteId Table\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_InternetGatewayDevice_VpnRemoteId - FAILURE\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhTest (*Join
                       (FsVpnRemoteIdStatus, 12, FsVpnRemoteIdTableINDEX, 2,
                        u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                       (VOID *) DESTROY);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetMand: Test failed for FsVpnRemoteIdStatus DESTROY\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_InternetGatewayDevice_VpnRemoteId - FAILURE\n");

            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnRemoteIdStatus, 12, FsVpnRemoteIdTableINDEX, 2,
                       u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) DESTROY);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetMand: Set failed for FsVpnRemoteIdStatus DESTROY\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_InternetGatewayDevice_VpnRemoteId - FAILURE\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhTest (*Join
                       (FsVpnRemoteIdStatus, 12, FsVpnRemoteIdTableINDEX, 2,
                        u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                       (VOID *) CREATE_AND_WAIT);

        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetMand: Test failed for FsVpnRemoteIdStatus CREATE_AND_WAIT\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_InternetGatewayDevice_VpnRemoteId - FAILURE\n");

            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnRemoteIdStatus, 12, FsVpnRemoteIdTableINDEX, 2,
                       u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) CREATE_AND_WAIT);

        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetMand: Set failed for FsVpnRemoteIdStatus CREATE_AND_WAIT\n");

            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_InternetGatewayDevice_VpnRemoteId - FAILURE\n");

            return SNMP_FAILURE;
        }

        /* First update NewNode with the new value */
        /* Then SET all the values for the new row */
        i4rc2 =
            TrSetAll_InternetGatewayDevice_VpnRemoteId (name, &NewNode, value,
                                                        !TR_DO_SNMP_SET);
        i4rc =
            TrSetAll_InternetGatewayDevice_VpnRemoteId (NULL, &NewNode, value,
                                                        TR_DO_SNMP_SET);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetMand: Unable to set the new value\n");

            MynmhSet (*Join
                      (FsVpnRemoteIdStatus, 12, FsVpnRemoteIdTableINDEX, 2,
                       u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) DESTROY);

            MEMSET (OctetStrIdx.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
            OctetStrIdx.i4_Length = STRLEN (pNode->au1Value);
            MEMCPY (OctetStrIdx.pu1_OctetList,
                    pNode->au1Value, OctetStrIdx.i4_Length);
            u4Idx0 = (UINT4) pNode->i4Type;

            MynmhSet (*Join
                      (FsVpnRemoteIdStatus, 12, FsVpnRemoteIdTableINDEX, 2,
                       u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) CREATE_AND_WAIT);

            i4rc =
                TrSetAll_InternetGatewayDevice_VpnRemoteId (NULL, pNode, value,
                                                            TR_DO_SNMP_SET);
            i4rc2 = SNMP_FAILURE;
        }
        if (strcmp (name, "Status") != 0)
        {
            i4rc =
                MynmhSet (*Join
                          (FsVpnRemoteIdStatus, 12, FsVpnRemoteIdTableINDEX, 2,
                           u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) ACTIVE);
        }
        MEMCPY (pNode, &NewNode, sizeof (NewNode));

        if (i4rc2 == SNMP_FAILURE)
        {
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetMand_InternetGatewayDevice_VpnRemoteId - FAILURE\n");

            return SNMP_FAILURE;
        }

    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetMand_InternetGatewayDevice_VpnRemoteId - SUCCESS\n");

    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
 *  Description     : Set function for all objects of a table.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tVPN entry.
 *                    bTrDoSnmpSet - Indicates whether SNMP Set is to be done.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4 
     
     
     
     
     
     
     
    TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
    (char *name, tVPN * pNode, ParameterValue * value, BOOL1 bTrDoSnmpSet)
{
    INT4                i4rc = 0;
    UINT4               u4val = 0;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tUtlInAddr          IpAddr;

    /* INDICES: */
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN\n");
    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStr.pu1_OctetList = au1OctetList;

    OctetStrIdx.i4_Length = STRLEN (pNode->au1PolicyName);
    MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1PolicyName,
            OctetStrIdx.i4_Length);

    if (!name || STRCMP (name, "IkePhase1PeerIdType") == 0)
    {

        if (!bTrDoSnmpSet)
        {

            if (pNode->i4PolicyType == TR69_VPN_IPSEC_MANUAL)
            {
                return SNMP_FAILURE;
            }

            if (STRCASECMP (value->in_cval, "ipv4") == 0)
            {
                u4val = 1;
                MEMCPY (pNode->au1IkePhase1PeerIdType, "IPv4", 4);

            }
            else if (STRCASECMP (value->in_cval, "email") == 0)
            {
                MEMCPY (pNode->au1IkePhase1PeerIdType, "EMAIL", 5);
                u4val = 3;
            }
            else if (STRCASECMP (value->in_cval, "fqdn") == 0)
            {

                u4val = 2;
                MEMCPY (pNode->au1IkePhase1PeerIdType, "FQDN", 4);

            }
            else if (STRCASECMP (value->in_cval, "keyId") == 0)
            {
                u4val = 11;
                MEMCPY (pNode->au1IkePhase1PeerIdType, "KEYID", 5);

            }
            else
                return SNMP_FAILURE;

            pNode->i4IkePhase1PeerIdType = u4val;
            pNode->u4ValMask |= FS_VPN_IKE_PHASE1_PEER_ID_TYPE_MASK;
            return SNMP_SUCCESS;
        }
        u4val = pNode->i4IkePhase1PeerIdType;
        if ((pNode->i4PolicyType != TR69_VPN_IPSEC_MANUAL)
            && (pNode->i4IkePhase1PeerIdType != TR69_NOT_CONFIG))
        {
            i4rc =
                MynmhTest (*Join
                           (FsVpnIkePhase1PeerIdType, 12, FsVpnTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FsVpnIkePhase1PeerIdType\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnIkePhase1PeerIdType, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Set failed for FsVpnIkePhase1PeerIdType\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }
        }

    }
    if (!name || STRCMP (name, "IkePhase1DHGroup") == 0)
    {
        if (!bTrDoSnmpSet)
        {

            if (pNode->i4PolicyType == TR69_VPN_IPSEC_MANUAL)
            {
                return SNMP_FAILURE;
            }
            pNode->i4IkePhase1DHGroup = u4val = value->in_int;
            pNode->u4ValMask |= FS_VPN_IKE_PHASE1_DH_GROUP_MASK;
            return SNMP_SUCCESS;
        }

        u4val = pNode->i4IkePhase1DHGroup;

        if (pNode->i4PolicyType != TR69_VPN_IPSEC_MANUAL)
        {
            if (u4val == 0)
                u4val = pNode->i4IkePhase1DHGroup = 2;

            i4rc =
                MynmhTest (*Join
                           (FsVpnIkePhase1DHGroup, 12, FsVpnTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FsVpnIkePhase1DHGroup\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnIkePhase1DHGroup, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) u4val);
        }
    }
    if (!name || STRCMP (name, "IkePhase1HashAlgo") == 0)
    {
        if (!bTrDoSnmpSet)
        {

            if (pNode->i4PolicyType == TR69_VPN_IPSEC_MANUAL)
            {
                return SNMP_FAILURE;
            }

            pNode->i4IkePhase1HashAlgo = u4val = value->in_int;
            pNode->u4ValMask |= FS_VPN_IKE_PHASE1_HASH_ALGO_MASK;
            return SNMP_SUCCESS;
        }

        u4val = pNode->i4IkePhase1HashAlgo;
        if (pNode->i4PolicyType != TR69_VPN_IPSEC_MANUAL)
        {
            if (u4val == 0)
            {
                u4val = pNode->i4IkePhase1HashAlgo = 2;
            }
            i4rc =
                MynmhTest (*Join
                           (FsVpnIkePhase1HashAlgo, 12, FsVpnTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FsVpnIkePhase1HashAlgo\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnIkePhase1HashAlgo, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) u4val);
        }
    }
    if (!name || STRCMP (name, "PolicyIntfIndex") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            pNode->i4PolicyIntfIndex = u4val = value->in_int;
            return SNMP_SUCCESS;
        }

        u4val = pNode->i4PolicyIntfIndex;
        i4rc =
            MynmhTest (*Join
                       (FsVpnPolicyIntfIndex, 12, FsVpnTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetAll: Test failed for FsVpnPolicyIntfIndex\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnPolicyIntfIndex, 12, FsVpnTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
    }
    if (!name || STRCMP (name, "IkePhase1Mode") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            if (pNode->i4PolicyType == TR69_VPN_IPSEC_MANUAL)
            {
                return SNMP_FAILURE;
            }

            pNode->i4IkePhase1Mode = u4val = value->in_int;
            pNode->u4ValMask |= FS_VPN_IKE_PHASE1_MODE_MASK;

            return SNMP_SUCCESS;
        }

        u4val = pNode->i4IkePhase1Mode;
        if ((pNode->i4PolicyType != TR69_VPN_IPSEC_MANUAL)
            && (pNode->i4IkePhase1Mode != TR69_NOT_CONFIG))
        {
            i4rc =
                MynmhTest (*Join
                           (FsVpnIkePhase1Mode, 12, FsVpnTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FsVpnIkePhase1Mode\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }
            i4rc =
                MynmhSet (*Join
                          (FsVpnIkePhase1Mode, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) u4val);
        }
    }
    if (!name || STRCMP (name, "LocalProtectNetwork") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            MEMSET (pNode->au1LocalProtectNetwork, '\0', 16);
            STRCPY (pNode->au1LocalProtectNetwork, value->in_cval);
            if (UtlInetAton ((const CHR1 *) value->in_cval, &IpAddr) == 0)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Invalid value for FsVpnLocalProtectNetwork\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }
            u4val = OSIX_NTOHL (IpAddr.u4Addr);

            pNode->u4ValMask |= FS_VPN_LOCAL_PROTECT_NETWORK_MASK;
            return SNMP_SUCCESS;
        }
        if (UtlInetAton ((CHR1 *) pNode->au1LocalProtectNetwork, &IpAddr) == 0)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetAll: Invalid value for FsVpnLocalProtectNetwork\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");

            return SNMP_FAILURE;
        }
        u4val = OSIX_NTOHL (IpAddr.u4Addr);
        i4rc =
            MynmhTest (*Join
                       (FsVpnLocalProtectNetwork, 12, FsVpnTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_IP_ADDR_PRIM,
                       (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetAll: Test failed for FsVpnLocalProtectNetwork\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnLocalProtectNetwork, 12, FsVpnTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_IP_ADDR_PRIM,
                      (VOID *) u4val);
    }
    if (!name || STRCMP (name, "AntiReplay") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            if ((pNode->i4PolicyType != TR69_VPN_IPSEC_MANUAL)
                && (pNode->i4PolicyType != TR69_NOT_CONFIG))
            {
                return SNMP_FAILURE;
            }

            pNode->i4AntiReplay = u4val = value->in_int;
            pNode->u4ValMask |= FS_VPN_ANTI_REPLAY_MASK;

            return SNMP_SUCCESS;
        }

        u4val = pNode->i4AntiReplay;
        if (pNode->i4PolicyType == TR69_VPN_IPSEC_MANUAL)
        {

            if (u4val == 0)
            {
                u4val = 1;
                pNode->i4AntiReplay = 1;
            }

            i4rc =
                MynmhTest (*Join
                           (FsVpnAntiReplay, 12, FsVpnTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FsVpnAntiReplay\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }
            i4rc =
                MynmhSet (*Join
                          (FsVpnAntiReplay, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) u4val);
        }
    }

    if (!name || STRCMP (name, "PolicyType") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            u4val = value->in_int;

            if (u4val == 1)
            {
                /* Ipsec Manual Tyep */
                pNode->u4GlobalValMask = (VPN_VAL_MASK | VPN_IPSEC_VAL_MASK);
            }
            else if (u4val == 2)
            {
                /* psecIkePreshearedType */
                pNode->u4GlobalValMask = (VPN_VAL_MASK | VPN_IKE_VAL_MASK);
            }
            pNode->u4ValMask |= FS_VPN_POLICY_TYPE_MASK;
            pNode->i4PolicyType = u4val;
            return SNMP_SUCCESS;
        }

        u4val = pNode->i4PolicyType;
        i4rc =
            MynmhTest (*Join
                       (FsVpnPolicyType, 12, FsVpnTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetAll: Test failed for FsVpnPolicyType\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
            return SNMP_FAILURE;
        }
        i4rc =
            MynmhSet (*Join
                      (FsVpnPolicyType, 12, FsVpnTableINDEX, 1, &OctetStrIdx),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);

        if (u4val == 1)
        {
            /* Ipsec Manual Tyep */
            pNode->u4GlobalValMask = (VPN_VAL_MASK | VPN_IPSEC_VAL_MASK);
        }
        else if (u4val == 2)
        {
            /* psecIkePreshearedType */
            pNode->u4GlobalValMask = (VPN_VAL_MASK | VPN_IKE_VAL_MASK);
        }

    }
    if (!name || STRCMP (name, "IkePhase2LifeTimeType") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            if (pNode->i4PolicyType == TR69_VPN_IPSEC_MANUAL)
            {
                return SNMP_FAILURE;
            }
            if (value->in_int == TR_LIFE_TIME_SECS)
            {
                pNode->i4IkePhase2LifeTimeType = u4val = value->in_int;
            }
            else if (value->in_int == TR_LIFE_TIME_MINS)
            {
                pNode->i4IkePhase2LifeTimeType = u4val = LIFE_TIME_MINS;
            }
            else if (value->in_int == TR_LIFE_TIME_HOURS)
            {
                pNode->i4IkePhase2LifeTimeType = u4val = LIFE_TIME_HOURS;
            }
            else if (value->in_int == TR_LIFE_TIME_DAYS)
            {
                pNode->i4IkePhase2LifeTimeType = u4val = LIFE_TIME_DAYS;
            }
            else
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Invalid Phase2LifeTimeType value\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

            pNode->u4ValMask |= FS_VPN_IKE_PHASE2_LIFE_TIME_TYPE_MASK;
            return SNMP_SUCCESS;
        }

        u4val = pNode->i4IkePhase2LifeTimeType;

        if (u4val == 0)
            u4val = pNode->i4IkePhase2LifeTimeType = 1;

        if (pNode->i4PolicyType != TR69_VPN_IPSEC_MANUAL)
        {

            i4rc =
                MynmhTest (*Join
                           (FsVpnIkePhase2LifeTimeType, 12, FsVpnTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FsVpnIkePhase2LifeTimeType\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnIkePhase2LifeTimeType, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) u4val);
        }
    }
    if (!name || STRCMP (name, "IkePhase1LifeTimeType") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            if (pNode->i4PolicyType == TR69_VPN_IPSEC_MANUAL)
            {
                return SNMP_FAILURE;
            }
            if (value->in_int == TR_LIFE_TIME_SECS)
            {
                pNode->i4IkePhase1LifeTimeType = u4val = value->in_int;
            }

            else if (value->in_int == TR_LIFE_TIME_MINS)
            {
                pNode->i4IkePhase1LifeTimeType = u4val = LIFE_TIME_MINS;
            }
            else if (value->in_int == TR_LIFE_TIME_HOURS)
            {
                pNode->i4IkePhase1LifeTimeType = u4val = LIFE_TIME_HOURS;
            }
            else if (value->in_int == TR_LIFE_TIME_DAYS)
            {
                pNode->i4IkePhase1LifeTimeType = u4val = LIFE_TIME_DAYS;
            }
            else
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Invalid Phase1LifeTimeType value\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

            pNode->u4ValMask |= FS_VPN_IKE_PHASE1_LIFE_TIME_TYPE_MASK;
            return SNMP_SUCCESS;
        }

        u4val = pNode->i4IkePhase1LifeTimeType;
        if (pNode->i4PolicyType != TR69_VPN_IPSEC_MANUAL)
        {

            if (u4val == 0)
                u4val = pNode->i4IkePhase2LifeTimeType = 1;

            i4rc =
                MynmhTest (*Join
                           (FsVpnIkePhase1LifeTimeType, 12, FsVpnTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FsVpnIkePhase1LifeTimeType\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnIkePhase1LifeTimeType, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) u4val);
        }
    }
    if (!name || STRCMP (name, "PolicyFlag") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            pNode->i4PolicyFlag = u4val = value->in_int;
            pNode->u4ValMask |= FS_VPN_POLICY_FLAG_MASK;
            return SNMP_SUCCESS;
        }
        if (pNode->i4PolicyFlag != 0)
        {
            u4val = pNode->i4PolicyFlag;
            i4rc =
                MynmhTest (*Join
                           (FsVpnPolicyFlag, 12, FsVpnTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FsVpnPolicyFlag\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnPolicyFlag, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) u4val);
        }
    }
    if (!name || STRCMP (name, "PolicyPriority") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            pNode->i4PolicyPriority = u4val = value->in_int;
            return SNMP_SUCCESS;
        }
        if (pNode->i4PolicyPriority != TR69_NOT_CONFIG)
        {
            u4val = pNode->i4PolicyPriority;
            i4rc =
                MynmhTest (*Join
                           (FsVpnPolicyPriority, 12, FsVpnTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER32,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FsVpnPolicyPriority\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnPolicyPriority, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER32,
                          (VOID *) u4val);
        }
    }
    if (!name || STRCMP (name, "EncrAlgo") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            if ((pNode->i4PolicyType != TR69_VPN_IPSEC_MANUAL)
                && (pNode->i4PolicyType != TR69_NOT_CONFIG))
            {
                return SNMP_FAILURE;
            }
            pNode->i4EncrAlgo = u4val = value->in_int;
            pNode->u4ValMask |= FS_VPN_ENCR_ALGO_MASK;
            return SNMP_SUCCESS;
        }

        if ((pNode->i4EncrAlgo != 0) &&
            (pNode->i4SecurityProtocol == TR69_VPN_AH))
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetAll: Check failed for FsVpnEncrAlgo \n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
            return SNMP_FAILURE;
        }

        u4val = pNode->i4EncrAlgo;
        if (pNode->i4PolicyType == TR69_VPN_IPSEC_MANUAL
            && pNode->i4EncrAlgo != TR69_NOT_CONFIG)
        {
            i4rc =
                MynmhTest (*Join
                           (FsVpnEncrAlgo, 12, FsVpnTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FsVpnEncrAlgo\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnEncrAlgo, 12, FsVpnTableINDEX, 1, &OctetStrIdx),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        }
    }
    if (!name || STRCMP (name, "IkePhase1LifeTime") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            if (pNode->i4PolicyType == TR69_VPN_IPSEC_MANUAL)
            {
                return SNMP_FAILURE;
            }

            pNode->i4IkePhase1LifeTime = u4val = value->in_int;
            pNode->u4ValMask |= FS_VPN_IKE_PHASE1_LIFE_TIME_MASK;
            return SNMP_SUCCESS;
        }

        u4val = pNode->i4IkePhase1LifeTime;
        if (pNode->i4PolicyType != TR69_VPN_IPSEC_MANUAL)
        {

            if (u4val == 0)
                u4val = pNode->i4IkePhase1LifeTime = 2400;

            i4rc =
                MynmhTest (*Join
                           (FsVpnIkePhase1LifeTime, 12, FsVpnTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER32,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FsVpnIkePhase1LifeTime\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnIkePhase1LifeTime, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER32,
                          (VOID *) u4val);
        }
    }
    if (!name || STRCMP (name, "RemoteProtectNetwork") == 0)
    {

        if (!bTrDoSnmpSet)
        {
            MEMSET (pNode->au1RemoteProtectNetwork, '\0', 16);
            STRCPY (pNode->au1RemoteProtectNetwork, value->in_cval);
            if (UtlInetAton ((const CHR1 *) value->in_cval, &IpAddr) == 0)
            {
                return SNMP_FAILURE;
            }
            u4val = OSIX_NTOHL (IpAddr.u4Addr);
            pNode->u4ValMask |= FS_VPN_REMOTE_PROTECT_NETWORK_MASK;
            return SNMP_SUCCESS;
        }
        UtlInetAton ((CHR1 *) pNode->au1RemoteProtectNetwork, &IpAddr);
        u4val = OSIX_NTOHL (IpAddr.u4Addr);
        i4rc =
            MynmhTest (*Join
                       (FsVpnRemoteProtectNetwork, 12, FsVpnTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_IP_ADDR_PRIM,
                       (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetAll: Test failed for FsVpnRemoteProtectNetwork\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnRemoteProtectNetwork, 12, FsVpnTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_IP_ADDR_PRIM,
                      (VOID *) u4val);
    }
    if (!name || STRCMP (name, "IkePhase2DHGroup") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            if (pNode->i4PolicyType == TR69_VPN_IPSEC_MANUAL)
            {
                return SNMP_FAILURE;
            }
            pNode->i4IkePhase2DHGroup = u4val = value->in_int;
            pNode->u4ValMask |= FS_VPN_IKE_PHASE2_DH_GROUP_MASK;
            return SNMP_SUCCESS;
        }

        u4val = pNode->i4IkePhase2DHGroup;
        if (pNode->i4PolicyType != TR69_VPN_IPSEC_MANUAL)
        {
            if (u4val != 0)
            {
                i4rc =
                    MynmhTest (*Join
                               (FsVpnIkePhase2DHGroup, 12, FsVpnTableINDEX, 1,
                                &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                               (VOID *) u4val);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E- TrSetAll: Test failed for FsVpnIkePhase2DHGroup\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                    return SNMP_FAILURE;
                }

                i4rc =
                    MynmhSet (*Join
                              (FsVpnIkePhase2DHGroup, 12, FsVpnTableINDEX, 1,
                               &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                              (VOID *) u4val);
            }
        }
    }
    if (!name || STRCMP (name, "LocalProtectSubnetMask") == 0)
    {

        if (!bTrDoSnmpSet)
        {
            MEMSET (pNode->au1LocalProtectSubnetMask, '\0', 16);
            STRCPY (pNode->au1LocalProtectSubnetMask, value->in_cval);
            UtlInetAton ((const CHR1 *) value->in_cval, &IpAddr);
            u4val = OSIX_NTOHL (IpAddr.u4Addr);
            pNode->u4ValMask |= FS_VPN_LOCAL_PROTECT_SUBNET_MASK_MASK;
            return SNMP_SUCCESS;
        }
        UtlInetAton ((CHR1 *) pNode->au1LocalProtectSubnetMask, &IpAddr);
        u4val = OSIX_NTOHL (IpAddr.u4Addr);
        i4rc =
            MynmhTest (*Join
                       (FsVpnLocalProtectSubnetMask, 12, FsVpnTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_IP_ADDR_PRIM,
                       (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetAll: Test failed for FsVpnLocalProtectSubnetMask\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnLocalProtectSubnetMask, 12, FsVpnTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_IP_ADDR_PRIM,
                      (VOID *) u4val);
    }
    if (!name || STRCMP (name, "RemoteTunTermAddr") == 0)
    {

        if (!bTrDoSnmpSet)
        {
            MEMSET (pNode->au1RemoteTunTermAddr, '\0', 16);
            STRCPY (pNode->au1RemoteTunTermAddr, value->in_cval);
            UtlInetAton ((const CHR1 *) value->in_cval, &IpAddr);
            u4val = OSIX_NTOHL (IpAddr.u4Addr);
            pNode->u4ValMask |= FS_VPN_REMOTE_TUN_TERM_ADDR_MASK;
            return SNMP_SUCCESS;
        }
        UtlInetAton ((CHR1 *) pNode->au1RemoteTunTermAddr, &IpAddr);
        u4val = OSIX_NTOHL (IpAddr.u4Addr);
        i4rc =
            MynmhTest (*Join
                       (FsVpnRemoteTunTermAddr, 12, FsVpnTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_IP_ADDR_PRIM,
                       (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetAll: Test failed for FsVpnRemoteTunTermAddr\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnRemoteTunTermAddr, 12, FsVpnTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_IP_ADDR_PRIM,
                      (VOID *) u4val);
    }
    if (!name || STRCMP (name, "IkePhase1EncryptionAlgo") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            if (pNode->i4PolicyType == TR69_VPN_IPSEC_MANUAL)
            {
                return SNMP_FAILURE;
            }
            pNode->i4IkePhase1EncryptionAlgo = u4val = value->in_int;
            pNode->u4ValMask |= FS_VPN_IKE_PHASE1_ENCRYPTION_ALGO_MASK;
            return SNMP_SUCCESS;
        }

        u4val = pNode->i4IkePhase1EncryptionAlgo;
        if (pNode->i4PolicyType != TR69_VPN_IPSEC_MANUAL)
        {

            if (u4val == 0)
            {
                u4val = pNode->i4IkePhase1EncryptionAlgo = 4;
            }
            i4rc =
                MynmhTest (*Join
                           (FsVpnIkePhase1EncryptionAlgo, 12, FsVpnTableINDEX,
                            1, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FsVpnIkePhase1EncryptionAlgo\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnIkePhase1EncryptionAlgo, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) u4val);
        }
    }

    if (!name || STRCMP (name, "SecurityProtocol") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            pNode->i4SecurityProtocol = u4val = value->in_int;
            pNode->u4ValMask |= FS_VPN_SECURITY_PROTOCOL_MASK;
            return SNMP_SUCCESS;
        }

        if (pNode->i4PolicyType == TR69_VPN_IPSEC_MANUAL)
        {
            if ((pNode->i4EncrAlgo != 0) &&
                (pNode->i4SecurityProtocol == TR69_VPN_AH))
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Check failed for FsVpnSecurityProtocol\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }
        }
        else
        {
            if ((pNode->i4IkePhase2EspEncryptionAlgo != 0) &&
                (pNode->i4SecurityProtocol == TR69_VPN_AH))
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Check failed for FsVpnSecurityProtocol\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }
        }

        u4val = pNode->i4SecurityProtocol;
        i4rc =
            MynmhTest (*Join
                       (FsVpnSecurityProtocol, 12, FsVpnTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetAll: Test failed for FsVpnSecurityProtocol\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnSecurityProtocol, 12, FsVpnTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
    }
    if (!name || STRCMP (name, "AuthAlgo") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            if ((pNode->i4PolicyType != TR69_VPN_IPSEC_MANUAL)
                && (pNode->i4PolicyType != TR69_NOT_CONFIG))
            {
                return SNMP_FAILURE;
            }
            pNode->i4AuthAlgo = u4val = value->in_int;
            pNode->u4ValMask |= FS_VPN_AUTH_ALGO_MASK;
            return SNMP_SUCCESS;
        }

        u4val = pNode->i4AuthAlgo;
        if ((pNode->i4PolicyType == TR69_VPN_IPSEC_MANUAL)
            && (pNode->i4AuthAlgo != TR69_NOT_CONFIG))
        {
            i4rc =
                MynmhTest (*Join
                           (FsVpnAuthAlgo, 12, FsVpnTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FsVpnAuthAlgo\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnAuthAlgo, 12, FsVpnTableINDEX, 1, &OctetStrIdx),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        }
    }
    if (!name || STRCMP (name, "EspKey") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            if ((pNode->i4PolicyType != TR69_VPN_IPSEC_MANUAL)
                && (pNode->i4PolicyType != TR69_NOT_CONFIG))
            {
                return SNMP_FAILURE;
            }
            MEMSET (pNode->au1EspKey, '\0', VPN_ESP_KEY);
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1EspKey, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);
            pNode->u4ValMask |= FS_VPN_ESP_KEY_MASK;
            return SNMP_SUCCESS;
        }

        OctetStr.i4_Length = STRLEN (pNode->au1EspKey);
        MEMCPY (OctetStr.pu1_OctetList, pNode->au1EspKey, OctetStr.i4_Length);
        if (pNode->i4PolicyType == TR69_VPN_IPSEC_MANUAL)
        {
            if (OctetStr.i4_Length != 0)
            {
                i4rc =
                    MynmhTest (*Join
                               (FsVpnEspKey, 12, FsVpnTableINDEX, 1,
                                &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                               (VOID *) &OctetStr);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E- TrSetAll: Test failed for FsVpnEspKey\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                    return SNMP_FAILURE;
                }

                i4rc =
                    MynmhSet (*Join
                              (FsVpnEspKey, 12, FsVpnTableINDEX, 1,
                               &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                              (VOID *) &OctetStr);
            }
        }
    }
    if (!name || STRCMP (name, "AhKey") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            if ((pNode->i4PolicyType != TR69_VPN_IPSEC_MANUAL)
                && (pNode->i4PolicyType != TR69_NOT_CONFIG))
            {
                return SNMP_FAILURE;
            }
            MEMSET (pNode->au1AhKey, '\0', VPN_AH_KEY);
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1AhKey, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);
            pNode->u4ValMask |= FS_VPN_AH_KEY_MASK;
            return SNMP_SUCCESS;
        }

        OctetStr.i4_Length = STRLEN (pNode->au1AhKey);
        MEMCPY (OctetStr.pu1_OctetList, pNode->au1AhKey, OctetStr.i4_Length);
        if (pNode->i4PolicyType == TR69_VPN_IPSEC_MANUAL)
        {

            if (OctetStr.i4_Length != 0)
            {
                i4rc =
                    MynmhTest (*Join
                               (FsVpnAhKey, 12, FsVpnTableINDEX, 1,
                                &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                               (VOID *) &OctetStr);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E- TrSetAll: Test failed for FsVpnAhKey\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                    return SNMP_FAILURE;
                }

                i4rc =
                    MynmhSet (*Join
                              (FsVpnAhKey, 12, FsVpnTableINDEX, 1,
                               &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                              (VOID *) &OctetStr);
            }
        }
    }
    if (!name || STRCMP (name, "Mode") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            pNode->i4Mode = u4val = value->in_int;
            pNode->u4ValMask |= FS_VPN_MODE_MASK;
            return SNMP_SUCCESS;
        }

        u4val = pNode->i4Mode;
        if (pNode->i4Mode != TR69_NOT_CONFIG)
        {
            i4rc =
                MynmhTest (*Join
                           (FsVpnMode, 12, FsVpnTableINDEX, 1, &OctetStrIdx),
                           SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FsVpnMode\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnMode, 12, FsVpnTableINDEX, 1, &OctetStrIdx),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        }
    }
    if (!name || STRCMP (name, "RemoteProtectSubnetMask") == 0)
    {

        if (!bTrDoSnmpSet)
        {
            MEMSET (pNode->au1RemoteProtectSubnetMask, '\0', 16);
            STRCPY (pNode->au1RemoteProtectSubnetMask, value->in_cval);
            UtlInetAton ((const CHR1 *) value->in_cval, &IpAddr);
            u4val = OSIX_NTOHL (IpAddr.u4Addr);
            pNode->u4ValMask |= FS_VPN_REMOTE_PROTECT_SUBNET_MASK_MASK;

            return SNMP_SUCCESS;
        }
        UtlInetAton ((CHR1 *) pNode->au1RemoteProtectSubnetMask, &IpAddr);
        u4val = OSIX_NTOHL (IpAddr.u4Addr);
        i4rc =
            MynmhTest (*Join
                       (FsVpnRemoteProtectSubnetMask, 12, FsVpnTableINDEX,
                        1, &OctetStrIdx), SNMP_DATA_TYPE_IP_ADDR_PRIM,
                       (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetAll: Test failed for FsVpnRemoteProtectSubnetMask\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
            return SNMP_FAILURE;
        }
        i4rc =
            MynmhSet (*Join
                      (FsVpnRemoteProtectSubnetMask, 12, FsVpnTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_IP_ADDR_PRIM,
                      (VOID *) u4val);
    }
    if (!name || STRCMP (name, "IkePhase1PeerIdValue") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            if (pNode->i4PolicyType == TR69_VPN_IPSEC_MANUAL)
            {
                return SNMP_FAILURE;
            }
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMSET (pNode->au1IkePhase1PeerIdValue, '\0',
                    VPN_IKE_PHASE1_PEER_ID_VALUE);
            MEMSET (au1OctetList, '\0', MAX_OCTETSTRING_SIZE);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1IkePhase1PeerIdValue, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);
            pNode->u4ValMask |= FS_VPN_IKE_PHASE1_PEER_ID_VALUE_MASK;
            return SNMP_SUCCESS;
        }

        OctetStr.i4_Length = STRLEN (pNode->au1IkePhase1PeerIdValue);
        MEMCPY (OctetStr.pu1_OctetList, pNode->au1IkePhase1PeerIdValue,
                OctetStr.i4_Length);
        if (pNode->i4PolicyType != TR69_VPN_IPSEC_MANUAL)
        {

            if (OctetStr.i4_Length != 0)
            {

                i4rc =
                    MynmhTest (*Join
                               (FsVpnIkePhase1PeerIdValue, 12, FsVpnTableINDEX,
                                1, &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                               (VOID *) &OctetStr);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E- TrSetAll: Test failed for FsVpnIkePhase1PeerIdValue\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                    return SNMP_FAILURE;
                }

                i4rc =
                    MynmhSet (*Join
                              (FsVpnIkePhase1PeerIdValue, 12, FsVpnTableINDEX,
                               1, &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                              (VOID *) &OctetStr);
            }
        }
    }
    if (!name || STRCMP (name, "IkePhase2LifeTime") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            if (pNode->i4PolicyType == TR69_VPN_IPSEC_MANUAL)
            {
                return SNMP_FAILURE;
            }
            pNode->i4IkePhase2LifeTime = u4val = value->in_int;
            pNode->u4ValMask |= FS_VPN_IKE_PHASE2_LIFE_TIME_MASK;
            return SNMP_SUCCESS;
        }

        u4val = pNode->i4IkePhase2LifeTime;

        if (u4val == 0)
            u4val = pNode->i4IkePhase2LifeTime = 800;

        if (pNode->i4PolicyType != TR69_VPN_IPSEC_MANUAL)
        {
            i4rc =
                MynmhTest (*Join
                           (FsVpnIkePhase2LifeTime, 12, FsVpnTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER32,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FsVpnIkePhase2LifeTime\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnIkePhase2LifeTime, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER32,
                          (VOID *) u4val);
        }
    }
    if (!name || STRCMP (name, "Protocol") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            pNode->i4Protocol = u4val = value->in_int;
            pNode->u4ValMask |= FS_VPN_PROTOCOL_MASK;
            return SNMP_SUCCESS;
        }

        u4val = pNode->i4Protocol;
        if (u4val != TR69_NOT_CONFIG)
        {
            i4rc =
                MynmhTest (*Join
                           (FsVpnProtocol, 12, FsVpnTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FsVpnProtocol\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnProtocol, 12, FsVpnTableINDEX, 1, &OctetStrIdx),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        }
    }
    if (!name || STRCMP (name, "IkePhase2AuthAlgo") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            if (pNode->i4PolicyType == TR69_VPN_IPSEC_MANUAL)
            {
                return SNMP_FAILURE;
            }
            pNode->i4IkePhase2AuthAlgo = u4val = value->in_int;
            pNode->u4ValMask |= FS_VPN_IKE_PHASE2_AUTH_ALGO_MASK;
            return SNMP_SUCCESS;
        }

        u4val = pNode->i4IkePhase2AuthAlgo;
        if ((pNode->i4PolicyType != TR69_VPN_IPSEC_MANUAL)
            && (pNode->i4IkePhase2AuthAlgo != 0))
        {
            i4rc =
                MynmhTest (*Join
                           (FsVpnIkePhase2AuthAlgo, 12, FsVpnTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FsVpnIkePhase2AuthAlgo\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnIkePhase2AuthAlgo, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) u4val);
        }
    }
    if (!name || STRCMP (name, "IkePhase2EspEncryptionAlgo") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            if (pNode->i4PolicyType == TR69_VPN_IPSEC_MANUAL)
            {
                return SNMP_FAILURE;
            }
            pNode->i4IkePhase2EspEncryptionAlgo = u4val = value->in_int;
            pNode->u4ValMask |= FS_VPN_IKE_PHASE2_ESP_ENCRYPTION_ALGO_MASK;
            return SNMP_SUCCESS;
        }

        if ((pNode->i4IkePhase2EspEncryptionAlgo != 0) &&
            (pNode->i4SecurityProtocol == TR69_VPN_AH))
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetAll: Check failed for FsVpnIkePhase2EspEncryptionAlgo\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
            return SNMP_FAILURE;
        }

        u4val = pNode->i4IkePhase2EspEncryptionAlgo;
        if (pNode->i4PolicyType != TR69_VPN_IPSEC_MANUAL
            && pNode->i4IkePhase2EspEncryptionAlgo != 0)
        {
            i4rc =
                MynmhTest (*Join
                           (FsVpnIkePhase2EspEncryptionAlgo, 12,
                            FsVpnTableINDEX, 1, &OctetStrIdx),
                           SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FsVpnIkePhase2EspEncryptionAlgo\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnIkePhase2EspEncryptionAlgo, 12,
                           FsVpnTableINDEX, 1, &OctetStrIdx),
                          SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        }
    }
    if (!name || STRCMP (name, "OutboundSpi") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            if ((pNode->i4PolicyType != TR69_VPN_IPSEC_MANUAL)
                && (pNode->i4PolicyType != TR69_NOT_CONFIG))
            {
                return SNMP_FAILURE;
            }
            pNode->i4OutboundSpi = u4val = value->in_int;
            pNode->u4ValMask |= FS_VPN_OUTBOUND_SPI_MASK;
            return SNMP_SUCCESS;
        }

        u4val = pNode->i4OutboundSpi;
        if (pNode->i4PolicyType == TR69_VPN_IPSEC_MANUAL)
        {

            i4rc =
                MynmhTest (*Join
                           (FsVpnOutboundSpi, 12, FsVpnTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FsVpnOutboundSpi\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnOutboundSpi, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) u4val);
        }
    }

    if (!name || STRCMP (name, "InboundSpi") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            if ((pNode->i4PolicyType != TR69_VPN_IPSEC_MANUAL)
                && (pNode->i4PolicyType != TR69_NOT_CONFIG))
            {
                return SNMP_FAILURE;
            }
            pNode->i4InboundSpi = u4val = value->in_int;
            pNode->u4ValMask |= FS_VPN_INBOUND_SPI_MASK;
            return SNMP_SUCCESS;
        }

        u4val = pNode->i4InboundSpi;
        if (pNode->i4PolicyType == TR69_VPN_IPSEC_MANUAL)
        {

            i4rc =
                MynmhTest (*Join
                           (FsVpnInboundSpi, 12, FsVpnTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FsVpnInboundSpi\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnInboundSpi, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) u4val);
        }
    }
    if (!name || STRCMP (name, "IpComp") == 0)
    {
        if (!bTrDoSnmpSet)
        {

            TR69_TRC (TR69_DBG_TRC,
                      "-E- TrSetAll: Test failed for FsVpnIpComp : Parameter Not Supported\n");
            return SNMP_SUCCESS;
        }
    }
    if (!name || STRCMP (name, "IkeRAInfoProtectedNetBundle") == 0)
    {
        if (!bTrDoSnmpSet)
        {

            TR69_TRC (TR69_DBG_TRC,
                      "-E- Test failed for FsVpnIkeRAInfoProtectedNetBundle : Parameter Not Supported\n");
            return SNMP_SUCCESS;
        }

    }
    if (!name || STRCMP (name, "IkePhase1LocalIdType") == 0)
    {

        if (!bTrDoSnmpSet)
        {
            if (pNode->i4PolicyType == TR69_VPN_IPSEC_MANUAL)
            {
                return SNMP_FAILURE;
            }
            if (STRCASECMP (value->in_cval, "ipv4") == 0)
            {
                u4val = 1;
            }
            else if (STRCASECMP (value->in_cval, "email") == 0)
            {
                u4val = 3;
            }
            else if (STRCASECMP (value->in_cval, "fqdn") == 0)
            {
                u4val = 2;
            }
            else if (STRCASECMP (value->in_cval, "keyId") == 0)
            {
                u4val = 11;
            }
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMSET (pNode->au1IkePhase1LocalIdType, '\0',
                    VPN_IKE_PHASE1_LOCAL_ID_TYPE);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1IkePhase1LocalIdType, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);

            pNode->u4ValMask |= FS_VPN_IKE_PHASE1_LOCAL_ID_TYPE_MASK;
            return SNMP_SUCCESS;
        }
        if (pNode->i4PolicyType != TR69_VPN_IPSEC_MANUAL)
        {
            if (STRCASECMP (pNode->au1IkePhase1LocalIdType, "ipv4") == 0)
            {
                u4val = 1;
            }
            else if (STRCASECMP (pNode->au1IkePhase1LocalIdType, "email") == 0)
            {
                u4val = 3;
            }
            else if (STRCASECMP (pNode->au1IkePhase1LocalIdType, "fqdn") == 0)
            {
                u4val = 2;
            }
            else if (STRCASECMP (pNode->au1IkePhase1LocalIdType, "keyId") == 0)
            {
                u4val = 11;
            }

            i4rc =
                MynmhTest (*Join
                           (FsVpnIkePhase1LocalIdType, 12, FsVpnTableINDEX, 1,
                            &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                           (VOID *) u4val);
            if (i4rc == SNMP_FAILURE)
            {
                TR69_TRC (TR69_DBG_TRC,
                          "-E- TrSetAll: Test failed for FsVpnIkePhase1LocalIdType\n");
                TR69_TRC (TR69_ENTRY_EXIT_TRC,
                          "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                return SNMP_FAILURE;
            }

            i4rc =
                MynmhSet (*Join
                          (FsVpnIkePhase1LocalIdType, 12, FsVpnTableINDEX, 1,
                           &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                          (VOID *) u4val);
        }
    }
    if (!name || STRCMP (name, "IkePhase1LocalIdValue") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            if (pNode->i4PolicyType == TR69_VPN_IPSEC_MANUAL)
            {
                return SNMP_FAILURE;
            }
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMSET (pNode->au1IkePhase1LocalIdValue, '\0',
                    VPN_IKE_PHASE1_LOCAL_ID_VALUE);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1IkePhase1LocalIdValue, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);
            pNode->u4ValMask |= FS_VPN_IKE_PHASE1_LOCAL_ID_VALUE_MASK;
            return SNMP_SUCCESS;
        }
        OctetStr.i4_Length = STRLEN (pNode->au1IkePhase1LocalIdValue);
        MEMCPY (OctetStr.pu1_OctetList, pNode->au1IkePhase1LocalIdValue,
                OctetStr.i4_Length);
        if (pNode->i4PolicyType != TR69_VPN_IPSEC_MANUAL)
        {
            if (OctetStr.i4_Length != 0)
            {

                i4rc =
                    MynmhTest (*Join
                               (FsVpnIkePhase1LocalIdValue, 12, FsVpnTableINDEX,
                                1, &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                               (VOID *) &OctetStr);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E- TrSetAll: Test failed for FsVpnIkePhase1LocalIdValue\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
                    return SNMP_FAILURE;
                }

                i4rc =
                    MynmhSet (*Join
                              (FsVpnIkePhase1LocalIdValue, 12, FsVpnTableINDEX,
                               1, &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                              (VOID *) &OctetStr);
            }
        }
    }
    if (!name || STRCMP (name, "PolicyName") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            MEMSET (pNode->au1PolicyName, '\0', VPN_POLICY_NAME);
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1PolicyName, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - SUCCESS\n");
            return SNMP_SUCCESS;
        }
    }
    if (!name || STRCMP (name, "PolicyRowStatus") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            if (1 == value->in_int)
            {
                pNode->u4PolicyRowStatus = u4val = TR_ENABLE_ROWSTATUS;
            }
            else
            {
                pNode->u4PolicyRowStatus = u4val = TR_DISABLE_ROWSTATUS;
            }
            return SNMP_SUCCESS;
        }

        u4val = pNode->u4PolicyRowStatus;
        i4rc =
            MynmhTest (*Join
                       (FsVpnPolicyRowStatus, 12, FsVpnTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- Test failed for FsVpnPolicyRowStatus\n");
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - FAILURE\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnPolicyRowStatus, 12, FsVpnTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN - SUCCESS\n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrSetAll_InternetGatewayDevice_RAVPN_User
 *  Description     : Set function for all objects of a table.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tUser entry.
 *                    bTrDoSnmpSet - Indicates whether SNMP Set is to be done.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetAll_InternetGatewayDevice_RAVPN_User (char *name, tUser * pNode,
                                           ParameterValue * value,
                                           BOOL1 bTrDoSnmpSet)
{

    INT4                i4rc = 0;
    UINT4               u4val = 0;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];

    /* INDICES: */
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetAll_InternetGatewayDevice_RAVPN_User\n");
    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStr.pu1_OctetList = au1OctetList;

    OctetStrIdx.i4_Length = STRLEN (pNode->au1Name);
    MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1Name, OctetStrIdx.i4_Length);

    if (!name || STRCMP (name, "Name") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            MEMSET (pNode->au1Name, '\0', USER_NAME);
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1Name, OctetStr.pu1_OctetList, OctetStr.i4_Length);
            pNode->u4ValMask |= FS_VPN_RA_USER_NAME_MASK;
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_RAVPN_User Caching - SUCCESS\n");
            return SNMP_SUCCESS;
        }
    }
    if (!name || STRCMP (name, "Secret") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            MEMSET (pNode->au1Secret, '\0', USER_SECRET);
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1Secret, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);
            pNode->u4ValMask |= FS_VPN_RA_USER_SECRET_MASK;
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_RAVPN_User Caching - SUCCESS\n");
            return SNMP_SUCCESS;
        }
        OctetStr.i4_Length = STRLEN (pNode->au1Secret);
        MEMCPY (OctetStr.pu1_OctetList, pNode->au1Secret, OctetStr.i4_Length);
        i4rc =
            MynmhTest (*Join
                       (FsVpnRaUserSecret, 12, FsVpnRaUsersTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                       (VOID *) &OctetStr);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetAll: Test failed for FsVpnRaUserSecret %s\n",
                           OctetStr.pu1_OctetList);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_RAVPN_User - FAILURE\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnRaUserSecret, 12, FsVpnRaUsersTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                      (VOID *) &OctetStr);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetAll: Set failed for FsVpnRaUserSecret %s\n",
                           OctetStr.pu1_OctetList);

            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_RAVPN_User - FAILURE\n");
            return SNMP_FAILURE;
        }
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "TrSetAll: Set success for FsVpnRaUserSecret %s\n",
                       OctetStr.pu1_OctetList);

    }
    if (!name || STRCMP (name, "RowStatus") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            pNode->u4RowStatus = u4val = value->in_uint;
            pNode->u4ValMask |= FS_VPN_RA_USER_ROW_STATUS_MASK;
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_RAVPN_User Caching - SUCCESS\n");

            return SNMP_SUCCESS;
        }

        u4val = pNode->u4RowStatus;
        i4rc =
            MynmhTest (*Join
                       (FsVpnRaUserRowStatus, 12, FsVpnRaUsersTableINDEX, 1,
                        &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetAll: Test failed for FsVpnRaUserRowStatus %d\n",
                           u4val);

            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_RAVPN_User - FAILURE\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnRaUserRowStatus, 12, FsVpnRaUsersTableINDEX, 1,
                       &OctetStrIdx), SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetAll: Set failed for FsVpnRaUserRowStatus %d\n",
                           u4val);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_RAVPN_User - FAILURE\n");
            return SNMP_FAILURE;
        }
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "TrSetAll: Set success for FsVpnRaUserRowStatus %d\n",
                       u4val);

    }
    /* Set the NonMand objects */
    /*TrSetNonMand_InternetGatewayDevice_RAVPN_User (name, value, pNode); */
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetAll_InternetGatewayDevice_RAVPN_User - SUCCESS\n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrSetAll_InternetGatewayDevice_RAVPN_AddressPool
 *  Description     : Set function for all objects of a table.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tAddressPool entry.
 *                    bTrDoSnmpSet - Indicates whether SNMP Set is to be done.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetAll_InternetGatewayDevice_RAVPN_AddressPool (char *name,
                                                  tAddressPool * pNode,
                                                  ParameterValue * value,
                                                  BOOL1 bTrDoSnmpSet)
{

    INT4                i4rc = 0;
    UINT4               u4val = 0;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];

    /* INDICES: */
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];
    tUtlInAddr          IpAddr;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetAll_InternetGatewayDevice_RAVPN_AddressPool\n");
    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStr.pu1_OctetList = au1OctetList;

    /* TOCHECK Assign the indices from pNode. */
    OctetStrIdx.i4_Length = STRLEN (pNode->au1Name);
    MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1Name, OctetStrIdx.i4_Length);
    if (!name || STRCMP (name, "Name") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMSET (pNode->au1Name, '\0', ADDRESSPOOL_NAME);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1Name, OctetStr.pu1_OctetList, OctetStr.i4_Length);
            pNode->u4ValMask |= FS_VPN_RA_ADDRESS_POOL_NAME_MASK;
            return SNMP_SUCCESS;
        }
    }
    if (!name || STRCMP (name, "StartAddress") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            /* TOCHECK: Convert value->in_cval to u4val */
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMSET (pNode->au1StartAddress, 0, ADDRESSPOOL_START_ADDRESS);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1StartAddress, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);
            pNode->u4ValMask |= FS_VPN_RA_ADDRESS_POOL_START_MASK;
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_RAVPN_AddressPool StartAddress Caching - SUCCESS\n");
            return SNMP_SUCCESS;
        }

        /* TOCHECK: Convert value->in_cval to u4val */
        if (UtlInetAton ((const CHR1 *) pNode->au1StartAddress, &IpAddr) == 0)
            return SNMP_FAILURE;
        u4val = OSIX_NTOHL (IpAddr.u4Addr);

        i4rc =
            MynmhTest (*Join
                       (FsVpnRaAddressPoolStart, 12,
                        FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                       SNMP_DATA_TYPE_IP_ADDR_PRIM, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetAll: Test failed for FsVpnRaAddressPoolStart %d\n",
                           u4val);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_RAVPN_AddressPool - FAILURE\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnRaAddressPoolStart, 12,
                       FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                      SNMP_DATA_TYPE_IP_ADDR_PRIM, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetAll: Set failed for FsVpnRaAddressPoolStart %d\n",
                           u4val);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_RAVPN_AddressPool - FAILURE\n");

            return SNMP_FAILURE;
        }
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "TrSetAll: Set success for FsVpnRaAddressPoolStart %d\n",
                       u4val);

    }
    if (!name || STRCMP (name, "EndAddress") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            /* TOCHECK: Convert value->in_cval to u4val */
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMSET (pNode->au1EndAddress, 0, ADDRESSPOOL_END_ADDRESS);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMCPY (pNode->au1EndAddress, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);

            pNode->u4ValMask |= FS_VPN_RA_ADDRESS_POOL_END_MASK;
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_RAVPN_AddressPool EndAddress Caching - SUCCESS\n");

            return SNMP_SUCCESS;
        }

        if (UtlInetAton ((CHR1 *) pNode->au1EndAddress, &IpAddr) == 0)
            return SNMP_FAILURE;
        u4val = OSIX_NTOHL (IpAddr.u4Addr);

        i4rc =
            MynmhTest (*Join
                       (FsVpnRaAddressPoolEnd, 12, FsVpnRaAddressPoolTableINDEX,
                        1, &OctetStrIdx), SNMP_DATA_TYPE_IP_ADDR_PRIM,
                       (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetAll: Test failed for FsVpnRaAddressPoolEnd %d\n",
                           u4val);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_RAVPN_AddressPool - FAILURE\n");

            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnRaAddressPoolEnd, 12, FsVpnRaAddressPoolTableINDEX,
                       1, &OctetStrIdx), SNMP_DATA_TYPE_IP_ADDR_PRIM,
                      (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetAll: Set failed for FsVpnRaAddressPoolEnd %d\n",
                           u4val);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_RAVPN_AddressPool - FAILURE\n");

            return SNMP_FAILURE;
        }
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "TrSetAll: Set success for FsVpnRaAddressPoolEnd %d\n",
                       u4val);

    }
    if (!name || STRCMP (name, "RowStatus") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            pNode->u4RowStatus = u4val = value->in_uint;
            pNode->u4ValMask |= FS_VPN_RA_ADDRESS_POOL_ROW_STATUS_MASK;
            return SNMP_SUCCESS;
        }

        u4val = pNode->u4RowStatus;
        i4rc =
            MynmhTest (*Join
                       (FsVpnRaAddressPoolRowStatus, 12,
                        FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                       SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetAll: Test failed for FsVpnRaAddressPoolRowStatus %d\n",
                           u4val);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_RAVPN_AddressPool - FAILURE\n");

            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnRaAddressPoolRowStatus, 12,
                       FsVpnRaAddressPoolTableINDEX, 1, &OctetStrIdx),
                      SNMP_DATA_TYPE_INTEGER, (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC_ARG1 (TR69_DBG_TRC,
                           "-E- TrSetAll: Set failed for FsVpnRaAddressPoolRowStatus %d\n",
                           u4val);
            TR69_TRC (TR69_ENTRY_EXIT_TRC,
                      "Exit: TrSetAll_InternetGatewayDevice_RAVPN_AddressPool - FAILURE\n");

            return SNMP_FAILURE;
        }
        TR69_TRC_ARG1 (TR69_DBG_TRC,
                       "TrSetAll: Set success for FsVpnRaAddressPoolRowStatus %d\n",
                       u4val);

    }
    /* Set the NonMand objects */
    /*TrSetNonMand_InternetGatewayDevice_RAVPN_AddressPool (name, value, pNode); */
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrSetAll_InternetGatewayDevice_RAVPN_AddressPool - SUCCESS\n");

    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrSetAll_InternetGatewayDevice_VpnRemoteId
 *  Description     : Set function for all objects of a table.
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *                    pNode - Pointer to a tVpnRemoteId entry.
 *                    bTrDoSnmpSet - Indicates whether SNMP Set is to be done.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrSetAll_InternetGatewayDevice_VpnRemoteId (char *name, tVpnRemoteId * pNode,
                                            ParameterValue * value,
                                            BOOL1 bTrDoSnmpSet)
{

    INT4                i4rc = 0;
    UINT4               u4val = 0;
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];

    /* INDICES: */
    UINT4               u4Idx0 = 0;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrSetAll_InternetGatewayDevice_VpnRemoteId\n");
    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStr.pu1_OctetList = au1OctetList;

    OctetStrIdx.i4_Length = STRLEN (pNode->au1Value);
    MEMCPY (OctetStrIdx.pu1_OctetList, pNode->au1Value, OctetStrIdx.i4_Length);
    u4Idx0 = (UINT4) pNode->i4Type;

    if (!name || STRCMP (name, "Value") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMSET (pNode->au1Value, '\0', VPNREMOTEID_VALUE);
            MEMCPY (pNode->au1Value, OctetStr.pu1_OctetList,
                    OctetStr.i4_Length);
            pNode->u4ValMask |= FS_VPN_REMOTE_ID_VALUE_MASK;
            return SNMP_SUCCESS;
        }
    }
    if (!name || STRCMP (name, "Type") == 0)
    {
        if (!bTrDoSnmpSet)
        {

            if (STRCASECMP (value->in_cval, "ipv4") == 0)
            {
                pNode->i4Type = 1;
                MEMCPY (pNode->au1Type, "IPv4", 4);

            }
            else if (STRCASECMP (value->in_cval, "email") == 0)
            {
                MEMCPY (pNode->au1Type, "EMAIL", 5);
                pNode->i4Type = 3;
            }
            else if (STRCASECMP (value->in_cval, "fqdn") == 0)
            {

                pNode->i4Type = 2;
                MEMCPY (pNode->au1Type, "FQDN", 4);

            }
            else if (STRCASECMP (value->in_cval, "keyId") == 0)
            {
                pNode->i4Type = 11;
                MEMCPY (pNode->au1Type, "KEYID", 5);

            }
            else
                return SNMP_FAILURE;

            pNode->u4ValMask |= FS_VPN_REMOTE_ID_TYPE_MASK;
            return SNMP_SUCCESS;
        }
    }

    if (!name || STRCMP (name, "Key") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            OctetStr.i4_Length = STRLEN (value->in_cval);
            MEMCPY (OctetStr.pu1_OctetList, value->in_cval, OctetStr.i4_Length);
            MEMSET (pNode->au1Key, '\0', VPNREMOTEID_KEY);
            MEMCPY (pNode->au1Key, OctetStr.pu1_OctetList, OctetStr.i4_Length);
            pNode->u4ValMask |= FS_VPN_REMOTE_ID_KEY_MASK;
            return SNMP_SUCCESS;
        }
        OctetStr.i4_Length = STRLEN (pNode->au1Key);
        MEMCPY (OctetStr.pu1_OctetList, pNode->au1Key, OctetStr.i4_Length);
        i4rc =
            MynmhTest (*Join
                       (FsVpnRemoteIdKey, 12, FsVpnRemoteIdTableINDEX, 2,
                        u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                       (VOID *) &OctetStr);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "-E- Test failed for FsVpnRemoteIdKey\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnRemoteIdKey, 12, FsVpnRemoteIdTableINDEX, 2, u4Idx0,
                       &OctetStrIdx), SNMP_DATA_TYPE_OCTET_PRIM,
                      (VOID *) &OctetStr);
    }
    if (!name || STRCMP (name, "Status") == 0)
    {
        if (!bTrDoSnmpSet)
        {
            pNode->u4Status = u4val = value->in_uint;
            pNode->u4ValMask |= FS_VPN_REMOTE_ID_STATUS_MASK;
            return SNMP_SUCCESS;
        }
        u4val = pNode->u4Status;
        i4rc =
            MynmhTest (*Join
                       (FsVpnRemoteIdStatus, 12, FsVpnRemoteIdTableINDEX, 2,
                        u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                       (VOID *) u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- Test failed for FsVpnRemoteIdStatus\n");
            return SNMP_FAILURE;
        }

        i4rc =
            MynmhSet (*Join
                      (FsVpnRemoteIdStatus, 12, FsVpnRemoteIdTableINDEX, 2,
                       u4Idx0, &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                      (VOID *) u4val);
    }
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrGetInternetGatewayDevice_VPN_VPNStatistics
 *  Description     : Get function for InternetGatewayDevice_VPN_VPNStatistics 
 *
 *  Input           : name - parameter name
 *                    value - parameter value
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/

int
TrGetInternetGatewayDevice_VPN_VPNStatistics (char *name,
                                              ParameterValue * value)
{
    UINT4               u4val = 0;
    INT4                i4rc = 0;
    UINT1               au1OctetList[MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    MEMSET (&OctetStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetList, 0, MAX_OCTETSTRING_SIZE);

    if (strcmp (name, "TotalIpPktIn") == 0)
    {
        i4rc =
            MynmhGet (*Join (FsVpnIpPktsIn, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "-E- Get failed for FsVpnIpPktsIn\n");
            return SNMP_FAILURE;
        }
        value->out_uint = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FsVpnIpPktsIn ret %ld\n", u4val);
        return SNMP_SUCCESS;
    }

    else if (strcmp (name, "IkeSANegnDone") == 0)
    {
        i4rc =
            MynmhGet (*Join (FsVpnIkeSAsActive, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "-E- Get failed for FsVpnIkeSAsActive\n");
            return SNMP_FAILURE;
        }
        value->out_uint = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FsVpnIkeSAsActive ret %ld\n", u4val);
        return SNMP_SUCCESS;
    }

    else if (strcmp (name, "IPSecSANegnDone") == 0)
    {
        i4rc =
            MynmhGet (*Join (FsVpnIPSecSAsActive, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "-E- Get failed for FsVpnIPSecSAsActive\n");
            return SNMP_FAILURE;
        }
        value->out_uint = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FsVpnIPSecSAsActive ret %ld\n",
                       u4val);
        return SNMP_SUCCESS;
    }

    else if (strcmp (name, "IkeSANegnRekeys") == 0)
    {
        i4rc =
            MynmhGet (*Join (FsVpnIkeRekeys, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "-E- Get failed for FsVpnIkeRekeys\n");
            return SNMP_FAILURE;
        }
        value->out_uint = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FsVpnIkeRekeys ret %ld\n", u4val);
        return SNMP_SUCCESS;
    }

    else if (strcmp (name, "TotalIpPktOut") == 0)
    {
        i4rc =
            MynmhGet (*Join (FsVpnIpPktsOut, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "-E- Get failed for FsVpnIpPktsOut\n");
            return SNMP_FAILURE;
        }
        value->out_uint = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FsVpnIpPktsOut ret %ld\n", u4val);
        return SNMP_SUCCESS;
    }

    else if (strcmp (name, "IPSecSANegnFailed") == 0)
    {
        i4rc =
            MynmhGet (*Join (FsVpnIPSecNegoFailed, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC,
                      "-E- Get failed for FsVpnIPSecNegoFailed\n");
            return SNMP_FAILURE;
        }
        value->out_uint = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FsVpnIPSecNegoFailed ret %ld\n",
                       u4val);
        return SNMP_SUCCESS;
    }

    else if (strcmp (name, "IkeSANegnFailed") == 0)
    {
        i4rc =
            MynmhGet (*Join (FsVpnIkeNegoFailed, 10, NULL, 1, 0),
                      SNMP_DATA_TYPE_COUNTER32, &u4val);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "-E- Get failed for FsVpnIkeNegoFailed\n");
            return SNMP_FAILURE;
        }
        value->out_uint = u4val;
        TR69_TRC_ARG1 (TR69_DBG_TRC, "Get: FsVpnIkeNegoFailed ret %ld\n",
                       u4val);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/************************************************************************
 *  Function Name   : TrScanPPP_VPN
 *  Description     : Scans and detects available VPNs.
 *
 *  Input           : none.
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
UINT4
TrScanPPP_VPN (tWANDevice * pDev, tWANConnDev * pWANConnDev,
               tPPPConnection * pWANPPPConnection)
{
    tSNMP_OCTET_STRING_TYPE PolicyName;
    tSNMP_OCTET_STRING_TYPE NextPolicyName;
    UINT1               au1PolicyName[MAX_OCTETSTRING_SIZE];
    UINT1               au1NextPolicyName[MAX_OCTETSTRING_SIZE];
    tVPN               *pVPN = NULL;
    tVPN               *pNextVPN = NULL;
    UINT4               u4NewNode = 0;
    INT4                i4rc = 0;
    INT4                i4PolicyIntfIndex = 0;
    UINT4               u4TrIdx = 0;
    tTMO_SLL           *pVPNList = NULL;
    CHR1                ac1ObjName[256];
    UINT4              *ptr = NULL;
    UINT4               u4Size = 0;

    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Entry: TrScanPPP_VPN\n");

    MEMSET (ac1ObjName, '\0', 256);

    MEMSET (&PolicyName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextPolicyName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MEMSET (au1PolicyName, 0, MAX_OCTETSTRING_SIZE);
    MEMSET (au1NextPolicyName, 0, MAX_OCTETSTRING_SIZE);

    PolicyName.pu1_OctetList = au1PolicyName;
    NextPolicyName.pu1_OctetList = au1NextPolicyName;

    i4rc = nmhGetFirstIndexFsVpnTable (&PolicyName);
    if (i4rc == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    STRNCPY (NextPolicyName.pu1_OctetList, PolicyName.pu1_OctetList,
             PolicyName.i4_Length);
    NextPolicyName.i4_Length = PolicyName.i4_Length;

    pVPNList = &VPNList;

    UTL_SLL_OFFSET_SCAN (pVPNList, pVPN, pNextVPN, tVPN *)
    {
        pVPN->u4Visited = 0;
    }
    while (1)
    {
        pVPN = NULL;
        u4NewNode = 1;

        /* Get the interface index to which the policy is
         * attached. This policy node is to be associated
         * to the TR instances of only those interface nodes.
         */
        i4rc = nmhGetFsVpnPolicyIntfIndex (&NextPolicyName, &i4PolicyIntfIndex);
        if (SNMP_FAILURE == i4rc)
        {
            u4NewNode = 0;
        }
        else
        {
            if (pWANConnDev->u4IfIdx == (UINT4) (i4PolicyIntfIndex))
            {
                /* Do not instantiate if already present */
                UTL_SLL_OFFSET_SCAN (pVPNList, pVPN, pNextVPN, tVPN *)
                {
                    if (STRCMP
                        (pVPN->au1PolicyName,
                         NextPolicyName.pu1_OctetList) == 0)
                    {
                        TR69_TRC_ARG1 (TR69_DBG_TRC,
                                       "Policy %s Node already present \n",
                                       NextPolicyName.pu1_OctetList);
                        u4NewNode = 0;
                        pVPN->u4Visited = 1;
                        break;
                    }
                }
            }
            else
            {
                /* The policy does not belong to this WAN Interface. 
                 * So no need to add.
                 */
                u4NewNode = 0;
            }
        }

        /* Instantiate if this is a new instance */
        if (u4NewNode)
        {
            SNPRINTF (ac1ObjName, sizeof (ac1ObjName), "%s%lu%s%lu%s%lu%s",
                      "InternetGatewayDevice.WANDevice.", pDev->u4TrInstance,
                      ".WANConnectionDevice.", pWANConnDev->u4TrInstance,
                      ".WANPPPConnection.", pWANPPPConnection->u4TrInstance,
                      ".vpn");
            u4TrIdx = 0;
            i4rc = addObjectIntern (ac1ObjName, (int *) &u4TrIdx);

            if (i4rc == 0)
            {
                pVPN = TrAdd_VPN (u4TrIdx, &NextPolicyName,
                                  pWANConnDev->u4IfIdx);
                if (pVPN == NULL)
                {
                    TR69_TRC (TR69_DBG_TRC, "TrScan: Malloc Failure");
                    return SNMP_FAILURE;
                }
            }
            else
            {
                TR69_TRC_ARG1 (TR69_DBG_TRC,
                               "addObjectIntern failed, rc = %d\n", i4rc);
            }
        }

        STRNCPY (PolicyName.pu1_OctetList, NextPolicyName.pu1_OctetList,
                 NextPolicyName.i4_Length);
        PolicyName.i4_Length = NextPolicyName.i4_Length;
        MEMSET (NextPolicyName.pu1_OctetList, 0, MAX_OCTETSTRING_SIZE);
        i4rc = nmhGetNextIndexFsVpnTable (&PolicyName, &NextPolicyName);
        if (i4rc == SNMP_FAILURE)
        {
            TR69_TRC (TR69_DBG_TRC, "BREAKING from PPP_VPN scan\n");
            break;
        }
    }
    UTL_SLL_OFFSET_SCAN (pVPNList, pVPN, pNextVPN, tVPN *)
    {
        /* Before initializing  the structure elements to zero, check 
           is made against both visited flag and the table mask in order to make 
           sure that this does not intervene the row creation process */

        if ((pVPN->u4Visited == 0) &&
            ((pVPN->u4ValMask & pVPN->u4GlobalValMask) ==
             pVPN->u4GlobalValMask)
            && (pWANConnDev->u4IfIdx == (UINT4) (pVPN->i4PolicyIntfIndex)))
        {
            TR69_TRC (TR69_DBG_TRC,
                      "Deleting the node as it is not present in SNMP\n");

            ptr = &(pVPN->u4ValMask);
            u4Size =
                sizeof (tVPN) - (3 * sizeof (UINT4) + sizeof (tTMO_SLL_NODE));
            MEMSET (ptr, 0, u4Size);
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC, "Exit: TrScanPPP_VPN - SUCCESS\n");

    return OSIX_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrInitInternetGatewayDevice__RAVPN_User
 *  Description     : Creates a TR Instance for FsVpnRaUsersTable
 *
 *  Input           : idx1 - TR Instance
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrInitInternetGatewayDevice_RAVPN_User (int idx1)
{
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrInitInternetGatewayDevice_RAVPN_User\n");

    if (idx1 == 0)
    {
        return 0;
    }
    TrAdd_User (idx1, NULL);
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrInitInternetGatewayDevice_RAVPN_User\n");

    return 0;
}

/************************************************************************
 *  Function Name   : TrDeleteInternetGatewayDevice__RAVPN_User
 *  Description     : Delete TR Instance for FsVpnRaUsersTable
 *
 *  Input           : idx1 - TR Instance
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrDeleteInternetGatewayDevice_RAVPN_User (int idx1)
{

    tUser              *pUser = NULL;
    tUser              *pNextUser = NULL;
    INT4                i4rc = SNMP_FAILURE;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrDeleteInternetGatewayDevice_RAVPN_User\n");
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStrIdx.i4_Length = STRLEN (au1OctetListIdx);
    UTL_SLL_OFFSET_SCAN (&UserList, pUser, pNextUser, tUser *)
    {
        if (pUser->u4TrInstance == (UINT4) idx1)
        {
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "-E-Deleting User TrInstance:%d String:%s\r\n",
                           pUser->u4TrInstance, pUser->au1Name);

            OctetStrIdx.i4_Length = STRLEN (pUser->au1Name);
            MEMCPY (OctetStrIdx.pu1_OctetList, pUser->au1Name,
                    OctetStrIdx.i4_Length);
            if (nmhValidateIndexInstanceFsVpnRaUsersTable (&OctetStrIdx) ==
                SNMP_SUCCESS)
            {
                i4rc = MynmhTest (*Join
                                  (FsVpnRaUserRowStatus, 12,
                                   FsVpnRaUsersTableINDEX, 1, &OctetStrIdx),
                                  SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E-Deleting User Entry Test Failed\r\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrDeleteInternetGatewayDevice_RAVPN_User\n");
                    return SNMP_FAILURE;
                }

                i4rc = MynmhSet (*Join
                                 (FsVpnRaUserRowStatus, 12,
                                  FsVpnRaUsersTableINDEX, 1, &OctetStrIdx),
                                 SNMP_DATA_TYPE_INTEGER, (VOID *) DESTROY);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E-Deleting User Entry Failed\r\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrDeleteInternetGatewayDevice_RAVPN_User\n");
                    return SNMP_FAILURE;
                }

            }
            TMO_SLL_Delete (&UserList, (tTMO_SLL_NODE *) pUser);
            MEM_FREE (pUser);
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrDeleteInternetGatewayDevice_RAVPN_User\n");

    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrInitInternetGatewayDevice__RAVPN_AddressPool
 *  Description     : Creates a TR Instance for FsVpnRaAddressPoolTable
 *
 *  Input           : idx1 - TR Instance
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrInitInternetGatewayDevice_RAVPN_AddressPool (int idx1)
{
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrInitInternetGatewayDevice_RAVPN_AddressPool\n");

    if (idx1 == 0)
    {
        return 0;
    }
    TrAdd_AddressPool (idx1, NULL);
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrInitInternetGatewayDevice_RAVPN_AddressPool\n");
    return 0;
}

/************************************************************************
 *  Function Name   : TrDeleteInternetGatewayDevice__RAVPN_AddressPool
 *  Description     : Delete TR Instance for FsVpnRaAddressPoolTable
 *
 *  Input           : idx1 - TR Instance
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrDeleteInternetGatewayDevice_RAVPN_AddressPool (int idx1)
{

    tAddressPool       *pAddressPool = NULL;
    tAddressPool       *pNextAddressPool = NULL;

    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    INT4                i4rc = SNMP_FAILURE;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrDeleteInternetGatewayDevice_RAVPN_AddressPool\n");
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStrIdx.i4_Length = STRLEN (au1OctetListIdx);
    UTL_SLL_OFFSET_SCAN (&AddressPoolList, pAddressPool,
                         pNextAddressPool, tAddressPool *)
    {
        if (pAddressPool->u4TrInstance == (UINT4) idx1)
        {
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "-E-Deleting AddressPool TrInstance:%d String:%s\r\n",
                           pAddressPool->u4TrInstance, pAddressPool->au1Name);

            OctetStrIdx.i4_Length = STRLEN (pAddressPool->au1Name);
            MEMCPY (OctetStrIdx.pu1_OctetList, pAddressPool->au1Name,
                    OctetStrIdx.i4_Length);
            if (nmhValidateIndexInstanceFsVpnRaAddressPoolTable (&OctetStrIdx)
                == SNMP_SUCCESS)
            {
                i4rc = MynmhTest (*Join
                                  (FsVpnRaAddressPoolRowStatus, 12,
                                   FsVpnRaAddressPoolTableINDEX, 1,
                                   &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                                  (VOID *) DESTROY);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E-Deleting Address Pool Entry Test Failed\r\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrDeleteInternetGatewayDevice_RAVPN_AddressPool\n");
                    return SNMP_FAILURE;
                }

                i4rc = MynmhSet (*Join
                                 (FsVpnRaAddressPoolRowStatus, 12,
                                  FsVpnRaAddressPoolTableINDEX, 1,
                                  &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                                 (VOID *) DESTROY);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E-Deleting Address Pool Entry Failed\r\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrDeleteInternetGatewayDevice_RAVPN_AddressPool\n");
                    return SNMP_FAILURE;
                }

            }
            TMO_SLL_Delete (&AddressPoolList, (tTMO_SLL_NODE *) pAddressPool);
            MEM_FREE (pAddressPool);
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrDeleteInternetGatewayDevice_RAVPN_AddressPool\n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrInitInternetGatewayDevice_WANDevice_
 *                       WANConnectionDevice_WANIPConnection_VPN
 *  Description     : Creates a TR Instance for FsVpnPolicyTable
 *
 *  Input           : idx1 - TR Instance
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/

INT4 
     
     
     
     
     
     
     
    TrInitInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
    (int idx1, int idx2, int idx3, int idx4)
{

    UINT4               u4IfIdx = 0;
    tWANIPConnection   *pWANIPConnection = NULL;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrInitInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN\n");

    if (idx1 == 0)
    {
        return 0;
    }
    if (idx2 == 0)
    {
        return 0;
    }
    if (idx3 == 0)
    {
        return 0;
    }
    if (idx4 == 0)
    {
        return 0;
    }

    pWANIPConnection =
        (tWANIPConnection *) TrGetWANConnectionInstance (idx1, idx2, idx3,
                                                         &u4IfIdx);

    if (pWANIPConnection == NULL)
    {
        return SNMP_FAILURE;
    }

    TrAdd_VPN (idx4, NULL, u4IfIdx);
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrInitInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN\n");

    return 0;
}

/************************************************************************
 *  Function Name   : TrDeleteInternetGatewayDevice_WANDevice_
 *                       WANConnectionDevice_WANIPConnection_VPN
 *  Description     : Creates a TR Instance for FsVpnPolicyTable
 *
 *  Input           : idx1 - TR Instance
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/

INT4 
     
     
     
     
     
     
     
    TrDeleteInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
    (int idx1, int idx2, int idx3, int idx4)
{

    tVPN               *pVpn = NULL;
    tVPN               *pNextVpn = NULL;
    INT4                i4rc = SNMP_FAILURE;

    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrDeleteInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN\n");
    UNUSED_PARAM (idx1);
    UNUSED_PARAM (idx2);
    UNUSED_PARAM (idx3);
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStrIdx.i4_Length = STRLEN (au1OctetListIdx);

    UTL_SLL_OFFSET_SCAN (&VPNList, pVpn, pNextVpn, tVPN *)
    {
        if (pVpn->u4TrInstance == (UINT4) idx4)
        {
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "-E-Deleting IP VPN Policy TrInstance:%d String:%s\r\n",
                           pVpn->u4TrInstance, pVpn->au1PolicyName);

            OctetStrIdx.i4_Length = STRLEN (pVpn->au1PolicyName);
            MEMCPY (OctetStrIdx.pu1_OctetList, pVpn->au1PolicyName,
                    OctetStrIdx.i4_Length);
            if (nmhValidateIndexInstanceFsVpnTable (&OctetStrIdx) ==
                SNMP_SUCCESS)
            {
                i4rc = MynmhTest (*Join
                                  (FsVpnPolicyRowStatus, 12, FsVpnTableINDEX, 1,
                                   &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                                  (VOID *) DESTROY);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E-Deleting IP Vpn Policy Test Failed\r\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrDeleteInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN\n");
                    return SNMP_FAILURE;
                }

                i4rc = MynmhSet (*Join
                                 (FsVpnPolicyRowStatus, 12, FsVpnTableINDEX, 1,
                                  &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                                 (VOID *) DESTROY);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E-Deleting IP Vpn Policy Failed\r\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrDeleteInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN\n");
                    return SNMP_FAILURE;
                }

            }
            TMO_SLL_Delete (&VPNList, (tTMO_SLL_NODE *) pVpn);
            MEM_FREE (pVpn);
        }
    }

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrDeleteInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN\n");

    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrInitInternetGatewayDevice__VPN_VpnRemoteId
 *  Description     : Creates a TR Instance for FsVpnRemoteIdTable
 *
 *  Input           : idx1 - TR Instance
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrInitInternetGatewayDevice_VPN_VpnRemoteId (int idx1)
{
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrInitInternetGatewayDevice_VPN_VpnRemoteId\n");

    if (idx1 == 0)
    {
        return 0;
    }
    TrAdd_VpnRemoteId (idx1, NULL, 0);
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrInitInternetGatewayDevice_VPN_VpnRemoteId\n");
    return 0;
}

/************************************************************************
 *  Function Name   : TrDeleteInternetGatewayDevice__VPN_VpnRemoteId
 *  Description     : Delete TR Instance for FsVpnRemoteIdTable
 *
 *  Input           : idx1 - TR Instance
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/
INT4
TrDeleteInternetGatewayDevice_VPN_VpnRemoteId (int idx1)
{

    tVpnRemoteId       *pVpnRemoteId = NULL;
    tVpnRemoteId       *pNextVpnRemoteId = NULL;
    INT4                i4rc = SNMP_FAILURE;
    UINT4               u4Idx0 = 0;

    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrDeleteInternetGatewayDevice_VPN_VpnRemoteId\n");
    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStrIdx.i4_Length = STRLEN (au1OctetListIdx);
    UTL_SLL_OFFSET_SCAN (&VpnRemoteIdList, pVpnRemoteId,
                         pNextVpnRemoteId, tVpnRemoteId *)
    {
        if (pVpnRemoteId->u4TrInstance == (UINT4) idx1)
        {
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "-E-Deleting VpnRemoteId TrInstance:%d String:%s\r\n",
                           pVpnRemoteId->u4TrInstance, pVpnRemoteId->au1Value);

            OctetStrIdx.i4_Length = STRLEN (pVpnRemoteId->au1Value);
            MEMCPY (OctetStrIdx.pu1_OctetList, pVpnRemoteId->au1Value,
                    OctetStrIdx.i4_Length);
            u4Idx0 = (UINT4) pVpnRemoteId->i4Type;
            if (nmhValidateIndexInstanceFsVpnRemoteIdTable
                (u4Idx0, &OctetStrIdx) == SNMP_SUCCESS)
            {
                i4rc = MynmhTest (*Join
                                  (FsVpnRemoteIdStatus, 12,
                                   FsVpnRemoteIdTableINDEX, 2, u4Idx0,
                                   &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                                  (VOID *) DESTROY);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E-Deleting VpnRemoteId Test Failed\r\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrDeleteInternetGatewayDevice_VPN_VpnRemoteId\n");
                    return SNMP_FAILURE;
                }

                i4rc = MynmhSet (*Join
                                 (FsVpnRemoteIdStatus, 12,
                                  FsVpnRemoteIdTableINDEX, 2, u4Idx0,
                                  &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                                 (VOID *) DESTROY);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E-Deleting VpnRemoteId Failed\r\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrDeleteInternetGatewayDevice_VPN_VpnRemoteId\n");
                    return SNMP_FAILURE;
                }

            }
            TMO_SLL_Delete (&VpnRemoteIdList, (tTMO_SLL_NODE *) pVpnRemoteId);
            MEM_FREE (pVpnRemoteId);
        }
    }
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrDeleteInternetGatewayDevice_VPN_VpnRemoteId\n");
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : TrInitInternetGatewayDevice_WANDevice_
 *                       WANConnectionDevice_WANPPPConnection_VPN
 *  Description     : Creates a TR Instance for FsVpnPolicyTable
 *
 *  Input           : idx1 - TR Instance
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/

INT4 
     
     
     
     
     
     
     
    TrInitInternetGatewayDevice_WANDevice_WANConnectionDevice_WANPPPConnection_VPN
    (int idx1, int idx2, int idx3, int idx4)
{

    UINT4               u4IfIdx = 0;
    tPPPConnection     *pPPPConnection = NULL;

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrInitInternetGatewayDevice_WANDevice_WANConnectionDevice_WANPPPConnection_VPN\n");

    if (idx1 == 0)
    {
        return 0;
    }
    if (idx2 == 0)
    {
        return 0;
    }
    if (idx3 == 0)
    {
        return 0;
    }
    if (idx4 == 0)
    {
        return 0;
    }

    pPPPConnection =
        (tPPPConnection *) TrGetWANConnectionInstance (idx1, idx2, idx3,
                                                       &u4IfIdx);

    if (pPPPConnection == NULL)
    {
        return SNMP_FAILURE;
    }

    TrAdd_VPN (idx4, NULL, u4IfIdx);
    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrInitInternetGatewayDevice_WANDevice_WANConnectionDevice_WANPPPConnection_VPN\n");

    return 0;
}

/************************************************************************
 *  Function Name   : TrDeleteInternetGatewayDevice_WANDevice_
 *                       WANConnectionDevice_WANPPPConnection_VPN
 *  Description     : Deletes a TR Instance for FsVpnPolicyTable
 *
 *  Input           : idx1 - TR Instance
 *  Output          : None
 *  Returns         : 0/-1
 ************************************************************************/

INT4 
     
     
     
     
     
     
     
    TrDeleteInternetGatewayDevice_WANDevice_WANConnectionDevice_WANPPPConnection_VPN
    (int idx1, int idx2, int idx3, int idx4)
{

    tVPN               *pVpn = NULL;
    tVPN               *pNextVpn = NULL;
    INT4                i4rc = SNMP_FAILURE;
    UINT4               u4IfIdx = 0;
    tPPPConnection     *pPPPConnection = NULL;

    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1               au1OctetListIdx[MAX_OCTETSTRING_SIZE];

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Entry: TrDeleteInternetGatewayDevice_WANDevice_WANConnectionDevice_WANPPPConnection_VPN\n");

    MEMSET (&OctetStrIdx, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OctetListIdx, 0, MAX_OCTETSTRING_SIZE);
    OctetStrIdx.pu1_OctetList = au1OctetListIdx;
    OctetStrIdx.i4_Length = STRLEN (au1OctetListIdx);

    pPPPConnection =
        (tPPPConnection *) TrGetWANConnectionInstance (idx1, idx2, idx3,
                                                       &u4IfIdx);
    if (pPPPConnection == NULL)
    {
        return SNMP_FAILURE;
    }

    UTL_SLL_OFFSET_SCAN (&VPNList, pVpn, pNextVpn, tVPN *)
    {
        if ((pVpn->u4TrInstance == (UINT4) idx4)
            && (pVpn->i4PolicyIntfIndex == (INT4) u4IfIdx))

        {
            TR69_TRC_ARG2 (TR69_DBG_TRC,
                           "-E-Deleting Vpn Policy TrInstance:%d String:%s\r\n",
                           pVpn->u4TrInstance, pVpn->au1PolicyName);

            OctetStrIdx.i4_Length = STRLEN (pVpn->au1PolicyName);
            MEMCPY (OctetStrIdx.pu1_OctetList, pVpn->au1PolicyName,
                    OctetStrIdx.i4_Length);
            if (nmhValidateIndexInstanceFsVpnTable (&OctetStrIdx) ==
                SNMP_SUCCESS)
            {
                i4rc = MynmhTest (*Join
                                  (FsVpnPolicyRowStatus, 12, FsVpnTableINDEX, 1,
                                   &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                                  (VOID *) DESTROY);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E-Deleting PPP VPN Policy Test Failed\r\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrDeleteInternetGatewayDevice_WANDevice_WANConnectionDevice_WANPPPConnection_VPN\n");
                    return SNMP_FAILURE;
                }

                i4rc = MynmhSet (*Join
                                 (FsVpnPolicyRowStatus, 12, FsVpnTableINDEX, 1,
                                  &OctetStrIdx), SNMP_DATA_TYPE_INTEGER,
                                 (VOID *) DESTROY);
                if (i4rc == SNMP_FAILURE)
                {
                    TR69_TRC (TR69_DBG_TRC,
                              "-E-Deleting PPP VPN POlicy Failed\r\n");
                    TR69_TRC (TR69_ENTRY_EXIT_TRC,
                              "Exit: TrDeleteInternetGatewayDevice_WANDevice_WANConnectionDevice_WANPPPConnection_VPN\n");
                    return SNMP_FAILURE;
                }
            }

            TMO_SLL_Delete (&VPNList, (tTMO_SLL_NODE *) pVpn);
            MEM_FREE (pVpn);
        }
    }

    TR69_TRC (TR69_ENTRY_EXIT_TRC,
              "Exit: TrDeleteInternetGatewayDevice_WANDevice_WANConnectionDevice_WANPPPConnection_VPN\n");

    return SNMP_SUCCESS;
}
