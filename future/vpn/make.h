##########################################################################
# Copyright (C) 2010 Aricent Inc . All Rights Reserved                   #
# ------------------------------------------                             #
# $Id: make.h,v 1.4 2014/03/16 11:29:00 siva Exp $ #
# DESCRIPTION  : make.h file for VPN                                     #
##########################################################################

############################################################################
#                         Directories                                      #
############################################################################

VPN_DIR = ${BASE_DIR}/vpn

VPN_INC_DIR = ${VPN_DIR}/inc
VPN_SRC_DIR = ${VPN_DIR}/src
VPN_OBJ_DIR = ${VPN_DIR}/obj

VPN_DPNDS = ${VPN_INC_DIR}/fsvpnlw.h \
            ${VPN_INC_DIR}/fsvpnwr.h \
            ${VPN_INC_DIR}/fsvpndb.h \
            ${VPN_INC_DIR}/fsvpnextn.h\
            ${VPN_INC_DIR}/fsvpndefn.h\
            ${VPN_INC_DIR}/fsvpnproto.h\
            ${VPN_INC_DIR}/vpnweb.h\
            ${VPN_INC_DIR}/vpnglob.h\
            ${VPN_INC_DIR}/vpninc.h\
            ${VPN_INC_DIR}/vpntrc.h\
            ${VPN_INC_DIR}/vpnbufif.h
 
ifeq (${KERNEL}, YES)
VPN_DPNDS  += $(VPN_INC_DIR)/vpnwdefs.h \
              $(VPN_INC_DIR)/vpnwtdfs.h \
              $(VPN_INC_DIR)/vpnwincs.h
endif                                    

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

SEC_KEY_OPNS  = -USECv4_PRIVATE_KEY
COMMON_INCLUDE_DIRS += -I${VPN_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}
TOTAL_OPNS =  ${SEC_KEY_OPNS} $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

#############################################################################
