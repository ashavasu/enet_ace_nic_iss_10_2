/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vpnbufif.h,v 1.1 2014/03/16 11:31:44 siva Exp $
 *
 * Description:This file contains constants, typedefs &   
 *             macros related to Buffer interface.        
 *
 *******************************************************************/
#ifndef _FWLBUFIF_H
#define _FWLBUFIF_H

/*****************************************************************************/
/*            Macros for defining respective PIDs (Pool Id)                  */
/*****************************************************************************/

#define  VPN_CA_CERT_PID          VPNMemPoolIds[MAX_VPN_CA_CERT_BLOCKS_SIZING_ID]
#define  VPN_CERT_PID             VPNMemPoolIds[MAX_VPN_CERT_BLOCKS_SIZING_ID]
#define  VPN_ID_PID               VPNMemPoolIds[MAX_VPN_ID_BLOCKS_SIZING_ID]
#define  VPN_POLICY_PID           VPNMemPoolIds[MAX_VPN_POLICY_BLOCKS_SIZING_ID]
#define  VPN_RA_ADDRESS_NODE_PID  VPNMemPoolIds[MAX_VPN_RA_ADDR_NODE_BLOCKS_SIZING_ID]
#define  VPN_RA_ADDRESS_POOL_PID  VPNMemPoolIds[MAX_RA_VPN_ADDRESS_POOL_BLOCKS_SIZING_ID]
#define  VPN_RA_USERS_PID         VPNMemPoolIds[MAX_RA_VPN_USERS_BLOCKS_SIZING_ID]
#define  VPN_Q_PID                VPNMemPoolIds[VPN_MAX_Q_SIZE_SIZING_ID]
#define  VPN_WEB_IKE_POLICY_PID   VPNMemPoolIds[MAX_WEB_IKE_POLICY_BLOCKS_SIZING_ID]
#define  VPN_WEB_IPSEC_POLICY_PID VPNMemPoolIds[MAX_WEB_IPSEC_POLICY_BLOCKS_SIZING_ID]
#define  VPN_WEB_POLL_BUFF_PID	  VPNMemPoolIds[MAX_WEB_POL_BUFF_BLOCKS_SIZING_ID]
#endif

/*****************************************************************************/
/*              End Of File vpnbufif.h                                       */
/*****************************************************************************/
