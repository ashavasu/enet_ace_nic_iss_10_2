/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsvpnextn.h,v 1.11 2012/05/04 12:38:15 siva Exp $
*
* Description: Proto types exported from dependency modules
*********************************************************************/

#ifndef _FSVPNEXTN_H
#define _FSVPNEXTN_H

/* Low Level TEST Routines for.  */

/* IPSecv4 related Prototypes */
extern INT1 
nmhGetFsipv4SecGlobalStatus PROTO ((INT4 *pi4RetValFsipv4SecGlobalStatus));

extern INT1 
nmhGetFsipv4SecIfInPkts PROTO ((INT4 i4Fsipv4SecIfIndex,
                                UINT4 *pu4RetValFsipv4SecIfInPkts));
extern INT1 
nmhGetFsipv4SecIfOutPkts PROTO ((INT4 i4Fsipv4SecIfIndex,
                                 UINT4 *pu4RetValFsipv4SecIfOutPkts));

extern INT1 
nmhGetFsipv4SecIfPktsApply PROTO ((INT4 i4Fsipv4SecIfIndex,
                                   UINT4 *pu4RetValFsipv4SecIfPktsApply));
PUBLIC INT1
nmhGetFsipv4SecDummyPktGen PROTO ((INT4 *pi4RetValFsipv4SecDummyPktGen));

PUBLIC INT1
nmhGetFsipv4SecDummyPktParam PROTO ((INT4 *pi4RetValFsipv4SecDummyPktParam));

extern INT1 
nmhGetFsipv4SecIfPktsDiscard PROTO ((INT4 i4Fsipv4SecIfIndex,
                                     UINT4 *pu4RetValFsipv4SecIfPktsDiscard));
extern INT1 
nmhTestv2Fsipv4SecGlobalStatus PROTO ((UINT4 *pu4ErrorCode,
                                       INT4 i4TestValFsipv4SecGlobalStatus));

PUBLIC INT1
nmhTestv2Fsipv4SecDummyPktGen PROTO ((UINT4 *pu4ErrorCode,
   INT4 i4TestValFsipv4SecDummyPktGen));
PUBLIC INT1
nmhTestv2Fsipv4SecDummyPktParam PROTO ((UINT4 *pu4ErrorCode,
   INT4 i4TestValFsipv4SecDummyPktParam));

extern INT1
nmhSetFsipv4SecGlobalStatus PROTO ((INT4 i4SetValFsipv4SecGlobalStatus));

PUBLIC INT1
nmhSetFsipv4SecDummyPktGen PROTO ((INT4 i4SetValFsipv4SecDummyPktGen));

PUBLIC INT1
nmhSetFsipv4SecDummyPktParam PROTO ((INT4 i4SetValFsipv4SecDummyPktParam));

extern VOID Secv4GetAssocEntryCount PROTO ((UINT4 *u4IPSecSAsActive));
extern INT1 VpnFillIpsecParams PROTO ((tVpnPolicy * pVpnPolicy));
extern INT1 VpnDeleteIpsecParams PROTO ((tVpnPolicy * pVpnPolicy));

extern VOID AuthInit PROTO ((VOID));

extern INT1
nmhGetFsikeGlobalDebug (INT4 *pi4RetValFsikeGlobalDebug);

extern INT1
nmhSetFsikeGlobalDebug (INT4 i4SetValFsikeGlobalDebug);

extern INT1
nmhGetFsipv4SecGlobalDebug (INT4 *pi4RetValFsipv4SecGlobalDebug);

extern INT1
nmhSetFsipv4SecGlobalDebug (INT4 i4SetValFsipv4SecGlobalDebug);

#ifdef IPSECv6_WANTED
extern INT1
nmhSetFsipv6SecGlobalDebug (INT4 i4SetValFsipv4SecGlobalDebug);
#endif

#ifdef IKE_WANTED
extern INT1
nmhTestv2FsIkeEngineTunnelTermAddr ARG_LIST ((UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValFsIkeEngineTunnelTermAddr));


extern INT1
nmhSetFsIkeEngineTunnelTermAddr ARG_LIST ((tSNMP_OCTET_STRING_TYPE * pFsIkeEngineName,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValFsIkeEngineTunnelTermAddr));
extern VOID TriggerIke PROTO ((tIkeIpAddr * IkeIpAddr, UINT1 u1IkeVersion));
#endif

extern VOID
IkeGetActiveSessions (INT4 i4Index,UINT4 *pu4IkeActiveSessions);

extern VOID 
IkeGetNoOfPhase1SessionsSucc (INT4 i4Index, UINT4 *pu4IkeNoOfPhase1SessionsSucc);

extern VOID 
IkeGetNoOfPhase1SessionFailed (INT4 i4Index,UINT4 *pu4IkeNoOfSessionsFailure);

extern VOID
IkeGetNoOfSessionsSucc (INT4 i4Index,UINT4 *pu4IkeNoOfSessionsSucc);

extern VOID
IkeGetNoOfSessionsFail (INT4 i4Index,UINT4 *pu4IkeNoOfSessionsFail);

extern VOID
IkeGetNoOfIkePhase1Rekeys (INT4 i4Index, UINT4 *pu4IkePhase1Rekeys);

extern VOID
IkeGetNoOfIkePhase2Rekeys (INT4 i4Index, UINT4 *pu4IkePhase2Rekeys);

extern VOID
IkeGetActiveSACount (UINT4 *u4IkeActiveSA);

extern tSNMP_OCTET_STRING_TYPE *
allocmem_octetstring      PROTO((INT4));

extern VOID 
free_octetstring          PROTO((tSNMP_OCTET_STRING_TYPE *));
#endif /*_FSVPNEXTN_H */
