/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsvpndefn.h,v 1.12 2013/11/28 10:17:09 siva Exp $
 *
 * Description: Macro definitons
 ********************************************************************/
#ifndef _FSVPNDEFN_H
#define _FSVPNDEFN_H

#define VPN_DS_LOCK                        (CONST UINT1 *) "VPND"

#define VPN_EVENT_QUEUE_DEPTH              10

#define VPN_AUTH_PRESHARED                  2 
#define VPN_AUTH_CERT                       3

#define VPN_MAX_RA_USER_SECRET_LEN       RA_USER_SECRET_LENGTH
#define VPN_MIN_RA_USER_SECRET_LEN          4

#define VPN_HTONL                        OSIX_HTONL 
#define VPN_NTOHL                        OSIX_NTOHL 
#define VPN_HTONS(x)                     (UINT2) OSIX_HTONS(x) 
#define VPN_NTOHS(x)                     (UINT2) OSIX_NTOHS(x) 
#define VPN_MEMCMP                       MEMCMP 
#define VPN_MEMCPY                       MEMCPY 
#define VPN_MEMSET                       MEMSET 
#define VPN_MALLOC                       MEM_MALLOC 
#define VPN_CALLOC                       MEM_CALLOC  
#define VPN_MEMFREE                      MEM_FREE 
#define VPN_STRCPY                       STRCPY 
#define VPN_STRCMP                       STRCMP 
#define CLI_GET_VPN_POLICY_ID()                   CliGetModeInfo()
#define CLI_SET_VPN_POLICY_ID(u4Index)         CliSetModeInfo(u4Index) 
/* Constants specified in secv4.mib  and secv6.mib */ 
 
#define  VPN_FILTER                          1 
#define  VPN_ALLOW                           2 
#define  VPN_APPLY                           3 
#define  VPN_BYPASS                          4 
#define  VPN_REJECT                         -1 

#define  VPN_ENABLE                          1 
#define  VPN_DISABLE                         2 

#define VPN_TCP                          6 
#define VPN_UDP                         17 
#define VPN_ICMP                         1 
#define VPN_ANY_PROTOCOL              9000 
#define VPN_ANY_DIRECTION                   3 
#define VPN_ANY_IFINDEX                     0 

#define  VPN_HMACMD5_LEN                    32 
#define  VPN_XCBCMAC_LEN                    32
#define  VPN_SHA2_256_LEN                   64
#define  VPN_SHA2_384_LEN                   96
#define  VPN_SHA2_512_LEN                   128
#define  VPN_HMACSHA1_LEN                   40 
#define  VPN_ESP_DES_KEY_LEN                16   /* For DES-CBC Key Length */
#define  VPN_ESP_3DES_KEY_LEN               48   /* For AES Key3 Length */
#define  VPN_ESP_AES_KEY1_LEN               32   /* For AES Key Length */
#define  VPN_ESP_AES_KEY2_LEN               48   /* For AES Key2 Length */
#define  VPN_ESP_AES_KEY3_LEN               64   /* For AES Key3 Length */

#define ESP_KEY_SEPERATOR                  '.'
#define ESP_FIRST_KEY_TERMINATION           16
#define ESP_SECOND_KEY_TERMINATION          33
#define ESP_START_SECOND_KEY                17
#define ESP_START_THIRD_KEY                 34

/*********** IKE SPECIFIC MACROS **************/
#define VPN_DH_GROUP1                        1
#define VPN_DH_GROUP2                        2
#define VPN_DH_GROUP5                        5


/* LifeTime (Type) */
#define VPN_LIFE_TYPE_SECS                1
#define VPN_LIFE_TYPE_KB                  2
/* Propreitary */
#define VPN_LIFE_TYPE_MINS                3
#define VPN_LIFE_TYPE_HRS                 4
#define VPN_LIFE_TYPE_DAYS                5

#define VPN_MIN_LIFETIME_SECS        300
#define VPN_MIN_LIFETIME_KB          100 /* 100 KB */
#define VPN_MAX_LIFETIME_KB          10000000 /* 10 GB */

#define VPN_DEFAULT_IKE_LTIME_TYPE     VPN_LIFE_TYPE_SECS
#define VPN_DEFAULT_IKE_LTIME_VALUE    2400
#define VPN_DEFAULT_IPSEC_LTIME_TYPE   VPN_LIFE_TYPE_SECS
#define VPN_DEFAULT_IPSEC_LTIME_VALUE  800
#define VPN_PHASE1_MAX_LIFETIME_SECS      86400
#define VPN_PHASE2_MAX_LIFETIME_SECS      28800

#define VPN_IKE_MODE_MAIN                   2
#define VPN_IKE_MODE_AGGRESSIVE             4

#define VPN_SECS_PER_MIN             60
#define VPN_SECS_PER_HOUR          3600
#define VPN_SECS_PER_DAY          86400
#define VPN_UDP_HDR_LEN               8
#define VPN_IKE_INIT_COOKIE_LEN       8
#define VPN_PORT_RANGE_LENGTH        11
#define VPN_MAX_SRC_DST_PORT_VALUE     65535


#define VPN_CONVERT_LIFETIME_TO_SECS(LifeTimeType, Lifetime)     \
{                                                                \
    switch (LifeTimeType)                                        \
    {                                                            \
        case (VPN_LIFE_TYPE_SECS):                              \
            break;                                               \
        case (VPN_LIFE_TYPE_MINS):                              \
            Lifetime *= VPN_SECS_PER_MIN;                      \
            break;                                               \
        case (VPN_LIFE_TYPE_HRS):                               \
            Lifetime *= VPN_SECS_PER_HOUR;                     \
            break;                                               \
        case (VPN_LIFE_TYPE_DAYS):                              \
            Lifetime *= VPN_SECS_PER_DAY;                      \
            break;                                               \
        case VPN_LIFE_TYPE_KB:                                 \
            break;                                             \
        default:  /* No matching type found - invalid */       \
            Lifetime = 0;                                      \
    }                                                           \
}
#define VPN_PRESHARED_KEY_ID               2

#define  VPN_MIN_SPI                       255 
#define  VPN_MAX_SPI                2147483647  /* The Max Range of INT4 */
#define  VPN_MAX_PRIORITY                  2147483647
#define  VPN_MIN_PRIORITY                    1
#define  VPN_MAX_BUNDLE_SA                   8 

#define  VPN_ANTI_REPLAY_ENABLE              1 
#define  VPN_ANTI_REPLAY_DISABLE             2 


#define  SEC_MAX_IP_ADDRESS                0xffffffff
#define  SEC_MIN_IP_ADDRESS                0x00000000
#define  SEC_IP_HEADER_LEN                   1
#define  SEC_IP_HEADER_LEN_MASK              0x0f

#define VPN_IS_ADDR_CLASS_A(u4Addr)   ((u4Addr & 0x80000000) == 0)
#define VPN_IS_ADDR_CLASS_B(u4Addr)   ((u4Addr & 0xc0000000) == 0x80000000)
#define VPN_IS_ADDR_CLASS_C(u4Addr)   ((u4Addr & 0xe0000000) == 0xc0000000)
#define VPN_IS_ADDR_CLASS_D(u4Addr)   ((u4Addr & 0xf0000000) == 0xe0000000)
#define VPN_IS_ADDR_CLASS_E(u4Addr)   ((u4Addr & 0xf8000000) == 0xf0000000)

#define  t_SLL                              tTMO_SLL
#define  t_SLL_NODE                         tTMO_SLL_NODE
#define  SLL_SCAN(List,node,Type)     TMO_SLL_Scan(List,node,Type)
#define  SLL_NEXT(List,Node)          TMO_SLL_Next(List,Node)
#define  SLL_FIRST(List)              TMO_SLL_First(List)

#define INVALID_VPN_PARAMETER   0

#define VPN_STATUS_ACTIVE                 ACTIVE
#define VPN_STATUS_NOT_IN_SERVICE         NOT_IN_SERVICE
#define VPN_STATUS_CREATE_AND_GO          CREATE_AND_GO
#define VPN_STATUS_CREATE_AND_WAIT        CREATE_AND_WAIT
#define VPN_STATUS_DESTROY                DESTROY

#define VPN_INET_NTOA(pString, u4Value)\
{\
    CHR1               *pcByte = NULL;\
    u4Value = OSIX_NTOHL (u4Value);\
    pcByte = (CHR1 *)&(u4Value);\
    SNPRINTF ((CHR1 *)pString, IPSEC_IPADDR_LEN,"%d.%d.%d.%d", (UINT1) pcByte[0],\
              (UINT1) pcByte[1], (UINT1) pcByte[2], (UINT1) pcByte[3]);\
}

#define VPN_INET6_NTOA(pString, Ip6Addr)\
{\
   CHR1               *pcByte = NULL;\
   pcByte = (CHR1 *) &(Ip6Addr.u1_addr);\
   (void) SNPRINTF (pString, 40, "%.4x:%.4x:%.4x:%.4x:%.4x:%.4x:%.4x:%.4x",\
                  ((UINT2)\
                   ((((UINT2) pcByte[0]) << 8) | (pcByte[1] & 0xff))),\
                  ((UINT2)\
                   ((((UINT2) pcByte[2]) << 8) | (pcByte[3] & 0xff))),\
                  ((UINT2)\
                   ((((UINT2) pcByte[4]) << 8) | (pcByte[5] & 0xff))),\
                  ((UINT2)\
                   ((((UINT2) pcByte[6]) << 8) | (pcByte[7] & 0xff))),\
                  ((UINT2)\
                   ((((UINT2) pcByte[8]) << 8) | (pcByte[9] & 0xff))),\
                  ((UINT2)\
                   ((((UINT2) pcByte[10]) << 8) | (pcByte[11] & 0xff))),\
                  ((UINT2)\
                   ((((UINT2) pcByte[12]) << 8) | (pcByte[13] & 0xff))),\
                   ((UINT2)\
                   ((((UINT2) pcByte[14]) << 8) | (pcByte[15] & 0xff))));\
}

#define RA_VPN_MAX_USERS_DB               30    /* Maximum Users to be added  */
#define RA_VPN_MAX_ADDRESS_POOL_DB         5    /* Maximum address pools created per mem pool */
#define RA_VPN_MAX_ADDRESS_POOL            1    /* Maximum address pools to be configured */

#define VPN_MAX_Q_SIZE            128    /* Maximum Q size*/
#define VPN_MEM_FREE(pValue)\
{\
    if(pValue != NULL)\
    {\
        MEM_FREE(pValue);\
    }\
}

#define IF_CHAR_IS_HEX(ch)           (((ch) >= '0' && (ch) <= '9') ||  \
                                      ((ch) >= 'a' && (ch) <= 'f') ||  \
                                      ((ch) >= 'A' && (ch) <= 'F'))

#define IS_POLICY_VPN_IPSEC_MANUAL(pVpnPolicy)  \
                       (pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL)
#define IS_POLICY_VPN_IKE(pVpnPolicy)  \
                       (pVpnPolicy->u4VpnPolicyType != VPN_IPSEC_MANUAL)

/* Remote VPN user information table */
#define VPN_RA_USER_NAME(pVpnRaUserInfo)    ((pVpnRaUserInfo)->au1VpnRaUserName)
#define VPN_RA_USER_SECRET(pVpnRaUserInfo)  ((pVpnRaUserInfo)->\
                                              au1VpnRaUserSecret)
#define VPN_RA_USER_STATUS(pVpnRaUserInfo)  ((pVpnRaUserInfo)->i4RowStatus)

/* Policy Table */
#define VPN_POLICY_TYPE(pVpnPolicy)         (pVpnPolicy->u4VpnPolicyType)

/* Crypto map phase-I information */
#define VPN_POLICY_PHASE1_PARAMS(pVpnPolicy)      (pVpnPolicy->uVpnKeyMode.\
                                                   VpnIkeDb.IkePhase1)
#define VPN_POLICY_PHASE1_LTIME_TYPE(pVpnPolicy)  \
    (VPN_POLICY_PHASE1_PARAMS(pVpnPolicy).u4LifeTimeType)
#define VPN_POLICY_PHASE1_LTIME_VALUE(pVpnPolicy) \
    (VPN_POLICY_PHASE1_PARAMS(pVpnPolicy).u4LifeTime)
#define VPN_POLICY_PHASE1_EXCHG_MODE(pVpnPolicy)  \
    (VPN_POLICY_PHASE1_PARAMS(pVpnPolicy).u4Mode)
#define VPN_POLICY_PHASE1_LOCAL_ID_TYPE(pVpnPolicy)     \
    (VPN_POLICY_PHASE1_PARAMS(pVpnPolicy).LocalID.i2IdType)
#define VPN_POLICY_PHASE1_PEER_ID_TYPE(pVpnPolicy)     \
    (VPN_POLICY_PHASE1_PARAMS(pVpnPolicy).PolicyPeerID.i2IdType)
                                                  
/* Crypto map phase-I information */
#define VPN_POLICY_PHASE2_PARAMS(pVpnPolicy)      (pVpnPolicy->uVpnKeyMode.\
                                                   VpnIkeDb.IkePhase2)
#define VPN_POLICY_PHASE2_LTIME_TYPE(pVpnPolicy)  \
    (VPN_POLICY_PHASE2_PARAMS(pVpnPolicy).u1LifeTimeType)
#define VPN_POLICY_PHASE2_LTIME_VALUE(pVpnPolicy) \
    (VPN_POLICY_PHASE2_PARAMS(pVpnPolicy).u4LifeTime)


#define        VPN_MAX_CIDR    32

#ifdef _VPN_UTILS_C__ 
UINT4               gau4VpnCidrSubnetMask[VPN_MAX_CIDR + 1] = {
    0x00000000, 0x80000000, 0xC0000000, 0xE0000000, 0xF0000000, 0xF8000000,
    0xFC000000, 0xFE000000, 0xFF000000, 0xFF800000, 0xFFC00000, 0xFFE00000,
    0xFFF00000, 0xFFF80000, 0xFFFC0000, 0xFFFE0000, 0xFFFF0000, 0xFFFF8000,
    0xFFFFC000, 0xFFFFE000, 0xFFFFF000, 0xFFFFF800, 0xFFFFFC00, 0xFFFFFE00,
    0xFFFFFF00, 0xFFFFFF80, 0xFFFFFFC0, 0xFFFFFFE0, 0xFFFFFFF0, 0xFFFFFFF8,
    0xFFFFFFFC, 0xFFFFFFFE, 0xFFFFFFFF
};
#else
extern UINT4               gau4VpnCidrSubnetMask[VPN_MAX_CIDR + 1];
#endif

 /* Constants used for validating IP address */
#define  VPN_BCAST_ADDR               255
#define  VPN_MCAST_ADDR               224
#define  VPN_ZERO_NETW_ADDR           0
#define  VPN_ZERO_ADDR                100
#define  VPN_LOOPBACK_ADDR            127
#define  VPN_UCAST_ADDR               1
#define  VPN_INVALID_ADDR             2
#define  VPN_CLASS_NETADDR            10  
#define  VPN_CLASS_BCASTADDR          200  
#define  VPN_CLASSE_ADDR              240
/* MAX valid CIDR subnet mask number */
#define   VPN_MAX_CIDR                   32               

#define VPN_LOG_BUFFSIZE   1440

#define VPN_INTF_DOWN    1
#define VPN_INTF_UP      2
#define VPN_INTF_IP_CHNG     3
#define VPN_INTF_DELETE      4
#define VPN_ADDR_ADD      5
#define VPN_ADDR_DEL      6
#define VPN_CALLBACK_FN  gaVpnCallBack 
#define VPN_CALLBACK_MAX_ARGS 3
#endif /* _FSVPNDEFN_H */


