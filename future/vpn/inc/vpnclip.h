/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vpnclip.h,v 1.12 2017/11/30 14:00:53 siva Exp $
 *
 * Description: File containing cli function prototypes
 *
 *******************************************************************/

#ifndef __VPNCLIP_H__
#define __VPNCLIP_H__

#include "vpncli.h"
#include "vpn.h"
#include "sec.h"

#ifdef __VPNCLI_C__
INT4
VpnCreatePolicy(tCliHandle CliHandle ,  tSNMP_OCTET_STRING_TYPE * pPolicyName );

INT4
VpnDeletePolicy(tCliHandle CliHandle , tSNMP_OCTET_STRING_TYPE * pPolicyName );

PRIVATE INT4
VpnDeletePolicyAll (tCliHandle CliHandle);

INT4
VpnSetEncryptType (tCliHandle CliHandle ,tSNMP_OCTET_STRING_TYPE * pPolicyName,
                                    UINT4 u4Type);

INT4
VpnSetPolicyType (tCliHandle CliHandle ,tSNMP_OCTET_STRING_TYPE * pPolicyName,
                                    UINT4 u4Index );

INT4
 VpnSetPeerIp (tCliHandle CliHandle ,tSNMP_OCTET_STRING_TYPE * pPolicyName ,
                              UINT4 u4PeerAddress);

INT4
VpnSetAccessParams(tSNMP_OCTET_STRING_TYPE *  pPolicyName,
                   tVpnNetwork *pLocalNet , tVpnNetwork *pRemoteNet,
                   UINT4  u4Action, UINT4 u4Proto,
                   tSNMP_OCTET_STRING_TYPE * pSrcPortRange, 
                   tSNMP_OCTET_STRING_TYPE * pDstPortRange);

INT4
VpnSetCryptoParams(tCliHandle CliHandle,tSNMP_OCTET_STRING_TYPE *  pPolicyName,
                   UINT4 u4AuthProto, UINT4 u4AuthAlgo, UINT1 * au1SecAssocAhKey,
                   UINT4 u4InboundSpi, UINT4 u4OutBoundSpi,INT4 i4EspProto,
                   UINT1 * au1SecAssocEspKey,UINT4 u4AntiReplay);

INT4
VpnSetPolicyInterface(tCliHandle CliHandle,tSNMP_OCTET_STRING_TYPE* pPolicyName,
                      UINT4 u4IfIndex );

INT4
VpnShowPolicyParms (tCliHandle CliHandle,tSNMP_OCTET_STRING_TYPE* pPolicyName );

INT4        
VpnSetPolicyMode(tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE *pPolicyName,
                 UINT4 u4Mode);

INT4
VpnShowPolicy (tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE * pPolicyName);

INT4
VpnCliSetVpnPolicyIpsecProposal (tCliHandle , tSNMP_OCTET_STRING_TYPE *,UINT4,
                                 UINT4 ,UINT4 , UINT4 , UINT4 ,UINT4);

INT4
VpnCliSetVpnPolicyIkeProposal (tCliHandle ,tSNMP_OCTET_STRING_TYPE *, UINT4 ,
                               UINT4, UINT4 , UINT4 , UINT4 , UINT4 );

INT4
VpnAddRemoteIdInfo ARG_LIST ((tCliHandle CliHandle, INT4 i4IdType,
                              UINT1 *pu1IdValue, UINT4 u4AuthType,
                              UINT1 *pu1PresharedKey));

INT4
VpnSetPeerType ( tCliHandle , tSNMP_OCTET_STRING_TYPE *, UINT4 , UINT1 *);

INT4
VpnSetLocalIdType ( tCliHandle , tSNMP_OCTET_STRING_TYPE *, UINT4 , UINT1 *);

PRIVATE INT4
VpnDisplayError ARG_LIST ((tCliHandle CliHandle, INT4 i4Error));

INT4 
VpnAddRaUserInfo(tCliHandle ,
                      tSNMP_OCTET_STRING_TYPE *pUserName,
                      tSNMP_OCTET_STRING_TYPE *pUserSecret);

INT4 
VpnDelRaUserInfo(tCliHandle ,
                 tSNMP_OCTET_STRING_TYPE *pUserName);

INT4 
VpnAddAddressPool(tCliHandle ,
                 tSNMP_OCTET_STRING_TYPE *pVpnRaAddressPoolName,
                 UINT4 , UINT4 );

INT4 
VpnDelAddressPool(tCliHandle CliHandle,
                  tSNMP_OCTET_STRING_TYPE *pVpnRaAddressPoolName);

INT4
VpnShowRaUsers (tCliHandle CliHandle );

INT4
VpnShowRaAddressPool (tCliHandle CliHandle );

INT4
VpnShowGlobPktsStats (tCliHandle CliHandle);
INT4
VpnShowGlobIkeStats (tCliHandle CliHandle);
INT4
VpnShowConfiguration (tCliHandle CliHandle);
INT4
VpnShowRemoteIdInfo ARG_LIST ((tCliHandle CliHandle, INT4 i4IdType));

INT4
VpnCliIkeTrace (tCliHandle CliHandle, UINT1 u1TraceFlag, INT4 i4TraceLevel);

INT4
VpnCliIpsecTrace (tCliHandle CliHandle, UINT1 u1TraceFlag, INT4 i4TraceLevel);



/* Certificate related changes - End */

INT4
VpnRAGlobalConfig (tCliHandle CliHandle, INT4 i4XAuthType);

INT4
VpnIkeCliTrigger (tCliHandle CliHandle, UINT4 u4PeerIp);INT4
VpnSetIkeVersion (tCliHandle CliHandle,
                  tSNMP_OCTET_STRING_TYPE * pPolicyName,
                  UINT4 u4VpnIkeVersion);
/* IPv6 related CLI commands */
INT4
VpnSetIpv6Peer (tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE * pPolicyName,
                tIp6Addr * pIp6Addr);
INT4 
VpnIkeIpv6CliTrigger (tCliHandle CliHandle, tIp6Addr * pPeerAddress);
INT4
VpnSetIpv6AccessParams (tSNMP_OCTET_STRING_TYPE * pPolicyName,
                             tVpnNetwork * pLocalNet, tVpnNetwork * pRemoteNet,
                             UINT4 u4Action, UINT4 u4Protocol,
                             tSNMP_OCTET_STRING_TYPE * pSrcPortRange,
                             tSNMP_OCTET_STRING_TYPE * pDstPortRange);
INT4
VpnIpv6AddAddressPool (tCliHandle CliHandle,
                       tSNMP_OCTET_STRING_TYPE * pVpnRaAddressPoolName,
                       tIp6Addr * pStartIpv6Address,
                       tIp6Addr * pEndIpv6Address, UINT4 u4PrefixLength);

UINT1 
VpnCliIkeImportKey (tCliHandle CliHandle, 
                    UINT1 *pu1KeyName, 
                    UINT1 *pu1FileName, 
                    INT4 u4Type);
UINT1
VpnCliIkeImportCert (tCliHandle CliHandle, 
                     UINT1 *pu1KeyName,
                     UINT1 *pu1FileName,
                     UINT1 u1EncodeType);

UINT1
VpnCliIkeImportCACert (tCliHandle CliHandle, 
                       UINT1 *pu1CaCertKeyString, 
                       UINT1 *pu1CaFileName,
                       UINT1 u1EncodeType);

UINT1 VpnCliIkeDelKeys (tCliHandle CliHandle, UINT1 *pu1KeyName);
UINT1 VpnCliIkeDelCerts (tCliHandle CliHandle,
                         UINT1 u1Option,
                         UINT1 *pu1KeyName);

INT4 
VpnShowRunningConfig (tCliHandle CliHandle);

INT4 
VpnShowRunningRemoteIds(tCliHandle CliHandle);

INT4 
VpnShowRunningRaUserTable(tCliHandle CliHandle);

INT4 
VpnShowRunningPolicyTables(tCliHandle CliHandle);

INT4 
VpnShowRunningRaVpnPoolTable(tCliHandle CliHandle);

INT4 
ShowRunningCaCertInfo(tCliHandle CliHandle);

INT4 
ShowRunningCertInfo(tCliHandle CliHandle);

INT4 
ShowRunningKeyInfo(tCliHandle CliHandle);
INT4 
VpnShowRunningPhase1PolicyEncryption(tCliHandle CliHandle, tVpnPolicy *pPolicy, tSNMP_OCTET_STRING_TYPE *pu1PolicyName );
INT4 
VpnShowRunningPhase2Policy(tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE *pu1PolicyName );
INT4 
VpnShowRunningAccessList(tCliHandle CliHandle,tVpnPolicy *pPolicy, tSNMP_OCTET_STRING_TYPE *pu1PolicyName );
INT4 
VpnShowRunningSessionKey( tCliHandle CliHandle,tSNMP_OCTET_STRING_TYPE *pu1PolicyName);
INT4
VpnShowRunningInterfacePolicyTable (tCliHandle CliHandle,tSNMP_OCTET_STRING_TYPE *pu1PolicyName);
#else
extern INT4
VpnCreatePolicy(tCliHandle CliHandle ,  tSNMP_OCTET_STRING_TYPE * pPolicyName );

extern INT4
VpnDeletePolicy(tCliHandle CliHandle , tSNMP_OCTET_STRING_TYPE * pPolicyName );

extern INT4
VpnSetEncryptType (tCliHandle CliHandle ,tSNMP_OCTET_STRING_TYPE * pPolicyName,
                                    UINT4 u4Type);

extern INT4
VpnSetPolicyType (tCliHandle CliHandle ,tSNMP_OCTET_STRING_TYPE * pPolicyName,
                                    UINT4 u4Index );

extern INT4
 VpnSetPeerIp (tCliHandle CliHandle ,tSNMP_OCTET_STRING_TYPE * pPolicyName ,
                              UINT4 u4PeerAddress);

extern INT4
VpnSetAccessParams(tSNMP_OCTET_STRING_TYPE *  pPolicyName,
                   tVpnNetwork *pLocalNet , tVpnNetwork *pRemoteNet,
                   UINT4  u4Action,UINT4 u4Proto,
                   tSNMP_OCTET_STRING_TYPE * pSrcPortRange, 
                   tSNMP_OCTET_STRING_TYPE * pDstPortRange);

extern INT4
VpnSetCryptoParams(tCliHandle CliHandle,tSNMP_OCTET_STRING_TYPE *  pPolicyName,
                   UINT4 u4AuthProto, UINT4 u4AuthAlgo, UINT1 * au1SecAssocAhKey,
                   UINT4 u4InboundSpi, UINT4 u4OutBoundSpi,INT4 i4EspProto,
                   UINT1 * au1SecAssocEspKey,UINT4 u4AntiReplay);

extern INT4
VpnSetPolicyInterface(tCliHandle CliHandle,tSNMP_OCTET_STRING_TYPE* pPolicyName,
                      UINT4 u4IfIndex );

extern INT4
VpnShowPolicyParms (tCliHandle CliHandle,tSNMP_OCTET_STRING_TYPE* pPolicyName );

extern INT4        
VpnSetPolicyMode(tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE *pPolicyName,
                 UINT4 u4Mode);

extern INT4
VpnShowPolicy (tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE * pPolicyName);

extern INT4
VpnCliSetVpnPolicyIpsecProposal (tCliHandle , tSNMP_OCTET_STRING_TYPE *,UINT4,
                                 UINT4 ,UINT4 , UINT4 , UINT4 ,UINT4);

extern INT4
VpnCliSetVpnPolicyIkeProposal (tCliHandle ,tSNMP_OCTET_STRING_TYPE *, UINT4 ,
                               UINT4, UINT4 , UINT4 , UINT4 , UINT4 );

extern INT4
VpnAddRemoteIdInfo (tCliHandle CliHandle, INT4 i4IdType, UINT1 *pu1IdValue,
                    UINT4, UINT1 *pu1PresharedKey);

extern INT4
VpnSetPeerType ( tCliHandle , tSNMP_OCTET_STRING_TYPE *, UINT4 , UINT1 *);

extern INT4
VpnSetLocalIdType ( tCliHandle , tSNMP_OCTET_STRING_TYPE *, UINT4 , UINT1 *);

extern INT4 
VpnAddRaUserInfo(tCliHandle ,
                      tSNMP_OCTET_STRING_TYPE *pUserName,
                      tSNMP_OCTET_STRING_TYPE *pUserSecret);

extern INT4 
VpnDelRaUserInfo(tCliHandle ,
                 tSNMP_OCTET_STRING_TYPE *pUserName);

extern INT4 
VpnAddAddressPool(tCliHandle ,
                 tSNMP_OCTET_STRING_TYPE *pVpnRaAddressPoolName,
                 UINT4 , UINT4 );

extern INT4 
VpnDelAddressPool(tCliHandle CliHandle,
                  tSNMP_OCTET_STRING_TYPE *pVpnRaAddressPoolName);

extern INT4
VpnShowConfiguration (tCliHandle CliHandle);

extern INT4
VpnClearVpnLogBuff (tCliHandle CliHandle);



/* Certificate related changes - Start */

extern INT4
VpnIkeDelRsaKeys(UINT1*, UINT4);

/* Certificate related changes - End */


extern INT4
VpnRAGlobalConfig (tCliHandle CliHandle, UINT4 u4RaVpnMode);

extern INT4
VpnSetIkeVersion (tCliHandle CliHandle,
                  tSNMP_OCTET_STRING_TYPE * pPolicyName,
                  UINT4 u4VpnIkeVersion);

/* IPv6 related CLI commands */
extern INT4
VpnSetIpv6Peer (tCliHandle CliHandle,
            tSNMP_OCTET_STRING_TYPE * pPolicyName, UINT1 * pu1PeerAddress);
extern INT4
VpnIkeIpv6CliTrigger (tCliHandle CliHandle, UINT1 * pu1PeerAddress);

extern INT4
VpnSetIpv6AccessParams (tSNMP_OCTET_STRING_TYPE * pPolicyName,
                             tVpnNetwork * pLocalNet, tVpnNetwork * pRemoteNet,
                             UINT4 u4Action, UINT4 u4Protocol,
                             tSNMP_OCTET_STRING_TYPE * pSrcPortRange,
                             tSNMP_OCTET_STRING_TYPE * pDstPortRange);
extern INT4
VpnIpv6AddAddressPool (tCliHandle CliHandle,
                       tSNMP_OCTET_STRING_TYPE * pVpnRaAddressPoolName,
                       UINT1* pu1VpnRaStartIpv6Address,
                       UINT1* pu1VpnRaEndIpv6Address, UINT4 u4PrefixLength);

#endif /* __VPNCLI_C__*/

#endif
