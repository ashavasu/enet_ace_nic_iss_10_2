/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vpnwtdfs.h,v 1.10 2012/11/08 11:07:40 siva Exp $
 *
 * Description: File structure defenitions for user-kernel framework
 *
 *******************************************************************/

typedef struct {
    int     cmd;
    INT4 *  pi4RetValFsVpnGlobalStatus;
    INT4    cliWebSetError;
    INT1    rval;
} tNpwnmhGetFsVpnGlobalStatus;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValFsVpnRaServer;
    INT4    cliWebSetError;
    INT1    rval;
} tNpwnmhGetFsVpnRaServer;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValFsVpnDummyPktGen;
    INT4    cliWebSetError;
    INT1    rval;
} tNpwnmhGetFsVpnDummyPktGen;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValFsVpnDummyPktParam;
    INT4    cliWebSetError;
    INT1    rval;
} tNpwnmhGetFsVpnDummyPktParam;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValFsIpsecTraceOption;
    INT4    cliWebSetError;
    INT1    rval;
} tNpwnmhGetFsIpsecTraceOption;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValFsVpnMaxTunnels;
    INT4    cliWebSetError;
    INT1    rval;
} tNpwnmhGetFsVpnMaxTunnels;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFsVpnIpPktsIn;
    INT4     cliWebSetError;
    INT1     rval;
} tNpwnmhGetFsVpnIpPktsIn;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFsVpnIpPktsOut;
    INT4     cliWebSetError;
    INT1     rval;
} tNpwnmhGetFsVpnIpPktsOut;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFsVpnPktsSecured;
    INT4     cliWebSetError;
    INT1     rval;
} tNpwnmhGetFsVpnPktsSecured;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFsVpnPktsDropped;
    INT4     cliWebSetError;
    INT1     rval;
} tNpwnmhGetFsVpnPktsDropped;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFsVpnIkeSAsActive;
    INT4     cliWebSetError;
    INT1     rval;
} tNpwnmhGetFsVpnIkeSAsActive;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFsVpnIkeNegotiations;
    INT4     cliWebSetError;
    INT1     rval;
} tNpwnmhGetFsVpnIkeNegotiations;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFsVpnIkeRekeys;
    INT4     cliWebSetError;
    INT1     rval;
} tNpwnmhGetFsVpnIkeRekeys;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFsVpnIkeNegoFailed;
    INT4     cliWebSetError;
    INT1     rval;
} tNpwnmhGetFsVpnIkeNegoFailed;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFsVpnIPSecSAsActive;
    INT4     cliWebSetError;
    INT1     rval;
} tNpwnmhGetFsVpnIPSecSAsActive;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFsVpnIPSecNegotiations;
    INT4     cliWebSetError;
    INT1     rval;
} tNpwnmhGetFsVpnIPSecNegotiations;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFsVpnIPSecNegoFailed;
    INT4     cliWebSetError;
    INT1     rval;
} tNpwnmhGetFsVpnIPSecNegoFailed;

typedef struct {
    int      cmd;
    UINT4 *  pu4RetValFsVpnTotalRekeys;
    INT4     cliWebSetError;
    INT1     rval;
} tNpwnmhGetFsVpnTotalRekeys;

typedef struct {
    int   cmd;
    INT4  i4SetValFsVpnGlobalStatus;
    INT4  cliWebSetError;
    INT1  rval;
} tNpwnmhSetFsVpnGlobalStatus;

typedef struct {
    int   cmd;
    INT4  i4SetValFsVpnRaServer;
    INT4  cliWebSetError;
    INT1  rval;
} tNpwnmhSetFsVpnRaServer;

typedef struct {
    int   cmd;
    INT4  i4SetValFsVpnDummyPktGen;
    INT4  cliWebSetError;
    INT1  rval;
} tNpwnmhSetFsVpnDummyPktGen;

typedef struct {
    int   cmd;
    INT4  i4SetValFsVpnDummyPktParam;
    INT4  cliWebSetError;
    INT1  rval;
} tNpwnmhSetFsVpnDummyPktParam;


typedef struct {
    int   cmd;
    INT4  i4SetValFsIpsecTraceOption;
    INT4  cliWebSetError;
    INT1  rval;
} tNpwnmhSetFsIpsecTraceOption;


typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValFsVpnGlobalStatus;
    INT4     cliWebSetError;
    INT1     rval;
} tNpwnmhTestv2FsVpnGlobalStatus;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValFsVpnRaServer;
    INT4     cliWebSetError;
    INT1     rval;
} tNpwnmhTestv2FsVpnRaServer;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValFsVpnDummyPktGen;
    INT4     cliWebSetError;
    INT1     rval;
} tNpwnmhTestv2FsVpnDummyPktGen;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValFsVpnDummyPktParam;
    INT4     cliWebSetError;
    INT1     rval;
} tNpwnmhTestv2FsVpnDummyPktParam;

typedef struct {
    int      cmd;
    UINT4 *  pu4ErrorCode;
    INT4     i4TestValFsIpsecTraceOption;
    INT4     cliWebSetError;
    INT1     rval;
} tNpwnmhTestv2FsIpsecTraceOption;

typedef struct {
    int               cmd;
    UINT4 *           pu4ErrorCode;
    tSnmpIndexList *  pSnmpIndexList;
    tSNMP_VAR_BIND *  pSnmpVarBind;
    INT4              cliWebSetError;
    INT1              rval;
} tNpwnmhDepv2FsVpnGlobalStatus;

typedef struct {
    int               cmd;
    UINT4 *           pu4ErrorCode;
    tSnmpIndexList *  pSnmpIndexList;
    tSNMP_VAR_BIND *  pSnmpVarBind;
    INT4              cliWebSetError;
    INT1              rval;
} tNpwnmhDepv2FsVpnRaServer;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhValidateIndexInstanceFsVpnTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFirstIndexFsVpnTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pNextFsVpnPolicyName;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetNextIndexFsVpnTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnPolicyType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnPolicyType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnCertAlgoType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnCertAlgoType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnPolicyPriority;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnPolicyPriority;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnTunTermAddrType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnTunTermAddrType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pRetValFsVpnLocalTunTermAddr;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnLocalTunTermAddr;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pRetValFsVpnRemoteTunTermAddr;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnRemoteTunTermAddr;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnProtectNetworkType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnProtectNetworkType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pRetValFsVpnLocalProtectNetwork;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnLocalProtectNetwork;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    UINT4 *                    pu4RetValFsVpnLocalProtectNetworkPrefixLen;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnLocalProtectNetworkPrefixLen;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pRetValFsVpnRemoteProtectNetwork;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnRemoteProtectNetwork;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    UINT4 *                    pu4RetValFsVpnRemoteProtectNetworkPrefixLen;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnRemoteProtectNetworkPrefixLen;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pRetValFsVpnIkeSrcPortRange;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnIkeSrcPortRange;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pRetValFsVpnIkeDstPortRange;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnIkeDstPortRange;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnSecurityProtocol;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnSecurityProtocol;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnInboundSpi;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnInboundSpi;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnOutboundSpi;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnOutboundSpi;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnMode;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnMode;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnAuthAlgo;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnAuthAlgo;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pRetValFsVpnAhKey;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnAhKey;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnEncrAlgo;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnEncrAlgo;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pRetValFsVpnEspKey;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnEspKey;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnAntiReplay;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnAntiReplay;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnPolicyFlag;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnPolicyFlag;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnProtocol;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnProtocol;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnPolicyIntfIndex;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnPolicyIntfIndex;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnIkePhase1HashAlgo;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnIkePhase1HashAlgo;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnIkePhase1EncryptionAlgo;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnIkePhase1EncryptionAlgo;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnIkePhase1DHGroup;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnIkePhase1DHGroup;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnIkePhase1LocalIdType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnIkePhase1LocalIdType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pRetValFsVpnIkePhase1LocalIdValue;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnIkePhase1LocalIdValue;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnIkePhase1PeerIdType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnIkePhase1PeerIdType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pRetValFsVpnIkePhase1PeerIdValue;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnIkePhase1PeerIdValue;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnIkePhase1LifeTimeType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnIkePhase1LifeTimeType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnIkePhase1LifeTime;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnIkePhase1LifeTime;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnIkePhase1Mode;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnIkePhase1Mode;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnIkePhase2AuthAlgo;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnIkePhase2AuthAlgo;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnIkePhase2EspEncryptionAlgo;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnIkePhase2EspEncryptionAlgo;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnIkePhase2LifeTimeType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnIkePhase2LifeTimeType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnIkePhase2LifeTime;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnIkePhase2LifeTime;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnIkePhase2DHGroup;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnIkePhase2DHGroup;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnIkeVersion;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnIkeVersion;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4 *                     pi4RetValFsVpnPolicyRowStatus;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnPolicyRowStatus;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnPolicyType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnPolicyType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnCertAlgoType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnCertAlgoType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnPolicyPriority;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnPolicyPriority;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnTunTermAddrType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnTunTermAddrType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pSetValFsVpnLocalTunTermAddr;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnLocalTunTermAddr;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pSetValFsVpnRemoteTunTermAddr;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnRemoteTunTermAddr;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnProtectNetworkType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnProtectNetworkType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pSetValFsVpnLocalProtectNetwork;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnLocalProtectNetwork;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    UINT4                      u4SetValFsVpnLocalProtectNetworkPrefixLen;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnLocalProtectNetworkPrefixLen;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pSetValFsVpnRemoteProtectNetwork;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnRemoteProtectNetwork;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    UINT4                      u4SetValFsVpnRemoteProtectNetworkPrefixLen;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnRemoteProtectNetworkPrefixLen;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pSetValFsVpnIkeSrcPortRange;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnIkeSrcPortRange;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pSetValFsVpnIkeDstPortRange;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnIkeDstPortRange;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnSecurityProtocol;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnSecurityProtocol;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnInboundSpi;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnInboundSpi;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnOutboundSpi;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnOutboundSpi;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnMode;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnMode;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnAuthAlgo;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnAuthAlgo;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pSetValFsVpnAhKey;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnAhKey;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnEncrAlgo;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnEncrAlgo;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pSetValFsVpnEspKey;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnEspKey;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnAntiReplay;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnAntiReplay;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnPolicyFlag;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnPolicyFlag;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnProtocol;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnProtocol;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnPolicyIntfIndex;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnPolicyIntfIndex;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnIkePhase1HashAlgo;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnIkePhase1HashAlgo;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnIkePhase1EncryptionAlgo;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnIkePhase1EncryptionAlgo;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnIkePhase1DHGroup;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnIkePhase1DHGroup;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnIkePhase1LocalIdType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnIkePhase1LocalIdType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pSetValFsVpnIkePhase1LocalIdValue;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnIkePhase1LocalIdValue;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnIkePhase1PeerIdType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnIkePhase1PeerIdType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pSetValFsVpnIkePhase1PeerIdValue;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnIkePhase1PeerIdValue;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnIkePhase1LifeTimeType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnIkePhase1LifeTimeType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnIkePhase1LifeTime;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnIkePhase1LifeTime;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnIkePhase1Mode;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnIkePhase1Mode;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnIkePhase2AuthAlgo;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnIkePhase2AuthAlgo;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnIkePhase2EspEncryptionAlgo;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnIkePhase2EspEncryptionAlgo;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnIkePhase2LifeTimeType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnIkePhase2LifeTimeType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnIkePhase2LifeTime;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnIkePhase2LifeTime;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnIkePhase2DHGroup;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnIkePhase2DHGroup;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnIkeVersion;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnIkeVersion;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4SetValFsVpnPolicyRowStatus;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnPolicyRowStatus;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnPolicyType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnPolicyType;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnCertAlgoType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnCertAlgoType;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnPolicyPriority;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnPolicyPriority;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnTunTermAddrType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnTunTermAddrType;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pTestValFsVpnLocalTunTermAddr;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnLocalTunTermAddr;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pTestValFsVpnRemoteTunTermAddr;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnRemoteTunTermAddr;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnProtectNetworkType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnProtectNetworkType;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pTestValFsVpnLocalProtectNetwork;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnLocalProtectNetwork;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    UINT4                      u4TestValFsVpnLocalProtectNetworkPrefixLen;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnLocalProtectNetworkPrefixLen;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pTestValFsVpnRemoteProtectNetwork;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnRemoteProtectNetwork;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    UINT4                      u4TestValFsVpnRemoteProtectNetworkPrefixLen;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnRemoteProtectNetworkPrefixLen;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pTestValFsVpnIkeSrcPortRange;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnIkeSrcPortRange;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pTestValFsVpnIkeDstPortRange;
    INT1                       rval;
    INT4                       cliWebSetError;
} tNpwnmhTestv2FsVpnIkeDstPortRange;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnSecurityProtocol;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnSecurityProtocol;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnInboundSpi;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnInboundSpi;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnOutboundSpi;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnOutboundSpi;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnMode;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnMode;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnAuthAlgo;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnAuthAlgo;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pTestValFsVpnAhKey;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnAhKey;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnEncrAlgo;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnEncrAlgo;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pTestValFsVpnEspKey;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnEspKey;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnAntiReplay;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnAntiReplay;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnPolicyFlag;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnPolicyFlag;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnProtocol;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnProtocol;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnPolicyIntfIndex;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnPolicyIntfIndex;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnIkePhase1HashAlgo;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnIkePhase1HashAlgo;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnIkePhase1EncryptionAlgo;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnIkePhase1EncryptionAlgo;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnIkePhase1DHGroup;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnIkePhase1DHGroup;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnIkePhase1LocalIdType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnIkePhase1LocalIdType;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pTestValFsVpnIkePhase1LocalIdValue;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnIkePhase1LocalIdValue;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnIkePhase1PeerIdType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnIkePhase1PeerIdType;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    tSNMP_OCTET_STRING_TYPE *  pTestValFsVpnIkePhase1PeerIdValue;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnIkePhase1PeerIdValue;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnIkePhase1LifeTimeType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnIkePhase1LifeTimeType;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnIkePhase1LifeTime;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnIkePhase1LifeTime;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnIkePhase1Mode;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnIkePhase1Mode;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnIkePhase2AuthAlgo;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnIkePhase2AuthAlgo;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnIkePhase2EspEncryptionAlgo;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnIkePhase2EspEncryptionAlgo;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnIkePhase2LifeTimeType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnIkePhase2LifeTimeType;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnIkePhase2LifeTime;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnIkePhase2LifeTime;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnIkePhase2DHGroup;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnIkePhase2DHGroup;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnIkeVersion;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnIkeVersion;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnPolicyName;
    INT4                       i4TestValFsVpnPolicyRowStatus;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnPolicyRowStatus;

typedef struct {
    int               cmd;
    UINT4 *           pu4ErrorCode;
    tSnmpIndexList *  pSnmpIndexList;
    tSNMP_VAR_BIND *  pSnmpVarBind;
    INT4                       cliWebSetError;
    INT1              rval;
} tNpwnmhDepv2FsVpnTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaUserName;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhValidateIndexInstanceFsVpnRaUsersTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaUserName;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFirstIndexFsVpnRaUsersTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaUserName;
    tSNMP_OCTET_STRING_TYPE *  pNextFsVpnRaUserName;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetNextIndexFsVpnRaUsersTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaUserName;
    tSNMP_OCTET_STRING_TYPE *  pRetValFsVpnRaUserSecret;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnRaUserSecret;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaUserName;
    INT4 *                     pi4RetValFsVpnRaUserRowStatus;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnRaUserRowStatus;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaUserName;
    tSNMP_OCTET_STRING_TYPE *  pSetValFsVpnRaUserSecret;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnRaUserSecret;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaUserName;
    INT4                       i4SetValFsVpnRaUserRowStatus;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnRaUserRowStatus;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaUserName;
    tSNMP_OCTET_STRING_TYPE *  pTestValFsVpnRaUserSecret;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnRaUserSecret;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaUserName;
    INT4                       i4TestValFsVpnRaUserRowStatus;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnRaUserRowStatus;

typedef struct {
    int               cmd;
    UINT4 *           pu4ErrorCode;
    tSnmpIndexList *  pSnmpIndexList;
    tSNMP_VAR_BIND *  pSnmpVarBind;
    INT4                       cliWebSetError;
    INT1              rval;
} tNpwnmhDepv2FsVpnRaUsersTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaAddressPoolName;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhValidateIndexInstanceFsVpnRaAddressPoolTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaAddressPoolName;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFirstIndexFsVpnRaAddressPoolTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaAddressPoolName;
    tSNMP_OCTET_STRING_TYPE *  pNextFsVpnRaAddressPoolName;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetNextIndexFsVpnRaAddressPoolTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaAddressPoolName;
    INT4 *                     pi4RetValFsVpnRaAddressPoolAddrType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnRaAddressPoolAddrType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaAddressPoolName;
    tSNMP_OCTET_STRING_TYPE *  pRetValFsVpnRaAddressPoolStart;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnRaAddressPoolStart;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaAddressPoolName;
    tSNMP_OCTET_STRING_TYPE *  pRetValFsVpnRaAddressPoolEnd;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnRaAddressPoolEnd;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaAddressPoolName;
    UINT4 *                    pu4RetValFsVpnRaAddressPoolPrefixLen;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnRaAddressPoolPrefixLen;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaAddressPoolName;
    INT4 *                     pi4RetValFsVpnRaAddressPoolRowStatus;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnRaAddressPoolRowStatus;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaAddressPoolName;
    INT4                       i4SetValFsVpnRaAddressPoolAddrType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnRaAddressPoolAddrType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaAddressPoolName;
    tSNMP_OCTET_STRING_TYPE *  pSetValFsVpnRaAddressPoolStart;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnRaAddressPoolStart;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaAddressPoolName;
    tSNMP_OCTET_STRING_TYPE *  pSetValFsVpnRaAddressPoolEnd;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnRaAddressPoolEnd;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaAddressPoolName;
    UINT4                      u4SetValFsVpnRaAddressPoolPrefixLen;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnRaAddressPoolPrefixLen;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaAddressPoolName;
    INT4                       i4SetValFsVpnRaAddressPoolRowStatus;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnRaAddressPoolRowStatus;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaAddressPoolName;
    INT4                       i4TestValFsVpnRaAddressPoolAddrType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnRaAddressPoolAddrType;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaAddressPoolName;
    tSNMP_OCTET_STRING_TYPE *  pTestValFsVpnRaAddressPoolStart;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnRaAddressPoolStart;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaAddressPoolName;
    tSNMP_OCTET_STRING_TYPE *  pTestValFsVpnRaAddressPoolEnd;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnRaAddressPoolEnd;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaAddressPoolName;
    UINT4                      u4TestValFsVpnRaAddressPoolPrefixLen;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnRaAddressPoolPrefixLen;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRaAddressPoolName;
    INT4                       i4TestValFsVpnRaAddressPoolRowStatus;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnRaAddressPoolRowStatus;

typedef struct {
    int               cmd;
    UINT4 *           pu4ErrorCode;
    tSnmpIndexList *  pSnmpIndexList;
    tSNMP_VAR_BIND *  pSnmpVarBind;
    INT4              cliWebSetError;
    INT1              rval;
} tNpwnmhDepv2FsVpnRaAddressPoolTable;

typedef struct {
    int                        cmd;
    INT4                       i4FsVpnRemoteIdType;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRemoteIdValue;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhValidateIndexInstanceFsVpnRemoteIdTable;

typedef struct {
    int                        cmd;
    INT4 *                     pi4FsVpnRemoteIdType;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRemoteIdValue;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFirstIndexFsVpnRemoteIdTable;

typedef struct {
    int                        cmd;
    INT4                       i4FsVpnRemoteIdType;
    INT4 *                     pi4NextFsVpnRemoteIdType;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRemoteIdValue;
    tSNMP_OCTET_STRING_TYPE *  pNextFsVpnRemoteIdValue;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetNextIndexFsVpnRemoteIdTable;

typedef struct {
    int                        cmd;
    INT4                       i4FsVpnRemoteIdType;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRemoteIdValue;
    tSNMP_OCTET_STRING_TYPE *  pRetValFsVpnRemoteIdKey;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnRemoteIdKey;

typedef struct {
    int                        cmd;
    INT4                       i4FsVpnRemoteIdType;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRemoteIdValue;
    INT4        *  pi4RetValFsVpnRemoteIdAuthType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnRemoteIdAuthType;

typedef struct {
    int                        cmd;
    INT4                       i4FsVpnRemoteIdType;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRemoteIdValue;
    INT4           i4SetValFsVpnRemoteIdAuthType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnRemoteIdAuthType;

typedef struct {
    int                        cmd;
    UINT4                   *  pu4ErrorCode;
    INT4           i4FsVpnRemoteIdType;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRemoteIdValue;
    INT4           i4TestValFsVpnRemoteIdAuthType;
    INT4                       cliWebSetError;
    INT1                       rval;
}tNpwnmhTestv2FsVpnRemoteIdAuthType; 

typedef struct {
    int                        cmd;
    INT4                       i4FsVpnRemoteIdType;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRemoteIdValue;
    INT4 *                     pi4RetValFsVpnRemoteIdStatus;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnRemoteIdStatus;

typedef struct {
    int                        cmd;
    INT4                       i4FsVpnRemoteIdType;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRemoteIdValue;
    tSNMP_OCTET_STRING_TYPE *  pSetValFsVpnRemoteIdKey;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnRemoteIdKey;

typedef struct {
    int                        cmd;
    INT4                       i4FsVpnRemoteIdType;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRemoteIdValue;
    INT4                       i4SetValFsVpnRemoteIdStatus;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnRemoteIdStatus;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    INT4                       i4FsVpnRemoteIdType;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRemoteIdValue;
    tSNMP_OCTET_STRING_TYPE *  pTestValFsVpnRemoteIdKey;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnRemoteIdKey;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    INT4                       i4FsVpnRemoteIdType;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnRemoteIdValue;
    INT4                       i4TestValFsVpnRemoteIdStatus;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnRemoteIdStatus;

typedef struct {
    int               cmd;
    UINT4 *           pu4ErrorCode;
    tSnmpIndexList *  pSnmpIndexList;
    tSNMP_VAR_BIND *  pSnmpVarBind;
    INT4              cliWebSetError;
    INT1              rval;
} tNpwnmhDepv2FsVpnRemoteIdTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCertKeyString;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhValidateIndexInstanceFsVpnCertInfoTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCertKeyString;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFirstIndexFsVpnCertInfoTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCertKeyString;
    tSNMP_OCTET_STRING_TYPE *  pNextFsVpnCertKeyString;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetNextIndexFsVpnCertInfoTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCertKeyString;
    INT4 *                     pi4RetValFsVpnCertKeyType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnCertKeyType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCertKeyString;
    tSNMP_OCTET_STRING_TYPE *  pRetValFsVpnCertKeyFileName;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnCertKeyFileName;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCertKeyString;
    tSNMP_OCTET_STRING_TYPE *  pRetValFsVpnCertFileName;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnCertFileName;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCertKeyString;
    INT4 *                     pi4RetValFsVpnCertEncodeType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnCertEncodeType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCertKeyString;
    INT4 *                     pi4RetValFsVpnCertStatus;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnCertStatus;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCertKeyString;
    INT4                       i4SetValFsVpnCertKeyType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnCertKeyType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCertKeyString;
    tSNMP_OCTET_STRING_TYPE *  pSetValFsVpnCertKeyFileName;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnCertKeyFileName;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCertKeyString;
    tSNMP_OCTET_STRING_TYPE *  pSetValFsVpnCertFileName;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnCertFileName;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCertKeyString;
    INT4                       i4SetValFsVpnCertEncodeType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnCertEncodeType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCertKeyString;
    INT4                       i4SetValFsVpnCertStatus;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnCertStatus;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCertKeyString;
    INT4                       i4TestValFsVpnCertKeyType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnCertKeyType;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCertKeyString;
    tSNMP_OCTET_STRING_TYPE *  pTestValFsVpnCertKeyFileName;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnCertKeyFileName;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCertKeyString;
    tSNMP_OCTET_STRING_TYPE *  pTestValFsVpnCertFileName;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnCertFileName;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCertKeyString;
    INT4                       i4TestValFsVpnCertEncodeType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnCertEncodeType;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCertKeyString;
    INT4                       i4TestValFsVpnCertStatus;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnCertStatus;

typedef struct {
    int               cmd;
    UINT4 *           pu4ErrorCode;
    tSnmpIndexList *  pSnmpIndexList;
    tSNMP_VAR_BIND *  pSnmpVarBind;
    INT4              cliWebSetError;
    INT1              rval;
} tNpwnmhDepv2FsVpnCertInfoTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCaCertKeyString;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhValidateIndexInstanceFsVpnCaCertInfoTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCaCertKeyString;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFirstIndexFsVpnCaCertInfoTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCaCertKeyString;
    tSNMP_OCTET_STRING_TYPE *  pNextFsVpnCaCertKeyString;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetNextIndexFsVpnCaCertInfoTable;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCaCertKeyString;
    tSNMP_OCTET_STRING_TYPE *  pRetValFsVpnCaCertFileName;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnCaCertFileName;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCaCertKeyString;
    INT4 *                     pi4RetValFsVpnCaCertEncodeType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnCaCertEncodeType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCaCertKeyString;
    INT4 *                     pi4RetValFsVpnCaCertStatus;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhGetFsVpnCaCertStatus;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCaCertKeyString;
    tSNMP_OCTET_STRING_TYPE *  pSetValFsVpnCaCertFileName;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnCaCertFileName;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCaCertKeyString;
    INT4                       i4SetValFsVpnCaCertEncodeType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnCaCertEncodeType;

typedef struct {
    int                        cmd;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCaCertKeyString;
    INT4                       i4SetValFsVpnCaCertStatus;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhSetFsVpnCaCertStatus;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCaCertKeyString;
    tSNMP_OCTET_STRING_TYPE *  pTestValFsVpnCaCertFileName;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnCaCertFileName;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCaCertKeyString;
    INT4                       i4TestValFsVpnCaCertEncodeType;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnCaCertEncodeType;

typedef struct {
    int                        cmd;
    UINT4 *                    pu4ErrorCode;
    tSNMP_OCTET_STRING_TYPE *  pFsVpnCaCertKeyString;
    INT4                       i4TestValFsVpnCaCertStatus;
    INT4                       cliWebSetError;
    INT1                       rval;
} tNpwnmhTestv2FsVpnCaCertStatus;

typedef struct {
    int               cmd;
    UINT4 *           pu4ErrorCode;
    tSnmpIndexList *  pSnmpIndexList;
    tSNMP_VAR_BIND *  pSnmpVarBind;
    INT4                       cliWebSetError;
    INT1              rval;
} tNpwnmhDepv2FsVpnCaCertInfoTable;
typedef struct {
    int     cmd;
    INT4 *  pi4RetValSecv4BypassCapability;
    INT4    cliWebSetError;
    INT1    rval;
} tNpwnmhGetSecv4BypassCapability;

typedef struct {
    int     cmd;
    INT4    pi4SetValSecv4BypassCapability;
    INT4    cliWebSetError;
    INT1    rval;
} tNpwnmhSetSecv4BypassCapability;

typedef struct {
    int     cmd;
    INT4 *  pi4RetValSecv6BypassCapability;
    INT4    cliWebSetError;
    INT1    rval;
} tNpwnmhGetSecv6BypassCapability;

typedef struct {
    int     cmd;
    INT4    pi4SetValSecv6BypassCapability;
    INT4    cliWebSetError;
    INT1    rval;
} tNpwnmhSetSecv6BypassCapability;

union unNpapi {
    tNpwnmhGetFsVpnGlobalStatus nmhGetFsVpnGlobalStatus;
    tNpwnmhGetFsVpnRaServer nmhGetFsVpnRaServer;
    tNpwnmhGetFsVpnMaxTunnels nmhGetFsVpnMaxTunnels;
    tNpwnmhGetFsVpnIpPktsIn nmhGetFsVpnIpPktsIn;
    tNpwnmhGetFsVpnIpPktsOut nmhGetFsVpnIpPktsOut;
    tNpwnmhGetFsVpnPktsSecured nmhGetFsVpnPktsSecured;
    tNpwnmhGetFsVpnPktsDropped nmhGetFsVpnPktsDropped;
    tNpwnmhGetFsVpnIkeSAsActive nmhGetFsVpnIkeSAsActive;
    tNpwnmhGetFsVpnIkeNegotiations nmhGetFsVpnIkeNegotiations;
    tNpwnmhGetFsVpnIkeRekeys nmhGetFsVpnIkeRekeys;
    tNpwnmhGetFsVpnIkeNegoFailed nmhGetFsVpnIkeNegoFailed;
    tNpwnmhGetFsVpnIPSecSAsActive nmhGetFsVpnIPSecSAsActive;
    tNpwnmhGetFsVpnIPSecNegotiations nmhGetFsVpnIPSecNegotiations;
    tNpwnmhGetFsVpnIPSecNegoFailed nmhGetFsVpnIPSecNegoFailed;
    tNpwnmhGetFsVpnTotalRekeys nmhGetFsVpnTotalRekeys;
    tNpwnmhSetFsVpnGlobalStatus nmhSetFsVpnGlobalStatus;
    tNpwnmhSetFsVpnRaServer nmhSetFsVpnRaServer;
    tNpwnmhTestv2FsVpnGlobalStatus nmhTestv2FsVpnGlobalStatus;
    tNpwnmhTestv2FsVpnRaServer nmhTestv2FsVpnRaServer;
    tNpwnmhDepv2FsVpnGlobalStatus nmhDepv2FsVpnGlobalStatus;
    tNpwnmhDepv2FsVpnRaServer nmhDepv2FsVpnRaServer;
    tNpwnmhValidateIndexInstanceFsVpnTable nmhValidateIndexInstanceFsVpnTable;
    tNpwnmhGetFirstIndexFsVpnTable nmhGetFirstIndexFsVpnTable;
    tNpwnmhGetNextIndexFsVpnTable nmhGetNextIndexFsVpnTable;
    tNpwnmhGetFsVpnPolicyType nmhGetFsVpnPolicyType;
    tNpwnmhGetFsVpnCertAlgoType nmhGetFsVpnCertAlgoType;
    tNpwnmhGetFsVpnPolicyPriority nmhGetFsVpnPolicyPriority;
    tNpwnmhGetFsVpnTunTermAddrType nmhGetFsVpnTunTermAddrType;
    tNpwnmhGetFsVpnLocalTunTermAddr nmhGetFsVpnLocalTunTermAddr;
    tNpwnmhGetFsVpnRemoteTunTermAddr nmhGetFsVpnRemoteTunTermAddr;
    tNpwnmhGetFsVpnProtectNetworkType nmhGetFsVpnProtectNetworkType;
    tNpwnmhGetFsVpnLocalProtectNetwork nmhGetFsVpnLocalProtectNetwork;
    tNpwnmhGetFsVpnLocalProtectNetworkPrefixLen nmhGetFsVpnLocalProtectNetworkPrefixLen;
    tNpwnmhGetFsVpnRemoteProtectNetwork nmhGetFsVpnRemoteProtectNetwork;
    tNpwnmhGetFsVpnRemoteProtectNetworkPrefixLen nmhGetFsVpnRemoteProtectNetworkPrefixLen;
    tNpwnmhGetFsVpnIkeSrcPortRange nmhGetFsVpnIkeSrcPortRange;
    tNpwnmhGetFsVpnIkeDstPortRange nmhGetFsVpnIkeDstPortRange;
    tNpwnmhGetFsVpnSecurityProtocol nmhGetFsVpnSecurityProtocol;
    tNpwnmhGetFsVpnInboundSpi nmhGetFsVpnInboundSpi;
    tNpwnmhGetFsVpnOutboundSpi nmhGetFsVpnOutboundSpi;
    tNpwnmhGetFsVpnMode nmhGetFsVpnMode;
    tNpwnmhGetFsVpnAuthAlgo nmhGetFsVpnAuthAlgo;
    tNpwnmhGetFsVpnAhKey nmhGetFsVpnAhKey;
    tNpwnmhGetFsVpnEncrAlgo nmhGetFsVpnEncrAlgo;
    tNpwnmhGetFsVpnEspKey nmhGetFsVpnEspKey;
    tNpwnmhGetFsVpnAntiReplay nmhGetFsVpnAntiReplay;
    tNpwnmhGetFsVpnPolicyFlag nmhGetFsVpnPolicyFlag;
    tNpwnmhGetFsVpnProtocol nmhGetFsVpnProtocol;
    tNpwnmhGetFsVpnPolicyIntfIndex nmhGetFsVpnPolicyIntfIndex;
    tNpwnmhGetFsVpnIkePhase1HashAlgo nmhGetFsVpnIkePhase1HashAlgo;
    tNpwnmhGetFsVpnIkePhase1EncryptionAlgo nmhGetFsVpnIkePhase1EncryptionAlgo;
    tNpwnmhGetFsVpnIkePhase1DHGroup nmhGetFsVpnIkePhase1DHGroup;
    tNpwnmhGetFsVpnIkePhase1LocalIdType nmhGetFsVpnIkePhase1LocalIdType;
    tNpwnmhGetFsVpnIkePhase1LocalIdValue nmhGetFsVpnIkePhase1LocalIdValue;
    tNpwnmhGetFsVpnIkePhase1PeerIdType nmhGetFsVpnIkePhase1PeerIdType;
    tNpwnmhGetFsVpnIkePhase1PeerIdValue nmhGetFsVpnIkePhase1PeerIdValue;
    tNpwnmhGetFsVpnIkePhase1LifeTimeType nmhGetFsVpnIkePhase1LifeTimeType;
    tNpwnmhGetFsVpnIkePhase1LifeTime nmhGetFsVpnIkePhase1LifeTime;
    tNpwnmhGetFsVpnIkePhase1Mode nmhGetFsVpnIkePhase1Mode;
    tNpwnmhGetFsVpnIkePhase2AuthAlgo nmhGetFsVpnIkePhase2AuthAlgo;
    tNpwnmhGetFsVpnIkePhase2EspEncryptionAlgo nmhGetFsVpnIkePhase2EspEncryptionAlgo;
    tNpwnmhGetFsVpnIkePhase2LifeTimeType nmhGetFsVpnIkePhase2LifeTimeType;
    tNpwnmhGetFsVpnIkePhase2LifeTime nmhGetFsVpnIkePhase2LifeTime;
    tNpwnmhGetFsVpnIkePhase2DHGroup nmhGetFsVpnIkePhase2DHGroup;
    tNpwnmhGetFsVpnIkeVersion nmhGetFsVpnIkeVersion;
    tNpwnmhGetFsVpnPolicyRowStatus nmhGetFsVpnPolicyRowStatus;
    tNpwnmhSetFsVpnPolicyType nmhSetFsVpnPolicyType;
    tNpwnmhSetFsVpnCertAlgoType nmhSetFsVpnCertAlgoType;
    tNpwnmhSetFsVpnPolicyPriority nmhSetFsVpnPolicyPriority;
    tNpwnmhSetFsVpnTunTermAddrType nmhSetFsVpnTunTermAddrType;
    tNpwnmhSetFsVpnLocalTunTermAddr nmhSetFsVpnLocalTunTermAddr;
    tNpwnmhSetFsVpnRemoteTunTermAddr nmhSetFsVpnRemoteTunTermAddr;
    tNpwnmhSetFsVpnProtectNetworkType nmhSetFsVpnProtectNetworkType;
    tNpwnmhSetFsVpnLocalProtectNetwork nmhSetFsVpnLocalProtectNetwork;
    tNpwnmhSetFsVpnLocalProtectNetworkPrefixLen nmhSetFsVpnLocalProtectNetworkPrefixLen;
    tNpwnmhSetFsVpnRemoteProtectNetwork nmhSetFsVpnRemoteProtectNetwork;
    tNpwnmhSetFsVpnRemoteProtectNetworkPrefixLen nmhSetFsVpnRemoteProtectNetworkPrefixLen;
    tNpwnmhSetFsVpnIkeSrcPortRange nmhSetFsVpnIkeSrcPortRange;
    tNpwnmhSetFsVpnIkeDstPortRange nmhSetFsVpnIkeDstPortRange;
    tNpwnmhSetFsVpnSecurityProtocol nmhSetFsVpnSecurityProtocol;
    tNpwnmhSetFsVpnInboundSpi nmhSetFsVpnInboundSpi;
    tNpwnmhSetFsVpnOutboundSpi nmhSetFsVpnOutboundSpi;
    tNpwnmhSetFsVpnMode nmhSetFsVpnMode;
    tNpwnmhSetFsVpnAuthAlgo nmhSetFsVpnAuthAlgo;
    tNpwnmhSetFsVpnAhKey nmhSetFsVpnAhKey;
    tNpwnmhSetFsVpnEncrAlgo nmhSetFsVpnEncrAlgo;
    tNpwnmhSetFsVpnEspKey nmhSetFsVpnEspKey;
    tNpwnmhSetFsVpnAntiReplay nmhSetFsVpnAntiReplay;
    tNpwnmhSetFsVpnPolicyFlag nmhSetFsVpnPolicyFlag;
    tNpwnmhSetFsVpnProtocol nmhSetFsVpnProtocol;
    tNpwnmhSetFsVpnPolicyIntfIndex nmhSetFsVpnPolicyIntfIndex;
    tNpwnmhSetFsVpnIkePhase1HashAlgo nmhSetFsVpnIkePhase1HashAlgo;
    tNpwnmhSetFsVpnIkePhase1EncryptionAlgo nmhSetFsVpnIkePhase1EncryptionAlgo;
    tNpwnmhSetFsVpnIkePhase1DHGroup nmhSetFsVpnIkePhase1DHGroup;
    tNpwnmhSetFsVpnIkePhase1LocalIdType nmhSetFsVpnIkePhase1LocalIdType;
    tNpwnmhSetFsVpnIkePhase1LocalIdValue nmhSetFsVpnIkePhase1LocalIdValue;
    tNpwnmhSetFsVpnIkePhase1PeerIdType nmhSetFsVpnIkePhase1PeerIdType;
    tNpwnmhSetFsVpnIkePhase1PeerIdValue nmhSetFsVpnIkePhase1PeerIdValue;
    tNpwnmhSetFsVpnIkePhase1LifeTimeType nmhSetFsVpnIkePhase1LifeTimeType;
    tNpwnmhSetFsVpnIkePhase1LifeTime nmhSetFsVpnIkePhase1LifeTime;
    tNpwnmhSetFsVpnIkePhase1Mode nmhSetFsVpnIkePhase1Mode;
    tNpwnmhSetFsVpnIkePhase2AuthAlgo nmhSetFsVpnIkePhase2AuthAlgo;
    tNpwnmhSetFsVpnIkePhase2EspEncryptionAlgo nmhSetFsVpnIkePhase2EspEncryptionAlgo;
    tNpwnmhSetFsVpnIkePhase2LifeTimeType nmhSetFsVpnIkePhase2LifeTimeType;
    tNpwnmhSetFsVpnIkePhase2LifeTime nmhSetFsVpnIkePhase2LifeTime;
    tNpwnmhSetFsVpnIkePhase2DHGroup nmhSetFsVpnIkePhase2DHGroup;
    tNpwnmhSetFsVpnIkeVersion nmhSetFsVpnIkeVersion;
    tNpwnmhSetFsVpnPolicyRowStatus nmhSetFsVpnPolicyRowStatus;
    tNpwnmhTestv2FsVpnPolicyType nmhTestv2FsVpnPolicyType;
    tNpwnmhTestv2FsVpnCertAlgoType nmhTestv2FsVpnCertAlgoType;
    tNpwnmhTestv2FsVpnPolicyPriority nmhTestv2FsVpnPolicyPriority;
    tNpwnmhTestv2FsVpnTunTermAddrType nmhTestv2FsVpnTunTermAddrType;
    tNpwnmhTestv2FsVpnLocalTunTermAddr nmhTestv2FsVpnLocalTunTermAddr;
    tNpwnmhTestv2FsVpnRemoteTunTermAddr nmhTestv2FsVpnRemoteTunTermAddr;
    tNpwnmhTestv2FsVpnProtectNetworkType nmhTestv2FsVpnProtectNetworkType;
    tNpwnmhTestv2FsVpnLocalProtectNetwork nmhTestv2FsVpnLocalProtectNetwork;
    tNpwnmhTestv2FsVpnLocalProtectNetworkPrefixLen nmhTestv2FsVpnLocalProtectNetworkPrefixLen;
    tNpwnmhTestv2FsVpnRemoteProtectNetwork nmhTestv2FsVpnRemoteProtectNetwork;
    tNpwnmhTestv2FsVpnRemoteProtectNetworkPrefixLen nmhTestv2FsVpnRemoteProtectNetworkPrefixLen;
    tNpwnmhTestv2FsVpnIkeSrcPortRange nmhTestv2FsVpnIkeSrcPortRange;
    tNpwnmhTestv2FsVpnIkeDstPortRange nmhTestv2FsVpnIkeDstPortRange;
    tNpwnmhTestv2FsVpnSecurityProtocol nmhTestv2FsVpnSecurityProtocol;
    tNpwnmhTestv2FsVpnInboundSpi nmhTestv2FsVpnInboundSpi;
    tNpwnmhTestv2FsVpnOutboundSpi nmhTestv2FsVpnOutboundSpi;
    tNpwnmhTestv2FsVpnMode nmhTestv2FsVpnMode;
    tNpwnmhTestv2FsVpnAuthAlgo nmhTestv2FsVpnAuthAlgo;
    tNpwnmhTestv2FsVpnAhKey nmhTestv2FsVpnAhKey;
    tNpwnmhTestv2FsVpnEncrAlgo nmhTestv2FsVpnEncrAlgo;
    tNpwnmhTestv2FsVpnEspKey nmhTestv2FsVpnEspKey;
    tNpwnmhTestv2FsVpnAntiReplay nmhTestv2FsVpnAntiReplay;
    tNpwnmhTestv2FsVpnPolicyFlag nmhTestv2FsVpnPolicyFlag;
    tNpwnmhTestv2FsVpnProtocol nmhTestv2FsVpnProtocol;
    tNpwnmhTestv2FsVpnPolicyIntfIndex nmhTestv2FsVpnPolicyIntfIndex;
    tNpwnmhTestv2FsVpnIkePhase1HashAlgo nmhTestv2FsVpnIkePhase1HashAlgo;
    tNpwnmhTestv2FsVpnIkePhase1EncryptionAlgo nmhTestv2FsVpnIkePhase1EncryptionAlgo;
    tNpwnmhTestv2FsVpnIkePhase1DHGroup nmhTestv2FsVpnIkePhase1DHGroup;
    tNpwnmhTestv2FsVpnIkePhase1LocalIdType nmhTestv2FsVpnIkePhase1LocalIdType;
    tNpwnmhTestv2FsVpnIkePhase1LocalIdValue nmhTestv2FsVpnIkePhase1LocalIdValue;
    tNpwnmhTestv2FsVpnIkePhase1PeerIdType nmhTestv2FsVpnIkePhase1PeerIdType;
    tNpwnmhTestv2FsVpnIkePhase1PeerIdValue nmhTestv2FsVpnIkePhase1PeerIdValue;
    tNpwnmhTestv2FsVpnIkePhase1LifeTimeType nmhTestv2FsVpnIkePhase1LifeTimeType;
    tNpwnmhTestv2FsVpnIkePhase1LifeTime nmhTestv2FsVpnIkePhase1LifeTime;
    tNpwnmhTestv2FsVpnIkePhase1Mode nmhTestv2FsVpnIkePhase1Mode;
    tNpwnmhTestv2FsVpnIkePhase2AuthAlgo nmhTestv2FsVpnIkePhase2AuthAlgo;
    tNpwnmhTestv2FsVpnIkePhase2EspEncryptionAlgo nmhTestv2FsVpnIkePhase2EspEncryptionAlgo;
    tNpwnmhTestv2FsVpnIkePhase2LifeTimeType nmhTestv2FsVpnIkePhase2LifeTimeType;
    tNpwnmhTestv2FsVpnIkePhase2LifeTime nmhTestv2FsVpnIkePhase2LifeTime;
    tNpwnmhTestv2FsVpnIkePhase2DHGroup nmhTestv2FsVpnIkePhase2DHGroup;
    tNpwnmhTestv2FsVpnIkeVersion nmhTestv2FsVpnIkeVersion;
    tNpwnmhTestv2FsVpnPolicyRowStatus nmhTestv2FsVpnPolicyRowStatus;
    tNpwnmhDepv2FsVpnTable nmhDepv2FsVpnTable;
    tNpwnmhValidateIndexInstanceFsVpnRaUsersTable nmhValidateIndexInstanceFsVpnRaUsersTable;
    tNpwnmhGetFirstIndexFsVpnRaUsersTable nmhGetFirstIndexFsVpnRaUsersTable;
    tNpwnmhGetNextIndexFsVpnRaUsersTable nmhGetNextIndexFsVpnRaUsersTable;
    tNpwnmhGetFsVpnRaUserSecret nmhGetFsVpnRaUserSecret;
    tNpwnmhGetFsVpnRaUserRowStatus nmhGetFsVpnRaUserRowStatus;
    tNpwnmhSetFsVpnRaUserSecret nmhSetFsVpnRaUserSecret;
    tNpwnmhSetFsVpnRaUserRowStatus nmhSetFsVpnRaUserRowStatus;
    tNpwnmhTestv2FsVpnRaUserSecret nmhTestv2FsVpnRaUserSecret;
    tNpwnmhTestv2FsVpnRaUserRowStatus nmhTestv2FsVpnRaUserRowStatus;
    tNpwnmhDepv2FsVpnRaUsersTable nmhDepv2FsVpnRaUsersTable;
    tNpwnmhValidateIndexInstanceFsVpnRaAddressPoolTable nmhValidateIndexInstanceFsVpnRaAddressPoolTable;
    tNpwnmhGetFirstIndexFsVpnRaAddressPoolTable nmhGetFirstIndexFsVpnRaAddressPoolTable;
    tNpwnmhGetNextIndexFsVpnRaAddressPoolTable nmhGetNextIndexFsVpnRaAddressPoolTable;
    tNpwnmhGetFsVpnRaAddressPoolAddrType nmhGetFsVpnRaAddressPoolAddrType;
    tNpwnmhGetFsVpnRaAddressPoolStart nmhGetFsVpnRaAddressPoolStart;
    tNpwnmhGetFsVpnRaAddressPoolEnd nmhGetFsVpnRaAddressPoolEnd;
    tNpwnmhGetFsVpnRaAddressPoolPrefixLen nmhGetFsVpnRaAddressPoolPrefixLen;
    tNpwnmhGetFsVpnRaAddressPoolRowStatus nmhGetFsVpnRaAddressPoolRowStatus;
    tNpwnmhSetFsVpnRaAddressPoolAddrType nmhSetFsVpnRaAddressPoolAddrType;
    tNpwnmhSetFsVpnRaAddressPoolStart nmhSetFsVpnRaAddressPoolStart;
    tNpwnmhSetFsVpnRaAddressPoolEnd nmhSetFsVpnRaAddressPoolEnd;
    tNpwnmhSetFsVpnRaAddressPoolPrefixLen nmhSetFsVpnRaAddressPoolPrefixLen;
    tNpwnmhSetFsVpnRaAddressPoolRowStatus nmhSetFsVpnRaAddressPoolRowStatus;
    tNpwnmhTestv2FsVpnRaAddressPoolAddrType nmhTestv2FsVpnRaAddressPoolAddrType;
    tNpwnmhTestv2FsVpnRaAddressPoolStart nmhTestv2FsVpnRaAddressPoolStart;
    tNpwnmhTestv2FsVpnRaAddressPoolEnd nmhTestv2FsVpnRaAddressPoolEnd;
    tNpwnmhTestv2FsVpnRaAddressPoolPrefixLen nmhTestv2FsVpnRaAddressPoolPrefixLen;
    tNpwnmhTestv2FsVpnRaAddressPoolRowStatus nmhTestv2FsVpnRaAddressPoolRowStatus;
    tNpwnmhDepv2FsVpnRaAddressPoolTable nmhDepv2FsVpnRaAddressPoolTable;
    tNpwnmhValidateIndexInstanceFsVpnRemoteIdTable nmhValidateIndexInstanceFsVpnRemoteIdTable;
    tNpwnmhGetFirstIndexFsVpnRemoteIdTable nmhGetFirstIndexFsVpnRemoteIdTable;
    tNpwnmhGetNextIndexFsVpnRemoteIdTable nmhGetNextIndexFsVpnRemoteIdTable;
    tNpwnmhGetFsVpnRemoteIdKey nmhGetFsVpnRemoteIdKey;
    tNpwnmhGetFsVpnRemoteIdStatus nmhGetFsVpnRemoteIdStatus;
    tNpwnmhSetFsVpnRemoteIdKey nmhSetFsVpnRemoteIdKey;
    tNpwnmhSetFsVpnRemoteIdStatus nmhSetFsVpnRemoteIdStatus;
    tNpwnmhTestv2FsVpnRemoteIdKey nmhTestv2FsVpnRemoteIdKey;
    tNpwnmhTestv2FsVpnRemoteIdStatus nmhTestv2FsVpnRemoteIdStatus;
    tNpwnmhDepv2FsVpnRemoteIdTable nmhDepv2FsVpnRemoteIdTable;
    tNpwnmhGetFsVpnRemoteIdAuthType nmhGetFsVpnRemoteIdAuthType;
    tNpwnmhSetFsVpnRemoteIdAuthType nmhSetFsVpnRemoteIdAuthType;
    tNpwnmhTestv2FsVpnRemoteIdAuthType nmhTestv2FsVpnRemoteIdAuthType;
    tNpwnmhValidateIndexInstanceFsVpnCertInfoTable nmhValidateIndexInstanceFsVpnCertInfoTable;
    tNpwnmhGetFirstIndexFsVpnCertInfoTable nmhGetFirstIndexFsVpnCertInfoTable;
    tNpwnmhGetNextIndexFsVpnCertInfoTable nmhGetNextIndexFsVpnCertInfoTable;
    tNpwnmhGetFsVpnCertKeyType nmhGetFsVpnCertKeyType;
    tNpwnmhGetFsVpnCertKeyFileName nmhGetFsVpnCertKeyFileName;
    tNpwnmhGetFsVpnCertFileName nmhGetFsVpnCertFileName;
    tNpwnmhGetFsVpnCertEncodeType nmhGetFsVpnCertEncodeType;
    tNpwnmhGetFsVpnCertStatus nmhGetFsVpnCertStatus;
    tNpwnmhSetFsVpnCertKeyType nmhSetFsVpnCertKeyType;
    tNpwnmhSetFsVpnCertKeyFileName nmhSetFsVpnCertKeyFileName;
    tNpwnmhSetFsVpnCertFileName nmhSetFsVpnCertFileName;
    tNpwnmhSetFsVpnCertEncodeType nmhSetFsVpnCertEncodeType;
    tNpwnmhSetFsVpnCertStatus nmhSetFsVpnCertStatus;
    tNpwnmhTestv2FsVpnCertKeyType nmhTestv2FsVpnCertKeyType;
    tNpwnmhTestv2FsVpnCertKeyFileName nmhTestv2FsVpnCertKeyFileName;
    tNpwnmhTestv2FsVpnCertFileName nmhTestv2FsVpnCertFileName;
    tNpwnmhTestv2FsVpnCertEncodeType nmhTestv2FsVpnCertEncodeType;
    tNpwnmhTestv2FsVpnCertStatus nmhTestv2FsVpnCertStatus;
    tNpwnmhDepv2FsVpnCertInfoTable nmhDepv2FsVpnCertInfoTable;
    tNpwnmhValidateIndexInstanceFsVpnCaCertInfoTable nmhValidateIndexInstanceFsVpnCaCertInfoTable;
    tNpwnmhGetFirstIndexFsVpnCaCertInfoTable nmhGetFirstIndexFsVpnCaCertInfoTable;
    tNpwnmhGetNextIndexFsVpnCaCertInfoTable nmhGetNextIndexFsVpnCaCertInfoTable;
    tNpwnmhGetFsVpnCaCertFileName nmhGetFsVpnCaCertFileName;
    tNpwnmhGetFsVpnCaCertEncodeType nmhGetFsVpnCaCertEncodeType;
    tNpwnmhGetFsVpnCaCertStatus nmhGetFsVpnCaCertStatus;
    tNpwnmhSetFsVpnCaCertFileName nmhSetFsVpnCaCertFileName;
    tNpwnmhSetFsVpnCaCertEncodeType nmhSetFsVpnCaCertEncodeType;
    tNpwnmhSetFsVpnCaCertStatus nmhSetFsVpnCaCertStatus;
    tNpwnmhTestv2FsVpnCaCertFileName nmhTestv2FsVpnCaCertFileName;
    tNpwnmhTestv2FsVpnCaCertEncodeType nmhTestv2FsVpnCaCertEncodeType;
    tNpwnmhTestv2FsVpnCaCertStatus nmhTestv2FsVpnCaCertStatus;
    tNpwnmhDepv2FsVpnCaCertInfoTable nmhDepv2FsVpnCaCertInfoTable;
    tNpwnmhGetSecv4BypassCapability Secv4GetBypassCapability;
    tNpwnmhSetSecv4BypassCapability Secv4SetBypassCapability;
    tNpwnmhGetSecv6BypassCapability Secv6GetBypassCapability;
    tNpwnmhSetSecv6BypassCapability Secv6SetBypassCapability;
 tNpwnmhGetFsVpnDummyPktGen nmhGetFsVpnDummyPktGen;
 tNpwnmhSetFsVpnDummyPktGen nmhSetFsVpnDummyPktGen;
 tNpwnmhTestv2FsVpnDummyPktGen nmhTestv2FsVpnDummyPktGen;
 tNpwnmhGetFsVpnDummyPktParam nmhGetFsVpnDummyPktParam;
 tNpwnmhSetFsVpnDummyPktParam nmhSetFsVpnDummyPktParam;
 tNpwnmhTestv2FsVpnDummyPktParam nmhTestv2FsVpnDummyPktParam;
 tNpwnmhGetFsIpsecTraceOption nmhGetFsIpsecTraceOption;
 tNpwnmhSetFsIpsecTraceOption nmhSetFsIpsecTraceOption;
 tNpwnmhTestv2FsIpsecTraceOption nmhTestv2FsIpsecTraceOption;
    };
