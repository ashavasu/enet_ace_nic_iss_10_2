/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vpnwincs.h,v 1.3 
 *
 * Description: File containing all the header file used by T1E1 Module.
 *
 *******************************************************************/
#ifndef _VPNWINCS_H_
#define _VPNWINCS_H_

#include "lr.h"
#include "vpninc.h"
#include "vpnwdefs.h"
#include "vpnwtdfs.h"
#include "npapi.h"
#include "cfa.h"
#include "secmod.h"
#ifdef SECURITY_KERNEL_WANTED
#include "arsec.h"
#include "osix.h"
#include "secgen.h"
#include "seckgen.h"
#endif
int VpnNmhIoctl (unsigned long p);
#endif /*_VPNWINCS_H_ */

