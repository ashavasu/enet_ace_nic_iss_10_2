/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id:vpntrc.h 
 *
 * Description: File containing macros for functions related to trace
 *
 *******************************************************************/
#ifndef __VPNTRC_H__
#define __VPNTRC_H__ 1

/* Trace and debug levels supported */
/* Trace messages should log errors and events */
/* debug should have protocol operation and packet trace */

/* Module names */
#define   VPN_MOD_NAME           ((const char *)"VPN")

#define   VPN_TRC_FLAG           gu4VpnTraceFlag

#define VPN_TRC(TraceType, Str)  \
        UtlTrcLog(VPN_TRC_FLAG, TraceType, VPN_MOD_NAME, (const char *)Str)

#define VPN_TRC_ARG1(TraceType, Str, Arg1)  \
        UtlTrcLog(VPN_TRC_FLAG, TraceType, VPN_MOD_NAME, (const char *)Str,Arg1)

#define VPN_TRC_ARG2(TraceType, Str, Arg1, Arg2)  \
        UtlTrcLog(VPN_TRC_FLAG, TraceType, VPN_MOD_NAME, (const char *)Str,Arg1,Arg2)

#define VPN_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3) \
        UtlTrcLog(VPN_TRC_FLAG, TraceType, VPN_MOD_NAME, \
        (const char *)Str, Arg1, Arg2, Arg3)

#define VPN_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3, Arg4) \
        UtlTrcLog(VPN_TRC_FLAG, TraceType, VPN_MOD_NAME, \
        (const char *)Str, Arg1, Arg2, Arg3, Arg4)

#define VPN_TRC_ARG5(TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5) \
        UtlTrcLog(VPN_TRC_FLAG, TraceType, VPN_MOD_NAME, \
        (const char *)Str, Arg1, Arg2, Arg3, Arg4, Arg5)

#define VPN_TRC_ARG6(TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6) \
        UtlTrcLog(VPN_TRC_FLAG, TraceType, VPN_MOD_NAME, \
        (const char *)Str, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)

#endif /* __VPNTRC_H __ */
