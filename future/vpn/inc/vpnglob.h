/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vpnglob.h,v 1.3
 *
 * Description: File containing all global varible declaration and definition
 *
 *******************************************************************/

#if(defined( _VPN_MAIN_C__) || defined( _VPN_KMAIN_C__))

tTMO_SLL            gVpnRaUserList;
tTMO_SLL            gVpnRaAddressPoolList;
UINT4               gu4VPNMaxTunnels;
UINT4               gu4VpnRaServer;
tTMO_SLL            gVpnRemoteIdList;
tTMO_SLL            gVpnCertInfoList;
tTMO_SLL            gVpnCaCertInfoList;
tOsixSemId          gVpnDsSemId;
UINT4               gu4VpnClearedLogBuff;
tOsixQId            gVpnQId;
UINT4               gu4VpnTraceFlag;
tTMO_SLL            VpnPolicyList;
UINT4               gu4RaRefCount;
UINT4               gu4VpnRestoredConfig;
#else

extern tTMO_SLL            gVpnRaUserList;
extern tTMO_SLL            gVpnRaAddressPoolList;
extern UINT4               gu4VPNMaxTunnels;
extern UINT4               gu4VpnRaServer;
extern tTMO_SLL            gVpnRemoteIdList;
extern tTMO_SLL            gVpnCertInfoList;
extern tTMO_SLL            gVpnCaCertInfoList;
extern tOsixSemId          gVpnDsSemId;
extern UINT4               gu4VpnClearedLogBuff;
extern UINT4               gu4VpnTraceFlag;
extern tTMO_SLL            VpnPolicyList;
extern UINT4               gu4RaRefCount;
extern UINT4               gu4VpnRestoredConfig;
#endif
