/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: vpninc.h,v 1.10 2014/11/07 12:14:48 siva Exp $
 *
 * Description: File containing all the header file  
 *
 *******************************************************************/
#ifndef _VPNINC_H_
#define _VPNINC_H_

/* Common among T1E1 Modules */
#include "lr.h"
#include "ip.h"
#include "vpn.h"
#include "sec.h"
#include "fsike.h"
#include "fsvpndefn.h"
#include "fsvpnproto.h"
#include "fsvpnextn.h"
#include "vpnglob.h"
#include "fsvpnlw.h"
#include "fsvpnwr.h"
#include "cli.h"
#include "secmod.h"
#include "arsec.h"
#include "fips.h"
#include "vpntrc.h"
#include "vpnsz.h"
#include "vpnbufif.h"
#ifdef NPAPI_WANTED
#include "secnp.h"
#endif
#endif /*_VPNINC_H_ */



/*Macros for Hrd coded values in VPN Module*/
#define VPN_INVALID     -1
#define VPN_ZERO         0
#define VPN_ONE          1
#define VPN_TWO          2
#define VPN_THREE        3
#define VPN_FOUR         4
#define VPN_TEN          10
#define VPN_VAL_11       11

#define VPN_ERROR           0
#define VPN_TRUE            1
#define VPN_FALSE           0
#define VPN_BIT_MASK_ffff       0xffff


#define VPN_MASK_0x0000ffff     0x0000ffff
#define VPN_MASK_0xffffffff     0xffffffff
#define VPN_MASK_0xff000000     0xff000000
#define VPN_MASK_0x7f000000     0x7f000000
#define VPN_MASK_0xf0000000     0xf0000000
#define VPN_MASK_0xe0000000     0xe0000000
#define VPN_MASK_0xf8000000     0xf8000000
#define VPN_MASK_0x00ffffff     0x00ffffff
#define VPN_MASK_0x80000000     0x80000000
#define VPN_MASK_0xc0000000     0xc0000000
#define VPN_MASK_0x000000ff     0x000000ff
#define VPN_MASK_0xF            0xF
#define VPN_OFFSET_0xFF         0xFF

#define VPN_DES_KEY_MAX     3

#define VPN_INDEX_0         0
#define VPN_INDEX_1         1
#define VPN_INDEX_2         2
#define VPN_INDEX_3         3
#define VPN_INDEX_4         4
#define VPN_INDEX_5         5
#define VPN_INDEX_6         6
#define VPN_INDEX_7         7
#define VPN_INDEX_8         8
#define VPN_INDEX_9         9
#define VPN_ID_MAX_LEN      10
#define VPN_ARR_MAX         11
#define VPN_MAX_ROW_LEN     16
#define VPN_MAX_SIZE        17
#define VPN_BUFFER_SIZE     20
#define VPN_MAX_LEN         150

#define VPN_PREFIX_LENGTH_8     8
#define VPN_PREFIX_LENGTH_16    16
#define VPN_PREFIX_LENGTH_24    24

