/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsvpndb.h,v 1.9 2014/07/03 10:38:47 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSVPNPDB_H
#define _FSVPNPDB_H

UINT1 FsVpnTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsVpnRaUsersTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsVpnRaAddressPoolTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsVpnRemoteIdTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsVpnCertInfoTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsVpnCaCertInfoTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 fsvpnp [] ={1,3,6,1,4,1,2076,143};
tSNMP_OID_TYPE fsvpnpOID = {8, fsvpnp};


UINT4 FsVpnGlobalStatus [ ] ={1,3,6,1,4,1,2076,143,2,1};
UINT4 FsVpnMaxTunnels [ ] ={1,3,6,1,4,1,2076,143,2,2};
UINT4 FsVpnIpPktsIn [ ] ={1,3,6,1,4,1,2076,143,2,3};
UINT4 FsVpnIpPktsOut [ ] ={1,3,6,1,4,1,2076,143,2,4};
UINT4 FsVpnPktsSecured [ ] ={1,3,6,1,4,1,2076,143,2,5};
UINT4 FsVpnPktsDropped [ ] ={1,3,6,1,4,1,2076,143,2,6};
UINT4 FsVpnIkeSAsActive [ ] ={1,3,6,1,4,1,2076,143,2,7};
UINT4 FsVpnIkeNegotiations [ ] ={1,3,6,1,4,1,2076,143,2,8};
UINT4 FsVpnIkeRekeys [ ] ={1,3,6,1,4,1,2076,143,2,9};
UINT4 FsVpnIkeNegoFailed [ ] ={1,3,6,1,4,1,2076,143,2,10};
UINT4 FsVpnIPSecSAsActive [ ] ={1,3,6,1,4,1,2076,143,2,11};
UINT4 FsVpnIPSecNegotiations [ ] ={1,3,6,1,4,1,2076,143,2,12};
UINT4 FsVpnIPSecNegoFailed [ ] ={1,3,6,1,4,1,2076,143,2,13};
UINT4 FsVpnTotalRekeys [ ] ={1,3,6,1,4,1,2076,143,2,14};
UINT4 FsVpnRaServer [ ] ={1,3,6,1,4,1,2076,143,2,15};
UINT4 FsVpnDummyPktGen [ ] ={1,3,6,1,4,1,2076,143,2,16};
UINT4 FsVpnDummyPktParam [ ] ={1,3,6,1,4,1,2076,143,2,17};
UINT4 FsIkeTraceOption [ ] ={1,3,6,1,4,1,2076,143,2,18};
UINT4 FsIpsecTraceOption [ ] ={1,3,6,1,4,1,2076,143,2,19};
UINT4 FsVpnPolicyName [ ] ={1,3,6,1,4,1,2076,143,1,1,1,1};
UINT4 FsVpnPolicyType [ ] ={1,3,6,1,4,1,2076,143,1,1,1,2};
UINT4 FsVpnPolicyPriority [ ] ={1,3,6,1,4,1,2076,143,1,1,1,3};
UINT4 FsVpnTunTermAddrType [ ] ={1,3,6,1,4,1,2076,143,1,1,1,4};
UINT4 FsVpnLocalTunTermAddr [ ] ={1,3,6,1,4,1,2076,143,1,1,1,5};
UINT4 FsVpnRemoteTunTermAddr [ ] ={1,3,6,1,4,1,2076,143,1,1,1,6};
UINT4 FsVpnProtectNetworkType [ ] ={1,3,6,1,4,1,2076,143,1,1,1,7};
UINT4 FsVpnLocalProtectNetwork [ ] ={1,3,6,1,4,1,2076,143,1,1,1,8};
UINT4 FsVpnLocalProtectNetworkPrefixLen [ ] ={1,3,6,1,4,1,2076,143,1,1,1,9};
UINT4 FsVpnRemoteProtectNetwork [ ] ={1,3,6,1,4,1,2076,143,1,1,1,10};
UINT4 FsVpnRemoteProtectNetworkPrefixLen [ ] ={1,3,6,1,4,1,2076,143,1,1,1,11};
UINT4 FsVpnIkeSrcPortRange [ ] ={1,3,6,1,4,1,2076,143,1,1,1,12};
UINT4 FsVpnIkeDstPortRange [ ] ={1,3,6,1,4,1,2076,143,1,1,1,13};
UINT4 FsVpnSecurityProtocol [ ] ={1,3,6,1,4,1,2076,143,1,1,1,14};
UINT4 FsVpnInboundSpi [ ] ={1,3,6,1,4,1,2076,143,1,1,1,15};
UINT4 FsVpnOutboundSpi [ ] ={1,3,6,1,4,1,2076,143,1,1,1,16};
UINT4 FsVpnMode [ ] ={1,3,6,1,4,1,2076,143,1,1,1,17};
UINT4 FsVpnAuthAlgo [ ] ={1,3,6,1,4,1,2076,143,1,1,1,18};
UINT4 FsVpnAhKey [ ] ={1,3,6,1,4,1,2076,143,1,1,1,19};
UINT4 FsVpnEncrAlgo [ ] ={1,3,6,1,4,1,2076,143,1,1,1,20};
UINT4 FsVpnEspKey [ ] ={1,3,6,1,4,1,2076,143,1,1,1,21};
UINT4 FsVpnAntiReplay [ ] ={1,3,6,1,4,1,2076,143,1,1,1,22};
UINT4 FsVpnPolicyFlag [ ] ={1,3,6,1,4,1,2076,143,1,1,1,23};
UINT4 FsVpnProtocol [ ] ={1,3,6,1,4,1,2076,143,1,1,1,24};
UINT4 FsVpnPolicyIntfIndex [ ] ={1,3,6,1,4,1,2076,143,1,1,1,25};
UINT4 FsVpnIkePhase1HashAlgo [ ] ={1,3,6,1,4,1,2076,143,1,1,1,26};
UINT4 FsVpnIkePhase1EncryptionAlgo [ ] ={1,3,6,1,4,1,2076,143,1,1,1,27};
UINT4 FsVpnIkePhase1DHGroup [ ] ={1,3,6,1,4,1,2076,143,1,1,1,28};
UINT4 FsVpnIkePhase1LocalIdType [ ] ={1,3,6,1,4,1,2076,143,1,1,1,29};
UINT4 FsVpnIkePhase1LocalIdValue [ ] ={1,3,6,1,4,1,2076,143,1,1,1,30};
UINT4 FsVpnIkePhase1PeerIdType [ ] ={1,3,6,1,4,1,2076,143,1,1,1,31};
UINT4 FsVpnIkePhase1PeerIdValue [ ] ={1,3,6,1,4,1,2076,143,1,1,1,32};
UINT4 FsVpnIkePhase1LifeTimeType [ ] ={1,3,6,1,4,1,2076,143,1,1,1,33};
UINT4 FsVpnIkePhase1LifeTime [ ] ={1,3,6,1,4,1,2076,143,1,1,1,34};
UINT4 FsVpnIkePhase1Mode [ ] ={1,3,6,1,4,1,2076,143,1,1,1,35};
UINT4 FsVpnIkePhase2AuthAlgo [ ] ={1,3,6,1,4,1,2076,143,1,1,1,36};
UINT4 FsVpnIkePhase2EspEncryptionAlgo [ ] ={1,3,6,1,4,1,2076,143,1,1,1,37};
UINT4 FsVpnIkePhase2LifeTimeType [ ] ={1,3,6,1,4,1,2076,143,1,1,1,38};
UINT4 FsVpnIkePhase2LifeTime [ ] ={1,3,6,1,4,1,2076,143,1,1,1,39};
UINT4 FsVpnIkePhase2DHGroup [ ] ={1,3,6,1,4,1,2076,143,1,1,1,40};
UINT4 FsVpnIkeVersion [ ] ={1,3,6,1,4,1,2076,143,1,1,1,41};
UINT4 FsVpnCertAlgoType [ ] ={1,3,6,1,4,1,2076,143,1,1,1,42};
UINT4 FsVpnPolicyRowStatus [ ] ={1,3,6,1,4,1,2076,143,1,1,1,43};
UINT4 FsVpnRaUserName [ ] ={1,3,6,1,4,1,2076,143,1,2,1,1};
UINT4 FsVpnRaUserSecret [ ] ={1,3,6,1,4,1,2076,143,1,2,1,2};
UINT4 FsVpnRaUserRowStatus [ ] ={1,3,6,1,4,1,2076,143,1,2,1,3};
UINT4 FsVpnRaAddressPoolName [ ] ={1,3,6,1,4,1,2076,143,1,3,1,1};
UINT4 FsVpnRaAddressPoolAddrType [ ] ={1,3,6,1,4,1,2076,143,1,3,1,2};
UINT4 FsVpnRaAddressPoolStart [ ] ={1,3,6,1,4,1,2076,143,1,3,1,3};
UINT4 FsVpnRaAddressPoolEnd [ ] ={1,3,6,1,4,1,2076,143,1,3,1,4};
UINT4 FsVpnRaAddressPoolPrefixLen [ ] ={1,3,6,1,4,1,2076,143,1,3,1,5};
UINT4 FsVpnRaAddressPoolRowStatus [ ] ={1,3,6,1,4,1,2076,143,1,3,1,6};
UINT4 FsVpnRemoteIdType [ ] ={1,3,6,1,4,1,2076,143,1,4,1,1};
UINT4 FsVpnRemoteIdValue [ ] ={1,3,6,1,4,1,2076,143,1,4,1,2};
UINT4 FsVpnRemoteIdKey [ ] ={1,3,6,1,4,1,2076,143,1,4,1,3};
UINT4 FsVpnRemoteIdAuthType [ ] ={1,3,6,1,4,1,2076,143,1,4,1,4};
UINT4 FsVpnRemoteIdStatus [ ] ={1,3,6,1,4,1,2076,143,1,4,1,5};
UINT4 FsVpnCertKeyString [ ] ={1,3,6,1,4,1,2076,143,1,5,1,1};
UINT4 FsVpnCertKeyType [ ] ={1,3,6,1,4,1,2076,143,1,5,1,2};
UINT4 FsVpnCertKeyFileName [ ] ={1,3,6,1,4,1,2076,143,1,5,1,3};
UINT4 FsVpnCertFileName [ ] ={1,3,6,1,4,1,2076,143,1,5,1,4};
UINT4 FsVpnCertEncodeType [ ] ={1,3,6,1,4,1,2076,143,1,5,1,5};
UINT4 FsVpnCertStatus [ ] ={1,3,6,1,4,1,2076,143,1,5,1,6};
UINT4 FsVpnCaCertKeyString [ ] ={1,3,6,1,4,1,2076,143,1,6,1,1};
UINT4 FsVpnCaCertFileName [ ] ={1,3,6,1,4,1,2076,143,1,6,1,2};
UINT4 FsVpnCaCertEncodeType [ ] ={1,3,6,1,4,1,2076,143,1,6,1,3};
UINT4 FsVpnCaCertStatus [ ] ={1,3,6,1,4,1,2076,143,1,6,1,4};




tMbDbEntry fsvpnpMibEntry[]= {

{{12,FsVpnPolicyName}, GetNextIndexFsVpnTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnPolicyType}, GetNextIndexFsVpnTable, FsVpnPolicyTypeGet, FsVpnPolicyTypeSet, FsVpnPolicyTypeTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnPolicyPriority}, GetNextIndexFsVpnTable, FsVpnPolicyPriorityGet, FsVpnPolicyPrioritySet, FsVpnPolicyPriorityTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnTunTermAddrType}, GetNextIndexFsVpnTable, FsVpnTunTermAddrTypeGet, FsVpnTunTermAddrTypeSet, FsVpnTunTermAddrTypeTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnLocalTunTermAddr}, GetNextIndexFsVpnTable, FsVpnLocalTunTermAddrGet, FsVpnLocalTunTermAddrSet, FsVpnLocalTunTermAddrTest, FsVpnTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnRemoteTunTermAddr}, GetNextIndexFsVpnTable, FsVpnRemoteTunTermAddrGet, FsVpnRemoteTunTermAddrSet, FsVpnRemoteTunTermAddrTest, FsVpnTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnProtectNetworkType}, GetNextIndexFsVpnTable, FsVpnProtectNetworkTypeGet, FsVpnProtectNetworkTypeSet, FsVpnProtectNetworkTypeTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnLocalProtectNetwork}, GetNextIndexFsVpnTable, FsVpnLocalProtectNetworkGet, FsVpnLocalProtectNetworkSet, FsVpnLocalProtectNetworkTest, FsVpnTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnLocalProtectNetworkPrefixLen}, GetNextIndexFsVpnTable, FsVpnLocalProtectNetworkPrefixLenGet, FsVpnLocalProtectNetworkPrefixLenSet, FsVpnLocalProtectNetworkPrefixLenTest, FsVpnTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnRemoteProtectNetwork}, GetNextIndexFsVpnTable, FsVpnRemoteProtectNetworkGet, FsVpnRemoteProtectNetworkSet, FsVpnRemoteProtectNetworkTest, FsVpnTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnRemoteProtectNetworkPrefixLen}, GetNextIndexFsVpnTable, FsVpnRemoteProtectNetworkPrefixLenGet, FsVpnRemoteProtectNetworkPrefixLenSet, FsVpnRemoteProtectNetworkPrefixLenTest, FsVpnTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnIkeSrcPortRange}, GetNextIndexFsVpnTable, FsVpnIkeSrcPortRangeGet, FsVpnIkeSrcPortRangeSet, FsVpnIkeSrcPortRangeTest, FsVpnTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnIkeDstPortRange}, GetNextIndexFsVpnTable, FsVpnIkeDstPortRangeGet, FsVpnIkeDstPortRangeSet, FsVpnIkeDstPortRangeTest, FsVpnTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnSecurityProtocol}, GetNextIndexFsVpnTable, FsVpnSecurityProtocolGet, FsVpnSecurityProtocolSet, FsVpnSecurityProtocolTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnInboundSpi}, GetNextIndexFsVpnTable, FsVpnInboundSpiGet, FsVpnInboundSpiSet, FsVpnInboundSpiTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnOutboundSpi}, GetNextIndexFsVpnTable, FsVpnOutboundSpiGet, FsVpnOutboundSpiSet, FsVpnOutboundSpiTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnMode}, GetNextIndexFsVpnTable, FsVpnModeGet, FsVpnModeSet, FsVpnModeTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnAuthAlgo}, GetNextIndexFsVpnTable, FsVpnAuthAlgoGet, FsVpnAuthAlgoSet, FsVpnAuthAlgoTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnAhKey}, GetNextIndexFsVpnTable, FsVpnAhKeyGet, FsVpnAhKeySet, FsVpnAhKeyTest, FsVpnTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnEncrAlgo}, GetNextIndexFsVpnTable, FsVpnEncrAlgoGet, FsVpnEncrAlgoSet, FsVpnEncrAlgoTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnEspKey}, GetNextIndexFsVpnTable, FsVpnEspKeyGet, FsVpnEspKeySet, FsVpnEspKeyTest, FsVpnTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnAntiReplay}, GetNextIndexFsVpnTable, FsVpnAntiReplayGet, FsVpnAntiReplaySet, FsVpnAntiReplayTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, "1"},

{{12,FsVpnPolicyFlag}, GetNextIndexFsVpnTable, FsVpnPolicyFlagGet, FsVpnPolicyFlagSet, FsVpnPolicyFlagTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnProtocol}, GetNextIndexFsVpnTable, FsVpnProtocolGet, FsVpnProtocolSet, FsVpnProtocolTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnPolicyIntfIndex}, GetNextIndexFsVpnTable, FsVpnPolicyIntfIndexGet, FsVpnPolicyIntfIndexSet, FsVpnPolicyIntfIndexTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnIkePhase1HashAlgo}, GetNextIndexFsVpnTable, FsVpnIkePhase1HashAlgoGet, FsVpnIkePhase1HashAlgoSet, FsVpnIkePhase1HashAlgoTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, "2"},

{{12,FsVpnIkePhase1EncryptionAlgo}, GetNextIndexFsVpnTable, FsVpnIkePhase1EncryptionAlgoGet, FsVpnIkePhase1EncryptionAlgoSet, FsVpnIkePhase1EncryptionAlgoTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, "4"},

{{12,FsVpnIkePhase1DHGroup}, GetNextIndexFsVpnTable, FsVpnIkePhase1DHGroupGet, FsVpnIkePhase1DHGroupSet, FsVpnIkePhase1DHGroupTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, "2"},

{{12,FsVpnIkePhase1LocalIdType}, GetNextIndexFsVpnTable, FsVpnIkePhase1LocalIdTypeGet, FsVpnIkePhase1LocalIdTypeSet, FsVpnIkePhase1LocalIdTypeTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnIkePhase1LocalIdValue}, GetNextIndexFsVpnTable, FsVpnIkePhase1LocalIdValueGet, FsVpnIkePhase1LocalIdValueSet, FsVpnIkePhase1LocalIdValueTest, FsVpnTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnIkePhase1PeerIdType}, GetNextIndexFsVpnTable, FsVpnIkePhase1PeerIdTypeGet, FsVpnIkePhase1PeerIdTypeSet, FsVpnIkePhase1PeerIdTypeTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnIkePhase1PeerIdValue}, GetNextIndexFsVpnTable, FsVpnIkePhase1PeerIdValueGet, FsVpnIkePhase1PeerIdValueSet, FsVpnIkePhase1PeerIdValueTest, FsVpnTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnIkePhase1LifeTimeType}, GetNextIndexFsVpnTable, FsVpnIkePhase1LifeTimeTypeGet, FsVpnIkePhase1LifeTimeTypeSet, FsVpnIkePhase1LifeTimeTypeTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, "1"},

{{12,FsVpnIkePhase1LifeTime}, GetNextIndexFsVpnTable, FsVpnIkePhase1LifeTimeGet, FsVpnIkePhase1LifeTimeSet, FsVpnIkePhase1LifeTimeTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, "2400"},

{{12,FsVpnIkePhase1Mode}, GetNextIndexFsVpnTable, FsVpnIkePhase1ModeGet, FsVpnIkePhase1ModeSet, FsVpnIkePhase1ModeTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnIkePhase2AuthAlgo}, GetNextIndexFsVpnTable, FsVpnIkePhase2AuthAlgoGet, FsVpnIkePhase2AuthAlgoSet, FsVpnIkePhase2AuthAlgoTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnIkePhase2EspEncryptionAlgo}, GetNextIndexFsVpnTable, FsVpnIkePhase2EspEncryptionAlgoGet, FsVpnIkePhase2EspEncryptionAlgoSet, FsVpnIkePhase2EspEncryptionAlgoTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnIkePhase2LifeTimeType}, GetNextIndexFsVpnTable, FsVpnIkePhase2LifeTimeTypeGet, FsVpnIkePhase2LifeTimeTypeSet, FsVpnIkePhase2LifeTimeTypeTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, "1"},

{{12,FsVpnIkePhase2LifeTime}, GetNextIndexFsVpnTable, FsVpnIkePhase2LifeTimeGet, FsVpnIkePhase2LifeTimeSet, FsVpnIkePhase2LifeTimeTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, "800"},

{{12,FsVpnIkePhase2DHGroup}, GetNextIndexFsVpnTable, FsVpnIkePhase2DHGroupGet, FsVpnIkePhase2DHGroupSet, FsVpnIkePhase2DHGroupTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnIkeVersion}, GetNextIndexFsVpnTable, FsVpnIkeVersionGet, FsVpnIkeVersionSet, FsVpnIkeVersionTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnCertAlgoType}, GetNextIndexFsVpnTable, FsVpnCertAlgoTypeGet, FsVpnCertAlgoTypeSet, FsVpnCertAlgoTypeTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnPolicyRowStatus}, GetNextIndexFsVpnTable, FsVpnPolicyRowStatusGet, FsVpnPolicyRowStatusSet, FsVpnPolicyRowStatusTest, FsVpnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnTableINDEX, 1, 0, 1, NULL},

{{12,FsVpnRaUserName}, GetNextIndexFsVpnRaUsersTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsVpnRaUsersTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnRaUserSecret}, GetNextIndexFsVpnRaUsersTable, FsVpnRaUserSecretGet, FsVpnRaUserSecretSet, FsVpnRaUserSecretTest, FsVpnRaUsersTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsVpnRaUsersTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnRaUserRowStatus}, GetNextIndexFsVpnRaUsersTable, FsVpnRaUserRowStatusGet, FsVpnRaUserRowStatusSet, FsVpnRaUserRowStatusTest, FsVpnRaUsersTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnRaUsersTableINDEX, 1, 0, 1, NULL},

{{12,FsVpnRaAddressPoolName}, GetNextIndexFsVpnRaAddressPoolTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsVpnRaAddressPoolTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnRaAddressPoolAddrType}, GetNextIndexFsVpnRaAddressPoolTable, FsVpnRaAddressPoolAddrTypeGet, FsVpnRaAddressPoolAddrTypeSet, FsVpnRaAddressPoolAddrTypeTest, FsVpnRaAddressPoolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnRaAddressPoolTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnRaAddressPoolStart}, GetNextIndexFsVpnRaAddressPoolTable, FsVpnRaAddressPoolStartGet, FsVpnRaAddressPoolStartSet, FsVpnRaAddressPoolStartTest, FsVpnRaAddressPoolTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsVpnRaAddressPoolTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnRaAddressPoolEnd}, GetNextIndexFsVpnRaAddressPoolTable, FsVpnRaAddressPoolEndGet, FsVpnRaAddressPoolEndSet, FsVpnRaAddressPoolEndTest, FsVpnRaAddressPoolTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsVpnRaAddressPoolTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnRaAddressPoolPrefixLen}, GetNextIndexFsVpnRaAddressPoolTable, FsVpnRaAddressPoolPrefixLenGet, FsVpnRaAddressPoolPrefixLenSet, FsVpnRaAddressPoolPrefixLenTest, FsVpnRaAddressPoolTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsVpnRaAddressPoolTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnRaAddressPoolRowStatus}, GetNextIndexFsVpnRaAddressPoolTable, FsVpnRaAddressPoolRowStatusGet, FsVpnRaAddressPoolRowStatusSet, FsVpnRaAddressPoolRowStatusTest, FsVpnRaAddressPoolTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnRaAddressPoolTableINDEX, 1, 0, 1, NULL},

{{12,FsVpnRemoteIdType}, GetNextIndexFsVpnRemoteIdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsVpnRemoteIdTableINDEX, 2, 0, 0, NULL},

{{12,FsVpnRemoteIdValue}, GetNextIndexFsVpnRemoteIdTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsVpnRemoteIdTableINDEX, 2, 0, 0, NULL},

{{12,FsVpnRemoteIdKey}, GetNextIndexFsVpnRemoteIdTable, FsVpnRemoteIdKeyGet, FsVpnRemoteIdKeySet, FsVpnRemoteIdKeyTest, FsVpnRemoteIdTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsVpnRemoteIdTableINDEX, 2, 0, 0, NULL},

{{12,FsVpnRemoteIdAuthType}, GetNextIndexFsVpnRemoteIdTable, FsVpnRemoteIdAuthTypeGet, FsVpnRemoteIdAuthTypeSet, FsVpnRemoteIdAuthTypeTest, FsVpnRemoteIdTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsVpnRemoteIdTableINDEX, 2, 0, 0, NULL},

{{12,FsVpnRemoteIdStatus}, GetNextIndexFsVpnRemoteIdTable, FsVpnRemoteIdStatusGet, FsVpnRemoteIdStatusSet, FsVpnRemoteIdStatusTest, FsVpnRemoteIdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnRemoteIdTableINDEX, 2, 0, 1, NULL},

{{12,FsVpnCertKeyString}, GetNextIndexFsVpnCertInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsVpnCertInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnCertKeyType}, GetNextIndexFsVpnCertInfoTable, FsVpnCertKeyTypeGet, FsVpnCertKeyTypeSet, FsVpnCertKeyTypeTest, FsVpnCertInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnCertInfoTableINDEX, 1, 0, 0, "1"},

{{12,FsVpnCertKeyFileName}, GetNextIndexFsVpnCertInfoTable, FsVpnCertKeyFileNameGet, FsVpnCertKeyFileNameSet, FsVpnCertKeyFileNameTest, FsVpnCertInfoTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsVpnCertInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnCertFileName}, GetNextIndexFsVpnCertInfoTable, FsVpnCertFileNameGet, FsVpnCertFileNameSet, FsVpnCertFileNameTest, FsVpnCertInfoTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsVpnCertInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnCertEncodeType}, GetNextIndexFsVpnCertInfoTable, FsVpnCertEncodeTypeGet, FsVpnCertEncodeTypeSet, FsVpnCertEncodeTypeTest, FsVpnCertInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnCertInfoTableINDEX, 1, 0, 0, "1"},

{{12,FsVpnCertStatus}, GetNextIndexFsVpnCertInfoTable, FsVpnCertStatusGet, FsVpnCertStatusSet, FsVpnCertStatusTest, FsVpnCertInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnCertInfoTableINDEX, 1, 0, 1, NULL},

{{12,FsVpnCaCertKeyString}, GetNextIndexFsVpnCaCertInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsVpnCaCertInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnCaCertFileName}, GetNextIndexFsVpnCaCertInfoTable, FsVpnCaCertFileNameGet, FsVpnCaCertFileNameSet, FsVpnCaCertFileNameTest, FsVpnCaCertInfoTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsVpnCaCertInfoTableINDEX, 1, 0, 0, NULL},

{{12,FsVpnCaCertEncodeType}, GetNextIndexFsVpnCaCertInfoTable, FsVpnCaCertEncodeTypeGet, FsVpnCaCertEncodeTypeSet, FsVpnCaCertEncodeTypeTest, FsVpnCaCertInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnCaCertInfoTableINDEX, 1, 0, 0, "1"},

{{12,FsVpnCaCertStatus}, GetNextIndexFsVpnCaCertInfoTable, FsVpnCaCertStatusGet, FsVpnCaCertStatusSet, FsVpnCaCertStatusTest, FsVpnCaCertInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsVpnCaCertInfoTableINDEX, 1, 0, 1, NULL},

{{10,FsVpnGlobalStatus}, NULL, FsVpnGlobalStatusGet, FsVpnGlobalStatusSet, FsVpnGlobalStatusTest, FsVpnGlobalStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsVpnMaxTunnels}, NULL, FsVpnMaxTunnelsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsVpnIpPktsIn}, NULL, FsVpnIpPktsInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsVpnIpPktsOut}, NULL, FsVpnIpPktsOutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsVpnPktsSecured}, NULL, FsVpnPktsSecuredGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsVpnPktsDropped}, NULL, FsVpnPktsDroppedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsVpnIkeSAsActive}, NULL, FsVpnIkeSAsActiveGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsVpnIkeNegotiations}, NULL, FsVpnIkeNegotiationsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsVpnIkeRekeys}, NULL, FsVpnIkeRekeysGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsVpnIkeNegoFailed}, NULL, FsVpnIkeNegoFailedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsVpnIPSecSAsActive}, NULL, FsVpnIPSecSAsActiveGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsVpnIPSecNegotiations}, NULL, FsVpnIPSecNegotiationsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsVpnIPSecNegoFailed}, NULL, FsVpnIPSecNegoFailedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsVpnTotalRekeys}, NULL, FsVpnTotalRekeysGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FsVpnRaServer}, NULL, FsVpnRaServerGet, FsVpnRaServerSet, FsVpnRaServerTest, FsVpnRaServerDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FsVpnDummyPktGen}, NULL, FsVpnDummyPktGenGet, FsVpnDummyPktGenSet, FsVpnDummyPktGenTest, FsVpnDummyPktGenDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsVpnDummyPktParam}, NULL, FsVpnDummyPktParamGet, FsVpnDummyPktParamSet, FsVpnDummyPktParamTest, FsVpnDummyPktParamDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "25"},

{{10,FsIkeTraceOption}, NULL, FsIkeTraceOptionGet, FsIkeTraceOptionSet, FsIkeTraceOptionTest, FsIkeTraceOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FsIpsecTraceOption}, NULL, FsIpsecTraceOptionGet, FsIpsecTraceOptionSet, FsIpsecTraceOptionTest, FsIpsecTraceOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},
};
tMibData fsvpnpEntry = { 86, fsvpnpMibEntry };

#endif /* _FSVPNPDB_H */

