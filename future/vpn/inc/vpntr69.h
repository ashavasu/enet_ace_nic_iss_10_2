/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id:vpntr69.h 
 *
 * Description: File containing all the macros,structure defenitions 
 *              and function prototype by T1E1 Module.
 *
 *******************************************************************/

#define TR_FALSE   0
#define TR_TRUE    1

#define TR_LIFE_TIME_SECS 1
#define TR_LIFE_TIME_MINS 2
#define TR_LIFE_TIME_HOURS 3
#define TR_LIFE_TIME_DAYS 4

#define LIFE_TIME_MINS 3
#define LIFE_TIME_HOURS 4
#define LIFE_TIME_DAYS 5

#define TR69_VPN_AH 51
#define TR69_NOT_CONFIG 0

#define VPN_LOCAL_PROTECT_NETWORK 16
#define VPN_LOCAL_TUN_TERM_ADDR 16
#define VPN_REMOTE_PROTECT_NETWORK 16
#define VPN_POLICY_NAME 64
#define VPN_LOCAL_PROTECT_SUBNET_MASK 16
#define VPN_REMOTE_TUN_TERM_ADDR 16
#define VPN_ESP_KEY 512
#define VPN_AH_KEY 68
#define VPN_REMOTE_PROTECT_SUBNET_MASK 16
#define VPN_IKE_PHASE1_PEER_ID_VALUE 64
#define VPN_IKE_PHASE1_PEER_ID_TYPE 12
#define VPN_IKE_RA_INFO_PROTECTED_NET_BUNDLE 16
#define VPN_IKE_PHASE1_LOCAL_ID_TYPE 12
#define VPN_IKE_PHASE1_LOCAL_ID_VALUE 64
/* ValMasks for InternetGatewayDevice.WANDevice.0.WANConnectionDevice.0.WANIPConnection.0.VPN.0 */

#define FS_VPN_POLICY_NAME_MASK                   (1)
#define FS_VPN_POLICY_TYPE_MASK                   (1 << 1)
#define FS_VPN_MODE_MASK                          (1 << 2)
#define FS_VPN_POLICY_INTF_INDEX_MASK             (1 << 3)
#define FS_VPN_SECURITY_PROTOCOL_MASK             (1 << 4)
#define FS_VPN_LOCAL_TUN_TERM_ADDR_MASK           (1 << 5)
#define FS_VPN_REMOTE_TUN_TERM_ADDR_MASK          (1 << 6)
#define FS_VPN_LOCAL_PROTECT_NETWORK_MASK         (1 << 7)
#define FS_VPN_LOCAL_PROTECT_SUBNET_MASK_MASK     (1 << 8)
#define FS_VPN_REMOTE_PROTECT_NETWORK_MASK        (1 << 9)
#define FS_VPN_REMOTE_PROTECT_SUBNET_MASK_MASK    (1 << 10)
#define FS_VPN_POLICY_FLAG_MASK                   (1 << 11)
#define FS_VPN_PROTOCOL_MASK                      (1 << 12)
#define FS_VPN_POLICY_ROW_STATUS_MASK             (1 << 13)
#define FS_VPN_POLICY_PRIORITY_MASK               (1 << 14)

#define FS_VPN_AUTH_ALGO_MASK                     (1 << 15)
#define FS_VPN_AH_KEY_MASK                        (1 << 16)
#define FS_VPN_ENCR_ALGO_MASK                     (1 << 17)
#define FS_VPN_ESP_KEY_MASK                       (1 << 18)
#define FS_VPN_OUTBOUND_SPI_MASK                  (1 << 19)
#define FS_VPN_INBOUND_SPI_MASK                   (1 << 20)
#define FS_VPN_ANTI_REPLAY_MASK                   (1 << 21)

#define FS_VPN_IKE_PHASE1_LOCAL_ID_TYPE_MASK      (1 << 15)
#define FS_VPN_IKE_PHASE1_PEER_ID_TYPE_MASK       (1 << 16)
#define FS_VPN_IKE_PHASE1_PEER_ID_VALUE_MASK      (1 << 17)
#define FS_VPN_IKE_PHASE1_MODE_MASK               (1 << 18)
#define FS_VPN_IKE_PHASE1_HASH_ALGO_MASK          (1 << 19)
#define FS_VPN_IKE_PHASE1_ENCRYPTION_ALGO_MASK    (1 << 20)
#define FS_VPN_IKE_PHASE1_DH_GROUP_MASK           (1 << 21)
#define FS_VPN_IKE_PHASE1_LIFE_TIME_TYPE_MASK     (1 << 22)
#define FS_VPN_IKE_PHASE1_LIFE_TIME_MASK          (1 << 23)
#define FS_VPN_IKE_PHASE2_AUTH_ALGO_MASK          (1 << 24)
#define FS_VPN_IKE_PHASE2_ESP_ENCRYPTION_ALGO_MASK (1 << 25)
#define FS_VPN_IKE_PHASE2_DH_GROUP_MASK           (1 << 26)
#define FS_VPN_IKE_PHASE2_LIFE_TIME_TYPE_MASK     (1 << 27)
#define FS_VPN_IKE_PHASE2_LIFE_TIME_MASK          (1 << 28)
#define FS_VPN_IKE_PHASE1_LOCAL_ID_VALUE_MASK     (1 << 29)

#define VPN_VAL_MASK ( FS_VPN_POLICY_TYPE_MASK | FS_VPN_MODE_MASK \
                      | FS_VPN_SECURITY_PROTOCOL_MASK \
                      | FS_VPN_REMOTE_TUN_TERM_ADDR_MASK \
                      | FS_VPN_LOCAL_PROTECT_NETWORK_MASK \
                      | FS_VPN_LOCAL_PROTECT_SUBNET_MASK_MASK \
                      | FS_VPN_REMOTE_PROTECT_NETWORK_MASK \
                      | FS_VPN_REMOTE_PROTECT_SUBNET_MASK_MASK \
                      | FS_VPN_POLICY_FLAG_MASK | FS_VPN_PROTOCOL_MASK)

#define VPN_IPSEC_VAL_MASK ( FS_VPN_AUTH_ALGO_MASK | FS_VPN_AH_KEY_MASK \
                             | FS_VPN_ENCR_ALGO_MASK | FS_VPN_ESP_KEY_MASK \
                             | FS_VPN_OUTBOUND_SPI_MASK | FS_VPN_INBOUND_SPI_MASK \
                             | FS_VPN_ANTI_REPLAY_MASK ) 

#define VPN_IKE_VAL_MASK ( FS_VPN_IKE_PHASE1_LOCAL_ID_TYPE_MASK | FS_VPN_IKE_PHASE1_PEER_ID_TYPE_MASK \
                           | FS_VPN_IKE_PHASE1_PEER_ID_VALUE_MASK | FS_VPN_IKE_PHASE1_MODE_MASK \
                           | FS_VPN_IKE_PHASE1_HASH_ALGO_MASK \
                           | FS_VPN_IKE_PHASE1_ENCRYPTION_ALGO_MASK \
                           | FS_VPN_IKE_PHASE1_DH_GROUP_MASK \
                           | FS_VPN_IKE_PHASE1_LIFE_TIME_TYPE_MASK \
                           | FS_VPN_IKE_PHASE1_LIFE_TIME_MASK \
                           | FS_VPN_IKE_PHASE2_AUTH_ALGO_MASK \
                           | FS_VPN_IKE_PHASE2_ESP_ENCRYPTION_ALGO_MASK \
                           | FS_VPN_IKE_PHASE2_DH_GROUP_MASK \
                           | FS_VPN_IKE_PHASE2_LIFE_TIME_TYPE_MASK \
                           | FS_VPN_IKE_PHASE2_LIFE_TIME_MASK \
                           | FS_VPN_IKE_PHASE1_LOCAL_ID_VALUE_MASK )

/* flags for the VPN trace call  */
#define   VPN_TRC_NONE              (0x00000000)
#define   VPN_TRC_ALL               (0xffffffff)

/* InternetGatewayDevice.WANDevice.0.WANConnectionDevice.0.WANIPConnection.0.VPN.0 */
typedef struct {
    tTMO_SLL_NODE Link;
    UINT4 u4TrInstance;
    UINT4 u4GlobalValMask;
    INT4  i4PolicyIntfIndex;
    UINT4 u4ValMask;
    UINT4 u4Visited;

    /* INDICES: */
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1 au1OctetListIdx[MAX_OCTETSTRING_SIZE];


    INT4  i4IkePhase1PeerIdType;
    UINT1 au1IkePhase1PeerIdType [VPN_IKE_PHASE1_PEER_ID_TYPE];
    INT4  i4IkePhase1DHGroup;
    INT4  i4IkePhase1HashAlgo;
    UINT1 au1LocalProtectNetwork [VPN_LOCAL_PROTECT_NETWORK];
    INT4  i4AntiReplay;
    INT4  i4PolicyType;
    INT4  i4IkePhase2LifeTimeType;
    UINT1 au1LocalTunTermAddr [VPN_LOCAL_TUN_TERM_ADDR];
    INT4  i4IkePhase1Mode;
    INT4  i4IkePhase1LifeTimeType;
    INT4  i4PolicyFlag;
    INT4  i4PolicyPriority;
    UINT4 u4PolicyRowStatus;
    INT4  i4EncrAlgo;
    INT4  i4IkePhase1LifeTime;
    UINT1 au1RemoteProtectNetwork [VPN_REMOTE_PROTECT_NETWORK];
    INT4  i4IkePhase2DHGroup;
    UINT1 au1PolicyName [VPN_POLICY_NAME];
    UINT1 au1LocalProtectSubnetMask [VPN_LOCAL_PROTECT_SUBNET_MASK];
    UINT1 au1RemoteTunTermAddr [VPN_REMOTE_TUN_TERM_ADDR];
    INT4  i4IkePhase1EncryptionAlgo;
    INT4  i4SecurityProtocol;
    INT4  i4AuthAlgo;
    UINT1 au1EspKey [VPN_ESP_KEY];
    INT4  i4Mode;
    UINT1 au1AhKey [VPN_AH_KEY];
    UINT1 au1RemoteProtectSubnetMask [VPN_REMOTE_PROTECT_SUBNET_MASK];
    INT4  i4OutboundSpi;
    UINT1 au1IkePhase1PeerIdValue [VPN_IKE_PHASE1_PEER_ID_VALUE];
    INT4  i4InboundSpi;
    INT4  i4IkePhase2LifeTime;
    INT4  i4Protocol;
    INT4  i4IkePhase2AuthAlgo;
    INT4  i4IkePhase2EspEncryptionAlgo;
    UINT1 au1IkeRAInfoProtectedNetBundle [VPN_IKE_RA_INFO_PROTECTED_NET_BUNDLE];
    INT4  i4IpComp;
    UINT1 au1IkePhase1LocalIdType [VPN_IKE_PHASE1_LOCAL_ID_TYPE];
    UINT1 au1IkePhase1LocalIdValue [VPN_IKE_PHASE1_LOCAL_ID_VALUE];

} tVPN;

/* InternetGatewayDevice.RAVPN */
typedef struct {

    tTMO_SLL UserList;
    tTMO_SLL AddressPoolList;

} tRAVPN;

/* InternetGatewayDevice.RAVPN.User.0 */
typedef struct {
    tTMO_SLL_NODE Link;
    UINT4 u4TrInstance;
    UINT4 u4ValMask;
    UINT4 u4Visited;

    /* INDICES: */
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1 au1OctetListIdx[MAX_OCTETSTRING_SIZE];

#define USER_NAME 32
#define USER_SECRET 32
    UINT4 u4RowStatus;
    UINT1 au1Name [USER_NAME];
    UINT1 au1Secret [USER_SECRET];
/* ValMasks for InternetGatewayDevice.RAVPN.User.0 */
#define FS_VPN_RA_USER_ROW_STATUS_MASK (1)
#define FS_VPN_RA_USER_NAME_MASK       (1 << 2)
#define FS_VPN_RA_USER_SECRET_MASK     (1 << 3)

#define USER_VAL_MASK FS_VPN_RA_USER_NAME_MASK | FS_VPN_RA_USER_SECRET_MASK
} tUser;

/* InternetGatewayDevice.RAVPN.AddressPool.0 */
typedef struct {
    tTMO_SLL_NODE Link;
    UINT4 u4TrInstance;
    UINT4 u4ValMask;
    UINT4 u4Visited;

    /* INDICES: */
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1 au1OctetListIdx[MAX_OCTETSTRING_SIZE];

#define ADDRESSPOOL_END_ADDRESS 16
#define ADDRESSPOOL_START_ADDRESS 16
#define ADDRESSPOOL_NAME 32
    UINT1 au1EndAddress [ADDRESSPOOL_END_ADDRESS];
    UINT1 au1StartAddress [ADDRESSPOOL_START_ADDRESS];
    UINT4 u4RowStatus;
    UINT1 au1Name [ADDRESSPOOL_NAME];
/* ValMasks for InternetGatewayDevice.RAVPN.AddressPool.0 */
#define FS_VPN_RA_ADDRESS_POOL_END_MASK        (1)
#define FS_VPN_RA_ADDRESS_POOL_START_MASK      (1 << 2)
#define FS_VPN_RA_ADDRESS_POOL_ROW_STATUS_MASK (1 << 3)
#define FS_VPN_RA_ADDRESS_POOL_NAME_MASK       (1 << 4)

#define ADDRESS_POOL_VAL_MASK ( FS_VPN_RA_ADDRESS_POOL_END_MASK | \
                                FS_VPN_RA_ADDRESS_POOL_START_MASK | \
                                FS_VPN_RA_ADDRESS_POOL_NAME_MASK)
} tAddressPool;


 
/* InternetGatewayDevice.VpnRemoteId.0 */
typedef struct {
    tTMO_SLL_NODE Link;
    UINT4 u4TrInstance;
    UINT4 u4ValMask;
    UINT4 u4Visited;

    /* INDICES: */
    UINT4 u4Idx0;
    tSNMP_OCTET_STRING_TYPE OctetStrIdx;
    UINT1 au1OctetListIdx[MAX_OCTETSTRING_SIZE];

#define VPNREMOTEID_VALUE 32
#define VPNREMOTEID_TYPE 8
#define VPNREMOTEID_KEY 32
    UINT1 au1Value [VPNREMOTEID_VALUE];
    UINT4 u4Status;
    UINT1 au1Type [VPNREMOTEID_TYPE];
    UINT1 au1Key [VPNREMOTEID_KEY];
    INT4  i4Type;
/* ValMasks for InternetGatewayDevice.VpnRemoteId.0 */
#define FS_VPN_REMOTE_ID_VALUE_MASK  (1)
#define FS_VPN_REMOTE_ID_STATUS_MASK (1 << 2)
#define FS_VPN_REMOTE_ID_TYPE_MASK   (1 << 3)
#define FS_VPN_REMOTE_ID_KEY_MASK    (1 << 4)

#define VPN_REMOTE_ID_VAL_MASK ( FS_VPN_REMOTE_ID_KEY_MASK | \
                                 FS_VPN_REMOTE_ID_TYPE_MASK | \
                                 FS_VPN_REMOTE_ID_VALUE_MASK)
} tVpnRemoteId;


extern UINT4 FsVpnIkePhase1PeerIdType [];
extern UINT4 FsVpnIkePhase1DHGroup [];
extern UINT4 FsVpnIkePhase1HashAlgo [];
extern UINT4 FsVpnPolicyIntfIndex [];
extern UINT4 FsVpnLocalProtectNetwork [];
extern UINT4 FsVpnAntiReplay [];
extern UINT4 FsVpnPolicyType [];
extern UINT4 FsVpnIkePhase2LifeTimeType [];
extern UINT4 FsVpnLocalTunTermAddr [];
extern UINT4 FsVpnIkePhase1Mode [];
extern UINT4 FsVpnIkePhase1LifeTimeType [];
extern UINT4 FsVpnPolicyFlag [];
extern UINT4 FsVpnPolicyPriority [];
extern UINT4 FsVpnPolicyRowStatus [];
extern UINT4 FsVpnEncrAlgo [];
extern UINT4 FsVpnIkePhase1LifeTime [];
extern UINT4 FsVpnRemoteProtectNetwork [];
extern UINT4 FsVpnIkePhase2DHGroup [];
extern UINT4 FsVpnPolicyName [];
extern UINT4 FsVpnLocalProtectSubnetMask [];
extern UINT4 FsVpnRemoteTunTermAddr [];
extern UINT4 FsVpnIkePhase1EncryptionAlgo [];
extern UINT4 FsVpnSecurityProtocol [];
extern UINT4 FsVpnAuthAlgo [];
extern UINT4 FsVpnEspKey [];
extern UINT4 FsVpnMode [];
extern UINT4 FsVpnAhKey [];
extern UINT4 FsVpnRemoteProtectSubnetMask [];
extern UINT4 FsVpnOutboundSpi [];
extern UINT4 FsVpnIkePhase1PeerIdValue [];
extern UINT4 FsVpnInboundSpi [];
extern UINT4 FsVpnIkePhase2LifeTime [];
extern UINT4 FsVpnProtocol [];
extern UINT4 FsVpnIkePhase2AuthAlgo [];
extern UINT4 FsVpnIkePhase2EspEncryptionAlgo [];
extern UINT4 FsVpnIpPktsIn [];
extern UINT4 FsVpnIkeSAsActive [];
extern UINT4 FsVpnIPSecSAsActive [];
extern UINT4 FsVpnIkeRekeys [];
extern UINT4 FsVpnIpPktsOut [];
extern UINT4 FsVpnIPSecNegoFailed [];
extern UINT4 FsVpnIkeNegoFailed [];


extern UINT4 FsVpnIpComp [];
extern UINT4 FsVpnIkeRAInfoProtectedNetBundle [];
extern UINT4 FsVpnIkePhase1LocalIdType [];
extern UINT4 FsVpnIkePhase1LocalIdValue [];

extern UINT4 FsVpnRaUserRowStatus [];
extern UINT4 FsVpnRaUserName [];
extern UINT4 FsVpnRaUserSecret [];
extern UINT4 FsVpnRaAddressPoolEnd [];
extern UINT4 FsVpnRaAddressPoolStart [];
extern UINT4 FsVpnRaAddressPoolRowStatus [];
extern UINT4 FsVpnRaAddressPoolName [];
extern UINT4 FsVpnProtectNetAddr [];
extern UINT4 FsVpnProtectNetMask [];
extern UINT4 FsVpnProtectNetStatus [];
extern UINT4 FsVpnProtectNetName [];

extern UINT4 FsVpnRemoteIdValue [];
extern UINT4 FsVpnRemoteIdStatus [];
extern UINT4 FsVpnRemoteIdType [];
extern UINT4 FsVpnRemoteIdKey [];
extern UINT1 FsVpnRemoteIdTableINDEX [];

extern UINT1 FsVpnTableINDEX [];
extern UINT1 FsVpnRaUsersTableINDEX [];
extern UINT1 FsVpnProtectNetTableINDEX [];
extern UINT1 FsVpnRaAddressPoolTableINDEX [];
extern UINT4 IfMainWanType [];
extern UINT1 IfMainTableINDEX [];

tVPN   *            
TrAdd_VPN (UINT4, tSNMP_OCTET_STRING_TYPE *, UINT4);

INT4                
TrScan_VpnRemoteId (VOID);

int  
TrGetInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
    (char *name, int idx1, int idx2, int idx3, int idx4,
     ParameterValue * value);

int  
TrSetInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
    (char *name, int idx1, int idx2, int idx3, int idx4,
     ParameterValue * value);

INT4 
TrSetNonMand_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
    (char *name, ParameterValue * value, tVPN * pNode);

INT4 
TrSetMand_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
    (char *name, ParameterValue * value, tVPN * pNode);

INT4 
TrSetAll_InternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
    (char *name, tVPN * pNode, ParameterValue * value, BOOL1 bTrDoSnmpSet);

int                 
TrGetInternetGatewayDevice_VPN_VPNStatistics (char *name,
                                                    ParameterValue
                                                    * value);

INT4 
TrScan_User    PROTO ((void));
tUser              *
TrAdd_User (UINT4, tSNMP_OCTET_STRING_TYPE *);
INT4 
TrScan_AddressPool PROTO ((void));
tAddressPool       *
TrAdd_AddressPool (UINT4, tSNMP_OCTET_STRING_TYPE *);
INT4                
TrScan_VpnRemoteId ();

tVpnRemoteId       *
TrAdd_VpnRemoteId (UINT4 u4TrIdx,
                                       tSNMP_OCTET_STRING_TYPE * pOctetStrIdx,
                                       INT4 i4Idx0);
int
TrGetInternetGatewayDevice_RAVPN_User (char *name, int idx1,
                                       ParameterValue * value);
int
TrGetInternetGatewayDevice_RAVPN_AddressPool (char *name, int idx1,
                                              ParameterValue * value);
int
TrGetInternetGatewayDevice_VpnRemoteId (char *name, int idx1,
                                        ParameterValue * value);
int
TrSetInternetGatewayDevice_RAVPN_User (char *name, int idx1,
                                       ParameterValue * value);
INT4
 TrSetNonMand_InternetGatewayDevice_RAVPN_User (char *name,
                                                ParameterValue * value,
                                                tUser * pNode);
int
TrSetInternetGatewayDevice_VpnRemoteId (char *name, int idx1,
                                        ParameterValue * value);
INT4
 TrSetNonMand_InternetGatewayDevice_VpnRemoteId (char *name,
                                                 ParameterValue * value,
                                                 tVpnRemoteId * pNode);
INT4
 TrSetMand_InternetGatewayDevice_VpnRemoteId (char *name,
                                              ParameterValue * value,
                                              tVpnRemoteId * pNode);
INT4                
TrSetNonMand_InternetGatewayDevice_RAVPN (char *name,
                                                ParameterValue *
                                                value,
                                                tRAVPN * pNode);
INT4                
TrSetAll_InternetGatewayDevice_VpnRemoteId (char *name,
                                                  tVpnRemoteId *
                                                  pNode,
                                                  ParameterValue *
                                                  value,
                                                  BOOL1
                                                  bTrDoSnmpSet);
int                 
TrSetInternetGatewayDevice_RAVPN_AddressPool (char *name,
                                                    int idx1,
                                                    ParameterValue
                                                    * value);

INT4                
TrSetNonMand_InternetGatewayDevice_RAVPN_AddressPool (char
                                                            *name,
                                                            ParameterValue
                                                            *
                                                            value,
                                                            tAddressPool
                                                            *
                                                            pNode);
INT4                
TrSetMand_InternetGatewayDevice_RAVPN_User (char *name,
                                                  ParameterValue *
                                                  value,
                                                  tUser * pNode);
INT4                
TrSetMand_InternetGatewayDevice_RAVPN_AddressPool (char
                                                         *name,
                                                         ParameterValue
                                                         * value,
                                                         tAddressPool
                                                         * pNode);

INT4                
TrSetAll_InternetGatewayDevice_RAVPN_User (char *name,
                                                 tUser * pNode,
                                                 ParameterValue *
                                                 value,
                                                 BOOL1
                                                 bTrDoSnmpSet);

INT4                
TrSetAll_InternetGatewayDevice_RAVPN_AddressPool (char
                                                        *name,
                                                        tAddressPool
                                                        * pNode,
                                                        ParameterValue
                                                        * value,
                                                        BOOL1
                                                        bTrDoSnmpSet);
INT4 
TrInitInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
    (int idx1, int idx2, int idx3, int idx4);

INT4 
TrDeleteInternetGatewayDevice_WANDevice_WANConnectionDevice_WANIPConnection_VPN
    (int idx1, int idx2, int idx3, int idx4);

INT4 
TrDeleteInternetGatewayDevice_WANDevice_WANConnectionDevice_WANPPPConnection_VPN
    (int idx1, int idx2, int idx3, int idx4);
INT4 
TrInitInternetGatewayDevice_WANDevice_WANConnectionDevice_WANPPPConnection_VPN
    (int idx1, int idx2, int idx3, int idx4);

INT4                
TrInitInternetGatewayDevice_RAVPN_User (int idx1);

INT4                
TrDeleteInternetGatewayDevice_RAVPN_User (int idx1);

INT4                
TrInitInternetGatewayDevice_RAVPN_AddressPool (int idx1);

INT4                
TrDeleteInternetGatewayDevice_RAVPN_AddressPool (int idx1);

INT4                
TrInitInternetGatewayDevice_VPN_VpnRemoteId (int idx1);

INT4                
TrDeleteInternetGatewayDevice_VPN_VpnRemoteId (int idx1);
