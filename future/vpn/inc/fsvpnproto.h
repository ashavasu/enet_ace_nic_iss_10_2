/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fsvpnproto.h,v 1.18 2015/07/17 09:49:17 siva Exp $
 *
 * Description: Function prototypes 
 *
 *******************************************************************/

#ifndef _FSVPNPROTO_H
#define _FSVPNPROTO_H

tVpnPolicy         *
VpnSnmpGetPolicy (CONST UINT1 *pPolicyName, INT4 NameLen);

tVpnRaAddressPool  *
VpnGetRaAddrPool ARG_LIST ((CONST UINT1 *pRaAddrPoolName, INT4 i4NameLen));

INT4 VpnGetMsrSaveStatus PROTO ((VOID));

INT4
VpnCheckIpsecManualMandatoryParams ( tVpnPolicy   * );

INT4
VpnCheckIkeMandatoryParams ( tVpnPolicy * pVpnPolicy);

INT4
VpnCheckMandatoryParams(tSNMP_OCTET_STRING_TYPE *);

UINT2
VpnUtilConvMask2Prefix ARG_LIST ((UINT4 u4NetMask));

VOID VpnRaDBAddUser(tVpnRaUserInfo *pVpnRaUserInfoNode,
                    tSNMP_OCTET_STRING_TYPE *pFsVpnRaUserName);

VOID VpnRaDBAddAddressPool(tVpnRaAddressPool *pVpnRaAddressPoolNode,
                           tSNMP_OCTET_STRING_TYPE *pFsVpnRaAddressPoolName);

INT4 
VpnUtilValidateEmailAddr ARG_LIST ((tSNMP_OCTET_STRING_TYPE *pEmailId));

INT1 
VpnUtilGetConfigTunnels ARG_LIST ((UINT4 *pu4RetValFsVpnConfigTunnels));

VOID
VpnUtilInitPolicyConfig ARG_LIST ((tVpnPolicy *pVpnPolicy));

VOID
VpnGetPolicyByIndex(UINT4 u4Index, tSNMP_OCTET_STRING_TYPE *pPolicyName );

UINT4
VpnUtilValidateIpAddress ARG_LIST ((UINT4 u4IpAddr));
UINT4
VpnUtilValidateMask ARG_LIST ((UINT4 u4Mask));

INT4
VpnIntfStateChngTrigger PROTO ((UINT2 u2State, UINT4 u4IfIndex,
                                     UINT4 u4IpAddress, UINT1 u1NwType,
                                     UINT1 u1WanType));
INT4
VpnIpv6IntfStateChngTrigger PROTO ((UINT2 u2State, UINT4 u4IfIndex,
                                    UINT1 u1NwType, UINT1 u1WanType));
INT4
VpnUtilsIsFqdn ARG_LIST ((CONST CHR1 * pc1Fqdn, INT4 i4Len));

INT4
VpnUtilsIsDn  ARG_LIST ((CONST CHR1 * pc1dn, INT4 i4Length));

INT1
VpnUtilGetIkeVersion (tVpnIpAddr * pPeerIpAddr, UINT1 *pu1IkeVersion);

UINT4
VpnUtilGetNextAddr ARG_LIST ((tVpnIpAddr * pVpnIpAddr,
                              tVpnIpAddr * pVpnStartIp,
                              tVpnIpAddr * pVpnEndIp));

INT4   VpnReadLine ARG_LIST((INT4 , UINT1 *)); 

UINT1 VpnValCryptoMapName (UINT1 *);
/*Certificates interface related prototypes*/
INT4
VpnIkeGenKeyPair(UINT1 *, UINT4, UINT4);
INT4
VpnIkeImportKey(UINT1*, UINT1*, UINT4);
INT4
VpnIkeShowKeys(UINT4);

INT4
VpnIkeDelKeys(UINT1*, UINT4);

INT4
VpnIkeGenCertReq(UINT1*, UINT1*, UINT1*, UINT4);

INT4
VpnIkeImportCert(UINT1*, UINT1, UINT1*);

INT4
VpnIkeImportPeerCert(UINT1*, UINT1*, UINT1, UINT1);

INT4
VpnIkeImportCACert(UINT1*, UINT1*, UINT1);

INT4
VpnIkeShowCerts(UINT4, UINT1*);
INT4
VpnIkeDelCerts(UINT4, UINT1*);
INT4
VpnIkeCertMapPeer(UINT1*, UINT1*, UINT1*);

INT4
VpnIkeDelCertMap(UINT1*, UINT1*, UINT1*);

INT4
VpnIkeSaveCerts(VOID);

tVpnRaUserInfo     *
VpnGetRaVpnUserInfo ARG_LIST ((CONST UINT1 *pu1UserName, INT4 i4NameLen));

INT4 
VpnSetRemoteIdRefCnt ARG_LIST ((tVpnPolicy *pVpnPolicy, INT2 i2Flags));
INT4
VpnSetCertInfoRefCnt ARG_LIST ((tVpnPolicy * pVpnPolicy, INT2 i2Flags));

VOID
VpnInit ARG_LIST ((INT1 *pi1Param));

INT4 
VpnSetRavpnPoolRefCnt ARG_LIST ((INT2 i2Flags));

INT4
VpnRestorePreSharedKey (UINT1 *, UINT1 *, INT1 );

INT4   
VpnUtilCallBack PROTO ((UINT4, ...));
#ifdef IKE_WANTED
INT1
VpnUtilFillIkeParams ARG_LIST ((tVpnPolicy * pVpnPolicy));
VOID
VpnUtilDelIkeParams ARG_LIST ((tVpnPolicy * pVpnPolicy));
INT1
VpnUtilDeleteKeyDB ARG_LIST ((tVpnIdInfo *pVpnIdInfo)); 

INT1
VpnUtilDeleteCertKeys ARG_LIST ((tVpnCertInfo *pVpnCertInfo));

INT1
VpnUtilUpdateCertDB ARG_LIST ((tVpnCertInfo *pVpnCertInfo)); 

INT1
VpnUtilDeleteCertDB ARG_LIST ((tVpnCertInfo *pVpnCertInfo)); 

INT1
VpnUtilUpdateCaCertDB ARG_LIST ((tVpnCaCertInfo *pVpnCaCertInfo)); 

INT1
VpnUtilDeleteCaCertDB ARG_LIST ((tVpnCaCertInfo *pVpnCaCertInfo)); 

INT1
VpnIkeCreateAndUpdateKeyDB ARG_LIST ((tVpnIdInfo *pVpnIdInfo, UINT1 *pu1IkeEngineName));
INT1
VpnUtilIkeUpdateRaUserDetails ARG_LIST ((tVpnRaUserInfo *pVpnRaUserInfoNode, INT4 i4AddDelFlag));
INT4
VpnUtilCheckIfRaAddrFree ARG_LIST ((tTMO_SLL * pAllocRaAddrList,
                                    tVpnIpAddr * pVpnIpAddr));
INT4
VpnUtilCheckRaAddrExistsforPeer ARG_LIST ((tVpnIpAddr * pVpnPeerAddr,
                      tVpnIpAddr * pVpnIpAddr, UINT4 *pu4PrefixLen ));
INT4
VpnUtilAddAddrToAllocList ARG_LIST ((tTMO_SLL * pAllocRaAddrList,
               tVpnIpAddr * pVpnIpAddr, tVpnIpAddr * pVpnPeerAddr));
INT4
VpnUtilDelAddrFromAllocList ARG_LIST ((tVpnIpAddr *pVpnIpAddr));
INT4
VpnUtilGetFreeAddrFromPool ARG_LIST ((tVpnIpAddr *pVpnPeerAddr,
                      tVpnIpAddr *pFreeIPAddr, UINT4 *pu4PrefixLen));

/*Certificates related function prototypes - Start */
INT4
IkeVpnGenKeyPair ARG_LIST((UINT1*, UINT1*, UINT4, UINT4));

INT4
IkeVpnImportKey ARG_LIST((UINT1*, UINT1*, UINT1*, UINT4));

INT4
IkeVpnShowKeys ARG_LIST((UINT1*, UINT4));

INT4
IkeVpnDelKeys ARG_LIST((UINT1*, UINT1*, UINT4));

INT4
IkeVpnGenCertReq ARG_LIST((UINT1*, UINT1*, UINT1*, UINT1*, UINT4));

INT4
IkeVpnImportCert ARG_LIST((UINT1*, UINT1*, UINT1, UINT1*));

INT4
IkeVpnImportPeerCert ARG_LIST((UINT1*, UINT1*, UINT1*, UINT1, UINT1));

INT4
IkeVpnImportCACert ARG_LIST((UINT1*, UINT1*, UINT1*, UINT1));

INT4
IkeVpnShowCerts ARG_LIST((UINT1*, UINT4, UINT1*));

INT4
IkeVpnDelCerts ARG_LIST((UINT1*, UINT4, UINT1*));

INT4
IkeVpnCertMapPeer ARG_LIST((UINT1*, UINT1*, UINT1*, UINT1*));

INT4
IkeVpnDelCertMap ARG_LIST((UINT1*, UINT1*, UINT1*, UINT1*));

INT4
IkeVpnSaveCerts ARG_LIST((UINT1 *));

/*Certificates related function prototypes - End */
#endif
UINT4
VpnUtilIpv4IsSameNet ARG_LIST((tIp4Addr * pIp4Addr, tIp4Addr *pIp4NetAddr,
                      UINT4 u4PrefixLen));
VOID
Secv4SendIcmpErrMsgToUser (tCRU_BUF_CHAIN_HEADER * pBuf, t_ICMP * pIcmp);
#ifdef SECURITY_KERNEL_MAKE_WANTED
UINT2
Secv4IPCalcChkSum (UINT1 *pBuf, UINT4 u4Size);
#endif
UINT1 VpnUtilRaAddrDbLookUp (UINT4 u4DestAddr);

INT4
VpnAccessListCheckIpMask (UINT4 u4LocalMask,UINT4 u4LocalIpAddr,
                          UINT4 u4RemoteMask,UINT4 u4RemoteIpAddr);
#endif /*_FSVPNPROTO_H */

 
