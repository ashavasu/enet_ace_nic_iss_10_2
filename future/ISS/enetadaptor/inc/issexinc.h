/* $Id: issexinc.h,v 1.1 2012/03/02 11:16:11 siva Exp $*/

#ifndef _ISSEXINC_H
#define _ISSEXINC_H

/* Common Includes */
#include "lr.h"
#include "cfa.h"
#include "tcp.h"
#ifndef LNXIP4_WANTED
#include "sli.h"
#endif
#include "rmgr.h"
#include "iss.h"
#include "msr.h"
#include "ip.h"

#include "snmccons.h"
#include "snmcdefn.h"
#include "fssnmp.h"

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "issnpwr.h"
#include "issnp.h"
#endif

/* ISS includes */
#include "issmacro.h"
#include "isstdfs.h"
#include "issglob.h"
#include "issexmacr.h"
#include "issextdfs.h"
#include "issexglob.h"
#include "issexprot.h"

#ifdef MBSM_WANTED
#include "mbsm.h"
#include "issmbsm.h"
#endif

#ifdef ISS_METRO_WANTED
#include "fsvlan.h"
#endif

#include "hwaud.h"
#include "hwaudmap.h"
#endif /* _ISSEXINC_H */
