
/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: issexweb.h,v 1.1 2012/03/02 11:16:11 siva Exp $
 * *
 * * Description: Proto types for Low Level  Routines
 * *********************************************************************/

#ifndef _ISSEXINC_H
#define _ISSEXINC_H

#include "webiss.h"
#include "isshttp.h"
#include "aclcli.h"
#include "iss.h"
#include "fsisselw.h"
#include "diffsrv.h"


#define   ACL_EXTENDED_START 1001
#define   FILTER_OFFSET_LENGTH 128

/* Prototypes for specific pages processing */
INT4 IssProcessCustomPages (tHttp * pHttp);
VOID IssRedirectMacFilterPage (tHttp *pHttp);
VOID IssRedirectDiffSrvPage (tHttp *pHttp);
VOID IssDxProcessIPFilterConfPage (tHttp * pHttp);
VOID IssDxProcessIPStdFilterConfPage (tHttp * pHttp);
VOID IssDxProcessMACFilterConfPage (tHttp * pHttp);
VOID IssDxProcessIPFilterConfPageSet (tHttp * pHttp);
VOID IssDxProcessIPFilterConfPageGet (tHttp * pHttp);
VOID IssDxProcessIPStdFilterConfPageGet (tHttp * pHttp);
VOID IssDxProcessIPStdFilterConfPageSet (tHttp * pHttp);
VOID IssDxProcessMACFilterConfPageGet (tHttp * pHttp);
VOID IssDxProcessMACFilterConfPageSet (tHttp * pHttp);
VOID IssDxProcessDfsPolicyMapPage (tHttp * pHttp);
VOID IssDxProcessDfsPolicyMapPageGet (tHttp * pHttp);
VOID IssDxProcessDfsPolicyMapPageSet (tHttp * pHttp);
VOID IssDxProcessSchdAlgoPage (tHttp * pHttp);
VOID IssDxProcessSchdAlgoPageGet (tHttp * pHttp);
VOID IssDxProcessSchdAlgoPageSet (tHttp * pHttp);
VOID IssDxProcessDfsCosqWeightBWConfPage (tHttp * pHttp);
VOID IssDxProcessDfsCosqWeightBWConfPageGet (tHttp * pHttp);
VOID IssDxProcessDfsCosqWeightBWConfPageSet (tHttp * pHttp); 


VOID IssProcessUDBFilterPage (tHttp * pHttp);
VOID IssProcessUDBFilterPageGet (tHttp * pHttp);
VOID IssProcessUDBFilterPageSet (tHttp * pHttp); 


VOID IssProcessRedirectPage (tHttp * pHttp);
VOID IssProcessRedirectPageGet (tHttp * pHttp);
VOID IssProcessRedirectPageSet (tHttp * pHttp); 

VOID IssProcessProvisionModePage (tHttp * pHttp);
VOID IssProcessProvisionModePageGet (tHttp * pHttp);
VOID IssProcessProvisionModePageSet (tHttp * pHttp);
extern INT4 IssIsMemberPort (UINT1 *, UINT4 , UINT4);


/* nmh routines used for scheduling */
extern INT4 nmhGetFirstIndexFsDiffServCoSqAlgorithmTable (INT4 *);
extern INT4 nmhGetNextIndexFsDiffServCoSqAlgorithmTable (INT4, INT4 *);
extern INT4 nmhTestv2FsDiffServCoSqAlgorithm (UINT4 *, INT4, INT4);
extern INT4 nmhSetFsDiffServCoSqAlgorithm (INT4, INT4);
extern INT4 nmhGetFsDiffServCoSqAlgorithm (INT4, INT4 *);
extern INT4 nmhGetFirstIndexFsDiffServCoSqWeightBwTable (INT4 *, INT4 *);
extern INT4 nmhGetFsDiffServCoSqWeight (INT4, INT4, UINT4 *);
extern INT4 nmhGetFsDiffServCoSqBwMin (INT4, INT4, UINT4 *); 
extern INT4 nmhGetFsDiffServCoSqBwMax (INT4, INT4, UINT4 *);
extern INT4 nmhGetFsDiffServCoSqBwFlags (INT4, INT4, INT4 *); 
extern INT4 nmhTestv2FsDiffServCoSqWeight (UINT4 *, INT4, INT4, UINT4);
extern INT4 nmhTestv2FsDiffServCoSqBwMin (UINT4 *, INT4, INT4, UINT4);
extern INT4 nmhTestv2FsDiffServCoSqBwMax (UINT4 *, INT4, INT4, UINT4);
extern INT4 nmhTestv2FsDiffServCoSqBwFlags (UINT4 *, INT4, INT4, INT4);
extern INT4 nmhSetFsDiffServCoSqWeight (INT4, INT4, UINT4);
extern INT4 nmhSetFsDiffServCoSqBwMin (INT4, INT4, UINT4);
extern INT4 nmhSetFsDiffServCoSqBwMax(INT4, INT4, UINT4);
extern INT4 nmhSetFsDiffServCoSqBwFlags(INT4, INT4, INT4);
extern INT1 nmhTestv2DiffServMultiFieldClfrStorage(UINT4 *, UINT4, INT4);
extern INT1 nmhSetDiffServMultiFieldClfrStorage (UINT4 ,INT4);
extern INT1 nmhGetDiffServMultiFieldClfrStorage(UINT4 , INT4 *);

/*nmh routines for UDB And Redirect */
extern INT1 nmhValidateIndexInstanceIssAclUserDefinedFilterTable ARG_LIST((UINT4 ));
extern INT1 nmhGetFirstIndexIssAclUserDefinedFilterTable ARG_LIST((UINT4 *));
extern INT1 nmhGetNextIndexIssAclUserDefinedFilterTable ARG_LIST((UINT4 , UINT4 *));
extern INT1 nmhGetIssAclUserDefinedFilterPktType ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetIssAclUserDefinedFilterOffSetBase ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetIssAclUserDefinedFilterOffSetValue ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetIssAclUserDefinedFilterOffSetMask ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetIssAclUserDefinedFilterPriority ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetIssAclUserDefinedFilterAction ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetIssAclUserDefinedFilterIdOneType ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetIssAclUserDefinedFilterIdOne ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetIssAclUserDefinedFilterIdOneHwStatus ARG_LIST((UINT4 ,INT4 *));


extern INT1 nmhGetIssAclUserDefinedFilterIdTwoType ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetIssAclUserDefinedFilterIdTwo ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetIssAclUserDefinedFilterIdTwoHwStatus ARG_LIST((UINT4 ,INT4 *));


extern INT1 nmhGetIssAclUserDefinedFilterIdTwo ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetIssAclUserDefinedFilterIdTwoHwStatus ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetIssAclUserDefinedFilterStatus ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhSetIssAclUserDefinedFilterPktType ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhSetIssAclUserDefinedFilterOffSetBase ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhSetIssAclUserDefinedFilterOffSetValue ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetIssAclUserDefinedFilterOffSetMask ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetIssAclUserDefinedFilterPriority ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhSetIssAclUserDefinedFilterAction ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhSetIssAclUserDefinedFilterIdOneType ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhSetIssAclUserDefinedFilterIdOne ARG_LIST((UINT4  ,UINT4 ));
extern INT1 nmhSetIssAclUserDefinedFilterIdTwoType ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhSetIssAclUserDefinedFilterIdTwo ARG_LIST((UINT4  ,UINT4 ));
extern INT1 nmhSetIssAclUserDefinedFilterStatus ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhTestv2IssAclUserDefinedFilterPktType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2IssAclUserDefinedFilterOffSetBase ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2IssAclUserDefinedFilterOffSetValue ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2IssAclUserDefinedFilterOffSetMask ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2IssAclUserDefinedFilterPriority ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2IssAclUserDefinedFilterAction ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2IssAclUserDefinedFilterIdOneType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2IssAclUserDefinedFilterIdOne ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
extern INT1 nmhTestv2IssAclUserDefinedFilterIdTwoType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2IssAclUserDefinedFilterIdTwo ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
extern INT1 nmhTestv2IssAclUserDefinedFilterStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/*Redirect */
extern INT1 nmhGetFirstIndexIssRedirectInterfaceGrpTable ARG_LIST((UINT4 *)); 
extern INT1 nmhGetNextIndexIssRedirectInterfaceGrpTable ARG_LIST((UINT4 , UINT4 *)); 
extern INT1 nmhGetIssRedirectInterfaceGrpFilterType ARG_LIST((UINT4 ,INT4 *)); 
extern INT1 nmhGetIssRedirectInterfaceGrpFilterId ARG_LIST((UINT4 ,UINT4 *)); 
extern INT1 nmhGetIssRedirectInterfaceGrpDistByte ARG_LIST((UINT4 ,INT4 *)); 
extern INT1 nmhGetIssRedirectInterfaceGrpPortList ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * )); 
extern INT1 nmhGetIssRedirectInterfaceGrpType ARG_LIST((UINT4 ,INT4 *)); 
extern INT1 nmhGetIssRedirectInterfaceGrpStatus ARG_LIST((UINT4 ,INT4 *)); 
extern INT1 nmhSetIssRedirectInterfaceGrpFilterType ARG_LIST((UINT4  ,INT4 )); 
extern INT1 nmhSetIssRedirectInterfaceGrpFilterId ARG_LIST((UINT4  ,UINT4 )); 
extern INT1 nmhSetIssRedirectInterfaceGrpDistByte ARG_LIST((UINT4  ,INT4 )); 
extern INT1 nmhSetIssRedirectInterfaceGrpPortList ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *)); 
extern INT1 nmhSetIssRedirectInterfaceGrpType ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhSetIssRedirectInterfaceGrpStatus ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhTestv2IssRedirectInterfaceGrpFilterType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2IssRedirectInterfaceGrpFilterId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
extern INT1 nmhTestv2IssRedirectInterfaceGrpDistByte ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2IssRedirectInterfaceGrpPortList ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2IssRedirectInterfaceGrpType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2IssRedirectInterfaceGrpStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1  nmhGetIssAclL3FilterOutPortList (INT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1  nmhGetIssAclL3FilterInPortList (INT4, tSNMP_OCTET_STRING_TYPE *);




extern UINT1 nmhTestv2IssAclL3FilteAddrType(UINT4 * , 
                                            INT4 , INT4 );

extern UINT1 nmhSetIssAclL3FilteAddrType(INT4 ,INT4); 
extern UINT1 nmhTestv2IssAclL3FilterSrcIpAddr (UINT4 * , INT4 ,tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhSetIssAclL3FilterSrcIpAddr (INT4, tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhTestv2IssAclL3FilterDstIpAddr (UINT4 *e, INT4, tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhSetIssAclL3FilterDstIpAddr (INT4, tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhTestv2IssAclL3FilterDstIpAddrPrefixLength  (UINT4*, INT4, UINT4);
extern UINT1 nmhSetIssAclL3FilterDstIpAddrPrefixLength (INT4, UINT4);
extern UINT1 nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength (UINT4 *, INT4, UINT4);
extern UINT1 nmhSetIssAclL3FilterSrcIpAddrPrefixLength (INT4, UINT4);
extern UINT1 nmhTestv2IssAclL3FilterFlowId (UINT4 *, INT4, UINT4);
extern UINT1 nmhSetIssAclL3FilterFlowId (INT4 , UINT4);
extern UINT1 nmhGetIssAclL3FilteAddrType (INT4, INT4 *);
extern UINT1 nmhGetIssAclL3FilterSrcIpAddr (INT4, tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhGetIssAclL3FilterDstIpAddr (INT4, tSNMP_OCTET_STRING_TYPE *); 
extern UINT1 nmhGetIssAclL3FilterDstIpAddrPrefixLength (INT4, UINT4 *);
extern UINT1 nmhGetIssAclL3FilterSrcIpAddrPrefixLength (INT4, UINT4 *);
extern UINT1 nmhGetIssAclL3FilterFlowId (INT4, UINT4 *);

extern UINT1 nmhGetIssAclL2FilterSubAction ARG_LIST((INT4 ,INT4 *));
extern UINT1 nmhGetIssAclL2FilterSubActionId ARG_LIST((INT4 ,INT4 *));
extern UINT1 nmhGetIssAclUserDefinedFilterSubAction ARG_LIST((UINT4 ,INT4 *));
extern UINT1 nmhGetIssAclUserDefinedFilterSubActionId ARG_LIST((UINT4 ,INT4 *));
extern UINT1 nmhGetIssAclUserDefinedFilterInPortList ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern UINT1 nmhTestv2IssAclUserDefinedFilterSubAction ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern UINT1 nmhTestv2IssAclUserDefinedFilterSubActionId ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern UINT1 nmhSetIssAclUserDefinedFilterSubActionId ARG_LIST((UINT4  ,INT4 ));
extern UINT1 nmhGetIssRedirectInterfaceGrpUdbPosition ARG_LIST((UINT4 ,INT4 *));
extern UINT1 nmhGetIssRedirectInterfaceGrpIdNextFree ARG_LIST((UINT4 *));
extern UINT1 nmhTestv2IssRedirectInterfaceGrpUdbPosition ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern UINT1 nmhSetIssRedirectInterfaceGrpUdbPosition ARG_LIST((UINT4  ,INT4 ));
extern UINT1 nmhTestv2IssAclL3FilterSubAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern UINT1 nmhTestv2IssAclL3FilterSubActionId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern UINT1 nmhSetIssAclL3FilterSubAction ARG_LIST((INT4  ,INT4 ));
extern UINT1 nmhSetIssAclL3FilterSubActionId ARG_LIST((INT4  ,INT4 ));
extern UINT1 nmhGetIssAclL3FilterSubAction ARG_LIST((INT4 ,INT4 *));
extern UINT1 nmhGetIssAclL3FilterSubActionId ARG_LIST((INT4 ,INT4 *));
extern UINT1 nmhTestv2IssAclL2FilterSubAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern UINT1 nmhTestv2IssAclL2FilterSubActionId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern UINT1 nmhSetIssAclL2FilterSubAction ARG_LIST((INT4  ,INT4 ));
extern UINT1 nmhSetIssAclL2FilterSubActionId ARG_LIST((INT4  ,INT4 ));
extern UINT1 nmhSetIssAclUserDefinedFilterSubAction ARG_LIST((UINT4  ,INT4 ));
extern UINT1 nmhSetIssAclUserDefinedFilterInPortList ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));



extern INT4
DsWebnmGetPolicyMapEntry (INT4 i4PolicyMapId, tDiffServWebClfrData * pClfrData);

extern INT4
DsWebnmSetPolicyMapEntry (tDiffServWebSetClfrEntry * pClfrEntry,
                          UINT1 u1RowStatus, UINT1 *pu1ErrString);

tSpecificPage       asIssTargetpages[] = {
    {"wintegra_ip_filterconf.html", IssDxProcessIPFilterConfPage},
    {"wintegra_ip_stdfilterconf.html", IssDxProcessIPStdFilterConfPage},
    {"wintegra_mac_filterconf.html", IssDxProcessMACFilterConfPage},
    {"wintegra_commit_action.html", IssProcessProvisionModePage},
    {"acl_userdefined_filters.html",IssProcessUDBFilterPage},
    {"acl_redirect.html", IssProcessRedirectPage},
#ifdef DIFFSRV_WANTED
    {"wintegra_dfs_policymapconf.html", IssDxProcessDfsPolicyMapPage},
 {"wintegra_dfs_cosqschdalgorithm.html", IssDxProcessSchdAlgoPage},
 {"wintegra_dfs_cosqweightbandwd.html", IssDxProcessDfsCosqWeightBWConfPage},
#endif
    {"", NULL}
};

#endif /* _ISSWEB_H */
