/* $Id: issexglob.h,v 1.1 2012/03/02 11:16:11 siva Exp $*/

#ifndef _ISSEXGLOB_H        
#define _ISSEXGLOB_H        

#ifdef _ISSEXSYS_C
tIssExtGlobalInfo                gIssExGlobalInfo;
tIssRedirectIntfGrpTable         *gpIssRedirectIntfInfo;
#else
extern tIssExtGlobalInfo         gIssExGlobalInfo;
extern tIssRedirectIntfGrpTable         *gpIssRedirectIntfInfo;
/* This is already defined in issglob.h in ISS/common/system */ 
extern UINT4                  gu4IssCidrSubnetMask[ISS_MAX_CIDR + 1];
#endif

extern tIssPriorityFilterTable  *gaPriorityTable[ISS_MAX_FILTER_PRIORITY];
extern UINT4                  gu4IssDebugFlags;
#endif
