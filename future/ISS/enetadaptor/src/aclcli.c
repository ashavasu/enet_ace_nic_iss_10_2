/* $Id: aclcli.c,v 1.3 2013/01/18 11:59:44 siva Exp $ */
/************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                 */
/* Licensee Aricent Inc., 2004-2005       */
/* $Id: aclcli.c,v 1.3 2013/01/18 11:59:44 siva Exp $*/
/*  FILE NAME             : aclcli.c                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                 */
/*  SUBSYSTEM NAME        : ISS                             */
/*  MODULE NAME           : CLI                             */
/*  LANGUAGE              : C                               */
/*  TARGET ENVIRONMENT    :                                 */
/*  DATE OF FIRST RELEASE :                                 */
/*  AUTHOR                : Aricent Inc.                 */
/*  DESCRIPTION           : This file contains CLI routines */
/*                          related to system commands      */
/*                                                          */
/************************************************************/
#ifndef __ACLCLI_C__
#define __ACLCLI_C__

#include "lr.h"
#include "issexinc.h"
#include "fsissewr.h"
#include "fsisselw.h"
#include "fsissalw.h"
#include "aclcli.h"
#include "isscli.h"
#include "aclmcli.h"
#include "fsissecli.h"
#include "fsissacli.h"
#include "fsissewr.h"
#include "fssnmp.h"

/***************************************************************/
/*  Function Name   : cli_process_acl_cmd                      */
/*  Description     : This function servers as the handler for */
/*                    all system related CLI commands          */
/*  Input(s)        :                                          */
/*                    CliHandle - CLI Handle                   */
/*                    u4Command - Command given by user        */
/*                    Variable set of inputs depending on      */
/*                    the user command                         */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : None                                     */
/***************************************************************/
INT4
cli_process_acl_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[CLI_ACL_MAX_ARGS];
    UINT1               au1Tmp[CFA_MAX_PORT_NAME_LENGTH];
    INT1                argno = 0;
    UINT1               u1UdbFilterPosition = 0;
    UINT1              *pu1Alias = NULL;
    UINT4               u4ErrCode = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT4               u4SrcPermit = 0;
    UINT4               u4DstPermit = 0;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4SrcIpMask = 0;
    UINT4               u4SrcPrefixLen = 0;
    UINT4               u4DstPrefixLen = 0;
    UINT4               u4DestIpAddr = 0;
    UINT4               u4DestIpMask = 0;
    UINT4               u4SubAction = ISS_NONE;
    UINT4               u4MaxSrcPort = 65535;
    UINT4               u4MinSrcPort = 1;
    UINT4               u4MinDstPort = 1;
    UINT4               u4MaxDstPort = 65535;
    INT4                i4Tos = 0;
    UINT4               u4BitType = 0;
    INT4                i4Priority = 0;
    INT4                i4Action = 0;
    INT4                i4Protocol = ISS_ANY;
    INT4                i4MsgType = ISS_ANY;
    INT4                i4MsgCode = ISS_ANY;
    INT4                i4FilterNo = 0;
    INT4                i4FilterType = 0;
    INT4                i4SVlan = 0;
    INT4                i4SVlanPrio = -1;
    INT4                i4CVlan = 0;
    INT4                i4CVlanPrio = -1;
    tMacAddr            SrcMacAddr;
    tMacAddr            DestMacAddr;
    UINT4               u4IsValidSrcPort;
    UINT4               u4IsValidDestPort;
    INT4                i4OuterEType = 0;
    INT4                i4EtherType = 0;
    INT4                i4TagType = 0;
    INT4                i4NextFilterType = ISS_NOFILTER;
    INT4                i4NextFilterId = 0;
    UINT4               u4PktType = 0;
    UINT2               u2SubActionId = 0;
    UINT2               u2PktStart = 0;
    UINT2               u2Offset1Pos = 0xFFFF;
    UINT2               u2Offset1Val = 0;

    UINT2               u2Offset2Pos = 0xFFFF;
    UINT2               u2Offset2Val = 0;
    UINT2               u2Offset3Pos = 0xFFFF;
    UINT2               u2Offset3Val = 0;
    UINT2               u2Offset4Pos = 0xFFFF;
    UINT2               u2Offset4Val = 0;
    UINT2               u2Offset5Pos = 0xFFFF;
    UINT2               u2Offset5Val = 0;
    UINT2               u2Offset6Pos = 0xFFFF;
    UINT2               u2Offset6Val = 0;
    INT4                i4FilterOper = 0;
    UINT4               u4Filter1Id = 0;
    UINT4               u4Filter2Id = 0;
    UINT4               u4Filter3Id = 0;
    UINT4               u4Priority = 0;
    UINT1              *pu1Inst = NULL;
    va_start (ap, u4Command);
    UINT4               u4FlowId = 0;
    tIp6Addr            SrcIp6Addr;
    tIp6Addr            DstIp6Addr;

    MEMSET (au1Tmp, 0, CFA_MAX_PORT_NAME_LENGTH);
    pu1Alias = &au1Tmp[0];

    /* Third arguement is always interface name/index */
    pu1Inst = (UINT1 *) va_arg (ap, UINT4 *);

    /* Walk through the rest of the arguements and store in args array.  */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == CLI_ACL_MAX_ARGS)
            break;
    }

    va_end (ap);

    CLI_SET_ERR (0);

    CliRegisterLock (CliHandle, IssLock, IssUnLock);

    ISS_LOCK ();

    switch (u4Command)
    {
        case CLI_IP_ACL:
            i4RetStatus =
                AclCreateIPFilter (CliHandle, CLI_PTR_TO_U4 (args[0]),
                                   CLI_PTR_TO_I4 (args[1]));
            break;

        case CLI_NO_IP_ACL:
            i4RetStatus = AclDestroyIPFilter (CliHandle,
                                              CLI_PTR_TO_I4 (args[1]));
            break;

        case CLI_MAC_ACL:
            i4RetStatus = AclCreateMacFilter (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_USR_DEF_ACL:
            i4RetStatus =
                AclCreateUserDefFilter (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_NO_MAC_ACL:
            i4RetStatus = AclDestroyMacFilter (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_NO_USR_DEF_ACL:
            i4RetStatus =
                AclDestroyUserDefFilter (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_ACL_PERMIT:
        case CLI_ACL_DENY:
            /* Get the source IP,source mask, destination IP and destination mask
             */

            /* Redirect Filter Change */

            /* arg [6] -----------> Redirect True /False 
             * arg [7] ------------> Redirect to Single Interface or PortList 
             * arg [8] -----------> Traffic Distribution Method
             * arg [9] -------> Interface Type either ENET/LAGG
             * arg [10] ------> Port List of Interface type either ENET/LAGG
             * arg [11] -------> Interface Type either ENET/LAGG
             * arg [12] -------> Port List of Interface type either ENET/LAGG
             * arg [13] -----> Interface Type of Single Index
             * arg [14] -----> Port Number */

            if (args[1] != NULL)
            {
                u4SrcIpAddr = *args[1];
            }
            if ((args[2]) != NULL)
            {
                u4SrcIpMask = *args[2];
            }
            if ((args[4]) != NULL)
            {
                u4DestIpAddr = *args[4];
            }
            if ((args[5]) != NULL)
            {
                u4DestIpMask = *args[5];
            }
            if (u4Command == CLI_ACL_DENY)
            {
                i4Action = ISS_DROP;
                if (args[6] != NULL)
                {
                    u4Priority = *(UINT4 *) args[6];
                }

            }
            else
            {
                if (CLI_PTR_TO_I4 (args[6]) == ISS_REDIRECT_TO)
                {
                    i4Action = ISS_REDIRECT_TO;
                    i4FilterType = ISS_L3_REDIRECT;

                }
                else
                {
                    i4Action = ISS_ALLOW;
                }

                if ((UINT4 *) (args[16]) != NULL)
                {

                    u2SubActionId = (UINT2) *(UINT4 *) (args[16]);
                }

                u4SubAction = CLI_PTR_TO_U4 (args[15]);

            }

            if (args[17] != NULL)
            {
                u4Priority = *(UINT4 *) args[17];
            }

            i4RetStatus =
                AclStdIpFilterConfig (CliHandle, i4Action,
                                      CLI_PTR_TO_U4 (args[0]),
                                      u4SrcIpAddr, u4SrcIpMask,
                                      CLI_PTR_TO_U4 (args[3]), u4DestIpAddr,
                                      u4DestIpMask, u4SubAction,
                                      u2SubActionId, u4Priority);

            if (i4Action == ISS_REDIRECT_TO)
            {

                AclRedirectFilterConfig (CliHandle, i4FilterType,
                                         CLI_PTR_TO_I4 (args[7]),
                                         CLI_PTR_TO_I4 (args[8]),
                                         (INT1 *) args[9], (INT1 *) args[10],
                                         (INT1 *) args[11], (INT1 *) args[12],
                                         (INT1 *) args[13], (INT1 *) args[14],
                                         0);
            }

            break;

        case CLI_USR_DEFANDORNOT_FILTER:
            if (args[0] != NULL)
            {
                i4FilterOper = CLI_PTR_TO_I4 (args[0]);
            }
            if (args[1] != NULL)
            {
                u4Filter1Id = *(UINT4 *) args[1];
            }
            if (args[2] != NULL)
            {
                u4Filter2Id = *(UINT4 *) args[2];
            }
            if (args[3] != NULL)
            {
                u4Filter3Id = *(UINT4 *) args[3];
            }
            if (args[4] != NULL)
            {
                u4Priority = *(UINT4 *) args[4];
            }
            i4RetStatus = AclUserDefinedOperations (CliHandle, i4FilterOper,
                                                    u4Filter1Id, u4Filter2Id,
                                                    u4Filter3Id, u4Priority);
            break;
        case CLI_ACL_UDB_PERMIT:
        case CLI_ACL_UDB_DENY:

            if ((UINT4 *) (args[0]) != NULL)
            {
                u4PktType = CLI_PTR_TO_U4 (args[0]);
            }
            if ((UINT4 *) (args[1]) != NULL)
            {
                u2PktStart = (UINT2) CLI_PTR_TO_U4 (args[1]);
            }

            if ((UINT4 *) (args[2]) != NULL)
            {
                u2Offset1Pos = (UINT2) *(UINT4 *) (args[2]);
            }
            if ((UINT4 *) (args[3]) != NULL)
            {
                u2Offset1Val = (UINT2) *(UINT4 *) (args[3]);
            }
            if ((UINT4 *) (args[4]) != NULL)
            {
                u2Offset2Pos = (UINT2) *(UINT4 *) (args[4]);
            }
            if ((UINT4 *) (args[5]) != NULL)
            {
                u2Offset2Val = (UINT2) *(UINT4 *) (args[5]);
            }
            if ((UINT4 *) (args[6]) != NULL)
            {
                u2Offset3Pos = (UINT2) *(UINT4 *) (args[6]);
            }
            if ((UINT4 *) (args[7]) != NULL)
            {
                u2Offset3Val = (UINT2) *(UINT4 *) (args[7]);
            }
            if ((UINT4 *) (args[8]) != NULL)
            {
                u2Offset4Pos = (UINT2) *(UINT4 *) (args[8]);
            }
            if ((UINT4 *) (args[9]) != NULL)
            {
                u2Offset4Val = (UINT2) *(UINT4 *) (args[9]);
            }
            if ((UINT4 *) (args[10]) != NULL)
            {
                u2Offset5Pos = (UINT2) *(UINT4 *) (args[10]);
            }
            if ((UINT4 *) (args[11]) != NULL)
            {
                u2Offset5Val = (UINT2) *(UINT4 *) (args[11]);
            }
            if ((UINT4 *) (args[12]) != NULL)
            {
                u2Offset6Pos = (UINT2) *(UINT4 *) (args[12]);
            }
            if ((UINT4 *) (args[13]) != NULL)
            {
                u2Offset6Val = (UINT2) *(UINT4 *) (args[13]);
            }

            if (u4Command == CLI_ACL_UDB_DENY)
            {
                i4Action = ISS_DROP;
                if (args[14] != NULL)
                {
                    u4Priority = *(UINT4 *) args[14];
                }
            }
            else
            {
                if (CLI_PTR_TO_I4 (args[14]) == ISS_REDIRECT_TO)
                {
                    i4Action = ISS_REDIRECT_TO;
                    i4FilterType = ISS_UDB_REDIRECT;

                }
                else
                {
                    i4Action = ISS_ALLOW;
                }

                if ((UINT4 *) (args[24]) != NULL)
                {
                    u2SubActionId = (UINT2) *(UINT4 *) (args[24]);
                }

                u4SubAction = CLI_PTR_TO_U4 (args[23]);

                if (args[26] != NULL)
                {
                    u4Priority = *(UINT4 *) args[26];
                }
            }

            i4RetStatus =
                AclUserDefinedFilterConfig (CliHandle, i4Action,
                                            u4PktType, u2PktStart,
                                            u2Offset1Pos,
                                            u2Offset1Val,
                                            u2Offset2Pos,
                                            u2Offset2Val,
                                            u2Offset3Pos,
                                            u2Offset3Val,
                                            u2Offset4Pos,
                                            u2Offset4Val,
                                            u2Offset5Pos,
                                            u2Offset5Val,
                                            u2Offset6Pos, u2Offset6Val,
                                            u4SubAction, u2SubActionId,
                                            u4Priority);

            if (i4Action == ISS_REDIRECT_TO)
            {
                if ((UINT4 *) args[25] != NULL)
                {
                    u1UdbFilterPosition = (UINT1) *(UINT4 *) args[25];
                }

                AclRedirectFilterConfig (CliHandle, i4FilterType,
                                         CLI_PTR_TO_I4 (args[15]),
                                         CLI_PTR_TO_I4 (args[16]),
                                         (INT1 *) args[17], (INT1 *) args[18],
                                         (INT1 *) args[19], (INT1 *) args[20],
                                         (INT1 *) args[21], (INT1 *) args[22],
                                         u1UdbFilterPosition);
            }

            break;

        case CLI_ACL_PERMIT_PROTO:
        case CLI_ACL_DENY_PROTO:
        case CLI_ACL_PB_PERMIT_PROTO:
        case CLI_ACL_PB_DENY_PROTO:

            /* Get the source IP,source mask, destination IP and destination mask
             */
            if ((UINT4 *) (args[2]) != NULL)
            {
                u4SrcIpAddr = *(UINT4 *) (args[2]);
            }
            if ((UINT4 *) (args[3]) != NULL)
            {
                u4SrcIpMask = *(UINT4 *) (args[3]);
            }

            if ((UINT4 *) (args[5]) != NULL)
            {
                u4DestIpAddr = *(UINT4 *) (args[5]);
            }
            if ((UINT4 *) (args[6]) != NULL)
            {
                u4DestIpMask = *(UINT4 *) (args[6]);
            }

            if ((UINT4 *) (args[9]) != NULL)
            {
                i4Priority = (*(UINT4 *) (args[9]));
            }
            else
            {
                i4Priority = ISS_DEFAULT_FILTER_PRIORITY;
            }

            if ((u4Command == CLI_ACL_PB_PERMIT_PROTO) ||
                (u4Command == CLI_ACL_PB_DENY_PROTO))
            {
                if ((INT4 *) (args[10]) != NULL)
                {
                    i4SVlan = *(INT4 *) (args[10]);
                }
                if ((INT4 *) (args[11]) != NULL)
                {
                    i4SVlanPrio = *(INT4 *) (args[11]);
                }

                if ((INT4 *) (args[12]) != NULL)
                {
                    i4CVlan = *(INT4 *) (args[12]);
                }
                if ((INT4 *) (args[13]) != NULL)
                {
                    i4CVlanPrio = *(INT4 *) (args[13]);
                }
                i4TagType = CLI_PTR_TO_I4 (args[14]);
            }

            if ((u4Command == CLI_ACL_DENY_PROTO) ||
                (u4Command == CLI_ACL_PB_DENY_PROTO))
            {
                i4Action = ISS_DROP;
            }
            else
            {
                if ((CLI_PTR_TO_I4 (args[10]) == ISS_REDIRECT_TO) ||
                    (CLI_PTR_TO_I4 (args[15]) == ISS_REDIRECT_TO))
                {
                    i4Action = ISS_REDIRECT_TO;
                    i4FilterType = ISS_L3_REDIRECT;
                }
                else
                {
                    i4Action = ISS_ALLOW;
                }
                if (u4Command == CLI_ACL_PERMIT_PROTO)
                {
                    if ((UINT4 *) (args[20]) != NULL)
                    {
                        u2SubActionId = (UINT2) *(UINT4 *) (args[20]);
                    }

                    u4SubAction = CLI_PTR_TO_U4 (args[19]);
                }
                if (u4Command == CLI_ACL_PB_PERMIT_PROTO)
                {
                    if ((UINT4 *) (args[25]) != NULL)
                    {
                        u2SubActionId = (UINT2) *(UINT4 *) (args[25]);
                    }

                    u4SubAction = CLI_PTR_TO_U4 (args[24]);
                }

            }
            i4RetStatus =
                AclExtIpFilterConfig
                (CliHandle, i4Action, CLI_PTR_TO_I4 (args[0]),
                 CLI_PTR_TO_U4 (args[1]),
                 u4SrcIpAddr, u4SrcIpMask, CLI_PTR_TO_U4 (args[4]),
                 u4DestIpAddr, u4DestIpMask, CLI_PTR_TO_I4 (args[7]),
                 CLI_PTR_TO_I4 (args[8]), i4Priority,
                 u4SubAction, u2SubActionId);

            if (i4Action == ISS_REDIRECT_TO)
            {
                if (u4Command == CLI_ACL_PERMIT_PROTO)
                {
                    AclRedirectFilterConfig (CliHandle, i4FilterType,
                                             CLI_PTR_TO_I4 (args[11]),
                                             CLI_PTR_TO_I4 (args[12]),
                                             (INT1 *) args[13],
                                             (INT1 *) args[14],
                                             (INT1 *) args[15],
                                             (INT1 *) args[16],
                                             (INT1 *) args[17],
                                             (INT1 *) args[18], 0);
                }
                if (u4Command == CLI_ACL_PB_PERMIT_PROTO)
                {
                    AclRedirectFilterConfig (CliHandle, i4FilterType,
                                             CLI_PTR_TO_I4 (args[16]),
                                             CLI_PTR_TO_I4 (args[17]),
                                             (INT1 *) args[18],
                                             (INT1 *) args[19],
                                             (INT1 *) args[20],
                                             (INT1 *) args[21],
                                             (INT1 *) args[22],
                                             (INT1 *) args[23], 0);
                }
            }

            if (i4RetStatus == CLI_SUCCESS)
            {
                i4RetStatus =
                    AclExtPbL3FilterConfig (CliHandle, i4SVlan,
                                            i4SVlanPrio, i4CVlan,
                                            i4CVlanPrio, i4TagType);
            }

            break;

        case CLI_ACL_PERMIT_IPV6_PROTO:
        case CLI_ACL_DENY_IPV6_PROTO:

            u4SrcPermit = CLI_PTR_TO_U4 (args[0]);
            if (u4SrcPermit == ACL_ANY)
            {
                MEMSET (&SrcIp6Addr, 0, sizeof (tIp6Addr));
            }
            if ((UINT4 *) (args[1]) != NULL)
            {
                MEMSET (&SrcIp6Addr, 0, sizeof (tIp6Addr));
                INET_ATON6 (args[1], &SrcIp6Addr);
            }
            if ((UINT4 *) (args[2]) != NULL)
            {
                u4SrcPrefixLen = *(UINT4 *) (args[2]);
            }
            u4DstPermit = CLI_PTR_TO_U4 (args[3]);
            if (u4DstPermit == ACL_ANY)
            {
                MEMSET (&DstIp6Addr, 0, sizeof (tIp6Addr));
            }
            if ((UINT4 *) (args[4]) != NULL)
            {
                MEMSET (&DstIp6Addr, 0, sizeof (tIp6Addr));
                INET_ATON6 (args[4], &DstIp6Addr);
            }
            if ((UINT4 *) (args[5]) != NULL)
            {
                u4DstPrefixLen = *(UINT4 *) (args[5]);
            }
            if ((UINT4 *) (args[6]) != NULL)
            {
                u4FlowId = CLI_PTR_TO_U4 (args[6]);
            }

            if (u4Command == CLI_ACL_PERMIT_IPV6_PROTO)
            {
                if (CLI_PTR_TO_I4 (args[7]) == ISS_REDIRECT_TO)
                {
                    i4Action = ISS_REDIRECT_TO;
                    i4FilterType = ISS_L3_REDIRECT;
                }
                else
                {
                    i4Action = ISS_ALLOW;
                }
            }
            else if (u4Command == CLI_ACL_DENY_IPV6_PROTO)
            {
                i4Action = ISS_DROP;
                if (args[7] != NULL)
                {
                    u4Priority = *(UINT4 *) args[7];
                }

            }

            if ((UINT4 *) (args[17]) != NULL)
            {
                u2SubActionId = (UINT2) *(UINT4 *) (args[17]);
            }

            u4SubAction = CLI_PTR_TO_U4 (args[16]);

            if (args[18] != NULL)
            {
                u4Priority = *(UINT4 *) args[18];
            }

            i4RetStatus =
                AclExtIp6FilterConfig (CliHandle, u4SrcPermit, SrcIp6Addr,
                                       u4SrcPrefixLen, u4DstPermit, DstIp6Addr,
                                       u4DstPrefixLen, u4FlowId, i4Action,
                                       u4SubAction, u2SubActionId, u4Priority);

            if (i4Action == ISS_REDIRECT_TO)
            {
                AclRedirectFilterConfig (CliHandle, i4FilterType,
                                         CLI_PTR_TO_I4 (args[8]),
                                         CLI_PTR_TO_I4 (args[9]),
                                         (INT1 *) args[10], (INT1 *) args[11],
                                         (INT1 *) args[12], (INT1 *) args[13],
                                         (INT1 *) args[14], (INT1 *) args[15],
                                         0);
            }
            break;

        case CLI_ACL_PERMIT_TCP:
        case CLI_ACL_DENY_TCP:
        case CLI_ACL_PERMIT_UDP:
        case CLI_ACL_DENY_UDP:
        case CLI_ACL_PB_PERMIT_TCP:
        case CLI_ACL_PB_DENY_TCP:
        case CLI_ACL_PB_PERMIT_UDP:
        case CLI_ACL_PB_DENY_UDP:

            if ((u4Command == CLI_ACL_DENY_TCP) ||
                (u4Command == CLI_ACL_DENY_UDP) ||
                (u4Command == CLI_ACL_PB_DENY_TCP) ||
                (u4Command == CLI_ACL_PB_DENY_UDP))
            {
                i4Action = ISS_DROP;
            }
            else if ((u4Command == CLI_ACL_PB_PERMIT_TCP) ||
                     (u4Command == CLI_ACL_PB_PERMIT_UDP))
            {
                i4Action = ISS_ALLOW;
                if (CLI_PTR_TO_I4 (args[21]) == ISS_REDIRECT_TO)
                {
                    i4Action = ISS_REDIRECT_TO;
                    i4FilterType = ISS_L3_REDIRECT;
                }
                if ((UINT4 *) (args[31]) != NULL)
                {
                    u2SubActionId = (UINT2) *(UINT4 *) (args[31]);
                }
                u4SubAction = CLI_PTR_TO_I4 (args[30]);
            }
            else if ((u4Command == CLI_ACL_PERMIT_TCP) ||
                     (u4Command == CLI_ACL_PERMIT_UDP))
            {
                i4Action = ISS_ALLOW;
                if (CLI_PTR_TO_I4 (args[16]) == ISS_REDIRECT_TO)
                {
                    i4Action = ISS_REDIRECT_TO;
                    i4FilterType = ISS_L3_REDIRECT;
                }
                if ((UINT4 *) (args[26]) != NULL)
                {
                    u2SubActionId = (UINT2) *(UINT4 *) (args[26]);
                }
                u4SubAction = CLI_PTR_TO_I4 (args[25]);
            }
            /* Get the protocol type - This can be TCP or UDP only */

            if ((u4Command == CLI_ACL_PERMIT_TCP) ||
                (u4Command == CLI_ACL_DENY_TCP) ||
                (u4Command == CLI_ACL_PB_PERMIT_TCP) ||
                (u4Command == CLI_ACL_PB_DENY_TCP))
            {
                i4Protocol = ISS_PROT_TCP;
            }
            else
            {
                i4Protocol = ISS_PROT_UDP;
            }

            /* Get the source IP,source mask, destination IP and destination mask
             */

            if ((UINT4 *) (args[1]) != NULL)
            {
                u4SrcIpAddr = *(UINT4 *) (args[1]);
            }
            if ((UINT4 *) (args[2]) != NULL)
            {
                u4SrcIpMask = *(UINT4 *) (args[2]);
            }

            if ((UINT4 *) (args[7]) != NULL)
            {
                u4DestIpAddr = *(UINT4 *) (args[7]);
            }
            if ((UINT4 *) (args[8]) != NULL)
            {
                u4DestIpMask = *(UINT4 *) (args[8]);
            }

            u4IsValidSrcPort = FALSE;
            /* Processing for Source ports */

            if (CLI_PTR_TO_U4 (args[3]) == ACL_GREATER_THAN_PORT)
            {
                u4MinSrcPort = (*(UINT4 *) (args[4])) + 1;
                u4MaxSrcPort = ISS_MAX_PORT_VALUE;
                u4IsValidSrcPort = TRUE;
            }
            else if (CLI_PTR_TO_U4 (args[3]) == ACL_LESSER_THAN_PORT)
            {
                u4MinSrcPort = 1;
                u4MaxSrcPort = (*(UINT4 *) (args[4])) - 1;
                u4IsValidSrcPort = TRUE;
            }
            else if (CLI_PTR_TO_U4 (args[3]) == ACL_EQUAL_TO_PORT)
            {
                u4MinSrcPort = *(UINT4 *) (args[4]);
                u4MaxSrcPort = *(UINT4 *) (args[4]);
                u4IsValidSrcPort = TRUE;
            }
            else if (CLI_PTR_TO_U4 (args[3]) == ACL_RANGE_PORT)
            {
                u4MinSrcPort = *(UINT4 *) (args[4]);
                u4MaxSrcPort = *(UINT4 *) (args[5]);
                u4IsValidSrcPort = TRUE;
            }

            /* Processing for destination ports */

            u4IsValidDestPort = FALSE;

            if (CLI_PTR_TO_U4 (args[9]) == ACL_GREATER_THAN_PORT)
            {
                u4MinDstPort = (*(UINT4 *) (args[10])) + 1;
                u4MaxDstPort = ISS_MAX_PORT_VALUE;
                u4IsValidDestPort = TRUE;
            }
            else if (CLI_PTR_TO_U4 (args[9]) == ACL_LESSER_THAN_PORT)
            {
                u4MinDstPort = 1;
                u4MaxDstPort = (*(UINT4 *) (args[10])) - 1;
                u4IsValidDestPort = TRUE;
            }
            else if (CLI_PTR_TO_U4 (args[9]) == ACL_EQUAL_TO_PORT)
            {
                u4MinDstPort = *(UINT4 *) (args[10]);
                u4MaxDstPort = *(UINT4 *) (args[10]);
                u4IsValidDestPort = TRUE;
            }
            else if (CLI_PTR_TO_U4 (args[9]) == ACL_RANGE_PORT)
            {
                u4MinDstPort = *(UINT4 *) (args[10]);
                u4MaxDstPort = *(UINT4 *) (args[11]);
                u4IsValidDestPort = TRUE;
            }

            if ((u4IsValidSrcPort == TRUE) && (u4IsValidDestPort == TRUE))
            {

                CliUnRegisterLock (CliHandle);

                ISS_UNLOCK ();
                CliPrintf (CliHandle, "\r%% Cannot specify both source "
                           " and destination ports\r\n");
                return (CLI_FAILURE);
            }

            /* Processing for RST/ACK BIT */

            if ((UINT4 *) (args[12]) != NULL)
            {
                u4BitType = CLI_PTR_TO_U4 (args[12]);
            }

            i4Tos = CLI_PTR_TO_I4 (args[13]);

            if ((UINT4 *) (args[15]) != NULL)
            {
                i4Priority = *(INT4 *) (args[15]);
            }
            else
            {
                i4Priority = ISS_DEFAULT_FILTER_PRIORITY;
            }

            if ((u4Command == CLI_ACL_PB_PERMIT_TCP) ||
                (u4Command == CLI_ACL_PB_DENY_TCP) ||
                (u4Command == CLI_ACL_PB_PERMIT_UDP) ||
                (u4Command == CLI_ACL_PB_DENY_UDP))
            {
                if ((INT4 *) (args[16]) != NULL)
                {
                    i4SVlan = *(INT4 *) (args[16]);
                }
                if ((INT4 *) (args[17]) != NULL)
                {
                    i4SVlanPrio = *(INT4 *) (args[17]);
                }

                if ((INT4 *) (args[18]) != NULL)
                {
                    i4CVlan = *(INT4 *) (args[18]);
                }
                if ((INT4 *) (args[19]) != NULL)
                {
                    i4CVlanPrio = *(INT4 *) (args[19]);
                }
                i4TagType = CLI_PTR_TO_I4 (args[20]);
            }
            i4RetStatus = AclExtIpFilterTcpUdpConfig
                (CliHandle, i4Action, i4Protocol,
                 CLI_PTR_TO_U4 (args[0]), u4SrcIpAddr,
                 u4SrcIpMask, u4IsValidSrcPort,
                 u4MinSrcPort, u4MaxSrcPort,
                 CLI_PTR_TO_U4 (args[6]), u4DestIpAddr,
                 u4DestIpMask, u4IsValidDestPort,
                 u4MinDstPort, u4MaxDstPort, u4BitType, i4Tos,
                 CLI_PTR_TO_I4 (args[14]), i4Priority,
                 u4SubAction, u2SubActionId);
            if (i4RetStatus == CLI_SUCCESS)
            {
                i4RetStatus =
                    AclExtPbL3FilterConfig (CliHandle, i4SVlan,
                                            i4SVlanPrio, i4CVlan,
                                            i4CVlanPrio, i4TagType);

            }

            if (i4Action == ISS_REDIRECT_TO)
            {
                if ((u4Command == CLI_ACL_PERMIT_TCP) ||
                    (u4Command == CLI_ACL_PERMIT_UDP))
                {
                    AclRedirectFilterConfig (CliHandle, i4FilterType,
                                             CLI_PTR_TO_I4 (args[17]),
                                             CLI_PTR_TO_I4 (args[18]),
                                             (INT1 *) args[19],
                                             (INT1 *) args[20],
                                             (INT1 *) args[21],
                                             (INT1 *) args[22],
                                             (INT1 *) args[23],
                                             (INT1 *) args[24], 0);
                }
                else if ((u4Command == CLI_ACL_PB_PERMIT_TCP) ||
                         (u4Command == CLI_ACL_PB_PERMIT_UDP))
                {
                    AclRedirectFilterConfig (CliHandle, i4FilterType,
                                             CLI_PTR_TO_I4 (args[22]),
                                             CLI_PTR_TO_I4 (args[23]),
                                             (INT1 *) args[24],
                                             (INT1 *) args[25],
                                             (INT1 *) args[26],
                                             (INT1 *) args[27],
                                             (INT1 *) args[28],
                                             (INT1 *) args[29], 0);
                }
            }

            break;

        case CLI_ACL_PERMIT_ICMP:
        case CLI_ACL_DENY_ICMP:
        case CLI_ACL_PB_PERMIT_ICMP:
        case CLI_ACL_PB_DENY_ICMP:

            /* Get the source IP,source mask, destination IP and destination mask
             */
            if ((UINT4 *) (args[1]) != NULL)
            {
                u4SrcIpAddr = *(UINT4 *) (args[1]);
            }
            if ((UINT4 *) (args[2]) != NULL)
            {
                u4SrcIpMask = *(UINT4 *) (args[2]);
            }

            if ((UINT4 *) (args[4]) != NULL)
            {
                u4DestIpAddr = *(UINT4 *) (args[4]);
            }
            if ((UINT4 *) (args[5]) != NULL)
            {
                u4DestIpMask = *(UINT4 *) (args[5]);
            }

            if ((u4Command == CLI_ACL_DENY_ICMP) ||
                (u4Command == CLI_ACL_PB_DENY_ICMP))
            {
                i4Action = ISS_DROP;
            }
            else if (u4Command == CLI_ACL_PERMIT_ICMP)
            {
                i4Action = ISS_ALLOW;
                if (CLI_PTR_TO_I4 (args[9]) == ISS_REDIRECT_TO)
                {
                    i4Action = ISS_REDIRECT_TO;
                    i4FilterType = ISS_L3_REDIRECT;
                }
            }
            else if (u4Command == CLI_ACL_PB_PERMIT_ICMP)
            {
                i4Action = ISS_ALLOW;
                if (CLI_PTR_TO_I4 (args[14]) == ISS_REDIRECT_TO)
                {
                    i4Action = ISS_REDIRECT_TO;
                    i4FilterType = ISS_L3_REDIRECT;
                }
            }

            /* Get the ICMP Message Type and code */

            if ((UINT4 *) (args[6]) != NULL)
            {
                i4MsgType = *(UINT4 *) (args[6]);
            }
            if ((UINT4 *) (args[7]) != NULL)
            {
                i4MsgCode = *(UINT4 *) (args[7]);
            }

            if ((INT4 *) (args[8]) != NULL)
            {
                i4Priority = *(INT4 *) (args[8]);
            }
            else
            {
                i4Priority = ISS_DEFAULT_FILTER_PRIORITY;
            }

            if ((u4Command == CLI_ACL_PB_PERMIT_ICMP) ||
                (u4Command == CLI_ACL_PB_DENY_ICMP))
            {
                if ((INT4 *) (args[9]) != NULL)
                {
                    i4SVlan = *(INT4 *) (args[9]);
                }
                if ((INT4 *) (args[10]) != NULL)
                {
                    i4SVlanPrio = *(INT4 *) (args[10]);
                }

                if ((INT4 *) (args[11]) != NULL)
                {
                    i4CVlan = *(INT4 *) (args[11]);
                }
                if ((INT4 *) (args[12]) != NULL)
                {
                    i4CVlanPrio = *(INT4 *) (args[12]);
                }
                i4TagType = CLI_PTR_TO_I4 (args[13]);
            }
            i4RetStatus =
                AclExtIpFilterIcmpConfig
                (CliHandle, i4Action,
                 CLI_PTR_TO_U4 (args[0]), u4SrcIpAddr, u4SrcIpMask,
                 CLI_PTR_TO_U4 (args[3]), u4DestIpAddr, u4DestIpMask,
                 i4MsgType, i4MsgCode, i4Priority);

            if (i4RetStatus == CLI_SUCCESS)
            {
                i4RetStatus =
                    AclExtPbL3FilterConfig (CliHandle, i4SVlan,
                                            i4SVlanPrio, i4CVlan,
                                            i4CVlanPrio, i4TagType);
            }
            if (i4Action == ISS_REDIRECT_TO)
            {
                if (u4Command == CLI_ACL_PERMIT_ICMP)
                {
                    AclRedirectFilterConfig (CliHandle, i4FilterType,
                                             CLI_PTR_TO_I4 (args[10]),
                                             CLI_PTR_TO_I4 (args[11]),
                                             (INT1 *) args[12],
                                             (INT1 *) args[13],
                                             (INT1 *) args[14],
                                             (INT1 *) args[15],
                                             (INT1 *) args[16],
                                             (INT1 *) args[17], 0);
                }
                else if (u4Command == CLI_ACL_PB_PERMIT_ICMP)
                {
                    AclRedirectFilterConfig (CliHandle, i4FilterType,
                                             CLI_PTR_TO_I4 (args[15]),
                                             CLI_PTR_TO_I4 (args[16]),
                                             (INT1 *) args[17],
                                             (INT1 *) args[18],
                                             (INT1 *) args[19],
                                             (INT1 *) args[20],
                                             (INT1 *) args[21],
                                             (INT1 *) args[22], 0);
                }
            }
            break;

        case CLI_PERMIT_MAC_ACL:
        case CLI_DENY_MAC_ACL:
        case CLI_PB_PERMIT_MAC_ACL:
        case CLI_PB_DENY_MAC_ACL:

            if (u4Command == CLI_PERMIT_MAC_ACL)
            {
                if (CLI_PTR_TO_I4 (args[8]) == ISS_REDIRECT_TO)
                {
                    i4Action = ISS_REDIRECT_TO;
                    i4FilterType = ISS_L2_REDIRECT;
                }
                else
                {
                    i4Action = ISS_ALLOW;
                }
                if ((UINT4 *) (args[18]) != NULL)
                {
                    u2SubActionId = (UINT2) *(UINT4 *) (args[18]);
                }

                u4SubAction = CLI_PTR_TO_U4 (args[17]);
                if ((UINT4 *) (args[20]) != NULL)
                {
                    i4NextFilterId = (INT4) *(UINT4 *) (args[20]);
                    i4NextFilterType = CLI_PTR_TO_I4 (args[19]);
                }

            }
            else if (u4Command == CLI_PB_PERMIT_MAC_ACL)
            {
                i4Action = ISS_ALLOW;
                if (CLI_PTR_TO_I4 (args[13]) == ISS_REDIRECT_TO)
                {
                    i4Action = ISS_REDIRECT_TO;
                    i4FilterType = ISS_L2_REDIRECT;
                }
                if ((UINT4 *) (args[23]) != NULL)
                {
                    u2SubActionId = (UINT2) *(UINT4 *) (args[23]);
                }

                u4SubAction = CLI_PTR_TO_U4 (args[22]);
                if ((UINT4 *) (args[25]) != NULL)
                {
                    i4NextFilterId = (INT4) *(UINT4 *) (args[25]);
                    i4NextFilterType = CLI_PTR_TO_I4 (args[24]);
                }

            }
            if ((u4Command == CLI_DENY_MAC_ACL)
                || (u4Command == CLI_PB_DENY_MAC_ACL))
            {
                i4Action = ISS_DROP;
            }

            /* Get the Source and destination MAC address */

            if ((UINT1 *) (args[1]) != NULL)
            {
                StrToMac ((UINT1 *) args[1], SrcMacAddr);
            }
            if ((UINT1 *) (args[3]) != NULL)
            {
                StrToMac ((UINT1 *) args[3], DestMacAddr);
            }

            /* Get the Ether Type and filter priority */

            if ((INT4 *) (args[5]) != NULL)
            {
                i4EtherType = *(INT4 *) (args[5]);
            }
            if ((INT4 *) (args[6]) != NULL)
            {
                i4CVlan = *(INT4 *) (args[6]);
            }
            if ((INT4 *) (args[7]) != NULL)
            {
                i4Priority = *(INT4 *) (args[7]);
            }
            else
            {
                i4Priority = ISS_DEFAULT_FILTER_PRIORITY;
            }

            if ((u4Command == CLI_PB_PERMIT_MAC_ACL) ||
                (u4Command == CLI_PB_DENY_MAC_ACL))
            {
                if ((INT4 *) (args[8]) != NULL)
                {
                    i4OuterEType = *(INT4 *) (args[8]);
                }
                if ((INT4 *) (args[9]) != NULL)
                {
                    i4SVlan = *(INT4 *) (args[9]);
                }
                if ((INT4 *) (args[10]) != NULL)
                {
                    i4CVlanPrio = *(INT4 *) (args[10]);
                }

                if ((INT4 *) (args[11]) != NULL)
                {
                    i4SVlanPrio = *(INT4 *) (args[11]);
                }
                i4TagType = CLI_PTR_TO_I4 (args[12]);
            }
            i4RetStatus =
                AclExtMacFilterConfig (CliHandle, i4Action,
                                       CLI_PTR_TO_U4 (args[0]),
                                       SrcMacAddr, CLI_PTR_TO_U4 (args[2]),
                                       DestMacAddr, CLI_PTR_TO_I4 (args[4]),
                                       i4EtherType, i4CVlan, i4Priority,
                                       u4SubAction, u2SubActionId,
                                       i4NextFilterType, i4NextFilterId);

            if (i4Action == ISS_REDIRECT_TO)
            {
                if (u4Command == CLI_PERMIT_MAC_ACL)
                {
                    AclRedirectFilterConfig (CliHandle, i4FilterType,
                                             CLI_PTR_TO_I4 (args[9]),
                                             CLI_PTR_TO_I4 (args[10]),
                                             (INT1 *) args[11],
                                             (INT1 *) args[12],
                                             (INT1 *) args[13],
                                             (INT1 *) args[14],
                                             (INT1 *) args[15],
                                             (INT1 *) args[16], 0);
                }
                else if (u4Command == CLI_PB_PERMIT_MAC_ACL)
                {
                    AclRedirectFilterConfig (CliHandle, i4FilterType,
                                             CLI_PTR_TO_I4 (args[14]),
                                             CLI_PTR_TO_I4 (args[15]),
                                             (INT1 *) args[16],
                                             (INT1 *) args[17],
                                             (INT1 *) args[18],
                                             (INT1 *) args[19],
                                             (INT1 *) args[20],
                                             (INT1 *) args[21], 0);
                }
            }

            if (i4RetStatus == CLI_SUCCESS)
            {
                i4RetStatus =
                    AclExtPbL2FilterConfig (CliHandle, i4OuterEType,
                                            i4SVlan, i4SVlanPrio,
                                            i4CVlanPrio, i4TagType);

            }
            break;

        case CLI_ACL_IP_ACCESS_GRP:

            i4RetStatus =
                AclIpAccessGroup (CliHandle, *(INT4 *) (args[0]),
                                  CLI_PTR_TO_I4 (args[1]));
            break;

        case CLI_ACL_IP_NO_ACCESS_GRP:

            if ((INT4 *) (args[0]) != NULL)
            {
                i4FilterNo = (*(INT4 *) (args[0]));
            }
            i4RetStatus =
                AclNoIpAccessGroup (CliHandle, i4FilterNo,
                                    CLI_PTR_TO_I4 (args[1]));
            break;

        case CLI_ACL_MAC_ACCESS_GRP:

            i4RetStatus =
                AclMacExtAccessGroup (CliHandle, *(INT4 *) (args[0]),
                                      CLI_PTR_TO_I4 (args[1]));
            break;

        case CLI_ACL_MAC_NO_ACCESS_GRP:

            if ((INT4 *) (args[0]) != NULL)
            {
                i4FilterNo = (*(INT4 *) (args[0]));
            }
            i4RetStatus =
                AclNoMacExtAccessGroup (CliHandle, i4FilterNo,
                                        CLI_PTR_TO_I4 (args[1]));
            break;

        case CLI_ACL_UDB_ACCESS_GRP:

            i4RetStatus =
                AclUserDefinedAccessGroup (CliHandle, *(INT4 *) (args[0]),
                                           CLI_PTR_TO_I4 (args[1]));
            break;
        case CLI_ACL_UDB_NO_ACCESS_GRP:

            if ((INT4 *) (args[0]) != NULL)
            {
                i4FilterNo = (*(INT4 *) (args[0]));
            }
            i4RetStatus =
                AclNoUserDefinedAccessGroup (CliHandle, i4FilterNo,
                                             ACL_ACCESS_IN);
            break;

        case CLI_ACL_SHOW:
            i4FilterType = CLI_PTR_TO_I4 (args[0]);
            if ((INT4 *) (args[1]) != NULL)
            {
                i4FilterNo = (*(INT4 *) (args[1]));
            }
            i4RetStatus =
                AclShowAccessLists (CliHandle, i4FilterType, i4FilterNo);
            break;

    }

    if ((CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_ACL_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", AclCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    CliUnRegisterLock (CliHandle);

    ISS_UNLOCK ();

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclCreateIPFilter                                  */
/*                                                                           */
/*     DESCRIPTION      : This function creates an IP ACL filter and enters  */
/*                        the corresponding configuration mode               */
/*                                                                           */
/*     INPUT            : u4Type  - Standard /Extended ACL                   */
/*                        i4Filterno - IP ACL number                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclCreateIPFilter (tCliHandle CliHandle, UINT4 u4Type, INT4 i4FilterNo)
{
    INT4                i4Status = 0;
    UINT4               u4ErrCode;
    UINT1               au1IfName[ACL_MAX_NAME_LENGTH];

    /* if filter already exists, do nothing and enter the configuration mode */

    if (nmhGetIssExtL3FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        /* Create a filter */
        if (nmhTestv2IssExtL3FilterStatus (&u4ErrCode,
                                           i4FilterNo,
                                           ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }

    /* Enter IP ACL configuration mode */

    if (u4Type == ACL_STANDARD)
    {
        /* Set the mode information for CLI */
        CLI_SET_STDACLID (i4FilterNo);
        SNPRINTF ((CHR1 *) au1IfName, ACL_MAX_NAME_LENGTH,
                  "%s%d", CLI_STDACL_MODE, i4FilterNo);
    }

    else

    {
        /* Set the mode information for CLI */
        CLI_SET_EXTACL (i4FilterNo);
        SNPRINTF ((CHR1 *) au1IfName, ACL_MAX_NAME_LENGTH,
                  "%s%d", CLI_EXTACL_MODE, i4FilterNo);
    }

    /* Return ACL configuration prompt */
    CliChangePath ((CHR1 *) au1IfName);

    UNUSED_PARAM (CliHandle);
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclDestroyIPFilter                                 */
/*                                                                           */
/*     DESCRIPTION      : This function destroys a IP ACL filter and enters  */
/*                        the corresponding configuration mode               */
/*                                                                           */
/*     INPUT            : i4Filterno - IP ACL number                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclDestroyIPFilter (tCliHandle CliHandle, INT4 i4FilterNo)
{
    INT4                i4Status;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;
    UINT4               u4RedirectGrpId = 0;
    UINT4               u4ErrCode;

    if (nmhGetIssExtL3FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        /* Filter does not exist */
        CliPrintf (CliHandle, "\r%% Invalid Filter number \r\n");
        return (CLI_FAILURE);
    }

    else

    {
        if (nmhGetIssExtL3FilterAction (i4FilterNo, &i4ActionOld) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (i4ActionOld == ISS_REDIRECT_TO)
        {
            nmhGetIssAclL3FilterRedirectId (i4FilterNo,
                                            (INT4 *) &u4RedirectGrpId);

            if (u4RedirectGrpId != 0)
            {
                if (nmhGetIssRedirectInterfaceGrpStatus (u4RedirectGrpId,
                                                         &i4RedirectStatus) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }

                if (i4RedirectStatus != 0)
                {
                    if (nmhSetIssRedirectInterfaceGrpStatus
                        (u4RedirectGrpId, ISS_DESTROY) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return (CLI_FAILURE);
                    }
                }
            }
        }
        if (nmhTestv2IssExtL3FilterStatus (&u4ErrCode,
                                           i4FilterNo,
                                           ISS_DESTROY) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclCreateMacFilter                                 */
/*                                                                           */
/*     DESCRIPTION      : This function creates an MAC ACL filter and enters */
/*                        the corresponding configuration mode               */
/*                                                                           */
/*     INPUT            : i4Filterno - MAC ACL number                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclCreateMacFilter (tCliHandle CliHandle, INT4 i4FilterNo)
{
    INT4                i4Status;
    UINT4               u4ErrCode;
    UINT1               au1IfName[ACL_MAX_NAME_LENGTH];

    /* if filter already exists, do nothing and enter the configuration mode */

    if (nmhGetIssExtL2FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        /* Create a filter */
        if (nmhTestv2IssExtL2FilterStatus (&u4ErrCode,
                                           i4FilterNo,
                                           ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }

    /* Enter MAC ACL configuration mode */

    /* Set the mode information for CLI */
    CLI_SET_MACACL (i4FilterNo);
    SNPRINTF ((CHR1 *) au1IfName, ACL_MAX_NAME_LENGTH, "%s%d",
              CLI_MACACL_MODE, i4FilterNo);
    /* Return ACL configuration prompt */
    CliChangePath ((CHR1 *) au1IfName);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclDestroyMacFilter                                */
/*                                                                           */
/*     DESCRIPTION      : This function destroys a MAC ACL filter and enters */
/*                        the corresponding configuration mode               */
/*                                                                           */
/*     INPUT            : i4Filterno - MAC ACL number                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclDestroyMacFilter (tCliHandle CliHandle, INT4 i4FilterNo)
{
    INT4                i4Status;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;
    UINT4               u4RedirectGrpId = 0;
    UINT4               u4ErrCode;

    if (nmhGetIssExtL2FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        /* Filter does not exist */
        CliPrintf (CliHandle, "\r%% Invalid Filter number \r\n");
        return (CLI_FAILURE);
    }

    else

    {
        if (nmhTestv2IssExtL2FilterStatus (&u4ErrCode,
                                           i4FilterNo,
                                           ISS_DESTROY) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhGetIssExtL2FilterAction (i4FilterNo, &i4ActionOld) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (i4ActionOld == ISS_REDIRECT_TO)
        {
            nmhGetIssAclL2FilterRedirectId (i4FilterNo,
                                            (INT4 *) &u4RedirectGrpId);
            if (u4RedirectGrpId != 0)
            {

                if (nmhGetIssRedirectInterfaceGrpStatus (u4RedirectGrpId,
                                                         &i4RedirectStatus) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }

                if (i4RedirectStatus != 0)
                {
                    if (nmhSetIssRedirectInterfaceGrpStatus
                        (u4RedirectGrpId, ISS_DESTROY) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return (CLI_FAILURE);
                    }
                }
            }
        }
        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclDestroyUserDefFilter                            */
/*                                                                           */
/*     DESCRIPTION      : This function destroys a MAC ACL filter and enters */
/*                        the corresponding configuration mode               */
/*                                                                           */
/*     INPUT            : i4Filterno - MAC ACL number                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclDestroyUserDefFilter (tCliHandle CliHandle, INT4 i4FilterNo)
{
    INT4                i4Status;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;
    UINT4               u4RedirectGrpId = 0;
    UINT4               u4ErrCode;

    if (nmhGetIssAclUserDefinedFilterStatus (i4FilterNo, &i4Status) ==
        SNMP_FAILURE)
    {
        /* Filter does not exist */
        CliPrintf (CliHandle, "\r%% Invalid Filter number \r\n");
        return (CLI_FAILURE);
    }
    else
    {

        if (nmhGetIssAclUserDefinedFilterAction (i4FilterNo, &i4ActionOld) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (i4ActionOld == ISS_REDIRECT_TO)
        {
            nmhGetIssAclUserDefinedFilterRedirectId (i4FilterNo,
                                                     (INT4 *) &u4RedirectGrpId);

            if (u4RedirectGrpId != 0)
            {

                if (nmhGetIssRedirectInterfaceGrpStatus
                    (u4RedirectGrpId, &i4RedirectStatus) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }

                if (i4RedirectStatus != 0)
                {
                    if (nmhSetIssRedirectInterfaceGrpStatus
                        (u4RedirectGrpId, ISS_DESTROY) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return (CLI_FAILURE);
                    }
                }
            }
        }
        if (nmhTestv2IssAclUserDefinedFilterStatus (&u4ErrCode,
                                                    i4FilterNo,
                                                    ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhSetIssAclUserDefinedFilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclCreateUserDefFilter                             */
/*                                                                           */
/*     DESCRIPTION      : This function creates an MAC ACL filter and enters */
/*                        the corresponding configuration mode               */
/*                                                                           */
/*     INPUT            : i4Filterno - MAC ACL number                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclCreateUserDefFilter (tCliHandle CliHandle, INT4 i4FilterNo)
{
    INT4                i4Status;
    UINT4               u4ErrCode;
    UINT1               au1IfName[ACL_MAX_NAME_LENGTH];

    if (nmhGetIssAclUserDefinedFilterStatus (i4FilterNo, &i4Status) ==
        SNMP_FAILURE)
    {
        if (nmhTestv2IssAclUserDefinedFilterStatus (&u4ErrCode,
                                                    i4FilterNo,
                                                    ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhSetIssAclUserDefinedFilterStatus
            (i4FilterNo, ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    CLI_SET_USERDEFFACL (i4FilterNo);
    SNPRINTF ((CHR1 *) au1IfName, ACL_MAX_NAME_LENGTH, "%s%d",
              CLI_USRDEFACL_MODE, i4FilterNo);
    CliChangePath ((CHR1 *) au1IfName);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclStdIpFilterConfig                               */
/*                                                                           */
/*     DESCRIPTION      : This function configures a standard IP ACL filter  */
/*                         and enters the corresponding configuration mode   */
/*                                                                           */
/*     INPUT            :  u4Action -Permit/Deny                             */
/*                         u4SrcType - any/source network/ source host       */
/*                         u4SrcIp - Source IP Address                       */
/*                         u4SrcMask - Source IP Mask                        */
/*                         u4DestType - any/dest network/dest host           */
/*                         u4DestIp - Dest IP Address                        */
/*                         u4DestMask - Dest IP Mask                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclStdIpFilterConfig (tCliHandle CliHandle, INT4 i4Action, UINT4 u4SrcType,
                      UINT4 u4SrcIpAddr, UINT4 u4SrcMask, UINT4 u4DestType,
                      UINT4 u4DestIpAddr, UINT4 u4DestMask, UINT4 u4SubAction,
                      UINT2 u2VlanActonId, UINT4 u4Priority)
{
    INT4                i4FilterNo;
    INT4                i4Status;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;
    UINT4               u4RedirectGrpId = 0;
    UINT4               u4ErrCode;

    /*Get the IP Access list number from the current mode in CLI */
    i4FilterNo = CLI_GET_STDACLID ();
    if (nmhGetIssExtL3FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (i4Status != ISS_NOT_READY)
    {
        /* Filter has previously been configured. This must be over-written 
         * First destroy the filter and create again 
         */
        if (nmhGetIssExtL3FilterAction (i4FilterNo, &i4ActionOld) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (i4ActionOld == ISS_REDIRECT_TO)
        {
            nmhGetIssAclL3FilterRedirectId (i4FilterNo,
                                            (INT4 *) &u4RedirectGrpId);
            if (u4RedirectGrpId != 0)
            {
                if (nmhGetIssRedirectInterfaceGrpStatus (u4RedirectGrpId,
                                                         &i4RedirectStatus) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }
                if (i4RedirectStatus != 0)
                {
                    if (nmhSetIssRedirectInterfaceGrpStatus
                        (u4RedirectGrpId, ISS_DESTROY) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return (CLI_FAILURE);
                    }
                }
            }
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }

    if (nmhTestv2IssExtL3FilterAction (&u4ErrCode,
                                       i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /*Test the user input for destination IP and mask */
    if (AclTestIpParams (ACL_SRC, i4FilterNo, u4SrcType, u4SrcIpAddr, u4SrcMask)
        == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /*Test the user input for destination IP and mask */
    if (AclTestIpParams
        (ACL_DST, i4FilterNo, u4DestType, u4DestIpAddr,
         u4DestMask) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (AclSetIpParams (ACL_SRC, i4FilterNo, u4SrcType, u4SrcIpAddr, u4SrcMask)
        == CLI_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (AclSetIpParams
        (ACL_DST, i4FilterNo, u4DestType, u4DestIpAddr,
         u4DestMask) == CLI_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    /* Set the Vlan Id and Action */

    if (nmhTestv2IssAclL3FilterSubAction (&u4ErrCode,
                                          i4FilterNo, (INT4) u4SubAction)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssAclL3FilterSubAction (i4FilterNo, (INT4) u4SubAction) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssAclL3FilterSubActionId (&u4ErrCode,
                                            i4FilterNo,
                                            (INT4) u2VlanActonId)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssAclL3FilterSubActionId (i4FilterNo,
                                         (INT4) u2VlanActonId) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhTestv2IssAclL3FilterPriority (&u4ErrCode,
                                         i4FilterNo,
                                         u4Priority) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssAclL3FilterPriority (i4FilterNo, u4Priority) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclUserDefinedOperations                           */
/*                                                                           */
/*     DESCRIPTION      : This function does AND/OR/NOT of filters           */
/*                                                                           */
/*                                                                           */
/*     INPUT            :  i4FilterOper - Type of Filter Operation           */
/*                         u4Filter1Id - Filter 1 Id                         */
/*                         u4Filter2Id - Filter 2 Id                         */
/*                         u4Filter3Id - Filter 3 Id                         */
/*                         u4Priority  - Priority                            */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclUserDefinedOperations (tCliHandle CliHandle, INT4 i4FilterOper, UINT4
                          u4Filter1Id, UINT4 u4Filter2Id, UINT4 u4Filter3Id,
                          UINT4 u4Priority)
{
    INT4                i4FilteOneType = 0;
    INT4                i4FilterTwoType = 0;
    INT4                i4Action = 0;
    INT4                i4Status = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4FilterNo = 0;

    u4FilterNo = CLI_GET_STDACLID ();

    if (nmhGetIssAclUserDefinedFilterStatus (u4FilterNo, &i4Status) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (i4Status == ISS_ACTIVE)
    {
        if (nmhSetIssAclUserDefinedFilterStatus (u4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhSetIssAclUserDefinedFilterStatus
            (u4FilterNo, ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4FilterOper == ISS_USERDEFINED_L3FILTER_APPLY_AND_OP)
    {
        i4FilteOneType = ISS_L3_FILTER;
        i4FilterTwoType = ISS_L3_FILTER;
        i4Action = ISS_AND;
    }

    if (i4FilterOper == ISS_USERDEFINED_L3FILTER_APPLY_OR_OP)
    {
        i4FilteOneType = ISS_L3_FILTER;
        i4FilterTwoType = ISS_L3_FILTER;
        i4Action = ISS_OR;
    }
    if (i4FilterOper == ISS_USERDEFINED_L2FILTER_APPLY_AND_OP)
    {
        i4FilteOneType = ISS_L2_FILTER;
        i4FilterTwoType = ISS_L2_FILTER;
        i4Action = ISS_AND;
    }
    if (i4FilterOper == ISS_USERDEFINED_L2L3FILTER_APPLY_AND_OP)
    {
        i4FilteOneType = ISS_L2_FILTER;
        i4FilterTwoType = ISS_L3_FILTER;
        i4Action = ISS_AND;

    }
    if (i4FilterOper == ISS_USERDEFINED_L2FILTER_APPLY_OR_OP)
    {
        i4FilteOneType = ISS_L2_FILTER;
        i4FilterTwoType = ISS_L2_FILTER;
        i4Action = ISS_OR;
    }
    if (i4FilterOper == ISS_USERDEFINED_L2L3FILTER_APPLY_OR_OP)
    {
        i4FilteOneType = ISS_L2_FILTER;
        i4FilterTwoType = ISS_L3_FILTER;
        i4Action = ISS_OR;
    }
    if (i4FilterOper == ISS_USERDEFINED_L2FILTER_APPLY_NOT_OP)
    {
        i4FilteOneType = ISS_L2_FILTER;
        i4Action = ISS_NOT;
        u4Filter1Id = u4Filter3Id;
    }
    if (i4FilterOper == ISS_USERDEFINED_L3FILTER_APPLY_NOT_OP)
    {
        i4FilteOneType = ISS_L3_FILTER;
        i4Action = ISS_NOT;
        u4Filter1Id = u4Filter3Id;
    }
    if (nmhTestv2IssAclUserDefinedFilterAction (&u4ErrCode,
                                                u4FilterNo,
                                                i4Action) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIssAclUserDefinedFilterAction (u4FilterNo, i4Action)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssAclUserDefinedFilterIdOneType (&u4ErrCode,
                                                   u4FilterNo,
                                                   i4FilteOneType) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIssAclUserDefinedFilterIdOneType (u4FilterNo, i4FilteOneType)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhTestv2IssAclUserDefinedFilterIdOne (&u4ErrCode,
                                               u4FilterNo,
                                               u4Filter1Id) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIssAclUserDefinedFilterIdOne (u4FilterNo, u4Filter1Id)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhTestv2IssAclUserDefinedFilterIdTwoType (&u4ErrCode,
                                                   u4FilterNo,
                                                   i4FilterTwoType) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIssAclUserDefinedFilterIdTwoType (u4FilterNo, i4FilterTwoType)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhTestv2IssAclUserDefinedFilterIdTwo (&u4ErrCode,
                                               u4FilterNo,
                                               u4Filter2Id) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIssAclUserDefinedFilterIdTwo (u4FilterNo, u4Filter2Id)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);

    }

    if (nmhTestv2IssAclUserDefinedFilterPriority (&u4ErrCode,
                                                  u4FilterNo,
                                                  u4Priority) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIssAclUserDefinedFilterPriority (u4FilterNo, u4Priority)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);

    }

    if (nmhTestv2IssAclUserDefinedFilterStatus (&u4ErrCode, u4FilterNo,
                                                ISS_ACTIVE) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIssAclUserDefinedFilterStatus (u4FilterNo, ISS_ACTIVE) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclUserDefinedFilterConfig                         */
/*                                                                           */
/*     DESCRIPTION      : This function configures a User Defined filter     */
/*                         and enters the corresponding configuration mode   */
/*                                                                           */
/*     INPUT            :  u4Action -Permit/Deny                             */
/*                         u4PktType - Type of Packet                        */
/*                         u2PktStart - Base offset                          */
/*                u2offset1Pos - Offset1 Position             */
/*               u2offset1Val - Offset1 Value                      */
/*                         u2offset2Pos - Offset2 Position                   */
/*                         u2offset2Val - Offset2 Value                      */
/*                         u2offset3Pos - Offset3 Position                   */
/*                         u2offset3Val - Offset3 Value                      */
/*                         u2offset4Pos - Offset4 Position                   */
/*                         u2offset4Val - Offset4 Value                      */
/*                         u2offset5Pos - Offset5 Position                   */
/*                         u2offset5Val - Offset5 Value                      */
/*                         u2offset6Pos - Offset6 Position                   */
/*                         u2offset6Val - Offset7 Value                      */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclUserDefinedFilterConfig (tCliHandle CliHandle, INT4 i4Action,
                            UINT4 u4PktType, UINT2 u2PktStart,
                            UINT2 u2Offset1Pos, UINT2 u2Offset1Val,
                            UINT2 u2Offset2Pos, UINT2 u2Offset2Val,
                            UINT2 u2Offset3Pos, UINT2 u2Offset3Val,
                            UINT2 u2Offset4Pos, UINT2 u2Offset4Val,
                            UINT2 u2Offset5Pos, UINT2 u2Offset5Val,
                            UINT2 u2Offset6Pos, UINT2 u2Offset6Val,
                            UINT4 u4SubAction, UINT2 u2SubActionId,
                            UINT4 u4Priority)
{
    INT4                i4FilterNo = 0;
    INT4                i4Status = 0;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4RedirectGrpId = 0;
    UINT1               au1OffsetValue[ISS_UDB_MAX_OFFSET];
    UINT1               au1OffsetMask[ISS_UDB_MAX_OFFSET];
    tSNMP_OCTET_STRING_TYPE OffsetValue;
    tSNMP_OCTET_STRING_TYPE OffsetMask;

    ISS_MEMSET (&OffsetValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    ISS_MEMSET (&OffsetMask, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    ISS_MEMSET (au1OffsetValue, 0, ISS_UDB_MAX_OFFSET);
    ISS_MEMSET (au1OffsetMask, 0, ISS_UDB_MAX_OFFSET);

    OffsetValue.pu1_OctetList = au1OffsetValue;
    OffsetValue.i4_Length = ISS_UDB_MAX_OFFSET;
    OffsetMask.pu1_OctetList = au1OffsetMask;
    OffsetMask.i4_Length = ISS_UDB_MAX_OFFSET;

    i4FilterNo = CLI_GET_STDACLID ();
    if (nmhGetIssAclUserDefinedFilterStatus (i4FilterNo, &i4Status) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (i4Status != ISS_NOT_READY)
    {
        /* Filter has previously been configured. This must be over-written 
         * First destroy the filter and create again 
         */
        if (nmhGetIssAclUserDefinedFilterAction (i4FilterNo, &i4ActionOld) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (i4ActionOld == ISS_REDIRECT_TO)
        {

            nmhGetIssAclUserDefinedFilterRedirectId (i4FilterNo,
                                                     (INT4 *) &u4RedirectGrpId);
            if (u4RedirectGrpId != 0)
            {

                if (nmhGetIssRedirectInterfaceGrpStatus
                    (u4RedirectGrpId, &i4RedirectStatus) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }

                if (i4RedirectStatus != 0)
                {
                    if (nmhSetIssRedirectInterfaceGrpStatus
                        (u4RedirectGrpId, ISS_DESTROY) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return (CLI_FAILURE);
                    }
                }
            }
        }

        if (nmhSetIssAclUserDefinedFilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhSetIssAclUserDefinedFilterStatus
            (i4FilterNo, ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }
    if (u2Offset1Pos != 0xFFFF)
    {
        au1OffsetValue[u2Offset1Pos] |= u2Offset1Val;
        au1OffsetMask[u2Offset1Pos] |= 0xFF;
    }
    if (u2Offset2Pos != 0xFFFF)
    {
        au1OffsetValue[u2Offset2Pos] |= u2Offset2Val;
        au1OffsetMask[u2Offset2Pos] |= 0xFF;
    }

    if (u2Offset3Pos != 0xFFFF)
    {
        au1OffsetValue[u2Offset3Pos] |= u2Offset3Val;
        au1OffsetMask[u2Offset3Pos] |= 0xFF;
    }
    if (u2Offset4Pos != 0xFFFF)
    {
        au1OffsetValue[u2Offset4Pos] |= u2Offset4Val;
        au1OffsetMask[u2Offset4Pos] |= 0xFF;
    }
    if (u2Offset5Pos != 0xFFFF)
    {
        au1OffsetValue[u2Offset5Pos] |= u2Offset5Val;
        au1OffsetMask[u2Offset5Pos] |= 0xFF;
    }
    if (u2Offset6Pos != 0xFFFF)
    {
        au1OffsetValue[u2Offset6Pos] |= u2Offset6Val;
        au1OffsetMask[u2Offset6Pos] |= 0xFF;
    }
    if (nmhTestv2IssAclUserDefinedFilterAction (&u4ErrCode,
                                                i4FilterNo,
                                                i4Action) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIssAclUserDefinedFilterAction (i4FilterNo, i4Action)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhTestv2IssAclUserDefinedFilterPktType (&u4ErrCode, i4FilterNo,
                                                 u4PktType) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIssAclUserDefinedFilterPktType (i4FilterNo, u4PktType) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhTestv2IssAclUserDefinedFilterOffSetBase (&u4ErrCode, i4FilterNo,
                                                    u2PktStart) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIssAclUserDefinedFilterOffSetBase (i4FilterNo,
                                                 u2PktStart) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhTestv2IssAclUserDefinedFilterOffSetValue (&u4ErrCode, i4FilterNo,
                                                     (tSNMP_OCTET_STRING_TYPE
                                                      *) (VOID *) &OffsetValue)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIssAclUserDefinedFilterOffSetValue (i4FilterNo,
                                                  (tSNMP_OCTET_STRING_TYPE
                                                   *) (VOID *) &OffsetValue)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhTestv2IssAclUserDefinedFilterOffSetMask (&u4ErrCode, i4FilterNo,
                                                    (tSNMP_OCTET_STRING_TYPE
                                                     *) (VOID *) &OffsetMask)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIssAclUserDefinedFilterOffSetMask (i4FilterNo,
                                                 (tSNMP_OCTET_STRING_TYPE
                                                  *) (VOID *) &OffsetMask)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /* Set the Vlan Id and Action */

    if (nmhTestv2IssAclUserDefinedFilterSubAction (&u4ErrCode,
                                                   i4FilterNo,
                                                   (INT4) u4SubAction)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssAclUserDefinedFilterSubAction
        (i4FilterNo, (INT4) u4SubAction) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssAclUserDefinedFilterSubActionId (&u4ErrCode,
                                                     i4FilterNo,
                                                     (INT4) u2SubActionId)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssAclUserDefinedFilterSubActionId (i4FilterNo,
                                                  (INT4) u2SubActionId) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhTestv2IssAclUserDefinedFilterPriority (&u4ErrCode,
                                                  i4FilterNo,
                                                  u4Priority) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIssAclUserDefinedFilterPriority (i4FilterNo, u4Priority)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);

    }
    if (nmhSetIssAclUserDefinedFilterStatus (i4FilterNo, ISS_ACTIVE)
        == SNMP_FAILURE)
    {
        nmhSetIssAclUserDefinedFilterStatus (i4FilterNo, ISS_DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtIp6FilterConfig                               */
/*                                                                           */
/*     DESCRIPTION      : This function configures a standard IP ACL filter  */
/*                         and enters the corresponding configuration mode   */
/*                                                                           */
/*     INPUT            :  u4Action -Permit/Deny                             */
/*                         u4SrcType - any/source network/ source host       */
/*                         u4SrcIp - Source IP Address                       */
/*                         u4SrcMask - Source IP Mask                        */
/*                         u4DestType - any/dest network/dest host           */
/*                         u4DestIp - Dest IP Address                        */
/*                         u4DestMask - Dest IP Mask                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclExtIp6FilterConfig (tCliHandle CliHandle, UINT4 u4SrcIpAddr,
                       tIp6Addr SrcIp6Addr, UINT4 u4SrcIpMask,
                       UINT4 u4DestIpAddr, tIp6Addr DstIp6Addr,
                       UINT4 u4DestIpMask, UINT4 u4FlowId, INT4 i4Action,
                       UINT4 u4SubAction, UINT2 u2SubActionId, UINT4 u4Priority)
{

    INT4                i4FilterNo = 0;
    INT4                i4Status = 0;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;
    UINT4               u4ErrorCode;
    UINT4               u4RedirectGrpId = 0;
    UINT4               u4ErrCode = 0;
    tSNMP_OCTET_STRING_TYPE SrcIpv6;
    tSNMP_OCTET_STRING_TYPE DstIpv6;
    UNUSED_PARAM (i4Action);
    i4FilterNo = CLI_GET_EXTACL ();
    nmhGetIssAclL3FilterStatus (i4FilterNo, &i4Status);

    if (i4Status != ISS_NOT_READY)
    {

        if (nmhGetIssExtL3FilterAction (i4FilterNo, &i4ActionOld) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (i4ActionOld == ISS_REDIRECT_TO)
        {
            nmhGetIssAclL3FilterRedirectId (i4FilterNo,
                                            (INT4 *) &u4RedirectGrpId);
            if (u4RedirectGrpId != 0)
            {

                if (nmhGetIssRedirectInterfaceGrpStatus (u4RedirectGrpId,
                                                         &i4RedirectStatus) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }

                if (i4RedirectStatus != 0)
                {
                    if (nmhSetIssRedirectInterfaceGrpStatus
                        (u4RedirectGrpId, ISS_DESTROY) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return (CLI_FAILURE);
                    }
                }
            }
        }
        /* Filter has previously been configured. This must be over-written 
         * First destroy the filter and create again */

        if (nmhSetIssAclL3FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhSetIssAclL3FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }
    nmhSetIssAclL3FilteAddrType (i4FilterNo, QOS_IPV6);
    nmhSetIssAclL3FilterProtocol (i4FilterNo, ISS_PROT_ANY);

    if (u4SrcIpAddr != ACL_ANY)
    {
        /*Used diffserv since we did not have mIB for fsqos */
        nmhSetIssAclL3FilterFlowId (i4FilterNo, 0);

        SrcIpv6.pu1_OctetList = MEM_MALLOC (sizeof (tIp6Addr), UINT1);

        if (SrcIpv6.pu1_OctetList == NULL)
        {
            return CLI_FAILURE;
        }

        MEMCPY (SrcIpv6.pu1_OctetList, &SrcIp6Addr, IP6_ADDR_SIZE);
        SrcIpv6.i4_Length = IP6_ADDR_SIZE;

        if (nmhTestv2IssAclL3FilterSrcIpAddr
            (&u4ErrorCode, i4FilterNo, &SrcIpv6) == SNMP_FAILURE)
        {
            MEM_FREE ((UINT1 *) SrcIpv6.pu1_OctetList);
            return CLI_FAILURE;
        }

        if (nmhSetIssAclL3FilterSrcIpAddr (i4FilterNo, &SrcIpv6) ==
            SNMP_FAILURE)
        {
            MEM_FREE ((UINT1 *) SrcIpv6.pu1_OctetList);
            return CLI_FAILURE;
        }
        if (nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength (&u4ErrorCode,
                                                          i4FilterNo,
                                                          u4SrcIpMask)
            == SNMP_FAILURE)
        {
            MEM_FREE ((UINT1 *) SrcIpv6.pu1_OctetList);
            return CLI_FAILURE;
        }
        if (nmhSetIssAclL3FilterSrcIpAddrPrefixLength (i4FilterNo,
                                                       u4SrcIpMask) ==
            SNMP_FAILURE)
        {
            MEM_FREE ((UINT1 *) SrcIpv6.pu1_OctetList);
            return CLI_FAILURE;
        }

        MEM_FREE ((UINT1 *) SrcIpv6.pu1_OctetList);
    }

    if (u4DestIpAddr != ACL_ANY)
    {
        /*Used diffserv since we did not have mIB for fsqos */

        nmhSetIssAclL3FilterFlowId (i4FilterNo, 0);
        DstIpv6.pu1_OctetList = MEM_MALLOC (sizeof (tIp6Addr), UINT1);

        if (DstIpv6.pu1_OctetList == NULL)
        {
            return CLI_FAILURE;
        }

        MEMCPY (DstIpv6.pu1_OctetList, &DstIp6Addr, IP6_ADDR_SIZE);
        DstIpv6.i4_Length = IP6_ADDR_SIZE;

        if (nmhTestv2IssAclL3FilterDstIpAddr
            (&u4ErrorCode, i4FilterNo, &DstIpv6) == SNMP_FAILURE)
        {
            MEM_FREE ((UINT1 *) DstIpv6.pu1_OctetList);
            return CLI_FAILURE;
        }

        if (nmhSetIssAclL3FilterDstIpAddr (i4FilterNo, &DstIpv6) ==
            SNMP_FAILURE)
        {
            MEM_FREE ((UINT1 *) DstIpv6.pu1_OctetList);
            return CLI_FAILURE;
        }
        if (nmhTestv2IssAclL3FilterDstIpAddrPrefixLength (&u4ErrorCode,
                                                          i4FilterNo,
                                                          u4DestIpMask)
            == SNMP_FAILURE)
        {
            MEM_FREE ((UINT1 *) DstIpv6.pu1_OctetList);
            return CLI_FAILURE;
        }
        if (nmhSetIssAclL3FilterDstIpAddrPrefixLength (i4FilterNo,
                                                       u4DestIpMask) ==
            SNMP_FAILURE)
        {
            MEM_FREE ((UINT1 *) DstIpv6.pu1_OctetList);
            return CLI_FAILURE;
        }

        MEM_FREE ((UINT1 *) DstIpv6.pu1_OctetList);
    }

    if (u4FlowId != 0)
    {
        SrcIpv6.pu1_OctetList = MEM_MALLOC (sizeof (tIp6Addr), UINT1);
        if (SrcIpv6.pu1_OctetList == NULL)
        {
            return CLI_FAILURE;
        }
        MEMSET (SrcIpv6.pu1_OctetList, 0, IP6_ADDR_SIZE);
        SrcIpv6.i4_Length = IP6_ADDR_SIZE;
        if (nmhSetIssAclL3FilterSrcIpAddr (i4FilterNo, &SrcIpv6) ==
            SNMP_FAILURE)
        {
            MEM_FREE ((UINT1 *) SrcIpv6.pu1_OctetList);
            return CLI_FAILURE;
        }
        DstIpv6.pu1_OctetList = MEM_MALLOC (sizeof (tIp6Addr), UINT1);

        if (DstIpv6.pu1_OctetList == NULL)
        {
            MEM_FREE ((UINT1 *) SrcIpv6.pu1_OctetList);
            return CLI_FAILURE;
        }

        MEMSET (DstIpv6.pu1_OctetList, 0, IP6_ADDR_SIZE);
        DstIpv6.i4_Length = IP6_ADDR_SIZE;
        if (nmhSetIssAclL3FilterDstIpAddr (i4FilterNo, &DstIpv6) ==
            SNMP_FAILURE)
        {
            MEM_FREE ((UINT1 *) SrcIpv6.pu1_OctetList);
            MEM_FREE ((UINT1 *) DstIpv6.pu1_OctetList);
            return CLI_FAILURE;
        }
        /*Before need to remove the Ipv6Address configurations */
        if (nmhTestv2IssAclL3FilterFlowId (&u4ErrorCode, i4FilterNo, u4FlowId)
            == SNMP_FAILURE)
        {
            MEM_FREE ((UINT1 *) SrcIpv6.pu1_OctetList);
            MEM_FREE ((UINT1 *) DstIpv6.pu1_OctetList);
            return CLI_FAILURE;
        }

        if (nmhSetIssAclL3FilterFlowId (i4FilterNo, u4FlowId) == SNMP_FAILURE)
        {
            MEM_FREE ((UINT1 *) SrcIpv6.pu1_OctetList);
            MEM_FREE ((UINT1 *) DstIpv6.pu1_OctetList);
            CLI_SET_ERR (CLI_ACL_SRC_DST_IP_ADDR_CONF_ERR);
            return CLI_FAILURE;
        }

        MEM_FREE ((UINT1 *) SrcIpv6.pu1_OctetList);
        MEM_FREE ((UINT1 *) DstIpv6.pu1_OctetList);
    }

    /* Set the action (permit/deny) accorring to the user config value */
    if (nmhTestv2IssExtL3FilterAction (&u4ErrorCode,
                                       i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    /* Set the Vlan Id and Action */

    if (nmhTestv2IssAclL3FilterSubAction (&u4ErrorCode,
                                          i4FilterNo, (INT4) u4SubAction)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssAclL3FilterSubAction (i4FilterNo, (INT4) u4SubAction) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssAclL3FilterSubActionId (&u4ErrorCode,
                                            i4FilterNo,
                                            (INT4) u2SubActionId)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssAclL3FilterSubActionId (i4FilterNo,
                                         (INT4) u2SubActionId) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhTestv2IssAclL3FilterPriority (&u4ErrCode,
                                         i4FilterNo,
                                         u4Priority) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssAclL3FilterPriority (i4FilterNo, u4Priority) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssAclL3FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtIpFilterConfig                               */
/*                                                                           */
/*     DESCRIPTION      : This function configures a extended IP ACL filter  */
/*                         and enters the corresponding configuration mode   */
/*                                                                           */
/*     INPUT            :  i4Action -Permit/Deny                             */
/*                         i4Protocol - Protocol value                       */
/*                         u4SrcType - any/source network/ source host       */
/*                         u4SrcIp - Source IP Address                       */
/*                         u4SrcMask - Source IP Mask                        */
/*                         u4DestType - any/dest network/dest host           */
/*                         u4DestIp - Dest IP Address                        */
/*                         u4DestMask - Dest IP Mask                         */
/*                         i4Priority - Filter priority                      */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclExtIpFilterConfig (tCliHandle CliHandle, INT4 i4Action,
                      INT4 i4Protocol, UINT4 u4SrcType,
                      UINT4 u4SrcIpAddr, UINT4 u4SrcMask,
                      UINT4 u4DestType, UINT4 u4DestIpAddr,
                      UINT4 u4DestMask, INT4 i4Tos, INT4 i4Dscp,
                      INT4 i4Priority, UINT4 u4SubAction, UINT2 u2SubActionId)
{
    INT4                i4FilterNo;
    INT4                i4Status;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;
    UINT4               u4RedirectGrpId = 0;
    UINT4               u4ErrCode;

    /*Get the IP Access list number from the current mode in CLI */
    i4FilterNo = CLI_GET_EXTACL ();
    nmhGetIssExtL3FilterStatus (i4FilterNo, &i4Status);

    if (i4Status != ISS_NOT_READY)
    {
        /* Filter has previously been configured. This must be over-written 
         * First destroy the filter and create again */
        if (nmhGetIssExtL3FilterAction (i4FilterNo, &i4ActionOld) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (i4ActionOld == ISS_REDIRECT_TO)
        {
            nmhGetIssAclL3FilterRedirectId (i4FilterNo,
                                            (INT4 *) &u4RedirectGrpId);

            if (u4RedirectGrpId != 0)
            {

                if (nmhGetIssRedirectInterfaceGrpStatus (u4RedirectGrpId,
                                                         &i4RedirectStatus) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }

                if (i4RedirectStatus != 0)
                {

                    if (nmhSetIssRedirectInterfaceGrpStatus
                        (u4RedirectGrpId, ISS_DESTROY) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return (CLI_FAILURE);
                    }
                }
            }
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }
    if (nmhTestv2IssExtL3FilterAction (&u4ErrCode,
                                       i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssExtL3FilterProtocol (&u4ErrCode,
                                         i4FilterNo,
                                         i4Protocol) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /*Test the user input for source IP, mask */
    if (AclTestIpParams (ACL_SRC, i4FilterNo, u4SrcType, u4SrcIpAddr, u4SrcMask)
        == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /*Test the user input for destination IP, mask */
    if (AclTestIpParams
        (ACL_DST, i4FilterNo, u4DestType, u4DestIpAddr,
         u4DestMask) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (i4Tos != ISS_TOS_INVALID)
    {
        if (nmhTestv2IssExtL3FilterTos (&u4ErrCode, i4FilterNo, i4Tos)
            == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (i4Dscp != ISS_DSCP_INVALID)
    {
        if (nmhTestv2IssExtL3FilterDscp (&u4ErrCode, i4FilterNo, i4Dscp)
            == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (nmhTestv2IssExtL3FilterPriority (&u4ErrCode, i4FilterNo, i4Priority)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterProtocol (i4FilterNo, i4Protocol) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (AclSetIpParams (ACL_SRC, i4FilterNo, u4SrcType, u4SrcIpAddr, u4SrcMask)
        == CLI_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (AclSetIpParams
        (ACL_DST, i4FilterNo, u4DestType, u4DestIpAddr,
         u4DestMask) == CLI_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (i4Tos != ISS_TOS_INVALID)
    {
        if (nmhSetIssExtL3FilterTos (i4FilterNo, i4Tos) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4Dscp != ISS_DSCP_INVALID)
    {
        if (nmhSetIssExtL3FilterDscp (i4FilterNo, i4Dscp) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }
    if (nmhSetIssExtL3FilterPriority (i4FilterNo, i4Priority) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    /* Set the Vlan Id and Action */

    if (nmhTestv2IssAclL3FilterSubAction (&u4ErrCode,
                                          i4FilterNo, (INT4) u4SubAction)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssAclL3FilterSubAction (i4FilterNo, (INT4) u4SubAction) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssAclL3FilterSubActionId (&u4ErrCode,
                                            i4FilterNo,
                                            (INT4) u2SubActionId)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssAclL3FilterSubActionId (i4FilterNo,
                                         (INT4) u2SubActionId) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtPbL3FilterConfig                             */
/*                                                                           */
/*     DESCRIPTION      : This function configures a extended IP ACL filter  */
/*                                                                           */
/*     INPUT            :  i4SVlan  - Service vlan Id                        */
/*                         i4SVlanPrio - Service Vlan Priority               */
/*                         i4CVlan - Cutomer Vlan Id                         */
/*                         i4CVlanPrio - Customer Vlan Priority              */
/*                         i4TagType - Packet Tag type on which the filter   */
/*                                     will be applied                       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclExtPbL3FilterConfig (tCliHandle CliHandle, INT4 i4SVlan, INT4 i4SVlanPrio,
                        INT4 i4CVlan, INT4 i4CVlanPrio, INT4 i4TagType)
{

    INT4                i4FilterNo;
    UINT4               u4ErrCode;

    i4FilterNo = CLI_GET_EXTACL ();

    if (AclPbTestL3Filter (&u4ErrCode, CliHandle, i4FilterNo, i4SVlan,
                           i4SVlanPrio, i4CVlan, i4CVlanPrio) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (AclPbSetL3Filter (CliHandle, i4FilterNo, i4SVlan, i4SVlanPrio, i4CVlan,
                          i4CVlanPrio) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (i4TagType != ISS_ZERO_ENTRY)
    {
        if (AclPbTestSetL3TagType (&u4ErrCode, CliHandle, i4FilterNo,
                                   i4TagType) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }
    if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtPbL2FilterConfig                             */
/*                                                                           */
/*     DESCRIPTION      : This function configures a extended IP ACL filter  */
/*                                                                           */
/*     INPUT            :  i4SVlan  - Service vlan Id                        */
/*                         i4SVlanPrio - Service Vlan Priority               */
/*                         i4CVlan - Cutomer Vlan Id                         */
/*                         i4CVlanPrio - Customer Vlan Priority              */
/*                         i4TagType - Packet Tag type on which the filter   */
/*                                     will be applied                       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclExtPbL2FilterConfig (tCliHandle CliHandle, INT4 i4OuterEType,
                        INT4 i4SVlan, INT4 i4SVlanPrio,
                        INT4 i4CVlanPrio, INT4 i4TagType)
{

    INT4                i4FilterNo;
    UINT4               u4ErrCode;

    i4FilterNo = CLI_GET_MACACL ();

    if (AclPbTestL2Filter (&u4ErrCode, CliHandle, i4FilterNo, i4OuterEType,
                           i4SVlan, i4SVlanPrio, i4CVlanPrio) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (AclPbSetL2Filter (CliHandle, i4FilterNo, i4OuterEType,
                          i4SVlan, i4SVlanPrio, i4CVlanPrio) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (i4TagType != 0)
    {
        if (AclPbTestSetL2TagType (&u4ErrCode, CliHandle, i4FilterNo,
                                   i4TagType) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtIpFilterTcpUdpConfig                         */
/*                                                                           */
/*     DESCRIPTION      : This function configures a extended IP ACL filter  */
/*                         and enters the corresponding configuration mode   */
/*                                                                           */
/*     INPUT            :  i4Action -Permit/Deny                             */
/*                         i4Protocol - Protocol value                       */
/*                         u4SrcType - any/source network/ source host       */
/*                         u4SrcIp - Source IP Address                       */
/*                         u4SrcMask - Source IP Mask                        */
/*                         u4DestType - any/dest network/dest host           */
/*                         u4DestIp - Dest IP Address                        */
/*                         u4DestMask - Dest IP Mask                         */
/*                         i4Priority - Filter priority                      */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclExtIpFilterTcpUdpConfig (tCliHandle CliHandle, INT4 i4Action,
                            INT4 i4Protocol, UINT4 u4SrcType, UINT4 u4SrcIpAddr,
                            UINT4 u4SrcMask, UINT4 u4SrcPortFlag,
                            UINT4 u4SrcMinPort, UINT4 u4SrcMaxPort,
                            UINT4 u4DestType, UINT4 u4DestIpAddr,
                            UINT4 u4DestMask, UINT4 u4DestPortFlag,
                            UINT4 u4DestMinPort, UINT4 u4DestMaxPort,
                            UINT4 u4BitType, INT4 i4Tos, INT4 i4Dscp,
                            INT4 i4Priority, UINT4 u4SubAction,
                            UINT2 u2SubActionId)
{
    INT4                i4FilterNo;
    INT4                i4Status;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;
    UINT4               u4ErrCode;
    UINT4               u4RedirectGrpId = 0;
    /*Get the IP Access list number from the current mode in CLI */
    i4FilterNo = CLI_GET_EXTACL ();

    if (nmhGetIssExtL3FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        /* Filter does not exist */
        CliPrintf (CliHandle, "\r%% Invalid Filter number \r\n");
        return (CLI_FAILURE);
    }

    if (i4Status != ISS_NOT_READY)
    {

        /* Filter has previously been configured. This must be over-written 
         * First destroy the filter and create again */
        if (nmhGetIssExtL3FilterAction (i4FilterNo, &i4ActionOld) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (i4ActionOld == ISS_REDIRECT_TO)
        {
            nmhGetIssAclL3FilterRedirectId (i4FilterNo,
                                            (INT4 *) &u4RedirectGrpId);

            if (u4RedirectGrpId != 0)
            {

                if (nmhGetIssRedirectInterfaceGrpStatus (u4RedirectGrpId,
                                                         &i4RedirectStatus) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }

                if (i4RedirectStatus != 0)
                {

                    if (nmhSetIssRedirectInterfaceGrpStatus
                        (u4RedirectGrpId, ISS_DESTROY) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return (CLI_FAILURE);
                    }
                }
            }
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }
    if (nmhTestv2IssExtL3FilterAction (&u4ErrCode,
                                       i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssExtL3FilterProtocol (&u4ErrCode,
                                         i4FilterNo,
                                         i4Protocol) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /*Test the user input for source IP, mask */
    if (AclTestIpParams (ACL_SRC, i4FilterNo, u4SrcType, u4SrcIpAddr, u4SrcMask)
        == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (u4SrcPortFlag == TRUE)
    {
        if (nmhTestv2IssExtL3FilterMinSrcProtPort
            (&u4ErrCode, i4FilterNo, u4SrcMinPort) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhTestv2IssExtL3FilterMaxSrcProtPort
            (&u4ErrCode, i4FilterNo, u4SrcMaxPort) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    /*Test the user input for destination IP, mask */
    if (AclTestIpParams
        (ACL_DST, i4FilterNo, u4DestType, u4DestIpAddr,
         u4DestMask) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (u4DestPortFlag == TRUE)
    {
        if (nmhTestv2IssExtL3FilterMinDstProtPort
            (&u4ErrCode, i4FilterNo, u4DestMinPort) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhTestv2IssExtL3FilterMaxDstProtPort
            (&u4ErrCode, i4FilterNo, u4DestMinPort) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (u4BitType == ACL_ACK)
    {
        /* filter TCP ACK bits */
        if (nmhTestv2IssExtL3FilterAckBit (&u4ErrCode, i4FilterNo,
                                           ISS_ACK_ESTABLISH) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }
    else if (u4BitType == ACL_RST)
    {
        /* filter TCP RST bits */
        if (nmhTestv2IssExtL3FilterRstBit (&u4ErrCode, i4FilterNo, ISS_RST_SET)
            == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (i4Tos != ISS_TOS_INVALID)
    {
        if (nmhTestv2IssExtL3FilterTos (&u4ErrCode, i4FilterNo, i4Tos)
            == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }
    if (i4Dscp != ISS_DSCP_INVALID)
    {
        if (nmhTestv2IssExtL3FilterDscp (&u4ErrCode, i4FilterNo, i4Dscp)
            == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }
    if (nmhTestv2IssExtL3FilterPriority (&u4ErrCode, i4FilterNo, i4Priority)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterProtocol (i4FilterNo, i4Protocol) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (AclSetIpParams (ACL_SRC, i4FilterNo, u4SrcType, u4SrcIpAddr, u4SrcMask)
        == CLI_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (u4SrcPortFlag == TRUE)
    {
        if (nmhSetIssExtL3FilterMinSrcProtPort (i4FilterNo, u4SrcMinPort)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        if (nmhSetIssExtL3FilterMaxSrcProtPort (i4FilterNo, u4SrcMaxPort)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (AclSetIpParams
        (ACL_DST, i4FilterNo, u4DestType, u4DestIpAddr,
         u4DestMask) == CLI_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (u4DestPortFlag == TRUE)
    {
        if (nmhSetIssExtL3FilterMinDstProtPort (i4FilterNo, u4DestMinPort)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        if (nmhSetIssExtL3FilterMaxDstProtPort (i4FilterNo, u4DestMaxPort)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4Tos != ISS_TOS_INVALID)
    {
        if (nmhSetIssExtL3FilterTos (i4FilterNo, i4Tos) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4Dscp != ISS_DSCP_INVALID)
    {
        if (nmhSetIssExtL3FilterDscp (i4FilterNo, i4Dscp) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }
    if (nmhSetIssExtL3FilterPriority (i4FilterNo, i4Priority) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (u4BitType == ACL_ACK)
    {
        /* Filter TCP ACK bits */
        if (nmhSetIssExtL3FilterAckBit (i4FilterNo, ISS_ACK_ESTABLISH) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    else if (u4BitType == ACL_RST)
    {
        /* Filter TCP RST bits */
        if (nmhSetIssExtL3FilterRstBit (i4FilterNo, ISS_RST_SET) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    /* Set the Vlan Id and Action */

    if (nmhTestv2IssAclL3FilterSubAction (&u4ErrCode,
                                          i4FilterNo, (INT4) u4SubAction)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssAclL3FilterSubAction (i4FilterNo, (INT4) u4SubAction) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssAclL3FilterSubActionId (&u4ErrCode,
                                            i4FilterNo,
                                            (INT4) u2SubActionId)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssAclL3FilterSubActionId (i4FilterNo,
                                         (INT4) u2SubActionId) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtIpFilterIcmpConfig                           */
/*                                                                           */
/*     DESCRIPTION      : This function configures a extended IP ACL filter  */
/*                         and enters the corresponding configuration mode   */
/*                                                                           */
/*     INPUT            :  i4Action -Permit/Deny                             */
/*                         u4SrcType - any/source network/ source host       */
/*                         u4SrcIp - Source IP Address                       */
/*                         u4SrcMask - Source IP Mask                        */
/*                         u4DestType - any/dest network/dest host           */
/*                         u4DestIp - Dest IP Address                        */
/*                         u4DestMask - Dest IP Mask                         */
/*                         u4MesageType, u4MessageCode - ICMP details        */
/*                         i4Priority - Filter priority                      */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclExtIpFilterIcmpConfig (tCliHandle CliHandle, INT4 i4Action,
                          UINT4 u4SrcType, UINT4 u4SrcIpAddr, UINT4 u4SrcMask,
                          UINT4 u4DestType, UINT4 u4DestIpAddr,
                          UINT4 u4DestMask, INT4 i4MessageType,
                          INT4 i4MessageCode, INT4 i4Priority)
{
    INT4                i4FilterNo;
    INT4                i4Status;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;
    UINT4               u4RedirectGrpId = 0;
    UINT4               u4ErrCode;
    /*Get the IP Access list number from the current mode in CLI */
    i4FilterNo = CLI_GET_EXTACL ();

    nmhGetIssExtL3FilterStatus (i4FilterNo, &i4Status);

    if (i4Status != ISS_NOT_READY)
    {

        /* Filter has previously been configured. This must be over-written 
         * First destroy the filter and create again */
        if (nmhGetIssExtL3FilterAction (i4FilterNo, &i4ActionOld) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (i4ActionOld == ISS_REDIRECT_TO)
        {
            nmhGetIssAclL3FilterRedirectId (i4FilterNo,
                                            (INT4 *) &u4RedirectGrpId);

            if (u4RedirectGrpId != 0)
            {

                if (nmhGetIssRedirectInterfaceGrpStatus (u4RedirectGrpId,
                                                         &i4RedirectStatus) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }

                if (i4RedirectStatus == ISS_ACTIVE)
                {

                    if (nmhSetIssRedirectInterfaceGrpStatus
                        (u4RedirectGrpId, ISS_DESTROY) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return (CLI_FAILURE);
                    }
                }
            }
        }
        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }

    if (nmhTestv2IssExtL3FilterAction (&u4ErrCode,
                                       i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /*Test the user input for source IP, mask */

    if (AclTestIpParams (ACL_SRC, i4FilterNo, u4SrcType, u4SrcIpAddr, u4SrcMask)
        == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /*Test the user input for destination IP, mask */
    if (AclTestIpParams
        (ACL_DST, i4FilterNo, u4DestType, u4DestIpAddr,
         u4DestMask) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssExtL3FilterProtocol (&u4ErrCode,
                                         i4FilterNo,
                                         ISS_PROT_ICMP) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssExtL3FilterMessageType
        (&u4ErrCode, i4FilterNo, i4MessageType) == SNMP_FAILURE)

    {
        /* ICMP Message Type */
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssExtL3FilterMessageCode
        (&u4ErrCode, i4FilterNo, i4MessageCode) == SNMP_FAILURE)
    {
        /* ICMP Message Code */
        return (CLI_FAILURE);
    }
    if (nmhSetIssExtL3FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    /* Filter Priority */
    if (nmhTestv2IssExtL3FilterPriority (&u4ErrCode, i4FilterNo, i4Priority)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (AclSetIpParams (ACL_SRC, i4FilterNo, u4SrcType, u4SrcIpAddr, u4SrcMask)
        == CLI_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (AclSetIpParams
        (ACL_DST, i4FilterNo, u4DestType, u4DestIpAddr,
         u4DestMask) == CLI_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterProtocol (i4FilterNo, ISS_PROT_ICMP) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (nmhSetIssExtL3FilterMessageType (i4FilterNo, i4MessageType)
        == SNMP_FAILURE)
    {
        /* ICMP Message Type */
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterMessageCode (i4FilterNo, i4MessageCode)
        == SNMP_FAILURE)
    {
        /* ICMP Message Code */
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (nmhSetIssExtL3FilterPriority (i4FilterNo, i4Priority) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclIpAccessGroup                                   */
/*                                                                           */
/*     DESCRIPTION      : This function associated an IP acl to an interface */
/*                                                                           */
/*     INPUT            : i4FilterNo - IP ACL number                         */
/*                        i4Direction - In or out                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclIpAccessGroup (tCliHandle CliHandle, INT4 i4FilterNo, INT4 i4Direction)
{
    UINT4               u4IfIndex;
    UINT4               u4L3RedirectIndex = 0;
    UINT1               au1PortList[ISS_PORTLIST_LEN];
    UINT1               au1OldPortList[ISS_PORTLIST_LEN];
    tSNMP_OCTET_STRING_TYPE PortList;
    tSNMP_OCTET_STRING_TYPE OldPortList;
    UINT4               u4ErrCode;
    INT4                i4Status;
    INT4                i4DirectionStatus;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;

    MEMSET (au1PortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1OldPortList, 0, ISS_PORTLIST_LEN);

    PortList.i4_Length = ISS_PORTLIST_LEN;
    PortList.pu1_OctetList = &au1PortList[0];

    OldPortList.i4_Length = ISS_PORTLIST_LEN;
    OldPortList.pu1_OctetList = &au1OldPortList[0];
    /*Get the interface index from the current mode in CLI */
    u4IfIndex = CLI_GET_IFINDEX ();

    if ((nmhGetIssExtL3FilterStatus (i4FilterNo, &i4Status)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid IP Filter \r\n");
        return (CLI_FAILURE);
    }
    if (nmhGetIssExtL3FilterDirection (i4FilterNo, &i4DirectionStatus)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* If Action is Redirection for Filter then call our API */
    if (nmhGetIssExtL3FilterAction (i4FilterNo, &i4ActionOld) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if ((i4Direction != ACL_ACCESS_IN) && (i4ActionOld == ISS_REDIRECT_TO))
    {
        CliPrintf (CliHandle,
                   "\r%% Redirect Filter Not Applicable For OutPort \r\n");
        return CLI_FAILURE;
    }

    if (i4ActionOld == ISS_REDIRECT_TO)
    {
        nmhGetIssAclL3FilterRedirectId (i4FilterNo,
                                        (INT4 *) &u4L3RedirectIndex);
        if (u4L3RedirectIndex == 0)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (i4Direction == ACL_ACCESS_IN)

    {
        nmhGetIssExtL3FilterInPortList (i4FilterNo, &PortList);
    }
    else
    {
        nmhGetIssExtL3FilterOutPortList (i4FilterNo, &PortList);
    }
    MEMCPY (OldPortList.pu1_OctetList, PortList.pu1_OctetList,
            PortList.i4_Length);
    OldPortList.i4_Length = PortList.i4_Length;

    ACL_ADD_PORT_LIST (au1PortList, u4IfIndex);

    if (i4Status == ISS_ACTIVE)
    {
        if (nmhTestv2IssExtL3FilterStatus
            (&u4ErrCode, i4FilterNo, ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        if (u4L3RedirectIndex != 0)
        {
            nmhGetIssRedirectInterfaceGrpStatus (u4L3RedirectIndex,
                                                 &i4RedirectStatus);

            if (i4RedirectStatus == ISS_ACTIVE)
            {
                if (nmhTestv2IssRedirectInterfaceGrpStatus (&u4ErrCode,
                                                            u4L3RedirectIndex,
                                                            ISS_NOT_IN_SERVICE)
                    == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }

                if (nmhSetIssRedirectInterfaceGrpStatus (u4L3RedirectIndex,
                                                         ISS_NOT_IN_SERVICE) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }
            }
        }
    }

    if (nmhTestv2IssExtL3FilterDirection
        (&u4ErrCode, i4FilterNo, i4Direction) == SNMP_FAILURE)
    {
        if (nmhSetIssExtL3FilterStatus (i4FilterNo, i4Status) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        return CLI_FAILURE;
    }

    if (nmhSetIssExtL3FilterDirection (i4FilterNo, i4Direction) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    /* Based on the direction we need to set the OutPortlist or the InPortlist
     * The status of the filter entry is set to NOT_IN_SERVICE and the port-list
     * should be updated.
     */

    if (i4Direction == ACL_ACCESS_IN)
    {
        if (nmhTestv2IssExtL3FilterInPortList
            (&u4ErrCode, i4FilterNo, &PortList) == SNMP_FAILURE)
        {
            if (nmhSetIssExtL3FilterDirection (i4FilterNo, i4DirectionStatus)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetIssExtL3FilterStatus (i4FilterNo, i4Status) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

            return (CLI_FAILURE);
        }
    }
    else
    {
        if (nmhTestv2IssExtL3FilterOutPortList
            (&u4ErrCode, i4FilterNo, &PortList) == SNMP_FAILURE)
        {
            if (nmhSetIssExtL3FilterDirection (i4FilterNo, i4DirectionStatus)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetIssExtL3FilterStatus (i4FilterNo, i4Status) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

            return (CLI_FAILURE);
        }
    }

    if (i4Direction == ACL_ACCESS_IN)
    {
        if (nmhSetIssExtL3FilterInPortList (i4FilterNo, &PortList) ==
            SNMP_FAILURE)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, i4Status);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else
    {
        if (nmhSetIssExtL3FilterOutPortList (i4FilterNo, &PortList)
            == SNMP_FAILURE)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, i4Status);
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* Activate the filter entry after configuring all the necessary parameters */

    if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        if (i4Direction == ACL_ACCESS_IN)
        {
            nmhSetIssExtL3FilterInPortList (i4FilterNo, &OldPortList);
        }
        else
        {
            nmhSetIssExtL3FilterOutPortList (i4FilterNo, &OldPortList);
        }
        nmhSetIssExtL3FilterStatus (i4FilterNo, i4Status);
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        return (CLI_FAILURE);
    }

    if (u4L3RedirectIndex != 0)
    {
        if (nmhSetIssRedirectInterfaceGrpStatus (u4L3RedirectIndex,
                                                 ISS_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
    /*                                                                           */
/*     FUNCTION NAME    : AclNoIpAccessGroup                                 */
/*                                                                           */
/*     DESCRIPTION      : This function remove an IP acls from  an interface */
/*                                                                           */
/*     INPUT            : i4FilterNo - IP ACL number                         */
/*                        i4Direction - In or out                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclNoIpAccessGroup (tCliHandle CliHandle, INT4 i4FilterNo, INT4 i4Direction)
{
    tSNMP_OCTET_STRING_TYPE PortList;
    UINT1               au1PortList[ISS_PORTLIST_LEN];
    UINT1               au1PortListAll[ISS_PORTLIST_LEN];
    UINT4               u4IfIndex;
    INT4                i4CurrentFilter = 0;
    UINT1               u1Flag = 1;
    UINT4               u4ErrCode;
    UINT4               u4L3RedirectIndex = 0;
    INT4                i4RedirectStatus = 0;
    INT4                i4ActionOld = 0;

    MEMSET (au1PortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1PortListAll, 0, ISS_PORTLIST_LEN);

    PortList.i4_Length = ISS_PORTLIST_LEN;
    PortList.pu1_OctetList = &au1PortList[0];

    /*Get the interface index from the current mode in CLI */
    u4IfIndex = CLI_GET_IFINDEX ();

    if (i4FilterNo == 0)
    {
        /* The boolean u1Flag is used to specify whether the operation 
         * needs to be performed for all filters or for specific filter
         * only.
         */

        u1Flag = 0;

        if (nmhGetFirstIndexIssExtL3FilterTable (&i4FilterNo) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n No IP ACLs have been configured\r\n");
            return (CLI_SUCCESS);
        }
    }

    do
    {
        if (i4Direction == ACL_ACCESS_IN)
        {
            nmhGetIssExtL3FilterInPortList (i4FilterNo, &PortList);
            /* If Action is Redirection for Filter then call our API */
            if (nmhGetIssExtL3FilterAction (i4FilterNo, &i4ActionOld) ==
                SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (i4ActionOld == ISS_REDIRECT_TO)
            {
                nmhGetIssAclL3FilterRedirectId (i4FilterNo,
                                                (INT4 *) &u4L3RedirectIndex);
                if (u4L3RedirectIndex == 0)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
            }
        }

        else
        {
            nmhGetIssExtL3FilterOutPortList (i4FilterNo, &PortList);
        }

        if (CliIsMemberPort (PortList.pu1_OctetList,
                             ISS_PORTLIST_LEN, u4IfIndex) != CLI_SUCCESS)

        {
            if (u1Flag == 1)
            {

                CliPrintf (CliHandle,
                           "\r%% This filter has not been configured for this "
                           "interface or for this direction in this interface"
                           "\r\n");
                return (CLI_SUCCESS);
            }

        }
        else
        {

            ACL_DEL_PORT_LIST (au1PortList, u4IfIndex);

            /* If there are no ports left in the port list,
             * then apply the filter on all ports */

            /* Filter exists for this interface */

            if (nmhTestv2IssExtL3FilterStatus
                (&u4ErrCode, i4FilterNo, ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
            if (u4L3RedirectIndex != 0)
            {
                nmhGetIssRedirectInterfaceGrpStatus (u4L3RedirectIndex,
                                                     &i4RedirectStatus);

                if (i4RedirectStatus == ISS_ACTIVE)
                {
                    if (nmhTestv2IssRedirectInterfaceGrpStatus (&u4ErrCode,
                                                                u4L3RedirectIndex,
                                                                ISS_NOT_IN_SERVICE)
                        == SNMP_FAILURE)
                    {
                        return (CLI_FAILURE);
                    }

                    if (nmhSetIssRedirectInterfaceGrpStatus (u4L3RedirectIndex,
                                                             ISS_NOT_IN_SERVICE)
                        == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return (CLI_FAILURE);
                    }
                }
            }

            if (nmhTestv2IssExtL3FilterDirection
                (&u4ErrCode, i4FilterNo, i4Direction) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetIssExtL3FilterDirection (i4FilterNo, i4Direction) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (i4Direction == ACL_ACCESS_IN)
            {
                if (nmhTestv2IssExtL3FilterInPortList (&u4ErrCode, i4FilterNo,
                                                       &PortList) ==
                    SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                if (nmhSetIssExtL3FilterInPortList (i4FilterNo, &PortList)
                    == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }
            }
            else
            {
                if (nmhTestv2IssExtL3FilterOutPortList (&u4ErrCode, i4FilterNo,
                                                        &PortList) ==
                    SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                if (nmhSetIssExtL3FilterOutPortList (i4FilterNo, &PortList)
                    == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }
            }

            if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_ACTIVE) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);

            }
            if (u4L3RedirectIndex != 0)
            {
                if (nmhSetIssRedirectInterfaceGrpStatus (u4L3RedirectIndex,
                                                         ISS_ACTIVE)
                    == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }
            }

        }

        i4CurrentFilter = i4FilterNo;

    }
    while ((nmhGetNextIndexIssExtL3FilterTable (i4CurrentFilter, &i4FilterNo) ==
            SNMP_SUCCESS) && (u1Flag == 0));

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtMacFilterConfig                              */
/*                                                                           */
/*     DESCRIPTION      : This function to configure MAC acls                */
/*                                                                           */
/*     INPUT            : u4SrcType - ANY/ Source HOST MAC                   */
/*                        SrcMacAddr - Source Mac Address                    */
/*                        u4DestType - ANY/ Dest Source MAC                  */
/*                        i4Protocol - Protocol value                        */
/*                        i4EtherType - Ethertype                            */
/*                        u4VlanId - Vlan Id                                 */
/*                        u4Priority - Priority value                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclExtMacFilterConfig (tCliHandle CliHandle, INT4 i4Action,
                       UINT4 u4SrcType, tMacAddr SrcMacAddr,
                       UINT4 u4DestType, tMacAddr DestMacAddr,
                       INT4 i4Protocol, INT4 i4EtherType,
                       UINT4 u4VlanId, INT4 i4Priority,
                       UINT4 u4SubAction, UINT2 u2SubActionId,
                       INT4 i4NextFilterType, INT4 i4NextFilterId)
{

    INT4                i4FilterNo;
    INT4                i4Status;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;
    UINT4               u4RedirectGrpId = 0;
    UINT4               u4ErrCode = 0;
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    tIssUserDefinedFilterTable *pIssAclUdbFilterTableEntry = NULL;
    /*Get the MAC access list number from the current mode in CLI */
    if (i4NextFilterId != 0)
    {
        switch (i4NextFilterType)
        {
            case ISS_L2FILTER:
                pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4NextFilterId);
                if (pIssAclL2FilterEntry == NULL)
                {
                    return (CLI_FAILURE);
                }
                break;
            case ISS_L3FILTER:
                pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4NextFilterId);
                if (pIssAclL3FilterEntry == NULL)
                {
                    return (CLI_FAILURE);
                }
                break;
            case ISS_UDBFILTER:
                pIssAclUdbFilterTableEntry =
                    IssExtGetUdbFilterTableEntry ((UINT4) i4NextFilterId);
                if (pIssAclUdbFilterTableEntry == NULL)
                {
                    return (CLI_FAILURE);
                }
                break;
            default:
                return (CLI_FAILURE);
        }
    }
    i4FilterNo = CLI_GET_MACACL ();

    nmhGetIssExtL2FilterStatus (i4FilterNo, &i4Status);

    if (i4Status != ISS_NOT_READY)
    {
        /* Filter has previously been configured, it must be overwriten
         */
        if (nmhGetIssExtL2FilterAction (i4FilterNo, &i4ActionOld) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (i4ActionOld == ISS_REDIRECT_TO)
        {
            nmhGetIssAclL2FilterRedirectId (i4FilterNo,
                                            (INT4 *) &u4RedirectGrpId);
            if (u4RedirectGrpId != 0)
            {

                if (nmhGetIssRedirectInterfaceGrpStatus (u4RedirectGrpId,
                                                         &i4RedirectStatus) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }

                if (i4RedirectStatus != 0)
                {

                    if (nmhSetIssRedirectInterfaceGrpStatus
                        (u4RedirectGrpId, ISS_DESTROY) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return (CLI_FAILURE);
                    }
                }
            }
        }

        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }
    if (nmhTestv2IssExtL2FilterAction (&u4ErrCode,
                                       i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (u4SrcType != ACL_ANY)
    {
        /* Source MAC has been specified */

        if (nmhTestv2IssExtL2FilterSrcMacAddr (&u4ErrCode, i4FilterNo,
                                               SrcMacAddr) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (u4DestType != ACL_ANY)
    {
        /* Destination MAC has been specified */

        if (nmhTestv2IssExtL2FilterDstMacAddr (&u4ErrCode, i4FilterNo,
                                               DestMacAddr) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (i4Protocol != 0)
    {
        if (nmhTestv2IssExtL2FilterProtocolType (&u4ErrCode, i4FilterNo,
                                                 i4Protocol) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (i4EtherType != 0)
    {
        if (nmhTestv2IssExtL2FilterEtherType
            (&u4ErrCode, i4FilterNo, i4EtherType) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (u4VlanId != 0)
    {
        if (nmhTestv2IssExtL2FilterVlanId (&u4ErrCode,
                                           i4FilterNo,
                                           u4VlanId) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }
    if (nmhTestv2IssExtL2FilterPriority (&u4ErrCode, i4FilterNo, i4Priority)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL2FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (u4SrcType != ACL_ANY)
    {

        if (nmhSetIssExtL2FilterSrcMacAddr (i4FilterNo, SrcMacAddr)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (u4DestType != ACL_ANY)
    {

        if (nmhSetIssExtL2FilterDstMacAddr (i4FilterNo, DestMacAddr)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4Protocol != 0)
    {
        if (nmhSetIssExtL2FilterProtocolType (i4FilterNo, i4Protocol) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4EtherType != 0)
    {
        if (nmhSetIssExtL2FilterEtherType (i4FilterNo, i4EtherType) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (u4VlanId != 0)
    {
        if (nmhSetIssExtL2FilterVlanId (i4FilterNo, u4VlanId) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    if (nmhSetIssExtL2FilterPriority (i4FilterNo, i4Priority) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    /* Set the Vlan Id and Action */

    if (nmhTestv2IssAclL2FilterSubAction (&u4ErrCode,
                                          i4FilterNo, (INT4) u4SubAction)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssAclL2FilterSubAction (i4FilterNo, (INT4) u4SubAction) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssAclL2FilterSubActionId (&u4ErrCode,
                                            i4FilterNo,
                                            (INT4) u2SubActionId)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssAclL2FilterSubActionId (i4FilterNo,
                                         (INT4) u2SubActionId) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhTestv2IssAclL2NextFilterType (&u4ErrCode,
                                         i4FilterNo, i4NextFilterType)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssAclL2NextFilterType (i4FilterNo, i4NextFilterType) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssAclL2NextFilterNo (&u4ErrCode, i4FilterNo, i4NextFilterId)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssAclL2NextFilterNo (i4FilterNo, i4NextFilterId) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclMacExtAccessGroup                                */
/*                                                                           */
/*     DESCRIPTION      : This function associated an MAC acl to an interface*/
/*                                                                           */
/*     INPUT            : i4FilterNo - MAC ACL number                        */
/*                        i4Direction - In or out                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclMacExtAccessGroup (tCliHandle CliHandle, INT4 i4FilterNo, INT4 i4Direction)
{
    UINT4               u4IfIndex;
    UINT1               au1PortList[ISS_PORTLIST_LEN];
    UINT1               au1OldPortList[ISS_PORTLIST_LEN];
    tSNMP_OCTET_STRING_TYPE PortList;
    tSNMP_OCTET_STRING_TYPE OldPortList;
    UINT4               u4ErrCode;
    UINT4               u4L2RedirectIndex = 0;
    INT4                i4Status;
    INT4                i4DirectionStatus;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;
    MEMSET (au1PortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1OldPortList, 0, ISS_PORTLIST_LEN);

    PortList.i4_Length = ISS_PORTLIST_LEN;
    PortList.pu1_OctetList = &au1PortList[0];

    OldPortList.i4_Length = ISS_PORTLIST_LEN;
    OldPortList.pu1_OctetList = &au1OldPortList[0];

    /*Get the interface index from the current mode in CLI */
    u4IfIndex = CLI_GET_IFINDEX ();

    if ((nmhGetIssExtL2FilterStatus (i4FilterNo, &i4Status)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid MAC Filter \r\n");
        return (CLI_FAILURE);
    }
    if (nmhGetIssExtL2FilterDirection (i4FilterNo, &i4DirectionStatus)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* If Action is Redirection for Filter then call our API */
    if (nmhGetIssExtL2FilterAction (i4FilterNo, &i4ActionOld) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if ((i4Direction != ACL_ACCESS_IN) && (i4ActionOld == ISS_REDIRECT_TO))
    {
        CliPrintf (CliHandle,
                   "\r%% Redirect Filter Not Applicable For OutPort \r\n");
        return CLI_FAILURE;
    }

    if (i4ActionOld == ISS_REDIRECT_TO)
    {
        nmhGetIssAclL2FilterRedirectId (i4FilterNo,
                                        (INT4 *) &u4L2RedirectIndex);

        if (u4L2RedirectIndex == 0)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }

    if (i4Direction == ACL_ACCESS_IN)
    {
        nmhGetIssExtL2FilterInPortList (i4FilterNo, &PortList);
    }
    else
    {
        nmhGetIssExtL2FilterOutPortList (i4FilterNo, &PortList);
    }
    MEMCPY (OldPortList.pu1_OctetList, PortList.pu1_OctetList,
            PortList.i4_Length);
    OldPortList.i4_Length = PortList.i4_Length;

    ACL_ADD_PORT_LIST (au1PortList, u4IfIndex);

    if (nmhTestv2IssExtL2FilterStatus
        (&u4ErrCode, i4FilterNo, ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (i4Status == ISS_ACTIVE)
    {
        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        if (u4L2RedirectIndex != 0)
        {
            nmhGetIssRedirectInterfaceGrpStatus (u4L2RedirectIndex,
                                                 &i4RedirectStatus);

            if (i4RedirectStatus == ISS_ACTIVE)
            {

                if (nmhTestv2IssRedirectInterfaceGrpStatus (&u4ErrCode,
                                                            u4L2RedirectIndex,
                                                            ISS_NOT_IN_SERVICE)
                    == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }

                if (nmhSetIssRedirectInterfaceGrpStatus (u4L2RedirectIndex,
                                                         ISS_NOT_IN_SERVICE) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }
            }
        }
    }
    if (nmhTestv2IssExtL2FilterDirection (&u4ErrCode, i4FilterNo, i4Direction)
        == SNMP_FAILURE)
    {
        if (nmhSetIssExtL2FilterStatus (i4FilterNo, i4Status) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        return CLI_FAILURE;
    }

    if (nmhSetIssExtL2FilterDirection (i4FilterNo, i4Direction) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return SNMP_FAILURE;
    }

    if (i4Direction == ACL_ACCESS_IN)
    {

        if (nmhTestv2IssExtL2FilterInPortList
            (&u4ErrCode, i4FilterNo, &PortList) == SNMP_FAILURE)
        {
            if (nmhSetIssExtL2FilterDirection (i4FilterNo, i4DirectionStatus)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetIssExtL2FilterStatus (i4FilterNo, i4Status) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

            return (CLI_FAILURE);
        }
    }
    else
    {
        if (nmhTestv2IssExtL2FilterOutPortList
            (&u4ErrCode, i4FilterNo, &PortList) == SNMP_FAILURE)
        {
            if (nmhSetIssExtL2FilterDirection (i4FilterNo, i4DirectionStatus)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetIssExtL2FilterStatus (i4FilterNo, i4Status) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

            return (CLI_FAILURE);
        }

    }

    if (i4Direction == ACL_ACCESS_IN)
    {
        if (nmhSetIssExtL2FilterInPortList (i4FilterNo, &PortList) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else
    {
        if (nmhSetIssExtL2FilterOutPortList (i4FilterNo, &PortList)
            == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        if (i4Direction == ACL_ACCESS_IN)
        {
            nmhSetIssExtL2FilterInPortList (i4FilterNo, &OldPortList);
        }
        else
        {
            nmhSetIssExtL2FilterOutPortList (i4FilterNo, &OldPortList);
        }
        nmhSetIssExtL2FilterStatus (i4FilterNo, i4Status);
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        return (CLI_FAILURE);

    }
    if (u4L2RedirectIndex != 0)
    {
        if (nmhSetIssRedirectInterfaceGrpStatus (u4L2RedirectIndex, ISS_ACTIVE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    UNUSED_PARAM (CliHandle);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclNoMacExtAccessGroup                              */
/*                                                                           */
/*     DESCRIPTION      : This function remove an IP acls from  an interface */
/*                                                                           */
/*     INPUT            : i4FilterNo - IP ACL number                         */
/*                        i4Direction - In or out                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclNoMacExtAccessGroup (tCliHandle CliHandle, INT4 i4FilterNo, INT4 i4Direction)
{
    tSNMP_OCTET_STRING_TYPE PortList;
    UINT1               au1PortList[ISS_PORTLIST_LEN];
    UINT1               au1PortListAll[ISS_PORTLIST_LEN];
    UINT4               u4IfIndex;
    INT4                i4CurrentFilter;
    UINT1               u1Flag = 1;
    UINT4               u4ErrCode = 0;
    UINT4               u4L2RedirectIndex = 0;
    INT4                i4RedirectStatus = 0;
    INT4                i4ActionOld = 0;

    MEMSET (au1PortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1PortListAll, 0, ISS_PORTLIST_LEN);

    PortList.i4_Length = ISS_PORTLIST_LEN;
    PortList.pu1_OctetList = &au1PortList[0];

    /*Get the interface index from the current mode in CLI */
    u4IfIndex = CLI_GET_IFINDEX ();

    if (i4FilterNo == 0)
    {
        u1Flag = 0;
        /* to delete all IP ACL 's in a given interface */

        if (nmhGetFirstIndexIssExtL2FilterTable (&i4FilterNo) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n No MAC ACLs have been configured\r\n");
            return (CLI_SUCCESS);
        }
    }

    do
    {
        if (i4Direction == ACL_ACCESS_IN)
        {
            nmhGetIssExtL2FilterInPortList (i4FilterNo, &PortList);
            /* If Action is Redirection for Filter then call our API */
            if (nmhGetIssExtL2FilterAction (i4FilterNo, &i4ActionOld) ==
                SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (i4ActionOld == ISS_REDIRECT_TO)
            {
                nmhGetIssAclL2FilterRedirectId (i4FilterNo,
                                                (INT4 *) &u4L2RedirectIndex);

                if (u4L2RedirectIndex == 0)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }

            }

        }
        else
        {
            nmhGetIssExtL2FilterOutPortList (i4FilterNo, &PortList);
        }

        if (CliIsMemberPort (PortList.pu1_OctetList,
                             ISS_PORTLIST_LEN, u4IfIndex) == CLI_SUCCESS)

        {
            ACL_DEL_PORT_LIST (au1PortList, u4IfIndex);

            /* If there are no ports left in the port list, then apply the filter
             * on all ports . This happens when portlist is 0 */

            /* Filter exists for this interface */

            if (nmhTestv2IssExtL2FilterStatus
                (&u4ErrCode, i4FilterNo, ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
            if (u4L2RedirectIndex != 0)
            {
                nmhGetIssRedirectInterfaceGrpStatus (u4L2RedirectIndex,
                                                     &i4RedirectStatus);

                if (i4RedirectStatus == ISS_ACTIVE)
                {
                    if (nmhTestv2IssRedirectInterfaceGrpStatus (&u4ErrCode,
                                                                u4L2RedirectIndex,
                                                                ISS_NOT_IN_SERVICE)
                        == SNMP_FAILURE)
                    {
                        return (CLI_FAILURE);
                    }

                    if (nmhSetIssRedirectInterfaceGrpStatus (u4L2RedirectIndex,
                                                             ISS_NOT_IN_SERVICE)
                        == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return (CLI_FAILURE);
                    }
                }
            }

            if (nmhTestv2IssExtL2FilterDirection
                (&u4ErrCode, i4FilterNo, i4Direction) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetIssExtL2FilterDirection (i4FilterNo, i4Direction) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return SNMP_FAILURE;
            }

            if (i4Direction == ACL_ACCESS_IN)
            {
                if (nmhTestv2IssExtL2FilterInPortList
                    (&u4ErrCode, i4FilterNo, &PortList) == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                if (nmhSetIssExtL2FilterInPortList (i4FilterNo, &PortList)
                    == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }
            }
            else
            {
                if (nmhTestv2IssExtL2FilterOutPortList
                    (&u4ErrCode, i4FilterNo, &PortList) == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                if (nmhSetIssExtL2FilterOutPortList (i4FilterNo, &PortList) ==
                    SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
            }

            if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_ACTIVE)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);

            }
            if (u4L2RedirectIndex != 0)
            {
                if (nmhSetIssRedirectInterfaceGrpStatus
                    (u4L2RedirectIndex, ISS_ACTIVE) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }
            }

            return (CLI_SUCCESS);
        }

        else if (u1Flag == 1)
        {
            CliPrintf (CliHandle,
                       "\r%% This filter has not been configured for this interface\r\n");
            return (CLI_SUCCESS);
        }
        i4CurrentFilter = i4FilterNo;

    }
    while ((nmhGetNextIndexIssExtL2FilterTable (i4CurrentFilter, &i4FilterNo) ==
            SNMP_SUCCESS) && (u1Flag == 0));

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclUserDefinedAccessGroup                          */
/*                                                                           */
/*     DESCRIPTION      : This function associated an UDB acl to an interface*/
/*                                                                           */
/*     INPUT            : i4FilterNo - UDB ACL number                        */
/*                        i4Direction - In or out                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclUserDefinedAccessGroup (tCliHandle CliHandle, INT4 i4FilterNo,
                           INT4 i4Direction)
{
    UINT4               u4IfIndex = 0;
    UINT1               au1PortList[ISS_PORTLIST_LEN];
    UINT1               au1OldPortList[ISS_PORTLIST_LEN];
    tSNMP_OCTET_STRING_TYPE PortList;
    tSNMP_OCTET_STRING_TYPE OldPortList;
    UINT4               u4UdbRedirectIndex = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4Status = 0;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;

    MEMSET (au1PortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1OldPortList, 0, ISS_PORTLIST_LEN);

    PortList.i4_Length = ISS_PORTLIST_LEN;
    PortList.pu1_OctetList = &au1PortList[0];

    OldPortList.i4_Length = ISS_PORTLIST_LEN;
    OldPortList.pu1_OctetList = &au1OldPortList[0];

    if (i4Direction != ACL_ACCESS_IN)
    {
        CliPrintf (CliHandle, "\r%% UserDefined Filter is Applicable only for"
                   "Ingress port \r\n");
        return (CLI_FAILURE);
    }
    /*Get the interface index from the current mode in CLI */
    u4IfIndex = CLI_GET_IFINDEX ();

    if ((nmhGetIssAclUserDefinedFilterStatus (i4FilterNo, &i4Status)) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid UDB Filter \r\n");
        return (CLI_FAILURE);
    }

    /* If Action is Redirection for Filter then call our API */
    if (nmhGetIssAclUserDefinedFilterAction (i4FilterNo, &i4ActionOld) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (i4ActionOld == ISS_REDIRECT_TO)
    {

        nmhGetIssAclUserDefinedFilterRedirectId (i4FilterNo,
                                                 (INT4 *) &u4UdbRedirectIndex);
        if (u4UdbRedirectIndex == 0)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    nmhGetIssAclUserDefinedFilterInPortList (i4FilterNo, &PortList);

    MEMCPY (OldPortList.pu1_OctetList, PortList.pu1_OctetList,
            PortList.i4_Length);
    OldPortList.i4_Length = PortList.i4_Length;

    ACL_ADD_PORT_LIST (au1PortList, u4IfIndex);

    if (i4Status == ISS_ACTIVE)
    {

        if (nmhTestv2IssAclUserDefinedFilterStatus
            (&u4ErrCode, i4FilterNo, ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetIssAclUserDefinedFilterStatus (i4FilterNo, ISS_NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (u4UdbRedirectIndex != 0)
        {
            nmhGetIssRedirectInterfaceGrpStatus (u4UdbRedirectIndex,
                                                 &i4RedirectStatus);

            if (i4RedirectStatus == ISS_ACTIVE)
            {
                if (nmhTestv2IssRedirectInterfaceGrpStatus (&u4ErrCode,
                                                            u4UdbRedirectIndex,
                                                            ISS_NOT_IN_SERVICE)
                    == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }

                if (nmhSetIssRedirectInterfaceGrpStatus (u4UdbRedirectIndex,
                                                         ISS_NOT_IN_SERVICE) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }
            }
        }
    }
    /* Based on the direction we need to set the OutPortlist or the InPortlist
     * The status of the filter entry is set to NOT_IN_SERVICE and the port-list
     * should be updated.
     */

    if (nmhTestv2IssAclUserDefinedFilterInPortList
        (&u4ErrCode, i4FilterNo, &PortList) == SNMP_FAILURE)
    {

        if (nmhSetIssAclUserDefinedFilterStatus (i4FilterNo, i4Status) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        return (CLI_FAILURE);
    }
    if (nmhSetIssAclUserDefinedFilterInPortList (i4FilterNo, &PortList) ==
        SNMP_FAILURE)
    {
        nmhSetIssAclUserDefinedFilterStatus (i4FilterNo, i4Status);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    /* Activate the filter entry after configuring all the necessary parameters */
    if (nmhTestv2IssAclUserDefinedFilterStatus (&u4ErrCode, i4FilterNo,
                                                ISS_ACTIVE) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIssAclUserDefinedFilterStatus (i4FilterNo, ISS_ACTIVE) ==
        SNMP_FAILURE)
    {
        nmhSetIssAclUserDefinedFilterInPortList (i4FilterNo, &OldPortList);
        nmhSetIssAclUserDefinedFilterStatus (i4FilterNo, i4Status);
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        return (CLI_FAILURE);
    }

    if (u4UdbRedirectIndex != 0)
    {
        if (nmhTestv2IssRedirectInterfaceGrpStatus
            (&u4ErrCode, u4UdbRedirectIndex, ISS_ACTIVE) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhSetIssRedirectInterfaceGrpStatus (u4UdbRedirectIndex,
                                                 ISS_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclNoUserDefinedAccessGroup                        */
/*                                                                           */
/*     DESCRIPTION      : This function removes an UDB acls from an interface*/
/*                                                                           */
/*     INPUT            : i4FilterNo - UDB ACL number                        */
/*                        i4Direction - In or out                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclNoUserDefinedAccessGroup (tCliHandle CliHandle, INT4 i4FilterNo,
                             INT4 i4Direction)
{
    tSNMP_OCTET_STRING_TYPE PortList;
    UINT1               au1PortList[ISS_PORTLIST_LEN];
    UINT1               au1PortListAll[ISS_PORTLIST_LEN];
    UINT4               u4IfIndex = 0;
    INT4                i4CurrentFilter = 0;
    UINT1               u1Flag = 1;
    UINT4               u4ErrCode = 0;
    UINT4               u4UdbRedirectIndex = 0;
    INT4                i4Status = 0;
    INT4                i4ActionOld = 0;
    INT4                i4RedirectStatus = 0;
    MEMSET (au1PortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1PortListAll, 0, ISS_PORTLIST_LEN);

    PortList.i4_Length = ISS_PORTLIST_LEN;
    PortList.pu1_OctetList = &au1PortList[0];

    if (i4Direction != ACL_ACCESS_IN)
    {
        CliPrintf (CliHandle, "\r%% UserDefined Filter is Applicable only for"
                   "Ingress port \r\n");
        return (CLI_FAILURE);
    }
    /*Get the interface index from the current mode in CLI */
    u4IfIndex = CLI_GET_IFINDEX ();

    if (i4FilterNo == 0)
    {
        u1Flag = 0;

        if (nmhGetFirstIndexIssAclUserDefinedFilterTable ((UINT4 *) &i4FilterNo)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n No UDB have been configured\r\n");
            return (CLI_SUCCESS);
        }
    }
    if (nmhGetIssAclUserDefinedFilterAction (i4FilterNo, &i4ActionOld) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (i4ActionOld == ISS_REDIRECT_TO)
    {

        nmhGetIssAclUserDefinedFilterRedirectId (i4FilterNo,
                                                 (INT4 *) &u4UdbRedirectIndex);
        if (u4UdbRedirectIndex == 0)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    do
    {
        if (i4Direction == ACL_ACCESS_IN)
        {
            nmhGetIssAclUserDefinedFilterInPortList (i4FilterNo, &PortList);
        }

        if (CliIsMemberPort (PortList.pu1_OctetList,
                             ISS_PORTLIST_LEN, u4IfIndex) == CLI_SUCCESS)

        {
            ACL_DEL_PORT_LIST (au1PortList, u4IfIndex);
            if ((nmhGetIssAclUserDefinedFilterStatus (i4FilterNo, &i4Status)) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid UDB Filter \r\n");
                return (CLI_FAILURE);
            }
            if (i4Status == ISS_ACTIVE)
            {
                /* If there are no ports left in the port list, then apply the filter
                 * on all ports . This happens when portlist is 0 */

                /* Filter exists for this interface */
                if (nmhTestv2IssAclUserDefinedFilterStatus
                    (&u4ErrCode, i4FilterNo,
                     ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }

                if (nmhSetIssAclUserDefinedFilterStatus
                    (i4FilterNo, ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }
                if (u4UdbRedirectIndex != 0)
                {
                    nmhGetIssRedirectInterfaceGrpStatus (u4UdbRedirectIndex,
                                                         &i4RedirectStatus);

                    if (i4RedirectStatus == ISS_ACTIVE)
                    {
                        if (nmhTestv2IssRedirectInterfaceGrpStatus (&u4ErrCode,
                                                                    u4UdbRedirectIndex,
                                                                    ISS_NOT_IN_SERVICE)
                            == SNMP_FAILURE)
                        {
                            return (CLI_FAILURE);
                        }

                        if (nmhSetIssRedirectInterfaceGrpStatus
                            (u4UdbRedirectIndex,
                             ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
                        {
                            CLI_FATAL_ERROR (CliHandle);
                            return (CLI_FAILURE);
                        }
                    }
                }
            }
            if (nmhTestv2IssAclUserDefinedFilterInPortList
                (&u4ErrCode, i4FilterNo, &PortList) == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }
            if (nmhSetIssAclUserDefinedFilterInPortList (i4FilterNo, &PortList)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
            if (nmhSetIssAclUserDefinedFilterStatus (i4FilterNo, ISS_ACTIVE)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);

            }
            if (u4UdbRedirectIndex != 0)
            {
                if (nmhTestv2IssRedirectInterfaceGrpStatus
                    (&u4ErrCode, u4UdbRedirectIndex,
                     ISS_ACTIVE) == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                if (nmhSetIssRedirectInterfaceGrpStatus (u4UdbRedirectIndex,
                                                         ISS_ACTIVE) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }
            }
            return (CLI_SUCCESS);
        }

        else if (u1Flag == 1)
        {
            CliPrintf (CliHandle,
                       "\r%% This filter has not been configured for this interface\r\n");
            return (CLI_SUCCESS);
        }
        i4CurrentFilter = i4FilterNo;

    }
    while ((nmhGetNextIndexIssAclUserDefinedFilterTable
            ((UINT4) i4CurrentFilter, (UINT4 *) &i4FilterNo) == SNMP_SUCCESS)
           && (u1Flag == 0));

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclShowAccessLists                                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays all the acls conifgured in  */
/*                        switch                                             */
/*                                                                           */
/*     INPUT            : CliHandle - CLI HAndler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclShowAccessLists (tCliHandle CliHandle, INT4 i4FilterType, INT4 i4FilterNo)
{
    INT4                i4NextFilter = 0;
    INT4                i4PrevFilter;
    UINT1               u1Quit = CLI_FAILURE;

    if (i4FilterNo != 0)
    {
        if (i4FilterType == CLI_IP_ACL)
        {
            if (AclShowL3Filter (CliHandle, i4FilterNo) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nInvalid IP Access List \r\n");
                return CLI_FAILURE;
            }
        }
        else if (i4FilterType == CLI_MAC_ACL)
        {
            if (AclShowL2Filter (CliHandle, i4FilterNo) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nInvalid Mac Access List \r\n");
                return CLI_FAILURE;
            }
        }
        else if (i4FilterType == CLI_UDB_ACL)
        {
            if (AclShowUDBFilter (CliHandle, i4FilterNo) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nInvalid UDB Filter \r\n");
                return CLI_FAILURE;
            }
        }
        else
        {
            if (AclShowL3Filter (CliHandle, i4FilterNo) == CLI_SUCCESS)
            {
                u1Quit = CLI_SUCCESS;
            }
            if (AclShowL2Filter (CliHandle, i4FilterNo) == CLI_SUCCESS)
            {
                u1Quit = CLI_SUCCESS;
            }
            if (AclShowUDBFilter (CliHandle, i4FilterNo) == CLI_SUCCESS)
            {
                u1Quit = CLI_SUCCESS;
            }
            if (u1Quit == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nInvalid Access List \r\n");
                return CLI_FAILURE;
            }
        }
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\nIP ACCESS LISTS \r\n");
    CliPrintf (CliHandle, "-----------------\r\n");

    if (nmhGetFirstIndexIssExtL3FilterTable (&i4NextFilter) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r No IP Access Lists have been configured \r\n");
    }
    else
    {
        do
        {
            AclShowL3Filter (CliHandle, i4NextFilter);
            i4PrevFilter = i4NextFilter;
            u1Quit = (UINT1) CliPrintf (CliHandle, "\r\n");
        }
        while ((INT4) (nmhGetNextIndexIssExtL3FilterTable
                       (i4PrevFilter, &i4NextFilter) != SNMP_FAILURE)
               && (u1Quit != CLI_FAILURE));

        if (u1Quit == CLI_FAILURE)
        {
            return (u1Quit);
        }
    }

    CliPrintf (CliHandle, "\r\nMAC ACCESS LISTS\r\n");
    CliPrintf (CliHandle, "-----------------\r\n");

    if (nmhGetFirstIndexIssExtL2FilterTable (&i4NextFilter) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n No MAC Access Lists have been configured\r\n\r\n");
    }
    else
    {
        do
        {
            AclShowL2Filter (CliHandle, i4NextFilter);
            i4PrevFilter = i4NextFilter;
            u1Quit = (UINT1) CliPrintf (CliHandle, "\r\n\r\n");
        }
        while ((INT4) (nmhGetNextIndexIssExtL2FilterTable
                       (i4PrevFilter, &i4NextFilter) != SNMP_FAILURE)
               && (u1Quit != CLI_FAILURE));

        if (u1Quit == CLI_FAILURE)
        {
            return (u1Quit);
        }
    }

    CliPrintf (CliHandle, "\r\nUSER DEFINED LISTS\r\n");
    CliPrintf (CliHandle, "----------------------\r\n");

    if (nmhGetFirstIndexIssAclUserDefinedFilterTable ((UINT4 *) &i4NextFilter)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n No User Defined Lists have been configured\r\n\r\n");
    }
    else
    {
        do
        {
            AclShowUDBFilter (CliHandle, i4NextFilter);
            i4PrevFilter = i4NextFilter;
            u1Quit = (UINT1) CliPrintf (CliHandle, "\r\n\r\n");
        }
        while ((INT4) (nmhGetNextIndexIssAclUserDefinedFilterTable
                       ((UINT4) i4PrevFilter,
                        (UINT4 *) &i4NextFilter) != SNMP_FAILURE)
               && (u1Quit != CLI_FAILURE));

        if (u1Quit == CLI_FAILURE)
        {
            return (u1Quit);
        }
    }

    return (u1Quit);
}

INT4
AclShowL3Filter (tCliHandle CliHandle, INT4 i4NextFilter)
{
    INT4                i4FltrTos = 0;
    INT4                i4FltrPrio = 0;
    INT4                i4MsgCode = 0;
    INT4                i4MsgType = 0;
    INT4                i4Protocol = 0;
    INT4                i4FltrAckBit = 0;
    INT4                i4FltrRstBit = 0;
    INT4                i4FltrAction = 0;
    INT4                i4RowStatus = 0;
    INT4                i4SVlan = 0;
    INT4                i4SVlanPrio = 0;
    INT4                i4CVlan = 0;
    INT4                i4CVlanPrio = 0;
    INT4                i4TagType = 0;
    INT4                i4TrafficDistByte = 0;
    INT4                i4RedirectInterfaceGrpType = 0;
    INT4                i4SubActionId = 0;
    INT4                i4SubAction = 0;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4SrcIpMask = 0;
    UINT4               u4DstIpAddr = 0;
    UINT4               u4DstIpMask = 0;
    UINT4               u4MinSrcProtPort = 0;
    UINT4               u4MaxSrcProtPort = 0;
    UINT4               u4MinDstProtPort = 0;
    UINT4               u4MaxDstProtPort = 0;
    UINT4               u4InterfaceIndex = 0;
    INT4                i4AddrType = 0;
    UINT4               u4FlowId = 0;
    CHR1               *pu1String;
    UINT1               au1String[ISS_MAX_LEN];
    UINT1               au1InPortList[ISS_PORTLIST_LEN];
    UINT1               au1OutPortList[ISS_PORTLIST_LEN];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    tSNMP_OCTET_STRING_TYPE InPortList;
    tSNMP_OCTET_STRING_TYPE OutPortList;
    tSNMP_OCTET_STRING_TYPE RedirectsPorts;
    INT1               *piIfName;
    INT4                i4Count;
    INT4                i4CommaCount;
    UINT1               u1StdAclFlag;
    INT4                i4FltrDscp = 0;
    UINT4               u4SrcIpPrefixLength;
    UINT4               u4DstIpPrefixLength;
    UINT4               u4NewRedirectGrpIndex = 0;
    UINT1               au1Temp[ISS_ADDR_LEN];
    UINT1               au1Temp1[ISS_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE SrcIpAddrOctet;
    tSNMP_OCTET_STRING_TYPE DstIpAddrOctet;
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;
    tPortList          *pPorts = NULL;
    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4NextFilter);
    SrcIpAddrOctet.pu1_OctetList = &au1Temp[0];
    DstIpAddrOctet.pu1_OctetList = &au1Temp1[0];

    InPortList.i4_Length = ISS_PORTLIST_LEN;
    OutPortList.i4_Length = ISS_PORTLIST_LEN;

    InPortList.pu1_OctetList = &au1InPortList[0];
    OutPortList.pu1_OctetList = &au1OutPortList[0];
    pu1String = (CHR1 *) & au1String[0];
    piIfName = (INT1 *) &au1IfName[0];

    MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1String, 0, ISS_MAX_LEN);
    MEMSET (au1Temp, 0, ISS_ADDR_LEN);
    MEMSET (au1Temp1, 0, ISS_ADDR_LEN);
    MEMSET (&RedirectsPorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (nmhValidateIndexInstanceIssExtL3FilterTable (i4NextFilter) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    u1StdAclFlag = 0;
    /* Standard ACLs can have numbers upto 1000 */
    if (i4NextFilter < 1001)
    {
        u1StdAclFlag = 1;
    }

    nmhGetIssExtL3FilterPriority (i4NextFilter, &i4FltrPrio);
    if (!u1StdAclFlag)
    {

        nmhGetIssExtL3FilterProtocol (i4NextFilter, &i4Protocol);

        AclPbGetL3Filter (i4NextFilter, &i4SVlan, &i4SVlanPrio,
                          &i4CVlan, &i4CVlanPrio, &i4TagType);
    }

    if (i4Protocol == ISS_PROT_ICMP)
    {
        nmhGetIssExtL3FilterMessageType (i4NextFilter, &i4MsgType);
        nmhGetIssExtL3FilterMessageCode (i4NextFilter, &i4MsgCode);
    }

    nmhGetIssAclL3FilteAddrType (i4NextFilter, &i4AddrType);
    nmhGetIssAclL3FilterSrcIpAddr (i4NextFilter, &SrcIpAddrOctet);

    nmhGetIssAclL3FilterSrcIpAddrPrefixLength (i4NextFilter,
                                               &u4SrcIpPrefixLength);
    IssUtlConvertPrefixToMask (u4SrcIpPrefixLength, &u4SrcIpMask);
    nmhGetIssAclL3FilterDstIpAddr (i4NextFilter, &DstIpAddrOctet);
    nmhGetIssAclL3FilterDstIpAddrPrefixLength (i4NextFilter,
                                               &u4DstIpPrefixLength);
    IssUtlConvertPrefixToMask (u4DstIpPrefixLength, &u4DstIpMask);

    nmhGetIssAclL3FilterFlowId (i4NextFilter, &u4FlowId);
    nmhGetIssAclL3FilterInPortList (i4NextFilter, &InPortList);
    nmhGetIssAclL3FilterOutPortList (i4NextFilter, &OutPortList);

    nmhGetIssAclL3FilterSubAction (i4NextFilter, &i4SubAction);
    nmhGetIssAclL3FilterSubActionId (i4NextFilter, &i4SubActionId);

    if (i4Protocol == ISS_PROT_TCP)
    {
        nmhGetIssExtL3FilterAckBit (i4NextFilter, &i4FltrAckBit);
        nmhGetIssExtL3FilterRstBit (i4NextFilter, &i4FltrRstBit);
    }

    if ((i4NextFilter > ACL_STANDARD_MAX_VALUE) &&
        (i4Protocol != ISS_PROT_ICMP))
    {
        /* TOS bits */
        nmhGetIssExtL3FilterTos (i4NextFilter, &i4FltrTos);
        nmhGetIssExtL3FilterDscp (i4NextFilter, &i4FltrDscp);
    }

    /* Source and destination protocol port ranges for TCP/UDP */
    if ((i4Protocol == ISS_PROT_TCP) || (i4Protocol == ISS_PROT_UDP))
    {
        nmhGetIssExtL3FilterMinSrcProtPort (i4NextFilter, &u4MinSrcProtPort);
        nmhGetIssExtL3FilterMaxSrcProtPort (i4NextFilter, &u4MaxSrcProtPort);
        nmhGetIssExtL3FilterMinDstProtPort (i4NextFilter, &u4MinDstProtPort);
        nmhGetIssExtL3FilterMaxDstProtPort (i4NextFilter, &u4MaxDstProtPort);
    }

    nmhGetIssExtL3FilterAction (i4NextFilter, &i4FltrAction);
    nmhGetIssExtL3FilterStatus (i4NextFilter, &i4RowStatus);

    /* Standard ACLs can have numbers upto 1000 */
    if (i4NextFilter < 1001)
    {
        CliPrintf (CliHandle,
                   "\r\nStandard IP Access List %d\r\n", i4NextFilter);
        CliPrintf (CliHandle, "----------------------------\r\n");
    }
    /* Extended ACLs can have numbers in the range 1001-65535 */
    else
    {
        CliPrintf (CliHandle,
                   "\r\nExtended IP Access List %d\r\n", i4NextFilter);
        CliPrintf (CliHandle, "-----------------------------\r\n");
    }

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1String, 0, ISS_MAX_LEN);

    CliPrintf (CliHandle, "%-33s : %d\r\n", " Filter Priority", i4FltrPrio);
    /* Filter priority and protocol type fields are not
     * configurable for Standard ACL's */
    if (!u1StdAclFlag)
    {

        CliPrintf (CliHandle, "%-33s : ", " Filter Protocol Type");
        switch (i4Protocol)
        {
            case ISS_PROT_ICMP:
                CliPrintf (CliHandle, "ICMP\r\n");
                break;
            case ISS_PROT_IGMP:
                CliPrintf (CliHandle, "IGMP\r\n");
                break;
            case ISS_PROT_GGP:
                CliPrintf (CliHandle, "GGP\r\n");
                break;
            case ISS_PROT_IP:
                CliPrintf (CliHandle, "IP\r\n");
                break;
            case ISS_PROT_TCP:
                CliPrintf (CliHandle, "TCP\r\n");
                break;
            case ISS_PROT_EGP:
                CliPrintf (CliHandle, "EGP\r\n");
                break;
            case ISS_PROT_IGP:
                CliPrintf (CliHandle, "IGP\r\n");
                break;
            case ISS_PROT_NVP:
                CliPrintf (CliHandle, "NVP\r\n");
                break;
            case ISS_PROT_UDP:
                CliPrintf (CliHandle, "UDP\r\n");
                break;
            case ISS_PROT_IRTP:
                CliPrintf (CliHandle, "IRTP\r\n");
                break;
            case ISS_PROT_IDPR:
                CliPrintf (CliHandle, "IDPR\r\n");
                break;
            case ISS_PROT_RSVP:
                CliPrintf (CliHandle, "RSVP\r\n");
                break;
            case ISS_PROT_MHRP:
                CliPrintf (CliHandle, "MHRP\r\n");
                break;
            case ISS_PROT_IGRP:
                CliPrintf (CliHandle, "IGRP\r\n");
                break;
            case ISS_PROT_OSPFIGP:
                CliPrintf (CliHandle, "OSPF\r\n");
                break;
            case ISS_PROT_PIM:
                CliPrintf (CliHandle, "PIM\r\n");
                break;
            case ISS_PROT_ANY:
                CliPrintf (CliHandle, "ANY\r\n");
                break;
            default:
                CliPrintf (CliHandle, "%d\r\n", i4Protocol);
                break;
        }
        if (i4Protocol == ISS_PROT_ICMP)
        {

            switch (i4MsgType)
            {
                case ISS_ECHO_REPLY:
                    CliPrintf (CliHandle,
                               "%-33s : Echo reply\r\n", " ICMP type");
                    break;
                case ISS_DEST_UNREACH:
                    CliPrintf (CliHandle,
                               "%-33s : Destination unreachable\r\n",
                               " ICMP type");
                    break;
                case ISS_SRC_QUENCH:
                    CliPrintf (CliHandle,
                               "%-33s : Source quench\r\n", " ICMP type");
                    break;
                case ISS_REDIRECT:
                    CliPrintf (CliHandle, "%-33s : Redirect\r\n", " ICMP type");
                    break;
                case ISS_ECHO_REQ:
                    CliPrintf (CliHandle,
                               "%-33s : Echo request\r\n", " ICMP type");
                    break;
                case ISS_TIME_EXCEED:
                    CliPrintf (CliHandle,
                               "%-33s : Time exceeded\r\n", " ICMP type");
                    break;
                case ISS_PARAM_PROB:
                    CliPrintf (CliHandle,
                               "%-33s : Parameter problem\r\n", " ICMP type");
                    break;
                case ISS_TIMEST_REQ:
                    CliPrintf (CliHandle,
                               "%-33s : Timestamp request\r\n", " ICMP type");
                    break;
                case ISS_TIMEST_REP:
                    CliPrintf (CliHandle,
                               "%-33s : Timestamp reply\r\n", " ICMP type");
                    break;
                case ISS_INFO_REQ:
                    CliPrintf (CliHandle,
                               "%-33s : Information request\r\n", " ICMP type");
                    break;
                case ISS_INTO_REP:
                    CliPrintf (CliHandle,
                               "%-33s : Information reply\r\n", " ICMP type");
                    break;
                case ISS_ADD_MK_REQ:
                    CliPrintf (CliHandle,
                               "%-33s : Address mask request\r\n",
                               " ICMP type");
                    break;
                case ISS_ADD_MK_REP:
                    CliPrintf (CliHandle,
                               "%-33s : Address mask reply\r\n", " ICMP type");
                    break;
                case ISS_NO_ICMP_TYPE:
                    CliPrintf (CliHandle,
                               "%-33s : No ICMP types to be filtered\r\n",
                               " ICMP type");
                    break;
                default:
                    CliPrintf (CliHandle,
                               "%-33s : %d\r\n", " ICMP type", i4MsgType);
                    break;
            }

            switch (i4MsgCode)
            {
                case ISS_NET_UNREACH:
                    CliPrintf (CliHandle,
                               "%-33s : Network unreachable\r\n", " ICMP code");
                    break;
                case ISS_HOST_UNREACH:
                    CliPrintf (CliHandle,
                               "%-33s : Host unreachable\r\n", " ICMP code");
                    break;
                case ISS_PROT_UNREACH:
                    CliPrintf (CliHandle,
                               "%-33s : Protocol unreachable\r\n",
                               " ICMP code");
                    break;
                case ISS_PORT_UNREACH:
                    CliPrintf (CliHandle,
                               "%-33s : Port unreachable\r\n", " ICMP code");
                    break;
                case ISS_FRG_NEED:
                    CliPrintf (CliHandle,
                               "%-33s : Fragment need\r\n", " ICMP code");
                    break;
                case ISS_SRC_RT_FAIL:
                    CliPrintf (CliHandle,
                               "%-33s : Source route failed\r\n", " ICMP code");
                    break;
                case ISS_DST_NET_UNK:
                    CliPrintf (CliHandle,
                               "%-33s : Destination network unknown\r\n",
                               " ICMP code");
                    break;
                case ISS_DST_HOST_UNK:
                    CliPrintf (CliHandle,
                               "%-33s : Destination host unknown\r\n",
                               " ICMP code");
                    break;
                case ISS_SRC_HOST_ISO:
                    CliPrintf (CliHandle,
                               "%-33s : Source host isolated\r\n",
                               " ICMP code");
                    break;
                case ISS_DST_NET_ADMP:
                    CliPrintf (CliHandle,
                               "%-33s : Destination network "
                               "administratively prohibited\r\n", " ICMP code");
                    break;
                case ISS_DST_HOST_ADMP:
                    CliPrintf (CliHandle,
                               "%-33s : Destination host"
                               "administratively prohibited\r\n", " ICMP code");
                    break;
                case ISS_NET_UNRE_TOS:
                    CliPrintf (CliHandle,
                               "%-33s : Network unreachable TOS\r\n",
                               " ICMP code");
                    break;
                case ISS_HOST_UNRE_TOS:
                    CliPrintf (CliHandle,
                               "%-33s : Host unreachable TOS\r\n",
                               " ICMP code");
                    break;
                case ISS_NO_ICMP_CODE:
                    CliPrintf (CliHandle,
                               "%-33s : No ICMP codes to be filtered\r\n",
                               " ICMP code");
                    break;
                default:
                    CliPrintf (CliHandle,
                               "%-33s : %d\r\n", " ICMP code", i4MsgCode);
                    break;
            }
        }
    }
    /*filter source IP address mask */
    if (i4AddrType == QOS_IPV6)
    {
        CliPrintf (CliHandle, "%-33s : %-17s\r\n", " IP address Type", "IPV6");
    }
    if (i4AddrType == QOS_IPV4)
    {
        CliPrintf (CliHandle, "%-33s : %-17s\r\n", " IP address Type", "IPV4");

    }

    /*filter source IP address mask */
    if (i4AddrType == QOS_IPV6)
    {
        CliPrintf (CliHandle, "%-33s : %-17s\r\n", " Source IP address",
                   Ip6PrintNtop ((tIp6Addr *) &
                                 (pIssExtL3FilterEntry->ipv6SrcIpAddress)));
    }
    else
    {
        PTR_FETCH4 (u4SrcIpAddr, SrcIpAddrOctet.pu1_OctetList);
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcIpAddr);
        CliPrintf (CliHandle,
                   "%-33s : %-17s\r\n", " Source IP address", pu1String);

    }

    MEMSET (au1String, 0, ISS_MAX_LEN);

    if (i4AddrType == QOS_IPV4)
    {
        /*filter source IP address mask */
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcIpMask);
        CliPrintf (CliHandle,
                   "%-33s : %-17s\r\n", " Source IP address mask", pu1String);
    }
    MEMSET (au1String, 0, ISS_MAX_LEN);
    CliPrintf (CliHandle,
               "%-33s : %-17d\r\n", " Source IP Prefix Length",
               u4SrcIpPrefixLength);

    /*filter destination IP address */
    if (i4AddrType == QOS_IPV6)
    {
        CliPrintf (CliHandle, "%-33s : %-17s\r\n", " Destination IP address",
                   Ip6PrintNtop ((tIp6Addr *) &
                                 (pIssExtL3FilterEntry->ipv6DstIpAddress)));
    }
    else
    {
        PTR_FETCH4 (u4DstIpAddr, DstIpAddrOctet.pu1_OctetList);
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4DstIpAddr);
        CliPrintf (CliHandle,
                   "%-33s : %-17s\r\n", " Destination IP address", pu1String);
    }

    MEMSET (au1String, 0, ISS_MAX_LEN);
    if (i4AddrType == QOS_IPV4)
    {
        /*filter destination IP address */
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4DstIpMask);
        CliPrintf (CliHandle,
                   "%-33s : %-17s\r\n", " Destination IP address mask",
                   pu1String);
    }
    MEMSET (au1String, 0, ISS_MAX_LEN);
    CliPrintf (CliHandle,
               "%-33s : %-17d\r\n", " Destination IP Prefix Length",
               u4DstIpPrefixLength);

    CliPrintf (CliHandle, "%-33s : %-17d\r\n", " Flow Identifier ", u4FlowId);

    /* in ports */

    CliPrintf (CliHandle, "%-33s : ", " In Port List");

    i4Count = 0;
    i4CommaCount = 0;
    do
    {
        if (CliIsMemberPort (InPortList.pu1_OctetList,
                             ISS_PORTLIST_LEN, i4Count) == CLI_SUCCESS)
        {
            i4CommaCount++;
            CfaCliGetIfName ((UINT4) i4Count, piIfName);
            /* This function will format the display of the port list */
            AclCliPrintPortList (CliHandle, i4CommaCount, (UINT1 *) piIfName);
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        }

        i4Count++;
    }
    while (i4Count <= SYS_DEF_MAX_PHYSICAL_INTERFACES);
    /*If no port list has been configured */
    if (i4CommaCount == 0)
    {
        CliPrintf (CliHandle, "NIL");
    }

    MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
    CliPrintf (CliHandle, "\r\n");

    /* out ports */

    CliPrintf (CliHandle, "%-33s : ", " Out Port List");
    i4Count = 0;
    i4CommaCount = 0;
    do
    {
        if (CliIsMemberPort (OutPortList.pu1_OctetList,
                             ISS_PORTLIST_LEN, i4Count) == CLI_SUCCESS)
        {
            i4CommaCount++;
            CfaCliGetIfName ((UINT4) i4Count, piIfName);
            AclCliPrintPortList (CliHandle, i4CommaCount, (UINT1 *) piIfName);
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        }

        i4Count++;
    }
    while (i4Count <= SYS_DEF_MAX_PHYSICAL_INTERFACES);
    /*If no port list has been configured */
    if (i4CommaCount == 0)
    {
        CliPrintf (CliHandle, "NIL");
    }

    MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);
    CliPrintf (CliHandle, "\r\n");

    if (i4Protocol == ISS_PROT_TCP)
    {
        /* ACK bit */
        if (i4FltrAckBit == ISS_ACK_ESTABLISH)
        {
            CliPrintf (CliHandle, " Filtering ACK bit\r\n");
        }

        /* RST bit */
        if (i4FltrRstBit == ISS_RST_SET)
        {
            CliPrintf (CliHandle, " Filtering RST bit\r\n");
        }

    }
    /* Display TOS if  access list is extended and protocol
     * is not ICMP 
     */
    if ((i4NextFilter > ACL_STANDARD_MAX_VALUE) &&
        (i4Protocol != ISS_PROT_ICMP))
    {

        switch (i4FltrTos)
        {
            case ISS_TOS_NONE:
                CliPrintf (CliHandle, "%-33s : None\r\n", " Filter TOS");
                break;
            case ISS_TOS_HI_REL:
                CliPrintf (CliHandle, "%-33s : High reliablity\r\n",
                           " Filter TOS");
                break;
            case ISS_TOS_HI_THR:
                CliPrintf (CliHandle, "%-33s : High throughput\r\n",
                           " Filter TOS");
                break;
            case ISS_TOS_HI_REL_HI_THR:
                CliPrintf (CliHandle, "%-33s : High reliability"
                           " and high throughput\r\n", " Filter TOS");
                break;
            case ISS_TOS_LO_DEL:
                CliPrintf (CliHandle, "%-33s : Low delay\r\n", " Filter TOS");
                break;
            case ISS_TOS_LO_DEL_HI_REL:
                CliPrintf (CliHandle, "%-33s : Low delay and high"
                           " reliability\r\n", " Filter TOS");
                break;
            case ISS_TOS_LO_DEL_HI_THR:
                CliPrintf (CliHandle, "%-33s : Low delay and high"
                           " throughput\r\n", " Filter TOS");
                break;
            case ISS_TOS_LO_DEL_HI_THR_HI_REL:
                CliPrintf (CliHandle, "%-33s : Low delay, high"
                           " throughput and high reliability\r\n",
                           " Filter TOS");
                break;
            default:
                CliPrintf (CliHandle, "%-33s : Invalid combination\r\n",
                           " Filter TOS");
                break;
        }

        if (i4FltrDscp == ISS_DSCP_INVALID)
        {
            CliPrintf (CliHandle, "%-33s : NIL\r\n", " Filter DSCP");
        }
        else
        {
            CliPrintf (CliHandle, "%-33s : %d\r\n", " Filter DSCP", i4FltrDscp);
        }
    }

    /* Source and destination protocol port ranges for TCP/UDP */
    if ((i4Protocol == ISS_PROT_TCP) || (i4Protocol == ISS_PROT_UDP))
    {
        CliPrintf (CliHandle,
                   "%-33s : %d\r\n",
                   " Filter Source Ports From", u4MinSrcProtPort);

        CliPrintf (CliHandle, "%-33s : %d\r\n",
                   " Filter Source Ports Till", u4MaxSrcProtPort);

        CliPrintf (CliHandle, "%-33s : %d\r\n",
                   " Filter Destination Ports From", u4MinDstProtPort);

        CliPrintf (CliHandle, "%-33s : %d\r\n",
                   " Filter Destination Ports Till", u4MaxDstProtPort);
    }

    if (!u1StdAclFlag)
    {
        AclPbShowL3Filter (CliHandle, i4SVlan, i4SVlanPrio, i4CVlan,
                           i4CVlanPrio, i4TagType);
    }
    /* filter action */
    if (i4FltrAction == ISS_ALLOW)
    {
        CliPrintf (CliHandle, "%-33s : Permit\r\n", " Filter Action");
    }
    else if (i4FltrAction == ISS_DROP)
    {
        CliPrintf (CliHandle, "%-33s : Deny\r\n", " Filter Action");
    }
    else if (i4FltrAction == ISS_REDIRECT_TO)
    {
        CliPrintf (CliHandle, "%-33s : Redirect\r\n", " Filter Action");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Unknown\r\n", " Filter Action");
    }

    /* Print Redirect Port List */

    if (i4FltrAction == ISS_REDIRECT_TO)
    {
        nmhGetIssAclL3FilterRedirectId (i4NextFilter,
                                        (INT4 *) &u4NewRedirectGrpIndex);
    }
    i4CommaCount = 0;
    CliPrintf (CliHandle, "%-33s : ", " Redirect Port List");
    if (u4NewRedirectGrpIndex != 0)
    {
        pPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

        if (pPorts == NULL)
        {
            return CLI_FAILURE;
        }

        MEMSET (pPorts, 0, sizeof (tPortList));

        RedirectsPorts.pu1_OctetList = *pPorts;
        RedirectsPorts.i4_Length = sizeof (tPortList);

        nmhGetIssRedirectInterfaceGrpType (u4NewRedirectGrpIndex,
                                           &i4RedirectInterfaceGrpType);

        nmhGetIssRedirectInterfaceGrpPortList (u4NewRedirectGrpIndex,
                                               &RedirectsPorts);
        nmhGetIssRedirectInterfaceGrpDistByte (u4NewRedirectGrpIndex,
                                               &i4TrafficDistByte);

        i4Count = 0;
        i4CommaCount = 0;

        while ((i4Count < ISS_REDIRECT_MAX_PORTS) &&
               (i4RedirectInterfaceGrpType == ISS_REDIRECT_TO_PORTLIST))
        {
            if (CliIsMemberPort (RedirectsPorts.pu1_OctetList,
                                 ISS_REDIRECT_PORT_LIST_SIZE,
                                 i4Count) == CLI_SUCCESS)
            {
                i4CommaCount++;
                CfaCliGetIfName ((UINT4) i4Count, piIfName);
                AclCliPrintPortList (CliHandle, i4CommaCount,
                                     (UINT1 *) piIfName);
                MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
            }

            i4Count++;
        }

        if (i4RedirectInterfaceGrpType != ISS_REDIRECT_TO_PORTLIST)
        {
            MEMCPY (&u4InterfaceIndex, RedirectsPorts.pu1_OctetList,
                    sizeof (UINT4));

            i4CommaCount++;
            CfaCliGetIfName (u4InterfaceIndex, piIfName);
            AclCliPrintPortList (CliHandle, i4CommaCount, (UINT1 *) piIfName);
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        }

        FsUtilReleaseBitList (*pPorts);
    }

    /*If no port list has been configured */
    if (i4CommaCount == 0)
    {
        CliPrintf (CliHandle, "NIL");
    }

    CliPrintf (CliHandle, "\r\n");

    switch (i4TrafficDistByte)
    {
        case ISS_REDIRECT_UDB_FIELD:

            CliPrintf (CliHandle, "%-33s : UDB", " TrafficDistField");
            break;
        case ISS_TRAFF_DIST_BYTE_SRCIP:
            CliPrintf (CliHandle, "%-33s : SRC IP", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_DSTIP:
            CliPrintf (CliHandle, "%-33s : DST IP", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_SMAC:
            CliPrintf (CliHandle, "%-33s : SRC MAC", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_DMAC:
            CliPrintf (CliHandle, "%-33s : DST MAC", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_STCPPORT:
            CliPrintf (CliHandle, "%-33s : SRC TCP PORT", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_DTCPPORT:
            CliPrintf (CliHandle, "%-33s : DST TCP PORT", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_SUDPPORT:
            CliPrintf (CliHandle, "%-33s : SRC UDP PORT", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_DUDPPORT:
            CliPrintf (CliHandle, "%-33s : DST UDP PORT", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_VLANID:
            CliPrintf (CliHandle, "%-33s : VLAN ID", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_ETHERTYPE:
            CliPrintf (CliHandle, "%-33s : ETHER-TYPE", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_IPPROTOCOL:
            CliPrintf (CliHandle, "%-33s : INNER IP", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_INNER_SRCIP:
            CliPrintf (CliHandle, "%-33s : INNER SRCIP", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_INNER_DESTIP:

            CliPrintf (CliHandle, "%-33s : INNER DSTIP", " TrafficDistField");
            break;
        default:

            CliPrintf (CliHandle, "%-33s : Unknown", " TrafficDistField");

    }

    CliPrintf (CliHandle, "\r\n");

    if (i4SubAction == ISS_NONE)
    {
        CliPrintf (CliHandle, "%-33s : NONE", " Sub Action");
    }
    else if (i4SubAction == ISS_MODIFY_VLAN)
    {
        CliPrintf (CliHandle, "%-33s : MODIFY VLAN", " Sub Action");
    }
    else if (i4SubAction == ISS_NESTED_VLAN)
    {
        CliPrintf (CliHandle, "%-33s : NESTED VLAN", " Sub Action");
    }
    else if (i4SubAction == ISS_STRIP_OUTER_HDR)
    {
        CliPrintf (CliHandle, "%-33s : STRIP OUTER HEADER", " Sub Action");
    }

    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "%-33s : %d\r\n", " Sub Action Id", i4SubActionId);

    if (i4RowStatus == ISS_ACTIVE)
    {
        CliPrintf (CliHandle, "%-33s : Active", " Status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : InActive", " Status");
    }
    CliPrintf (CliHandle, "\r\n\r\n");
    return CLI_SUCCESS;
}

INT4
AclShowL2Filter (tCliHandle CliHandle, INT4 i4NextFilter)
{
    tMacAddr            DstMacAddr;
    tMacAddr            SrcMacAddr;
    INT4                i4FltrPrio;
    INT4                i4Protocol;
    INT4                i4VlanId;
    INT4                i4FltrAction;
    INT4                i4RowStatus;
    INT4                i4Count;
    INT4                i4CommaCount;
    INT4                i4OuterEType = 0;
    INT4                i4EtherType;
    INT4                i4SVlan = ACL_DEF_SVLAN;
    INT4                i4SVlanPrio = ACL_DEF_SVLAN_PRIO;
    INT4                i4CVlanPrio = ACL_DEF_CVLAN_PRIO;
    INT4                i4TagType = ISS_FILTER_SINGLE_TAG;
    INT4                i4TrafficDistByte = 0;
    INT4                i4RedirectInterfaceGrpType = 0;
    INT4                i4SubAction = 0;
    INT4                i4SubActionId = 0;
    UINT4               u4NewRedirectGrpIndex = 0;
    UINT4               u4InterfaceIndex = 0;
    CHR1               *pu1String;
    INT1               *piIfName;
    UINT1               au1String[ISS_MAX_LEN];
    UINT1               au1InPortList[ISS_PORTLIST_LEN];
    UINT1               au1OutPortList[ISS_PORTLIST_LEN];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    tSNMP_OCTET_STRING_TYPE InPortList;
    tSNMP_OCTET_STRING_TYPE OutPortList;
    tSNMP_OCTET_STRING_TYPE RedirectsPorts;
    tPortList          *pPorts = NULL;

    InPortList.i4_Length = ISS_PORTLIST_LEN;
    OutPortList.i4_Length = ISS_PORTLIST_LEN;

    InPortList.pu1_OctetList = &au1InPortList[0];
    OutPortList.pu1_OctetList = &au1OutPortList[0];
    pu1String = (CHR1 *) & au1String[0];
    piIfName = (INT1 *) &au1IfName[0];

    MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1String, 0, ISS_MAX_LEN);
    MEMSET (&RedirectsPorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (nmhValidateIndexInstanceIssExtL2FilterTable (i4NextFilter) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhGetIssExtL2FilterPriority (i4NextFilter, &i4FltrPrio);
    nmhGetIssExtL2FilterEtherType (i4NextFilter, &i4EtherType);
    nmhGetIssExtL2FilterProtocolType (i4NextFilter, (UINT4 *) &i4Protocol);
    nmhGetIssExtL2FilterVlanId (i4NextFilter, &i4VlanId);
    nmhGetIssExtL2FilterDstMacAddr (i4NextFilter, (tMacAddr *) & DstMacAddr);
    nmhGetIssExtL2FilterSrcMacAddr (i4NextFilter, (tMacAddr *) & SrcMacAddr);
    nmhGetIssExtL2FilterInPortList (i4NextFilter, &InPortList);
    nmhGetIssExtL2FilterOutPortList (i4NextFilter, &OutPortList);

    AclPbGetL2Filter (i4NextFilter, &i4OuterEType, &i4SVlan,
                      &i4SVlanPrio, &i4CVlanPrio, &i4TagType);

    nmhGetIssExtL2FilterAction (i4NextFilter, &i4FltrAction);
    nmhGetIssExtL2FilterStatus (i4NextFilter, &i4RowStatus);
    nmhGetIssAclL2FilterSubAction (i4NextFilter, &i4SubAction);
    nmhGetIssAclL2FilterSubActionId (i4NextFilter, &i4SubActionId);

    CliPrintf (CliHandle, "\r\nExtended MAC Access List %d\r\n", i4NextFilter);
    CliPrintf (CliHandle, "-----------------------------\r\n");

    CliPrintf (CliHandle, "%-33s : %d\r\n", " Filter Priority", i4FltrPrio);

    CliPrintf (CliHandle, "%-33s : %d\r\n", " Ether Type", i4EtherType);

    CliPrintf (CliHandle, "%-33s : ", " Protocol Type");
    switch (i4Protocol)
    {
        case AARP:
            CliPrintf (CliHandle, "AARP\r\n");
            break;

        case AMBER:
            CliPrintf (CliHandle, "AMBER\r\n");
            break;

        case DEC_SPANNING:
            CliPrintf (CliHandle, "DEC_SPANNING\r\n");
            break;

        case DIAGNOSTIC:
            CliPrintf (CliHandle, "DIAGNOSTIC\r\n");
            break;

        case DSM:
            CliPrintf (CliHandle, "DSM\r\n");
            break;

        case ETYPE_6000:
            CliPrintf (CliHandle, "ETYPE_6000\r\n");
            break;

        case ETYPE_8042:
            CliPrintf (CliHandle, "ETYPE_8042\r\n");
            break;

        case LAT:
            CliPrintf (CliHandle, "LAT\r\n");
            break;

        case LAVC_SCA:
            CliPrintf (CliHandle, "LAVC_SCA\r\n");
            break;

        case MOP_CONSOLE:
            CliPrintf (CliHandle, "MOP_CONSOLE\r\n");
            break;

        case MSDOS:
            CliPrintf (CliHandle, "MSDOS\r\n");
            break;

        case MUMPS:
            CliPrintf (CliHandle, "MUMPS\r\n");
            break;

        case NET_BIOS:
            CliPrintf (CliHandle, "NET_BIOS\r\n");
            break;

        case VINES_ECHO:
            CliPrintf (CliHandle, "VINES_ECHO\r\n");
            break;

        case VINES_IP:
            CliPrintf (CliHandle, "VINES_IP\r\n");
            break;

        case XNS_ID:
            CliPrintf (CliHandle, "XNS_ID\r\n");
            break;

        default:
            CliPrintf (CliHandle, "%d\r\n", i4Protocol);
            break;
    }

    CliPrintf (CliHandle, "%-33s : %d\r\n", " Vlan Id", i4VlanId);

    CliPrintf (CliHandle, "%-33s : ", " Destination MAC Address");
    PrintMacAddress (DstMacAddr, (UINT1 *) pu1String);
    CliPrintf (CliHandle, "%s", pu1String);
    MEMSET (au1String, 0, ISS_MAX_LEN);
    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "%-33s : ", " Source MAC Address");
    PrintMacAddress (SrcMacAddr, (UINT1 *) pu1String);
    CliPrintf (CliHandle, "%s", pu1String);
    MEMSET (au1String, 0, ISS_MAX_LEN);
    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "%-33s : ", " In Port List");
    i4Count = 0;
    i4CommaCount = 0;
    do
    {
        if (CliIsMemberPort (InPortList.pu1_OctetList,
                             ISS_PORTLIST_LEN, i4Count) == CLI_SUCCESS)
        {
            i4CommaCount++;
            CfaCliGetIfName ((UINT4) i4Count, piIfName);
            /* This function will format the display of the port list */
            AclCliPrintPortList (CliHandle, i4CommaCount, (UINT1 *) piIfName);
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        }

        i4Count++;
    }
    while (i4Count <= SYS_DEF_MAX_PHYSICAL_INTERFACES);

    /*If no port list has been configured */
    if (i4CommaCount == 0)
    {
        CliPrintf (CliHandle, "NIL");
    }

    MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
    CliPrintf (CliHandle, "\r\n");

    /* out ports */
    CliPrintf (CliHandle, "%-33s : ", " Out Port List");
    i4Count = 0;
    i4CommaCount = 0;
    do
    {
        if (CliIsMemberPort (OutPortList.pu1_OctetList,
                             ISS_PORTLIST_LEN, i4Count) == CLI_SUCCESS)
        {
            i4CommaCount++;
            CfaCliGetIfName ((UINT4) i4Count, piIfName);
            /* This function will format the display of the port list */
            AclCliPrintPortList (CliHandle, i4CommaCount, (UINT1 *) piIfName);
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        }

        i4Count++;
    }
    while (i4Count <= SYS_DEF_MAX_PHYSICAL_INTERFACES);

    /*If no port list has been configured */
    if (i4CommaCount == 0)
    {
        CliPrintf (CliHandle, "NIL");
    }

    MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);
    CliPrintf (CliHandle, "\r\n");

    AclPbShowL2Filter (CliHandle, i4OuterEType, i4SVlan, i4SVlanPrio,
                       i4CVlanPrio, i4TagType);

    if (i4FltrAction == ISS_ALLOW)
    {
        CliPrintf (CliHandle, "%-33s : Permit\r\n", " Filter Action");
    }
    else if (i4FltrAction == ISS_DROP)
    {
        CliPrintf (CliHandle, "%-33s : Deny\r\n", " Filter Action");
    }
    else if (i4FltrAction == ISS_REDIRECT_TO)
    {
        CliPrintf (CliHandle, "%-33s : Redirect\r\n", " Filter Action");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Unknown\r\n", " Filter Action");
    }
    /* Print Redirect Port List */

    if (i4FltrAction == ISS_REDIRECT_TO)
    {
        nmhGetIssAclL2FilterRedirectId (i4NextFilter,
                                        (INT4 *) &u4NewRedirectGrpIndex);
    }
    i4CommaCount = 0;
    CliPrintf (CliHandle, "%-33s : ", " Redirect Port List");

    if (u4NewRedirectGrpIndex != 0)
    {
        pPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

        if (pPorts == NULL)
        {
            return CLI_FAILURE;
        }

        MEMSET (pPorts, 0, sizeof (tPortList));

        RedirectsPorts.pu1_OctetList = *pPorts;
        RedirectsPorts.i4_Length = sizeof (tPortList);

        nmhGetIssRedirectInterfaceGrpType (u4NewRedirectGrpIndex,
                                           &i4RedirectInterfaceGrpType);

        nmhGetIssRedirectInterfaceGrpPortList (u4NewRedirectGrpIndex,
                                               &RedirectsPorts);
        nmhGetIssRedirectInterfaceGrpDistByte (u4NewRedirectGrpIndex,
                                               &i4TrafficDistByte);

        i4Count = 0;
        i4CommaCount = 0;

        while ((i4Count < ISS_REDIRECT_MAX_PORTS) &&
               (i4RedirectInterfaceGrpType == ISS_REDIRECT_TO_PORTLIST))
        {
            if (CliIsMemberPort (RedirectsPorts.pu1_OctetList,
                                 ISS_REDIRECT_PORT_LIST_SIZE,
                                 i4Count) == CLI_SUCCESS)
            {
                i4CommaCount++;
                CfaCliGetIfName ((UINT4) i4Count, piIfName);
                AclCliPrintPortList (CliHandle, i4CommaCount,
                                     (UINT1 *) piIfName);
                MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
            }

            i4Count++;
        }

        if (i4RedirectInterfaceGrpType != ISS_REDIRECT_TO_PORTLIST)
        {
            MEMCPY (&u4InterfaceIndex, RedirectsPorts.pu1_OctetList,
                    sizeof (UINT4));

            i4CommaCount++;
            CfaCliGetIfName (u4InterfaceIndex, piIfName);
            AclCliPrintPortList (CliHandle, i4CommaCount, (UINT1 *) piIfName);
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        }

        FsUtilReleaseBitList (*pPorts);
    }

    /*If no port list has been configured */
    if (i4CommaCount == 0)
    {
        CliPrintf (CliHandle, "NIL");
    }

    CliPrintf (CliHandle, "\r\n");

    switch (i4TrafficDistByte)
    {
        case ISS_REDIRECT_UDB_FIELD:

            CliPrintf (CliHandle, "%-33s : UDB", " TrafficDistField");
            break;
        case ISS_TRAFF_DIST_BYTE_SRCIP:
            CliPrintf (CliHandle, "%-33s : SRC IP", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_DSTIP:
            CliPrintf (CliHandle, "%-33s : DST IP", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_SMAC:
            CliPrintf (CliHandle, "%-33s : SRC MAC", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_DMAC:
            CliPrintf (CliHandle, "%-33s : DST MAC", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_STCPPORT:
            CliPrintf (CliHandle, "%-33s : SRC TCP PORT", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_DTCPPORT:
            CliPrintf (CliHandle, "%-33s : DST TCP PORT", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_SUDPPORT:
            CliPrintf (CliHandle, "%-33s : SRC UDP PORT", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_DUDPPORT:
            CliPrintf (CliHandle, "%-33s : DST UDP PORT", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_VLANID:
            CliPrintf (CliHandle, "%-33s : VLAN ID", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_ETHERTYPE:
            CliPrintf (CliHandle, "%-33s : ETHER-TYPE", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_IPPROTOCOL:
            CliPrintf (CliHandle, "%-33s : INNER IP", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_INNER_SRCIP:
            CliPrintf (CliHandle, "%-33s : INNER SRCIP", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_INNER_DESTIP:

            CliPrintf (CliHandle, "%-33s : INNER DSTIP", " TrafficDistField");
            break;
        default:

            CliPrintf (CliHandle, "%-33s : Unknown", " TrafficDistField");

    }

    CliPrintf (CliHandle, "\r\n");

    if (i4SubAction == ISS_NONE)
    {
        CliPrintf (CliHandle, "%-33s : NONE", " Sub Action");
    }
    else if (i4SubAction == ISS_MODIFY_VLAN)
    {
        CliPrintf (CliHandle, "%-33s : MODIFY VLAN", " Sub Action");
    }
    else if (i4SubAction == ISS_NESTED_VLAN)
    {
        CliPrintf (CliHandle, "%-33s : NESTED VLAN", " Sub Action");
    }
    else if (i4SubAction == ISS_STRIP_OUTER_HDR)
    {
        CliPrintf (CliHandle, "%-33s : STRIP OUTER HEADER", " Sub Action");
    }

    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "%-33s : %d\r\n", " Sub Action Id", i4SubActionId);

    if (i4RowStatus == ISS_ACTIVE)
    {
        CliPrintf (CliHandle, "%-33s : Active", " Status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : InActive", " Status");
    }
    CliPrintf (CliHandle, "\r\n\r\n");
    return CLI_SUCCESS;
}

INT4
AclShowUDBFilter (tCliHandle CliHandle, INT4 i4NextFilter)
{
    INT4                i4PktType = 0;
    INT4                i4Base = 0;
    INT4                i4Priority = 0;
    INT4                i4FilterOneType = 0;
    INT4                i4FilterOneId = 0;
    INT4                i4FilterTwoType = 0;
    INT4                i4FilterTwoId = 0;
    INT4                i4Action = 0;
    INT4                i4RowStatus;
    INT4                i4RedirectInterfaceGrpType = 0;
    INT4                i4SubActionId = 0;
    INT4                i4SubAction = 0;
    INT4                i4Count = 0;
    INT4                i4CommaCount = 0;
    INT4                i4TrafficDistByte = 0;
    INT4                i4RedirectUdbPosition = 0;
    INT4                i4Length = 0;
    INT4                i4ColonCount = 0;
    UINT4               u4NewRedirectGrpIndex = 0;
    UINT4               u4InterfaceIndex = 0;
    CHR1               *pu1String;
    INT1               *piIfName;
    UINT1               au1String[ISS_MAX_LEN];
    UINT1               au1InPortList[ISS_PORTLIST_LEN];
    UINT1               au1SetValue[ISS_UDB_MAX_OFFSET];
    UINT1               au1SetMask[ISS_UDB_MAX_OFFSET];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    tSNMP_OCTET_STRING_TYPE InPortList;
    tSNMP_OCTET_STRING_TYPE SetValue;
    tSNMP_OCTET_STRING_TYPE SetMask;
    tSNMP_OCTET_STRING_TYPE RedirectsPorts;
    tPortList          *pPorts = NULL;

    MEMSET (au1SetValue, 0, ISS_UDB_MAX_OFFSET);
    MEMSET (au1SetMask, 0, ISS_UDB_MAX_OFFSET);

    InPortList.i4_Length = ISS_PORTLIST_LEN;
    SetValue.i4_Length = ISS_PORTLIST_LEN;
    SetMask.i4_Length = ISS_PORTLIST_LEN;

    InPortList.pu1_OctetList = &au1InPortList[0];
    SetValue.pu1_OctetList = &au1SetValue[0];
    SetMask.pu1_OctetList = &au1SetMask[0];
    pu1String = (CHR1 *) & au1String[0];
    piIfName = (INT1 *) &au1IfName[0];
    MEMSET (SetValue.pu1_OctetList, 0, sizeof (SetValue.pu1_OctetList));

    MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1String, 0, ISS_MAX_LEN);
    MEMSET (&RedirectsPorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (nmhValidateIndexInstanceIssAclUserDefinedFilterTable (i4NextFilter) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhGetIssAclUserDefinedFilterPktType (i4NextFilter, &i4PktType);
    nmhGetIssAclUserDefinedFilterOffSetBase (i4NextFilter, &i4Base);
    nmhGetIssAclUserDefinedFilterOffSetValue (i4NextFilter, &SetValue);
    nmhGetIssAclUserDefinedFilterOffSetMask (i4NextFilter, &SetMask);
    nmhGetIssAclUserDefinedFilterPriority (i4NextFilter, &i4Priority);
    nmhGetIssAclUserDefinedFilterAction (i4NextFilter, &i4Action);
    nmhGetIssAclUserDefinedFilterInPortList (i4NextFilter, &InPortList);
    nmhGetIssAclUserDefinedFilterIdOneType (i4NextFilter, &i4FilterOneType);
    nmhGetIssAclUserDefinedFilterIdOne (i4NextFilter, (UINT4 *) &i4FilterOneId);
    nmhGetIssAclUserDefinedFilterIdTwoType (i4NextFilter, &i4FilterTwoType);
    nmhGetIssAclUserDefinedFilterIdTwo (i4NextFilter, (UINT4 *) &i4FilterTwoId);
    nmhGetIssAclUserDefinedFilterStatus (i4NextFilter, &i4RowStatus);

    nmhGetIssAclUserDefinedFilterSubAction (i4NextFilter, &i4SubAction);
    nmhGetIssAclUserDefinedFilterSubActionId (i4NextFilter, &i4SubActionId);

    CliPrintf (CliHandle, "\r\n User Defined Access List %d\r\n", i4NextFilter);
    CliPrintf (CliHandle, "-----------------------------\r\n");

    CliPrintf (CliHandle, "%-33s : %d\r\n", " Priority", i4Priority);
    CliPrintf (CliHandle, "%-33s : ", " Packet Type");
    switch (i4PktType)
    {
        case ISS_UDB_PKT_TYPE_USER_DEF:
            CliPrintf (CliHandle, "User-Defined\r\n");
            break;

        case ISS_UDB_PKT_TYPE_IPV4_TCP:
            CliPrintf (CliHandle, "TCP\r\n");
            break;

        case ISS_UDB_PKT_TYPE_IPV4_UDP:
            CliPrintf (CliHandle, "UDP\r\n");
            break;

        case ISS_UDB_PKT_TYPE_MPLS:
            CliPrintf (CliHandle, "MPLS\r\n");
            break;

        case ISS_UDB_PKT_TYPE_IPV4:
            CliPrintf (CliHandle, "IPv4\r\n");
            break;

        case ISS_UDB_PKT_TYPE_IPV6:
            CliPrintf (CliHandle, "IPv6\r\n");
            break;

        case ISS_UDB_PKT_TYPE_FRAG:
            CliPrintf (CliHandle, "FRAG-IP\r\n");
            break;

        default:
            CliPrintf (CliHandle, "%d\r\n", i4PktType);
            break;
    }

    CliPrintf (CliHandle, "%-33s : ", " Offset Base");
    switch (i4Base)
    {
        case ISS_UDB_OFFSET_L2:
            CliPrintf (CliHandle, "L2\r\n");
            break;

        case ISS_UDB_OFFSET_L3:
            CliPrintf (CliHandle, "L3\r\n");
            break;

        case ISS_UDB_OFFSET_L4:
            CliPrintf (CliHandle, "L4\r\n");
            break;

        case ISS_UDB_OFFSET_IPV6_EXT_HDR:
            CliPrintf (CliHandle, "IPV6\r\n");
            break;

        case ISS_UDB_OFFSET_L3_MINUS_2:
            CliPrintf (CliHandle, "Ether-Type\r\n");
            break;

        case ISS_UDB_OFFSET_MPLS_MINUS_2:
            CliPrintf (CliHandle, "Ether-Type\r\n");
            break;

        default:
            CliPrintf (CliHandle, "%d\r\n", i4Base);
            break;
    }

    CliPrintf (CliHandle, "%-33s", " OffSet Position");
    CliPrintf (CliHandle, " : ", "");
    i4ColonCount = 0;
    for (i4Length = 0; i4Length < SetValue.i4_Length; i4Length++)
    {
        if (au1SetMask[i4Length] != 0)

        {
            if (i4ColonCount != 0)
            {
                CliPrintf (CliHandle, ":");
            }
            /*Display in HEX:"%02x:" */
            CliPrintf (CliHandle, "%02d", i4Length);
            i4ColonCount++;
        }
    }
    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "%-33s", " OffSet Value");
    CliPrintf (CliHandle, " : ", "");
    i4ColonCount = 0;
    for (i4Length = 0; i4Length < SetValue.i4_Length; i4Length++)
    {
        if (au1SetMask[i4Length] != 0)

        {
            if (i4ColonCount != 0)
            {
                CliPrintf (CliHandle, ":");
            }
            /*Display in HEX:"%02x:" */
            CliPrintf (CliHandle, "%02d", au1SetValue[i4Length]);
            i4ColonCount++;
        }
    }
    CliPrintf (CliHandle, "\r\n");

    if (i4Action == ISS_ALLOW)
    {
        CliPrintf (CliHandle, "%-33s : Permit\r\n", " Filter Action");
    }
    else if (i4Action == ISS_DROP)
    {
        CliPrintf (CliHandle, "%-33s : Deny\r\n", " Filter Action");
    }
    else if (i4Action == ISS_AND)
    {
        CliPrintf (CliHandle, "%-33s : AND\r\n", " Filter Action");
    }
    else if (i4Action == ISS_OR)
    {
        CliPrintf (CliHandle, "%-33s : OR\r\n", " Filter Action");
    }
    else if (i4Action == ISS_NOT)
    {
        CliPrintf (CliHandle, "%-33s : NOT\r\n", " Filter Action");
    }
    else if (i4Action == ISS_REDIRECT_TO)
    {
        CliPrintf (CliHandle, "%-33s : Redirect\r\n", " Filter Action");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Unknown\r\n", " Filter Action");
    }

    CliPrintf (CliHandle, "%-33s : ", " In Port List");
    i4Count = 0;
    i4CommaCount = 0;
    do
    {
        if (CliIsMemberPort (InPortList.pu1_OctetList,
                             ISS_PORTLIST_LEN, i4Count) == CLI_SUCCESS)
        {
            i4CommaCount++;
            CfaCliGetIfName ((UINT4) i4Count, piIfName);
            /* This function will format the display of the port list */
            AclCliPrintPortList (CliHandle, i4CommaCount, (UINT1 *) piIfName);
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        }

        i4Count++;
    }
    while (i4Count <= SYS_DEF_MAX_PHYSICAL_INTERFACES);

    /* Print Redirect Port List */

    if (i4Action == ISS_REDIRECT_TO)
    {
        nmhGetIssAclUserDefinedFilterRedirectId (i4NextFilter,
                                                 (INT4 *)
                                                 &u4NewRedirectGrpIndex);
    }

    /*If no port list has been configured */
    if (i4CommaCount == 0)
    {
        CliPrintf (CliHandle, "NIL");
    }

    MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
    CliPrintf (CliHandle, "\r\n");

    if (i4FilterOneType == ISS_L2_FILTER)
    {
        CliPrintf (CliHandle, "%-33s : L2 Filter\r\n", " Filter One Type");
    }
    else if (i4FilterOneType == ISS_L3_FILTER)
    {
        CliPrintf (CliHandle, "%-33s : L3 Filter\r\n", " Filter One Type");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : None\r\n", " Filter One Type");
    }

    CliPrintf (CliHandle, "%-33s : %d\r\n", " Filter Id", i4FilterOneId);

    if (i4FilterTwoType == ISS_L2_FILTER)
    {
        CliPrintf (CliHandle, "%-33s : L2 Filter\r\n", " Filter Two Type");
    }
    else if (i4FilterTwoType == ISS_L3_FILTER)
    {
        CliPrintf (CliHandle, "%-33s : L3 Filter\r\n", " Filter Two Type");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : None\r\n", " Filter Two Type");
    }

    CliPrintf (CliHandle, "%-33s : %d\r\n", " Filter Id", i4FilterTwoId);

    i4CommaCount = 0;
    CliPrintf (CliHandle, "%-33s : ", " Redirect Port List");

    if (u4NewRedirectGrpIndex != 0)
    {
        pPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

        if (pPorts == NULL)
        {
            return CLI_FAILURE;
        }

        MEMSET (pPorts, 0, sizeof (tPortList));

        RedirectsPorts.pu1_OctetList = *pPorts;
        RedirectsPorts.i4_Length = sizeof (tPortList);

        nmhGetIssRedirectInterfaceGrpType (u4NewRedirectGrpIndex,
                                           &i4RedirectInterfaceGrpType);

        nmhGetIssRedirectInterfaceGrpPortList (u4NewRedirectGrpIndex,
                                               &RedirectsPorts);
        nmhGetIssRedirectInterfaceGrpDistByte (u4NewRedirectGrpIndex,
                                               &i4TrafficDistByte);

        nmhGetIssRedirectInterfaceGrpUdbPosition (u4NewRedirectGrpIndex,
                                                  &i4RedirectUdbPosition);

        i4Count = 0;
        i4CommaCount = 0;

        while ((i4Count < ISS_REDIRECT_MAX_PORTS) &&
               (i4RedirectInterfaceGrpType == ISS_REDIRECT_TO_PORTLIST))
        {
            if (CliIsMemberPort (RedirectsPorts.pu1_OctetList,
                                 ISS_REDIRECT_PORT_LIST_SIZE,
                                 i4Count) == CLI_SUCCESS)
            {
                i4CommaCount++;
                CfaCliGetIfName ((UINT4) i4Count, piIfName);
                AclCliPrintPortList (CliHandle, i4CommaCount,
                                     (UINT1 *) piIfName);
                MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
            }

            i4Count++;
        }

        if (i4RedirectInterfaceGrpType != ISS_REDIRECT_TO_PORTLIST)
        {
            MEMCPY (&u4InterfaceIndex, RedirectsPorts.pu1_OctetList,
                    sizeof (UINT4));

            i4CommaCount++;
            CfaCliGetIfName (u4InterfaceIndex, piIfName);
            AclCliPrintPortList (CliHandle, i4CommaCount, (UINT1 *) piIfName);
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        }

        FsUtilReleaseBitList (*pPorts);
    }

    /*If no port list has been configured */
    if (i4CommaCount == 0)
    {
        CliPrintf (CliHandle, "NIL");
    }

    CliPrintf (CliHandle, "\r\n");
    switch (i4TrafficDistByte)
    {
        case ISS_REDIRECT_UDB_FIELD:

            CliPrintf (CliHandle, "%-33s : UDB:%d", " TrafficDistField",
                       i4RedirectUdbPosition);
            break;
        case ISS_TRAFF_DIST_BYTE_SRCIP:
            CliPrintf (CliHandle, "%-33s : SRC IP", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_DSTIP:
            CliPrintf (CliHandle, "%-33s : DST IP", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_SMAC:
            CliPrintf (CliHandle, "%-33s : SRC MAC", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_DMAC:
            CliPrintf (CliHandle, "%-33s : DST MAC", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_STCPPORT:
            CliPrintf (CliHandle, "%-33s : SRC TCP PORT", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_DTCPPORT:
            CliPrintf (CliHandle, "%-33s : DST TCP PORT", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_SUDPPORT:
            CliPrintf (CliHandle, "%-33s : SRC UDP PORT", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_DUDPPORT:
            CliPrintf (CliHandle, "%-33s : DST UDP PORT", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_VLANID:
            CliPrintf (CliHandle, "%-33s : VLAN ID", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_ETHERTYPE:
            CliPrintf (CliHandle, "%-33s : ETHER-TYPE", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_IPPROTOCOL:
            CliPrintf (CliHandle, "%-33s : INNER IP", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_INNER_SRCIP:
            CliPrintf (CliHandle, "%-33s : INNER SRCIP", " TrafficDistField");
            break;

        case ISS_TRAFF_DIST_BYTE_INNER_DESTIP:

            CliPrintf (CliHandle, "%-33s : INNER DSTIP", " TrafficDistField");
            break;
        default:

            CliPrintf (CliHandle, "%-33s : Unknown", " TrafficDistField");

    }

    CliPrintf (CliHandle, "\r\n");

    if (i4SubAction == ISS_NONE)
    {
        CliPrintf (CliHandle, "%-33s : NONE", " Sub Action");
    }
    else if (i4SubAction == ISS_MODIFY_VLAN)
    {
        CliPrintf (CliHandle, "%-33s : MODIFY VLAN", " Sub Action");
    }
    else if (i4SubAction == ISS_NESTED_VLAN)
    {
        CliPrintf (CliHandle, "%-33s : NESTED VLAN", " Sub Action");
    }
    else if (i4SubAction == ISS_STRIP_OUTER_HDR)
    {
        CliPrintf (CliHandle, "%-33s : STRIP OUTER HEADER", " Sub Action");
    }

    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "%-33s : %d\r\n", " Sub Action Id", i4SubActionId);

    if (i4RowStatus == ISS_ACTIVE)
    {
        CliPrintf (CliHandle, "%-33s : Active", " Status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : InActive", " Status");
    }
    CliPrintf (CliHandle, "\r\n\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclShowRunningConfig                               */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the  RSTP Configuration    */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
AclShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{

    INT4                i4Index = -1;

    AclShowRunningConfigTables (CliHandle);

    if (u4Module == ISS_ACL_SHOW_RUNNING_CONFIG)
    {
        AclShowRunningConfigInterfaceDetails (CliHandle, i4Index);

    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*     FUNCTION NAME    : AclShowRunningConfigTables                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays table  objects in ACL  for  */
/*                        show running configuration.                        */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
AclShowRunningConfigTables (tCliHandle CliHandle)
{

    tMacAddr            DstMacAddr;
    tMacAddr            SrcMacAddr;
    tMacAddr            zeroAddr;
    INT4                i4NextFilter = 0;
    INT4                i4PrevFilter = 0;
    INT4                i4FltrAction;
    INT4                i4Tos = 0;
    INT4                i4Dscp = 0;
    INT4                i4MsgType;
    INT4                i4MsgCode;
    INT4                i4FltrAckBit;
    INT4                i4FltrRstBit;
    INT4                i4EtherType;
    INT4                i4FltrPrio;
    INT4                i4TotOffsets = 0;
    INT4                i4Protocol = 0;
    INT4                i4SVlan = ACL_DEF_SVLAN;
    INT4                i4SVlanPrio = ACL_DEF_SVLAN_PRIO;
    INT4                i4VlanId = ACL_DEF_CVLAN;
    INT4                i4CVlanPrio = ACL_DEF_CVLAN_PRIO;
    INT4                i4TagType = ISS_FILTER_SINGLE_TAG;
    INT4                i4AddrType = QOS_IPV4;
    INT4                i4TrafficDistByte = 0;
    INT4                i4PktType = 0;
    INT4                i4Base = 0;
    INT4                i4Priority = 0;
    INT4                i4FilterOneType = 0;
    INT4                i4FilterOneId = 0;
    INT4                i4FilterTwoType = 0;
    INT4                i4FilterTwoId = 0;
    INT4                i4RedirectInterfaceGrpType = 0;
    INT4                i4Length = 0;
    INT4                i4SubAction = 0;
    INT4                i4SubActionId = 0;
    INT1                i1OutCome;
    UINT4               u4NextFilter = 0;
    UINT4               u4PrevFilter = 0;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4SrcIpMask;
    UINT4               u4DstIpAddr = 0;
    UINT4               u4DstIpMask;
    UINT4               u4MinSrcProtPort = 0;
    UINT4               u4MaxSrcProtPort = 0;
    UINT4               u4MinDstProtPort = 0;
    UINT4               u4MaxDstProtPort = 0;
    UINT4               u4SrcIpPrefixLength = 0;
    UINT4               u4DstIpPrefixLength = 0;
    UINT4               u4FlowId = 0;
    UINT4               u4RedirectGrpId = 0;
    UINT4               u4PagingStatus = 0;
    UINT4               u4InterfaceIndex = 0;
    INT4                i4OuterEType = 0;
    INT4                i4RedirectUdbPosition = 0;
    CHR1               *pu1String;
    UINT1               au1SrcAddr[ISS_ADDR_LEN];
    UINT1               au1SrcMask[ISS_ADDR_LEN];
    UINT1               au1DstAddr[ISS_ADDR_LEN];
    UINT1               au1DstMask[ISS_ADDR_LEN];
    UINT4               u4Quit = CLI_SUCCESS;
    CHR1                au1String[ISS_ADDR_LEN];
    UINT1               u1StdAclFlag = 0;
    UINT1               au1SrcTemp[4];
    UINT1               au1DstTemp[4];
    tIp6Addr            SrcIp6Addr;
    tIp6Addr            DstIp6Addr;
    tSNMP_OCTET_STRING_TYPE SrcIpAddrOctet;
    tSNMP_OCTET_STRING_TYPE DstIpAddrOctet;
    tSNMP_OCTET_STRING_TYPE RedirectsPorts;
    tSNMP_OCTET_STRING_TYPE InPortList;
    tSNMP_OCTET_STRING_TYPE SetValue;
    tSNMP_OCTET_STRING_TYPE SetMask;
    tPortList          *pPorts = NULL;
    INT1               *piIfName;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1InPortList[ISS_PORTLIST_LEN];
    UINT1               au1SetValue[ISS_UDB_MAX_OFFSET];
    UINT1               au1SetMask[ISS_UDB_MAX_OFFSET];

    CLI_MEMSET (&RedirectsPorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    piIfName = (INT1 *) &au1IfName[0];

    pu1String = &au1String[0];
    SrcIpAddrOctet.pu1_OctetList = au1SrcTemp;
    DstIpAddrOctet.pu1_OctetList = au1DstTemp;
    InPortList.pu1_OctetList = &au1InPortList[0];

    SetValue.i4_Length = ISS_PORTLIST_LEN;
    SetMask.i4_Length = ISS_PORTLIST_LEN;

    InPortList.pu1_OctetList = &au1InPortList[0];
    SetValue.pu1_OctetList = &au1SetValue[0];
    SetMask.pu1_OctetList = &au1SetMask[0];

    CLI_MEMSET (&SrcIp6Addr, 0, sizeof (tIp6Addr));
    CLI_MEMSET (&DstIp6Addr, 0, sizeof (tIp6Addr));
    CLI_MEMSET (au1String, 0, ISS_ADDR_LEN);
    CLI_MEMSET (zeroAddr, 0, MAC_ADDR_LEN);
    CLI_MEMSET (&RedirectsPorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    CLI_MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    CLI_MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
    CLI_MEMSET (au1SetValue, 0, ISS_UDB_MAX_OFFSET);
    CLI_MEMSET (au1SetMask, 0, ISS_UDB_MAX_OFFSET);
    CliRegisterLock (CliHandle, IssLock, IssUnLock);
    ISS_LOCK ();

    /*Ip Access lists */

    i1OutCome = nmhGetFirstIndexIssExtL3FilterTable (&i4NextFilter);

    CliPrintf (CliHandle, "\r\n");

    while (i1OutCome != SNMP_FAILURE)
    {

        u1StdAclFlag = 0;

        if (i4NextFilter < 1001)
        {
            CliPrintf (CliHandle, "ip access-list standard %d\r\n",
                       i4NextFilter);
            u1StdAclFlag = 1;
        }
        else
        {
            CliPrintf (CliHandle, "\nip  access-list extended %d\r\n",
                       i4NextFilter);
        }

        if (!u1StdAclFlag)
        {
            nmhGetIssExtL3FilterProtocol (i4NextFilter, &i4Protocol);
            nmhGetIssAclL3FilteAddrType (i4NextFilter, &i4AddrType);

            AclPbGetL3Filter (i4NextFilter, &i4SVlan, &i4SVlanPrio,
                              &i4VlanId, &i4CVlanPrio, &i4TagType);
        }

        if (i4Protocol == ISS_PROT_ICMP)
        {
            nmhGetIssExtL3FilterMessageType (i4NextFilter, &i4MsgType);
            nmhGetIssExtL3FilterMessageCode (i4NextFilter, &i4MsgCode);
        }

        /* Source and destination protocol port ranges for TCP/UDP */
        if ((i4Protocol == ISS_PROT_TCP) || (i4Protocol == ISS_PROT_UDP))
        {
            nmhGetIssExtL3FilterMinSrcProtPort (i4NextFilter,
                                                &u4MinSrcProtPort);
            nmhGetIssExtL3FilterMaxSrcProtPort (i4NextFilter,
                                                &u4MaxSrcProtPort);
            nmhGetIssExtL3FilterMinDstProtPort (i4NextFilter,
                                                &u4MinDstProtPort);
            nmhGetIssExtL3FilterMaxDstProtPort (i4NextFilter,
                                                &u4MaxDstProtPort);
        }

        nmhGetIssExtL3FilterPriority (i4NextFilter, &i4FltrPrio);
        nmhGetIssAclL3FilterAction (i4NextFilter, &i4FltrAction);
        nmhGetIssAclL3FilterSrcIpAddr (i4NextFilter, &SrcIpAddrOctet);
        nmhGetIssAclL3FilterDstIpAddr (i4NextFilter, &DstIpAddrOctet);
        nmhGetIssAclL3FilterSrcIpAddrPrefixLength (i4NextFilter,
                                                   &u4SrcIpPrefixLength);
        nmhGetIssAclL3FilterDstIpAddrPrefixLength (i4NextFilter,
                                                   &u4DstIpPrefixLength);
        nmhGetIssAclL3FilterFlowId (i4NextFilter, &u4FlowId);

        if (i4AddrType == QOS_IPV4)
        {
            PTR_FETCH4 (u4SrcIpAddr, SrcIpAddrOctet.pu1_OctetList);
            IssUtlConvertPrefixToMask (u4SrcIpPrefixLength, &u4SrcIpMask);
            PTR_FETCH4 (u4DstIpAddr, DstIpAddrOctet.pu1_OctetList);
            IssUtlConvertPrefixToMask (u4DstIpPrefixLength, &u4DstIpMask);
        }
        if (i4AddrType == QOS_IPV6)
        {
            MEMCPY (&SrcIp6Addr, SrcIpAddrOctet.pu1_OctetList, IP6_ADDR_SIZE);
            MEMCPY (&DstIp6Addr, DstIpAddrOctet.pu1_OctetList, IP6_ADDR_SIZE);
        }

        if (!u1StdAclFlag)
        {
            if ((i4NextFilter > ACL_STANDARD_MAX_VALUE) &&
                (i4Protocol != ISS_PROT_ICMP))
            {
                nmhGetIssExtL3FilterTos (i4NextFilter, &i4Tos);

                nmhGetIssExtL3FilterDscp (i4NextFilter, &i4Dscp);
            }
        }

        if (i4Protocol == ISS_PROT_TCP)
        {
            nmhGetIssExtL3FilterAckBit (i4NextFilter, &i4FltrAckBit);
            nmhGetIssExtL3FilterRstBit (i4NextFilter, &i4FltrRstBit);
        }

        if ((i4FltrAction == ISS_ALLOW) || (i4FltrAction == ISS_REDIRECT_TO))
        {
            CliPrintf (CliHandle, "permit ");
        }
        else if (i4FltrAction == ISS_DROP)
        {
            CliPrintf (CliHandle, "deny ");
        }

        if (!u1StdAclFlag)
        {
            if (i4Protocol == ISS_PROTO_ANY)
            {
                CliPrintf (CliHandle, " ip ");
            }
            else if (i4Protocol == ISS_PROT_OSPFIGP)
            {
                CliPrintf (CliHandle, " ospf ");

            }
            else if (i4Protocol == ISS_PROT_PIM)
            {
                CliPrintf (CliHandle, " pim ");
            }
            else if (i4Protocol == ISS_PROT_TCP)
            {
                CliPrintf (CliHandle, " tcp ");
            }
            else if (i4Protocol == ISS_PROT_UDP)
            {
                CliPrintf (CliHandle, " udp ");
            }
            else if (i4Protocol == ISS_PROT_ICMP)
            {
                CliPrintf (CliHandle, " icmp ");
            }
            else if (i4AddrType == QOS_IPV6)
            {
                CliPrintf (CliHandle, " ipv6 ");
            }
            else
            {
                CliPrintf (CliHandle, "%d ", i4Protocol);
            }

        }

        if (i4AddrType == QOS_IPV4)
        {
            /*Source Ip Address */
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcIpAddr);
            CLI_MEMCPY (au1SrcAddr, pu1String, ISS_ADDR_LEN);

            CLI_MEMSET (au1String, 0, ISS_ADDR_LEN);
            /*Source Ip Mask */
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcIpMask);
            CLI_MEMCPY (au1SrcMask, pu1String, ISS_ADDR_LEN);

            CLI_MEMSET (au1String, 0, ISS_ADDR_LEN);
            /*filter destination IP address */
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4DstIpAddr);
            CLI_MEMCPY (au1DstAddr, pu1String, ISS_ADDR_LEN);

            CLI_MEMSET (au1String, 0, ISS_ADDR_LEN);

            /*filter destination IP address */
            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4DstIpMask);
            CLI_MEMCPY (au1DstMask, pu1String, ISS_ADDR_LEN);

            if (u4SrcIpAddr == 0)
            {
                CliPrintf (CliHandle, " any  ");
            }

            else if ((u4SrcIpAddr != 0) && (CLI_STRCMP (au1SrcMask,
                                                        "255.255.255.255") ==
                                            0))
            {
                CliPrintf (CliHandle, "host %s ", au1SrcAddr);
            }
            else if ((u4SrcIpAddr != 0) && (CLI_STRCMP (au1SrcMask,
                                                        "255.255.255.255") !=
                                            0))
            {
                CliPrintf (CliHandle, "%s %s ", au1SrcAddr, au1SrcMask);
            }
        }
        if ((i4AddrType == QOS_IPV6) && (u4FlowId == 0))
        {
            if ((CLI_IPV6_EQUAL_ZERO (SrcIp6Addr))
                && (CLI_IPV6_EQUAL_ZERO (DstIp6Addr)))
            {
                CliPrintf (CliHandle, " any any ");
            }
            else if ((!CLI_IPV6_EQUAL_ZERO (SrcIp6Addr))
                     && (CLI_IPV6_EQUAL_ZERO (DstIp6Addr)))
            {
                CliPrintf (CliHandle, " host %s %d any",
                           Ip6PrintNtop ((tIp6Addr *) & (SrcIp6Addr)),
                           u4SrcIpPrefixLength);
            }
            else if ((!CLI_IPV6_EQUAL_ZERO (DstIp6Addr))
                     && (CLI_IPV6_EQUAL_ZERO (SrcIp6Addr)))
            {
                CliPrintf (CliHandle, " any host %s %d",
                           Ip6PrintNtop ((tIp6Addr *) & (DstIp6Addr)),
                           u4DstIpPrefixLength);
            }
            else if ((!CLI_IPV6_EQUAL_ZERO (SrcIp6Addr))
                     && (!CLI_IPV6_EQUAL_ZERO (DstIp6Addr)))
            {
                CliPrintf (CliHandle, " host %s %d host %s %d",
                           Ip6PrintNtop ((tIp6Addr *) & (SrcIp6Addr)),
                           u4SrcIpPrefixLength,
                           Ip6PrintNtop ((tIp6Addr *) & (DstIp6Addr)),
                           u4DstIpPrefixLength);
            }
        }
        if ((i4AddrType == QOS_IPV6) && (u4FlowId != 0))
        {
            CliPrintf (CliHandle, "flow-label %d ", u4FlowId);
        }
        if ((!u1StdAclFlag) && ((i4Protocol == ISS_PROT_TCP) ||
                                (i4Protocol == ISS_PROT_UDP)))
        {
            if ((u4MinSrcProtPort != 0) && (u4MaxSrcProtPort != 0))
            {
                if (u4MinSrcProtPort == u4MaxSrcProtPort)
                {
                    CliPrintf (CliHandle, "eq %d ", u4MinSrcProtPort);
                }
                else if (u4MinSrcProtPort == 1 && u4MaxSrcProtPort != 0xffff)
                {
                    CliPrintf (CliHandle, "lt %d ", u4MaxSrcProtPort + 1);
                }
                else if (u4MaxSrcProtPort == 0xffff && u4MinSrcProtPort != 1)
                {
                    CliPrintf (CliHandle, "gt %d ", u4MinSrcProtPort - 1);
                }
                else
                {
                    CliPrintf (CliHandle, " range %d  %d ", u4MinSrcProtPort,
                               u4MaxSrcProtPort);
                }
            }
        }
        if (i4AddrType == QOS_IPV4)
        {
            if (u4DstIpAddr == 0)
            {
                CliPrintf (CliHandle, " any ");
            }
            else if ((u4DstIpAddr != 0) && (CLI_STRCMP (au1DstMask,
                                                        "255.255.255.255") ==
                                            0))
            {
                CliPrintf (CliHandle, "host %s ", au1DstAddr);
            }
            else if ((u4DstIpAddr != 0) && (CLI_STRCMP (au1DstMask,
                                                        "255.255.255.255") !=
                                            0))
            {
                CliPrintf (CliHandle, "%s %s ", au1DstAddr, au1DstMask);
            }
        }

        if ((!u1StdAclFlag) && ((i4Protocol == ISS_PROT_TCP) ||
                                (i4Protocol == ISS_PROT_UDP)))
        {
            if ((u4MinDstProtPort != 0) && (u4MaxDstProtPort != 0))
            {
                if (u4MinDstProtPort == u4MaxDstProtPort)
                {
                    CliPrintf (CliHandle, "eq %d ", u4MinDstProtPort);
                }
                else if (u4MinDstProtPort == 1 && u4MaxDstProtPort != 0xffff)
                {
                    CliPrintf (CliHandle, "lt %d ", u4MaxDstProtPort + 1);
                }
                else if (u4MaxDstProtPort == 0xffff && u4MinDstProtPort != 1)
                {
                    CliPrintf (CliHandle, "gt %d ", u4MinDstProtPort - 1);
                }
                else
                {
                    CliPrintf (CliHandle, " range %d  %d ", u4MinDstProtPort,
                               u4MaxDstProtPort);
                }
            }
        }

        if (i4Protocol == ISS_PROT_TCP)
        {
            /* ACK bit */
            if (i4FltrAckBit == ISS_ACK_ESTABLISH)
            {
                CliPrintf (CliHandle, " ack");
            }

            /* RST bit */
            if (i4FltrRstBit == ISS_RST_SET)
            {
                CliPrintf (CliHandle, " rst");
            }

        }

        if (i4Protocol == ISS_PROT_ICMP)
        {
            if (i4MsgType != 255)
            {
                CliPrintf (CliHandle, " %d ", i4MsgType);
            }
            if (i4MsgCode != 255)
            {
                CliPrintf (CliHandle, "%d", i4MsgCode);
            }

        }

        if (!u1StdAclFlag)
        {
            if (i4Protocol != ISS_PROT_ICMP)
            {
                if (i4Tos != 8)
                {
                    if (i4Tos == ISS_TOS_HI_REL)
                    {
                        CliPrintf (CliHandle, " tos max-reliability");
                    }
                    else if (i4Tos == ISS_TOS_HI_THR)
                    {

                        CliPrintf (CliHandle, " tos max-throughput");
                    }
                    else if (i4Tos == ISS_TOS_LO_DEL)
                    {

                        CliPrintf (CliHandle, " tos min-delay");
                    }
                    else if (i4Tos == ISS_TOS_NONE)
                    {

                        CliPrintf (CliHandle, " tos normal");
                    }
                    else
                    {
                        CliPrintf (CliHandle, " tos %d", i4Tos);
                    }
                }

                if (i4Dscp != ISS_DSCP_INVALID)
                {
                    CliPrintf (CliHandle, " dscp %d", i4Dscp);

                }
            }

            if ((i4FltrPrio != 0) && (i4FltrPrio != 1))
            {
                CliPrintf (CliHandle, " priority %d", i4FltrPrio);

            }

            if (i4SVlan != ACL_DEF_SVLAN)
            {
                CliPrintf (CliHandle, " svlan-id %d", i4SVlan);
            }
            if (i4SVlanPrio != ACL_DEF_SVLAN_PRIO)
            {
                CliPrintf (CliHandle, " svlan-priority %d", i4SVlanPrio);
            }
            if (i4VlanId != ACL_DEF_CVLAN)
            {
                CliPrintf (CliHandle, " cvlan-id %d", i4VlanId);
            }
            if (i4CVlanPrio != ACL_DEF_CVLAN_PRIO)
            {
                CliPrintf (CliHandle, " cvlan-priority %d", i4CVlanPrio);
            }
            if (i4TagType != ISS_FILTER_SINGLE_TAG)
            {
                CliPrintf (CliHandle, " double-tag");
            }
        }
        if (i4FltrAction == ISS_REDIRECT_TO)
        {
            CliPrintf (CliHandle, "redirect ");
            nmhGetIssAclL3FilterRedirectId (i4NextFilter,
                                            (INT4 *) &u4RedirectGrpId);

            if (u4RedirectGrpId != 0)
            {
                pPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

                if (pPorts == NULL)
                    return;

                MEMSET (pPorts, 0, sizeof (tPortList));

                RedirectsPorts.pu1_OctetList = *pPorts;
                RedirectsPorts.i4_Length = sizeof (tPortList);

                nmhGetIssRedirectInterfaceGrpDistByte
                    (u4RedirectGrpId, &i4TrafficDistByte);

                nmhGetIssRedirectInterfaceGrpType
                    (u4RedirectGrpId, &i4RedirectInterfaceGrpType);

                nmhGetIssRedirectInterfaceGrpPortList
                    (u4RedirectGrpId, &RedirectsPorts);

                if ((FsUtilBitListIsAllZeros
                     (RedirectsPorts.pu1_OctetList,
                      RedirectsPorts.i4_Length) == OSIX_FALSE) &&
                    (i4RedirectInterfaceGrpType == ISS_REDIRECT_TO_PORTLIST))
                {
                    u4PagingStatus = CLI_SUCCESS;
                    CliConfOctetToIfName (CliHandle, "",
                                          NULL,
                                          &RedirectsPorts, &u4PagingStatus);
                }
                if (i4RedirectInterfaceGrpType != ISS_REDIRECT_TO_PORTLIST)
                {
                    MEMCPY (&u4InterfaceIndex, RedirectsPorts.pu1_OctetList,
                            sizeof (UINT4));

                    CfaCliGetIfName (u4InterfaceIndex, piIfName);

                    CliPrintf (CliHandle, " interface %s", piIfName);
                }
                FsUtilReleaseBitList (*pPorts);
                switch (i4TrafficDistByte)
                {
                    case ISS_TRAFF_DIST_BYTE_SRCIP:
                        CliPrintf (CliHandle, " load-balance src-ip ");
                        break;

                    case ISS_TRAFF_DIST_BYTE_DSTIP:
                        CliPrintf (CliHandle, "  load-balance dst-ip ");
                        break;

                    case ISS_TRAFF_DIST_BYTE_SMAC:
                        CliPrintf (CliHandle, "  load-balance src-mac ");
                        break;

                    case ISS_TRAFF_DIST_BYTE_DMAC:
                        CliPrintf (CliHandle, " load-balance dst-mac ");
                        break;

                    case ISS_TRAFF_DIST_BYTE_STCPPORT:
                        CliPrintf (CliHandle, "  load-balance src-tcpport ");
                        break;

                    case ISS_TRAFF_DIST_BYTE_DTCPPORT:
                        CliPrintf (CliHandle, "  load-balance dst-tcpport ");
                        break;

                    case ISS_TRAFF_DIST_BYTE_SUDPPORT:
                        CliPrintf (CliHandle, "  load-balance src-udpport ");
                        break;

                    case ISS_TRAFF_DIST_BYTE_DUDPPORT:
                        CliPrintf (CliHandle, "  load-balance dst-udpport ");
                        break;

                    case ISS_TRAFF_DIST_BYTE_VLANID:
                        CliPrintf (CliHandle, "  load-balance vlanid ");
                        break;
                    default:
                        CliPrintf (CliHandle, " ");
                }
            }
        }

        nmhGetIssAclL3FilterSubAction (i4NextFilter, &i4SubAction);
        nmhGetIssAclL3FilterSubActionId (i4NextFilter, &i4SubActionId);

        if (i4SubAction == ISS_MODIFY_VLAN)
        {
            CliPrintf (CliHandle, "  sub-action modify-vlan %d", i4SubActionId);
        }
        else if (i4SubAction == ISS_NESTED_VLAN)
        {
            CliPrintf (CliHandle, "  sub-action nested-vlan %d", i4SubActionId);
        }

        if (u1StdAclFlag)
        {
            if (i4FltrPrio != 0)
            {
                CliPrintf (CliHandle, "priority %d ", i4FltrPrio);

            }
        }

        u4Quit = CliPrintf (CliHandle, "\n!\r\n");
        if (u4Quit == CLI_FAILURE)
        {
            ISS_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return;
        }

        i4PrevFilter = i4NextFilter;

        i1OutCome =
            nmhGetNextIndexIssExtL3FilterTable (i4PrevFilter, &i4NextFilter);

    }

    /*MacAccessLists */

    CliPrintf (CliHandle, "\r\n");

    u4RedirectGrpId = 0;
    u4InterfaceIndex = 0;
    i4TrafficDistByte = 0;
    i4RedirectInterfaceGrpType = 0;
    i4SubActionId = 0;
    i4SubAction = 0;
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&RedirectsPorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&SrcMacAddr, 0, sizeof (tMacAddr));
    MEMSET (&DstMacAddr, 0, sizeof (tMacAddr));
    i1OutCome = nmhGetFirstIndexIssExtL2FilterTable (&i4NextFilter);

    while (i1OutCome != SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "mac access-list extended %d\r\n", i4NextFilter);

        nmhGetIssExtL2FilterPriority (i4NextFilter, &i4FltrPrio);
        nmhGetIssExtL2FilterEtherType (i4NextFilter, &i4EtherType);
        nmhGetIssExtL2FilterProtocolType (i4NextFilter, (UINT4 *) &i4Protocol);
        nmhGetIssExtL2FilterVlanId (i4NextFilter, &i4VlanId);
        nmhGetIssExtL2FilterDstMacAddr (i4NextFilter,
                                        (tMacAddr *) & DstMacAddr);
        nmhGetIssExtL2FilterSrcMacAddr (i4NextFilter,
                                        (tMacAddr *) & SrcMacAddr);
        AclPbGetL2Filter (i4NextFilter, &i4OuterEType, &i4SVlan, &i4SVlanPrio,
                          &i4CVlanPrio, &i4TagType);

        nmhGetIssExtL2FilterAction (i4NextFilter, &i4FltrAction);

        if ((i4FltrAction == ISS_ALLOW) || (i4FltrAction == ISS_REDIRECT_TO))

        {
            CliPrintf (CliHandle, "permit ");
        }
        else if (i4FltrAction == ISS_DROP)
        {
            CliPrintf (CliHandle, "deny ");
        }

        CLI_MEMSET (au1String, 0, ISS_ADDR_LEN);

        if (CLI_MEMCMP (SrcMacAddr, zeroAddr, MAC_ADDR_LEN) != 0)
        {
            PrintMacAddress (SrcMacAddr, (UINT1 *) pu1String);
            CliPrintf (CliHandle, "host %s", pu1String);
        }

        if (CLI_MEMCMP (SrcMacAddr, zeroAddr, MAC_ADDR_LEN) == 0)
        {
            CliPrintf (CliHandle, " any ");
        }

        CLI_MEMSET (au1String, 0, ISS_ADDR_LEN);

        if (CLI_MEMCMP (DstMacAddr, zeroAddr, MAC_ADDR_LEN) != 0)
        {
            PrintMacAddress (DstMacAddr, (UINT1 *) pu1String);
            CliPrintf (CliHandle, "host %s", pu1String);
        }

        if (CLI_MEMCMP (DstMacAddr, zeroAddr, MAC_ADDR_LEN) == 0)
        {
            CliPrintf (CliHandle, "any ");
        }

        switch (i4Protocol)
        {
            case AARP:
                CliPrintf (CliHandle, "aarp ");
                break;

            case AMBER:
                CliPrintf (CliHandle, "amber ");
                break;

            case DEC_SPANNING:
                CliPrintf (CliHandle, "dec-spanning ");
                break;

            case DIAGNOSTIC:
                CliPrintf (CliHandle, "diagnostic ");
                break;

            case DSM:
                CliPrintf (CliHandle, "dsm ");
                break;

            case ETYPE_6000:
                CliPrintf (CliHandle, "etype-6000 ");
                break;
            case ETYPE_8042:
                CliPrintf (CliHandle, "etype-8042");
                break;

            case LAT:
                CliPrintf (CliHandle, "lat ");
                break;

            case LAVC_SCA:
                CliPrintf (CliHandle, "lavc-sca ");
                break;

            case MOP_CONSOLE:
                CliPrintf (CliHandle, "mop-console ");
                break;

            case MSDOS:
                CliPrintf (CliHandle, "msdos ");
                break;

            case MUMPS:
                CliPrintf (CliHandle, "mumps ");
                break;

            case NET_BIOS:
                CliPrintf (CliHandle, "net-bios ");
                break;

            case VINES_ECHO:
                CliPrintf (CliHandle, "vines-echo ");
                break;

            case VINES_IP:
                CliPrintf (CliHandle, "vines-ip ");
                break;
            case XNS_ID:
                CliPrintf (CliHandle, "xns-id ");
                break;

            default:
                if (i4Protocol != 0)
                {
                    CliPrintf (CliHandle, "%d ", i4Protocol);
                }

                break;
        }

        if (i4EtherType != 0)
        {
            CliPrintf (CliHandle, "encaptype %d ", i4EtherType);
        }
        if (i4VlanId != 0)
        {
            CliPrintf (CliHandle, "vlan %d ", i4VlanId);
        }
        if (i4FltrPrio != 1)
        {
            CliPrintf (CliHandle, " priority %d ", i4FltrPrio);
        }
        if (i4OuterEType != ACL_DEF_ETYPE)
        {
            CliPrintf (CliHandle, " outerEtherType %d", i4OuterEType);
        }
        if (i4SVlan != ACL_DEF_SVLAN)
        {
            CliPrintf (CliHandle, " svlan-id %d", i4SVlan);
        }

        if (i4CVlanPrio != ACL_DEF_CVLAN_PRIO)
        {
            CliPrintf (CliHandle, " cvlan-priority %d", i4CVlanPrio);
        }
        if (i4SVlanPrio != ACL_DEF_SVLAN_PRIO)
        {
            CliPrintf (CliHandle, " svlan-priority %d", i4SVlanPrio);
        }

        if (i4TagType != ISS_FILTER_SINGLE_TAG)
        {
            CliPrintf (CliHandle, " double-tag");
        }
        if (i4FltrAction == ISS_REDIRECT_TO)
        {
            CliPrintf (CliHandle, "redirect ");
            nmhGetIssAclL2FilterRedirectId (i4NextFilter,
                                            (INT4 *) &u4RedirectGrpId);
            if (u4RedirectGrpId != 0)
            {
                pPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

                if (pPorts == NULL)

                {
                    return;
                }
                MEMSET (pPorts, 0, sizeof (tPortList));
                RedirectsPorts.pu1_OctetList = *pPorts;
                RedirectsPorts.i4_Length = sizeof (tPortList);

                nmhGetIssRedirectInterfaceGrpDistByte
                    (u4RedirectGrpId, &i4TrafficDistByte);

                nmhGetIssRedirectInterfaceGrpType
                    (u4RedirectGrpId, &i4RedirectInterfaceGrpType);

                nmhGetIssRedirectInterfaceGrpPortList
                    (u4RedirectGrpId, &RedirectsPorts);

                if ((FsUtilBitListIsAllZeros
                     (RedirectsPorts.pu1_OctetList,
                      RedirectsPorts.i4_Length) == OSIX_FALSE) &&
                    (i4RedirectInterfaceGrpType == ISS_REDIRECT_TO_PORTLIST))
                {
                    u4PagingStatus = CLI_SUCCESS;
                    CliConfOctetToIfName (CliHandle, "",
                                          NULL,
                                          &RedirectsPorts, &u4PagingStatus);
                }
                if (i4RedirectInterfaceGrpType != ISS_REDIRECT_TO_PORTLIST)
                {
                    MEMCPY (&u4InterfaceIndex, RedirectsPorts.pu1_OctetList,
                            sizeof (UINT4));

                    CfaCliGetIfName (u4InterfaceIndex, piIfName);

                    CliPrintf (CliHandle, " interface %s", piIfName);
                }
                FsUtilReleaseBitList (*pPorts);
                switch (i4TrafficDistByte)
                {
                    case ISS_TRAFF_DIST_BYTE_SRCIP:
                        CliPrintf (CliHandle, " load-balance src-ip ");
                        break;

                    case ISS_TRAFF_DIST_BYTE_DSTIP:
                        CliPrintf (CliHandle, "  load-balance dst-ip ");
                        break;

                    case ISS_TRAFF_DIST_BYTE_SMAC:
                        CliPrintf (CliHandle, "  load-balance src-mac ");
                        break;

                    case ISS_TRAFF_DIST_BYTE_DMAC:
                        CliPrintf (CliHandle, " load-balance dst-mac ");
                        break;

                    case ISS_TRAFF_DIST_BYTE_STCPPORT:
                        CliPrintf (CliHandle, "  load-balance src-tcpport ");
                        break;

                    case ISS_TRAFF_DIST_BYTE_DTCPPORT:
                        CliPrintf (CliHandle, "  load-balance dst-tcpport ");
                        break;

                    case ISS_TRAFF_DIST_BYTE_SUDPPORT:
                        CliPrintf (CliHandle, "  load-balance src-udpport ");
                        break;

                    case ISS_TRAFF_DIST_BYTE_DUDPPORT:
                        CliPrintf (CliHandle, "  load-balance dst-udpport ");
                        break;

                    case ISS_TRAFF_DIST_BYTE_VLANID:
                        CliPrintf (CliHandle, "  load-balance vlanid ");
                        break;
                    default:
                        CliPrintf (CliHandle, " ");
                }
            }
        }

        nmhGetIssAclL2FilterSubAction (i4NextFilter, &i4SubAction);
        nmhGetIssAclL2FilterSubActionId (i4NextFilter, &i4SubActionId);

        if (i4SubAction == ISS_MODIFY_VLAN)
        {
            CliPrintf (CliHandle, "  sub-action modify-vlan %d", i4SubActionId);
        }
        else if (i4SubAction == ISS_NESTED_VLAN)
        {
            CliPrintf (CliHandle, "  sub-action nested-vlan %d", i4SubActionId);
        }
        else if (i4SubAction == ISS_STRIP_OUTER_HDR)
        {
            CliPrintf (CliHandle, "  sub-action strip-outer-hdr");
        }

        u4Quit = CliPrintf (CliHandle, "\n!\r\n");

        if (u4Quit == CLI_FAILURE)
        {
            ISS_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return;
        }

        i4PrevFilter = i4NextFilter;

        i1OutCome =
            nmhGetNextIndexIssExtL2FilterTable (i4PrevFilter, &i4NextFilter);
    }

    /*UserDef Filter Start */
    i1OutCome = nmhGetFirstIndexIssAclUserDefinedFilterTable (&u4NextFilter);
    while (i1OutCome != SNMP_FAILURE)
    {

        u1StdAclFlag = 0;

        CliPrintf (CliHandle, "\n user-defined access-list %d\r\n",
                   u4NextFilter);
        nmhGetIssAclUserDefinedFilterPktType (u4NextFilter, &i4PktType);
        nmhGetIssAclUserDefinedFilterOffSetBase (u4NextFilter, &i4Base);
        nmhGetIssAclUserDefinedFilterOffSetValue (u4NextFilter, &SetValue);
        nmhGetIssAclUserDefinedFilterOffSetMask (u4NextFilter, &SetMask);
        nmhGetIssAclUserDefinedFilterPriority (u4NextFilter, &i4Priority);
        nmhGetIssAclUserDefinedFilterAction (u4NextFilter, &i4FltrAction);
        nmhGetIssAclUserDefinedFilterInPortList (u4NextFilter, &InPortList);
        nmhGetIssAclUserDefinedFilterIdOneType (u4NextFilter, &i4FilterOneType);
        nmhGetIssAclUserDefinedFilterIdOne (u4NextFilter,
                                            (UINT4 *) &i4FilterOneId);
        nmhGetIssAclUserDefinedFilterIdTwoType (u4NextFilter, &i4FilterTwoType);
        nmhGetIssAclUserDefinedFilterIdTwo (u4NextFilter,
                                            (UINT4 *) &i4FilterTwoId);
        i4TotOffsets = 0;
        if ((i4FltrAction == ISS_ALLOW) || (i4FltrAction == ISS_REDIRECT_TO))
        {
            CliPrintf (CliHandle, "permit ");
        }
        else if (i4FltrAction == ISS_DROP)
        {
            CliPrintf (CliHandle, "deny ");
        }
        if ((i4FltrAction == ISS_AND) || (i4FltrAction == ISS_OR)
            || (i4FltrAction == ISS_NOT))
        {
            CliPrintf (CliHandle, "userdefined-list");
            if ((i4FilterOneType == ISS_L3_FILTER)
                && ((i4FilterTwoType == ISS_L3_FILTER)))
            {
                if (i4FltrAction == ISS_AND)
                {
                    CliPrintf (CliHandle,
                               " ip-acl1-and-ip-acl2 %d %d",
                               i4FilterOneId, i4FilterTwoId);
                }
                else
                {
                    CliPrintf (CliHandle,
                               " ip-acl1-or-ip-acl2 %d %d",
                               i4FilterOneId, i4FilterTwoId);
                }

            }
            if ((i4FilterOneType == ISS_L2_FILTER)
                && ((i4FilterTwoType == ISS_L2_FILTER)))
            {
                if (i4FltrAction == ISS_AND)
                {
                    CliPrintf (CliHandle,
                               " mac-acl1-and-mac-acl2 %d %d",
                               i4FilterOneId, i4FilterTwoId);
                }
                else
                {
                    CliPrintf (CliHandle,
                               " mac-acl1-or-mac-acl2 %d %d",
                               i4FilterOneId, i4FilterTwoId);
                }

            }
            if ((i4FilterOneType == ISS_L2_FILTER)
                && ((i4FilterTwoType == ISS_L3_FILTER)))
            {
                if (i4FltrAction == ISS_AND)
                {
                    CliPrintf (CliHandle,
                               " mac-acl1-and-ip-acl2 %d %d",
                               i4FilterOneId, i4FilterTwoId);
                }

            }
            if ((i4FilterOneType == ISS_L3_FILTER)
                && ((i4FilterTwoType == ISS_L2_FILTER)))
            {
                if (i4FltrAction == ISS_OR)
                {
                    CliPrintf (CliHandle,
                               " ip-acl1-or-mac-acl2 %d %d",
                               i4FilterOneId, i4FilterTwoId);
                }

            }
            if ((i4FilterOneType == ISS_NOT)
                && ((i4FilterOneType == ISS_L2_FILTER))
                && ((i4FilterOneId != 0)))
            {
                CliPrintf (CliHandle, "not-mac-acl %d ", i4FilterOneId);
            }
            if ((i4FilterOneType == ISS_NOT)
                && ((i4FilterTwoType == ISS_L2_FILTER))
                && ((i4FilterTwoId != 0)))
            {
                CliPrintf (CliHandle, "not-mac-acl %d ", i4FilterTwoId);
            }
            if ((i4FilterOneType == ISS_NOT)
                && ((i4FilterOneType == ISS_L3_FILTER))
                && ((i4FilterOneId != 0)))
            {
                CliPrintf (CliHandle, "not-ip-acl %d ", i4FilterOneId);
            }
            if ((i4FilterTwoType == ISS_NOT)
                && ((i4FilterTwoType == ISS_L3_FILTER))
                && ((i4FilterTwoId != 0)))
            {
                CliPrintf (CliHandle, "not-ip-acl %d ", i4FilterTwoId);
            }

        }
        else
        {
            if (i4PktType != 0)
            {
                CliPrintf (CliHandle, " usr-defined-packet-type ");
                switch (i4PktType)
                {
                    case ISS_UDB_PKT_TYPE_USER_DEF:
                        CliPrintf (CliHandle, " user-def");
                        break;

                    case ISS_UDB_PKT_TYPE_IPV4_TCP:
                        CliPrintf (CliHandle, "tcp");
                        break;

                    case ISS_UDB_PKT_TYPE_IPV4_UDP:
                        CliPrintf (CliHandle, "udp");
                        break;

                    case ISS_UDB_PKT_TYPE_MPLS:
                        CliPrintf (CliHandle, "mpls");
                        break;

                    case ISS_UDB_PKT_TYPE_IPV4:
                        CliPrintf (CliHandle, "ipv4");
                        break;

                    case ISS_UDB_PKT_TYPE_IPV6:
                        CliPrintf (CliHandle, "ipv6");
                        break;

                    case ISS_UDB_PKT_TYPE_FRAG:
                        CliPrintf (CliHandle, "frag-ip");
                        break;
                }
                CliPrintf (CliHandle, "%s  ", " offset-base");
                switch (i4Base)
                {
                    case ISS_UDB_OFFSET_L2:
                        CliPrintf (CliHandle, "L2");
                        break;

                    case ISS_UDB_OFFSET_L3:
                        CliPrintf (CliHandle, "L3");
                        break;

                    case ISS_UDB_OFFSET_L4:
                        CliPrintf (CliHandle, "L4");
                        break;

                    case ISS_UDB_OFFSET_IPV6_EXT_HDR:
                        CliPrintf (CliHandle, "IPV6");
                        break;

                    case ISS_UDB_OFFSET_L3_MINUS_2:
                        CliPrintf (CliHandle, "Ether-Type");
                        break;

                    case ISS_UDB_OFFSET_MPLS_MINUS_2:
                        CliPrintf (CliHandle, "Ether-Type");
                        break;

                    default:
                        CliPrintf (CliHandle, "%d\r\n", i4Base);
                        break;
                }

                for (i4Length = 0; i4Length < SetValue.i4_Length; i4Length++)
                {
                    if (au1SetValue[i4Length] != 0)

                    {
                        if ((i4TotOffsets == 0) && (i4TotOffsets < 6))
                        {
                            CliPrintf (CliHandle, " offset1 %d %d ", i4Length,
                                       au1SetValue[i4Length]);
                        }
                        if ((i4TotOffsets == 1) && (i4TotOffsets < 6))
                        {
                            CliPrintf (CliHandle, " offset2 %d %d ", i4Length,
                                       au1SetValue[i4Length]);
                        }
                        if ((i4TotOffsets == 2) && (i4TotOffsets < 6))
                        {
                            CliPrintf (CliHandle, " offset3 %d %d ", i4Length,
                                       au1SetValue[i4Length]);
                        }
                        if ((i4TotOffsets == 3) && (i4TotOffsets < 6))
                        {
                            CliPrintf (CliHandle, " offset4 %d %d ", i4Length,
                                       au1SetValue[i4Length]);
                        }
                        if ((i4TotOffsets == 4) && (i4TotOffsets < 6))
                        {
                            CliPrintf (CliHandle, " offset5 %d %d ", i4Length,
                                       au1SetValue[i4Length]);
                        }
                        if ((i4TotOffsets == 5) && (i4TotOffsets < 6))
                        {
                            CliPrintf (CliHandle, " offset6 %d %d ", i4Length,
                                       au1SetValue[i4Length]);
                        }
                        i4TotOffsets++;
                    }
                }
            }

            u4RedirectGrpId = 0;
            u4InterfaceIndex = 0;
            i4TrafficDistByte = 0;
            i4RedirectInterfaceGrpType = 0;
            i4SubActionId = 0;
            i4SubAction = 0;
            MEMSET (&RedirectsPorts, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

            if (i4FltrAction == ISS_REDIRECT_TO)
            {
                CliPrintf (CliHandle, "redirect ");
                nmhGetIssAclUserDefinedFilterRedirectId (u4NextFilter,
                                                         (INT4 *)
                                                         &u4RedirectGrpId);

                if (u4RedirectGrpId != 0)
                {
                    pPorts = (tPortList *)
                        FsUtilAllocBitList (sizeof (tPortList));

                    if (pPorts == NULL)
                        return;

                    MEMSET (pPorts, 0, sizeof (tPortList));

                    RedirectsPorts.pu1_OctetList = *pPorts;
                    RedirectsPorts.i4_Length = sizeof (tPortList);

                    nmhGetIssRedirectInterfaceGrpDistByte
                        (u4RedirectGrpId, &i4TrafficDistByte);

                    nmhGetIssRedirectInterfaceGrpUdbPosition
                        (u4RedirectGrpId, &i4RedirectUdbPosition);

                    nmhGetIssRedirectInterfaceGrpType
                        (u4RedirectGrpId, &i4RedirectInterfaceGrpType);

                    nmhGetIssRedirectInterfaceGrpPortList
                        (u4RedirectGrpId, &RedirectsPorts);

                    if ((FsUtilBitListIsAllZeros
                         (RedirectsPorts.pu1_OctetList,
                          RedirectsPorts.i4_Length) == OSIX_FALSE) &&
                        (i4RedirectInterfaceGrpType ==
                         ISS_REDIRECT_TO_PORTLIST))
                    {
                        u4PagingStatus = CLI_SUCCESS;
                        CliConfOctetToIfName (CliHandle, "",
                                              NULL,
                                              &RedirectsPorts, &u4PagingStatus);
                    }
                    if (i4RedirectInterfaceGrpType != ISS_REDIRECT_TO_PORTLIST)
                    {
                        MEMCPY (&u4InterfaceIndex,
                                RedirectsPorts.pu1_OctetList, sizeof (UINT4));

                        CfaCliGetIfName (u4InterfaceIndex, piIfName);

                        CliPrintf (CliHandle, " interface %s", piIfName);
                    }
                    FsUtilReleaseBitList (*pPorts);
                    switch (i4TrafficDistByte)
                    {
                        case ISS_TRAFF_DIST_BYTE_SRCIP:
                            CliPrintf (CliHandle, " load-balance src-ip ");
                            break;

                        case ISS_TRAFF_DIST_BYTE_DSTIP:
                            CliPrintf (CliHandle, "  load-balance dst-ip ");
                            break;

                        case ISS_TRAFF_DIST_BYTE_SMAC:
                            CliPrintf (CliHandle, "  load-balance src-mac ");
                            break;

                        case ISS_TRAFF_DIST_BYTE_DMAC:
                            CliPrintf (CliHandle, " load-balance dst-mac ");
                            break;

                        case ISS_TRAFF_DIST_BYTE_STCPPORT:
                            CliPrintf (CliHandle,
                                       "  load-balance src-tcpport ");
                            break;

                        case ISS_TRAFF_DIST_BYTE_DTCPPORT:
                            CliPrintf (CliHandle,
                                       "  load-balance dst-tcpport ");
                            break;

                        case ISS_TRAFF_DIST_BYTE_SUDPPORT:
                            CliPrintf (CliHandle,
                                       "  load-balance src-udpport ");
                            break;

                        case ISS_TRAFF_DIST_BYTE_DUDPPORT:
                            CliPrintf (CliHandle,
                                       "  load-balance dst-udpport ");
                            break;

                        case ISS_TRAFF_DIST_BYTE_VLANID:
                            CliPrintf (CliHandle, "  load-balance vlanid ");
                            break;

                        case ISS_REDIRECT_UDB_FIELD:
                            CliPrintf (CliHandle, " load-balance udb  %d",
                                       i4RedirectUdbPosition);
                            break;
                        default:
                            CliPrintf (CliHandle, " ");
                    }
                }
            }

            nmhGetIssAclUserDefinedFilterSubAction (u4NextFilter, &i4SubAction);
            nmhGetIssAclUserDefinedFilterSubActionId (u4NextFilter,
                                                      &i4SubActionId);

            if (i4SubAction == ISS_MODIFY_VLAN)
            {
                CliPrintf (CliHandle, "  sub-action modify-vlan %d",
                           i4SubActionId);
            }
            else if (i4SubAction == ISS_NESTED_VLAN)
            {
                CliPrintf (CliHandle, "  sub-action nested-vlan %d",
                           i4SubActionId);
            }

        }
        u4Quit = CliPrintf (CliHandle, "\n!\r\n");
        if (u4Quit == CLI_FAILURE)
        {
            ISS_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return;
        }

        u4PrevFilter = u4NextFilter;
        i1OutCome =
            nmhGetNextIndexIssAclUserDefinedFilterTable (u4PrevFilter,
                                                         &u4NextFilter);
    }
    ISS_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return;

}

/*****************************************************************************/
/*     FUNCTION NAME    : AclShowRunningConfigInterfaceDetails               */
/*                                                                           */
/*     DESCRIPTION      : This function displays the  Interface objects in ACL*/
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
AclShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index)
{
    INT4                i4Direction;
    INT4                i4Count;
    INT4                i4NextFilter;
    INT4                i4PrevFilter;
    INT4                i4RowStatus;
    INT1                i1OutCome;
    UINT1               au1InPortList[ISS_PORTLIST_LEN];
    UINT1               au1OutPortList[ISS_PORTLIST_LEN];
    tSNMP_OCTET_STRING_TYPE InPortList;
    tSNMP_OCTET_STRING_TYPE OutPortList;

    InPortList.pu1_OctetList = &au1InPortList[0];
    InPortList.i4_Length = ISS_PORTLIST_LEN;

    OutPortList.pu1_OctetList = &au1OutPortList[0];
    OutPortList.i4_Length = ISS_PORTLIST_LEN;

    CliRegisterLock (CliHandle, IssLock, IssUnLock);
    ISS_LOCK ();
    /*Ip Access Lists */
    i1OutCome = nmhGetFirstIndexIssExtL3FilterTable (&i4NextFilter);

    while (i1OutCome != SNMP_FAILURE)
    {

        MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
        MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);

        nmhGetIssExtL3FilterStatus (i4NextFilter, &i4RowStatus);

        if (i4RowStatus == ISS_ACTIVE)
        {
            nmhGetIssExtL3FilterDirection (i4NextFilter, &i4Direction);
            nmhGetIssExtL3FilterInPortList (i4NextFilter, &InPortList);
            nmhGetIssExtL3FilterOutPortList (i4NextFilter, &OutPortList);

            if (i4Index == -1)
            {
                i4Count = 0;

                if (i4Direction == ACL_ACCESS_IN)
                {
                    do
                    {
                        if (CliIsMemberPort (InPortList.pu1_OctetList,
                                             ISS_PORTLIST_LEN,
                                             i4Count) == CLI_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "interface gigabitethernet 0/%d\r\n",
                                       i4Count);
                            CliPrintf (CliHandle, "ip access-group %d in\r\n",
                                       i4NextFilter);
                            CliPrintf (CliHandle, "!\r\n");

                        }

                        i4Count++;

                    }
                    while (i4Count <= SYS_DEF_MAX_PHYSICAL_INTERFACES);

                }

                i4Count = 0;
                if (i4Direction == ACL_ACCESS_OUT)
                {
                    do
                    {

                        if (CliIsMemberPort (OutPortList.pu1_OctetList,
                                             ISS_PORTLIST_LEN,
                                             i4Count) == CLI_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "interface gigabitethernet 0/%d\r\n",
                                       i4Count);
                            CliPrintf (CliHandle, "ip access-group %d out\r\n",
                                       i4NextFilter);
                            CliPrintf (CliHandle, "!\r\n");

                        }

                        i4Count++;
                    }
                    while (i4Count <= SYS_DEF_MAX_PHYSICAL_INTERFACES);

                }
            }
            else
            {

                if (CliIsMemberPort (InPortList.pu1_OctetList,
                                     ISS_PORTLIST_LEN, i4Index) == CLI_SUCCESS)
                {
                    if (i4Direction == ACL_ACCESS_IN)
                    {
                        CliPrintf (CliHandle, "ip access-group %d in\r\n",
                                   i4NextFilter);
                        CliPrintf (CliHandle, "!\r\n");
                    }

                }

                if (CliIsMemberPort (OutPortList.pu1_OctetList,
                                     ISS_PORTLIST_LEN, i4Index) == CLI_SUCCESS)
                {
                    if (i4Direction == ACL_ACCESS_OUT)
                    {
                        CliPrintf (CliHandle, "ip access-group %d out\r\n",
                                   i4NextFilter);
                        CliPrintf (CliHandle, "!\r\n");
                    }

                }

            }

        }

        i4PrevFilter = i4NextFilter;
        i1OutCome =
            nmhGetNextIndexIssExtL3FilterTable (i4PrevFilter, &i4NextFilter);
    }

    /*MacAccessLists */

    i1OutCome = nmhGetFirstIndexIssExtL2FilterTable (&i4NextFilter);

    while (i1OutCome != SNMP_FAILURE)
    {

        i4Count = 0;

        MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
        MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);

        nmhGetIssExtL2FilterStatus (i4NextFilter, &i4RowStatus);

        if (i4RowStatus == ISS_ACTIVE)
        {
            nmhGetIssExtL2FilterDirection (i4NextFilter, &i4Direction);
            nmhGetIssExtL2FilterInPortList (i4NextFilter, &InPortList);
            nmhGetIssExtL2FilterOutPortList (i4NextFilter, &OutPortList);

            if (i4Index == -1)
            {
                i4Count = 0;

                if (i4Direction == ACL_ACCESS_IN)
                {

                    do
                    {

                        if (CliIsMemberPort (InPortList.pu1_OctetList,
                                             ISS_PORTLIST_LEN,
                                             i4Count) == CLI_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "interface gigabitethernet 0/%d\n",
                                       i4Count);
                            CliPrintf (CliHandle, "mac access-group %d in\r\n",
                                       i4NextFilter);
                            CliPrintf (CliHandle, "!\r\n");
                        }

                        i4Count++;
                    }
                    while (i4Count <= SYS_DEF_MAX_PHYSICAL_INTERFACES);
                }
                else
                {
                    do
                    {

                        if (CliIsMemberPort (OutPortList.pu1_OctetList,
                                             ISS_PORTLIST_LEN,
                                             i4Count) == CLI_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "interface gigabitethernet 0/%d\n",
                                       i4Count);
                            CliPrintf (CliHandle, "mac access-group %d out\r\n",
                                       i4NextFilter);
                            CliPrintf (CliHandle, "!\r\n");
                        }

                        i4Count++;
                    }
                    while (i4Count <= SYS_DEF_MAX_PHYSICAL_INTERFACES);

                }
            }
            else
            {
                if (i4Direction == ACL_ACCESS_IN)
                {

                    if (CliIsMemberPort (InPortList.pu1_OctetList,
                                         ISS_PORTLIST_LEN,
                                         i4Index) == CLI_SUCCESS)
                    {
                        CliPrintf (CliHandle, "mac access-group %d in\r\n",
                                   i4NextFilter);
                        CliPrintf (CliHandle, "!\r\n");
                    }
                }
                else
                {

                    if (CliIsMemberPort (OutPortList.pu1_OctetList,
                                         ISS_PORTLIST_LEN,
                                         i4Index) == CLI_SUCCESS)
                    {
                        CliPrintf (CliHandle, "mac access-group %d out\r\n",
                                   i4NextFilter);
                        CliPrintf (CliHandle, "!\r\n");
                    }
                }

            }
        }

        i4PrevFilter = i4NextFilter;

        i1OutCome =
            nmhGetNextIndexIssExtL2FilterTable (i4PrevFilter, &i4NextFilter);
    }

    /*UDB Access Lists */
    i1OutCome =
        nmhGetFirstIndexIssAclUserDefinedFilterTable ((UINT4 *) &i4NextFilter);

    while (i1OutCome != SNMP_FAILURE)
    {

        MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
        MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);

        nmhGetIssAclUserDefinedFilterStatus (i4NextFilter, &i4RowStatus);

        if (i4RowStatus == ISS_ACTIVE)
        {
            i4Direction = ACL_ACCESS_IN;

            nmhGetIssAclUserDefinedFilterInPortList (i4NextFilter, &InPortList);

            if (i4Index == -1)
            {
                i4Count = 0;

                if (i4Direction == ACL_ACCESS_IN)
                {
                    do
                    {
                        if (CliIsMemberPort (InPortList.pu1_OctetList,
                                             ISS_PORTLIST_LEN,
                                             i4Count) == CLI_SUCCESS)
                        {
                            CliPrintf (CliHandle,
                                       "interface gigabitethernet 0/%d\r\n",
                                       i4Count);
                            CliPrintf (CliHandle,
                                       "user-defined access-group %d in\r\n",
                                       i4NextFilter);
                            CliPrintf (CliHandle, "!\r\n");

                        }

                        i4Count++;

                    }
                    while (i4Count <= SYS_DEF_MAX_PHYSICAL_INTERFACES);

                }

            }
            else
            {

                if (CliIsMemberPort (InPortList.pu1_OctetList,
                                     ISS_PORTLIST_LEN, i4Index) == CLI_SUCCESS)
                {
                    if (i4Direction == ACL_ACCESS_IN)
                    {
                        CliPrintf (CliHandle,
                                   "user-defined access-group %d in\r\n",
                                   i4NextFilter);
                        CliPrintf (CliHandle, "!\r\n");
                    }

                }

            }

        }

        i4PrevFilter = i4NextFilter;
        i1OutCome =
            nmhGetNextIndexIssAclUserDefinedFilterTable ((UINT4) i4PrevFilter,
                                                         (UINT4 *)
                                                         &i4NextFilter);
    }

    ISS_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : StdAclGetCfgPrompt                                 */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
StdAclGetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    INT4                i4StdAclIndex;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    /* NULL is passed to return "config-std-nacl" as the prompt 
     * for the mode  */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-std-nacl");
        return TRUE;
    }

    u4Len = STRLEN (CLI_STDACL_MODE);

    if (STRNCMP (pi1ModeName, CLI_STDACL_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    i4StdAclIndex = CLI_ATOI (pi1ModeName);

    /* Set the mode information for CLI */
    CLI_SET_STDACLID (i4StdAclIndex);

    STRCPY (pi1DispStr, "(config-std-nacl)#");
    return TRUE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ExtAclGetCfgPrompt                                 */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
ExtAclGetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    INT4                i4ExtAclIndex;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    /* NULL is passed to return "config-std-nacl" as the prompt 
     * for the mode  */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-ext-nacl");
        return TRUE;
    }

    u4Len = STRLEN (CLI_EXTACL_MODE);
    if (STRNCMP (pi1ModeName, CLI_EXTACL_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    i4ExtAclIndex = CLI_ATOI (pi1ModeName);

    /* Set the mode information for CLI */
    CLI_SET_EXTACL (i4ExtAclIndex);

    STRCPY (pi1DispStr, "(config-ext-nacl)#");
    return TRUE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MacAclGetCfgPrompt                                 */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
MacAclGetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    INT4                i4MacAclIndex;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    /* NULL is passed to return "config-std-nacl" as the prompt 
     * for the mode  */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-ext-macl");
        return TRUE;
    }

    u4Len = STRLEN (CLI_MACACL_MODE);
    if (STRNCMP (pi1ModeName, CLI_MACACL_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    i4MacAclIndex = CLI_ATOI (pi1ModeName);

    /* Set the mode information for CLI */
    CLI_SET_MACACL (i4MacAclIndex);

    STRCPY (pi1DispStr, "(config-ext-macl)#");
    return TRUE;

}

/*****************************************************************************/
/*     FUNCTION NAME    : AclIPv6GetCfgPrompt                                */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/
INT1
AclIPv6GetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = 0;
    INT4                i4IPv6AclIndex = 0;

    if (pi1DispStr == NULL)
    {
        return FALSE;
    }

    /* NULL is passed to return "config-ipv6-acl" as the prompt
       for the mode  */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-ipv6-acl");
        return TRUE;
    }

    u4Len = STRLEN (CLI_IPV6ACL_MODE);
    if (STRNCMP (pi1ModeName, CLI_IPV6ACL_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;

    i4IPv6AclIndex = CLI_ATOI (pi1ModeName);

    /* Set the mode information for CLI */
    CLI_SET_IPV6ACL (i4IPv6AclIndex);
    STRCPY (pi1DispStr, "(config-ipv6-acl)#");
    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UserDefAclGetCfgPrompt                             */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
UserDefAclGetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    INT4                i4UserDefAclIndex;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    /* NULL is passed to return "config-std-nacl" as the prompt 
     * for the mode  */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-userdef-acl");
        return TRUE;
    }

    u4Len = STRLEN (CLI_USRDEFACL_MODE);
    if (STRNCMP (pi1ModeName, CLI_USRDEFACL_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    i4UserDefAclIndex = CLI_ATOI (pi1ModeName);

    /* Set the mode information for CLI */
    CLI_SET_USERDEFFACL (i4UserDefAclIndex);

    STRCPY (pi1DispStr, "(config-userdef-acl)#");
    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclTestIpParams                                    */
/*                                                                           */
/*     DESCRIPTION      : This function tests the IP address, mask for a     */
/*                        filter                                             */
/*                                                                           */
/*     INPUT            : CliHandle - CLI HAndler                            */
/*                        u1Type - whether Source/Destination Ip address     */
/*                        information must be tested                         */
/*                        i4FilterNo - IP Filter Number                      */
/*                        i4IpType - Whether IPaddress and/or mask is user   */
/*                        input                                              */
/*                        u4IpAddr - Ip Address to be tested                 */
/*                        u4IpMask - Ip Mask to be tested                    */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclTestIpParams (UINT1 u1Type, INT4 i4FilterNo, UINT4 u4IpType, UINT4 u4IpAddr,
                 UINT4 u4IpMask)
{
    UINT4               u4ErrCode;

    if (u1Type == ACL_SRC)
    {
        if (u4IpType != ACL_ANY)
        {
            /* Source IP address has been specified */
            if (nmhTestv2IssExtL3FilterSrcIpAddr
                (&u4ErrCode, i4FilterNo, u4IpAddr) == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (u4IpType != ACL_HOST_IP)
            {
                /*Subnet Mask has been specified */
                if (nmhTestv2IssExtL3FilterSrcIpAddrMask (&u4ErrCode,
                                                          i4FilterNo,
                                                          u4IpMask) ==
                    SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
            }
        }
    }
    else if (u1Type == ACL_DST)
    {
        if (u4IpType != ACL_ANY)
        {
            /* Destination IP address has been specified */
            if (nmhTestv2IssExtL3FilterDstIpAddr
                (&u4ErrCode, i4FilterNo, u4IpAddr) == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (u4IpType != ACL_HOST_IP)
            {
                if (nmhTestv2IssExtL3FilterDstIpAddrMask
                    (&u4ErrCode, i4FilterNo, u4IpMask) == SNMP_FAILURE)
                {
                    /*Subnet Mask has been specified */
                    return (CLI_FAILURE);
                }
            }
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclSetIpParams                                     */
/*                                                                           */
/*     DESCRIPTION      : This function sets the IP address, mask for a      */
/*                        filter                                             */
/*                                                                           */
/*     INPUT            : CliHandle - CLI HAndler                            */
/*                        u1Type - whether Source/Destination Ip address     */
/*                        information must                                   */
/*                        be set                                             */
/*                        i4FilterNo - IP Filter Number                      */
/*                        i4IpType - Whether IPaddress and/or mask is user   */
/*                        input                                              */
/*                        u4IpAddr - Ip Address to be set                    */
/*                        u4IpMask - Ip Maxk to be set                       */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/

INT4
AclSetIpParams (UINT1 u1Type, INT4 i4FilterNo, UINT4 u4IpType, UINT4 u4IpAddr,
                UINT4 u4IpMask)
{
    UINT4               u4IpSubnet;

    if (u1Type == ACL_SRC)
    {
        if (u4IpType != ACL_ANY)
        {
            /* Source IP address has been specified */
            if (u4IpType != ACL_HOST_IP)
            {
                u4IpSubnet = (u4IpAddr & u4IpMask);

                if (nmhSetIssExtL3FilterSrcIpAddr (i4FilterNo, u4IpSubnet)
                    == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                /*Subnet Mask has been specified */
                if (nmhSetIssExtL3FilterSrcIpAddrMask (i4FilterNo,
                                                       u4IpMask) ==
                    SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
            }
            else
            {
                if (nmhSetIssExtL3FilterSrcIpAddr (i4FilterNo, u4IpAddr)
                    == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                if (nmhSetIssExtL3FilterSrcIpAddrMask (i4FilterNo,
                                                       (UINT4)
                                                       CLI_INET_ADDR
                                                       (HOST_MASK)) ==
                    SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
            }
        }
    }
    else if (u1Type == ACL_DST)
    {
        if (u4IpType != ACL_ANY)
        {
            /* Destination IP address has been specified */
            if (u4IpType != ACL_HOST_IP)
            {
                u4IpSubnet = (u4IpAddr & u4IpMask);

                if (nmhSetIssExtL3FilterDstIpAddr (i4FilterNo, u4IpSubnet)
                    == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                if (nmhSetIssExtL3FilterDstIpAddrMask (i4FilterNo,
                                                       u4IpMask) ==
                    SNMP_FAILURE)
                {
                    /*Subnet Mask has been specified */
                    return (CLI_FAILURE);
                }
            }
            else
            {
                if (nmhSetIssExtL3FilterDstIpAddr (i4FilterNo, u4IpAddr)
                    == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                if (nmhSetIssExtL3FilterDstIpAddrMask (i4FilterNo,
                                                       (UINT4)
                                                       CLI_INET_ADDR
                                                       (HOST_MASK)) ==
                    SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
            }

        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclCliPrintPortList                                */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Port list               */
/*                                                                           */
/*     INPUT            :  piIfName - Port Name                              */
/*                         CliHandle-CLI Handler                             */
/*                         i4CommaCount- position of port in port list       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

VOID
AclCliPrintPortList (tCliHandle CliHandle, INT4 i4CommaCount, UINT1 *piIfName)
{
    INT4                i4Times;
    INT1                i1Loop;

    i4Times = ISS_PORTLIST_LEN * 2;

    if (i4CommaCount == 1)
    {
        CliPrintf (CliHandle, "%s", piIfName);
        return;
    }
    for (i1Loop = 1; i1Loop < i4Times; i1Loop++)
    {
        /* Only 4 ports per line will be displayed */
        if (i4CommaCount <= (4 * i1Loop))
        {
            if (i4CommaCount == (4 * (i1Loop - 1)) + 1)
            {
                CliPrintf (CliHandle, " \r\n%-33s", " ");
            }
            CliPrintf (CliHandle, " , %s", piIfName);
            break;
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclCliPrintPortList                                */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Port list               */
/*                                                                           */
/*     INPUT            :  piIfName - Port Name                              */
/*                         CliHandle-CLI Handler                             */
/*                         i4CommaCount- position of port in port list       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
AclRedirectFilterConfig (tCliHandle CliHandle, INT4 i4Action, INT4 i4IsPortList,
                         INT4 i4TrafficDist,
                         INT1 *pi1IfName0, INT1 *pi1IfListStr0,
                         INT1 *pi1IfName1, INT1 *pi1IfListStr1,
                         INT1 *pi1IfName2, INT1 *pi1IfListStr2,
                         UINT1 u1UdbFilterPosition)
{

    tSNMP_OCTET_STRING_TYPE RedirectPortGroup;
    UINT4               u4NewRedirectGrpIndex = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4IfIndex = 0x0;
    INT4                i4FilterNo = 0;
    tPortList          *pPorts = NULL;

    ISS_MEMSET (&RedirectPortGroup, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (i4IsPortList != ISS_REDIRECT_TO_PORTLIST)
    {
        if (pi1IfListStr2 != NULL)
        {
            if (CfaCliGetIfIndex (pi1IfName2, pi1IfListStr2, &u4IfIndex) ==
                CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Port Channel does Not Exist\r\n");
                return CLI_FAILURE;
            }

        }

        RedirectPortGroup.pu1_OctetList = (UINT1 *) &u4IfIndex;
        RedirectPortGroup.i4_Length = sizeof (UINT4);
    }
    else
    {
        pPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

        if (pPorts == NULL)
        {
            CliPrintf (CliHandle,
                       "\r%% Error in Allocating memory for bitlist for Redirect Port List \r\n");
            return CLI_FAILURE;
        }

        ISS_MEMSET (pPorts, 0, sizeof (tPortList));

        if (pi1IfListStr0 != NULL)
        {
            CfaCliGetIfList (pi1IfName0, pi1IfListStr0, *pPorts,
                             sizeof (tPortList));
        }

        if (pi1IfListStr1 != NULL)
        {
            CfaCliGetIfList (pi1IfName1, pi1IfListStr1, *pPorts,
                             sizeof (tPortList));
        }

        RedirectPortGroup.pu1_OctetList = *pPorts;
        RedirectPortGroup.i4_Length = sizeof (tPortList);
    }

    i4FilterNo = CLI_GET_STDACLID ();

    /* Step 1: Get the Corresponding L2/L3/UDB Filter Status and
     * see if the This Filter is already matched to some
     * Redirect Group. If Not Go to Step 2 */

    /* Step 2: Run Through save the lowest free Index
     * in the case match succeeds use that index else use lowest
     * free Index */

    /*Step 3: If the Filter is newly attached then Set the Parameters
     * of the for new Group . This is done by first setting the 
     * Row Status as Create And Wait and then moving to ACtive */

    /* Step 4: If the filter is already matches the Redirect Group
     * then See if any of the Parameter has got changed If So
     * delete all the filters using (NOT_IN_SERVICE) and then used
     * newly formed parametrs and set the filters again by moving
     * to Active */

    /* Please Note : Currently Comaprison of Value
     * is not being done. Need to be taken care. */

    nmhGetIssRedirectInterfaceGrpIdNextFree (&u4NewRedirectGrpIndex);
    if (u4NewRedirectGrpIndex == 0)
    {
        CliPrintf (CliHandle,
                   "\r%% No More Free Index For Redirect Group Id  \r\n");
        if (pPorts != NULL)
        {
            FsUtilReleaseBitList (*pPorts);
        }

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2IssRedirectInterfaceGrpStatus
        (&u4ErrCode, u4NewRedirectGrpIndex,
         ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        if (pPorts != NULL)
        {
            FsUtilReleaseBitList (*pPorts);
        }

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetIssRedirectInterfaceGrpStatus
        (u4NewRedirectGrpIndex, ISS_CREATE_AND_WAIT) == SNMP_FAILURE)

    {
        if (pPorts != NULL)
        {
            FsUtilReleaseBitList (*pPorts);
        }

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2IssRedirectInterfaceGrpFilterId
        (&u4ErrCode, u4NewRedirectGrpIndex, i4FilterNo) == SNMP_FAILURE)
    {
        if (pPorts != NULL)
        {
            FsUtilReleaseBitList (*pPorts);
        }

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetIssRedirectInterfaceGrpFilterId
        (u4NewRedirectGrpIndex, i4FilterNo) == SNMP_FAILURE)
    {
        if (pPorts != NULL)
        {
            FsUtilReleaseBitList (*pPorts);
        }

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2IssRedirectInterfaceGrpFilterType
        (&u4ErrCode, u4NewRedirectGrpIndex, i4Action) == SNMP_FAILURE)

    {
        if (pPorts != NULL)
        {
            FsUtilReleaseBitList (*pPorts);
        }

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetIssRedirectInterfaceGrpFilterType
        (u4NewRedirectGrpIndex, i4Action) == SNMP_FAILURE)
    {
        if (pPorts != NULL)
        {
            FsUtilReleaseBitList (*pPorts);
        }

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2IssRedirectInterfaceGrpType
        (&u4ErrCode, u4NewRedirectGrpIndex, i4IsPortList) == SNMP_FAILURE)
    {
        if (pPorts != NULL)
        {
            FsUtilReleaseBitList (*pPorts);
        }

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetIssRedirectInterfaceGrpType
        (u4NewRedirectGrpIndex, i4IsPortList) == SNMP_FAILURE)
    {
        if (pPorts != NULL)
        {
            FsUtilReleaseBitList (*pPorts);
        }

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2IssRedirectInterfaceGrpDistByte
        (&u4ErrCode, u4NewRedirectGrpIndex, i4TrafficDist) == SNMP_FAILURE)
    {
        if (pPorts != NULL)
        {
            FsUtilReleaseBitList (*pPorts);
        }

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetIssRedirectInterfaceGrpDistByte
        (u4NewRedirectGrpIndex, i4TrafficDist) == SNMP_FAILURE)
    {
        if (pPorts != NULL)
        {
            FsUtilReleaseBitList (*pPorts);
        }

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2IssRedirectInterfaceGrpPortList
        (&u4ErrCode, u4NewRedirectGrpIndex, &RedirectPortGroup) == SNMP_FAILURE)

    {
        if (pPorts != NULL)
        {
            FsUtilReleaseBitList (*pPorts);
        }

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetIssRedirectInterfaceGrpPortList
        (u4NewRedirectGrpIndex, &RedirectPortGroup) == SNMP_FAILURE)

    {
        if (pPorts != NULL)
        {
            FsUtilReleaseBitList (*pPorts);
        }

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (pPorts != NULL)
    {
        FsUtilReleaseBitList (*pPorts);
    }
    if (nmhTestv2IssRedirectInterfaceGrpUdbPosition
        (&u4ErrCode, u4NewRedirectGrpIndex,
         (INT4) u1UdbFilterPosition) == SNMP_FAILURE)
    {

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetIssRedirectInterfaceGrpUdbPosition
        (u4NewRedirectGrpIndex, (INT4) u1UdbFilterPosition) == SNMP_FAILURE)
    {

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return (CLI_SUCCESS);
}
#endif
