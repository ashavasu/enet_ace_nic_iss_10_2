/* $Id: fsissewr.c,v 1.1 2012/03/02 11:16:13 siva Exp $*/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsisselw.h"
# include  "fsissewr.h"
# include  "fsissedb.h"
# include  "iss.h"

INT4
GetNextIndexIssExtRateCtrlTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIssExtRateCtrlTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIssExtRateCtrlTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

VOID
RegisterFSISSE ()
{
    SNMPRegisterMibWithLock (&fsisseOID, &fsisseEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsisseOID, (const UINT1 *) "fsissext");
}

VOID
UnRegisterFSISSE ()
{
    SNMPUnRegisterMib (&fsisseOID, &fsisseEntry);
    SNMPDelSysorEntry (&fsisseOID, (const UINT1 *) "fsissext");
}

INT4
IssExtRateCtrlDLFLimitValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtRateCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtRateCtrlDLFLimitValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssExtRateCtrlBCASTLimitValueGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtRateCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtRateCtrlBCASTLimitValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssExtRateCtrlMCASTLimitValueGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtRateCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtRateCtrlMCASTLimitValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssExtRateCtrlPortRateLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtRateCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtRateCtrlPortRateLimit
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssExtRateCtrlPortBurstSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtRateCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtRateCtrlPortBurstSize
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssExtRateCtrlDLFLimitValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtRateCtrlDLFLimitValue
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssExtRateCtrlBCASTLimitValueSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetIssExtRateCtrlBCASTLimitValue
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssExtRateCtrlMCASTLimitValueSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetIssExtRateCtrlMCASTLimitValue
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssExtRateCtrlPortRateLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtRateCtrlPortRateLimit
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssExtRateCtrlPortBurstSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtRateCtrlPortBurstSize
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssExtRateCtrlDLFLimitValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2IssExtRateCtrlDLFLimitValue (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
IssExtRateCtrlBCASTLimitValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2IssExtRateCtrlBCASTLimitValue (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
IssExtRateCtrlMCASTLimitValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2IssExtRateCtrlMCASTLimitValue (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
IssExtRateCtrlPortRateLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2IssExtRateCtrlPortRateLimit (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
IssExtRateCtrlPortBurstSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2IssExtRateCtrlPortBurstSize (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
IssExtRateCtrlTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssExtRateCtrlTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIssExtL2FilterTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIssExtL2FilterTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIssExtL2FilterTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IssExtL2FilterPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL2FilterPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
IssExtL2FilterEtherTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL2FilterEtherType (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
IssExtL2FilterProtocolTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL2FilterProtocolType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IssExtL2FilterDstMacAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetIssExtL2FilterDstMacAddr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList));

}

INT4
IssExtL2FilterSrcMacAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetIssExtL2FilterSrcMacAddr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList));

}

INT4
IssExtL2FilterVlanIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL2FilterVlanId (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
IssExtL2FilterInPortListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL2FilterInPortList
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
IssExtL2FilterActionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL2FilterAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
IssExtL2FilterMatchCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL2FilterMatchCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IssExtL2FilterStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL2FilterStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
IssExtL2FilterOutPortListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL2FilterOutPortList
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
IssExtL2FilterDirectionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL2FilterDirection (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
IssExtL2FilterPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL2FilterPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
IssExtL2FilterEtherTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL2FilterEtherType (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
IssExtL2FilterProtocolTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL2FilterProtocolType
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
IssExtL2FilterDstMacAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL2FilterDstMacAddr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)));

}

INT4
IssExtL2FilterSrcMacAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL2FilterSrcMacAddr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)));

}

INT4
IssExtL2FilterVlanIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL2FilterVlanId (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
IssExtL2FilterInPortListSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL2FilterInPortList
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
IssExtL2FilterActionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL2FilterAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
IssExtL2FilterStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL2FilterStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
IssExtL2FilterOutPortListSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL2FilterOutPortList
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
IssExtL2FilterDirectionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL2FilterDirection (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
IssExtL2FilterPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL2FilterPriority (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
IssExtL2FilterEtherTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL2FilterEtherType (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
IssExtL2FilterProtocolTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL2FilterProtocolType (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->u4_ULongValue));

}

INT4
IssExtL2FilterDstMacAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return (nmhTestv2IssExtL2FilterDstMacAddr (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               (*(tMacAddr *) pMultiData->
                                                pOctetStrValue->
                                                pu1_OctetList)));

}

INT4
IssExtL2FilterSrcMacAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return (nmhTestv2IssExtL2FilterSrcMacAddr (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               (*(tMacAddr *) pMultiData->
                                                pOctetStrValue->
                                                pu1_OctetList)));

}

INT4
IssExtL2FilterVlanIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL2FilterVlanId (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
IssExtL2FilterInPortListTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL2FilterInPortList (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->pOctetStrValue));

}

INT4
IssExtL2FilterActionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL2FilterAction (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
IssExtL2FilterStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL2FilterStatus (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
IssExtL2FilterOutPortListTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL2FilterOutPortList (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->pOctetStrValue));

}

INT4
IssExtL2FilterDirectionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL2FilterDirection (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
IssExtL2FilterTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssExtL2FilterTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIssExtL3FilterTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIssExtL3FilterTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIssExtL3FilterTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IssExtL3FilterPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL3FilterPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
IssExtL3FilterProtocolGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL3FilterProtocol (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
IssExtL3FilterMessageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL3FilterMessageType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssExtL3FilterMessageCodeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL3FilterMessageCode
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssExtL3FilterDstIpAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL3FilterDstIpAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
IssExtL3FilterSrcIpAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL3FilterSrcIpAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
IssExtL3FilterDstIpAddrMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL3FilterDstIpAddrMask
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IssExtL3FilterSrcIpAddrMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL3FilterSrcIpAddrMask
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IssExtL3FilterMinDstProtPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL3FilterMinDstProtPort
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IssExtL3FilterMaxDstProtPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL3FilterMaxDstProtPort
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IssExtL3FilterMinSrcProtPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL3FilterMinSrcProtPort
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IssExtL3FilterMaxSrcProtPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL3FilterMaxSrcProtPort
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IssExtL3FilterInPortListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL3FilterInPortList
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
IssExtL3FilterOutPortListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL3FilterOutPortList
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
IssExtL3FilterAckBitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL3FilterAckBit (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
IssExtL3FilterRstBitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL3FilterRstBit (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
IssExtL3FilterTosGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL3FilterTos (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
IssExtL3FilterDscpGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL3FilterDscp (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
IssExtL3FilterDirectionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL3FilterDirection (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
IssExtL3FilterActionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL3FilterAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
IssExtL3FilterMatchCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL3FilterMatchCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IssExtL3FilterStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssExtL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssExtL3FilterStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
IssExtL3FilterPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL3FilterPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
IssExtL3FilterProtocolSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL3FilterProtocol (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
IssExtL3FilterMessageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL3FilterMessageType
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssExtL3FilterMessageCodeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL3FilterMessageCode
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssExtL3FilterDstIpAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL3FilterDstIpAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
IssExtL3FilterSrcIpAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL3FilterSrcIpAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
IssExtL3FilterDstIpAddrMaskSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL3FilterDstIpAddrMask
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
IssExtL3FilterSrcIpAddrMaskSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL3FilterSrcIpAddrMask
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
IssExtL3FilterMinDstProtPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL3FilterMinDstProtPort
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
IssExtL3FilterMaxDstProtPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL3FilterMaxDstProtPort
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
IssExtL3FilterMinSrcProtPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL3FilterMinSrcProtPort
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
IssExtL3FilterMaxSrcProtPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL3FilterMaxSrcProtPort
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
IssExtL3FilterInPortListSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL3FilterInPortList
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
IssExtL3FilterOutPortListSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL3FilterOutPortList
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
IssExtL3FilterAckBitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL3FilterAckBit (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
IssExtL3FilterRstBitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL3FilterRstBit (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
IssExtL3FilterTosSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL3FilterTos (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
IssExtL3FilterDscpSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL3FilterDscp (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
IssExtL3FilterDirectionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL3FilterDirection (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
IssExtL3FilterActionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL3FilterAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
IssExtL3FilterStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssExtL3FilterStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
IssExtL3FilterPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL3FilterPriority (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
IssExtL3FilterProtocolTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL3FilterProtocol (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
IssExtL3FilterMessageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL3FilterMessageType (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
IssExtL3FilterMessageCodeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL3FilterMessageCode (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
IssExtL3FilterDstIpAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL3FilterDstIpAddr (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
IssExtL3FilterSrcIpAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL3FilterSrcIpAddr (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
IssExtL3FilterDstIpAddrMaskTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL3FilterDstIpAddrMask (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->u4_ULongValue));

}

INT4
IssExtL3FilterSrcIpAddrMaskTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL3FilterSrcIpAddrMask (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->u4_ULongValue));

}

INT4
IssExtL3FilterMinDstProtPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL3FilterMinDstProtPort (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->u4_ULongValue));

}

INT4
IssExtL3FilterMaxDstProtPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL3FilterMaxDstProtPort (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->u4_ULongValue));

}

INT4
IssExtL3FilterMinSrcProtPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL3FilterMinSrcProtPort (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->u4_ULongValue));

}

INT4
IssExtL3FilterMaxSrcProtPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL3FilterMaxSrcProtPort (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->u4_ULongValue));

}

INT4
IssExtL3FilterInPortListTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL3FilterInPortList (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->pOctetStrValue));

}

INT4
IssExtL3FilterOutPortListTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL3FilterOutPortList (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->pOctetStrValue));

}

INT4
IssExtL3FilterAckBitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL3FilterAckBit (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
IssExtL3FilterRstBitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL3FilterRstBit (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
IssExtL3FilterTosTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL3FilterTos (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
IssExtL3FilterDscpTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL3FilterDscp (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
IssExtL3FilterDirectionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL3FilterDirection (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
IssExtL3FilterActionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL3FilterAction (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
IssExtL3FilterStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2IssExtL3FilterStatus (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
IssExtL3FilterTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssExtL3FilterTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
