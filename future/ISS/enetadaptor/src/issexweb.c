/* $Id: issexweb.c,v 1.1 2012/03/02 11:16:13 siva Exp $ */

#ifdef WEBNM_WANTED
#include "issexweb.h"
#include "fsissecli.h"
#include "fsissewr.h"
extern tSNMP_OCTET_STRING_TYPE *allocmem_octetstring (INT4);
extern void         free_octetstring (tSNMP_OCTET_STRING_TYPE *);
/*********************************************************************
*  Function Name : IssProcessCustomPages
*  Description   : This function processes the Pages specific to
*                  target
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
INT4
IssProcessCustomPages (tHttp * pHttp)
{
    UINT4               u4Count;

    for (u4Count = 0; asIssTargetpages[u4Count].au1Page[0] != '\0'; u4Count++)
    {
        if (STRCMP (pHttp->ai1HtmlName, asIssTargetpages[u4Count].au1Page) == 0)
        {
            asIssTargetpages[u4Count].pfunctPtr (pHttp);
            return ISS_SUCCESS;
        }
    }
    return ISS_FAILURE;
}

/************************************************************************ 
*  Function Name   : IssRedirectMacFilterPage 
*  Description     : This function will redirect Mac Filter page 
*  Input           : pHttp - Pointer to http entry where the html file 
*                    name needs to be changed(copied) 
*  Output          : None 
*  Returns         : None 
************************************************************************/
VOID
IssRedirectMacFilterPage (tHttp * pHttp)
{
    STRCPY (pHttp->ai1HtmlName, "wintegra_mac_filterconf.html");
}

/************************************************************************ 
*  Function Name   : IssRedirectDiffSrvPage 
*  Description     : This function will redirect Diffsrv page 
*  Input           : pHttp - Pointer to http entry where the html file 
*                    name needs to be changed(copied) 
*  Output          : None 
*  Returns         : None 
************************************************************************/
VOID
IssRedirectDiffSrvPage (tHttp * pHttp)
{
    STRCPY (pHttp->ai1HtmlName, "wintegra_dfs_globalconf.html");
}

/************************************************************************ 
*  Function Name   : IssRedirectRateCtrlPage
*  Description     : This function will redirect Mac Filter page
*  Input           : pHttp - Pointer to http entry where the html file
*                    name needs to be changed(copied)
*  Output          : None
*  Returns         : None
************************************************************************/
VOID
IssRedirectRateCtrlPage (tHttp * pHttp)
{
    STRCPY (pHttp->ai1HtmlName, "wintegra_port_ratectrl.html");
}

/*********************************************************************
*  Function Name : IssDxProcessIPFilterConfPage 
*  Description   : This function processes the request coming for the IP 
*                  Filter Configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssDxProcessIPFilterConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssDxProcessIPFilterConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssDxProcessIPFilterConfPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessUDBFilterPage 
*  Description   : This function processes the request coming for the IP 
*                  Filter Configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssProcessUDBFilterPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessUDBFilterPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessUDBFilterPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessRedirectPage 
*  Description   : This function processes the request coming for the IP 
*                  Filter Configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssProcessRedirectPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessRedirectPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessRedirectPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessProvisionModePage 
*  Description   : This function processes the request coming for the IP 
*                  Filter Configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssProcessProvisionModePage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssProcessProvisionModePageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssProcessProvisionModePageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessProvisionModePageGet 
*  Description   : This function processes the request coming for the IP 
*                  Filter Configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssProcessProvisionModePageGet (tHttp * pHttp)
{
    INT4                i4ProvisionMode = 0;
    INT4                i4TriggerCommit = 0;

    WebnmRegisterLock (pHttp, IssLock, IssUnLock);
    ISS_LOCK ();
    STRCPY (pHttp->au1KeyString, "COMMIT_SUPPORT_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    if (nmhGetIssAclProvisionMode (&i4ProvisionMode) == SNMP_SUCCESS)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ProvisionMode);
    }
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));

    STRCPY (pHttp->au1KeyString, "COMMIT_ACTION_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    if (nmhGetIssAclTriggerCommit (&i4TriggerCommit) == SNMP_SUCCESS)
    {
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TriggerCommit);
    }

    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));

    ISS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : IssProcessProvisionModePageSet 
*  Description   : This function processes the request coming for the IP 
*                  Filter Configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssProcessProvisionModePageSet (tHttp * pHttp)
{
    INT4                i4ProvisionMode = 0;
    INT4                i4TriggerCommit = 0;
    UINT4               u4ErrCode = 0;

    ISS_LOCK ();

    STRCPY (pHttp->au1Name, "COMMIT_SUPPORT");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4ProvisionMode = ATOI (pHttp->au1Value);

    if (nmhTestv2IssAclProvisionMode (&u4ErrCode, i4ProvisionMode) ==
        SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to Test Commit Support");
        return;
    }
    if (nmhSetIssAclProvisionMode (i4ProvisionMode) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to set Commit Support");
        return;
    }
    STRCPY (pHttp->au1Name, "COMMIT_ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4TriggerCommit = ATOI (pHttp->au1Value);

    if (nmhTestv2IssAclTriggerCommit (&u4ErrCode, i4TriggerCommit) ==
        SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to Test Commit Action");
        return;
    }
    if (nmhSetIssAclTriggerCommit (i4TriggerCommit) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (CONST INT1 *) "Unable to set Commit Action");
        return;
    }
    ISS_UNLOCK ();
    IssProcessProvisionModePageGet (pHttp);

}

/*********************************************************************
*  Function Name : IssDxProcessIPStdFilterConfPage 
*  Description   : This function processes the request coming for the IP 
*                  standard Filter Configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssDxProcessIPStdFilterConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssDxProcessIPStdFilterConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssDxProcessIPStdFilterConfPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssProcessUDBFilterPageGet 
*  Description   : This function processes the get request for the IP
*                  standard filter configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessUDBFilterPageGet (tHttp * pHttp)
{
    INT4                i4FilterNo, i4CurrentFilterNo, i4RetVal, i4OutCome;
    UINT4               u4Temp = 0;
    UINT1               au1OffsetValue[ISS_UDB_MAX_OFFSET];
    UINT1               au1OffsetMask[ISS_UDB_MAX_OFFSET];
    tSNMP_OCTET_STRING_TYPE OffsetValue;
    tSNMP_OCTET_STRING_TYPE OffsetMask;
    tSNMP_MULTI_DATA_TYPE *pu1DataString = NULL;
    tSNMP_MULTI_DATA_TYPE *pu1Data = NULL;
    tPortList           InTempPortList;
    INT4                i4Counter = 0;
    INT4                i4LastCounter = 0;
    INT4                i4PosValue = 0;
    INT4                i4Form1 = 0;
    INT4                i4Form2 = 0;

    MEMSET (au1OffsetValue, 0, ISS_UDB_MAX_OFFSET);
    MEMSET (au1OffsetMask, 0, ISS_UDB_MAX_OFFSET);

    OffsetValue.pu1_OctetList = &au1OffsetValue[0];
    OffsetValue.i4_Length = ISS_UDB_MAX_OFFSET;
    OffsetMask.pu1_OctetList = &au1OffsetMask[0];
    OffsetMask.i4_Length = ISS_UDB_MAX_OFFSET;

    if ((pu1DataString = WebnmAllocMultiData ()) == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }

    if ((pu1Data = WebnmAllocMultiData ()) == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }

    WebnmRegisterLock (pHttp, IssLock, IssUnLock);
    ISS_LOCK ();

    pHttp->i4Write = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = pHttp->i4Write;

    i4OutCome =
        (INT4) (nmhGetFirstIndexIssAclUserDefinedFilterTable
                ((UINT4 *) &i4FilterNo));
    if ((i4OutCome == SNMP_FAILURE || i4FilterNo == 0))
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }
    do
    {
        i4LastCounter = 0;
        nmhGetIssAclUserDefinedFilterAction (i4FilterNo, &i4RetVal);

        if ((i4RetVal == 8) || (i4RetVal == 9) || (i4RetVal == 10))
        {
            i4CurrentFilterNo = i4FilterNo;
            continue;
        }
        i4Form1 = 1;

        pHttp->i4Write = u4Temp;
        STRCPY (pHttp->au1KeyString, "USER_DEFINED_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4FilterNo);

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "UB_ACTION");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "PACKET_TYPE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssAclUserDefinedFilterPktType (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OFFSET_BASE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssAclUserDefinedFilterOffSetBase (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetIssAclUserDefinedFilterOffSetValue (i4FilterNo, &OffsetValue);
        STRCPY (pHttp->au1KeyString, "OFFSET_POS1");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4Counter = 0;
        i4PosValue = 0;
        MEMSET (pHttp->au1DataString, 0, STRLEN (pHttp->au1DataString));
        for (i4Counter = 0; i4Counter < ISS_UDB_MAX_OFFSET; i4Counter++)
        {
            if (OffsetValue.pu1_OctetList[i4Counter] != 0)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Counter);
                i4PosValue = i4PosValue + 4;
                break;
            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OFFSET_VALUE1");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        i4Counter = 0;
        i4PosValue = 0;
        MEMSET (pHttp->au1DataString, 0, STRLEN (pHttp->au1DataString));
        for (i4Counter = i4LastCounter; i4Counter < ISS_UDB_MAX_OFFSET;
             i4Counter++)
        {

            if (OffsetValue.pu1_OctetList[i4Counter] != 0)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         OffsetValue.pu1_OctetList[i4Counter]);
                i4PosValue = i4PosValue + 4;
                i4LastCounter = i4Counter;
                break;
            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OFFSET_POS2");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (pHttp->au1DataString, 0, STRLEN (pHttp->au1DataString));
        for (i4Counter = i4LastCounter + 1; i4Counter < ISS_UDB_MAX_OFFSET;
             i4Counter++)
        {
            if (OffsetValue.pu1_OctetList[i4Counter] != 0)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Counter);
                i4PosValue = i4PosValue + 4;
                break;
            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OFFSET_VALUE2");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (pHttp->au1DataString, 0, STRLEN (pHttp->au1DataString));
        for (i4Counter = i4LastCounter + 1; i4Counter < ISS_UDB_MAX_OFFSET;
             i4Counter++)
        {

            if (OffsetValue.pu1_OctetList[i4Counter] != 0)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         OffsetValue.pu1_OctetList[i4Counter]);
                i4PosValue = i4PosValue + 4;
                i4LastCounter = i4Counter;
                break;

            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OFFSET_POS3");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (pHttp->au1DataString, 0, STRLEN (pHttp->au1DataString));
        for (i4Counter = i4LastCounter + 1; i4Counter < ISS_UDB_MAX_OFFSET;
             i4Counter++)
        {
            if (OffsetValue.pu1_OctetList[i4Counter] != 0)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Counter);
                i4PosValue = i4PosValue + 4;
                break;
            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OFFSET_VALUE3");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (pHttp->au1DataString, 0, STRLEN (pHttp->au1DataString));
        for (i4Counter = i4LastCounter + 1; i4Counter < ISS_UDB_MAX_OFFSET;
             i4Counter++)
        {

            if (OffsetValue.pu1_OctetList[i4Counter] != 0)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         OffsetValue.pu1_OctetList[i4Counter]);
                i4PosValue = i4PosValue + 4;
                i4LastCounter = i4Counter;
                break;
            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OFFSET_POS4");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (pHttp->au1DataString, 0, STRLEN (pHttp->au1DataString));
        for (i4Counter = i4LastCounter + 1; i4Counter < ISS_UDB_MAX_OFFSET;
             i4Counter++)
        {
            if (OffsetValue.pu1_OctetList[i4Counter] != 0)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Counter);
                i4PosValue = i4PosValue + 4;
                break;
            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OFFSET_VALUE4");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (pHttp->au1DataString, 0, STRLEN (pHttp->au1DataString));
        for (i4Counter = i4LastCounter + 1; i4Counter < ISS_UDB_MAX_OFFSET;
             i4Counter++)
        {

            if (OffsetValue.pu1_OctetList[i4Counter] != 0)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         OffsetValue.pu1_OctetList[i4Counter]);
                i4PosValue = i4PosValue + 4;
                i4LastCounter = i4Counter;
                break;
            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OFFSET_POS5");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (pHttp->au1DataString, 0, STRLEN (pHttp->au1DataString));
        for (i4Counter = i4LastCounter + 1; i4Counter < ISS_UDB_MAX_OFFSET;
             i4Counter++)
        {
            if (OffsetValue.pu1_OctetList[i4Counter] != 0)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Counter);
                i4PosValue = i4PosValue + 4;
                break;
            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OFFSET_VALUE5");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (pHttp->au1DataString, 0, STRLEN (pHttp->au1DataString));
        for (i4Counter = i4LastCounter + 1; i4Counter < ISS_UDB_MAX_OFFSET;
             i4Counter++)
        {

            if (OffsetValue.pu1_OctetList[i4Counter] != 0)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         OffsetValue.pu1_OctetList[i4Counter]);
                i4PosValue = i4PosValue + 4;
                i4LastCounter = i4Counter;
                break;
            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OFFSET_POS6");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (pHttp->au1DataString, 0, STRLEN (pHttp->au1DataString));
        for (i4Counter = i4LastCounter + 1; i4Counter < ISS_UDB_MAX_OFFSET;
             i4Counter++)
        {
            if (OffsetValue.pu1_OctetList[i4Counter] != 0)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Counter);
                i4PosValue = i4PosValue + 4;
                break;
            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OFFSET_VALUE6");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (pHttp->au1DataString, 0, STRLEN (pHttp->au1DataString));
        for (i4Counter = i4LastCounter + 1; i4Counter < ISS_UDB_MAX_OFFSET;
             i4Counter++)
        {

            if (OffsetValue.pu1_OctetList[i4Counter] != 0)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d",
                         OffsetValue.pu1_OctetList[i4Counter]);
                i4PosValue = i4PosValue + 4;
                i4LastCounter = i4Counter;
                break;
            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OFFSET_MASK");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssAclUserDefinedFilterOffSetMask (i4FilterNo, &OffsetMask);
        i4Counter = 0;
        i4PosValue = 0;
        MEMSET (pHttp->au1DataString, 0, STRLEN (pHttp->au1DataString));
        for (i4Counter = 0; i4Counter < ISS_UDB_MAX_OFFSET; i4Counter++)
        {
            if (OffsetMask.pu1_OctetList[i4Counter] != 0)
            {
                SPRINTF ((CHR1 *) & pHttp->au1DataString[i4PosValue], ":%x   ",
                         OffsetMask.pu1_OctetList[i4Counter]);
                i4PosValue = i4PosValue + 4;

            }
        }
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "SUB_VALUE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssAclUserDefinedFilterSubAction (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "SUB_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssAclUserDefinedFilterSubActionId (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "UB_PRIORITY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssAclUserDefinedFilterPriority (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        MEMSET (InTempPortList, 0, VLAN_PORT_LIST_SIZE);
        nmhGetIssAclUserDefinedFilterInPortList (i4FilterNo,
                                                 pu1DataString->pOctetStrValue);
        IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                InTempPortList);
        MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                InTempPortList, pu1DataString->pOctetStrValue->i4_Length);
        WebnmSendToSocket (pHttp, "IN_PORTS_LIST", pu1DataString,
                           ENM_PORT_LIST);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        i4CurrentFilterNo = i4FilterNo;
    }
    while (nmhGetNextIndexIssAclUserDefinedFilterTable
           ((UINT4) i4CurrentFilterNo, (UINT4 *) &i4FilterNo) == SNMP_SUCCESS);

    if (i4Form1 == 0)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    WebnmSendString (pHttp, ISS_TABLE_FLAG);

    u4Temp = pHttp->i4Write;
    i4OutCome =
        (INT4) (nmhGetFirstIndexIssAclUserDefinedFilterTable
                ((UINT4 *) &i4FilterNo));
    if ((i4OutCome == SNMP_FAILURE || i4FilterNo == 0))
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    do
    {
        nmhGetIssAclUserDefinedFilterAction (i4FilterNo, &i4RetVal);
        if ((i4RetVal == 1) || (i4RetVal == 2) || (i4RetVal == 7))
        {
            i4CurrentFilterNo = i4FilterNo;
            continue;
        }
        i4Form2 = 1;
        pHttp->i4Write = u4Temp;

        STRCPY (pHttp->au1KeyString, "USER_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4FilterNo);

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "UB_ACTION");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "FILTER_ONE_TYPE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssAclUserDefinedFilterIdOneType (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "FILTER_ONE_ID");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssAclUserDefinedFilterIdOne (i4FilterNo, (UINT4 *) &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "FILTER_TWO_TYPE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssAclUserDefinedFilterIdTwoType (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "FILTER_TWO_ID");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssAclUserDefinedFilterIdTwo (i4FilterNo, (UINT4 *) &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "UB_PRIORITY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssAclUserDefinedFilterPriority (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        MEMSET (InTempPortList, 0, VLAN_PORT_LIST_SIZE);
        nmhGetIssAclUserDefinedFilterInPortList (i4FilterNo,
                                                 pu1DataString->pOctetStrValue);
        IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                InTempPortList);
        MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                InTempPortList, pu1DataString->pOctetStrValue->i4_Length);
        WebnmSendToSocket (pHttp, "IN_PORTS_LIST", pu1DataString,
                           ENM_PORT_LIST);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        i4CurrentFilterNo = i4FilterNo;
    }
    while (nmhGetNextIndexIssAclUserDefinedFilterTable
           ((UINT4) i4CurrentFilterNo, (UINT4 *) &i4FilterNo) == SNMP_SUCCESS);

    if (i4Form2 == 0)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
    }

    ISS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : IssProcessUDBFilterPageSet 
*  Description   : This function processes the set request for the IP
*                 standard filter configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None 
*********************************************************************/

VOID
IssProcessUDBFilterPageSet (tHttp * pHttp)
{
    INT4                i4FilterNo = 0, i4PktType = 0, i4OffsetBase =
        0, i4Priority = 0, i4Action = 0, i4OneType = 0, i4FilterOneId =
        0, i4TwoType = 0, i4FilterTwoId = 0;
    INT4                i4SubAction, i4SubActionId;
    UINT4               u4ErrorCode = 0;
    UINT2               u2Offset1Pos = 0;
    UINT2               u2Offset1Val = 0;
    UINT2               u2Offset2Pos = 0;
    UINT2               u2Offset2Val = 0;
    UINT2               u2Offset3Pos = 0;
    UINT2               u2Offset3Val = 0;
    UINT2               u2Offset4Pos = 0;
    UINT2               u2Offset4Val = 0;
    UINT2               u2Offset5Pos = 0;
    UINT2               u2Offset5Val = 0;
    UINT2               u2Offset6Pos = 0;
    UINT2               u2Offset6Val = 0;
    UINT1               au1OffsetValueArray[ISS_UDB_MAX_OFFSET];
    UINT1               au1OffsetMaskArray[ISS_UDB_MAX_OFFSET];
    tSNMP_OCTET_STRING_TYPE SetValue, SetMask, InPortList;
    UINT1               u1Add = 0, u1Modify = 0, u1RowStatus;
    UINT1               u1InPortList[ISS_PORTLIST_LEN];

    INT4                i4RediretGrpFlag = 0x0;
    UINT4               u4MapFilterId = 0x0;
    INT4                i4GroupId = 0x0;
    INT4                i4OutCome = 0x0;
    INT4                i4CurrentAction = 0;
    INT4                i4CurrentGroupId = 0;
    INT4                i4MapFilterType = 0;

    MEMSET (au1OffsetValueArray, 0, ISS_UDB_MAX_OFFSET);
    MEMSET (au1OffsetMaskArray, 0, ISS_UDB_MAX_OFFSET);
    MEMSET (&SetValue, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&SetMask, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    SetValue.pu1_OctetList = au1OffsetValueArray;
    SetValue.i4_Length = ISS_UDB_MAX_OFFSET;

    SetMask.pu1_OctetList = au1OffsetMaskArray;
    SetMask.i4_Length = ISS_UDB_MAX_OFFSET;

    STRCPY (pHttp->au1Name, "userdefid");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4FilterNo = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    ISS_LOCK ();
    /* check for delete */
    if (STRCMP (pHttp->au1Value, ISS_DELETE) == 0)
    {
        if (nmhGetIssAclUserDefinedFilterAction (i4FilterNo, &i4CurrentAction)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            return;
        }
        if (i4CurrentAction == ISS_REDIRECT_TO)
        {
            i4OutCome =
                (INT4) (nmhGetFirstIndexIssRedirectInterfaceGrpTable
                        ((UINT4 *) &i4GroupId));
            if ((i4OutCome == SNMP_FAILURE))
            {
                ISS_UNLOCK ();
                IssSendError (pHttp,
                              (INT1 *)
                              "Delete : Redirect Group Not Available.");
                return;
            }
            do
            {
                if (nmhGetIssRedirectInterfaceGrpFilterType (i4GroupId,
                                                             &i4MapFilterType)
                    == SNMP_FAILURE)
                {
                    ISS_UNLOCK ();
                    return;
                }
                if (nmhGetIssRedirectInterfaceGrpFilterId
                    (i4GroupId, &u4MapFilterId) == SNMP_FAILURE)
                {
                    ISS_UNLOCK ();
                    return;
                }

                if ((i4MapFilterType == ISS_UDB_REDIRECT) &&
                    (u4MapFilterId == (UINT4) i4FilterNo))
                {

                    i4RediretGrpFlag = i4GroupId;
                }
                i4CurrentGroupId = i4GroupId;
            }
            while (nmhGetNextIndexIssRedirectInterfaceGrpTable
                   ((UINT4) i4CurrentGroupId,
                    (UINT4 *) &i4GroupId) == SNMP_SUCCESS);

            if (i4RediretGrpFlag != 0)
            {
                if (nmhSetIssRedirectInterfaceGrpStatus
                    (i4RediretGrpFlag, ISS_DESTROY) == SNMP_FAILURE)
                {
                    ISS_UNLOCK ();
                    IssSendError (pHttp,
                                  (INT1 *)
                                  "SET : Redirect Group Not Available.");
                    return;
                }
            }
        }

        if (nmhTestv2IssAclUserDefinedFilterStatus
            (&u4ErrorCode, i4FilterNo, ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "TEST :User Defined Filter Not Available.");
            return;
        }

        if (nmhSetIssAclUserDefinedFilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "SET :User Defined Filter Not Available.");
            return;
        }
        ISS_UNLOCK ();
        IssProcessUDBFilterPageGet (pHttp);
        return;
    }
    if (STRCMP (pHttp->au1Value, "Add") == 0)
    {
        u1Add = 1;
        u1RowStatus = ISS_CREATE_AND_WAIT;
        if (nmhTestv2IssAclUserDefinedFilterStatus
            (&u4ErrorCode, i4FilterNo, u1RowStatus) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Test : User Defined Filter Not Available.");
            return;
        }
        if (nmhSetIssAclUserDefinedFilterStatus (i4FilterNo, u1RowStatus) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "SET :User Defined Filter Not Available.");
            return;
        }
    }

    if (STRCMP (pHttp->au1Value, "Modify") == 0)
    {
        u1Modify = 1;
        u1RowStatus = ISS_NOT_IN_SERVICE;

        if (nmhSetIssAclUserDefinedFilterStatus (i4FilterNo, u1RowStatus) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *)
                          "SET MODIFY :User Defined Filter Not Available.");
            return;
        }
    }
    STRCPY (pHttp->au1Name, "filteraction");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);
    if (nmhTestv2IssAclUserDefinedFilterAction (&u4ErrorCode, i4FilterNo,
                                                i4Action) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Test: Invalid Action");
        return;
    }
    if (nmhSetIssAclUserDefinedFilterAction (i4FilterNo, i4Action) ==
        SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Set: Invalid Action");
        return;
    }

    STRCPY (pHttp->au1Name, "packettype");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PktType = ATOI (pHttp->au1Value);
    if (nmhTestv2IssAclUserDefinedFilterPktType (&u4ErrorCode, i4FilterNo,
                                                 i4PktType) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Test: Invalid Packet Type");
        return;
    }
    if (nmhSetIssAclUserDefinedFilterPktType (i4FilterNo, i4PktType) ==
        SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Set: Invalid Packet Type");
        return;
    }

    STRCPY (pHttp->au1Name, "offsetbase");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4OffsetBase = ATOI (pHttp->au1Value);
    if (nmhTestv2IssAclUserDefinedFilterOffSetBase (&u4ErrorCode, i4FilterNo,
                                                    i4OffsetBase) ==
        SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Test: Invalid Offset Base");
        return;
    }
    if (nmhSetIssAclUserDefinedFilterOffSetBase (i4FilterNo, i4OffsetBase) ==
        SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Set: Invalid Offset Base");
        return;
    }

    STRCPY (pHttp->au1Name, "offsetpos1");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u2Offset1Pos = (UINT2) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "offsetvalue1");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u2Offset1Val = (UINT2) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "offsetpos2");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u2Offset2Pos = (UINT2) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "offsetvalue2");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u2Offset2Val = (UINT2) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "offsetpos3");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u2Offset3Pos = (UINT2) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "offsetvalue3");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u2Offset3Val = (UINT2) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "offsetpos4");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u2Offset4Pos = (UINT2) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "offsetvalue4");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u2Offset4Val = (UINT2) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "offsetpos5");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u2Offset5Pos = (UINT2) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "offsetvalue5");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u2Offset5Val = (UINT2) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "offsetpos6");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u2Offset6Pos = (UINT2) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "offsetvalue6");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u2Offset6Val = (UINT2) ATOI (pHttp->au1Value);
    if (u2Offset1Val != 0)
    {
        au1OffsetValueArray[u2Offset1Pos] |= u2Offset1Val;
        au1OffsetMaskArray[u2Offset1Pos] |= 0xFF;
    }
    if (u2Offset2Val != 0)
    {
        au1OffsetValueArray[u2Offset2Pos] |= u2Offset2Val;
        au1OffsetMaskArray[u2Offset2Pos] |= 0xFF;
    }
    if (u2Offset3Val != 0)
    {
        au1OffsetValueArray[u2Offset3Pos] |= u2Offset3Val;
        au1OffsetMaskArray[u2Offset3Pos] |= 0xFF;
    }
    if (u2Offset4Val != 0)
    {
        au1OffsetValueArray[u2Offset4Pos] |= u2Offset4Val;
        au1OffsetMaskArray[u2Offset4Pos] |= 0xFF;
    }
    if (u2Offset5Val != 0)
    {
        au1OffsetValueArray[u2Offset5Pos] |= u2Offset5Val;
        au1OffsetMaskArray[u2Offset5Pos] |= 0xFF;
    }
    if (u2Offset6Val != 0)
    {
        au1OffsetValueArray[u2Offset6Pos] |= u2Offset6Val;
        au1OffsetMaskArray[u2Offset6Pos] |= 0xFF;
    }

    if (nmhTestv2IssAclUserDefinedFilterOffSetValue
        (&u4ErrorCode, i4FilterNo, &SetValue) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Test :Invalid Set value .");
        return;
    }

    if (nmhSetIssAclUserDefinedFilterOffSetValue (i4FilterNo, &SetValue) ==
        SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Set : Unable to set value.");
        return;
    }

    if (nmhTestv2IssAclUserDefinedFilterOffSetMask
        (&u4ErrorCode, i4FilterNo, &SetMask) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Test :Invalid Set value .");
        return;
    }

    if (nmhSetIssAclUserDefinedFilterOffSetMask (i4FilterNo, &SetMask) ==
        SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Set : Unable to set mask.");
        return;
    }
    STRCPY (pHttp->au1Name, "SUB_VALUE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4SubAction = ATOI (pHttp->au1Value);

    if (nmhTestv2IssAclUserDefinedFilterSubAction (&u4ErrorCode, i4FilterNo,
                                                   i4SubAction) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Test: Ivalid SubAction");
        return;
    }
    if (nmhSetIssAclUserDefinedFilterSubAction (i4FilterNo, i4SubAction) ==
        SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Set: Invalid SubAction");
        return;
    }
    STRCPY (pHttp->au1Name, "SUB_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4SubActionId = ATOI (pHttp->au1Value);

    if (nmhTestv2IssAclUserDefinedFilterSubActionId (&u4ErrorCode, i4FilterNo,
                                                     i4SubActionId) ==
        SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Test: Ivalid SubAction");
        return;
    }
    if (nmhSetIssAclUserDefinedFilterSubActionId (i4FilterNo, i4SubActionId) ==
        SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Set: Invalid SubAction");
        return;
    }

    STRCPY (pHttp->au1Name, "priority");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Priority = ATOI (pHttp->au1Value);
    if (nmhTestv2IssAclUserDefinedFilterPriority (&u4ErrorCode, i4FilterNo,
                                                  i4Priority) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Test: Invalid Priority");
        return;
    }
    if (nmhSetIssAclUserDefinedFilterPriority (i4FilterNo, i4Priority) ==
        SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Set: Invalid Priority");
        return;
    }
    STRCPY (pHttp->au1Name, "IN_PORTS_LIST");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        MEMSET (u1InPortList, 0, ISS_PORTLIST_LEN);
        issDecodeSpecialChar (pHttp->au1Value);
        if (STRCMP (pHttp->au1Value, "0") != 0)
        {
            if (IssPortListStringParser (pHttp->au1Value, u1InPortList) ==
                ENM_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp, (INT1 *) "Check the allowed in port list");
                return;
            }
        }
        InPortList.i4_Length = ISS_PORTLIST_LEN;
        InPortList.pu1_OctetList = MEM_CALLOC (InPortList.i4_Length, 1, UINT1);
        if (InPortList.pu1_OctetList == NULL)
        {
            ISS_UNLOCK ();
            return;
        }
        MEMCPY (InPortList.pu1_OctetList, u1InPortList, InPortList.i4_Length);
    }
    if ((InPortList.pu1_OctetList != NULL) &&
        (nmhSetIssAclUserDefinedFilterInPortList
         (i4FilterNo, &InPortList) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        if (u1Add != 1)
        {
            nmhSetIssAclUserDefinedFilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set In Port List.");
        return;
    }

    STRCPY (pHttp->au1Name, "filteronetype");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4OneType = ATOI (pHttp->au1Value);
    if (nmhSetIssAclUserDefinedFilterIdOneType (i4FilterNo, i4OneType) ==
        SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Set: Invalid One Type");
        return;
    }

    STRCPY (pHttp->au1Name, "filteroneid");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4FilterOneId = ATOI (pHttp->au1Value);
    if (i4FilterOneId != 0)
    {
        if (nmhTestv2IssAclUserDefinedFilterIdOne (&u4ErrorCode, i4FilterNo,
                                                   (UINT4) i4FilterOneId) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Test: Invalid Filter One Id");
            return;
        }
        if (nmhSetIssAclUserDefinedFilterIdOne
            (i4FilterNo, (UINT4) i4FilterOneId) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Set: Invalid Filter One Id ");
            return;
        }
    }
    STRCPY (pHttp->au1Name, "filtertwotype");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4TwoType = ATOI (pHttp->au1Value);
    if (nmhSetIssAclUserDefinedFilterIdTwoType (i4FilterNo, i4TwoType) ==
        SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Set: Invalid Two Type");
        return;
    }

    STRCPY (pHttp->au1Name, "filtertwoid");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4FilterTwoId = ATOI (pHttp->au1Value);
    if (i4FilterTwoId != 0)
    {
        if (nmhTestv2IssAclUserDefinedFilterIdTwo (&u4ErrorCode, i4FilterNo,
                                                   (UINT4) i4FilterTwoId) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Test: Invalid Filter Two Id");
            return;
        }
        if (nmhSetIssAclUserDefinedFilterIdTwo
            (i4FilterNo, (UINT4) i4FilterTwoId) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Set: Invalid Filter Two Id ");
            return;
        }

    }

    if ((u1Add == 1) || (u1Modify == 1))
    {
        if (nmhSetIssAclUserDefinedFilterStatus (i4FilterNo, ISS_ACTIVE) ==
            SNMP_FAILURE)
        {
            if (u1Modify != 1)
            {
                nmhSetIssAclUserDefinedFilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set Filter Status");
            return;
        }
    }
    ISS_UNLOCK ();
    IssProcessUDBFilterPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssProcessRedirectPageGet 
*  Description   : This function processes the get request for the IP
*                  standard filter configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssProcessRedirectPageGet (tHttp * pHttp)
{
    INT4                i4GroupId = 0, i4CurrentGroupId = 0, i4GrpFilterType =
        0, i4GrpFilterId = 0, i4OutCome = 0, i4GrpDistByte = 0, i4GrpType = 0;
    INT4                i4UdbPosition = 0;
    UINT4               u4Temp = 0;
    UINT4               u4InterfaceIndex = 0;
    tSNMP_MULTI_DATA_TYPE *pu1DataString = NULL;
    tPortList           PortList;
    INT1               *piIfName;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    piIfName = (INT1 *) au1IfName;
    MEMSET (PortList, 0, VLAN_PORT_LIST_SIZE);

    if ((pu1DataString = WebnmAllocMultiData ()) == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }

    pHttp->i4Write = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = pHttp->i4Write;
    WebnmRegisterLock (pHttp, IssLock, IssUnLock);

    ISS_LOCK ();

    i4OutCome =
        (INT4) (nmhGetFirstIndexIssRedirectInterfaceGrpTable
                ((UINT4 *) &i4GroupId));
    if ((i4OutCome == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }
    u4Temp = pHttp->i4Write;
    do
    {

        pHttp->i4Write = u4Temp;
        STRCPY (pHttp->au1KeyString, "GROUP_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4GroupId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "GP_FILTER_TYPE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssRedirectInterfaceGrpFilterType (i4GroupId, &i4GrpFilterType);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4GrpFilterType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "GP_FILTER_ID");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssRedirectInterfaceGrpFilterId (i4GroupId,
                                               (UINT4 *) &i4GrpFilterId);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4GrpFilterId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        MEMSET (pu1DataString, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (PortList, 0, BRG_PORT_LIST_SIZE);

        nmhGetIssRedirectInterfaceGrpType (i4GroupId, &i4GrpType);
        nmhGetIssRedirectInterfaceGrpPortList (i4GroupId,
                                               pu1DataString->pOctetStrValue);
        if (i4GrpType == ISS_REDIRECT_TO_PORTLIST)
        {
            IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                    PortList);
            MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList, PortList,
                    pu1DataString->pOctetStrValue->i4_Length);
            WebnmSendToSocket (pHttp, "GP_PORT_LIST", pu1DataString,
                               ENM_PORT_LIST);
        }

        if (i4GrpType == ISS_REDIRECT_TO_PORT)
        {
            STRCPY (pHttp->au1KeyString, "GP_PORT_LIST");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMCPY (&u4InterfaceIndex,
                    pu1DataString->pOctetStrValue->pu1_OctetList,
                    sizeof (UINT4));
            CfaCliGetIfName ((UINT4) u4InterfaceIndex, piIfName);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", piIfName);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        }

        STRCPY (pHttp->au1KeyString, "GP_DIST_BYTE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssRedirectInterfaceGrpDistByte (i4GroupId, &i4GrpDistByte);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4GrpDistByte);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "GP_UDBPOSITION_ID");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssRedirectInterfaceGrpUdbPosition (i4GroupId,
                                                  (INT4 *) &i4UdbPosition);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4UdbPosition);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        i4CurrentGroupId = i4GroupId;
    }
    while (nmhGetNextIndexIssRedirectInterfaceGrpTable
           ((UINT4) i4CurrentGroupId, (UINT4 *) &i4GroupId) == SNMP_SUCCESS);

    WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
    ISS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : IssProcessRedirectPageSet 
*  Description   : This function processes the set request for the IP
*                 standard filter configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None 
*********************************************************************/

VOID
IssProcessRedirectPageSet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE InLocalPortList;
    tSNMP_OCTET_STRING_TYPE InSetPortList;
    UINT4               u4ErrorCode = 0;
    UINT4               u4PortCount = 0;
    UINT4               u4Count = 0;
    UINT4               u4PortNum = 0x0;
    INT4                i4GroupId, i4GrpFilterType, i4GrpFilterId, i4GrpType =
        0;
    INT4                i4GrpDistByte = 0;
    INT4                i4UdbPosition = 0;
    UINT1               u1InPortList[ISS_PORTLIST_LEN];
    UINT1               u1Add = 0, u1Modify = 0, u1RowStatus;

    ISS_LOCK ();

    nmhGetIssRedirectInterfaceGrpIdNextFree ((UINT4 *) &i4GroupId);
    if (i4GroupId == 0)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Check the Group Id Value");
        return;

    }

    HttpGetValuebyName (ISS_ACTION, pHttp->au1Value, pHttp->au1PostQuery);

    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {
        STRCPY (pHttp->au1Name, "GroupId");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4GroupId = ATOI (pHttp->au1Value);
        if (nmhTestv2IssRedirectInterfaceGrpStatus
            (&u4ErrorCode, i4GroupId, ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "TEST :Redirect Group Not Available.");
            return;
        }

        if (nmhSetIssRedirectInterfaceGrpStatus (i4GroupId, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "SET : Redirect Group Not Available.");
            return;
        }
        ISS_UNLOCK ();
        IssProcessRedirectPageGet (pHttp);
        return;
    }
    if (STRCMP (pHttp->au1Value, "Add") == 0)
    {
        u1Add = 1;
        u1RowStatus = ISS_CREATE_AND_WAIT;

        if (nmhSetIssRedirectInterfaceGrpStatus (i4GroupId, u1RowStatus) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "SET :Add Redirect Group Not Available.");
            return;
        }
    }

    if (STRCMP (pHttp->au1Value, "Modify") == 0)
    {
        u1Modify = 1;
        STRCPY (pHttp->au1Name, "GroupId");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4GroupId = ATOI (pHttp->au1Value);
        u1RowStatus = ISS_NOT_IN_SERVICE;

        if (nmhSetIssRedirectInterfaceGrpStatus (i4GroupId, u1RowStatus) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "SET :MODIFY Redirect Group Not Available.");
            return;
        }
    }

    STRCPY (pHttp->au1Name, "filtertype");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4GrpFilterType = ATOI (pHttp->au1Value);

    if (nmhTestv2IssRedirectInterfaceGrpFilterType (&u4ErrorCode, i4GroupId,
                                                    i4GrpFilterType) ==
        SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Test: Invalid Filter Type");
        return;
    }
    if (nmhSetIssRedirectInterfaceGrpFilterType (i4GroupId, i4GrpFilterType) ==
        SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Set: Invalid Filter Type");
        return;
    }

    STRCPY (pHttp->au1Name, "interfilterid");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4GrpFilterId = ATOI (pHttp->au1Value);

    if (nmhTestv2IssRedirectInterfaceGrpFilterId (&u4ErrorCode, i4GroupId,
                                                  (UINT4) i4GrpFilterId) ==
        SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        if (u1Modify != 1)
        {
            nmhSetIssRedirectInterfaceGrpStatus (i4GroupId, ISS_DESTROY);
        }
        IssSendError (pHttp, (INT1 *) "Test: Invalid Group Filter Id");
        return;
    }
    if (nmhSetIssRedirectInterfaceGrpFilterId (i4GroupId, (UINT4) i4GrpFilterId)
        == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Set: Invalid Group Filter Id");
        return;
    }

    STRCPY (pHttp->au1Name, "portlist");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (STRLEN (pHttp->au1Value) != 0)
    {
        MEMSET (u1InPortList, 0, ISS_PORTLIST_LEN);
        issDecodeSpecialChar (pHttp->au1Value);
        if (STRCMP (pHttp->au1Value, "0") != 0)
        {
            if (IssPortListStringParser (pHttp->au1Value, u1InPortList) ==
                ENM_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp, (INT1 *) "Check the port list");
                return;
            }
        }

        MEMSET (&InLocalPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (&InSetPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        InLocalPortList.pu1_OctetList = MEM_CALLOC (ISS_PORTLIST_LEN, 1, UINT1);
        if (InLocalPortList.pu1_OctetList == NULL)
        {
            ISS_UNLOCK ();
            return;
        }
        MEMCPY (InLocalPortList.pu1_OctetList, u1InPortList,
                sizeof (u1InPortList));
        InLocalPortList.i4_Length = sizeof (u1InPortList);
        while (u4Count < ISS_REDIRECT_MAX_PORTS)
        {
            if (IssIsMemberPort (InLocalPortList.pu1_OctetList,
                                 ISS_PORTLIST_LEN, u4Count) == ISS_SUCCESS)
            {
                u4PortCount++;
                if (u4PortCount == 1)
                {
                    u4PortNum = u4Count;
                }
            }
            u4Count++;
        }
        if (u4PortCount > 1)
        {
            i4GrpType = ISS_REDIRECT_TO_PORTLIST;
        }
        else if (u4PortCount == 1)
        {
            i4GrpType = ISS_REDIRECT_TO_PORT;
        }
        if (i4GrpType == ISS_REDIRECT_TO_PORT)
        {
            InSetPortList.pu1_OctetList = (UINT1 *) &u4PortNum;
            InSetPortList.i4_Length = sizeof (UINT4);

        }
        else if (i4GrpType == ISS_REDIRECT_TO_PORTLIST)
        {
            InSetPortList.pu1_OctetList =
                MEM_CALLOC (ISS_PORTLIST_LEN, 1, UINT1);
            if (InSetPortList.pu1_OctetList == NULL)
            {
                ISS_UNLOCK ();
                return;
            }
            MEMCPY (InSetPortList.pu1_OctetList, u1InPortList,
                    sizeof (u1InPortList));
            InSetPortList.i4_Length = sizeof (u1InPortList);
        }
    }
    if (i4GrpType == ISS_REDIRECT_TO_PORTLIST)
    {
        STRCPY (pHttp->au1Name, "disbyte");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4GrpDistByte = ATOI (pHttp->au1Value);
        if (i4GrpDistByte == 0)
        {
            ISS_UNLOCK ();
            if (u1Modify != 1)
            {
                nmhSetIssRedirectInterfaceGrpStatus (i4GroupId, ISS_DESTROY);
            }
            MEM_FREE (InLocalPortList.pu1_OctetList);
            MEM_FREE (InSetPortList.pu1_OctetList);
            IssSendError (pHttp,
                          (INT1 *)
                          "Distribution byte cannot be None for Redirect to Multiple ports");
            return;
        }
    }
    if (i4GrpType == ISS_REDIRECT_TO_PORT)
    {
        STRCPY (pHttp->au1Name, "disbyte");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4GrpDistByte = ATOI (pHttp->au1Value);
        if (i4GrpDistByte != 0)
        {
            ISS_UNLOCK ();
            if (u1Modify != 1)
            {
                nmhSetIssRedirectInterfaceGrpStatus (i4GroupId, ISS_DESTROY);
            }
            MEM_FREE (InLocalPortList.pu1_OctetList);
            IssSendError (pHttp,
                          (INT1 *)
                          "Distribution byte should be None for Redirect to single ports");
            return;
        }
    }

    if (nmhSetIssRedirectInterfaceGrpPortList (i4GroupId, &InSetPortList) ==
        SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InLocalPortList.pu1_OctetList);
        if (i4GrpType == ISS_REDIRECT_TO_PORTLIST)
        {
            MEM_FREE (InSetPortList.pu1_OctetList);
        }
        IssSendError (pHttp, (INT1 *) "Set: Invalid Group Port List");
        return;
    }
    if (nmhTestv2IssRedirectInterfaceGrpType (&u4ErrorCode, i4GroupId,
                                              i4GrpType) == SNMP_FAILURE)
    {
        MEM_FREE (InLocalPortList.pu1_OctetList);
        if (i4GrpType == ISS_REDIRECT_TO_PORTLIST)
        {
            MEM_FREE (InSetPortList.pu1_OctetList);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Test: Invalid Group Type ");
        return;
    }
    if (nmhSetIssRedirectInterfaceGrpType (i4GroupId, i4GrpType) ==
        SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InLocalPortList.pu1_OctetList);
        if (i4GrpType == ISS_REDIRECT_TO_PORTLIST)
        {
            MEM_FREE (InSetPortList.pu1_OctetList);
        }
        IssSendError (pHttp, (INT1 *) "Set: Invalid Group Type");
        return;
    }
    STRCPY (pHttp->au1Name, "disbyte");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4GrpDistByte = ATOI (pHttp->au1Value);
    if (nmhTestv2IssRedirectInterfaceGrpDistByte (&u4ErrorCode, i4GroupId,
                                                  i4GrpDistByte) ==
        SNMP_FAILURE)
    {
        if (u1Modify != 1)
        {
            nmhSetIssRedirectInterfaceGrpStatus (i4GroupId, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        MEM_FREE (InLocalPortList.pu1_OctetList);
        if (i4GrpType == ISS_REDIRECT_TO_PORTLIST)
        {
            MEM_FREE (InSetPortList.pu1_OctetList);
        }
        IssSendError (pHttp, (INT1 *) "Test: Invalid Group Distribution Byte");
        return;
    }
    if (nmhSetIssRedirectInterfaceGrpDistByte (i4GroupId, i4GrpDistByte) ==
        SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InLocalPortList.pu1_OctetList);
        if (i4GrpType == ISS_REDIRECT_TO_PORTLIST)
        {
            MEM_FREE (InSetPortList.pu1_OctetList);
        }
        IssSendError (pHttp, (INT1 *) "Set: Invalid Group Distribution Byte");
        return;
    }

    STRCPY (pHttp->au1Name, "udbposition");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4UdbPosition = ATOI (pHttp->au1Value);

    if (nmhTestv2IssRedirectInterfaceGrpUdbPosition (&u4ErrorCode, i4GroupId,
                                                     (UINT4) i4UdbPosition) ==
        SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InLocalPortList.pu1_OctetList);
        if (i4GrpType == ISS_REDIRECT_TO_PORTLIST)
        {
            MEM_FREE (InSetPortList.pu1_OctetList);
        }
        IssSendError (pHttp, (INT1 *) "Test: Invalid Udb Position");
        return;
    }
    if (nmhSetIssRedirectInterfaceGrpUdbPosition
        (i4GroupId, (UINT4) i4UdbPosition) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InLocalPortList.pu1_OctetList);
        if (i4GrpType == ISS_REDIRECT_TO_PORTLIST)
        {
            MEM_FREE (InSetPortList.pu1_OctetList);
        }
        IssSendError (pHttp, (INT1 *) "Set: Invalid Udb Position");
        return;
    }
    if ((u1Add == 1) || (u1Modify == 1))
    {
        if (nmhSetIssRedirectInterfaceGrpStatus (i4GroupId, ISS_ACTIVE) ==
            SNMP_FAILURE)
        {
            MEM_FREE (InLocalPortList.pu1_OctetList);
            if (i4GrpType == ISS_REDIRECT_TO_PORTLIST)
            {
                MEM_FREE (InSetPortList.pu1_OctetList);
            }
            if (u1Modify != 1)
            {
                nmhSetIssRedirectInterfaceGrpStatus (i4GroupId, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set Interface Group Status");
            return;
        }
    }
    ISS_UNLOCK ();
    if (i4GrpType == ISS_REDIRECT_TO_PORTLIST)
    {
        MEM_FREE (InSetPortList.pu1_OctetList);
    }
    MEM_FREE (InLocalPortList.pu1_OctetList);
    IssProcessRedirectPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssDxProcessMACFilterConfPage 
*  Description   : This function processes the request coming for the MAC 
*                  Filter Configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssDxProcessMACFilterConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssDxProcessMACFilterConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssDxProcessMACFilterConfPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssDxProcessIPFilterConfPageSet
*  Description   : This function processes the set request for the IP
*                 filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssDxProcessIPFilterConfPageSet (tHttp * pHttp)
{
    INT4                i4RediretGrpFlag = 0x0;
    UINT4               u4MapFilterId = 0x0;
    INT4                i4GroupId = 0x0;
    INT4                i4OutCome = 0x0;
    INT4                i4CurrentAction = 0;
    INT4                i4CurrentGroupId = 0;
    INT4                i4SubAction = 0;
    INT4                i4SubActionId = 0;
    INT4                i4MapFilterType = 0;

    tSNMP_OCTET_STRING_TYPE SrcIpAddrOctet;
    tSNMP_OCTET_STRING_TYPE DstIpAddrOctet;
    tSNMP_OCTET_STRING_TYPE SrcIpv6;
    tSNMP_OCTET_STRING_TYPE DstIpv6;
    tSNMP_OCTET_STRING_TYPE InPortList;
    tSNMP_OCTET_STRING_TYPE OutPortList;
    tPortList           InTempPortList;
    tPortList           OutTempPortList;
    tIp6Addr            SrcIp6Addr;
    tIp6Addr            DstIp6Addr;
    UINT1              *pu1String;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4SrcAddrMask = 0;
    UINT4               u4DestIpAddr = 0;
    UINT4               u4DestAddrMask = 0;
    UINT4               u4SrcPortFrom = 0;
    UINT4               u4SrcPortTo = 0;
    UINT4               u4DstPortFrom = 0;
    UINT4               u4DstPortTo = 0;
    UINT4               u4Priority = 0;
    UINT4               u4Code = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4Protocol = 0;
    UINT4               u4Type = 0;
    UINT4               u4DstPrefix = 0;
    UINT4               u4SrcPrefix = 0;
    UINT4               u4FlowId = 0;
    UINT4               au4SrcTemp[4];
    UINT4               au4DstTemp[4];
    INT4                i4AckBit = ISS_ACK_ANY;
    INT4                i4RstBit = 0;
    INT4                i4TOS = 0;
    INT4                i4Action = 0;
    INT4                i4FilterNo = 0;
    INT4                i4Dscp = 0;
    INT4                i4AddressType = 0;
#ifdef QOSX_WANTED
    INT4                i4Storage = 0;
#endif
    UINT1               au1String[ISS_MAX_LEN];
    UINT1               u1InPortList[ISS_PORTLIST_LEN];
    UINT1               u1OutPortList[ISS_PORTLIST_LEN];
    UINT1               u1Apply = 0;
    UINT1               u1RowStatus = ISS_CREATE_AND_WAIT;

    pu1String = (UINT1 *) &au1String[0];
    SrcIpAddrOctet.pu1_OctetList = (UINT1 *) au4SrcTemp;
    DstIpAddrOctet.pu1_OctetList = (UINT1 *) au4DstTemp;

    MEMSET (&SrcIp6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&DstIp6Addr, 0, sizeof (tIp6Addr));

    HttpGetValuebyName ((UINT1 *) "FltNo", pHttp->au1Value,
                        pHttp->au1PostQuery);
    i4FilterNo = ATOI (pHttp->au1Value);

    MEMSET (InTempPortList, 0, VLAN_PORT_LIST_SIZE);
    MEMSET (OutTempPortList, 0, VLAN_PORT_LIST_SIZE);
    MEMSET (u1InPortList, 0, VLAN_PORT_LIST_SIZE);
    MEMSET (u1OutPortList, 0, VLAN_PORT_LIST_SIZE);
    MEMSET (&InPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&OutPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    /* Check whether the request is for ADD/DELETE */

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    ISS_LOCK ();

    /* check for delete */
    if (STRCMP (pHttp->au1Value, "DELETE") == 0)
    {
        if (nmhGetIssExtL3FilterAction (i4FilterNo, &i4CurrentAction) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            return;
        }
        if (i4CurrentAction == ISS_REDIRECT_TO)
        {
            i4OutCome =
                (INT4) (nmhGetFirstIndexIssRedirectInterfaceGrpTable
                        ((UINT4 *) &i4GroupId));
            if ((i4OutCome == SNMP_FAILURE))
            {
                ISS_UNLOCK ();
                IssSendError (pHttp,
                              (INT1 *)
                              "Delete : Redirect Group Not Available.");
                return;
            }
            do
            {
                if (nmhGetIssRedirectInterfaceGrpFilterType (i4GroupId,
                                                             &i4MapFilterType)
                    == SNMP_FAILURE)
                {
                    ISS_UNLOCK ();
                    return;
                }
                if (nmhGetIssRedirectInterfaceGrpFilterId
                    (i4GroupId, &u4MapFilterId) == SNMP_FAILURE)
                {
                    ISS_UNLOCK ();
                    return;
                }

                if ((i4MapFilterType == ISS_L3_REDIRECT) &&
                    (u4MapFilterId == (UINT4) i4FilterNo))
                {

                    i4RediretGrpFlag = i4GroupId;
                    break;
                }
                i4CurrentGroupId = i4GroupId;
            }
            while (nmhGetNextIndexIssRedirectInterfaceGrpTable
                   ((UINT4) i4CurrentGroupId,
                    (UINT4 *) &i4GroupId) == SNMP_SUCCESS);

            if (i4RediretGrpFlag != 0)
            {
                if (nmhSetIssRedirectInterfaceGrpStatus
                    (i4RediretGrpFlag, ISS_DESTROY) == SNMP_FAILURE)
                {
                    ISS_UNLOCK ();
                    IssSendError (pHttp,
                                  (INT1 *)
                                  "SET : Redirect Group Not Available.");
                    return;
                }
            }
        }

        if (nmhTestv2IssExtL3FilterStatus
            (&u4ErrorCode, i4FilterNo, ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "IP filter not available.");
            return;
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "IP filter not available.");
            return;
        }

        ISS_UNLOCK ();

        IssDxProcessIPFilterConfPageGet (pHttp);
        return;
    }

    if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {
        u1Apply = 1;
        u1RowStatus = ISS_NOT_IN_SERVICE;
    }

    HttpGetValuebyName ((UINT1 *) "FltOption", pHttp->au1Value,
                        pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    HttpGetValuebyName ((UINT1 *) "AddressType", pHttp->au1Value,
                        pHttp->au1PostQuery);
    i4AddressType = ATOI (pHttp->au1Value);

    HttpGetValuebyName ((UINT1 *) "SrcIp", pHttp->au1Value,
                        pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        u4SrcIpAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    }

    HttpGetValuebyName ((UINT1 *) "SrcMask", pHttp->au1Value,
                        pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        u4SrcAddrMask = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    }

    HttpGetValuebyName ((UINT1 *) "DestIp", pHttp->au1Value,
                        pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        u4DestIpAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    }

    HttpGetValuebyName ((UINT1 *) "DestMask", pHttp->au1Value,
                        pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        u4DestAddrMask = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    }

    HttpGetValuebyName ((UINT1 *) "InPrtList", pHttp->au1Value,
                        pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        MEMSET (u1InPortList, 0, ISS_PORTLIST_LEN);
        issDecodeSpecialChar (pHttp->au1Value);
        if (STRCMP (pHttp->au1Value, "0") != 0)
        {
            if (IssPortListStringParser (pHttp->au1Value, u1InPortList) ==
                ENM_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp, (INT1 *) "Check the allowed in port list");
                return;
            }
        }
        InPortList.i4_Length = ISS_PORTLIST_LEN;
        InPortList.pu1_OctetList = MEM_CALLOC (InPortList.i4_Length, 1, UINT1);
        if (InPortList.pu1_OctetList == NULL)
        {
            ISS_UNLOCK ();
            return;
        }
        MEMCPY (InPortList.pu1_OctetList, u1InPortList, InPortList.i4_Length);
    }

    HttpGetValuebyName ((UINT1 *) "OutPrtList", pHttp->au1Value,
                        pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        MEMSET (u1OutPortList, 0, ISS_PORTLIST_LEN);
        issDecodeSpecialChar (pHttp->au1Value);

        if (STRCMP (pHttp->au1Value, "0") != 0)
        {
            if (IssPortListStringParser (pHttp->au1Value, u1OutPortList) ==
                ENM_FAILURE)
            {
                ISS_UNLOCK ();
                MEM_FREE (InPortList.pu1_OctetList);
                IssSendError (pHttp,
                              (INT1 *) "Check the allowed out port list");
                return;
            }
        }
        OutPortList.i4_Length = ISS_PORTLIST_LEN;
        OutPortList.pu1_OctetList =
            MEM_CALLOC (OutPortList.i4_Length, 1, UINT1);
        if (OutPortList.pu1_OctetList == NULL)
        {
            ISS_UNLOCK ();
            if (InPortList.pu1_OctetList != NULL)
            {
                MEM_FREE (InPortList.pu1_OctetList);

            }
            return;
        }
        MEMCPY (OutPortList.pu1_OctetList, u1OutPortList,
                OutPortList.i4_Length);
    }

    /* Determine the type of ACL from the filter number and process the SET
     * request
     */

    HttpGetValuebyName ((UINT1 *) "Protocol", pHttp->au1Value,
                        pHttp->au1PostQuery);
    u4Protocol = ATOI (pHttp->au1Value);
    if (u4Protocol == ISS_PROT_OTHER)
    {
        HttpGetValuebyName ((UINT1 *) "Other", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Protocol = ATOI (pHttp->au1Value);
        /*This is done since if we try to change the protocol from some other
         * field like IP/ICMP/TCP to Other and if  we don't give any value
         * the protocol change to OTHER is not getting reflected . So to the
         * hardware the protcol value will go as 255, but actual protocol value
         * will be set only if it is not 255, so no issue*/
        if (u4Protocol == 0)
        {
            u4Protocol = ISS_PROT_OTHER;
        }
    }
    /* For Processing ICMP options. */
    if (u4Protocol == 1)
    {
        HttpGetValuebyName ((UINT1 *) "MsgCode", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Code = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "MsgType", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Type = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "Priority", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Priority = ATOI (pHttp->au1Value);
    }

    else
    {

        HttpGetValuebyName ((UINT1 *) "Priority", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Priority = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "Dscp", pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4Dscp = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "Tos", pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4TOS = ATOI (pHttp->au1Value);

        /* Configure either Dscp or TOS */
        if ((i4TOS > 0) && (i4Dscp > 0))
        {
            ISS_UNLOCK ();
            if (InPortList.pu1_OctetList != NULL)
            {
                MEM_FREE (InPortList.pu1_OctetList);
            }
            if (OutPortList.pu1_OctetList != NULL)
            {
                MEM_FREE (OutPortList.pu1_OctetList);
            }
            IssSendError (pHttp, (INT1 *)
                          "Unable to configure both Dscp and TOS, configure one at a time");
            return;
        }
    }

    /* For Processing TCP options. */
    if (u4Protocol == 6)
    {
        HttpGetValuebyName ((UINT1 *) "AckBit", pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4AckBit = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "RstBit", pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4RstBit = ATOI (pHttp->au1Value);

    }

    /* For Processing TCP/UDP options. */
    if (u4Protocol == 17 || u4Protocol == 6)
    {
        HttpGetValuebyName ((UINT1 *) "SrcPortMin", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4SrcPortFrom = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "SrcPortMax", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4SrcPortTo = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "DstPortMin", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4DstPortFrom = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "DstPortMax", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4DstPortTo = ATOI (pHttp->au1Value);
    }

    if (nmhTestv2IssExtL3FilterStatus (&u4ErrorCode, i4FilterNo,
                                       u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        IssSendError (pHttp, (INT1 *) "Invalid IP filter number.");
        return;
    }

    if ((u1RowStatus == ISS_CREATE_AND_WAIT)
        || (u1RowStatus == ISS_NOT_IN_SERVICE))
    {
        if (nmhSetIssExtL3FilterStatus (i4FilterNo, u1RowStatus) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            IssSendError (pHttp, (INT1 *) "Setting Invalid IP filter number.");
            return;
        }
    }

    if (i4AddressType != 0)
    {
        if (nmhTestv2IssAclL3FilteAddrType
            (&u4ErrorCode, i4FilterNo, i4AddressType) == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *)
                          "Unable to set In Address Type Test Failed .");
            return;
        }
        if (nmhSetIssAclL3FilteAddrType (i4FilterNo, i4AddressType) ==
            SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set In Address Type  .");
            return;
        }
    }

    if (i4AddressType == 2)
    {
        HttpGetValuebyName ((UINT1 *) "SrcIp", pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);
        INET_ATON6 (pHttp->au1Value, &SrcIp6Addr);
        SrcIpv6.pu1_OctetList = MEM_MALLOC (sizeof (tIp6Addr), UINT1);
        if (NULL == SrcIpv6.pu1_OctetList)
        {
            ISS_UNLOCK ();
            if (InPortList.pu1_OctetList != NULL)
            {
                MEM_FREE (InPortList.pu1_OctetList);
            }
            if (OutPortList.pu1_OctetList != NULL)
            {
                MEM_FREE (OutPortList.pu1_OctetList);
            }
            return;
        }
        MEMCPY (SrcIpv6.pu1_OctetList, &SrcIp6Addr, IP6_ADDR_SIZE);
        SrcIpv6.i4_Length = IP6_ADDR_SIZE;

        if ((nmhTestv2IssAclL3FilterSrcIpAddr
             (&u4ErrorCode, i4FilterNo, &SrcIpv6)) == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *)
                          "Unable to set the filter with this source IPV6 Address.");
            return;
        }

        if ((nmhSetIssAclL3FilterSrcIpAddr
             (i4FilterNo, &SrcIpv6) == SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set the l3 Source IPV6 Address.");
            return;
        }

        HttpGetValuebyName ((UINT1 *) "DestIp", pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);
        INET_ATON6 (pHttp->au1Value, &DstIp6Addr);
        DstIpv6.pu1_OctetList = MEM_MALLOC (sizeof (tIp6Addr), UINT1);
        if (NULL == DstIpv6.pu1_OctetList)
        {
            ISS_UNLOCK ();
            if (InPortList.pu1_OctetList != NULL)
            {
                MEM_FREE (InPortList.pu1_OctetList);
            }
            if (OutPortList.pu1_OctetList != NULL)
            {
                MEM_FREE (OutPortList.pu1_OctetList);
            }
            return;
        }
        MEMCPY (DstIpv6.pu1_OctetList, &DstIp6Addr, IP6_ADDR_SIZE);
        DstIpv6.i4_Length = IP6_ADDR_SIZE;

        if ((nmhTestv2IssAclL3FilterDstIpAddr
             (&u4ErrorCode, i4FilterNo, &DstIpv6) == SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *)
                          "Unable to set the filter with this Destination IPV6 Address.");
            return;
        }

        if ((nmhSetIssAclL3FilterDstIpAddr
             (i4FilterNo, &DstIpv6) == SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set the l3 Source Address Mask.");
            return;
        }

    }
    if ((i4Action != 0) && (nmhTestv2IssExtL3FilterAction
                            (&u4ErrorCode, i4FilterNo,
                             i4Action)) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the l3 filter action.");
        return;
    }

    if (i4AddressType == 1)
    {
        if ((u4SrcIpAddr > 0) && (nmhTestv2IssExtL3FilterSrcIpAddr
                                  (&u4ErrorCode, i4FilterNo, u4SrcIpAddr))
            == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *)
                          "Unable to set the filter with this source IP Address.");
            return;
        }

        if ((u4SrcAddrMask > 0) && (nmhTestv2IssExtL3FilterSrcIpAddrMask
                                    (&u4ErrorCode, i4FilterNo, u4SrcAddrMask)
                                    == SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *)
                          "Unable to set the filter with this source Address Mask.");
            return;
        }

        if ((u4DestIpAddr > 0) && (nmhTestv2IssExtL3FilterDstIpAddr
                                   (&u4ErrorCode, i4FilterNo, u4DestIpAddr)
                                   == SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *)
                          "Unable to set the filter with this Destination IP Address.");
            return;
        }

        if ((u4DestAddrMask > 0) && (nmhTestv2IssExtL3FilterDstIpAddrMask
                                     (&u4ErrorCode, i4FilterNo, u4DestAddrMask)
                                     == SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *)
                          "Unable to set the filter with this Destination Address Mask.");
            return;
        }
    }

    if ((InPortList.pu1_OctetList != NULL) && (nmhTestv2IssExtL3FilterInPortList
                                               (&u4ErrorCode, i4FilterNo,
                                                &InPortList) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set this in port list.");
        return;
    }

    if ((OutPortList.pu1_OctetList != NULL)
        &&
        (nmhTestv2IssExtL3FilterOutPortList
         (&u4ErrorCode, i4FilterNo, &OutPortList) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set this Out port list.");
        return;
    }

    if ((u4Protocol != 0) && (nmhTestv2IssExtL3FilterProtocol
                              (&u4ErrorCode, i4FilterNo, u4Protocol))
        == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the l3 filter protocol.");
        return;
    }
    if (u4Protocol == 1)
    {
        if (nmhTestv2IssExtL3FilterMessageType
            (&u4ErrorCode, i4FilterNo, u4Type) == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set ICMP MessageType.");
            return;
        }

        if (nmhTestv2IssExtL3FilterMessageCode
            (&u4ErrorCode, i4FilterNo, u4Code) == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set ICMP MessageCode.");
            return;
        }
    }
    if ((u4Priority != 0) && (nmhTestv2IssExtL3FilterPriority
                              (&u4ErrorCode, i4FilterNo, u4Priority)
                              == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter priority.");
        return;
    }

    if (u4Protocol == 6)
    {
        if (nmhTestv2IssExtL3FilterAckBit (&u4ErrorCode, i4FilterNo, i4AckBit)
            == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Test: Unable to set TCP Ack Bit.");
            return;
        }

        if (nmhTestv2IssExtL3FilterRstBit (&u4ErrorCode, i4FilterNo, i4RstBit)
            == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set TCP Rst Bit.");
            return;
        }
    }
    if (u4Protocol == 17 || u4Protocol == 6)
    {
        if ((u4SrcPortFrom != 0) || (u4SrcPortTo != 0))
        {
            if (nmhTestv2IssExtL3FilterMinSrcProtPort (&u4ErrorCode, i4FilterNo,
                                                       u4SrcPortFrom) ==
                SNMP_FAILURE)
            {
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                ISS_UNLOCK ();
                IssSendError (pHttp,
                              (INT1 *) "Unable to set TCP/UDP Src Port Start.");
                return;
            }

            if (nmhTestv2IssExtL3FilterMaxSrcProtPort (&u4ErrorCode, i4FilterNo,
                                                       u4SrcPortTo) ==
                SNMP_FAILURE)
            {
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                ISS_UNLOCK ();
                IssSendError (pHttp,
                              (INT1 *) "Unable to set TCP/UDP Src Port End.");
                return;
            }
        }

        if ((u4DstPortFrom != 0) || (u4DstPortTo != 0))
        {
            if (nmhTestv2IssExtL3FilterMinDstProtPort (&u4ErrorCode, i4FilterNo,
                                                       u4DstPortFrom) ==
                SNMP_FAILURE)
            {
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                ISS_UNLOCK ();
                IssSendError (pHttp,
                              (INT1 *) "Unable to set TCP/UDP Dst Port Start.");
                return;
            }

            if (nmhTestv2IssExtL3FilterMaxDstProtPort (&u4ErrorCode, i4FilterNo,
                                                       u4DstPortTo) ==
                SNMP_FAILURE)
            {
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                ISS_UNLOCK ();
                IssSendError (pHttp,
                              (INT1 *) "Unable to set TCP/UDP Dst Port End.");
                return;
            }
        }
    }

    if ((i4Action != 0) && (nmhSetIssExtL3FilterAction (i4FilterNo, i4Action)
                            == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the l3 filter action.");
        return;
    }

    if (i4AddressType == 1)
    {
        if ((u4SrcIpAddr > 0) && (nmhSetIssExtL3FilterSrcIpAddr
                                  (i4FilterNo, u4SrcIpAddr) == SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set the l3 Source Ip Address.");
            return;
        }

        if ((u4SrcAddrMask > 0) && (nmhSetIssExtL3FilterSrcIpAddrMask
                                    (i4FilterNo,
                                     u4SrcAddrMask) == SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set the l3 Source Address Mask.");
            return;
        }

        if ((u4DestIpAddr > 0) && (nmhSetIssExtL3FilterDstIpAddr
                                   (i4FilterNo, u4DestIpAddr) == SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *)
                          "Unable to set the l3 Destination Ip Address.");
            return;
        }

        if ((u4DestAddrMask > 0) && (nmhSetIssExtL3FilterDstIpAddrMask
                                     (i4FilterNo,
                                      u4DestAddrMask) == SNMP_FAILURE))
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *)
                          "Unable to set the l3 Destination Address Mask.");
            return;
        }
    }
    if ((InPortList.pu1_OctetList != NULL) && (nmhSetIssExtL3FilterInPortList
                                               (i4FilterNo, &InPortList)
                                               == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set In Port List.");
        return;
    }

    if ((OutPortList.pu1_OctetList != NULL) && (nmhSetIssExtL3FilterOutPortList
                                                (i4FilterNo, &OutPortList)
                                                == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set Out Port List.");
        return;
    }

    if ((u4Protocol != 0) && (nmhSetIssExtL3FilterProtocol
                              (i4FilterNo, u4Protocol) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter protocol.");
        return;
    }
    if (u4Protocol == 1)
    {
        if (nmhSetIssExtL3FilterMessageType (i4FilterNo, u4Type) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            IssSendError (pHttp, (INT1 *) "Unable to set ICMP MessageType.");
            return;
        }

        if (nmhSetIssExtL3FilterMessageCode (i4FilterNo, u4Code) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            IssSendError (pHttp, (INT1 *) "Unable to set ICMP MessageCode.");
            return;
        }
    }
    else
    {
        if (i4Dscp != ISS_DSCP_INVALID)
        {
            if (nmhSetIssExtL3FilterDscp (i4FilterNo, i4Dscp) == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                IssSendError (pHttp, (INT1 *) "Unable to set Filter Dscp.");
                return;
            }
        }

        if (i4TOS != ISS_TOS_INVALID)
        {
            if (nmhSetIssExtL3FilterTos (i4FilterNo, i4TOS) == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                IssSendError (pHttp, (INT1 *) "Unable to set TCP TOS.");
                return;
            }
        }
    }

    if ((u4Priority != 0) && (nmhSetIssExtL3FilterPriority
                              (i4FilterNo, u4Priority) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter priority.");
        return;
    }

    if (u4Protocol == 6)
    {
        if (nmhSetIssExtL3FilterAckBit (i4FilterNo, i4AckBit) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            IssSendError (pHttp, (INT1 *) "Unable to set TCP Ack Bit.");
            return;
        }
        if (nmhSetIssExtL3FilterRstBit (i4FilterNo, i4RstBit) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            IssSendError (pHttp, (INT1 *) "Unable to set TCP Rst Bit.");
            return;
        }
    }
    if (u4Protocol == 17 || u4Protocol == 6)
    {
        if ((u4SrcPortFrom != 0) || (u4SrcPortTo != 0))
        {
            if (nmhSetIssExtL3FilterMinSrcProtPort (i4FilterNo, u4SrcPortFrom)
                == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                IssSendError (pHttp,
                              (INT1 *) "Unable to set TCP/UDP Src Port Start.");
                return;
            }

            if (nmhSetIssExtL3FilterMaxSrcProtPort (i4FilterNo, u4SrcPortTo)
                == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                IssSendError (pHttp,
                              (INT1 *) "Unable to set TCP/UDP Src Port End.");
                return;
            }
        }

        if ((u4DstPortFrom != 0) || (u4DstPortTo != 0))
        {
            if (nmhSetIssExtL3FilterMinDstProtPort (i4FilterNo, u4DstPortFrom)
                == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                IssSendError (pHttp,
                              (INT1 *) "Unable to set TCP/UDP Dst Port Start.");
                return;
            }

            if (nmhSetIssExtL3FilterMaxDstProtPort (i4FilterNo, u4DstPortTo)
                == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                IssSendError (pHttp,
                              (INT1 *) "Unable to set TCP/UDP Dst Port End.");
                return;
            }
        }
    }

    HttpGetValuebyName ((UINT1 *) "DstPrefix", pHttp->au1Value,
                        pHttp->au1PostQuery);
    u4DstPrefix = ATOI (pHttp->au1Value);
    if (nmhTestv2IssAclL3FilterDstIpAddrPrefixLength
        (&u4ErrorCode, i4FilterNo, u4DstPrefix) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (INT1 *)
                      "Unable to set In Dst Prefix Length Test Failed.");
        return;
    }

    if ((u4DstPrefix != 0)
        &&
        (nmhSetIssAclL3FilterDstIpAddrPrefixLength (i4FilterNo, u4DstPrefix)
         == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set In Dst Prefix Length .");
        return;
    }

    HttpGetValuebyName ((UINT1 *) "SrcPrefix", pHttp->au1Value,
                        pHttp->au1PostQuery);
    u4SrcPrefix = ATOI (pHttp->au1Value);
    if (nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength
        (&u4ErrorCode, i4FilterNo, u4SrcPrefix) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (INT1 *)
                      "Unable to set In Src Prefix Length Test Failed .");
        return;
    }
    if ((u4SrcPrefix != 0) && (nmhSetIssAclL3FilterSrcIpAddrPrefixLength
                               (i4FilterNo, u4SrcPrefix) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set In Src Prefix Length.");
        return;
    }

    if (i4AddressType == 2)
    {
        HttpGetValuebyName ((UINT1 *) "FlowId", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4FlowId = ATOI (pHttp->au1Value);

        if (nmhTestv2IssAclL3FilterFlowId
            (&u4ErrorCode, i4FilterNo, u4FlowId) == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set TCP/UDP Dst Port Start.");
            return;
        }

        if (nmhSetIssAclL3FilterFlowId (i4FilterNo, u4FlowId) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set In Flow Id.");
            return;
        }
    }
#ifdef QOSX_WANTED
    HttpGetValuebyName ((UINT1 *) "Storage", pHttp->au1Value,
                        pHttp->au1PostQuery);
    i4Storage = ATOI (pHttp->au1Value);
    if (nmhTestv2DiffServMultiFieldClfrStorage
        (&u4ErrorCode, i4FilterNo, i4Storage) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set In Storage.");
        return;
    }

    if (nmhSetDiffServMultiFieldClfrStorage ((UINT4) i4FilterNo, i4Storage) ==
        SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set In Storage.");
        return;
    }
#endif

    STRCPY (pHttp->au1Name, "SUB_VALUE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4SubAction = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "SUB_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4SubActionId = ATOI (pHttp->au1Value);

    if (nmhTestv2IssAclL3FilterSubAction
        (&u4ErrorCode, i4FilterNo, i4SubAction) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Test: Unable to set SubAction.");
        return;
    }

    if ((nmhSetIssAclL3FilterSubAction (i4FilterNo, i4SubAction)
         == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Set: Unable to Set SubAction.");
        return;
    }

    if ((i4SubActionId != 0) && (nmhSetIssAclL3FilterSubActionId
                                 (i4FilterNo, i4SubActionId) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Set: Unable to set SubActionId.");
        return;
    }
    if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        IssSendError (pHttp, (INT1 *) "Unable to set Filter Status");
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        return;
    }

    ISS_UNLOCK ();

    MEM_FREE (InPortList.pu1_OctetList);
    MEM_FREE (OutPortList.pu1_OctetList);
    IssDxProcessIPFilterConfPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssDxProcessIPFilterConfPageGet
*  Description   : This function processes the get request for the IP
*                  filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssDxProcessIPFilterConfPageGet (tHttp * pHttp)
{
    tSNMP_OCTET_STRING_TYPE SrcIpAddrOctet;
    tSNMP_OCTET_STRING_TYPE DstIpAddrOctet;
    tSNMP_OCTET_STRING_TYPE SrcIpv6;
    tSNMP_OCTET_STRING_TYPE DstIpv6;
    tSNMP_OCTET_STRING_TYPE *InPortList = NULL;
    tSNMP_OCTET_STRING_TYPE *OutPortList = NULL;
    tSNMP_MULTI_DATA_TYPE *pu1DataString = NULL;
    tPortList           InTempPortList;
    tPortList           OutTempPortList;
    tIp6Addr            SrcIp6Addr;
    tIp6Addr            DstIp6Addr;
    UINT4               u4RetVal;
    UINT4               u4Temp;
    UINT4               au4SrcTemp[4];
    UINT4               au4DstTemp[4];
    UINT4               u4DstPrefix = 0;
    UINT4               u4SrcPrefix = 0;
    UINT4               u4FlowId = 0;
    INT4                i4FilterNo;
    INT4                i4CurrentFilterNo;
    INT4                i4RetVal;
    INT4                i4OutCome;
    INT4                i4Protocol;
    INT4                i4AddressType = 0;
    INT4                i4SubAction, i4SubActionId;
#ifdef QOSX_WANTED
    INT4                i4Storage = 0;
#endif
    UINT1               au1String[ISS_MAX_LEN];
    UINT1              *pu1String;

    pu1String = &au1String[0];

    SrcIpAddrOctet.pu1_OctetList = (UINT1 *) au4SrcTemp;
    DstIpAddrOctet.pu1_OctetList = (UINT1 *) au4DstTemp;

    MEMSET (&SrcIp6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&DstIp6Addr, 0, sizeof (tIp6Addr));
    MEMSET (au1String, 0, ISS_ADDR_LEN);

    MEMSET (InTempPortList, 0, VLAN_PORT_LIST_SIZE);
    MEMSET (OutTempPortList, 0, VLAN_PORT_LIST_SIZE);

    if ((pu1DataString = WebnmAllocMultiData ()) == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }

#ifdef QOSX_WANTED

    STRCPY (pHttp->ai1HtmlName, "wintegra_ip_filterconf.html");

    if (HttpReadhtml (pHttp) == ENM_FAILURE)
    {
        IssSendError (pHttp, (CONST INT1 *) ("Unable to read html \n"));
        return;
    }
#endif

    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = pHttp->i4Write;

    WebnmRegisterLock (pHttp, IssLock, IssUnLock);

    ISS_LOCK ();

    i4OutCome = (INT4) (nmhGetFirstIndexIssExtL3FilterTable (&i4FilterNo));
    if (i4OutCome == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    InPortList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);

    if (InPortList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    OutPortList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);
    if (OutPortList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (InPortList);
        return;
    }

    do
    {
        MEMSET (InTempPortList, 0, VLAN_PORT_LIST_SIZE);
        MEMSET (OutTempPortList, 0, VLAN_PORT_LIST_SIZE);
        pHttp->i4Write = u4Temp;

        /* Display only extended lists */
        if (i4FilterNo >= ACL_EXTENDED_START)
        {
            STRCPY (pHttp->au1KeyString, "FILTER_NUMBER");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4FilterNo);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            STRCPY (pHttp->au1KeyString, "FltOption");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssExtL3FilterAction (i4FilterNo, &i4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "ADDRESS_TYPE");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssAclL3FilteAddrType (i4FilterNo, &i4AddressType);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4AddressType);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            if (i4AddressType == 2)
            {

                STRCPY (pHttp->au1KeyString, "SOURCE_IP_ADDRESS");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssAclL3FilterSrcIpAddr (i4FilterNo, &SrcIpAddrOctet);
                MEMCPY (&SrcIp6Addr, SrcIpAddrOctet.pu1_OctetList,
                        IP6_ADDR_SIZE);
                SrcIpv6.i4_Length = IP6_ADDR_SIZE;
                STRCPY (pu1String,
                        Ip6PrintNtop ((tIp6Addr *) (VOID *) SrcIpAddrOctet.
                                      pu1_OctetList));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "SOURCE_MASK");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "None");
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DESTINATION_IP_ADDRESS");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssAclL3FilterDstIpAddr (i4FilterNo, &DstIpAddrOctet);
                MEMCPY (&DstIp6Addr, DstIpAddrOctet.pu1_OctetList,
                        IP6_ADDR_SIZE);
                DstIpv6.i4_Length = IP6_ADDR_SIZE;
                STRCPY (pu1String,
                        Ip6PrintNtop ((tIp6Addr *) (VOID *) DstIpAddrOctet.
                                      pu1_OctetList));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DESTINATION_MASK");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", "None");
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            }
            if (i4AddressType == 1)
            {
                STRCPY (pHttp->au1KeyString, "SOURCE_IP_ADDRESS");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterSrcIpAddr (i4FilterNo, &u4RetVal);
                WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "SOURCE_MASK");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterSrcIpAddrMask (i4FilterNo, &u4RetVal);
                WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DESTINATION_IP_ADDRESS");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterDstIpAddr (i4FilterNo, &u4RetVal);
                WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DESTINATION_MASK");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterDstIpAddrMask (i4FilterNo, &u4RetVal);
                WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }
            MEMSET (InPortList->pu1_OctetList, 0, ISS_PORTLIST_LEN);
            nmhGetIssAclL3FilterInPortList (i4FilterNo,
                                            pu1DataString->pOctetStrValue);
            IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                    InTempPortList);
            MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                    InTempPortList, pu1DataString->pOctetStrValue->i4_Length);
            WebnmSendToSocket (pHttp, "IN_PORTS_LIST", pu1DataString,
                               ENM_PORT_LIST);

            MEMSET (OutPortList->pu1_OctetList, 0, ISS_PORTLIST_LEN);
            nmhGetIssAclL3FilterOutPortList (i4FilterNo,
                                             pu1DataString->pOctetStrValue);
            IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                    OutTempPortList);
            MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                    OutTempPortList, pu1DataString->pOctetStrValue->i4_Length);
            WebnmSendToSocket (pHttp, "OUT_PORTS_LIST", pu1DataString,
                               ENM_PORT_LIST);

            STRCPY (pHttp->au1KeyString, "PROTOCOL");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssExtL3FilterProtocol (i4FilterNo, &i4Protocol);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Protocol);

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "OTHER");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Protocol);

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            if (i4Protocol == 1)
            {
                STRCPY (pHttp->au1KeyString, "CODE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterMessageCode (i4FilterNo, &i4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "TYPE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterMessageType (i4FilterNo, &i4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "PRIORITY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterPriority (i4FilterNo, &i4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            }
            else
            {
                STRCPY (pHttp->au1KeyString, "CODE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                STRCPY (pHttp->au1DataString, "");
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "TYPE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                STRCPY (pHttp->au1DataString, "");
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "PRIORITY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterPriority (i4FilterNo, &i4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DSCP");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterDscp (i4FilterNo, &i4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "TOS");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterTos (i4FilterNo, &i4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            }

            if (i4Protocol == 6)
            {
                STRCPY (pHttp->au1KeyString, "ACK_BIT");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterAckBit (i4FilterNo, &i4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "RST_BIT");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterRstBit (i4FilterNo, &i4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }

            if (i4Protocol == 6 || i4Protocol == 17)
            {
                STRCPY (pHttp->au1KeyString, "SOURCE_PORT_START");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterMinSrcProtPort (i4FilterNo, &u4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "SOURCE_PORT_END");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterMaxSrcProtPort (i4FilterNo, &u4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DESTINATION_PORT_START");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterMinDstProtPort (i4FilterNo, &u4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DESTINATION_PORT_END");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterMaxDstProtPort (i4FilterNo, &u4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }

            STRCPY (pHttp->au1KeyString, "DST_PREFIX");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssAclL3FilterDstIpAddrPrefixLength (i4FilterNo,
                                                       &u4DstPrefix);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4DstPrefix);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "SRC_PREFIX");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssAclL3FilterSrcIpAddrPrefixLength (i4FilterNo,
                                                       &u4SrcPrefix);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4SrcPrefix);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "FLOW_ID");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssAclL3FilterFlowId (i4FilterNo, &u4FlowId);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4FlowId);

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "MFC_STORAGE");
            WebnmSendString (pHttp, pHttp->au1KeyString);
#ifdef QOSX_WANTED
            nmhGetDiffServMultiFieldClfrStorage (i4FilterNo, &i4Storage);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", i4Storage);
#endif
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "SUB_VALUE_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssAclL3FilterSubAction (i4FilterNo, &i4SubAction);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4SubAction);

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "SUB_ID_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssAclL3FilterSubActionId (i4FilterNo, &i4SubActionId);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4SubActionId);

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        }
        i4CurrentFilterNo = i4FilterNo;
    }
    while (nmhGetNextIndexIssExtL3FilterTable
           (i4CurrentFilterNo, &i4FilterNo) == SNMP_SUCCESS);

    ISS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    free_octetstring (InPortList);
    free_octetstring (OutPortList);
    if (i4FilterNo >= ACL_EXTENDED_START)
    {
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
    }
}

/*********************************************************************
*  Function Name : IssDxProcessIPStdFilterConfPageGet 
*  Description   : This function processes the get request for the IP
*                  standard filter configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssDxProcessIPStdFilterConfPageGet (tHttp * pHttp)
{
    INT4                i4FilterNo, i4CurrentFilterNo, i4RetVal, i4OutCome;
    UINT4               u4RetVal, u4Temp;
    tSNMP_OCTET_STRING_TYPE *InPortList = NULL, *OutPortList = NULL;
    tSNMP_MULTI_DATA_TYPE *pu1DataString = NULL;
    tPortList           InTempPortList;
    tPortList           OutTempPortList;
    UINT1              *pu1String;

    MEMSET (InTempPortList, 0, VLAN_PORT_LIST_SIZE);
    MEMSET (OutTempPortList, 0, VLAN_PORT_LIST_SIZE);

    if ((pu1DataString = WebnmAllocMultiData ()) == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }

    pHttp->i4Write = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = pHttp->i4Write;
    WebnmRegisterLock (pHttp, IssLock, IssUnLock);

    ISS_LOCK ();

    i4OutCome = (INT4) (nmhGetFirstIndexIssExtL3FilterTable (&i4FilterNo));
    if ((i4OutCome == SNMP_FAILURE) || (i4FilterNo >= ACL_EXTENDED_START))
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    InPortList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);

    if (InPortList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    OutPortList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);

    if (OutPortList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (InPortList);
        return;
    }

    do
    {

        /* Display only if the ACL number is in the range 1 to 1000 */
        MEMSET (InTempPortList, 0, VLAN_PORT_LIST_SIZE);
        MEMSET (OutTempPortList, 0, VLAN_PORT_LIST_SIZE);

        if (i4FilterNo < ACL_EXTENDED_START)
        {
            pHttp->i4Write = u4Temp;
            STRCPY (pHttp->au1KeyString, "FILTER_NUMBER");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4FilterNo);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            i4RetVal = 0;
            STRCPY (pHttp->au1KeyString, "SELECT_ACTIONS");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssExtL3FilterAction (i4FilterNo, &i4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "SOURCE_IP_ADDRESS");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssExtL3FilterSrcIpAddr (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            u4RetVal = 0;

            STRCPY (pHttp->au1KeyString, "SOURCE_MASK");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssExtL3FilterSrcIpAddrMask (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            u4RetVal = 0;

            STRCPY (pHttp->au1KeyString, "DESTINATION_IP_ADDRESS");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssExtL3FilterDstIpAddr (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            u4RetVal = 0;

            STRCPY (pHttp->au1KeyString, "DESTINATION_MASK");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssExtL3FilterDstIpAddrMask (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            u4RetVal = 0;
            nmhGetIssExtL3FilterInPortList (i4FilterNo,
                                            pu1DataString->pOctetStrValue);
            IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                    InTempPortList);
            MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                    InTempPortList, pu1DataString->pOctetStrValue->i4_Length);
            WebnmSendToSocket (pHttp, "IN_PORTS_LIST", pu1DataString,
                               ENM_PORT_LIST);

            nmhGetIssExtL3FilterOutPortList (i4FilterNo,
                                             pu1DataString->pOctetStrValue);
            IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                    OutTempPortList);
            MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                    OutTempPortList, pu1DataString->pOctetStrValue->i4_Length);
            WebnmSendToSocket (pHttp, "OUT_PORTS_LIST", pu1DataString,
                               ENM_PORT_LIST);
            i4RetVal = 0;
            STRCPY (pHttp->au1KeyString, "PRIORITY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssExtL3FilterPriority (i4FilterNo, &i4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            i4RetVal = 0;
            STRCPY (pHttp->au1KeyString, "SUB_IP_ACTION_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssAclL3FilterSubAction (i4FilterNo, &i4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            i4RetVal = 0;
            STRCPY (pHttp->au1KeyString, "SUB_IP_ACTION_ID_KEY");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssAclL3FilterSubActionId (i4FilterNo, &i4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        }
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        i4CurrentFilterNo = i4FilterNo;
    }
    while (nmhGetNextIndexIssExtL3FilterTable
           (i4CurrentFilterNo, &i4FilterNo) == SNMP_SUCCESS);

    ISS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    free_octetstring (InPortList);
    free_octetstring (OutPortList);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : IssDxProcessIPStdFilterConfPageSet 
*  Description   : This function processes the set request for the IP
*                 standard filter configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None 
*********************************************************************/

VOID
IssDxProcessIPStdFilterConfPageSet (tHttp * pHttp)
{
    UINT4               u4SrcIpAddr = 0, u4SrcAddrMask = 0, u4DestIpAddr =
        0, u4DestAddrMask = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4Action = 0, i4FilterNo = 0;
    INT4                i4SubAction = 0, i4SubActionId = 0;
    tSNMP_OCTET_STRING_TYPE InPortList, OutPortList;
    UINT1               u1InPortList[ISS_PORTLIST_LEN],
        u1OutPortList[ISS_PORTLIST_LEN];
    UINT1               u1Apply = 0, u1RowStatus = ISS_CREATE_AND_WAIT;
    INT4                i4RediretGrpFlag = 0x0;
    UINT4               u4MapFilterId = 0x0;
    UINT4               u4IpSubnet = 0;
    INT4                i4GroupId = 0x0;
    INT4                i4OutCome = 0x0;
    INT4                i4CurrentAction = 0;
    INT4                i4CurrentGroupId = 0;
    INT4                i4MapFilterType = 0;
    INT4                i4Priority = 0;

    MEMSET (&InPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&OutPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    STRCPY (pHttp->au1Name, "FILTER_NUMBER");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4FilterNo = ATOI (pHttp->au1Value);

    /* Check whether the request is for ADD/DELETE */
    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    ISS_LOCK ();
    /* check for delete */
    if (STRCMP (pHttp->au1Value, "Delete") == 0)
    {

        if (nmhGetIssExtL3FilterAction (i4FilterNo, &i4CurrentAction) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            return;
        }
        if (i4CurrentAction == ISS_REDIRECT_TO)
        {
            i4OutCome =
                (INT4) (nmhGetFirstIndexIssRedirectInterfaceGrpTable
                        ((UINT4 *) &i4GroupId));
            if ((i4OutCome == SNMP_FAILURE))
            {
                ISS_UNLOCK ();
                IssSendError (pHttp,
                              (INT1 *)
                              "Delete : Redirect Group Not Available.");
                return;
            }
            do
            {
                if (nmhGetIssRedirectInterfaceGrpFilterType (i4GroupId,
                                                             &i4MapFilterType)
                    == SNMP_FAILURE)
                {
                    ISS_UNLOCK ();
                    return;
                }
                if (nmhGetIssRedirectInterfaceGrpFilterId
                    (i4GroupId, &u4MapFilterId) == SNMP_FAILURE)
                {
                    ISS_UNLOCK ();
                    return;
                }

                if ((i4MapFilterType == ISS_L3_REDIRECT) &&
                    (u4MapFilterId == (UINT4) i4FilterNo))
                {

                    i4RediretGrpFlag = i4GroupId;
                }
                i4CurrentGroupId = i4GroupId;
            }
            while (nmhGetNextIndexIssRedirectInterfaceGrpTable
                   ((UINT4) i4CurrentGroupId,
                    (UINT4 *) &i4GroupId) == SNMP_SUCCESS);

            if (i4RediretGrpFlag != 0)
            {
                if (nmhSetIssRedirectInterfaceGrpStatus
                    (i4RediretGrpFlag, ISS_DESTROY) == SNMP_FAILURE)
                {
                    ISS_UNLOCK ();
                    IssSendError (pHttp,
                                  (INT1 *)
                                  "SET : Redirect Group Not Available.");
                    return;
                }
            }
        }

        if (nmhTestv2IssExtL3FilterStatus
            (&u4ErrorCode, i4FilterNo, ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "ACL with this number not available.");
            return;
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "ACL with this number not available.");
            return;
        }
        ISS_UNLOCK ();
        IssDxProcessIPStdFilterConfPageGet (pHttp);
        return;
    }

    if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {
        u1Apply = 1;
        u1RowStatus = ISS_NOT_IN_SERVICE;
    }
    STRCPY (pHttp->au1Name, "SELECT_ACTIONS");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "SOURCE_ADDRESS");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        u4SrcIpAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));

    STRCPY (pHttp->au1Name, "SOURCE_MASK");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        u4SrcAddrMask = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));

    STRCPY (pHttp->au1Name, "DESTINATION_ADDRESS");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        u4DestIpAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));

    STRCPY (pHttp->au1Name, "DESTINATION_MASK");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        u4DestAddrMask = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    STRCPY (pHttp->au1Name, "IN_PORTS_LIST");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (STRLEN (pHttp->au1Value) != 0)
    {
        MEMSET (u1InPortList, 0, ISS_PORTLIST_LEN);
        issDecodeSpecialChar (pHttp->au1Value);
        if (STRCMP (pHttp->au1Value, "0") != 0)
        {
            if (IssPortListStringParser (pHttp->au1Value, u1InPortList) ==
                ENM_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp, (INT1 *) "Check the allowed in port list");
                return;
            }
        }

        MEM_FREE (InPortList.pu1_OctetList);

        InPortList.i4_Length = ISS_PORTLIST_LEN;
        InPortList.pu1_OctetList = MEM_CALLOC (InPortList.i4_Length, 1, UINT1);
        if (InPortList.pu1_OctetList == NULL)
        {
            ISS_UNLOCK ();
            return;
        }
        MEMCPY (InPortList.pu1_OctetList, u1InPortList, InPortList.i4_Length);
    }

    STRCPY (pHttp->au1Name, "OUT_PORTS_LIST");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        MEMSET (u1OutPortList, 0, ISS_PORTLIST_LEN);
        issDecodeSpecialChar (pHttp->au1Value);

        if (STRCMP (pHttp->au1Value, "0") != 0)
        {

            if (IssPortListStringParser (pHttp->au1Value, u1OutPortList) ==
                ENM_FAILURE)
            {
                ISS_UNLOCK ();
                if (InPortList.pu1_OctetList != NULL)
                {
                    MEM_FREE (InPortList.pu1_OctetList);
                }
                IssSendError (pHttp, (INT1 *) "Check the allowed in port list");
                return;
            }
        }
        MEM_FREE (OutPortList.pu1_OctetList);
        OutPortList.i4_Length = ISS_PORTLIST_LEN;
        OutPortList.pu1_OctetList =
            MEM_CALLOC (OutPortList.i4_Length, 1, UINT1);
        if (OutPortList.pu1_OctetList == NULL)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            return;
        }
        MEMCPY (OutPortList.pu1_OctetList, u1OutPortList,
                OutPortList.i4_Length);
    }
    STRCPY (pHttp->au1Name, "PRIORITY");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Priority = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "SUB_VALUE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4SubAction = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "SUB_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4SubActionId = ATOI (pHttp->au1Value);

    if (nmhTestv2IssExtL3FilterStatus (&u4ErrorCode, i4FilterNo,
                                       u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        IssSendError (pHttp, (INT1 *) "Invalid ACL number.");
        return;
    }
    if (nmhSetIssExtL3FilterStatus (i4FilterNo, u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        IssSendError (pHttp, (INT1 *) "Entry already exists for this IP"
                      " filter number");
        return;
    }

    if ((i4Action != 0) && (nmhSetIssExtL3FilterAction (i4FilterNo, i4Action)
                            == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        IssSendError (pHttp,
                      (INT1 *)
                      "Unable to set the Standard ACL  filter action.");
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        return;
    }

    u4IpSubnet = (u4SrcIpAddr & u4SrcAddrMask);

    if ((u4IpSubnet > 0) && (nmhSetIssExtL3FilterSrcIpAddr
                             (i4FilterNo, u4IpSubnet) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (INT1 *) "Unable to set the ACL source IP address.");
        return;
    }
    if ((u4SrcAddrMask > 0) && (nmhSetIssExtL3FilterSrcIpAddrMask
                                (i4FilterNo, u4SrcAddrMask) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set ACL source address mask.");
        return;
    }

    if ((u4DestIpAddr > 0) && (nmhSetIssExtL3FilterDstIpAddr
                               (i4FilterNo, u4DestIpAddr) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (INT1 *) "Unable to set ACL destination IP address.");
        return;
    }
    if ((u4DestAddrMask > 0) && (nmhSetIssExtL3FilterDstIpAddrMask
                                 (i4FilterNo, u4DestAddrMask) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (INT1 *)
                      "Unable to set the destination IP address mask.");
        return;
    }

    if ((InPortList.pu1_OctetList != NULL) && (nmhSetIssExtL3FilterInPortList
                                               (i4FilterNo, &InPortList)
                                               == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set In Port List.");
        return;
    }
    if ((OutPortList.pu1_OctetList != NULL) && (nmhSetIssExtL3FilterOutPortList
                                                (i4FilterNo, &OutPortList)
                                                == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set Out Port List.");
        return;
    }

    if (nmhSetIssExtL3FilterPriority (i4FilterNo, i4Priority) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter priority.");
        return;
    }
    if (nmhTestv2IssAclL3FilterSubAction
        (&u4ErrorCode, i4FilterNo, i4SubAction) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Test: Unable to set SubAction.");
        return;
    }

    if ((nmhSetIssAclL3FilterSubAction (i4FilterNo, i4SubAction)
         == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Set: Unable to Set SubAction.");
        return;
    }

    if ((i4SubActionId != 0) && (nmhSetIssAclL3FilterSubActionId
                                 (i4FilterNo, i4SubActionId) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Set: Unable to set SubActionId.");
        return;
    }

    if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set Filter Status");
        return;
    }

    ISS_UNLOCK ();
    MEM_FREE (InPortList.pu1_OctetList);
    MEM_FREE (OutPortList.pu1_OctetList);
    IssDxProcessIPStdFilterConfPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssDxProcessMACFilterConfPageGet
*  Description   : This function processes the get request for the MAC
*                  filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssDxProcessMACFilterConfPageGet (tHttp * pHttp)
{
    INT4                i4CurrentFilterNo, i4FilterNo, i4RetVal, i4OutCome;
    UINT4               u4RetVal, u4Temp;
    UINT1               au1String[20];
    tMacAddr            SrcMacAddr, DestMacAddr;
    tSNMP_OCTET_STRING_TYPE *InPortList = NULL;
    tPortList           InTempPortList;

    tSNMP_MULTI_DATA_TYPE *pu1DataString = NULL;
    MEMSET (InTempPortList, 0, VLAN_PORT_LIST_SIZE);

    if ((pu1DataString = WebnmAllocMultiData ()) == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }

    pHttp->i4Write = 0;

    IssPrintAvailableVlans (pHttp);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = pHttp->i4Write;

    WebnmRegisterLock (pHttp, IssLock, IssUnLock);

    ISS_LOCK ();
    i4OutCome = (INT4) (nmhGetFirstIndexIssExtL2FilterTable (&i4FilterNo));
    if (i4OutCome == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    InPortList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);

    if (InPortList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    do
    {
        MEMSET (InTempPortList, 0, VLAN_PORT_LIST_SIZE);
        pHttp->i4Write = u4Temp;
        STRCPY (pHttp->au1KeyString, "FILTER_NUMBER");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4FilterNo);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "SOURCE_MAC");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssExtL2FilterSrcMacAddr (i4FilterNo, &SrcMacAddr);
        IssPrintMacAddress (SrcMacAddr, au1String);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "DESTINATION_MAC");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssExtL2FilterDstMacAddr (i4FilterNo, &DestMacAddr);
        IssPrintMacAddress (DestMacAddr, au1String);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        i4RetVal = 0;
        STRCPY (pHttp->au1KeyString, "ACTION_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssExtL2FilterAction (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        i4RetVal = 0;
        STRCPY (pHttp->au1KeyString, "PRIORITY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssExtL2FilterPriority (i4FilterNo, &i4RetVal);
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        IssPrintAvailableVlans (pHttp);
        STRCPY (pHttp->au1KeyString, "VLAN_ID");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmRegisterLock (pHttp, IssLock, IssUnLock);
        ISS_LOCK ();
        i4RetVal = 0;
        nmhGetIssExtL2FilterVlanId (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        MEMSET (InTempPortList, 0, VLAN_PORT_LIST_SIZE);
        MEMSET (InPortList->pu1_OctetList, 0, ISS_PORTLIST_LEN);
        nmhGetIssExtL2FilterInPortList (i4FilterNo,
                                        pu1DataString->pOctetStrValue);

        IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                InTempPortList);

        MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList, InTempPortList,
                pu1DataString->pOctetStrValue->i4_Length);
        WebnmSendToSocket (pHttp, "IN_PORT_LIST", pu1DataString, ENM_PORT_LIST);

        i4RetVal = 0;
        STRCPY (pHttp->au1KeyString, "ENCAPTYPE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssExtL2FilterEtherType (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "PROTOCOL");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssExtL2FilterProtocolType (i4FilterNo, &u4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OTHER");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        i4RetVal = 0;
        STRCPY (pHttp->au1KeyString, "SUB_VALUE_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssAclL2FilterSubAction (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        i4RetVal = 0;
        STRCPY (pHttp->au1KeyString, "SUB_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssAclL2FilterSubActionId (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        i4CurrentFilterNo = i4FilterNo;
    }
    while (nmhGetNextIndexIssExtL2FilterTable
           (i4CurrentFilterNo, &i4FilterNo) == SNMP_SUCCESS);

    ISS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    free_octetstring (InPortList);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : IssDxProcessMACFilterConfPageSet
*  Description   : This function processes the set request for the MAC
*                  filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssDxProcessMACFilterConfPageSet (tHttp * pHttp)
{
    INT4                i4Action, i4FilterNo, i4Priority, i4EtherType;
    INT4                i4SubAction, i4SubActionId = 0;
    INT4                i4VlanId;
    UINT4               u4ErrorCode, u4Protocol;
    tMacAddr            SrcMacAddr, DestMacAddr;
    tSNMP_OCTET_STRING_TYPE InPortList;
    UINT1               u1PortList[ISS_PORTLIST_LEN], u1Apply = 0;
    UINT1               u1RowStatus = ISS_CREATE_AND_WAIT;
    UINT1               au1ZeroMac[ISS_MAC_LEN];
    UINT1               au1MemberPorts[VLAN_IFPORT_LIST_SIZE];
    INT4                i4RediretGrpFlag = 0x0;
    UINT4               u4MapFilterId = 0x0;
    INT4                i4GroupId = 0x0;
    INT4                i4OutCome = 0x0;
    INT4                i4CurrentAction = 0;
    INT4                i4CurrentGroupId = 0;
    INT4                i4MapFilterType = 0;

    MEMSET (&InPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1MemberPorts, 0, VLAN_IFPORT_LIST_SIZE);
    MEMSET (au1ZeroMac, 0, ISS_MAC_LEN);
    MEMSET (u1PortList, 0, ISS_PORTLIST_LEN);
    STRCPY (pHttp->au1Name, "FILTER_NUMBER");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4FilterNo = ATOI (pHttp->au1Value);

    /* Check whether the request is for ADD/DELETE */
    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    ISS_LOCK ();
    /* check for delete */
    if (STRCMP (pHttp->au1Value, "DELETE") == 0)
    {
        if (nmhGetIssExtL2FilterAction (i4FilterNo, &i4CurrentAction) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            return;
        }
        if (i4CurrentAction == ISS_REDIRECT_TO)
        {
            i4OutCome =
                (INT4) (nmhGetFirstIndexIssRedirectInterfaceGrpTable
                        ((UINT4 *) &i4GroupId));
            if ((i4OutCome == SNMP_FAILURE))
            {
                ISS_UNLOCK ();
                IssSendError (pHttp,
                              (INT1 *)
                              "Delete : Redirect Group Not Available.");
                return;
            }
            do
            {
                if (nmhGetIssRedirectInterfaceGrpFilterType (i4GroupId,
                                                             &i4MapFilterType)
                    == SNMP_FAILURE)
                {
                    ISS_UNLOCK ();
                    return;
                }
                if (nmhGetIssRedirectInterfaceGrpFilterId
                    (i4GroupId, &u4MapFilterId) == SNMP_FAILURE)
                {
                    ISS_UNLOCK ();
                    return;
                }

                if ((i4MapFilterType == ISS_L2_REDIRECT) &&
                    (u4MapFilterId == (UINT4) i4FilterNo))
                {

                    i4RediretGrpFlag = i4GroupId;
                    break;
                }
                i4CurrentGroupId = i4GroupId;
            }
            while (nmhGetNextIndexIssRedirectInterfaceGrpTable
                   ((UINT4) i4CurrentGroupId,
                    (UINT4 *) &i4GroupId) == SNMP_SUCCESS);

            if (i4RediretGrpFlag != 0)
            {
                if (nmhSetIssRedirectInterfaceGrpStatus
                    (i4RediretGrpFlag, ISS_DESTROY) == SNMP_FAILURE)
                {
                    ISS_UNLOCK ();
                    IssSendError (pHttp,
                                  (INT1 *)
                                  "SET : Redirect Group Not Available.");
                    return;
                }
            }
        }

        if (nmhTestv2IssExtL2FilterStatus
            (&u4ErrorCode, i4FilterNo, ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "MAC filter not available.");
            return;
        }

        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "MAC filter not available.");
            return;
        }
        ISS_UNLOCK ();
        IssDxProcessMACFilterConfPageGet (pHttp);
        return;
    }

    if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {
        u1Apply = 1;
        u1RowStatus = ISS_NOT_IN_SERVICE;
    }

    STRCPY (pHttp->au1Name, "SOURCE_MAC");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    StrToMac (pHttp->au1Value, SrcMacAddr);

    STRCPY (pHttp->au1Name, "DESTINATION_MAC");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    StrToMac (pHttp->au1Value, DestMacAddr);

    STRCPY (pHttp->au1Name, "ACTION_KEY");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PRIORITY");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Priority = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "VLAN_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4VlanId = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "IN_PORT_LIST");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        MEMSET (u1PortList, 0, ISS_PORTLIST_LEN);
        issDecodeSpecialChar (pHttp->au1Value);

        if (STRCMP (pHttp->au1Value, "0") != 0)
        {
            if (IssPortListStringParser (pHttp->au1Value, u1PortList) ==
                ENM_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp, (INT1 *) "Check the allowed port list");
                return;
            }
        }
        InPortList.i4_Length = ISS_PORTLIST_LEN;
        InPortList.pu1_OctetList = MEM_CALLOC (InPortList.i4_Length, 1, UINT1);
        if (InPortList.pu1_OctetList == NULL)
        {
            ISS_UNLOCK ();
            return;
        }
        MEMCPY (InPortList.pu1_OctetList, u1PortList, InPortList.i4_Length);
    }
    STRCPY (pHttp->au1Name, "ENCAPTYPE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4EtherType = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PROTOCOL");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4Protocol = ATOI (pHttp->au1Value);

    if (u4Protocol == OTHER)
    {
        STRCPY (pHttp->au1Name, "OTHER");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Protocol = ATOI (pHttp->au1Value);
        if (u4Protocol == 0)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Value Range should be 1-65535");
            return;
        }
    }
    STRCPY (pHttp->au1Name, "SUB_VALUE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4SubAction = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "SUB_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4SubActionId = ATOI (pHttp->au1Value);
    if (((i4SubAction == 1) || (i4SubAction == 2)) && (i4SubActionId == 0))
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Enter SubAction Id.");
        return;
    }

    if (nmhTestv2IssExtL2FilterStatus (&u4ErrorCode, i4FilterNo,
                                       u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        IssSendError (pHttp, (INT1 *) "Invalid MAC filter number.");
        return;
    }

    if (nmhSetIssExtL2FilterStatus (i4FilterNo, u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        IssSendError (pHttp, (INT1 *) "Entry already exists for this MAC"
                      " filter number");
        return;
    }

    if (nmhSetIssExtL2FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the l2 filter action.");
        return;
    }

    if (MEMCMP (au1ZeroMac, SrcMacAddr, ISS_MAC_LEN))
    {
        if (nmhSetIssExtL2FilterSrcMacAddr (i4FilterNo, SrcMacAddr)
            == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set the source MAC Address.");
            return;
        }
    }

    if (MEMCMP (au1ZeroMac, DestMacAddr, ISS_MAC_LEN))
    {
        if (nmhSetIssExtL2FilterDstMacAddr
            (i4FilterNo, DestMacAddr) == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *)
                          "Unable to set the Destination MAC Address.");
            return;
        }
    }

    if (nmhSetIssExtL2FilterPriority (i4FilterNo, i4Priority) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter priority.");
        return;
    }

    if ((u4Protocol != 0) && (nmhSetIssExtL2FilterProtocolType
                              (i4FilterNo, u4Protocol) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter protocol type.");
        return;
    }

    if ((i4EtherType != 0) && (nmhSetIssExtL2FilterEtherType
                               (i4FilterNo, i4EtherType) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter ether type.");
        return;
    }

    if ((i4VlanId != 0) && (nmhSetIssExtL2FilterVlanId
                            (i4FilterNo, i4VlanId) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter VLAN ID.");
        return;
    }

    if ((InPortList.pu1_OctetList != NULL) && (nmhTestv2IssExtL2FilterInPortList
                                               (&u4ErrorCode, i4FilterNo,
                                                &InPortList) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Test: Unable to Set In-PortList");
        return;
    }
    if ((InPortList.pu1_OctetList != NULL) && (nmhSetIssExtL2FilterInPortList
                                               (i4FilterNo, &InPortList)
                                               == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set In Port List.");
        return;
    }
    if (nmhTestv2IssAclL2FilterSubAction
        (&u4ErrorCode, i4FilterNo, i4SubAction) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set SubAction.");
        return;
    }
    if (nmhSetIssAclL2FilterSubAction (i4FilterNo, i4SubAction) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set SubAction.");
        return;
    }

    if ((i4SubActionId != 0) && (nmhSetIssAclL2FilterSubActionId
                                 (i4FilterNo, i4SubActionId) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set SubActionId.");
        return;
    }

    if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the filter status.");
        return;
    }
    ISS_UNLOCK ();
    MEM_FREE (InPortList.pu1_OctetList);
    IssDxProcessMACFilterConfPageGet (pHttp);
}

#ifdef DIFFSRV_WANTED

/*********************************************************************
 *  Function Name : IssDxProcessDfsPolicyMapPage
 *  Description   : This function processes the request coming for the 
 *                  DiffSrv Policy Map page.
 *                     
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssDxProcessDfsPolicyMapPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssDxProcessDfsPolicyMapPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssDxProcessDfsPolicyMapPageSet (pHttp);
    }
    return;
}

/***************************************************************************
 *  Function Name : IssDxProcessDfsPolicyMapPageGet
 *  Description   : This function processes the Get request coming for the 
 *                  Diff Server Policy Map Page.
 *                     
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *****************************************************************************/
VOID
IssDxProcessDfsPolicyMapPageGet (tHttp * pHttp)
{
    tDiffServWebClfrData PolicyMapData;
    INT4                i4ClfrId;
    INT4                i4OutCome = 0;
    INT4                i4PolicyMapId, i4NextPolicyMapId;
    INT4                i4OutProfActionId = 0;
    INT4                i4MeterId;
    INT4                i4InProfActionType = 0;
    INT4                i4OutProfActionType = 0;
    UINT4               u4TrafficRate;
    UINT4               u4Temp = 0;

    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    WebnmRegisterLock (pHttp, DfsLock, DfsUnLock);

    DFS_LOCK ();

    i4OutCome = (INT4) nmhGetFirstIndexFsDiffServClfrTable (&i4PolicyMapId);

    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        DFS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        return;
    }
    u4Temp = pHttp->i4Write;
    do
    {
        pHttp->i4Write = u4Temp;
        STRCPY (pHttp->au1KeyString, "POLICY_MAPID");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4PolicyMapId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        i4OutCome = nmhGetFsDiffServClfrMFClfrId (i4PolicyMapId, &i4ClfrId);
        STRCPY (pHttp->au1KeyString, "CLFR_ID");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4OutCome != SNMP_FAILURE)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ClfrId);
        }
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));

        i4OutCome = nmhGetFsDiffServClfrOutProActionId (i4PolicyMapId,
                                                        &i4OutProfActionId);
        i4OutCome &= nmhGetFsDiffServOutProfileActionMID (i4OutProfActionId,
                                                          &(i4MeterId));
        u4TrafficRate = 0;
        i4OutCome &= nmhGetFsDiffServMeterRefreshCount (i4MeterId,
                                                        &u4TrafficRate);
        STRCPY (pHttp->au1KeyString, "TRAFFIC_RATE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if ((i4OutProfActionId != 0) && (i4MeterId != 0) &&
            (i4OutCome != SNMP_FAILURE))
        {
            if (u4TrafficRate > 0)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4TrafficRate);
            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, " ");
            }
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, " ");
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        if (DsWebnmGetPolicyMapEntry (i4PolicyMapId, &PolicyMapData)
            == SNMP_FAILURE)
        {
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            i4NextPolicyMapId = i4PolicyMapId;
            continue;
        }

        STRCPY (pHttp->au1KeyString, "INPROF_ACT_TYPE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (STRCMP (PolicyMapData.au1DsClfrInProfActionType, "Policed-Dscp") ==
            0)
        {
            i4InProfActionType = 1;
        }
        else if (STRCMP
                 (PolicyMapData.au1DsClfrInProfActionType,
                  "Policed-Precedence") == 0)
        {
            i4InProfActionType = 2;
        }
        else if (STRCMP
                 (PolicyMapData.au1DsClfrInProfActionType,
                  "Policed-Priority") == 0)
        {
            i4InProfActionType = 3;
        }
        else
        {
            i4InProfActionType = 0;
        }
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4InProfActionType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "INPROF_ACT_VALUE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                 PolicyMapData.au1DsClfrInProfActionValue);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OUTPROF_ACT_TYPE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (STRCMP (PolicyMapData.au1DsClfrOutProfActionType, "Drop") == 0)
        {
            i4OutProfActionType = 1;
        }
        else if (STRCMP
                 (PolicyMapData.au1DsClfrOutProfActionType,
                  "Policed-Dscp") == 0)
        {
            i4OutProfActionType = 2;
        }
        else
        {
            i4OutProfActionType = 0;
        }
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4OutProfActionType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OUTPROF_ACT_VALUE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                 PolicyMapData.au1DsClfrOutProfActionValue);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        i4NextPolicyMapId = i4PolicyMapId;
    }
    while (nmhGetNextIndexFsDiffServClfrTable (i4NextPolicyMapId,
                                               &i4PolicyMapId) == SNMP_SUCCESS);

    DFS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 *  Function Name : IssDxProcessDfsPolicyMapPageSet
 *  Description   : This function processes the request coming for the 
 *                  Diff Server Policy Map Page.
 *                     
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ***********************************************************************/
VOID
IssDxProcessDfsPolicyMapPageSet (tHttp * pHttp)
{
    tDiffServWebSetClfrEntry ClfrEntry;
    UINT1               au1ErrStr[255];
    UINT1               u1Apply = 0;
    INT4                i4PolicyMapId;
    INT4                i4ClassMapId;
    INT4                i4InProfActionId;
    INT4                i4OutProfActionId;
    INT4                i4MeterId;
    INT4                i4FirstClassMapId;
    INT4                i4NextClassMapId;
    INT4                i4MFCId;
    INT4                i4Status;
    INT4                i4DsStatus;
    UINT4               u4ErrorCode;
    UINT4               u4TrafficRate;
    UINT4               u4OutProfActType;
    UINT4               u4InDscp = 0;
    UINT4               u4InPrec = 0;
    UINT4               u4OutDscp = 0;
    UINT4               u4CosValue = 0;
    UINT4               u4InProfActType = 0;
    UINT1               u1RowStatus = ISS_CREATE_AND_WAIT;

    /* Get the Policy-Map ID */
    STRCPY (pHttp->au1Name, "POLICY_MAPID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PolicyMapId = ATOI (pHttp->au1Value);

    DFS_LOCK ();

    nmhGetFsDsStatus (&i4DsStatus);
    if (i4DsStatus == 2)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "DiffServer not enabled.");
        return;
    }
    /* Check whether the request is for ADD/DELETE */
    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    /* check for delete */
    if (STRCMP (pHttp->au1Value, "DELETE") == 0)
    {
        if (nmhGetFsDiffServClfrStatus
            (i4PolicyMapId, &i4Status) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Policy Map entry not available.");
            return;
        }

        if ((i4Status == ISS_ACTIVE) &&
            (nmhSetFsDiffServClfrStatus (i4PolicyMapId,
                                         ISS_NOT_IN_SERVICE) == SNMP_FAILURE))
        {
            DFS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Cannot modify Policy Map entry status.");
            return;
        }

        nmhGetFsDiffServClfrInProActionId (i4PolicyMapId, &i4InProfActionId);
        if (i4InProfActionId != 0)
        {
            if (nmhGetFsDiffServInProfileActionStatus
                (i4InProfActionId, &i4Status) != SNMP_FAILURE)
            {

                if (nmhSetFsDiffServInProfileActionStatus
                    (i4InProfActionId, ISS_DESTROY) == SNMP_FAILURE)
                {
                    DFS_UNLOCK ();
                    IssSendError (pHttp,
                                  (INT1 *) "In-Profile Id does not exists");
                    return;
                }
            }
        }

        nmhGetFsDiffServClfrOutProActionId (i4PolicyMapId, &i4OutProfActionId);
        nmhGetFsDiffServOutProfileActionMID (i4OutProfActionId, &i4MeterId);
        if (i4OutProfActionId != 0)
        {
            if (nmhGetFsDiffServOutProfileActionStatus
                (i4OutProfActionId, &i4Status) != SNMP_FAILURE)
            {
                if (nmhSetFsDiffServOutProfileActionStatus
                    (i4OutProfActionId, ISS_DESTROY) == SNMP_FAILURE)
                {
                    DFS_UNLOCK ();
                    IssSendError (pHttp,
                                  (INT1 *) "Out-Profile Id does not exists");
                    return;
                }
            }
            if (i4MeterId != 0)
            {
                if (nmhGetFsDiffServMeterStatus
                    (i4MeterId, &i4Status) != SNMP_FAILURE)
                {
                    if (nmhSetFsDiffServMeterStatus
                        (i4MeterId, ISS_DESTROY) == SNMP_FAILURE)
                    {
                        DFS_UNLOCK ();
                        IssSendError (pHttp,
                                      (INT1 *) "Meter Id does not exists");
                        return;
                    }
                }
            }
        }
        if (nmhGetFsDiffServClfrStatus
            (i4PolicyMapId, &i4Status) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            return;
        }

        if (nmhSetFsDiffServClfrStatus (i4PolicyMapId,
                                        ISS_DESTROY) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Cannot delete Policy Map entry.");
            return;
        }
        DFS_UNLOCK ();
        IssDxProcessDfsPolicyMapPageGet (pHttp);
        return;
    }

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "apply") == 0)
    {
        u1Apply = 1;
        u1RowStatus = ISS_NOT_IN_SERVICE;
    }

    STRCPY (pHttp->au1Name, "CLFR_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4ClassMapId = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Add") == 0)
    {
        /* Checking with Policy-Map ID */
        STRCPY (pHttp->au1Name, "POLICY_MAPID");
        if (nmhTestv2FsDiffServClfrStatus
            (&u4ErrorCode, i4PolicyMapId, u1RowStatus) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Invalid Policy Map ID.");
            return;
        }
        /* Check if class map exists */
        if (nmhGetFsDiffServMultiFieldClfrStatus
            (i4ClassMapId, &i4Status) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Invalid Class Map ID");
            return;
        }

        /* Check if the class-map already allocated to another policy-map.
         * We can't allocate the same class-map to different policy-map, because
         * Class-Map id == Datapath id */
        if (nmhGetFirstIndexFsDiffServClfrTable (&i4FirstClassMapId)
            != SNMP_FAILURE)
        {
            /* For the first time */
            i4NextClassMapId = i4FirstClassMapId;
            do
            {
                nmhGetFsDiffServClfrMFClfrId (i4NextClassMapId, &i4MFCId);
                if (i4MFCId == i4ClassMapId)
                {
                    DFS_UNLOCK ();
                    IssSendError (pHttp, (INT1 *)
                                  "Class map already allocated to another policy-map");
                    return;
                }

                i4FirstClassMapId = i4NextClassMapId;
            }
            while (nmhGetNextIndexFsDiffServClfrTable
                   (i4FirstClassMapId, &i4NextClassMapId) != SNMP_FAILURE);
        }
    }

    STRCPY (pHttp->au1Name, "TRAFFIC_RATE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4TrafficRate = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "INPROF_ACT_TYPE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4InProfActType = ATOI (pHttp->au1Value);

    if (u4InProfActType == 1)
    {
        STRCPY (pHttp->au1Name, "INPROF_ACT_VALUE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4InDscp = ATOI (pHttp->au1Value);
    }
    else if (u4InProfActType == 2)
    {
        STRCPY (pHttp->au1Name, "INPROF_ACT_VALUE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4InPrec = ATOI (pHttp->au1Value);
    }
    else if (u4InProfActType == 3)
    {
        STRCPY (pHttp->au1Name, "INPROF_ACT_VALUE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4CosValue = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "OUTPROF_ACT_TYPE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4OutProfActType = ATOI (pHttp->au1Value);

    if (u4OutProfActType == 2)
    {
        STRCPY (pHttp->au1Name, "OUTPROF_ACT_VALUE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4OutDscp = ATOI (pHttp->au1Value);
    }

    MEMSET (&ClfrEntry, 0, sizeof (tDiffServWebSetClfrEntry));
    ClfrEntry.i4DsClfrId = i4PolicyMapId;
    ClfrEntry.i4DsClfrMFClfrId = i4ClassMapId;
    ClfrEntry.i4DsClfrInProActionId = i4PolicyMapId;
    ClfrEntry.u4InProfActType = u4InProfActType;
    ClfrEntry.u4InDscp = u4InDscp;
    ClfrEntry.u4InPrec = u4InPrec;
    ClfrEntry.u4CosValue = u4CosValue;
    ClfrEntry.i4DsClfrOutProActionId = i4PolicyMapId;
    ClfrEntry.i4MeterId = i4PolicyMapId;
    ClfrEntry.u4OutProfActType = u4OutProfActType;
    ClfrEntry.u4OutDscp = u4OutDscp;
    ClfrEntry.u4TrafficRate = u4TrafficRate;

    if (DsWebnmSetPolicyMapEntry (&ClfrEntry, u1RowStatus, au1ErrStr) ==
        SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) au1ErrStr);
        IssDxProcessDfsPolicyMapPageGet (pHttp);
        return;
    }

    DFS_UNLOCK ();
    IssDxProcessDfsPolicyMapPageGet (pHttp);
}

/*********************************************************************
 *  function name : IssDxProcessSchdAlgoPage
 *  description   : This function processes the request coming for the 
 *                  diffsrv Cosq Schedule Algorithm configuration page.
 *                     
 *  input(s)      : phttp - pointer to the global http data structure.
 *  output(s)     : none.
 *  return values : none
 *********************************************************************/

VOID
IssDxProcessSchdAlgoPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssDxProcessSchdAlgoPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssDxProcessSchdAlgoPageSet (pHttp);
    }
    return;
}

/*********************************************************************
 *  function name : IssDxProcessSchdAlgoPageGet
 *  description   : This function processes the get request coming for the 
 *                  diffsrv Cosq Schedule Algorithm configuration page.
 *                  Get   
 *  input(s)      : phttp - pointer to the global http data structure.
 *  output(s)     : none.
 *  return values : none
 *********************************************************************/

VOID
IssDxProcessSchdAlgoPageGet (tHttp * pHttp)
{
    INT4                i4NextIndex = 0;
    INT4                i4PreIndex = 0;
    INT4                i4CoSqAlg = 0;
    INT4                i4OutCome = 0;
    UINT4               u4Temp = 0;

    WebnmRegisterLock (pHttp, DfsLock, DfsUnLock);
    DFS_LOCK ();

    i4OutCome = nmhGetFirstIndexFsDiffServCoSqAlgorithmTable (&i4NextIndex);
    if (i4OutCome == SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = pHttp->i4Write;

    do
    {
        pHttp->i4Write = u4Temp;

        STRCPY (pHttp->au1KeyString, "PORT_NUM_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDiffServCoSqAlgorithm (i4NextIndex, &i4CoSqAlg);
        STRCPY (pHttp->au1KeyString, "COSQ_ALGO_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4CoSqAlg);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        i4PreIndex = i4NextIndex;
    }
    while (nmhGetNextIndexFsDiffServCoSqAlgorithmTable
           (i4PreIndex, &i4NextIndex) == SNMP_SUCCESS);
    DFS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;

}

/*********************************************************************
 *  function name : IssDxProcessSchdAlgoPageSet
 *  description   : This function processes the set request coming for the 
 *                  diffsrv Cosq Schedule Algorithm configuration page.
 *                  Set   
 *  input(s)      : phttp - pointer to the global http data structure.
 *  output(s)     : none.
 *  return values : none
 *********************************************************************/

VOID
IssDxProcessSchdAlgoPageSet (tHttp * pHttp)
{
    INT4                i4PortNum = 0;
    INT4                i4CoSqAlg = 0;
    UINT4               u4ErrCode = 0;

    STRCPY (pHttp->au1Name, "PORT_NUM");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PortNum = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "COSQ_ALGO");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4CoSqAlg = ATOI (pHttp->au1Value);

    WebnmRegisterLock (pHttp, DfsLock, DfsUnLock);
    DFS_LOCK ();

    if (nmhTestv2FsDiffServCoSqAlgorithm
        (&u4ErrCode, i4PortNum, i4CoSqAlg) == SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the CoSq Algorithm");
        return;
    }

    if (nmhSetFsDiffServCoSqAlgorithm (i4PortNum, i4CoSqAlg) == SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the CoSq Algorithm");
        return;
    }

    DFS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    IssDxProcessSchdAlgoPageGet (pHttp);
}

/*********************************************************************
 *  function name : IssDxProcessDfsCosqWeightBWConfPage
 *  description   : This function processes the request coming for the 
 *                  diffsrv Cosq Weight and Bandwidth configuration page.
 *                     
 *  input(s)      : phttp - pointer to the global http data structure.
 *  output(s)     : none.
 *  return values : none
 *********************************************************************/

VOID
IssDxProcessDfsCosqWeightBWConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssDxProcessDfsCosqWeightBWConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssDxProcessDfsCosqWeightBWConfPageSet (pHttp);
    }
    return;
}

/*********************************************************************
 *  function name : IssDxProcessDfsCosqWeightBWConfPageGet
 *  description   : This function processes the get request coming for the 
 *                  diffsrv Cosq Weight and Bandwidth configuration page.
 *                  Get   
 *  input(s)      : phttp - pointer to the global http data structure.
 *  output(s)     : none.
 *  return values : none
 *********************************************************************/

VOID
IssDxProcessDfsCosqWeightBWConfPageGet (tHttp * pHttp)
{
    INT4                i4PortNum = 0;
    INT4                i4CosqId = 0;
    UINT4               u4CosqMaxBw = 0;
    UINT4               u4CosqMinBw = 0;
    UINT4               u4CosqWt = 0;
    UINT4               i4CosqFlag = 0;
    UINT4               u4Temp = 0;

    IssPrintAvailableL2Ports (pHttp);

    WebnmRegisterLock (pHttp, DfsLock, DfsUnLock);
    DFS_LOCK ();

    STRCPY (pHttp->au1Name, "PORT_NUM");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_FAILURE)
    {
        nmhGetFirstIndexFsDiffServCoSqWeightBwTable (&i4PortNum, &i4CosqId);
    }
    else
    {
        i4PortNum = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1KeyString, "PORT_NUM_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4PortNum);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = pHttp->i4Write;

    for (; i4CosqId <= 7; i4CosqId++)
    {
        pHttp->i4Write = u4Temp;

        STRCPY (pHttp->au1KeyString, "COSQ_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4CosqId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDiffServCoSqWeight (i4PortNum, i4CosqId, &u4CosqWt);
        STRCPY (pHttp->au1KeyString, "COSQ_WEIGHT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4CosqWt);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDiffServCoSqBwMin (i4PortNum, i4CosqId, &u4CosqMinBw);
        STRCPY (pHttp->au1KeyString, "COSQ_MIN_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4CosqMinBw);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDiffServCoSqBwMax (i4PortNum, i4CosqId, &u4CosqMaxBw);
        STRCPY (pHttp->au1KeyString, "COSQ_MAX_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4CosqMaxBw);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDiffServCoSqBwFlags (i4PortNum, i4CosqId, (INT4 *) &i4CosqFlag);
        STRCPY (pHttp->au1KeyString, "COSQ_FLAG_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4CosqFlag);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    DFS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;

}

/*********************************************************************
 *  function name : IssDxProcessDfsCosqWeightBWConfPageSet
 *  description   : This function processes the request coming for the 
 *                  diffsrv Cosq Weight and Bandwidth configuration page.
 *                  Set   
 *  input(s)      : phttp - pointer to the global http data structure.
 *  output(s)     : none.
 *  return values : none
 *********************************************************************/

VOID
IssDxProcessDfsCosqWeightBWConfPageSet (tHttp * pHttp)
{
    INT4                i4PortNum = 0;
    INT4                i4CosqId = 0;
    UINT4               u4CosqMinBw = 0;
    UINT4               u4CosqWt = 0;
    UINT4               u4ErrCode = 0;

    STRCPY (pHttp->au1Name, "PORT_NUM");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PortNum = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "COSQ_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4CosqId = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "COSQ_WEIGHT");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4CosqWt = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "COSQ_MIN");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4CosqMinBw = ATOI (pHttp->au1Value);

    WebnmRegisterLock (pHttp, DfsLock, DfsUnLock);
    DFS_LOCK ();

    if (nmhTestv2FsDiffServCoSqWeight
        (&u4ErrCode, i4PortNum, i4CosqId, u4CosqWt) == SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the CoSq Weight");
        return;
    }

    if (nmhSetFsDiffServCoSqWeight (i4PortNum, i4CosqId, u4CosqWt) ==
        SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the CoSq Weight");
        return;
    }

    if (u4CosqMinBw != 0)
    {
        if (nmhTestv2FsDiffServCoSqBwMin
            (&u4ErrCode, i4PortNum, i4CosqId, u4CosqMinBw) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set the CoSq Minimum Bandwidth");
            return;
        }

        if (nmhSetFsDiffServCoSqBwMin
            (i4PortNum, i4CosqId, u4CosqMinBw) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set the CoSq Minimum Bandwidth");
            return;
        }
    }

    DFS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    IssDxProcessDfsCosqWeightBWConfPageGet (pHttp);
}
#endif
#endif
