/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: issexinc.h,v 1.10 2012/10/31 09:46:19 siva Exp $
 *
 * Description: This file include all the header files required 
 *              for the operation of the ACL module
 ****************************************************************************/
#ifndef _ISSEXINC_H
#define _ISSEXINC_H

/* Common Includes */
#include "lr.h"
#include "cfa.h"
#include "tcp.h"
#ifndef BSDCOMP_SLI_WANTED
#include "sli.h"
#endif
#include "rmgr.h"
#include "iss.h"
#include "msr.h"
#include "ip.h"

#include "fsvlan.h"
#include "hwaud.h"
#include "hwaudmap.h"


#include "snmccons.h"
#include "snmcdefn.h"
#include "fssnmp.h"


#ifdef NPAPI_WANTED
#include "npapi.h"
#include "issnpwr.h"
#include "issnp.h"
#endif

/* ISS includes */
#include "issmacro.h"
#ifdef L2RED_WANTED
#include "issred.h"
#endif /* L2RED_WANTED */
#include "isstdfs.h"
#include "issglob.h"
#include "issexmacr.h"
#include "issextdfs.h"
#include "issexglob.h"
#include "issexprot.h"

#ifdef MBSM_WANTED
#include "mbsm.h"
#include "issmbsm.h"
#endif

#ifdef ISS_METRO_WANTED
#include "fsvlan.h"
#endif
#include "ip6util.h"


#endif /* _ISSEXINC_H */
