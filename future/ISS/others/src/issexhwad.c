/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: issexhwad.c,v 1.7 2014/06/03 13:10:48 siva Exp $
 *
 * Description: This file contains the  NP SYNC handling functions 
 * for hardware audit process.
 *****************************************************************************/

#include "issexinc.h"

/*****************************************************************************/
/* Function Name      : NpSyncIssHwUpdateL2Filter                 */
/*                                                                           */
/* Description        : This function sends the NP Sync message and          */
/*            calls the corresponding NPAPI function and updates   */
/*             the NP Sync buffer table in case of Standby Node.    */
/*                                                                           */
/* Input(s)           : pIssL2FilterEntry- Pointer to the structure of       */
/*                    L2 Filter Entry              */
/*            i4Value - Filter Add/Delete                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                 */
/*****************************************************************************/
PUBLIC INT4
NpSyncIssHwUpdateL2Filter (tIssL2FilterEntry * pIssL2FilterEntry, INT4 i4Value)
{
    unNpSync            NpSync;
    INT4                i4RetVal = OSIX_SUCCESS;
    INT4                i4FnpRetVal = FNP_SUCCESS;
    UINT4               u4NodeState = ISS_ZERO_ENTRY;

    MEMSET (&NpSync, ISS_ZERO_ENTRY, sizeof (unNpSync));

    NpSync.IssHwUpdateL2Filter.i4IssL2FilterNo =
        pIssL2FilterEntry->i4IssL2FilterNo;
    MEMCPY (&(NpSync.IssHwUpdateL2Filter.IssFilterShadowTable),
            pIssL2FilterEntry->pIssFilterShadowInfo,
            sizeof (tIssFilterShadowTable));
    NpSync.IssHwUpdateL2Filter.i4Value = i4Value;

    u4NodeState = (UINT4) AclPortRmGetNodeState ();

    if (u4NodeState == RM_ACTIVE)
    {
        if (AclPortRmGetStandbyNodeCount () != ISS_ZERO_ENTRY)
        {
            if (ACL_NPSYNC_BLK () == ISS_ZERO_ENTRY)
            {
                NpSyncIssHwUpdateL2FilterSync (NpSync.IssHwUpdateL2Filter,
                                               RM_ACL_APP_ID,
                                               NPSYNC_ISS_HW_UPDATE_L2_FILTER,
                                               ISS_ACL_NP_SYNC_UPDATE);
            }
        }
#ifdef NPAPI_WANTED
#undef IsssysIssHwUpdateL2Filter
        i4FnpRetVal = IsssysIssHwUpdateL2Filter (pIssL2FilterEntry, i4Value);
        if (i4FnpRetVal == FNP_FAILURE)
        {
            i4RetVal = OSIX_FAILURE;
        }
#endif

        if (AclPortRmGetStandbyNodeCount () != ISS_ZERO_ENTRY)
        {
            if (ACL_NPSYNC_BLK () == ISS_ZERO_ENTRY)
            {
#ifdef NPAPI_WANTED
#undef IsssysIssHwUpdateL2Filter
                if (i4RetVal == OSIX_FAILURE)
                {
                    /* if NP call is failure, send the NP sync message to the 
                       standby to delete the entry from the Np Sync Buffer table */
                    NpSyncIssHwUpdateL2FilterSync (NpSync.IssHwUpdateL2Filter,
                                                   RM_ACL_APP_ID,
                                                   NPSYNC_ISS_HW_UPDATE_L2_FILTER,
                                                   ISS_ACL_NP_SYNC_FAILURE);
                }
#endif
                if (i4RetVal == OSIX_SUCCESS)
                {
                    /* if NP call is success, update the Hardware Handle in the
                       Np Sync message and send the Sync message to the standby */
                    MEMCPY (&(NpSync.IssHwUpdateL2Filter.
                              IssFilterShadowTable), pIssL2FilterEntry->
                            pIssFilterShadowInfo,
                            sizeof (tIssFilterShadowTable));
                    NpSyncIssHwUpdateL2FilterSync (NpSync.IssHwUpdateL2Filter,
                                                   RM_ACL_APP_ID,
                                                   NPSYNC_ISS_HW_UPDATE_L2_FILTER,
                                                   ISS_ACL_NP_SYNC_SUCCESS);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (ACL_NPSYNC_BLK () == ISS_ZERO_ENTRY)
        {
            AclRedUpdateNpSyncBufferTable
                (&NpSync, NPSYNC_ISS_HW_UPDATE_L2_FILTER,
                 ISS_ACL_NP_SYNC_UPDATE);
        }
    }
    return (i4FnpRetVal);
}

/*****************************************************************************/
/* Function Name      : NpSyncIssHwUpdateL3Filter                 */
/*                                                                           */
/* Description        : This function sends the NP Sync message and          */
/*            calls the corresponding NPAPI function and updates   */
/*             the NP Sync buffer table in case of Standby Node.    */
/*                                                                           */
/* Input(s)           : pIssL3FilterEntry- Pointer to the structure of       */
/*                    L3 Filter Entry              */
/*            i4Value - Filter Add/Delete                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                 */
/*****************************************************************************/
PUBLIC INT4
NpSyncIssHwUpdateL3Filter (tIssL3FilterEntry * pIssL3FilterEntry, INT4 i4Value)
{
    unNpSync            NpSync;
    INT4                i4RetVal = OSIX_SUCCESS;
    INT4                i4FnpRetVal = FNP_SUCCESS;
    UINT4               u4NodeState = ISS_ZERO_ENTRY;

    MEMSET (&NpSync, ISS_ZERO_ENTRY, sizeof (unNpSync));
    NpSync.IssHwUpdateL3Filter.i4IssL3FilterNo =
        pIssL3FilterEntry->i4IssL3FilterNo;
    MEMCPY (&(NpSync.IssHwUpdateL3Filter.IssFilterShadowTable),
            pIssL3FilterEntry->pIssFilterShadowInfo,
            sizeof (tIssFilterShadowTable));
    NpSync.IssHwUpdateL3Filter.i4Value = i4Value;

    u4NodeState = (UINT4) AclPortRmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (AclPortRmGetStandbyNodeCount () != ISS_ZERO_ENTRY)
        {
            if (ACL_NPSYNC_BLK () == ISS_ZERO_ENTRY)
            {
                NpSyncIssHwUpdateL3FilterSync (NpSync.IssHwUpdateL3Filter,
                                               RM_ACL_APP_ID,
                                               NPSYNC_ISS_HW_UPDATE_L3_FILTER,
                                               ISS_ACL_NP_SYNC_UPDATE);
            }
        }
#ifdef NPAPI_WANTED
#undef IsssysIssHwUpdateL3Filter
        i4FnpRetVal = IsssysIssHwUpdateL3Filter (pIssL3FilterEntry, i4Value);
        if (i4FnpRetVal == FNP_FAILURE)
        {
            i4RetVal = OSIX_FAILURE;
        }
#endif
        if (AclPortRmGetStandbyNodeCount () != ISS_ZERO_ENTRY)
        {
            if (ACL_NPSYNC_BLK () == ISS_ZERO_ENTRY)
            {
#ifdef NPAPI_WANTED
#undef IsssysIssHwUpdateL3Filter
                if (i4RetVal == OSIX_FAILURE)
                {
                    /* if NP call is failure, send the NP sync message to the 
                       standby to delete the entry from the Np Sync Buffer table */
                    NpSyncIssHwUpdateL3FilterSync (NpSync.IssHwUpdateL3Filter,
                                                   RM_ACL_APP_ID,
                                                   NPSYNC_ISS_HW_UPDATE_L3_FILTER,
                                                   ISS_ACL_NP_SYNC_FAILURE);
                }
#endif
                if (i4RetVal == OSIX_SUCCESS)
                {
                    /* if NP call is success, update the Hardware Handle in the
                       Np Sync message and send the Sync message to the standby */
                    MEMCPY (&(NpSync.IssHwUpdateL3Filter.
                              IssFilterShadowTable), pIssL3FilterEntry->
                            pIssFilterShadowInfo,
                            sizeof (tIssFilterShadowTable));
                    NpSyncIssHwUpdateL3FilterSync (NpSync.IssHwUpdateL3Filter,
                                                   RM_ACL_APP_ID,
                                                   NPSYNC_ISS_HW_UPDATE_L3_FILTER,
                                                   ISS_ACL_NP_SYNC_SUCCESS);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (ACL_NPSYNC_BLK () == ISS_ZERO_ENTRY)
        {
            AclRedUpdateNpSyncBufferTable
                (&NpSync, NPSYNC_ISS_HW_UPDATE_L3_FILTER,
                 ISS_ACL_NP_SYNC_UPDATE);
        }
    }
    return (i4FnpRetVal);
}

/*****************************************************************************/
/* Function Name      : NpSyncIssHwSetRateLimitingValue                 */
/*                                                                           */
/* Description        : This function sends the NP Sync message and          */
/*            calls the corresponding NPAPI function and updates   */
/*             the NP Sync buffer table in case of Standby Node.    */
/*                                                                           */
/* Input(s)           : Inputs to the NPAPI calls                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                 */
/*****************************************************************************/
PUBLIC INT4
NpSyncIssHwSetRateLimitingValue (UINT4 u4IfIndex,
                                 UINT1 u1PacketType, INT4 i4RateLimitVal)
{
    unNpSync            NpSync;
    INT4                i4RetVal = OSIX_SUCCESS;
    INT4                i4FnpRetVal = FNP_SUCCESS;
    UINT4               u4NodeState = ISS_ZERO_ENTRY;

    MEMSET (&NpSync, ISS_ZERO_ENTRY, sizeof (unNpSync));
    NpSync.IssHwSetRateLimitingValue.u4IfIndex = u4IfIndex;
    NpSync.IssHwSetRateLimitingValue.u1PacketType = u1PacketType;
    NpSync.IssHwSetRateLimitingValue.i4RateLimitVal = i4RateLimitVal;

    u4NodeState = (UINT4) AclPortRmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (AclPortRmGetStandbyNodeCount () != ISS_ZERO_ENTRY)
        {
            if (ACL_NPSYNC_BLK () == ISS_ZERO_ENTRY)
            {
                NpSyncIssHwSetRateLimitingValueSync
                    (NpSync.IssHwSetRateLimitingValue, RM_ACL_APP_ID,
                     NPSYNC_ISS_HW_SET_RATE_LIMITING_VALUE);
            }
        }
#ifdef NPAPI_WANTED
#undef IsssysIssHwSetRateLimitingValue
        i4FnpRetVal = IsssysIssHwSetRateLimitingValue (u4IfIndex, u1PacketType,
                                                 i4RateLimitVal);
        if (i4FnpRetVal == FNP_FAILURE)
        {
            i4RetVal = OSIX_FAILURE;
        }
        if (i4RetVal == OSIX_FAILURE)
        {
            if (AclPortRmGetStandbyNodeCount () != ISS_ZERO_ENTRY)
            {
                if (ACL_NPSYNC_BLK () == ISS_ZERO_ENTRY)
                {
                    NpSyncIssHwSetRateLimitingValueSync
                        (NpSync.IssHwSetRateLimitingValue, RM_ACL_APP_ID,
                         NPSYNC_ISS_HW_SET_RATE_LIMITING_VALUE);
                }
            }
        }
#endif
    }
    else if (u4NodeState == RM_STANDBY)
    {
        UNUSED_PARAM (i4RetVal);
        if (ACL_NPSYNC_BLK () == ISS_ZERO_ENTRY)
        {
            AclRedUpdateNpSyncBufferTable
                (&NpSync, NPSYNC_ISS_HW_SET_RATE_LIMITING_VALUE,
                 ISS_ZERO_ENTRY);
        }

    }
    return (i4FnpRetVal);
}

/*****************************************************************************/
/* Function Name      : NpSyncIssHwSetPortEgressPktRate                 */
/*                                                                           */
/* Description        : This function sends the NP Sync message and          */
/*            calls the corresponding NPAPI function and updates   */
/*             the NP Sync buffer table in case of Standby Node.    */
/*                                                                           */
/* Input(s)           : Inputs to the NPAPI calls                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                 */
/*****************************************************************************/
PUBLIC INT4
NpSyncIssHwSetPortEgressPktRate (UINT4 u4IfIndex,
                                 INT4 i4PktRate, INT4 i4BurstRate)
{
    unNpSync            NpSync;
    INT4                i4RetVal = OSIX_SUCCESS;
    INT4                i4FnpRetVal = FNP_SUCCESS;
    UINT4               u4NodeState = ISS_ZERO_ENTRY;

    MEMSET (&NpSync, ISS_ZERO_ENTRY, sizeof (unNpSync));
    NpSync.IssHwSetPortEgressPktRate.u4IfIndex = u4IfIndex;
    NpSync.IssHwSetPortEgressPktRate.i4PktRate = i4PktRate;
    NpSync.IssHwSetPortEgressPktRate.i4BurstRate = i4BurstRate;

    u4NodeState = (UINT4) AclPortRmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (AclPortRmGetStandbyNodeCount () != ISS_ZERO_ENTRY)
        {
            if (ACL_NPSYNC_BLK () == ISS_ZERO_ENTRY)
            {
                NpSyncIssHwSetPortEgressPktRateSync
                    (NpSync.IssHwSetPortEgressPktRate, RM_ACL_APP_ID,
                     NPSYNC_ISS_HW_SET_PORT_EGRESS_PKT_RATE);
            }
        }
#ifdef NPAPI_WANTED
#undef IsssysIssHwSetPortEgressPktRate
        i4FnpRetVal = IsssysIssHwSetPortEgressPktRate (u4IfIndex, i4PktRate,
                                                 i4BurstRate);
        if (i4FnpRetVal == FNP_FAILURE)
        {
            i4RetVal = OSIX_FAILURE;
        }
        if (i4RetVal == OSIX_FAILURE)
        {
            if (AclPortRmGetStandbyNodeCount () != ISS_ZERO_ENTRY)
            {
                if (ACL_NPSYNC_BLK () == ISS_ZERO_ENTRY)
                {
                    NpSyncIssHwSetPortEgressPktRateSync
                        (NpSync.IssHwSetPortEgressPktRate, RM_ACL_APP_ID,
                         NPSYNC_ISS_HW_SET_PORT_EGRESS_PKT_RATE);
                }
            }
        }
#endif
    }
    else if (u4NodeState == RM_STANDBY)
    {
        UNUSED_PARAM (i4RetVal);
        if (ACL_NPSYNC_BLK () == ISS_ZERO_ENTRY)
        {
            AclRedUpdateNpSyncBufferTable
                (&NpSync, NPSYNC_ISS_HW_SET_PORT_EGRESS_PKT_RATE,
                 ISS_ZERO_ENTRY);
        }
    }
    return (i4FnpRetVal);
}

/*****************************************************************************/
/* Function Name      : NpSyncIssHwUpdateL2FilterSync                 */
/*                                                                           */
/* Description        : This function forms the the NP Sync message and      */
/*            post the message to RM Module.                  */
/*                                                                           */
/* Input(s)           : Union to the NP Sync Entry, NPAPI ID, Application ID */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
VOID
NpSyncIssHwUpdateL2FilterSync (tNpSyncIssHwUpdateL2Filter
                               wUpdateL2Filter, UINT4 u4AppId,
                               UINT4 u4NpApiId, UINT4 u4Status)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT4               u4Count = ISS_ZERO_ENTRY;
    UINT4               u4OffSet = ISS_ZERO_ENTRY;
    UINT2               u2MsgSize = ISS_ZERO_ENTRY;

    MEMSET (&ProtoEvt, ISS_ZERO_ENTRY, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH +
        NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (INT4) + sizeof (INT4) +
        sizeof (tIssFilterShadowTable) + sizeof (INT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        AclPortRmApiHandleProtocolEvent (&ProtoEvt);
        UNUSED_PARAM (u4Count);
        UNUSED_PARAM (u4OffSet);
        UNUSED_PARAM (wUpdateL2Filter);
        UNUSED_PARAM (u4NpApiId);
        UNUSED_PARAM (u4Status);
    }
#ifdef L2RED_WANTED
    else
    {
        u4OffSet = ISS_ZERO_ENTRY;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u4OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u4OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u4OffSet, u4Status);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u4OffSet, wUpdateL2Filter.i4IssL2FilterNo);
        for (u4Count = ISS_ZERO_ENTRY; u4Count < ISS_FILTER_SHADOW_MEM_SIZE;
             u4Count++)
        {
            NPSYNC_RM_PUT_4_BYTE (pMsg, &u4OffSet,
                                  wUpdateL2Filter.IssFilterShadowTable.
                                  au4HwHandle[u4Count]);
        }
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u4OffSet, wUpdateL2Filter.i4Value);

        if (AclPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet, u4AppId,
                                 u4AppId) == OSIX_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            AclPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
#else
    UNUSED_PARAM (u2MsgSize);
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : NpSyncIssHwUpdateL3FilterSync                 */
/*                                                                           */
/* Description        : This function forms the the NP Sync message and      */
/*            post the message to RM Module.                  */
/*                                                                           */
/* Input(s)           : Union to the NP Sync Entry, NPAPI ID, Application ID */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
VOID
NpSyncIssHwUpdateL3FilterSync (tNpSyncIssHwUpdateL3Filter
                               wUpdateL3Filter, UINT4 u4AppId,
                               UINT4 u4NpApiId, UINT4 u4Status)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT4               u4Count = ISS_ZERO_ENTRY;
    UINT4               u4OffSet = ISS_ZERO_ENTRY;
    UINT2               u2MsgSize = ISS_ZERO_ENTRY;

    MEMSET (&ProtoEvt, ISS_ZERO_ENTRY, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH +
        NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (INT4) + sizeof (INT4) +
        sizeof (tIssFilterShadowTable) + sizeof (INT4);

    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        AclPortRmApiHandleProtocolEvent (&ProtoEvt);
        UNUSED_PARAM (u4OffSet);
        UNUSED_PARAM (u4Count);
        UNUSED_PARAM (u4NpApiId);
        UNUSED_PARAM (wUpdateL3Filter);
        UNUSED_PARAM (u4Status);
    }
#ifdef L2RED_WANTED
    else
    {
        u4OffSet = ISS_ZERO_ENTRY;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u4OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u4OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u4OffSet, u4Status);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u4OffSet, wUpdateL3Filter.i4IssL3FilterNo);
        for (u4Count = ISS_ZERO_ENTRY; u4Count < ISS_FILTER_SHADOW_MEM_SIZE;
             u4Count++)
        {
            NPSYNC_RM_PUT_4_BYTE (pMsg, &u4OffSet,
                                  wUpdateL3Filter.IssFilterShadowTable.
                                  au4HwHandle[u4Count]);
        }
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u4OffSet, wUpdateL3Filter.i4Value);

        if (AclPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet, u4AppId,
                                 u4AppId) == OSIX_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            AclPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
#else
    UNUSED_PARAM (u2MsgSize);
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : NpSyncIssHwSetRateLimitingValueSync             */
/*                                                                           */
/* Description        : This function forms the the NP Sync message and      */
/*            post the message to RM Module.                  */
/*                                                                           */
/* Input(s)           : Union to the NP Sync Entry, NPAPI ID, Application ID */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
VOID
NpSyncIssHwSetRateLimitingValueSync (tNpSyncIssHwSetRateLimitingValue
                                     wSetRateLimitingValue,
                                     UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = ISS_ZERO_ENTRY;
    UINT2               u2MsgSize = ISS_ZERO_ENTRY;

    MEMSET (&ProtoEvt, ISS_ZERO_ENTRY, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH +
        NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (UINT4) + sizeof (UINT1) +
        sizeof (INT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        AclPortRmApiHandleProtocolEvent (&ProtoEvt);
        UNUSED_PARAM (u4OffSet);
        UNUSED_PARAM (u4NpApiId);
        UNUSED_PARAM (wSetRateLimitingValue);
    }
#ifdef L2RED_WANTED
    else
    {
        u4OffSet = ISS_ZERO_ENTRY;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u4OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u4OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u4OffSet, wSetRateLimitingValue.u4IfIndex);
        NPSYNC_RM_PUT_1_BYTE (pMsg, &u4OffSet,
                              wSetRateLimitingValue.u1PacketType);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u4OffSet,
                              wSetRateLimitingValue.i4RateLimitVal);
        if (AclPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet, u4AppId,
                                 u4AppId) == OSIX_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            AclPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
#else
    UNUSED_PARAM (u2MsgSize);
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : NpSyncIssHwSetPortEgressPktRateSync             */
/*                                                                           */
/* Description        : This function forms the the NP Sync message and      */
/*            post the message to RM Module.                  */
/*                                                                           */
/* Input(s)           : Union to the NP Sync Entry, NPAPI ID, Application ID */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
VOID
NpSyncIssHwSetPortEgressPktRateSync (tNpSyncIssHwSetPortEgressPktRate
                                     wSetPortEgressPktRate,
                                     UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = ISS_ZERO_ENTRY;
    UINT2               u2MsgSize = ISS_ZERO_ENTRY;

    MEMSET (&ProtoEvt, ISS_ZERO_ENTRY, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH +
        NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (UINT4) + sizeof (INT4) +
        sizeof (INT4);

    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        AclPortRmApiHandleProtocolEvent (&ProtoEvt);
        UNUSED_PARAM (u4OffSet);
        UNUSED_PARAM (wSetPortEgressPktRate);
        UNUSED_PARAM (u4NpApiId);
    }
#ifdef L2RED_WANTED
    else
    {
        u4OffSet = ISS_ZERO_ENTRY;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u4OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u4OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u4OffSet, wSetPortEgressPktRate.u4IfIndex);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u4OffSet, wSetPortEgressPktRate.i4PktRate);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u4OffSet,
                              wSetPortEgressPktRate.i4BurstRate);

        if (AclPortRmEnqMsgToRm (pMsg, (UINT2) u4OffSet, u4AppId,
                                 u4AppId) == OSIX_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            AclPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
#else
    UNUSED_PARAM (u2MsgSize);
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : AclProcessNpSyncMsg                     */
/*                                                                           */
/* Description        : This function process the NP Sync message sent by the*/
/*            Peer node                          */
/*                                                                           */
/* Input(s)           : Pointer to RM message and Offset             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
VOID
AclProcessNpSyncMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    INT4                i4NpApiId = ISS_ZERO_ENTRY;
    UINT4               u4Status = ISS_ZERO_ENTRY;
    UINT4               u4Count = ISS_ZERO_ENTRY;
    unNpSync            NpSync;
    UINT4               u4NpApiId = (UINT4) i4NpApiId;
    UINT4               u4IssL2FilterNo;
    UINT4               u4IssL3FilterNo;
    UINT4               u4Value;
    UINT4               u4RateLimitVal;
    UINT4               u4BurstRate;
    UINT4               u4PktRate;
    UINT4               u4TempOffSet = (UINT4) *pu2OffSet;

    MEMSET (&NpSync, ISS_ZERO_ENTRY, sizeof (unNpSync));
    NPSYNC_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4NpApiId);
    i4NpApiId = (INT4) u4NpApiId;
    switch (i4NpApiId)
    {
        case NPSYNC_ISS_HW_UPDATE_L2_FILTER:
            NPSYNC_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4Status);
            u4IssL2FilterNo =
                (UINT4) NpSync.IssHwUpdateL2Filter.i4IssL2FilterNo;
            NPSYNC_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4IssL2FilterNo);
            NpSync.IssHwUpdateL2Filter.i4IssL2FilterNo = (INT4) u4IssL2FilterNo;
            for (u4Count = ISS_ZERO_ENTRY; u4Count < ISS_FILTER_SHADOW_MEM_SIZE;
                 u4Count++)
            {
                NPSYNC_RM_GET_4_BYTE (pMsg, &u4TempOffSet,
                                      NpSync.IssHwUpdateL2Filter.
                                      IssFilterShadowTable.
                                      au4HwHandle[u4Count]);
            }
            u4Value = (UINT4) NpSync.IssHwUpdateL2Filter.i4Value;
            NPSYNC_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4Value);
            NpSync.IssHwUpdateL2Filter.i4Value = (INT4) u4Value;
            AclRedUpdateNpSyncBufferTable
                (&NpSync, NPSYNC_ISS_HW_UPDATE_L2_FILTER, u4Status);
            break;

        case NPSYNC_ISS_HW_UPDATE_L3_FILTER:
            NPSYNC_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4Status);
            u4IssL3FilterNo =
                (UINT4) NpSync.IssHwUpdateL3Filter.i4IssL3FilterNo;
            NPSYNC_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4IssL3FilterNo);
            NpSync.IssHwUpdateL3Filter.i4IssL3FilterNo = (INT4) u4IssL3FilterNo;
            for (u4Count = ISS_ZERO_ENTRY; u4Count < ISS_FILTER_SHADOW_MEM_SIZE;
                 u4Count++)
            {
                NPSYNC_RM_GET_4_BYTE (pMsg, &u4TempOffSet,
                                      NpSync.IssHwUpdateL3Filter.
                                      IssFilterShadowTable.
                                      au4HwHandle[u4Count]);
            }
            u4Value = (UINT4) NpSync.IssHwUpdateL3Filter.i4Value;
            NPSYNC_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4Value);
            NpSync.IssHwUpdateL3Filter.i4Value = (INT4) u4Value;
            AclRedUpdateNpSyncBufferTable
                (&NpSync, NPSYNC_ISS_HW_UPDATE_L3_FILTER, u4Status);
            break;

        case NPSYNC_ISS_HW_SET_RATE_LIMITING_VALUE:
            NPSYNC_RM_GET_4_BYTE (pMsg, &u4TempOffSet,
                                  NpSync.IssHwSetRateLimitingValue.u4IfIndex);
            NPSYNC_RM_GET_1_BYTE (pMsg, &u4TempOffSet,
                                  NpSync.IssHwSetRateLimitingValue.
                                  u1PacketType);
            u4RateLimitVal =
                (UINT4) NpSync.IssHwSetRateLimitingValue.i4RateLimitVal;
            NPSYNC_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4RateLimitVal);
            NpSync.IssHwSetRateLimitingValue.i4RateLimitVal =
                (INT4) u4RateLimitVal;
            AclRedUpdateNpSyncBufferTable (&NpSync,
                                           NPSYNC_ISS_HW_SET_RATE_LIMITING_VALUE,
                                           ISS_ZERO_ENTRY);
            break;

        case NPSYNC_ISS_HW_SET_PORT_EGRESS_PKT_RATE:
            NPSYNC_RM_GET_4_BYTE (pMsg, &u4TempOffSet,
                                  NpSync.IssHwSetPortEgressPktRate.u4IfIndex);
            u4PktRate = (UINT4) NpSync.IssHwSetPortEgressPktRate.i4PktRate;
            NPSYNC_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4PktRate);
            NpSync.IssHwSetPortEgressPktRate.i4PktRate = (INT4) u4PktRate;
            u4BurstRate = (UINT4) NpSync.IssHwSetPortEgressPktRate.i4BurstRate;
            NPSYNC_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4BurstRate);
            NpSync.IssHwSetPortEgressPktRate.i4BurstRate = (INT4) u4BurstRate;
            AclRedUpdateNpSyncBufferTable
                (&NpSync, NPSYNC_ISS_HW_SET_PORT_EGRESS_PKT_RATE,
                 ISS_ZERO_ENTRY);
            break;

        default:
            break;
    }                            /* switch */
    *pu2OffSet = (UINT2) u4TempOffSet;

}
