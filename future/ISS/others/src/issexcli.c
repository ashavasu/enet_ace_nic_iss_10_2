/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: issexcli.c,v 1.12 2014/07/13 11:38:47 siva Exp $
 *
 * Description: This file implements the CLI routines for the ACL commands
 ****************************************************************************/

#ifndef  __ISSEXCLI_C__
#define  __ISSEXCLI_C__

#include "lr.h"
#include "fssnmp.h"
#include "iss.h"
#include "msr.h"
#include "issexinc.h"
#include "fsisselw.h"
#include "isscli.h"

/***************************************************************/
/*  Function Name   : cli_process_iss_ext_cmd                  */
/*  Description     : This function servers as the handler for */
/*                    extended system CLI commands             */
/*  Input(s)        :                                          */
/*                    CliHandle - CLI Handle                   */
/*                    u4Command - Command given by user        */
/*                    Variable set of inputs depending on      */
/*                    the user command                         */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : None                                     */
/***************************************************************/

VOID
cli_process_iss_ext_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[CLI_ISS_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4Inst;
    UINT4               u4IfIndex;
    INT4                i4RetStatus = CLI_SUCCESS;
    INT4                i4RateLimit = ISS_CLI_RATE_LIMIT_INVALID;
    INT4                i4BurstSize = ISS_CLI_RATE_LIMIT_INVALID;

    va_start (ap, u4Command);

    /* Third arguement is always interface name/index */
    i4Inst = va_arg (ap, INT4);
    UNUSED_PARAM (i4Inst);

    /* Walk through the rest of the arguements and store in args array.  */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == CLI_ISS_MAX_ARGS)
            break;
    }
    va_end (ap);

    CLI_SET_ERR (0);

    CliRegisterLock (CliHandle, IssLock, IssUnLock);

    ISS_LOCK ();

    switch (u4Command)
    {
        case CLI_ISS_STORM_CONTROL:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            CliSetIssStormControl (CliHandle, u4IfIndex,
                                   CLI_PTR_TO_U4 (args[0]), *args[1]);
            break;

        case CLI_ISS_NO_STORM_CONTROL:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            CliSetIssStormControl (CliHandle, u4IfIndex,
                                   CLI_PTR_TO_U4 (args[0]), 0);
            break;

        case CLI_ISS_PORT_RATE_LIMIT:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            if ((args[0]) != NULL)
            {
                i4RateLimit = *(INT4 *) args[0];

            }
            if ((args[1]) != NULL)
            {
                i4BurstSize = *(INT4 *) args[1];
            }

            i4RetStatus = CliSetIssConfPortRateLimit (CliHandle, u4IfIndex,
                                                      i4RateLimit, i4BurstSize);
            break;

        case CLI_ISS_NO_PORT_RATE_LIMIT:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            if ((args[0]) != NULL)
            {
                i4RateLimit = ISS_ZERO_ENTRY;
            }
            if ((args[1]) != NULL)
            {
                i4BurstSize = ISS_ZERO_ENTRY;
            }

            i4RetStatus = CliSetIssConfPortRateLimit (CliHandle, u4IfIndex,
                                                      i4RateLimit, i4BurstSize);
            break;
        default:
            break;

    }
    if ((CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode >= CLI_ERR_START_ID_ISS) &&
            (u4ErrCode < CLI_ISS_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s",
                       IssCliErrString[CLI_ERR_OFFSET_ISS (u4ErrCode)]);
        }

        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    CliUnRegisterLock (CliHandle);

    ISS_UNLOCK ();
}

/******************************************************************/
/*  Function Name   : CliSetIssStormControl                       */
/*  Description     : This function is used to set the            */
/*                    strom control limit                         */
/*  Input(s)        :                                             */
/*                    CliHandle   - CLI Handle                    */
/*                    u4IfIndex   - Port on which Storm control   */
/*                                  is applied.                   */
/*                    u4Mode      - Mode (Bcast/Mcast/DLF)        */
/*                    u4Rate      - Rate value                    */
/*  Output(s)       : None                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                     */
/******************************************************************/

INT4
CliSetIssStormControl (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4Mode,
                       UINT4 u4Rate)
{
    UINT4               u4ErrorCode;
    INT4                i4RetVal = CLI_SUCCESS;

    switch (u4Mode)
    {
        case ISS_RATE_DLF_LIMIT:
            if (nmhTestv2IssExtRateCtrlDLFLimitValue (&u4ErrorCode,
                                                      (INT4) u4IfIndex,
                                                      (INT4) u4Rate) ==
                SNMP_FAILURE)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }

            if (nmhSetIssExtRateCtrlDLFLimitValue
                ((INT4) u4IfIndex, (INT4) u4Rate) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                i4RetVal = CLI_FAILURE;
                break;
            }

            break;

        case ISS_RATE_BCAST_LIMIT:
            if (nmhTestv2IssExtRateCtrlBCASTLimitValue (&u4ErrorCode,
                                                        (INT4) u4IfIndex,
                                                        (INT4) u4Rate) ==
                SNMP_FAILURE)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }

            if (nmhSetIssExtRateCtrlBCASTLimitValue
                ((INT4) u4IfIndex, (INT4) u4Rate) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                i4RetVal = CLI_FAILURE;
                break;
            }
            break;

        case ISS_RATE_MCAST_LIMIT:
            if (nmhTestv2IssExtRateCtrlMCASTLimitValue (&u4ErrorCode,
                                                        (INT4) u4IfIndex,
                                                        (INT4) u4Rate) ==
                SNMP_FAILURE)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }

            if (nmhSetIssExtRateCtrlMCASTLimitValue
                ((INT4) u4IfIndex, (INT4) u4Rate) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                i4RetVal = CLI_FAILURE;
                break;
            }
            break;

        default:
            i4RetVal = CLI_FAILURE;
            break;
    }

    return i4RetVal;
}

/***************************************************************/
/*  Function Name   : CliSetIssConfPortRateLimit               */
/*  Description     : This function is used to configure rate  */
/*                    limit for a port                         */
/*  Input(s)        : CliHandle        -  CLI Handle           */
/*                    u4IfIndex        -  Interface Index      */
/*                    i4PortRateLimit  -  Port rate limit      */
/*                    i4PortBurstSize  -  Burst size limit     */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
INT4
CliSetIssConfPortRateLimit (tCliHandle CliHandle, UINT4 u4IfIndex,
                            INT4 i4PortRateLimit, INT4 i4PortBurstSize)
{
    UINT4               u4ErrorCode;

    if (i4PortRateLimit != ISS_CLI_RATE_LIMIT_INVALID)
    {
        if (nmhTestv2IssExtRateCtrlPortRateLimit
            (&u4ErrorCode, (INT4) u4IfIndex, i4PortRateLimit) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetIssExtRateCtrlPortRateLimit
            ((INT4) u4IfIndex, i4PortRateLimit) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (i4PortBurstSize != ISS_CLI_RATE_LIMIT_INVALID)
    {
        if (nmhTestv2IssExtRateCtrlPortBurstSize
            (&u4ErrorCode, (INT4) u4IfIndex, i4PortBurstSize) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetIssExtRateCtrlPortBurstSize
            ((INT4) u4IfIndex, i4PortBurstSize) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AclCliGetShowCmdOutputToFile                         */
/*                                                                           */
/* Description        : This function handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1FileName - The output of the show cmd is          */
/*                      redirected to this file                              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/
INT4
AclCliGetShowCmdOutputToFile (UINT1 *pu1FileName)
{
    if (FileStat ((const CHR1 *) pu1FileName) == OSIX_SUCCESS)
    {
        if (0 != FileDelete (pu1FileName))
        {
            return ISS_FAILURE;
        }
    }
    if (CliGetShowCmdOutputToFile
        ((UINT1 *) pu1FileName, (UINT1 *) ACL_AUDIT_SHOW_CMD) == CLI_FAILURE)
    {
        return ISS_FAILURE;
    }
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AclCliCalcSwAudCheckSum                              */
/*                                                                           */
/* Description        : This function handles the Calculation of checksum    */
/*                      for the data in the given input file.                */
/*                                                                           */
/* Input(s)           : pu1FileName - The checksum is calculated for the data*/
/*                      in this file                                         */
/*                                                                           */
/* Output(s)          : pu2ChkSum - The calculated checksum is stored here   */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/
INT4
AclCliCalcSwAudCheckSum (UINT1 *pu1FileName, UINT2 *pu2ChkSum)
{
    INT1                ai1Buf[ISS_CLI_MAX_GROUPS_LINE_LEN + 1];
    INT4                i4Fd;
    INT2                i2ReadLen;
    UINT4               u4Sum = 0;
    UINT4               u4Last = 0;
    UINT2               u2CkSum = 0;

    i4Fd = FileOpen ((CONST UINT1 *) pu1FileName, ISS_CLI_RDONLY);
    if (i4Fd == -1)
    {
        return ISS_FAILURE;
    }
    MEMSET (ai1Buf, 0, ISS_CLI_MAX_GROUPS_LINE_LEN + 1);
    while (AclCliReadLineFromFile (i4Fd, ai1Buf, ISS_CLI_MAX_GROUPS_LINE_LEN,
                                   &i2ReadLen) != ISS_CLI_EOF)

    {
        if (i2ReadLen > 0)
        {
            UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                              &u2CkSum, (UINT4) i2ReadLen, CKSUM_DATA);
            MEMSET (ai1Buf, '\0', ISS_CLI_MAX_GROUPS_LINE_LEN + 1);
        }
    }
    /*CheckSum for last line */
    if ((u4Sum != 0) || (i2ReadLen != 0))
    {
        UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                          &u2CkSum, (UINT4) i2ReadLen, CKSUM_LASTDATA);

    }
    *pu2ChkSum = u2CkSum;
    if (FileClose (i4Fd) < 0)
    {
        return ISS_FAILURE;
    }
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AclCliReadLineFromFile                               */
/*                                                                           */
/* Description        : It is a utility to read a line from the given file   */
/*                      descriptor.                                          */
/*                                                                           */
/* Input(s)           : i4Fd - File Descriptor for the file                  */
/*                      i2MaxLen - Maximum length that can be read and store */
/*                                                                           */
/* Output(s)          : pi1Buf - Buffer to store the line read from the file */
/*                      pi2ReadLen - the total length of the data read       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/
INT1
AclCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen,
                        INT2 *pi2ReadLen)
{
    INT1                i1Char;

    *pi2ReadLen = 0;

    while (FileRead (i4Fd, (CHR1 *) & i1Char, 1) == 1)
    {
        pi1Buf[*pi2ReadLen] = i1Char;
        (*pi2ReadLen)++;
        if (i1Char == '\n')
        {
            return (ISS_CLI_NO_EOF);
        }
        if (*pi2ReadLen == i2MaxLen)
        {
            *pi2ReadLen = 0;
            break;
        }
    }
    pi1Buf[*pi2ReadLen] = '\0';
    return (ISS_CLI_EOF);
}
#endif /* __ACLEXCLI_C__ */
