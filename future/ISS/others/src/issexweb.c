/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: issexweb.c,v 1.9 2017/12/27 11:02:22 siva Exp $
 *
 *******************************************************************/

#ifdef WEBNM_WANTED
#include "issexweb.h"
#include "fsissecli.h"
#include "fsissewr.h"
#include "snmputil.h"

#ifdef WLC_WANTED
extern UINT4        WebnmGetWebAuthClientInfo (INT4 i4Sockid);
extern INT1
        ArpResolve (UINT4 u4IpAddr, INT1 *pi1Hw_addr, UINT1 *pu1EncapType);
#endif

/*********************************************************************
*  Function Name : IssProcessCustomPages
*  Description   : This function processes the Pages specific to
*                  target
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
INT4
IssProcessCustomPages (tHttp * pHttp)
{
    UINT4               u4Count;
#ifdef WLC_WANTED
    UINT4               u4CliAddr = 0;
    UINT1               u1EncapType = 0;
    UINT1               au1DstMac[MAC_ADDR_LEN];
#endif

    for (u4Count = 0; asIssTargetpages[u4Count].au1Page[0] != '\0'; u4Count++)
    {
        if (STRCMP (pHttp->ai1HtmlName, asIssTargetpages[u4Count].au1Page) == 0)
        {
            asIssTargetpages[u4Count].pfunctPtr (pHttp);
            return ISS_SUCCESS;
        }
    }
#ifdef WLC_WANTED
    /* The below code is added for customizable web login portal. */
    MEMSET (au1DstMac, 0, MAC_ADDR_LEN);
    u4CliAddr = WebnmGetWebAuthClientInfo (pHttp->i4Sockid);
    /* Get the MAC address of the Client using ARP resolve */
    if (ArpResolve (u4CliAddr, (INT1 *) au1DstMac, &u1EncapType) ==
        OSIX_SUCCESS)
    {
        return ISS_SUCCESS;
    }
#endif
    return ISS_FAILURE;
}

/************************************************************************
*  Function Name   : IssRedirectMacFilterPage
*  Description     : This function will redirect Mac Filter page
*  Input           : pHttp - Pointer to http entry where the html file
*                    name needs to be changed(copied)
*  Output          : None
*  Returns         : None
************************************************************************/
VOID
IssRedirectMacFilterPage (tHttp * pHttp)
{
    STRCPY (pHttp->ai1HtmlName, "others_mac_filterconf.html");
}

/*********************************************************************
*  Function Name : IssOthersProcessMACFilterConfPage
*  Description   : This function processes the request coming for the MAC
*                  Filter Configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssOthersProcessMACFilterConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssOthersProcessMACFilterConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssOthersProcessMACFilterConfPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssOthersProcessIPFilterConfPage
*  Description   : This function processes the request coming for the IP
*                  Filter Configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssOthersProcessIPFilterConfPage (tHttp * pHttp)
{

    if (pHttp->i4Method == ISS_GET)
    {
        IssOthersProcessIPFilterConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssOthersProcessIPFilterConfPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssOthersProcessIPStdFilterConfPage
*  Description   : This function processes the request coming for the IP
*                  standard Filter Configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssOthersProcessIPStdFilterConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssOthersProcessIPStdFilterConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssOthersProcessIPStdFilterConfPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssOthersProcessIPFilterConfPageSet
*  Description   : This function processes the set request for the IP
*                 filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssOthersProcessIPFilterConfPageSet (tHttp * pHttp)
{
    INT4                i4AckBit = 0, i4RstBit = 0, i4TOS = 0;
    UINT4               u4SrcIpAddr = 0, u4SrcAddrMask = 0, u4DestIpAddr =
        0, u4DestAddrMask = 0;
    UINT4               u4SrcPortFrom = 0, u4SrcPortTo = 0, u4DstPortFrom = 0;
    UINT4               u4DstPortTo = 0, u4Priority = 0, u4Code = 0;
    UINT4               u4ErrorCode = 0, u4Protocol = 0, u4Type = 0;
    INT4                i4Action = 0, i4FilterNo = 0;
    INT4                i4Dscp = 0;
    INT4                i4AddressType = 0;
    UINT4               u4DstPrefix = 0;
    UINT4               u4SrcPrefix = 0;
    UINT4               u4FlowId = 0;
#ifdef QOSX_WANTED
    INT4                i4Storage = 0;
#endif
    tIp6Addr            SrcIp6Addr;
    tIp6Addr            DstIp6Addr;
    tSNMP_OCTET_STRING_TYPE SrcIpAddrOctet;
    tSNMP_OCTET_STRING_TYPE DstIpAddrOctet;
    tSNMP_OCTET_STRING_TYPE SrcIpv6;
    tSNMP_OCTET_STRING_TYPE DstIpv6;
    UINT4               au4SrcTemp[4];
    UINT4               au4DstTemp[4];

    tSNMP_OCTET_STRING_TYPE InPortList, OutPortList;
    UINT1               u1InPortList[ISS_PORTLIST_LEN],
        u1OutPortList[ISS_PORTLIST_LEN];
    UINT1               u1Apply = 0, u1RowStatus = ISS_CREATE_AND_WAIT;
    UINT1               au1SrcIpv6Temp[16] = { 0 };
    UINT1               au1DstIpv6Temp[16] = { 0 };
    UINT1               au1InPortList[ISS_PORTLIST_LEN];
    UINT1               au1OutPortList[ISS_PORTLIST_LEN];

    SrcIpAddrOctet.pu1_OctetList = (UINT1 *) au4SrcTemp;
    DstIpAddrOctet.pu1_OctetList = (UINT1 *) au4DstTemp;
    SrcIpv6.pu1_OctetList = au1SrcIpv6Temp;
    DstIpv6.pu1_OctetList = au1DstIpv6Temp;
    UNUSED_PARAM (DstIpAddrOctet);
    UNUSED_PARAM (SrcIpAddrOctet);

    MEMSET (&SrcIp6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&DstIp6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&InPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&OutPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    InPortList.pu1_OctetList = &au1InPortList[0];
    OutPortList.pu1_OctetList = &au1OutPortList[0];

    HttpGetValuebyName ((UINT1 *) "FltNo", pHttp->au1Value,
                        pHttp->au1PostQuery);
    i4FilterNo = ATOI (pHttp->au1Value);

    /* Check whether the request is for ADD/DELETE */
    HttpGetValuebyName (ISS_ACTION, pHttp->au1Value, pHttp->au1PostQuery);

    ISS_LOCK ();
    /* check for delete */
    if (STRCMP (pHttp->au1Value, ISS_DELETE) == 0)
    {
        if (nmhTestv2IssExtL3FilterStatus
            (&u4ErrorCode, i4FilterNo, ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "IP filter not available.");
            return;
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "IP filter not available.");
            return;
        }

        ISS_UNLOCK ();
        IssOthersProcessIPFilterConfPageGet (pHttp);
        return;
    }

    if (STRCMP (pHttp->au1Value, ISS_APPLY) == 0)
    {
        u1Apply = 1;
        u1RowStatus = ISS_NOT_IN_SERVICE;
    }

    HttpGetValuebyName ((UINT1 *) "FltOption", pHttp->au1Value,
                        pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    HttpGetValuebyName ((UINT1 *) "AddressType", pHttp->au1Value,
                        pHttp->au1PostQuery);
    i4AddressType = ATOI (pHttp->au1Value);

    if (i4AddressType == 1)
    {
        HttpGetValuebyName ((UINT1 *) "SrcIp", pHttp->au1Value,
                            pHttp->au1PostQuery);
        if (STRLEN (pHttp->au1Value) != 0)
        {
            u4SrcIpAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
        }

        HttpGetValuebyName ((UINT1 *) "SrcMask", pHttp->au1Value,
                            pHttp->au1PostQuery);
        if (STRLEN (pHttp->au1Value) != 0)
        {
            u4SrcAddrMask = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
        }
        HttpGetValuebyName ((UINT1 *) "DestIp", pHttp->au1Value,
                            pHttp->au1PostQuery);
        if (STRLEN (pHttp->au1Value) != 0)
        {
            u4DestIpAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
        }

        HttpGetValuebyName ((UINT1 *) "DestMask", pHttp->au1Value,
                            pHttp->au1PostQuery);
        if (STRLEN (pHttp->au1Value) != 0)
        {
            u4DestAddrMask = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
        }
    }

    HttpGetValuebyName ((UINT1 *) "InPrtList", pHttp->au1Value,
                        pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        MEMSET (u1InPortList, 0, ISS_PORTLIST_LEN);
        issDecodeSpecialChar (pHttp->au1Value);
        if (STRCMP (pHttp->au1Value, "0") != 0)
        {
            if (IssPortListStringParser (pHttp->au1Value, u1InPortList) ==
                ENM_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp, (INT1 *) "Check the allowed in port list");
                return;
            }
        }
        InPortList.i4_Length = ISS_PORTLIST_LEN;
        MEMCPY (InPortList.pu1_OctetList, u1InPortList, InPortList.i4_Length);
    }

    HttpGetValuebyName ((UINT1 *) "OutPrtList", pHttp->au1Value,
                        pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        MEMSET (u1OutPortList, 0, ISS_PORTLIST_LEN);
        issDecodeSpecialChar (pHttp->au1Value);

        if (STRCMP (pHttp->au1Value, "0") != 0)
        {
            if (IssPortListStringParser (pHttp->au1Value, u1OutPortList) ==
                ENM_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp,
                              (INT1 *) "Check the allowed out port list");
                return;
            }
        }
        OutPortList.i4_Length = ISS_PORTLIST_LEN;
        MEMCPY (OutPortList.pu1_OctetList, u1OutPortList,
                OutPortList.i4_Length);
    }

    /* Determine the type of ACL from the filter number and process the SET
     * request
     */

    HttpGetValuebyName ((UINT1 *) "Protocol", pHttp->au1Value,
                        pHttp->au1PostQuery);
    u4Protocol = (UINT4) ATOI (pHttp->au1Value);
    if (u4Protocol == ISS_PROT_OTHER)
    {
        HttpGetValuebyName ((UINT1 *) "Other", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Protocol = (UINT4) ATOI (pHttp->au1Value);
    }
    /* For Processing ICMP options. */
    if (u4Protocol == 1)
    {
        HttpGetValuebyName ((UINT1 *) "MsgCode", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Code = (UINT4) ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "MsgType", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Type = (UINT4) ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "Priority", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Priority = (UINT4) ATOI (pHttp->au1Value);
    }

    if (u4Protocol == 4)
    {
        /*We need not install the L4 protocol vbalue for IP */
        u4Protocol = 0;
    }

    else
    {
        HttpGetValuebyName ((UINT1 *) "Priority", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Priority = (UINT4) ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "Dscp", pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4Dscp = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "Tos", pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4TOS = ATOI (pHttp->au1Value);

        /* Configure either Dscp or TOS */
        if ((i4TOS > 0) && (i4Dscp > 0))
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *)
                          "Unable to configure both Dscp and TOS, configure one at a time");
            return;
        }
        else if (i4Dscp > 0)
        {
            i4TOS = ISS_TOS_INVALID;
        }
        else
        {
            i4Dscp = ISS_DSCP_INVALID;
        }
    }

    /* For Processing TCP options. */
    if (u4Protocol == 6)
    {
        HttpGetValuebyName ((UINT1 *) "AckBit", pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4AckBit = ATOI (pHttp->au1Value);
        if (i4AckBit == 0)
            i4AckBit = 3;

        HttpGetValuebyName ((UINT1 *) "RstBit", pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4RstBit = ATOI (pHttp->au1Value);
        if (i4RstBit == 0)
            i4RstBit = 3;

    }

    /* For Processing TCP/UDP options. */
    if (u4Protocol == 17 || u4Protocol == 6)
    {
        HttpGetValuebyName ((UINT1 *) "SrcPortMin", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4SrcPortFrom = (UINT4) ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "SrcPortMax", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4SrcPortTo = (UINT4) ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "DstPortMin", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4DstPortFrom = (UINT4) ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "DstPortMax", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4DstPortTo = (UINT4) ATOI (pHttp->au1Value);
    }

    if (nmhTestv2IssExtL3FilterStatus (&u4ErrorCode, i4FilterNo,
                                       u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Invalid IP filter number.");
        return;
    }
    if ((u1RowStatus == ISS_CREATE_AND_WAIT)
        || (u1RowStatus == ISS_NOT_IN_SERVICE))
    {
        if (nmhSetIssExtL3FilterStatus (i4FilterNo, u1RowStatus) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Setting Invalid IP filter number.");
            return;
        }
    }

    if (i4AddressType != 0)
    {
        if (nmhTestv2IssAclL3FilteAddrType
            (&u4ErrorCode, i4FilterNo, i4AddressType) == SNMP_FAILURE)
        {
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *)
                          "Unable to set In Address Type Test Failed .");
            return;
        }
        if (nmhSetIssAclL3FilteAddrType (i4FilterNo, i4AddressType) ==
            SNMP_FAILURE)
        {
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set In Address Type  .");
            return;
        }
    }

    if (i4AddressType == 2)
    {
        HttpGetValuebyName ((UINT1 *) "SrcIp", pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);
        INET_ATON6 (pHttp->au1Value, &SrcIp6Addr);
        MEMCPY (SrcIpv6.pu1_OctetList, &SrcIp6Addr, IP6_ADDR_SIZE);
        SrcIpv6.i4_Length = IP6_ADDR_SIZE;

        if ((nmhTestv2IssAclL3FilterSrcIpAddr
             (&u4ErrorCode, i4FilterNo, &SrcIpv6)) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *)
                          "Unable to set the filter with this source IPV6 Address.");
            return;
        }

        if ((nmhSetIssAclL3FilterSrcIpAddr
             (i4FilterNo, &SrcIpv6) == SNMP_FAILURE))
        {
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set the l3 Source IPV6 Address.");
            return;
        }

        HttpGetValuebyName ((UINT1 *) "DestIp", pHttp->au1Value,
                            pHttp->au1PostQuery);
        issDecodeSpecialChar (pHttp->au1Value);
        INET_ATON6 (pHttp->au1Value, &DstIp6Addr);
        MEMCPY (DstIpv6.pu1_OctetList, &DstIp6Addr, IP6_ADDR_SIZE);
        DstIpv6.i4_Length = IP6_ADDR_SIZE;

        if ((nmhTestv2IssAclL3FilterDstIpAddr
             (&u4ErrorCode, i4FilterNo, &DstIpv6) == SNMP_FAILURE))
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *)
                          "Unable to set the filter with this Destination IPV6 Address.");
            return;
        }

        if ((nmhSetIssAclL3FilterDstIpAddr
             (i4FilterNo, &DstIpv6) == SNMP_FAILURE))
        {
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set the l3 Source Address Mask.");
            return;
        }

    }
    if ((i4Action != 0) && (nmhTestv2IssExtL3FilterAction
                            (&u4ErrorCode, i4FilterNo,
                             i4Action)) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the l3 filter action.");
        return;
    }

    if (i4AddressType == 1)
    {
        if ((u4SrcIpAddr > 0) && (nmhTestv2IssExtL3FilterSrcIpAddr
                                  (&u4ErrorCode, i4FilterNo, u4SrcIpAddr))
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *)
                          "Unable to set the filter with this source IP Address.");
            return;
        }

        if ((u4SrcAddrMask > 0) && (nmhTestv2IssExtL3FilterSrcIpAddrMask
                                    (&u4ErrorCode, i4FilterNo, u4SrcAddrMask)
                                    == SNMP_FAILURE))
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *)
                          "Unable to set the filter with this source Address Mask.");
            return;
        }

        if ((u4DestIpAddr > 0) && (nmhTestv2IssExtL3FilterDstIpAddr
                                   (&u4ErrorCode, i4FilterNo, u4DestIpAddr)
                                   == SNMP_FAILURE))
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *)
                          "Unable to set the filter with this Destination IP Address.");
            return;
        }

        if ((u4DestAddrMask > 0) && (nmhTestv2IssExtL3FilterDstIpAddrMask
                                     (&u4ErrorCode, i4FilterNo, u4DestAddrMask)
                                     == SNMP_FAILURE))
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *)
                          "Unable to set the filter with this Destination Address Mask.");
            return;
        }
    }

    if ((u4Protocol != 0) && (nmhTestv2IssExtL3FilterProtocol
                              (&u4ErrorCode, i4FilterNo, (INT4) u4Protocol))
        == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the l3 filter protocol.");
        return;
    }
    if (u4Protocol == 1)
    {
        if (nmhTestv2IssExtL3FilterMessageType
            (&u4ErrorCode, i4FilterNo, (INT4) u4Type) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set ICMP MessageType.");
            return;
        }

        if (nmhTestv2IssExtL3FilterMessageCode
            (&u4ErrorCode, i4FilterNo, (INT4) u4Code) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set ICMP MessageCode.");
            return;
        }
    }
    else
    {
        if (i4Dscp != ISS_DSCP_INVALID)
        {
            if (nmhTestv2IssExtL3FilterDscp (&u4ErrorCode, i4FilterNo, i4Dscp)
                == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp, (INT1 *) "Unable to set Filter Dscp.");
                return;
            }
        }

        if (i4TOS != ISS_TOS_INVALID)
        {
            if (nmhTestv2IssExtL3FilterTos (&u4ErrorCode, i4FilterNo, i4TOS)
                == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp, (INT1 *) "Unable to set TCP TOS.");
                return;
            }
        }
    }

    if ((u4Priority != 0) && (nmhTestv2IssExtL3FilterPriority
                              (&u4ErrorCode, i4FilterNo, (INT4) u4Priority)
                              == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter priority.");
        return;
    }

    if (u4Protocol == 6)
    {
        if (nmhTestv2IssExtL3FilterAckBit (&u4ErrorCode, i4FilterNo, i4AckBit)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set TCP Ack Bit.");
            return;
        }

        if (nmhTestv2IssExtL3FilterRstBit (&u4ErrorCode, i4FilterNo, i4RstBit)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set TCP Rst Bit.");
            return;
        }
    }
    if (u4Protocol == 17 || u4Protocol == 6)
    {
        if ((u4SrcPortFrom != 0) || (u4SrcPortTo != 0))
        {
            if (nmhTestv2IssExtL3FilterMinSrcProtPort (&u4ErrorCode, i4FilterNo,
                                                       u4SrcPortFrom) ==
                SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp,
                              (INT1 *) "Unable to set TCP/UDP Src Port Start.");
                return;
            }
            if (nmhTestv2IssExtL3FilterMaxSrcProtPort (&u4ErrorCode, i4FilterNo,
                                                       u4SrcPortTo) ==
                SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp,
                              (INT1 *) "Unable to set TCP/UDP Src Port End.");
                return;
            }
        }

        if ((u4DstPortFrom != 0) || (u4DstPortTo != 0))
        {
            if (nmhTestv2IssExtL3FilterMinDstProtPort (&u4ErrorCode, i4FilterNo,
                                                       u4DstPortFrom) ==
                SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp,
                              (INT1 *) "Unable to set TCP/UDP Dst Port Start.");
                return;
            }
            if (nmhTestv2IssExtL3FilterMaxDstProtPort (&u4ErrorCode, i4FilterNo,
                                                       u4DstPortTo) ==
                SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp,
                              (INT1 *) "Unable to set TCP/UDP Dst Port End.");
                return;
            }
        }
    }

    if ((i4Action != 0) && (nmhSetIssExtL3FilterAction (i4FilterNo, i4Action)
                            == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the l3 filter action.");
        return;
    }
    if (i4AddressType == 1)
    {
        if ((u4SrcIpAddr > 0) && (nmhSetIssExtL3FilterSrcIpAddr
                                  (i4FilterNo, u4SrcIpAddr) == SNMP_FAILURE))
        {
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set the l3 Source Ip Address.");
            return;
        }

        if ((u4SrcAddrMask > 0) && (nmhSetIssExtL3FilterSrcIpAddrMask
                                    (i4FilterNo,
                                     u4SrcAddrMask) == SNMP_FAILURE))
        {
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set the l3 Source Address Mask.");
            return;
        }

        if ((u4DestIpAddr > 0) && (nmhSetIssExtL3FilterDstIpAddr
                                   (i4FilterNo, u4DestIpAddr) == SNMP_FAILURE))
        {
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *)
                          "Unable to set the l3 Destination Ip Address.");
            return;
        }

        if ((u4DestAddrMask > 0) && (nmhSetIssExtL3FilterDstIpAddrMask
                                     (i4FilterNo,
                                      u4DestAddrMask) == SNMP_FAILURE))
        {
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *)
                          "Unable to set the l3 Destination Address Mask.");
            return;
        }
    }

    if ((u4Protocol != 0) && (nmhSetIssExtL3FilterProtocol
                              (i4FilterNo, u4Protocol) == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter priority.");
        return;
    }
    if (u4Protocol == 1)
    {
        if (nmhSetIssExtL3FilterMessageType (i4FilterNo, u4Type) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set ICMP MessageType.");
            return;
        }

        if (nmhSetIssExtL3FilterMessageCode (i4FilterNo, u4Code) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set ICMP MessageCode.");
            return;
        }
    }
    else
    {
        if (i4Dscp != ISS_DSCP_INVALID)
        {
            if (nmhSetIssExtL3FilterDscp (i4FilterNo, i4Dscp) == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp, (INT1 *) "Unable to set Filter Dscp.");
                return;
            }
        }

        if (i4TOS != ISS_TOS_INVALID)
        {
            if (nmhSetIssExtL3FilterTos (i4FilterNo, i4TOS) == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp, (INT1 *) "Unable to set TCP TOS.");
                return;
            }
        }
    }

    if ((u4Priority != 0) && (nmhSetIssExtL3FilterPriority
                              (i4FilterNo, u4Priority) == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter priority.");
        return;
    }

    if (u4Protocol == 6)
    {
        if (nmhSetIssExtL3FilterAckBit (i4FilterNo, i4AckBit) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set TCP Ack Bit.");
            return;
        }
        if (nmhSetIssExtL3FilterRstBit (i4FilterNo, i4RstBit) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set TCP Rst Bit.");
            return;
        }
    }
    if (u4Protocol == 17 || u4Protocol == 6)
    {
        if ((u4SrcPortFrom != 0) || (u4SrcPortTo != 0))
        {
            if (nmhSetIssExtL3FilterMinSrcProtPort (i4FilterNo, u4SrcPortFrom)
                == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp,
                              (INT1 *) "Unable to set TCP/UDP Src Port Start.");
                return;
            }

            if (nmhSetIssExtL3FilterMaxSrcProtPort (i4FilterNo, u4SrcPortTo)
                == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp,
                              (INT1 *) "Unable to set TCP/UDP Src Port End.");
                return;
            }
        }

        if ((u4DstPortFrom != 0) || (u4DstPortTo != 0))
        {
            if (nmhSetIssExtL3FilterMinDstProtPort (i4FilterNo, u4DstPortFrom)
                == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp,
                              (INT1 *) "Unable to set TCP/UDP Dst Port Start.");
                return;
            }

            if (nmhSetIssExtL3FilterMaxDstProtPort (i4FilterNo, u4DstPortTo)
                == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp,
                              (INT1 *) "Unable to set TCP/UDP Dst Port End.");
                return;
            }
        }
    }

    if (i4AddressType == 2)
    {
        HttpGetValuebyName ((UINT1 *) "DestMask", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4DstPrefix = (UINT4) ATOI (pHttp->au1Value);
        if (nmhTestv2IssAclL3FilterDstIpAddrPrefixLength
            (&u4ErrorCode, i4FilterNo, u4DstPrefix) == SNMP_FAILURE)
        {
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *)
                          "Unable to set In Dst Prefix Length Test Failed.");
            return;
        }

        if ((u4DstPrefix != 0)
            &&
            (nmhSetIssAclL3FilterDstIpAddrPrefixLength (i4FilterNo, u4DstPrefix)
             == SNMP_FAILURE))
        {
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set In Dst Prefix Length .");
            return;
        }

        HttpGetValuebyName ((UINT1 *) "SrcMask", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4SrcPrefix = (UINT4) ATOI (pHttp->au1Value);
        if (nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength
            (&u4ErrorCode, i4FilterNo, u4SrcPrefix) == SNMP_FAILURE)
        {
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *)
                          "Unable to set In Src Prefix Length Test Failed .");
            return;
        }
        if ((u4SrcPrefix != 0) && (nmhSetIssAclL3FilterSrcIpAddrPrefixLength
                                   (i4FilterNo, u4SrcPrefix) == SNMP_FAILURE))
        {
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set In Src Prefix Length.");
            return;
        }
    }

    if (i4AddressType == 2)
    {
        HttpGetValuebyName ((UINT1 *) "FlowId", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4FlowId = (UINT4) ATOI (pHttp->au1Value);

        if (nmhTestv2IssAclL3FilterFlowId
            (&u4ErrorCode, i4FilterNo, u4FlowId) == SNMP_FAILURE)
        {
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set In Flow Id  Test Failed .");
            return;
        }

        if (nmhSetIssAclL3FilterFlowId (i4FilterNo, u4FlowId) == SNMP_FAILURE)
        {
            if (u1Apply != 1)
            {
                nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set In Flow Id.");
            return;
        }
    }
#ifdef QOSX_WANTED
    HttpGetValuebyName ((UINT1 *) "Storage", pHttp->au1Value,
                        pHttp->au1PostQuery);
    i4Storage = ATOI (pHttp->au1Value);
    if (nmhTestv2DiffServMultiFieldClfrStorage
        (&u4ErrorCode, (UINT4) i4FilterNo, i4Storage) == SNMP_FAILURE)
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set In Storage.");
        return;
    }

    if (nmhSetDiffServMultiFieldClfrStorage ((UINT4) i4FilterNo, i4Storage) ==
        SNMP_FAILURE)
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set In Storage.");
        return;
    }
#endif
    if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        IssSendError (pHttp, (INT1 *) "Unable to set Filter Status");
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        return;
    }

    if ((InPortList.pu1_OctetList != NULL) && (nmhTestv2IssExtL3FilterInPortList
                                               (&u4ErrorCode, i4FilterNo,
                                                &InPortList) == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set this in port list.");
        return;
    }

    if ((OutPortList.pu1_OctetList != NULL)
        &&
        (nmhTestv2IssExtL3FilterOutPortList
         (&u4ErrorCode, i4FilterNo, &OutPortList) == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set this Out port list.");
        return;
    }

    if ((InPortList.pu1_OctetList != NULL) && (nmhSetIssExtL3FilterInPortList
                                               (i4FilterNo, &InPortList)
                                               == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set In Port List.");
        return;
    }

    if ((OutPortList.pu1_OctetList != NULL) && (nmhSetIssExtL3FilterOutPortList
                                                (i4FilterNo, &OutPortList)
                                                == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set Out Port List.");
        return;
    }
    ISS_UNLOCK ();

    IssOthersProcessIPFilterConfPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssOthersProcessIPFilterConfPageGet
*  Description   : This function processes the get request for the IP
*                  filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssOthersProcessIPFilterConfPageGet (tHttp * pHttp)
{
    INT4                i4FilterNo, i4CurrentFilterNo, i4RetVal, i4OutCome;
    INT4                i4Protocol;
    UINT4               u4RetVal, u4Temp;
    tSNMP_OCTET_STRING_TYPE *InPortList = NULL, *OutPortList = NULL;
    UINT1              *pu1String;
    INT4                i4AddressType = 0;
    UINT4               u4DstPrefix = 0;
    UINT4               u4SrcPrefix = 0;
    UINT4               u4FlowId = 0;
#ifdef QOSX_WANTED
    INT4                i4Storage = 0;
#endif
    UINT4               au4SrcTemp[4];
    UINT4               au4DstTemp[4];

    UINT1               au1String[ISS_MAX_LEN];
    tIp6Addr            SrcIp6Addr;
    tIp6Addr            DstIp6Addr;
    tSNMP_OCTET_STRING_TYPE SrcIpAddrOctet;
    tSNMP_OCTET_STRING_TYPE DstIpAddrOctet;
    tSNMP_OCTET_STRING_TYPE SrcIpv6;
    tSNMP_OCTET_STRING_TYPE DstIpv6;

    pu1String = &au1String[0];
    SrcIpAddrOctet.pu1_OctetList = (UINT1 *) au4SrcTemp;
    DstIpAddrOctet.pu1_OctetList = (UINT1 *) au4DstTemp;
    UNUSED_PARAM (DstIpv6);
    UNUSED_PARAM (SrcIpv6);

    MEMSET (&SrcIp6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&DstIp6Addr, 0, sizeof (tIp6Addr));
    MEMSET (au1String, 0, ISS_ADDR_LEN);
    tPortList          *pInTempPortList = NULL;
    tPortList          *pOutTempPortList = NULL;
    tSNMP_MULTI_DATA_TYPE *pu1DataString = NULL;
    pInTempPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pInTempPortList == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }
    MEMSET (*pInTempPortList, 0, sizeof (tPortList));
    pOutTempPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pOutTempPortList == NULL)
    {
        FsUtilReleaseBitList ((UINT1 *) pInTempPortList);
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }
    MEMSET (*pOutTempPortList, 0, sizeof (tPortList));

    if ((pu1DataString = WebnmAllocMultiData ()) == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        FsUtilReleaseBitList ((UINT1 *) pInTempPortList);
        FsUtilReleaseBitList ((UINT1 *) pOutTempPortList);
        return;
    }

    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    WebnmRegisterLock (pHttp, IssLock, IssUnLock);

    ISS_LOCK ();

    i4OutCome = (INT4) (nmhGetFirstIndexIssExtL3FilterTable (&i4FilterNo));
    if (i4OutCome == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        FsUtilReleaseBitList ((UINT1 *) pInTempPortList);
        FsUtilReleaseBitList ((UINT1 *) pOutTempPortList);
        return;
    }

    InPortList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);

    if (InPortList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        FsUtilReleaseBitList ((UINT1 *) pInTempPortList);
        FsUtilReleaseBitList ((UINT1 *) pOutTempPortList);
        return;
    }

    OutPortList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);
    if (OutPortList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (InPortList);
        FsUtilReleaseBitList ((UINT1 *) pInTempPortList);
        FsUtilReleaseBitList ((UINT1 *) pOutTempPortList);
        return;
    }

    do
    {
        pHttp->i4Write = (INT4) u4Temp;

        /* Display only extended lists */
        if (i4FilterNo >= ACL_EXTENDED_START)
        {
            STRCPY (pHttp->au1KeyString, "FILTER_NUMBER");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4FilterNo);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            STRCPY (pHttp->au1KeyString, "ACTION");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssExtL3FilterAction (i4FilterNo, &i4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetIssAclL3FilteAddrType (i4FilterNo, &i4AddressType);
            STRCPY (pHttp->au1KeyString, "ADDRESS_TYPE");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssAclL3FilteAddrType (i4FilterNo, &i4AddressType);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4AddressType);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            if (i4AddressType == 2)
            {

                STRCPY (pHttp->au1KeyString, "SOURCE_IP_ADDRESS");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssAclL3FilterSrcIpAddr (i4FilterNo, &SrcIpAddrOctet);
                MEMCPY (&SrcIp6Addr, SrcIpAddrOctet.pu1_OctetList,
                        IP6_ADDR_SIZE);
                SrcIpv6.i4_Length = IP6_ADDR_SIZE;
                STRCPY (pu1String,
                        Ip6PrintNtop ((tIp6Addr *) (VOID *) SrcIpAddrOctet.
                                      pu1_OctetList));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "SOURCE_MASK");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssAclL3FilterSrcIpAddrPrefixLength (i4FilterNo,
                                                           &u4SrcPrefix);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4SrcPrefix);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DESTINATION_IP_ADDRESS");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssAclL3FilterDstIpAddr (i4FilterNo, &DstIpAddrOctet);
                MEMCPY (&DstIp6Addr, DstIpAddrOctet.pu1_OctetList,
                        IP6_ADDR_SIZE);
                DstIpv6.i4_Length = IP6_ADDR_SIZE;
                STRCPY (pu1String,
                        Ip6PrintNtop ((tIp6Addr *) (VOID *) DstIpAddrOctet.
                                      pu1_OctetList));
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DESTINATION_MASK");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssAclL3FilterDstIpAddrPrefixLength (i4FilterNo,
                                                           &u4DstPrefix);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4DstPrefix);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            }
            if (i4AddressType == 1)
            {
                STRCPY (pHttp->au1KeyString, "SOURCE_IP_ADDRESS");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterSrcIpAddr (i4FilterNo, &u4RetVal);
                WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "SOURCE_MASK");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterSrcIpAddrMask (i4FilterNo, &u4RetVal);
                WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DESTINATION_IP_ADDRESS");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterDstIpAddr (i4FilterNo, &u4RetVal);
                WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DESTINATION_MASK");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterDstIpAddrMask (i4FilterNo, &u4RetVal);
                WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            }
            MEMSET (InPortList->pu1_OctetList, 0, ISS_PORTLIST_LEN);
            nmhGetIssL3FilterInPortList (i4FilterNo,
                                         pu1DataString->pOctetStrValue);
            MEMSET (*pInTempPortList, 0, VLAN_PORT_LIST_SIZE);
            IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                    *pInTempPortList);
            MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                    *pInTempPortList, pu1DataString->pOctetStrValue->i4_Length);
            WebnmSendToSocket (pHttp, "IN_PORTS_LIST", pu1DataString,
                               ENM_PORT_LIST);

            MEMSET (OutPortList->pu1_OctetList, 0, ISS_PORTLIST_LEN);
            nmhGetIssL3FilterOutPortList (i4FilterNo,
                                          pu1DataString->pOctetStrValue);
            MEMSET (*pOutTempPortList, 0, VLAN_PORT_LIST_SIZE);
            IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                    *pOutTempPortList);
            MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                    *pOutTempPortList,
                    pu1DataString->pOctetStrValue->i4_Length);
            WebnmSendToSocket (pHttp, "OUT_PORTS_LIST", pu1DataString,
                               ENM_PORT_LIST);

            STRCPY (pHttp->au1KeyString, "PROTOCOL");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssExtL3FilterProtocol (i4FilterNo, &i4Protocol);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Protocol);

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "OTHER");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Protocol);

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            if (i4Protocol == 1)
            {
                STRCPY (pHttp->au1KeyString, "CODE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterMessageCode (i4FilterNo, &i4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "TYPE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterMessageType (i4FilterNo, &i4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
                STRCPY (pHttp->au1KeyString, "PRIORITY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterPriority (i4FilterNo, &i4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            }
            else
            {
                STRCPY (pHttp->au1KeyString, "CODE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                STRCPY (pHttp->au1DataString, "");
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "TYPE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                STRCPY (pHttp->au1DataString, "");
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "PRIORITY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterPriority (i4FilterNo, &i4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DSCP");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterDscp (i4FilterNo, &i4RetVal);
                /*web_change */
                if ((i4RetVal < 0) || (i4RetVal > 63))
                {
                    STRCPY (pHttp->au1DataString, "");
                }
                else
                {
                    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                }
                /*web_change */
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "TOS");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterTos (i4FilterNo, &i4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            }
            if (i4Protocol == 6)
            {
                STRCPY (pHttp->au1KeyString, "ACK_BIT");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterAckBit (i4FilterNo, &i4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "RST_BIT");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterRstBit (i4FilterNo, &i4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }

            if (i4Protocol == 6 || i4Protocol == 17)
            {
                STRCPY (pHttp->au1KeyString, "SOURCE_PORT_START");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterMinSrcProtPort (i4FilterNo, &u4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "SOURCE_PORT_END");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterMaxSrcProtPort (i4FilterNo, &u4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DESTINATION_PORT_START");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterMinDstProtPort (i4FilterNo, &u4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DESTINATION_PORT_END");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssExtL3FilterMaxDstProtPort (i4FilterNo, &u4RetVal);
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                (INT4) STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            }

            STRCPY (pHttp->au1KeyString, "FLOW_ID");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssAclL3FilterFlowId (i4FilterNo, &u4FlowId);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", u4FlowId);

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "MFC_STORAGE");
            WebnmSendString (pHttp, pHttp->au1KeyString);
#ifdef QOSX_WANTED
            nmhGetDiffServMultiFieldClfrStorage ((UINT4) i4FilterNo,
                                                 &i4Storage);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%u", i4Storage);
#endif
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        }
        i4CurrentFilterNo = i4FilterNo;
    }
    while (nmhGetNextIndexIssExtL3FilterTable
           (i4CurrentFilterNo, &i4FilterNo) == SNMP_SUCCESS);

    ISS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    free_octetstring (InPortList);
    FsUtilReleaseBitList ((UINT1 *) pInTempPortList);
    FsUtilReleaseBitList ((UINT1 *) pOutTempPortList);
    free_octetstring (OutPortList);
    if (i4FilterNo >= ACL_EXTENDED_START)
    {
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
    }
}

/*********************************************************************
*  Function Name : IssOthersProcessIPStdFilterConfPageGet
*  Description   : This function processes the get request for the IP
*                  standard filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssOthersProcessIPStdFilterConfPageGet (tHttp * pHttp)
{
    INT4                i4FilterNo, i4CurrentFilterNo, i4RetVal, i4OutCome;
    UINT4               u4RetVal, u4Temp;
    tSNMP_OCTET_STRING_TYPE *InPortList = NULL, *OutPortList = NULL;
    tSNMP_MULTI_DATA_TYPE *pu1DataString = NULL;
    tPortList          *pInTempPortList = NULL;
    tPortList          *pOutTempPortList = NULL;
    UINT1              *pu1String;

    pInTempPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pInTempPortList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (InPortList);
        free_octetstring (OutPortList);
        return;
    }
    MEMSET (*pInTempPortList, 0, sizeof (tPortList));
    pOutTempPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pOutTempPortList == NULL)
    {
        FsUtilReleaseBitList ((UINT1 *) pInTempPortList);
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }
    MEMSET (*pOutTempPortList, 0, sizeof (tPortList));
    if ((pu1DataString = WebnmAllocMultiData ()) == NULL)
    {
        FsUtilReleaseBitList ((UINT1 *) pInTempPortList);
        FsUtilReleaseBitList ((UINT1 *) pOutTempPortList);
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }

    pHttp->i4Write = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    WebnmRegisterLock (pHttp, IssLock, IssUnLock);

    ISS_LOCK ();

    i4OutCome = (INT4) (nmhGetFirstIndexIssExtL3FilterTable (&i4FilterNo));
    if ((i4OutCome == SNMP_FAILURE) || (i4FilterNo >= ACL_EXTENDED_START))
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        FsUtilReleaseBitList ((UINT1 *) pInTempPortList);
        FsUtilReleaseBitList ((UINT1 *) pOutTempPortList);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    InPortList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);

    if (InPortList == NULL)
    {
        ISS_UNLOCK ();
        FsUtilReleaseBitList ((UINT1 *) pInTempPortList);
        FsUtilReleaseBitList ((UINT1 *) pOutTempPortList);
        WebnmUnRegisterLock (pHttp);
        return;
    }
    OutPortList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);

    if (OutPortList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (InPortList);
        FsUtilReleaseBitList ((UINT1 *) pInTempPortList);
        FsUtilReleaseBitList ((UINT1 *) pOutTempPortList);
        return;
    }

    do
    {

        /* Display only if the ACL number is in the range 1 to 1000 */

        if (i4FilterNo < ACL_EXTENDED_START)
        {
            pHttp->i4Write = (INT4) u4Temp;
            STRCPY (pHttp->au1KeyString, "FILTER_NUMBER");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4FilterNo);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            STRCPY (pHttp->au1KeyString, "ACTION");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssExtL3FilterAction (i4FilterNo, &i4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "SOURCE_IP_ADDRESS");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssExtL3FilterSrcIpAddr (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "SOURCE_MASK");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssExtL3FilterSrcIpAddrMask (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "DESTINATION_IP_ADDRESS");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssExtL3FilterDstIpAddr (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "DESTINATION_MASK");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssExtL3FilterDstIpAddrMask (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            (INT4) STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            nmhGetIssL3FilterInPortList (i4FilterNo,
                                         pu1DataString->pOctetStrValue);
            MEMSET (*pInTempPortList, 0, sizeof (tPortList));
            IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                    *pInTempPortList);
            MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                    *pInTempPortList, pu1DataString->pOctetStrValue->i4_Length);
            WebnmSendToSocket (pHttp, "IN_PORTS_LIST", pu1DataString,
                               ENM_PORT_LIST);

            nmhGetIssL3FilterOutPortList (i4FilterNo,
                                          pu1DataString->pOctetStrValue);
            MEMSET (*pOutTempPortList, 0, sizeof (tPortList));
            IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                    *pOutTempPortList);
            MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList,
                    *pOutTempPortList,
                    pu1DataString->pOctetStrValue->i4_Length);
            WebnmSendToSocket (pHttp, "OUT_PORTS_LIST", pu1DataString,
                               ENM_PORT_LIST);

        }

        i4CurrentFilterNo = i4FilterNo;
    }
    while (nmhGetNextIndexIssExtL3FilterTable
           (i4CurrentFilterNo, &i4FilterNo) == SNMP_SUCCESS);

    ISS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    free_octetstring (InPortList);
    free_octetstring (OutPortList);
    FsUtilReleaseBitList ((UINT1 *) pInTempPortList);
    FsUtilReleaseBitList ((UINT1 *) pOutTempPortList);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : IssOthersProcessIPStdFilterConfPageSet
*  Description   : This function processes the set request for the IP
*                 standard filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssOthersProcessIPStdFilterConfPageSet (tHttp * pHttp)
{
    UINT4               u4SrcIpAddr = 0, u4SrcAddrMask = 0, u4DestIpAddr =
        0, u4DestAddrMask = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4Action = 0, i4FilterNo = 0;
    tSNMP_OCTET_STRING_TYPE InPortList, OutPortList;
    UINT1               u1InPortList[ISS_PORTLIST_LEN],
        u1OutPortList[ISS_PORTLIST_LEN];
    UINT1               u1Apply = 0, u1RowStatus = ISS_CREATE_AND_WAIT;
    UINT1               au1InPortList[ISS_PORTLIST_LEN];
    UINT1               au1OutPortList[ISS_PORTLIST_LEN];

    MEMSET (&InPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&OutPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    InPortList.pu1_OctetList = &au1InPortList[0];
    OutPortList.pu1_OctetList = &au1OutPortList[0];

    STRCPY (pHttp->au1Name, "FILTER_NUMBER");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4FilterNo = ATOI (pHttp->au1Value);

    /* Check whether the request is for ADD/DELETE */
    STRCPY (pHttp->au1Name, "DELETE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    ISS_LOCK ();
    /* check for delete */
    if (STRCMP (pHttp->au1Value, "DELETE") == 0)
    {
        if (nmhTestv2IssExtL3FilterStatus
            (&u4ErrorCode, i4FilterNo, ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "ACL with this number not available.");
            return;
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "ACL with this number not available.");
            return;
        }
        ISS_UNLOCK ();
        IssOthersProcessIPStdFilterConfPageGet (pHttp);
        return;
    }
    if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {
        u1Apply = 1;
        u1RowStatus = ISS_NOT_IN_SERVICE;
    }
    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "SOURCE_ADDRESS");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        u4SrcIpAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));

    STRCPY (pHttp->au1Name, "SOURCE_MASK");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        u4SrcAddrMask = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));

    STRCPY (pHttp->au1Name, "DESTINATION_ADDRESS");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        u4DestIpAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));

    STRCPY (pHttp->au1Name, "DESTINATION_MASK");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        u4DestAddrMask = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    STRCPY (pHttp->au1Name, "IN_PORTS_LIST");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (STRLEN (pHttp->au1Value) != 0)
    {
        MEMSET (u1InPortList, 0, ISS_PORTLIST_LEN);
        issDecodeSpecialChar (pHttp->au1Value);
        if (STRCMP (pHttp->au1Value, "0") != 0)
        {
            if (IssPortListStringParser (pHttp->au1Value, u1InPortList) ==
                ENM_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp, (INT1 *) "Check the allowed in port list");
                return;
            }
        }

        InPortList.i4_Length = ISS_PORTLIST_LEN;
        MEMCPY (InPortList.pu1_OctetList, u1InPortList, InPortList.i4_Length);
    }

    STRCPY (pHttp->au1Name, "OUT_PORTS_LIST");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        MEMSET (u1OutPortList, 0, ISS_PORTLIST_LEN);
        issDecodeSpecialChar (pHttp->au1Value);

        if (STRCMP (pHttp->au1Value, "0") != 0)
        {

            if (IssPortListStringParser (pHttp->au1Value, u1OutPortList) ==
                ENM_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp, (INT1 *) "Check the allowed in port list");
                return;
            }
        }
        OutPortList.i4_Length = ISS_PORTLIST_LEN;
        MEMCPY (OutPortList.pu1_OctetList, u1OutPortList,
                OutPortList.i4_Length);
    }
    if (nmhTestv2IssExtL3FilterStatus (&u4ErrorCode, i4FilterNo,
                                       u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Invalid ACL number.");
        return;
    }
    if (nmhSetIssExtL3FilterStatus (i4FilterNo, u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Entry already exists for this IP"
                      " filter number");
        return;
    }

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    if ((i4Action != 0) && (nmhTestv2IssExtL3FilterAction
                            (&u4ErrorCode, i4FilterNo,
                             i4Action)) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        IssSendError (pHttp,
                      (INT1 *) "Unable to set the standard ACL filter action.");
        return;
    }

    if ((u4SrcIpAddr > 0) && (nmhTestv2IssExtL3FilterSrcIpAddr
                              (&u4ErrorCode, i4FilterNo, u4SrcIpAddr))
        == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        IssSendError (pHttp,
                      (INT1 *)
                      "Unable to set ACL with this source IP address.");
        return;
    }

    if ((u4SrcAddrMask > 0) && (nmhTestv2IssExtL3FilterSrcIpAddrMask
                                (&u4ErrorCode, i4FilterNo, u4SrcAddrMask)
                                == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        IssSendError (pHttp,
                      (INT1 *)
                      "Unable to set ACL with this source IP address Mask.");
        return;
    }
    if ((u4DestIpAddr > 0) && (nmhTestv2IssExtL3FilterDstIpAddr
                               (&u4ErrorCode, i4FilterNo, u4DestIpAddr)
                               == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        IssSendError (pHttp,
                      (INT1 *)
                      "Unable to set ACL with this destination IP Address.");
        return;
    }

    if ((u4DestAddrMask > 0) && (nmhTestv2IssExtL3FilterDstIpAddrMask
                                 (&u4ErrorCode, i4FilterNo, u4DestAddrMask)
                                 == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        IssSendError (pHttp,
                      (INT1 *)
                      "Unable to set ACL with this destination IP address mask");
        return;
    }

    if ((InPortList.pu1_OctetList != NULL) && (nmhTestv2IssExtL3FilterInPortList
                                               (&u4ErrorCode, i4FilterNo,
                                                &InPortList) == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        IssSendError (pHttp, (INT1 *) "Unable to set this in port list.");
        return;
    }

    if ((OutPortList.pu1_OctetList != NULL)
        &&
        (nmhTestv2IssExtL3FilterOutPortList
         (&u4ErrorCode, i4FilterNo, &OutPortList) == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        IssSendError (pHttp, (INT1 *) "Unable to set this Out port list.");
        return;
    }

    if ((i4Action != 0) && (nmhSetIssExtL3FilterAction (i4FilterNo, i4Action)
                            == SNMP_FAILURE))
    {
        IssSendError (pHttp,
                      (INT1 *)
                      "Unable to set the Standard ACL  filter action.");
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        return;
    }

    if ((u4SrcIpAddr > 0) && (nmhSetIssExtL3FilterSrcIpAddr
                              (i4FilterNo, u4SrcIpAddr) == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (INT1 *) "Unable to set the ACL source IP address.");
        return;
    }
    if ((u4SrcAddrMask > 0) && (nmhSetIssExtL3FilterSrcIpAddrMask
                                (i4FilterNo, u4SrcAddrMask) == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set ACL source address mask.");
        return;
    }

    if ((u4DestIpAddr > 0) && (nmhSetIssExtL3FilterDstIpAddr
                               (i4FilterNo, u4DestIpAddr) == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (INT1 *) "Unable to set ACL destination IP address.");
        return;
    }
    if ((u4DestAddrMask > 0) && (nmhSetIssExtL3FilterDstIpAddrMask
                                 (i4FilterNo, u4DestAddrMask) == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (INT1 *)
                      "Unable to set the destination IP address mask.");
        return;
    }

    if ((InPortList.pu1_OctetList != NULL) && (nmhSetIssExtL3FilterInPortList
                                               (i4FilterNo, &InPortList)
                                               == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set In Port List.");
        return;
    }
    if ((OutPortList.pu1_OctetList != NULL) && (nmhSetIssExtL3FilterOutPortList
                                                (i4FilterNo, &OutPortList)
                                                == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set Out Port List.");
        return;
    }

    if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set Filter Status");
        return;
    }

    ISS_UNLOCK ();
    IssOthersProcessIPStdFilterConfPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssOthersProcessMACFilterConfPageGet
*  Description   : This function processes the get request for the MAC
*                  filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssOthersProcessMACFilterConfPageGet (tHttp * pHttp)
{
    INT4                i4CurrentFilterNo, i4FilterNo, i4RetVal, i4OutCome;
    UINT4               u4RetVal, u4Temp;
    UINT1               au1String[20];
    tMacAddr            SrcMacAddr, DestMacAddr;
    tSNMP_OCTET_STRING_TYPE *InPortList = NULL;
    tPortList          *pInTempPortList = NULL;

    tSNMP_MULTI_DATA_TYPE *pu1DataString = NULL;

    pInTempPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pInTempPortList == NULL)
    {
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }
    MEMSET (*pInTempPortList, 0, sizeof (tPortList));
    if ((pu1DataString = WebnmAllocMultiData ()) == NULL)
    {
        FsUtilReleaseBitList ((UINT1 *) pInTempPortList);
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }
    pHttp->i4Write = 0;

    IssPrintAvailableVlans (pHttp);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;

    WebnmRegisterLock (pHttp, IssLock, IssUnLock);

    ISS_LOCK ();
    i4OutCome = (INT4) (nmhGetFirstIndexIssExtL2FilterTable (&i4FilterNo));
    if (i4OutCome == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        FsUtilReleaseBitList ((UINT1 *) pInTempPortList);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    InPortList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);

    if (InPortList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        FsUtilReleaseBitList ((UINT1 *) pInTempPortList);
        return;
    }
    do
    {
        pHttp->i4Write = (INT4) u4Temp;
        STRCPY (pHttp->au1KeyString, "FILTER_NUMBER");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4FilterNo);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "SOURCE_MAC");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssExtL2FilterSrcMacAddr (i4FilterNo, &SrcMacAddr);
        IssPrintMacAddress (SrcMacAddr, au1String);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "DESTINATION_MAC");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssExtL2FilterDstMacAddr (i4FilterNo, &DestMacAddr);
        IssPrintMacAddress (DestMacAddr, au1String);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "ACTION_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssExtL2FilterAction (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "PRIORITY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssExtL2FilterPriority (i4FilterNo, &i4RetVal);
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        IssPrintAvailableVlans (pHttp);
        STRCPY (pHttp->au1KeyString, "VLAN_ID");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmRegisterLock (pHttp, IssLock, IssUnLock);
        ISS_LOCK ();
        nmhGetIssExtL2FilterVlanId (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        MEMSET (InPortList->pu1_OctetList, 0, ISS_PORTLIST_LEN);
        nmhGetIssExtL2FilterInPortList (i4FilterNo,
                                        pu1DataString->pOctetStrValue);
        MEMSET (*pInTempPortList, 0, sizeof (tPortList));
        IssConvertToIfPortList (pHttp, pu1DataString->pOctetStrValue,
                                *pInTempPortList);

        MEMCPY (pu1DataString->pOctetStrValue->pu1_OctetList, *pInTempPortList,
                pu1DataString->pOctetStrValue->i4_Length);
        WebnmSendToSocket (pHttp, "IN_PORT_LIST", pu1DataString, ENM_PORT_LIST);

        STRCPY (pHttp->au1KeyString, "ENCAPTYPE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssExtL2FilterEtherType (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "PROTOCOL");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssExtL2FilterProtocolType (i4FilterNo, &u4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OTHER");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        i4CurrentFilterNo = i4FilterNo;
    }
    while (nmhGetNextIndexIssExtL2FilterTable
           (i4CurrentFilterNo, &i4FilterNo) == SNMP_SUCCESS);

    ISS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    free_octetstring (InPortList);
    FsUtilReleaseBitList ((UINT1 *) pInTempPortList);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : IssOthersProcessMACFilterConfPageSet
*  Description   : This function processes the set request for the MAC
*                  filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssOthersProcessMACFilterConfPageSet (tHttp * pHttp)
{
    INT4                i4Action, i4FilterNo, i4Priority, i4EtherType;
    INT4                i4VlanId;
    UINT4               u4ErrorCode, u4Protocol;
    tMacAddr            SrcMacAddr, DestMacAddr;
    tSNMP_OCTET_STRING_TYPE InPortList;
    UINT1               u1PortList[ISS_PORTLIST_LEN], u1Apply = 0;
    UINT1               u1RowStatus = ISS_CREATE_AND_WAIT;
    UINT1               au1ZeroMac[ISS_MAC_LEN];
    MEMSET (au1ZeroMac, 0, ISS_MAC_LEN);
    UINT1               au1InPortList[ISS_PORTLIST_LEN];
    MEMSET (&InPortList, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    InPortList.pu1_OctetList = &au1InPortList[0];

    STRCPY (pHttp->au1Name, "FILTER_NUMBER");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4FilterNo = ATOI (pHttp->au1Value);

    /* Check whether the request is for ADD/DELETE */
    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    ISS_LOCK ();
    /* check for delete */
    if (STRCMP (pHttp->au1Value, "DELETE") == 0)
    {
        if (nmhTestv2IssExtL2FilterStatus
            (&u4ErrorCode, i4FilterNo, ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "MAC filter not available.");
            return;
        }

        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "MAC filter not available.");
            return;
        }
        ISS_UNLOCK ();
        IssOthersProcessMACFilterConfPageGet (pHttp);
        return;
    }

    if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {
        u1Apply = 1;
        u1RowStatus = ISS_NOT_IN_SERVICE;
    }

    STRCPY (pHttp->au1Name, "SOURCE_MAC");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    StrToMac (pHttp->au1Value, SrcMacAddr);

    STRCPY (pHttp->au1Name, "DESTINATION_MAC");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    StrToMac (pHttp->au1Value, DestMacAddr);

    STRCPY (pHttp->au1Name, "ACTION_KEY");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PRIORITY");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Priority = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "VLAN_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4VlanId = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "IN_PORT_LIST");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        MEMSET (u1PortList, 0, ISS_PORTLIST_LEN);
        issDecodeSpecialChar (pHttp->au1Value);

        if (STRCMP (pHttp->au1Value, "0") != 0)
        {
            if (IssPortListStringParser (pHttp->au1Value, u1PortList) ==
                ENM_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp, (INT1 *) "Check the allowed port list");
                return;
            }
        }
        InPortList.i4_Length = ISS_PORTLIST_LEN;
        MEMCPY (InPortList.pu1_OctetList, u1PortList, InPortList.i4_Length);
    }
    STRCPY (pHttp->au1Name, "ENCAPTYPE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4EtherType = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PROTOCOL");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4Protocol = (UINT4) ATOI (pHttp->au1Value);
    if (u4Protocol == OTHER)
    {
        STRCPY (pHttp->au1Name, "OTHER");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Protocol = (UINT4) ATOI (pHttp->au1Value);
    }
    if (nmhTestv2IssExtL2FilterStatus (&u4ErrorCode, i4FilterNo,
                                       u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Invalid MAC filter number.");
        return;
    }

    if (nmhTestv2IssExtL2FilterAction (&u4ErrorCode, i4FilterNo, i4Action)
        == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the l2 filter action.");
        return;
    }

    if (MEMCMP (au1ZeroMac, SrcMacAddr, ISS_MAC_LEN))
    {
        if (nmhTestv2IssExtL2FilterSrcMacAddr (&u4ErrorCode, i4FilterNo,
                                               SrcMacAddr) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set the filter with "
                          "this source MAC Address.");
            return;
        }
    }

    if (MEMCMP (au1ZeroMac, DestMacAddr, ISS_MAC_LEN))
    {
        if (nmhTestv2IssExtL2FilterDstMacAddr (&u4ErrorCode, i4FilterNo,
                                               DestMacAddr) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set the filter with this"
                          " destination MAC Address.");
            return;
        }
    }

    if (nmhTestv2IssExtL2FilterPriority (&u4ErrorCode, i4FilterNo, i4Priority)
        == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter priority");
        return;
    }

    if ((u4Protocol != 0) && (nmhTestv2IssExtL2FilterProtocolType
                              (&u4ErrorCode, i4FilterNo, u4Protocol)
                              == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (INT1 *) "Unable to set MAC filter protocol type!");
        return;
    }

    if ((i4EtherType != 0) && (nmhTestv2IssExtL2FilterEtherType
                               (&u4ErrorCode, i4FilterNo, i4EtherType)
                               == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (INT1 *) "Unable to set MAC the filter ether type");
        return;
    }

    if ((i4VlanId != 0) && (nmhTestv2IssExtL2FilterVlanId
                            (&u4ErrorCode, i4FilterNo,
                             i4VlanId) == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set MAC filter VLAN ID.");
        return;
    }

    if ((InPortList.pu1_OctetList != NULL) && (nmhTestv2IssExtL2FilterInPortList
                                               (&u4ErrorCode, i4FilterNo,
                                                &InPortList)) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set this in port list.");
        return;
    }
    if (nmhSetIssExtL2FilterStatus (i4FilterNo, u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Entry already exists for this MAC"
                      " filter number");
        return;
    }

    if (nmhSetIssExtL2FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the l2 filter action.");
        return;
    }

    if (MEMCMP (au1ZeroMac, SrcMacAddr, ISS_MAC_LEN))
    {
        if (nmhSetIssExtL2FilterSrcMacAddr (i4FilterNo, SrcMacAddr)
            == SNMP_FAILURE)
        {
            if (u1Apply != 1)
            {
                nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set the source MAC Address.");
            return;
        }
    }

    if (MEMCMP (au1ZeroMac, DestMacAddr, ISS_MAC_LEN))
    {
        if (nmhSetIssExtL2FilterDstMacAddr
            (i4FilterNo, DestMacAddr) == SNMP_FAILURE)
        {
            if (u1Apply != 1)
            {
                nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *)
                          "Unable to set the Destination MAC Address.");
            return;
        }
    }
    if (nmhSetIssExtL2FilterPriority (i4FilterNo, i4Priority) == SNMP_FAILURE)
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter priority.");
        return;
    }

    if ((u4Protocol != 0) && (nmhSetIssExtL2FilterProtocolType
                              (i4FilterNo, u4Protocol) == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter protocol type.");
        return;
    }

    if ((i4EtherType != 0) && (nmhSetIssExtL2FilterEtherType
                               (i4FilterNo, i4EtherType) == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter ether type.");
        return;
    }

    if ((i4VlanId != 0) && (nmhSetIssExtL2FilterVlanId
                            (i4FilterNo, i4VlanId) == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter VLAN ID.");
        return;
    }

    if ((InPortList.pu1_OctetList != NULL) && (nmhSetIssExtL2FilterInPortList
                                               (i4FilterNo, &InPortList)
                                               == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set In Port List.");
        return;
    }

    if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        if (u1Apply != 1)
        {
            nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the filter status.");
        return;
    }
    ISS_UNLOCK ();
    IssOthersProcessMACFilterConfPageGet (pHttp);
}

VOID
IssOthersProcessStormControlConfPageGet (tHttp * pHttp)
{
    INT4                i4Index = 0;
    INT4                i4BCASTLimit = 0;
    INT4                i4DLFLimit = 0;
    INT4                i4MCASTLimit = 0;
    UINT4               u4Temp = 0;
    INT4                i4First = 0;
    tSNMP_MULTI_DATA_TYPE *pu1DataString = NULL;

    if ((pu1DataString = WebnmAllocMultiData ()) == NULL)
    {
        UNUSED_PARAM (pu1DataString);
        IssSendError (pHttp, (CONST INT1 *) ("Insufficient Memory \n"));
        return;
    }
    pHttp->i4Write = 0;

    IssPrintAvailableL2Ports (pHttp);
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = (UINT4) pHttp->i4Write;
    WebnmRegisterLock (pHttp, IssLock, IssUnLock);

    ISS_LOCK ();
    i4First = nmhGetFirstIndexIssAclRateCtrlTable (&i4Index);
    if (i4First == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
        WebnmUnRegisterLock (pHttp);
        ISS_UNLOCK ();
    }

    do
    {
        if (nmhValidateIndexInstanceIssPortCtrlTable (i4Index) != SNMP_SUCCESS)
        {
            IssSendError (pHttp, (INT1 *) "Invaild Index\n");
            WebnmUnRegisterLock (pHttp);
            ISS_UNLOCK ();
            return;
        }
        pHttp->i4Write = (INT4) u4Temp;
        IssPrintAvailableL2Ports (pHttp);
        STRCPY (pHttp->au1KeyString, "port_Key");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Index);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "bcast_lvlno_Key");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssRateCtrlBCASTLimitValue (i4Index, &i4BCASTLimit);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4BCASTLimit);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "mcast_lvlno_Key");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssRateCtrlMCASTLimitValue (i4Index, &i4MCASTLimit);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4MCASTLimit);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "dlf_lvlno_Key");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssRateCtrlDLFLimitValue (i4Index, &i4DLFLimit);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4DLFLimit);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        (INT4) STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    while (nmhGetNextIndexIssAclRateCtrlTable (i4Index, &i4Index));
    ISS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

VOID
IssOthersProcessStromControlConfPageSet (tHttp * pHttp)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4BcastType = 0;
    UINT4               u4McastType = 0;
    UINT4               u4DlfType = 0;
    UINT4               u4BcastLvlno = 0;
    UINT4               u4McastLvlno = 0;
    UINT4               u4DLFLvlno = 0;
    UINT4               u4ErrorCode = 0;

    ISS_LOCK ();

    STRCPY (pHttp->au1Name, "port");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4IfIndex = (UINT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "bcast");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (!STRCMP (pHttp->au1Value, "true"))
    {
        u4BcastType = 1;
    }
    else
    {
        u4BcastType = 0;
    }
    STRCPY (pHttp->au1Name, "bcast_lvlno");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4BcastLvlno = (UINT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "mcast");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (!STRCMP (pHttp->au1Value, "true"))
    {
        u4McastType = 1;
    }
    else
    {
        u4McastType = 0;
    }
    STRCPY (pHttp->au1Name, "mcast_lvlno");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4McastLvlno = (UINT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "dlf");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (!STRCMP (pHttp->au1Value, "true"))
    {
        u4DlfType = 1;
    }
    else
    {
        u4DlfType = 0;
    }
    STRCPY (pHttp->au1Name, "dlf_lvlno");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4DLFLvlno = (UINT4) ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if ((!STRCMP (pHttp->au1Value, "Add"))
        || (!STRCMP (pHttp->au1Value, "delete")))
    {
        if (!STRCMP (pHttp->au1Value, "delete"))
        {
            if (u4BcastType)
            {
                u4BcastLvlno = 0;
            }
            if (u4McastType)
            {
                u4McastLvlno = 0;
            }
            if (u4DlfType)
            {
                u4DLFLvlno = 0;
            }
        }
        if (nmhTestv2IssExtRateCtrlBCASTLimitValue
            (&u4ErrorCode, (INT4) u4IfIndex,
             (INT4) u4BcastLvlno) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Verification failed in BroadCast Limit\n");
            return;
        }
        if (nmhSetIssExtRateCtrlBCASTLimitValue
            ((INT4) u4IfIndex, (INT4) u4BcastLvlno) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set BroadCast Limit");
            return;
        }
        if (nmhTestv2IssExtRateCtrlMCASTLimitValue
            (&u4ErrorCode, (INT4) u4IfIndex,
             (INT4) u4McastLvlno) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Verification failed in MultiCast Limit");
            return;
        }
        if (nmhSetIssExtRateCtrlMCASTLimitValue
            ((INT4) u4IfIndex, (INT4) u4McastLvlno) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set MultiCast Limit");
            return;
        }
        if (nmhTestv2IssExtRateCtrlDLFLimitValue
            (&u4ErrorCode, (INT4) u4IfIndex, (INT4) u4DLFLvlno) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Verification failed in DLF Limit");
            return;
        }
        if (nmhSetIssExtRateCtrlDLFLimitValue
            ((INT4) u4IfIndex, (INT4) u4DLFLvlno) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set DLF Limit");
            return;
        }
    }
    else
    {
        if (u4BcastType)
        {
            if (nmhTestv2IssExtRateCtrlBCASTLimitValue
                (&u4ErrorCode, (INT4) u4IfIndex,
                 (INT4) u4BcastLvlno) == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp,
                              (INT1 *)
                              "Verification failed in BroadCast Limit\n");
                return;
            }
            if (nmhSetIssExtRateCtrlBCASTLimitValue
                ((INT4) u4IfIndex, (INT4) u4BcastLvlno) == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp, (INT1 *) "Unable to set BroadCast Limit");
                return;
            }

            if (u4McastType)
            {
                if (nmhTestv2IssExtRateCtrlMCASTLimitValue
                    (&u4ErrorCode, (INT4) u4IfIndex,
                     (INT4) u4McastLvlno) == SNMP_FAILURE)
                {
                    ISS_UNLOCK ();
                    IssSendError (pHttp,
                                  (INT1 *)
                                  "Verification failed in MultiCast Limit");
                    return;
                }
                if (nmhSetIssExtRateCtrlMCASTLimitValue
                    ((INT4) u4IfIndex, (INT4) u4McastLvlno) == SNMP_FAILURE)
                {
                    ISS_UNLOCK ();
                    IssSendError (pHttp,
                                  (INT1 *) "Unable to set MultiCast Limit");
                    return;
                }
            }
            if (u4DlfType)
            {
                if (nmhTestv2IssExtRateCtrlDLFLimitValue
                    (&u4ErrorCode, (INT4) u4IfIndex,
                     (INT4) u4DLFLvlno) == SNMP_FAILURE)
                {
                    ISS_UNLOCK ();
                    IssSendError (pHttp,
                                  (INT1 *) "Verification failed in DLF Limit");
                    return;
                }
                if (nmhSetIssExtRateCtrlDLFLimitValue
                    ((INT4) u4IfIndex, (INT4) u4DLFLvlno) == SNMP_FAILURE)
                {
                    ISS_UNLOCK ();
                    IssSendError (pHttp, (INT1 *) "Unable to set DLF Limit");
                    return;
                }
            }
        }
    }
    ISS_UNLOCK ();

    IssOthersProcessStormControlConfPageGet (pHttp);

}

VOID
IssOthersProcessStormControlConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssOthersProcessStormControlConfPageGet (pHttp);
    }
    else
    {
        IssOthersProcessStromControlConfPageSet (pHttp);
    }

}

#endif
