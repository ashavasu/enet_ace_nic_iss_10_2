
#ifndef _ISSEXGLOB_H        
#define _ISSEXGLOB_H        

#ifndef _ISSEXSYS_C

/* This is already defined in issglob.h in ISS/common/system */ 
extern UINT4                  gu4IssCidrSubnetMask[ISS_MAX_CIDR + 1];
#endif

#endif
