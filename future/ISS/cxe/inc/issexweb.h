#ifndef _BCMISSWEB_H
#define _BCMISSWEB_H

#include "webiss.h"
#include "isshttp.h"
#include "aclcxecli.h"
#include "iss.h"
#include "fsisselw.h"
#include "dscxe.h"

#define   ACL_EXTENDED_START 51
/* Prototypes for specific pages processing */
INT4 IssProcessCustomPages (tHttp * pHttp);
VOID IssRedirectMacFilterPage (tHttp *pHttp);
VOID IssRedirectDiffSrvPage (tHttp *pHttp);
VOID IssCxeProcessIPFilterConfPage (tHttp * pHttp);
VOID IssCxeProcessIPFilterConfPageSet (tHttp * pHttp);
VOID IssCxeProcessIPFilterConfPageGet (tHttp * pHttp);
VOID IssCxeProcessIPStdFilterConfPage (tHttp * pHttp);
VOID IssCxeProcessIPStdFilterConfPageGet (tHttp * pHttp);
VOID IssCxeProcessIPStdFilterConfPageSet (tHttp * pHttp);
VOID IssCxeProcessMACFilterConfPage (tHttp * pHttp);
VOID IssCxeProcessMACFilterConfPageGet (tHttp * pHttp);
VOID IssCxeProcessMACFilterConfPageSet (tHttp * pHttp);
#ifdef DIFFSRV_WANTED
VOID IssCxeProcessDfsPolicyMapPage (tHttp * pHttp);
VOID IssCxeProcessDfsPolicyMapPageGet (tHttp * pHttp);
VOID IssCxeProcessDfsPolicyMapPageSet (tHttp * pHttp);
VOID IssCxeProcessDfsTrafficClassPage (tHttp * pHttp);
VOID IssCxeProcessDfsTrafficClassPageGet (tHttp * pHttp);
VOID IssCxeProcessDfsTrafficClassPageSet (tHttp * pHttp);
VOID IssCxeProcessDfsRedCurveConfigPage (tHttp * pHttp);
VOID IssCxeProcessDfsRedCurveConfigPageGet (tHttp * pHttp);
VOID IssCxeProcessDfsRedCurveConfigPageSet (tHttp * pHttp);
#endif

tSpecificPage       asIssTargetpages[] = {
    {"cxe_ip_filterconf.html", IssCxeProcessIPFilterConfPage},
    {"cxe_ip_stdfilterconf.html", IssCxeProcessIPStdFilterConfPage},
    {"cxe_mac_filterconf.html", IssCxeProcessMACFilterConfPage},
#ifdef DIFFSRV_WANTED
    {"cxe_dfs_policymapconf.html", IssCxeProcessDfsPolicyMapPage},
    {"cxe_dfs_traffclasstableconf.html", IssCxeProcessDfsTrafficClassPage},
    {"cxe_dfs_redconf.html", IssCxeProcessDfsRedCurveConfigPage},
#endif
    {"", NULL}
};

typedef struct DiffServTraffClassEntry
{
    INT4           i4TraffClassId;
    INT4           i4OutPort;
    INT4           i4MaxBw;
    INT4           i4GrntdBw;
    INT4           i4DropLv11;
    INT4           i4DropLv12;
    INT4           i4DropLv13;
    INT4           i4DropAtMaxBw;
    INT4           i4WgtFactor;
    INT4           i4WgtShift;
    INT4           i4FlowGroups;
    INT4           i4RedCurveId;
}tDiffServTraffClassEntry;

typedef struct DSREDCurveEntry
{
    INT4           i4RedCurveId;
    INT4           i4RedCurveStart;
    INT4           i4RedCurveStop;
    INT4           i4RedCurveQRange;
    INT4           i4RedCurveStopProb;
}tDSREDCurveEntry;

INT4 ConfigureDfsTrafficClassTableObjects (tHttp * ,tDiffServTraffClassEntry *);

extern INT4 DsWebnmGetPolicyMapEntry PROTO 
                    ((INT4 i4PolicyMapId,
                      tDiffServWebClfrData *pClfrData));

extern INT4 DsWebnmSetPolicyMapEntry PROTO 
                    ((tDiffServWebSetClfrEntry *pClfrEntry,
                      UINT1 *pu1ErrString));
#ifdef DIFFSRV_WANTED
extern INT1 nmhGetFsDiffServClfrTrafficClassId (INT4 ,INT4 *);
extern INT1 nmhGetFirstIndexFsDsTrafficClassConfigTable (INT4 *);
extern INT1 nmhGetFsDsTCEntryStatus (INT4 ,INT4 *);
extern INT1 nmhGetFsDsTCPortNum (INT4 i4FsDsTrafficClassId,
                                 INT4 *pi4RetValFsDsTCPortNum);
extern INT1 nmhGetFsDsTCPortNum (INT4 i4FsDsTrafficClassId, 
                                 INT4 *pi4RetValFsDsTCPortNum);
extern INT1 nmhGetFsDsTCMaxBandwidth (INT4 i4FsDsTrafficClassId,                                                      INT4 *pi4RetValFsDsTCMaxBandwidth);
extern INT1 nmhGetFsDsTCGuaranteedBandwidth (INT4 i4FsDsTrafficClassId,                                              INT4 *pi4RetValFsDsTCGuaranteedBandwidth);
extern INT1  nmhGetFsDsTCDropLevel1 (INT4 i4FsDsTrafficClassId,                                                      INT4 *pi4RetValFsDsTCDropLevel1);
extern INT1 nmhGetFsDsTCDropLevel2 (INT4 i4FsDsTrafficClassId,                                                      INT4 *pi4RetValFsDsTCDropLevel2);
extern INT1 nmhGetFsDsTCDropLevel3 (INT4 i4FsDsTrafficClassId,                                                      INT4 *pi4RetValFsDsTCDropLevel3);
extern  INT1 nmhGetFsDsTCDropAtMaxBandwidth (INT4 i4FsDsTrafficClassId,                                               INT4 *pi4RetValFsDsTCDropAtMaxBandwidth);
extern  INT1 nmhGetFsDsTCWeightFactor (INT4 i4FsDsTrafficClassId,                                                      INT4 *pi4RetValFsDsTCWeightFactor);
extern INT1 nmhGetFsDsTCNumFlowGroups (INT4 i4FsDsTrafficClassId,                                                      INT4 *pi4RetValFsDsTCNumFlowGroups);
extern INT1 nmhGetFsDsTCNumFlowGroups (INT4 i4FsDsTrafficClassId,                                                      INT4 *pi4RetValFsDsTCNumFlowGroups);
extern INT1 nmhGetNextIndexFsDsTrafficClassConfigTable (
                                              INT4 i4FsDsTrafficClassId,                                                      INT4 *pi4NextFsDsTrafficClassId);
extern INT1 nmhTestv2FsDsTCMaxBandwidth (UINT4 *pu4ErrorCode,
                                         INT4 i4FsDsTrafficClassId, 
                                         INT4 i4TestValFsDsTCMaxBandwidth);
extern INT1 nmhSetFsDsTCMaxBandwidth (INT4 i4FsDsTrafficClassId,                                                      INT4 i4SetValFsDsTCMaxBandwidth);
extern INT1                                                                            nmhTestv2FsDsTCGuaranteedBandwidth (UINT4 *pu4ErrorCode,                                                            INT4 i4FsDsTrafficClassId,                                                 INT4 i4TestValFsDsTCGuaranteedBandwidth);
extern INT1                                                                            nmhSetFsDsTCGuaranteedBandwidth (INT4 i4FsDsTrafficClassId,                                                     INT4 i4SetValFsDsTCGuaranteedBandwidth);
extern INT1 nmhTestv2FsDsTCDropLevel1 (UINT4 *pu4ErrorCode,
                                       INT4 i4FsDsTrafficClassId,
                                       INT4 i4TestValFsDsTCDropLevel1);
extern INT1 nmhSetFsDsTCDropLevel1 (INT4 i4FsDsTrafficClassId,                                                      INT4 i4SetValFsDsTCDropLevel1);
extern INT1 nmhTestv2FsDsTCDropLevel2 (UINT4 *pu4ErrorCode, 
                                       INT4 i4FsDsTrafficClassId,                                 INT4 i4TestValFsDsTCDropLevel2);
extern INT1 nmhSetFsDsTCDropLevel2 (INT4 i4FsDsTrafficClassId,                                                      INT4 i4SetValFsDsTCDropLevel2);
extern INT1 nmhTestv2FsDsTCDropLevel3 (UINT4 *pu4ErrorCode, 
                                       INT4 i4FsDsTrafficClassId,                                 INT4 i4TestValFsDsTCDropLevel3);
extern  INT1 nmhSetFsDsTCDropLevel3 (INT4 i4FsDsTrafficClassId,                                                      INT4 i4SetValFsDsTCDropLevel3);
extern INT1                                                                            nmhTestv2FsDsTCDropAtMaxBandwidth (UINT4 *pu4ErrorCode,                                                            INT4 i4FsDsTrafficClassId,                                                   INT4 i4TestValFsDsTCDropAtMaxBandwidth);
extern INT1                                                                            nmhSetFsDsTCDropAtMaxBandwidth (INT4 i4FsDsTrafficClassId,                                                      INT4 i4SetValFsDsTCDropAtMaxBandwidth);
extern INT1                                                                            nmhTestv2FsDsTCWeightFactor (UINT4 *pu4ErrorCode, INT4 i4FsDsTrafficClassId,                                 INT4 i4TestValFsDsTCWeightFactor);
extern INT1                                                                            nmhSetFsDsTCWeightFactor (INT4 i4FsDsTrafficClassId,                                                      INT4 i4SetValFsDsTCWeightFactor);
extern INT1                                                                            nmhTestv2FsDsTCWeightShift (UINT4 *pu4ErrorCode, INT4 i4FsDsTrafficClassId,                                 INT4 i4TestValFsDsTCWeightShift);
extern INT1                                                                            nmhSetFsDsTCWeightShift (INT4 i4FsDsTrafficClassId,                                                      INT4 i4SetValFsDsTCWeightShift);
extern INT1                                                                            nmhTestv2FsDsTCNumFlowGroups (UINT4 *pu4ErrorCode, INT4 i4FsDsTrafficClassId,                                 INT4 i4TestValFsDsTCNumFlowGroups);
extern INT1 nmhGetFsDsTCWeightShift (INT4 i4FsDsTrafficClassId,
                                     INT4 *pi4RetValFsDsTCWeightShift);
extern INT1                                                                            nmhGetFsDsTCREDCurveId (INT4 i4FsDsTrafficClassId,
                        INT4 *pi4RetValFsDsTCREDCurveId);
extern INT1                                                                            nmhSetFsDsTCNumFlowGroups (INT4 i4FsDsTrafficClassId,
                           INT4 i4SetValFsDsTCNumFlowGroups);
extern INT1                                                                            nmhTestv2FsDsTCREDCurveId (UINT4 *pu4ErrorCode, 
                           INT4 i4FsDsTrafficClassId,
                           INT4 i4TestValFsDsTCREDCurveId);
extern INT1                                                                            nmhSetFsDsTCREDCurveId (INT4 i4FsDsTrafficClassId,
                        INT4 i4SetValFsDsTCREDCurveId);
extern INT1                                                                            nmhTestv2FsDsTCEntryStatus (UINT4 *pu4ErrorCode,
                                INT4 i4FsDsTrafficClassId,
                                INT4 i4TestValFsDsTCEntryStatus);
extern INT1 nmhSetFsDsTCEntryStatus (INT4 i4FsDsTrafficClassId,
                                     INT4 i4SetValFsDsTCEntryStatus);
extern INT1                                                                            nmhTestv2FsDsTCPortNum (UINT4 *pu4ErrorCode, INT4 i4FsDsTrafficClassId,
                        INT4 i4TestValFsDsTCPortNum);
extern INT1                                                                            nmhSetFsDsTCPortNum (INT4 i4FsDsTrafficClassId,
                            INT4 i4SetValFsDsTCPortNum);
extern INT1
nmhGetFirstIndexFsDsREDConfigTable (INT4 *pi4FsDsREDCurveId);

extern INT1
nmhGetFsDsREDCurveStatus (INT4 i4FsDsREDCurveId,
                          INT4 *pi4RetValFsDsREDCurveStatus);
extern INT1
nmhGetNextIndexFsDsREDConfigTable (INT4 i4FsDsREDCurveId,
                                   INT4 *pi4NextFsDsREDCurveId);
extern INT1
nmhTestv2FsDsREDCurveStatus (UINT4 *pu4ErrorCode, INT4 i4FsDsREDCurveId,
                             INT4 i4TestValFsDsREDCurveStatus);
extern INT1
nmhSetFsDsREDCurveStatus (INT4 i4FsDsREDCurveId,
                          INT4 i4SetValFsDsREDCurveStatus);
extern INT1
nmhTestv2FsDsREDCurveStart (UINT4 *pu4ErrorCode, INT4 i4FsDsREDCurveId,
                            INT4 i4TestValFsDsREDCurveStart);
extern INT1
nmhTestv2FsDsREDCurveStop (UINT4 *pu4ErrorCode, INT4 i4FsDsREDCurveId,
                           INT4 i4TestValFsDsREDCurveStop);
extern INT1
nmhTestv2FsDsREDCurveQRange (UINT4 *pu4ErrorCode, INT4 i4FsDsREDCurveId,
                             INT4 i4TestValFsDsREDCurveQRange);
extern INT1
nmhTestv2FsDsREDCurveStopProbability (UINT4 *pu4ErrorCode,
                                      INT4 i4FsDsREDCurveId,
                                      INT4 i4TestValFsDsREDCurveStopProbability);
extern INT1
nmhSetFsDsREDCurveStart (INT4 i4FsDsREDCurveId, INT4 i4SetValFsDsREDCurveStart);
extern INT1
nmhSetFsDsREDCurveStop (INT4 i4FsDsREDCurveId, INT4 i4SetValFsDsREDCurveStop);
extern INT1
nmhSetFsDsREDCurveQRange (INT4 i4FsDsREDCurveId,
                          INT4 i4SetValFsDsREDCurveQRange);
extern INT1
nmhSetFsDsREDCurveStopProbability (INT4 i4FsDsREDCurveId,
                                   INT4 i4SetValFsDsREDCurveStopProbability);
extern INT1
nmhGetFsDsREDCurveStart (INT4 , INT4 *);
extern INT1
nmhGetFsDsREDCurveStop (INT4 , INT4 *);
extern INT1
nmhGetFsDsREDCurveQRange (INT4 ,INT4 *);
extern INT1
nmhGetFsDsREDCurveStopProbability (INT4 ,INT4 *);
#endif
#endif /* _ISSWEB_H */
