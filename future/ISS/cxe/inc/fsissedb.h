/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissedb.h,v 1.4 2008/08/22 09:33:36 prabuc-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSISSEDB_H
#define _FSISSEDB_H

UINT1 IssRateCtrlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 IssL2FilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 IssL3FilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fsisse [] ={1,3,6,1,4,1,2076,81,5,1};
tSNMP_OID_TYPE fsisseOID = {10, fsisse};


UINT4 IssRateCtrlIndex [ ] ={1,3,6,1,4,1,2076,81,4,1,1,1};
UINT4 IssRateCtrlDLFLimitValue [ ] ={1,3,6,1,4,1,2076,81,4,1,1,2};
UINT4 IssRateCtrlBCASTLimitValue [ ] ={1,3,6,1,4,1,2076,81,4,1,1,3};
UINT4 IssRateCtrlMCASTLimitValue [ ] ={1,3,6,1,4,1,2076,81,4,1,1,4};
UINT4 IssL2FilterNo [ ] ={1,3,6,1,4,1,2076,81,5,1,1,1};
UINT4 IssL2FilterEtherType [ ] ={1,3,6,1,4,1,2076,81,5,1,1,3};
UINT4 IssL2FilterProtocolType [ ] ={1,3,6,1,4,1,2076,81,5,1,1,4};
UINT4 IssL2FilterDstMacAddr [ ] ={1,3,6,1,4,1,2076,81,5,1,1,5};
UINT4 IssL2FilterSrcMacAddr [ ] ={1,3,6,1,4,1,2076,81,5,1,1,6};
UINT4 IssL2FilterVlanId [ ] ={1,3,6,1,4,1,2076,81,5,1,1,7};
UINT4 IssL2FilterInPort [ ] ={1,3,6,1,4,1,2076,81,5,1,1,8};
UINT4 IssL2FilterOutPort [ ] ={1,3,6,1,4,1,2076,81,5,1,1,9};
UINT4 IssL2FilterAction [ ] ={1,3,6,1,4,1,2076,81,5,1,1,10};
UINT4 IssL2FilterMatchCount [ ] ={1,3,6,1,4,1,2076,81,5,1,1,11};
UINT4 IssL2FilterStatus [ ] ={1,3,6,1,4,1,2076,81,5,1,1,12};
UINT4 IssL3FilterNo [ ] ={1,3,6,1,4,1,2076,81,6,1,1,1};
UINT4 IssL3FilterProtocol [ ] ={1,3,6,1,4,1,2076,81,6,1,1,2};
UINT4 IssL3FilterMessageType [ ] ={1,3,6,1,4,1,2076,81,6,1,1,3};
UINT4 IssL3FilterMessageCode [ ] ={1,3,6,1,4,1,2076,81,6,1,1,4};
UINT4 IssL3FilterDstIpAddr [ ] ={1,3,6,1,4,1,2076,81,6,1,1,5};
UINT4 IssL3FilterSrcIpAddr [ ] ={1,3,6,1,4,1,2076,81,6,1,1,6};
UINT4 IssL3FilterDstIpAddrMask [ ] ={1,3,6,1,4,1,2076,81,6,1,1,7};
UINT4 IssL3FilterSrcIpAddrMask [ ] ={1,3,6,1,4,1,2076,81,6,1,1,8};
UINT4 IssL3FilterMinDstProtPort [ ] ={1,3,6,1,4,1,2076,81,6,1,1,9};
UINT4 IssL3FilterMaxDstProtPort [ ] ={1,3,6,1,4,1,2076,81,6,1,1,10};
UINT4 IssL3FilterMinSrcProtPort [ ] ={1,3,6,1,4,1,2076,81,6,1,1,11};
UINT4 IssL3FilterMaxSrcProtPort [ ] ={1,3,6,1,4,1,2076,81,6,1,1,12};
UINT4 IssL3FilterVlan [ ] ={1,3,6,1,4,1,2076,81,6,1,1,13};
UINT4 IssL3FilterAckBit [ ] ={1,3,6,1,4,1,2076,81,6,1,1,14};
UINT4 IssL3FilterRstBit [ ] ={1,3,6,1,4,1,2076,81,6,1,1,15};
UINT4 IssL3FilterTos [ ] ={1,3,6,1,4,1,2076,81,6,1,1,16};
UINT4 IssL3FilterDscp [ ] ={1,3,6,1,4,1,2076,81,6,1,1,17};
UINT4 IssL3FilterAction [ ] ={1,3,6,1,4,1,2076,81,6,1,1,18};
UINT4 IssL3FilterMatchCount [ ] ={1,3,6,1,4,1,2076,81,6,1,1,19};
UINT4 IssL3FilterStatus [ ] ={1,3,6,1,4,1,2076,81,6,1,1,20};


tMbDbEntry fsisseMibEntry[]= {

{{12,IssRateCtrlIndex}, GetNextIndexIssRateCtrlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssRateCtrlTableINDEX, 1, 0, 0, NULL},

{{12,IssRateCtrlDLFLimitValue}, GetNextIndexIssRateCtrlTable, IssRateCtrlDLFLimitValueGet, IssRateCtrlDLFLimitValueSet, IssRateCtrlDLFLimitValueTest, IssRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssRateCtrlTableINDEX, 1, 0, 0, "0"},

{{12,IssRateCtrlBCASTLimitValue}, GetNextIndexIssRateCtrlTable, IssRateCtrlBCASTLimitValueGet, IssRateCtrlBCASTLimitValueSet, IssRateCtrlBCASTLimitValueTest, IssRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssRateCtrlTableINDEX, 1, 0, 0, "0"},

{{12,IssRateCtrlMCASTLimitValue}, GetNextIndexIssRateCtrlTable, IssRateCtrlMCASTLimitValueGet, IssRateCtrlMCASTLimitValueSet, IssRateCtrlMCASTLimitValueTest, IssRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssRateCtrlTableINDEX, 1, 0, 0, "0"},

{{12,IssL2FilterNo}, GetNextIndexIssL2FilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssL2FilterTableINDEX, 1, 0, 0, NULL},

{{12,IssL2FilterEtherType}, GetNextIndexIssL2FilterTable, IssL2FilterEtherTypeGet, IssL2FilterEtherTypeSet, IssL2FilterEtherTypeTest, IssL2FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL2FilterTableINDEX, 1, 0, 0, NULL},

{{12,IssL2FilterProtocolType}, GetNextIndexIssL2FilterTable, IssL2FilterProtocolTypeGet, IssL2FilterProtocolTypeSet, IssL2FilterProtocolTypeTest, IssL2FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssL2FilterTableINDEX, 1, 0, 0, "0"},

{{12,IssL2FilterDstMacAddr}, GetNextIndexIssL2FilterTable, IssL2FilterDstMacAddrGet, IssL2FilterDstMacAddrSet, IssL2FilterDstMacAddrTest, IssL2FilterTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, IssL2FilterTableINDEX, 1, 0, 0, NULL},

{{12,IssL2FilterSrcMacAddr}, GetNextIndexIssL2FilterTable, IssL2FilterSrcMacAddrGet, IssL2FilterSrcMacAddrSet, IssL2FilterSrcMacAddrTest, IssL2FilterTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, IssL2FilterTableINDEX, 1, 0, 0, NULL},

{{12,IssL2FilterVlanId}, GetNextIndexIssL2FilterTable, IssL2FilterVlanIdGet, IssL2FilterVlanIdSet, IssL2FilterVlanIdTest, IssL2FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL2FilterTableINDEX, 1, 0, 0, "0"},

{{12,IssL2FilterInPort}, GetNextIndexIssL2FilterTable, IssL2FilterInPortGet, IssL2FilterInPortSet, IssL2FilterInPortTest, IssL2FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL2FilterTableINDEX, 1, 0, 0, "0"},

{{12,IssL2FilterOutPort}, GetNextIndexIssL2FilterTable, IssL2FilterOutPortGet, IssL2FilterOutPortSet, IssL2FilterOutPortTest, IssL2FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL2FilterTableINDEX, 1, 0, 0, "0"},

{{12,IssL2FilterAction}, GetNextIndexIssL2FilterTable, IssL2FilterActionGet, IssL2FilterActionSet, IssL2FilterActionTest, IssL2FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssL2FilterTableINDEX, 1, 0, 0, "1"},

{{12,IssL2FilterMatchCount}, GetNextIndexIssL2FilterTable, IssL2FilterMatchCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IssL2FilterTableINDEX, 1, 0, 0, NULL},

{{12,IssL2FilterStatus}, GetNextIndexIssL2FilterTable, IssL2FilterStatusGet, IssL2FilterStatusSet, IssL2FilterStatusTest, IssL2FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssL2FilterTableINDEX, 1, 0, 1, NULL},

{{12,IssL3FilterNo}, GetNextIndexIssL3FilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssL3FilterTableINDEX, 1, 0, 0, NULL},

{{12,IssL3FilterProtocol}, GetNextIndexIssL3FilterTable, IssL3FilterProtocolGet, IssL3FilterProtocolSet, IssL3FilterProtocolTest, IssL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 0, 0, "255"},

{{12,IssL3FilterMessageType}, GetNextIndexIssL3FilterTable, IssL3FilterMessageTypeGet, IssL3FilterMessageTypeSet, IssL3FilterMessageTypeTest, IssL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 0, 0, "255"},

{{12,IssL3FilterMessageCode}, GetNextIndexIssL3FilterTable, IssL3FilterMessageCodeGet, IssL3FilterMessageCodeSet, IssL3FilterMessageCodeTest, IssL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 0, 0, "255"},

{{12,IssL3FilterDstIpAddr}, GetNextIndexIssL3FilterTable, IssL3FilterDstIpAddrGet, IssL3FilterDstIpAddrSet, IssL3FilterDstIpAddrTest, IssL3FilterTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 0, 0, NULL},

{{12,IssL3FilterSrcIpAddr}, GetNextIndexIssL3FilterTable, IssL3FilterSrcIpAddrGet, IssL3FilterSrcIpAddrSet, IssL3FilterSrcIpAddrTest, IssL3FilterTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 0, 0, NULL},

{{12,IssL3FilterDstIpAddrMask}, GetNextIndexIssL3FilterTable, IssL3FilterDstIpAddrMaskGet, IssL3FilterDstIpAddrMaskSet, IssL3FilterDstIpAddrMaskTest, IssL3FilterTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 0, 0, "4294967295"},

{{12,IssL3FilterSrcIpAddrMask}, GetNextIndexIssL3FilterTable, IssL3FilterSrcIpAddrMaskGet, IssL3FilterSrcIpAddrMaskSet, IssL3FilterSrcIpAddrMaskTest, IssL3FilterTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 0, 0, "4294967295"},

{{12,IssL3FilterMinDstProtPort}, GetNextIndexIssL3FilterTable, IssL3FilterMinDstProtPortGet, IssL3FilterMinDstProtPortSet, IssL3FilterMinDstProtPortTest, IssL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 0, 0, "0"},

{{12,IssL3FilterMaxDstProtPort}, GetNextIndexIssL3FilterTable, IssL3FilterMaxDstProtPortGet, IssL3FilterMaxDstProtPortSet, IssL3FilterMaxDstProtPortTest, IssL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 0, 0, "65535"},

{{12,IssL3FilterMinSrcProtPort}, GetNextIndexIssL3FilterTable, IssL3FilterMinSrcProtPortGet, IssL3FilterMinSrcProtPortSet, IssL3FilterMinSrcProtPortTest, IssL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 0, 0, "0"},

{{12,IssL3FilterMaxSrcProtPort}, GetNextIndexIssL3FilterTable, IssL3FilterMaxSrcProtPortGet, IssL3FilterMaxSrcProtPortSet, IssL3FilterMaxSrcProtPortTest, IssL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 0, 0, "65535"},

{{12,IssL3FilterVlan}, GetNextIndexIssL3FilterTable, IssL3FilterVlanGet, IssL3FilterVlanSet, IssL3FilterVlanTest, IssL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 0, 0, "0"},

{{12,IssL3FilterAckBit}, GetNextIndexIssL3FilterTable, IssL3FilterAckBitGet, IssL3FilterAckBitSet, IssL3FilterAckBitTest, IssL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 0, 0, "3"},

{{12,IssL3FilterRstBit}, GetNextIndexIssL3FilterTable, IssL3FilterRstBitGet, IssL3FilterRstBitSet, IssL3FilterRstBitTest, IssL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 0, 0, "3"},

{{12,IssL3FilterTos}, GetNextIndexIssL3FilterTable, IssL3FilterTosGet, IssL3FilterTosSet, IssL3FilterTosTest, IssL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 0, 0, "0"},

{{12,IssL3FilterDscp}, GetNextIndexIssL3FilterTable, IssL3FilterDscpGet, IssL3FilterDscpSet, IssL3FilterDscpTest, IssL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 0, 0, "0"},

{{12,IssL3FilterAction}, GetNextIndexIssL3FilterTable, IssL3FilterActionGet, IssL3FilterActionSet, IssL3FilterActionTest, IssL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 0, 0, "1"},

{{12,IssL3FilterMatchCount}, GetNextIndexIssL3FilterTable, IssL3FilterMatchCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IssL3FilterTableINDEX, 1, 0, 0, NULL},

{{12,IssL3FilterStatus}, GetNextIndexIssL3FilterTable, IssL3FilterStatusGet, IssL3FilterStatusSet, IssL3FilterStatusTest, IssL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 0, 1, NULL},
};
tMibData fsisseEntry = { 35, fsisseMibEntry };
#endif /* _FSISSEDB_H */

