/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsisselw.h,v 1.3 2007/02/01 14:54:08 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for IssRateCtrlTable. */
INT1
nmhValidateIndexInstanceIssRateCtrlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssRateCtrlTable  */

INT1
nmhGetFirstIndexIssRateCtrlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssRateCtrlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssRateCtrlDLFLimitValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssRateCtrlBCASTLimitValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssRateCtrlMCASTLimitValue ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssRateCtrlDLFLimitValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssRateCtrlBCASTLimitValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssRateCtrlMCASTLimitValue ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssRateCtrlDLFLimitValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssRateCtrlBCASTLimitValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssRateCtrlMCASTLimitValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for IssL2FilterTable. */
INT1
nmhValidateIndexInstanceIssL2FilterTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssL2FilterTable  */

INT1
nmhGetFirstIndexIssL2FilterTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssL2FilterTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssL2FilterEtherType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL2FilterProtocolType ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssL2FilterDstMacAddr ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetIssL2FilterSrcMacAddr ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetIssL2FilterVlanId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL2FilterInPort ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL2FilterOutPort ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL2FilterAction ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL2FilterMatchCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssL2FilterStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssL2FilterEtherType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL2FilterProtocolType ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssL2FilterDstMacAddr ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetIssL2FilterSrcMacAddr ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetIssL2FilterVlanId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL2FilterInPort ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL2FilterOutPort ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL2FilterAction ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL2FilterStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssL2FilterEtherType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL2FilterProtocolType ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssL2FilterDstMacAddr ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2IssL2FilterSrcMacAddr ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2IssL2FilterVlanId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL2FilterInPort ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL2FilterOutPort ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL2FilterAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL2FilterStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for IssL3FilterTable. */
INT1
nmhValidateIndexInstanceIssL3FilterTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssL3FilterTable  */

INT1
nmhGetFirstIndexIssL3FilterTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssL3FilterTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssL3FilterProtocol ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL3FilterMessageType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL3FilterMessageCode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL3FilterDstIpAddr ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssL3FilterSrcIpAddr ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssL3FilterDstIpAddrMask ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssL3FilterSrcIpAddrMask ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssL3FilterMinDstProtPort ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssL3FilterMaxDstProtPort ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssL3FilterMinSrcProtPort ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssL3FilterMaxSrcProtPort ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssL3FilterVlan ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL3FilterAckBit ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL3FilterRstBit ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL3FilterTos ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL3FilterDscp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL3FilterAction ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL3FilterMatchCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssL3FilterStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssL3FilterProtocol ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL3FilterMessageType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL3FilterMessageCode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL3FilterDstIpAddr ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssL3FilterSrcIpAddr ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssL3FilterDstIpAddrMask ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssL3FilterSrcIpAddrMask ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssL3FilterMinDstProtPort ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssL3FilterMaxDstProtPort ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssL3FilterMinSrcProtPort ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssL3FilterMaxSrcProtPort ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssL3FilterVlan ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL3FilterAckBit ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL3FilterRstBit ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL3FilterTos ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL3FilterDscp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL3FilterAction ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL3FilterStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssL3FilterProtocol ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL3FilterMessageType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL3FilterMessageCode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL3FilterDstIpAddr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssL3FilterSrcIpAddr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssL3FilterDstIpAddrMask ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssL3FilterSrcIpAddrMask ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssL3FilterMinDstProtPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssL3FilterMaxDstProtPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssL3FilterMinSrcProtPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssL3FilterMaxSrcProtPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssL3FilterVlan ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL3FilterAckBit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL3FilterRstBit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL3FilterTos ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL3FilterDscp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL3FilterAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL3FilterStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT4 IssSnmpLowValidateIndexPortRateTable (INT4 i4PortIndex);

INT4 IssValidateRateCtrlEntry (INT4 i4RateCtrlIndex);

INT4 IssSnmpLowGetFirstValidL3FilterTableIndex (INT4 *pi4FirstL3FilterIndex);

INT4 IssSnmpLowGetNextValidL3FilterTableIndex (INT4 i4IssL3FilterNo,
                                               INT4 *pi4NextL3FilterNo);
INT4 IssSnmpLowGetFirstValidL2FilterTableIndex (INT4 *pi4FirstL2FilterIndex);

INT4 IssSnmpLowGetNextValidL2FilterTableIndex (INT4 i4IssL2FilterNo,
                                               INT4 *pi4NextL2FilterNo);

