
#ifndef _ISSEXMACR_H
#define _ISSEXMACR_H

#define ISS_MAX_RATE_VALUE              65535
#define ISS_DEF_RATE_VALUE              500
#define ISS_MIN_RATE_VALUE              1

/* Macros for validattion */
#define ISS_MIN_PROTOCOL_ID             1536
#define ISS_MAX_PROTOCOL_ID             65535
#define ISS_MIN_L2FILTER_ID             0
#define ISS_MAX_L2FILTER_ID             ISS_MAX_L2_FILTERS
#define ISS_MIN_L3FILTER_ID             0
#define ISS_MAX_L3FILTER_ID             ISS_MAX_L3_FILTERS
#define ISS_DEFAULT_PROTOCOL_TYPE       0
#define ISS_DEF_FILTER_MASK             0
#define ISS_MIN_DSCP_VALUE              0
#define ISS_MAX_DSCP_VALUE              63
/* Max Standard L3 filter ID */
#define ISS_MAX_STD_L3FILTER_ID         (ISS_MAX_L3_FILTERS/2 + 5)

/* MEM Pool Id Definitions */
#define ISS_RATEENTRY_POOL_ID            gIssExGlobalInfo.IssRateCtrlPoolId
#define ISS_L2FILTERENTRY_POOL_ID        gIssExGlobalInfo.IssL2FilterPoolId
#define ISS_L3FILTERENTRY_POOL_ID        gIssExGlobalInfo.IssL3FilterPoolId

/* Filter List */
#define ISS_L2FILTER_LIST               gIssExGlobalInfo.IssL2FilterListHead
#define ISS_L2FILTER_LIST_HEAD          (&ISS_L2FILTER_LIST)->Tail

#define ISS_IS_L2FILTER_ID_VALID(ISSId) \
        (((ISSId <= ISS_MIN_L2FILTER_ID) || \
        (ISSId > ISS_MAX_L2FILTER_ID)) ? ISS_FALSE : ISS_TRUE)

#define ISS_IS_L3FILTER_ID_VALID(ISSId) \
        (((ISSId <= ISS_MIN_L3FILTER_ID) || \
        (ISSId > ISS_MAX_L3FILTER_ID)) ? ISS_FALSE : ISS_TRUE)

#define ISS_L2FILTERENTRY_MEMBLK_COUNT  ISS_MAX_L2_FILTERS
#define ISS_L3FILTERENTRY_MEMBLK_COUNT  ISS_MAX_L3_FILTERS

#define ISS_RATEENTRY_MEMBLK_SIZE       sizeof(tIssRateCtrlEntry)

#define ISS_L2FILTERENTRY_MEMBLK_SIZE   sizeof(tIssL2FilterEntry)

#define  ISS_CREATE_RATEENTRY_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_CREATE_L2FILTERENTRY_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,MEM_HEAP_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_CREATE_L3FILTERENTRY_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,MEM_HEAP_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_DELETE_RATEENTRY_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_DELETE_L2FILTERENTRY_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_DELETE_L3FILTERENTRY_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_RATEENTRY_ALLOC_MEM_BLOCK(ppu1Msg)\
         MemAllocateMemBlock(ISS_RATEENTRY_POOL_ID, (UINT1 **)ppu1Msg)

#define  ISS_L2FILTERENTRY_ALLOC_MEM_BLOCK(ppu1Msg)\
         MemAllocateMemBlock(ISS_L2FILTERENTRY_POOL_ID, (UINT1 **)ppu1Msg)

#define  ISS_L3FILTERENTRY_ALLOC_MEM_BLOCK(ppu1Msg)\
         MemAllocateMemBlock(ISS_L3FILTERENTRY_POOL_ID, (UINT1 **)ppu1Msg)

#define  ISS_RATEENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_RATEENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define  ISS_L2FILTERENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_L2FILTERENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define  ISS_L3FILTERENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_L3FILTERENTRY_POOL_ID, (UINT1 *)pu1Msg)

#endif  /* _ISSEXMACR_H */
