#ifdef WEBNM_WANTED
#include "issexweb.h"

/*********************************************************************
*  Function Name : IssProcessCustomPages
*  Description   : This function processes the Pages specific to
*                  target
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
INT4
IssProcessCustomPages (tHttp * pHttp)
{
    UINT4               u4Count;

    for (u4Count = 0; asIssTargetpages[u4Count].au1Page[0] != '\0'; u4Count++)
    {
        if (STRCMP (pHttp->ai1HtmlName, asIssTargetpages[u4Count].au1Page) == 0)
        {
            asIssTargetpages[u4Count].pfunctPtr (pHttp);
            return ISS_SUCCESS;
        }
    }
    return ISS_FAILURE;
}

/************************************************************************ 
*  Function Name   : IssRedirectMacFilterPage 
*  Description     : This function will redirect Mac Filter page 
*  Input           : pHttp - Pointer to http entry where the html file 
*                    name needs to be changed(copied) 
*  Output          : None 
*  Returns         : None 
************************************************************************/
VOID
IssRedirectMacFilterPage (tHttp * pHttp)
{
    STRCPY (pHttp->ai1HtmlName, "cxe_mac_filterconf.html");
}

/************************************************************************ 
*  Function Name   : IssRedirectDiffSrvPage 
*  Description     : This function will redirect Diffsrv page 
*  Input           : pHttp - Pointer to http entry where the html file 
*                    name needs to be changed(copied) 
*  Output          : None 
*  Returns         : None 
************************************************************************/
VOID
IssRedirectDiffSrvPage (tHttp * pHttp)
{
    STRCPY (pHttp->ai1HtmlName, "cxe_dfs_globalconf.html");
}

/*********************************************************************
*  Function Name : IssCxeProcessIPFilterConfPage 
*  Description   : This function processes the request coming for the IP 
*                  Filter Configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssCxeProcessIPFilterConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssCxeProcessIPFilterConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssCxeProcessIPFilterConfPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssCxeProcessIPStdFilterConfPage 
*  Description   : This function processes the request coming for the IP 
*                  standard Filter Configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssCxeProcessIPStdFilterConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssCxeProcessIPStdFilterConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssCxeProcessIPStdFilterConfPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssCxeProcessMACFilterConfPage 
*  Description   : This function processes the request coming for the MAC 
*                  Filter Configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssCxeProcessMACFilterConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssCxeProcessMACFilterConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssCxeProcessMACFilterConfPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssCxeProcessIPFilterConfPageSet
*  Description   : This function processes the set request for the IP
*                 filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssCxeProcessIPFilterConfPageSet (tHttp * pHttp)
{
    INT4                i4AckBit = 0, i4RstBit = 0, i4TOS = 0;
    UINT4               u4SrcIpAddr = 0, u4SrcAddrMask = 0, u4DestIpAddr =
        0, u4DestAddrMask = 0;
    UINT4               u4SrcPortFrom = 0, u4SrcPortTo = 0, u4DstPortFrom = 0;
    UINT4               u4DstPortTo = 0, u4Code = 0;
    UINT4               u4ErrorCode = 0, u4Protocol = 0, u4Type = 0;
    INT4                i4Action = 0, i4FilterNo = 0;

    UINT1               u1Apply = 0, u1RowStatus = ISS_CREATE_AND_WAIT;

    HttpGetValuebyName ("FltNo", pHttp->au1Value, pHttp->au1PostQuery);
    i4FilterNo = ATOI (pHttp->au1Value);

    /* Check whether the request is for ADD/DELETE */
    HttpGetValuebyName (ISS_ACTION, pHttp->au1Value, pHttp->au1PostQuery);

    ISS_LOCK ();

    /* check for delete */
    if (STRCMP (pHttp->au1Value, ISS_DELETE) == 0)
    {
        if (nmhTestv2IssL3FilterStatus (&u4ErrorCode, i4FilterNo, ISS_DESTROY)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "IP filter not available.");
            return;
        }

        if (nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "IP filter not available.");
            return;
        }

        ISS_UNLOCK ();

        IssCxeProcessIPFilterConfPageGet (pHttp);
        return;
    }

    if (STRCMP (pHttp->au1Value, ISS_APPLY) == 0)
    {
        u1Apply = 1;
        u1RowStatus = ISS_NOT_IN_SERVICE;
    }

    HttpGetValuebyName ("FltOption", pHttp->au1Value, pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    HttpGetValuebyName ("SrcIp", pHttp->au1Value, pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        u4SrcIpAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    }

    HttpGetValuebyName ("SrcMask", pHttp->au1Value, pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        u4SrcAddrMask = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    }

    HttpGetValuebyName ("DestIp", pHttp->au1Value, pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        u4DestIpAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    }

    HttpGetValuebyName ("DestMask", pHttp->au1Value, pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        u4DestAddrMask = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    }

    /* Determine the type of ACL from the filter number and process the SET
     * request
     */

    HttpGetValuebyName ("Protocol", pHttp->au1Value, pHttp->au1PostQuery);
    u4Protocol = ATOI (pHttp->au1Value);
    if (u4Protocol == ISS_PROT_OTHER)
    {
        HttpGetValuebyName ("Other", pHttp->au1Value, pHttp->au1PostQuery);
        u4Protocol = ATOI (pHttp->au1Value);
    }
    /* For Processing ICMP options. */
    if (u4Protocol == 1)
    {
        HttpGetValuebyName ("MsgCode", pHttp->au1Value, pHttp->au1PostQuery);
        u4Code = ATOI (pHttp->au1Value);

        HttpGetValuebyName ("MsgType", pHttp->au1Value, pHttp->au1PostQuery);
        u4Type = ATOI (pHttp->au1Value);
    }

    else
    {
        HttpGetValuebyName ("Tos", pHttp->au1Value, pHttp->au1PostQuery);
        i4TOS = ATOI (pHttp->au1Value);
    }

    /* For Processing TCP options. */
    if (u4Protocol == 6)
    {
        HttpGetValuebyName ("AckBit", pHttp->au1Value, pHttp->au1PostQuery);
        i4AckBit = ATOI (pHttp->au1Value);

        HttpGetValuebyName ("RstBit", pHttp->au1Value, pHttp->au1PostQuery);
        i4RstBit = ATOI (pHttp->au1Value);

    }

    /* For Processing TCP/UDP options. */
    if (u4Protocol == 17 || u4Protocol == 6)
    {
        HttpGetValuebyName ("SrcPortMin", pHttp->au1Value, pHttp->au1PostQuery);
        u4SrcPortFrom = ATOI (pHttp->au1Value);

        HttpGetValuebyName ("SrcPortMax", pHttp->au1Value, pHttp->au1PostQuery);
        u4SrcPortTo = ATOI (pHttp->au1Value);

        HttpGetValuebyName ("DstPortMin", pHttp->au1Value, pHttp->au1PostQuery);
        u4DstPortFrom = ATOI (pHttp->au1Value);

        HttpGetValuebyName ("DstPortMax", pHttp->au1Value, pHttp->au1PostQuery);
        u4DstPortTo = ATOI (pHttp->au1Value);
    }

    if (nmhTestv2IssL3FilterStatus (&u4ErrorCode, i4FilterNo,
                                    u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Invalid IP filter number.");
        return;
    }

    if ((i4Action != 0) && (nmhTestv2IssL3FilterAction
                            (&u4ErrorCode, i4FilterNo,
                             i4Action)) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the l3 filter action.");
        return;
    }

    if ((u4SrcIpAddr > 0) && (nmhTestv2IssL3FilterSrcIpAddr
                              (&u4ErrorCode, i4FilterNo, u4SrcIpAddr))
        == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (INT1 *)
                      "Unable to set the filter with this source IP Address.");
        return;
    }

    if ((u4SrcAddrMask > 0) && (nmhTestv2IssL3FilterSrcIpAddrMask
                                (&u4ErrorCode, i4FilterNo, u4SrcAddrMask)
                                == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (INT1 *)
                      "Unable to set the filter with this source Address Mask.");
        return;
    }

    if ((u4DestIpAddr > 0) && (nmhTestv2IssL3FilterDstIpAddr
                               (&u4ErrorCode, i4FilterNo, u4DestIpAddr)
                               == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (INT1 *)
                      "Unable to set the filter with this Destination IP Address.");
        return;
    }

    if ((u4DestAddrMask > 0) && (nmhTestv2IssL3FilterDstIpAddrMask
                                 (&u4ErrorCode, i4FilterNo, u4DestAddrMask)
                                 == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (INT1 *)
                      "Unable to set the filter with this Destination Address Mask.");
        return;
    }

    if ((u4Protocol != 0) && (nmhTestv2IssL3FilterProtocol
                              (&u4ErrorCode, i4FilterNo, u4Protocol))
        == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the l3 filter protocol.");
        return;
    }
    if (u4Protocol == 1)
    {
        if (nmhTestv2IssL3FilterMessageType (&u4ErrorCode, i4FilterNo, u4Type)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set ICMP MessageType.");
            return;
        }

        if (nmhTestv2IssL3FilterMessageCode (&u4ErrorCode, i4FilterNo, u4Code)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set ICMP MessageCode.");
            return;
        }
    }

    else
    {
        if (nmhTestv2IssL3FilterTos (&u4ErrorCode, i4FilterNo, i4TOS)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set TCP TOS.");
            return;
        }
    }
    if (u4Protocol == 6)
    {
        if (nmhTestv2IssL3FilterAckBit (&u4ErrorCode, i4FilterNo, i4AckBit)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set TCP Ack Bit.");
            return;
        }

        if (nmhTestv2IssL3FilterRstBit (&u4ErrorCode, i4FilterNo, i4RstBit)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set TCP Rst Bit.");
            return;
        }
    }
    if (u4Protocol == 17 || u4Protocol == 6)
    {
        if (nmhTestv2IssL3FilterMinSrcProtPort (&u4ErrorCode, i4FilterNo,
                                                u4SrcPortFrom) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set TCP/UDP Src Port Start.");
            return;
        }

        if (nmhTestv2IssL3FilterMaxSrcProtPort (&u4ErrorCode, i4FilterNo,
                                                u4SrcPortTo) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set TCP/UDP Src Port End.");
            return;
        }

        if (nmhTestv2IssL3FilterMinDstProtPort (&u4ErrorCode, i4FilterNo,
                                                u4DstPortFrom) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set TCP/UDP Dst Port Start.");
            return;
        }

        if (nmhTestv2IssL3FilterMaxDstProtPort (&u4ErrorCode, i4FilterNo,
                                                u4DstPortTo) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set TCP/UDP Dst Port End.");
            return;
        }
    }

    if (nmhSetIssL3FilterStatus (i4FilterNo, u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Entry already exists for this IP"
                      " filter number");
        return;
    }

    if ((i4Action != 0) && (nmhSetIssL3FilterAction (i4FilterNo, i4Action)
                            == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the l3 filter action.");
        return;
    }

    if ((u4SrcIpAddr > 0) && (nmhSetIssL3FilterSrcIpAddr
                              (i4FilterNo, u4SrcIpAddr) == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (INT1 *) "Unable to set the l3 Source Ip Address.");
        return;
    }

    if ((u4SrcAddrMask > 0) && (nmhSetIssL3FilterSrcIpAddrMask
                                (i4FilterNo, u4SrcAddrMask) == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (INT1 *) "Unable to set the l3 Source Address Mask.");
        return;
    }

    if ((u4DestIpAddr > 0) && (nmhSetIssL3FilterDstIpAddr
                               (i4FilterNo, u4DestIpAddr) == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (INT1 *) "Unable to set the l3 Destination Ip Address.");
        return;
    }

    if ((u4DestAddrMask > 0) && (nmhSetIssL3FilterDstIpAddrMask
                                 (i4FilterNo, u4DestAddrMask) == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (INT1 *)
                      "Unable to set the l3 Destination Address Mask.");
        return;
    }

    if ((u4Protocol != 0) && (nmhSetIssL3FilterProtocol
                              (i4FilterNo, u4Protocol) == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter priority.");
        return;
    }
    if (u4Protocol == 1)
    {
        if (nmhSetIssL3FilterMessageType (i4FilterNo, u4Type) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set ICMP MessageType.");
            return;
        }

        if (nmhSetIssL3FilterMessageCode (i4FilterNo, u4Code) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set ICMP MessageCode.");
            return;
        }
    }
    else
    {
        if (nmhSetIssL3FilterTos (i4FilterNo, i4TOS) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, "Unable to set TCP TOS.");
            return;
        }
    }
    if (u4Protocol == 6)
    {
        if (nmhSetIssL3FilterAckBit (i4FilterNo, i4AckBit) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set TCP Ack Bit.");
            return;
        }
        if (nmhSetIssL3FilterRstBit (i4FilterNo, i4RstBit) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set TCP Rst Bit.");
            return;
        }
    }
    if (u4Protocol == 17 || u4Protocol == 6)
    {
        if (nmhSetIssL3FilterMinSrcProtPort (i4FilterNo, u4SrcPortFrom)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set TCP/UDP Src Port Start.");
            return;
        }

        if (nmhSetIssL3FilterMaxSrcProtPort (i4FilterNo, u4SrcPortTo)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set TCP/UDP Src Port End.");
            return;
        }

        if (nmhSetIssL3FilterMinDstProtPort (i4FilterNo, u4DstPortFrom)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set TCP/UDP Dst Port Start.");
            return;
        }

        if (nmhSetIssL3FilterMaxDstProtPort (i4FilterNo, u4DstPortTo)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set TCP/UDP Dst Port End.");
            return;
        }
    }
    if (nmhSetIssL3FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, "Unable to set Filter Status");
        return;
    }

    ISS_UNLOCK ();

    IssCxeProcessIPFilterConfPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssCxeProcessIPFilterConfPageGet
*  Description   : This function processes the get request for the IP
*                  filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssCxeProcessIPFilterConfPageGet (tHttp * pHttp)
{
    INT4                i4FilterNo, i4CurrentFilterNo, i4RetVal, i4OutCome;
    INT4                i4Protocol;
    UINT4               u4RetVal, u4Temp;
    UINT1              *pu1String;
    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = pHttp->i4Write;

    WebnmRegisterLock (pHttp, IssLock, IssUnLock);

    ISS_LOCK ();

    i4OutCome = (INT4) (nmhGetFirstIndexIssL3FilterTable (&i4FilterNo));
    if (i4OutCome == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    do
    {
        pHttp->i4Write = u4Temp;

        /* Display only extended lists */
        if (i4FilterNo >= ACL_EXTENDED_START)
        {
            STRCPY (pHttp->au1KeyString, "FILTER_NUMBER");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4FilterNo);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            STRCPY (pHttp->au1KeyString, "ACTION");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssL3FilterAction (i4FilterNo, &i4RetVal);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "SOURCE_IP_ADDRESS");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssL3FilterSrcIpAddr (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "SOURCE_MASK");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssL3FilterSrcIpAddrMask (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "DESTINATION_IP_ADDRESS");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssL3FilterDstIpAddr (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "DESTINATION_MASK");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssL3FilterDstIpAddrMask (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "PROTOCOL");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssL3FilterProtocol (i4FilterNo, &i4Protocol);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4Protocol);

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "OTHER");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4Protocol);

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            if (i4Protocol == 1)
            {
                STRCPY (pHttp->au1KeyString, "CODE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssL3FilterMessageCode (i4FilterNo, &i4RetVal);
                SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "TYPE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssL3FilterMessageType (i4FilterNo, &i4RetVal);
                SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }
            else
            {
                STRCPY (pHttp->au1KeyString, "CODE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                STRCPY (pHttp->au1DataString, "");
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "TYPE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                STRCPY (pHttp->au1DataString, "");
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "TOS");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssL3FilterTos (i4FilterNo, &i4RetVal);
                SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }

            if (i4Protocol == 6)
            {
                STRCPY (pHttp->au1KeyString, "ACK_BIT");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssL3FilterAckBit (i4FilterNo, &i4RetVal);
                SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "RST_BIT");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssL3FilterRstBit (i4FilterNo, &i4RetVal);
                SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }

            if (i4Protocol == 6 || i4Protocol == 17)
            {
                STRCPY (pHttp->au1KeyString, "SOURCE_PORT_START");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssL3FilterMinSrcProtPort (i4FilterNo, &i4RetVal);
                SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "SOURCE_PORT_END");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssL3FilterMaxSrcProtPort (i4FilterNo, &i4RetVal);
                SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DESTINATION_PORT_START");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssL3FilterMinDstProtPort (i4FilterNo, &i4RetVal);
                SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DESTINATION_PORT_END");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssL3FilterMaxDstProtPort (i4FilterNo, &i4RetVal);
                SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }

            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        }
        i4CurrentFilterNo = i4FilterNo;
    }
    while (nmhGetNextIndexIssL3FilterTable
           (i4CurrentFilterNo, &i4FilterNo) == SNMP_SUCCESS);

    ISS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    if (i4FilterNo >= ACL_EXTENDED_START)
    {
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
    }
}

/*********************************************************************
*  Function Name : IssCxeProcessIPStdFilterConfPageGet 
*  Description   : This function processes the get request for the IP
*                  standard filter configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssCxeProcessIPStdFilterConfPageGet (tHttp * pHttp)
{
    INT4                i4FilterNo, i4CurrentFilterNo, i4RetVal, i4OutCome;
    UINT4               u4RetVal, u4Temp;
    UINT1              *pu1String;

    pHttp->i4Write = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = pHttp->i4Write;
    WebnmRegisterLock (pHttp, IssLock, IssUnLock);

    ISS_LOCK ();

    i4OutCome = (INT4) (nmhGetFirstIndexIssL3FilterTable (&i4FilterNo));
    if ((i4OutCome == SNMP_FAILURE) || (i4FilterNo >= ACL_EXTENDED_START))
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    do
    {
        /* Display only if the ACL number is in the range 1 to 1000 */

        if (i4FilterNo < ACL_EXTENDED_START)
        {
            pHttp->i4Write = u4Temp;
            STRCPY (pHttp->au1KeyString, "FILTER_NUMBER");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4FilterNo);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "FILTER_OPTION");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssL3FilterAction (i4FilterNo, &i4RetVal);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "SOURCE_IP_ADDRESS");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssL3FilterSrcIpAddr (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "SOURCE_MASK");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssL3FilterSrcIpAddrMask (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "DESTINATION_IP_ADDRESS");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssL3FilterDstIpAddr (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "DESTINATION_MASK");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssL3FilterDstIpAddrMask (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        }

        i4CurrentFilterNo = i4FilterNo;
    }
    while (nmhGetNextIndexIssL3FilterTable
           (i4CurrentFilterNo, &i4FilterNo) == SNMP_SUCCESS);

    ISS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : IssCxeProcessIPStdFilterConfPageSet 
*  Description   : This function processes the set request for the IP
*                 standard filter configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None 
*********************************************************************/

VOID
IssCxeProcessIPStdFilterConfPageSet (tHttp * pHttp)
{
    UINT4               u4SrcIpAddr = 0, u4SrcAddrMask = 0, u4DestIpAddr =
        0, u4DestAddrMask = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4Action = 0, i4FilterNo = 0;
    UINT1               u1Apply = 0, u1RowStatus = ISS_CREATE_AND_WAIT;

    STRCPY (pHttp->au1Name, "FILTER_NUMBER");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4FilterNo = ATOI (pHttp->au1Value);
    /* Check whether the request is for ADD/DELETE */
    HttpGetValuebyName (ISS_ACTION, pHttp->au1Value, pHttp->au1PostQuery);

    ISS_LOCK ();
    /* check for delete */
    if (STRCMP (pHttp->au1Value, ISS_DELETE) == 0)
    {
        if (nmhTestv2IssL3FilterStatus (&u4ErrorCode, i4FilterNo, ISS_DESTROY)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, "ACL with this number not available.");
            return;
        }

        if (nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, "ACL with this number not available.");
            return;
        }
        ISS_UNLOCK ();
        IssCxeProcessIPStdFilterConfPageGet (pHttp);
        return;
    }

    if (STRCMP (pHttp->au1Value, ISS_APPLY) == 0)
    {
        u1Apply = 1;
        u1RowStatus = ISS_NOT_IN_SERVICE;
    }
    STRCPY (pHttp->au1Name, "FILTER_OPTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "SOURCE_ADDRESS");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        u4SrcIpAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));

    STRCPY (pHttp->au1Name, "SOURCE_MASK");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        u4SrcAddrMask = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));

    STRCPY (pHttp->au1Name, "DESTINATION_ADDRESS");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        u4DestIpAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));

    STRCPY (pHttp->au1Name, "DESTINATION_MASK");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        u4DestAddrMask = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));

    if (nmhTestv2IssL3FilterStatus (&u4ErrorCode, i4FilterNo,
                                    u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, "Invalid ACL number.");
        return;
    }

    STRCPY (pHttp->au1Name, "FILTER_OPTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    if ((i4Action != 0) && (nmhTestv2IssL3FilterAction
                            (&u4ErrorCode, i4FilterNo,
                             i4Action)) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, "Invalid standard ACL filter action.");
        return;
    }

    if ((u4SrcIpAddr > 0) && (nmhTestv2IssL3FilterSrcIpAddr
                              (&u4ErrorCode, i4FilterNo, u4SrcIpAddr))
        == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Invalid source IP address");
        return;
    }

    if ((u4SrcAddrMask > 0) && (nmhTestv2IssL3FilterSrcIpAddrMask
                                (&u4ErrorCode, i4FilterNo, u4SrcAddrMask)
                                == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, "Invalid source IP address Mask.");
        return;
    }
    if ((u4DestIpAddr > 0) && (nmhTestv2IssL3FilterDstIpAddr
                               (&u4ErrorCode, i4FilterNo, u4DestIpAddr)
                               == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, "Invalid destination IP Address.");
        return;
    }

    if ((u4DestAddrMask > 0) && (nmhTestv2IssL3FilterDstIpAddrMask
                                 (&u4ErrorCode, i4FilterNo, u4DestAddrMask)
                                 == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, "Invalid destination IP address mask");
        return;
    }

    if (nmhSetIssL3FilterStatus (i4FilterNo, u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, "Entry already exists for this IP"
                      " filter number");
        return;
    }
    if ((i4Action != 0) && (nmhSetIssL3FilterAction (i4FilterNo, i4Action)
                            == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, "Unable to set the Standard ACL  filter action.");
        return;
    }

    if ((u4SrcIpAddr > 0) && (nmhSetIssL3FilterSrcIpAddr
                              (i4FilterNo, u4SrcIpAddr) == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, "Unable to set the ACL source IP address.");
        return;
    }
    if ((u4SrcAddrMask > 0) && (nmhSetIssL3FilterSrcIpAddrMask
                                (i4FilterNo, u4SrcAddrMask) == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, "Unable to set ACL source address mask.");
        return;
    }

    if ((u4DestIpAddr > 0) && (nmhSetIssL3FilterDstIpAddr
                               (i4FilterNo, u4DestIpAddr) == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, "Unable to set ACL destination IP address.");
        return;
    }
    if ((u4DestAddrMask > 0) && (nmhSetIssL3FilterDstIpAddrMask
                                 (i4FilterNo, u4DestAddrMask) == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, "Unable to set the destination IP address mask.");
        return;
    }

    if (nmhSetIssL3FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set Filter Status");
        return;
    }

    ISS_UNLOCK ();
    IssCxeProcessIPStdFilterConfPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssCxeProcessMACFilterConfPageGet
*  Description   : This function processes the get request for the MAC
*                  filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssCxeProcessMACFilterConfPageGet (tHttp * pHttp)
{
    INT4                i4CurrentFilterNo, i4FilterNo, i4RetVal, i4OutCome;
    UINT4               u4RetVal, u4Temp;
    UINT1               au1String[20];
    tMacAddr            SrcMacAddr, DestMacAddr;

    pHttp->i4Write = 0;

    IssPrintAvailableVlans (pHttp);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = pHttp->i4Write;

    WebnmRegisterLock (pHttp, IssLock, IssUnLock);

    ISS_LOCK ();
    i4OutCome = (INT4) (nmhGetFirstIndexIssL2FilterTable (&i4FilterNo));
    if (i4OutCome == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    do
    {
        pHttp->i4Write = u4Temp;
        STRCPY (pHttp->au1KeyString, "FILTER_NUMBER");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4FilterNo);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "SOURCE_MAC");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssL2FilterSrcMacAddr (i4FilterNo, &SrcMacAddr);
        IssPrintMacAddress (SrcMacAddr, au1String);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "DESTINATION_MAC");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssL2FilterDstMacAddr (i4FilterNo, &DestMacAddr);
        IssPrintMacAddress (DestMacAddr, au1String);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "FILTER_OPTION");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssL2FilterAction (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);

        IssPrintAvailableVlans (pHttp);
        STRCPY (pHttp->au1KeyString, "VLAN_ID");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmRegisterLock (pHttp, IssLock, IssUnLock);
        ISS_LOCK ();
        nmhGetIssL2FilterVlanId (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "IN_PORT");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssL2FilterInPort (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OUT_PORT");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssL2FilterOutPort (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "ENCAPTYPE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssL2FilterEtherType (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "PROTOCOL");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssL2FilterProtocolType (i4FilterNo, &u4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OTHER");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        i4CurrentFilterNo = i4FilterNo;
    }
    while (nmhGetNextIndexIssL2FilterTable
           (i4CurrentFilterNo, &i4FilterNo) == SNMP_SUCCESS);

    ISS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : IssCxeProcessMACFilterConfPageSet
*  Description   : This function processes the set request for the MAC
*                  filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssCxeProcessMACFilterConfPageSet (tHttp * pHttp)
{
    INT4                i4Action, i4FilterNo, i4EtherType;
    INT4                i4InPort, i4OutPort;
    INT4                i4VlanId;
    UINT4               u4ErrorCode, u4Protocol;
    tMacAddr            SrcMacAddr, DestMacAddr;
    UINT1               u1RowStatus = ISS_CREATE_AND_WAIT;
    UINT1               au1ZeroMac[ISS_MAC_LEN];
    UINT1               u1Apply = 0;

    MEMSET (au1ZeroMac, 0, ISS_MAC_LEN);

    HttpGetValuebyName ("FILTER_NUMBER", pHttp->au1Value, pHttp->au1PostQuery);
    i4FilterNo = ATOI (pHttp->au1Value);

    /* Check whether the request is for ADD/DELETE */
    HttpGetValuebyName (ISS_ACTION, pHttp->au1Value, pHttp->au1PostQuery);

    ISS_LOCK ();
    /* check for delete */
    if (STRCMP (pHttp->au1Value, ISS_DELETE) == 0)
    {
        if (nmhTestv2IssL2FilterStatus (&u4ErrorCode, i4FilterNo, ISS_DESTROY)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "MAC filter not available.");
            return;
        }

        if (nmhSetIssL2FilterStatus (i4FilterNo, ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "unable to set Mac Filter Status.");
            return;
        }
        ISS_UNLOCK ();
        IssCxeProcessMACFilterConfPageGet (pHttp);
        return;
    }

    if (STRCMP (pHttp->au1Value, ISS_APPLY) == 0)
    {
        u1Apply = 1;
        u1RowStatus = ISS_NOT_IN_SERVICE;
    }

    HttpGetValuebyName ("SOURCE_MAC", pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    StrToMac (pHttp->au1Value, SrcMacAddr);

    HttpGetValuebyName ("DESTINATION_MAC", pHttp->au1Value,
                        pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    StrToMac (pHttp->au1Value, DestMacAddr);

    HttpGetValuebyName ("FILTER_OPTION", pHttp->au1Value, pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    HttpGetValuebyName ("VLAN_ID", pHttp->au1Value, pHttp->au1PostQuery);
    i4VlanId = ATOI (pHttp->au1Value);

    HttpGetValuebyName ("IN_PORT", pHttp->au1Value, pHttp->au1PostQuery);
    i4InPort = ATOI (pHttp->au1Value);

    HttpGetValuebyName ("OUT_PORT", pHttp->au1Value, pHttp->au1PostQuery);
    i4OutPort = ATOI (pHttp->au1Value);

    HttpGetValuebyName ("ENCAPTYPE", pHttp->au1Value, pHttp->au1PostQuery);
    i4EtherType = ATOI (pHttp->au1Value);

    HttpGetValuebyName ("PROTOCOL", pHttp->au1Value, pHttp->au1PostQuery);
    u4Protocol = ATOI (pHttp->au1Value);

    if (u4Protocol == OTHER)
    {
        HttpGetValuebyName ("OTHER", pHttp->au1Value, pHttp->au1PostQuery);
        u4Protocol = ATOI (pHttp->au1Value);
    }
    if (nmhTestv2IssL2FilterStatus (&u4ErrorCode, i4FilterNo,
                                    u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Invalid MAC filter number.");
        return;
    }

    if (nmhTestv2IssL2FilterAction (&u4ErrorCode, i4FilterNo, i4Action)
        == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Invalid l2 filter action.");
        return;
    }

    if (MEMCMP (au1ZeroMac, SrcMacAddr, ISS_MAC_LEN))
    {
        if (nmhTestv2IssL2FilterSrcMacAddr (&u4ErrorCode, i4FilterNo,
                                            SrcMacAddr) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Invalid source MAC Address.");
            return;
        }
    }

    if (MEMCMP (au1ZeroMac, DestMacAddr, ISS_MAC_LEN))
    {
        if (nmhTestv2IssL2FilterDstMacAddr (&u4ErrorCode, i4FilterNo,
                                            DestMacAddr) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) " Invalid destination MAC Address.");
            return;
        }
    }

    if ((u4Protocol != 0) && (nmhTestv2IssL2FilterProtocolType
                              (&u4ErrorCode, i4FilterNo, u4Protocol)
                              == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Invalid protocol type!");
        return;
    }

    if ((i4EtherType != 0) && (nmhTestv2IssL2FilterEtherType
                               (&u4ErrorCode, i4FilterNo, i4EtherType)
                               == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Invalid ether type");
        return;
    }

    if ((i4VlanId != 0) && (nmhTestv2IssL2FilterVlanId
                            (&u4ErrorCode, i4FilterNo,
                             i4VlanId) == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Invalid VLAN ID.");
        return;
    }

    if (nmhTestv2IssL2FilterInPort (&u4ErrorCode, i4FilterNo, i4InPort)
        == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Invalid In port");
        return;
    }

    if (nmhTestv2IssL2FilterOutPort (&u4ErrorCode, i4FilterNo, i4OutPort)
        == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Invalid Out port");
        return;
    }

    if (nmhSetIssL2FilterStatus (i4FilterNo, u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Entry already exists for this MAC"
                      " filter number");
        return;
    }

    if (nmhSetIssL2FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        if (u1Apply != 1)
        {
            nmhSetIssL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the l2 filter action.");
        return;
    }

    if (MEMCMP (au1ZeroMac, SrcMacAddr, ISS_MAC_LEN))
    {
        if (nmhSetIssL2FilterSrcMacAddr (i4FilterNo, SrcMacAddr)
            == SNMP_FAILURE)
        {
            if (u1Apply != 1)
            {
                nmhSetIssL2FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set the source MAC Address.");
            return;
        }
    }

    if (MEMCMP (au1ZeroMac, DestMacAddr, ISS_MAC_LEN))
    {
        if (nmhSetIssL2FilterDstMacAddr
            (i4FilterNo, DestMacAddr) == SNMP_FAILURE)
        {
            if (u1Apply != 1)
            {
                nmhSetIssL2FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *)
                          "Unable to set the Destination MAC Address.");
            return;
        }
    }

    if ((u4Protocol != 0) && (nmhSetIssL2FilterProtocolType
                              (i4FilterNo, u4Protocol) == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter protocol type.");
        return;
    }

    if ((i4EtherType != 0) && (nmhSetIssL2FilterEtherType
                               (i4FilterNo, i4EtherType) == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter ether type.");
        return;
    }

    if ((i4VlanId != 0) && (nmhSetIssL2FilterVlanId
                            (i4FilterNo, i4VlanId) == SNMP_FAILURE))
    {
        if (u1Apply != 1)
        {
            nmhSetIssL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter VLAN ID.");
        return;
    }

    if (nmhSetIssL2FilterInPort (i4FilterNo, i4InPort) == SNMP_FAILURE)
    {
        if (u1Apply != 1)
        {
            nmhSetIssL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter In Port.");
        return;
    }

    if (nmhSetIssL2FilterOutPort (i4FilterNo, i4OutPort) == SNMP_FAILURE)
    {
        if (u1Apply != 1)
        {
            nmhSetIssL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter Out Port.");
        return;
    }

    if (nmhSetIssL2FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        if (u1Apply != 1)
        {
            nmhSetIssL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set L2 filter status.");
        return;
    }
    ISS_UNLOCK ();
    IssCxeProcessMACFilterConfPageGet (pHttp);
}

#ifdef DIFFSRV_WANTED

/*********************************************************************
 *  Function Name : IssCxeProcessDfsPolicyMapPage
 *  Description   : This function processes the request coming for the 
 *                  DiffSrv Policy Map page.
 *                     
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssCxeProcessDfsPolicyMapPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssCxeProcessDfsPolicyMapPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssCxeProcessDfsPolicyMapPageSet (pHttp);
    }
    return;
}

/***************************************************************************
 *  Function Name : IssCxeProcessDfsPolicyMapPageGet
 *  Description   : This function processes the Get request coming for the 
 *                  Diff Server Policy Map Page.
 *                     
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *****************************************************************************/
VOID
IssCxeProcessDfsPolicyMapPageGet (tHttp * pHttp)
{
    INT4                i4Retval;
    INT1                i1OutCome = 0;
    INT4                i4PolicyId, i4NextPolicyId = 0;
    UINT4               u4Temp = 0;
    tDiffServWebClfrData PolicyMapData;

    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    WebnmRegisterLock (pHttp, DfsLock, DfsUnLock);

    DFS_LOCK ();

    i1OutCome = nmhGetFirstIndexFsDiffServClfrTable (&i4PolicyId);

    if (i1OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        DFS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        return;
    }
    u4Temp = pHttp->i4Write;
    do
    {
        i1OutCome = nmhGetFsDiffServClfrStatus (i4PolicyId, &i4Retval);
        if (i4Retval != ACTIVE)
        {
            i4NextPolicyId = i4PolicyId;
            continue;
        }
        pHttp->i4Write = u4Temp;
        WebnmSendString (pHttp, "POLICY_MAPID");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4PolicyId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, "POLICY_MAPID");
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDiffServClfrMFClfrId (i4PolicyId, &i4Retval);
        WebnmSendString (pHttp, "CLFR_ID");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Retval);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDiffServClfrTrafficClassId (i4PolicyId, &i4Retval);
        WebnmSendString (pHttp, "TRAFFIC_CLASS");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Retval);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        if (DsWebnmGetPolicyMapEntry (i4PolicyId, &PolicyMapData)
            == SNMP_FAILURE)
        {
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            i4NextPolicyId = i4PolicyId;
            continue;
        }
        STRCPY (pHttp->au1KeyString, "INPROF_ACT_TYPE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                 PolicyMapData.au1DsClfrInProfActionType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "INPROF_ACT_VALUE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                 PolicyMapData.au1DsClfrInProfActionValue);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        i4NextPolicyId = i4PolicyId;
    }
    while (nmhGetNextIndexFsDiffServClfrTable (i4NextPolicyId,
                                               &i4PolicyId) == SNMP_SUCCESS);

    DFS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));

}

/*********************************************************************
 *  Function Name : IssCxeProcessDfsPolicyMapPageSet
 *  Description   : This function processes the request coming for the 
 *                  Diff Server Policy Map Page.
 *                     
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ***********************************************************************/
VOID
IssCxeProcessDfsPolicyMapPageSet (tHttp * pHttp)
{
    tDiffServWebSetClfrEntry ClfrEntry;
    UINT1               au1ErrStr[255];
    INT4                i4PolicyMapId;
    INT4                i4ClassMapId;
    INT4                i4InProfActionId;
    INT4                i4FirstClassMapId;
    INT4                i4NextClassMapId;
    INT4                i4MFCId;
    INT4                i4Status;
    INT4                i4DsStatus;
    INT4                i4TrafficClass;
    UINT4               u4ErrorCode;
    UINT4               u4InDscp = 0;
    UINT4               u4InPrec = 0;
    UINT4               u4CosValue = 0;
    UINT4               u4InProfActType = 0;
    UINT1               u1RowStatus = ISS_CREATE_AND_WAIT;

    /* Get the Policy-Map ID */
    HttpGetValuebyName ("POLICY_MAPID", pHttp->au1Value, pHttp->au1PostQuery);
    i4PolicyMapId = ATOI (pHttp->au1Value);

    DFS_LOCK ();

    nmhGetFsDsStatus (&i4DsStatus);
    if (i4DsStatus == 2)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "DiffServer not enabled.");
        return;
    }
    /* Check whether the request is for ADD/DELETE */
    HttpGetValuebyName (ISS_ACTION, pHttp->au1Value, pHttp->au1PostQuery);

    /* check for delete */
    if (STRCMP (pHttp->au1Value, ISS_DELETE) == 0)
    {
        if (nmhGetFsDiffServClfrStatus
            (i4PolicyMapId, &i4Status) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Policy Map entry not available.");
            return;
        }

        if ((i4Status == ISS_ACTIVE) &&
            (nmhSetFsDiffServClfrStatus (i4PolicyMapId,
                                         ISS_NOT_IN_SERVICE) == SNMP_FAILURE))
        {
            DFS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Cannot modify Policy Map entry status.");
            return;
        }

        nmhGetFsDiffServClfrInProActionId (i4PolicyMapId, &i4InProfActionId);
        if (i4InProfActionId != 0)
        {
            if (nmhGetFsDiffServInProfileActionStatus
                (i4InProfActionId, &i4Status) != SNMP_FAILURE)
            {

                if (nmhSetFsDiffServInProfileActionStatus
                    (i4InProfActionId, ISS_DESTROY) == SNMP_FAILURE)
                {
                    DFS_UNLOCK ();
                    IssSendError (pHttp,
                                  (INT1 *) "In-Profile Id does not exists");
                    return;
                }
            }
        }

        if (nmhSetFsDiffServClfrStatus (i4PolicyMapId,
                                        ISS_DESTROY) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Cannot delete Policy Map entry.");
            return;
        }
        DFS_UNLOCK ();
        IssCxeProcessDfsPolicyMapPageGet (pHttp);
        return;
    }

    /* Checking with Policy-Map ID */
    if (nmhTestv2FsDiffServClfrStatus
        (&u4ErrorCode, i4PolicyMapId, u1RowStatus) == SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Invalid Policy Map ID.");
        return;
    }

    STRCPY (pHttp->au1Name, "CLFR_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4ClassMapId = ATOI (pHttp->au1Value);

    /* Check if class map exists */
    if (nmhGetFsDiffServMultiFieldClfrStatus
        (i4ClassMapId, &i4Status) == SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Invalid Class Map ID");
        return;
    }

    /* Check if the class-map already allocated to another policy-map.
     * We can't allocate the same class-map to different policy-map, because
     * Class-Map id == Datapath id */
    if (nmhGetFirstIndexFsDiffServClfrTable (&i4FirstClassMapId)
        != SNMP_FAILURE)
    {
        /* For the first time */
        i4NextClassMapId = i4FirstClassMapId;
        do
        {
            nmhGetFsDiffServClfrMFClfrId (i4NextClassMapId, &i4MFCId);
            if (i4MFCId == i4ClassMapId)
            {
                DFS_UNLOCK ();
                IssSendError (pHttp,
                              (INT1 *)
                              "Class map already allocated to another policy-map");
                return;
            }
            i4FirstClassMapId = i4NextClassMapId;
        }
        while (nmhGetNextIndexFsDiffServClfrTable
               (i4FirstClassMapId, &i4NextClassMapId) != SNMP_FAILURE);
    }

    HttpGetValuebyName ("TRAFFIC_CLASS", pHttp->au1Value, pHttp->au1PostQuery);
    i4TrafficClass = ATOI (pHttp->au1Value);

    /* Check if Traffic Class exists */
    if (nmhGetFsDsTCEntryStatus (i4TrafficClass, &i4Status) == SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        return;
    }

    HttpGetValuebyName ("INPROF_ACT_TYPE", pHttp->au1Value,
                        pHttp->au1PostQuery);
    u4InProfActType = ATOI (pHttp->au1Value);
    HttpGetValuebyName ("INPROF_ACT_VALUE", pHttp->au1Value,
                        pHttp->au1PostQuery);

    if (u4InProfActType == 1)
    {
        u4InDscp = ATOI (pHttp->au1Value);
    }
    else if (u4InProfActType == 2)
    {
        u4InPrec = ATOI (pHttp->au1Value);
    }
    else if (u4InProfActType == 3)
    {
        u4CosValue = ATOI (pHttp->au1Value);
    }

    MEMSET (&ClfrEntry, 0, sizeof (tDiffServWebSetClfrEntry));
    ClfrEntry.i4DsClfrId = i4PolicyMapId;
    ClfrEntry.i4DsClfrMFClfrId = i4ClassMapId;
    ClfrEntry.i4DsClfrInProActionId = i4PolicyMapId;
    ClfrEntry.u4InProfActType = u4InProfActType;
    ClfrEntry.u4InDscp = u4InDscp;
    ClfrEntry.u4InPrec = u4InPrec;
    ClfrEntry.u4CosValue = u4CosValue;
    ClfrEntry.i4DsClfrTrafficClassId = i4TrafficClass;

    if (DsWebnmSetPolicyMapEntry (&ClfrEntry, au1ErrStr) == SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) au1ErrStr);
        IssCxeProcessDfsPolicyMapPageGet (pHttp);
        return;
    }

    DFS_UNLOCK ();
    IssCxeProcessDfsPolicyMapPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssCxeProcessDfsTrafficClassPage 
*  Description   : This function processes the request coming for the  
*                  traffic class Configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssCxeProcessDfsTrafficClassPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssCxeProcessDfsTrafficClassPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssCxeProcessDfsTrafficClassPageSet (pHttp);
    }
}

/***************************************************************************
 *  Function Name : IssCxeProcessDfsTrafficClassPageGet
 *  Description   : This function processes the Get request coming for the 
 *                  Diff Server traffic class Page.
 *                     
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *****************************************************************************/
VOID
IssCxeProcessDfsTrafficClassPageGet (tHttp * pHttp)
{
    INT4                i4Retval;
    INT1                i1OutCome = 0;
    INT4                i4TrafficClassId, i4NextTrafficClassId;
    UINT4               u4Temp = 0;

    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    WebnmRegisterLock (pHttp, DfsLock, DfsUnLock);

    DFS_LOCK ();

    i1OutCome = nmhGetFirstIndexFsDsTrafficClassConfigTable (&i4TrafficClassId);

    if (i1OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        DFS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        return;
    }
    u4Temp = pHttp->i4Write;
    do
    {
        i1OutCome = nmhGetFsDsTCEntryStatus (i4TrafficClassId, &i4Retval);
        if (i4Retval != ACTIVE)
        {
            i4NextTrafficClassId = i4TrafficClassId;
            continue;
        }
        pHttp->i4Write = u4Temp;
        WebnmSendString (pHttp, "TC_ID");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4TrafficClassId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, "TC_ID");
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDsTCPortNum (i4TrafficClassId, &i4Retval);
        WebnmSendString (pHttp, "OUT_PORT");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Retval);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDsTCMaxBandwidth (i4TrafficClassId, &i4Retval);
        WebnmSendString (pHttp, "MAX_BW");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Retval);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDsTCGuaranteedBandwidth (i4TrafficClassId, &i4Retval);
        WebnmSendString (pHttp, "GRNTD_BW");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Retval);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDsTCDropLevel1 (i4TrafficClassId, &i4Retval);
        WebnmSendString (pHttp, "DRP_LEVEL_1");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Retval);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDsTCDropLevel2 (i4TrafficClassId, &i4Retval);
        WebnmSendString (pHttp, "DRP_LEVEL_2");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Retval);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDsTCDropLevel3 (i4TrafficClassId, &i4Retval);
        WebnmSendString (pHttp, "DRP_LEVEL_3");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Retval);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDsTCDropAtMaxBandwidth (i4TrafficClassId, &i4Retval);
        WebnmSendString (pHttp, "DRP_AT_MAX");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Retval);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDsTCWeightFactor (i4TrafficClassId, &i4Retval);
        WebnmSendString (pHttp, "WGT_FACTOR");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Retval);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDsTCWeightShift (i4TrafficClassId, &i4Retval);
        WebnmSendString (pHttp, "WGT_SHIFT");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Retval);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDsTCNumFlowGroups (i4TrafficClassId, &i4Retval);
        WebnmSendString (pHttp, "FLOW_GRPS");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Retval);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDsTCREDCurveId (i4TrafficClassId, &i4Retval);
        WebnmSendString (pHttp, "RED_CURVE_ID");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Retval);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        i4NextTrafficClassId = i4TrafficClassId;
    }
    while (nmhGetNextIndexFsDsTrafficClassConfigTable (i4NextTrafficClassId,
                                                       &i4TrafficClassId) ==
           SNMP_SUCCESS);

    DFS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 *  Function Name : ConfigureDfsTrafficClassTableObjects
 *  Description   : This function configures the traffic class table 
 *                  objects
 *                     
 *  Input(s)      : pDSTraffClassEntry - Pointer to the Traffic Class Entry
 *                  structure.
 *  Output(s)     : None.
 *  Return Values : SNMP_SUCCESS or SNMP_FAILURE
 ***********************************************************************/
INT4
ConfigureDfsTrafficClassTableObjects (tHttp * pHttp,
                                      tDiffServTraffClassEntry
                                      * pDSTraffClassEntry)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsDsTCMaxBandwidth (&u4ErrCode,
                                     pDSTraffClassEntry->i4TraffClassId,
                                     pDSTraffClassEntry->i4MaxBw) ==
        SNMP_FAILURE)
    {
        IssSendError (pHttp, " Invalid Max Bw");
        return SNMP_FAILURE;
    }

    if (nmhSetFsDsTCMaxBandwidth (pDSTraffClassEntry->i4TraffClassId,
                                  pDSTraffClassEntry->i4MaxBw) == SNMP_FAILURE)
    {
        IssSendError (pHttp, " set max Bw failed");
        return SNMP_FAILURE;
    }

    if (nmhTestv2FsDsTCGuaranteedBandwidth (&u4ErrCode,
                                            pDSTraffClassEntry->i4TraffClassId,
                                            pDSTraffClassEntry->i4GrntdBw) ==
        SNMP_FAILURE)
    {
        IssSendError (pHttp, " Invalid guaranteed MaxBw value");
        return SNMP_FAILURE;
    }

    if (nmhSetFsDsTCGuaranteedBandwidth (pDSTraffClassEntry->i4TraffClassId,
                                         pDSTraffClassEntry->i4GrntdBw)
        == SNMP_FAILURE)
    {
        IssSendError (pHttp, " set guaranteed MaxBw failed");
        return SNMP_FAILURE;
    }

    if (nmhTestv2FsDsTCDropLevel1 (&u4ErrCode,
                                   pDSTraffClassEntry->i4TraffClassId,
                                   pDSTraffClassEntry->i4DropLv11) ==
        SNMP_FAILURE)
    {
        IssSendError (pHttp, " Invalid DropLeve1 1 value");
        return SNMP_FAILURE;
    }

    if (nmhSetFsDsTCDropLevel1 (pDSTraffClassEntry->i4TraffClassId,
                                pDSTraffClassEntry->i4DropLv11) == SNMP_FAILURE)
    {
        IssSendError (pHttp, " set DropLeve1 1 failed");
        return SNMP_FAILURE;
    }

    if (nmhTestv2FsDsTCDropLevel2 (&u4ErrCode,
                                   pDSTraffClassEntry->i4TraffClassId,
                                   pDSTraffClassEntry->i4DropLv12) ==
        SNMP_FAILURE)
    {
        IssSendError (pHttp, " Invalid DropLeve1 2 value");
        return SNMP_FAILURE;
    }

    if (nmhSetFsDsTCDropLevel2 (pDSTraffClassEntry->i4TraffClassId,
                                pDSTraffClassEntry->i4DropLv12) == SNMP_FAILURE)
    {
        IssSendError (pHttp, " set DropLeve1 2 failed");
        return SNMP_FAILURE;
    }

    if (nmhTestv2FsDsTCDropLevel3 (&u4ErrCode,
                                   pDSTraffClassEntry->i4TraffClassId,
                                   pDSTraffClassEntry->i4DropLv13) ==
        SNMP_FAILURE)
    {
        IssSendError (pHttp, " Invalid DropLeve1 3 value");
        return SNMP_FAILURE;
    }

    if (nmhSetFsDsTCDropLevel3 (pDSTraffClassEntry->i4TraffClassId,
                                pDSTraffClassEntry->i4DropLv13) == SNMP_FAILURE)
    {
        IssSendError (pHttp, " set DropLeve1 3 failed");
        return SNMP_FAILURE;
    }

    if (nmhTestv2FsDsTCDropAtMaxBandwidth (&u4ErrCode,
                                           pDSTraffClassEntry->i4TraffClassId,
                                           pDSTraffClassEntry->i4DropAtMaxBw) ==
        SNMP_FAILURE)
    {
        IssSendError (pHttp, " Invalid dropAtMaxBw value");
        return SNMP_FAILURE;
    }

    if (nmhSetFsDsTCDropAtMaxBandwidth (pDSTraffClassEntry->i4TraffClassId,
                                        pDSTraffClassEntry->i4DropAtMaxBw)
        == SNMP_FAILURE)
    {
        IssSendError (pHttp, " set dropAtMaxBw failed");
        return SNMP_FAILURE;
    }

    if (nmhTestv2FsDsTCWeightFactor (&u4ErrCode,
                                     pDSTraffClassEntry->i4TraffClassId,
                                     pDSTraffClassEntry->i4WgtFactor) ==
        SNMP_FAILURE)
    {
        IssSendError (pHttp, " Invalid wgt factor value");
        return SNMP_FAILURE;
    }

    if (nmhSetFsDsTCWeightFactor (pDSTraffClassEntry->i4TraffClassId,
                                  pDSTraffClassEntry->i4WgtFactor)
        == SNMP_FAILURE)
    {
        IssSendError (pHttp, " set wgt factor failed");
        return SNMP_FAILURE;
    }

    if (nmhTestv2FsDsTCWeightShift (&u4ErrCode,
                                    pDSTraffClassEntry->i4TraffClassId,
                                    pDSTraffClassEntry->i4WgtShift) ==
        SNMP_FAILURE)
    {
        IssSendError (pHttp, " Invalid wgt shift  value");
        return SNMP_FAILURE;
    }

    if (nmhSetFsDsTCWeightShift (pDSTraffClassEntry->i4TraffClassId,
                                 pDSTraffClassEntry->i4WgtShift)
        == SNMP_FAILURE)
    {
        IssSendError (pHttp, " set wgt shift  failed");
        return SNMP_FAILURE;
    }

    if (nmhTestv2FsDsTCNumFlowGroups (&u4ErrCode,
                                      pDSTraffClassEntry->i4TraffClassId,
                                      pDSTraffClassEntry->i4FlowGroups) ==
        SNMP_FAILURE)
    {
        IssSendError (pHttp, " Invalid no of flow groups  value");
        return SNMP_FAILURE;
    }

    if (nmhSetFsDsTCNumFlowGroups (pDSTraffClassEntry->i4TraffClassId,
                                   pDSTraffClassEntry->i4FlowGroups)
        == SNMP_FAILURE)
    {
        IssSendError (pHttp, " set no of flow groups  failed");
        return SNMP_FAILURE;
    }

    if (nmhTestv2FsDsTCREDCurveId (&u4ErrCode,
                                   pDSTraffClassEntry->i4TraffClassId,
                                   pDSTraffClassEntry->i4RedCurveId) ==
        SNMP_FAILURE)
    {
        IssSendError (pHttp, " Invalid RED curve Id  value");
        return SNMP_FAILURE;
    }

    if (nmhSetFsDsTCREDCurveId (pDSTraffClassEntry->i4TraffClassId,
                                pDSTraffClassEntry->i4RedCurveId)
        == SNMP_FAILURE)
    {
        IssSendError (pHttp, " set RED curve Id failed");
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*********************************************************************
 *  Function Name : IssCxeProcessDfsTrafficClassPageSet 
 *  Description   : This function processes the request coming for the 
 *                  Diff Server traffic class page.
 *                     
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ***********************************************************************/
VOID
IssCxeProcessDfsTrafficClassPageSet (tHttp * pHttp)
{
    INT4                i4RowStatus;
    UINT4               u4ErrCode;
    tDiffServTraffClassEntry DSTraffClassEntry;

    if (HttpGetValuebyName (ISS_ACTION, pHttp->au1Array,
                            pHttp->au1PostQuery) != ENM_SUCCESS)
    {
        IssSendError (pHttp, "Invalid Request");
        return;
    }

    HttpGetValuebyName ("TC_ID", pHttp->au1Value, pHttp->au1PostQuery);
    DSTraffClassEntry.i4TraffClassId = ATOI (pHttp->au1Value);

    DFS_LOCK ();
    /*Deleting an existing entry in the traffic class table */
    if (STRCMP (pHttp->au1Array, ISS_DELETE) == 0)
    {
        if (nmhTestv2FsDsTCEntryStatus (&u4ErrCode,
                                        DSTraffClassEntry.i4TraffClassId,
                                        DESTROY) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, " Invalid Rowstatus");
            return;
        }

        if (nmhSetFsDsTCEntryStatus (DSTraffClassEntry.i4TraffClassId,
                                     DESTROY) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, " Rowstatus set failed");
            return;
        }
        DFS_UNLOCK ();
        IssCxeProcessDfsTrafficClassPageGet (pHttp);
        return;
    }

    HttpGetValuebyName ("MAX_BW", pHttp->au1Value, pHttp->au1PostQuery);
    DSTraffClassEntry.i4MaxBw = ATOI (pHttp->au1Value);

    HttpGetValuebyName ("GRNTD_BW", pHttp->au1Value, pHttp->au1PostQuery);
    DSTraffClassEntry.i4GrntdBw = ATOI (pHttp->au1Value);

    HttpGetValuebyName ("DRP_LEVEL_1", pHttp->au1Value, pHttp->au1PostQuery);
    DSTraffClassEntry.i4DropLv11 = ATOI (pHttp->au1Value);

    HttpGetValuebyName ("DRP_LEVEL_2", pHttp->au1Value, pHttp->au1PostQuery);
    DSTraffClassEntry.i4DropLv12 = ATOI (pHttp->au1Value);

    HttpGetValuebyName ("DRP_LEVEL_3", pHttp->au1Value, pHttp->au1PostQuery);
    DSTraffClassEntry.i4DropLv13 = ATOI (pHttp->au1Value);

    HttpGetValuebyName ("DRP_AT_MAX", pHttp->au1Value, pHttp->au1PostQuery);
    DSTraffClassEntry.i4DropAtMaxBw = ATOI (pHttp->au1Value);

    HttpGetValuebyName ("WGT_FACTOR", pHttp->au1Value, pHttp->au1PostQuery);
    DSTraffClassEntry.i4WgtFactor = ATOI (pHttp->au1Value);

    HttpGetValuebyName ("WGT_SHIFT", pHttp->au1Value, pHttp->au1PostQuery);
    DSTraffClassEntry.i4WgtShift = ATOI (pHttp->au1Value);

    HttpGetValuebyName ("FLOW_GRPS", pHttp->au1Value, pHttp->au1PostQuery);
    DSTraffClassEntry.i4FlowGroups = ATOI (pHttp->au1Value);

    HttpGetValuebyName ("RED_CURVE_ID", pHttp->au1Value, pHttp->au1PostQuery);
    DSTraffClassEntry.i4RedCurveId = ATOI (pHttp->au1Value);

    /*Adding or modifying a new entry in the traffic class table */
    if (nmhGetFsDsTCEntryStatus (DSTraffClassEntry.i4TraffClassId,
                                 &i4RowStatus) != SNMP_SUCCESS)
    {
        /* Traffic class does not exist - create newly */
        if (nmhTestv2FsDsTCEntryStatus (&u4ErrCode,
                                        DSTraffClassEntry.i4TraffClassId,
                                        CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, " Invalid Rowstatus");
            return;
        }

        if (nmhSetFsDsTCEntryStatus (DSTraffClassEntry.i4TraffClassId,
                                     CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, " Rowstatus set failed");
            return;
        }

        HttpGetValuebyName ("OUT_PORT", pHttp->au1Value, pHttp->au1PostQuery);
        DSTraffClassEntry.i4OutPort = ATOI (pHttp->au1Value);

        if (DSTraffClassEntry.i4OutPort != 0)
        {
            if (DSTraffClassEntry.i4OutPort > DS_MAX_PORTS)
            {
                DFS_UNLOCK ();
                IssSendError (pHttp, "Invalid output port");
                return;
            }

            /* Attach the traffic class to the output port specified */
            if (nmhTestv2FsDsTCPortNum (&u4ErrCode,
                                        DSTraffClassEntry.i4TraffClassId,
                                        DSTraffClassEntry.i4OutPort) ==
                SNMP_FAILURE)
            {
                DFS_UNLOCK ();
                IssSendError (pHttp, "Invalid output port");
                return;
            }

            if (nmhSetFsDsTCPortNum (DSTraffClassEntry.i4TraffClassId,
                                     DSTraffClassEntry.i4OutPort) ==
                SNMP_FAILURE)
            {
                DFS_UNLOCK ();
                IssSendError (pHttp, "output port set failed");
                return;
            }
        }

        /* Make the Traffic class entry active */
        if (nmhTestv2FsDsTCEntryStatus (&u4ErrCode,
                                        DSTraffClassEntry.i4TraffClassId,
                                        ACTIVE) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, " Invalid Rowstatus");
            return;
        }

        if (nmhSetFsDsTCEntryStatus (DSTraffClassEntry.i4TraffClassId,
                                     ACTIVE) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, " Rowstatus set failed");
            return;
        }
    }
    /*Setting other objects in traffic class table */
    if (ConfigureDfsTrafficClassTableObjects (pHttp, &DSTraffClassEntry) ==
        SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        return;
    }

    DFS_UNLOCK ();
    IssCxeProcessDfsTrafficClassPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssCxeProcessDfsRedCurveConfigPage
*  Description   : This function processes the request coming for the 
*                  RED curve Configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssCxeProcessDfsRedCurveConfigPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssCxeProcessDfsRedCurveConfigPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssCxeProcessDfsRedCurveConfigPageSet (pHttp);
    }
}

/***************************************************************************
 *  Function Name : IssCxeProcessDfsRedCurveConfigPageGet 
 *  Description   : This function processes the Get request coming for the 
 *                  Diff Server RED curve Page.
 *                     
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *****************************************************************************/
VOID
IssCxeProcessDfsRedCurveConfigPageGet (tHttp * pHttp)
{
    INT4                i4Retval;
    INT1                i1OutCome = 0;
    INT4                i4RedId, i4NextRedId;
    UINT4               u4Temp = 0;

    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    WebnmRegisterLock (pHttp, DfsLock, DfsUnLock);

    DFS_LOCK ();

    i1OutCome = nmhGetFirstIndexFsDsREDConfigTable (&i4RedId);

    if (i1OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        DFS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        return;
    }
    u4Temp = pHttp->i4Write;
    do
    {
        i1OutCome = nmhGetFsDsREDCurveStatus (i4RedId, &i4Retval);
        if (i4Retval != ACTIVE)
        {
            i4NextRedId = i4RedId;
            continue;
        }
        pHttp->i4Write = u4Temp;
        WebnmSendString (pHttp, "REDC_ID");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RedId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, "REDC_ID");
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDsREDCurveStart (i4RedId, &i4Retval);
        WebnmSendString (pHttp, "REDC_START");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Retval);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDsREDCurveStop (i4RedId, &i4Retval);
        WebnmSendString (pHttp, "REDC_STOP");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Retval);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDsREDCurveQRange (i4RedId, &i4Retval);
        WebnmSendString (pHttp, "REDC_Q_RANGE");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Retval);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDsREDCurveStopProbability (i4RedId, &i4Retval);
        WebnmSendString (pHttp, "RED_C_STOP_PROB");
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4Retval);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        i4NextRedId = i4RedId;
    }
    while (nmhGetNextIndexFsDsREDConfigTable (i4NextRedId,
                                              &i4RedId) == SNMP_SUCCESS);

    DFS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 *  Function Name : IssCxeProcessDfsRedCurveConfigPageSet 
 *  Description   : This function processes the request coming for the 
 *                  Diff Server RED Curve page.
 *                     
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ***********************************************************************/
VOID
IssCxeProcessDfsRedCurveConfigPageSet (tHttp * pHttp)
{
    INT4                i4RowStatus;
    UINT4               u4ErrCode;
    tDSREDCurveEntry    DSREDCurveEntry;

    if (HttpGetValuebyName (ISS_ACTION, pHttp->au1Array,
                            pHttp->au1PostQuery) != ENM_SUCCESS)
    {
        IssSendError (pHttp, "Invalid Request");
        return;
    }

    HttpGetValuebyName ("REDC_ID", pHttp->au1Value, pHttp->au1PostQuery);
    DSREDCurveEntry.i4RedCurveId = ATOI (pHttp->au1Value);
    DFS_LOCK ();
    /*Deleting an existing entry in the Red Curve table */
    if (STRCMP (pHttp->au1Array, ISS_DELETE) == 0)
    {
        if (nmhTestv2FsDsREDCurveStatus (&u4ErrCode,
                                         DSREDCurveEntry.i4RedCurveId,
                                         ISS_DESTROY) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, " Invalid Rowstatus");
            return;
        }
        if (nmhSetFsDsREDCurveStatus (DSREDCurveEntry.i4RedCurveId,
                                      ISS_DESTROY) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, " Rowstatus Destroy failed");
            return;
        }
        DFS_UNLOCK ();
        IssCxeProcessDfsRedCurveConfigPageGet (pHttp);
        return;
    }

    HttpGetValuebyName ("REDC_START", pHttp->au1Value, pHttp->au1PostQuery);
    DSREDCurveEntry.i4RedCurveStart = ATOI (pHttp->au1Value);

    HttpGetValuebyName ("REDC_STOP", pHttp->au1Value, pHttp->au1PostQuery);
    DSREDCurveEntry.i4RedCurveStop = ATOI (pHttp->au1Value);

    HttpGetValuebyName ("REDC_Q_RANGE", pHttp->au1Value, pHttp->au1PostQuery);
    DSREDCurveEntry.i4RedCurveQRange = ATOI (pHttp->au1Value);

    HttpGetValuebyName ("RED_C_STOP_PROB", pHttp->au1Value,
                        pHttp->au1PostQuery);
    DSREDCurveEntry.i4RedCurveStopProb = ATOI (pHttp->au1Value);

    /*Adding or modifying a new entry in the traffic class table */
    if (nmhGetFsDsREDCurveStatus (DSREDCurveEntry.i4RedCurveId,
                                  &i4RowStatus) == SNMP_SUCCESS)
    {
        /* RED curve exists. Put it into not-in-service */
        if (nmhTestv2FsDsREDCurveStatus (&u4ErrCode,
                                         DSREDCurveEntry.i4RedCurveId,
                                         NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, " Invalid Rowstatus");
            return;
        }

        if (nmhSetFsDsREDCurveStatus (DSREDCurveEntry.i4RedCurveId,
                                      NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, " Rowstatus set failed");
            return;
        }
    }
    else
    {
        if (nmhTestv2FsDsREDCurveStatus (&u4ErrCode,
                                         DSREDCurveEntry.i4RedCurveId,
                                         CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, " Invalid Rowstatus");
            return;
        }
        if (nmhSetFsDsREDCurveStatus (DSREDCurveEntry.i4RedCurveId,
                                      CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, " Rowstatus set failed");
            return;
        }
    }

    if (nmhTestv2FsDsREDCurveStart (&u4ErrCode,
                                    DSREDCurveEntry.i4RedCurveId,
                                    DSREDCurveEntry.i4RedCurveStart)
        == SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, " Invalid Red Curve Start value");
        return;
    }

    if (nmhTestv2FsDsREDCurveStop (&u4ErrCode,
                                   DSREDCurveEntry.i4RedCurveId,
                                   DSREDCurveEntry.i4RedCurveStop)
        == SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, " Invalid Red Curve Stop value");
        return;
    }

    if (nmhTestv2FsDsREDCurveQRange (&u4ErrCode,
                                     DSREDCurveEntry.i4RedCurveId,
                                     DSREDCurveEntry.i4RedCurveQRange)
        == SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, " Invalid Red Curve Q Range value");
        return;
    }

    if (nmhTestv2FsDsREDCurveStopProbability (&u4ErrCode,
                                              DSREDCurveEntry.i4RedCurveId,
                                              DSREDCurveEntry.
                                              i4RedCurveStopProb) ==
        SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, " Invalid Red Curve Stop Probability value");
        return;
    }

    if (nmhSetFsDsREDCurveStart (DSREDCurveEntry.i4RedCurveId,
                                 DSREDCurveEntry.i4RedCurveStart)
        == SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, " Red Curve Start set failed");
        return;
    }

    if (nmhSetFsDsREDCurveStop (DSREDCurveEntry.i4RedCurveId,
                                DSREDCurveEntry.i4RedCurveStop) == SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, " Red Curve Stop set failed");
        return;
    }

    if (nmhSetFsDsREDCurveQRange (DSREDCurveEntry.i4RedCurveId,
                                  DSREDCurveEntry.i4RedCurveQRange)
        == SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, " Red Curve Q Range set failed");
        return;
    }

    if (nmhSetFsDsREDCurveStopProbability (DSREDCurveEntry.i4RedCurveId,
                                           DSREDCurveEntry.i4RedCurveStopProb)
        == SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, " Red Curve Stop Probability set failed");
        return;
    }
    /* Make the RED entry active */
    if (nmhTestv2FsDsREDCurveStatus (&u4ErrCode,
                                     DSREDCurveEntry.i4RedCurveId,
                                     ACTIVE) == SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, " Invalid Rowstatus");
        return;
    }

    if (nmhSetFsDsREDCurveStatus (DSREDCurveEntry.i4RedCurveId,
                                  ACTIVE) == SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, " Rowstatus set failed");
        return;
    }

    DFS_UNLOCK ();
    IssCxeProcessDfsRedCurveConfigPageGet (pHttp);
}
#endif
#endif
