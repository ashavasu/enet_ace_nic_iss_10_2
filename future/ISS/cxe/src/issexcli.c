/* $Id: issexcli.c,v 1.4 2013/08/29 14:38:59 siva Exp $   */
#ifndef  __ISSEXCLI_C__
#define  __ISSEXCLI_C__

#include "lr.h"
#include "fssnmp.h"
#include "iss.h"
#include "msr.h"
#include "issexinc.h"
#include "fsisselw.h"
#include "isscli.h"
#include "fsissecli.h"

/***************************************************************/
/*  Function Name   : cli_process_iss_ext_cmd                  */
/*  Description     : This function servers as the handler for */
/*                    extended system CLI commands             */
/*  Input(s)        :                                          */
/*                    CliHandle - CLI Handle                   */
/*                    u4Command - Command given by user        */
/*                    Variable set of inputs depending on      */
/*                    the user command                         */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : None                                     */
/***************************************************************/

VOID
cli_process_iss_ext_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[CLI_ISS_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4Inst;
    UINT4               u4IfIndex;
    INT4                i4RetStatus = CLI_SUCCESS;

    va_start (ap, u4Command);

    /* Third arguement is always interface name/index */
    i4Inst = va_arg (ap, INT4);

    /* Walk through the rest of the arguements and store in args array.  */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == CLI_ISS_MAX_ARGS)
            break;
    }
    va_end (ap);

    CLI_SET_ERR (0);

    CliRegisterLock (CliHandle, IssLock, IssUnLock);

    ISS_LOCK ();

    switch (u4Command)
    {
        case CLI_ISS_STORM_CONTROL:
            u4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus =
                CliSetIssStormControl (CliHandle, u4IfIndex, (UINT4) args[0],
                                       *(UINT4 *) args[1]);
            break;

        case CLI_ISS_NO_STORM_CONTROL:
            u4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus =
                CliSetIssStormControl (CliHandle, u4IfIndex, (UINT4) args[0],
                                       0);
            break;
    }

    if ((CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode >= CLI_ERR_START_ID_ISS) &&
            (u4ErrCode < CLI_ISS_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s",
                       IssCliErrString[CLI_ERR_OFFSET_ISS (u4ErrCode)]);
        }

        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);

    ISS_UNLOCK ();
}

/******************************************************************/
/*  Function Name   : CliSetIssStormControl                       */
/*  Description     : This function is used to set the            */
/*                    strom control limit                         */
/*  Input(s)        :                                             */
/*                    CliHandle   - CLI Handle                    */
/*                    u4IfIndex   - Port on which Storm control   */
/*                                  is applied.                   */
/*                    u4Mode      - Mode (Bcast/Mcast/DLF)        */
/*                    u4Rate      - Rate value                    */
/*  Output(s)       : None                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                     */
/******************************************************************/

INT4
CliSetIssStormControl (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4Mode,
                       UINT4 u4Rate)
{
    UINT4               u4ErrorCode;
    INT4                i4RetVal = CLI_SUCCESS;

    switch (u4Mode)
    {
        case ISS_RATE_DLF_LIMIT:
            if (nmhTestv2IssRateCtrlDLFLimitValue (&u4ErrorCode,
                                                   u4IfIndex, u4Rate)
                == SNMP_FAILURE)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }

            if (nmhSetIssRateCtrlDLFLimitValue (u4IfIndex, u4Rate)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                i4RetVal = CLI_FAILURE;
                break;
            }

            break;

        case ISS_RATE_BCAST_LIMIT:
            if (nmhTestv2IssRateCtrlBCASTLimitValue (&u4ErrorCode,
                                                     u4IfIndex, u4Rate)
                == SNMP_FAILURE)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }

            if (nmhSetIssRateCtrlBCASTLimitValue (u4IfIndex, u4Rate)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                i4RetVal = CLI_FAILURE;
                break;
            }
            break;

        case ISS_RATE_MCAST_LIMIT:
            if (nmhTestv2IssRateCtrlMCASTLimitValue (&u4ErrorCode,
                                                     u4IfIndex, u4Rate)
                == SNMP_FAILURE)
            {
                i4RetVal = CLI_FAILURE;
                break;
            }

            if (nmhSetIssRateCtrlMCASTLimitValue (u4IfIndex, u4Rate)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                i4RetVal = CLI_FAILURE;
                break;
            }
            break;

        default:
            i4RetVal = CLI_FAILURE;
            break;
    }

    return i4RetVal;
}
#endif /* __ISSEXCLI_C__ */
