/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsisselw.c,v 1.9 2012/10/31 09:46:13 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "iss.h"
# include  "issexinc.h"
# include  "fsisselw.h"
# include  "isscli.h"
# include  "aclcxecli.h"
# include  "dscxe.h"

/* LOW LEVEL Routines for Table : IssRateCtrlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssRateCtrlTable
 Input       :  The Indices
                IssRateCtrlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssRateCtrlTable (INT4 i4IssRateCtrlIndex)
{

    if (IssSnmpLowValidateIndexPortRateTable (i4IssRateCtrlIndex) ==
        ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssRateCtrlTable
 Input       :  The Indices
                IssRateCtrlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssRateCtrlTable (INT4 *pi4IssRateCtrlIndex)
{
    return (nmhGetNextIndexIssRateCtrlTable (0, pi4IssRateCtrlIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssRateCtrlTable
 Input       :  The Indices
                IssRateCtrlIndex
                nextIssRateCtrlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssRateCtrlTable (INT4 i4IssRateCtrlIndex,
                                 INT4 *pi4NextIssRateCtrlIndex)
{
    INT4                i4Count = 0;

    if (i4IssRateCtrlIndex < 0)
    {
        return SNMP_FAILURE;
    }

    for (i4Count = i4IssRateCtrlIndex + 1; i4Count <= ISS_MAX_PORTS; i4Count++)
    {
        if ((gIssExGlobalInfo.apIssRateCtrlEntry[i4Count]) != NULL)
        {
            *pi4NextIssRateCtrlIndex = i4Count;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIssRateCtrlDLFLimitValue
 Input       :  The Indices
                IssRateCtrlIndex

                The Object 
                retValIssRateCtrlDLFLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssRateCtrlDLFLimitValue (INT4 i4IssRateCtrlIndex,
                                INT4 *pi4RetValIssRateCtrlDLFLimitValue)
{
    tIssRateCtrlEntry  *pIssRateCtrlEntry;

    if (IssValidateRateCtrlEntry (i4IssRateCtrlIndex) != ISS_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pIssRateCtrlEntry = gIssExGlobalInfo.apIssRateCtrlEntry[i4IssRateCtrlIndex];

    *pi4RetValIssRateCtrlDLFLimitValue =
        (INT4) pIssRateCtrlEntry->u4IssRateCtrlDLFLimitValue;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssRateCtrlBCASTLimitValue
 Input       :  The Indices
                IssRateCtrlIndex

                The Object 
                retValIssRateCtrlBCASTLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssRateCtrlBCASTLimitValue (INT4 i4IssRateCtrlIndex,
                                  INT4 *pi4RetValIssRateCtrlBCASTLimitValue)
{
    tIssRateCtrlEntry  *pIssRateCtrlEntry;

    if (IssValidateRateCtrlEntry (i4IssRateCtrlIndex) != ISS_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pIssRateCtrlEntry = gIssExGlobalInfo.apIssRateCtrlEntry[i4IssRateCtrlIndex];

    *pi4RetValIssRateCtrlBCASTLimitValue =
        (INT4) pIssRateCtrlEntry->u4IssRateCtrlBCASTLimitValue;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssRateCtrlMCASTLimitValue
 Input       :  The Indices
                IssRateCtrlIndex

                The Object 
                retValIssRateCtrlMCASTLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssRateCtrlMCASTLimitValue (INT4 i4IssRateCtrlIndex,
                                  INT4 *pi4RetValIssRateCtrlMCASTLimitValue)
{
    tIssRateCtrlEntry  *pIssRateCtrlEntry;

    if (IssValidateRateCtrlEntry (i4IssRateCtrlIndex) != ISS_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pIssRateCtrlEntry = gIssExGlobalInfo.apIssRateCtrlEntry[i4IssRateCtrlIndex];

    *pi4RetValIssRateCtrlMCASTLimitValue =
        (INT4) pIssRateCtrlEntry->u4IssRateCtrlMCASTLimitValue;

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIssRateCtrlDLFLimitValue
 Input       :  The Indices
                IssRateCtrlIndex

                The Object 
                setValIssRateCtrlDLFLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssRateCtrlDLFLimitValue (INT4 i4IssRateCtrlIndex,
                                INT4 i4SetValIssRateCtrlDLFLimitValue)
{
    tIssRateCtrlEntry  *pIssRateCtrlEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    pIssRateCtrlEntry = gIssExGlobalInfo.apIssRateCtrlEntry[i4IssRateCtrlIndex];

    if (pIssRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssRateCtrlEntry->u4IssRateCtrlDLFLimitValue == (UINT4)
        i4SetValIssRateCtrlDLFLimitValue)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    i4RetVal = IsssysIssHwSetRateLimitingValue ((UINT4) i4IssRateCtrlIndex,
                                                ISS_RATE_DLF,
                                                i4SetValIssRateCtrlDLFLimitValue);

    if (i4RetVal != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif

    pIssRateCtrlEntry->u4IssRateCtrlDLFLimitValue = (UINT4)
        i4SetValIssRateCtrlDLFLimitValue;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssRateCtrlBCASTLimitValue
 Input       :  The Indices
                IssRateCtrlIndex

                The Object 
                setValIssRateCtrlBCASTLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssRateCtrlBCASTLimitValue (INT4 i4IssRateCtrlIndex,
                                  INT4 i4SetValIssRateCtrlBCASTLimitValue)
{
    tIssRateCtrlEntry  *pIssRateCtrlEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    pIssRateCtrlEntry = gIssExGlobalInfo.apIssRateCtrlEntry[i4IssRateCtrlIndex];

    if (pIssRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssRateCtrlEntry->u4IssRateCtrlBCASTLimitValue == (UINT4)
        i4SetValIssRateCtrlBCASTLimitValue)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    i4RetVal = IsssysIssHwSetRateLimitingValue ((UINT4) i4IssRateCtrlIndex,
                                                ISS_RATE_BCAST,
                                                i4SetValIssRateCtrlBCASTLimitValue);

    if (i4RetVal != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif

    pIssRateCtrlEntry->u4IssRateCtrlBCASTLimitValue = (UINT4)
        i4SetValIssRateCtrlBCASTLimitValue;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssRateCtrlMCASTLimitValue
 Input       :  The Indices
                IssRateCtrlIndex

                The Object 
                setValIssRateCtrlMCASTLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssRateCtrlMCASTLimitValue (INT4 i4IssRateCtrlIndex,
                                  INT4 i4SetValIssRateCtrlMCASTLimitValue)
{
    tIssRateCtrlEntry  *pIssRateCtrlEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    pIssRateCtrlEntry = gIssExGlobalInfo.apIssRateCtrlEntry[i4IssRateCtrlIndex];

    if (pIssRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssRateCtrlEntry->u4IssRateCtrlMCASTLimitValue == (UINT4)
        i4SetValIssRateCtrlMCASTLimitValue)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    i4RetVal = IsssysIssHwSetRateLimitingValue ((UINT4) i4IssRateCtrlIndex,
                                                ISS_RATE_MCAST,
                                                i4SetValIssRateCtrlMCASTLimitValue);

    if (i4RetVal != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif

    pIssRateCtrlEntry->u4IssRateCtrlMCASTLimitValue = (UINT4)
        i4SetValIssRateCtrlMCASTLimitValue;
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2IssRateCtrlDLFLimitValue
 Input       :  The Indices
                IssRateCtrlIndex

                The Object 
                testValIssRateCtrlDLFLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssRateCtrlDLFLimitValue (UINT4 *pu4ErrorCode,
                                   INT4 i4IssRateCtrlIndex,
                                   INT4 i4TestValIssRateCtrlDLFLimitValue)
{

    if (IssValidateRateCtrlEntry (i4IssRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssRateCtrlDLFLimitValue < 0)
        || (i4TestValIssRateCtrlDLFLimitValue > RATE_LIMIT_MAX_VALUE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssRateCtrlBCASTLimitValue
 Input       :  The Indices
                IssRateCtrlIndex

                The Object 
                testValIssRateCtrlBCASTLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssRateCtrlBCASTLimitValue (UINT4 *pu4ErrorCode,
                                     INT4 i4IssRateCtrlIndex,
                                     INT4 i4TestValIssRateCtrlBCASTLimitValue)
{

    if (IssValidateRateCtrlEntry (i4IssRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssRateCtrlBCASTLimitValue < 0)
        || (i4TestValIssRateCtrlBCASTLimitValue > RATE_LIMIT_MAX_VALUE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssRateCtrlMCASTLimitValue
 Input       :  The Indices
                IssRateCtrlIndex

                The Object 
                testValIssRateCtrlMCASTLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssRateCtrlMCASTLimitValue (UINT4 *pu4ErrorCode,
                                     INT4 i4IssRateCtrlIndex,
                                     INT4 i4TestValIssRateCtrlMCASTLimitValue)
{

    if (IssValidateRateCtrlEntry (i4IssRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssRateCtrlMCASTLimitValue < 0)
        || (i4TestValIssRateCtrlMCASTLimitValue > RATE_LIMIT_MAX_VALUE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : IssL2FilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssL2FilterTable
 Input       :  The Indices
                IssL2FilterNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssL2FilterTable (INT4 i4IssL2FilterNo)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssL2FilterNo) != ISS_TRUE)
    {
        return SNMP_FAILURE;
    }
    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_LIST);
    while (pSllNode != NULL)
    {
        pIssL2FilterEntry = (tIssL2FilterEntry *) pSllNode;
        if (i4IssL2FilterNo == pIssL2FilterEntry->i4IssL2FilterNo)
        {
            return SNMP_SUCCESS;
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_LIST, pSllNode);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL2FilterEtherType
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL2FilterEtherType (UINT4 *pu4Error, INT4 i4IssL2FilterNo,
                               INT4 i4Element)
{
    if (ISS_IS_L2FILTER_ID_VALID (i4IssL2FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4Element <= 0) || (i4Element > 4))
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssL2FilterEtherType
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL2FilterEtherType (INT4 i4IssL2FilterNo, INT4 i4Element)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        if (pIssL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }
        pIssL2FilterEntry->i4IssL2FilterEtherType = i4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL2FilterEtherType
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL2FilterEtherType (INT4 i4IssL2FilterNo, INT4 *pElement)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        *pElement = pIssL2FilterEntry->i4IssL2FilterEtherType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL2FilterProtocolType
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL2FilterProtocolType (UINT4 *pu4Error, INT4 i4IssL2FilterNo,
                                  UINT4 u4Element)
{
    if (ISS_IS_L2FILTER_ID_VALID (i4IssL2FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((u4Element < ISS_MIN_PROTOCOL_ID) && (u4Element > ISS_MAX_PROTOCOL_ID))
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssL2FilterProtocolType
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL2FilterProtocolType (INT4 i4IssL2FilterNo, UINT4 u4Element)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        if (pIssL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }
        pIssL2FilterEntry->u4IssL2FilterProtocolType = u4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL2FilterProtocolType
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL2FilterProtocolType (INT4 i4IssL2FilterNo, UINT4 *pElement)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        *pElement = pIssL2FilterEntry->u4IssL2FilterProtocolType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL2FilterDstMacAddr
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL2FilterDstMacAddr (UINT4 *pu4Error, INT4 i4IssL2FilterNo,
                                tMacAddr pElement)
{
    tMacAddr            zeroAddr;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssL2FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    ISS_MEMSET (zeroAddr, 0, ISS_ETHERNET_ADDR_SIZE);

    if (!ISS_MEMCMP (pElement, zeroAddr, ISS_ETHERNET_ADDR_SIZE))
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssL2FilterDstMacAddr
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL2FilterDstMacAddr (INT4 i4IssL2FilterNo, tMacAddr pElement)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        if (pIssL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }
        ISS_MEMSET (pIssL2FilterEntry->IssL2FilterDstMacAddr, 0,
                    ISS_ETHERNET_ADDR_SIZE);
        ISS_MEMCPY (pIssL2FilterEntry->IssL2FilterDstMacAddr,
                    pElement, ISS_ETHERNET_ADDR_SIZE);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL2FilterDstMacAddr
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL2FilterDstMacAddr (INT4 i4IssL2FilterNo, tMacAddr * pElement)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        ISS_MEMCPY ((UINT1 *) pElement,
                    pIssL2FilterEntry->IssL2FilterDstMacAddr,
                    ISS_ETHERNET_ADDR_SIZE);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL2FilterSrcMacAddr
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL2FilterSrcMacAddr (UINT4 *pu4Error, INT4 i4IssL2FilterNo,
                                tMacAddr pElement)
{
    tMacAddr            zeroAddr;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssL2FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    ISS_MEMSET (zeroAddr, 0, ISS_ETHERNET_ADDR_SIZE);

    if (!ISS_MEMCMP (pElement, zeroAddr, ISS_ETHERNET_ADDR_SIZE))
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssL2FilterSrcMacAddr
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL2FilterSrcMacAddr (INT4 i4IssL2FilterNo, tMacAddr pElement)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        if (pIssL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }
        ISS_MEMSET (pIssL2FilterEntry->IssL2FilterSrcMacAddr, 0,
                    ISS_ETHERNET_ADDR_SIZE);
        ISS_MEMCPY (pIssL2FilterEntry->IssL2FilterSrcMacAddr,
                    pElement, ISS_ETHERNET_ADDR_SIZE);

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL2FilterSrcMacAddr
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL2FilterSrcMacAddr (INT4 i4IssL2FilterNo, tMacAddr * pElement)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        ISS_MEMCPY ((UINT1 *) pElement,
                    pIssL2FilterEntry->IssL2FilterSrcMacAddr,
                    ISS_ETHERNET_ADDR_SIZE);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL2FilterVlanId
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL2FilterVlanId (UINT4 *pu4Error, INT4 i4IssL2FilterNo,
                            INT4 i4Element)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssL2FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4Element < 1) || (i4Element > 4094))
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssL2FilterVlanId
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL2FilterVlanId (INT4 i4IssL2FilterNo, INT4 i4Element)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        if (pIssL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssL2FilterEntry->i4IssL2FilterVlanId = i4Element;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL2FilterVlanId
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL2FilterVlanId (INT4 i4IssL2FilterNo, INT4 *pElement)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        *pElement = pIssL2FilterEntry->i4IssL2FilterVlanId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL2FilterInPort
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                testValIssL2FilterInPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL2FilterInPort (UINT4 *pu4Error, INT4 i4IssL2FilterNo,
                            INT4 i4TestValIssL2FilterInPort)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssL2FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValIssL2FilterInPort < 0 ||
        i4TestValIssL2FilterInPort > ISS_MAX_PORTS)
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValIssL2FilterInPort == 0)
    {
        return SNMP_SUCCESS;
    }

    pIssL2FilterEntry = IssGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        if (pIssL2FilterEntry->i4IssL2FilterInPort != 0)
        {
            *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ACL_FILTER_INPORT_PRESENT_ERR);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssL2FilterInPort
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                setValIssL2FilterInPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL2FilterInPort (INT4 i4IssL2FilterNo, INT4 i4SetValIssL2FilterInPort)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        if (pIssL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssL2FilterEntry->i4IssL2FilterInPort = i4SetValIssL2FilterInPort;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL2FilterInPort
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                retValIssL2FilterInPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL2FilterInPort (INT4 i4IssL2FilterNo, INT4 *pi4RetValIssL2FilterInPort)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        *pi4RetValIssL2FilterInPort = pIssL2FilterEntry->i4IssL2FilterInPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL2FilterOutPort
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                testValIssL2FilterOutPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL2FilterOutPort (UINT4 *pu4Error, INT4 i4IssL2FilterNo,
                             INT4 i4TestValIssL2FilterOutPort)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssL2FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValIssL2FilterOutPort < 0 ||
        i4TestValIssL2FilterOutPort > ISS_MAX_PORTS)
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValIssL2FilterOutPort == 0)
    {
        return SNMP_SUCCESS;
    }

    pIssL2FilterEntry = IssGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        if (pIssL2FilterEntry->i4IssL2FilterOutPort != 0)
        {
            *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ACL_FILTER_OUTPORT_PRESENT_ERR);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssL2FilterOutPort
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                setValIssL2FilterOutPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL2FilterOutPort (INT4 i4IssL2FilterNo, INT4 i4SetValIssL2FilterOutPort)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        if (pIssL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssL2FilterEntry->i4IssL2FilterOutPort = i4SetValIssL2FilterOutPort;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL2FilterOutPort
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                retValIssL2FilterOutPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL2FilterOutPort (INT4 i4IssL2FilterNo,
                          INT4 *pi4RetValIssL2FilterOutPort)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        *pi4RetValIssL2FilterOutPort = pIssL2FilterEntry->i4IssL2FilterOutPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL2FilterAction
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL2FilterAction (UINT4 *pu4Error, INT4 i4IssL2FilterNo,
                            INT4 i4Element)
{
    if (ISS_IS_L2FILTER_ID_VALID (i4IssL2FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4Element == ISS_ALLOW) || (i4Element == ISS_DROP))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIssL2FilterAction
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL2FilterAction (INT4 i4IssL2FilterNo, INT4 i4Element)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        if (pIssL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }
        pIssL2FilterEntry->IssL2FilterAction = i4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL2FilterAction
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL2FilterAction (INT4 i4IssL2FilterNo, INT4 *pElement)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        *pElement = pIssL2FilterEntry->IssL2FilterAction;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL2FilterMatchCount
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL2FilterMatchCount (INT4 i4IssL2FilterNo, UINT4 *pElement)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        *pElement = pIssL2FilterEntry->u4IssL2FilterMatchCount;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL2FilterStatus
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL2FilterStatus (UINT4 *pu4Error, INT4 i4IssL2FilterNo,
                            INT4 i4Element)
{
    if (ISS_IS_L2FILTER_ID_VALID (i4IssL2FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4Element < ISS_ACTIVE) || (i4Element > ISS_DESTROY))
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

#if defined (DIFFSRV_WANTED)
    if ((i4Element == ISS_DESTROY) || (i4Element == ISS_NOT_IN_SERVICE))
    {
        ISS_UNLOCK ();

        if (DsCheckMFClfrTable (i4IssL2FilterNo, DS_MAC_FILTER) == DS_FAILURE)
        {
            ISS_LOCK ();
            CLI_SET_ERR (CLI_ACL_FILTER_NO_CHANGE);
            return SNMP_FAILURE;
        }

        ISS_LOCK ();
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssL2FilterStatus
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL2FilterStatus (INT4 i4IssL2FilterNo, INT4 i4Element)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    pIssL2FilterEntry = IssGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        if (pIssL2FilterEntry->u1IssL2FilterStatus == (UINT1) i4Element)
        {
            return SNMP_SUCCESS;
        }

        if (i4Element == ISS_CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* This Filter Entry is not there in the Database */
        if (i4Element != ISS_CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }
    }

    switch (i4Element)
    {

        case ISS_CREATE_AND_WAIT:

            if (ISS_L2FILTERENTRY_ALLOC_MEM_BLOCK (&pIssL2FilterEntry) !=
                MEM_SUCCESS)
            {
                ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "No Free L2 Filter Entry"
                         "or h/w init failed at startup\n");
                return SNMP_FAILURE;
            }

            ISS_MEMSET (pIssL2FilterEntry, 0, sizeof (tIssL2FilterEntry));

            /* Setting all the default values for the objects */
            pIssL2FilterEntry->i4IssL2FilterNo = i4IssL2FilterNo;
            pIssL2FilterEntry->i4IssL2FilterEtherType = 0;
            pIssL2FilterEntry->u4IssL2FilterProtocolType =
                ISS_DEFAULT_PROTOCOL_TYPE;
            pIssL2FilterEntry->i4IssL2FilterVlanId = 0;
            pIssL2FilterEntry->IssL2FilterAction = ISS_ALLOW;
            pIssL2FilterEntry->u4IssL2FilterMatchCount = 0;
            pIssL2FilterEntry->u1IssL2FilterStatus = 0;

            /* Adding the Entry to the Filter List */
            ISS_SLL_ADD (&(ISS_L2FILTER_LIST),
                         &(pIssL2FilterEntry->IssNextNode));

            /* Setting the RowStatus to NOT_READY as all 
             * mandatory objects are not set taken. */
            pIssL2FilterEntry->u1IssL2FilterStatus = ISS_NOT_READY;

            break;

        case ISS_NOT_IN_SERVICE:

            if (pIssL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
            {
#ifdef NPAPI_WANTED
                if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                {
                    /* Call the hardware API to update the filter */
                    i4RetVal =
                        IsssysIssHwUpdateL2Filter (pIssL2FilterEntry,
                                                   ISS_L2FILTER_DELETE);

                    if (i4RetVal != FNP_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                }
#endif

                pIssL2FilterEntry->u1IssL2FilterStatus = ISS_NOT_IN_SERVICE;

                return SNMP_SUCCESS;
            }
            else
            {
                return SNMP_FAILURE;
            }

            break;

        case ISS_ACTIVE:

            /* Checking the L2 filter for the data sufficiency */
            if (IssQualifyL2FilterData (&pIssL2FilterEntry) != ISS_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            if (pIssL2FilterEntry->u1IssL2FilterStatus != ISS_NOT_IN_SERVICE)
            {
                return SNMP_FAILURE;
            }

#ifdef NPAPI_WANTED
            if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
            {
                /* Call the hardware API to add the L2 filter */
                i4RetVal =
                    IsssysIssHwUpdateL2Filter (pIssL2FilterEntry,
                                               ISS_L2FILTER_ADD);

                if (i4RetVal != FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
#endif

            pIssL2FilterEntry->u1IssL2FilterStatus = ISS_ACTIVE;

            break;

        case ISS_DESTROY:

            if (pIssL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
            {
#ifdef NPAPI_WANTED
                if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                {
                    /* Call the hardware API to delete the filter */
                    i4RetVal =
                        IsssysIssHwUpdateL2Filter (pIssL2FilterEntry,
                                                   ISS_L2FILTER_DELETE);
                    if (i4RetVal != FNP_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                }
#endif
            }

            /* Deleting the node from the list */
            ISS_SLL_DELETE (&(ISS_L2FILTER_LIST),
                            &(pIssL2FilterEntry->IssNextNode));
            /* Delete the Entry from the Software */
            if (ISS_L2FILTERENTRY_FREE_MEM_BLOCK (pIssL2FilterEntry)
                != MEM_SUCCESS)
            {
                ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "L2 Filter Entry Free Failure\n");
                return SNMP_FAILURE;
            }

            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssL2FilterStatus
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL2FilterStatus (INT4 i4IssL2FilterNo, INT4 *pElement)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        *pElement = (INT4) pIssL2FilterEntry->u1IssL2FilterStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : IssL3FilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssL3FilterTable
 Input       :  The Indices
                IssL3FilterNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssL3FilterTable (INT4 i4IssL3FilterNo)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pSllNode = ISS_SLL_FIRST (&ISS_L3FILTER_LIST);

    if (ISS_IS_L3FILTER_ID_VALID (i4IssL3FilterNo) != ISS_TRUE)
    {
        return SNMP_FAILURE;
    }
    while (pSllNode != NULL)
    {
        pIssL3FilterEntry = (tIssL3FilterEntry *) pSllNode;
        if (pIssL3FilterEntry->i4IssL3FilterNo == i4IssL3FilterNo)
        {
            return SNMP_SUCCESS;
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L3FILTER_LIST, pSllNode);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL3FilterProtocol
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL3FilterProtocol (UINT4 *pu4Error, INT4 i4IssL3FilterNo,
                              INT4 i4Element)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4Element < 0) || (i4Element > 255))
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssL3FilterProtocol
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL3FilterProtocol (INT4 i4IssL3FilterNo, INT4 i4Element)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }
        pIssL3FilterEntry->IssL3FilterProtocol = i4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL3FilterProtocol
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL3FilterProtocol (INT4 i4IssL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = pIssL3FilterEntry->IssL3FilterProtocol;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL3FilterMessageType
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL3FilterMessageType (UINT4 *pu4Error, INT4 i4IssL3FilterNo,
                                 INT4 i4Element)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4Element < 0) || (i4Element > 255))
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssL3FilterMessageType
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL3FilterMessageType (INT4 i4IssL3FilterNo, INT4 i4Element)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }
        pIssL3FilterEntry->i4IssL3FilterMessageType = i4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL3FilterMessageType
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL3FilterMessageType (INT4 i4IssL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = pIssL3FilterEntry->i4IssL3FilterMessageType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL3FilterMessageCode
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL3FilterMessageCode (UINT4 *pu4Error, INT4 i4IssL3FilterNo,
                                 INT4 i4Element)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4Element < 0) || (i4Element > 255))
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssL3FilterMessageCode
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL3FilterMessageCode (INT4 i4IssL3FilterNo, INT4 i4Element)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }
        pIssL3FilterEntry->i4IssL3FilterMessageCode = i4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL3FilterMessageCode
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL3FilterMessageCode (INT4 i4IssL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = pIssL3FilterEntry->i4IssL3FilterMessageCode;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL3FilterDstIpAddr
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL3FilterDstIpAddr (UINT4 *pu4Error, INT4 i4IssL3FilterNo,
                               UINT4 u4Element)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Allow IP address which are Class A, B or C */
    if (!((ISS_IS_ADDR_CLASS_A (u4Element)) ||
          (ISS_IS_ADDR_CLASS_B (u4Element)) ||
          (ISS_IS_ADDR_CLASS_C (u4Element))))
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssL3FilterDstIpAddr
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL3FilterDstIpAddr (INT4 i4IssL3FilterNo, UINT4 u4Element)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }
        pIssL3FilterEntry->u4IssL3FilterDstIpAddr = u4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL3FilterDstIpAddr
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL3FilterDstIpAddr (INT4 i4IssL3FilterNo, UINT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = pIssL3FilterEntry->u4IssL3FilterDstIpAddr;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL3FilterSrcIpAddr
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL3FilterSrcIpAddr (UINT4 *pu4Error, INT4 i4IssL3FilterNo,
                               UINT4 u4Element)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Allow IP address which are Class A, B or C */
    if (!((ISS_IS_ADDR_CLASS_A (u4Element)) ||
          (ISS_IS_ADDR_CLASS_B (u4Element)) ||
          (ISS_IS_ADDR_CLASS_C (u4Element))))
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssL3FilterSrcIpAddr
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL3FilterSrcIpAddr (INT4 i4IssL3FilterNo, UINT4 u4Element)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }
        pIssL3FilterEntry->u4IssL3FilterSrcIpAddr = u4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL3FilterSrcIpAddr
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL3FilterSrcIpAddr (INT4 i4IssL3FilterNo, UINT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = pIssL3FilterEntry->u4IssL3FilterSrcIpAddr;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL3FilterDstIpAddrMask
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL3FilterDstIpAddrMask (UINT4 *pu4Error, INT4 i4IssL3FilterNo,
                                   UINT4 u4Element)
{
    UINT1               u1Counter;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* The valid subnet address is from 0 to 32 */
    for (u1Counter = 0; u1Counter <= ISS_MAX_CIDR; ++u1Counter)
    {
        if (gu4IssCidrSubnetMask[u1Counter] == u4Element)
        {
            break;
        }
    }

    if (u1Counter > ISS_MAX_CIDR)
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhSetIssL3FilterDstIpAddrMask
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL3FilterDstIpAddrMask (INT4 i4IssL3FilterNo, UINT4 u4Element)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }
        pIssL3FilterEntry->u4IssL3FilterDstIpAddrMask = u4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL3FilterDstIpAddrMask
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL3FilterDstIpAddrMask (INT4 i4IssL3FilterNo, UINT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = pIssL3FilterEntry->u4IssL3FilterDstIpAddrMask;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL3FilterSrcIpAddrMask
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL3FilterSrcIpAddrMask (UINT4 *pu4Error, INT4 i4IssL3FilterNo,
                                   UINT4 u4Element)
{
    UINT1               u1Counter;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* The valid subnet address is from 0 to 32 */
    for (u1Counter = 0; u1Counter <= ISS_MAX_CIDR; ++u1Counter)
    {
        if (gu4IssCidrSubnetMask[u1Counter] == u4Element)
        {
            break;
        }
    }

    if (u1Counter > ISS_MAX_CIDR)
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhSetIssL3FilterSrcIpAddrMask
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL3FilterSrcIpAddrMask (INT4 i4IssL3FilterNo, UINT4 u4Element)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }
        pIssL3FilterEntry->u4IssL3FilterSrcIpAddrMask = u4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL3FilterSrcIpAddrMask
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL3FilterSrcIpAddrMask (INT4 i4IssL3FilterNo, UINT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = pIssL3FilterEntry->u4IssL3FilterSrcIpAddrMask;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL3FilterMinDstProtPort
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL3FilterMinDstProtPort (UINT4 *pu4Error, INT4 i4IssL3FilterNo,
                                    UINT4 u4Element)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (u4Element <= 65535)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIssL3FilterMinDstProtPort
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL3FilterMinDstProtPort (INT4 i4IssL3FilterNo, UINT4 u4Element)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }
        pIssL3FilterEntry->u4IssL3FilterMinDstProtPort = u4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL3FilterMinDstProtPort
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL3FilterMinDstProtPort (INT4 i4IssL3FilterNo, UINT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = pIssL3FilterEntry->u4IssL3FilterMinDstProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL3FilterMaxDstProtPort
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL3FilterMaxDstProtPort (UINT4 *pu4Error, INT4 i4IssL3FilterNo,
                                    UINT4 u4Element)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (u4Element <= 65535)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIssL3FilterMaxDstProtPort
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL3FilterMaxDstProtPort (INT4 i4IssL3FilterNo, UINT4 u4Element)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }
        pIssL3FilterEntry->u4IssL3FilterMaxDstProtPort = u4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL3FilterMaxDstProtPort
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL3FilterMaxDstProtPort (INT4 i4IssL3FilterNo, UINT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = pIssL3FilterEntry->u4IssL3FilterMaxDstProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL3FilterMinSrcProtPort
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL3FilterMinSrcProtPort (UINT4 *pu4Error, INT4 i4IssL3FilterNo,
                                    UINT4 u4Element)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (u4Element <= 65535)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIssL3FilterMinSrcProtPort
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL3FilterMinSrcProtPort (INT4 i4IssL3FilterNo, UINT4 u4Element)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }
        pIssL3FilterEntry->u4IssL3FilterMinSrcProtPort = u4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL3FilterMinSrcProtPort
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL3FilterMinSrcProtPort (INT4 i4IssL3FilterNo, UINT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = pIssL3FilterEntry->u4IssL3FilterMinSrcProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL3FilterMaxSrcProtPort
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL3FilterMaxSrcProtPort (UINT4 *pu4Error, INT4 i4IssL3FilterNo,
                                    UINT4 u4Element)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (u4Element <= 65535)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIssL3FilterMaxSrcProtPort
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL3FilterMaxSrcProtPort (INT4 i4IssL3FilterNo, UINT4 u4Element)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }
        pIssL3FilterEntry->u4IssL3FilterMaxSrcProtPort = u4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL3FilterMaxSrcProtPort
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL3FilterMaxSrcProtPort (INT4 i4IssL3FilterNo, UINT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = pIssL3FilterEntry->u4IssL3FilterMaxSrcProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL3FilterVlan
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                testValIssL3FilterVlan
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL3FilterVlan (UINT4 *pu4Error, INT4 i4IssL3FilterNo,
                          INT4 i4TestValIssL3FilterVlan)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValIssL3FilterVlan < 0 ||
        i4TestValIssL3FilterVlan > VLAN_DEV_MAX_VLAN_ID)
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssL3FilterVlan
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                setValIssL3FilterVlan
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL3FilterVlan (INT4 i4IssL3FilterNo, INT4 i4SetValIssL3FilterVlan)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssL3FilterEntry->i4IssL3FilterVlan = i4SetValIssL3FilterVlan;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL3FilterVlan
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                retValIssL3FilterVlan
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL3FilterVlan (INT4 i4IssL3FilterNo, INT4 *pi4RetValIssL3FilterVlan)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pi4RetValIssL3FilterVlan = pIssL3FilterEntry->i4IssL3FilterVlan;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL3FilterAckBit
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL3FilterAckBit (UINT4 *pu4Error, INT4 i4IssL3FilterNo,
                            INT4 i4Element)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4Element >= ISS_ACK_ESTABLISH) && (i4Element <= ISS_ACK_ANY))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIssL3FilterAckBit
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL3FilterAckBit (INT4 i4IssL3FilterNo, INT4 i4Element)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }
        pIssL3FilterEntry->IssL3FilterAckBit = i4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL3FilterAckBit
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL3FilterAckBit (INT4 i4IssL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = pIssL3FilterEntry->IssL3FilterAckBit;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL3FilterRstBit
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL3FilterRstBit (UINT4 *pu4Error, INT4 i4IssL3FilterNo,
                            INT4 i4Element)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4Element >= ISS_RST_SET) && (i4Element <= ISS_RST_ANY))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIssL3FilterRstBit
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL3FilterRstBit (INT4 i4IssL3FilterNo, INT4 i4Element)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }
        pIssL3FilterEntry->IssL3FilterRstBit = i4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL3FilterRstBit
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL3FilterRstBit (INT4 i4IssL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = pIssL3FilterEntry->IssL3FilterRstBit;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL3FilterTos
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL3FilterTos (UINT4 *pu4Error, INT4 i4IssL3FilterNo, INT4 i4Element)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->i4IssL3FilterDscp != ISS_DSCP_INVALID)
        {
            *pu4Error = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    if ((i4Element >= ISS_TOS_NONE) && (i4Element < ISS_TOS_INVALID))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIssL3FilterTos
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL3FilterTos (INT4 i4IssL3FilterNo, INT4 i4Element)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }
        pIssL3FilterEntry->IssL3FilterTos = i4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL3FilterTos
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL3FilterTos (INT4 i4IssL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = pIssL3FilterEntry->IssL3FilterTos;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL3FilterDscp
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                testValIssL3FilterDscp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL3FilterDscp (UINT4 *pu4ErrorCode, INT4 i4IssL3FilterNo,
                          INT4 i4TestValIssL3FilterDscp)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    if (ISS_IS_L3FILTER_ID_VALID (i4IssL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry->IssL3FilterTos != ISS_TOS_INVALID)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssL3FilterDscp >= ISS_MIN_DSCP_VALUE) &&
        (i4TestValIssL3FilterDscp <= ISS_MAX_DSCP_VALUE))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIssL3FilterDscp
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                setValIssL3FilterDscp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL3FilterDscp (INT4 i4IssL3FilterNo, INT4 i4SetValIssL3FilterDscp)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }
        pIssL3FilterEntry->i4IssL3FilterDscp = i4SetValIssL3FilterDscp;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL3FilterDscp
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                retValIssL3FilterDscp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL3FilterDscp (INT4 i4IssL3FilterNo, INT4 *pi4RetValIssL3FilterDscp)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pi4RetValIssL3FilterDscp = pIssL3FilterEntry->i4IssL3FilterDscp;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL3FilterAction
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL3FilterAction (UINT4 *pu4Error, INT4 i4IssL3FilterNo,
                            INT4 i4Element)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4Element == ISS_ALLOW) || (i4Element == ISS_DROP))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIssL3FilterAction
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL3FilterAction (INT4 i4IssL3FilterNo, INT4 i4Element)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }
        pIssL3FilterEntry->IssL3FilterAction = i4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL3FilterAction
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL3FilterAction (INT4 i4IssL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = pIssL3FilterEntry->IssL3FilterAction;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssL3FilterMatchCount
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL3FilterMatchCount (INT4 i4IssL3FilterNo, UINT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = pIssL3FilterEntry->u4IssL3FilterMatchCount;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssL3FilterStatus
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssL3FilterStatus (UINT4 *pu4Error, INT4 i4IssL3FilterNo,
                            INT4 i4Element)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4Element < ISS_ACTIVE) || (i4Element > ISS_DESTROY))
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

#if defined (DIFFSRV_WANTED)
    if ((i4Element == ISS_DESTROY) || (i4Element == ISS_NOT_IN_SERVICE))
    {
        ISS_UNLOCK ();

        if (DsCheckMFClfrTable (i4IssL3FilterNo, DS_IP_FILTER) == DS_FAILURE)
        {
            ISS_LOCK ();
            CLI_SET_ERR (CLI_ACL_FILTER_NO_CHANGE);
            return SNMP_FAILURE;
        }

        ISS_LOCK ();
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssL3FilterStatus
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssL3FilterStatus (INT4 i4IssL3FilterNo, INT4 i4Element)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->u1IssL3FilterStatus == (UINT1) i4Element)
        {
            return SNMP_SUCCESS;
        }

        if (i4Element == ISS_CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* This Filter Entry is not there in the Database */
        if (i4Element != ISS_CREATE_AND_WAIT)
        {
            return SNMP_FAILURE;
        }
    }

    switch (i4Element)
    {

        case ISS_CREATE_AND_WAIT:

            if (ISS_L3FILTERENTRY_ALLOC_MEM_BLOCK (&pIssL3FilterEntry) !=
                MEM_SUCCESS)
            {
                ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "No Free L3 Filter Entry"
                         "or h/w init failed at startup\n");
                return SNMP_FAILURE;
            }

            ISS_MEMSET (pIssL3FilterEntry, 0, sizeof (tIssL3FilterEntry));

            /* Setting all the default values for the objects */
            pIssL3FilterEntry->i4IssL3FilterNo = i4IssL3FilterNo;
            pIssL3FilterEntry->IssL3FilterProtocol = ISS_PROTO_ANY;
            pIssL3FilterEntry->i4IssL3FilterMessageType = ISS_ANY;
            pIssL3FilterEntry->i4IssL3FilterMessageCode = ISS_ANY;
            pIssL3FilterEntry->u4IssL3FilterDstIpAddr = ISS_ZERO_ENTRY;
            pIssL3FilterEntry->u4IssL3FilterSrcIpAddr = ISS_ZERO_ENTRY;
            pIssL3FilterEntry->u4IssL3FilterDstIpAddrMask = ISS_DEF_FILTER_MASK;
            pIssL3FilterEntry->u4IssL3FilterSrcIpAddrMask = ISS_DEF_FILTER_MASK;
            pIssL3FilterEntry->u4IssL3FilterMinDstProtPort = ISS_ZERO_ENTRY;
            pIssL3FilterEntry->u4IssL3FilterMaxDstProtPort = ISS_MAX_PORT_VALUE;
            pIssL3FilterEntry->u4IssL3FilterMinSrcProtPort = ISS_ZERO_ENTRY;
            pIssL3FilterEntry->u4IssL3FilterMaxSrcProtPort = ISS_MAX_PORT_VALUE;
            pIssL3FilterEntry->IssL3FilterAckBit = ISS_ACK_ANY;
            pIssL3FilterEntry->IssL3FilterRstBit = ISS_ACK_ANY;
            pIssL3FilterEntry->IssL3FilterTos = ISS_TOS_INVALID;
            pIssL3FilterEntry->i4IssL3FilterDscp = ISS_DSCP_INVALID;
            pIssL3FilterEntry->i4IssL3FilterVlan = ISS_ZERO_ENTRY;
            pIssL3FilterEntry->IssL3FilterAction = ISS_ALLOW;
            pIssL3FilterEntry->u4IssL3FilterMatchCount = ISS_ZERO_ENTRY;
            pIssL3FilterEntry->u1IssL3FilterStatus = 0;

            /* Adding the Entry to the Filter List */
            ISS_SLL_ADD (&(ISS_L3FILTER_LIST),
                         &(pIssL3FilterEntry->IssNextNode));

            /* Setting the RowStatus to NOT_READY as all 
             * mandatory objects are not set taken. */
            pIssL3FilterEntry->u1IssL3FilterStatus = ISS_NOT_READY;

            break;

        case ISS_NOT_IN_SERVICE:

            if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
            {
#ifdef NPAPI_WANTED
                if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                {
                    /* Call the hardware API to update the filter */
                    i4RetVal =
                        IsssysIssHwUpdateL3Filter (pIssL3FilterEntry,
                                                   ISS_L3FILTER_DELETE);
                    if (i4RetVal != FNP_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                }
#endif

                pIssL3FilterEntry->u1IssL3FilterStatus = ISS_NOT_IN_SERVICE;

                return SNMP_SUCCESS;
            }
            else
            {
                return SNMP_FAILURE;
            }

            break;

        case ISS_ACTIVE:

            /* Checking the L3 filter for the data sufficiency */
            if (IssQualifyL3FilterData (&pIssL3FilterEntry) != ISS_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            if (pIssL3FilterEntry->u1IssL3FilterStatus != ISS_NOT_IN_SERVICE)
            {
                return SNMP_FAILURE;
            }

#ifdef NPAPI_WANTED
            if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
            {
                /* Call the hardware API to add the L3 filter */
                i4RetVal =
                    IsssysIssHwUpdateL3Filter (pIssL3FilterEntry,
                                               ISS_L3FILTER_ADD);

                if (i4RetVal != FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
#endif

            pIssL3FilterEntry->u1IssL3FilterStatus = ISS_ACTIVE;

            break;

        case ISS_DESTROY:

            if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
            {
#ifdef NPAPI_WANTED
                if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                {
                    /* Call the hardware API to delete the filter */
                    i4RetVal =
                        IsssysIssHwUpdateL3Filter (pIssL3FilterEntry,
                                                   ISS_L3FILTER_DELETE);
                    if (i4RetVal != FNP_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                }
#endif
            }

            /* Deleting the node from the list */
            ISS_SLL_DELETE (&(ISS_L3FILTER_LIST),
                            &(pIssL3FilterEntry->IssNextNode));
            /* Delete the Entry from the Software */
            if (ISS_L3FILTERENTRY_FREE_MEM_BLOCK (pIssL3FilterEntry)
                != MEM_SUCCESS)
            {
                ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "L3 Filter Entry Free Failure\n");
                return SNMP_FAILURE;
            }

            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssL3FilterStatus
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssL3FilterStatus (INT4 i4IssL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = (INT4) pIssL3FilterEntry->u1IssL3FilterStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssL3FilterTable
 Input       :  The Indices
                IssFilterL3No
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssL3FilterTable (INT4 *pi4IssL3FilterNo)
{
    if ((IssSnmpLowGetFirstValidL3FilterTableIndex (pi4IssL3FilterNo)) ==
        ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssL3FilterTable
 Input       :  The Indices
                IssL3FilterNo
                nextIssL3FilterNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssL3FilterTable (INT4 i4IssL3FilterNo,
                                 INT4 *pi4IssL3FilterNoOUT)
{

    if (i4IssL3FilterNo < 0)
    {
        return SNMP_FAILURE;
    }

    if ((IssSnmpLowGetNextValidL3FilterTableIndex
         (i4IssL3FilterNo, pi4IssL3FilterNoOUT)) == ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssL2FilterTable
 Input       :  The Indices
                IssL2FilterNo
                nextIssL2FilterNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssL2FilterTable (INT4 i4IssL2FilterNo,
                                 INT4 *pi4IssL2FilterNoOUT)
{

    if (i4IssL2FilterNo < 0)
    {
        return SNMP_FAILURE;
    }

    if ((IssSnmpLowGetNextValidL2FilterTableIndex
         (i4IssL2FilterNo, pi4IssL2FilterNoOUT)) == ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssL2FilterTable
 Input       :  The Indices
                IssFilterL2No
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssL2FilterTable (INT4 *pi4IssL2FilterNo)
{
    if ((IssSnmpLowGetFirstValidL2FilterTableIndex (pi4IssL2FilterNo)) ==
        ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
* Function    :  IssSnmpLowValidateIndexPortRateTable
* Input       :  i4CtrlIndex(PortCtrl Table Index or Rate Ctrl Table Index)
* Output      :  None
* Returns     :  ISS_SUCCESS/ISS_FAILURE
*****************************************************************************/
INT4
IssSnmpLowValidateIndexPortRateTable (INT4 i4PortIndex)
{
    /* This routine is only for Port Ctrl Table */
    if (i4PortIndex > ISS_ZERO_ENTRY && i4PortIndex <= ISS_MAX_PORTS)
    {
        return ISS_SUCCESS;
    }
    return ISS_FAILURE;
}

/****************************************************************************
* Function    :  IssValidateRateCtrlEntry
* Input       :  i4RateCtrlIndex
* Output      :  None
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssValidateRateCtrlEntry (INT4 i4RateCtrlIndex)
{
    /* Validating the entry */
    if ((i4RateCtrlIndex <= ISS_ZERO_ENTRY) ||
        (i4RateCtrlIndex > ISS_MAX_PORTS))
    {
        return ISS_FAILURE;
    }

    if ((gIssExGlobalInfo.apIssRateCtrlEntry[i4RateCtrlIndex] == NULL))
    {
        ISS_TRC_ARG1 (DATA_PATH_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "ISS: RATE CTRL ENTRY %d DOES NOT EXIST\n",
                      i4RateCtrlIndex);
        return ISS_FAILURE;
    }
    return ISS_SUCCESS;
}

/****************************************************************************
* Function    :  IssSnmpLowGetFirstValidL3FilterTableIndex
* Input       :  None
* Output      :  INT4 *pi4FirstL3FilterIndex
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssSnmpLowGetFirstValidL3FilterTableIndex (INT4 *pi4FirstL3FilterIndex)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    tIssL3FilterEntry  *pFirstL3FilterEntry = NULL;
    UINT1               u1FoundFlag = ISS_FALSE;

    pSllNode = ISS_SLL_FIRST (&ISS_L3FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssL3FilterEntry = (tIssL3FilterEntry *) pSllNode;
        if (u1FoundFlag == ISS_FALSE)
        {
            pFirstL3FilterEntry = pIssL3FilterEntry;
            u1FoundFlag = ISS_TRUE;
        }
        else
        {
            if (pFirstL3FilterEntry->i4IssL3FilterNo >
                pIssL3FilterEntry->i4IssL3FilterNo)
            {
                pFirstL3FilterEntry = pIssL3FilterEntry;
            }
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L3FILTER_LIST, pSllNode);
    }

    if (u1FoundFlag == ISS_TRUE)
    {
        *pi4FirstL3FilterIndex = pFirstL3FilterEntry->i4IssL3FilterNo;
        return ISS_SUCCESS;
    }

    return ISS_FAILURE;
}

/****************************************************************************
* Function    :  IssSnmpLowGetFirstValidL3FilterTableIndex
* Input       :  INT4 i4Iss3FilterNo
* Output      :  INT4 *pi4NextL3FilterNo
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssSnmpLowGetNextValidL3FilterTableIndex (INT4 i4IssL3FilterNo,
                                          INT4 *pi4NextL3FilterNo)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    tIssL3FilterEntry  *pNextL3FilterEntry = NULL;
    UINT1               u1FoundFlag = ISS_FALSE;

    pSllNode = ISS_SLL_FIRST (&ISS_L3FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssL3FilterEntry = (tIssL3FilterEntry *) pSllNode;

        if (i4IssL3FilterNo < pIssL3FilterEntry->i4IssL3FilterNo)
        {
            if (u1FoundFlag == ISS_FALSE)
            {
                pNextL3FilterEntry = pIssL3FilterEntry;
                u1FoundFlag = ISS_TRUE;
            }
            else
            {
                if (pNextL3FilterEntry->i4IssL3FilterNo >
                    pIssL3FilterEntry->i4IssL3FilterNo)
                {
                    pNextL3FilterEntry = pIssL3FilterEntry;
                }
            }
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L3FILTER_LIST, pSllNode);
    }

    if (u1FoundFlag == ISS_TRUE)
    {
        *pi4NextL3FilterNo = pNextL3FilterEntry->i4IssL3FilterNo;
        return ISS_SUCCESS;
    }
    else
    {
        return ISS_FAILURE;
    }
}

/****************************************************************************
* Function    :  IssSnmpLowGetFirstValidL2FilterTableIndex
* Input       :  None
* Output      :  INT4 *pi4FirstL2FilterIndex
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssSnmpLowGetFirstValidL2FilterTableIndex (INT4 *pi4FirstL2FilterIndex)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;
    tIssL2FilterEntry  *pFirstL2FilterEntry = NULL;
    UINT1               u1FoundFlag = ISS_FALSE;

    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssL2FilterEntry = (tIssL2FilterEntry *) pSllNode;
        if (u1FoundFlag == ISS_FALSE)
        {
            pFirstL2FilterEntry = pIssL2FilterEntry;
            u1FoundFlag = ISS_TRUE;
        }
        else
        {
            if (pFirstL2FilterEntry->i4IssL2FilterNo >
                pIssL2FilterEntry->i4IssL2FilterNo)
            {
                pFirstL2FilterEntry = pIssL2FilterEntry;
            }
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_LIST, pSllNode);
    }

    if (u1FoundFlag == ISS_TRUE)
    {
        *pi4FirstL2FilterIndex = pFirstL2FilterEntry->i4IssL2FilterNo;
        return ISS_SUCCESS;
    }

    return ISS_FAILURE;
}

/****************************************************************************
* Function    :  IssSnmpLowGetFirstValidL2FilterTableIndex
* Input       :  INT4 i4IssL2FilterNo
* Output      :  INT4 *pi4NextL2FilterNo
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssSnmpLowGetNextValidL2FilterTableIndex (INT4 i4IssL2FilterNo,
                                          INT4 *pi4NextL2FilterNo)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;
    tIssL2FilterEntry  *pNextL2FilterEntry = NULL;
    UINT1               u1FoundFlag = ISS_FALSE;

    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssL2FilterEntry = (tIssL2FilterEntry *) pSllNode;

        if (i4IssL2FilterNo < pIssL2FilterEntry->i4IssL2FilterNo)
        {
            if (u1FoundFlag == ISS_FALSE)
            {
                pNextL2FilterEntry = pIssL2FilterEntry;
                u1FoundFlag = ISS_TRUE;
            }
            else
            {
                if (pNextL2FilterEntry->i4IssL2FilterNo >
                    pIssL2FilterEntry->i4IssL2FilterNo)
                {
                    pNextL2FilterEntry = pIssL2FilterEntry;
                }
            }
        }

        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_LIST, pSllNode);
    }

    if (u1FoundFlag == ISS_TRUE)
    {
        *pi4NextL2FilterNo = pNextL2FilterEntry->i4IssL2FilterNo;
        return ISS_SUCCESS;
    }
    else
    {
        return ISS_FAILURE;
    }
}

/****************************************************************************
* Function    :  IssGetL2FilterEntry
* Input       :  INT4 i4IssL2FilterNo
* Output      :  None
* Returns     :  tIssL2FilterEntry * 
*****************************************************************************/
tIssL2FilterEntry  *
IssGetL2FilterEntry (INT4 i4IssL2FilterNo)
{

    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssL2FilterEntry = (tIssL2FilterEntry *) pSllNode;

        if (pIssL2FilterEntry->i4IssL2FilterNo == i4IssL2FilterNo)
        {
            return pIssL2FilterEntry;
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_LIST, pSllNode);
    }
    return NULL;
}

/****************************************************************************
* Function    :  IssQualifyL2FilterData 
* Input       :  tIssL2FilterEntry **ppIssL2FilterEntry
* Output      :  None
* Returns     :  INT4
*****************************************************************************/
INT4
IssQualifyL2FilterData (tIssL2FilterEntry ** ppIssL2FilterEntry)
{
    if ((*ppIssL2FilterEntry)->IssL2FilterAction != 0)
    {
        (*ppIssL2FilterEntry)->u1IssL2FilterStatus = ISS_NOT_IN_SERVICE;
        return ISS_SUCCESS;
    }
    else
    {
        (*ppIssL2FilterEntry)->u1IssL2FilterStatus = ISS_NOT_READY;
        return ISS_FAILURE;
    }
}

/****************************************************************************
* Function    :  IssGetL3FilterEntry
* Input       :  INT4 i4IssL3FilterNo
* Output      :  None
* Returns     :  tIssL3FilterEntry * 
*****************************************************************************/
tIssL3FilterEntry  *
IssGetL3FilterEntry (INT4 i4IssL3FilterNo)
{

    tIssSllNode        *pSllNode = NULL;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pSllNode = ISS_SLL_FIRST (&ISS_L3FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssL3FilterEntry = (tIssL3FilterEntry *) pSllNode;

        if (pIssL3FilterEntry->i4IssL3FilterNo == i4IssL3FilterNo)
        {
            return pIssL3FilterEntry;
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L3FILTER_LIST, pSllNode);
    }
    return NULL;
}

/****************************************************************************
* Function    :  IssQualifyL3FilterData 
* Input       :  tIssL3FilterEntry **ppIssL3FilterEntry
* Output      :  None
* Returns     :  INT4
*****************************************************************************/
INT4
IssQualifyL3FilterData (tIssL3FilterEntry ** ppIssL3FilterEntry)
{
    if ((*ppIssL3FilterEntry)->IssL3FilterAction != 0)
    {
        (*ppIssL3FilterEntry)->u1IssL3FilterStatus = ISS_NOT_IN_SERVICE;
        return ISS_SUCCESS;
    }
    else
    {
        (*ppIssL3FilterEntry)->u1IssL3FilterStatus = ISS_NOT_READY;
        return ISS_FAILURE;
    }
}
