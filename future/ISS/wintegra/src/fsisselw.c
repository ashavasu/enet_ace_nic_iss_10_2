/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsisselw.c,v 1.4 2016/03/19 13:10:52 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "iss.h"
# include  "issexinc.h"
# include  "fsisselw.h"
# include  "fsissawr.h"
# include  "fsissalw.h"
# include  "isscli.h"
# include  "aclcli.h"
# include  "diffsrv.h"
# include  "qosxtd.h"
# include  "fsissacli.h"
static UINT1        gNullMacAddr[MAC_ADDR_LEN] = { 0, 0, 0, 0, 0, 0 };
UINT4               gu4AclOverLa = 1;
/* LOW LEVEL Routines for Table : IssExtRateCtrlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssExtRateCtrlTable
 Input       :  The Indices
                IssExtRateCtrlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssExtRateCtrlTable (INT4 i4IssExtRateCtrlIndex)
{
    return (nmhValidateIndexInstanceIssAclRateCtrlTable
            (i4IssExtRateCtrlIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssExtRateCtrlTable
 Input       :  The Indices
                IssExtRateCtrlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssExtRateCtrlTable (INT4 *pi4IssExtRateCtrlIndex)
{
    return (nmhGetFirstIndexIssAclRateCtrlTable (pi4IssExtRateCtrlIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssExtRateCtrlTable
 Input       :  The Indices
                IssExtRateCtrlIndex
                nextIssRateCtrlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssExtRateCtrlTable (INT4 i4IssExtRateCtrlIndex,
                                    INT4 *pi4NextIssRateCtrlIndex)
{
    return (nmhGetNextIndexIssAclRateCtrlTable
            (i4IssExtRateCtrlIndex, pi4NextIssRateCtrlIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIssExtRateCtrlDLFLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                retValIssExtRateCtrlDLFLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtRateCtrlDLFLimitValue (INT4 i4IssExtRateCtrlIndex,
                                   INT4 *pi4RetValIssExtRateCtrlDLFLimitValue)
{
    return (nmhGetIssAclRateCtrlDLFLimitValue
            (i4IssExtRateCtrlIndex, pi4RetValIssExtRateCtrlDLFLimitValue));
}

/****************************************************************************
 Function    :  nmhGetIssExtRateCtrlBCASTLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                retValIssExtRateCtrlBCASTLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtRateCtrlBCASTLimitValue (INT4 i4IssExtRateCtrlIndex,
                                     INT4
                                     *pi4RetValIssExtRateCtrlBCASTLimitValue)
{
    return (nmhGetIssAclRateCtrlBCASTLimitValue
            (i4IssExtRateCtrlIndex, pi4RetValIssExtRateCtrlBCASTLimitValue));
}

/****************************************************************************
 Function    :  nmhGetIssExtRateCtrlMCASTLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                retValIssExtRateCtrlMCASTLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtRateCtrlMCASTLimitValue (INT4 i4IssExtRateCtrlIndex,
                                     INT4
                                     *pi4RetValIssExtRateCtrlMCASTLimitValue)
{
    return (nmhGetIssAclRateCtrlMCASTLimitValue
            (i4IssExtRateCtrlIndex, pi4RetValIssExtRateCtrlMCASTLimitValue));
}

/****************************************************************************
 Function    :  nmhGetIssExtRateCtrlPortRateLimit
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                retValIssExtRateCtrlPortRateLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtRateCtrlPortRateLimit (INT4 i4IssExtRateCtrlIndex,
                                   INT4 *pi4RetValIssExtRateCtrlPortRateLimit)
{
    return (nmhGetIssAclRateCtrlPortRateLimit
            (i4IssExtRateCtrlIndex, pi4RetValIssExtRateCtrlPortRateLimit));
}

/****************************************************************************
 Function    :  nmhGetIssExtRateCtrlPortBurstSize
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                retValIssExtRateCtrlPortBurstSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtRateCtrlPortBurstSize (INT4 i4IssExtRateCtrlIndex,
                                   INT4 *pi4RetValIssExtRateCtrlPortBurstSize)
{
    return (nmhGetIssAclRateCtrlPortBurstSize
            (i4IssExtRateCtrlIndex, pi4RetValIssExtRateCtrlPortBurstSize));
}

/****************************************************************************
 Function    :  nmhGetIssExtDefaultRateCtrlStatus
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                retValIssExtDefaultRateCtrlStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIssExtDefaultRateCtrlStatus(INT4 i4IssExtRateCtrlIndex , INT4 *pi4RetValIssExtDefaultRateCtrlStatus)
{
  UNUSED_PARAM(i4IssExtRateCtrlIndex);
  UNUSED_PARAM (*pi4RetValIssExtDefaultRateCtrlStatus);
  return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIssExtRateCtrlDLFLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                setValIssExtRateCtrlDLFLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtRateCtrlDLFLimitValue (INT4 i4IssExtRateCtrlIndex,
                                   INT4 i4SetValIssExtRateCtrlDLFLimitValue)
{
    return (nmhSetIssAclRateCtrlDLFLimitValue
            (i4IssExtRateCtrlIndex, i4SetValIssExtRateCtrlDLFLimitValue));
}

/****************************************************************************
 Function    :  nmhSetIssExtRateCtrlBCASTLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                setValIssExtRateCtrlBCASTLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtRateCtrlBCASTLimitValue (INT4 i4IssExtRateCtrlIndex,
                                     INT4 i4SetValIssExtRateCtrlBCASTLimitValue)
{
    return (nmhSetIssAclRateCtrlBCASTLimitValue
            (i4IssExtRateCtrlIndex, i4SetValIssExtRateCtrlBCASTLimitValue));
}

/****************************************************************************
 Function    :  nmhSetIssExtRateCtrlMCASTLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                setValIssExtRateCtrlMCASTLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtRateCtrlMCASTLimitValue (INT4 i4IssExtRateCtrlIndex,
                                     INT4 i4SetValIssExtRateCtrlMCASTLimitValue)
{
    return (nmhSetIssAclRateCtrlMCASTLimitValue
            (i4IssExtRateCtrlIndex, i4SetValIssExtRateCtrlMCASTLimitValue));
}

/****************************************************************************
 Function    :  nmhSetIssExtRateCtrlPortRateLimit
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                setValIssExtRateCtrlPortRateLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtRateCtrlPortRateLimit (INT4 i4IssExtRateCtrlIndex,
                                   INT4 i4SetValIssExtRateCtrlPortRateLimit)
{
    return (nmhSetIssAclRateCtrlPortRateLimit
            (i4IssExtRateCtrlIndex, i4SetValIssExtRateCtrlPortRateLimit));
}

/****************************************************************************
 Function    :  nmhSetIssExtRateCtrlPortBurstSize
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                setValIssExtRateCtrlPortBurstSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtRateCtrlPortBurstSize (INT4 i4IssExtRateCtrlIndex,
                                   INT4 i4SetValIssExtRateCtrlPortBurstSize)
{
    return (nmhSetIssAclRateCtrlPortBurstSize
            (i4IssExtRateCtrlIndex, i4SetValIssExtRateCtrlPortBurstSize));
}

/****************************************************************************
 Function    :  nmhSetIssExtDefaultRateCtrlStatus
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                setValIssExtDefaultRateCtrlStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetIssExtDefaultRateCtrlStatus(INT4 i4IssExtRateCtrlIndex , INT4 i4SetValIssExtDefaultRateCtrlStatus)
{
  UNUSED_PARAM(i4IssExtRateCtrlIndex);
  UNUSED_PARAM (i4SetValIssExtDefaultRateCtrlStatus);
  return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2IssExtRateCtrlDLFLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                testValIssExtRateCtrlDLFLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtRateCtrlDLFLimitValue (UINT4 *pu4ErrorCode,
                                      INT4 i4IssExtRateCtrlIndex,
                                      INT4 i4TestValIssExtRateCtrlDLFLimitValue)
{
    return (nmhTestv2IssAclRateCtrlDLFLimitValue
            (pu4ErrorCode, i4IssExtRateCtrlIndex,
             i4TestValIssExtRateCtrlDLFLimitValue));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtRateCtrlBCASTLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                testValIssExtRateCtrlBCASTLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtRateCtrlBCASTLimitValue (UINT4 *pu4ErrorCode,
                                        INT4 i4IssExtRateCtrlIndex,
                                        INT4
                                        i4TestValIssExtRateCtrlBCASTLimitValue)
{
    return (nmhTestv2IssAclRateCtrlBCASTLimitValue
            (pu4ErrorCode, i4IssExtRateCtrlIndex,
             i4TestValIssExtRateCtrlBCASTLimitValue));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtRateCtrlMCASTLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                testValIssExtRateCtrlMCASTLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtRateCtrlMCASTLimitValue (UINT4 *pu4ErrorCode,
                                        INT4 i4IssExtRateCtrlIndex,
                                        INT4
                                        i4TestValIssExtRateCtrlMCASTLimitValue)
{
    return (nmhTestv2IssAclRateCtrlMCASTLimitValue
            (pu4ErrorCode, i4IssExtRateCtrlIndex,
             i4TestValIssExtRateCtrlMCASTLimitValue));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtRateCtrlPortRateLimit
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                testValIssExtRateCtrlPortRateLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtRateCtrlPortRateLimit (UINT4 *pu4ErrorCode,
                                      INT4 i4IssExtRateCtrlIndex,
                                      INT4 i4TestValIssExtRateCtrlPortRateLimit)
{
    return (nmhTestv2IssAclRateCtrlPortRateLimit
            (pu4ErrorCode, i4IssExtRateCtrlIndex,
             i4TestValIssExtRateCtrlPortRateLimit));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtRateCtrlPortBurstSize
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                testValIssExtRateCtrlPortBurstSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtRateCtrlPortBurstSize (UINT4 *pu4ErrorCode,
                                      INT4 i4IssExtRateCtrlIndex,
                                      INT4 i4TestValIssExtRateCtrlPortBurstSize)
{
    return (nmhTestv2IssAclRateCtrlPortBurstSize
            (pu4ErrorCode, i4IssExtRateCtrlIndex,
             i4TestValIssExtRateCtrlPortBurstSize));
}
/****************************************************************************
 Function    :  nmhTestv2IssExtDefaultRateCtrlStatus
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                testValIssExtDefaultRateCtrlStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2IssExtDefaultRateCtrlStatus(UINT4 *pu4ErrorCode , INT4 i4IssExtRateCtrlIndex , INT4 i4TestValIssExtDefaultRateCtrlStatus)
{
    UNUSED_PARAM(*pu4ErrorCode);
    UNUSED_PARAM(i4IssExtRateCtrlIndex);
    UNUSED_PARAM (i4TestValIssExtDefaultRateCtrlStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssExtRateCtrlTable
 Input       :  The Indices
                IssExtRateCtrlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssExtRateCtrlTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IssExtL2FilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssExtL2FilterTable
 Input       :  The Indices
                IssExtL2FilterNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssExtL2FilterTable (INT4 i4IssExtL2FilterNo)
{
    return (nmhValidateIndexInstanceIssAclL2FilterTable (i4IssExtL2FilterNo));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterPriority
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterPriority (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                                 INT4 i4Element)
{
    return (nmhTestv2IssAclL2FilterPriority
            (pu4Error, i4IssExtL2FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterPriority
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterPriority (INT4 i4IssExtL2FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL2FilterPriority (i4IssExtL2FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterPriority
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterPriority (INT4 i4IssExtL2FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL2FilterPriority (i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterEtherType
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                testValIssExtL2FilterEtherType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterEtherType (UINT4 *pu4ErrorCode,
                                  INT4 i4IssExtL2FilterNo,
                                  INT4 i4TestValIssExtL2FilterEtherType)
{
    return (nmhTestv2IssAclL2FilterEtherType
            (pu4ErrorCode, i4IssExtL2FilterNo,
             i4TestValIssExtL2FilterEtherType));
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterEtherType
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                setValIssExtL2FilterEtherType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterEtherType (INT4 i4IssExtL2FilterNo,
                               INT4 i4SetValIssExtL2FilterEtherType)
{
    return (nmhSetIssAclL2FilterEtherType
            (i4IssExtL2FilterNo, i4SetValIssExtL2FilterEtherType));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterEtherType
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                retValIssExtL2FilterEtherType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterEtherType (INT4 i4IssExtL2FilterNo,
                               INT4 *pi4RetValIssExtL2FilterEtherType)
{
    return (nmhGetIssAclL2FilterEtherType
            (i4IssExtL2FilterNo, pi4RetValIssExtL2FilterEtherType));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterProtocolType
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterProtocolType (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                                     UINT4 u4Element)
{
    return (nmhTestv2IssAclL2FilterProtocolType
            (pu4Error, i4IssExtL2FilterNo, u4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterProtocolType
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterProtocolType (INT4 i4IssExtL2FilterNo, UINT4 u4Element)
{
    return (nmhSetIssAclL2FilterProtocolType (i4IssExtL2FilterNo, u4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterProtocolType
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterProtocolType (INT4 i4IssExtL2FilterNo, UINT4 *pElement)
{
    return (nmhGetIssAclL2FilterProtocolType (i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterDstMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterDstMacAddr (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                                   tMacAddr pElement)
{
    return (nmhTestv2IssAclL2FilterDstMacAddr
            (pu4Error, i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterDstMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterDstMacAddr (INT4 i4IssExtL2FilterNo, tMacAddr MacAddress)
{
    return (nmhSetIssAclL2FilterDstMacAddr (i4IssExtL2FilterNo, MacAddress));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterDstMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterDstMacAddr (INT4 i4IssExtL2FilterNo, tMacAddr * pMacAddress)
{
    return (nmhGetIssAclL2FilterDstMacAddr (i4IssExtL2FilterNo, pMacAddress));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterSrcMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterSrcMacAddr (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                                   tMacAddr pElement)
{
    return (nmhTestv2IssAclL2FilterSrcMacAddr
            (pu4Error, i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterSrcMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterSrcMacAddr (INT4 i4IssExtL2FilterNo, tMacAddr pElement)
{
    return (nmhSetIssAclL2FilterSrcMacAddr (i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterSrcMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterSrcMacAddr (INT4 i4IssExtL2FilterNo, tMacAddr * pElement)
{
    return (nmhGetIssAclL2FilterSrcMacAddr (i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterVlanId
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterVlanId (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                               INT4 i4TestValIssExtL2FilterCustomerVlanId)
{
    return (nmhTestv2IssAclL2FilterVlanId
            (pu4Error, i4IssExtL2FilterNo,
             i4TestValIssExtL2FilterCustomerVlanId));
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterVlanId
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterVlanId (INT4 i4IssExtL2FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL2FilterVlanId (i4IssExtL2FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterVlanId
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterVlanId (INT4 i4IssExtL2FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL2FilterVlanId (i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterInPortList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterInPortList (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                                   tSNMP_OCTET_STRING_TYPE * pElement)
{
    return (nmhTestv2IssAclL2FilterInPortList
            (pu4Error, i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterInPortList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterInPortList (INT4 i4IssExtL2FilterNo,
                                tSNMP_OCTET_STRING_TYPE * pElement)
{
    return (nmhSetIssAclL2FilterInPortList (i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterInPortList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterInPortList (INT4 i4IssExtL2FilterNo,
                                tSNMP_OCTET_STRING_TYPE * pElement)
{
    return (nmhGetIssAclL2FilterInPortList (i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterOutPortList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                testValIssExtL2FilterOutPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterOutPortList (UINT4 *pu4ErrorCode,
                                    INT4 i4IssExtL2FilterNo,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValOutPortList)
{
    return (nmhTestv2IssAclL2FilterOutPortList
            (pu4ErrorCode, i4IssExtL2FilterNo, pTestValOutPortList));
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterOutPortList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                setValIssExtL2FilterOutPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterOutPortList (INT4 i4IssExtL2FilterNo,
                                 tSNMP_OCTET_STRING_TYPE * pSetValOutPortList)
{
    return (nmhSetIssAclL2FilterOutPortList
            (i4IssExtL2FilterNo, pSetValOutPortList));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterOutPortList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                retValIssExtL2FilterOutPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterOutPortList (INT4 i4IssExtL2FilterNo,
                                 tSNMP_OCTET_STRING_TYPE * pGetValOutPortList)
{
    return (nmhGetIssAclL2FilterOutPortList
            (i4IssExtL2FilterNo, pGetValOutPortList));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterDirection
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                testValIssExtL2FilterDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterDirection (UINT4 *pu4ErrorCode,
                                  INT4 i4IssExtL2FilterNo,
                                  INT4 i4TestValIssExtL2FilterDirection)
{
    return (nmhTestv2IssAclL2FilterDirection
            (pu4ErrorCode, i4IssExtL2FilterNo,
             i4TestValIssExtL2FilterDirection));
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterDirection
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                setValIssExtL2FilterDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterDirection (INT4 i4IssExtL2FilterNo,
                               INT4 i4SetValIssExtL2FilterDirection)
{
    return (nmhSetIssAclL2FilterDirection
            (i4IssExtL2FilterNo, i4SetValIssExtL2FilterDirection));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterDirection
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                retValIssExtL2FilterDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterDirection (INT4 i4IssExtL2FilterNo,
                               INT4 *pi4RetValIssExtL2FilterDirection)
{
    return (nmhGetIssAclL2FilterDirection
            (i4IssExtL2FilterNo, pi4RetValIssExtL2FilterDirection));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterAction
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterAction (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                               INT4 i4Element)
{
    return (nmhTestv2IssAclL2FilterAction
            (pu4Error, i4IssExtL2FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterAction
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterAction (INT4 i4IssExtL2FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL2FilterAction (i4IssExtL2FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterAction
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterAction (INT4 i4IssExtL2FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL2FilterAction (i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterMatchCount
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterMatchCount (INT4 i4IssExtL2FilterNo, UINT4 *pElement)
{
    return (nmhGetIssAclL2FilterMatchCount (i4IssExtL2FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterStatus
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterStatus (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                               INT4 i4Element)
{
    return (nmhTestv2IssAclL2FilterStatus
            (pu4Error, i4IssExtL2FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterStatus
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterStatus (INT4 i4IssExtL2FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL2FilterStatus (i4IssExtL2FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterStatus
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterStatus (INT4 i4IssExtL2FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL2FilterStatus (i4IssExtL2FilterNo, pElement));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssExtL2FilterTable
 Input       :  The Indices
                IssExtL2FilterNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssExtL2FilterTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IssExtL3FilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssExtL3FilterTable
 Input       :  The Indices
                IssExtL3FilterNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssExtL3FilterTable (INT4 i4IssExtL3FilterNo)
{
    return (nmhValidateIndexInstanceIssAclL3FilterTable (i4IssExtL3FilterNo));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterPriority
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterPriority (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                 INT4 i4Element)
{
    return (nmhTestv2IssAclL3FilterPriority
            (pu4Error, i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterPriority
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterPriority (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL3FilterPriority (i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterPriority
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterPriority (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL3FilterPriority (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterProtocol
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterProtocol (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                 INT4 i4Element)
{
    return (nmhTestv2IssAclL3FilterProtocol
            (pu4Error, i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterProtocol
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterProtocol (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL3FilterProtocol (i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterProtocol
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterProtocol (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL3FilterProtocol (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterMessageType
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterMessageType (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                    INT4 i4Element)
{
    return (nmhTestv2IssAclL3FilterMessageType
            (pu4Error, i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterMessageType
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterMessageType (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL3FilterMessageType (i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMessageType
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMessageType (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL3FilterMessageType (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterMessageCode
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterMessageCode (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                    INT4 i4Element)
{
    return (nmhTestv2IssAclL3FilterMessageCode
            (pu4Error, i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterMessageCode
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterMessageCode (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL3FilterMessageCode (i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMessageCode
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMessageCode (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL3FilterMessageCode (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterDstIpAddr
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterDstIpAddr (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                  UINT4 u4Element)
{
    tSNMP_OCTET_STRING_TYPE IpvAddrOctet;
    UINT1               au1Tmp[16];

    IpvAddrOctet.pu1_OctetList = au1Tmp;

    PTR_ASSIGN4 (IpvAddrOctet.pu1_OctetList, u4Element);

    IpvAddrOctet.i4_Length = QOS_IPV4_LEN;

    return (nmhTestv2IssAclL3FilterDstIpAddr
            (pu4Error, i4IssExtL3FilterNo, &IpvAddrOctet));

}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterDstIpAddr
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterDstIpAddr (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    tSNMP_OCTET_STRING_TYPE IpvAddrOctet;
    UINT1               au1Tmp[16];

    IpvAddrOctet.pu1_OctetList = au1Tmp;

    PTR_ASSIGN4 (IpvAddrOctet.pu1_OctetList, u4Element);

    IpvAddrOctet.i4_Length = QOS_IPV4_LEN;

    return (nmhSetIssAclL3FilterDstIpAddr (i4IssExtL3FilterNo, &IpvAddrOctet));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterDstIpAddr
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterDstIpAddr (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    tSNMP_OCTET_STRING_TYPE IpvAddrOctet;
    UINT1               au1Tmp[16];

    IpvAddrOctet.pu1_OctetList = au1Tmp;

    if (nmhGetIssAclL3FilterDstIpAddr (i4IssExtL3FilterNo, &IpvAddrOctet) ==
        SNMP_SUCCESS)
    {
        PTR_FETCH4 (*pElement, IpvAddrOctet.pu1_OctetList);
        return SNMP_SUCCESS;

    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterSrcIpAddr
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterSrcIpAddr (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                  UINT4 u4Element)
{
    tSNMP_OCTET_STRING_TYPE IpvAddrOctet;
    UINT1               au4Tmp[16];

    IpvAddrOctet.pu1_OctetList = au4Tmp;

    PTR_ASSIGN4 (IpvAddrOctet.pu1_OctetList, u4Element);

    IpvAddrOctet.i4_Length = QOS_IPV4_LEN;

    return (nmhTestv2IssAclL3FilterSrcIpAddr
            (pu4Error, i4IssExtL3FilterNo, &IpvAddrOctet));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterSrcIpAddr
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterSrcIpAddr (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    tSNMP_OCTET_STRING_TYPE IpvAddrOctet;
    UINT1               au1Tmp[16];

    IpvAddrOctet.pu1_OctetList = au1Tmp;

    PTR_ASSIGN4 (IpvAddrOctet.pu1_OctetList, u4Element);

    IpvAddrOctet.i4_Length = QOS_IPV4_LEN;

    return (nmhSetIssAclL3FilterSrcIpAddr (i4IssExtL3FilterNo, &IpvAddrOctet));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterSrcIpAddr
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterSrcIpAddr (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    tSNMP_OCTET_STRING_TYPE IpvAddrOctet;
    UINT1               au1Tmp[16];

    IpvAddrOctet.pu1_OctetList = au1Tmp;

    if (nmhGetIssAclL3FilterSrcIpAddr (i4IssExtL3FilterNo, &IpvAddrOctet) ==
        SNMP_SUCCESS)
    {
        PTR_FETCH4 (*pElement, IpvAddrOctet.pu1_OctetList);
        return SNMP_SUCCESS;

    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterDstIpAddrMask
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterDstIpAddrMask (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                      UINT4 u4Element)
{
    UINT4               u4Prefix;

    IssUtlConvertMaskToPrefix (u4Element, &u4Prefix);

    return (nmhTestv2IssAclL3FilterDstIpAddrPrefixLength
            (pu4Error, i4IssExtL3FilterNo, u4Prefix));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterDstIpAddrMask
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterDstIpAddrMask (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    UINT4               u4Prefix;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);
    if (pIssL3FilterEntry != NULL)
    {
        pIssL3FilterEntry->u4IssL3FilterDstIpAddrMask = u4Element;
    }

    IssUtlConvertMaskToPrefix (u4Element, &u4Prefix);
    return (nmhSetIssAclL3FilterDstIpAddrPrefixLength
            (i4IssExtL3FilterNo, u4Prefix));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterDstIpAddrMask
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterDstIpAddrMask (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    UINT4               u4Prefix;

    if (nmhGetIssAclL3FilterDstIpAddrPrefixLength
        (i4IssExtL3FilterNo, &u4Prefix) == SNMP_SUCCESS)
    {
        IssUtlConvertPrefixToMask (u4Prefix, pElement);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterSrcIpAddrMask
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterSrcIpAddrMask (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                      UINT4 u4Element)
{
    UINT4               u4Prefix;

    IssUtlConvertMaskToPrefix (u4Element, &u4Prefix);

    return (nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength
            (pu4Error, i4IssExtL3FilterNo, u4Prefix));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterSrcIpAddrMask
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterSrcIpAddrMask (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    UINT4               u4Prefix;
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);
    if (pIssL3FilterEntry != NULL)
    {
        pIssL3FilterEntry->u4IssL3FilterSrcIpAddrMask = u4Element;
    }

    IssUtlConvertMaskToPrefix (u4Element, &u4Prefix);

    return (nmhSetIssAclL3FilterSrcIpAddrPrefixLength
            (i4IssExtL3FilterNo, u4Prefix));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterSrcIpAddrMask
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterSrcIpAddrMask (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    UINT4               u4Prefix;

    if (nmhGetIssAclL3FilterSrcIpAddrPrefixLength
        (i4IssExtL3FilterNo, &u4Prefix) == SNMP_SUCCESS)
    {
        IssUtlConvertPrefixToMask (u4Prefix, pElement);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterMinDstProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterMinDstProtPort (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                       UINT4 u4Element)
{
    return (nmhTestv2IssAclL3FilterMinDstProtPort
            (pu4Error, i4IssExtL3FilterNo, u4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterMinDstProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterMinDstProtPort (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    return (nmhSetIssAclL3FilterMinDstProtPort (i4IssExtL3FilterNo, u4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMinDstProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMinDstProtPort (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    return (nmhGetIssAclL3FilterMinDstProtPort (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterMaxDstProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterMaxDstProtPort (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                       UINT4 u4Element)
{
    return (nmhTestv2IssAclL3FilterMaxDstProtPort
            (pu4Error, i4IssExtL3FilterNo, u4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterMaxDstProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterMaxDstProtPort (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    return (nmhSetIssAclL3FilterMaxDstProtPort (i4IssExtL3FilterNo, u4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMaxDstProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMaxDstProtPort (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    return (nmhGetIssAclL3FilterMaxDstProtPort (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterMinSrcProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterMinSrcProtPort (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                       UINT4 u4Element)
{
    return (nmhTestv2IssAclL3FilterMinSrcProtPort
            (pu4Error, i4IssExtL3FilterNo, u4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterMinSrcProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterMinSrcProtPort (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    return (nmhSetIssAclL3FilterMinSrcProtPort (i4IssExtL3FilterNo, u4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMinSrcProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMinSrcProtPort (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    return (nmhGetIssAclL3FilterMinSrcProtPort (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterMaxSrcProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterMaxSrcProtPort (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                       UINT4 u4Element)
{
    return (nmhTestv2IssAclL3FilterMaxSrcProtPort
            (pu4Error, i4IssExtL3FilterNo, u4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterMaxSrcProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterMaxSrcProtPort (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    return (nmhSetIssAclL3FilterMaxSrcProtPort (i4IssExtL3FilterNo, u4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMaxSrcProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMaxSrcProtPort (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    return (nmhGetIssAclL3FilterMaxSrcProtPort (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterInPortList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterInPortList (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                   tSNMP_OCTET_STRING_TYPE * pElement)
{
    return (nmhTestv2IssAclL3FilterInPortList
            (pu4Error, i4IssExtL3FilterNo, pElement));

}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterInPortList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterInPortList (INT4 i4IssExtL3FilterNo,
                                tSNMP_OCTET_STRING_TYPE * pElement)
{
    return (nmhSetIssAclL3FilterInPortList (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterInPortList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterInPortList (INT4 i4IssExtL3FilterNo,
                                tSNMP_OCTET_STRING_TYPE * pElement)
{
    return (nmhGetIssAclL3FilterInPortList (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterOutPortList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterOutPortList (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                    tSNMP_OCTET_STRING_TYPE * pElement)
{
    return (nmhTestv2IssAclL3FilterOutPortList
            (pu4Error, i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterOutPortList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterOutPortList (INT4 i4IssExtL3FilterNo,
                                 tSNMP_OCTET_STRING_TYPE * pElement)
{
    return (nmhSetIssAclL3FilterOutPortList (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterOutPortList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterOutPortList (INT4 i4IssExtL3FilterNo,
                                 tSNMP_OCTET_STRING_TYPE * pElement)
{
    return (nmhGetIssAclL3FilterOutPortList (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterAckBit
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterAckBit (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                               INT4 i4Element)
{
    return (nmhTestv2IssAclL3FilterAckBit
            (pu4Error, i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterAckBit
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterAckBit (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL3FilterAckBit (i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterAckBit
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterAckBit (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL3FilterAckBit (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterRstBit
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterRstBit (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                               INT4 i4Element)
{
    return (nmhTestv2IssAclL3FilterRstBit
            (pu4Error, i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterRstBit
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterRstBit (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL3FilterRstBit (i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterRstBit
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterRstBit (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL3FilterRstBit (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterTos
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterTos (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                            INT4 i4Element)
{
    return (nmhTestv2IssAclL3FilterTos
            (pu4Error, i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterTos
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterTos (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL3FilterTos (i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterTos
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterTos (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL3FilterTos (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterDscp
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                testValIssExtL3FilterDscp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterDscp (UINT4 *pu4ErrorCode, INT4 i4IssExtL3FilterNo,
                             INT4 i4TestValIssExtL3FilterDscp)
{
    return (nmhTestv2IssAclL3FilterDscp
            (pu4ErrorCode, i4IssExtL3FilterNo, i4TestValIssExtL3FilterDscp));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterDscp
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                setValIssExtL3FilterDscp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterDscp (INT4 i4IssExtL3FilterNo,
                          INT4 i4SetValIssExtL3FilterDscp)
{
    return (nmhSetIssAclL3FilterDscp
            (i4IssExtL3FilterNo, i4SetValIssExtL3FilterDscp));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterDscp
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                retValIssExtL3FilterDscp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterDscp (INT4 i4IssExtL3FilterNo,
                          INT4 *pi4RetValIssExtL3FilterDscp)
{
    return (nmhGetIssAclL3FilterDscp
            (i4IssExtL3FilterNo, pi4RetValIssExtL3FilterDscp));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterDirection
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterDirection (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                  INT4 i4Element)
{
    return (nmhTestv2IssAclL3FilterDirection
            (pu4Error, i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterDirection
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterDirection (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL3FilterDirection (i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterDirection
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterDirection (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL3FilterDirection (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterAction
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterAction (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                               INT4 i4Element)
{
    return (nmhTestv2IssAclL3FilterAction
            (pu4Error, i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterAction
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterAction (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL3FilterAction (i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterAction
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterAction (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL3FilterAction (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMatchCount
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMatchCount (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    return (nmhGetIssAclL3FilterMatchCount (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterStatus
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterStatus (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                               INT4 i4Element)
{
    return (nmhTestv2IssAclL3FilterStatus
            (pu4Error, i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterStatus
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterStatus (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    return (nmhSetIssAclL3FilterStatus (i4IssExtL3FilterNo, i4Element));
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterStatus
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterStatus (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    return (nmhGetIssAclL3FilterStatus (i4IssExtL3FilterNo, pElement));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssExtL3FilterTable
 Input       :  The Indices
                IssExtFilterL3No
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssExtL3FilterTable (INT4 *pi4IssExtL3FilterNo)
{
    return (nmhGetFirstIndexIssAclL3FilterTable (pi4IssExtL3FilterNo));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssExtL3FilterTable
 Input       :  The Indices
                IssExtL3FilterNo
                nextIssL3FilterNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssExtL3FilterTable (INT4 i4IssExtL3FilterNo,
                                    INT4 *pi4IssExtL3FilterNoOUT)
{
    return (nmhGetNextIndexIssAclL3FilterTable
            (i4IssExtL3FilterNo, pi4IssExtL3FilterNoOUT));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssExtL2FilterTable
 Input       :  The Indices
                IssExtL2FilterNo
                nextIssL2FilterNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssExtL2FilterTable (INT4 i4IssExtL2FilterNo,
                                    INT4 *pi4IssExtL2FilterNoOUT)
{

    if (i4IssExtL2FilterNo < 0)
    {
        return SNMP_FAILURE;
    }

    if ((IssExtSnmpLowGetNextValidL2FilterTableIndex
         (i4IssExtL2FilterNo, pi4IssExtL2FilterNoOUT)) == ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssExtL2FilterTable
 Input       :  The Indices
                IssExtFilterL2No
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssExtL2FilterTable (INT4 *pi4IssExtL2FilterNo)
{
    if ((IssExtSnmpLowGetFirstValidL2FilterTableIndex (pi4IssExtL2FilterNo)) ==
        ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
* Function    :  IssExtSnmpLowValidateIndexPortRateTable
* Input       :  i4CtrlIndex(PortCtrl Table Index or Rate Ctrl Table Index)
* Output      :  None
* Returns     :  ISS_SUCCESS/ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowValidateIndexPortRateTable (INT4 i4PortIndex)
{
    /* This routine is only for Port Ctrl Table */
    if (i4PortIndex > ISS_ZERO_ENTRY && i4PortIndex <= ISS_MAX_PORTS)
    {
        return ISS_SUCCESS;
    }
    return ISS_FAILURE;
}

/****************************************************************************
* Function    :  IssExtValidateRateCtrlEntry
* Input       :  i4RateCtrlIndex
* Output      :  None
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtValidateRateCtrlEntry (INT4 i4RateCtrlIndex)
{
    /* Validating the entry */
    if ((i4RateCtrlIndex <= ISS_ZERO_ENTRY) ||
        (i4RateCtrlIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        return ISS_FAILURE;
    }

    if ((gIssExGlobalInfo.apIssRateCtrlEntry[i4RateCtrlIndex] == NULL))
    {
        ISS_TRC_ARG1 (DATA_PATH_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "ISS: RATE CTRL ENTRY %d DOES NOT EXIST\n",
                      i4RateCtrlIndex);
        return ISS_FAILURE;
    }
    return ISS_SUCCESS;
}

/****************************************************************************
* Function    :  IssExtSnmpLowGetFirstValidL3FilterTableIndex
* Input       :  None
* Output      :  INT4 *pi4FirstL3FilterIndex
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowGetFirstValidL3FilterTableIndex (INT4 *pi4FirstL3FilterIndex)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;
    tIssL3FilterEntry  *pFirstL3FilterEntry = NULL;
    UINT1               u1FoundFlag = ISS_FALSE;

    pSllNode = ISS_SLL_FIRST (&ISS_L3FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssExtL3FilterEntry = (tIssL3FilterEntry *) pSllNode;
        if (u1FoundFlag == ISS_FALSE)
        {
            pFirstL3FilterEntry = pIssExtL3FilterEntry;
            u1FoundFlag = ISS_TRUE;
        }
        else
        {
            if (pFirstL3FilterEntry->i4IssL3FilterNo >
                pIssExtL3FilterEntry->i4IssL3FilterNo)
            {
                pFirstL3FilterEntry = pIssExtL3FilterEntry;
            }
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L3FILTER_LIST, pSllNode);
    }

    if (u1FoundFlag == ISS_TRUE)
    {
        *pi4FirstL3FilterIndex = pFirstL3FilterEntry->i4IssL3FilterNo;
        return ISS_SUCCESS;
    }

    return ISS_FAILURE;
}

/****************************************************************************
* Function    :  IssExtSnmpLowGetFirstValidL3FilterTableIndex
* Input       :  INT4 i4IssExt3FilterNo
* Output      :  INT4 *pi4NextL3FilterNo
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowGetNextValidL3FilterTableIndex (INT4 i4IssExtL3FilterNo,
                                             INT4 *pi4NextL3FilterNo)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;
    tIssL3FilterEntry  *pNextL3FilterEntry = NULL;
    UINT1               u1FoundFlag = ISS_FALSE;

    pSllNode = ISS_SLL_FIRST (&ISS_L3FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssExtL3FilterEntry = (tIssL3FilterEntry *) pSllNode;

        if (i4IssExtL3FilterNo < pIssExtL3FilterEntry->i4IssL3FilterNo)
        {
            if (u1FoundFlag == ISS_FALSE)
            {
                pNextL3FilterEntry = pIssExtL3FilterEntry;
                u1FoundFlag = ISS_TRUE;
            }
            else
            {
                if (pNextL3FilterEntry->i4IssL3FilterNo >
                    pIssExtL3FilterEntry->i4IssL3FilterNo)
                {
                    pNextL3FilterEntry = pIssExtL3FilterEntry;
                }
            }
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L3FILTER_LIST, pSllNode);
    }

    if (u1FoundFlag == ISS_TRUE)
    {
        *pi4NextL3FilterNo = pNextL3FilterEntry->i4IssL3FilterNo;
        return ISS_SUCCESS;
    }
    else
    {
        return ISS_FAILURE;
    }
}

/****************************************************************************
* Function    :  IssExtSnmpLowGetFirstValidL2FilterTableIndex
* Input       :  None
* Output      :  INT4 *pi4FirstL2FilterIndex
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowGetFirstValidL2FilterTableIndex (INT4 *pi4FirstL2FilterIndex)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;
    tIssL2FilterEntry  *pFirstL2FilterEntry = NULL;
    UINT1               u1FoundFlag = ISS_FALSE;

    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssExtL2FilterEntry = (tIssL2FilterEntry *) pSllNode;
        if (u1FoundFlag == ISS_FALSE)
        {
            pFirstL2FilterEntry = pIssExtL2FilterEntry;
            u1FoundFlag = ISS_TRUE;
        }
        else
        {
            if (pFirstL2FilterEntry->i4IssL2FilterNo >
                pIssExtL2FilterEntry->i4IssL2FilterNo)
            {
                pFirstL2FilterEntry = pIssExtL2FilterEntry;
            }
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_LIST, pSllNode);
    }

    if (u1FoundFlag == ISS_TRUE)
    {
        *pi4FirstL2FilterIndex = pFirstL2FilterEntry->i4IssL2FilterNo;
        return ISS_SUCCESS;
    }

    return ISS_FAILURE;
}

/****************************************************************************
* Function    :  IssExtSnmpLowGetFirstValidL2FilterTableIndex
* Input       :  INT4 i4IssExtL2FilterNo
* Output      :  INT4 *pi4NextL2FilterNo
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowGetNextValidL2FilterTableIndex (INT4 i4IssExtL2FilterNo,
                                             INT4 *pi4NextL2FilterNo)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;
    tIssL2FilterEntry  *pNextL2FilterEntry = NULL;
    UINT1               u1FoundFlag = ISS_FALSE;

    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssExtL2FilterEntry = (tIssL2FilterEntry *) pSllNode;

        if (i4IssExtL2FilterNo < pIssExtL2FilterEntry->i4IssL2FilterNo)
        {
            if (u1FoundFlag == ISS_FALSE)
            {
                pNextL2FilterEntry = pIssExtL2FilterEntry;
                u1FoundFlag = ISS_TRUE;
            }
            else
            {
                if (pNextL2FilterEntry->i4IssL2FilterNo >
                    pIssExtL2FilterEntry->i4IssL2FilterNo)
                {
                    pNextL2FilterEntry = pIssExtL2FilterEntry;
                }
            }
        }

        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_LIST, pSllNode);
    }

    if (u1FoundFlag == ISS_TRUE)
    {
        *pi4NextL2FilterNo = pNextL2FilterEntry->i4IssL2FilterNo;
        return ISS_SUCCESS;
    }
    else
    {
        return ISS_FAILURE;
    }
}

/****************************************************************************
* Function    :  IssExtGetL2FilterEntry
* Input       :  INT4 i4IssExtL2FilterNo
* Output      :  None
* Returns     :  tIssL2FilterEntry * 
*****************************************************************************/
tIssL2FilterEntry  *
IssExtGetL2FilterEntry (INT4 i4IssExtL2FilterNo)
{

    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssExtL2FilterEntry = (tIssL2FilterEntry *) pSllNode;

        if (pIssExtL2FilterEntry->i4IssL2FilterNo == i4IssExtL2FilterNo)
        {
            return pIssExtL2FilterEntry;
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_LIST, pSllNode);
    }
    return NULL;
}

/****************************************************************************
* Function    :  IssExtQualifyL2FilterData 
* Input       :  tIssL2FilterEntry **ppIssExtL2FilterEntry
* Output      :  None
* Returns     :  INT4
*****************************************************************************/
INT4
IssExtQualifyL2FilterData (tIssL2FilterEntry ** ppIssExtL2FilterEntry)
{
    INT4                i4RetVal = ISS_FAILURE;

    /* This intilization is done to avoid klocwork error */
    (*ppIssExtL2FilterEntry)->u1IssL2FilterStatus = ISS_NOT_READY;

    if ((*ppIssExtL2FilterEntry)->IssL2FilterAction != 0)
    {
        (*ppIssExtL2FilterEntry)->u1IssL2FilterStatus = ISS_NOT_IN_SERVICE;
        i4RetVal = ISS_SUCCESS;
    }
    return i4RetVal;
}

/****************************************************************************
* Function    :  IssExtGetL3FilterEntry
* Input       :  INT4 i4IssExtL3FilterNo
* Output      :  None
* Returns     :  tIssL3FilterEntry * 
*****************************************************************************/
tIssL3FilterEntry  *
IssExtGetL3FilterEntry (INT4 i4IssExtL3FilterNo)
{

    tIssSllNode        *pSllNode = NULL;
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pSllNode = ISS_SLL_FIRST (&ISS_L3FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssExtL3FilterEntry = (tIssL3FilterEntry *) pSllNode;

        if (pIssExtL3FilterEntry->i4IssL3FilterNo == i4IssExtL3FilterNo)
        {
            return pIssExtL3FilterEntry;
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L3FILTER_LIST, pSllNode);
    }
    return NULL;
}

/****************************************************************************
* Function    :  IssExtQualifyL3FilterData 
* Input       :  tIssL3FilterEntry **ppIssExtL3FilterEntry
* Output      :  None
* Returns     :  INT4
*****************************************************************************/
INT4
IssExtQualifyL3FilterData (tIssL3FilterEntry ** ppIssExtL3FilterEntry)
{
    INT4                i4RetVal = ISS_FAILURE;

    /* This intilization is done to avoid klocwork error */
    (*ppIssExtL3FilterEntry)->u1IssL3FilterStatus = ISS_NOT_READY;

    if (((*ppIssExtL3FilterEntry)->IssL3FilterAction != 0) &&
        ((*ppIssExtL3FilterEntry)->IssL3FilterDirection != 0))
    {
        (*ppIssExtL3FilterEntry)->u1IssL3FilterStatus = ISS_NOT_IN_SERVICE;
        i4RetVal = ISS_SUCCESS;
    }
    return i4RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssExtL3FilterTable
 Input       :  The Indices
                IssExtL3FilterNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssExtL3FilterTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

tIssL2FilterEntry  *
IssExtGetL2FilterTableEntry (INT4 i4IssExtL2FilterNo)
{
    tIssL2FilterEntry  *pL2Filter = NULL;

    ISS_LOCK ();

    pL2Filter = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    ISS_UNLOCK ();

    return (pL2Filter);
}

tIssL3FilterEntry  *
IssExtGetL3FilterTableEntry (INT4 i4IssExtL3FilterNo)
{
    tIssL3FilterEntry  *pL3Filter = NULL;

    ISS_LOCK ();

    pL3Filter = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    ISS_UNLOCK ();

    return (pL3Filter);
}

/****************************************************************************
* Function    :  IssExtUpdateFilterRefCount
* Description :  This function is used to Incremet or Decrement the Referenc 
*                Conunt of the Given L2 or L3 Filter Entry
* Input       :  u4FilterId     - L2 / L3 Filter Entry Id
*             :  u4FilterType   - L2 (ISS_L2FILTER) / L3 (ISS_L3FILTER)        
*             :  u4Action       - ISS_INCR (Incremet) / ISS_DECR (Decrement)
* Output      :  None
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtUpdateFilterRefCount (UINT4 u4FilterId, UINT4 u4FilterType,
                            UINT4 u4Action)
{
    tIssL2FilterEntry  *pL2FilterEntry = NULL;
    tIssL3FilterEntry  *pL3FilterEntry = NULL;

    ISS_LOCK ();

    if (u4FilterType == ISS_L2FILTER)
    {
        /* Check the L2 Filter Id */
        pL2FilterEntry = IssExtGetL2FilterEntry (u4FilterId);
        if (pL2FilterEntry == NULL)
        {
            ISS_UNLOCK ();
            return (ISS_FAILURE);
        }

        if (u4Action == ISS_INCR)
        {
            /* Incremnet the RefCount of L2Filter Entry in the L2Filter Table */
            pL2FilterEntry->u4RefCount = (pL2FilterEntry->u4RefCount) + 1;
        }
        else
        {
            /* Decrement the RefCount of L2Filter Entry in the L2Filter Table */
            pL2FilterEntry->u4RefCount = (pL2FilterEntry->u4RefCount) - 1;
        }
    }
    else
    {
        /* Check the L3 Filter Id */
        pL3FilterEntry = IssExtGetL3FilterEntry (u4FilterId);
        if (pL3FilterEntry == NULL)
        {
            ISS_UNLOCK ();
            return (ISS_FAILURE);
        }

        if (u4Action == ISS_INCR)
        {
            /* Incremnet the RefCount of L3Filter Entry in the L3Filter Table */
            pL3FilterEntry->u4RefCount = (pL3FilterEntry->u4RefCount) + 1;
        }
        else
        {
            /* Decrement the RefCount of L3Filter Entry in the L3Filter Table */
            pL3FilterEntry->u4RefCount = (pL3FilterEntry->u4RefCount) - 1;
        }

    }                            /* End of if - u4FilterType */

    ISS_UNLOCK ();
    return (ISS_SUCCESS);

}

/****************************************************************************
* Function    :  IssExtGetUdbFilterTableEntry
* Input       :  INT4 i4IssExtL3FilterNo
* Output      :  None
* Returns     :  tIssL3FilterEntry * 
*****************************************************************************/
tIssUserDefinedFilterTable *
IssExtGetUdbFilterTableEntry (UINT4 u4IssUdbFilterNo)
{

    tIssSllNode        *pSllNode = NULL;
    tIssUserDefinedFilterTable *pIssUdbFilterTableEntry = NULL;

    pSllNode = ISS_SLL_FIRST (&ISS_UDB_FILTER_TABLE_LIST);

    while (pSllNode != NULL)
    {
        pIssUdbFilterTableEntry = (tIssUserDefinedFilterTable *) pSllNode;
        if (pIssUdbFilterTableEntry == NULL
            || pIssUdbFilterTableEntry->pAccessFilterEntry == NULL)
        {
            return NULL;
        }

        if (pIssUdbFilterTableEntry->pAccessFilterEntry->
            u4UDBFilterId == u4IssUdbFilterNo)
        {
            return pIssUdbFilterTableEntry;
        }
        pSllNode = ISS_SLL_NEXT (&ISS_UDB_FILTER_TABLE_LIST, pSllNode);
    }
    return NULL;
}

/****************************************************************************
* Function    :  IssExtSnmpLowGetFirstValidUDBFilterTableIndex
* Input       :  None
* Output      :  UINT4 *pu4FirstUdbFilterIndex
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowGetFirstValidUDBFilterTableIndex (INT4 *pu4FirstUdbFilterIndex)
{
    tIssSllNode        *pSllNode = NULL;
    tIssUserDefinedFilterTable *pIssUdbFilterEntry = NULL;
    tIssUserDefinedFilterTable *pFirstUdbFilterEntry = NULL;
    UINT1               u1FoundFlag = ISS_FALSE;

    pSllNode = ISS_SLL_FIRST (&ISS_UDB_FILTER_TABLE_LIST);

    while (pSllNode != NULL)
    {
        pIssUdbFilterEntry = (tIssUserDefinedFilterTable *) pSllNode;

        if (pIssUdbFilterEntry->pAccessFilterEntry == NULL)
        {
            return ISS_FAILURE;
        }

        if (u1FoundFlag == ISS_FALSE)
        {
            pFirstUdbFilterEntry = pIssUdbFilterEntry;
            u1FoundFlag = ISS_TRUE;
        }
        else
        {
            if (pFirstUdbFilterEntry->pAccessFilterEntry->u4UDBFilterId >
                pIssUdbFilterEntry->pAccessFilterEntry->u4UDBFilterId)
            {
                pFirstUdbFilterEntry = pIssUdbFilterEntry;
            }
        }
        pSllNode = ISS_SLL_NEXT (&ISS_UDB_FILTER_TABLE_LIST, pSllNode);
    }

    if (u1FoundFlag == ISS_TRUE)
    {
        *pu4FirstUdbFilterIndex = pFirstUdbFilterEntry->
            pAccessFilterEntry->u4UDBFilterId;
        return ISS_SUCCESS;
    }

    return ISS_FAILURE;
}

/****************************************************************************
* Function    :  IssSnmpLowGetNextValidUdbFilterTableIndex
* Input       :  UINT4 u4IssUdbFilterNo
* Output      :  INT4 *pu4NextUdbFilterNo
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssSnmpLowGetNextValidUdbFilterTableIndex (UINT4 u4IssUdbFilterNo,
                                           UINT4 *pu4NextUdbFilterNo)
{
    tIssSllNode        *pSllNode = NULL;
    tIssUserDefinedFilterTable *pIssUdbFilterTableEntry = NULL;
    tIssUserDefinedFilterTable *pNextUdbFilterTableEntry = NULL;
    UINT1               u1FoundFlag = ISS_FALSE;

    pSllNode = ISS_SLL_FIRST (&ISS_UDB_FILTER_TABLE_LIST);

    while (pSllNode != NULL)
    {
        pIssUdbFilterTableEntry = (tIssUserDefinedFilterTable *) pSllNode;

        if (pIssUdbFilterTableEntry->pAccessFilterEntry == NULL)
        {
            return ISS_FAILURE;
        }

        if (u4IssUdbFilterNo <
            pIssUdbFilterTableEntry->pAccessFilterEntry->u4UDBFilterId)
        {
            if (u1FoundFlag == ISS_FALSE)
            {
                pNextUdbFilterTableEntry = pIssUdbFilterTableEntry;
                u1FoundFlag = ISS_TRUE;
            }
            else
            {
                if (pNextUdbFilterTableEntry->
                    pAccessFilterEntry->u4UDBFilterId >
                    pIssUdbFilterTableEntry->pAccessFilterEntry->u4UDBFilterId)
                {
                    pNextUdbFilterTableEntry = pIssUdbFilterTableEntry;
                }
            }
        }
        pSllNode = ISS_SLL_NEXT (&ISS_UDB_FILTER_TABLE_LIST, pSllNode);
    }

    if (u1FoundFlag == ISS_TRUE)
    {
        *pu4NextUdbFilterNo = pNextUdbFilterTableEntry->
            pAccessFilterEntry->u4UDBFilterId;
        return ISS_SUCCESS;
    }
    else
    {
        return ISS_FAILURE;
    }
}

/****************************************************************************
* Function    :  IssProcessAndOrNotFilterParamForL2
* Input       :  pIssAclL2FilterEntryOne,pIssAclL2FilterEntryTwo,u1Action,
*                pIssAclL2FilterEntry,pu1Result
* Output      :  None
* Returns     :  ISS_FAILURE / ISS_SUCCESS
*****************************************************************************/

INT4
IssProcessAndOrNotFilterParamForL2 (tIssL2FilterEntry * pIssAclL2FilterEntryOne,
                                    tIssL2FilterEntry * pIssAclL2FilterEntryTwo,
                                    UINT1 u1Action,
                                    tIssL2FilterEntry * pIssAclL2FilterEntry,
                                    UINT1 *pu1Result)
{
    if (u1Action == ISS_AND)
    {
        if ((pIssAclL2FilterEntryOne->IssL2FilterAction == ISS_REDIRECT_TO) ||
            (pIssAclL2FilterEntryTwo->IssL2FilterAction == ISS_REDIRECT_TO))
        {
            return ISS_FAILURE;
        }

        if (pIssAclL2FilterEntryOne->IssL2FilterAction !=
            pIssAclL2FilterEntryTwo->IssL2FilterAction)
        {
            return ISS_FAILURE;
        }

        if ((pIssAclL2FilterEntryOne->u4IssL2FilterProtocolType != 0) &&
            (pIssAclL2FilterEntryTwo->u4IssL2FilterProtocolType != 0))
        {
            return ISS_FAILURE;
        }
        else if (pIssAclL2FilterEntryOne->u4IssL2FilterProtocolType != 0)
        {
            pIssAclL2FilterEntry->u4IssL2FilterProtocolType =
                pIssAclL2FilterEntryOne->u4IssL2FilterProtocolType;

        }
        else if (pIssAclL2FilterEntryTwo->u4IssL2FilterProtocolType != 0)
        {
            pIssAclL2FilterEntry->u4IssL2FilterProtocolType =
                pIssAclL2FilterEntryTwo->u4IssL2FilterProtocolType;

        }

        if ((pIssAclL2FilterEntryOne->u4IssL2FilterCustomerVlanId != 0) &&
            (pIssAclL2FilterEntryTwo->u4IssL2FilterCustomerVlanId != 0))
        {
            return ISS_FAILURE;
        }
        else if (pIssAclL2FilterEntryOne->u4IssL2FilterCustomerVlanId != 0)
        {
            pIssAclL2FilterEntry->u4IssL2FilterCustomerVlanId =
                pIssAclL2FilterEntryOne->u4IssL2FilterCustomerVlanId;

        }
        else if (pIssAclL2FilterEntryTwo->u4IssL2FilterCustomerVlanId != 0)
        {
            pIssAclL2FilterEntry->u4IssL2FilterCustomerVlanId =
                pIssAclL2FilterEntryTwo->u4IssL2FilterCustomerVlanId;
        }

        if ((pIssAclL2FilterEntryOne->u2InnerEtherType != 0) &&
            (pIssAclL2FilterEntryTwo->u2InnerEtherType != 0))
        {
            return ISS_FAILURE;
        }
        else if (pIssAclL2FilterEntryOne->u2InnerEtherType != 0)
        {
            pIssAclL2FilterEntry->u2InnerEtherType =
                pIssAclL2FilterEntryOne->u2InnerEtherType;
        }
        else if (pIssAclL2FilterEntryTwo->u2InnerEtherType != 0)
        {
            pIssAclL2FilterEntry->u2InnerEtherType =
                pIssAclL2FilterEntryTwo->u2InnerEtherType;
        }
        if ((MEMCMP
             (pIssAclL2FilterEntryOne->IssL2FilterDstMacAddr, gNullMacAddr,
              sizeof (tMacAddr)) != 0)
            &&
            (MEMCMP
             (pIssAclL2FilterEntryTwo->IssL2FilterDstMacAddr, gNullMacAddr,
              sizeof (tMacAddr)) != 0))
        {
            return ISS_FAILURE;
        }

        else if (MEMCMP
                 (pIssAclL2FilterEntryOne->IssL2FilterDstMacAddr, gNullMacAddr,
                  sizeof (tMacAddr)) != 0)
        {
            MEMCPY (pIssAclL2FilterEntry->IssL2FilterDstMacAddr,
                    pIssAclL2FilterEntryOne->IssL2FilterDstMacAddr,
                    sizeof (tMacAddr));
        }
        else if (MEMCMP
                 (pIssAclL2FilterEntryTwo->IssL2FilterDstMacAddr, gNullMacAddr,
                  sizeof (tMacAddr)) != 0)
        {
            MEMCPY (pIssAclL2FilterEntry->IssL2FilterDstMacAddr,
                    pIssAclL2FilterEntryTwo->IssL2FilterDstMacAddr,
                    sizeof (tMacAddr));
        }

        if ((MEMCMP
             (pIssAclL2FilterEntryOne->IssL2FilterSrcMacAddr, gNullMacAddr,
              sizeof (tMacAddr) != 0))
            &&
            (MEMCMP
             (pIssAclL2FilterEntryTwo->IssL2FilterSrcMacAddr, gNullMacAddr,
              sizeof (tMacAddr)) != 0))
        {
            return ISS_FAILURE;
        }

        else if (MEMCMP
                 (pIssAclL2FilterEntryOne->IssL2FilterSrcMacAddr, gNullMacAddr,
                  sizeof (tMacAddr)) != 0)
        {
            MEMCPY (pIssAclL2FilterEntry->IssL2FilterSrcMacAddr,
                    pIssAclL2FilterEntryOne->IssL2FilterSrcMacAddr,
                    sizeof (tMacAddr));
        }
        else if (MEMCMP
                 (pIssAclL2FilterEntryTwo->IssL2FilterSrcMacAddr, gNullMacAddr,
                  sizeof (tMacAddr)) != 0)
        {
            MEMCPY (pIssAclL2FilterEntry->IssL2FilterSrcMacAddr,
                    pIssAclL2FilterEntryTwo->IssL2FilterSrcMacAddr,
                    sizeof (tMacAddr));
        }
        *pu1Result = ISS_USERDEFINED_L2FILTER_APPLY_AND_OP;

        pIssAclL2FilterEntry->IssL2FilterAction =
            pIssAclL2FilterEntryOne->IssL2FilterAction;
    }
    else if (u1Action == ISS_OR)
    {
        if ((pIssAclL2FilterEntryOne->IssL2FilterAction == ISS_REDIRECT_TO) ||
            (pIssAclL2FilterEntryTwo->IssL2FilterAction == ISS_REDIRECT_TO))
        {
            return ISS_FAILURE;
        }

        if (pIssAclL2FilterEntryOne->IssL2FilterAction ==
            pIssAclL2FilterEntryTwo->IssL2FilterAction)
        {
            return ISS_FAILURE;
        }

        *pu1Result = ISS_USERDEFINED_L2FILTER_APPLY_OR_OP;

        MEMCPY (pIssAclL2FilterEntry, pIssAclL2FilterEntryTwo,
                sizeof (tIssL2FilterEntry));

        pIssAclL2FilterEntry->IssL2FilterAction =
            pIssAclL2FilterEntryOne->IssL2FilterAction;
    }
    else if (u1Action == ISS_NOT)
    {
        *pu1Result = ISS_USERDEFINED_L2FILTER_APPLY_NOT_OP;

        MEMCPY (pIssAclL2FilterEntry, pIssAclL2FilterEntryOne,
                sizeof (tIssL2FilterEntry));

        if (pIssAclL2FilterEntryOne->IssL2FilterAction == ISS_ALLOW)
        {
            pIssAclL2FilterEntry->IssL2FilterAction = ISS_DROP;
        }
        else if (pIssAclL2FilterEntryOne->IssL2FilterAction == ISS_DROP)
        {
            pIssAclL2FilterEntry->IssL2FilterAction = ISS_ALLOW;
        }
        else
        {
            return ISS_FAILURE;
        }
    }
    else
    {
        return ISS_FAILURE;
    }

    return ISS_SUCCESS;
}

/****************************************************************************
* Function    :  IssProcessAndOrNotFilterParamForL3
* Input       :  pIssAclL3FilterEntryOne,pIssAclL3FilterEntryTwo,u1Action,
*                pIssAclL3FilterEntry,pu1Result
* Output      :  None
* Returns     :  ISS_FAILURE / ISS_SUCCESS
*****************************************************************************/

INT4
IssProcessAndOrNotFilterParamForL3 (tIssL3FilterEntry * pIssAclL3FilterEntryOne,
                                    tIssL3FilterEntry * pIssAclL3FilterEntryTwo,
                                    UINT1 u1Action,
                                    tIssL3FilterEntry * pIssAclL3FilterEntry,
                                    UINT1 *pu1Result)
{
    tIp6Addr            NullIp6Addr;

    MEMSET (&NullIp6Addr, 0, sizeof (tIp6Addr));

    if (u1Action == ISS_AND)
    {
        if ((pIssAclL3FilterEntryOne->IssL3FilterAction == ISS_REDIRECT_TO) ||
            (pIssAclL3FilterEntryTwo->IssL3FilterAction == ISS_REDIRECT_TO))
        {
            return ISS_FAILURE;
        }

        if (pIssAclL3FilterEntryOne->IssL3FilterAction !=
            pIssAclL3FilterEntryTwo->IssL3FilterAction)
        {
            return ISS_FAILURE;
        }
        if (pIssAclL3FilterEntryOne->i4IssL3MultiFieldClfrAddrType !=
            pIssAclL3FilterEntryTwo->i4IssL3MultiFieldClfrAddrType)
        {
            return ISS_FAILURE;
        }

        if ((pIssAclL3FilterEntryOne->i4IssL3FilterPriority != 1) &&
            (pIssAclL3FilterEntryTwo->i4IssL3FilterPriority != 1))
        {
            return ISS_FAILURE;
        }
        else if (pIssAclL3FilterEntryOne->i4IssL3FilterPriority != 0)
        {
            pIssAclL3FilterEntry->i4IssL3FilterPriority =
                pIssAclL3FilterEntryOne->i4IssL3FilterPriority;

        }
        else if (pIssAclL3FilterEntryTwo->i4IssL3FilterPriority != 0)
        {
            pIssAclL3FilterEntry->i4IssL3FilterPriority =
                pIssAclL3FilterEntryTwo->i4IssL3FilterPriority;

        }

        if ((pIssAclL3FilterEntryOne->IssL3FilterProtocol != ISS_PROTO_ANY) &&
            (pIssAclL3FilterEntryTwo->IssL3FilterProtocol != ISS_PROTO_ANY))
        {
            return ISS_FAILURE;
        }
        else if (pIssAclL3FilterEntryOne->IssL3FilterProtocol != 0)
        {
            pIssAclL3FilterEntry->IssL3FilterProtocol =
                pIssAclL3FilterEntryOne->IssL3FilterProtocol;

        }
        else if (pIssAclL3FilterEntryTwo->IssL3FilterProtocol != 0)
        {
            pIssAclL3FilterEntry->IssL3FilterProtocol =
                pIssAclL3FilterEntryTwo->IssL3FilterProtocol;

        }

        if ((pIssAclL3FilterEntryOne->i4IssL3FilterMessageType != ISS_ANY) &&
            (pIssAclL3FilterEntryTwo->i4IssL3FilterMessageType != ISS_ANY))
        {
            return ISS_FAILURE;
        }
        else if (pIssAclL3FilterEntryOne->i4IssL3FilterMessageType != 0)
        {
            pIssAclL3FilterEntry->i4IssL3FilterMessageType =
                pIssAclL3FilterEntryOne->i4IssL3FilterMessageType;

        }
        else if (pIssAclL3FilterEntryTwo->i4IssL3FilterMessageType != 0)
        {
            pIssAclL3FilterEntry->i4IssL3FilterMessageType =
                pIssAclL3FilterEntryTwo->i4IssL3FilterMessageType;

        }

        if ((pIssAclL3FilterEntryOne->i4IssL3FilterMessageCode != ISS_ANY) &&
            (pIssAclL3FilterEntryTwo->i4IssL3FilterMessageCode != ISS_ANY))
        {
            return ISS_FAILURE;
        }
        else if (pIssAclL3FilterEntryOne->i4IssL3FilterMessageCode != 0)
        {
            pIssAclL3FilterEntry->i4IssL3FilterMessageCode =
                pIssAclL3FilterEntryOne->i4IssL3FilterMessageCode;

        }
        else if (pIssAclL3FilterEntryTwo->i4IssL3FilterMessageCode != 0)
        {
            pIssAclL3FilterEntry->i4IssL3FilterMessageCode =
                pIssAclL3FilterEntryTwo->i4IssL3FilterMessageCode;

        }

        if ((pIssAclL3FilterEntryOne->u4IssL3FilterDstIpAddr != 0) &&
            (pIssAclL3FilterEntryTwo->u4IssL3FilterDstIpAddr != 0))
        {
            return ISS_FAILURE;
        }
        else if (pIssAclL3FilterEntryOne->u4IssL3FilterDstIpAddr != 0)
        {
            pIssAclL3FilterEntry->u4IssL3FilterDstIpAddr =
                pIssAclL3FilterEntryOne->u4IssL3FilterDstIpAddr;

        }
        else if (pIssAclL3FilterEntryTwo->u4IssL3FilterDstIpAddr != 0)
        {
            pIssAclL3FilterEntry->u4IssL3FilterDstIpAddr =
                pIssAclL3FilterEntryTwo->u4IssL3FilterDstIpAddr;

        }

        if ((pIssAclL3FilterEntryOne->u4IssL3FilterSrcIpAddr != 0) &&
            (pIssAclL3FilterEntryTwo->u4IssL3FilterSrcIpAddr != 0))
        {
            return ISS_FAILURE;
        }
        else if (pIssAclL3FilterEntryOne->u4IssL3FilterSrcIpAddr != 0)
        {
            pIssAclL3FilterEntry->u4IssL3FilterSrcIpAddr =
                pIssAclL3FilterEntryOne->u4IssL3FilterSrcIpAddr;

        }
        else if (pIssAclL3FilterEntryTwo->u4IssL3FilterSrcIpAddr != 0)
        {
            pIssAclL3FilterEntry->u4IssL3FilterSrcIpAddr =
                pIssAclL3FilterEntryTwo->u4IssL3FilterSrcIpAddr;

        }

        if ((pIssAclL3FilterEntryOne->u4IssL3FilterDstIpAddrMask != 0) &&
            (pIssAclL3FilterEntryTwo->u4IssL3FilterDstIpAddrMask != 0))
        {
            return ISS_FAILURE;
        }
        else if (pIssAclL3FilterEntryOne->u4IssL3FilterDstIpAddrMask != 0)
        {
            pIssAclL3FilterEntry->u4IssL3FilterDstIpAddrMask =
                pIssAclL3FilterEntryOne->u4IssL3FilterDstIpAddrMask;

        }
        else if (pIssAclL3FilterEntryTwo->u4IssL3FilterDstIpAddrMask != 0)
        {
            pIssAclL3FilterEntry->u4IssL3FilterDstIpAddrMask =
                pIssAclL3FilterEntryTwo->u4IssL3FilterDstIpAddrMask;

        }

        if ((pIssAclL3FilterEntryOne->u4IssL3FilterSrcIpAddrMask != 0) &&
            (pIssAclL3FilterEntryTwo->u4IssL3FilterSrcIpAddrMask != 0))
        {
            return ISS_FAILURE;
        }
        else if (pIssAclL3FilterEntryOne->u4IssL3FilterSrcIpAddrMask != 0)
        {
            pIssAclL3FilterEntry->u4IssL3FilterSrcIpAddrMask =
                pIssAclL3FilterEntryOne->u4IssL3FilterSrcIpAddrMask;

        }
        else if (pIssAclL3FilterEntryTwo->u4IssL3FilterSrcIpAddrMask != 0)
        {
            pIssAclL3FilterEntry->u4IssL3FilterSrcIpAddrMask =
                pIssAclL3FilterEntryTwo->u4IssL3FilterSrcIpAddrMask;

        }

        if ((pIssAclL3FilterEntryOne->u4IssL3FilterMinDstProtPort !=
             ISS_ZERO_ENTRY)
            && (pIssAclL3FilterEntryTwo->u4IssL3FilterMinDstProtPort !=
                ISS_ZERO_ENTRY))
        {
            return ISS_FAILURE;
        }
        else if (pIssAclL3FilterEntryOne->u4IssL3FilterMinDstProtPort != 0)
        {
            pIssAclL3FilterEntry->u4IssL3FilterMinDstProtPort =
                pIssAclL3FilterEntryOne->u4IssL3FilterMinDstProtPort;

        }
        else if (pIssAclL3FilterEntryTwo->u4IssL3FilterMinDstProtPort != 0)
        {
            pIssAclL3FilterEntry->u4IssL3FilterMinDstProtPort =
                pIssAclL3FilterEntryTwo->u4IssL3FilterMinDstProtPort;

        }

        if ((pIssAclL3FilterEntryOne->u4IssL3FilterMaxDstProtPort !=
             ISS_MAX_PORT_VALUE)
            && (pIssAclL3FilterEntryTwo->u4IssL3FilterMaxDstProtPort !=
                ISS_MAX_PORT_VALUE))
        {
            return ISS_FAILURE;
        }
        else if (pIssAclL3FilterEntryOne->u4IssL3FilterMaxDstProtPort != 0)
        {
            pIssAclL3FilterEntry->u4IssL3FilterMaxDstProtPort =
                pIssAclL3FilterEntryOne->u4IssL3FilterMaxDstProtPort;

        }
        else if (pIssAclL3FilterEntryTwo->u4IssL3FilterMaxDstProtPort != 0)
        {
            pIssAclL3FilterEntry->u4IssL3FilterMaxDstProtPort =
                pIssAclL3FilterEntryTwo->u4IssL3FilterMaxDstProtPort;

        }

        if ((pIssAclL3FilterEntryOne->u4IssL3FilterMinSrcProtPort !=
             ISS_ZERO_ENTRY)
            && (pIssAclL3FilterEntryTwo->u4IssL3FilterMinSrcProtPort !=
                ISS_ZERO_ENTRY))
        {
            return ISS_FAILURE;
        }
        else if (pIssAclL3FilterEntryOne->u4IssL3FilterMinSrcProtPort != 0)
        {
            pIssAclL3FilterEntry->u4IssL3FilterMinSrcProtPort =
                pIssAclL3FilterEntryOne->u4IssL3FilterMinSrcProtPort;

        }
        else if (pIssAclL3FilterEntryTwo->u4IssL3FilterMinSrcProtPort != 0)
        {
            pIssAclL3FilterEntry->u4IssL3FilterMinSrcProtPort =
                pIssAclL3FilterEntryTwo->u4IssL3FilterMinSrcProtPort;

        }

        if ((pIssAclL3FilterEntryOne->u4IssL3FilterMaxSrcProtPort !=
             ISS_MAX_PORT_VALUE)
            && (pIssAclL3FilterEntryTwo->u4IssL3FilterMaxSrcProtPort !=
                ISS_MAX_PORT_VALUE))
        {
            return ISS_FAILURE;
        }
        else if (pIssAclL3FilterEntryOne->u4IssL3FilterMaxSrcProtPort != 0)
        {
            pIssAclL3FilterEntry->u4IssL3FilterMaxSrcProtPort =
                pIssAclL3FilterEntryOne->u4IssL3FilterMaxSrcProtPort;

        }
        else if (pIssAclL3FilterEntryTwo->u4IssL3FilterMaxSrcProtPort != 0)
        {
            pIssAclL3FilterEntry->u4IssL3FilterMaxSrcProtPort =
                pIssAclL3FilterEntryTwo->u4IssL3FilterMaxSrcProtPort;

        }

        if ((pIssAclL3FilterEntryOne->IssL3FilterAckBit != ISS_ACK_ANY) &&
            (pIssAclL3FilterEntryTwo->IssL3FilterAckBit != ISS_ACK_ANY))
        {
            return ISS_FAILURE;
        }
        else if (pIssAclL3FilterEntryOne->IssL3FilterAckBit != 0)
        {
            pIssAclL3FilterEntry->IssL3FilterAckBit =
                pIssAclL3FilterEntryOne->IssL3FilterAckBit;

        }
        else if (pIssAclL3FilterEntryTwo->IssL3FilterAckBit != 0)
        {
            pIssAclL3FilterEntry->IssL3FilterAckBit =
                pIssAclL3FilterEntryTwo->IssL3FilterAckBit;

        }

        if ((pIssAclL3FilterEntryOne->IssL3FilterRstBit != ISS_ACK_ANY) &&
            (pIssAclL3FilterEntryTwo->IssL3FilterRstBit != ISS_ACK_ANY))
        {
            return ISS_FAILURE;
        }
        else if (pIssAclL3FilterEntryOne->IssL3FilterRstBit != 0)
        {
            pIssAclL3FilterEntry->IssL3FilterRstBit =
                pIssAclL3FilterEntryOne->IssL3FilterRstBit;

        }
        else if (pIssAclL3FilterEntryTwo->IssL3FilterRstBit != 0)
        {
            pIssAclL3FilterEntry->IssL3FilterRstBit =
                pIssAclL3FilterEntryTwo->IssL3FilterRstBit;

        }

        if ((pIssAclL3FilterEntryOne->IssL3FilterTos != ISS_TOS_INVALID) &&
            (pIssAclL3FilterEntryTwo->IssL3FilterTos != ISS_TOS_INVALID))
        {
            return ISS_FAILURE;
        }
        else if (pIssAclL3FilterEntryOne->IssL3FilterTos != 0)
        {
            pIssAclL3FilterEntry->IssL3FilterTos =
                pIssAclL3FilterEntryOne->IssL3FilterTos;

        }
        else if (pIssAclL3FilterEntryTwo->IssL3FilterTos != 0)
        {
            pIssAclL3FilterEntry->IssL3FilterTos =
                pIssAclL3FilterEntryTwo->IssL3FilterTos;

        }

        if ((pIssAclL3FilterEntryOne->i4IssL3FilterDscp != ISS_DSCP_INVALID) &&
            (pIssAclL3FilterEntryTwo->i4IssL3FilterDscp != ISS_DSCP_INVALID))
        {
            return ISS_FAILURE;
        }
        else if (pIssAclL3FilterEntryOne->i4IssL3FilterDscp != 0)
        {
            pIssAclL3FilterEntry->i4IssL3FilterDscp =
                pIssAclL3FilterEntryOne->i4IssL3FilterDscp;

        }
        else if (pIssAclL3FilterEntryTwo->i4IssL3FilterDscp != 0)
        {
            pIssAclL3FilterEntry->i4IssL3FilterDscp =
                pIssAclL3FilterEntryTwo->i4IssL3FilterDscp;

        }

        if ((pIssAclL3FilterEntryOne->IssL3FilterDirection != ISS_DIRECTION_IN)
            && (pIssAclL3FilterEntryTwo->IssL3FilterDirection !=
                ISS_DIRECTION_IN))
        {
            return ISS_FAILURE;
        }
        else if (pIssAclL3FilterEntryOne->IssL3FilterDirection != 0)
        {
            pIssAclL3FilterEntry->IssL3FilterDirection =
                pIssAclL3FilterEntryOne->IssL3FilterDirection;

        }
        else if (pIssAclL3FilterEntryTwo->IssL3FilterDirection != 0)
        {
            pIssAclL3FilterEntry->IssL3FilterDirection =
                pIssAclL3FilterEntryTwo->IssL3FilterDirection;

        }

        if ((pIssAclL3FilterEntryOne->u4IssL3FilterMatchCount != ISS_ZERO_ENTRY)
            && (pIssAclL3FilterEntryTwo->u4IssL3FilterMatchCount !=
                ISS_ZERO_ENTRY))
        {
            return ISS_FAILURE;
        }
        else if (pIssAclL3FilterEntryOne->u4IssL3FilterMatchCount != 0)
        {
            pIssAclL3FilterEntry->u4IssL3FilterMatchCount =
                pIssAclL3FilterEntryOne->u4IssL3FilterMatchCount;

        }
        else if (pIssAclL3FilterEntryTwo->u4IssL3FilterMatchCount != 0)
        {
            pIssAclL3FilterEntry->u4IssL3FilterMatchCount =
                pIssAclL3FilterEntryTwo->u4IssL3FilterMatchCount;

        }
        if ((MEMCMP (&pIssAclL3FilterEntryOne->ipv6SrcIpAddress, &NullIp6Addr,
                     sizeof (tIp6Addr)) != 0) &&
            (MEMCMP (&pIssAclL3FilterEntryTwo->ipv6SrcIpAddress, &NullIp6Addr,
                     sizeof (tIp6Addr)) != 0))
        {
            return ISS_FAILURE;
        }

        else if (MEMCMP
                 (&pIssAclL3FilterEntryOne->ipv6SrcIpAddress, &NullIp6Addr,
                  sizeof (tIp6Addr)) != 0)
        {
            MEMCPY (&pIssAclL3FilterEntry->ipv6SrcIpAddress,
                    &pIssAclL3FilterEntryOne->ipv6SrcIpAddress,
                    sizeof (tIp6Addr));
        }
        else if (MEMCMP
                 (&pIssAclL3FilterEntryTwo->ipv6SrcIpAddress, &NullIp6Addr,
                  sizeof (tIp6Addr)) != 0)
        {
            MEMCPY (&pIssAclL3FilterEntry->ipv6SrcIpAddress,
                    &pIssAclL3FilterEntryTwo->ipv6SrcIpAddress,
                    sizeof (tIp6Addr));
        }

        if ((MEMCMP (&pIssAclL3FilterEntryOne->ipv6DstIpAddress, &NullIp6Addr,
                     sizeof (tIp6Addr)) != 0) &&
            (MEMCMP (&pIssAclL3FilterEntryTwo->ipv6DstIpAddress, &NullIp6Addr,
                     sizeof (tIp6Addr)) != 0))
        {
            return ISS_FAILURE;
        }

        else if (MEMCMP
                 (&pIssAclL3FilterEntryOne->ipv6DstIpAddress, &NullIp6Addr,
                  sizeof (tIp6Addr)) != 0)
        {
            MEMCPY (&pIssAclL3FilterEntry->ipv6DstIpAddress,
                    &pIssAclL3FilterEntryOne->ipv6DstIpAddress,
                    sizeof (tIp6Addr));
        }
        else if (MEMCMP
                 (&pIssAclL3FilterEntryTwo->ipv6DstIpAddress, &NullIp6Addr,
                  sizeof (tIp6Addr)) != 0)
        {
            MEMCPY (&pIssAclL3FilterEntry->ipv6DstIpAddress,
                    &pIssAclL3FilterEntryTwo->ipv6DstIpAddress,
                    sizeof (tIp6Addr));
        }
        *pu1Result = ISS_USERDEFINED_L3FILTER_APPLY_AND_OP;

        pIssAclL3FilterEntry->IssL3FilterAction =
            pIssAclL3FilterEntryOne->IssL3FilterAction;
    }
    else if (u1Action == ISS_OR)
    {
        if ((pIssAclL3FilterEntryOne->IssL3FilterAction == ISS_REDIRECT_TO) ||
            (pIssAclL3FilterEntryTwo->IssL3FilterAction == ISS_REDIRECT_TO))
        {
            return ISS_FAILURE;
        }

        if (pIssAclL3FilterEntryOne->IssL3FilterAction ==
            pIssAclL3FilterEntryTwo->IssL3FilterAction)
        {
            return ISS_FAILURE;
        }

        *pu1Result = ISS_USERDEFINED_L3FILTER_APPLY_OR_OP;

        MEMCPY (pIssAclL3FilterEntry, pIssAclL3FilterEntryTwo,
                sizeof (tIssL3FilterEntry));

        pIssAclL3FilterEntry->IssL3FilterAction =
            pIssAclL3FilterEntryOne->IssL3FilterAction;
    }
    else if (u1Action == ISS_NOT)
    {
        *pu1Result = ISS_USERDEFINED_L3FILTER_APPLY_NOT_OP;

        MEMCPY (pIssAclL3FilterEntry, pIssAclL3FilterEntryOne,
                sizeof (tIssL3FilterEntry));

        if (pIssAclL3FilterEntryOne->IssL3FilterAction == ISS_ALLOW)
        {
            pIssAclL3FilterEntry->IssL3FilterAction = ISS_DROP;
        }
        else if (pIssAclL3FilterEntryOne->IssL3FilterAction == ISS_DROP)
        {
            pIssAclL3FilterEntry->IssL3FilterAction = ISS_ALLOW;
        }
        else
        {
            return ISS_FAILURE;
        }
    }
    else
    {
        return ISS_FAILURE;
    }

    return ISS_SUCCESS;
}
