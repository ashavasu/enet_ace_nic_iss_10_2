/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: issacl.c,v 1.2 2013/05/24 13:33:35 siva Exp $
 *
 * Description: This file contains routines for traffic separation and 
 *              protection against control plane CPU overloading, when 
 *              multiple protocol control traffic is directed to CPU for 
 *              processing and software controlled MAC learning and ageing 
 *              mechanism. This feature early detects the unwanted traffic 
 *              flow to control plane and avoids or rate limits this traffic 
 *              reaching to control plane with the help of ACL and QoS 
 *              features. Thus provides predictability in processing of 
 *              traffic at control plane.
 *********************************************************************/
#include "issexinc.h"
#include "fsissalw.h"
#include "fsissmlw.h"
#include "qosxtd.h"
#include "issacl.h"
#include "webiss.h"
/*****************************************************************************/
/* Function Name      :  IssAclInstallL2Filter                               */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                      to install new L2 ACL rule.                          */
/*                                                                           */
/* Input(s)           : pIssL2Filter - Pointer to ACL L2 Filter entry        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclInstallL2Filter (tIssL2FilterEntry * pIssL2Filter)
{
    INT4                i4Status = ISS_ZERO_ENTRY;
    UINT4               u4ErrCode = ISS_ZERO_ENTRY;
    tSNMP_OCTET_STRING_TYPE PortList;
    tIssL2FilterEntry  *pIssL2FilterInfo = NULL;
    INT1               *pu1String = NULL;
    UINT1               au1String[ISS_ACL_MAX_LEN];
    UINT1               au1InPortList[ISS_PORT_LIST_SIZE];
    UINT1               au1OutPortList[ISS_PORT_LIST_SIZE];

    ISS_TRC (INIT_SHUT_TRC, "\nACLL2:Entry IssAclInstallL2Filter function \n");

    MEMSET (&PortList, 0x0, sizeof (PortList));
    MEMSET (au1String, 0x0, ISS_ACL_MAX_LEN);
    MEMSET (au1InPortList, 0, ISS_PORT_LIST_SIZE);
    MEMSET (au1OutPortList, 0, ISS_PORT_LIST_SIZE);

    pu1String = (INT1 *) &au1String[0];

    if (pIssL2Filter == NULL)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL2:pIssL2Filter is NULL in %s, %d\n",
                      __FUNCTION__, __LINE__);
        return (ISS_FAILURE);
    }

    /* if filter already exists, do nothing and enter the configuration mode */

    if (nmhGetIssAclL2FilterStatus (pIssL2Filter->i4IssL2FilterNo, &i4Status)
        == SNMP_FAILURE)
    {
        /* Create a filter */
        if (nmhTestv2IssAclL2FilterStatus (&u4ErrCode,
                                           pIssL2Filter->i4IssL2FilterNo,
                                           ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL2:Failure of "
                          "nmhTestv2IssAclL2FilterStatus in %s, %d\n",
                          __FUNCTION__, __LINE__);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL2:ErrCode[%d]\tFilterNo[%d]\n",
                          u4ErrCode, pIssL2Filter->i4IssL2FilterNo);
            return (ISS_FAILURE);
        }

        if (nmhSetIssAclL2FilterStatus (pIssL2Filter->i4IssL2FilterNo,
                                        ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL2:Failure of "
                          "nmhSetIssAclL2FilterStatus in %s, %d FilterNo[%d]\n",
                          __FUNCTION__, __LINE__,
                          pIssL2Filter->i4IssL2FilterNo);
            return (ISS_FAILURE);
        }
    }
    else if (i4Status != ISS_NOT_READY)
    {
        /* Filter has previously been configured, it must be overwriten */
        if (nmhSetIssAclL2FilterStatus (pIssL2Filter->i4IssL2FilterNo,
                                        ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL2:Failure of "
                          "nmhSetIssAclL2FilterStatus in %s, %d FilterNo[%d]\n",
                          __FUNCTION__, __LINE__,
                          pIssL2Filter->i4IssL2FilterNo);
            return (ISS_FAILURE);
        }
        if (nmhSetIssAclL2FilterStatus (pIssL2Filter->i4IssL2FilterNo,
                                        ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL2:Failure of "
                          "nmhSetIssAclL2FilterStatus in %s, %dFilterNo[%d]\n",
                          __FUNCTION__, __LINE__,
                          pIssL2Filter->i4IssL2FilterNo);
            return (ISS_FAILURE);
        }
    }

    /* OID:issAclL2FilterPriority */
    if (nmhTestv2IssAclL2FilterPriority (&u4ErrCode,
                                         pIssL2Filter->i4IssL2FilterNo,
                                         pIssL2Filter->i4IssL2FilterPriority)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL2:Failure of "
                      "nmhTestv2IssAclL2FilterPriority in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL2: ErrCode[%d] FilterNo[%d]"
                      "FilterPriority[%d]\n", u4ErrCode,
                      pIssL2Filter->i4IssL2FilterNo,
                      pIssL2Filter->i4IssL2FilterPriority);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL2FilterPriority (pIssL2Filter->i4IssL2FilterNo,
                                  pIssL2Filter->i4IssL2FilterPriority);

    /* OID:issAclL2FilterEtherType */
    if (nmhTestv2IssAclL2FilterEtherType (&u4ErrCode,
                                          pIssL2Filter->i4IssL2FilterNo,
                                          pIssL2Filter->u2InnerEtherType)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL2:Failure of "
                      "nmhTestv2IssAclL2FilterEtherType in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL2: ErrCode[%d] FilterNo[%d]"
                      "InnerEtherType[%d]\n", u4ErrCode,
                      pIssL2Filter->i4IssL2FilterNo,
                      pIssL2Filter->u2InnerEtherType);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL2FilterEtherType (pIssL2Filter->i4IssL2FilterNo,
                                   pIssL2Filter->u2InnerEtherType);

    /* OID:issAclL2FilterProtocolType */
    if (nmhTestv2IssAclL2FilterProtocolType
        (&u4ErrCode,
         pIssL2Filter->i4IssL2FilterNo,
         pIssL2Filter->u4IssL2FilterProtocolType) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL2:Failure of "
                      "nmhTestv2IssAclL2FilterProtocolType in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL2: ErrCode[%d] FilterNo[%d]"
                      "L2FilterProtocolType[%d]\n", u4ErrCode,
                      pIssL2Filter->i4IssL2FilterNo,
                      pIssL2Filter->u4IssL2FilterProtocolType);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL2FilterProtocolType
        (pIssL2Filter->i4IssL2FilterNo,
         pIssL2Filter->u4IssL2FilterProtocolType);

    /* OID:issAclL2FilterDstMacAddr */
    if (nmhTestv2IssAclL2FilterDstMacAddr (&u4ErrCode,
                                           pIssL2Filter->i4IssL2FilterNo,
                                           pIssL2Filter->IssL2FilterDstMacAddr)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL2:Failure of "
                      "nmhTestv2IssAclL2FilterDstMacAddr in %s, %d\n",
                      __FUNCTION__, __LINE__);
        PrintMacAddress (pIssL2Filter->IssL2FilterDstMacAddr,
                         (UINT1 *) pu1String);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL2: ErrCode[%d] FilterNo[%d]"
                      "L2FilterDstMacAddr[%s]\n", u4ErrCode,
                      pIssL2Filter->i4IssL2FilterNo, pu1String);
        MEMSET (au1String, 0x0, ISS_ACL_MAX_LEN);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL2FilterDstMacAddr (pIssL2Filter->i4IssL2FilterNo,
                                    pIssL2Filter->IssL2FilterDstMacAddr);

    /* OID:issAclL2FilterSrcMacAddr */
    if (nmhTestv2IssAclL2FilterSrcMacAddr (&u4ErrCode,
                                           pIssL2Filter->i4IssL2FilterNo,
                                           pIssL2Filter->IssL2FilterSrcMacAddr)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL2:Failure of"
                      "nmhTestv2IssAclL2FilterSrcMacAddr in %s, %d\n",
                      __FUNCTION__, __LINE__);
        PrintMacAddress (pIssL2Filter->IssL2FilterDstMacAddr,
                         (UINT1 *) pu1String);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL2: ErrCode[%d] FilterNo[%d]"
                      "L2FilterSrcMacAddr[%s]\n", u4ErrCode,
                      pIssL2Filter->i4IssL2FilterNo, pu1String);
        MEMSET (au1String, 0x0, ISS_ACL_MAX_LEN);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL2FilterSrcMacAddr (pIssL2Filter->i4IssL2FilterNo,
                                    pIssL2Filter->IssL2FilterSrcMacAddr);

    /* OID:issAclL2FilterVlanId */
    if (nmhTestv2IssAclL2FilterVlanId
        (&u4ErrCode,
         pIssL2Filter->i4IssL2FilterNo,
         pIssL2Filter->u4IssL2FilterCustomerVlanId) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL2:Failure of "
                      "nmhTestv2IssAclL2FilterVlanId in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL2: ErrCode[%d] FilterNo[%d]"
                      "L2FilterCustomerVlanId[%d]\n", u4ErrCode,
                      pIssL2Filter->i4IssL2FilterNo,
                      pIssL2Filter->u4IssL2FilterCustomerVlanId);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL2FilterVlanId (pIssL2Filter->i4IssL2FilterNo,
                                pIssL2Filter->u4IssL2FilterCustomerVlanId);

    /* L2FilterInPortList */
    PortList.pu1_OctetList = &au1InPortList[0];
    MEMSET (PortList.pu1_OctetList, 0x0, sizeof (tIssPortList));
    MEMCPY (PortList.pu1_OctetList, pIssL2Filter->IssL2FilterInPortList,
            ISS_PORT_LIST_SIZE);
    PortList.i4_Length = ISS_PORT_LIST_SIZE;

    /* OID:issAclL2FilterInPortList */
    if (nmhTestv2IssAclL2FilterInPortList (&u4ErrCode,
                                           pIssL2Filter->i4IssL2FilterNo,
                                           &PortList) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL2:Failure of "
                      "nmhTestv2IssAclL2FilterInPortList in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL2: ErrCode[%d] FilterNo[%d]"
                      "L2FilterInPortList[%x]\n", u4ErrCode,
                      pIssL2Filter->i4IssL2FilterNo, PortList.pu1_OctetList);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL2FilterInPortList (pIssL2Filter->i4IssL2FilterNo, &PortList);

    /* OID:issAclL2FilterAction */
    if (nmhTestv2IssAclL2FilterAction (&u4ErrCode,
                                       pIssL2Filter->i4IssL2FilterNo,
                                       pIssL2Filter->IssL2FilterAction)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL2:Failure of "
                      "nmhTestv2IssAclL2FilterAction in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL2: ErrCode[%d] FilterNo[%d]"
                      "L2FilterAction[%d]\n", u4ErrCode,
                      pIssL2Filter->i4IssL2FilterNo,
                      pIssL2Filter->IssL2FilterAction);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL2FilterAction (pIssL2Filter->i4IssL2FilterNo,
                                pIssL2Filter->IssL2FilterAction);

    /* OID:issAclL2FilterMatchCount --- Read-Only */

    /* OID:issAclL2FilterOutPortList */
    PortList.pu1_OctetList = &au1OutPortList[0];
    MEMSET (PortList.pu1_OctetList, 0x0, sizeof (tIssPortList));
    MEMCPY (PortList.pu1_OctetList, pIssL2Filter->IssL2FilterOutPortList,
            ISS_PORT_LIST_SIZE);
    PortList.i4_Length = ISS_PORT_LIST_SIZE;
    if (nmhTestv2IssAclL2FilterOutPortList (&u4ErrCode,
                                            pIssL2Filter->i4IssL2FilterNo,
                                            &PortList) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL2:Failure of"
                      "nmhTestv2IssAclL2FilterOutPortList in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL2: ErrCode[%d] FilterNo[%d]"
                      "L2FilterOutPortList[%x]\n", u4ErrCode,
                      pIssL2Filter->i4IssL2FilterNo, PortList.pu1_OctetList);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL2FilterOutPortList (pIssL2Filter->i4IssL2FilterNo, &PortList);

    /* OID:issAclL2FilterDirection */
    if (nmhTestv2IssAclL2FilterDirection (&u4ErrCode,
                                          pIssL2Filter->i4IssL2FilterNo,
                                          pIssL2Filter->u1FilterDirection)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL2:Failure of "
                      "nmhTestv2IssAclL2FilterDirection in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL2: ErrCode[%d] FilterNo[%d]"
                      "L2FilterDirection[%d]\n", u4ErrCode,
                      pIssL2Filter->i4IssL2FilterNo,
                      pIssL2Filter->u1FilterDirection);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL2FilterDirection (pIssL2Filter->i4IssL2FilterNo,
                                   pIssL2Filter->u1FilterDirection);

    /* Updating Metro L2 parameters */
    pIssL2FilterInfo = IssExtGetL2FilterEntry (pIssL2Filter->i4IssL2FilterNo);

    /* OID:issAclL2FilterCreationMode */
    if (pIssL2FilterInfo != NULL)
    {
        pIssL2FilterInfo->u1IssL2FilterCreationMode
            = pIssL2Filter->u1IssL2FilterCreationMode;
    }

#ifdef ISS_METRO_WANTED
    /* OID: issMetroL2FilterOuterEtherType from "fsissmet.mib" */
    if (nmhTestv2IssMetroL2FilterOuterEtherType (&u4ErrCode,
                                                 pIssL2Filter->i4IssL2FilterNo,
                                                 pIssL2Filter->
                                                 u2OuterEtherType) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL2:Failure of"
                      "nmhTestv2IssMetroL2FilterOuterEtherType in %s, %d\n",
                      __FUNCTION__, __LINE__);
        return (ISS_FAILURE);
    }

    nmhSetIssMetroL2FilterOuterEtherType (pIssL2Filter->i4IssL2FilterNo,
                                          pIssL2Filter->u2OuterEtherType);

    /* OID: issMetroL2FilterSVlanId from "fsissmet.mib" */
    if (nmhTestv2IssMetroL2FilterSVlanId (&u4ErrCode,
                                          pIssL2Filter->i4IssL2FilterNo,
                                          pIssL2Filter->
                                          u4IssL2FilterServiceVlanId) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG2
            (ALL_FAILURE_TRC,
             "ACLL2:Failure of nmhTestv2IssMetroL2FilterSVlanId in %s, %d\n",
             __FUNCTION__, __LINE__);
        return (ISS_FAILURE);
    }

    nmhSetIssMetroL2FilterSVlanId (pIssL2Filter->i4IssL2FilterNo,
                                   pIssL2Filter->u4IssL2FilterServiceVlanId);

    /* OID: issMetroL2FilterSVlanPriority from "fsissmet.mib" */
    if (nmhTestv2IssMetroL2FilterSVlanPriority (&u4ErrCode,
                                                pIssL2Filter->i4IssL2FilterNo,
                                                pIssL2Filter->
                                                i1IssL2FilterSVlanPriority) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG2
            (ALL_FAILURE_TRC,
             "ACLL2:Failure of nmhTestv2IssMetroL2FilterSVlanPriority in"
             "%s, %d\n", __FUNCTION__, __LINE__);
        return (ISS_FAILURE);
    }

    nmhSetIssMetroL2FilterSVlanPriority
        (pIssL2Filter->i4IssL2FilterNo,
         pIssL2Filter->i1IssL2FilterSVlanPriority);

    /* OID: issMetroL2FilterCVlanPriority from "fsissmet.mib" */
    if (nmhTestv2IssMetroL2FilterCVlanPriority (&u4ErrCode,
                                                pIssL2Filter->i4IssL2FilterNo,
                                                pIssL2Filter->
                                                i1IssL2FilterCVlanPriority) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG2
            (ALL_FAILURE_TRC,
             "ACLL2:Failure of nmhTestv2IssMetroL2FilterCVlanPriority"
             "in %s, %d\n", __FUNCTION__, __LINE__);
        return (ISS_FAILURE);
    }

    nmhSetIssMetroL2FilterCVlanPriority
        (pIssL2Filter->i4IssL2FilterNo,
         pIssL2Filter->i1IssL2FilterCVlanPriority);

    /* OID: issMetroL2FilterPacketTagType from "fsissmet.mib" */
    if (nmhTestv2IssMetroL2FilterPacketTagType (&u4ErrCode,
                                                pIssL2Filter->i4IssL2FilterNo,
                                                pIssL2Filter->
                                                u1IssL2FilterTagType) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG2
            (ALL_FAILURE_TRC,
             "ACLL2:Failure of nmhTestv2IssMetroL2FilterPacketTagType "
             "in %s, %d\n", __FUNCTION__, __LINE__);
        return (ISS_FAILURE);
    }

    nmhSetIssMetroL2FilterPacketTagType (pIssL2Filter->i4IssL2FilterNo,
                                         pIssL2Filter->u1IssL2FilterTagType);
#endif /*ISS_METRO_WANTED */

    /* OID:issAclL2FilterStatus */
    nmhSetIssAclL2FilterStatus (pIssL2Filter->i4IssL2FilterNo, ACTIVE);

    ISS_TRC (INIT_SHUT_TRC, "\nACLL2:Exit IssAclInstallL2Filter function \n");

    return (ISS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      :  IssAclUnInstallL2Filter                             */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                            to delete the added L2 ACL rule.               */
/*                                                                           */
/* Input(s)           : pIssL2Filter - Pointer to ACL L2 Filter entry        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclUnInstallL2Filter (tIssL2FilterEntry * pIssL2Filter)
{
    INT4                i4Status = ISS_ZERO_ENTRY;
    UINT4               u4ErrCode = ISS_ZERO_ENTRY;

    ISS_TRC (INIT_SHUT_TRC, "\nACLL2:Entry IssAclUnInstallL2Filter function\n");

    if (pIssL2Filter == NULL)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL2:pIssL2Filter is NULL,"
                      "in  %s and line %d\n", __FUNCTION__, __LINE__);
        return (ISS_FAILURE);
    }

    if (nmhGetIssAclL2FilterStatus (pIssL2Filter->i4IssL2FilterNo, &i4Status)
        == SNMP_FAILURE)
    {
        /* Filter does not exist */
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL2:Invalid Filter number [%d]"
                      "and status[%d] in %s\n",
                      pIssL2Filter->i4IssL2FilterNo, i4Status, __FUNCTION__);
        return (ISS_SUCCESS);
    }
    else
    {
        if (nmhTestv2IssAclL2FilterStatus (&u4ErrCode,
                                           pIssL2Filter->i4IssL2FilterNo,
                                           ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL2:nmhTest of IssAclL2FilterS"
                          "tatus fails in %s and Line %d FiltNo %d\n",
                          __FUNCTION__, __LINE__,
                          pIssL2Filter->i4IssL2FilterNo);
            return (ISS_FAILURE);
        }

        if (nmhSetIssAclL2FilterStatus (pIssL2Filter->i4IssL2FilterNo,
                                        ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "ACLL2:nmhSet of IssAclL2FilterStatus"
                          " fails in %s and line %d and FilterNo [%d]\n",
                          __FUNCTION__, __LINE__,
                          pIssL2Filter->i4IssL2FilterNo);
            return (ISS_FAILURE);
        }
    }

    ISS_TRC (INIT_SHUT_TRC, "\nACLL2:Exit IssAclUnInstallL2Filter function \n");
    return (ISS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      :  IssAclInstallL3Filter                               */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                            to install new L3 ACL rule.                    */
/*                                                                           */
/* Input(s)           : pIssL3Filter - Pointer to ACL L3 Filter entry        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclInstallL3Filter (tIssL3FilterEntry * pIssL3Filter)
{
    INT4                i4Status = ISS_ZERO_ENTRY;
    UINT4               u4ErrCode = ISS_ZERO_ENTRY;
    tSNMP_OCTET_STRING_TYPE IpAddr;
    tSNMP_OCTET_STRING_TYPE PortList;
    tIssL3FilterEntry  *pIssMetroL3Filter = NULL;
    UINT1               au1InPortList[ISS_PORT_LIST_SIZE];
    UINT1               au1OutPortList[ISS_PORT_LIST_SIZE];
    tIp6Addr            FilterIp6Addr;

    /* While adding these value check for the default values 
     * in NOT only call the function to configure
     */
    MEMSET (&IpAddr, 0x0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PortList, 0x0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1InPortList, 0, ISS_PORT_LIST_SIZE);
    MEMSET (au1OutPortList, 0, ISS_PORT_LIST_SIZE);

    ISS_TRC (INIT_SHUT_TRC, "\nACLL3:Entry IssAclInstallL3Filter function \n");

    if (pIssL3Filter == NULL)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:pIssL3Filter is NULL,"
                      "in  %s and line %d\n", __FUNCTION__, __LINE__);
        return (ISS_FAILURE);
    }

    /* if filter already exists, do nothing and enter the configuration mode */

    if (nmhGetIssAclL3FilterStatus (pIssL3Filter->i4IssL3FilterNo, &i4Status)
        == SNMP_FAILURE)
    {
        /* Create a filter */
        if (nmhTestv2IssAclL3FilterStatus (&u4ErrCode,
                                           pIssL3Filter->i4IssL3FilterNo,
                                           ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                          "nmhTestv2IssAclL3FilterStatus in %s, %d\n",
                          __FUNCTION__, __LINE__);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:ErrCode[%d]\tFilterNo[%d]\n",
                          u4ErrCode, pIssL3Filter->i4IssL3FilterNo);
            return (ISS_FAILURE);
        }

        if (nmhSetIssAclL3FilterStatus (pIssL3Filter->i4IssL3FilterNo,
                                        ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                          "nmhSetIssAclL3FilterStatus in %s, %d FilterNo[%d]\n",
                          __FUNCTION__, __LINE__,
                          pIssL3Filter->i4IssL3FilterNo);
            return (ISS_FAILURE);
        }

    }
    else if (ISS_NOT_READY != i4Status)
    {
        /* Filter has previously been configured. This must be over-written 
         * First destroy the filter and create again */

        if (nmhSetIssAclL3FilterStatus (pIssL3Filter->i4IssL3FilterNo,
                                        ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                          "nmhSetIssAclL3FilterStatus in %s, %d FilterNo[%d]\n",
                          __FUNCTION__, __LINE__,
                          pIssL3Filter->i4IssL3FilterNo);
            return (ISS_FAILURE);
        }

        if (nmhSetIssAclL3FilterStatus (pIssL3Filter->i4IssL3FilterNo,
                                        ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                          "nmhSetIssAclL3FilterStatus in %s, %d FilterNo[%d]\n",
                          __FUNCTION__, __LINE__,
                          pIssL3Filter->i4IssL3FilterNo);
            return (ISS_FAILURE);
        }
    }

    /* OID:issAclL3FilterPriority */
    if (nmhTestv2IssAclL3FilterPriority (&u4ErrCode,
                                         pIssL3Filter->i4IssL3FilterNo,
                                         pIssL3Filter->i4IssL3FilterPriority)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                      "nmhTestv2IssAclL3FilterPriority in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3: ErrCode[%d] FilterNo[%d]"
                      "FilterPriority[%d]\n", u4ErrCode,
                      pIssL3Filter->i4IssL3FilterNo,
                      pIssL3Filter->i4IssL3FilterPriority);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterPriority (pIssL3Filter->i4IssL3FilterNo,
                                  pIssL3Filter->i4IssL3FilterPriority);

    /* OID:issAclL3FilterProtocol */
    if (nmhTestv2IssAclL3FilterProtocol (&u4ErrCode,
                                         pIssL3Filter->i4IssL3FilterNo,
                                         pIssL3Filter->IssL3FilterProtocol)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                      "nmhTestv2IssAclL3FilterProtocol in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3: ErrCode[%d] FilterNo[%d]"
                      "FilterProtocol[%d]\n", u4ErrCode,
                      pIssL3Filter->i4IssL3FilterNo,
                      pIssL3Filter->IssL3FilterProtocol);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterProtocol (pIssL3Filter->i4IssL3FilterNo,
                                  pIssL3Filter->IssL3FilterProtocol);

    /* OID:issAclL3FilterMessageType */
    if (nmhTestv2IssAclL3FilterMessageType
        (&u4ErrCode,
         pIssL3Filter->i4IssL3FilterNo,
         pIssL3Filter->i4IssL3FilterMessageType) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                      "nmhTestv2IssAclL3FilterMessageType in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3: ErrCode[%d] FilterNo[%d]"
                      "FilterMessageType[%d]\n", u4ErrCode,
                      pIssL3Filter->i4IssL3FilterNo,
                      pIssL3Filter->i4IssL3FilterMessageType);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterMessageType (pIssL3Filter->i4IssL3FilterNo,
                                     pIssL3Filter->i4IssL3FilterMessageType);

    /* OID:issAclL3FilterMessageCode */
    if (nmhTestv2IssAclL3FilterMessageCode
        (&u4ErrCode,
         pIssL3Filter->i4IssL3FilterNo,
         pIssL3Filter->i4IssL3FilterMessageCode) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                      "nmhTestv2IssAclL3FilterMessageCode in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3: ErrCode[%d] FilterNo[%d]"
                      "FilterMessageCode[%d]\n", u4ErrCode,
                      pIssL3Filter->i4IssL3FilterNo,
                      pIssL3Filter->i4IssL3FilterMessageCode);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterMessageCode (pIssL3Filter->i4IssL3FilterNo,
                                     pIssL3Filter->i4IssL3FilterMessageCode);

    /* OID:issAclL3FilteAddrType */
    if (nmhTestv2IssAclL3FilteAddrType
        (&u4ErrCode,
         pIssL3Filter->i4IssL3FilterNo,
         pIssL3Filter->i4IssL3MultiFieldClfrAddrType) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                      "nmhTestv2IssAclL3FilteAddrType in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3: ErrCode[%d] FilterNo[%d]"
                      "FilterAddrType[%d]\n", u4ErrCode,
                      pIssL3Filter->i4IssL3FilterNo,
                      pIssL3Filter->i4IssL3MultiFieldClfrAddrType);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilteAddrType (pIssL3Filter->i4IssL3FilterNo,
                                 pIssL3Filter->i4IssL3MultiFieldClfrAddrType);

    if (pIssL3Filter->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
    {
        IpAddr.pu1_OctetList = (UINT1 *) &FilterIp6Addr;
        MEMCPY (IpAddr.pu1_OctetList, &(pIssL3Filter->u4IssL3FilterDstIpAddr),
                QOS_IPV4_LEN);
        IpAddr.i4_Length = QOS_IPV4_LEN;
    }
    else if (pIssL3Filter->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
    {
        IpAddr.pu1_OctetList = (UINT1 *) &FilterIp6Addr;
        Ip6AddrCopy ((tIp6Addr *) (VOID *) IpAddr.pu1_OctetList,
                     &(pIssL3Filter->ipv6DstIpAddress));
        IpAddr.i4_Length = IP6_ADDR_SIZE;
    }
    /* OID:issAclL3FilterDstIpAddr */
    if (pIssL3Filter->u4IssL3FilterDstIpAddr != ISS_ZERO_ENTRY)
    {
        if (nmhTestv2IssAclL3FilterDstIpAddr (&u4ErrCode,
                                              pIssL3Filter->i4IssL3FilterNo,
                                              &IpAddr) == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                          "nmhTestv2IssAclL3FilterDstIpAddr in %s, %d\n",
                          __FUNCTION__, __LINE__);
            ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3: ErrCode[%d] FilterNo[%d]"
                          "IpAddr[%x]\n", u4ErrCode,
                          pIssL3Filter->i4IssL3FilterNo, IpAddr.pu1_OctetList);
            return (ISS_FAILURE);
        }
    }

    nmhSetIssAclL3FilterDstIpAddr (pIssL3Filter->i4IssL3FilterNo, &IpAddr);

    if (pIssL3Filter->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
    {
        MEMCPY (IpAddr.pu1_OctetList, &(pIssL3Filter->u4IssL3FilterSrcIpAddr),
                QOS_IPV4_LEN);
        IpAddr.i4_Length = QOS_IPV4_LEN;
    }
    else if (pIssL3Filter->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
    {
        Ip6AddrCopy ((tIp6Addr *) (VOID *) IpAddr.pu1_OctetList,
                     &(pIssL3Filter->ipv6SrcIpAddress));
        IpAddr.i4_Length = IP6_ADDR_SIZE;
    }

    /* OID:issAclL3FilterSrcIpAddr */
    if (pIssL3Filter->u4IssL3FilterSrcIpAddr != ISS_ZERO_ENTRY)
    {
        if (nmhTestv2IssAclL3FilterSrcIpAddr (&u4ErrCode,
                                              pIssL3Filter->i4IssL3FilterNo,
                                              &IpAddr) == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                          "nmhTestv2IssAclL3FilterSrcIpAddr in %s, %d\n",
                          __FUNCTION__, __LINE__);
            ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3: ErrCode[%d] FilterNo[%d]"
                          "IpAddr[%x]\n", u4ErrCode,
                          pIssL3Filter->i4IssL3FilterNo, IpAddr.pu1_OctetList);
            return (ISS_FAILURE);
        }
    }

    nmhSetIssAclL3FilterSrcIpAddr (pIssL3Filter->i4IssL3FilterNo, &IpAddr);

    /* OID:issAclL3FilterDstIpAddrPrefixLength */
    if (nmhTestv2IssAclL3FilterDstIpAddrPrefixLength
        (&u4ErrCode,
         pIssL3Filter->i4IssL3FilterNo,
         pIssL3Filter->u4IssL3FilterMultiFieldClfrDstPrefixLength)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                      "nmhTestv2IssAclL3FilterDstIpAddrPrefixLength in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3: ErrCode[%d] FilterNo[%d]"
                      "FilterMFCDstPrefxLength[%d]\n", u4ErrCode,
                      pIssL3Filter->i4IssL3FilterNo,
                      pIssL3Filter->u4IssL3FilterMultiFieldClfrDstPrefixLength);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterDstIpAddrPrefixLength
        (pIssL3Filter->i4IssL3FilterNo,
         pIssL3Filter->u4IssL3FilterMultiFieldClfrDstPrefixLength);

    /* OID:issAclL3FilterSrcIpAddrPrefixLength */
    if (nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength
        (&u4ErrCode,
         pIssL3Filter->i4IssL3FilterNo,
         pIssL3Filter->u4IssL3FilterMultiFieldClfrSrcPrefixLength)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                      "nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3: ErrCode[%d] FilterNo[%d]"
                      "FilterMFCSrcPrefxLength[%d]\n", u4ErrCode,
                      pIssL3Filter->i4IssL3FilterNo,
                      pIssL3Filter->u4IssL3FilterMultiFieldClfrSrcPrefixLength);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterSrcIpAddrPrefixLength
        (pIssL3Filter->i4IssL3FilterNo,
         pIssL3Filter->u4IssL3FilterMultiFieldClfrSrcPrefixLength);

    /* OID:issAclL3FilterMinDstProtPort */
    if (nmhTestv2IssAclL3FilterMinDstProtPort
        (&u4ErrCode,
         pIssL3Filter->i4IssL3FilterNo,
         pIssL3Filter->u4IssL3FilterMinDstProtPort) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                      "nmhTestv2IssAclL3FilterMinDstProtPort in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3: ErrCode[%d] FilterNo[%d]"
                      "FilterMinDstProtPort[%d]\n", u4ErrCode,
                      pIssL3Filter->i4IssL3FilterNo,
                      pIssL3Filter->u4IssL3FilterMinDstProtPort);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterMinDstProtPort
        (pIssL3Filter->i4IssL3FilterNo,
         pIssL3Filter->u4IssL3FilterMinDstProtPort);

    /* OID:issAclL3FilterMaxDstProtPort */
    if (nmhTestv2IssAclL3FilterMaxDstProtPort
        (&u4ErrCode,
         pIssL3Filter->i4IssL3FilterNo,
         pIssL3Filter->u4IssL3FilterMaxDstProtPort) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                      "nmhTestv2IssAclL3FilterMaxDstProtPort in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3: ErrCode[%d] FilterNo[%d]"
                      "FilterMaxDstProtPort[%d]\n", u4ErrCode,
                      pIssL3Filter->i4IssL3FilterNo,
                      pIssL3Filter->u4IssL3FilterMaxDstProtPort);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterMaxDstProtPort (pIssL3Filter->i4IssL3FilterNo,
                                        pIssL3Filter->
                                        u4IssL3FilterMaxDstProtPort);

    /* OID:issAclL3FilterMinSrcProtPort */
    if (nmhTestv2IssAclL3FilterMinSrcProtPort
        (&u4ErrCode,
         pIssL3Filter->i4IssL3FilterNo,
         pIssL3Filter->u4IssL3FilterMinSrcProtPort) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                      "nmhTestv2IssAclL3FilterMinSrcProtPort in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3: ErrCode[%d] FilterNo[%d]"
                      "FilterMinSrcProtPort[%d]\n", u4ErrCode,
                      pIssL3Filter->i4IssL3FilterNo,
                      pIssL3Filter->u4IssL3FilterMinSrcProtPort);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterMinSrcProtPort
        (pIssL3Filter->i4IssL3FilterNo,
         pIssL3Filter->u4IssL3FilterMinSrcProtPort);

    /* OID:issAclL3FilterMaxSrcProtPort */
    if (nmhTestv2IssAclL3FilterMaxSrcProtPort
        (&u4ErrCode,
         pIssL3Filter->i4IssL3FilterNo,
         pIssL3Filter->u4IssL3FilterMaxSrcProtPort) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                      "nmhTestv2IssAclL3FilterMaxSrcProtPort in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3: ErrCode[%d] FilterNo[%d]"
                      "FilterMaxSrcProtPort[%d]\n", u4ErrCode,
                      pIssL3Filter->i4IssL3FilterNo,
                      pIssL3Filter->u4IssL3FilterMaxSrcProtPort);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterMaxSrcProtPort
        (pIssL3Filter->i4IssL3FilterNo,
         pIssL3Filter->u4IssL3FilterMaxSrcProtPort);

    PortList.pu1_OctetList = &au1InPortList[0];
    MEMSET (PortList.pu1_OctetList, 0x0, sizeof (tIssPortList));
    MEMCPY (PortList.pu1_OctetList, pIssL3Filter->IssL3FilterInPortList,
            ISS_PORT_LIST_SIZE);
    PortList.i4_Length = ISS_PORT_LIST_SIZE;

    /* OID:issAclL3FilterInPortList */
    if (nmhTestv2IssAclL3FilterInPortList (&u4ErrCode,
                                           pIssL3Filter->i4IssL3FilterNo,
                                           &PortList) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                      "nmhTestv2IssAclL3FilterInPortList in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3: ErrCode[%d] FilterNo[%d]"
                      "L3FilterInPortList[%x]\n", u4ErrCode,
                      pIssL3Filter->i4IssL3FilterNo, PortList.pu1_OctetList);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterInPortList (pIssL3Filter->i4IssL3FilterNo, &PortList);

    PortList.pu1_OctetList = &au1OutPortList[0];
    MEMSET (PortList.pu1_OctetList, 0x0, sizeof (tIssPortList));
    MEMCPY (PortList.pu1_OctetList, pIssL3Filter->IssL3FilterOutPortList,
            ISS_PORT_LIST_SIZE);
    PortList.i4_Length = ISS_PORT_LIST_SIZE;

    /* OID:issAclL3FilterOutPortList */
    if (nmhTestv2IssAclL3FilterOutPortList (&u4ErrCode,
                                            pIssL3Filter->i4IssL3FilterNo,
                                            &PortList) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                      "nmhTestv2IssAclL3FilterOutPortList in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3: ErrCode[%d] FilterNo[%d]"
                      "L3FilterOutPortList[%x]\n", u4ErrCode,
                      pIssL3Filter->i4IssL3FilterNo, PortList.pu1_OctetList);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterOutPortList (pIssL3Filter->i4IssL3FilterNo, &PortList);

    /* OID:issAclL3FilterAckBit */
    if (nmhTestv2IssAclL3FilterAckBit (&u4ErrCode,
                                       pIssL3Filter->i4IssL3FilterNo,
                                       pIssL3Filter->IssL3FilterAckBit)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                      "nmhTestv2IssAclL3FilterAckBit in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3: ErrCode[%d] FilterNo[%d]"
                      "FilterAckBit[%d]\n", u4ErrCode,
                      pIssL3Filter->i4IssL3FilterNo,
                      pIssL3Filter->IssL3FilterAckBit);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterAckBit (pIssL3Filter->i4IssL3FilterNo,
                                pIssL3Filter->IssL3FilterAckBit);

    /* OID:issAclL3FilterRstBit */
    if (nmhTestv2IssAclL3FilterRstBit (&u4ErrCode,
                                       pIssL3Filter->i4IssL3FilterNo,
                                       pIssL3Filter->IssL3FilterRstBit)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                      "nmhTestv2IssAclL3FilterRstBit in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3: ErrCode[%d] FilterNo[%d]"
                      "FilterRstBit[%d]\n", u4ErrCode,
                      pIssL3Filter->i4IssL3FilterNo,
                      pIssL3Filter->IssL3FilterRstBit);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterRstBit (pIssL3Filter->i4IssL3FilterNo,
                                pIssL3Filter->IssL3FilterRstBit);

    /* OID:issAclL3FilterTos */
    if (pIssL3Filter->IssL3FilterTos != ISS_TOS_INVALID)
    {
        if (nmhTestv2IssAclL3FilterTos (&u4ErrCode,
                                        pIssL3Filter->i4IssL3FilterNo,
                                        pIssL3Filter->IssL3FilterTos)
            == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                          "nmhTestv2IssAclL3FilterTos in %s, %d\n",
                          __FUNCTION__, __LINE__);
            ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3: ErrCode[%d] FilterNo[%d]"
                          "FilterTos[%d]\n", u4ErrCode,
                          pIssL3Filter->i4IssL3FilterNo,
                          pIssL3Filter->IssL3FilterTos);
            return (ISS_FAILURE);
        }
    }

    nmhSetIssAclL3FilterTos (pIssL3Filter->i4IssL3FilterNo,
                             pIssL3Filter->IssL3FilterTos);

    /* OID:issAclL3FilterDscp */
    if (nmhTestv2IssAclL3FilterDscp (&u4ErrCode,
                                     pIssL3Filter->i4IssL3FilterNo,
                                     pIssL3Filter->i4IssL3FilterDscp)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                      "nmhTestv2IssAclL3FilterDscp in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3: ErrCode[%d] FilterNo[%d]"
                      "FilterDscp[%d]\n", u4ErrCode,
                      pIssL3Filter->i4IssL3FilterNo,
                      pIssL3Filter->i4IssL3FilterDscp);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterDscp (pIssL3Filter->i4IssL3FilterNo,
                              pIssL3Filter->i4IssL3FilterDscp);

    /* OID:issAclL3FilterDirection */
    if (nmhTestv2IssAclL3FilterDirection (&u4ErrCode,
                                          pIssL3Filter->i4IssL3FilterNo,
                                          pIssL3Filter->IssL3FilterDirection)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                      "nmhTestv2IssAclL3FilterDirection in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3: ErrCode[%d] FilterNo[%d]"
                      "FilterDirection[%d]\n", u4ErrCode,
                      pIssL3Filter->i4IssL3FilterNo,
                      pIssL3Filter->IssL3FilterDirection);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterDirection (pIssL3Filter->i4IssL3FilterNo,
                                   pIssL3Filter->IssL3FilterDirection);

    /* OID:issAclL3FilterAction */
    if (nmhTestv2IssAclL3FilterAction (&u4ErrCode,
                                       pIssL3Filter->i4IssL3FilterNo,
                                       pIssL3Filter->IssL3FilterAction)
        == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                      "nmhTestv2IssAclL3FilterAction in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3: ErrCode[%d] FilterNo[%d]"
                      "FilterAction[%d]\n", u4ErrCode,
                      pIssL3Filter->i4IssL3FilterNo,
                      pIssL3Filter->IssL3FilterAction);
        return (ISS_FAILURE);
    }

    nmhSetIssAclL3FilterAction (pIssL3Filter->i4IssL3FilterNo,
                                pIssL3Filter->IssL3FilterAction);

    /*OID: issAclL3FilterMatchCount -- Read-only */

    /* OID:issAclL3FilterFlowId */
    if (pIssL3Filter->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
    {
        if (nmhTestv2IssAclL3FilterFlowId
            (&u4ErrCode,
             pIssL3Filter->i4IssL3FilterNo,
             pIssL3Filter->u4IssL3MultiFieldClfrFlowId) == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:Failure of "
                          "nmhTestv2IssAclL3FilterFlowId in %s, %d\n",
                          __FUNCTION__, __LINE__);
            ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3: ErrCode[%d] FilterNo[%d]"
                          "FilterMFCfrFlowId[%d]\n", u4ErrCode,
                          pIssL3Filter->i4IssL3FilterNo,
                          pIssL3Filter->u4IssL3MultiFieldClfrFlowId);
            return (ISS_FAILURE);
        }

        nmhSetIssAclL3FilterFlowId (pIssL3Filter->i4IssL3FilterNo,
                                    pIssL3Filter->u4IssL3MultiFieldClfrFlowId);
    }
    /* IssAclL3FilterCreationMode is Read-Only */
    pIssMetroL3Filter = IssExtGetL3FilterEntry (pIssL3Filter->i4IssL3FilterNo);

    /* OID:issAclL3FilterCreationMode */
    if (pIssMetroL3Filter != NULL)
    {
        pIssMetroL3Filter->u1IssL3FilterCreationMode
            = pIssL3Filter->u1IssL3FilterCreationMode;
    }

#ifdef ISS_METRO_WANTED
    /* OID:issMetroL3FilterSVlanId from fsissmet.mib */
    if (nmhTestv2IssMetroL3FilterSVlanId (&u4ErrCode,
                                          pIssL3Filter->i4IssL3FilterNo,
                                          pIssL3Filter->u4SVlanId) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "ACLL3:Failure of nmhTestv2IssMetroL3FilterSVlanId in %s, %d\n",
                      __FUNCTION__, __LINE__);
        return (ISS_FAILURE);
    }

    nmhSetIssMetroL3FilterSVlanId (pIssL3Filter->i4IssL3FilterNo,
                                   pIssL3Filter->u4SVlanId);

    /* OID:issMetroL3FilterSVlanPriority from fsissmet.mib */
    if (nmhTestv2IssMetroL3FilterSVlanPriority (&u4ErrCode,
                                                pIssL3Filter->i4IssL3FilterNo,
                                                pIssL3Filter->
                                                i1SVlanPriority) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "ACLL3:Failure of nmhTestv2IssMetroL3FilterSVlanPriority in %s, %d\n",
                      __FUNCTION__, __LINE__);
        return (ISS_FAILURE);
    }

    nmhSetIssMetroL3FilterSVlanPriority (pIssL3Filter->i4IssL3FilterNo,
                                         pIssL3Filter->i1SVlanPriority);

    /* OID:issMetroL3FilterCVlanId from fsissmet.mib */
    if (nmhTestv2IssMetroL3FilterCVlanId (&u4ErrCode,
                                          pIssL3Filter->i4IssL3FilterNo,
                                          pIssL3Filter->u4CVlanId) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "ACLL3:Failure of nmhTestv2IssMetroL3FilterCVlanId in %s, %d\n",
                      __FUNCTION__, __LINE__);
        return (ISS_FAILURE);
    }

    nmhSetIssMetroL3FilterCVlanId (pIssL3Filter->i4IssL3FilterNo,
                                   pIssL3Filter->u4CVlanId);

    /* OID:issMetroL3FilterCVlanPriority from fsissmet.mib */
    if (nmhTestv2IssMetroL3FilterCVlanPriority (&u4ErrCode,
                                                pIssL3Filter->i4IssL3FilterNo,
                                                pIssL3Filter->
                                                i1CVlanPriority) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "ACLL3:Failure of nmhTestv2IssMetroL3FilterCVlanPriority in %s, %d\n",
                      __FUNCTION__, __LINE__);
        return (ISS_FAILURE);
    }

    nmhSetIssMetroL3FilterCVlanPriority (pIssL3Filter->i4IssL3FilterNo,
                                         pIssL3Filter->i1CVlanPriority);

    /* OID:issMetroL3FilterPacketTagType from fsissmet.mib */
    if (nmhTestv2IssMetroL3FilterPacketTagType (&u4ErrCode,
                                                pIssL3Filter->i4IssL3FilterNo,
                                                pIssL3Filter->
                                                u1IssL3FilterTagType) ==
        SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "ACLL3:Failure of nmhTestv2IssMetroL3FilterPacketTagType in %s, %d\n",
                      __FUNCTION__, __LINE__);
        return (ISS_FAILURE);
    }

    nmhSetIssMetroL3FilterPacketTagType (pIssL3Filter->i4IssL3FilterNo,
                                         pIssL3Filter->u1IssL3FilterTagType);
#endif /* ISS_METRO_WANTED */
    /* OID:issAclL3FilterStatus */
    nmhSetIssAclL3FilterStatus (pIssL3Filter->i4IssL3FilterNo, ACTIVE);

    ISS_TRC (INIT_SHUT_TRC, "\nACLL3:Exit IssAclInstallL3Filter function \n");

    return (ISS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      :  IssAclUnInstallL3Filter                             */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                            to delete the added L3 ACL rule.               */
/*                                                                           */
/* Input(s)           : pIssL3Filter - Pointer to ACL L3 Filter entry        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclUnInstallL3Filter (tIssL3FilterEntry * pIssL3Filter)
{
    INT4                i4Status = ISS_ZERO_ENTRY;
    UINT4               u4ErrCode = ISS_ZERO_ENTRY;

    ISS_TRC (INIT_SHUT_TRC,
             "\nACLL3:Entry IssAclUnInstallL3Filter function \n");

    if (pIssL3Filter == NULL)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACLL3:pIssL3Filter is NULL,"
                      "in  %s and line %d", __FUNCTION__, __LINE__);
        return (ISS_FAILURE);
    }

    if (nmhGetIssAclL3FilterStatus (pIssL3Filter->i4IssL3FilterNo, &i4Status)
        == SNMP_FAILURE)
    {
        /* Filter does not exist */
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACLL3:Invalid Filter number [%d]"
                      "status[%d] in %s\n", pIssL3Filter->i4IssL3FilterNo,
                      i4Status, __FUNCTION__);
        return (ISS_SUCCESS);
    }
    else
    {
        if (nmhTestv2IssAclL3FilterStatus (&u4ErrCode,
                                           pIssL3Filter->i4IssL3FilterNo,
                                           ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_TRC_ARG1 (ALL_FAILURE_TRC, "ACLL3:nmhTest of"
                          "IssAclL3FilterStatus fails in %s\n", __FUNCTION__);
            return (ISS_FAILURE);
        }

        if (nmhSetIssAclL3FilterStatus (pIssL3Filter->i4IssL3FilterNo,
                                        ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_TRC_ARG1 (ALL_FAILURE_TRC, "ACLL3:nmhSet of"
                          "IssAclL3FilterStatus fails in %s\n", __FUNCTION__);

            return (ISS_FAILURE);
        }
    }

    ISS_TRC (INIT_SHUT_TRC, "\nACLL3:Exit IssAclUnInstallL3Filter function \n");
    return (ISS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      :  IssACLInstallFilter                                 */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                      to install L2 and L3 ACL rules.                      */
/*                                                                           */
/* Input(s)           :  u4FilterType -- Acl filter type (L2 or L3)          */
/*                       pIssL2Filter - Pointer to ACL L2 Filter entry       */
/*                       pIssL3Filter - Pointer to ACL L3 Filter entry       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssACLInstallFilter (UINT4 u4FilterType, tIssL2FilterEntry * pIssL2Filter,
                     tIssL3FilterEntry * pIssL3Filter)
{
    ISS_TRC (INIT_SHUT_TRC, "\nACL:Entry IssACLInstallFilter function \n");

    if (u4FilterType == QOS_ACL_L2_CONFIG)
    {
        if (IssAclInstallL2Filter (pIssL2Filter) == ISS_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "ACL:Failure of IssAclInstallL2Filter"
                          " in %s, %d\n", __FUNCTION__, __LINE__);
            return (ISS_FAILURE);
        }
        ISS_TRC (INIT_SHUT_TRC, "\nACL:Exit IssACLInstallFilter function\n");
        return (ISS_SUCCESS);
    }
    else if (u4FilterType == QOS_ACL_L3_CONFIG)
    {
        if (IssAclInstallL3Filter (pIssL3Filter) == ISS_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "ACL:Failure of IssAclInstallL3Filter"
                          "in %s, %d\n", __FUNCTION__, __LINE__);
            return (ISS_FAILURE);
        }

        ISS_TRC (INIT_SHUT_TRC, "\nACL:Exit IssACLInstallFilter function\n");
        return (ISS_SUCCESS);
    }

    /* No filters types to configure */
    ISS_TRC (INIT_SHUT_TRC, "\nACL:Failure IssACLInstallFilter function\n");
    return (ISS_FAILURE);
}

/*****************************************************************************/
/* Function Name      :  IssAclQosProcessL2Proto                             */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                      to install L2 ACL rules and QoS configurations.      */
/* Input(s)           :                                                      */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclQosProcessL2Proto (VOID)
{
    UINT4               u4FilterType = QOS_ACL_L2_CONFIG;
    UINT4               u4L2Protocols = ISS_ZERO_ENTRY;
    tAclQosMapInfo      AclQosL2MapInfo;

    MEMSET (&AclQosL2MapInfo, 0x0, sizeof (tAclQosMapInfo));

    ISS_TRC (INIT_SHUT_TRC, "\nACL:Entry IssAclQosProcessL2Proto function \n");

    for (u4L2Protocols = ACL_MIN_L2_SYSTEM_PROTOCOLS;
         u4L2Protocols <= ACL_MAX_L2_SYSTEM_PROTOCOLS; u4L2Protocols++)
    {
        /* L2 Filter Configuration */
        if (IssACLInstallFilter
            (u4FilterType,
             &gaQoSAcLInfoL2Protocols[u4L2Protocols].L2FilterInfo, NULL)
            == ISS_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "ACL:Failure of IssACLInstallFilter in %s, %d\n",
                          __FUNCTION__, __LINE__);
            return (ISS_FAILURE);
        }

        AclQosL2MapInfo.QosIds.u4ClassMapId
            = gaQoSAcLInfoL2Protocols[u4L2Protocols].QosIds.u4ClassMapId;
        AclQosL2MapInfo.QosIds.u4ClassId
            = gaQoSAcLInfoL2Protocols[u4L2Protocols].QosIds.u4ClassId;
        AclQosL2MapInfo.QosIds.u4MeterId
            = gaQoSAcLInfoL2Protocols[u4L2Protocols].QosIds.u4MeterId;
        AclQosL2MapInfo.QosIds.u4PolicyMapId
            = gaQoSAcLInfoL2Protocols[u4L2Protocols].QosIds.u4PolicyMapId;
        AclQosL2MapInfo.u4FilterId
            =
            gaQoSAcLInfoL2Protocols[u4L2Protocols].L2FilterInfo.i4IssL2FilterNo;
        AclQosL2MapInfo.u4ProtocolRate =
            gaQoSAcLInfoL2Protocols[u4L2Protocols].u4ProtocolRate;
        AclQosL2MapInfo.u1CpuQueueNo =
            gaQoSAcLInfoL2Protocols[u4L2Protocols].u1CpuQueueNo;
        /* QoS parameter configurations */
        ISS_UNLOCK ();
        if (QosApiAddMfcRateLimiter (u4FilterType, &AclQosL2MapInfo) ==
            QOS_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "ACL:Failure of QosAddMfcFilter in %s, %d\n",
                          __FUNCTION__, __LINE__);
            ISS_LOCK ();
            return (ISS_FAILURE);
        }
        ISS_LOCK ();
    }

    ISS_TRC (INIT_SHUT_TRC, "\nACL:Exit IssAclQosProcessL2Proto function \n");

    return (ISS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      :  IssAclQosProcessL3Proto                             */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                            to install L3 ACL rules and QoS configurations.*/
/*                                                                           */
/* Input(s)           :                                                      */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclQosProcessL3Proto (VOID)
{
    UINT4               u4FilterType = QOS_ACL_L3_CONFIG;
    UINT4               u4L3Protocols = ISS_ZERO_ENTRY;
    tAclQosMapInfo      AclQosL3MapInfo;

    MEMSET (&AclQosL3MapInfo, 0x0, sizeof (tAclQosMapInfo));

    ISS_TRC (INIT_SHUT_TRC, "\nACL:Entry IssAclQosProcessL3Proto function \n");

    for (u4L3Protocols = ACL_MIN_L3_SYSTEM_PROTOCOLS;
         u4L3Protocols <= ACL_MAX_L3_SYSTEM_PROTOCOLS; u4L3Protocols++)
    {
        /* L3 Filter Configuration */
        if (IssACLInstallFilter
            (u4FilterType, NULL,
             &(gaQoSAcLInfoL3Protocols[u4L3Protocols].L3FilterInfo))
            == ISS_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "ACL:Failure of IssACLInstallFilter in %s, %d\n",
                          __FUNCTION__, __LINE__);
            return (ISS_FAILURE);
        }

        AclQosL3MapInfo.QosIds.u4ClassMapId
            = gaQoSAcLInfoL3Protocols[u4L3Protocols].QosIds.u4ClassMapId;
        AclQosL3MapInfo.QosIds.u4ClassId
            = gaQoSAcLInfoL3Protocols[u4L3Protocols].QosIds.u4ClassId;
        AclQosL3MapInfo.QosIds.u4MeterId
            = gaQoSAcLInfoL3Protocols[u4L3Protocols].QosIds.u4MeterId;
        AclQosL3MapInfo.QosIds.u4PolicyMapId
            = gaQoSAcLInfoL3Protocols[u4L3Protocols].QosIds.u4PolicyMapId;
        AclQosL3MapInfo.u4FilterId
            =
            gaQoSAcLInfoL3Protocols[u4L3Protocols].L3FilterInfo.i4IssL3FilterNo;
        AclQosL3MapInfo.u4ProtocolRate =
            gaQoSAcLInfoL3Protocols[u4L3Protocols].u4ProtocolRate;
        AclQosL3MapInfo.u1CpuQueueNo =
            gaQoSAcLInfoL3Protocols[u4L3Protocols].u1CpuQueueNo;

        /* QoS parameter configurations */
        ISS_UNLOCK ();
        if (QosApiAddMfcRateLimiter (u4FilterType, &AclQosL3MapInfo) ==
            QOS_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "ACL:Failure of QosAddMfcFilter in %s, %d\n",
                          __FUNCTION__, __LINE__);
            ISS_LOCK ();
            return (ISS_FAILURE);
        }
        ISS_LOCK ();
    }

    ISS_TRC (INIT_SHUT_TRC, "\nACL:Exit IssAclQosProcessL3Proto function \n");
    return (ISS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      :  IssAclQosProcessL2L3Proto                           */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                      to install L2, L3 and DoS attack control ACL rules   */
/*                      and QoS configurations.                              */
/*                                                                           */
/* Input(s)           :                                                      */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclQosProcessL2L3Proto (VOID)
{
    INT4                i4RetVal = ISS_SUCCESS;

    ISS_TRC (INIT_SHUT_TRC, "\nACL:Entry IssAclQosProcessL2L3Proto function\n");

    if (gu4TrafficSeprtnControl == ACL_TRAFFICSEPRTN_CTRL_USER_DEFINED)
    {
        return (ISS_FAILURE);
    }

    if (IssAclQosProcessL2Proto () == ISS_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "ACL:Failure of IssAclQosProcessL2Proto in %s, %d\n",
                      __FUNCTION__, __LINE__);
        i4RetVal = ISS_FAILURE;
    }

    if (IssAclQosProcessL3Proto () == ISS_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "ACL:Failure of IssAclQosProcessL3Proto in %s, %d\n",
                      __FUNCTION__, __LINE__);
        i4RetVal = ISS_FAILURE;
    }

    /* TO be called after implementation of protection against DoS attack to CP 
     *  if (QoSProcessDoSAttackControl () == ISS_FAILURE) 
     */
    ISS_TRC (INIT_SHUT_TRC, "\nACL:Exit IssAclQosProcessL2L3Proto function\n");

    return (i4RetVal);
}

/*****************************************************************************/
/* Function Name      :  IssAclUninstallFilter                               */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                            to uninstall L2, L3  ACL rules.                */
/*                                                                           */
/* Input(s)           :                                                      */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclUninstallFilter (UINT4 u4FilterType, tIssL2FilterEntry * pIssL2Filter,
                       tIssL3FilterEntry * pIssL3Filter)
{
    ISS_TRC (INIT_SHUT_TRC, "\nACL:Entry IssAclUninstallFilter function \n");
    /* Set RowStatus destroy for issAclL2FilterTable or 
     * issAclL3FilterTable based on the filter type.
     */

    if ((u4FilterType == QOS_ACL_L2_CONFIG) && (pIssL2Filter != NULL))
    {
        if (IssAclUnInstallL2Filter (pIssL2Filter) == ISS_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "ACL:Failure of IssAclUnInstallL2Filter in %s, %d\n",
                          __FUNCTION__, __LINE__);
            return (ISS_FAILURE);
        }
        IssAclUnInstallL2Filter (pIssL2Filter);
        ISS_TRC (INIT_SHUT_TRC, "\nACL:Exit IssAclUninstallFilter function \n");
        return (ISS_SUCCESS);
    }
    else if ((u4FilterType == QOS_ACL_L3_CONFIG) && (pIssL3Filter != NULL))
    {
        if (IssAclUnInstallL3Filter (pIssL3Filter) == ISS_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "ACL:Failure of IssAclUnInstallL3Filter in %s, %d\n",
                          __FUNCTION__, __LINE__);
            return (ISS_FAILURE);
        }
        IssAclUnInstallL3Filter (pIssL3Filter);
        ISS_TRC (INIT_SHUT_TRC, "\nACL:Exit IssAclUninstallFilter function \n");
        return (ISS_SUCCESS);
    }

    /* No filters types to configure */
    ISS_TRC (INIT_SHUT_TRC, "\nACL:Fail IssAclUninstallFilter function \n");
    return (ISS_FAILURE);
}

/*****************************************************************************/
/* Function Name      :  IssAclQosDeleteL2L3Filter                           */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                      to uninstall L2, L3  ACL rules and                   */
/*                      QoS configuartions.                                  */
/*                                                                           */
/* Input(s)           :                                                      */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclQosDeleteL2L3Filter (VOID)
{
    UINT4               u4FilterType = ISS_ZERO_ENTRY;
    UINT4               u4L2Protocols = ISS_ZERO_ENTRY;
    INT4                i4IssAclFilterCreationMode = ISS_ZERO_ENTRY;
    tAclQosMapInfo      AclQosL2MapInfo;
    tAclQosMapInfo      AclQosL3MapInfo;

    MEMSET (&AclQosL2MapInfo, 0x0, sizeof (tAclQosMapInfo));
    MEMSET (&AclQosL3MapInfo, 0x0, sizeof (tAclQosMapInfo));

    ISS_TRC (INIT_SHUT_TRC,
             "\nACL:Entry IssAclQosDeleteL2L3Filter function \n");

    for (u4L2Protocols = ACL_MIN_L2_SYSTEM_PROTOCOLS,
         u4FilterType = QOS_ACL_L2_CONFIG;
         u4L2Protocols <= ACL_MAX_L2_SYSTEM_PROTOCOLS; u4L2Protocols++)
    {
        AclQosL2MapInfo.QosIds.u4ClassMapId
            = gaQoSAcLInfoL2Protocols[u4L2Protocols].QosIds.u4ClassMapId;
        AclQosL2MapInfo.QosIds.u4ClassId
            = gaQoSAcLInfoL2Protocols[u4L2Protocols].QosIds.u4ClassId;
        AclQosL2MapInfo.QosIds.u4MeterId
            = gaQoSAcLInfoL2Protocols[u4L2Protocols].QosIds.u4MeterId;
        AclQosL2MapInfo.QosIds.u4PolicyMapId
            = gaQoSAcLInfoL2Protocols[u4L2Protocols].QosIds.u4PolicyMapId;
        AclQosL2MapInfo.u4FilterId
            =
            gaQoSAcLInfoL2Protocols[u4L2Protocols].L2FilterInfo.i4IssL2FilterNo;
        AclQosL2MapInfo.u4ProtocolRate =
            gaQoSAcLInfoL2Protocols[u4L2Protocols].u4ProtocolRate;
        AclQosL2MapInfo.u1CpuQueueNo =
            gaQoSAcLInfoL2Protocols[u4L2Protocols].u1CpuQueueNo;

        nmhGetIssAclL2FilterCreationMode (AclQosL2MapInfo.u4FilterId,
                                          &i4IssAclFilterCreationMode);

        if (i4IssAclFilterCreationMode == ISS_ACL_CREATION_INTERNAL)
        {
            /* QoS parameter configurations */
            ISS_UNLOCK ();
            if (QosApiDeleteMfcRateLimiter (u4FilterType, &AclQosL2MapInfo)
                == QOS_FAILURE)
            {
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "ACL:Failure of QosDeleteMfcFilter in %s, %d\n",
                              __FUNCTION__, __LINE__);
                ISS_LOCK ();
                return (ISS_FAILURE);
            }
            ISS_LOCK ();

            /* L2 Filter deletion Configuration */
            if (IssAclUninstallFilter
                (u4FilterType,
                 &(gaQoSAcLInfoL2Protocols[u4L2Protocols].L2FilterInfo),
                 NULL) == ISS_FAILURE)
            {
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "ACL:Failure of IssAclUninstallFilter in %s, %d\n",
                              __FUNCTION__, __LINE__);
                return (ISS_FAILURE);
            }
        }
    }

    for (u4L2Protocols = ACL_MIN_L3_SYSTEM_PROTOCOLS,
         u4FilterType = QOS_ACL_L3_CONFIG;
         u4L2Protocols <= ACL_MAX_L3_SYSTEM_PROTOCOLS; u4L2Protocols++)
    {

        AclQosL3MapInfo.QosIds.u4ClassMapId
            = gaQoSAcLInfoL3Protocols[u4L2Protocols].QosIds.u4ClassMapId;
        AclQosL3MapInfo.QosIds.u4ClassId
            = gaQoSAcLInfoL3Protocols[u4L2Protocols].QosIds.u4ClassId;
        AclQosL3MapInfo.QosIds.u4MeterId
            = gaQoSAcLInfoL3Protocols[u4L2Protocols].QosIds.u4MeterId;
        AclQosL3MapInfo.QosIds.u4PolicyMapId
            = gaQoSAcLInfoL3Protocols[u4L2Protocols].QosIds.u4PolicyMapId;
        AclQosL3MapInfo.u4FilterId
            =
            gaQoSAcLInfoL3Protocols[u4L2Protocols].L3FilterInfo.i4IssL3FilterNo;
        AclQosL3MapInfo.u4ProtocolRate =
            gaQoSAcLInfoL3Protocols[u4L2Protocols].u4ProtocolRate;
        AclQosL3MapInfo.u1CpuQueueNo =
            gaQoSAcLInfoL3Protocols[u4L2Protocols].u1CpuQueueNo;

        nmhGetIssAclL3FilterCreationMode (AclQosL3MapInfo.u4FilterId,
                                          &i4IssAclFilterCreationMode);

        if (i4IssAclFilterCreationMode == ISS_ACL_CREATION_INTERNAL)
        {
            /* QoS parameter configurations */
            ISS_UNLOCK ();
            if (QosApiDeleteMfcRateLimiter (u4FilterType, &AclQosL3MapInfo)
                == QOS_FAILURE)
            {
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "ACL:Failure of QosDeleteMfcFilter in %s, %d\n",
                              __FUNCTION__, __LINE__);
                ISS_LOCK ();
                return (ISS_FAILURE);
            }
            ISS_LOCK ();

            /* L3 Filter Configuration */
            if (IssAclUninstallFilter
                (u4FilterType, NULL,
                 &(gaQoSAcLInfoL3Protocols[u4L2Protocols].L3FilterInfo))
                == ISS_FAILURE)
            {
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "ACL:Failure of IssAclUninstallFilter in %s, %d\n",
                              __FUNCTION__, __LINE__);
                return (ISS_FAILURE);
            }
        }
    }

    ISS_TRC (INIT_SHUT_TRC, "\nACL:Exit IssAclQosDeleteL2L3Filter function \n");

    return (ISS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : IssAclInstallUserDefFilter                           */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                      to install new user defined ACL rule.                */
/*                                                                           */
/* Input(s)           : pIssUserDefFilter - Pointer to user defined          */
/*                                          ACL entry.                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclInstallUserDefFilter (tIssUserDefinedFilterTable * pIssUserDefFilter)
{
    tIssUDBFilterEntry *pAccessFilterEntry = NULL;
    INT4                i4Status = ISS_ZERO_ENTRY;
    UINT4               u4ErrCode = ISS_ZERO_ENTRY;
    UINT4               u4UserDefFilterId = ISS_ZERO_ENTRY;
    tSNMP_OCTET_STRING_TYPE OffsetValue;
    tSNMP_OCTET_STRING_TYPE OffsetMask;
    tSNMP_OCTET_STRING_TYPE InPortList;
    UINT1               au1OffsetValue[ISS_UDB_MAX_OFFSET] = { 0 };
    UINT1               au1OffsetMask[ISS_UDB_MAX_OFFSET] = { 0 };
    UINT1               au1InPortList[ISS_PORT_LIST_SIZE] = { 0 };

    ISS_TRC (INIT_SHUT_TRC, "\nACL-UDB:Entry IssAclInstallUserDefFilter "
             "function \n");

    if (pIssUserDefFilter == NULL)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACL-UDB:pIssUserDefFilter is NULL in "
                      "%s, %d\n", __FUNCTION__, __LINE__);
        return (ISS_FAILURE);
    }

    pAccessFilterEntry = pIssUserDefFilter->pAccessFilterEntry;

    u4UserDefFilterId = pAccessFilterEntry->u4UDBFilterId;

    /* if filter already exists, do nothing and enter the configuration mode */
    if (nmhGetIssAclUserDefinedFilterStatus (u4UserDefFilterId, &i4Status)
        == SNMP_FAILURE)
    {
        /* Create a filter */
        if (nmhTestv2IssAclUserDefinedFilterStatus
            (&u4ErrCode, u4UserDefFilterId, ISS_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACL-UDB:Failure of "
                          "nmhTestv2IssAclUserDefinedFilterStatus in %s, %d\n",
                          __FUNCTION__, __LINE__);
            ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACL-UDB:ErrCode[%d]\tFilterNo[%d]"
                          "\n", u4ErrCode, u4UserDefFilterId);
            return (ISS_FAILURE);
        }

        if (nmhSetIssAclUserDefinedFilterStatus (u4UserDefFilterId,
                                                 ISS_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACL-UDB:Failure of "
                          "nmhSetIssAclUserDefinedFilterStatus in %s, %d "
                          "FilterNo[%d]\n", __FUNCTION__, __LINE__,
                          u4UserDefFilterId);
            return (ISS_FAILURE);
        }
    }
    else if (i4Status != ISS_NOT_READY)
    {
        /* Filter has previously been configured, it must be overwriten */
        if (nmhSetIssAclUserDefinedFilterStatus (u4UserDefFilterId,
                                                 ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACL-UDB:Failure of "
                          "nmhSetIssAclUserDefinedFilterStatus in %s, %d "
                          "FilterNo[%d]\n", __FUNCTION__, __LINE__,
                          u4UserDefFilterId);
            return (ISS_FAILURE);
        }
        if (nmhSetIssAclUserDefinedFilterStatus (u4UserDefFilterId,
                                                 ISS_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACL-UDB:Failure of "
                          "nmhSetIssAclUserDefinedFilterStatus in %s, %d"
                          "FilterNo[%d]\n", __FUNCTION__, __LINE__,
                          u4UserDefFilterId);
            return (ISS_FAILURE);
        }
    }

    /* OID: issAclUserDefinedFilterPktType */
    if (nmhTestv2IssAclUserDefinedFilterPktType
        (&u4ErrCode, u4UserDefFilterId,
         pAccessFilterEntry->u1AccessFilterPktType) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACL-UDB:Failure of "
                      "nmhTestv2IssAclUserDefinedFilterPktType in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACL-UDB: ErrCode[%d] FilterNo[%d]"
                      "Packet-Type[%d]\n", u4ErrCode, u4UserDefFilterId,
                      pAccessFilterEntry->u1AccessFilterPktType);
        return (ISS_FAILURE);
    }

    nmhSetIssAclUserDefinedFilterPktType
        (u4UserDefFilterId, pAccessFilterEntry->u1AccessFilterPktType);

    /* OID: issAclUserDefinedFilterOffSetBase */
    if (nmhTestv2IssAclUserDefinedFilterOffSetBase
        (&u4ErrCode, u4UserDefFilterId,
         pAccessFilterEntry->u2AccessFilterOffset) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACL-UDB:Failure of "
                      "nmhTestv2IssAclUserDefinedFilterOffSetBase in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACL-UDB: ErrCode[%d] FilterNo[%d]"
                      "Offset-Base[%d]\n", u4ErrCode, u4UserDefFilterId,
                      pAccessFilterEntry->u2AccessFilterOffset);
        return (ISS_FAILURE);
    }

    nmhSetIssAclUserDefinedFilterOffSetBase
        (u4UserDefFilterId, pAccessFilterEntry->u2AccessFilterOffset);

    /* OID : issAclUserDefinedFilterOffSetValue */
    OffsetValue.pu1_OctetList = &au1OffsetValue[0];

    MEMSET (OffsetValue.pu1_OctetList, 0x0, ISS_UDB_MAX_OFFSET);

    MEMCPY (OffsetValue.pu1_OctetList,
            pAccessFilterEntry->au1AccessFilterOffsetValue, ISS_UDB_MAX_OFFSET);
    OffsetValue.i4_Length = ISS_UDB_MAX_OFFSET;

    if (nmhTestv2IssAclUserDefinedFilterOffSetValue
        (&u4ErrCode, u4UserDefFilterId, &OffsetValue) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACL-UDB:Failure of "
                      "nmhTestv2IssAclUserDefinedFilterOffSetValue in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACL-UDB: ErrCode[%d] FilterNo[%d]\n",
                      u4ErrCode, u4UserDefFilterId);
        return (ISS_FAILURE);
    }

    nmhSetIssAclUserDefinedFilterOffSetValue (u4UserDefFilterId, &OffsetValue);

    /* OID : issAclUserDefinedFilterOffSetMask */
    OffsetMask.pu1_OctetList = &au1OffsetMask[0];

    MEMSET (OffsetValue.pu1_OctetList, 0x0, ISS_UDB_MAX_OFFSET);

    MEMCPY (OffsetMask.pu1_OctetList,
            pAccessFilterEntry->au1AccessFilterOffsetMask, ISS_UDB_MAX_OFFSET);

    OffsetMask.i4_Length = ISS_UDB_MAX_OFFSET;

    if (nmhTestv2IssAclUserDefinedFilterOffSetMask
        (&u4ErrCode, u4UserDefFilterId, &OffsetMask) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACL-UDB:Failure of "
                      "nmhTestv2IssAclUserDefinedFilterOffSetMask in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACL-UDB: ErrCode[%d] FilterNo[%d]\n",
                      u4ErrCode, u4UserDefFilterId);
        return (ISS_FAILURE);
    }

    nmhSetIssAclUserDefinedFilterOffSetMask (u4UserDefFilterId, &OffsetMask);

    /* OID : issAclUserDefinedFilterPriority   */

    if (nmhTestv2IssAclUserDefinedFilterPriority
        (&u4ErrCode, u4UserDefFilterId,
         pAccessFilterEntry->u1AccessFilterPriority) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACL-UDB:Failure of "
                      "nmhTestv2IssAclUserDefinedFilterPriority in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACL-UDB: ErrCode[%d] FilterNo[%d]"
                      "Priority[%d]\n", u4ErrCode, u4UserDefFilterId,
                      pAccessFilterEntry->u1AccessFilterPriority);
        return (ISS_FAILURE);
    }
    nmhSetIssAclUserDefinedFilterPriority
        (u4UserDefFilterId, pAccessFilterEntry->u1AccessFilterPriority);

    /* OID : issAclUserDefinedFilterAction */
    if (nmhTestv2IssAclUserDefinedFilterAction
        (&u4ErrCode, u4UserDefFilterId,
         pAccessFilterEntry->IssAccessFilterAction) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACL-UDB:Failure of "
                      "nmhTestv2IssAclUserDefinedFilterAction in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACL-UDB: ErrCode[%d] FilterNo[%d]"
                      "Action[%d]\n", u4ErrCode, u4UserDefFilterId,
                      pAccessFilterEntry->IssAccessFilterAction);
        return (ISS_FAILURE);
    }

    nmhSetIssAclUserDefinedFilterAction
        (u4UserDefFilterId, pAccessFilterEntry->IssAccessFilterAction);

    /* OID :  issAclUserDefinedFilterInPortList */
    InPortList.pu1_OctetList = &au1InPortList[0];
    MEMSET (InPortList.pu1_OctetList, 0x0, sizeof (tIssPortList));
    MEMCPY (InPortList.pu1_OctetList,
            pAccessFilterEntry->IssUdbFilterInPortList, ISS_PORT_LIST_SIZE);
    InPortList.i4_Length = ISS_PORT_LIST_SIZE;

    if (nmhTestv2IssAclUserDefinedFilterInPortList
        (&u4ErrCode, u4UserDefFilterId, &InPortList) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACL-UDB:Failure of "
                      "nmhSetIssAclUserDefinedFilterInPortList in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACL-UDB: ErrCode[%d] FilterNo[%d]\n",
                      u4ErrCode, u4UserDefFilterId);
        return (ISS_FAILURE);
    }

    nmhSetIssAclUserDefinedFilterInPortList (u4UserDefFilterId, &InPortList);

    /* OID :  issAclUserDefinedFilterIdOneType  */
    if (nmhTestv2IssAclUserDefinedFilterIdOneType
        (&u4ErrCode, u4UserDefFilterId,
         pIssUserDefFilter->u1AclOneType) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACL-UDB:Failure of "
                      "nmhTestv2IssAclUserDefinedFilterIdOneType in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACL-UDB: ErrCode[%d] FilterNo[%d]"
                      "Filter1-Type[%d]\n", u4ErrCode, u4UserDefFilterId,
                      pIssUserDefFilter->u1AclOneType);
        return (ISS_FAILURE);
    }

    nmhSetIssAclUserDefinedFilterIdOneType
        (u4UserDefFilterId, pIssUserDefFilter->u1AclOneType);

    /* OID :  issAclUserDefinedFilterIdOne      */
    if (nmhTestv2IssAclUserDefinedFilterIdOne
        (&u4ErrCode, u4UserDefFilterId,
         pIssUserDefFilter->u1AclOneType) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACL-UDB:Failure of "
                      "nmhTestv2IssAclUserDefinedFilterIdOne in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACL-UDB: ErrCode[%d] FilterNo[%d]"
                      "Filter1-Type[%d]\n", u4ErrCode, u4UserDefFilterId,
                      pIssUserDefFilter->u4BaseAclOne);
        return (ISS_FAILURE);
    }

    nmhSetIssAclUserDefinedFilterIdOne
        (u4UserDefFilterId, pIssUserDefFilter->u4BaseAclOne);

    /* OID :  issAclUserDefinedFilterIdTwoType  */
    if (nmhTestv2IssAclUserDefinedFilterIdTwoType
        (&u4ErrCode, u4UserDefFilterId,
         pIssUserDefFilter->u1AclTwoType) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACL-UDB:Failure of "
                      "nmhTestv2IssAclUserDefinedFilterIdTwoType in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACL-UDB: ErrCode[%d] FilterNo[%d]"
                      "Filter2-Type[%d]\n", u4ErrCode, u4UserDefFilterId,
                      pIssUserDefFilter->u1AclTwoType);
        return (ISS_FAILURE);
    }

    nmhSetIssAclUserDefinedFilterIdTwoType
        (u4UserDefFilterId, pIssUserDefFilter->u1AclTwoType);

    /* OID :  issAclUserDefinedFilterIdTwo      */
    if (nmhTestv2IssAclUserDefinedFilterIdTwo
        (&u4ErrCode, u4UserDefFilterId,
         pIssUserDefFilter->u1AclTwoType) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACL-UDB:Failure of "
                      "nmhTestv2IssAclUserDefinedFilterIdTwo in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACL-UDB: ErrCode[%d] FilterNo[%d]"
                      "Filter2[%d]\n", u4ErrCode, u4UserDefFilterId,
                      pIssUserDefFilter->u4BaseAclTwo);
        return (ISS_FAILURE);
    }

    nmhSetIssAclUserDefinedFilterIdTwo
        (u4UserDefFilterId, pIssUserDefFilter->u4BaseAclTwo);

    /* OID :  issAclUserDefinedFilterSubAction  */
    if (nmhTestv2IssAclUserDefinedFilterSubAction
        (&u4ErrCode, u4UserDefFilterId,
         pAccessFilterEntry->u1IssUdbSubAction) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACL-UDB:Failure of "
                      "nmhTestv2IssAclUserDefinedFilterSubAction in %s, %d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACL-UDB: ErrCode[%d] FilterNo[%d]"
                      "SubAction[%d]\n", u4ErrCode, u4UserDefFilterId,
                      pAccessFilterEntry->u1IssUdbSubAction);
        return (ISS_FAILURE);
    }

    nmhSetIssAclUserDefinedFilterSubAction
        (u4UserDefFilterId, pAccessFilterEntry->u1IssUdbSubAction);

    /* OID :  issAclUserDefinedFilterSubActionId */
    if (nmhTestv2IssAclUserDefinedFilterSubActionId
        (&u4ErrCode, u4UserDefFilterId,
         pAccessFilterEntry->u2IssUdbSubActionId) == SNMP_FAILURE)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACL-UDB:Failure of "
                      "nmhTestv2IssAclUserDefinedFilterSubActionId in %s,%d\n",
                      __FUNCTION__, __LINE__);
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACL-UDB: ErrCode[%d] FilterNo[%d]"
                      "SubActionId[%d]\n", u4ErrCode, u4UserDefFilterId,
                      pAccessFilterEntry->u2IssUdbSubActionId);
        return (ISS_FAILURE);
    }

    nmhSetIssAclUserDefinedFilterSubActionId
        (u4UserDefFilterId, pAccessFilterEntry->u2IssUdbSubActionId);

    /* OID :  issAclUserDefinedFilterStatus     */
    nmhSetIssAclUserDefinedFilterStatus (u4UserDefFilterId, ACTIVE);

    ISS_TRC (INIT_SHUT_TRC, "\nACL-UDB:Exit IssAclInstallUserDefFilter "
             "function \n");

    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssAclUnInstallUserDefFilter                         */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                      to delete the added user defined ACL rule.           */
/*                                                                           */
/* Input(s)           : pIssUserDefFilter - Pointer to user defined          */
/*                                          filter entry.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclUnInstallUserDefFilter (tIssUserDefinedFilterTable * pIssUserDefFilter)
{
    INT4                i4Status = ISS_ZERO_ENTRY;
    UINT4               u4ErrCode = ISS_ZERO_ENTRY;
    UINT4               u4UserDefFilterId = ISS_ZERO_ENTRY;

    ISS_TRC (INIT_SHUT_TRC, "\nACL-UDB:Entry IssAclUnInstallUserDefFilter "
             "function\n");

    if (pIssUserDefFilter == NULL)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC, "ACL-UDB:pIssUserDefFilter is NULL,"
                      "in  %s and line %d\n", __FUNCTION__, __LINE__);
        return (ISS_FAILURE);
    }

    u4UserDefFilterId = pIssUserDefFilter->pAccessFilterEntry->u4UDBFilterId;

    if (nmhGetIssAclUserDefinedFilterStatus (u4UserDefFilterId, &i4Status)
        == SNMP_FAILURE)
    {
        /* Filter does not exist */
        ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACL-UDB:Invalid Filter number [%d]"
                      "and status[%d] in %s\n",
                      u4UserDefFilterId, i4Status, __FUNCTION__);
        return (ISS_SUCCESS);
    }
    else
    {
        if (nmhTestv2IssAclUserDefinedFilterStatus (&u4ErrCode,
                                                    u4UserDefFilterId,
                                                    ISS_DESTROY)
            == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC, "ACL-UDB:nmhTestv2IssAclUserDefined"
                          "FilterStatus fails in %s and Line %d FiltNo %d\n",
                          __FUNCTION__, __LINE__, u4UserDefFilterId);
            return (ISS_FAILURE);
        }

        if (nmhSetIssAclUserDefinedFilterStatus (u4UserDefFilterId,
                                                 ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_TRC_ARG3 (ALL_FAILURE_TRC,
                          "ACL-UDB: nmhSetIssAclUserDefinedFilterStatus"
                          " fails in %s and line %d and FilterNo [%d]\n",
                          __FUNCTION__, __LINE__, u4UserDefFilterId);
            return (ISS_FAILURE);
        }
    }

    ISS_TRC (INIT_SHUT_TRC, "\nACL-UDB:Exiting IssAclUnInstallUserDefFilter"
             "function \n");
    return (ISS_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : IssAclUpdateL2FilterEntry                            */
/*                                                                           */
/* Description        : This function is called to update the pL2FilterEntry */
/*                      and invoke the installation of L2 filter entry.      */
/*                                                                           */
/* Input(s)           : pAclFilterInfo - Pointer to ACL filter information   */
/*                      u4L2FilterId - L2 Filter ID                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclUpdateL2FilterEntry (tAclFilterInfo * pAclFilterInfo, UINT4 u4L2FilterId)
{
    tIssL2FilterEntry   IssL2FilterEntry;

    MEMSET (&IssL2FilterEntry, 0x0, sizeof (tIssL2FilterEntry));

    IssL2FilterEntry.i4IssL2FilterNo = u4L2FilterId;
    IssL2FilterEntry.i4IssL2FilterPriority = pAclFilterInfo->u1Priority;
    IssL2FilterEntry.i4IssL2FilterEncapType = ISS_ZERO_ENTRY;
    IssL2FilterEntry.u4IssL2FilterProtocolType = ISS_ZERO_ENTRY;
#ifdef ISS_METRO_WANTED
    IssL2FilterEntry.u4IssL2FilterCustomerVlanId = ISS_ZERO_ENTRY;
    IssL2FilterEntry.u4IssL2FilterServiceVlanId = pAclFilterInfo->u2VlanId;
#else
    IssL2FilterEntry.u4IssL2FilterCustomerVlanId = pAclFilterInfo->u2VlanId;
    IssL2FilterEntry.u4IssL2FilterServiceVlanId = ISS_ZERO_ENTRY;
#endif
    IssL2FilterEntry.u4IssL2FilterMatchCount = ISS_ZERO_ENTRY;
    IssL2FilterEntry.u4RefCount = ISS_ZERO_ENTRY;
    IssL2FilterEntry.IssL2FilterAction = pAclFilterInfo->u1Action;
    MEMCPY (IssL2FilterEntry.IssL2FilterSrcMacAddr,
            pAclFilterInfo->HostMac, MAC_ADDR_LEN);
    MEMCPY (IssL2FilterEntry.IssL2FilterInPortList,
            pAclFilterInfo->PortList, ISS_PORT_LIST_SIZE);
    IssL2FilterEntry.u2InnerEtherType = ISS_ZERO_ENTRY;
    IssL2FilterEntry.u2OuterEtherType = ISS_ZERO_ENTRY;
    IssL2FilterEntry.i1IssL2FilterCVlanPriority = ISS_DEFAULT_VLAN_PRIORITY;
    IssL2FilterEntry.i1IssL2FilterSVlanPriority = ISS_DEFAULT_VLAN_PRIORITY;
    IssL2FilterEntry.u1IssL2FilterTagType = ISS_FILTER_SINGLE_TAG;
    IssL2FilterEntry.u1FilterDirection = ISS_DIRECTION_IN;
    IssL2FilterEntry.u1IssL2FilterStatus = ISS_NOT_READY;
    IssL2FilterEntry.u1IssL2FilterCreationMode = ISS_ACL_CREATION_INTERNAL;

    if (IssAclInstallL2Filter (&IssL2FilterEntry) == ISS_FAILURE)
    {
        return ISS_FAILURE;
    }
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssAclUpdateL3FilterEntry                            */
/*                                                                           */
/* Description        : This function is called to update the pL3FilterEntry */
/*                      and invoke the installation of L3 filter entry.      */
/*                                                                           */
/* Input(s)           : pAclFilterInfo - Pointer to ACL filter information   */
/*                      u4L3FilterId - L3 Filter ID                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclUpdateL3FilterEntry (tAclFilterInfo * pAclFilterInfo, UINT4 u4L3FilterId)
{
    tIssL3FilterEntry   IssL3FilterEntry;

    MEMSET (&IssL3FilterEntry, 0x0, sizeof (tIssL3FilterEntry));

    IssL3FilterEntry.i4IssL3FilterNo = u4L3FilterId;
    IssL3FilterEntry.i4IssL3FilterPriority = pAclFilterInfo->u1Priority;
    IssL3FilterEntry.IssL3FilterProtocol = pAclFilterInfo->u1ProtocolId;
    IssL3FilterEntry.i4IssL3FilterMessageType = ISS_ANY;
    IssL3FilterEntry.i4IssL3FilterMessageCode = ISS_ANY;
#ifdef ISS_METRO_WANTED
    IssL3FilterEntry.u4CVlanId = ISS_ZERO_ENTRY;
    IssL3FilterEntry.u4SVlanId = pAclFilterInfo->u2VlanId;
#else
    IssL3FilterEntry.u4CVlanId = pAclFilterInfo->u2VlanId;
    IssL3FilterEntry.u4SVlanId = ISS_ZERO_ENTRY;
#endif
    IssL3FilterEntry.u4IssL3FilterDstIpAddr = ISS_ZERO_ENTRY;
    IssL3FilterEntry.u4IssL3FilterSrcIpAddr =
        OSIX_HTONL (pAclFilterInfo->u4HostIp);
    IssL3FilterEntry.u4IssL3FilterDstIpAddrMask = ISS_DEF_FILTER_MASK;
    IssL3FilterEntry.u4IssL3FilterSrcIpAddrMask = ISS_DEF_FILTER_MASK;
    IssL3FilterEntry.u4IssL3FilterMinDstProtPort = ISS_ZERO_ENTRY;
    IssL3FilterEntry.u4IssL3FilterMaxDstProtPort = ISS_MAX_PORT_VALUE;
    IssL3FilterEntry.u4IssL3FilterMinSrcProtPort = ISS_ZERO_ENTRY;
    IssL3FilterEntry.u4IssL3FilterMaxSrcProtPort = ISS_MAX_PORT_VALUE;
    IssL3FilterEntry.IssL3FilterAckBit = ISS_ACK_ANY;
    IssL3FilterEntry.IssL3FilterRstBit = ISS_ACK_ANY;
    IssL3FilterEntry.IssL3FilterTos = ISS_TOS_INVALID;
    IssL3FilterEntry.i4IssL3FilterDscp = ISS_DSCP_INVALID;
    IssL3FilterEntry.IssL3FilterDirection = ISS_DIRECTION_IN;
    IssL3FilterEntry.IssL3FilterAction = pAclFilterInfo->u1Action;
    IssL3FilterEntry.u4IssL3FilterMatchCount = ISS_ZERO_ENTRY;
    IssL3FilterEntry.u4RefCount = ISS_ZERO_ENTRY;
    MEMCPY (IssL3FilterEntry.IssL3FilterInPortList,
            pAclFilterInfo->PortList, ISS_PORT_LIST_SIZE);
    IssL3FilterEntry.u4IssL3FilterMultiFieldClfrDstPrefixLength =
        ISS_ZERO_ENTRY;
    IssL3FilterEntry.u4IssL3FilterMultiFieldClfrSrcPrefixLength =
        ISS_ZERO_ENTRY;
    IssL3FilterEntry.u4IssL3MultiFieldClfrFlowId = ISS_ZERO_ENTRY;
    IssL3FilterEntry.i4IssL3MultiFieldClfrAddrType = QOS_IPV4;
    IssL3FilterEntry.i4StorageType = ISS_ZERO_ENTRY;
    IssL3FilterEntry.i1CVlanPriority = ISS_DEFAULT_VLAN_PRIORITY;
    IssL3FilterEntry.i1SVlanPriority = ISS_DEFAULT_VLAN_PRIORITY;
    IssL3FilterEntry.u1IssL3FilterTagType = ISS_FILTER_SINGLE_TAG;
    IssL3FilterEntry.u1IssL3FilterStatus = NOT_READY;
    IssL3FilterEntry.u1IssL3FilterCreationMode = ISS_ACL_CREATION_INTERNAL;

    if (IssAclInstallL3Filter (&IssL3FilterEntry) == ISS_FAILURE)
    {
        return ISS_FAILURE;
    }

    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssAclUpdateUDBFilterEntry                           */
/*                                                                           */
/* Description        : This function is called to update the                */
/*                      pIssUserDefFilterEntry and invoke the installation   */
/*                      user defined filter entry.                           */
/*                                                                           */
/* Input(s)           : pAclFilterInfo - Pointer to ACL filter information   */
/*                      u4L2FilterId - L2 filter ID                          */
/*                      u4L3FilterId - L3 filter ID                          */
/*                      u4UDBFilterId - User defined filter ID               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclUpdateUDBFilterEntry (tAclFilterInfo * pAclFilterInfo,
                            UINT4 u4L2FilterId, UINT4 u4L3FilterId,
                            UINT4 u4UDBFilterId)
{
    tIssUserDefinedFilterTable IssUserDefFilterEntry;
    tIssUDBFilterEntry  AccessFilterEntry;

    MEMSET (&IssUserDefFilterEntry, 0, sizeof (tIssUserDefinedFilterTable));

    MEMSET (&AccessFilterEntry, 0, sizeof (tIssUDBFilterEntry));

    IssUserDefFilterEntry.pAccessFilterEntry = &AccessFilterEntry;

    IssUserDefFilterEntry.pAccessFilterEntry->IssAccessFilterAction = ISS_AND;
    MEMCPY (IssUserDefFilterEntry.pAccessFilterEntry->
            IssUdbFilterInPortList,
            pAclFilterInfo->PortList, ISS_PORT_LIST_SIZE);

    IssUserDefFilterEntry.pAccessFilterEntry->u4UDBFilterId = u4UDBFilterId;
    IssUserDefFilterEntry.pAccessFilterEntry->u2IssUdbSubActionId =
        ISS_ZERO_ENTRY;
    IssUserDefFilterEntry.pAccessFilterEntry->u2AccessFilterOffset =
        ISS_ZERO_ENTRY;
    IssUserDefFilterEntry.pAccessFilterEntry->
        u1AccessFilterPktType = ISS_UDB_PKT_TYPE_USER_DEF;
    IssUserDefFilterEntry.pAccessFilterEntry->
        u1AccessFilterPriority = pAclFilterInfo->u1Priority;
    IssUserDefFilterEntry.pAccessFilterEntry->u1IssUdbSubAction = ISS_NONE;
    IssUserDefFilterEntry.u4BaseAclOne = u4L2FilterId;
    IssUserDefFilterEntry.u4BaseAclTwo = u4L3FilterId;
    IssUserDefFilterEntry.u1Operation = ISS_AND;
    IssUserDefFilterEntry.u1AclOneType = ISS_L2_FILTER;
    IssUserDefFilterEntry.u1AclTwoType = ISS_L3_FILTER;
    IssUserDefFilterEntry.u1RowStatus = NOT_READY;
    IssUserDefFilterEntry.u1PriorityFlag = ISS_FALSE;

    if (IssAclInstallUserDefFilter (&IssUserDefFilterEntry) == ISS_FAILURE)
    {
        return ISS_FAILURE;
    }

    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssAclGetValidL2FilterId                             */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                      to get a valid L2 filter ID.                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu4L2FilterId - Pointer to L2 Filter ID.             */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
VOID
IssAclGetValidL2FilterId (UINT4 *pu4L2FilterId)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;
    UINT4               u4L2FilterId = ISS_ZERO_ENTRY;

    /* Scan through the filter ID's from 0 to MAX_FILTER's. */
    ISS_L2_FILTER_ID_SCAN (u4L2FilterId)
    {
        /* Get the L2 filter entry for 'u4L2FilterId' */
        pIssL2FilterEntry = IssExtGetL2FilterEntry ((INT4) u4L2FilterId);

        if (pIssL2FilterEntry == NULL)
        {
            /* If L2 filter entry is not present for 'u4L2FilterId'
             * fill this ID as the valid free ID */
            *pu4L2FilterId = u4L2FilterId;
            break;
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : IssAclGetValidL3FilterId                             */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                      to get a valid L3 filter ID.                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu4L3FilterId - Pointer to L3 Filter ID.             */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
VOID
IssAclGetValidL3FilterId (UINT4 *pu4L3FilterId)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    UINT4               u4L3FilterId = ISS_ZERO_ENTRY;

    /* Scan through the filter ID's from 0 to MAX_FILTER's. */
    ISS_L3_FILTER_ID_SCAN (u4L3FilterId)
    {
        /* Get the L3 filter entry for 'u4L3FilterId' */
        pIssL3FilterEntry = IssExtGetL3FilterEntry ((INT4) u4L3FilterId);

        if (pIssL3FilterEntry == NULL)
        {
            /* If L3 filter entry is not present for 'u4L3FilterId'
             * fill this ID as the valid free ID */
            *pu4L3FilterId = u4L3FilterId;
            break;
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : IssAclGetValidUDBFilterId                            */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                      to get a valid user defined filter ID.               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu4UDBFilterId - Pointer to user defined filter ID   */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
VOID
IssAclGetValidUDBFilterId (UINT4 *pu4UDBFilterId)
{
    tIssUserDefinedFilterTable *pIssUDBFilterEntry = NULL;
    UINT4               u4UserDefFilterId = ISS_ZERO_ENTRY;

    /* Scan through the filter ID's from 0 to MAX_FILTER's. */
    ISS_UDB_FILTER_ID_SCAN (u4UserDefFilterId)
    {
        /* Get the User defined filter entry for 'u4UserDefFilterId' */
        pIssUDBFilterEntry =
            IssExtGetUdbFilterTableEntry ((INT4) u4UserDefFilterId);

        if (pIssUDBFilterEntry == NULL)
        {
            /* If UDB filter entry is not present for 'u4UserDefFilterId'
             * fill this ID as the valid free ID */
            *pu4UDBFilterId = u4UserDefFilterId;
            break;
        }
    }
    return;
}
