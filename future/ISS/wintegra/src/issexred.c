/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: issexred.c,v 1.2 2013/05/24 13:33:35 siva Exp $
 *
 * Description: This file contains the ACL Redundancy support routines and
 * utility routines.
 *****************************************************************************/

#include "issexinc.h"
#include "fsisselw.h"

/*****************************************************************************/
/* Function Name      : AclRedInitGlobalInfo                     */
/*                                                                           */
/* Description        : This function initialise Redundancy global         */
/*             infromation and allocates memory blocks            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gIssExGlobalInfo                                     */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS - On success                            */
/*                      OSIX_FAILURE - On failure                            */
/*****************************************************************************/
PUBLIC INT4
AclRedInitGlobalInfo (VOID)
{
    tRmRegParams        RmRegParams;

    MEMSET (&RmRegParams, ISS_ZERO_ENTRY, sizeof (tRmRegParams));

    RmRegParams.u4EntId = RM_ACL_APP_ID;
    RmRegParams.pFnRcvPkt = AclRedRmCallBack;

    if (AclRmRegisterProtocols (&RmRegParams) == OSIX_FAILURE)
    {
        ISS_TRC (INIT_SHUT_TRC, "RM Registration fails\n");
        return OSIX_FAILURE;
    }

    MEMSET (&gIssExGlobalInfo.AclRedGlobalInfo, ISS_ZERO_ENTRY,
            sizeof (tAclRedGlobalInfo));

    /* Default value of Node Status is IDLE */
    gIssExGlobalInfo.AclRedGlobalInfo.u1NodeStatus = RM_INIT;
    gIssExGlobalInfo.AclRedGlobalInfo.bBulkReqRcvd = OSIX_FALSE;
    gIssExGlobalInfo.AclRedGlobalInfo.u1NumOfStandbyNodesUp =
        AclPortRmGetStandbyNodeCount ();

    gIssExGlobalInfo.AclRedGlobalInfo.u1NpSyncBlockCount = ISS_ZERO_ENTRY;

    /* NP-Syncups are not to be sent from Idle node. NPSync buffer Table
     * should not be accessed when the node is Idle. Set the Count value to
     * 1 so that the Np sync table is blocked.       */

    AclRedHwAuditIncBlkCounter ();

    /* Hardware Audit Np-Sync Buffer Mempool */

    if (MemCreateMemPool (sizeof (tAclRedNpSyncEntry), ISS_MAX_HW_NP_BUF_ENTRY,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(gIssExGlobalInfo.AclRedGlobalInfo.
                            HwAuditTablePoolId)) == MEM_FAILURE)
    {
        ISS_TRC (BUFFER_TRC, "AclRedInitGlobalInfo : Memory Pool Creation "
                 "for NP Sync Buffer Entry Failed\n");
        AclRmDeRegisterProtocols ();
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AclRedDeInitGlobalInfo                               */
/*                                                                           */
/* Description        : This function Deinitialise Redundancy global         */
/*                      information and Deallocates memory blocks            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gIssExGlobalInfo                                     */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS - On success                            */
/*                      OSIX_FAILURE - On failure                            */
/*****************************************************************************/
PUBLIC INT4
AclRedDeInitGlobalInfo ()
{
    if (AclRmDeRegisterProtocols () != OSIX_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "AclRedDeInitGlobalInfo "
                 " DeRegistration with RM failed.\r\n");
        return OSIX_FAILURE;
    }
    if (gIssExGlobalInfo.AclRedGlobalInfo.HwAuditTablePoolId != ISS_ZERO_ENTRY)
    {
        MemDeleteMemPool (gIssExGlobalInfo.AclRedGlobalInfo.HwAuditTablePoolId);
        gIssExGlobalInfo.AclRedGlobalInfo.HwAuditTablePoolId = ISS_ZERO_ENTRY;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AclRedRmCallBack                                     */
/*                                                                           */
/* Description        : This function is the registered callback function    */
/*            to handle the RM events posted to this module.       */
/*                                                                           */
/* Input(s)           : u1Event- RM Event name                               */
/*            pData - RM Messaage Data                 */
/*            u2DataLen - RM message Data Length             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS - On success                            */
/*                      OSIX_FAILURE - On failure                            */
/*****************************************************************************/
PUBLIC INT4
AclRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tAclQMsg           *pMsg = NULL;

    /* If the received event is not any of the following, then just return
       without processing the event */
    if ((u1Event != RM_MESSAGE) &&
        (u1Event != GO_ACTIVE) &&
        (u1Event != GO_STANDBY) &&
        (u1Event != RM_STANDBY_UP) &&
        (u1Event != RM_STANDBY_DOWN) &&
        (u1Event != L2_INITIATE_BULK_UPDATES) &&
        (u1Event != RM_CONFIG_RESTORE_COMPLETE) &&
        (u1Event != RM_INIT_HW_AUDIT))
    {
        ISS_TRC (CONTROL_PLANE_TRC, "AclRedRmCallBack : Event is "
                 "not associated with RM\n");
        return OSIX_FAILURE;
    }

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Message absent and hence no need to process */
        ISS_TRC (BUFFER_TRC, "AclRedRmCallBack : Queue Message associated"
                 "with the event is not sent by RM\n");
        return OSIX_FAILURE;
    }

    if (ISS_ACL_MSG_QUEUE_ALLOC_MEM_BLOCK (pMsg) == NULL)
    {
        ISS_TRC (BUFFER_TRC, "AclRedRmCallBack: Allocation of memory "
                 "for message queue buffer failed \n");
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, ISS_ZERO_ENTRY, sizeof (tAclQMsg));

    pMsg->u4MsgType = ISS_ACL_RM_MESSAGE;
    pMsg->RmMsg.pData = pData;
    pMsg->RmMsg.u1Event = u1Event;
    pMsg->RmMsg.u2DataLen = u2DataLen;

    if (AclQueEnqMsg (pMsg) == OSIX_FAILURE)
    {
        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            AclPortRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        ISS_TRC (CONTROL_PLANE_TRC, "AclRedRmCallBack: AclQueEnqMsg "
                 " failed \n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AclQueEnqMsg                                         */
/*                                                                           */
/* Description        : This function posts the messge to the ACL queue      */
/*            and send the event to ISS System Task.             */
/*                                                                           */
/* Input(s)           : pMsg- ACL Queue Message                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS - On success                            */
/*                      OSIX_FAILURE - On failure                            */
/*****************************************************************************/
PUBLIC INT4
AclQueEnqMsg (tAclQMsg * pMsg)
{

    if (OsixQueSend (ISS_ACL_MSG_QUE_ID, (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        ISS_TRC (CONTROL_PLANE_TRC, "AclQueEnqMsg: Sending to Message"
                 "Queue failed\n");
        ISS_ACL_MSG_QUEUE_FREE_MEM_BLOCK (pMsg);
        return OSIX_FAILURE;
    }

    if (OsixEvtSend (ISS_SYS_TASK_ID, ISS_ACL_MSG_EVENT) != OSIX_SUCCESS)
    {
        ISS_TRC (CONTROL_PLANE_TRC, "AclQueEnqMsg: Sending Event to ISS"
                 "System Task failed\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AclRedHandleRmEvents                                 */
/*                                                                           */
/* Description        : This function handles the RM Message posted in the   */
/*            ACL queue.                         */
/*                                                                           */
/* Input(s)           : pMsg- ACL Queue Message                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
PUBLIC VOID
AclRedHandleRmEvents (tAclQMsg * pMsg)
{

    tRmProtoAck         ProtoAck;
    tRmProtoEvt         ProtoEvt;
    tRmNodeInfo        *pData = NULL;
    UINT4               u4SeqNum = ISS_ZERO_ENTRY;

    ProtoEvt.u4AppId = RM_ACL_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    switch (pMsg->RmMsg.u1Event)
    {
        case GO_ACTIVE:
            AclRedHandleGoActive ();
            break;

        case GO_STANDBY:
            AclRedHandleGoStandby ();
            break;

        case RM_INIT_HW_AUDIT:
            /* Hardware Audit for ACL is performed here     */
            AclRedInitHardwareAudit ();
            break;

        case RM_STANDBY_UP:
            ISS_TRC (CONTROL_PLANE_TRC,
                     "AclRedHandleRmEvents: RM_STANDBY_UP event reached"
                     " Updating Standby Nodes Count.\r\n");

            pData = (tRmNodeInfo *) pMsg->RmMsg.pData;

            if (pMsg->RmMsg.u2DataLen == RM_NODE_COUNT_MSG_SIZE)
            {
                gIssExGlobalInfo.AclRedGlobalInfo.u1NumOfStandbyNodesUp =
                    pData->u1NumStandby;
                if (gIssExGlobalInfo.AclRedGlobalInfo.bBulkReqRcvd == OSIX_TRUE)
                {
                    gIssExGlobalInfo.AclRedGlobalInfo.bBulkReqRcvd = OSIX_FALSE;
                    AclRedHandleBulkRequest ();
                }
            }
            else
            {
                /* Data length is incorrect */
                ISS_TRC (ALL_FAILURE_TRC,
                         "AclRedHandleRmEvents: RM_STANDBY_UP event reached"
                         "Data Lenght Incorrect. Failed processing the event \r\n");

            }
            AclPortRmReleaseMemoryForMsg ((UINT1 *) pData);
            break;

        case RM_STANDBY_DOWN:
            ISS_TRC (CONTROL_PLANE_TRC,
                     "AclRedHandleRmEvents: RM_STANDBY_DOWN event reached"
                     " Updating Standby Nodes Count.\r\n");

            pData = (tRmNodeInfo *) pMsg->RmMsg.pData;

            if (pMsg->RmMsg.u2DataLen == RM_NODE_COUNT_MSG_SIZE)
            {
                gIssExGlobalInfo.AclRedGlobalInfo.u1NumOfStandbyNodesUp =
                    pData->u1NumStandby;
                break;
            }
            else
            {
                /* Data length is incorrect */
                ISS_TRC (ALL_FAILURE_TRC,
                         "AclRedHandleRmEvents: RM_STANDBY_DOWN event reached"
                         "Data Lenght Incorrect. Failed processing the event \r\n");
            }

            AclPortRmReleaseMemoryForMsg ((UINT1 *) pData);
            break;

        case RM_CONFIG_RESTORE_COMPLETE:

            if (gIssExGlobalInfo.AclRedGlobalInfo.u1NodeStatus == RM_INIT)
            {
                if (AclPortRmGetNodeState () == RM_STANDBY)
                {
                    AclRedHandleIdleToStandby ();

                    ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

                    if (AclPortRmApiHandleProtocolEvent (&ProtoEvt) ==
                        OSIX_FAILURE)
                    {
                        ISS_TRC (ALL_FAILURE_TRC,
                                 "AclRedHandleRmEvents: Acknowledgement "
                                 " to RM for GO_STANDBY event failed!!!!\r\n");
                    }
                }
            }

            if (gIssExGlobalInfo.AclRedGlobalInfo.u1NodeStatus == RM_ACTIVE)
            {
                /* CONFIG_RESTORE_COMPLETE event reached active node. Reset
                 * the Np sync block flag so that the Np Sync-ups are sent
                 * on active node. By default it is blocked because during
                 * MSR Restoration in the active node, Np sync-ups should
                 * not be sent.   */
                AclRedHwAuditDecBlkCounter ();
            }

            break;
        case L2_INITIATE_BULK_UPDATES:
            AclRedSendBulkRequest ();

            break;
        case RM_MESSAGE:

            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM (pMsg->RmMsg.pData, &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR (pMsg->RmMsg.pData, pMsg->RmMsg.u2DataLen);

            ProtoAck.u4AppId = RM_ACL_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            if (gIssExGlobalInfo.AclRedGlobalInfo.u1NodeStatus == RM_ACTIVE)
            {
                /* Hardware Handle to be sent to standby as a dynamic bulk 
                   request */
                AclRedProcessPeerMsgAtActive (pMsg->RmMsg.pData,
                                              pMsg->RmMsg.u2DataLen);

            }
            else if (gIssExGlobalInfo.AclRedGlobalInfo.
                     u1NodeStatus == RM_STANDBY)
            {
                AclRedProcessPeerMsgAtStandby (pMsg->RmMsg.pData,
                                               pMsg->RmMsg.u2DataLen);
            }
            else
            {
                ISS_TRC (ALL_FAILURE_TRC,
                         "AclRedHandleRmEvents: Sync-up message"
                         "received at Idle Node!!!!\r\n");
            }

            RM_FREE (pMsg->RmMsg.pData);

            /* Sending ACK to RM */
            AclPortRmApiSendProtoAckToRM (&ProtoAck);
            break;

        default:
            ISS_TRC (ALL_FAILURE_TRC,
                     "AclRedHandleRmEvents: Invalid" " RM event received\r\n");
            break;
    }

    return;
}

/*****************************************************************************/
/* Function Name      : AclRedHandleGoActive                                 */
/*                                                                           */
/* Description        : This function handles the GO_ACTIVE event received   */
/*            from RM                             */
/*                                                                           */
/* Input(s)           : None                                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gIssExGlobalInfo                                     */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
PUBLIC VOID
AclRedHandleGoActive (VOID)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, ISS_ZERO_ENTRY, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_ACL_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    if (gIssExGlobalInfo.AclRedGlobalInfo.u1NodeStatus == RM_ACTIVE)
    {
        ISS_TRC (CONTROL_PLANE_TRC, "AclRedHandleGoActive:"
                 "GO_ACTIVE event reached"
                 " when node is already active!!!!\r\n");
        return;
    }

    if (gIssExGlobalInfo.AclRedGlobalInfo.u1NodeStatus == RM_INIT)
    {
        ISS_TRC (CONTROL_PLANE_TRC, "AclRedHandleGoActive: Idle to Active"
                 " transition...\r\n");

        AclRedHandleIdleToActive ();
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }
    else if (gIssExGlobalInfo.AclRedGlobalInfo.u1NodeStatus == RM_STANDBY)
    {
        ISS_TRC (CONTROL_PLANE_TRC, "AclRedHandleGoActive: Standby to Active"
                 " transition...\r\n");
        AclRedHandleStandbyToActive ();
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }
    if (gIssExGlobalInfo.AclRedGlobalInfo.bBulkReqRcvd == OSIX_TRUE)
    {
        gIssExGlobalInfo.AclRedGlobalInfo.bBulkReqRcvd = OSIX_FALSE;
        AclRedHandleBulkRequest ();
    }

    if (AclPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "AclRedHandleGoActive:"
                 "Acknowledgement to RM for" "GO_ACTIVE event failed!!!!\r\n");
    }

    return;

}

/*****************************************************************************/
/* Function Name      : AclRedHandleGoStandby                                */
/*                                                                           */
/* Description        : This function handles the GO_STANDBY event received  */
/*            from RM                             */
/*                                                                           */
/* Input(s)           : None                                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gIssExGlobalInfo                                     */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
PUBLIC VOID
AclRedHandleGoStandby (VOID)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, ISS_ZERO_ENTRY, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_ACL_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    if (gIssExGlobalInfo.AclRedGlobalInfo.u1NodeStatus == RM_STANDBY)
    {
        ISS_TRC (CONTROL_PLANE_TRC, "AclRedHandleGoStandby:"
                 "GO_STANDBY event reached"
                 " when node is already standby!!!!\r\n");
        return;
    }
    /* Create the Hardware Audit Np sync Buffer table */
    AclRedInitNpSyncBufferTable ();

    if (gIssExGlobalInfo.AclRedGlobalInfo.u1NodeStatus == RM_INIT)
    {
        ISS_TRC (CONTROL_PLANE_TRC, "AclRedHandleGoStandby:"
                 "GO_STANDBY event received" " when state is Idle.\r\n");

        /* GO_STANDBY event is not processed here. It is done when
         * CONFIG_RESTORE_COMPLETE event is received. Since static bulk
         * update will be completed only by then, the acknowledgement can be
         * sent during CONFIG_RESTORE_COMPLETE handling, which will trigger RM
         * to send dynamic bulk update event to modules.  */
    }
    else if (gIssExGlobalInfo.AclRedGlobalInfo.u1NodeStatus == RM_ACTIVE)
    {
        ISS_TRC (CONTROL_PLANE_TRC, "AclRedHandleGoStandby: Active to Standby"
                 " transition...\r\n");
        AclRedHandleActiveToStandby ();
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

        if (AclPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            ISS_TRC (ALL_FAILURE_TRC, "AclRedHandleGoStandby:"
                     "Acknowledgement to RM for"
                     "GO_STANDBY event failed!!!!\r\n");
        }
    }

    return;
}

/*****************************************************************************/
/* Function Name      : AclRedHandleStandbyToActive                          */
/*                                                                           */
/* Description        : This function handles the GO_ACTIVE event when the   */
/*            node transition from Standby to Active State         */
/*                                                                           */
/* Input(s)           : None                                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gIssExGlobalInfo                                     */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
PUBLIC VOID
AclRedHandleStandbyToActive (VOID)
{
    /* Update the Node Status and standby node count */
    gIssExGlobalInfo.AclRedGlobalInfo.u1NodeStatus = RM_ACTIVE;
    gIssExGlobalInfo.AclRedGlobalInfo.u1NumOfStandbyNodesUp =
        AclPortRmGetStandbyNodeCount ();
    return;
}

/*****************************************************************************/
/* Function Name      : AclRedHandleActiveToStandby                          */
/*                                                                           */
/* Description        : This function handles the GO_Standby event when the  */
/*            node transition from Active to Standby State         */
/*                                                                           */
/* Input(s)           : None                                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gIssExGlobalInfo                                     */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
PUBLIC VOID
AclRedHandleActiveToStandby (VOID)
{
    gIssExGlobalInfo.AclRedGlobalInfo.u1NodeStatus = RM_STANDBY;
    gIssExGlobalInfo.AclRedGlobalInfo.u1NumOfStandbyNodesUp = ISS_ZERO_ENTRY;

    /* During Force-switchover, the Np Sync-ups has to be blocked here, so
     * that bulk updates does not access the Np Sync Buffer table */
    AclRedHwAuditIncBlkCounter ();
    return;
}

/*****************************************************************************/
/* Function Name      : AclRedHandleIdleToStandby                            */
/*                                                                           */
/* Description        : This function handles the GO_Standby event when the  */
/*            node transition from Idle to Standby State         */
/*                                                                           */
/* Input(s)           : None                                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gIssExGlobalInfo                                     */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
PUBLIC VOID
AclRedHandleIdleToStandby (VOID)
{
    gIssExGlobalInfo.AclRedGlobalInfo.u1NodeStatus = RM_STANDBY;
    gIssExGlobalInfo.AclRedGlobalInfo.u1NumOfStandbyNodesUp = ISS_ZERO_ENTRY;
    return;
}

/*****************************************************************************/
/* Function Name      : AclRedHandleIdleToActive                             */
/*                                                                           */
/* Description        : This function handles the GO_ACTIVE event when the   */
/*            node transition from Idle to Active State         */
/*                                                                           */
/* Input(s)           : None                                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gIssExGlobalInfo                                     */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
PUBLIC VOID
AclRedHandleIdleToActive (VOID)
{
    gIssExGlobalInfo.AclRedGlobalInfo.u1NodeStatus = RM_ACTIVE;
    gIssExGlobalInfo.AclRedGlobalInfo.u1NumOfStandbyNodesUp =
        AclPortRmGetStandbyNodeCount ();

    /* Np Sync-ups must be sent from Active node, hence the unblocking
     * by decrementing the counter. It was blocked on Boot-up.   */
    AclRedHwAuditDecBlkCounter ();
    return;
}

/*****************************************************************************/
/* Function Name      : AclRedProcessPeerMsgAtStandby                        */
/*                                                                           */
/* Description        : This function process the RM Message send by          */
/*             the Active Node.                      */
/*                                                                           */
/* Input(s)           : pMsg - RM Message Data                     */
/*             u2DataLen = RM Message Data lenght                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
PUBLIC VOID
AclRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT2               u2Offset = ISS_ZERO_ENTRY;
    UINT2               u2MinLen = ISS_ZERO_ENTRY;
    UINT2               u2Length = ISS_ZERO_ENTRY;
    UINT2               u2ExtractMsgLen = ISS_ZERO_ENTRY;
    UINT1               u1MsgType = ISS_ZERO_ENTRY;

    MEMSET (&ProtoEvt, ISS_ZERO_ENTRY, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_ACL_APP_ID;
    u2MinLen = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH;

    while (u2Offset <= u2DataLen)
    {
        u2ExtractMsgLen = u2Offset;

        NPSYNC_RM_GET_1_BYTE (pMsg, &u2Offset, u1MsgType);
        NPSYNC_RM_GET_2_BYTE (pMsg, &u2Offset, u2Length);

        if (u2Length < u2MinLen)
        {
            /* The Length field in the RM packet is less than minimum
             * number of bytes, which is MessageType + Length.    */
            u2Offset += u2Length;
            continue;
        }

        /* If extracting information upto the length present in the length
         * field goes beyond the total length, means length is not correct
         * discard the remaining information.           */
        u2ExtractMsgLen += u2Length;

        if (u2ExtractMsgLen > u2DataLen)
        {
            /* The Length field in the RM packet is wrong, hence continuing
             * with the next packet */
            ProtoEvt.u4Error = RM_PROCESS_FAIL;
            if (AclPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
            {
                ISS_TRC (CONTROL_PLANE_TRC, "AclRedProcessPeerMsgAtStandby: "
                         "Acknowledgement to RM for length "
                         "mismatch failed!!\r\n");
            }
            ISS_TRC (CONTROL_PLANE_TRC, "AclRedProcessPeerMsgAtStandby: "
                     "Wrong length message received!!!\r\n");
            return;
        }
        switch (u1MsgType)
        {
            case ACL_DYN_L2_HW_INFO:
                AclProcessL2DynSyncMsg (pMsg, &u2Offset);
                break;
            case ACL_DYN_L3_HW_INFO:
                AclProcessL3DynSyncMsg (pMsg, &u2Offset);
                break;
            case ACL_RED_BULK_UPDT_TAIL_MSG:
                AclRedProcessBulkTailMsg ();
                break;
            case NPSYNC_MESSAGE:
                AclProcessNpSyncMsg (pMsg, &u2Offset);
                break;
            default:
                break;
        }

        if (u2Offset == u2DataLen)
        {
            /* All data extracted from the buffer */
            break;
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : AclRedProcessPeerMsgAtActive                 */
/*                                                                           */
/* Description        : This function process the RM Message send by          */
/*             the Standby Node.                      */
/*                                                                           */
/* Input(s)           : pMsg - RM Message Data                     */
/*             u2DataLen = RM Message Data lenght                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/

PUBLIC VOID
AclRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{

    tRmProtoEvt         ProtoEvt;
    UINT2               u2Offset = ISS_ZERO_ENTRY;
    UINT2               u2Length = ISS_ZERO_ENTRY;
    UINT1               u1MsgType = ISS_ZERO_ENTRY;

    MEMSET (&ProtoEvt, ISS_ZERO_ENTRY, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_ACL_APP_ID;

    NPSYNC_RM_GET_1_BYTE (pMsg, &u2Offset, u1MsgType);
    NPSYNC_RM_GET_2_BYTE (pMsg, &u2Offset, u2Length);

    if (u2Offset != u2DataLen)
    {
        /* The only RM packet expected to be processed at active node is
         * Bulk Request message which has only Type and length.
         * Hence this validation is done.              */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        if (AclPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {

            ISS_TRC (CONTROL_PLANE_TRC,
                     "AclRedProcessPeerMsgAtActive:Acknowledgement to"
                     "RM for length mismatch failed\r\n");
        }

        return;
    }

    if (u2DataLen != ACL_RED_BULK_REQ_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        if (AclPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            ISS_TRC (CONTROL_PLANE_TRC,
                     "AclRedProcessPeerMsgAtActive:Acknowledgement to"
                     "RM for invalid length failed\r\n");
        }

        return;
    }
    if (u1MsgType == ACL_RED_BULK_REQUEST_MSG)
    {
        if (gIssExGlobalInfo.AclRedGlobalInfo.u1NumOfStandbyNodesUp == 0)
        {
            /*Before the arrival of STANDBY_UP or GO_ACTIVE event,
               Bulk Request has arrived from the Standby node.
               Wait for STANDBY_UP or GO_ACTIVE event. */

            gIssExGlobalInfo.AclRedGlobalInfo.bBulkReqRcvd = OSIX_TRUE;
        }
        else
        {
            AclRedHandleBulkRequest ();
        }
    }

    return;
}

/*****************************************************************************/
/* Function Name      : AclRedSendBulkRequest                             */
/*                                                                           */
/* Description        : This function sends Bulk Request Message to the         */
/*             Actuve Node                          */
/*             the Active Node.                      */
/*                                                                           */
/* Input(s)           : None                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
PUBLIC VOID
AclRedSendBulkRequest (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT2               u2Offset = ISS_ZERO_ENTRY;

    pMsg = RM_ALLOC_TX_BUF (ACL_RED_BULK_REQ_SIZE);

    if (pMsg == NULL)
    {
        ISS_TRC (BUFFER_TRC, "AclRedSendBulkRequest: Allocation "
                 "of memory from RM  Failed\n");
        return;
    }
    NPSYNC_RM_PUT_1_BYTE (pMsg, &u2Offset, ACL_RED_BULK_REQUEST_MSG);
    NPSYNC_RM_PUT_2_BYTE (pMsg, &u2Offset, ACL_RED_BULK_REQ_SIZE);

    AclPortRmEnqMsgToRm (pMsg, u2Offset, RM_ACL_APP_ID, RM_ACL_APP_ID);
    return;
}

/*****************************************************************************/
/* Function Name      : AclRedHandleBulkRequest                     */
/*                                                                           */
/* Description        : This function process the Bulk request Message         */
/*              send by the Standby Node.                  */
/*                                                                           */
/* Input(s)           : None                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/

PUBLIC VOID
AclRedHandleBulkRequest (VOID)
{
    if (gIssExGlobalInfo.AclRedGlobalInfo.u1NumOfStandbyNodesUp ==
        ISS_ZERO_ENTRY)
    {
        AclPortRmSetBulkUpdatesStatus ();
        AclRedSendBulkUpdTailMsg ();

        return;
    }
    /* Update the L2 filter Hardware Handle and L3 filter Hardware Handle */

    AclRedSyncL2FilterHwHandle ();
    AclRedSyncL3FilterHwHandle ();

    AclPortRmSetBulkUpdatesStatus ();
    AclRedSendBulkUpdTailMsg ();
    return;
}

/*****************************************************************************/
/* Function Name      : AclRedSyncL2FilterHwHandle                 */
/*                                                                           */
/* Description        : This function sends the L2 filter hw handle          */
/*             Bulk Sync up Message to Stadnby Node               */
/*             the Active Node.                      */
/*                                                                           */
/* Input(s)           : None                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/

PUBLIC VOID
AclRedSyncL2FilterHwHandle (VOID)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = ISS_ZERO_ENTRY;
    UINT4               u4Count = ISS_ZERO_ENTRY;
    INT4                i4FirstFilterNo = ISS_ZERO_ENTRY;
    INT4                i4SecondFilterNo = ISS_ZERO_ENTRY;

    MEMSET (&ProtoEvt, ISS_ZERO_ENTRY, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_ACL_APP_ID;

    pMsg = RM_ALLOC_TX_BUF (ACL_DB_MAX_BUF_SIZE);

    if (pMsg == NULL)
    {
        ISS_TRC (BUFFER_TRC, "AclRedSyncL2FilterHwHandle : Allocation "
                 "of memory from RM  Failed\n");
        return;
    }

    if (IssExtSnmpLowGetFirstValidL2FilterTableIndex
        (&i4FirstFilterNo) == ISS_FAILURE)
    {
        RM_FREE (pMsg);
        return;
    }
    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4FirstFilterNo);

    while (pIssExtL2FilterEntry != NULL)
    {

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u4Offset, ACL_DYN_L2_HW_INFO);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u4Offset, ACL_DYN_L2_HW_INFO_SIZE);

        NPSYNC_RM_PUT_4_BYTE (pMsg, &u4Offset,
                              pIssExtL2FilterEntry->i4IssL2FilterNo);
        for (u4Count = ISS_ZERO_ENTRY; u4Count < ISS_FILTER_SHADOW_MEM_SIZE;
             u4Count++)
        {
            NPSYNC_RM_PUT_4_BYTE (pMsg, &u4Offset,
                                  pIssExtL2FilterEntry->pIssFilterShadowInfo->
                                  au4HwHandle[u4Count]);
        }

        if ((ACL_DB_MAX_BUF_SIZE - u4Offset) < ACL_DYN_L2_HW_INFO_SIZE)
        {

            AclPortRmEnqMsgToRm (pMsg, (UINT2) u4Offset,
                                 RM_ACL_APP_ID, RM_ACL_APP_ID);

            u4Offset = 0;
            pMsg = RM_ALLOC_TX_BUF (ACL_DB_MAX_BUF_SIZE);

            if (pMsg == NULL)
            {
                ISS_TRC (BUFFER_TRC, "AclRedSyncL2FilterHwHandle : Allocation "
                         "of memory from RM  Failed\n");
                return;
            }
        }

        if (IssExtSnmpLowGetNextValidL2FilterTableIndex
            (i4FirstFilterNo, &i4SecondFilterNo) == ISS_FAILURE)
        {
            break;
        }
        pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4SecondFilterNo);
        i4FirstFilterNo = i4SecondFilterNo;

    }

    AclPortRmEnqMsgToRm (pMsg, (UINT2) u4Offset, RM_ACL_APP_ID, RM_ACL_APP_ID);

    return;
}

/*****************************************************************************/
/* Function Name      : AclRedSyncL3FilterHwHandle                           */
/*                                                                           */
/* Description        : This function sends the L3 filter hw handle          */
/*                      Bulk Sync up Message to Stadnby Node                 */
/*                      the Active Node.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
AclRedSyncL3FilterHwHandle (VOID)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4Offset = ISS_ZERO_ENTRY;
    UINT4               u4Count = ISS_ZERO_ENTRY;
    INT4                i4FirstFilterNo = ISS_ZERO_ENTRY;
    INT4                i4SecondFilterNo = ISS_ZERO_ENTRY;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_ACL_APP_ID;

    pMsg = RM_ALLOC_TX_BUF (ACL_DB_MAX_BUF_SIZE);
    if (pMsg == NULL)
    {
        ISS_TRC (BUFFER_TRC, "AclRedSyncL3FilterHwHandle : Allocation "
                 "of memory from RM  Failed\n");
        return;
    }

    if (IssExtSnmpLowGetFirstValidL3FilterTableIndex
        (&i4FirstFilterNo) == ISS_FAILURE)
    {
        RM_FREE (pMsg);
        return;
    }
    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4FirstFilterNo);

    while (pIssExtL3FilterEntry != NULL)
    {

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u4Offset, ACL_DYN_L3_HW_INFO);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u4Offset, ACL_DYN_L3_HW_INFO_SIZE);

        NPSYNC_RM_PUT_4_BYTE (pMsg, &u4Offset,
                              pIssExtL3FilterEntry->i4IssL3FilterNo);
        for (u4Count = ISS_ZERO_ENTRY; u4Count < ISS_FILTER_SHADOW_MEM_SIZE;
             u4Count++)
        {
            NPSYNC_RM_PUT_4_BYTE (pMsg, &u4Offset,
                                  pIssExtL3FilterEntry->pIssFilterShadowInfo->
                                  au4HwHandle[u4Count]);
        }

        if ((ACL_DB_MAX_BUF_SIZE - u4Offset) < ACL_DYN_L3_HW_INFO_SIZE)
        {

            AclPortRmEnqMsgToRm (pMsg, (UINT2) u4Offset,
                                 RM_ACL_APP_ID, RM_ACL_APP_ID);

            u4Offset = 0;
            pMsg = RM_ALLOC_TX_BUF (ACL_DB_MAX_BUF_SIZE);

            if (pMsg == NULL)
            {
                ISS_TRC (BUFFER_TRC, "AclRedSyncL3FilterHwHandle: Allocation "
                         "of memory from RM  Failed\n");
                return;
            }
        }

        if (IssExtSnmpLowGetNextValidL3FilterTableIndex
            (i4FirstFilterNo, &i4SecondFilterNo) == ISS_FAILURE)
        {
            break;
        }
        pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4SecondFilterNo);
        i4FirstFilterNo = i4SecondFilterNo;
    }

    AclPortRmEnqMsgToRm (pMsg, (UINT2) u4Offset, RM_ACL_APP_ID, RM_ACL_APP_ID);
    return;
}

/*****************************************************************************/
/* Function Name      : AclProcessL2DynSyncMsg                     */
/*                                                                           */
/* Description        : This function process the Bulk Sync Message for      */
/*             L2 Hw Handle send by the Active Node.              */
/*                                                                           */
/* Input(s)           : pMsg - RM Message Data                     */
/*             u2DataLen = RM Message Data lenght                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
PUBLIC VOID
AclProcessL2DynSyncMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;
    tIssFilterShadowTable IssFilterShadowInfo;
    UINT4               u4Count = ISS_ZERO_ENTRY;
    INT4                i4L2FilterNo = ISS_ZERO_ENTRY;

    MEMSET (&IssFilterShadowInfo, ISS_ZERO_ENTRY,
            sizeof (tIssFilterShadowTable));

    NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, i4L2FilterNo);
    for (u4Count = ISS_ZERO_ENTRY; u4Count < ISS_FILTER_SHADOW_MEM_SIZE;
         u4Count++)
    {
        NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                              IssFilterShadowInfo.au4HwHandle[u4Count]);
    }
    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4L2FilterNo);
    if (pIssExtL2FilterEntry != NULL)
    {
        MEMCPY (pIssExtL2FilterEntry->pIssFilterShadowInfo,
                &IssFilterShadowInfo, sizeof (tIssFilterShadowTable));
    }
    return;
}

/*****************************************************************************/
/* Function Name      : AclProcessL3DynSyncMsg                     */
/*                                                                           */
/* Description        : This function process the Bulk Sync Message for      */
/*             L3 Hw Handle send by the Active Node.              */
/*                                                                           */
/* Input(s)           : pMsg - RM Message Data                     */
/*             u2DataLen = RM Message Data lenght                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
PUBLIC VOID
AclProcessL3DynSyncMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;
    tIssFilterShadowTable IssFilterShadowInfo;
    UINT4               u4Count = ISS_ZERO_ENTRY;
    INT4                i4L3FilterNo = ISS_ZERO_ENTRY;

    MEMSET (&IssFilterShadowInfo, ISS_ZERO_ENTRY,
            sizeof (tIssFilterShadowTable));

    NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, i4L3FilterNo);
    for (u4Count = ISS_ZERO_ENTRY; u4Count < ISS_FILTER_SHADOW_MEM_SIZE;
         u4Count++)
    {
        NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                              IssFilterShadowInfo.au4HwHandle[u4Count]);
    }
    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4L3FilterNo);
    if (pIssExtL3FilterEntry != NULL)
    {
        MEMCPY (pIssExtL3FilterEntry->pIssFilterShadowInfo,
                &IssFilterShadowInfo, sizeof (tIssFilterShadowTable));
    }
    return;
}

/*****************************************************************************/
/* Function Name      : AclRedSendBulkUpdTailMsg                     */
/*                                                                           */
/* Description        : This function Send the Bulk Tail Message to           */
/*             the Active Node.                      */
/*                                                                           */
/* Input(s)           : None                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
PUBLIC VOID
AclRedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT2               u2Offset = ISS_ZERO_ENTRY;

    pMsg = RM_ALLOC_TX_BUF (ACL_RED_BULK_UPDT_TAIL_SIZE);

    if (pMsg == NULL)
    {
        ISS_TRC (BUFFER_TRC, "AclRedSendBulkUpdTailMsg : Allocation "
                 "of memory from RM  Failed\n");
        return;
    }

    NPSYNC_RM_PUT_1_BYTE (pMsg, &u2Offset, ACL_RED_BULK_UPDT_TAIL_MSG);
    NPSYNC_RM_PUT_2_BYTE (pMsg, &u2Offset, ACL_RED_BULK_UPDT_TAIL_SIZE);

    AclPortRmEnqMsgToRm (pMsg, u2Offset, RM_ACL_APP_ID, RM_ACL_APP_ID);
    AclRedHwAuditDecBlkCounter ();

    return;
}

/*****************************************************************************/
/* Function Name      : AclRedProcessBulkTailMsg                 */
/*                                                                           */
/* Description        : This function process the Bulk Tail Message send by  */
/*             the Active Node.                      */
/*                                                                           */
/* Input(s)           : None                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/

PUBLIC VOID
AclRedProcessBulkTailMsg (VOID)
{
    tRmProtoEvt         ProtoEvt;

    /* Since Bulk updated is complted in Standby, alllow updating Np Sync
     * Buffer.           */
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_ACL_APP_ID;
    ProtoEvt.u4Error = RM_NONE;
    ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;

    if (AclPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
    {
        ISS_TRC (CONTROL_PLANE_TRC, "AclRedProcessBulkTailMsg:  Bulk update "
                 "completion indication to RM failed. \r\n");
    }

    AclRedHwAuditDecBlkCounter ();
    return;
}

/*****************************************************************************/
/* Function Name      : AclRedHwAuditIncBlkCounter                           */
/*                                                                           */
/* Description        : This function increment the NP Block counter so      */
/*            that Sync Message will not be sent to the peer node. */
/*                                                                           */
/* Input(s)           : None                                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gIssExGlobalInfo                                     */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
PUBLIC VOID
AclRedHwAuditIncBlkCounter (VOID)
{
    gIssExGlobalInfo.AclRedGlobalInfo.u1NpSyncBlockCount++;
    return;
}

/*****************************************************************************/
/* Function Name      : AclRedHwAuditDecBlkCounter                 */
/*                                                                           */
/* Description        : This function decrement the NP Block counter so      */
/*            that Sync Message can be sent to the peer node when  */
/*            this counter is zero.                     */
/*                                                                           */
/* Input(s)           : None                                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gIssExGlobalInfo                                     */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
PUBLIC VOID
AclRedHwAuditDecBlkCounter (VOID)
{
    if (gIssExGlobalInfo.AclRedGlobalInfo.u1NpSyncBlockCount != ISS_ZERO_ENTRY)
    {
        gIssExGlobalInfo.AclRedGlobalInfo.u1NpSyncBlockCount--;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : AclRedInitHardwareAudit                              */
/*                                                                           */
/* Description        : This function intiate hardware Audit when the node   */
/*            comes up as an Active Node and recieves an event     */
/*            RM_INIT_HW_AUDIT from the RM.                 */
/*                                                                           */
/* Input(s)           : None                                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gIssExGlobalInfo                                     */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
PUBLIC VOID
AclRedInitHardwareAudit (VOID)
{
    tAclRedNpSyncEntry *pBuf = NULL;
    tAclRedNpSyncEntry *pTempBuf = NULL;

    if (gIssExGlobalInfo.AclRedGlobalInfo.u1NodeStatus != RM_ACTIVE)
    {
        ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC, "AclRedInitHardwareAudit"
                 "INIT_HW_AUDIT event reached"
                 "for Standby node. Not processed.\r\n");
        return;
    }
    TMO_DYN_SLL_Scan (&gIssExGlobalInfo.AclRedGlobalInfo.HwAuditTable, pBuf,
                      pTempBuf, tAclRedNpSyncEntry *)
    {
        switch (pBuf->u4NpApiId)
        {

            case NPSYNC_ISS_HW_UPDATE_L2_FILTER:
                AclRedHandleL2FilterHwAudit
                    (pBuf->unNpData.IssHwUpdateL2Filter.i4IssL2FilterNo,
                     pBuf->unNpData.IssHwUpdateL2Filter.IssFilterShadowTable,
                     pBuf->unNpData.IssHwUpdateL2Filter.i4Value);
                break;
            case NPSYNC_ISS_HW_UPDATE_L3_FILTER:
                AclRedHandleL3FilterHwAudit
                    (pBuf->unNpData.IssHwUpdateL3Filter.i4IssL3FilterNo,
                     pBuf->unNpData.IssHwUpdateL3Filter.IssFilterShadowTable,
                     pBuf->unNpData.IssHwUpdateL3Filter.i4Value);
                break;
            case NPSYNC_ISS_HW_SET_RATE_LIMITING_VALUE:
                AclRedHandleRateLimitHwAudit
                    (pBuf->unNpData.IssHwSetRateLimitingValue.u4IfIndex,
                     pBuf->unNpData.IssHwSetRateLimitingValue.u1PacketType,
                     pBuf->unNpData.IssHwSetRateLimitingValue.i4RateLimitVal);
                break;
            case NPSYNC_ISS_HW_SET_PORT_EGRESS_PKT_RATE:
                AclRedHandlePortEgressRateHwAudit
                    (pBuf->unNpData.IssHwSetPortEgressPktRate.u4IfIndex,
                     pBuf->unNpData.IssHwSetPortEgressPktRate.i4PktRate,
                     pBuf->unNpData.IssHwSetPortEgressPktRate.i4BurstRate);
                break;
            default:
                break;
        }
        TMO_SLL_Delete (&gIssExGlobalInfo.AclRedGlobalInfo.
                        HwAuditTable, &(pBuf->Node));
        MemReleaseMemBlock (gIssExGlobalInfo.AclRedGlobalInfo.
                            HwAuditTablePoolId, (UINT1 *) pBuf);
        pBuf = pTempBuf;

    }
    return;
}

/*****************************************************************************/
/* Function Name      : AclRedHandleL2FilterHwAudit                          */
/*                                                                           */
/* Description        : This function Audit the pending entry in the            */
/*            hardware buffer table for L2 Filter entry and take   */
/*            actions accordingly.                     */
/*                                                                           */
/* Input(s)           : i4IssL2FilterNo    - L2 Filter Number             */
/*            IssFilterShadowTable -Filter Shadow table         */
/*            i4Value    - ISS_L2FILTER_ADD/ISS_L2FILTER_DELETE       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
PUBLIC VOID
AclRedHandleL2FilterHwAudit (INT4 i4IssL2FilterNo,
                             tIssFilterShadowTable IssFilterShadowTable,
                             INT4 i4Value)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    tIssL2FilterEntry   IssAclL2FilterEntry;
#ifdef NPAPI_WANTED
    INT4                i4RetVal = FNP_FAILURE;
#endif

    MEMSET (&IssAclL2FilterEntry, ISS_ZERO_ENTRY, sizeof (tIssL2FilterEntry));

    IssAclL2FilterEntry.i4IssL2FilterNo = i4IssL2FilterNo;

    if (ISS_FILTER_SHADOW_ALLOC_MEM_BLOCK
        (IssAclL2FilterEntry.pIssFilterShadowInfo) == NULL)
    {
        ISS_TRC (BUFFER_TRC, "AclRedHandleL2FilterHwAudit: Allocation "
                 "of memory for Filter Shadow table Failed\n");
        return;
    }
    MEMSET (IssAclL2FilterEntry.pIssFilterShadowInfo, ISS_ZERO_ENTRY,
            sizeof (tIssFilterShadowTable));

    MEMCPY (IssAclL2FilterEntry.pIssFilterShadowInfo, &IssFilterShadowTable,
            sizeof (tIssFilterShadowTable));

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssL2FilterNo);

    if (i4Value == ISS_L2FILTER_ADD)
    {
        if ((pIssAclL2FilterEntry == NULL) || ((pIssAclL2FilterEntry != NULL)
                                               && (pIssAclL2FilterEntry->
                                                   u1IssL2FilterStatus !=
                                                   ISS_ACTIVE)))

        {
#ifdef NPAPI_WANTED
            i4RetVal = IssHwUpdateL2Filter (&IssAclL2FilterEntry,
                                            ISS_L2FILTER_DELETE);
            if (i4RetVal == FNP_FAILURE)
            {
                ISS_TRC (ALL_FAILURE_TRC, "AclRedHandleL2FilterHwAudit: "
                         "Failure in programming the hardware \n");
            }
#endif
        }
    }
    else if (i4Value == ISS_L2FILTER_DELETE)
    {
        if (pIssAclL2FilterEntry != NULL)
        {
#ifdef NPAPI_WANTED
            i4RetVal = IssHwUpdateL2Filter (pIssAclL2FilterEntry,
                                            ISS_L2FILTER_DELETE);
            i4RetVal = IssHwUpdateL2Filter (pIssAclL2FilterEntry,
                                            ISS_L2FILTER_ADD);
            if (i4RetVal == FNP_FAILURE)
            {
                ISS_TRC (ALL_FAILURE_TRC, "AclRedHandleL2FilterHwAudit: "
                         "Failure in programming the hardware \n");
            }
#endif
        }
    }

    ISS_FILTER_SHADOW_FREE_MEM_BLOCK (IssAclL2FilterEntry.pIssFilterShadowInfo);
    return;
}

/*****************************************************************************/
/* Function Name      : AclRedHandleL3FilterHwAudit                          */
/*                                                                           */
/* Description        : This function Audit the pending entry in the            */
/*            hardware buffer table for L3 Filter entry and take   */
/*            actions accordingly.                     */
/*                                                                           */
/* Input(s)           : i4IssL3FilterNo    - L3 Filter Number             */
/*            IssFilterShadowTable -Filter Shadow table         */
/*            i4Value    - ISS_L3FILTER_ADD/ISS_L3FILTER_DELETE       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
PUBLIC VOID
AclRedHandleL3FilterHwAudit (INT4 i4IssL3FilterNo,
                             tIssFilterShadowTable IssFilterShadowTable,
                             INT4 i4Value)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    tIssL3FilterEntry   IssAclL3FilterEntry;
#ifdef NPAPI_WANTED
    INT4                i4RetVal = FNP_FAILURE;
#endif

    MEMSET (&IssAclL3FilterEntry, ISS_ZERO_ENTRY, sizeof (tIssL3FilterEntry));

    IssAclL3FilterEntry.i4IssL3FilterNo = i4IssL3FilterNo;
    if (ISS_FILTER_SHADOW_ALLOC_MEM_BLOCK
        (IssAclL3FilterEntry.pIssFilterShadowInfo) == NULL)
    {
        ISS_TRC (BUFFER_TRC, "AclRedHandleL3FilterHwAudit: Allocation "
                 "of memory for Filter Shadow table Failed\n");
        return;
    }

    MEMSET (IssAclL3FilterEntry.pIssFilterShadowInfo, ISS_ZERO_ENTRY,
            sizeof (tIssFilterShadowTable));

    MEMCPY (IssAclL3FilterEntry.pIssFilterShadowInfo, &IssFilterShadowTable,
            sizeof (tIssFilterShadowTable));
    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    if (i4Value == ISS_L3FILTER_ADD)
    {
        if ((pIssAclL3FilterEntry == NULL) || ((pIssAclL3FilterEntry != NULL)
                                               && (pIssAclL3FilterEntry->
                                                   u1IssL3FilterStatus !=
                                                   ISS_ACTIVE)))
        {
#ifdef NPAPI_WANTED
            i4RetVal = IssHwUpdateL3Filter (&IssAclL3FilterEntry,
                                            ISS_L3FILTER_DELETE);
            if (i4RetVal == FNP_FAILURE)
            {
                ISS_TRC (ALL_FAILURE_TRC, "AclRedHandleL3FilterHwAudit: "
                         "Failure in programming the hardware \n");
            }
#endif
        }
    }
    else if (i4Value == ISS_L3FILTER_DELETE)
    {
        if (pIssAclL3FilterEntry != NULL)
        {
#ifdef NPAPI_WANTED
            i4RetVal = IssHwUpdateL3Filter (pIssAclL3FilterEntry,
                                            ISS_L3FILTER_DELETE);
            i4RetVal = IssHwUpdateL3Filter (pIssAclL3FilterEntry,
                                            ISS_L3FILTER_ADD);
            if (i4RetVal == FNP_FAILURE)
            {
                ISS_TRC (ALL_FAILURE_TRC, "AclRedHandleL3FilterHwAudit: "
                         "Failure in programming the hardware \n");
            }
#endif
        }
    }

    ISS_FILTER_SHADOW_FREE_MEM_BLOCK (IssAclL3FilterEntry.pIssFilterShadowInfo);
    return;
}

/*****************************************************************************/
/* Function Name      : AclRedHandleRateLimitHwAudit                         */
/*                                                                           */
/* Description        : This function Audit the pending entry in the            */
/*            hardware buffer table for Rate Limit entry and take  */
/*            actions accordingly.                     */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                 */
/*            u1PacketType -Packet Type -DLF/MCAST/UCAST          */
/*            i4RateLimitVal - Rate limiting value             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
PUBLIC VOID
AclRedHandleRateLimitHwAudit (UINT4 u4IfIndex, UINT1 u1PacketType,
                              INT4 i4RateLimitVal)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal = FNP_FAILURE;
#else
    UNUSED_PARAM (u1PacketType);
    UNUSED_PARAM (i4RateLimitVal);
#endif

    pIssAclRateCtrlEntry = gIssExGlobalInfo.apIssRateCtrlEntry[u4IfIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "AclRedHandleRateLimitHwAudit: "
                 "Rate Control Entry is not present.\n");
        return;
    }

#ifdef NPAPI_WANTED

    if (u1PacketType == ISS_RATE_DLF)
    {
        i4RateLimitVal = (INT4) pIssAclRateCtrlEntry->
            u4IssRateCtrlDLFLimitValue;

    }
    else if (u1PacketType == ISS_RATE_BCAST)
    {
        i4RateLimitVal = (INT4) pIssAclRateCtrlEntry->
            u4IssRateCtrlBCASTLimitValue;

    }
    else if (u1PacketType == ISS_RATE_MCAST)
    {
        i4RateLimitVal = (INT4) pIssAclRateCtrlEntry->
            u4IssRateCtrlMCASTLimitValue;

    }
    i4RetVal = IssHwSetRateLimitingValue (u4IfIndex, u1PacketType,
                                          i4RateLimitVal);
    if (i4RetVal == FNP_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "AclRedHandleRateLimitHwAudit: "
                 "Failure in programming the hardware \n");
    }
#endif
    return;

}

/*****************************************************************************/
/* Function Name      : AclRedHandlePortEgressRateHwAudit                    */
/*                                                                           */
/* Description        : This function Audit the pending entry in the            */
/*            hardware buffer table for Rate Limit entryfor          */
/*            port Egress limting and take actions accordingly.    */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                 */
/*            i4PktRate - Packet Rate                  */
/*            i4BurstRate - Burst Rate                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
PUBLIC VOID
AclRedHandlePortEgressRateHwAudit (UINT4 u4IfIndex, INT4 i4PktRate,
                                   INT4 i4BurstRate)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal = FNP_FAILURE;
#endif

    UNUSED_PARAM (i4PktRate);
    UNUSED_PARAM (i4BurstRate);

    pIssAclRateCtrlEntry = gIssExGlobalInfo.apIssRateCtrlEntry[u4IfIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "AclRedHandlePortEgressRateHwAudit: "
                 "Rate Control Entry is not present. \n");
        return;
    }

#ifdef NPAPI_WANTED
    i4RetVal = IssHwSetPortEgressPktRate
        (u4IfIndex, pIssAclRateCtrlEntry->i4IssRateCtrlPortLimitRate,
         pIssAclRateCtrlEntry->i4IssRateCtrlPortBurstRate);

    if (i4RetVal == FNP_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "AclRedHandlePortEgressRateHwAudit: "
                 "Failure in programming the hardware \n");
    }

#endif
    return;
}

/*****************************************************************************/
/* Function Name      : AclRedInitNpSyncBufferTable                         */
/*                                                                           */
/* Description        : This function intialise the NP Sync Buffer         */
/*            table which will be used for Pending entry during    */
/*            Hardware Audit                          */
/*                                                                           */
/* Input(s)           : None                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gIssExGlobalInfo                                     */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
PUBLIC VOID
AclRedInitNpSyncBufferTable (VOID)
{

    tAclRedNpSyncEntry *pBuf = NULL;
    tAclRedNpSyncEntry *pTempBuf = NULL;

    /* If the SLL is not empty then delete all the buffer entries. */
    if (TMO_SLL_Count (&gIssExGlobalInfo.AclRedGlobalInfo.HwAuditTable)
        != ISS_ZERO_ENTRY)
    {
        TMO_DYN_SLL_Scan (&gIssExGlobalInfo.AclRedGlobalInfo.HwAuditTable,
                          pBuf, pTempBuf, tAclRedNpSyncEntry *)
        {
            TMO_SLL_Delete (&gIssExGlobalInfo.AclRedGlobalInfo.HwAuditTable,
                            &(pBuf->Node));
            MemReleaseMemBlock (gIssExGlobalInfo.AclRedGlobalInfo.
                                HwAuditTablePoolId, (UINT1 *) pBuf);
            pBuf = pTempBuf;
        }
    }

    TMO_SLL_Init (&(gIssExGlobalInfo.AclRedGlobalInfo.HwAuditTable));
    return;

}

/*****************************************************************************/
/* Function Name      : AclRedUpdateNpSyncBufferTable                         */
/*                                                                           */
/* Description        : This function Update the NP Sync Buffer                  */
/*            table after processing the NP Sync Message from the  */
/*            Peer node.                          */
/*                                                                           */
/* Input(s)           : pNpSync - Pointer to the Union of NP Sync Entry         */
/*            u4NpApiId - NPAPI ID                     */
/*            u4EventId - Event Id                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gIssExGlobalInfo                                     */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
PUBLIC VOID
AclRedUpdateNpSyncBufferTable (unNpSync * pNpSync, UINT4 u4NpApiId,
                               UINT4 u4EventId)
{
    tAclRedNpSyncEntry *pBuf = NULL;
    tAclRedNpSyncEntry *pTempBuf = NULL;
    tAclRedNpSyncEntry *pPrevBuf = NULL;
    UINT1               u1Match = OSIX_FALSE;

    if (u4EventId == ISS_ACL_NP_SYNC_SUCCESS)
    {
        AclUpdateHwFilterEntry (u4NpApiId, *pNpSync);
        return;
    }

    TMO_DYN_SLL_Scan (&gIssExGlobalInfo.AclRedGlobalInfo.HwAuditTable, pBuf,
                      pTempBuf, tAclRedNpSyncEntry *)
    {
        if ((u4NpApiId != ISS_ZERO_ENTRY) && (u4NpApiId == pBuf->u4NpApiId))
        {
            if (MEMCMP (&(pBuf->unNpData), pNpSync,
                        sizeof (unNpSync)) == ISS_ZERO_ENTRY)
            {
                TMO_SLL_Delete (&gIssExGlobalInfo.AclRedGlobalInfo.
                                HwAuditTable, &(pBuf->Node));
                MemReleaseMemBlock (gIssExGlobalInfo.AclRedGlobalInfo.
                                    HwAuditTablePoolId, (UINT1 *) pBuf);
                pBuf = NULL;
                u1Match = OSIX_TRUE;
            }
        }
        pPrevBuf = pBuf;
    }

    if (u1Match == OSIX_FALSE)
    {
        pTempBuf = NULL;

        pTempBuf = (tAclRedNpSyncEntry *) MemAllocMemBlk
            (gIssExGlobalInfo.AclRedGlobalInfo.HwAuditTablePoolId);

        if (pTempBuf == NULL)
        {
            ISS_TRC (ALL_FAILURE_TRC, "AclRedUpdateNpSyncBufferTable :"
                     "Hardware Audit entry memory "
                     "allocation failed!!!!\r\n");
            return;
        }

        TMO_SLL_Init_Node (&(pTempBuf->Node));

        MEMCPY (&pTempBuf->unNpData, pNpSync, sizeof (unNpSync));

        pTempBuf->u4NpApiId = u4NpApiId;

        pTempBuf->u4EventId = u4EventId;

        if (pPrevBuf == NULL)
        {
            /* Node to be inserted in first position */
            TMO_SLL_Insert (&gIssExGlobalInfo.AclRedGlobalInfo.HwAuditTable,
                            NULL, (tTMO_SLL_NODE *) & (pTempBuf->Node));
        }
        else
        {
            /* Node to be inserted in last position */
            TMO_SLL_Insert (&gIssExGlobalInfo.AclRedGlobalInfo.HwAuditTable,
                            (tTMO_SLL_NODE *) & (pPrevBuf->Node),
                            (tTMO_SLL_NODE *) & (pTempBuf->Node));
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : AclUpdateHwFilterEntry                                 */
/*                                                                           */
/* Description        : This function Update the L2 and L3 filter entry's    */
/*            Hardware handle infromation after processing the NP  */
/*            Sync Message while update the NP Sync buffer table.  */
/*                                                                           */
/* Input(s)           : unNpData - Pointer to the Union of NP Sync Entry     */
/*            u4NpApiId - NPAPI ID                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
PUBLIC VOID
AclUpdateHwFilterEntry (UINT4 u4NpApiId, unNpSync unNpData)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    if (u4NpApiId == NPSYNC_ISS_HW_UPDATE_L2_FILTER)
    {
        pIssAclL2FilterEntry = IssExtGetL2FilterEntry
            (unNpData.IssHwUpdateL2Filter.i4IssL2FilterNo);
        if (pIssAclL2FilterEntry != NULL)
        {
            MEMCPY (pIssAclL2FilterEntry->pIssFilterShadowInfo,
                    &unNpData.IssHwUpdateL2Filter.IssFilterShadowTable,
                    sizeof (tIssFilterShadowTable));
        }

    }
    if (u4NpApiId == NPSYNC_ISS_HW_UPDATE_L3_FILTER)
    {
        pIssAclL3FilterEntry = IssExtGetL3FilterEntry
            (unNpData.IssHwUpdateL3Filter.i4IssL3FilterNo);
        if (pIssAclL3FilterEntry != NULL)
        {
            MEMCPY (pIssAclL3FilterEntry->pIssFilterShadowInfo,
                    &unNpData.IssHwUpdateL3Filter.IssFilterShadowTable,
                    sizeof (tIssFilterShadowTable));
        }
    }
    return;
}
