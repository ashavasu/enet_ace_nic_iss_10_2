/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * * $Id: issexweb.h,v 1.2 2013/05/24 13:27:09 siva Exp $
 *
 *******************************************************************/

#ifndef _ISSEXWEB_H
#define _ISSEXWEB_H

#include "webiss.h"
#include "isshttp.h"
#include "aclcli.h"
#include "iss.h"
#include "fsisselw.h"
#include "diffsrv.h"


#define   ACL_EXTENDED_START 1001

/* Prototypes for specific pages processing */
INT4 IssProcessCustomPages (tHttp * pHttp);
VOID IssWintegraProcessMACFilterConfPage (tHttp * pHttp);
VOID IssWintegraProcessIPFilterConfPage (tHttp * pHttp);
VOID IssWintegraProcessIPStdFilterConfPage (tHttp * pHttp);
VOID IssWintegraProcessIPFilterConfPageSet (tHttp * pHttp);
VOID IssWintegraProcessIPFilterConfPageGet (tHttp * pHttp);
VOID IssWintegraProcessIPStdFilterConfPageGet (tHttp * pHttp);
VOID IssWintegraProcessIPStdFilterConfPageSet (tHttp * pHttp);
VOID IssWintegraProcessMACFilterConfPageGet (tHttp * pHttp);
VOID IssWintegraProcessMACFilterConfPageSet (tHttp * pHttp);

extern UINT1 nmhTestv2IssAclL3FilteAddrType(UINT4 * ,
                          INT4 , INT4 );

extern UINT1 nmhSetIssAclL3FilteAddrType(INT4 ,INT4);
extern UINT1 nmhTestv2IssAclL3FilterSrcIpAddr (UINT4 * , INT4 ,tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhSetIssAclL3FilterSrcIpAddr (INT4, tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhTestv2IssAclL3FilterDstIpAddr (UINT4 *e, INT4, tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhSetIssAclL3FilterDstIpAddr (INT4, tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhTestv2IssAclL3FilterDstIpAddrPrefixLength  (UINT4*, INT4, UINT4);
extern UINT1 nmhSetIssAclL3FilterDstIpAddrPrefixLength (INT4, UINT4);
extern UINT1 nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength (UINT4 *, INT4, UINT4);
extern UINT1 nmhSetIssAclL3FilterSrcIpAddrPrefixLength (INT4, UINT4);
extern UINT1 nmhTestv2IssAclL3FilterFlowId (UINT4 *, INT4, UINT4);
extern UINT1 nmhSetIssAclL3FilterFlowId (INT4 , UINT4);
extern UINT1 nmhGetIssAclL3FilteAddrType (INT4, INT4 *);
extern UINT1 nmhGetIssAclL3FilterSrcIpAddr (INT4, tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhGetIssAclL3FilterDstIpAddr (INT4, tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhGetIssAclL3FilterDstIpAddrPrefixLength (INT4, UINT4 *);
extern UINT1 nmhGetIssAclL3FilterSrcIpAddrPrefixLength (INT4, UINT4 *);
extern UINT1 nmhGetIssAclL3FilterFlowId (INT4, UINT4 *);
extern INT1  nmhGetIssL3FilterOutPortList (INT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1  nmhGetIssL3FilterInPortList (INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1  nmhTestv2DiffServMultiFieldClfrStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1  nmhSetDiffServMultiFieldClfrStorage ARG_LIST((UINT4  ,INT4 ));
extern INT1  nmhGetDiffServMultiFieldClfrStorage ARG_LIST((UINT4 ,INT4 *));


tSpecificPage asIssTargetpages [] ={
    {"wintegra_ip_filterconf.html", IssWintegraProcessIPFilterConfPage},
    {"wintegra_ip_stdfilterconf.html", IssWintegraProcessIPStdFilterConfPage},
    {"wintegra_mac_filterconf.html", IssWintegraProcessMACFilterConfPage},
 {"", NULL}
};

#endif
