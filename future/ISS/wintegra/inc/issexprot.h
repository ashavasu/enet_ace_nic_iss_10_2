/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: issexprot.h,v 1.4 2015/05/26 13:14:04 siva Exp $
 *
 * Description: This file include all the prototypes required 
 *              for the operation of the ACL module
 ****************************************************************************/

#ifndef _ISSEXPROT_H
#define _ISSEXPROT_H

VOID IssSetDefaultRateCtrlValues (UINT2 u2PortIndex,
                                  tIssRateCtrlEntry * pIssRateCtrlEntry);

tIssL2FilterEntry *IssExtGetL2FilterEntry (INT4 i4IssL2FilterNo);
tIssL3FilterEntry *IssExtGetL3FilterEntry (INT4 i4IssL3FilterNo);
tIssUserDefinedFilterTable * IssExtGetUdbFilterTableEntry (UINT4 u4UdbFilter);

INT4 IssExtQualifyL2FilterData (tIssL2FilterEntry ** ppIssL2FilterEntry);

INT4 IssExtQualifyL3FilterData (tIssL3FilterEntry ** ppIssL3FilterEntry);
INT4 IssExtSnmpLowGetFirstValidUDBFilterTableIndex (INT4 *pu4FirstUdbFilterIndex);
INT4 IssSnmpLowGetNextValidUdbFilterTableIndex (UINT4 u4IssUdbFilterNo,
                            UINT4 *pu4NextUdbFilterNo);

INT4 IssProcessAndOrNotFilterParamForL2 (tIssL2FilterEntry   *pIssAclL2FilterEntryOne,
                                         tIssL2FilterEntry   *pIssAclL2FilterEntryTwo,
                                         UINT1 u1Action,
                                         tIssL2FilterEntry *pIssAclL2FilterEntry,
                                         UINT1 *pu1Result);

INT4 IssProcessAndOrNotFilterParamForL3 (tIssL3FilterEntry   *pIssAclL3FilterEntryOne,
                                         tIssL3FilterEntry   *pIssAclL3FilterEntryTwo,
                                         UINT1 u1Action,
                                         tIssL3FilterEntry *pIssAclL3FilterEntry,
                                         UINT1 *pu1Result);
INT4 IssIsMemberPort (UINT1 *pu1PortArray, UINT4 u4PortArrayLen, UINT4 u4Port); 
UINT1 IssCalculateTrafficLogicForPort PROTO ((UINT4 u4Port,tIssRedirectPortMaskAndIndex
                                       *pIssRedirectPortMaskAndIndex,
                                       UINT1 *pu1mask, UINT4 *pu4NoOfRules,UINT1 u1ActivePortExist,
                                       tIssRedirectIfPortGroup *pIssRedirectIfPortGroup));

VOID IssGetNextActiveOutIndex PROTO ((tIssRedirectIfPortGroup *pIssRedirectIfPortGroup,UINT4 u4NumPorts,UINT4 *pu4CurrIndex));

VOID IssInterfaceGrpRedirectIdNextFree PROTO ((UINT4 *pu4RetValIssRedirectInterfaceGrpIdNextFree));

VOID IssExAddAclToPriorityTable PROTO ((INT4 i4Priority,tTMO_SLL_NODE *pFilterNode,UINT1 u1FilterType));
VOID IssExGetPriorityFilterTableCount PROTO ((UINT4 *u4PriFilterCount));
VOID IssExDeleteAclPriorityFilterTable PROTO ((INT4 i4Priority,tTMO_SLL_NODE *pFilterNode,UINT1 u1FilterType));
INT4 IssExGetIssCommitSupportImmediate PROTO ((VOID));
INT4 IssGetTriggerCommit PROTO ((VOID));
INT4 IssExPrgAclsToNpWithPriority PROTO ((VOID));

VOID
IssConvertRedirectPortListToArray PROTO ((tPortList LocalPortList,tIssRedirectIfPortGroup *pIssRedirectIfPortGroup,
                                   UINT4 *pu4IfPortArray, UINT4 *pu4NumPorts,UINT1 *pu1ActivePortExist));
INT4 AclCreateUserDefFilter PROTO ((tCliHandle CliHandle, INT4));
INT4 AclDestroyUserDefFilter PROTO ((tCliHandle CliHandle, INT4 i4FilterNo));

INT4 AclUserDefinedOperations PROTO ((tCliHandle CliHandle, INT4,UINT4,UINT4,UINT4,UINT4));

INT4 AclShowUDBFilter PROTO ((tCliHandle CliHandle, INT4 ));

INT4 AclUserDefinedAccessGroup PROTO ((tCliHandle CliHandle, INT4 , INT4 ));

INT4 AclNoUserDefinedAccessGroup PROTO ((tCliHandle CliHandle, INT4, INT4 ));
INT4 AclUserDefinedFilterConfig  PROTO (
                            (tCliHandle CliHandle, 
                            INT4 i4Action,UINT4 u4PktType, UINT2 u2PktStart,
                            UINT2 u2Offset1Pos, UINT2 u2Offset1Val,UINT2 u2Offset2Pos,
                            UINT2 u2Offset2Val,UINT2 u2Offset3Pos, UINT2 u2Offset3Val,
                            UINT2 u2Offset4Pos,UINT2 u2Offset4Val,UINT2 u2Offset5Pos,
                            UINT2 u2Offset5Val,UINT2 u2Offset6Pos,UINT2 u2Offset6Val,
                            UINT4 u4SubAction, UINT2 u2SubActionId,UINT4));
INT4 AclRedirectFilterConfig PROTO ((tCliHandle CliHandle, INT4 i4Action,
                          INT4 i4IsPortList, INT4 i4TrafficDist,
                          INT1 *pi1IfName0, INT1 *pi1IfListStr0,
                          INT1 *pi1IfName1, INT1 *pi1IfListStr1,
                          INT1 *pi1IfName2, INT1 *pi1IfListStr2,
                          UINT1));

/* Red */

VOID AclProcessQMsgEvent(VOID);

INT4 AclRedInitGlobalInfo (VOID);
INT4 AclRedDeInitGlobalInfo (VOID);
INT4 AclRmRegisterProtocols(tRmRegParams *pRmRegParams);
INT4 AclRmDeRegisterProtocols (VOID);
INT4 AclPortRmReleaseMemoryForMsg (UINT1 *pData);
INT4 AclPortRmApiHandleProtocolEvent (tRmProtoEvt * pEvt);
INT4 AclPortRmApiSendProtoAckToRM (tRmProtoAck * pProtoAck);
UINT4 AclPortRmEnqMsgToRm (tRmMsg * pRmMsg, UINT2 u2DataLen, UINT4 u4SrcEntId,
                      UINT4 u4DestEntId);
INT4 AclPortRmGetNodeState(VOID);
UINT1 AclPortRmGetStandbyNodeCount (VOID);
VOID AclPortRmSetBulkUpdatesStatus (VOID);
INT4 AclRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen);
INT4 AclQueEnqMsg(tAclQMsg * pMsg);
VOID AclRedHandleRmEvents (tAclQMsg * pMsg);
VOID AclRedHandleGoActive (VOID);
VOID AclRedHandleGoStandby (VOID);
VOID AclRedHandleStandbyToActive (VOID);
VOID AclRedHandleActiveToStandby (VOID);
VOID AclRedHandleIdleToStandby (VOID);
VOID AclRedHandleIdleToActive (VOID);
VOID AclRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen);
VOID AclRedHwAuditIncBlkCounter (VOID);
VOID AclRedHwAuditDecBlkCounter (VOID);
VOID AclRedInitNpSyncBufferTable (VOID);
VOID AclRedUpdateNpSyncBufferTable (unNpSync *pNpSync,UINT4 u4NpApiId,
                                        UINT4 u4EventId);
VOID AclUpdateHwFilterEntry(UINT4 u4NpApiId, unNpSync unNpData);
VOID AclProcessNpSyncMsg (tRmMsg  *pMsg, UINT2 *pu2OffSet);
VOID AclRedInitHardwareAudit (VOID);
VOID AclRedHandleL2FilterHwAudit(INT4 i4IssL2FilterNo, tIssFilterShadowTable IssFilterShadowTable,
                                                        INT4 i4Value);
VOID AclRedHandleL3FilterHwAudit(INT4 i4IssL3FilterNo, tIssFilterShadowTable IssFilterShadowTable,
                                                         INT4 i4Value);
VOID AclRedHandleRateLimitHwAudit(UINT4 u4IfIndex, UINT1 u1PacketType,INT4 i4RateLimitVal);
VOID AclRedHandlePortEgressRateHwAudit(UINT4 u4IfIndex, INT4 i4PktRate, INT4 i4BurstRate);

VOID AclRedSendBulkRequest (VOID);
VOID AclRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen);
VOID AclRedHandleBulkRequest (VOID);
VOID AclRedSyncL2FilterHwHandle (VOID);
VOID AclRedSyncL3FilterHwHandle (VOID);
VOID AclRedSendBulkUpdTailMsg (VOID);
VOID AclRedProcessBulkTailMsg (VOID);
VOID AclProcessL2DynSyncMsg (tRmMsg  *pMsg, UINT2 *pu2OffSet);
VOID AclProcessL3DynSyncMsg (tRmMsg  *pMsg, UINT2 *pu2OffSet);

/* End Red */



/******** Function Prototypes *************/

INT4  AclDestroyIPFilter PROTO ((tCliHandle,  INT4 ));

INT4 AclCreateMacFilter PROTO ((tCliHandle , INT4 ));

INT4 AclDestroyMacFilter PROTO ((tCliHandle , INT4 ));

INT4 AclMacAccessGroup PROTO ((tCliHandle CliHandle, INT4 i4FilterNo ));

INT4 AclNoMacAccessGroup PROTO ((tCliHandle CliHandle, INT4 i4FilterNo));
    
 INT4 AclMacExtAccessGroup PROTO ((tCliHandle, INT4, INT4));

 INT4 AclNoMacExtAccessGroup PROTO ((tCliHandle, INT4, INT4));

INT4 AclShowAccessLists PROTO ((tCliHandle CliHandle,INT4 i4FilterType,INT4 i4FilterNo));
INT4 AclShowL2Filter PROTO ((tCliHandle CliHandle,INT4 i4NextFilter));
INT4 AclShowL3Filter PROTO ((tCliHandle CliHandle,INT4 i4NextFilter));

VOID AclShowRunningConfigTables(tCliHandle);
VOID AclShowRunningConfigInterfaceDetails(tCliHandle,INT4);

INT4 AclTestIpParams PROTO ((UINT1 ,INT4 i4FilterNo,UINT4 u4SrcType,UINT4 u4SrcIpAddr, UINT4 u4SrcMask ));
                                           
INT4 AclSetIpParams PROTO ((UINT1 ,INT4 i4FilterNo,UINT4 u4SrcType,UINT4 u4SrcIpAddr, UINT4 u4SrcMask ));

VOID AclCliPrintPortList(tCliHandle CliHandle,
                         INT4 i4CommaCount, UINT1 * piIfName);
INT4 AclExtPbL3FilterConfig PROTO ((tCliHandle, INT4, INT4, INT4, INT4, INT4));
INT4 AclExtPbL2FilterConfig PROTO ((tCliHandle, INT4, INT4, INT4, INT4, INT4));

INT4 AclStdIpFilterConfig PROTO 
((tCliHandle, INT4,UINT4, UINT4 ,UINT4 ,UINT4, UINT4, UINT4, UINT4, UINT2,UINT4));

INT4 AclExtIpFilterConfig PROTO 
((tCliHandle, INT4, INT4, UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, INT4, INT4,
INT4, UINT4, UINT2));

INT4
AclExtIp6FilterConfig PROTO ((tCliHandle CliHandle, UINT4 u4SrcIpAddr,
                       tIp6Addr SrcIp6Addr, UINT4 u4SrcIpMask,
                       UINT4 u4DestIpAddr, tIp6Addr DstIp6Addr,
                 UINT4 u4DestIpMask, UINT4 u4FlowId, INT4 i4Action,
                       UINT4 u4SubAction, UINT2 u2SubActionId,UINT4));

INT4 AclExtIpFilterTcpUdpConfig PROTO ((tCliHandle, INT4 i4Action, INT4 i4Protocol,
  UINT4 u4SrcType , UINT4 u4SrcIpAddr,  UINT4 u4SrcIpMask, 
  UINT4 u4SrcFlag, UINT4 u4SrcMinPort,  UINT4 u4SrcMaxRangePort,
  UINT4 u4DestType, UINT4  u4DestIpAddr,UINT4 u4DestIpMask,
  UINT4 u4DestFlag, UINT4 u4DestMinPort ,UINT4  u4DestMaxRangePort, 
  UINT4 u4BitType , INT4 i4Tos, INT4 i4Dscp, INT4 i4Priority , UINT4 u4SubAction, 
  UINT2 u2SubActionId));

 INT4 AclExtIpFilterIcmpConfig PROTO
 ((tCliHandle, INT4 i4Action, UINT4,UINT4 , UINT4, UINT4, UINT4, UINT4 , INT4, INT4, INT4));

 INT4  AclIpAccessGroup PROTO ((tCliHandle, INT4 , INT4));
  
 INT4 AclNoIpAccessGroup PROTO ((tCliHandle, INT4 i4FilterNo, INT4 ));
 
 INT4 AclExtMacFilterConfig PROTO ((tCliHandle CliHandle, INT4 i4Action,
                       UINT4 u4SrcType, tMacAddr SrcMacAddr,
                       UINT4 u4DestType ,tMacAddr DestMacAddr,
                       INT4 i4Protocol, INT4 i4Encap,
                       UINT4 u4VlanId, INT4 u4Priority,
                       UINT4 u4SubAction, UINT2 u2SubActionId, INT4, INT4));

INT4 AclUpdateOverPortChannel PROTO ((UINT4 u4IfIndex));
INT4 AclEnabledForPort (UINT4 u4IfIndex);
INT4 AclIpFilterEnabledForPort (UINT4 u4IfIndex);
INT4 AclMacFilterEnabledForPort (UINT4 u4IfIndex);
INT4 AclUserDefinedFilterEnabledForPort  (UINT4 u4IfIndex);
#endif /* _ISSEXPROT_H */

