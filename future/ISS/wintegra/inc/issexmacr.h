/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: issexmacr.h,v 1.2 2013/05/24 13:27:09 siva Exp $
 *
 * Description: This file include all the macros required 
 *              for the operation of the ACL module
 ****************************************************************************/
#ifndef _ISSEXMACR_H
#define _ISSEXMACR_H

#define ISS_MAX_RATE_VALUE              65535
#define ISS_DEF_RATE_VALUE              500
#define ISS_MIN_RATE_VALUE              1

/* Macros for validattion */
#define ISS_MIN_PROTOCOL_ID             1536
#define ISS_MAX_PROTOCOL_ID             65535
#define ISS_MIN_FILTER_ID               0
#define ISS_MAX_FILTER_ID               65535
#define ISS_MAX_FILTER_PRIORITY         255
#define ISS_DEFAULT_FILTER_PRIORITY     1
#define ISS_DEFAULT_PROTOCOL_TYPE       0
#define ISS_DEF_FILTER_MASK             0
#define ISS_MIN_DSCP_VALUE              0
#define ISS_MAX_DSCP_VALUE              63

#define ISS_MIN_REDIRECT_GRP_ID   0
#define ISS_MAX_REDIRECT_GRP_ID   50
#define ISS_MAX_PRIORITY_GRP_ID   50
#define ISS_UDB_FILTER_ENTRY_POOL_ID 50
#define ISS_MAX_REDIRECT_GRP_SIZE   256
/* ISS l2  ACL */
#define ISS_DEFAULT_VLAN_PRIORITY       -1

#define ISS_SERVICE_VLAN_ETHER_TYPE     0x88a8
#define ISS_CUSTOMER_VLAN_ETHER_TYPE    0x8100


/* Macros for ACL Queue */
#define ISS_SYS_ACL_QUEUE  "ACLQ"
#define ISS_QUEUE_SIZE   30
#define ISS_ACL_RM_MESSAGE  1

/* Macros for Hardware Audit Np Sync */
#define ACL_NPSYNC_BLK() \
  (gIssExGlobalInfo.AclRedGlobalInfo.u1NpSyncBlockCount)
#define ISS_MAX_HW_NP_BUF_ENTRY  1

#define ISS_MAX_SHADOW_TEMP_SIZE  1 /* To allocate additional shodow 
    memory to assist L2/L3 hardware audit process */

#define ISS_ACL_NP_SYNC_UPDATE  1
#define ISS_ACL_NP_SYNC_FAILURE  2
#define ISS_ACL_NP_SYNC_SUCCESS  3

/* Macros of Redundancy Bulk Update */

#define ACL_RED_BULK_REQUEST_MSG 1
#define ACL_DYN_L2_HW_INFO  2
#define ACL_DYN_L3_HW_INFO  3
#define ACL_RED_BULK_UPDT_TAIL_MSG 4

#define ACL_RED_BULK_REQ_SIZE  3
#define ACL_DYN_L2_HW_INFO_SIZE  (3 + 4 + (4 * ISS_FILTER_SHADOW_MEM_SIZE))
                                        /* 3 bytes for Type of Length +
                                           4 Bytes for Filter Number + 
                                           (4 * ISS_FILTER_SHADOW_MEM_SIZE)
                                    bytes for Hardware Handle info
                                         */
#define ACL_DYN_L3_HW_INFO_SIZE  (3 + 4 + (4 * ISS_FILTER_SHADOW_MEM_SIZE))
                                        /* 3 bytes for Type of Length +
                                           4 Bytes for Filter Number + 
                                           (4 * ISS_FILTER_SHADOW_MEM_SIZE)
                                    bytes for Hardware Handle info
                                         */
#define ACL_RED_BULK_UPDT_TAIL_SIZE 3
#define ACL_DB_MAX_BUF_SIZE  1500

#define ISS_FILTER_SHADOW_POOL_SIZE ISS_MAX_L2_FILTERS + ISS_MAX_L3_FILTERS + \
        ISS_MAX_SHADOW_TEMP_SIZE

/* MEM Pool Id Definitions */
#define ISS_RATEENTRY_POOL_ID            gIssExGlobalInfo.IssRateCtrlPoolId
#define ISS_L2FILTERENTRY_POOL_ID        gIssExGlobalInfo.IssL2FilterPoolId
#define ISS_L3FILTERENTRY_POOL_ID        gIssExGlobalInfo.IssL3FilterPoolId

#define ISS_REDIRECT_INTF_GRP_POOL_ID    gIssExGlobalInfo.IssRedirectIntfGrpPoolId
#define ISS_UDB_FILTER_TABLE_POOL_ID     gIssExGlobalInfo.IssUdbTablePoolId
#define ISS_UDB_FILTERENTRY_POOL_ID      gIssExGlobalInfo.IssUdbFilterPoolId
#define ISS_FILTER_SHADOW_POOL_ID     gIssExGlobalInfo.IssFilterShadowPoolId
#define ISS_PRIORITY_FILTER_POOL_ID      gIssExGlobalInfo.IssPriorityFilterPoolId
#define ISS_ACL_MSG_QUEUE_POOL_ID     gIssExGlobalInfo.AclQueuePoolId
#define ISS_ACL_MSG_QUE_ID      gIssExGlobalInfo.AclQueId
#define ISS_REDIR_PORT_MSK_POOL_ID      gIssExGlobalInfo.IssRedirectPortMaskPoolId
#define ISS_REDIR_IFPORT_GRP_POOL_ID    gIssExGlobalInfo.IssRedirectIfPortGroupPoolId

/* Filter List */
#define ISS_L2FILTER_LIST               gIssExGlobalInfo.IssL2FilterListHead
#define ISS_L2FILTER_LIST_HEAD          (&ISS_L2FILTER_LIST)->Tail
#define ISS_IS_L2FILTER_ID_VALID(ISSId) \
        (((ISSId <= ISS_MIN_FILTER_ID) || \
        (ISSId > ISS_MAX_FILTER_ID)) ? ISS_FALSE : ISS_TRUE)

#define ISS_L3FILTER_LIST               gIssExGlobalInfo.IssL3FilterListHead
#define ISS_L3FILTER_LIST_HEAD          (&ISS_L3FILTER_LIST)->Tail
#define ISS_IS_L3FILTER_ID_VALID(ISSId) \
        (((ISSId <= ISS_MIN_FILTER_ID) || \
        (ISSId > ISS_MAX_FILTER_ID)) ? ISS_FALSE : ISS_TRUE)

#define ISS_L2_FILTER_ID_SCAN(u4L2FilterId) \
    for (u4L2FilterId = 1; \
         u4L2FilterId <= ISS_MAX_FILTER_ID; u4L2FilterId++)

#define ISS_L3_FILTER_ID_SCAN(u4L3FilterId) \
    for (u4L3FilterId = 1; \
         u4L3FilterId <= ISS_MAX_FILTER_ID; u4L3FilterId++)

#define ISS_UDB_FILTER_ID_SCAN(u4UdbFilterId) \
    for (u4UdbFilterId = 1; \
         u4UdbFilterId <= ISS_MAX_FILTER_ID; u4UdbFilterId++)

/* Redirect Group */
#define ISS_UDB_FILTER_TABLE_LIST        gIssExGlobalInfo.IssUdbFilterTableHead

#define ISS_IS_REDIRECT_GRP_ID_VALID(ISSId) \
        (((ISSId <= ISS_MIN_REDIRECT_GRP_ID) || \
        (ISSId > ISS_MAX_REDIRECT_GRP_ID)) ? ISS_FALSE : ISS_TRUE)

#define ISS_IS_UDB_FILTER_ID_VALID(ISSId) \
        (((ISSId == ISS_MIN_REDIRECT_GRP_ID) || \
        (ISSId > ISS_MAX_REDIRECT_GRP_ID)) ? ISS_FALSE : ISS_TRUE)


#define ISS_L2FILTERENTRY_MEMBLK_COUNT  ISS_MAX_L2_FILTERS
#define ISS_L3FILTERENTRY_MEMBLK_COUNT  ISS_MAX_L3_FILTERS
#define ISS_L3FILTERENTRY_MEMBLK_COUNT  ISS_MAX_L3_FILTERS

#define ISS_UDB_FILTER_TABLE_MEMBLK_COUNT ISS_MAX_UDB_FILTERS
#define ISS_UDB_FILTERENTRY_MEMBLK_COUNT  ISS_MAX_UDB_FILTERS

#define ISS_PRIORITY_FILTER_TABLE_MEMBLK_COUNT  ISS_MAX_FILTER_PRIORITY
#define ISS_REDIRECT_INTF_GRP_MEMBLK_COUNT  1

#define ISS_RATEENTRY_MEMBLK_COUNT      (BRG_MAX_PHY_PLUS_LOG_PORTS + 1)
#define ISS_REDIR_PORTMASK_MEMBLK_CNT   1
#define ISS_REDIR_IFPORT_MEMBLK_CNT     1

#define ISS_RATEENTRY_MEMBLK_SIZE       sizeof(tIssRateCtrlEntry)

#define ISS_L2FILTERENTRY_MEMBLK_SIZE   sizeof(tIssL2FilterEntry)

#define ISS_PRIORITY_FILTER_TABLE_MEMBLK_SIZE  sizeof(tIssPriorityFilterTable)

#define ISS_REDIRECT_INTF_GRP_MEMBLK_SIZE  sizeof (tIssRedirectIntfGrpTable) * ISS_MAX_REDIRECT_GRP_ID

#define ISS_UDB_FILTER_TABLE_MEMBLK_SIZE sizeof (tIssUserDefinedFilterTable)

#define ISS_UDB_FILTER_ENTRY_MEMBLK_SIZE   sizeof (tIssUDBFilterEntry)

#define ISS_REDIRECT_PORTMASK_MEMBLK_SIZE   sizeof (tIssRedirectPortMaskAndIndex) * ISS_MAX_REDIRECT_GRP_SIZE

#define ISS_REDIRECT_IFPORT_MEMBLK_SIZE   sizeof (tIssRedirectIfPortGroup) * ISS_MAX_REDIRECT_GRP_SIZE

#define  ISS_CREATE_RATEENTRY_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_CREATE_PRIORITYFILTERENTRY_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_CREATE_UDB_FILTER_ENTRY_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_CREATE_PRIORITY_FILTER_TABLE_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_CREATE_REDIRECT_INTF_GRP_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_CREATE_L2FILTERENTRY_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_CREATE_L3FILTERENTRY_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_CREATE_UDB_FILTER_TABLE_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_CREATE_ACL_MSG_QUEUE_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_CREATE_FILTER_SHADOW_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_CREATE_REDIR_PORT_MSK_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_CREATE_REDIR_IFPORT_GRP_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_DELETE_RATEENTRY_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_DELETE_L2FILTERENTRY_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_DELETE_L3FILTERENTRY_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_DELETE_UDB_FILTER_TABLE_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_DELETE_REDIRECT_INTF_GRP_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_DELETE_ACL_MSG_QUEUE_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_DELETE_FILTER_SHADOW_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_DELETE_PRIORITY_FILTER_TABLE_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_DELETE_REDIR_PORT_MSK_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_DELETE_REDIR_IFPORT_GRP_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_RATEENTRY_ALLOC_MEM_BLOCK(pu1Block) \
         (pu1Block = \
          (tIssRateCtrlEntry *)(MemAllocMemBlk (ISS_RATEENTRY_POOL_ID)))

#define  ISS_PRIORITY_FILTER_ALLOC_MEM_BLOCK(pu1Block) \
         (pu1Block = \
          (tIssPriorityFilterTable *)(MemAllocMemBlk (ISS_PRIORITY_FILTER_POOL_ID)))

#define  ISS_REDIRECT_INTF_GRP_ALLOC_MEM_BLOCK(pu1Block) \
         (pu1Block = \
          (tIssRedirectIntfGrpTable *)(MemAllocMemBlk (ISS_REDIRECT_INTF_GRP_POOL_ID)))

#define  ISS_L2FILTERENTRY_ALLOC_MEM_BLOCK(pu1Block) \
         (pu1Block = \
          (tIssL2FilterEntry *)(MemAllocMemBlk (ISS_L2FILTERENTRY_POOL_ID)))

#define  ISS_L3FILTERENTRY_ALLOC_MEM_BLOCK(pu1Block) \
         (pu1Block = \
          (tIssL3FilterEntry *)(MemAllocMemBlk (ISS_L3FILTERENTRY_POOL_ID)))

#define  ISS_UDB_FILTER_TABLE_ALLOC_MEM_BLOCK(pu1Block) \
         (pu1Block = \
          (tIssUserDefinedFilterTable *)(MemAllocMemBlk (ISS_UDB_FILTER_TABLE_POOL_ID)))

#define  ISS_UDB_FILTER_ENTRY_ALLOC_MEM_BLOCK(pu1Block) \
         (pu1Block = \
          (tIssUDBFilterEntry *)(MemAllocMemBlk (ISS_UDB_FILTERENTRY_POOL_ID)))

#define  ISS_ACL_MSG_QUEUE_ALLOC_MEM_BLOCK(pu1Block) \
         (pu1Block = \
          (tAclQMsg *)(MemAllocMemBlk (ISS_ACL_MSG_QUEUE_POOL_ID)))

#define  ISS_FILTER_SHADOW_ALLOC_MEM_BLOCK(pu1Block) \
         (pu1Block = \
          (tIssFilterShadowTable *)(MemAllocMemBlk (ISS_FILTER_SHADOW_POOL_ID)))

#define  ISS_REDIR_PORT_MSK_ALLOC_MEM_BLOCK(pu1Block) \
         (pu1Block = \
          (tIssRedirectPortMaskAndIndex *)(MemAllocMemBlk (ISS_REDIR_PORT_MSK_POOL_ID)))

#define  ISS_REDIR_IFPORT_GRP_ALLOC_MEM_BLOCK(pu1Block) \
         (pu1Block = \
          (tIssRedirectIfPortGroup *)(MemAllocMemBlk (ISS_REDIR_IFPORT_GRP_POOL_ID)))

#define  ISS_RATEENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_RATEENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define  ISS_PRIORITY_FILTER_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_PRIORITY_FILTER_POOL_ID, (UINT1 *)pu1Msg)

#define  ISS_REDIRECT_INTF_GRP_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_REDIRECT_INTF_GRP_POOL_ID, (UINT1 *)pu1Msg)

#define  ISS_L2FILTERENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_L2FILTERENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define  ISS_L3FILTERENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_L3FILTERENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define  ISS_UDB_FILTERENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_UDB_FILTERENTRY_POOL_ID, (UINT1 *)pu1Msg)


#define  ISS_UDB_FILTERTABLE_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_UDB_FILTER_TABLE_POOL_ID, (UINT1 *)pu1Msg)

#define  ISS_ACL_MSG_QUEUE_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_ACL_MSG_QUEUE_POOL_ID, (UINT1 *)pu1Msg)

#define  ISS_FILTER_SHADOW_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_FILTER_SHADOW_POOL_ID, (UINT1 *)pu1Msg)

#define  ISS_REDIR_PORT_MSK_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_REDIR_PORT_MSK_POOL_ID, (UINT1 *)pu1Msg)

#define  ISS_REDIR_IFPORT_GRP_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_REDIR_IFPORT_GRP_POOL_ID, (UINT1 *)pu1Msg)
/**************************************************
 *Traffic Separtion and Protection Control MACROS *
 **************************************************/
#define  ACL_MIN_L2_SYSTEM_PROTOCOLS                    (1)
#define  ACL_MIN_L3_SYSTEM_PROTOCOLS                    (1)
#endif  /* _ISSEXMACR_H */
