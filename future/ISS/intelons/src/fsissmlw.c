/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissmlw.c,v 1.1.1.1 2015/03/04 10:38:40 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "iss.h"
# include  "issexinc.h"
# include  "fsissmlw.h"
# include  "fsisselw.h"
# include  "isscli.h"
# include  "aclcli.h"
/* LOW LEVEL Routines for Table : IssMetroL2FilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssMetroL2FilterTable
 Input       :  The Indices
                IssL2FilterNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssMetroL2FilterTable (INT4 i4IssL2FilterNo)
{
    if (ISS_IS_L2FILTER_ID_VALID (i4IssL2FilterNo) != ISS_TRUE)
    {
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssMetroL2FilterTable
 Input       :  The Indices
                IssL2FilterNo
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssMetroL2FilterTable (INT4 *pi4IssL2FilterNo)
{
    if ((IssExtSnmpLowGetFirstValidL2FilterTableIndex (pi4IssL2FilterNo)) ==
        ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssMetroL2FilterTable
 Input       :  The Indices
                IssL2FilterNo
                nextIssL2FilterNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssMetroL2FilterTable (INT4 i4IssL2FilterNo,
                                      INT4 *pi4IssL2FilterNo)
{
    if (i4IssL2FilterNo < 0)
    {
        return SNMP_FAILURE;
    }

    if ((IssExtSnmpLowGetNextValidL2FilterTableIndex
         (i4IssL2FilterNo, pi4IssL2FilterNo)) == ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIssMetroL2FilterOuterEtherType
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                retValIssMetroL2FilterOuterEtherType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssMetroL2FilterOuterEtherType (INT4 i4IssL2FilterNo,
                                      INT4 *pi4RetValIssL2FilterOuterEtherType)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssExtGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        *pi4RetValIssL2FilterOuterEtherType =
            (INT4) pIssL2FilterEntry->u2OuterEtherType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssMetroL2FilterSVlanId
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                retValIssMetroL2FilterSVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssMetroL2FilterSVlanId (INT4 i4IssL2FilterNo,
                               INT4 *pi4RetValIssL2FilterServiceVlanId)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssExtGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        *pi4RetValIssL2FilterServiceVlanId =
            (INT4) pIssL2FilterEntry->u4IssL2FilterServiceVlanId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssMetroL2FilterSVlanPriority
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                retValIssMetroL2FilterSVlanPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssMetroL2FilterSVlanPriority (INT4 i4IssL2FilterNo,
                                     INT4 *pi4RetValIssL2FilterSVlanPriority)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssExtGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        *pi4RetValIssL2FilterSVlanPriority =
            (INT4) pIssL2FilterEntry->i1IssL2FilterSVlanPriority;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssMetroL2FilterCVlanPriority
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                retValIssMetroL2FilterCVlanPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssMetroL2FilterCVlanPriority (INT4 i4IssL2FilterNo,
                                     INT4
                                     *pi4RetValIssL2FilterCustomerVlanPriority)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssExtGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        *pi4RetValIssL2FilterCustomerVlanPriority =
            (INT4) pIssL2FilterEntry->i1IssL2FilterCVlanPriority;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssMetroL2FilterPacketTagType
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                retValIssMetroL2FilterPacketTagType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssMetroL2FilterPacketTagType (INT4 i4IssL2FilterNo,
                                     INT4 *pi4RetValIssL2FilterPacketTagType)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssExtGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        *pi4RetValIssL2FilterPacketTagType =
            (INT4) pIssL2FilterEntry->u1IssL2FilterTagType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIssMetroL2FilterOuterEtherType
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                setValIssMetroL2FilterOuterEtherType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssMetroL2FilterOuterEtherType (INT4 i4IssL2FilterNo,
                                      INT4 i4SetValIssL2FilterOuterEtherType)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssExtGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        pIssL2FilterEntry->u2OuterEtherType = (UINT2)
            i4SetValIssL2FilterOuterEtherType;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssMetroL2FilterSVlanId
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                setValIssMetroL2FilterSVlanId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssMetroL2FilterSVlanId (INT4 i4IssL2FilterNo,
                               INT4 i4SetValIssL2FilterServiceVlanId)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssExtGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        pIssL2FilterEntry->u4IssL2FilterServiceVlanId =
            (UINT4) i4SetValIssL2FilterServiceVlanId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssMetroL2FilterSVlanPriority
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                setValIssMetroL2FilterSVlanPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssMetroL2FilterSVlanPriority (INT4 i4IssL2FilterNo,
                                     INT4
                                     i4SetValIssL2FilterServiceVlanPriority)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssExtGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        pIssL2FilterEntry->i1IssL2FilterSVlanPriority =
            (INT1) i4SetValIssL2FilterServiceVlanPriority;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssMetroL2FilterCVlanPriority
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                setValIssMetroL2FilterCVlanPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssMetroL2FilterCVlanPriority (INT4 i4IssL2FilterNo,
                                     INT4
                                     i4SetValIssL2FilterCustomerVlanPriority)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssExtGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        pIssL2FilterEntry->i1IssL2FilterCVlanPriority =
            (INT1) i4SetValIssL2FilterCustomerVlanPriority;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssMetroL2FilterPacketTagType
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                setValIssMetroL2FilterPacketTagType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssMetroL2FilterPacketTagType (INT4 i4IssL2FilterNo,
                                     INT4 i4SetValIssL2FilterPacketTagType)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    pIssL2FilterEntry = IssExtGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry != NULL)
    {
        pIssL2FilterEntry->u1IssL2FilterTagType =
            (UINT1) i4SetValIssL2FilterPacketTagType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IssMetroL2FilterOuterEtherType
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                testValIssMetroL2FilterOuterEtherType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssMetroL2FilterOuterEtherType (UINT4 *pu4ErrorCode,
                                         INT4 i4IssL2FilterNo,
                                         INT4
                                         i4TestValIssL2FilterOuterEtherType)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssL2FilterOuterEtherType < 0) ||
        (i4TestValIssL2FilterOuterEtherType > 65535))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIssL2FilterEntry = IssExtGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssMetroL2FilterSVlanId
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                testValIssMetroL2FilterSVlanId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssMetroL2FilterSVlanId (UINT4 *pu4ErrorCode, INT4 i4IssL2FilterNo,
                                  INT4 i4TestValIssL2FilterServiceVlanId)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssL2FilterServiceVlanId < 0) ||
        (i4TestValIssL2FilterServiceVlanId > 4094))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIssL2FilterEntry = IssExtGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssL2FilterEntry != NULL &&
        pIssL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssMetroL2FilterSVlanPriority
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                testValIssMetroL2FilterSVlanPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssMetroL2FilterSVlanPriority (UINT4 *pu4ErrorCode,
                                        INT4 i4IssL2FilterNo,
                                        INT4
                                        i4TestValIssL2FilterServiceVlanPriority)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssL2FilterServiceVlanPriority <
         ISS_DEFAULT_VLAN_PRIORITY) ||
        (i4TestValIssL2FilterServiceVlanPriority > 7))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIssL2FilterEntry = IssExtGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssMetroL2FilterCVlanPriority
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                testValIssMetroL2FilterCVlanPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssMetroL2FilterCVlanPriority (UINT4 *pu4ErrorCode,
                                        INT4 i4IssL2FilterNo,
                                        INT4
                                        i4TestValIssL2FilterCustomerVlanPriority)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssL2FilterCustomerVlanPriority <
         ISS_DEFAULT_VLAN_PRIORITY) ||
        (i4TestValIssL2FilterCustomerVlanPriority > 7))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIssL2FilterEntry = IssExtGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssMetroL2FilterPacketTagType
 Input       :  The Indices
                IssL2FilterNo

                The Object 
                testValIssMetroL2FilterPacketTagType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssMetroL2FilterPacketTagType (UINT4 *pu4ErrorCode,
                                        INT4 i4IssL2FilterNo,
                                        INT4 i4TestValIssL2FilterPacketTagType)
{
    tIssL2FilterEntry  *pIssL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssL2FilterPacketTagType != ISS_FILTER_SINGLE_TAG) &&
        (i4TestValIssL2FilterPacketTagType != ISS_FILTER_DOUBLE_TAG))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIssL2FilterEntry = IssExtGetL2FilterEntry (i4IssL2FilterNo);

    if (pIssL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssMetroL2FilterTable
 Input       :  The Indices
                IssL2FilterNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssMetroL2FilterTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IssMetroL3FilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssMetroL3FilterTable
 Input       :  The Indices
                IssL3FilterNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssMetroL3FilterTable (INT4 i4IssL3FilterNo)
{
       UNUSED_PARAM(i4IssL3FilterNo);
        return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssMetroL3FilterTable
 Input       :  The Indices
                IssL3FilterNo
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssMetroL3FilterTable (INT4 *pi4IssL3FilterNo)
{
    UNUSED_PARAM(pi4IssL3FilterNo);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexIssMetroL3FilterTable
 Input       :  The Indices
                IssL3FilterNo
                nextIssL3FilterNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssMetroL3FilterTable (INT4 i4IssL3FilterNo,
                                      INT4 *pi4IssL3FilterNo)
{
    UNUSED_PARAM(i4IssL3FilterNo);
    UNUSED_PARAM(pi4IssL3FilterNo);
       return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIssMetroL3FilterSVlanId
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                retValIssMetroL3FilterSVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssMetroL3FilterSVlanId (INT4 i4IssL3FilterNo,
                               INT4 *pi4RetValIssL3FilterServiceVlanId)
{
    UNUSED_PARAM(i4IssL3FilterNo);
    UNUSED_PARAM(pi4RetValIssL3FilterServiceVlanId);
        return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssMetroL3FilterSVlanPriority
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                retValIssMetroL3FilterSVlanPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssMetroL3FilterSVlanPriority (INT4 i4IssL3FilterNo,
                                     INT4
                                     *pi4RetValIssL3FilterServiceVlanPriority)
{
    UNUSED_PARAM(i4IssL3FilterNo);
    UNUSED_PARAM(pi4RetValIssL3FilterServiceVlanPriority);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssMetroL3FilterCVlanId
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                retValIssMetroL3FilterCVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssMetroL3FilterCVlanId (INT4 i4IssL3FilterNo,
                               INT4 *pi4RetValIssL3FilterCustomerVlanId)
{
    UNUSED_PARAM(i4IssL3FilterNo);
    UNUSED_PARAM(pi4RetValIssL3FilterCustomerVlanId);
        return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssMetroL3FilterCVlanPriority
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                retValIssMetroL3FilterCVlanPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssMetroL3FilterCVlanPriority (INT4 i4IssL3FilterNo,
                                     INT4
                                     *pi4RetValIssL3FilterCustomerVlanPriority)
{
    UNUSED_PARAM(i4IssL3FilterNo);
    UNUSED_PARAM(pi4RetValIssL3FilterCustomerVlanPriority);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssMetroL3FilterPacketTagType
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                retValIssMetroL3FilterPacketTagType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssMetroL3FilterPacketTagType (INT4 i4IssL3FilterNo,
                                     INT4 *pi4RetValIssL3FilterPacketTagType)
{
    UNUSED_PARAM(i4IssL3FilterNo);
    UNUSED_PARAM(pi4RetValIssL3FilterPacketTagType);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIssMetroL3FilterSVlanId
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                setValIssMetroL3FilterSVlanId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssMetroL3FilterSVlanId (INT4 i4IssL3FilterNo,
                               INT4 i4SetValIssL3FilterServiceVlanId)
{
    UNUSED_PARAM(i4IssL3FilterNo);
    UNUSED_PARAM(i4SetValIssL3FilterServiceVlanId);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssMetroL3FilterSVlanPriority
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                setValIssMetroL3FilterSVlanPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssMetroL3FilterSVlanPriority (INT4 i4IssL3FilterNo,
                                     INT4
                                     i4SetValIssL3FilterServiceVlanPriority)
{
    UNUSED_PARAM(i4IssL3FilterNo);
    UNUSED_PARAM(i4SetValIssL3FilterServiceVlanPriority);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssMetroL3FilterCVlanId
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                setValIssMetroL3FilterCVlanId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssMetroL3FilterCVlanId (INT4 i4IssL3FilterNo,
                               INT4 i4SetValIssL3FilterCustomerVlanId)
{
    UNUSED_PARAM(i4IssL3FilterNo);
    UNUSED_PARAM(i4SetValIssL3FilterCustomerVlanId);
        return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssMetroL3FilterCVlanPriority
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                setValIssMetroL3FilterCVlanPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssMetroL3FilterCVlanPriority (INT4 i4IssL3FilterNo,
                                     INT4
                                     i4SetValIssL3FilterCustomerVlanPriority)
{
    UNUSED_PARAM(i4IssL3FilterNo);
    UNUSED_PARAM(i4SetValIssL3FilterCustomerVlanPriority);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssMetroL3FilterPacketTagType
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                setValIssMetroL3FilterPacketTagType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssMetroL3FilterPacketTagType (INT4 i4IssL3FilterNo,
                                     INT4 i4SetValIssL3FilterPacketTagType)
{
    UNUSED_PARAM(i4IssL3FilterNo);
    UNUSED_PARAM(i4SetValIssL3FilterPacketTagType);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IssMetroL3FilterSVlanId
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                testValIssMetroL3FilterSVlanId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssMetroL3FilterSVlanId (UINT4 *pu4ErrorCode, INT4 i4IssL3FilterNo,
                                  INT4 i4TestValIssL3FilterServiceVlanId)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(i4IssL3FilterNo);
    UNUSED_PARAM(i4TestValIssL3FilterServiceVlanId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssMetroL3FilterSVlanPriority
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                testValIssMetroL3FilterSVlanPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssMetroL3FilterSVlanPriority (UINT4 *pu4ErrorCode,
                                        INT4 i4IssL3FilterNo,
                                        INT4
                                        i4TestValIssL3FilterServiceVlanPriority)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(i4IssL3FilterNo);
    UNUSED_PARAM(i4TestValIssL3FilterServiceVlanPriority);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssMetroL3FilterCVlanId
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                testValIssMetroL3FilterCVlanId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssMetroL3FilterCVlanId (UINT4 *pu4ErrorCode,
                                  INT4 i4IssL3FilterNo,
                                  INT4 i4TestValIssL3FilterCustomerVlanId)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(i4IssL3FilterNo);
    UNUSED_PARAM(i4TestValIssL3FilterCustomerVlanId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssMetroL3FilterCVlanPriority
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                testValIssMetroL3FilterCVlanPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssMetroL3FilterCVlanPriority (UINT4 *pu4ErrorCode,
                                        INT4 i4IssL3FilterNo,
                                        INT4
                                        i4TestValIssL3FilterCustomerVlanPriority)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(i4IssL3FilterNo);
    UNUSED_PARAM(i4TestValIssL3FilterCustomerVlanPriority);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssMetroL3FilterPacketTagType
 Input       :  The Indices
                IssL3FilterNo

                The Object 
                testValIssMetroL3FilterPacketTagType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssMetroL3FilterPacketTagType (UINT4 *pu4ErrorCode,
                                        INT4 i4IssL3FilterNo,
                                        INT4 i4TestValIssL3FilterPacketTagType)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(i4IssL3FilterNo);
    UNUSED_PARAM(i4TestValIssL3FilterPacketTagType);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssMetroL3FilterTable
 Input       :  The Indices
                IssL3FilterNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssMetroL3FilterTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  fsissmlw.c                     */
/*-----------------------------------------------------------------------*/
