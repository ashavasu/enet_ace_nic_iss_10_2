/************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                 */
/* Licensee Aricent Inc., 2004-2005       */
/* $Id: aclcli.c,v 1.2 2015/04/08 08:23:28 siva Exp $*/
/*                                                          */
/*  FILE NAME             : aclcli.c                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                 */
/*  SUBSYSTEM NAME        : ISS                             */
/*  MODULE NAME           : CLI                             */
/*  LANGUAGE              : C                               */
/*  TARGET ENVIRONMENT    :                                 */
/*  DATE OF FIRST RELEASE :                                 */
/*  AUTHOR                : Aricent Inc.                 */
/*  DESCRIPTION           : This file contains CLI routines */
/*                          related to system commands      */
/*                                                          */
/************************************************************/
#ifndef __ACLCLI_C__
#define __ACLCLI_C__

#include "lr.h"
#include "issexinc.h"
#include "fsissewr.h"
#include "fsisselw.h"
#include "fsissalw.h"
#include "aclcli.h"
#include "isscli.h"
#include "aclmcli.h"
#include "fsissecli.h"
#include "fsissacli.h"
#include "fsissewr.h"
#include "fssnmp.h"
#include "ipvx.h"

/***************************************************************/
/*  Function Name   : cli_process_acl_cmd                      */
/*  Description     : This function servers as the handler for */
/*                    all system related CLI commands          */
/*  Input(s)        :                                          */
/*                    CliHandle - CLI Handle                   */
/*                    u4Command - Command given by user        */
/*                    Variable set of inputs depending on      */
/*                    the user command                         */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : None                                     */
/***************************************************************/
INT4
cli_process_acl_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4Command);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclCreateIPFilter                                  */
/*                                                                           */
/*     DESCRIPTION      : This function creates an IP ACL filter and enters  */
/*                        the corresponding configuration mode               */
/*                                                                           */
/*     INPUT            : u4Type  - Standard /Extended ACL                   */
/*                        i4Filterno - IP ACL number                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclCreateIPFilter (tCliHandle CliHandle, UINT4 u4Type, INT4 i4FilterNo)
{

    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4Type);
    UNUSED_PARAM (i4FilterNo);

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclDestroyIPFilter                                 */
/*                                                                           */
/*     DESCRIPTION      : This function destroys a IP ACL filter.            */
/*                                                                           */
/*     INPUT            : i4Filterno - IP ACL number                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclDestroyIPFilter (tCliHandle CliHandle, INT4 i4FilterNo)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4FilterNo);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* FUNCTION NAME    : AclCreateIP6Filter                                     */
/*                                                                           */
/* DESCRIPTION      : This function creates an IPv6 ACL filter and enters    */
/*                    the corresponding configuration mode                   */
/*                                                                           */
/* INPUT            : i4Filterno - IP ACL number                             */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : Success/Failure                                        */
/*****************************************************************************/
INT4
AclCreateIP6Filter (tCliHandle CliHandle, INT4 i4FilterNo)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4FilterNo);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* FUNCTION NAME    : AclDestroyIP6Filter                                    */
/*                                                                           */
/* DESCRIPTION      : This function deletes an IPv6 ACL filter.              */
/*                                                                           */
/* INPUT            : i4Filterno - IP ACL number                             */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : Success/Failure                                        */
/*****************************************************************************/
INT4
AclDestroyIP6Filter (tCliHandle CliHandle, INT4 i4FilterNo)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4FilterNo);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclCreateMacFilter                                 */
/*                                                                           */
/*     DESCRIPTION      : This function creates an MAC ACL filter and enters */
/*                        the corresponding configuration mode               */
/*                                                                           */
/*     INPUT            : i4Filterno - MAC ACL number                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclCreateMacFilter (tCliHandle CliHandle, INT4 i4FilterNo)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4FilterNo);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclDestroyMacFilter                                */
/*                                                                           */
/*     DESCRIPTION      : This function destroys a MAC ACL filter and enters */
/*                        the corresponding configuration mode               */
/*                                                                           */
/*     INPUT            : i4Filterno - MAC ACL number                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclDestroyMacFilter (tCliHandle CliHandle, INT4 i4FilterNo)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4FilterNo);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclStdIpFilterConfig                               */
/*                                                                           */
/*     DESCRIPTION      : This function configures a standard IP ACL filter  */
/*                         and enters the corresponding configuration mode   */
/*                                                                           */
/*     INPUT            :  u4Action -Permit/Deny                             */
/*                         u4SrcType - any/source network/ source host       */
/*                         u4SrcIp - Source IP Address                       */
/*                         u4SrcMask - Source IP Mask                        */
/*                         u4DestType - any/dest network/dest host           */
/*                         u4DestIp - Dest IP Address                        */
/*                         u4DestMask - Dest IP Mask                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclStdIpFilterConfig (tCliHandle CliHandle, INT4 i4Action, UINT4 u4SrcType,
                      UINT4 u4SrcIpAddr, UINT4 u4SrcMask, UINT4 u4DestType,
                      UINT4 u4DestIpAddr, UINT4 u4DestMask, UINT4 u4SubAction,
                      UINT2 u2VlanActonId, UINT4 u4Priority)
{

    UNUSED_PARAM (u4SubAction);
    UNUSED_PARAM (u2VlanActonId);
    UNUSED_PARAM (u4Priority);
    UNUSED_PARAM(CliHandle);
    UNUSED_PARAM(i4Action);
    UNUSED_PARAM(u4SrcType);
    UNUSED_PARAM(u4SrcIpAddr);
    UNUSED_PARAM(u4SrcMask);
    UNUSED_PARAM(u4DestType);
    UNUSED_PARAM(u4DestIpAddr);
    UNUSED_PARAM(u4DestMask);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* FUNCTION NAME    : AclIp6FilterConfig                                     */
/*                                                                           */
/* DESCRIPTION      : This function configures a IPv6 ACL filter.            */
/*                                                                           */
/* INPUT            : CliHandle     - CLI Handle                             */
/*                    i4Protocol    - Protocol Number                        */
/*                    u4SrcType     - Source Type (ACL_ANY/ ACL_HOST_IP/     */
/*                                                 ACL_HOST_IP_MASK)         */
/*                    SrcIp6Addr    - Source IPv6 Address                    */
/*                    u4SrcPrefixLen- Source Prefix length                   */
/*                    u4DestType    - Destination Type(ACL_ANY/ ACL_HOST_IP/ */
/*                                                     ACL_HOST_IP_MASK)     */
/*                    DstIp6Addr    - Destination IPv6 Address               */
/*                    u4DstPrefixLen- Destination Prefix Length              */
/*                    i4Dscp        - IPv6 DSCP value                        */
/*                    u4FlowId      - IPv6 Flow label                        */
/*                    i4Priority    - Filter Priority (1-7)                  */
/*                    i4Action      - ISS_ALLOW / ISS_DENY                   */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : Success/Failure                                        */
/*****************************************************************************/
INT4
AclIp6FilterConfig (tCliHandle CliHandle, INT4 i4Protocol, UINT4 u4SrcType,
                    tIp6Addr SrcIp6Addr, UINT4 u4SrcPrefixLen, UINT4 u4DestType,
                    tIp6Addr DstIp6Addr, UINT4 u4DstPrefixLen, INT4 i4Dscp,
                    UINT4 u4FlowId, INT4 i4Priority, INT4 i4Action)
{
    UNUSED_PARAM(CliHandle);
    UNUSED_PARAM(i4Protocol);
    UNUSED_PARAM(u4SrcType);
    UNUSED_PARAM(SrcIp6Addr);
    UNUSED_PARAM(u4SrcPrefixLen);
    UNUSED_PARAM(u4DestType);
    UNUSED_PARAM(DstIp6Addr);
    UNUSED_PARAM(u4DstPrefixLen);
    UNUSED_PARAM(i4Dscp);
    UNUSED_PARAM(u4FlowId);
    UNUSED_PARAM(i4Priority);
    UNUSED_PARAM(i4Action);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME    : AclIp6TCPUDPFilterConfig                               */
/*                                                                           */
/* DESCRIPTION      : This function configures a IPv6 ACL filter for TCP/UDP.*/
/*                                                                           */
/* INPUT            : CliHandle     - CLI Handle                             */
/*                    i4Protocol    - Protocol Number                        */
/*                    u4SrcType     - Source Type (ACL_ANY/ ACL_HOST_IP/     */
/*                                                 ACL_HOST_IP_MASK)         */
/*                    SrcIp6Addr    - Source IPv6 Address                    */
/*                    u4SrcPrefixLen- Source Prefix length                   */
/*                    u4SrcPortFlag - Source ports given or not              */
/*                    u4SrcMinPort  - Source port allowed                    */
/*                    u4SrcMaxPort  - Max Source port allowed                */
/*                    u4DestType    - Destination Type(ACL_ANY/ ACL_HOST_IP/ */
/*                                                     ACL_HOST_IP_MASK)     */
/*                    DstIp6Addr    - Destination IPv6 Address               */
/*                    u4DstPrefixLen- Destination Prefix Length              */
/*                    u4DstPortFlag - Destination ports given or not         */
/*                    u4DstMinPort  - Destination port allowed               */
/*                    u4DstMaxPort  - Max Destination port allowed           */
/*                    u4BitType     - ACK / RST                              */
/*                    i4Tos         - TCP TOS configuration                  */
/*                    i4Dscp        - IPv6 DSCP value                        */
/*                    u4FlowId      - IPv6 Flow label                        */
/*                    i4Priority    - Filter Priority (1-7)                  */
/*                    i4Action      - ISS_ALLOW / ISS_DENY                   */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : Success/Failure                                        */
/*****************************************************************************/
INT4
AclIp6TCPUDPFilterConfig (tCliHandle CliHandle, INT4 i4Protocol,
                          UINT4 u4SrcType, tIp6Addr SrcIp6Addr,
                          UINT4 u4SrcPrefixLen, UINT4 u4SrcPortFlag,
                          UINT4 u4SrcMinPort, UINT4 u4SrcMaxPort,
                          UINT4 u4DstType, tIp6Addr DstIp6Addr,
                          UINT4 u4DstPrefixLen, UINT4 u4DstPortFlag,
                          UINT4 u4DstMinPort, UINT4 u4DstMaxPort,
                          UINT4 u4BitType, INT4 i4Tos, INT4 i4Dscp,
                          UINT4 u4FlowId, INT4 i4Priority, INT4 i4Action)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4Protocol);
    UNUSED_PARAM (u4SrcType);
    UNUSED_PARAM (SrcIp6Addr);
    UNUSED_PARAM (u4SrcPrefixLen);
    UNUSED_PARAM (u4SrcPortFlag);
    UNUSED_PARAM (u4SrcMinPort);
    UNUSED_PARAM (u4SrcMaxPort);
    UNUSED_PARAM (u4DstType);
    UNUSED_PARAM (DstIp6Addr);
    UNUSED_PARAM (u4DstPrefixLen);
    UNUSED_PARAM (u4DstPortFlag);
    UNUSED_PARAM (u4DstMinPort);
    UNUSED_PARAM (u4DstMaxPort);
    UNUSED_PARAM (u4BitType);
    UNUSED_PARAM (i4Tos);
    UNUSED_PARAM (i4Dscp);
    UNUSED_PARAM (u4FlowId);
    UNUSED_PARAM (i4Priority);
    UNUSED_PARAM (i4Action);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* FUNCTION NAME    : AclIp6IcmpFilterConfig                                 */
/*                                                                           */
/* DESCRIPTION      : This function configures a IPv6 ACL filter for ICMP6.  */
/*                                                                           */
/* INPUT            : CliHandle     - CLI Handle                             */
/*                    i4Protocol    - Protocol Number                        */
/*                    u4SrcType     - Source Type (ACL_ANY/ ACL_HOST_IP/     */
/*                                                 ACL_HOST_IP_MASK)         */
/*                    SrcIp6Addr    - Source IPv6 Address                    */
/*                    u4SrcPrefixLen- Source Prefix length                   */
/*                    u4DstType     - Destination Type(ACL_ANY/ ACL_HOST_IP/ */
/*                                                     ACL_HOST_IP_MASK)     */
/*                    DstIp6Addr    - Destination IPv6 Address               */
/*                    u4DstPrefixLen- Destination Prefix Length              */
/*                    i4MsgType     - ICMP6 Message Type                     */
/*                    i4MsgCode     - ICMP6 Message Code                     */
/*                    i4Dscp        - IPv6 DSCP value                        */
/*                    u4FlowId      - IPv6 Flow label                        */
/*                    i4Priority    - Filter Priority (1-7)                  */
/*                    i4Action      - ISS_ALLOW / ISS_DENY                   */
/*                                                                           */
/* OUTPUT           : NONE                                                   */
/*                                                                           */
/* RETURNS          : Success/Failure                                        */
/*****************************************************************************/
INT4
AclIp6IcmpFilterConfig (tCliHandle CliHandle, INT4 i4Protocol, UINT4 u4SrcType,
                        tIp6Addr SrcIp6Addr, UINT4 u4SrcPrefixLen,
                        UINT4 u4DstType, tIp6Addr DstIp6Addr,
                        UINT4 u4DstPrefixLen, INT4 i4MsgType, INT4 i4MsgCode,
                        INT4 i4Dscp, UINT4 u4FlowId, INT4 i4Priority,
                        INT4 i4Action)
{

    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4Protocol);
    UNUSED_PARAM (u4SrcType);
    UNUSED_PARAM (SrcIp6Addr);
    UNUSED_PARAM (u4SrcPrefixLen);
    UNUSED_PARAM (u4DstType);
    UNUSED_PARAM (DstIp6Addr);
    UNUSED_PARAM (u4DstPrefixLen);
    UNUSED_PARAM (i4MsgType);
    UNUSED_PARAM (i4MsgCode);
    UNUSED_PARAM (i4Dscp);
    UNUSED_PARAM (u4FlowId);
    UNUSED_PARAM (i4Priority);
    UNUSED_PARAM (i4Action);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtIpFilterConfig                               */
/*                                                                           */
/*     DESCRIPTION      : This function configures a extended IP ACL filter  */
/*                         and enters the corresponding configuration mode   */
/*                                                                           */
/*     INPUT            :  i4Action -Permit/Deny                             */
/*                         i4Protocol - Protocol value                       */
/*                         u4SrcType - any/source network/ source host       */
/*                         u4SrcIp - Source IP Address                       */
/*                         u4SrcMask - Source IP Mask                        */
/*                         u4DestType - any/dest network/dest host           */
/*                         u4DestIp - Dest IP Address                        */
/*                         u4DestMask - Dest IP Mask                         */
/*                         i4Priority - Filter priority                      */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclExtIpFilterConfig (tCliHandle CliHandle, INT4 i4Action,
                      INT4 i4Protocol, UINT4 u4SrcType,
                      UINT4 u4SrcIpAddr, UINT4 u4SrcMask,
                      UINT4 u4DestType, UINT4 u4DestIpAddr,
                      UINT4 u4DestMask, INT4 i4Tos, INT4 i4Dscp,
                      INT4 i4Priority, UINT4 u4SubAction, UINT2 u2SubActionId)
{

    UNUSED_PARAM (u4SubAction);
    UNUSED_PARAM (u2SubActionId);
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4Action);
    UNUSED_PARAM (i4Protocol);
    UNUSED_PARAM (u4SrcType);
    UNUSED_PARAM (u4SrcIpAddr);
    UNUSED_PARAM (u4SrcMask);
    UNUSED_PARAM (u4DestType);
    UNUSED_PARAM (u4DestIpAddr);
    UNUSED_PARAM (u4DestMask);
    UNUSED_PARAM (i4Tos);
    UNUSED_PARAM (i4Dscp);
    UNUSED_PARAM (i4Priority);
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtPbL3FilterConfig                             */
/*                                                                           */
/*     DESCRIPTION      : This function configures a extended IP ACL filter  */
/*                                                                           */
/*     INPUT            :  i4SVlan  - Service vlan Id                        */
/*                         i4SVlanPrio - Service Vlan Priority               */
/*                         i4CVlan - Cutomer Vlan Id                         */
/*                         i4CVlanPrio - Customer Vlan Priority              */
/*                         i4TagType - Packet Tag type on which the filter   */
/*                                     will be applied                       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclExtPbL3FilterConfig (tCliHandle CliHandle, INT4 i4SVlan, INT4 i4SVlanPrio,
                        INT4 i4CVlan, INT4 i4CVlanPrio, INT4 i4TagType)
{

    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4SVlan);
    UNUSED_PARAM (i4SVlanPrio);
    UNUSED_PARAM (i4CVlan);
    UNUSED_PARAM (i4CVlanPrio);
    UNUSED_PARAM (i4TagType);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtPbL2FilterConfig                             */
/*                                                                           */
/*     DESCRIPTION      : This function configures a extended IP ACL filter  */
/*                                                                           */
/*     INPUT            :  i4SVlan  - Service vlan Id                        */
/*                         i4SVlanPrio - Service Vlan Priority               */
/*                         i4CVlan - Cutomer Vlan Id                         */
/*                         i4CVlanPrio - Customer Vlan Priority              */
/*                         i4TagType - Packet Tag type on which the filter   */
/*                                     will be applied                       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclExtPbL2FilterConfig (tCliHandle CliHandle, INT4 i4OuterEType,
                        INT4 i4SVlan, INT4 i4SVlanPrio,
                        INT4 i4CVlanPrio, INT4 i4TagType)
{

UNUSED_PARAM (CliHandle);
UNUSED_PARAM (i4OuterEType);
UNUSED_PARAM (i4SVlan);
UNUSED_PARAM (i4SVlanPrio);
UNUSED_PARAM (i4CVlanPrio);
UNUSED_PARAM (i4TagType);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtIpFilterTcpUdpConfig                         */
/*                                                                           */
/*     DESCRIPTION      : This function configures a extended IP ACL filter  */
/*                         and enters the corresponding configuration mode   */
/*                                                                           */
/*     INPUT            :  i4Action -Permit/Deny                             */
/*                         i4Protocol - Protocol value                       */
/*                         u4SrcType - any/source network/ source host       */
/*                         u4SrcIp - Source IP Address                       */
/*                         u4SrcMask - Source IP Mask                        */
/*                         u4DestType - any/dest network/dest host           */
/*                         u4DestIp - Dest IP Address                        */
/*                         u4DestMask - Dest IP Mask                         */
/*                         i4Priority - Filter priority                      */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclExtIpFilterTcpUdpConfig (tCliHandle CliHandle, INT4 i4Action,
                            INT4 i4Protocol, UINT4 u4SrcType, UINT4 u4SrcIpAddr,
                            UINT4 u4SrcMask, UINT4 u4SrcPortFlag,
                            UINT4 u4SrcMinPort, UINT4 u4SrcMaxPort,
                            UINT4 u4DestType, UINT4 u4DestIpAddr,
                            UINT4 u4DestMask, UINT4 u4DestPortFlag,
                            UINT4 u4DestMinPort, UINT4 u4DestMaxPort,
                            UINT4 u4BitType, INT4 i4Tos, INT4 i4Dscp,
                            INT4 i4Priority, UINT4 u4SubAction,
                            UINT2 u2SubActionId)
{


    UNUSED_PARAM (u4SrcPortFlag);
    UNUSED_PARAM (u4DestPortFlag);
    UNUSED_PARAM (u4SubAction);
    UNUSED_PARAM (u2SubActionId);
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4Action);
    UNUSED_PARAM (i4Protocol);
    UNUSED_PARAM (u4SrcType);
    UNUSED_PARAM (u4SrcIpAddr);
    UNUSED_PARAM (u4SrcMask);
    UNUSED_PARAM (u4SrcMinPort);
    UNUSED_PARAM (u4SrcMaxPort);
    UNUSED_PARAM (u4DestType);
    UNUSED_PARAM (u4DestIpAddr);
    UNUSED_PARAM (u4DestMask);
    UNUSED_PARAM (u4DestMinPort);
    UNUSED_PARAM (u4DestMaxPort);
    UNUSED_PARAM (u4BitType);
    UNUSED_PARAM (i4Tos);
    UNUSED_PARAM (i4Dscp);
    UNUSED_PARAM (i4Priority);

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtIpFilterIcmpConfig                           */
/*                                                                           */
/*     DESCRIPTION      : This function configures a extended IP ACL filter  */
/*                         and enters the corresponding configuration mode   */
/*                                                                           */
/*     INPUT            :  i4Action -Permit/Deny                             */
/*                         u4SrcType - any/source network/ source host       */
/*                         u4SrcIp - Source IP Address                       */
/*                         u4SrcMask - Source IP Mask                        */
/*                         u4DestType - any/dest network/dest host           */
/*                         u4DestIp - Dest IP Address                        */
/*                         u4DestMask - Dest IP Mask                         */
/*                         u4MesageType, u4MessageCode - ICMP details        */
/*                         i4Priority - Filter priority                      */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclExtIpFilterIcmpConfig (tCliHandle CliHandle, INT4 i4Action,
                          UINT4 u4SrcType, UINT4 u4SrcIpAddr, UINT4 u4SrcMask,
                          UINT4 u4DestType, UINT4 u4DestIpAddr,
                          UINT4 u4DestMask, INT4 i4MessageType,
                          INT4 i4MessageCode, INT4 i4Priority)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4Action);
    UNUSED_PARAM (u4SrcType);
    UNUSED_PARAM (u4SrcIpAddr);
    UNUSED_PARAM (u4SrcMask);
    UNUSED_PARAM (u4DestType);
    UNUSED_PARAM (u4DestIpAddr);
    UNUSED_PARAM (u4DestMask);
    UNUSED_PARAM (i4MessageType);
    UNUSED_PARAM (i4MessageCode);
    UNUSED_PARAM (i4Priority);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclIpAccessGroup                                   */
/*                                                                           */
/*     DESCRIPTION      : This function associated an IP acl to an interface */
/*                                                                           */
/*     INPUT            : i4FilterNo - IP ACL number                         */
/*                        i4CmdType - IPV4 or IPv6                           */
/*                        i4Direction - In or out                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclIpAccessGroup (tCliHandle CliHandle, INT4 i4FilterNo, INT4 i4CmdType,
                  INT4 i4Direction)
{

    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4FilterNo);
    UNUSED_PARAM (i4CmdType);
    UNUSED_PARAM (i4Direction);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclNoIpAccessGroup                                 */
/*                                                                           */
/*     DESCRIPTION      : This function remove an IP acls from  an interface */
/*                                                                           */
/*     INPUT            : i4FilterNo - IP ACL number                         */
/*                        i4Direction - In or out                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclNoIpAccessGroup (tCliHandle CliHandle, INT4 i4FilterNo, INT4 i4CmdType,
                    INT4 i4Direction)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4FilterNo);
    UNUSED_PARAM (i4CmdType);
    UNUSED_PARAM (i4Direction);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtMacFilterConfig                              */
/*                                                                           */
/*     DESCRIPTION      : This function to configure MAC acls                */
/*                                                                           */
/*     INPUT            : u4SrcType - ANY/ Source HOST MAC                   */
/*                        SrcMacAddr - Source Mac Address                    */
/*                        u4DestType - ANY/ Dest Source MAC                  */
/*                        i4Protocol - Protocol value                        */
/*                        i4EtherType - Ethertype                            */
/*                        u4VlanId - Vlan Id                                 */
/*                        u4Priority - Priority value                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclExtMacFilterConfig (tCliHandle CliHandle, INT4 i4Action,
                       UINT4 u4SrcType, tMacAddr SrcMacAddr,
                       UINT4 u4DestType, tMacAddr DestMacAddr,
                       INT4 i4Protocol, INT4 i4EtherType,
                       UINT4 u4VlanId, INT4 i4Priority,
                       UINT4 i4UserPriority, UINT2 u2SubActionId,
                       INT4 i4NextFilterType, INT4 i4NextFilterId)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4Action);
    UNUSED_PARAM (u4SrcType);
    UNUSED_PARAM (SrcMacAddr);
    UNUSED_PARAM (u4DestType);
    UNUSED_PARAM (DestMacAddr);
    UNUSED_PARAM (i4Protocol);
    UNUSED_PARAM (i4EtherType);
    UNUSED_PARAM (u4VlanId);
    UNUSED_PARAM (i4Priority);
    UNUSED_PARAM (i4UserPriority);
    UNUSED_PARAM (u2SubActionId);
    UNUSED_PARAM (i4NextFilterType);
    UNUSED_PARAM (i4NextFilterId);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclMacExtAccessGroup                                */
/*                                                                           */
/*     DESCRIPTION      : This function associated an MAC acl to an interface*/
/*                                                                           */
/*     INPUT            : i4FilterNo - MAC ACL number                        */
/*                        i4Direction - In or out                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclMacExtAccessGroup (tCliHandle CliHandle, INT4 i4FilterNo, INT4 i4Direction)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4FilterNo);
    UNUSED_PARAM (i4Direction);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclNoMacExtAccessGroup                              */
/*                                                                           */
/*     DESCRIPTION      : This function remove an IP acls from  an interface */
/*                                                                           */
/*     INPUT            : i4FilterNo - IP ACL number                         */
/*                        i4Direction - In or out                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclNoMacExtAccessGroup (tCliHandle CliHandle, INT4 i4FilterNo, INT4 i4Direction)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4FilterNo);
    UNUSED_PARAM (i4Direction);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclShowAccessLists                                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays all the acls conifgured in  */
/*                        switch                                             */
/*                                                                           */
/*     INPUT            : CliHandle - CLI HAndler                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclShowAccessLists (tCliHandle CliHandle, INT4 i4FilterType, INT4 i4FilterNo)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM(i4FilterType);
    UNUSED_PARAM (i4FilterNo);
    return (CLI_SUCCESS);
}

INT4
AclShowL3Filter (tCliHandle CliHandle, INT4 i4NextFilter)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4NextFilter);
    return CLI_SUCCESS;
}

INT4
AclShowL2Filter (tCliHandle CliHandle, INT4 i4NextFilter)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4NextFilter);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclShowRunningConfig                               */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the  RSTP Configuration    */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
AclShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4Module);
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*     FUNCTION NAME    : AclShowRunningConfigTables                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays table  objects in ACL  for  */
/*                        show running configuration.                        */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
AclShowRunningConfigTables (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    return;

}

/*****************************************************************************/
/*     FUNCTION NAME    : AclShowRunningConfigInterfaceDetails               */
/*                                                                           */
/*     DESCRIPTION      : This function displays the  Interface objects in ACL*/
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
AclShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (i4Index);
    return;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : StdAclGetCfgPrompt                                 */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
StdAclGetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UNUSED_PARAM (pi1ModeName);
    UNUSED_PARAM (pi1DispStr);
    return FALSE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ExtAclGetCfgPrompt                                 */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
ExtAclGetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UNUSED_PARAM (pi1ModeName);
    UNUSED_PARAM (pi1DispStr);
    return FALSE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MacAclGetCfgPrompt                                 */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
MacAclGetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UNUSED_PARAM (pi1ModeName);
    UNUSED_PARAM (pi1DispStr);
    return FALSE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclTestIpParams                                    */
/*                                                                           */
/*     DESCRIPTION      : This function tests the IP address, mask for a     */
/*                        filter                                             */
/*                                                                           */
/*     INPUT            : CliHandle - CLI HAndler                            */
/*                        u1Type - whether Source/Destination Ip address     */
/*                        information must be tested                         */
/*                        i4FilterNo - IP Filter Number                      */
/*                        i4IpType - Whether IPaddress and/or mask is user   */
/*                        input                                              */
/*                        u4IpAddr - Ip Address to be tested                 */
/*                        u4IpMask - Ip Mask to be tested                    */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclTestIpParams (UINT1 u1Type, INT4 i4FilterNo, UINT4 u4IpType, UINT4 u4IpAddr,
                 UINT4 u4IpMask)
{
    UNUSED_PARAM (u1Type);
    UNUSED_PARAM (i4FilterNo);
    UNUSED_PARAM (u4IpType);
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (u4IpMask);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclSetIpParams                                     */
/*                                                                           */
/*     DESCRIPTION      : This function sets the IP address, mask for a      */
/*                        filter                                             */
/*                                                                           */
/*     INPUT            : CliHandle - CLI HAndler                            */
/*                        u1Type - whether Source/Destination Ip address     */
/*                        information must                                   */
/*                        be set                                             */
/*                        i4FilterNo - IP Filter Number                      */
/*                        i4IpType - Whether IPaddress and/or mask is user   */
/*                        input                                              */
/*                        u4IpAddr - Ip Address to be set                    */
/*                        u4IpMask - Ip Maxk to be set                       */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/

INT4
AclSetIpParams (UINT1 u1Type, INT4 i4FilterNo, UINT4 u4IpType, UINT4 u4IpAddr,
                UINT4 u4IpMask)
{
    UNUSED_PARAM (u1Type);
    UNUSED_PARAM (i4FilterNo);
    UNUSED_PARAM (u4IpType);
    UNUSED_PARAM (u4IpAddr);
    UNUSED_PARAM (u4IpMask);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclCliPrintPortList                                */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Port list               */
/*                                                                           */
/*     INPUT            :  piIfName - Port Name                              */
/*                         CliHandle-CLI Handler                             */
/*                         i4CommaCount- position of port in port list       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

VOID
AclCliPrintPortList (tCliHandle CliHandle, INT4 i4CommaCount, UINT1 *piIfName)
{
    UNUSED_PARAM(CliHandle);
    UNUSED_PARAM(i4CommaCount);
    UNUSED_PARAM(piIfName);
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : AclIPv6GetCfgPrompt                                */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/
INT1
AclIPv6GetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UNUSED_PARAM (pi1ModeName);
    UNUSED_PARAM (pi1DispStr);

    return FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UserDefAclGetCfgPrompt                             */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
UserDefAclGetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UNUSED_PARAM (pi1ModeName);
    UNUSED_PARAM (pi1DispStr);

    return FALSE;
}
#endif
