
/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: issexweb.c,v 1.1.1.1 2015/03/04 10:38:40 siva Exp $
 *
 * Description: This file contains the ACL module initialisation routines
 *****************************************************************************/

#ifdef WEBNM_WANTED
#include "issexweb.h"
#include "fsissecli.h"
#include "fsissewr.h"

extern tSNMP_OCTET_STRING_TYPE *allocmem_octetstring (INT4 i4Size);
extern VOID         free_octetstring (tSNMP_OCTET_STRING_TYPE * pOctetStr);
/*********************************************************************
*  Function Name : IssProcessCustomPages
*  Description   : This function processes the Pages specific to
*                  target
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
INT4
IssProcessCustomPages (tHttp * pHttp)
{
    UNUSED_PARAM(pHttp);
            return ISS_SUCCESS;
}

/************************************************************************ 
*  Function Name   : IssRedirectMacFilterPage 
*  Description     : This function will redirect Mac Filter page 
*  Input           : pHttp - Pointer to http entry where the html file 
*                    name needs to be changed(copied) 
*  Output          : None 
*  Returns         : None 
************************************************************************/
VOID
IssRedirectMacFilterPage (tHttp * pHttp)
{
     UNUSED_PARAM(pHttp);
}

/*********************************************************************
*  Function Name : IssFulcrumProcessIPFilterConfPage 
*  Description   : This function processes the request coming for the IP 
*                  Filter Configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssFulcrumProcessIPFilterConfPage (tHttp * pHttp)
{
     UNUSED_PARAM(pHttp);
}

/*********************************************************************
*  Function Name : IssFulcrumProcessIPStdFilterConfPage 
*  Description   : This function processes the request coming for the IP 
*                  standard Filter Configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssFulcrumProcessIPStdFilterConfPage (tHttp * pHttp)
{
     UNUSED_PARAM(pHttp);
}

/*********************************************************************
*  Function Name : IssFulcrumProcessMACFilterConfPage 
*  Description   : This function processes the request coming for the MAC 
*                  Filter Configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssFulcrumProcessMACFilterConfPage (tHttp * pHttp)
{
    UNUSED_PARAM(pHttp);
}

/*********************************************************************
*  Function Name : IssFulcrumProcessIPFilterConfPageSet
*  Description   : This function processes the set request for the IP
*                  filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssFulcrumProcessIPFilterConfPageSet (tHttp * pHttp)
{
    UNUSED_PARAM(pHttp);

}

/*********************************************************************
*  Function Name : IssFulcrumProcessIPFilterConfPageGet
*  Description   : This function processes the get request for the IP
*                  filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssFulcrumProcessIPFilterConfPageGet (tHttp * pHttp)
{
    UNUSED_PARAM(pHttp);
}

/*********************************************************************
*  Function Name : IssFulcrumProcessIPStdFilterConfPageGet 
*  Description   : This function processes the get request for the IP
*                  standard filter configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssFulcrumProcessIPStdFilterConfPageGet (tHttp * pHttp)
{
    UNUSED_PARAM(pHttp);
}

/*********************************************************************
*  Function Name : IssFulcrumProcessIPStdFilterConfPageSet 
*  Description   : This function processes the set request for the IP
*                 standard filter configuration page. 
*                     
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None 
*********************************************************************/

VOID
IssFulcrumProcessIPStdFilterConfPageSet (tHttp * pHttp)
{
    UNUSED_PARAM(pHttp);
}

/*********************************************************************
*  Function Name : IssFulcrumProcessMACFilterConfPageGet
*  Description   : This function processes the get request for the MAC
*                  filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssFulcrumProcessMACFilterConfPageGet (tHttp * pHttp)
{
     UNUSED_PARAM(pHttp);
}

/*********************************************************************
*  Function Name : IssFulcrumProcessMACFilterConfPageSet
*  Description   : This function processes the set request for the MAC
*                  filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssFulcrumProcessMACFilterConfPageSet (tHttp * pHttp)
{
    UNUSED_PARAM(pHttp);
}

#endif
