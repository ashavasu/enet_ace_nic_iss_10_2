# include  "lr.h"
# include  "fssnmp.h"
# include  "fsissalw.h"
# include  "fsissawr.h"
# include  "fsissadb.h"
# include  "iss.h"

INT4
GetNextIndexIssAclRateCtrlTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    UNUSED_PARAM(pFirstMultiIndex);
    UNUSED_PARAM(pNextMultiIndex);
#if 0

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIssAclRateCtrlTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIssAclRateCtrlTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif
    return SNMP_SUCCESS;
}

VOID
RegisterFSISSA ()
{
#if 0
    SNMPRegisterMibWithLock (&fsissaOID, &fsissaEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsissaOID, (const UINT1 *) "fsissacl");
#endif
}

VOID
UnRegisterFSISSA ()
{
#if 0
    SNMPUnRegisterMib (&fsissaOID, &fsissaEntry);
    SNMPDelSysorEntry (&fsissaOID, (const UINT1 *) "fsissacl");
#endif
}

INT4
IssAclRateCtrlDLFLimitValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

UNUSED_PARAM(pMultiIndex);
UNUSED_PARAM(pMultiData);
#if 0
    if (nmhValidateIndexInstanceIssAclRateCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclRateCtrlDLFLimitValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclRateCtrlBCASTLimitValueGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
UNUSED_PARAM(pMultiIndex);
UNUSED_PARAM(pMultiData);

#if 0
    if (nmhValidateIndexInstanceIssAclRateCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclRateCtrlBCASTLimitValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));
#endif

return SNMP_SUCCESS;

}

INT4
IssAclRateCtrlMCASTLimitValueGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0
    if (nmhValidateIndexInstanceIssAclRateCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclRateCtrlMCASTLimitValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclRateCtrlPortRateLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
      UNUSED_PARAM(pMultiIndex);
          UNUSED_PARAM(pMultiData);

#if 0
    if (nmhValidateIndexInstanceIssAclRateCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclRateCtrlPortRateLimit
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

#endif
return SNMP_SUCCESS;
}

INT4
IssAclRateCtrlPortBurstSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
   UNUSED_PARAM(pMultiIndex);
   UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclRateCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclRateCtrlPortBurstSize
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclRateCtrlDLFLimitValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
     UNUSED_PARAM(pMultiIndex);
     UNUSED_PARAM(pMultiData);
#if 0

    return (nmhSetIssAclRateCtrlDLFLimitValue
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclRateCtrlBCASTLimitValueSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhSetIssAclRateCtrlBCASTLimitValue
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclRateCtrlMCASTLimitValueSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
        UNUSED_PARAM(pMultiData);
#if 0

    return (nmhSetIssAclRateCtrlMCASTLimitValue
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
#endif
        return SNMP_SUCCESS;

}

INT4
IssAclRateCtrlPortRateLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
            UNUSED_PARAM(pMultiData);
#if 0
    return (nmhSetIssAclRateCtrlPortRateLimit
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclRateCtrlPortBurstSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
        UNUSED_PARAM(pMultiIndex);
                    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhSetIssAclRateCtrlPortBurstSize
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
#endif
     return SNMP_SUCCESS;

}

INT4
IssAclRateCtrlDLFLimitValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
    #if 0
    return (nmhTestv2IssAclRateCtrlDLFLimitValue (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));
    #endif
    return SNMP_SUCCESS;

}

INT4
IssAclRateCtrlBCASTLimitValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
UNUSED_PARAM(pu4Error);
UNUSED_PARAM(pMultiIndex);
UNUSED_PARAM(pMultiData);
#if 0

    return (nmhTestv2IssAclRateCtrlBCASTLimitValue (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;


}

INT4
IssAclRateCtrlMCASTLimitValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0
    return (nmhTestv2IssAclRateCtrlMCASTLimitValue (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclRateCtrlPortRateLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0
    return (nmhTestv2IssAclRateCtrlPortRateLimit (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclRateCtrlPortBurstSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0
    return (nmhTestv2IssAclRateCtrlPortBurstSize (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclRateCtrlTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpvarbinds);
#if 0
    return (nmhDepv2IssAclRateCtrlTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
#endif
    return SNMP_SUCCESS;
}

INT4
GetNextIndexIssAclL2FilterTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    UNUSED_PARAM(pFirstMultiIndex);
    UNUSED_PARAM(pNextMultiIndex);
#if 0
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIssAclL2FilterTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIssAclL2FilterTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif
    return SNMP_SUCCESS;
}

INT4
IssAclL2FilterPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0
    if (nmhValidateIndexInstanceIssAclL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL2FilterPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL2FilterEtherTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0
    if (nmhValidateIndexInstanceIssAclL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL2FilterEtherType (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL2FilterProtocolTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0
    if (nmhValidateIndexInstanceIssAclL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL2FilterProtocolType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL2FilterDstMacAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0
    if (nmhValidateIndexInstanceIssAclL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetIssAclL2FilterDstMacAddr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList));
#endif
    return SNMP_SUCCESS;
}

INT4
IssAclL2FilterSrcMacAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetIssAclL2FilterSrcMacAddr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList));
#endif
    return SNMP_SUCCESS;


}

INT4
IssAclL2FilterVlanIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
        UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL2FilterVlanId (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

#endif
 return SNMP_SUCCESS;
}

INT4
IssAclL2FilterInPortListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL2FilterInPortList
           (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));
#endif
     return SNMP_SUCCESS;

}

INT4
IssAclL2FilterActionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL2FilterAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL2FilterMatchCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0
    if (nmhValidateIndexInstanceIssAclL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL2FilterMatchCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));
#endif
     return SNMP_SUCCESS;

}

INT4
IssAclL2FilterStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL2FilterStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));
#endif
     return SNMP_SUCCESS;

}

INT4
IssAclL2FilterOutPortListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL2FilterOutPortList
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));
#endif
         return SNMP_SUCCESS;

}

INT4
IssAclL2FilterDirectionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
        UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL2FilterDirection (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));
#endif
             return SNMP_SUCCESS;


}

INT4
IssAclL2FilterPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhSetIssAclL2FilterPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL2FilterEtherTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
     UNUSED_PARAM(pMultiIndex);
         UNUSED_PARAM(pMultiData);
#if 0

    return (nmhSetIssAclL2FilterEtherType (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));
#endif
     return SNMP_SUCCESS;

}

INT4
IssAclL2FilterProtocolTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhSetIssAclL2FilterProtocolType
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

#endif
return SNMP_SUCCESS;
}

INT4
IssAclL2FilterDstMacAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0
    return (nmhSetIssAclL2FilterDstMacAddr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL2FilterSrcMacAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0
    return (nmhSetIssAclL2FilterSrcMacAddr
            (pMultiIndex->pIndex[0].i4_SLongValue,
             (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL2FilterVlanIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#if 0
    return (nmhSetIssAclL2FilterVlanId (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));
#endif
     return SNMP_SUCCESS;

}

INT4
IssAclL2FilterInPortListSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#if 0

    return (nmhSetIssAclL2FilterInPortList
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL2FilterActionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#if 0

    return (nmhSetIssAclL2FilterAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));
#endif
        return SNMP_SUCCESS;

}

INT4
IssAclL2FilterStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#if 0

    return (nmhSetIssAclL2FilterStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL2FilterOutPortListSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#if 0
    return (nmhSetIssAclL2FilterOutPortList
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));
#endif
        return SNMP_SUCCESS;

}

INT4
IssAclL2FilterDirectionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#if 0

    return (nmhSetIssAclL2FilterDirection (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));
#endif
            return SNMP_SUCCESS;

}

INT4
IssAclL2FilterPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    UNUSED_PARAM(pu4Error);

#if 0

    return (nmhTestv2IssAclL2FilterPriority (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;


}

INT4
IssAclL2FilterEtherTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    UNUSED_PARAM(pu4Error);

#if 0

    return (nmhTestv2IssAclL2FilterEtherType (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL2FilterProtocolTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    UNUSED_PARAM(pu4Error);

#if 0

    return (nmhTestv2IssAclL2FilterProtocolType (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->u4_ULongValue));
#endif
     return SNMP_SUCCESS;

}

INT4
IssAclL2FilterDstMacAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    UNUSED_PARAM(pu4Error);

#if 0
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return (nmhTestv2IssAclL2FilterDstMacAddr (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               (*(tMacAddr *) pMultiData->
                                                pOctetStrValue->
                                                pu1_OctetList)));
#endif
   return SNMP_SUCCESS;


}

INT4
IssAclL2FilterSrcMacAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return (nmhTestv2IssAclL2FilterSrcMacAddr (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               (*(tMacAddr *) pMultiData->
                                                pOctetStrValue->
                                                pu1_OctetList)));
#endif
     return SNMP_SUCCESS;

}

INT4
IssAclL2FilterVlanIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhTestv2IssAclL2FilterVlanId (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL2FilterInPortListTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhTestv2IssAclL2FilterInPortList (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->pOctetStrValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL2FilterActionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhTestv2IssAclL2FilterAction (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL2FilterStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhTestv2IssAclL2FilterStatus (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL2FilterOutPortListTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhTestv2IssAclL2FilterOutPortList (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->pOctetStrValue));
#endif
    return SNMP_SUCCESS;
}

INT4
IssAclL2FilterDirectionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhTestv2IssAclL2FilterDirection (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;


}

INT4
IssAclL2FilterTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpvarbinds);
#if 0

    return (nmhDepv2IssAclL2FilterTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
#endif
    return SNMP_SUCCESS;
}

INT4
GetNextIndexIssAclL3FilterTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
   UNUSED_PARAM(pFirstMultiIndex);
   UNUSED_PARAM(pNextMultiIndex);
#if 0

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIssAclL3FilterTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIssAclL3FilterTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
#endif
    return SNMP_SUCCESS;
}

INT4
IssAclL3FilterPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0
    if (nmhValidateIndexInstanceIssAclL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL3FilterPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterProtocolGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0
    if (nmhValidateIndexInstanceIssAclL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL3FilterProtocol (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterMessageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
#if 0
    if (nmhValidateIndexInstanceIssAclL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL3FilterMessageType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));
#endif
    return SNMP_SUCCESS;


}

INT4
IssAclL3FilterMessageCodeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL3FilterMessageCode
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));
#endif
    return SNMP_SUCCESS;
}

INT4
IssAclL3FilteAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0


    if (nmhValidateIndexInstanceIssAclL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL3FilteAddrType (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));
#endif
        return SNMP_SUCCESS;
}

INT4
IssAclL3FilterDstIpAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL3FilterDstIpAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));
#endif
     return SNMP_SUCCESS;

}

INT4
IssAclL3FilterSrcIpAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL3FilterSrcIpAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterDstIpAddrPrefixLengthGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL3FilterDstIpAddrPrefixLength
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterSrcIpAddrPrefixLengthGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0


    if (nmhValidateIndexInstanceIssAclL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL3FilterSrcIpAddrPrefixLength
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterMinDstProtPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL3FilterMinDstProtPort
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterMaxDstProtPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
     UNUSED_PARAM(pMultiIndex);
         UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL3FilterMaxDstProtPort
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));
#endif
        return SNMP_SUCCESS;

}

INT4
IssAclL3FilterMinSrcProtPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL3FilterMinSrcProtPort
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

#endif
        return SNMP_SUCCESS;

}

INT4
IssAclL3FilterMaxSrcProtPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL3FilterMaxSrcProtPort
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));
#endif
            return SNMP_SUCCESS;

}

INT4
IssAclL3FilterInPortListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL3FilterInPortList
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));
#endif
                return SNMP_SUCCESS;


}

INT4
IssAclL3FilterOutPortListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL3FilterOutPortList
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));
#endif
    return SNMP_SUCCESS;


}

INT4
IssAclL3FilterAckBitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL3FilterAckBit (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterRstBitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
     UNUSED_PARAM(pMultiIndex);
         UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL3FilterRstBit (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));
#endif
     return SNMP_SUCCESS;

}

INT4
IssAclL3FilterTosGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL3FilterTos (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterDscpGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0
    if (nmhValidateIndexInstanceIssAclL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL3FilterDscp (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));
#endif
        return SNMP_SUCCESS;


}

INT4
IssAclL3FilterDirectionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
     UNUSED_PARAM(pMultiIndex);
                                       UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL3FilterDirection (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));
#endif
                            return SNMP_SUCCESS;

}

INT4
IssAclL3FilterActionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
                                  UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL3FilterAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

#endif
                        return SNMP_SUCCESS;

}

INT4
IssAclL3FilterMatchCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
     UNUSED_PARAM(pMultiIndex);
                              UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL3FilterMatchCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));
#endif
                        return SNMP_SUCCESS;

}

INT4
IssAclL3FilterFlowIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
     UNUSED_PARAM(pMultiIndex);
                         UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL3FilterFlowId (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));
#endif
                    return SNMP_SUCCESS;


}

INT4
IssAclL3FilterStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
     UNUSED_PARAM(pMultiIndex);
                    UNUSED_PARAM(pMultiData);
#if 0

    if (nmhValidateIndexInstanceIssAclL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssAclL3FilterStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));
#endif
                return SNMP_SUCCESS;


}

INT4
IssAclL3FilterPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
      UNUSED_PARAM(pMultiIndex);
               UNUSED_PARAM(pMultiData);
#if 0


    return (nmhSetIssAclL3FilterPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));
#endif
            return SNMP_SUCCESS;


}

INT4
IssAclL3FilterProtocolSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
     UNUSED_PARAM(pMultiIndex);
         UNUSED_PARAM(pMultiData);
#if 0

    return (nmhSetIssAclL3FilterProtocol (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));
#endif
        return SNMP_SUCCESS;

}

INT4
IssAclL3FilterMessageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0


    return (nmhSetIssAclL3FilterMessageType
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;


}

    INT4
IssAclL3FilterMessageCodeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhSetIssAclL3FilterMessageCode
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;


}

    INT4
IssAclL3FilteAddrTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhSetIssAclL3FilteAddrType (pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;


}

    INT4
IssAclL3FilterDstIpAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
        UNUSED_PARAM(pMultiData);
#if 0

    return (nmhSetIssAclL3FilterDstIpAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));
#endif
        return SNMP_SUCCESS;


}

INT4
IssAclL3FilterSrcIpAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0
    return (nmhSetIssAclL3FilterSrcIpAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterDstIpAddrPrefixLengthSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhSetIssAclL3FilterDstIpAddrPrefixLength
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));
#endif
        return SNMP_SUCCESS;

}

INT4
IssAclL3FilterSrcIpAddrPrefixLengthSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhSetIssAclL3FilterSrcIpAddrPrefixLength
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));
#endif
    return SNMP_SUCCESS;


}

INT4
IssAclL3FilterMinDstProtPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhSetIssAclL3FilterMinDstProtPort
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));
#endif
    return SNMP_SUCCESS;


}

INT4
IssAclL3FilterMaxDstProtPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
     UNUSED_PARAM(pMultiIndex);
                  UNUSED_PARAM(pMultiData);
#if 0


    return (nmhSetIssAclL3FilterMaxDstProtPort
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));
#endif
                    return SNMP_SUCCESS;
}

INT4
IssAclL3FilterMinSrcProtPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
     UNUSED_PARAM(pMultiIndex);
             UNUSED_PARAM(pMultiData);
#if 0

    return (nmhSetIssAclL3FilterMinSrcProtPort
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));
#endif
                return SNMP_SUCCESS;


}

INT4
IssAclL3FilterMaxSrcProtPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
        UNUSED_PARAM(pMultiData);
#if 0

    return (nmhSetIssAclL3FilterMaxSrcProtPort
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));
#endif
            return SNMP_SUCCESS;


}

INT4
IssAclL3FilterInPortListSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhSetIssAclL3FilterInPortList
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));
#endif
        return SNMP_SUCCESS;


}

INT4
IssAclL3FilterOutPortListSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhSetIssAclL3FilterOutPortList
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));
#endif
    return SNMP_SUCCESS;


}

INT4
IssAclL3FilterAckBitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhSetIssAclL3FilterAckBit (pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;


}

INT4
IssAclL3FilterRstBitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0
    return (nmhSetIssAclL3FilterRstBit (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterTosSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhSetIssAclL3FilterTos (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterDscpSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
     UNUSED_PARAM(pMultiIndex);
         UNUSED_PARAM(pMultiData);
#if 0


    return (nmhSetIssAclL3FilterDscp (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterDirectionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhSetIssAclL3FilterDirection (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));
#endif
     return SNMP_SUCCESS;
}

INT4
IssAclL3FilterActionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhSetIssAclL3FilterAction (pMultiIndex->pIndex[0].i4_SLongValue,
                pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterFlowIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhSetIssAclL3FilterFlowId (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->u4_ULongValue));
#endif
          return SNMP_SUCCESS;

}

INT4
IssAclL3FilterStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0
    return (nmhSetIssAclL3FilterStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhTestv2IssAclL3FilterPriority (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));
#endif
        return SNMP_SUCCESS;



}

INT4
IssAclL3FilterProtocolTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhTestv2IssAclL3FilterProtocol (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;


}

INT4
IssAclL3FilterMessageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhTestv2IssAclL3FilterMessageType (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));
#endif
            return SNMP_SUCCESS;

}

INT4
IssAclL3FilterMessageCodeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhTestv2IssAclL3FilterMessageCode (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));
#endif
        return SNMP_SUCCESS;

}

INT4
IssAclL3FilteAddrTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0


    return (nmhTestv2IssAclL3FilteAddrType (pu4Error,
                pMultiIndex->pIndex[0].
                i4_SLongValue,
                pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;


}

INT4
IssAclL3FilterDstIpAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhTestv2IssAclL3FilterDstIpAddr (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->pOctetStrValue));
#endif
        return SNMP_SUCCESS;

}

INT4
IssAclL3FilterSrcIpAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhTestv2IssAclL3FilterSrcIpAddr (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->pOctetStrValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterDstIpAddrPrefixLengthTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0
    return (nmhTestv2IssAclL3FilterDstIpAddrPrefixLength (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          i4_SLongValue,
                                                          pMultiData->
                                                          u4_ULongValue));
#endif
    return SNMP_SUCCESS;
}

INT4
IssAclL3FilterSrcIpAddrPrefixLengthTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          i4_SLongValue,
                                                          pMultiData->
                                                          u4_ULongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterMinDstProtPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhTestv2IssAclL3FilterMinDstProtPort (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->u4_ULongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterMaxDstProtPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhTestv2IssAclL3FilterMaxDstProtPort (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->u4_ULongValue));
#endif
        return SNMP_SUCCESS;

}

INT4
IssAclL3FilterMinSrcProtPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhTestv2IssAclL3FilterMinSrcProtPort (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->u4_ULongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterMaxSrcProtPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhTestv2IssAclL3FilterMaxSrcProtPort (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->u4_ULongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterInPortListTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0
    return (nmhTestv2IssAclL3FilterInPortList (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->pOctetStrValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterOutPortListTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhTestv2IssAclL3FilterOutPortList (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->pOctetStrValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterAckBitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhTestv2IssAclL3FilterAckBit (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterRstBitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhTestv2IssAclL3FilterRstBit (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));
#endif
            return SNMP_SUCCESS;
}

INT4
IssAclL3FilterTosTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhTestv2IssAclL3FilterTos (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));
#endif
        return SNMP_SUCCESS;
}

INT4
IssAclL3FilterDscpTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhTestv2IssAclL3FilterDscp (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterDirectionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0

    return (nmhTestv2IssAclL3FilterDirection (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;
}

INT4
IssAclL3FilterActionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0
    return (nmhTestv2IssAclL3FilterAction (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));
#endif
        return SNMP_SUCCESS;


}

INT4
IssAclL3FilterFlowIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0
    return (nmhTestv2IssAclL3FilterFlowId (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
#if 0
    return (nmhTestv2IssAclL3FilterStatus (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));
#endif
    return SNMP_SUCCESS;

}

INT4
IssAclL3FilterTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpvarbinds);
#if 0
    return (nmhDepv2IssAclL3FilterTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
#endif

    return SNMP_SUCCESS;
}
