/* $Id: issexcli.c,v 1.1.1.1 2015/03/04 10:38:40 siva Exp $   */
#ifndef  __ISSEXCLI_C__
#define  __ISSEXCLI_C__

#include "lr.h"
#include "fssnmp.h"
#include "iss.h"
#include "msr.h"
#include "issexinc.h"
#include "fsissewr.h"
#include "fsisselw.h"
#include "fsissecli.h"
#include "isscli.h"
/***************************************************************/
/*  Function Name   : cli_process_iss_ext_cmd                  */
/*  Description     : This function servers as the handler for */
/*                    extended system CLI commands             */
/*  Input(s)        :                                          */
/*                    CliHandle - CLI Handle                   */
/*                    u4Command - Command given by user        */
/*                    Variable set of inputs depending on      */
/*                    the user command                         */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : None                                     */
/***************************************************************/
VOID
cli_process_iss_ext_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    UNUSED_PARAM(CliHandle);
    UNUSED_PARAM(u4Command);

}

/******************************************************************/
/*  Function Name   : CliSetIssStormControl                       */
/*  Description     : This function is used to set the            */
/*                    strom control limit                         */
/*  Input(s)        :                                             */
/*                    CliHandle   - CLI Handle                    */
/*                    u4IfIndex   - Port on which Storm control   */
/*                                  is applied.                   */
/*                    u4Mode      - Mode (Bcast/Mcast/DLF)        */
/*                    u4Rate      - Rate value                    */
/*  Output(s)       : None                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                     */
/******************************************************************/

INT4
CliSetIssStormControl (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4Mode,
                       UINT4 u4Rate)
{
    UNUSED_PARAM(CliHandle);
    UNUSED_PARAM(u4IfIndex);
    UNUSED_PARAM(u4Mode);
    UNUSED_PARAM(u4Rate);
    return CLI_SUCCESS;

}

/***************************************************************/
/*  Function Name   : CliSetIssConfPortRateLimit               */
/*  Description     : This function is used to configure rate  */
/*                    limit for a port                         */
/*  Input(s)        : CliHandle        -  CLI Handle           */
/*                    u4IfIndex        -  Interface Index      */
/*                    i4PortRateLimit  -  Port rate limit      */
/*                    i4PortBurstSize  -  Burst size limit     */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
INT4
CliSetIssConfPortRateLimit (tCliHandle CliHandle, UINT4 u4IfIndex,
                            INT4 i4PortRateLimit, INT4 i4PortBurstSize)
{
    UNUSED_PARAM(CliHandle);
    UNUSED_PARAM(u4IfIndex);
    UNUSED_PARAM(i4PortRateLimit);
    UNUSED_PARAM(i4PortBurstSize);
    return CLI_SUCCESS;
}

#endif /* __ISSEXCLI_C__ */
