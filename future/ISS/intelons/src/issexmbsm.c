/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: issexmbsm.c,v 1.1.1.1 2015/03/04 10:38:40 siva Exp $
 *
 * Description: 
 *
 *******************************************************************/


#ifndef _ISSEXMBSM_C
#define _ISSEXMBSM_C

#include "issexinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : IssMbsmL2FilterCardInsert                                  */
/*                                                                           */
/* Description  : This function updates the L2 Filter HW configuration when  */
/*                a card is inserted into the MBSM System.                   */
/*                                                                           */
/* Input        : pPortInfo  - Pointer to Port Related Info.                 */
/*              : pSlotInfo  - Pointer to Slot Related Info.                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IssMbsmL2FilterCardInsert (tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM(pPortInfo);
    UNUSED_PARAM(pSlotInfo);
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssMbsmL2FilterCardRemove                                  */
/*                                                                           */
/* Description  : This function updates the L2 Filter HW configuration when  */
/*                a card is removed into the MBSM System.                    */
/*                                                                           */
/* Input        : pPortInfo  - Pointer to Port Related Info.                 */
/*              : pSlotInfo  - Pointer to Slot Related Info.                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IssMbsmL2FilterCardRemove (tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo)
{
    /* Filter deletion for L2 filters is not needed when Card Removal event
     * is received. Since we are maintaining a global L2 filter list for every 
     * L2 filter handle (via BCMX), no need to clean-up the filter list when a 
     * line card is removed. Just ignore the card removal event for this case.
     */

    UNUSED_PARAM (pPortInfo);
    UNUSED_PARAM (pSlotInfo);
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssMbsmL3FilterCardInsert                                  */
/*                                                                           */
/* Description  : This function updates the L3 Filter HW configuration when  */
/*                a card is inserted into the MBSM System.                   */
/*                                                                           */
/* Input        : pPortInfo  - Pointer to Port Related Info.                 */
/*              : pSlotInfo  - Pointer to Slot Related Info.                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IssMbsmL3FilterCardInsert (tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM(pPortInfo);
    UNUSED_PARAM(pSlotInfo);
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssMbsmL3FilterCardRemove                                  */
/*                                                                           */
/* Description  : This function updates the L3 Filter HW configuration when  */
/*                a card is removed into the MBSM System.                    */
/*                                                                           */
/* Input        : pPortInfo  - Pointer to Port Related Info.                 */
/*              : pSlotInfo  - Pointer to Slot Related Info.                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IssMbsmL3FilterCardRemove (tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo)
{
    /* Filter deletion for L3 filters is not needed when Card Removal event
     * is received. Since we are maintaining a global L3 filter list for every 
     * L3 filter handle (via BCMX), no need to clean-up the filter list when a 
     * line card is removed. Just ignore the card removal event for this case.
     */

    UNUSED_PARAM (pPortInfo);
    UNUSED_PARAM (pSlotInfo);
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssMbsmCardInsertRateCtrlTable                             */
/*                                                                           */
/* Description  : This function updates the port related HW configuration    */
/*                for Rate Control Table when                                */
/*                a card is inserted into the MBSM System.                   */
/*                                                                           */
/* Input        : pPortInfo  - Pointer to Port Related Info.                 */
/*              : pSlotInfo  - Pointer to Slot Related Info.                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
IssMbsmCardInsertRateCtrlTable (tMbsmPortInfo * pPortInfo,
                                tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM(pPortInfo);
    UNUSED_PARAM(pSlotInfo);
    return MBSM_SUCCESS;
}

#endif /* _ISSEXMBSM_C */
