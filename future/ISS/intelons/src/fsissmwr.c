# include  "lr.h"
# include  "fssnmp.h"
# include  "fsissmlw.h"
# include  "fsissmwr.h"
# include  "fsissmdb.h"
INT4
GetNextIndexIssMetroL2FilterTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    UNUSED_PARAM(pFirstMultiIndex);
    UNUSED_PARAM(pNextMultiIndex);
    return SNMP_SUCCESS;
}

VOID
RegisterFSISSM ()
{
}

VOID
UnRegisterFSISSM ()
{
}

INT4
IssMetroL2FilterOuterEtherTypeGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
    UNUSED_PARAM(pMultiData);
    return SNMP_SUCCESS;

}

INT4
IssMetroL2FilterSVlanIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
     UNUSED_PARAM(pMultiIndex);
         UNUSED_PARAM(pMultiData);
             return SNMP_SUCCESS;


}

INT4
IssMetroL2FilterSVlanPriorityGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
             UNUSED_PARAM(pMultiData);
                          return SNMP_SUCCESS;

}

INT4
IssMetroL2FilterCVlanPriorityGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
             UNUSED_PARAM(pMultiData);
                          return SNMP_SUCCESS;

}

INT4
IssMetroL2FilterPacketTagTypeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
             UNUSED_PARAM(pMultiData);
                          return SNMP_SUCCESS;

}

INT4
IssMetroL2FilterOuterEtherTypeSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
             UNUSED_PARAM(pMultiData);
                          return SNMP_SUCCESS;

}

INT4
IssMetroL2FilterSVlanIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
             UNUSED_PARAM(pMultiData);
                          return SNMP_SUCCESS;

}

INT4
IssMetroL2FilterSVlanPrioritySet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
             UNUSED_PARAM(pMultiData);
                          return SNMP_SUCCESS;

}

INT4
IssMetroL2FilterCVlanPrioritySet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
             UNUSED_PARAM(pMultiData);
                          return SNMP_SUCCESS;

}

INT4
IssMetroL2FilterPacketTagTypeSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM(pMultiIndex);
             UNUSED_PARAM(pMultiData);
                          return SNMP_SUCCESS;

}

INT4
IssMetroL2FilterOuterEtherTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pMultiIndex);
             UNUSED_PARAM(pMultiData);
                          return SNMP_SUCCESS;

}

INT4
IssMetroL2FilterSVlanIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
        UNUSED_PARAM(pMultiIndex);
                     UNUSED_PARAM(pMultiData);
                                               return SNMP_SUCCESS;

}

INT4
IssMetroL2FilterSVlanPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
        UNUSED_PARAM(pMultiIndex);
                     UNUSED_PARAM(pMultiData);
                                               return SNMP_SUCCESS;

}

INT4
IssMetroL2FilterCVlanPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
        UNUSED_PARAM(pMultiIndex);
                     UNUSED_PARAM(pMultiData);
                                               return SNMP_SUCCESS;

}

INT4
IssMetroL2FilterPacketTagTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM(pu4Error);
        UNUSED_PARAM(pMultiIndex);
                     UNUSED_PARAM(pMultiData);
                                               return SNMP_SUCCESS;

}

INT4
GetNextIndexIssMetroL3FilterTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
        UNUSED_PARAM(pFirstMultiIndex);
                     UNUSED_PARAM(pNextMultiIndex);
                                               return SNMP_SUCCESS;

    return SNMP_SUCCESS;
}

INT4
IssMetroL2FilterTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(pSnmpIndexList);
    UNUSED_PARAM(pSnmpvarbinds);
    return SNMP_SUCCESS;

}

INT4
IssMetroL3FilterSVlanIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return SNMP_SUCCESS;

}

INT4
IssMetroL3FilterSVlanPriorityGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
 UNUSED_PARAM (pMultiIndex);
     UNUSED_PARAM (pMultiData);
         return SNMP_SUCCESS;

}

INT4
IssMetroL3FilterCVlanIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
        UNUSED_PARAM (pMultiData);
            return SNMP_SUCCESS;


}

INT4
IssMetroL3FilterCVlanPriorityGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
        UNUSED_PARAM (pMultiData);
            return SNMP_SUCCESS;


}

INT4
IssMetroL3FilterPacketTagTypeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
        UNUSED_PARAM (pMultiData);
            return SNMP_SUCCESS;


}

INT4
IssMetroL3FilterSVlanIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
        UNUSED_PARAM (pMultiData);
            return SNMP_SUCCESS;


}

INT4
IssMetroL3FilterSVlanPrioritySet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
        UNUSED_PARAM (pMultiData);
            return SNMP_SUCCESS;


}

INT4
IssMetroL3FilterCVlanIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
        UNUSED_PARAM (pMultiData);
            return SNMP_SUCCESS;


}

INT4
IssMetroL3FilterCVlanPrioritySet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
        UNUSED_PARAM (pMultiData);
            return SNMP_SUCCESS;


}

INT4
IssMetroL3FilterPacketTagTypeSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
        UNUSED_PARAM (pMultiData);
            return SNMP_SUCCESS;


}

INT4
IssMetroL3FilterSVlanIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pu4Error);
    UNUSED_PARAM (pMultiIndex);
        UNUSED_PARAM (pMultiData);
            return SNMP_SUCCESS;


}

INT4
IssMetroL3FilterSVlanPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    
     UNUSED_PARAM (pu4Error);
         UNUSED_PARAM (pMultiIndex);
                 UNUSED_PARAM (pMultiData);
                             return SNMP_SUCCESS;


}

INT4
IssMetroL3FilterCVlanIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
     UNUSED_PARAM (pu4Error);
         UNUSED_PARAM (pMultiIndex);
                 UNUSED_PARAM (pMultiData);
                             return SNMP_SUCCESS;


}

INT4
IssMetroL3FilterCVlanPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
     UNUSED_PARAM (pu4Error);
         UNUSED_PARAM (pMultiIndex);
                 UNUSED_PARAM (pMultiData);
                             return SNMP_SUCCESS;


}

INT4
IssMetroL3FilterPacketTagTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
     UNUSED_PARAM (pu4Error);
         UNUSED_PARAM (pMultiIndex);
                 UNUSED_PARAM (pMultiData);
                             return SNMP_SUCCESS;


}

INT4
IssMetroL3FilterTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
     UNUSED_PARAM (pu4Error);
         UNUSED_PARAM (pSnmpIndexList);
                 UNUSED_PARAM (pSnmpvarbinds);
                             return SNMP_SUCCESS;

}
