/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsisselw.c,v 1.3 2016/03/19 13:10:51 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "iss.h"
# include  "issexinc.h"
# include  "fsisselw.h"
# include  "isscli.h"
# include  "aclcli.h"
# include  "diffsrv.h"
# include  "qosxtd.h"
# include  "fsissalw.h"

UINT4               gu4AclOverLa = 1;
/* LOW LEVEL Routines for Table : IssExtRateCtrlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssExtRateCtrlTable
 Input       :  The Indices
                IssExtRateCtrlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssExtRateCtrlTable (INT4 i4IssExtRateCtrlIndex)
{
    UNUSED_PARAM (i4IssExtRateCtrlIndex);
return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssExtRateCtrlTable
 Input       :  The Indices
                IssExtRateCtrlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssExtRateCtrlTable (INT4 *pi4IssExtRateCtrlIndex)
{
    UNUSED_PARAM (pi4IssExtRateCtrlIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssExtRateCtrlTable
 Input       :  The Indices
                IssExtRateCtrlIndex
                nextIssRateCtrlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssExtRateCtrlTable (INT4 i4IssExtRateCtrlIndex,
                                    INT4 *pi4NextIssRateCtrlIndex)
{
    UNUSED_PARAM(i4IssExtRateCtrlIndex);
    UNUSED_PARAM(pi4NextIssRateCtrlIndex);
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIssExtRateCtrlDLFLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                retValIssExtRateCtrlDLFLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtRateCtrlDLFLimitValue (INT4 i4IssExtRateCtrlIndex,
                                   INT4 *pi4RetValIssExtRateCtrlDLFLimitValue)
{
    UNUSED_PARAM(i4IssExtRateCtrlIndex);
    UNUSED_PARAM(pi4RetValIssExtRateCtrlDLFLimitValue);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtRateCtrlBCASTLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                retValIssExtRateCtrlBCASTLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtRateCtrlBCASTLimitValue (INT4 i4IssExtRateCtrlIndex,
                                     INT4
                                     *pi4RetValIssExtRateCtrlBCASTLimitValue)
{
    UNUSED_PARAM(i4IssExtRateCtrlIndex);
    UNUSED_PARAM(pi4RetValIssExtRateCtrlBCASTLimitValue);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtRateCtrlMCASTLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                retValIssExtRateCtrlMCASTLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtRateCtrlMCASTLimitValue (INT4 i4IssExtRateCtrlIndex,
                                     INT4
                                     *pi4RetValIssExtRateCtrlMCASTLimitValue)
{
    UNUSED_PARAM (i4IssExtRateCtrlIndex);
    UNUSED_PARAM (pi4RetValIssExtRateCtrlMCASTLimitValue);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtRateCtrlPortRateLimit
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                retValIssExtRateCtrlPortRateLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtRateCtrlPortRateLimit (INT4 i4IssExtRateCtrlIndex,
                                   INT4 *pi4RetValIssExtRateCtrlPortRateLimit)
{
    UNUSED_PARAM(i4IssExtRateCtrlIndex);
    UNUSED_PARAM(pi4RetValIssExtRateCtrlPortRateLimit);
    return SNMP_FAILURE;
    
}

/****************************************************************************
 Function    :  nmhGetIssExtRateCtrlPortBurstSize
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                retValIssExtRateCtrlPortBurstSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtRateCtrlPortBurstSize (INT4 i4IssExtRateCtrlIndex,
                                   INT4 *pi4RetValIssExtRateCtrlPortBurstSize)
{
    UNUSED_PARAM(i4IssExtRateCtrlIndex);
    UNUSED_PARAM(pi4RetValIssExtRateCtrlPortBurstSize);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtDefaultRateCtrlStatus
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                retValIssExtDefaultRateCtrlStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIssExtDefaultRateCtrlStatus(INT4 i4IssExtRateCtrlIndex , INT4 *pi4RetValIssExtDefaultRateCtrlStatus)
{
  UNUSED_PARAM(i4IssExtRateCtrlIndex);
  UNUSED_PARAM (*pi4RetValIssExtDefaultRateCtrlStatus);
  return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIssExtRateCtrlDLFLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                setValIssExtRateCtrlDLFLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtRateCtrlDLFLimitValue (INT4 i4IssExtRateCtrlIndex,
                                   INT4 i4SetValIssExtRateCtrlDLFLimitValue)
{
    UNUSED_PARAM(i4IssExtRateCtrlIndex);
    UNUSED_PARAM(i4SetValIssExtRateCtrlDLFLimitValue);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssExtRateCtrlBCASTLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                setValIssExtRateCtrlBCASTLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtRateCtrlBCASTLimitValue (INT4 i4IssExtRateCtrlIndex,
                                     INT4 i4SetValIssExtRateCtrlBCASTLimitValue)
{
    UNUSED_PARAM(i4IssExtRateCtrlIndex);
    UNUSED_PARAM(i4SetValIssExtRateCtrlBCASTLimitValue);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssExtRateCtrlMCASTLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                setValIssExtRateCtrlMCASTLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtRateCtrlMCASTLimitValue (INT4 i4IssExtRateCtrlIndex,
                                     INT4 i4SetValIssExtRateCtrlMCASTLimitValue)
{
    UNUSED_PARAM(i4IssExtRateCtrlIndex);
    UNUSED_PARAM(i4SetValIssExtRateCtrlMCASTLimitValue);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssExtRateCtrlPortRateLimit
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                setValIssExtRateCtrlPortRateLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtRateCtrlPortRateLimit (INT4 i4IssExtRateCtrlIndex,
                                   INT4 i4SetValIssExtRateCtrlPortRateLimit)
{
    UNUSED_PARAM(i4IssExtRateCtrlIndex);
    UNUSED_PARAM(i4SetValIssExtRateCtrlPortRateLimit);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssExtRateCtrlPortBurstSize
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                setValIssExtRateCtrlPortBurstSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtRateCtrlPortBurstSize (INT4 i4IssExtRateCtrlIndex,
                                   INT4 i4SetValIssExtRateCtrlPortBurstSize)
{
    UNUSED_PARAM(i4IssExtRateCtrlIndex);
    UNUSED_PARAM(i4SetValIssExtRateCtrlPortBurstSize);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssExtDefaultRateCtrlStatus
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                setValIssExtDefaultRateCtrlStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetIssExtDefaultRateCtrlStatus(INT4 i4IssExtRateCtrlIndex , INT4 i4SetValIssExtDefaultRateCtrlStatus)
{
  UNUSED_PARAM(i4IssExtRateCtrlIndex);
  UNUSED_PARAM (i4SetValIssExtDefaultRateCtrlStatus);
  return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2IssExtRateCtrlDLFLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                testValIssExtRateCtrlDLFLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtRateCtrlDLFLimitValue (UINT4 *pu4ErrorCode,
                                      INT4 i4IssExtRateCtrlIndex,
                                      INT4 i4TestValIssExtRateCtrlDLFLimitValue)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(i4IssExtRateCtrlIndex);
    UNUSED_PARAM(i4TestValIssExtRateCtrlDLFLimitValue);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtRateCtrlBCASTLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                testValIssExtRateCtrlBCASTLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtRateCtrlBCASTLimitValue (UINT4 *pu4ErrorCode,
                                        INT4 i4IssExtRateCtrlIndex,
                                        INT4
                                        i4TestValIssExtRateCtrlBCASTLimitValue)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(i4IssExtRateCtrlIndex);
    UNUSED_PARAM(i4TestValIssExtRateCtrlBCASTLimitValue);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtRateCtrlMCASTLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                testValIssExtRateCtrlMCASTLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtRateCtrlMCASTLimitValue (UINT4 *pu4ErrorCode,
                                        INT4 i4IssExtRateCtrlIndex,
                                        INT4
                                        i4TestValIssExtRateCtrlMCASTLimitValue)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(i4IssExtRateCtrlIndex);
    UNUSED_PARAM(i4TestValIssExtRateCtrlMCASTLimitValue);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtRateCtrlPortRateLimit
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                testValIssExtRateCtrlPortRateLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtRateCtrlPortRateLimit (UINT4 *pu4ErrorCode,
                                      INT4 i4IssExtRateCtrlIndex,
                                      INT4 i4TestValIssExtRateCtrlPortRateLimit)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(i4IssExtRateCtrlIndex);
    UNUSED_PARAM(i4TestValIssExtRateCtrlPortRateLimit);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtRateCtrlPortBurstSize
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                testValIssExtRateCtrlPortBurstSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtRateCtrlPortBurstSize (UINT4 *pu4ErrorCode,
                                      INT4 i4IssExtRateCtrlIndex,
                                      INT4 i4TestValIssExtRateCtrlPortBurstSize)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(i4IssExtRateCtrlIndex);
    UNUSED_PARAM(i4TestValIssExtRateCtrlPortBurstSize);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssExtDefaultRateCtrlStatus
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                testValIssExtDefaultRateCtrlStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2IssExtDefaultRateCtrlStatus(UINT4 *pu4ErrorCode , INT4 i4IssExtRateCtrlIndex , INT4 i4TestValIssExtDefaultRateCtrlStatus)
{
    UNUSED_PARAM(*pu4ErrorCode);
    UNUSED_PARAM(i4IssExtRateCtrlIndex);
    UNUSED_PARAM (i4TestValIssExtDefaultRateCtrlStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssExtRateCtrlTable
 Input       :  The Indices
                IssExtRateCtrlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssExtRateCtrlTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : IssExtL2FilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssExtL2FilterTable
 Input       :  The Indices
                IssExtL2FilterNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssExtL2FilterTable (INT4 i4IssExtL2FilterNo)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterPriority
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterPriority (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                                 INT4 i4Element)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterPriority
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterPriority (INT4 i4IssExtL2FilterNo, INT4 i4Element)
{
    UNUSED_PARAM (i4IssExtL2FilterNo);
    UNUSED_PARAM (i4Element);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterPriority
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterPriority (INT4 i4IssExtL2FilterNo, INT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterEtherType
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                testValIssExtL2FilterEtherType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterEtherType (UINT4 *pu4ErrorCode,
                                  INT4 i4IssExtL2FilterNo,
                                  INT4 i4TestValIssExtL2FilterEtherType)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(i4TestValIssExtL2FilterEtherType);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterEtherType
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                setValIssExtL2FilterEtherType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterEtherType (INT4 i4IssExtL2FilterNo,
                               INT4 i4SetValIssExtL2FilterEtherType)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(i4SetValIssExtL2FilterEtherType);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterEtherType
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                retValIssExtL2FilterEtherType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterEtherType (INT4 i4IssExtL2FilterNo,
                               INT4 *pi4RetValIssExtL2FilterEtherType)
{
   UNUSED_PARAM(i4IssExtL2FilterNo);
   UNUSED_PARAM(pi4RetValIssExtL2FilterEtherType);
   return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterProtocolType
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterProtocolType (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                                     UINT4 u4Element)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(u4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterProtocolType
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterProtocolType (INT4 i4IssExtL2FilterNo, UINT4 u4Element)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(u4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterProtocolType
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterProtocolType (INT4 i4IssExtL2FilterNo, UINT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterDstMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterDstMacAddr (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                                   tMacAddr pElement)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterDstMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterDstMacAddr (INT4 i4IssExtL2FilterNo, tMacAddr MacAddress)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(MacAddress);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterDstMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterDstMacAddr (INT4 i4IssExtL2FilterNo, tMacAddr * pMacAddress)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(pMacAddress);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterSrcMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterSrcMacAddr (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                                   tMacAddr pElement)
{
   UNUSED_PARAM(pu4Error);
   UNUSED_PARAM(i4IssExtL2FilterNo);
   UNUSED_PARAM(pElement);
   return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterSrcMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterSrcMacAddr (INT4 i4IssExtL2FilterNo, tMacAddr pElement)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterSrcMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterSrcMacAddr (INT4 i4IssExtL2FilterNo, tMacAddr * pElement)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterVlanId
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterVlanId (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                               INT4 i4TestValIssExtL2FilterCustomerVlanId)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(i4TestValIssExtL2FilterCustomerVlanId);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterVlanId
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterVlanId (INT4 i4IssExtL2FilterNo, INT4 i4Element)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterVlanId
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterVlanId (INT4 i4IssExtL2FilterNo, INT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterUserPriority
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                testValIssExtL2FilterUserPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterUserPriority (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                                     INT4 i4TestValIssExtL2FilterUserPriority)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(i4TestValIssExtL2FilterUserPriority);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterInPortList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterInPortList (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                                   tSNMP_OCTET_STRING_TYPE * pElement)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterInPortList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterInPortList (INT4 i4IssExtL2FilterNo,
                                tSNMP_OCTET_STRING_TYPE * pElement)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterUserPriority
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                setValIssExtL2FilterUserPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterUserPriority (INT4 i4IssExtL2FilterNo, INT4 i4Element)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterUserPriority
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                retValIssExtL2FilterUserPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterUserPriority (INT4 i4IssExtL2FilterNo, INT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(pElement);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterInPortList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterInPortList (INT4 i4IssExtL2FilterNo,
                                tSNMP_OCTET_STRING_TYPE * pElement)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterOutPortList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                testValIssExtL2FilterOutPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterOutPortList (UINT4 *pu4ErrorCode,
                                    INT4 i4IssExtL2FilterNo,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValOutPortList)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(pTestValOutPortList);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterOutPortList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                setValIssExtL2FilterOutPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterOutPortList (INT4 i4IssExtL2FilterNo,
                                 tSNMP_OCTET_STRING_TYPE * pSetValOutPortList)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(pSetValOutPortList);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterOutPortList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                retValIssExtL2FilterOutPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterOutPortList (INT4 i4IssExtL2FilterNo,
                                 tSNMP_OCTET_STRING_TYPE * pGetValOutPortList)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(pGetValOutPortList);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterDirection
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                testValIssExtL2FilterDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterDirection (UINT4 *pu4ErrorCode,
                                  INT4 i4IssExtL2FilterNo,
                                  INT4 i4TestValIssExtL2FilterDirection)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(i4TestValIssExtL2FilterDirection);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterDirection
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                setValIssExtL2FilterDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterDirection (INT4 i4IssExtL2FilterNo,
                               INT4 i4SetValIssExtL2FilterDirection)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(i4SetValIssExtL2FilterDirection);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterDirection
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                retValIssExtL2FilterDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterDirection (INT4 i4IssExtL2FilterNo,
                               INT4 *pi4RetValIssExtL2FilterDirection)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(pi4RetValIssExtL2FilterDirection);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterAction
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterAction (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                               INT4 i4Element)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterAction
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterAction (INT4 i4IssExtL2FilterNo, INT4 i4Element)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterAction
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterAction (INT4 i4IssExtL2FilterNo, INT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterMatchCount
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterMatchCount (INT4 i4IssExtL2FilterNo, UINT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterStatus
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterStatus (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                               INT4 i4Element)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterStatus
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterStatus (INT4 i4IssExtL2FilterNo, INT4 i4Element)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterStatus
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterStatus (INT4 i4IssExtL2FilterNo, INT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssExtL2FilterTable
 Input       :  The Indices
                IssExtL2FilterNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssExtL2FilterTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : IssExtL3FilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssExtL3FilterTable
 Input       :  The Indices
                IssExtL3FilterNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssExtL3FilterTable (INT4 i4IssExtL3FilterNo)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterPriority
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterPriority (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                 INT4 i4Element)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterPriority
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterPriority (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterPriority
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterPriority (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterProtocol
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterProtocol (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                 INT4 i4Element)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterProtocol
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterProtocol (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterProtocol
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterProtocol (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterMessageType
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterMessageType (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                    INT4 i4Element)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterMessageType
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterMessageType (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMessageType
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMessageType (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterMessageCode
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterMessageCode (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                    INT4 i4Element)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterMessageCode
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterMessageCode (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMessageCode
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMessageCode (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterDstIpAddr
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterDstIpAddr (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                  UINT4 u4Element)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(u4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterDstIpAddr
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterDstIpAddr (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(u4Element);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterDstIpAddr
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterDstIpAddr (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pElement);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterSrcIpAddr
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterSrcIpAddr (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                  UINT4 u4Element)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(u4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterSrcIpAddr
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterSrcIpAddr (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(u4Element);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterSrcIpAddr
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterSrcIpAddr (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterDstIpAddrMask
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterDstIpAddrMask (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                      UINT4 u4Element)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(u4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterDstIpAddrMask
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterDstIpAddrMask (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(u4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterDstIpAddrMask
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterDstIpAddrMask (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pElement);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterSrcIpAddrMask
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterSrcIpAddrMask (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                      UINT4 u4Element)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(u4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterSrcIpAddrMask
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterSrcIpAddrMask (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(u4Element);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterSrcIpAddrMask
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterSrcIpAddrMask (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterMinDstProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterMinDstProtPort (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                       UINT4 u4Element)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(u4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterMinDstProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterMinDstProtPort (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(u4Element);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMinDstProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMinDstProtPort (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterMaxDstProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterMaxDstProtPort (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                       UINT4 u4Element)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(u4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterMaxDstProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterMaxDstProtPort (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(u4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMaxDstProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMaxDstProtPort (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterMinSrcProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterMinSrcProtPort (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                       UINT4 u4Element)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(u4Element);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterMinSrcProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterMinSrcProtPort (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(u4Element);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMinSrcProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMinSrcProtPort (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterMaxSrcProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterMaxSrcProtPort (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                       UINT4 u4Element)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(u4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterMaxSrcProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterMaxSrcProtPort (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(u4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMaxSrcProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMaxSrcProtPort (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterInPortList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterInPortList (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                   tSNMP_OCTET_STRING_TYPE * pElement)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterInPortList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterInPortList (INT4 i4IssExtL3FilterNo,
                                tSNMP_OCTET_STRING_TYPE * pElement)
{
   UNUSED_PARAM(i4IssExtL3FilterNo);
   UNUSED_PARAM(pElement);
   return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterInPortList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterInPortList (INT4 i4IssExtL3FilterNo,
                                tSNMP_OCTET_STRING_TYPE * pElement)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterOutPortList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterOutPortList (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                    tSNMP_OCTET_STRING_TYPE * pElement)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterOutPortList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterOutPortList (INT4 i4IssExtL3FilterNo,
                                 tSNMP_OCTET_STRING_TYPE * pElement)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterOutPortList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterOutPortList (INT4 i4IssExtL3FilterNo,
                                 tSNMP_OCTET_STRING_TYPE * pElement)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterAckBit
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterAckBit (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                               INT4 i4Element)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterAckBit
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterAckBit (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterAckBit
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterAckBit (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterRstBit
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterRstBit (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                               INT4 i4Element)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterRstBit
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterRstBit (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterRstBit
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterRstBit (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterTos
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterTos (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                            INT4 i4Element)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterTos
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterTos (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterTos
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterTos (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterDscp
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                testValIssExtL3FilterDscp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterDscp (UINT4 *pu4ErrorCode, INT4 i4IssExtL3FilterNo,
                             INT4 i4TestValIssExtL3FilterDscp)
{
    UNUSED_PARAM(pu4ErrorCode);
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(i4TestValIssExtL3FilterDscp);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterDscp
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                setValIssExtL3FilterDscp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterDscp (INT4 i4IssExtL3FilterNo,
                          INT4 i4SetValIssExtL3FilterDscp)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(i4SetValIssExtL3FilterDscp);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterDscp
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                retValIssExtL3FilterDscp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterDscp (INT4 i4IssExtL3FilterNo,
                          INT4 *pi4RetValIssExtL3FilterDscp)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pi4RetValIssExtL3FilterDscp);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterDirection
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterDirection (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                  INT4 i4Element)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterDirection
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterDirection (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterDirection
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterDirection (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterAction
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterAction (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                               INT4 i4Element)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterAction
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterAction (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterAction
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterAction (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMatchCount
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMatchCount (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pElement);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterStatus
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterStatus (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                               INT4 i4Element)
{
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterStatus
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterStatus (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{

    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(i4Element);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterStatus
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterStatus (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    UNUSED_PARAM (i4IssExtL3FilterNo);
    UNUSED_PARAM (pElement);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssExtL3FilterTable
 Input       :  The Indices
                IssExtFilterL3No
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssExtL3FilterTable (INT4 *pi4IssExtL3FilterNo)
{
    UNUSED_PARAM (pi4IssExtL3FilterNo);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssExtL3FilterTable
 Input       :  The Indices
                IssExtL3FilterNo
                nextIssL3FilterNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssExtL3FilterTable (INT4 i4IssExtL3FilterNo,
                                    INT4 *pi4IssExtL3FilterNoOUT)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pi4IssExtL3FilterNoOUT);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssExtL2FilterTable
 Input       :  The Indices
                IssExtL2FilterNo
                nextIssL2FilterNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssExtL2FilterTable (INT4 i4IssExtL2FilterNo,
                                    INT4 *pi4IssExtL2FilterNoOUT)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(pi4IssExtL2FilterNoOUT);
        return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssExtL2FilterTable
 Input       :  The Indices
                IssExtFilterL2No
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssExtL2FilterTable (INT4 *pi4IssExtL2FilterNo)
{
    UNUSED_PARAM (pi4IssExtL2FilterNo);
    return SNMP_FAILURE;
}

/****************************************************************************
* Function    :  IssExtSnmpLowValidateIndexPortRateTable
* Input       :  i4CtrlIndex(PortCtrl Table Index or Rate Ctrl Table Index)
* Output      :  None
* Returns     :  ISS_SUCCESS/ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowValidateIndexPortRateTable (INT4 i4PortIndex)
{
    UNUSED_PARAM (i4PortIndex);
        return ISS_SUCCESS;
}

/****************************************************************************
* Function    :  IssExtValidateRateCtrlEntry
* Input       :  i4RateCtrlIndex
* Output      :  None
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtValidateRateCtrlEntry (INT4 i4RateCtrlIndex)
{
    UNUSED_PARAM ( i4RateCtrlIndex);
    return ISS_SUCCESS;
}

/****************************************************************************
* Function    :  IssExtSnmpLowGetFirstValidL3FilterTableIndex
* Input       :  None
* Output      :  INT4 *pi4FirstL3FilterIndex
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowGetFirstValidL3FilterTableIndex (INT4 *pi4FirstL3FilterIndex)
{
    UNUSED_PARAM ( pi4FirstL3FilterIndex);
    return ISS_SUCCESS;
}

/****************************************************************************
* Function    :  IssExtSnmpLowGetFirstValidL3FilterTableIndex
* Input       :  INT4 i4IssExt3FilterNo
* Output      :  INT4 *pi4NextL3FilterNo
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowGetNextValidL3FilterTableIndex (INT4 i4IssExtL3FilterNo,
                                             INT4 *pi4NextL3FilterNo)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    UNUSED_PARAM(pi4NextL3FilterNo);
        return ISS_SUCCESS;
}

/****************************************************************************
* Function    :  IssExtSnmpLowGetFirstValidL2FilterTableIndex
* Input       :  None
* Output      :  INT4 *pi4FirstL2FilterIndex
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowGetFirstValidL2FilterTableIndex (INT4 *pi4FirstL2FilterIndex)
{
    UNUSED_PARAM (pi4FirstL2FilterIndex);
    return ISS_FAILURE;
}

/****************************************************************************
* Function    :  IssExtSnmpLowGetFirstValidL2FilterTableIndex
* Input       :  INT4 i4IssExtL2FilterNo
* Output      :  INT4 *pi4NextL2FilterNo
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowGetNextValidL2FilterTableIndex (INT4 i4IssExtL2FilterNo,
                                             INT4 *pi4NextL2FilterNo)
{
    UNUSED_PARAM(i4IssExtL2FilterNo);
    UNUSED_PARAM(pi4NextL2FilterNo);

        return ISS_SUCCESS;
}

/****************************************************************************
* Function    :  IssExtGetL2FilterEntry
* Input       :  INT4 i4IssExtL2FilterNo
* Output      :  None
* Returns     :  tIssL2FilterEntry * 
*****************************************************************************/
tIssL2FilterEntry  *
IssExtGetL2FilterEntry (INT4 i4IssExtL2FilterNo)
{

    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;
    UNUSED_PARAM (i4IssExtL2FilterNo);
            return pIssExtL2FilterEntry;
}

/****************************************************************************
* Function    :  IssExtQualifyL2FilterData 
* Input       :  tIssL2FilterEntry **ppIssExtL2FilterEntry
* Output      :  None
* Returns     :  INT4
*****************************************************************************/
INT4
IssExtQualifyL2FilterData (tIssL2FilterEntry ** ppIssExtL2FilterEntry)
{
    UNUSED_PARAM (ppIssExtL2FilterEntry);
        return ISS_SUCCESS;
}

/****************************************************************************
* Function    :  IssExtGetL3FilterEntry
* Input       :  INT4 i4IssExtL3FilterNo
* Output      :  None
* Returns     :  tIssL3FilterEntry * 
*****************************************************************************/
tIssL3FilterEntry  *
IssExtGetL3FilterEntry (INT4 i4IssExtL3FilterNo)
{

    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;
    UNUSED_PARAM (i4IssExtL3FilterNo);
    return pIssExtL3FilterEntry;
}

/****************************************************************************
* Function    :  IssExtQualifyL3FilterData 
* Input       :  tIssL3FilterEntry **ppIssExtL3FilterEntry
* Output      :  None
* Returns     :  INT4
*****************************************************************************/
INT4
IssExtQualifyL3FilterData (tIssL3FilterEntry ** ppIssExtL3FilterEntry)
{
    UNUSED_PARAM (ppIssExtL3FilterEntry);
        return ISS_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssExtL3FilterTable
 Input       :  The Indices
                IssExtL3FilterNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssExtL3FilterTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_FAILURE;
}

tIssL2FilterEntry  *
IssExtGetL2FilterTableEntry (INT4 i4IssExtL2FilterNo)
{
    tIssL2FilterEntry  *pL2Filter = NULL;
    UNUSED_PARAM (i4IssExtL2FilterNo);
    return (pL2Filter);
}

tIssL3FilterEntry  *
IssExtGetL3FilterTableEntry (INT4 i4IssExtL3FilterNo)
{
    UNUSED_PARAM(i4IssExtL3FilterNo);
    tIssL3FilterEntry  *pL3Filter = NULL;
    return (pL3Filter);
}

/****************************************************************************
* Function    :  IssExtUpdateFilterRefCount
* Description :  This function is used to Incremet or Decrement the Referenc 
*                Conunt of the Given L2 or L3 Filter Entry
* Input       :  u4FilterId     - L2 / L3 Filter Entry Id
*             :  u4FilterType   - L2 (ISS_L2FILTER) / L3 (ISS_L3FILTER)        
*             :  u4Action       - ISS_INCR (Incremet) / ISS_DECR (Decrement)
* Output      :  None
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtUpdateFilterRefCount (UINT4 u4FilterId, UINT4 u4FilterType,
                            UINT4 u4Action)
{
    UNUSED_PARAM(u4FilterId);
    UNUSED_PARAM(u4FilterType);
    UNUSED_PARAM(u4Action);
    return (ISS_SUCCESS);

}
