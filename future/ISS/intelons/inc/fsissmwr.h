#ifndef _FSISSMWR_H
#define _FSISSMWR_H
INT4 GetNextIndexIssMetroL2FilterTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSISSM(VOID);

VOID UnRegisterFSISSM(VOID);
INT4 IssMetroL2FilterOuterEtherTypeGet(tSnmpIndex *, tRetVal *);
INT4 IssMetroL2FilterSVlanIdGet(tSnmpIndex *, tRetVal *);
INT4 IssMetroL2FilterSVlanPriorityGet(tSnmpIndex *, tRetVal *);
INT4 IssMetroL2FilterCVlanPriorityGet(tSnmpIndex *, tRetVal *);
INT4 IssMetroL2FilterPacketTagTypeGet(tSnmpIndex *, tRetVal *);
INT4 IssMetroL2FilterOuterEtherTypeSet(tSnmpIndex *, tRetVal *);
INT4 IssMetroL2FilterSVlanIdSet(tSnmpIndex *, tRetVal *);
INT4 IssMetroL2FilterSVlanPrioritySet(tSnmpIndex *, tRetVal *);
INT4 IssMetroL2FilterCVlanPrioritySet(tSnmpIndex *, tRetVal *);
INT4 IssMetroL2FilterPacketTagTypeSet(tSnmpIndex *, tRetVal *);
INT4 IssMetroL2FilterOuterEtherTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IssMetroL2FilterSVlanIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IssMetroL2FilterSVlanPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IssMetroL2FilterCVlanPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IssMetroL2FilterPacketTagTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IssMetroL2FilterTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);





INT4 GetNextIndexIssMetroL3FilterTable(tSnmpIndex *, tSnmpIndex *);
INT4 IssMetroL3FilterSVlanIdGet(tSnmpIndex *, tRetVal *);
INT4 IssMetroL3FilterSVlanPriorityGet(tSnmpIndex *, tRetVal *);
INT4 IssMetroL3FilterCVlanIdGet(tSnmpIndex *, tRetVal *);
INT4 IssMetroL3FilterCVlanPriorityGet(tSnmpIndex *, tRetVal *);
INT4 IssMetroL3FilterPacketTagTypeGet(tSnmpIndex *, tRetVal *);
INT4 IssMetroL3FilterSVlanIdSet(tSnmpIndex *, tRetVal *);
INT4 IssMetroL3FilterSVlanPrioritySet(tSnmpIndex *, tRetVal *);
INT4 IssMetroL3FilterCVlanIdSet(tSnmpIndex *, tRetVal *);
INT4 IssMetroL3FilterCVlanPrioritySet(tSnmpIndex *, tRetVal *);
INT4 IssMetroL3FilterPacketTagTypeSet(tSnmpIndex *, tRetVal *);
INT4 IssMetroL3FilterSVlanIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IssMetroL3FilterSVlanPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IssMetroL3FilterCVlanIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IssMetroL3FilterCVlanPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IssMetroL3FilterPacketTagTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 IssMetroL3FilterTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);





#endif /* _FSISSMWR_H */
