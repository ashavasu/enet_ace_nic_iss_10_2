/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: issexinc.h,v 1.1.1.1 2015/03/04 10:38:40 siva Exp $
 *
 * Description: This file include all the header files required 
 *              for the operation of the ACL module
 ****************************************************************************/

#ifndef _ISSEXINC_H
#define _ISSEXINC_H

/* Common Includes */
#include "lr.h"
#include "cli.h"
#include "cfa.h"
#include "rmgr.h"
#include "iss.h"
#include "msr.h"
#include "ip.h"

#include "snmccons.h"
#include "snmcdefn.h"
#include "fssnmp.h"


#ifdef NPAPI_WANTED
#include "npapi.h"
#include "issnp.h"
#endif

/* ISS includes */
#include "issmacro.h"
#include "issexmacr.h"
#include "issextdfs.h"
#include "issexglob.h"
#include "issexprot.h"

#include "qosxtd.h"

#ifdef MBSM_WANTED
#include "mbsm.h"
#include "issmbsm.h"
#endif

#endif /* _ISSEXINC_H */
