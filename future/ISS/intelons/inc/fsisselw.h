/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsisselw.h,v 1.2 2016/03/19 13:10:16 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for IssExtRateCtrlTable. */
INT1
nmhValidateIndexInstanceIssExtRateCtrlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssExtRateCtrlTable  */

INT1
nmhGetFirstIndexIssExtRateCtrlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssExtRateCtrlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssExtRateCtrlDLFLimitValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtRateCtrlBCASTLimitValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtRateCtrlMCASTLimitValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtRateCtrlPortRateLimit ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtRateCtrlPortBurstSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtDefaultRateCtrlStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssExtRateCtrlDLFLimitValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtRateCtrlBCASTLimitValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtRateCtrlMCASTLimitValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtRateCtrlPortRateLimit ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtRateCtrlPortBurstSize ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtDefaultRateCtrlStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssExtRateCtrlDLFLimitValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtRateCtrlBCASTLimitValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtRateCtrlMCASTLimitValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtRateCtrlPortRateLimit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtRateCtrlPortBurstSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtDefaultRateCtrlStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssExtRateCtrlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IssExtL2FilterTable. */
INT1
nmhValidateIndexInstanceIssExtL2FilterTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssExtL2FilterTable  */

INT1
nmhGetFirstIndexIssExtL2FilterTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssExtL2FilterTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssExtL2FilterPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtL2FilterEtherType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtL2FilterProtocolType ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssExtL2FilterDstMacAddr ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetIssExtL2FilterSrcMacAddr ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetIssExtL2FilterVlanId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtL2FilterUserPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtL2FilterInPortList ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssExtL2FilterAction ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtL2FilterMatchCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssExtL2FilterStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtL2FilterOutPortList ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssExtL2FilterDirection ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssExtL2FilterPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtL2FilterEtherType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtL2FilterProtocolType ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssExtL2FilterDstMacAddr ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetIssExtL2FilterSrcMacAddr ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetIssExtL2FilterVlanId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtL2FilterUserPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtL2FilterInPortList ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssExtL2FilterAction ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtL2FilterStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtL2FilterOutPortList ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssExtL2FilterDirection ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssExtL2FilterPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtL2FilterEtherType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtL2FilterProtocolType ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssExtL2FilterDstMacAddr ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2IssExtL2FilterSrcMacAddr ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2IssExtL2FilterVlanId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtL2FilterUserPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtL2FilterInPortList ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssExtL2FilterAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtL2FilterStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtL2FilterOutPortList ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssExtL2FilterDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssExtL2FilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IssExtL3FilterTable. */
INT1
nmhValidateIndexInstanceIssExtL3FilterTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssExtL3FilterTable  */

INT1
nmhGetFirstIndexIssExtL3FilterTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssExtL3FilterTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssExtL3FilterPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtL3FilterProtocol ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtL3FilterMessageType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtL3FilterMessageCode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtL3FilterDstIpAddr ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssExtL3FilterSrcIpAddr ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssExtL3FilterDstIpAddrMask ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssExtL3FilterSrcIpAddrMask ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssExtL3FilterMinDstProtPort ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssExtL3FilterMaxDstProtPort ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssExtL3FilterMinSrcProtPort ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssExtL3FilterMaxSrcProtPort ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssExtL3FilterInPortList ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssExtL3FilterOutPortList ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssExtL3FilterAckBit ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtL3FilterRstBit ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtL3FilterTos ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtL3FilterDscp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtL3FilterDirection ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtL3FilterAction ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssExtL3FilterMatchCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssExtL3FilterStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssExtL3FilterPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtL3FilterProtocol ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtL3FilterMessageType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtL3FilterMessageCode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtL3FilterDstIpAddr ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssExtL3FilterSrcIpAddr ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssExtL3FilterDstIpAddrMask ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssExtL3FilterSrcIpAddrMask ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssExtL3FilterMinDstProtPort ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssExtL3FilterMaxDstProtPort ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssExtL3FilterMinSrcProtPort ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssExtL3FilterMaxSrcProtPort ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssExtL3FilterInPortList ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssExtL3FilterOutPortList ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssExtL3FilterAckBit ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtL3FilterRstBit ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtL3FilterTos ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtL3FilterDscp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtL3FilterDirection ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtL3FilterAction ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssExtL3FilterStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssExtL3FilterPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtL3FilterProtocol ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtL3FilterMessageType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtL3FilterMessageCode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtL3FilterDstIpAddr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssExtL3FilterSrcIpAddr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssExtL3FilterDstIpAddrMask ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssExtL3FilterSrcIpAddrMask ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssExtL3FilterMinDstProtPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssExtL3FilterMaxDstProtPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssExtL3FilterMinSrcProtPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssExtL3FilterMaxSrcProtPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssExtL3FilterInPortList ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssExtL3FilterOutPortList ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssExtL3FilterAckBit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtL3FilterRstBit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtL3FilterTos ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtL3FilterDscp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtL3FilterDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtL3FilterAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssExtL3FilterStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssExtL3FilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT4 IssExtSnmpLowValidateIndexPortRateTable (INT4 i4PortIndex);

INT4 IssExtValidateRateCtrlEntry (INT4 i4RateCtrlIndex);

INT4 IssExtSnmpLowGetFirstValidL3FilterTableIndex (INT4 *pi4FirstL3FilterIndex);

INT4 IssExtSnmpLowGetNextValidL3FilterTableIndex (INT4 i4IssL3FilterNo,
                                               INT4 *pi4NextL3FilterNo);
INT4 IssExtSnmpLowGetFirstValidL2FilterTableIndex (INT4 *pi4FirstL2FilterIndex);

INT4 IssExtSnmpLowGetNextValidL2FilterTableIndex (INT4 i4IssL2FilterNo,
                                               INT4 *pi4NextL2FilterNo);

