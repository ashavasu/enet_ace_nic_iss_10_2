/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissadb.h,v 1.1.1.1 2015/03/04 10:38:40 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSISSADB_H
#define _FSISSADB_H

UINT1 IssAclRateCtrlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 IssAclL2FilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 IssAclL3FilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fsissa [] ={1,3,6,1,4,1,29601,2,21};
tSNMP_OID_TYPE fsissaOID = {9, fsissa};


UINT4 IssAclRateCtrlIndex [ ] ={1,3,6,1,4,1,29601,2,21,1,1,1,1};
UINT4 IssAclRateCtrlDLFLimitValue [ ] ={1,3,6,1,4,1,29601,2,21,1,1,1,2};
UINT4 IssAclRateCtrlBCASTLimitValue [ ] ={1,3,6,1,4,1,29601,2,21,1,1,1,3};
UINT4 IssAclRateCtrlMCASTLimitValue [ ] ={1,3,6,1,4,1,29601,2,21,1,1,1,4};
UINT4 IssAclRateCtrlPortRateLimit [ ] ={1,3,6,1,4,1,29601,2,21,1,1,1,5};
UINT4 IssAclRateCtrlPortBurstSize [ ] ={1,3,6,1,4,1,29601,2,21,1,1,1,6};
UINT4 IssAclL2FilterNo [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,1};
UINT4 IssAclL2FilterPriority [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,2};
UINT4 IssAclL2FilterEtherType [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,3};
UINT4 IssAclL2FilterProtocolType [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,4};
UINT4 IssAclL2FilterDstMacAddr [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,5};
UINT4 IssAclL2FilterSrcMacAddr [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,6};
UINT4 IssAclL2FilterVlanId [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,7};
UINT4 IssAclL2FilterInPortList [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,8};
UINT4 IssAclL2FilterAction [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,9};
UINT4 IssAclL2FilterMatchCount [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,10};
UINT4 IssAclL2FilterStatus [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,11};
UINT4 IssAclL2FilterOutPortList [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,12};
UINT4 IssAclL2FilterDirection [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,13};
UINT4 IssAclL3FilterNo [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,1};
UINT4 IssAclL3FilterPriority [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,2};
UINT4 IssAclL3FilterProtocol [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,3};
UINT4 IssAclL3FilterMessageType [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,4};
UINT4 IssAclL3FilterMessageCode [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,5};
UINT4 IssAclL3FilteAddrType [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,6};
UINT4 IssAclL3FilterDstIpAddr [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,7};
UINT4 IssAclL3FilterSrcIpAddr [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,8};
UINT4 IssAclL3FilterDstIpAddrPrefixLength [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,9};
UINT4 IssAclL3FilterSrcIpAddrPrefixLength [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,10};
UINT4 IssAclL3FilterMinDstProtPort [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,11};
UINT4 IssAclL3FilterMaxDstProtPort [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,12};
UINT4 IssAclL3FilterMinSrcProtPort [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,13};
UINT4 IssAclL3FilterMaxSrcProtPort [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,14};
UINT4 IssAclL3FilterInPortList [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,15};
UINT4 IssAclL3FilterOutPortList [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,16};
UINT4 IssAclL3FilterAckBit [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,17};
UINT4 IssAclL3FilterRstBit [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,18};
UINT4 IssAclL3FilterTos [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,19};
UINT4 IssAclL3FilterDscp [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,20};
UINT4 IssAclL3FilterDirection [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,21};
UINT4 IssAclL3FilterAction [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,22};
UINT4 IssAclL3FilterMatchCount [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,23};
UINT4 IssAclL3FilterFlowId [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,24};
UINT4 IssAclL3FilterStatus [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,25};


tMbDbEntry fsissaMibEntry[]= {

{{13,IssAclRateCtrlIndex}, GetNextIndexIssAclRateCtrlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssAclRateCtrlTableINDEX, 1, 0, 0, NULL},

{{13,IssAclRateCtrlDLFLimitValue}, GetNextIndexIssAclRateCtrlTable, IssAclRateCtrlDLFLimitValueGet, IssAclRateCtrlDLFLimitValueSet, IssAclRateCtrlDLFLimitValueTest, IssAclRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclRateCtrlTableINDEX, 1, 0, 0, "0"},

{{13,IssAclRateCtrlBCASTLimitValue}, GetNextIndexIssAclRateCtrlTable, IssAclRateCtrlBCASTLimitValueGet, IssAclRateCtrlBCASTLimitValueSet, IssAclRateCtrlBCASTLimitValueTest, IssAclRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclRateCtrlTableINDEX, 1, 0, 0, "0"},

{{13,IssAclRateCtrlMCASTLimitValue}, GetNextIndexIssAclRateCtrlTable, IssAclRateCtrlMCASTLimitValueGet, IssAclRateCtrlMCASTLimitValueSet, IssAclRateCtrlMCASTLimitValueTest, IssAclRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclRateCtrlTableINDEX, 1, 0, 0, "0"},

{{13,IssAclRateCtrlPortRateLimit}, GetNextIndexIssAclRateCtrlTable, IssAclRateCtrlPortRateLimitGet, IssAclRateCtrlPortRateLimitSet, IssAclRateCtrlPortRateLimitTest, IssAclRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclRateCtrlTableINDEX, 1, 0, 0, NULL},

{{13,IssAclRateCtrlPortBurstSize}, GetNextIndexIssAclRateCtrlTable, IssAclRateCtrlPortBurstSizeGet, IssAclRateCtrlPortBurstSizeSet, IssAclRateCtrlPortBurstSizeTest, IssAclRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclRateCtrlTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL2FilterNo}, GetNextIndexIssAclL2FilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssAclL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL2FilterPriority}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterPriorityGet, IssAclL2FilterPrioritySet, IssAclL2FilterPriorityTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 0, "1"},

{{13,IssAclL2FilterEtherType}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterEtherTypeGet, IssAclL2FilterEtherTypeSet, IssAclL2FilterEtherTypeTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL2FilterProtocolType}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterProtocolTypeGet, IssAclL2FilterProtocolTypeSet, IssAclL2FilterProtocolTypeTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssAclL2FilterDstMacAddr}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterDstMacAddrGet, IssAclL2FilterDstMacAddrSet, IssAclL2FilterDstMacAddrTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL2FilterSrcMacAddr}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterSrcMacAddrGet, IssAclL2FilterSrcMacAddrSet, IssAclL2FilterSrcMacAddrTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL2FilterVlanId}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterVlanIdGet, IssAclL2FilterVlanIdSet, IssAclL2FilterVlanIdTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssAclL2FilterInPortList}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterInPortListGet, IssAclL2FilterInPortListSet, IssAclL2FilterInPortListTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL2FilterAction}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterActionGet, IssAclL2FilterActionSet, IssAclL2FilterActionTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 0, "1"},

{{13,IssAclL2FilterMatchCount}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterMatchCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IssAclL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL2FilterStatus}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterStatusGet, IssAclL2FilterStatusSet, IssAclL2FilterStatusTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 1, NULL},

{{13,IssAclL2FilterOutPortList}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterOutPortListGet, IssAclL2FilterOutPortListSet, IssAclL2FilterOutPortListTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL2FilterDirection}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterDirectionGet, IssAclL2FilterDirectionSet, IssAclL2FilterDirectionTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 0, "1"},

{{13,IssAclL3FilterNo}, GetNextIndexIssAclL3FilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssAclL3FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL3FilterPriority}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterPriorityGet, IssAclL3FilterPrioritySet, IssAclL3FilterPriorityTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "1"},

{{13,IssAclL3FilterProtocol}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterProtocolGet, IssAclL3FilterProtocolSet, IssAclL3FilterProtocolTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "255"},

{{13,IssAclL3FilterMessageType}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterMessageTypeGet, IssAclL3FilterMessageTypeSet, IssAclL3FilterMessageTypeTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "255"},

{{13,IssAclL3FilterMessageCode}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterMessageCodeGet, IssAclL3FilterMessageCodeSet, IssAclL3FilterMessageCodeTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "255"},

{{13,IssAclL3FilteAddrType}, GetNextIndexIssAclL3FilterTable, IssAclL3FilteAddrTypeGet, IssAclL3FilteAddrTypeSet, IssAclL3FilteAddrTypeTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL3FilterDstIpAddr}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterDstIpAddrGet, IssAclL3FilterDstIpAddrSet, IssAclL3FilterDstIpAddrTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL3FilterSrcIpAddr}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterSrcIpAddrGet, IssAclL3FilterSrcIpAddrSet, IssAclL3FilterSrcIpAddrTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL3FilterDstIpAddrPrefixLength}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterDstIpAddrPrefixLengthGet, IssAclL3FilterDstIpAddrPrefixLengthSet, IssAclL3FilterDstIpAddrPrefixLengthTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssAclL3FilterSrcIpAddrPrefixLength}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterSrcIpAddrPrefixLengthGet, IssAclL3FilterSrcIpAddrPrefixLengthSet, IssAclL3FilterSrcIpAddrPrefixLengthTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssAclL3FilterMinDstProtPort}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterMinDstProtPortGet, IssAclL3FilterMinDstProtPortSet, IssAclL3FilterMinDstProtPortTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssAclL3FilterMaxDstProtPort}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterMaxDstProtPortGet, IssAclL3FilterMaxDstProtPortSet, IssAclL3FilterMaxDstProtPortTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "65535"},

{{13,IssAclL3FilterMinSrcProtPort}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterMinSrcProtPortGet, IssAclL3FilterMinSrcProtPortSet, IssAclL3FilterMinSrcProtPortTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssAclL3FilterMaxSrcProtPort}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterMaxSrcProtPortGet, IssAclL3FilterMaxSrcProtPortSet, IssAclL3FilterMaxSrcProtPortTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "65535"},

{{13,IssAclL3FilterInPortList}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterInPortListGet, IssAclL3FilterInPortListSet, IssAclL3FilterInPortListTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL3FilterOutPortList}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterOutPortListGet, IssAclL3FilterOutPortListSet, IssAclL3FilterOutPortListTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL3FilterAckBit}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterAckBitGet, IssAclL3FilterAckBitSet, IssAclL3FilterAckBitTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "3"},

{{13,IssAclL3FilterRstBit}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterRstBitGet, IssAclL3FilterRstBitSet, IssAclL3FilterRstBitTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "3"},

{{13,IssAclL3FilterTos}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterTosGet, IssAclL3FilterTosSet, IssAclL3FilterTosTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL3FilterDscp}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterDscpGet, IssAclL3FilterDscpSet, IssAclL3FilterDscpTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssAclL3FilterDirection}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterDirectionGet, IssAclL3FilterDirectionSet, IssAclL3FilterDirectionTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "1"},

{{13,IssAclL3FilterAction}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterActionGet, IssAclL3FilterActionSet, IssAclL3FilterActionTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "1"},

{{13,IssAclL3FilterMatchCount}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterMatchCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IssAclL3FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL3FilterFlowId}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterFlowIdGet, IssAclL3FilterFlowIdSet, IssAclL3FilterFlowIdTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL3FilterStatus}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterStatusGet, IssAclL3FilterStatusSet, IssAclL3FilterStatusTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 1, NULL},
};
tMibData fsissaEntry = { 44, fsissaMibEntry };
#endif /* _FSISSADB_H */

