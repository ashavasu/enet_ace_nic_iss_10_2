/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissalw.h,v 1.2 2011/04/08 10:32:34 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for IssAclRateCtrlTable. */
INT1
nmhValidateIndexInstanceIssAclRateCtrlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssAclRateCtrlTable  */

INT1
nmhGetFirstIndexIssAclRateCtrlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssAclRateCtrlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssAclRateCtrlDLFLimitValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssAclRateCtrlBCASTLimitValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssAclRateCtrlMCASTLimitValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssAclRateCtrlPortRateLimit ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssAclRateCtrlPortBurstSize ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssAclRateCtrlDLFLimitValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssAclRateCtrlBCASTLimitValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssAclRateCtrlMCASTLimitValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssAclRateCtrlPortRateLimit ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssAclRateCtrlPortBurstSize ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssAclRateCtrlDLFLimitValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssAclRateCtrlBCASTLimitValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssAclRateCtrlMCASTLimitValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssAclRateCtrlPortRateLimit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssAclRateCtrlPortBurstSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssAclRateCtrlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IssAclL2FilterTable. */
INT1
nmhValidateIndexInstanceIssAclL2FilterTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssAclL2FilterTable  */

INT1
nmhGetFirstIndexIssAclL2FilterTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssAclL2FilterTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssAclL2FilterPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssAclL2FilterEtherType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssAclL2FilterProtocolType ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssAclL2FilterDstMacAddr ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetIssAclL2FilterSrcMacAddr ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetIssAclL2FilterVlanId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssAclL2FilterInPortList ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssAclL2FilterAction ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssAclL2FilterMatchCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssAclL2FilterStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssAclL2FilterOutPortList ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssAclL2FilterDirection ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssAclL2FilterPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssAclL2FilterEtherType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssAclL2FilterProtocolType ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssAclL2FilterDstMacAddr ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetIssAclL2FilterSrcMacAddr ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetIssAclL2FilterVlanId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssAclL2FilterInPortList ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssAclL2FilterAction ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssAclL2FilterStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssAclL2FilterOutPortList ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssAclL2FilterDirection ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssAclL2FilterPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssAclL2FilterEtherType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssAclL2FilterProtocolType ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssAclL2FilterDstMacAddr ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2IssAclL2FilterSrcMacAddr ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2IssAclL2FilterVlanId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssAclL2FilterInPortList ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssAclL2FilterAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssAclL2FilterStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssAclL2FilterOutPortList ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssAclL2FilterDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssAclL2FilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IssAclL3FilterTable. */
INT1
nmhValidateIndexInstanceIssAclL3FilterTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssAclL3FilterTable  */

INT1
nmhGetFirstIndexIssAclL3FilterTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssAclL3FilterTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssAclL3FilterPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssAclL3FilterProtocol ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssAclL3FilterMessageType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssAclL3FilterMessageCode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssAclL3FilteAddrType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssAclL3FilterDstIpAddr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssAclL3FilterSrcIpAddr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssAclL3FilterDstIpAddrPrefixLength ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssAclL3FilterSrcIpAddrPrefixLength ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssAclL3FilterMinDstProtPort ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssAclL3FilterMaxDstProtPort ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssAclL3FilterMinSrcProtPort ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssAclL3FilterMaxSrcProtPort ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssAclL3FilterInPortList ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssAclL3FilterOutPortList ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssAclL3FilterAckBit ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssAclL3FilterRstBit ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssAclL3FilterTos ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssAclL3FilterDscp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssAclL3FilterDirection ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssAclL3FilterAction ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssAclL3FilterMatchCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssAclL3FilterFlowId ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssAclL3FilterStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssAclL3FilterPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssAclL3FilterProtocol ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssAclL3FilterMessageType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssAclL3FilterMessageCode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssAclL3FilteAddrType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssAclL3FilterDstIpAddr ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssAclL3FilterSrcIpAddr ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssAclL3FilterDstIpAddrPrefixLength ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssAclL3FilterSrcIpAddrPrefixLength ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssAclL3FilterMinDstProtPort ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssAclL3FilterMaxDstProtPort ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssAclL3FilterMinSrcProtPort ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssAclL3FilterMaxSrcProtPort ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssAclL3FilterInPortList ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssAclL3FilterOutPortList ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssAclL3FilterAckBit ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssAclL3FilterRstBit ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssAclL3FilterTos ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssAclL3FilterDscp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssAclL3FilterDirection ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssAclL3FilterAction ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssAclL3FilterFlowId ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssAclL3FilterStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssAclL3FilterPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssAclL3FilterProtocol ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssAclL3FilterMessageType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssAclL3FilterMessageCode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssAclL3FilteAddrType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssAclL3FilterDstIpAddr ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssAclL3FilterSrcIpAddr ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssAclL3FilterDstIpAddrPrefixLength ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssAclL3FilterMinDstProtPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssAclL3FilterMaxDstProtPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssAclL3FilterMinSrcProtPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssAclL3FilterMaxSrcProtPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssAclL3FilterInPortList ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssAclL3FilterOutPortList ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssAclL3FilterAckBit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssAclL3FilterRstBit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssAclL3FilterTos ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssAclL3FilterDscp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssAclL3FilterDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssAclL3FilterAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssAclL3FilterFlowId ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssAclL3FilterStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssAclL3FilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
