/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissmlw.h,v 1.2 2011/04/08 10:32:26 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for IssMetroL2FilterTable. */
INT1
nmhValidateIndexInstanceIssMetroL2FilterTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssMetroL2FilterTable  */

INT1
nmhGetFirstIndexIssMetroL2FilterTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssMetroL2FilterTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssMetroL2FilterOuterEtherType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssMetroL2FilterSVlanId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssMetroL2FilterSVlanPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssMetroL2FilterCVlanPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssMetroL2FilterPacketTagType ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssMetroL2FilterOuterEtherType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssMetroL2FilterSVlanId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssMetroL2FilterSVlanPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssMetroL2FilterCVlanPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssMetroL2FilterPacketTagType ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssMetroL2FilterOuterEtherType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssMetroL2FilterSVlanId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssMetroL2FilterSVlanPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssMetroL2FilterCVlanPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssMetroL2FilterPacketTagType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssMetroL2FilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IssMetroL3FilterTable. */
INT1
nmhValidateIndexInstanceIssMetroL3FilterTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssMetroL3FilterTable  */

INT1
nmhGetFirstIndexIssMetroL3FilterTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssMetroL3FilterTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssMetroL3FilterSVlanId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssMetroL3FilterSVlanPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssMetroL3FilterCVlanId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssMetroL3FilterCVlanPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssMetroL3FilterPacketTagType ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssMetroL3FilterSVlanId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssMetroL3FilterSVlanPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssMetroL3FilterCVlanId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssMetroL3FilterCVlanPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssMetroL3FilterPacketTagType ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssMetroL3FilterSVlanId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssMetroL3FilterSVlanPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssMetroL3FilterCVlanId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssMetroL3FilterCVlanPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssMetroL3FilterPacketTagType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssMetroL3FilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
