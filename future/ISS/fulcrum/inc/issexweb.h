#ifndef _ISSEXWEB_H
#define _ISSEXWEB_H

#include "webiss.h"
#include "isshttp.h"
#include "aclcli.h"
#include "fsisselw.h"
#include "iss.h"

#define   ACL_EXTENDED_START 11

extern INT1
nmhTestv2DiffServMultiFieldClfrStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern UINT1 nmhTestv2IssAclL3FilteAddrType(UINT4 * ,
                                            INT4 , INT4 );
extern INT1
nmhSetDiffServMultiFieldClfrStorage ARG_LIST((UINT4  ,INT4 ));

extern INT1
nmhGetDiffServMultiFieldClfrStorage ARG_LIST((UINT4 ,INT4 *));

extern UINT1 nmhSetIssAclL3FilteAddrType(INT4 ,INT4);
extern UINT1 nmhTestv2IssAclL3FilterSrcIpAddr (UINT4 * , INT4 ,tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhSetIssAclL3FilterSrcIpAddr (INT4, tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhTestv2IssAclL3FilterDstIpAddr (UINT4 *e, INT4, tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhSetIssAclL3FilterDstIpAddr (INT4, tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhTestv2IssAclL3FilterDstIpAddrPrefixLength  (UINT4*, INT4, UINT4);
extern UINT1 nmhSetIssAclL3FilterDstIpAddrPrefixLength (INT4, UINT4);
extern UINT1 nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength (UINT4 *, INT4, UINT4);
extern UINT1 nmhSetIssAclL3FilterSrcIpAddrPrefixLength (INT4, UINT4);
extern UINT1 nmhTestv2IssAclL3FilterFlowId (UINT4 *, INT4, UINT4);
extern UINT1 nmhSetIssAclL3FilterFlowId (INT4 , UINT4);
extern UINT1 nmhGetIssAclL3FilteAddrType (INT4, INT4 *);
extern UINT1 nmhGetIssAclL3FilterSrcIpAddr (INT4, tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhGetIssAclL3FilterDstIpAddr (INT4, tSNMP_OCTET_STRING_TYPE *);
extern UINT1 nmhGetIssAclL3FilterDstIpAddrPrefixLength (INT4, UINT4 *);
extern UINT1 nmhGetIssAclL3FilterSrcIpAddrPrefixLength (INT4, UINT4 *);
extern UINT1 nmhGetIssAclL3FilterFlowId (INT4, UINT4 *);
extern INT1  nmhGetIssL3FilterOutPortList (INT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1  nmhGetIssL3FilterInPortList (INT4, tSNMP_OCTET_STRING_TYPE *);

/* Prototypes for specific pages processing */
INT4 IssProcessCustomPages (tHttp * pHttp);
VOID IssRedirectMacFilterPage (tHttp *pHttp);
VOID IssFulcrumProcessIPFilterConfPage (tHttp * pHttp);
VOID IssFulcrumProcessIPStdFilterConfPage (tHttp * pHttp);
VOID IssFulcrumProcessMACFilterConfPage (tHttp * pHttp);
VOID IssFulcrumProcessIPFilterConfPageSet (tHttp * pHttp);
VOID IssFulcrumProcessIPFilterConfPageGet (tHttp * pHttp);
VOID IssFulcrumProcessIPStdFilterConfPageGet (tHttp * pHttp);
VOID IssFulcrumProcessIPStdFilterConfPageSet (tHttp * pHttp);
VOID IssFulcrumProcessMACFilterConfPageGet (tHttp * pHttp);
VOID IssFulcrumProcessMACFilterConfPageSet (tHttp * pHttp);

tSpecificPage asIssTargetpages [] ={
    {"fulcrum_ip_stdqosfilter.html", IssFulcrumProcessIPFilterConfPage},
    {"fulcrum_ip_filterconf.html", IssFulcrumProcessIPFilterConfPage},
    {"fulcrum_ip_stdfilterconf.html", IssFulcrumProcessIPStdFilterConfPage},
    {"fulcrum_mac_filterconf.html", IssFulcrumProcessMACFilterConfPage},
    {"", NULL}

};

INT4 IssProcessCustomPages (tHttp * pHttp);

#endif
