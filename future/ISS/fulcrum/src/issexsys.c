/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: issexsys.c,v 1.19 2016/06/14 12:31:18 siva Exp $
 *
 * Description: This file contains the ACL module initialisation routines
 *****************************************************************************/

#ifndef _ISSEXSYS_C
#define _ISSEXSYS_C

#include "issexinc.h"
#include "fsissewr.h"
#include "fsissawr.h"

UINT4               gu4TrafficSeprtnControl = ACL_TRAFFICSEPRTN_CTRL_NONE;

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssExInit                                        */
/*                                                                          */
/*    Description        : This function initialises the memory pools used  */
/*                         by the ISS Extension Module.                     */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ISS_FAILURE                          */
/****************************************************************************/
INT4
IssExInit ()
{
    /* Allocate memory pool for Rate ctrl table */
    if (ISS_CREATE_RATEENTRY_MEM_POOL (ISS_RATEENTRY_MEMBLK_SIZE,
                                       ISS_RATEENTRY_MEMBLK_COUNT,
                                       &(ISS_RATEENTRY_POOL_ID)) != MEM_SUCCESS)
    {
        return ISS_FAILURE;
    }

    /* Call the Hardware routine to initialise filters in hardware */

    /* Allocate memory pool for L2 Filter table */
    if (ISS_CREATE_L2FILTERENTRY_MEM_POOL (ISS_L2FILTERENTRY_MEMBLK_SIZE,
                                           ISS_L2FILTERENTRY_MEMBLK_COUNT,
                                           &(ISS_L2FILTERENTRY_POOL_ID))
        != MEM_SUCCESS)
    {
        return ISS_FAILURE;
    }

    /* Initialise the SLL lists for L2 Filter Table */
    ISS_SLL_INIT (&(ISS_L2FILTER_LIST));

    /* Allocate memory pool for L3 Filter table */
    if (ISS_CREATE_L3FILTERENTRY_MEM_POOL (ISS_L3FILTERENTRY_MEMBLK_SIZE,
                                           ISS_L3FILTERENTRY_MEMBLK_COUNT,
                                           &(ISS_L3FILTERENTRY_POOL_ID))
        != MEM_SUCCESS)
    {
        return ISS_FAILURE;
    }

    /* Initialise the SLL lists for L3 Filter Table */
    ISS_SLL_INIT (&(ISS_L3FILTER_LIST));

#ifdef SNMP_2_WANTED
    RegisterFSISSE ();
    RegisterFSISSM ();
    RegisterFSISSA ();
#endif

    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssSetDefaultRateCtrlValues                          */
/*                                                                           */
/* Description        : This function is called from IssCreatePort() to      */
/*                      initialise the Rate table entry with default values  */
/*                                                                           */
/* Input(s)           : tIssRateCtrlEntry *                                  */
/*                      u2PortIndex - port index                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IssSetDefaultRateCtrlValues (UINT2 u2PortIndex,
                             tIssRateCtrlEntry * pIssRateCtrlEntry)
{
    /* Setting the default values for the Rate Entry */
    pIssRateCtrlEntry->u4IssRateCtrlDLFLimitValue = 0;
    pIssRateCtrlEntry->u4IssRateCtrlBCASTLimitValue = 0;
    pIssRateCtrlEntry->u4IssRateCtrlMCASTLimitValue = 0;
    pIssRateCtrlEntry->i4IssRateCtrlPortLimitRate = 0;
    pIssRateCtrlEntry->i4IssRateCtrlPortBurstRate = 0;

#ifdef NPAPI_WANTED
    IsssysIssHwSetRateLimitingValue ((UINT4) u2PortIndex,
                                     ISS_RATE_DLF,
                                     (INT4) pIssRateCtrlEntry->
                                     u4IssRateCtrlDLFLimitValue);

    IsssysIssHwSetRateLimitingValue ((UINT4) u2PortIndex,
                                     ISS_RATE_BCAST,
                                     (INT4) pIssRateCtrlEntry->
                                     u4IssRateCtrlBCASTLimitValue);

    IsssysIssHwSetRateLimitingValue ((UINT4) u2PortIndex,
                                     ISS_RATE_MCAST,
                                     (INT4) pIssRateCtrlEntry->
                                     u4IssRateCtrlMCASTLimitValue);

    IsssysIssHwSetPortEgressPktRate ((UINT4) u2PortIndex, pIssRateCtrlEntry->
                                     i4IssRateCtrlPortLimitRate,
                                     pIssRateCtrlEntry->
                                     i4IssRateCtrlPortBurstRate);
#else
    UNUSED_PARAM (u2PortIndex);
#endif
}

/*****************************************************************************/
/* Function Name      : IssExCreatePortRateCtrl                              */
/*                                                                           */
/* Description        : This function allocates memory blocks for the port   */
/*                      based rate control info and sets the default values  */
/*                      for the entries.                                     */
/*                                                                           */
/* Input(s)           : u2PortIndex                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*****************************************************************************/
INT4
IssExCreatePortRateCtrl (UINT2 u2PortIndex, tIssTableName IssTableFlag)
{
    tIssRateCtrlEntry  *pIssRateCtrlEntry = NULL;

    if ((IssTableFlag == ISS_ALL_TABLES)
        || (IssTableFlag == ISS_RATECTRL_TABLE))
    {
        /* Check if an entry for Rate ctrl index exists */
        if (gIssExGlobalInfo.apIssRateCtrlEntry[u2PortIndex] != NULL)
        {
            return ISS_SUCCESS;
        }

        /* Allocate a block for Rate Ctrl entry */
        if (ISS_RATEENTRY_ALLOC_MEM_BLOCK (&pIssRateCtrlEntry) != MEM_SUCCESS)
        {
            return ISS_FAILURE;
        }
        /* Memset the Rate Ctrl entry to zero */
        ISS_MEMSET (pIssRateCtrlEntry, 0, sizeof (tIssRateCtrlEntry));
        gIssExGlobalInfo.apIssRateCtrlEntry[u2PortIndex] = pIssRateCtrlEntry;
        IssSetDefaultRateCtrlValues (u2PortIndex, pIssRateCtrlEntry);
    }

    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssExDeletePortRateCtrl                              */
/*                                                                           */
/* Description        : This function releases memory blocks of the port     */
/*                      based rate control info                              */
/*                                                                           */
/* Input(s)           : u2PortIndex                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*****************************************************************************/
INT4
IssExDeletePortRateCtrl (UINT2 u2PortIndex)
{
    tIssRateCtrlEntry  *pIssRateCtrlEntry = NULL;

    pIssRateCtrlEntry = gIssExGlobalInfo.apIssRateCtrlEntry[u2PortIndex];

    /* Release the RateCtrl Mem Block to the Pool */
    if (pIssRateCtrlEntry != NULL)
    {
        ISS_RATEENTRY_FREE_MEM_BLOCK (pIssRateCtrlEntry);
    }

    gIssExGlobalInfo.apIssRateCtrlEntry[u2PortIndex] = NULL;

    return ISS_SUCCESS;
}

/****************************************************************************
* Function    :  IssGetL3FilterAddrType
* Input       :  i4IssL3FilterNo    : Filter number
* Output      :  pElement           : Address Type (QOS_IPV4 / QOS_IPV6) 
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
UINT1
IssGetL3FilterAddrType (UINT4 u4IssL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry ((INT4) u4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = (INT4) pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType;
        return ISS_SUCCESS;
    }

    return ISS_FAILURE;

}

/****************************************************************************
 Function    :  IssSetL3FilterAddrType
 Input       :  i4IssL3FilterNo : Filter number 
                i4AddrType      : QOS_IPV4 / QOS_IPV6                
 Output      :  NONE                                      
 Returns     :  ISS_SUCCESS or ISS_FAILURE
****************************************************************************/
INT1
IssSetL3FilterAddrType (INT4 i4IssL3FilterNo, INT4 i4AddrType)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);
    if (pIssL3FilterEntry == NULL)
    {
        return ISS_FAILURE;
    }

    if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == i4AddrType)
    {
        return ISS_SUCCESS;
    }
    pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType = i4AddrType;
    return ISS_SUCCESS;
}

/****************************************************************************
* Function    :  IssGetL3FilterDstPrefixLength
* Input       :  i4IssL3FilterNo : Filter number              
* Output      :  pElement        : Destination Prefix Length
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
UINT1
IssGetL3FilterDstPrefixLength (INT4 i4IssL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement =
            (INT4) pIssL3FilterEntry->
            u4IssL3FilterMultiFieldClfrDstPrefixLength;
        return ISS_SUCCESS;
    }

    return ISS_FAILURE;

}

/****************************************************************************
 Function    :  IssSetL3FilterDstPrefixLength
 Input       :  i4IssL3FilterNo     : Filter number
                i4DstPrefixLength   : Destination Prefix Length 
 Output      :  NONE
 Returns     :  ISS_SUCCESS / ISS_FAILURE
****************************************************************************/
INT1
IssSetL3FilterDstPrefixLength (INT4 i4IssL3FilterNo, INT4 i4DstPrefixLength)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    UINT4               u4Mask;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);
    if (pIssL3FilterEntry == NULL)
    {
        return ISS_FAILURE;
    }

    if (pIssL3FilterEntry->u4IssL3FilterMultiFieldClfrDstPrefixLength ==
        (UINT4) i4DstPrefixLength)
    {
        return ISS_SUCCESS;
    }
    pIssL3FilterEntry->u4IssL3FilterMultiFieldClfrDstPrefixLength =
        i4DstPrefixLength;
    IssUtlConvertPrefixToMask (i4DstPrefixLength, &u4Mask);
    pIssL3FilterEntry->u4IssL3FilterDstIpAddrMask = u4Mask;
    return ISS_SUCCESS;
}

/****************************************************************************
* Function    :  IssGetL3FilterSrcPrefixLength
* Input       :  i4IssL3FilterNo     : Filter number
*                pElement            : Source Prefix Length
* Output      :  None
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
UINT1
IssGetL3FilterSrcPrefixLength (INT4 i4IssL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement =
            (INT4) pIssL3FilterEntry->
            u4IssL3FilterMultiFieldClfrSrcPrefixLength;
        return ISS_SUCCESS;
    }

    return ISS_FAILURE;
}

/****************************************************************************
 Function    :  IssSetL3FilterSrcPrefixLength
 Input       :  i4IssL3FilterNo      : Filter number
                i4SrcPrefixLength    : Source Prefix Length
 Output      :  NONE 
 Returns     :  ISS_SUCCESS / ISS_FAILURE
****************************************************************************/
INT1
IssSetL3FilterSrcPrefixLength (INT4 i4IssL3FilterNo, INT4 i4SrcPrefixLength)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    UINT4               u4Mask;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        return ISS_FAILURE;
    }

    if (pIssL3FilterEntry->u4IssL3FilterMultiFieldClfrSrcPrefixLength ==
        (UINT4) i4SrcPrefixLength)
    {
        return ISS_SUCCESS;
    }
    pIssL3FilterEntry->u4IssL3FilterMultiFieldClfrSrcPrefixLength =
        (UINT4) i4SrcPrefixLength;
    IssUtlConvertPrefixToMask (i4SrcPrefixLength, &u4Mask);
    pIssL3FilterEntry->u4IssL3FilterSrcIpAddrMask = u4Mask;
    return ISS_SUCCESS;
}

/****************************************************************************
* Function    :  IssGetL3FilterControlFlowId
* Input       :  i4IssL3FilterNo      : Filter number
* Output      :  pElement             : IPv6 Packet Flow ID
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
UINT1
IssGetL3FilterControlFlowId (INT4 i4IssL3FilterNo, UINT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = (INT4) pIssL3FilterEntry->u4IssL3MultiFieldClfrFlowId;
        return ISS_SUCCESS;
    }

    return ISS_FAILURE;
}

/****************************************************************************
 Function    :  IssSetL3FilterControlFlowId
 Input       :  i4IssL3FilterNo     : Filter number
                u4FlowId            : IPv6 Packet Flow ID
 Output      :  NONE
 Returns     :  ISS_SUCCESS / ISS_FAILURE
****************************************************************************/
INT1
IssSetL3FilterControlFlowId (INT4 i4IssL3FilterNo, UINT4 u4FlowId)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);
    if (pIssL3FilterEntry == NULL)
    {
        return ISS_FAILURE;
    }

    if (pIssL3FilterEntry->u4IssL3MultiFieldClfrFlowId == u4FlowId)
    {
        return ISS_SUCCESS;
    }
    if (pIssL3FilterEntry != NULL)
    {
        pIssL3FilterEntry->u4IssL3MultiFieldClfrFlowId = u4FlowId;
        return ISS_SUCCESS;
    }
    return ISS_FAILURE;
}

/****************************************************************************
* Function    :  IssGetL3FilterStorageType
* Input       :  i4IssL3FilterNo     : Filter number
* Output      :  pElement            : Storage Type
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
UINT1
IssGetL3FilterStorageType (INT4 i4IssL3FilterNo, UINT4 *pElement)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pElement = (INT4) pIssL3FilterEntry->i4StorageType;
        return ISS_SUCCESS;
    }

    return ISS_FAILURE;
}

/****************************************************************************
 Function    :  IssSetL3FilterStorageType
 Input       :  i4IssL3FilterNo     : Filter number
                i4StorageType       : Storage Type
 Output      :  NONE 
 Returns     :  ISS_SUCCESS / ISS_FAILURE
****************************************************************************/
INT1
IssSetL3FilterStorageType (INT4 i4IssL3FilterNo, INT4 i4StorageType)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssL3FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        return ISS_FAILURE;
    }

    if (pIssL3FilterEntry->i4StorageType == i4StorageType)
    {
        return ISS_SUCCESS;
    }
    pIssL3FilterEntry->i4StorageType = i4StorageType;
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssUtlConvertPrefixToMask                            */
/*                                                                           */
/* Description        : This function converts the given prefix to Mask      */
/*                                                                           */
/* Input(s)           : u4Prefix : Prefix                                    */
/*                                                                           */
/* Output(s)          : u4Mask : Mask                                        */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
IssUtlConvertPrefixToMask (UINT4 u4Prefix, UINT4 *pu4Mask)
{
    INT4                i4Count = 0;

    *pu4Mask = 0;

    for (i4Count = 0; i4Count < (INT4) u4Prefix; i4Count++)
    {
        *pu4Mask = *pu4Mask >> 1;
        *pu4Mask |= 0x80000000;
    }
}

/*****************************************************************************/
/* Function Name      : IssUtlConvertMaskToPrefix                            */
/*                                                                           */
/* Description        : This function converts the given Mask to Prefix      */
/*                                                                           */
/* Input(s)           : u4Mask : Mask                                        */
/*                                                                           */
/* Output(s)          : u4Prefix : Prefix                                    */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
IssUtlConvertMaskToPrefix (UINT4 u4Mask, UINT4 *pu4Prefix)
{
    UINT4               u4Count = 0;

    *pu4Prefix = 0;

    while (u4Mask)
    {
        u4Count++;
        u4Mask = u4Mask << 1;
    }

    *pu4Prefix = u4Count;
}

/**************************************************************************/
/* Function Name       : IssUpdatePortLinkStatus                          */
/*                                                                        */
/* Description         : This function Updates the port link status for   */
/*                       recalculation of hashing logic                   */
/*                                                                        */
/* Input(s)            : LocalPortList - Local port list                  */
/*                                                                        */
/* Output(s)           : pu4IfPortArray, u1NumPorts                       */
/*                                                                        */
/* Global Variables                                                       */
/* Referred            : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Modified            : None                                             */
/*                                                                        */
/* Exceptions or OS                                                       */
/* Error Handling      : None                                             */
/*                                                                        */
/* Use of Recursion    : None                                             */
/*                                                                        */
/* Returns             : None                                             */
/*                                                                        */
/**************************************************************************/
VOID
IssUpdatePortLinkStatus (UINT4 u4IfIndex, UINT1 u1IfType, UINT1 u1OperStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1IfType);
    UNUSED_PARAM (u1OperStatus);
    return;
}

/*****************************************************************************/
/* Function Name      : IssACLCreateFilter                                   */
/*                                                                           */
/* Description        : This function is called from the other modules       */
/*                      to create L2, L3 and user defined filter.            */
/*                                                                           */
/* Input(s)           : pAclFilterInfo - Pointer to ACL filter information   */
/*                                                                           */
/* Output(s)          : pu4L2FilterId  - Pointer to L2 filter ID             */
/*                      pu4L3FilterId  - Pointer to L3 filter ID             */
/*                      pu4UDBFilterId - Pointer to User defined filter ID   */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
IssACLCreateFilter (tAclFilterInfo * pAclFilterInfo, UINT4 *pu4L2FilterId,
                    UINT4 *pu4L3FilterId, UINT4 *pu4UDBFilterId)
{
    UNUSED_PARAM (pAclFilterInfo);
    *pu4L2FilterId = 0;
    *pu4L3FilterId = 0;
    *pu4UDBFilterId = 0;
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssACLDeleteFilter                                   */
/*                                                                           */
/* Description        : This function is called from the other Modules       */
/*                      to delete L2, L3 and user defined filter.            */
/*                                                                           */
/* Input(s)           : u1FilterType - Filter Type (L2/L3/User-defined)      */
/*                      u4L2FilterId - L2 filter ID                          */
/*                      u4L3FilterId - L3 filter ID                          */
/*                      u4UserDefFilterId - User defined filter ID           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
IssACLDeleteFilter (UINT1 u1FilterType, UINT4 u4L2FilterId,
                    UINT4 u4L3FilterId, UINT4 u4UserDefFilterId)
{
    UNUSED_PARAM (u1FilterType);
    UNUSED_PARAM (u4L2FilterId);
    UNUSED_PARAM (u4L3FilterId);
    UNUSED_PARAM (u4UserDefFilterId);
    return;
}

/*****************************************************************************/
/* Function Name      : IssHwInitMirrDataBase                                */
/*                                                                           */
/* Description        : This function is called at ISS module intialization. */
/*                      It initializes default Mirroring session parameters. */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS - On success                             */
/*                      ISS_FAILURE - On failure                             */
/*****************************************************************************/
INT4
IssHwInitMirrDataBase ()
{
    return FNP_SUCCESS;
}

/****************************************************************************
 *                                                                          *
 *     Function Name : IssExPrgAclsToNpWithPriority                         *
 *                                                                          *
 *     Description   : This function programs the h/w based on priority     *
 *                     table.                                               *
 *                                                                          *
 *                                                                          *
 *     Input(s)      : NULL                                                 *
 *                                                                          *
 *     Output(s)     : NULL                                                 *
 *                                                                          *
 *     Returns       :  NONE                                                *
 *                                                                          *
 *                                                                          *
 ****************************************************************************/

INT4
IssExPrgAclsToNpWithPriority (VOID)
{
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : AclProcessQMsgEvent                                  */
/*                                                                           */
/* Description        : This function process the ACL Queue message posted in*/
/*            ACl Queue.                                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
AclProcessQMsgEvent (VOID)
{
    /* This function is mainly used to process the RM Message.
     * Since HA ssupport is not implemented in this platform,
     * there is no defintion for this funtion.
     * This is just a stub function to avoid compilation error */
    return;
}

/*****************************************************************************/
/* Function Name      : IssACLApiModifyFilterEntry                           */
/*                                                                           */
/* Description        : This function is called from the DCBx Application    */
/*                      priority module to configure the priority for        */
/*                      a protocol.                                          */
/*                                                                           */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
IssACLApiModifyFilterEntry (tIssAclHwFilterInfo * pAppPriAclInfo,
                            UINT1 u1FilterType, UINT1 u1Status)
{
    UNUSED_PARAM (pAppPriAclInfo);
    UNUSED_PARAM (u1FilterType);
    UNUSED_PARAM (u1Status);
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssACLApiGetFilterEntry                           */
/*                                                                           */
/* Description        : This function is called from the DCBx Application    */
/*                      priority module to configure the priority for        */
/*                      a protocol.                                          */
/*                                                                           */
/* Input(s)           :                                                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
IssACLApiGetFilterEntry (tIssAclHwFilterInfo * pAppPriAclInfo,
                            UINT1 u1FilterType, UINT1 u1Status)
{
    UNUSED_PARAM (pAppPriAclInfo);
    UNUSED_PARAM (u1FilterType);
    UNUSED_PARAM (u1Status);
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssACLApiValidateL3AclConfigFlag                     */
/*                                                                           */
/* Description        : This function is called from msrval for validating   */
/*                      L3 Acl Config Flag (which is set based on            */
/*                      configuration) done through ACL or other protocol    */
/*                      a protocol.                                          */
/*                                                                           */
/* Input(s)           : i4L3AclFilterId - filter id                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/

VOID
IssACLApiValidateL3AclConfigFlag (INT4 i4L3AclFilterId, BOOL1 *pb1IsL3AclConfFromExt)
{
    UNUSED_PARAM (i4L3AclFilterId);
    UNUSED_PARAM (pb1IsL3AclConfFromExt);
    return;
}

/*****************************************************************************/
/* Function Name      : IssACLApiValidateL2AclConfigFlag                     */
/*                                                                           */
/* Description        : This function is called from msrval for validating   */
/*                      L2 Acl Config Flag (which is set based on            */
/*                      configuration) done through ACL or other protocol    */
/*                      a protocol.                                          */
/*                                                                           */
/* Input(s)           : i4L2AclFilterId - filter id                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/

VOID
IssACLApiValidateL2AclConfigFlag (INT4 i4L2AclFilterId, BOOL1 *pb1IsL2AclConfFromExt)
{
    UNUSED_PARAM (i4L2AclFilterId);
    UNUSED_PARAM (pb1IsL2AclConfFromExt);
    return;
}

/*****************************************************************************/
/* Function Name      :  IssAclQosDeleteL2L3Filter                           */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                      to uninstall L2, L3  ACL rules and                   */
/*                      QoS configuartions.                                  */
/*                                                                           */
/* Input(s)           :                                                      */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/

INT4
IssAclQosDeleteL2L3Filter (VOID)
{
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      :  IssAclQosProcessL2L3Proto                           */
/*                                                                           */
/* Description        : This function is called from the ACL Module          */
/*                      to install L2, L3 and DoS attack control ACL rules   */
/*                      and QoS configurations.                              */
/*                                                                           */
/* Input(s)           :                                                      */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : ACL Module                                           */
/*****************************************************************************/
INT4
IssAclQosProcessL2L3Proto (VOID)
{
    return ISS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclValidateMemberPort                            */
/*                                                                           */
/*     DESCRIPTION      : This function checks wheather the filter is        */
/*                   attached with the physical port                    */
/*                                                                           */
/*     INPUT            : u4IfIndex - Physical port number                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
AclValidateMemberPort (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return (CLI_SUCCESS);
}

/*****************************************************************************
**
**     FUNCTION NAME    : AclUpdateOverPortChannel
**
**     DESCRIPTION      : This function removes any L2/L3 ACL that is
**                        applied on a given port-channel interface.
**
**     INPUT            : u4IfIndex - Interface Index
**
**     OUTPUT           : NONE
**
**     RETURNS          : CFA_SUCCESS or CFA_FAILURE
**
*****************************************************************************/
INT4
AclUpdateOverPortChannel (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}
/*****************************************************************************
 **
 **     FUNCTION NAME    : AclIpFilterEnabledForPort
 **
 **     DESCRIPTION      : This function checks whether
 **                       IP any ACL is associated with this port
 **
 **
 **     INPUT            : u4IfIndex - Interface Index
 **
 **     OUTPUT           : NONE
 **
 **     RETURNS          :  CLI_SUCCESS / CLI_FAILURE
 **
 *****************************************************************************/
INT4
AclIpFilterEnabledForPort (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CLI_FAILURE;
}
/*****************************************************************************
 **
 **     FUNCTION NAME    : AclMacFilterEnabledForPort
 **
 **     DESCRIPTION      : This function checks whether
 **                       any MAC ACL is associated with this port
 **
 **
 **     INPUT            : u4IfIndex - Interface Index
 **
 **     OUTPUT           : NONE
 **
 **     RETURNS          :  CLI_SUCCESS / CLI_FAILURE
 **
 *****************************************************************************/
INT4
AclMacFilterEnabledForPort (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CLI_FAILURE;
}
/*****************************************************************************
 **
 **     FUNCTION NAME    : AclUserDefinedFilterEnabledForPort
 **
 **     DESCRIPTION      : This function checks whether
 **                       any UserDefined ACL is associated with this port
 **
 **
 **     INPUT            : u4IfIndex - Interface Index
 **
 **     OUTPUT           : NONE
 **
 **     RETURNS          :  CLI_SUCCESS / CLI_FAILURE
 **
 *****************************************************************************/
INT4
AclUserDefinedFilterEnabledForPort (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CLI_FAILURE;
}
/*****************************************************************************
 **
 **     FUNCTION NAME    : AclEnabledForPort
 **
 **     DESCRIPTION      : This function checks whether
 **                       any ACL is associated with this port
 **
 **
 **     INPUT            : u4IfIndex - Interface Index
 **
 **     OUTPUT           : NONE
 **
 **     RETURNS          : CLI_SUCCESS / CLI_FAILURE
 **
 *****************************************************************************/

INT4
AclEnabledForPort (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CLI_FAILURE;
}
#endif /* _ISSEXSYS_C */
