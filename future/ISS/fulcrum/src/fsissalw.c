
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissalw.c,v 1.15 2015/09/11 09:44:59 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsissalw.h"
# include  "issexinc.h"
# include  "fsisselw.h"
# include  "isscli.h"
# include  "aclcli.h"
# include  "diffsrv.h"
# include  "qosxtd.h"
# include  "fsissacli.h"
# include  "ipv6.h"

/* LOW LEVEL Routines for Table : IssAclRateCtrlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssAclRateCtrlTable
 Input       :  The Indices
                IssAclRateCtrlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssAclRateCtrlTable (INT4 i4IssAclRateCtrlIndex)
{
    if (IssExtSnmpLowValidateIndexPortRateTable (i4IssAclRateCtrlIndex) ==
        ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssAclRateCtrlTable
 Input       :  The Indices
                IssAclRateCtrlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssAclRateCtrlTable (INT4 *pi4IssAclRateCtrlIndex)
{
    return (nmhGetNextIndexIssAclRateCtrlTable (0, pi4IssAclRateCtrlIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssAclRateCtrlTable
 Input       :  The Indices
                IssAclRateCtrlIndex
                nextIssAclRateCtrlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssAclRateCtrlTable (INT4 i4IssAclRateCtrlIndex,
                                    INT4 *pi4NextIssAclRateCtrlIndex)
{
    INT4                i4Count = 0;

    if (i4IssAclRateCtrlIndex < 0)
    {
        return SNMP_FAILURE;
    }

    for (i4Count = i4IssAclRateCtrlIndex + 1; i4Count <= ISS_MAX_PORTS;
         i4Count++)
    {
        if ((gIssExGlobalInfo.apIssRateCtrlEntry[i4Count]) != NULL)
        {
            *pi4NextIssAclRateCtrlIndex = i4Count;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIssAclRateCtrlDLFLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                retValIssAclRateCtrlDLFLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclRateCtrlDLFLimitValue (INT4 i4IssAclRateCtrlIndex,
                                   INT4 *pi4RetValIssAclRateCtrlDLFLimitValue)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry;

    if (IssExtValidateRateCtrlEntry (i4IssAclRateCtrlIndex) != ISS_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    *pi4RetValIssAclRateCtrlDLFLimitValue =
        (INT4) pIssAclRateCtrlEntry->u4IssRateCtrlDLFLimitValue;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssAclRateCtrlBCASTLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                retValIssAclRateCtrlBCASTLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclRateCtrlBCASTLimitValue (INT4 i4IssAclRateCtrlIndex,
                                     INT4
                                     *pi4RetValIssAclRateCtrlBCASTLimitValue)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry;

    if (IssExtValidateRateCtrlEntry (i4IssAclRateCtrlIndex) != ISS_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    *pi4RetValIssAclRateCtrlBCASTLimitValue =
        (INT4) pIssAclRateCtrlEntry->u4IssRateCtrlBCASTLimitValue;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssAclRateCtrlMCASTLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                retValIssAclRateCtrlMCASTLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclRateCtrlMCASTLimitValue (INT4 i4IssAclRateCtrlIndex,
                                     INT4
                                     *pi4RetValIssAclRateCtrlMCASTLimitValue)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry;

    if (IssExtValidateRateCtrlEntry (i4IssAclRateCtrlIndex) != ISS_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    *pi4RetValIssAclRateCtrlMCASTLimitValue =
        (INT4) pIssAclRateCtrlEntry->u4IssRateCtrlMCASTLimitValue;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssAclRateCtrlPortRateLimit
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                retValIssAclRateCtrlPortRateLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclRateCtrlPortRateLimit (INT4 i4IssAclRateCtrlIndex,
                                   INT4 *pi4RetValIssAclRateCtrlPortRateLimit)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIssAclRateCtrlPortRateLimit =
        pIssAclRateCtrlEntry->i4IssRateCtrlPortLimitRate;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssAclRateCtrlPortBurstSize
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                retValIssAclRateCtrlPortBurstSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclRateCtrlPortBurstSize (INT4 i4IssAclRateCtrlIndex,
                                   INT4 *pi4RetValIssAclRateCtrlPortBurstSize)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIssAclRateCtrlPortBurstSize =
        pIssAclRateCtrlEntry->i4IssRateCtrlPortBurstRate;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIssAclRateCtrlDLFLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                setValIssAclRateCtrlDLFLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclRateCtrlDLFLimitValue (INT4 i4IssAclRateCtrlIndex,
                                   INT4 i4SetValIssAclRateCtrlDLFLimitValue)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclRateCtrlEntry->u4IssRateCtrlDLFLimitValue == (UINT4)
        i4SetValIssAclRateCtrlDLFLimitValue)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    i4RetVal = IsssysIssHwSetRateLimitingValue ((UINT4) i4IssAclRateCtrlIndex,
                                                ISS_RATE_DLF,
                                                i4SetValIssAclRateCtrlDLFLimitValue);

    if (i4RetVal != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif

    pIssAclRateCtrlEntry->u4IssRateCtrlDLFLimitValue = (UINT4)
        i4SetValIssAclRateCtrlDLFLimitValue;
    SnmpNotifyInfo.pu4ObjectId = IssAclRateCtrlDLFLimitValue;
    SnmpNotifyInfo.u4OidLen =
        sizeof (IssAclRateCtrlDLFLimitValue) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = IssLock;
    SnmpNotifyInfo.pUnLockPointer = IssUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IssAclRateCtrlIndex,
                      i4SetValIssAclRateCtrlDLFLimitValue));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclRateCtrlBCASTLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                setValIssAclRateCtrlBCASTLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclRateCtrlBCASTLimitValue (INT4 i4IssAclRateCtrlIndex,
                                     INT4 i4SetValIssAclRateCtrlBCASTLimitValue)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclRateCtrlEntry->u4IssRateCtrlBCASTLimitValue == (UINT4)
        i4SetValIssAclRateCtrlBCASTLimitValue)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    i4RetVal = IsssysIssHwSetRateLimitingValue ((UINT4) i4IssAclRateCtrlIndex,
                                                ISS_RATE_BCAST,
                                                i4SetValIssAclRateCtrlBCASTLimitValue);

    if (i4RetVal != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif

    pIssAclRateCtrlEntry->u4IssRateCtrlBCASTLimitValue = (UINT4)
        i4SetValIssAclRateCtrlBCASTLimitValue;
    SnmpNotifyInfo.pu4ObjectId = IssAclRateCtrlBCASTLimitValue;
    SnmpNotifyInfo.u4OidLen =
        sizeof (IssAclRateCtrlBCASTLimitValue) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = IssLock;
    SnmpNotifyInfo.pUnLockPointer = IssUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IssAclRateCtrlIndex,
                      i4SetValIssAclRateCtrlBCASTLimitValue));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclRateCtrlMCASTLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                setValIssAclRateCtrlMCASTLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclRateCtrlMCASTLimitValue (INT4 i4IssAclRateCtrlIndex,
                                     INT4 i4SetValIssAclRateCtrlMCASTLimitValue)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssAclRateCtrlEntry->u4IssRateCtrlMCASTLimitValue == (UINT4)
        i4SetValIssAclRateCtrlMCASTLimitValue)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    i4RetVal = IsssysIssHwSetRateLimitingValue ((UINT4) i4IssAclRateCtrlIndex,
                                                ISS_RATE_MCAST,
                                                i4SetValIssAclRateCtrlMCASTLimitValue);

    if (i4RetVal != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif

    pIssAclRateCtrlEntry->u4IssRateCtrlMCASTLimitValue = (UINT4)
        i4SetValIssAclRateCtrlMCASTLimitValue;
    SnmpNotifyInfo.pu4ObjectId = IssAclRateCtrlMCASTLimitValue;
    SnmpNotifyInfo.u4OidLen =
        sizeof (IssAclRateCtrlMCASTLimitValue) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = IssLock;
    SnmpNotifyInfo.pUnLockPointer = IssUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IssAclRateCtrlIndex,
                      i4SetValIssAclRateCtrlMCASTLimitValue));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclRateCtrlPortRateLimit
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                setValIssAclRateCtrlPortRateLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclRateCtrlPortRateLimit (INT4 i4IssAclRateCtrlIndex,
                                   INT4 i4SetValIssAclRateCtrlPortRateLimit)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif
    INT4                i4SetValIssAclRateCtrlPortBurstSize = 0;

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((INT4) (pIssAclRateCtrlEntry->i4IssRateCtrlPortLimitRate) ==
        i4SetValIssAclRateCtrlPortRateLimit)
    {
        return SNMP_SUCCESS;
    }

    i4SetValIssAclRateCtrlPortBurstSize =
        pIssAclRateCtrlEntry->i4IssRateCtrlPortBurstRate;

#ifdef NPAPI_WANTED
    i4RetVal = IsssysIssHwSetPortEgressPktRate ((UINT4) i4IssAclRateCtrlIndex,
                                                i4SetValIssAclRateCtrlPortRateLimit,
                                                i4SetValIssAclRateCtrlPortBurstSize);

    if (i4RetVal != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif
    pIssAclRateCtrlEntry->i4IssRateCtrlPortLimitRate =
        i4SetValIssAclRateCtrlPortRateLimit;
    SnmpNotifyInfo.pu4ObjectId = IssAclRateCtrlPortRateLimit;
    SnmpNotifyInfo.u4OidLen =
        sizeof (IssAclRateCtrlPortRateLimit) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = IssLock;
    SnmpNotifyInfo.pUnLockPointer = IssUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IssAclRateCtrlIndex,
                      i4SetValIssAclRateCtrlPortRateLimit));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclRateCtrlPortBurstSize
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                setValIssAclRateCtrlPortBurstSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclRateCtrlPortBurstSize (INT4 i4IssAclRateCtrlIndex,
                                   INT4 i4SetValIssAclRateCtrlPortBurstSize)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif
    INT4                i4SetValIssAclRateCtrlPortRateLimit = 0;

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((INT4) (pIssAclRateCtrlEntry->i4IssRateCtrlPortBurstRate) ==
        i4SetValIssAclRateCtrlPortBurstSize)
    {
        return SNMP_SUCCESS;
    }

    i4SetValIssAclRateCtrlPortRateLimit =
        pIssAclRateCtrlEntry->i4IssRateCtrlPortLimitRate;

#ifdef NPAPI_WANTED
    i4RetVal = IsssysIssHwSetPortEgressPktRate
        ((UINT4) i4IssAclRateCtrlIndex, i4SetValIssAclRateCtrlPortRateLimit,
         i4SetValIssAclRateCtrlPortBurstSize);

    if (i4RetVal != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif

    pIssAclRateCtrlEntry->i4IssRateCtrlPortBurstRate =
        i4SetValIssAclRateCtrlPortBurstSize;
    SnmpNotifyInfo.pu4ObjectId = IssAclRateCtrlPortBurstSize;
    SnmpNotifyInfo.u4OidLen =
        sizeof (IssAclRateCtrlPortBurstSize) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = IssLock;
    SnmpNotifyInfo.pUnLockPointer = IssUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IssAclRateCtrlIndex,
                      i4SetValIssAclRateCtrlPortBurstSize));
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IssAclRateCtrlDLFLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                testValIssAclRateCtrlDLFLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclRateCtrlDLFLimitValue (UINT4 *pu4ErrorCode,
                                      INT4 i4IssAclRateCtrlIndex,
                                      INT4 i4TestValIssAclRateCtrlDLFLimitValue)
{
    if (IssExtValidateRateCtrlEntry (i4IssAclRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclRateCtrlDLFLimitValue < RATE_LIMIT_MIN_VALUE)
        || (i4TestValIssAclRateCtrlDLFLimitValue > RATE_LIMIT_MAX_VALUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclRateCtrlBCASTLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                testValIssAclRateCtrlBCASTLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclRateCtrlBCASTLimitValue (UINT4 *pu4ErrorCode,
                                        INT4 i4IssAclRateCtrlIndex,
                                        INT4
                                        i4TestValIssAclRateCtrlBCASTLimitValue)
{
    if (IssExtValidateRateCtrlEntry (i4IssAclRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclRateCtrlBCASTLimitValue < RATE_LIMIT_MIN_VALUE)
        || (i4TestValIssAclRateCtrlBCASTLimitValue > RATE_LIMIT_MAX_VALUE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclRateCtrlMCASTLimitValue
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                testValIssAclRateCtrlMCASTLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclRateCtrlMCASTLimitValue (UINT4 *pu4ErrorCode,
                                        INT4 i4IssAclRateCtrlIndex,
                                        INT4
                                        i4TestValIssAclRateCtrlMCASTLimitValue)
{
    if (IssExtValidateRateCtrlEntry (i4IssAclRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclRateCtrlMCASTLimitValue < RATE_LIMIT_MIN_VALUE)
        || (i4TestValIssAclRateCtrlMCASTLimitValue > RATE_LIMIT_MAX_VALUE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclRateCtrlPortRateLimit
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                testValIssAclRateCtrlPortRateLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclRateCtrlPortRateLimit (UINT4 *pu4ErrorCode,
                                      INT4 i4IssAclRateCtrlIndex,
                                      INT4 i4TestValIssAclRateCtrlPortRateLimit)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry;

    if (IssExtValidateRateCtrlEntry (i4IssAclRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclRateCtrlPortRateLimit < RATE_LIMIT_MIN_VALUE) ||
        (i4TestValIssAclRateCtrlPortRateLimit > RATE_LIMIT_MAX_VALUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclRateCtrlPortBurstSize
 Input       :  The Indices
                IssAclRateCtrlIndex

                The Object 
                testValIssAclRateCtrlPortBurstSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclRateCtrlPortBurstSize (UINT4 *pu4ErrorCode,
                                      INT4 i4IssAclRateCtrlIndex,
                                      INT4 i4TestValIssAclRateCtrlPortBurstSize)
{
    tIssRateCtrlEntry  *pIssAclRateCtrlEntry;

    if (IssExtValidateRateCtrlEntry (i4IssAclRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pIssAclRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssAclRateCtrlIndex];

    if (pIssAclRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclRateCtrlPortBurstSize < RATE_LIMIT_MIN_VALUE) ||
        (i4TestValIssAclRateCtrlPortBurstSize > RATE_LIMIT_MAX_VALUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssAclRateCtrlTable
 Input       :  The Indices
                IssAclRateCtrlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssAclRateCtrlTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IssAclL2FilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssAclL2FilterTable
 Input       :  The Indices
                IssAclL2FilterNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssAclL2FilterTable (INT4 i4IssAclL2FilterNo)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) != ISS_TRUE)
    {
        return SNMP_FAILURE;
    }
    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_LIST);
    while (pSllNode != NULL)
    {
        pIssAclL2FilterEntry = (tIssL2FilterEntry *) pSllNode;
        if (i4IssAclL2FilterNo == pIssAclL2FilterEntry->i4IssL2FilterNo)
        {
            return SNMP_SUCCESS;
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_LIST, pSllNode);
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssAclL2FilterTable
 Input       :  The Indices
                IssAclL2FilterNo
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssAclL2FilterTable (INT4 *pi4IssAclL2FilterNo)
{
    if ((IssExtSnmpLowGetFirstValidL2FilterTableIndex (pi4IssAclL2FilterNo)) ==
        ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhGetNextIndexIssAclL2FilterTable
 Input       :  The Indices
                IssAclL2FilterNo
                nextIssAclL2FilterNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssAclL2FilterTable (INT4 i4IssAclL2FilterNo,
                                    INT4 *pi4NextIssAclL2FilterNo)
{
    if (i4IssAclL2FilterNo < 0)
    {
        return SNMP_FAILURE;
    }

    if ((IssExtSnmpLowGetNextValidL2FilterTableIndex
         (i4IssAclL2FilterNo, pi4NextIssAclL2FilterNo)) == ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterPriority
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterPriority (INT4 i4IssAclL2FilterNo,
                              INT4 *pi4RetValIssAclL2FilterPriority)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2FilterPriority =
            pIssAclL2FilterEntry->i4IssL2FilterPriority;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterEtherType
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterEtherType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterEtherType (INT4 i4IssAclL2FilterNo,
                               INT4 *pi4RetValIssAclL2FilterEtherType)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2FilterEtherType =
            pIssAclL2FilterEntry->u2InnerEtherType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterProtocolType
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterProtocolType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterProtocolType (INT4 i4IssAclL2FilterNo,
                                  UINT4 *pu4RetValIssAclL2FilterProtocolType)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pu4RetValIssAclL2FilterProtocolType =
            pIssAclL2FilterEntry->u4IssL2FilterProtocolType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterDstMacAddr
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterDstMacAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterDstMacAddr (INT4 i4IssAclL2FilterNo,
                                tMacAddr * pRetValIssAclL2FilterDstMacAddr)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        ISS_MEMCPY ((UINT1 *) pRetValIssAclL2FilterDstMacAddr,
                    pIssAclL2FilterEntry->IssL2FilterDstMacAddr,
                    ISS_ETHERNET_ADDR_SIZE);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterSrcMacAddr
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterSrcMacAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterSrcMacAddr (INT4 i4IssAclL2FilterNo,
                                tMacAddr * pRetValIssAclL2FilterSrcMacAddr)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        ISS_MEMCPY ((UINT1 *) pRetValIssAclL2FilterSrcMacAddr,
                    pIssAclL2FilterEntry->IssL2FilterSrcMacAddr,
                    ISS_ETHERNET_ADDR_SIZE);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterVlanId
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterVlanId (INT4 i4IssAclL2FilterNo,
                            INT4 *pi4RetValIssAclL2FilterVlanId)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2FilterVlanId =
            (INT4) pIssAclL2FilterEntry->u4IssL2FilterCustomerVlanId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterInPortList
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterInPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterInPortList (INT4 i4IssAclL2FilterNo,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValIssAclL2FilterInPortList)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        ISS_MEMSET (pRetValIssAclL2FilterInPortList->pu1_OctetList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_ADD_PORT_LIST (pRetValIssAclL2FilterInPortList->pu1_OctetList,
                           pIssAclL2FilterEntry->IssL2FilterInPortList);
        pRetValIssAclL2FilterInPortList->i4_Length = ISS_PORT_LIST_SIZE;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterAction
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterAction (INT4 i4IssAclL2FilterNo,
                            INT4 *pi4RetValIssAclL2FilterAction)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2FilterAction =
            pIssAclL2FilterEntry->IssL2FilterAction;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterMatchCount
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterMatchCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterMatchCount (INT4 i4IssAclL2FilterNo,
                                UINT4 *pu4RetValIssAclL2FilterMatchCount)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
#ifdef NPAPI_WANTED
    INT4 i4RetVal = FNP_FAILURE;
#endif
    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

#ifdef NPAPI_WANTED
    if ((pIssAclL2FilterEntry != NULL) && 
        (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE))
    {
        i4RetVal = IsssysIssHwUpdateL2Filter (pIssAclL2FilterEntry,
                                             ISS_L2FILTER_STAT_GET);
        if (i4RetVal != FNP_FAILURE)
        {
            *pu4RetValIssAclL2FilterMatchCount = 
                pIssAclL2FilterEntry->u4IssL2FilterMatchCount;
            return SNMP_SUCCESS;
        }
        return SNMP_FAILURE;
    }
#else
    if (pIssAclL2FilterEntry != NULL)
    {
        *pu4RetValIssAclL2FilterMatchCount =
            pIssAclL2FilterEntry->u4IssL2FilterMatchCount;
        return SNMP_SUCCESS;
    }
#endif
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterDirection
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterDirection (INT4 i4IssAclL2FilterNo,
                               INT4 *pi4RetValIssAclL2FilterDirection)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2FilterDirection =
            (INT4) pIssAclL2FilterEntry->u1FilterDirection;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterStatus
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterStatus (INT4 i4IssAclL2FilterNo,
                            INT4 *pi4RetValIssAclL2FilterStatus)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        *pi4RetValIssAclL2FilterStatus =
            (INT4) pIssAclL2FilterEntry->u1IssL2FilterStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL2FilterOutPortList
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                retValIssAclL2FilterOutPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL2FilterOutPortList (INT4 i4IssAclL2FilterNo,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValIssAclL2FilterOutPortList)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        ISS_MEMSET (pRetValIssAclL2FilterOutPortList->pu1_OctetList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_ADD_PORT_LIST (pRetValIssAclL2FilterOutPortList->pu1_OctetList,
                           pIssAclL2FilterEntry->IssL2FilterInPortList);
        pRetValIssAclL2FilterOutPortList->i4_Length = ISS_PORT_LIST_SIZE;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterPriority
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterPriority (INT4 i4IssAclL2FilterNo,
                              INT4 i4SetValIssAclL2FilterPriority)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL2FilterEntry->i4IssL2FilterPriority =
            i4SetValIssAclL2FilterPriority;
        SnmpNotifyInfo.pu4ObjectId = IssAclL2FilterPriority;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL2FilterPriority) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IssAclL2FilterNo,
                          i4SetValIssAclL2FilterPriority));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterEtherType
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterEtherType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterEtherType (INT4 i4IssAclL2FilterNo,
                               INT4 i4SetValIssAclL2FilterEtherType)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL2FilterEntry->u2InnerEtherType = (UINT2)
            i4SetValIssAclL2FilterEtherType;
        SnmpNotifyInfo.pu4ObjectId = IssAclL2FilterEtherType;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL2FilterEtherType) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IssAclL2FilterNo,
                          i4SetValIssAclL2FilterEtherType));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterProtocolType
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterProtocolType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterProtocolType (INT4 i4IssAclL2FilterNo,
                                  UINT4 u4SetValIssAclL2FilterProtocolType)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL2FilterEntry->u4IssL2FilterProtocolType =
            u4SetValIssAclL2FilterProtocolType;
        SnmpNotifyInfo.pu4ObjectId = IssAclL2FilterProtocolType;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL2FilterProtocolType) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u", i4IssAclL2FilterNo,
                          u4SetValIssAclL2FilterProtocolType));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterDstMacAddr
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterDstMacAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterDstMacAddr (INT4 i4IssAclL2FilterNo,
                                tMacAddr SetValIssAclL2FilterDstMacAddr)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        ISS_MEMSET (pIssAclL2FilterEntry->IssL2FilterDstMacAddr, 0,
                    ISS_ETHERNET_ADDR_SIZE);
        ISS_MEMCPY (pIssAclL2FilterEntry->IssL2FilterDstMacAddr,
                    SetValIssAclL2FilterDstMacAddr, ISS_ETHERNET_ADDR_SIZE);

        SnmpNotifyInfo.pu4ObjectId = IssAclL2FilterDstMacAddr;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL2FilterDstMacAddr) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m", i4IssAclL2FilterNo,
                          SetValIssAclL2FilterDstMacAddr));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterSrcMacAddr
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterSrcMacAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterSrcMacAddr (INT4 i4IssAclL2FilterNo,
                                tMacAddr SetValIssAclL2FilterSrcMacAddr)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {

        if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        ISS_MEMSET (pIssAclL2FilterEntry->IssL2FilterSrcMacAddr, 0,
                    ISS_ETHERNET_ADDR_SIZE);
        ISS_MEMCPY (pIssAclL2FilterEntry->IssL2FilterSrcMacAddr,
                    SetValIssAclL2FilterSrcMacAddr, ISS_ETHERNET_ADDR_SIZE);
        SnmpNotifyInfo.pu4ObjectId = IssAclL2FilterSrcMacAddr;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL2FilterSrcMacAddr) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %m", i4IssAclL2FilterNo,
                          SetValIssAclL2FilterSrcMacAddr));

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterVlanId
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterVlanId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterVlanId (INT4 i4IssAclL2FilterNo,
                            INT4 i4SetValIssAclL2FilterVlanId)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL2FilterEntry->u4IssL2FilterCustomerVlanId =
            (UINT4) i4SetValIssAclL2FilterVlanId;
        SnmpNotifyInfo.pu4ObjectId = IssAclL2FilterVlanId;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL2FilterVlanId) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IssAclL2FilterNo,
                          i4SetValIssAclL2FilterVlanId));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterInPortList
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterInPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterInPortList (INT4 i4IssAclL2FilterNo,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValIssAclL2FilterInPortList)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        ISS_MEMSET (pIssAclL2FilterEntry->IssL2FilterInPortList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_MEMCPY (pIssAclL2FilterEntry->IssL2FilterInPortList,
                    pSetValIssAclL2FilterInPortList->pu1_OctetList,
                    pSetValIssAclL2FilterInPortList->i4_Length);
        SnmpNotifyInfo.pu4ObjectId = IssAclL2FilterInPortList;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL2FilterInPortList) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s", i4IssAclL2FilterNo,
                          pSetValIssAclL2FilterInPortList));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterAction
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterAction (INT4 i4IssAclL2FilterNo,
                            INT4 i4SetValIssAclL2FilterAction)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL2FilterEntry->IssL2FilterAction = i4SetValIssAclL2FilterAction;
        SnmpNotifyInfo.pu4ObjectId = IssAclL2FilterAction;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL2FilterAction) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IssAclL2FilterNo,
                          i4SetValIssAclL2FilterAction));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterStatus
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterStatus (INT4 i4IssAclL2FilterNo,
                            INT4 i4SetValIssAclL2FilterStatus)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    tIssPortList        IssL2FilterNullPortList;

    ISS_MEMSET (IssL2FilterNullPortList, 0, sizeof (tIssPortList));

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        if (pIssAclL2FilterEntry->u1IssL2FilterStatus ==
            (UINT1) i4SetValIssAclL2FilterStatus)
        {
            return SNMP_SUCCESS;
        }

        if (i4SetValIssAclL2FilterStatus == ISS_CREATE_AND_WAIT)
        {
            CLI_SET_ERR (CLI_ACL_FILTER_EXISTS);
            return SNMP_FAILURE;
        }

    }
    else
    {
        /* This Filter Entry is not there in the Database */
        if ((i4SetValIssAclL2FilterStatus != ISS_CREATE_AND_WAIT))
        {
            CLI_SET_ERR (CLI_ACL_NO_FILTER);
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValIssAclL2FilterStatus)
    {

        case ISS_CREATE_AND_WAIT:

            if ((ISS_L2FILTERENTRY_ALLOC_MEM_BLOCK (pIssAclL2FilterEntry)) ==
                NULL)
            {
                return SNMP_FAILURE;
            }

            ISS_MEMSET (pIssAclL2FilterEntry, 0, sizeof (tIssL2FilterEntry));

            /* Setting all the default values for the objects */
            pIssAclL2FilterEntry->i4IssL2FilterNo = i4IssAclL2FilterNo;
            pIssAclL2FilterEntry->i4IssL2FilterPriority =
                ISS_DEFAULT_FILTER_PRIORITY;

            pIssAclL2FilterEntry->u4IssL2FilterProtocolType =
                ISS_DEFAULT_PROTOCOL_TYPE;

            pIssAclL2FilterEntry->i1IssL2FilterCVlanPriority =
                ISS_DEFAULT_VLAN_PRIORITY;
            pIssAclL2FilterEntry->i1IssL2FilterSVlanPriority =
                ISS_DEFAULT_VLAN_PRIORITY;

            pIssAclL2FilterEntry->u4IssL2FilterCustomerVlanId = 0;
            pIssAclL2FilterEntry->u4IssL2FilterServiceVlanId = 0;

            pIssAclL2FilterEntry->u2InnerEtherType = ISS_MIN_ETHER_TYPE_VAL;
            pIssAclL2FilterEntry->u2OuterEtherType = ISS_MIN_ETHER_TYPE_VAL;
            pIssAclL2FilterEntry->u1IssL2FilterTagType = ISS_FILTER_SINGLE_TAG;

            pIssAclL2FilterEntry->u1FilterDirection = ISS_DIRECTION_IN;

            pIssAclL2FilterEntry->IssL2FilterAction = ISS_ALLOW;

            pIssAclL2FilterEntry->u4IssL2FilterMatchCount = 0;

            /* Adding the Entry to the Filter List */
            ISS_SLL_ADD (&(ISS_L2FILTER_LIST),
                         &(pIssAclL2FilterEntry->IssNextNode));

            /* Setting the RowStatus to NOT_READY if the
             * filter if i4SetValIssAclL2FilterStatus is ISS_CREATE_AND_WAIT*/

            pIssAclL2FilterEntry->u1IssL2FilterStatus = ISS_NOT_READY;

            if (i4SetValIssAclL2FilterStatus == ISS_CREATE_AND_GO)
            {
#ifdef NPAPI_WANTED
                if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                {
                    i4RetVal =
                        IsssysIssHwUpdateL2Filter (pIssAclL2FilterEntry,
                                                   ISS_L2FILTER_ADD);

                    if (i4RetVal != FNP_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                }
#endif

                pIssAclL2FilterEntry->u1IssL2FilterStatus = ISS_ACTIVE;
            }

            break;

        case ISS_NOT_IN_SERVICE:

            if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
            {
#ifdef NPAPI_WANTED
                if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                {
                    /* Call the hardware API to update the filter */
                    i4RetVal =
                        IsssysIssHwUpdateL2Filter (pIssAclL2FilterEntry,
                                                   ISS_L2FILTER_DELETE);

                    if (i4RetVal != FNP_SUCCESS)
                    {
                        CLI_SET_ERR (CLI_ACL_HW_UPDATE_FAILED);
                        return SNMP_FAILURE;
                    }
                }
#endif
                pIssAclL2FilterEntry->u1IssL2FilterStatus = ISS_NOT_IN_SERVICE;
                return SNMP_SUCCESS;
            }
            else
            {
                CLI_SET_ERR (CLI_ACL_FILTER_NOT_ACTIVE);
                return SNMP_FAILURE;
            }
            break;

        case ISS_ACTIVE:

            if (ISS_MEMCMP (IssL2FilterNullPortList,
                            pIssAclL2FilterEntry->IssL2FilterInPortList,
                            sizeof (tIssPortList)) == 0)
            {
                /* In Portlist is NULL.
                 * Filter cannot be made active
                 */
                CLI_SET_ERR (CLI_ACL_FILTER_NO_MEMBERS);
                return SNMP_FAILURE;
            }

            /* Checking the L2 filter for the data sufficiency */
            if (IssExtQualifyL2FilterData (&pIssAclL2FilterEntry) !=
                ISS_SUCCESS)
            {
                CLI_SET_ERR (CLI_ACL_FILTER_DATA_INSUFFICIENCY);
                return SNMP_FAILURE;
            }

            if (pIssAclL2FilterEntry->u1IssL2FilterStatus != ISS_NOT_IN_SERVICE)
            {
                CLI_SET_ERR (CLI_ACL_FILTER_ACTIVE_FAILED);
                return SNMP_FAILURE;
            }

#ifdef NPAPI_WANTED
            if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
            {
                /* Call the hardware API to add the L2 filter */
                i4RetVal =
                    IsssysIssHwUpdateL2Filter (pIssAclL2FilterEntry,
                                               ISS_L2FILTER_ADD);

                if (i4RetVal != FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
#endif

            pIssAclL2FilterEntry->u1IssL2FilterStatus = ISS_ACTIVE;

            break;

        case ISS_DESTROY:

            if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
            {

#ifdef NPAPI_WANTED
                if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                {
                    /* Call the hardware API to delete the filter */
                    i4RetVal =
                        IsssysIssHwUpdateL2Filter (pIssAclL2FilterEntry,
                                                   ISS_L2FILTER_DELETE);
                    if (i4RetVal != FNP_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                }
#endif

            }
            /* Deleting the node from the list */
            ISS_SLL_DELETE (&(ISS_L2FILTER_LIST),
                            &(pIssAclL2FilterEntry->IssNextNode));
            /* Delete the Entry from the Software */
            if (ISS_L2FILTERENTRY_FREE_MEM_BLOCK (pIssAclL2FilterEntry)
                != MEM_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            break;

        default:
            return SNMP_FAILURE;
    }

    SnmpNotifyInfo.pu4ObjectId = IssAclL2FilterStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (IssAclL2FilterStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = IssLock;
    SnmpNotifyInfo.pUnLockPointer = IssUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IssAclL2FilterNo,
                      i4SetValIssAclL2FilterStatus));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterOutPortList
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterOutPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterOutPortList (INT4 i4IssAclL2FilterNo,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValIssAclL2FilterOutPortList)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        ISS_MEMSET (pIssAclL2FilterEntry->IssL2FilterOutPortList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_MEMCPY (pIssAclL2FilterEntry->IssL2FilterOutPortList,
                    pSetValIssAclL2FilterOutPortList->pu1_OctetList,
                    pSetValIssAclL2FilterOutPortList->i4_Length);

        SnmpNotifyInfo.pu4ObjectId = IssAclL2FilterOutPortList;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL2FilterOutPortList) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s", i4IssAclL2FilterNo,
                          pSetValIssAclL2FilterOutPortList));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL2FilterDirection
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                setValIssAclL2FilterDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL2FilterDirection (INT4 i4IssAclL2FilterNo,
                               INT4 i4SetValIssAclL2FilterDirection)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry != NULL)
    {
        pIssAclL2FilterEntry->u1FilterDirection =
            (UINT1) i4SetValIssAclL2FilterDirection;

        SnmpNotifyInfo.pu4ObjectId = IssAclL2FilterDirection;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL2FilterDirection) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IssAclL2FilterNo,
                          i4SetValIssAclL2FilterDirection));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterPriority
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterPriority (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                                 INT4 i4TestValIssAclL2FilterPriority)
{
    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
         CLI_SET_ERR (CLI_ACL_INVALID_FILTER_ID);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL2FilterPriority <= ISS_MAX_FILTER_PRIORITY) &&
        (i4TestValIssAclL2FilterPriority >= ISS_DEFAULT_FILTER_PRIORITY))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterEtherType
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterEtherType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterEtherType (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                                  INT4 i4TestValIssAclL2FilterEtherType)
{
    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ACL_INVALID_FILTER_ID);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL2FilterEtherType <= ISS_MIN_ETHER_TYPE_VAL)
        || (i4TestValIssAclL2FilterEtherType > ISS_MAX_ETHER_TYPE_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterProtocolType
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterProtocolType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterProtocolType (UINT4 *pu4ErrorCode,
                                     INT4 i4IssAclL2FilterNo,
                                     UINT4 u4TestValIssAclL2FilterProtocolType)
{
    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ACL_INVALID_FILTER_ID);
        return SNMP_FAILURE;
    }

    if ((u4TestValIssAclL2FilterProtocolType < ISS_MIN_PROTOCOL_ID)
        && (u4TestValIssAclL2FilterProtocolType > ISS_MAX_PROTOCOL_ID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterDstMacAddr
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterDstMacAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterDstMacAddr (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                                   tMacAddr TestValIssAclL2FilterDstMacAddr)
{
    UNUSED_PARAM (TestValIssAclL2FilterDstMacAddr);
    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterSrcMacAddr
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterSrcMacAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterSrcMacAddr (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                                   tMacAddr TestValIssAclL2FilterSrcMacAddr)
{
    UNUSED_PARAM (TestValIssAclL2FilterSrcMacAddr);

    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterVlanId
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterVlanId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterVlanId (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                               INT4 i4TestValIssAclL2FilterVlanId)
{
    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ACL_INVALID_FILTER_ID);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL2FilterVlanId < VLAN_DEV_MIN_VLAN_ID) ||
        (i4TestValIssAclL2FilterVlanId > VLAN_DEV_MAX_VLAN_ID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterInPortList
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterInPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterInPortList (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValIssAclL2FilterInPortList)
{
    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((pTestValIssAclL2FilterInPortList->i4_Length <= 0) ||
        (pTestValIssAclL2FilterInPortList->i4_Length > ISS_PORT_LIST_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    /* This function checks if any port greater than the maximum number of
     * ports in the system is in the port list.*/

    if (IssIsPortListValid
        (pTestValIssAclL2FilterInPortList->pu1_OctetList,
         pTestValIssAclL2FilterInPortList->i4_Length) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterAction
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterAction (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                               INT4 i4TestValIssAclL2FilterAction)
{
    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ACL_INVALID_FILTER_ID);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL2FilterAction == ISS_ALLOW)
        || (i4TestValIssAclL2FilterAction == ISS_DROP) 
        || (i4TestValIssAclL2FilterAction == ISS_REDIRECT_TO))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterStatus
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterStatus (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                               INT4 i4TestValIssAclL2FilterStatus)
{
    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL2FilterStatus < ISS_ACTIVE)
        || (i4TestValIssAclL2FilterStatus > ISS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Check if the filter is mapped to CLass Map entry. If its mapped 
     * filter should not be deleted. */
#if defined (QOSX_WANTED)
    if (i4TestValIssAclL2FilterStatus == ISS_DESTROY)
    {
        if (QoSUtlIsFilterMapToClsMapEntry (QOS_L2FILTER, i4IssAclL2FilterNo)
            == TRUE)
        {
            CLI_SET_ERR (CLI_ACL_FILTER_MAPPED_TO_CLSMAP);
            return SNMP_FAILURE;
        }
    }
#endif

#if defined (DIFFSRV_WANTED)
    if ((i4TestValIssAclL2FilterStatus == ISS_DESTROY)
        || (i4TestValIssAclL2FilterStatus == ISS_NOT_IN_SERVICE))
    {
        ISS_UNLOCK ();

        if (DsCheckMFClfrTable (i4IssAclL2FilterNo, DS_MAC_FILTER) ==
            DS_FAILURE)
        {
            CLI_SET_ERR (CLI_ACL_FILTER_NO_CHANGE);
            ISS_LOCK ();
            return SNMP_FAILURE;
        }

        ISS_LOCK ();
    }
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterOutPortList
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterOutPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterOutPortList (UINT4 *pu4ErrorCode,
                                    INT4 i4IssAclL2FilterNo,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValIssAclL2FilterOutPortList)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((pTestValIssAclL2FilterOutPortList->i4_Length <= 0) ||
        (pTestValIssAclL2FilterOutPortList->i4_Length > ISS_PORT_LIST_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    /* This function checks if any port greater than the maximum number of
     * ports in the system is in the port list.*/

    if (IssIsPortListValid (pTestValIssAclL2FilterOutPortList->pu1_OctetList,
                            pTestValIssAclL2FilterOutPortList->i4_Length) ==
        ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL2FilterDirection
 Input       :  The Indices
                IssAclL2FilterNo

                The Object 
                testValIssAclL2FilterDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL2FilterDirection (UINT4 *pu4ErrorCode, INT4 i4IssAclL2FilterNo,
                                  INT4 i4TestValIssAclL2FilterDirection)
{
    tIssL2FilterEntry  *pIssAclL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssAclL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssAclL2FilterEntry = IssExtGetL2FilterEntry (i4IssAclL2FilterNo);

    if (pIssAclL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssAclL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValIssAclL2FilterDirection != ISS_DIRECTION_IN &&
        i4TestValIssAclL2FilterDirection != ISS_DIRECTION_OUT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssAclL2FilterTable
 Input       :  The Indices
                IssAclL2FilterNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssAclL2FilterTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IssAclL3FilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssAclL3FilterTable
 Input       :  The Indices
                IssAclL3FilterNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssAclL3FilterTable (INT4 i4IssAclL3FilterNo)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pSllNode = ISS_SLL_FIRST (&ISS_L3FILTER_LIST);

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) != ISS_TRUE)
    {
        return SNMP_FAILURE;
    }
    while (pSllNode != NULL)
    {
        pIssAclL3FilterEntry = (tIssL3FilterEntry *) pSllNode;
        if (pIssAclL3FilterEntry->i4IssL3FilterNo == i4IssAclL3FilterNo)
        {
            return SNMP_SUCCESS;
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L3FILTER_LIST, pSllNode);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssAclL3FilterTable
 Input       :  The Indices
                IssAclL3FilterNo
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssAclL3FilterTable (INT4 *pi4IssAclL3FilterNo)
{
    if ((IssExtSnmpLowGetFirstValidL3FilterTableIndex (pi4IssAclL3FilterNo)) ==
        ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssAclL3FilterTable
 Input       :  The Indices
                IssAclL3FilterNo
                nextIssAclL3FilterNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssAclL3FilterTable (INT4 i4IssAclL3FilterNo,
                                    INT4 *pi4NextIssAclL3FilterNo)
{

    if (i4IssAclL3FilterNo < 0)
    {
        return SNMP_FAILURE;
    }

    if ((IssExtSnmpLowGetNextValidL3FilterTableIndex
         (i4IssAclL3FilterNo, pi4NextIssAclL3FilterNo)) == ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterPriority
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterPriority (INT4 i4IssAclL3FilterNo,
                              INT4 *pi4RetValIssAclL3FilterPriority)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterPriority =
            pIssAclL3FilterEntry->i4IssL3FilterPriority;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterProtocol
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterProtocol
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterProtocol (INT4 i4IssAclL3FilterNo,
                              INT4 *pi4RetValIssAclL3FilterProtocol)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if ((pIssAclL3FilterEntry->IssL3FilterProtocol == ISS_PROT_ANY)
            &&(pIssAclL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4))
        {
            if (MsrGetSaveStatus () == ISS_TRUE)
            {
                *pi4RetValIssAclL3FilterProtocol =
                    pIssAclL3FilterEntry->IssL3FilterProtocol;
            }
            else
            {
                *pi4RetValIssAclL3FilterProtocol = ISS_PROT_IP;
            }
        }
        else
        {
            *pi4RetValIssAclL3FilterProtocol =
                pIssAclL3FilterEntry->IssL3FilterProtocol;
        }
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterMessageType
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterMessageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterMessageType (INT4 i4IssAclL3FilterNo,
                                 INT4 *pi4RetValIssAclL3FilterMessageType)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterMessageType =
            pIssAclL3FilterEntry->i4IssL3FilterMessageType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterMessageCode
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterMessageCode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterMessageCode (INT4 i4IssAclL3FilterNo,
                                 INT4 *pi4RetValIssAclL3FilterMessageCode)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterMessageCode =
            pIssAclL3FilterEntry->i4IssL3FilterMessageCode;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilteAddrType
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilteAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilteAddrType (INT4 i4IssAclL3FilterNo,
                             INT4 *pi4RetValIssAclL3FilteAddrType)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIssAclL3FilteAddrType =
        pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterDstIpAddr
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterDstIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterDstIpAddr (INT4 i4IssAclL3FilterNo,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValIssAclL3FilterDstIpAddr)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
        {
            PTR_ASSIGN4 (pRetValIssAclL3FilterDstIpAddr->pu1_OctetList,
                         pIssL3FilterEntry->u4IssL3FilterDstIpAddr);
            pRetValIssAclL3FilterDstIpAddr->i4_Length = QOS_IPV4_LEN;
            return SNMP_SUCCESS;
        }
        else if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
        {
            Ip6AddrCopy ((tIp6Addr *)
                         pRetValIssAclL3FilterDstIpAddr->pu1_OctetList,
                         &pIssL3FilterEntry->ipv6DstIpAddress);

            pRetValIssAclL3FilterDstIpAddr->i4_Length = QOS_IPV6_LEN;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterSrcIpAddr
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterSrcIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterSrcIpAddr (INT4 i4IssAclL3FilterNo,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValIssAclL3FilterSrcIpAddr)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
        {
            PTR_ASSIGN4 (pRetValIssAclL3FilterSrcIpAddr->pu1_OctetList,
                         pIssL3FilterEntry->u4IssL3FilterSrcIpAddr);
            pRetValIssAclL3FilterSrcIpAddr->i4_Length = QOS_IPV4_LEN;
            return SNMP_SUCCESS;
        }
        else if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
        {
            Ip6AddrCopy ((tIp6Addr *) pRetValIssAclL3FilterSrcIpAddr->
                         pu1_OctetList, &pIssL3FilterEntry->ipv6SrcIpAddress);

            pRetValIssAclL3FilterSrcIpAddr->i4_Length = QOS_IPV6_LEN;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterDstIpAddrPrefixLength
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterDstIpAddrPrefixLength
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterDstIpAddrPrefixLength (INT4 i4IssAclL3FilterNo,
                                           UINT4
                                           *pu4RetValIssAclL3FilterDstIpAddrPrefixLength)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pu4RetValIssAclL3FilterDstIpAddrPrefixLength =
            pIssL3FilterEntry->u4IssL3FilterMultiFieldClfrDstPrefixLength;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterSrcIpAddrPrefixLength
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterSrcIpAddrPrefixLength
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterSrcIpAddrPrefixLength (INT4 i4IssAclL3FilterNo,
                                           UINT4
                                           *pu4RetValIssAclL3FilterSrcIpAddrPrefixLength)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        *pu4RetValIssAclL3FilterSrcIpAddrPrefixLength =
            pIssL3FilterEntry->u4IssL3FilterMultiFieldClfrSrcPrefixLength;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterMinDstProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterMinDstProtPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterMinDstProtPort (INT4 i4IssAclL3FilterNo,
                                    UINT4
                                    *pu4RetValIssAclL3FilterMinDstProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pu4RetValIssAclL3FilterMinDstProtPort =
            pIssAclL3FilterEntry->u4IssL3FilterMinDstProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterMaxDstProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterMaxDstProtPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterMaxDstProtPort (INT4 i4IssAclL3FilterNo,
                                    UINT4
                                    *pu4RetValIssAclL3FilterMaxDstProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pu4RetValIssAclL3FilterMaxDstProtPort =
            pIssAclL3FilterEntry->u4IssL3FilterMaxDstProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterMinSrcProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterMinSrcProtPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterMinSrcProtPort (INT4 i4IssAclL3FilterNo,
                                    UINT4
                                    *pu4RetValIssAclL3FilterMinSrcProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pu4RetValIssAclL3FilterMinSrcProtPort =
            pIssAclL3FilterEntry->u4IssL3FilterMinSrcProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterMaxSrcProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterMaxSrcProtPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterMaxSrcProtPort (INT4 i4IssAclL3FilterNo,
                                    UINT4
                                    *pu4RetValIssAclL3FilterMaxSrcProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pu4RetValIssAclL3FilterMaxSrcProtPort =
            pIssAclL3FilterEntry->u4IssL3FilterMaxSrcProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterInPortList
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterInPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterInPortList (INT4 i4IssAclL3FilterNo,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValIssAclL3FilterInPortList)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        ISS_MEMSET (pRetValIssAclL3FilterInPortList->pu1_OctetList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_ADD_PORT_LIST (pRetValIssAclL3FilterInPortList->pu1_OctetList,
                           pIssAclL3FilterEntry->IssL3FilterInPortList);
        pRetValIssAclL3FilterInPortList->i4_Length = ISS_PORT_LIST_SIZE;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterOutPortList
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterOutPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterOutPortList (INT4 i4IssAclL3FilterNo,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValIssAclL3FilterOutPortList)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        ISS_MEMSET (pRetValIssAclL3FilterOutPortList->pu1_OctetList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_ADD_PORT_LIST (pRetValIssAclL3FilterOutPortList->pu1_OctetList,
                           pIssAclL3FilterEntry->IssL3FilterOutPortList);
        pRetValIssAclL3FilterOutPortList->i4_Length = ISS_PORT_LIST_SIZE;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterAckBit
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterAckBit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterAckBit (INT4 i4IssAclL3FilterNo,
                            INT4 *pi4RetValIssAclL3FilterAckBit)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterAckBit =
            pIssAclL3FilterEntry->IssL3FilterAckBit;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterRstBit
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterRstBit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterRstBit (INT4 i4IssAclL3FilterNo,
                            INT4 *pi4RetValIssAclL3FilterRstBit)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterRstBit =
            pIssAclL3FilterEntry->IssL3FilterRstBit;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterTos
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterTos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterTos (INT4 i4IssAclL3FilterNo,
                         INT4 *pi4RetValIssAclL3FilterTos)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterTos = pIssAclL3FilterEntry->IssL3FilterTos;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterDscp
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterDscp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterDscp (INT4 i4IssAclL3FilterNo,
                          INT4 *pi4RetValIssAclL3FilterDscp)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterDscp = pIssAclL3FilterEntry->i4IssL3FilterDscp;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterDirection
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterDirection (INT4 i4IssAclL3FilterNo,
                               INT4 *pi4RetValIssAclL3FilterDirection)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterDirection =
            pIssAclL3FilterEntry->IssL3FilterDirection;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterAction
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterAction (INT4 i4IssAclL3FilterNo,
                            INT4 *pi4RetValIssAclL3FilterAction)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterAction =
            pIssAclL3FilterEntry->IssL3FilterAction;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterMatchCount
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterMatchCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterMatchCount (INT4 i4IssAclL3FilterNo,
                                UINT4 *pu4RetValIssAclL3FilterMatchCount)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
#ifdef NPAPI_WANTED
    INT4  i4RetVal = FNP_FAILURE;
#endif

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);
#ifdef NPAPI_WANTED
    if ((pIssAclL3FilterEntry != NULL) &&
         (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE))
    {
        i4RetVal = IsssysIssHwUpdateL3Filter (pIssAclL3FilterEntry,
                                                ISS_L3FILTER_STAT_GET);
        if (i4RetVal != FNP_FAILURE)
        {
            *pu4RetValIssAclL3FilterMatchCount = 
                pIssAclL3FilterEntry->u4IssL3FilterMatchCount;
            return SNMP_SUCCESS;
        }
        return SNMP_FAILURE;
    }
#else
    if (pIssAclL3FilterEntry != NULL)
    {
        *pu4RetValIssAclL3FilterMatchCount =
            pIssAclL3FilterEntry->u4IssL3FilterMatchCount;
        return SNMP_SUCCESS;
    }
#endif
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterFlowId
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterFlowId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterFlowId (INT4 i4IssAclL3FilterNo,
                            UINT4 *pu4RetValIssAclL3FilterFlowId)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);
    if (pIssL3FilterEntry != NULL)
    {
        *pu4RetValIssAclL3FilterFlowId =
            pIssL3FilterEntry->u4IssL3MultiFieldClfrFlowId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetIssAclL3FilterStatus
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                retValIssAclL3FilterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssAclL3FilterStatus (INT4 i4IssAclL3FilterNo,
                            INT4 *pi4RetValIssAclL3FilterStatus)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        *pi4RetValIssAclL3FilterStatus =
            (INT4) pIssAclL3FilterEntry->u1IssL3FilterStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterPriority
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterPriority (INT4 i4IssAclL3FilterNo,
                              INT4 i4SetValIssAclL3FilterPriority)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->i4IssL3FilterPriority =
            i4SetValIssAclL3FilterPriority;
        SnmpNotifyInfo.pu4ObjectId = IssAclL3FilterPriority;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL3FilterPriority) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IssAclL3FilterNo,
                          i4SetValIssAclL3FilterPriority));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterProtocol
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterProtocol
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterProtocol (INT4 i4IssAclL3FilterNo,
                              INT4 i4SetValIssAclL3FilterProtocol)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->IssL3FilterProtocol =
            i4SetValIssAclL3FilterProtocol;
        SnmpNotifyInfo.pu4ObjectId = IssAclL3FilterProtocol;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL3FilterProtocol) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IssAclL3FilterNo,
                          i4SetValIssAclL3FilterProtocol));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterMessageType
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterMessageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterMessageType (INT4 i4IssAclL3FilterNo,
                                 INT4 i4SetValIssAclL3FilterMessageType)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->i4IssL3FilterMessageType =
            i4SetValIssAclL3FilterMessageType;
        SnmpNotifyInfo.pu4ObjectId = IssAclL3FilterMessageType;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL3FilterMessageType) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IssAclL3FilterNo,
                          i4SetValIssAclL3FilterMessageType));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterMessageCode
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterMessageCode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterMessageCode (INT4 i4IssAclL3FilterNo,
                                 INT4 i4SetValIssAclL3FilterMessageCode)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->i4IssL3FilterMessageCode =
            i4SetValIssAclL3FilterMessageCode;
        SnmpNotifyInfo.pu4ObjectId = IssAclL3FilterMessageCode;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL3FilterMessageCode) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IssAclL3FilterNo,
                          i4SetValIssAclL3FilterMessageCode));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilteAddrType
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilteAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilteAddrType (INT4 i4IssAclL3FilterNo,
                             INT4 i4SetValIssAclL3FilteAddrType)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    if (IssSetL3FilterAddrType (i4IssAclL3FilterNo,
                                i4SetValIssAclL3FilteAddrType) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    SnmpNotifyInfo.pu4ObjectId = IssAclL3FilteAddrType;
    SnmpNotifyInfo.u4OidLen = sizeof (IssAclL3FilteAddrType) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = IssLock;
    SnmpNotifyInfo.pUnLockPointer = IssUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IssAclL3FilterNo,
                      i4SetValIssAclL3FilteAddrType));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterDstIpAddr
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterDstIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterDstIpAddr (INT4 i4IssAclL3FilterNo,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValIssAclL3FilterDstIpAddr)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    INT4                i4RetStatus = SNMP_FAILURE;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);
    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
        {
            Ip6AddrCopy (&pIssL3FilterEntry->ipv6DstIpAddress,
                         (tIp6Addr *) pSetValIssAclL3FilterDstIpAddr->
                         pu1_OctetList);
            i4RetStatus = SNMP_SUCCESS;
        }
        if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
        {
            PTR_FETCH4 (pIssL3FilterEntry->u4IssL3FilterDstIpAddr,
                        pSetValIssAclL3FilterDstIpAddr->pu1_OctetList);

            i4RetStatus = SNMP_SUCCESS;
        }
    }
    if (i4RetStatus == SNMP_SUCCESS)
    {
        SnmpNotifyInfo.pu4ObjectId = IssAclL3FilterDstIpAddr;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL3FilterDstIpAddr) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s", i4IssAclL3FilterNo,
                          pSetValIssAclL3FilterDstIpAddr));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterSrcIpAddr
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterSrcIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterSrcIpAddr (INT4 i4IssAclL3FilterNo,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValIssAclL3FilterSrcIpAddr)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    INT4                i4RetStatus = SNMP_FAILURE;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry != NULL)
    {
        if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
        {
            Ip6AddrCopy (&pIssL3FilterEntry->ipv6SrcIpAddress,
                         (tIp6Addr *) pSetValIssAclL3FilterSrcIpAddr->
                         pu1_OctetList);
            i4RetStatus = SNMP_SUCCESS;
        }
        if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
        {
            PTR_FETCH4 (pIssL3FilterEntry->u4IssL3FilterSrcIpAddr,
                        pSetValIssAclL3FilterSrcIpAddr->pu1_OctetList);

            i4RetStatus = SNMP_SUCCESS;
        }
    }

    if (i4RetStatus == SNMP_SUCCESS)
    {
        SnmpNotifyInfo.pu4ObjectId = IssAclL3FilterSrcIpAddr;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL3FilterSrcIpAddr) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s", i4IssAclL3FilterNo,
                          pSetValIssAclL3FilterSrcIpAddr));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterDstIpAddrPrefixLength
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterDstIpAddrPrefixLength
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterDstIpAddrPrefixLength (INT4 i4IssAclL3FilterNo,
                                           UINT4
                                           u4SetValIssAclL3FilterDstIpAddrPrefixLength)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    if (IssSetL3FilterDstPrefixLength (i4IssAclL3FilterNo,
                                       u4SetValIssAclL3FilterDstIpAddrPrefixLength)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    SnmpNotifyInfo.pu4ObjectId = IssAclL3FilterDstIpAddrPrefixLength;
    SnmpNotifyInfo.u4OidLen =
        sizeof (IssAclL3FilterDstIpAddrPrefixLength) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = IssLock;
    SnmpNotifyInfo.pUnLockPointer = IssUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u", i4IssAclL3FilterNo,
                      u4SetValIssAclL3FilterDstIpAddrPrefixLength));
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterSrcIpAddrPrefixLength
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterSrcIpAddrPrefixLength
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterSrcIpAddrPrefixLength (INT4 i4IssAclL3FilterNo,
                                           UINT4
                                           u4SetValIssAclL3FilterSrcIpAddrPrefixLength)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    if (IssSetL3FilterSrcPrefixLength (i4IssAclL3FilterNo,
                                       u4SetValIssAclL3FilterSrcIpAddrPrefixLength)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    SnmpNotifyInfo.pu4ObjectId = IssAclL3FilterSrcIpAddrPrefixLength;
    SnmpNotifyInfo.u4OidLen =
        sizeof (IssAclL3FilterSrcIpAddrPrefixLength) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = IssLock;
    SnmpNotifyInfo.pUnLockPointer = IssUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u", i4IssAclL3FilterNo,
                      u4SetValIssAclL3FilterSrcIpAddrPrefixLength));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterMinDstProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterMinDstProtPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterMinDstProtPort (INT4 i4IssAclL3FilterNo,
                                    UINT4 u4SetValIssAclL3FilterMinDstProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->u4IssL3FilterMinDstProtPort =
            u4SetValIssAclL3FilterMinDstProtPort;
        SnmpNotifyInfo.pu4ObjectId = IssAclL3FilterMinDstProtPort;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL3FilterMinDstProtPort) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u", i4IssAclL3FilterNo,
                          u4SetValIssAclL3FilterMinDstProtPort));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterMaxDstProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterMaxDstProtPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterMaxDstProtPort (INT4 i4IssAclL3FilterNo,
                                    UINT4 u4SetValIssAclL3FilterMaxDstProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->u4IssL3FilterMaxDstProtPort =
            u4SetValIssAclL3FilterMaxDstProtPort;
        SnmpNotifyInfo.pu4ObjectId = IssAclL3FilterMaxDstProtPort;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL3FilterMaxDstProtPort) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u", i4IssAclL3FilterNo,
                          u4SetValIssAclL3FilterMaxDstProtPort));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterMinSrcProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterMinSrcProtPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterMinSrcProtPort (INT4 i4IssAclL3FilterNo,
                                    UINT4 u4SetValIssAclL3FilterMinSrcProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->u4IssL3FilterMinSrcProtPort =
            u4SetValIssAclL3FilterMinSrcProtPort;
        SnmpNotifyInfo.pu4ObjectId = IssAclL3FilterMinSrcProtPort;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL3FilterMinSrcProtPort) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u", i4IssAclL3FilterNo,
                          u4SetValIssAclL3FilterMinSrcProtPort));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterMaxSrcProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterMaxSrcProtPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterMaxSrcProtPort (INT4 i4IssAclL3FilterNo,
                                    UINT4 u4SetValIssAclL3FilterMaxSrcProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->u4IssL3FilterMaxSrcProtPort =
            u4SetValIssAclL3FilterMaxSrcProtPort;
        SnmpNotifyInfo.pu4ObjectId = IssAclL3FilterMaxSrcProtPort;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL3FilterMaxSrcProtPort) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u", i4IssAclL3FilterNo,
                          u4SetValIssAclL3FilterMaxSrcProtPort));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterInPortList
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterInPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterInPortList (INT4 i4IssAclL3FilterNo,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValIssAclL3FilterInPortList)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        ISS_MEMSET (pIssAclL3FilterEntry->IssL3FilterInPortList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_MEMCPY (pIssAclL3FilterEntry->IssL3FilterInPortList,
                    pSetValIssAclL3FilterInPortList->pu1_OctetList,
                    pSetValIssAclL3FilterInPortList->i4_Length);
        SnmpNotifyInfo.pu4ObjectId = IssAclL3FilterInPortList;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL3FilterInPortList) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s", i4IssAclL3FilterNo,
                          pSetValIssAclL3FilterInPortList));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterOutPortList
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterOutPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterOutPortList (INT4 i4IssAclL3FilterNo,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValIssAclL3FilterOutPortList)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        ISS_MEMSET (pIssAclL3FilterEntry->IssL3FilterOutPortList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_MEMCPY (pIssAclL3FilterEntry->IssL3FilterOutPortList,
                    pSetValIssAclL3FilterOutPortList->pu1_OctetList,
                    pSetValIssAclL3FilterOutPortList->i4_Length);
        SnmpNotifyInfo.pu4ObjectId = IssAclL3FilterOutPortList;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL3FilterOutPortList) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s", i4IssAclL3FilterNo,
                          pSetValIssAclL3FilterOutPortList));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterAckBit
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterAckBit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterAckBit (INT4 i4IssAclL3FilterNo,
                            INT4 i4SetValIssAclL3FilterAckBit)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->IssL3FilterAckBit = i4SetValIssAclL3FilterAckBit;
        SnmpNotifyInfo.pu4ObjectId = IssAclL3FilterAckBit;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL3FilterAckBit) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IssAclL3FilterNo,
                          i4SetValIssAclL3FilterAckBit));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterRstBit
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterRstBit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterRstBit (INT4 i4IssAclL3FilterNo,
                            INT4 i4SetValIssAclL3FilterRstBit)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->IssL3FilterRstBit = i4SetValIssAclL3FilterRstBit;
        SnmpNotifyInfo.pu4ObjectId = IssAclL3FilterRstBit;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL3FilterRstBit) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IssAclL3FilterNo,
                          i4SetValIssAclL3FilterRstBit));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterTos
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterTos
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterTos (INT4 i4IssAclL3FilterNo,
                         INT4 i4SetValIssAclL3FilterTos)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->IssL3FilterTos = i4SetValIssAclL3FilterTos;
        SnmpNotifyInfo.pu4ObjectId = IssAclL3FilterTos;
        SnmpNotifyInfo.u4OidLen = sizeof (IssAclL3FilterTos) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IssAclL3FilterNo,
                          i4SetValIssAclL3FilterTos));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterDscp
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterDscp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterDscp (INT4 i4IssAclL3FilterNo,
                          INT4 i4SetValIssAclL3FilterDscp)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->i4IssL3FilterDscp = i4SetValIssAclL3FilterDscp;
        SnmpNotifyInfo.pu4ObjectId = IssAclL3FilterDscp;
        SnmpNotifyInfo.u4OidLen = sizeof (IssAclL3FilterDscp) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IssAclL3FilterNo,
                          i4SetValIssAclL3FilterDscp));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterDirection
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterDirection (INT4 i4IssAclL3FilterNo,
                               INT4 i4SetValIssAclL3FilterDirection)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->IssL3FilterDirection =
            i4SetValIssAclL3FilterDirection;
        SnmpNotifyInfo.pu4ObjectId = IssAclL3FilterDirection;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL3FilterDirection) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IssAclL3FilterNo,
                          i4SetValIssAclL3FilterDirection));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterAction
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterAction (INT4 i4IssAclL3FilterNo,
                            INT4 i4SetValIssAclL3FilterAction)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
        {
            return SNMP_FAILURE;
        }

        pIssAclL3FilterEntry->IssL3FilterAction = i4SetValIssAclL3FilterAction;
        SnmpNotifyInfo.pu4ObjectId = IssAclL3FilterAction;
        SnmpNotifyInfo.u4OidLen =
            sizeof (IssAclL3FilterAction) / sizeof (UINT4);
        SnmpNotifyInfo.u4SeqNum = 0;
        SnmpNotifyInfo.u1RowStatus = FALSE;
        SnmpNotifyInfo.pLockPointer = IssLock;
        SnmpNotifyInfo.pUnLockPointer = IssUnLock;
        SnmpNotifyInfo.u4Indices = 1;
        SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IssAclL3FilterNo,
                          i4SetValIssAclL3FilterAction));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterFlowId
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterFlowId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterFlowId (INT4 i4IssAclL3FilterNo,
                            UINT4 u4SetValIssAclL3FilterFlowId)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    if (IssSetL3FilterControlFlowId (i4IssAclL3FilterNo,
                                     u4SetValIssAclL3FilterFlowId) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    SnmpNotifyInfo.pu4ObjectId = IssAclL3FilterFlowId;
    SnmpNotifyInfo.u4OidLen = sizeof (IssAclL3FilterFlowId) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = IssLock;
    SnmpNotifyInfo.pUnLockPointer = IssUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %u", i4IssAclL3FilterNo,
                      u4SetValIssAclL3FilterFlowId));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssAclL3FilterStatus
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                setValIssAclL3FilterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssAclL3FilterStatus (INT4 i4IssAclL3FilterNo,
                            INT4 i4SetValIssAclL3FilterStatus)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif
    tIssPortList        IssL3FilterNullPortList;

    ISS_MEMSET (IssL3FilterNullPortList, 0, sizeof (tIssPortList));

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->u1IssL3FilterStatus ==
            (UINT1) i4SetValIssAclL3FilterStatus)
        {
            return SNMP_SUCCESS;
        }
    }
    else
    {
        /* This Filter Entry is not there in the Database */
        if (i4SetValIssAclL3FilterStatus != ISS_CREATE_AND_WAIT)
        {
            CLI_SET_ERR (CLI_ACL_NO_FILTER);
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValIssAclL3FilterStatus)
    {

        case ISS_CREATE_AND_WAIT:

            if ((ISS_L3FILTERENTRY_ALLOC_MEM_BLOCK (pIssAclL3FilterEntry)) ==
                NULL)
            {
                /* No Free L3 Filter Entry or h/w init failed at startup */
                CLI_SET_ERR (CLI_ACL_FILTER_MAX);
                return SNMP_FAILURE;
            }

            ISS_MEMSET (pIssAclL3FilterEntry, 0, sizeof (tIssL3FilterEntry));

            /* Setting all the default values for the objects */
            pIssAclL3FilterEntry->i4IssL3FilterNo = i4IssAclL3FilterNo;

            pIssAclL3FilterEntry->i4IssL3FilterPriority =
                ISS_DEFAULT_FILTER_PRIORITY;

            pIssAclL3FilterEntry->IssL3FilterProtocol = ISS_ANY;
            pIssAclL3FilterEntry->u4SVlanId = 0;
            pIssAclL3FilterEntry->i1SVlanPriority = ISS_DEFAULT_VLAN_PRIORITY;
            pIssAclL3FilterEntry->u4CVlanId = 0;
            pIssAclL3FilterEntry->i1CVlanPriority = ISS_DEFAULT_VLAN_PRIORITY;
            pIssAclL3FilterEntry->u1IssL3FilterTagType = ISS_FILTER_SINGLE_TAG;
            pIssAclL3FilterEntry->i4IssL3FilterMessageType =
                ISS_DEFAULT_MSG_TYPE;
            pIssAclL3FilterEntry->i4IssL3FilterMessageCode =
                ISS_DEFAULT_MSG_CODE;
            pIssAclL3FilterEntry->u4IssL3FilterDstIpAddr = ISS_ZERO_ENTRY;
            pIssAclL3FilterEntry->u4IssL3FilterSrcIpAddr = ISS_ZERO_ENTRY;
            pIssAclL3FilterEntry->u4IssL3FilterDstIpAddrMask =
                ISS_DEF_FILTER_MASK;
            pIssAclL3FilterEntry->u4IssL3FilterSrcIpAddrMask =
                ISS_DEF_FILTER_MASK;
            pIssAclL3FilterEntry->u4IssL3FilterMinDstProtPort =
                ISS_MIN_PORT_VALUE;
            pIssAclL3FilterEntry->u4IssL3FilterMaxDstProtPort =
                ISS_MAX_PORT_VALUE;
            pIssAclL3FilterEntry->u4IssL3FilterMinSrcProtPort =
                ISS_MIN_PORT_VALUE;;
            pIssAclL3FilterEntry->u4IssL3FilterMaxSrcProtPort =
                ISS_MAX_PORT_VALUE;
            pIssAclL3FilterEntry->IssL3FilterAckBit = ISS_ACK_ANY;
            pIssAclL3FilterEntry->IssL3FilterRstBit = ISS_RST_ANY;
            pIssAclL3FilterEntry->IssL3FilterTos = ISS_TOS_INVALID;
            pIssAclL3FilterEntry->i4IssL3FilterDscp = ISS_DSCP_INVALID;
            pIssAclL3FilterEntry->IssL3FilterDirection = ISS_DIRECTION_IN;
            pIssAclL3FilterEntry->IssL3FilterAction = ISS_ALLOW;
            pIssAclL3FilterEntry->u4IssL3FilterMatchCount = ISS_ZERO_ENTRY;
            pIssAclL3FilterEntry->u4IssL3FilterMultiFieldClfrDstPrefixLength =
                ISS_DEF_PREFIX_LEN;
            pIssAclL3FilterEntry->u4IssL3FilterMultiFieldClfrSrcPrefixLength =
                ISS_DEF_PREFIX_LEN;
            pIssAclL3FilterEntry->u4IssL3MultiFieldClfrFlowId = ISS_ZERO_ENTRY;
            pIssAclL3FilterEntry->i4IssL3MultiFieldClfrAddrType = QOS_IPV4;
            pIssAclL3FilterEntry->u4RefCount = ISS_ZERO_ENTRY;

            /* Adding the Entry to the Filter List */
            ISS_SLL_ADD (&(ISS_L3FILTER_LIST),
                         &(pIssAclL3FilterEntry->IssNextNode));

            /* Setting the RowStatus to NOT_READY as all 
             * mandatory objects are not set taken. */
            pIssAclL3FilterEntry->u1IssL3FilterStatus = ISS_NOT_READY;

            break;

        case ISS_NOT_IN_SERVICE:

            if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
            {

#ifdef NPAPI_WANTED
                if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                {
                    /* Call the hardware API to update the filter */
                    i4RetVal =
                        IsssysIssHwUpdateL3Filter (pIssAclL3FilterEntry,
                                                   ISS_L3FILTER_DELETE);
                    if (i4RetVal != FNP_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                }
#endif

                pIssAclL3FilterEntry->u1IssL3FilterStatus = ISS_NOT_IN_SERVICE;

                return SNMP_SUCCESS;
            }
            else
            {
                CLI_SET_ERR (CLI_ACL_FILTER_NOT_ACTIVE);
                return SNMP_FAILURE;
            }

            break;

        case ISS_ACTIVE:

            if ((ISS_MEMCMP (IssL3FilterNullPortList,
                             pIssAclL3FilterEntry->IssL3FilterInPortList,
                             sizeof (tIssPortList)) == 0)
                && (ISS_MEMCMP (IssL3FilterNullPortList,
                                pIssAclL3FilterEntry->IssL3FilterOutPortList,
                                sizeof (tIssPortList)) == 0))
            {
                /* In Portlist is NULL.
                 * Filter cannot be made active
                 */
                CLI_SET_ERR (CLI_ACL_FILTER_NO_MEMBERS);
                return SNMP_FAILURE;
            }

            /* Checking the L3 filter for the data sufficiency */
            if (IssExtQualifyL3FilterData (&pIssAclL3FilterEntry) !=
                ISS_SUCCESS)
            {
                CLI_SET_ERR (CLI_ACL_FILTER_DATA_INSUFFICIENCY);
                return SNMP_FAILURE;
            }

            if (pIssAclL3FilterEntry->u1IssL3FilterStatus != ISS_NOT_IN_SERVICE)
            {
                CLI_SET_ERR (CLI_ACL_FILTER_ACTIVE_FAILED);
                return SNMP_FAILURE;
            }

#ifdef NPAPI_WANTED
            if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
            {
                /* Call the hardware API to add the L3 filter */
                i4RetVal =
                    IsssysIssHwUpdateL3Filter (pIssAclL3FilterEntry,
                                               ISS_L3FILTER_ADD);

                if (i4RetVal != FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
#endif

            pIssAclL3FilterEntry->u1IssL3FilterStatus = ISS_ACTIVE;

            break;

        case ISS_DESTROY:

            if (pIssAclL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
            {

#ifdef NPAPI_WANTED
                if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                {
                    /* Call the hardware API to delete the filter */
                    i4RetVal =
                        IsssysIssHwUpdateL3Filter (pIssAclL3FilterEntry,
                                                   ISS_L3FILTER_DELETE);
                    if (i4RetVal != FNP_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                }
#endif

            }
            /* Deleting the node from the list */
            ISS_SLL_DELETE (&(ISS_L3FILTER_LIST),
                            &(pIssAclL3FilterEntry->IssNextNode));
            /* Delete the Entry from the Software */
            if (ISS_L3FILTERENTRY_FREE_MEM_BLOCK (pIssAclL3FilterEntry)
                != MEM_SUCCESS)
            {
                /* L3 Filter Entry Free Failure */
                CLI_SET_ERR (CLI_ACL_DELETION_FAILED);
                return SNMP_FAILURE;
            }

            break;

        default:
            return SNMP_FAILURE;
    }
#ifdef QOSX_WANTED
    QosUpdateDiffServMFCNextFree (i4IssAclL3FilterNo,
                                  i4SetValIssAclL3FilterStatus);
#endif
    SnmpNotifyInfo.pu4ObjectId = IssAclL3FilterStatus;
    SnmpNotifyInfo.u4OidLen = sizeof (IssAclL3FilterStatus) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = 0;
    SnmpNotifyInfo.u1RowStatus = TRUE;
    SnmpNotifyInfo.pLockPointer = IssLock;
    SnmpNotifyInfo.pUnLockPointer = IssUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4IssAclL3FilterNo,
                      i4SetValIssAclL3FilterStatus));
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterPriority
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterPriority (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                                 INT4 i4TestValIssAclL3FilterPriority)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ACL_INVALID_FILTER_ID);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterPriority <= ISS_MAX_FILTER_PRIORITY) &&
        (i4TestValIssAclL3FilterPriority >= ISS_DEFAULT_FILTER_PRIORITY))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterProtocol
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterProtocol
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterProtocol (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                                 INT4 i4TestValIssAclL3FilterProtocol)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ACL_INVALID_FILTER_ID);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterProtocol < ISS_L3_MIN_PROTOCOL)
        || (i4TestValIssAclL3FilterProtocol > ISS_L3_MAX_PROTOCOL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterMessageType
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterMessageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterMessageType (UINT4 *pu4ErrorCode,
                                    INT4 i4IssAclL3FilterNo,
                                    INT4 i4TestValIssAclL3FilterMessageType)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ACL_INVALID_FILTER_ID);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterMessageType < ISS_MIN_MSG_TYPE)
        || (i4TestValIssAclL3FilterMessageType > ISS_MAX_MSG_TYPE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterMessageCode
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterMessageCode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterMessageCode (UINT4 *pu4ErrorCode,
                                    INT4 i4IssAclL3FilterNo,
                                    INT4 i4TestValIssAclL3FilterMessageCode)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_ACL_INVALID_FILTER_ID);
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterMessageCode < ISS_MIN_MSG_CODE)
        || (i4TestValIssAclL3FilterMessageCode > ISS_MAX_MSG_CODE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilteAddrType
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilteAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilteAddrType (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                                INT4 i4TestValIssAclL3FilteAddrType)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValIssAclL3FilteAddrType == QOS_IPV4) ||
        (i4TestValIssAclL3FilteAddrType == QOS_IPV6))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterDstIpAddr
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterDstIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterDstIpAddr (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValIssAclL3FilterDstIpAddr)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
    {
        if (pTestValIssAclL3FilterDstIpAddr->i4_Length != QOS_IPV4_LEN)
        {
            CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
            return SNMP_FAILURE;
        }

        PTR_FETCH4 (pIssL3FilterEntry->u4IssL3FilterDstIpAddr,
                    pTestValIssAclL3FilterDstIpAddr->pu1_OctetList);

        /* Allow IP address which are Class A, B, C or D */
        if (!
            ((ISS_IS_ADDR_CLASS_A (pIssL3FilterEntry->u4IssL3FilterDstIpAddr))
             ||
             (ISS_IS_ADDR_CLASS_B (pIssL3FilterEntry->u4IssL3FilterDstIpAddr))
             ||
             (ISS_IS_ADDR_CLASS_C (pIssL3FilterEntry->u4IssL3FilterDstIpAddr))
             ||
             (ISS_IS_ADDR_CLASS_D (pIssL3FilterEntry->u4IssL3FilterDstIpAddr))))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
            return SNMP_FAILURE;
        }
    }

    if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
    {
        if (pTestValIssAclL3FilterDstIpAddr->i4_Length != QOS_IPV6_LEN)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterSrcIpAddr
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterSrcIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterSrcIpAddr (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValIssAclL3FilterSrcIpAddr)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
    {
        if (pTestValIssAclL3FilterSrcIpAddr->i4_Length != QOS_IPV4_LEN)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
            return SNMP_FAILURE;
        }

        PTR_FETCH4 (pIssL3FilterEntry->u4IssL3FilterSrcIpAddr,
                    pTestValIssAclL3FilterSrcIpAddr->pu1_OctetList);

        /* Allow IP address which are Class A, B, C or D */
        if (!
            ((ISS_IS_ADDR_CLASS_A (pIssL3FilterEntry->u4IssL3FilterSrcIpAddr))
             ||
             (ISS_IS_ADDR_CLASS_B (pIssL3FilterEntry->u4IssL3FilterSrcIpAddr))
             ||
             (ISS_IS_ADDR_CLASS_C (pIssL3FilterEntry->u4IssL3FilterSrcIpAddr))
             ||
             (ISS_IS_ADDR_CLASS_D (pIssL3FilterEntry->u4IssL3FilterSrcIpAddr))))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
            return SNMP_FAILURE;
        }

    }
    if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
    {
        if (pTestValIssAclL3FilterSrcIpAddr->i4_Length != QOS_IPV6_LEN)
        {
            CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterDstIpAddrPrefixLength
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterDstIpAddrPrefixLength
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterDstIpAddrPrefixLength (UINT4 *pu4ErrorCode,
                                              INT4 i4IssAclL3FilterNo,
                                              UINT4
                                              u4TestValIssAclL3FilterDstIpAddrPrefixLength)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
    {
        if (u4TestValIssAclL3FilterDstIpAddrPrefixLength >
            QOS_MF_PERFIX_IPV4_MAX)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
            return SNMP_FAILURE;
        }
    }
    else if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
    {
        if ((u4TestValIssAclL3FilterDstIpAddrPrefixLength >
             QOS_MF_PERFIX_IPV6_MAX))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterSrcIpAddrPrefixLength
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength (UINT4 *pu4ErrorCode,
                                              INT4 i4IssAclL3FilterNo,
                                              UINT4
                                              u4TestValIssAclL3FilterSrcIpAddrPrefixLength)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV4)
    {
        if (u4TestValIssAclL3FilterSrcIpAddrPrefixLength >
            QOS_MF_PERFIX_IPV4_MAX)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
            return SNMP_FAILURE;
        }
    }
    else if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType == QOS_IPV6)
    {
        if ((u4TestValIssAclL3FilterSrcIpAddrPrefixLength >
             QOS_MF_PERFIX_IPV6_MAX))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterMinDstProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterMinDstProtPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterMinDstProtPort (UINT4 *pu4ErrorCode,
                                       INT4 i4IssAclL3FilterNo,
                                       UINT4
                                       u4TestValIssAclL3FilterMinDstProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (((INT4)u4TestValIssAclL3FilterMinDstProtPort <= ISS_MAX_PORT_VALUE)
        && ((INT4)u4TestValIssAclL3FilterMinDstProtPort >= ISS_MIN_PORT_VALUE))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterMaxDstProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterMaxDstProtPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterMaxDstProtPort (UINT4 *pu4ErrorCode,
                                       INT4 i4IssAclL3FilterNo,
                                       UINT4
                                       u4TestValIssAclL3FilterMaxDstProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (((INT4)u4TestValIssAclL3FilterMaxDstProtPort <= ISS_MAX_PORT_VALUE)
        && ((INT4)u4TestValIssAclL3FilterMaxDstProtPort >= ISS_MIN_PORT_VALUE))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterMinSrcProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterMinSrcProtPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterMinSrcProtPort (UINT4 *pu4ErrorCode,
                                       INT4 i4IssAclL3FilterNo,
                                       UINT4
                                       u4TestValIssAclL3FilterMinSrcProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (((INT4)u4TestValIssAclL3FilterMinSrcProtPort <= ISS_MAX_PORT_VALUE)
        && ((INT4)u4TestValIssAclL3FilterMinSrcProtPort >= ISS_MIN_PORT_VALUE))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterMaxSrcProtPort
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterMaxSrcProtPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterMaxSrcProtPort (UINT4 *pu4ErrorCode,
                                       INT4 i4IssAclL3FilterNo,
                                       UINT4
                                       u4TestValIssAclL3FilterMaxSrcProtPort)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (((INT4)u4TestValIssAclL3FilterMaxSrcProtPort <= ISS_MAX_PORT_VALUE)
        && ((INT4)u4TestValIssAclL3FilterMaxSrcProtPort >=  ISS_MIN_PORT_VALUE))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterInPortList
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterInPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterInPortList (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValIssAclL3FilterInPortList)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((pTestValIssAclL3FilterInPortList->i4_Length <= 0) ||
        (pTestValIssAclL3FilterInPortList->i4_Length > ISS_PORT_LIST_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (IssIsPortListValid
        (pTestValIssAclL3FilterInPortList->pu1_OctetList,
         pTestValIssAclL3FilterInPortList->i4_Length) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterOutPortList
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterOutPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterOutPortList (UINT4 *pu4ErrorCode,
                                    INT4 i4IssAclL3FilterNo,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValIssAclL3FilterOutPortList)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((pTestValIssAclL3FilterOutPortList->i4_Length <= 0) ||
        (pTestValIssAclL3FilterOutPortList->i4_Length > ISS_PORT_LIST_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (IssIsPortListValid
        (pTestValIssAclL3FilterOutPortList->pu1_OctetList,
         pTestValIssAclL3FilterOutPortList->i4_Length) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterAckBit
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterAckBit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterAckBit (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                               INT4 i4TestValIssAclL3FilterAckBit)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterAckBit >= ISS_ACK_ESTABLISH)
        && (i4TestValIssAclL3FilterAckBit <= ISS_ACK_ANY))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterRstBit
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterRstBit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterRstBit (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                               INT4 i4TestValIssAclL3FilterRstBit)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterRstBit >= ISS_RST_SET)
        && (i4TestValIssAclL3FilterRstBit <= ISS_RST_ANY))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterTos
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterTos
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterTos (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                            INT4 i4TestValIssAclL3FilterTos)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if (pIssAclL3FilterEntry->i4IssL3FilterDscp != ISS_DSCP_INVALID)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValIssAclL3FilterTos >= ISS_TOS_NONE)
        && (i4TestValIssAclL3FilterTos < ISS_TOS_INVALID))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterDscp
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterDscp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterDscp (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                             INT4 i4TestValIssAclL3FilterDscp)
{
    tIssL3FilterEntry  *pIssAclL3FilterEntry = NULL;
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    pIssAclL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssAclL3FilterEntry != NULL)
    {
        if ((pIssAclL3FilterEntry->IssL3FilterTos != ISS_TOS_INVALID) &&
            (pIssAclL3FilterEntry->IssL3FilterTos != ISS_TOS_NONE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (((i4TestValIssAclL3FilterDscp >= ISS_MIN_DSCP_VALUE) &&
         (i4TestValIssAclL3FilterDscp <= ISS_MAX_DSCP_VALUE)) ||
        (i4TestValIssAclL3FilterDscp == ISS_DSCP_INVALID))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterDirection
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterDirection (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                                  INT4 i4TestValIssAclL3FilterDirection)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;
    tIssPortList        IssL3FilterNullPortList;

    ISS_MEMSET (IssL3FilterNullPortList, 0, sizeof (tIssPortList));

    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Ensure only portlist is configured for a single direction, 
     * if Inportlist is already configured then dont allow to 
     * configure outportlist and vice versa */
    if (i4TestValIssAclL3FilterDirection == ISS_DIRECTION_IN)
    {
        /* User is trying to create INGRESS ports so ensure 
         * no EGRESS ports are already present */

        if (ISS_MEMCMP (IssL3FilterNullPortList,
                        pIssL3FilterEntry->IssL3FilterOutPortList,
                        sizeof (tIssPortList)) != 0)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (i4TestValIssAclL3FilterDirection == ISS_DIRECTION_OUT)
    {
        /* User is trying to create EGRESS ACL so ensure 
         * no INGRESS Ports are already present */
        if (ISS_MEMCMP (IssL3FilterNullPortList,
                        pIssL3FilterEntry->IssL3FilterInPortList,
                        sizeof (tIssPortList)) != 0)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterAction
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterAction (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                               INT4 i4TestValIssAclL3FilterAction)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterAction == ISS_ALLOW)
        || (i4TestValIssAclL3FilterAction == ISS_DROP)
         || (i4TestValIssAclL3FilterAction == ISS_REDIRECT_TO))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterFlowId
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterFlowId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterFlowId (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                               UINT4 u4TestValIssAclL3FilterFlowId)
{
    tIssL3FilterEntry  *pIssL3FilterEntry = NULL;

    pIssL3FilterEntry = IssExtGetL3FilterEntry (i4IssAclL3FilterNo);

    if (pIssL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssL3FilterEntry->i4IssL3MultiFieldClfrAddrType != QOS_IPV6)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((u4TestValIssAclL3FilterFlowId == QOS_FLOW_ID_MIN) ||
        ((u4TestValIssAclL3FilterFlowId >= 1) &&
         (u4TestValIssAclL3FilterFlowId <= QOS_FLOW_ID_MAX)))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssAclL3FilterStatus
 Input       :  The Indices
                IssAclL3FilterNo

                The Object 
                testValIssAclL3FilterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssAclL3FilterStatus (UINT4 *pu4ErrorCode, INT4 i4IssAclL3FilterNo,
                               INT4 i4TestValIssAclL3FilterStatus)
{
    if (ISS_IS_L3FILTER_ID_VALID (i4IssAclL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssAclL3FilterStatus < ISS_ACTIVE)
        || (i4TestValIssAclL3FilterStatus > ISS_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Check if the filter is mapped to CLass Map entry. If its mapped 
     * filter should not be deleted. */
#if defined (QOSX_WANTED)
    if (i4TestValIssAclL3FilterStatus == ISS_DESTROY)
    {
        if (QoSUtlIsFilterMapToClsMapEntry (QOS_L3FILTER, i4IssAclL3FilterNo)
            == TRUE)
        {
            CLI_SET_ERR (CLI_ACL_FILTER_MAPPED_TO_CLSMAP);
            return SNMP_FAILURE;
        }
    }
#endif

#if defined (DIFFSRV_WANTED)
    if ((i4TestValIssAclL3FilterStatus == ISS_DESTROY)
        || (i4TestValIssAclL3FilterStatus == ISS_NOT_IN_SERVICE))
    {
        ISS_UNLOCK ();

        if (DsCheckMFClfrTable (i4IssAclL3FilterNo, DS_IP_FILTER) == DS_FAILURE)
        {
            ISS_LOCK ();
            CLI_SET_ERR (CLI_ACL_FILTER_NO_CHANGE);
            return SNMP_FAILURE;
        }

        ISS_LOCK ();
    }
#endif

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssAclL3FilterTable
 Input       :  The Indices
                IssAclL3FilterNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssAclL3FilterTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
