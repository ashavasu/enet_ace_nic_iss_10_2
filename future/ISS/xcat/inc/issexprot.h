
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: issexprot.h,v 1.12 2015/05/26 13:14:03 siva Exp $
*
*********************************************************************/
#ifndef _ISSEXPROT_H
#define _ISSEXPROT_H
#include "aclcli.h"
INT4 IssExInit (VOID);
VOID IssSetDefaultRateCtrlValues (UINT2 u2PortIndex,
                                  tIssRateCtrlEntry * pIssRateCtrlEntry);
INT4 IssExCreatePortRateCtrl (UINT2 u2PortIndex, tIssTableName IssTableFlag);
INT4 IssExDeletePortRateCtrl (UINT2 u2PortIndex);

tIssL2FilterEntry *IssExtGetL2FilterEntry (INT4 i4IssL2FilterNo);
tIssL3FilterEntry *IssExtGetL3FilterEntry (INT4 i4IssL3FilterNo);
tIssUserDefinedFilterTable * IssExtGetUdbFilterTableEntry (UINT4 u4UdbFilter);

INT4 IssExtQualifyL2FilterData (tIssL2FilterEntry ** ppIssL2FilterEntry);

INT4 IssExtQualifyL3FilterData (tIssL3FilterEntry ** ppIssL3FilterEntry);

INT4 IssExtSnmpLowGetFirstValidUDBFilterTableIndex (INT4 *pu4FirstUdbFilterIndex);
INT4 IssSnmpLowGetNextValidUdbFilterTableIndex (UINT4 u4IssUdbFilterNo,
                            UINT4 *pu4NextUdbFilterNo);

INT4 IssProcessAndOrNotFilterParamForL2 (tIssL2FilterEntry   *pIssAclL2FilterEntryOne,
                                         tIssL2FilterEntry   *pIssAclL2FilterEntryTwo,
                                         UINT1 u1Action,
                                         tIssL2FilterEntry *pIssAclL2FilterEntry,
                                         UINT1 *pu1Result);

INT4 IssProcessAndOrNotFilterParamForL3 (tIssL3FilterEntry   *pIssAclL3FilterEntryOne,
                                         tIssL3FilterEntry   *pIssAclL3FilterEntryTwo,
                                         UINT1 u1Action,
                                         tIssL3FilterEntry *pIssAclL3FilterEntry,
                                         UINT1 *pu1Result);

VOID AclProcessQMsgEvent(VOID);
VOID RegisterFSISSM (VOID);

INT4 IssIsMemberPort (UINT1 *pu1PortArray, UINT4 u4PortArrayLen, UINT4 u4Port); 
extern UINT1 * Ip6PrintNtop (tIp6Addr * pAddr);
extern VOID Ip6AddrCopy  PROTO ((tIp6Addr * pDst, tIp6Addr * pSrc));
UINT1 IssCalculateTrafficLogicForPort PROTO ((UINT4 u4Port,tIssRedirectPortMaskAndIndex
                                       *pIssRedirectPortMaskAndIndex,
                                       UINT1 *pu1mask, UINT4 *pu4NoOfRules,UINT1 u1ActivePortExist,
                                       tIssRedirectIfPortGroup *pIssRedirectIfPortGroup));

VOID IssGetNextActiveOutIndex PROTO ((tIssRedirectIfPortGroup *pIssRedirectIfPortGroup,UINT4 u4NumPorts,UINT4 *pu4CurrIndex));

VOID IssInterfaceGrpRedirectIdNextFree PROTO ((UINT4 *pu4RetValIssRedirectInterfaceGrpIdNextFree));

VOID IssExAddAclToPriorityTable PROTO ((INT4 i4Priority,tTMO_SLL_NODE *pFilterNode,UINT1 u1FilterType));
VOID IssExGetPriorityFilterTableCount PROTO ((UINT4 *u4PriFilterCount));
VOID IssExDeleteAclPriorityFilterTable PROTO ((INT4 i4Priority,tTMO_SLL_NODE *pFilterNode,UINT1 u1FilterType));
INT4 IssExGetIssCommitSupportImmediate PROTO ((VOID));
INT4 IssGetTriggerCommit PROTO ((VOID));
INT4 IssExPrgAclsToNpWithPriority PROTO ((VOID));

VOID
IssConvertRedirectPortListToArray PROTO ((tPortList LocalPortList,tIssRedirectIfPortGroup *pIssRedirectIfPortGroup,
                                   UINT4 *pu4IfPortArray, UINT4 *pu4NumPorts,UINT1 *pu1ActivePortExist));
INT4 AclCreateUserDefFilter PROTO ((tCliHandle CliHandle, INT4));
INT4 AclDestroyUserDefFilter PROTO ((tCliHandle CliHandle, INT4 i4FilterNo));
INT4 AclSetEgressFilterMode PROTO ((tCliHandle CliHandle, INT4 i4EgressAclMode));

INT4 AclUserDefinedOperations PROTO ((tCliHandle CliHandle, INT4,UINT4,UINT4,UINT4,UINT4));

INT4 AclShowUDBFilter PROTO ((tCliHandle CliHandle, INT4 ));

INT4 AclUserDefinedAccessGroup PROTO ((tCliHandle CliHandle, INT4 , INT4 ));

INT4 AclNoUserDefinedAccessGroup PROTO ((tCliHandle CliHandle, INT4, INT4 ));
INT4 AclUserDefinedFilterConfig  PROTO (
                            (tCliHandle CliHandle, 
                            INT4 i4Action,UINT4 u4PktType, UINT2 u2PktStart,
                            UINT2 u2Offset1Pos, UINT2 u2Offset1Val,UINT2 u2Offset2Pos,
                            UINT2 u2Offset2Val,UINT2 u2Offset3Pos, UINT2 u2Offset3Val,
                            UINT2 u2Offset4Pos,UINT2 u2Offset4Val,UINT2 u2Offset5Pos,
                            UINT2 u2Offset5Val,UINT2 u2Offset6Pos,UINT2 u2Offset6Val,
                            UINT4 u4SubAction, UINT2 u2SubActionId,UINT4));
INT4 AclRedirectFilterConfig PROTO ((tCliHandle CliHandle, INT4 i4Action,
                          INT4 i4IsPortList, INT4 i4TrafficDist,
                          INT1 *pi1IfName0, INT1 *pi1IfListStr0,
                          INT1 *pi1IfName1, INT1 *pi1IfListStr1,
                          INT1 *pi1IfName2, INT1 *pi1IfListStr2,
                          UINT1));
INT1
nmhSetIssAclL2FilterStatus ARG_LIST((INT4  ,INT4 ));
INT1
nmhSetIssAclL3FilterStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssAclUserDefinedFilterStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhGetIssAclL2FilterRedirectId ARG_LIST((INT4 ,INT4 *));
INT1
nmhGetIssAclL3FilterRedirectId ARG_LIST((INT4 ,INT4 *));
INT1
nmhGetIssAclUserDefinedFilterRedirectId ARG_LIST((UINT4 ,INT4 *));
INT1
nmhSetIssRedirectInterfaceGrpStatus ARG_LIST((UINT4  ,INT4 ));

INT4  AclCreateIPFilter PROTO ((tCliHandle, UINT4, INT4 ));

INT4  AclDestroyIPFilter PROTO ((tCliHandle,  INT4 ));

INT4 AclCreateMacFilter PROTO ((tCliHandle , INT4 ));

INT4 AclDestroyMacFilter PROTO ((tCliHandle , INT4 ));

INT4 AclMacAccessGroup PROTO ((tCliHandle CliHandle, INT4 i4FilterNo ));

INT4 AclNoMacAccessGroup PROTO ((tCliHandle CliHandle, INT4 i4FilterNo));
    
 INT4 AclMacExtAccessGroup PROTO ((tCliHandle, INT4, INT4));

 INT4 AclNoMacExtAccessGroup PROTO ((tCliHandle, INT4, INT4));

INT4 AclShowAccessLists PROTO ((tCliHandle CliHandle,INT4 i4FilterType,INT4 i4FilterNo));
INT4 AclShowL2Filter PROTO ((tCliHandle CliHandle,INT4 i4NextFilter));
INT4 AclShowL3Filter PROTO ((tCliHandle CliHandle,INT4 i4NextFilter));
INT4 AclShowEgressFilterMode (tCliHandle CliHandle);

INT4 AclShowRunningConfig(tCliHandle,UINT4);
VOID AclShowRunningConfigTables(tCliHandle);
VOID AclShowRunningConfigInterfaceDetails(tCliHandle,INT4);

INT4 AclTestIpParams PROTO ((UINT1 ,INT4 i4FilterNo,UINT4 u4SrcType,UINT4 u4SrcIpAddr, UINT4 u4SrcMask ));
                                           
INT4 AclSetIpParams PROTO ((UINT1 ,INT4 i4FilterNo,UINT4 u4SrcType,UINT4 u4SrcIpAddr, UINT4 u4SrcMask ));

VOID AclCliPrintPortList(tCliHandle CliHandle,
                         INT4 i4CommaCount, UINT1 * piIfName);
INT4 AclExtPbL3FilterConfig PROTO ((tCliHandle, INT4, INT4, INT4, INT4, INT4));
INT4 AclExtPbL2FilterConfig PROTO ((tCliHandle, INT4, INT4, INT4, INT4, INT4));

INT4 
AclTrafficSeprtnCliSetControl (tCliHandle CliHandle, INT4 i4TrafficControl);
INT4 AclTrafficSeprtnShowControl (tCliHandle CliHandle);
INT4 AclStdIpFilterConfig PROTO 
((tCliHandle, INT4,UINT4, UINT4 ,UINT4 ,UINT4, UINT4, UINT4, UINT4, UINT2,UINT4));

INT4 AclExtIpFilterConfig PROTO 
((tCliHandle, INT4, INT4, UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, INT4, INT4,
INT4, UINT4, UINT2));

INT4
AclExtIp6FilterConfig PROTO ((tCliHandle CliHandle, UINT4 u4SrcIpAddr,
                       tIp6Addr SrcIp6Addr, UINT4 u4SrcIpMask,
                       UINT4 u4DestIpAddr, tIp6Addr DstIp6Addr,
                 UINT4 u4DestIpMask, UINT4 u4FlowId, INT4 i4Action,
                       UINT4 u4SubAction, UINT2 u2SubActionId,UINT4));

INT4 AclExtIpFilterTcpUdpConfig PROTO ((tCliHandle, INT4 i4Action, INT4 i4Protocol,
  UINT4 u4SrcType , UINT4 u4SrcIpAddr,  UINT4 u4SrcIpMask, 
  UINT4 u4SrcFlag, UINT4 u4SrcMinPort,  UINT4 u4SrcMaxRangePort,
  UINT4 u4DestType, UINT4  u4DestIpAddr,UINT4 u4DestIpMask,
  UINT4 u4DestFlag, UINT4 u4DestMinPort ,UINT4  u4DestMaxRangePort, 
  UINT4 u4BitType , INT4 i4Tos, INT4 i4Dscp, INT4 i4Priority , UINT4 u4SubAction, 
  UINT2 u2SubActionId));

 INT4 AclExtIpFilterIcmpConfig PROTO
 ((tCliHandle, INT4 i4Action, UINT4,UINT4 , UINT4, UINT4, UINT4, UINT4 , INT4, INT4, INT4, UINT4, UINT2));

 INT4  AclIpAccessGroup PROTO ((tCliHandle, INT4 , INT4));
  
 INT4 AclNoIpAccessGroup PROTO ((tCliHandle, INT4 i4FilterNo, INT4 ));
 
 INT4 AclExtMacFilterConfig PROTO ((tCliHandle CliHandle, INT4 i4Action,
                       UINT4 u4SrcType, tMacAddr SrcMacAddr,
                       UINT4 u4DestType ,tMacAddr DestMacAddr,
                       INT4 i4Protocol, INT4 i4Encap,
                       UINT4 u4VlanId, INT4 u4Priority,
                       UINT4 u4SubAction, UINT2 u2SubActionId, INT4, INT4));
 INT4 AclCfiDeiFilterConfig PROTO((tCliHandle, INT4, INT4, INT4, INT4));
 INT4 AclDpFilterConfig PROTO((tCliHandle, INT4, INT4, INT4));
 INT4 AclShowCfiDeiDpL2Filter PROTO((tCliHandle , INT4 , INT4 ));

INT4 AclUpdateOverPortChannel PROTO ((UINT4 u4IfIndex));
INT4 AclEnabledForPort (UINT4 u4IfIndex);
INT4 AclIpFilterEnabledForPort (UINT4 u4IfIndex);
INT4 AclMacFilterEnabledForPort (UINT4 u4IfIndex);
INT4 AclUserDefinedFilterEnabledForPort  (UINT4 u4IfIndex);
#endif /* _ISSEXPROT_H */

