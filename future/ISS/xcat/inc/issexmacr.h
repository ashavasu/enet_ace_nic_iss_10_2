
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: issexmacr.h,v 1.10 2014/10/28 11:19:09 siva Exp $
*
*********************************************************************/
#ifndef _ISSEXMACR_H
#define _ISSEXMACR_H

#define ISS_MAX_RATE_VALUE              65535
#define ISS_DEF_RATE_VALUE              500
#define ISS_MIN_RATE_VALUE              1

/* Macros for validattion */
#define ISS_MIN_PROTOCOL_ID             1536
#define ISS_MAX_PROTOCOL_ID             65535
#define ISS_MIN_FILTER_ID               0
#define ISS_MAX_FILTER_ID               65535
#define ISS_MAX_FILTER_PRIORITY         255
#define ISS_DEFAULT_FILTER_PRIORITY     1
#define ISS_DEFAULT_PROTOCOL_TYPE       0
#define ISS_DEF_FILTER_MASK             0
#define ISS_MIN_DSCP_VALUE              0
#define ISS_MAX_DSCP_VALUE              63

#define ISS_MIN_REDIRECT_GRP_ID   0
#define ISS_MAX_REDIRECT_GRP_ID   50
#define ISS_MAX_PRIORITY_GRP_ID   50
#define ISS_UDB_FILTER_ENTRY_POOL_ID 50

/* ISS l2  ACL */
#define ISS_DEFAULT_VLAN_PRIORITY       -1

#define ISS_SERVICE_VLAN_ETHER_TYPE     0x88a8
#define ISS_CUSTOMER_VLAN_ETHER_TYPE    0x8100

/* Command identifier to distinguish default values*/
#define ISS_ACL_DEF_SVLAN            0
#define ISS_ACL_DEF_SVLAN_PRIO       -1
#define ISS_ACL_DEF_CVLAN            0
#define ISS_ACL_DEF_CVLAN_PRIO       -1
#define ISS_ACL_DEF_ETYPE            0
#define ISS_ACL_DEF_DP               1
#define ISS_ACL_DEF_CFI_DEI          1

/* MEM Pool Id Definitions */
#define ISS_RATEENTRY_POOL_ID            gIssExGlobalInfo.IssRateCtrlPoolId
#define ISS_L2FILTERENTRY_POOL_ID        gIssExGlobalInfo.IssL2FilterPoolId
#define ISS_L3FILTERENTRY_POOL_ID        gIssExGlobalInfo.IssL3FilterPoolId

#define ISS_UDB_FILTER_TABLE_POOL_ID     gIssExGlobalInfo.IssUdbTablePoolId
#define ISS_UDB_FILTERENTRY_POOL_ID      gIssExGlobalInfo.IssUdbFilterPoolId



/* Filter List */
#define ISS_L2FILTER_LIST               gIssExGlobalInfo.IssL2FilterListHead
#define ISS_L2FILTER_LIST_HEAD          (&ISS_L2FILTER_LIST)->Tail
#define ISS_IS_L2FILTER_ID_VALID(ISSId) \
        (((ISSId <= ISS_MIN_FILTER_ID) || \
        (ISSId > ISS_MAX_FILTER_ID)) ? ISS_FALSE : ISS_TRUE)

#define ISS_L3FILTER_LIST               gIssExGlobalInfo.IssL3FilterListHead
#define ISS_L3FILTER_LIST_HEAD          (&ISS_L3FILTER_LIST)->Tail
#define ISS_IS_L3FILTER_ID_VALID(ISSId) \
        (((ISSId <= ISS_MIN_FILTER_ID) || \
        (ISSId > ISS_MAX_FILTER_ID)) ? ISS_FALSE : ISS_TRUE)

/* Redirect Group */
#define ISS_UDB_FILTER_TABLE_LIST        gIssExGlobalInfo.IssUdbFilterTableHead

#define ISS_IS_REDIRECT_GRP_ID_VALID(ISSId) \
        (((ISSId <= ISS_MIN_REDIRECT_GRP_ID) || \
        (ISSId > ISS_MAX_REDIRECT_GRP_ID)) ? ISS_FALSE : ISS_TRUE)

#define ISS_IS_UDB_FILTER_ID_VALID(ISSId) \
        (((ISSId == ISS_MIN_REDIRECT_GRP_ID) || \
        (ISSId > ISS_MAX_REDIRECT_GRP_ID)) ? ISS_FALSE : ISS_TRUE)


#define ISS_L2FILTERENTRY_MEMBLK_COUNT  ISS_MAX_L2_FILTERS
#define ISS_L3FILTERENTRY_MEMBLK_COUNT  ISS_MAX_L3_FILTERS
#define ISS_L3FILTERENTRY_MEMBLK_COUNT  ISS_MAX_L3_FILTERS

#define ISS_UDB_FILTER_TABLE_MEMBLK_COUNT ISS_MAX_UDB_FILTERS
#define ISS_UDB_FILTERENTRY_MEMBLK_COUNT  ISS_MAX_UDB_FILTERS


#define ISS_RATEENTRY_MEMBLK_COUNT      (BRG_MAX_PHY_PLUS_LOG_PORTS + 1)

#define ISS_RATEENTRY_MEMBLK_SIZE       sizeof(tIssRateCtrlEntry)

#define ISS_L2FILTERENTRY_MEMBLK_SIZE   sizeof(tIssL2FilterEntry)

#define ISS_UDB_FILTER_TABLE_MEMBLK_SIZE sizeof (tIssUserDefinedFilterTable)

#define ISS_UDB_FILTER_ENTRY_MEMBLK_SIZE   sizeof (tIssUDBFilterEntry)

#define  ISS_CREATE_RATEENTRY_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_CREATE_PRIORITYFILTERENTRY_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)


#define  ISS_CREATE_L2FILTERENTRY_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_CREATE_L3FILTERENTRY_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_CREATE_UDB_FILTER_TABLE_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_CREATE_UDB_FILTER_ENTRY_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_DELETE_RATEENTRY_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_DELETE_L2FILTERENTRY_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_DELETE_L3FILTERENTRY_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_DELETE_UDB_FILTER_TABLE_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_RATEENTRY_ALLOC_MEM_BLOCK(pu1Block) \
         (pu1Block = \
          (tIssRateCtrlEntry *)(MemAllocMemBlk (ISS_RATEENTRY_POOL_ID)))

#define  ISS_L2FILTERENTRY_ALLOC_MEM_BLOCK(pu1Block) \
         (pu1Block = \
          (tIssL2FilterEntry *)(MemAllocMemBlk (ISS_L2FILTERENTRY_POOL_ID)))

#define  ISS_L3FILTERENTRY_ALLOC_MEM_BLOCK(pu1Block) \
         (pu1Block = \
          (tIssL3FilterEntry *)(MemAllocMemBlk (ISS_L3FILTERENTRY_POOL_ID)))

#define  ISS_UDB_FILTER_TABLE_ALLOC_MEM_BLOCK(pu1Block) \
         (pu1Block = \
          (tIssUserDefinedFilterTable *)(MemAllocMemBlk (ISS_UDB_FILTER_TABLE_POOL_ID)))

#define  ISS_UDB_FILTER_ENTRY_ALLOC_MEM_BLOCK(pu1Block) \
         (pu1Block = \
          (tIssUDBFilterEntry *)(MemAllocMemBlk (ISS_UDB_FILTERENTRY_POOL_ID)))


#define  ISS_RATEENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_RATEENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define  ISS_L2FILTERENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_L2FILTERENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define  ISS_L3FILTERENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_L3FILTERENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define  ISS_UDB_FILTERENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_UDB_FILTERENTRY_POOL_ID, (UINT1 *)pu1Msg)


#define  ISS_UDB_FILTERTABLE_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_UDB_FILTER_TABLE_POOL_ID, (UINT1 *)pu1Msg)

 
#define ISS_DP_STRING(u1Dp) \
        (u1Dp==1?"green":(u1Dp==2)?"yellow":"red") 
        
/* Default Value for Egress Filter Mode */
#define DEF_EG_ACL EG_IP_ACL
#endif  /* _ISSEXMACR_H */
