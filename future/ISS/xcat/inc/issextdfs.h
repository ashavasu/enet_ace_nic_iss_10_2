/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: issextdfs.h,v 1.5 2013/07/10 13:23:50 siva Exp $
 *
 * Description: This file include all the structures definition required
 *              for the operation of the ACL module
 ****************************************************************************/


#ifndef _ISSEXTDFS_H
#define _ISSEXTDFS_H

typedef tTMO_SLL            tIssExSll;

typedef struct IssRateCtrlEntry {

   UINT4                u4IssRateCtrlDLFLimitValue;
   UINT4                u4IssRateCtrlBCASTLimitValue;
   UINT4                u4IssRateCtrlMCASTLimitValue;
   INT4                 i4IssRateCtrlPortLimitRate;
   INT4                 i4IssRateCtrlPortBurstRate;

}tIssRateCtrlEntry;

/* Iss Extension Global Info */
typedef struct { 

   tMemPoolId              IssRateCtrlPoolId;
   tMemPoolId              IssL2FilterPoolId;
   tMemPoolId              IssL3FilterPoolId;
   tMemPoolId              IssUdbTablePoolId;
   tMemPoolId              IssUdbFilterPoolId;
   tIssRateCtrlEntry      *apIssRateCtrlEntry[ISS_RATEENTRY_MEMBLK_COUNT];
   tIssExSll               IssL2FilterListHead;
   tIssExSll               IssL3FilterListHead;
   tIssExSll               IssUdbFilterTableHead;
   UINT4                    u4PriorityTableSet;
   INT4                   i4IssEgressFilterMode;
}tIssExtGlobalInfo;

typedef struct _IssRedirectIntfGrpTable {
   tPortList      PortList;             /* List of ports belonging to this group */
   UINT4          u4RedirectIntfGrpId;  /*  Redirect Interface Group Id; */
   UINT4          u4AclId;              /* Acl Id mapped to this Group */
   UINT2          u2EgressVlanId;       /* Egress VLAN Id  */
   UINT2          u2TrafficDistByte;    /*Virtual trunk traffic distribution byte . */
   UINT1          u1AclIdType;          /* L2_FILTER or L3_FILTER or USERDEFINED_FILTER */
   UINT1          u1PortListType;       /*ISS_REDIRECT_TO_PORT if redirect interface is single port
                                          ISS_REDIRECT_TO_PORTLIST if redirect interface is port list*/ 
   UINT1          u1RowStatus;
   UINT1          u1UdbPosition;    /*Position of UserDefined Byte*/ 
   UINT1          u1PriorityFlag;
   UINT1          u1Pad[3];
}tIssRedirectIntfGrpTable;

typedef struct _IssRedirectPortMaskAndIndex
{
   UINT4 u4PortIndex;
   UINT1 u1PortMaskMsb;
   UINT1 u1PortMaskLsb;
   UINT1 u1PortValueMsb;
   UINT1 u1PortValueLsb;

}tIssRedirectPortMaskAndIndex;


typedef struct _IssRedirectIfPortGrp
{
    UINT4 u4IfIndex;              /* CFA If Index */
    UINT1 u1OperStatus;           /* UP/DOWN */
    UINT1 u1Pad[3];

}tIssRedirectIfPortGroup;

/* Validity Value */
typedef enum {

   ISS_OK = 1,
   ISS_NOTOK

}tValid;
#endif
