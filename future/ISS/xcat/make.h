#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 10/05/2002                                    |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            including for building the FutureISS          |
# |                            product.                                      |
# |                                                                          |
# +--------------------------------------------------------------------------+
#
#     CHANGE RECORD :
# +--------------------------------------------------------------------------+
# | VERSION | AUTHOR/    | DESCRIPTION OF CHANGE                             |
# |         | DATE       |                                                   |
# +---------|------------|---------------------------------------------------+
# |   1     | ISS        | Creation of makefile                              |
# |         | 10/05/2002 |                                                   |
# +--------------------------------------------------------------------------+


# Set the PROJ_BASE_DIR as the directory where you untar the project files
PROJECT_NAME		   = FutureISSExt
PROJECT_BASE_DIR	   = ${BASE_DIR}/ISS/xcat
PROJECT_SOURCE_DIR	   = ${PROJECT_BASE_DIR}/src
PROJECT_INCLUDE_DIR	   = ${PROJECT_BASE_DIR}/inc
PROJECT_OBJECT_DIR	   = ${PROJECT_BASE_DIR}/obj

PROJECT_CMN_BASE_DIR	   = ${ISS_COMMON_DIR}/system
PROJECT_INCLUDE_WEBINC_DIR	= ${BASE_DIR}/ISS/common/web/inc
PROJECT_CMN_INCLUDE_DIR	= ${PROJECT_CMN_BASE_DIR}/inc
FUTURE_INC_XCATCLI_DIR  = $(BASE_DIR)/inc/cli/xcat

# Specify the project include directories and dependencies
PROJECT_INCLUDE_FILES  = $(PROJECT_CMN_INCLUDE_DIR)/issmacro.h \
         $(PROJECT_INCLUDE_DIR)/issextdfs.h \
			$(PROJECT_INCLUDE_DIR)/issexmacr.h \
			$(PROJECT_INCLUDE_DIR)/issexprot.h \
			$(PROJECT_INCLUDE_DIR)/issexglob.h \
			$(PROJECT_INCLUDE_DIR)/issexweb.h \
			$(PROJECT_INCLUDE_DIR)/trgt_htmldata.h \
			$(PROJECT_INCLUDE_DIR)/issexinc.h \
			$(PROJECT_INCLUDE_DIR)/fsisselw.h

PROJECT_FINAL_INCLUDES_DIRS	=       -I$(PROJECT_INCLUDE_DIR) \
					-I$(PROJECT_CMN_INCLUDE_DIR) \
					-I$(PROJECT_INCLUDE_WEBINC_DIR) \
					  $(COMMON_INCLUDE_DIRS) \
                                 -I$(FUTURE_INC_XCATCLI_DIR)

PROJECT_FINAL_INCLUDE_FILES	= $(PROJECT_INCLUDE_FILES)

PROJECT_DEPENDENCIES	= $(COMMON_DEPENDENCIES) \
				$(PROJECT_FINAL_INCLUDE_FILES) \
				$(PROJECT_BASE_DIR)/Makefile \
				$(PROJECT_BASE_DIR)/make.h


