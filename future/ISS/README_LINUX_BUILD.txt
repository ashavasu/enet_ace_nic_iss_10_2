README

How to compile ISS in Linux


1) set the  
    TARGET_OS      = OS_TMO
   in the code/future/LR/make.h.

2) set the 
    BASEDIR  to the location where the code is present
    in the code/future/LR/make.h 

e.g.
ifeq (${TARGET_OS}, OS_TMO)
BASE_DIR                     = /home1/iss/code/future
endif


3) give "make ISS" in code/future/LR to build the executable "ISS.exe"
