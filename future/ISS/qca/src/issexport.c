/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: issexport.c,v 1.4 2015/05/27 12:32:00 siva Exp $
 *
 * Description: This file contains the the function ported from other Modules
 * (like RM Module).
 *****************************************************************************/

#include "issexinc.h"

/*****************************************************************************/
/* Function Name      : AclRmRegisterProtocols                                 */
/*                                                                           */
/* Description        : This function Register ACL module with RM Module     */
/*                                                                           */
/* Input(s)           : pRmRegParams - Pointer to the structure of RM          */
/*                       register parameters              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                     */
/*****************************************************************************/
PUBLIC INT4
AclRmRegisterProtocols (tRmRegParams * pRmRegParams)
{
    INT4                i4RetVal = RM_SUCCESS;
#ifdef L2RED_WANTED
    i4RetVal = RmRegisterProtocols (pRmRegParams);
#else
    UNUSED_PARAM (pRmRegParams);
#endif
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : AclRmDeRegisterProtocols                         */
/*                                                                           */
/* Description        : This function De-Register ACL module with RM Module  */
/*                                                                           */
/* Input(s)           : None                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                     */
/*****************************************************************************/
PUBLIC INT4
AclRmDeRegisterProtocols ()
{
    INT4                i4RetVal = RM_SUCCESS;
#ifdef L2RED_WANTED
    i4RetVal = RmDeRegisterProtocols (RM_ACL_APP_ID);
#endif
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : AclPortRmReleaseMemoryForMsg                         */
/*                                                                           */
/* Description        : This function Release RM Message memory postedf to   */
/*            ACL module                         */
/*                                                                           */
/* Input(s)           : pData - Pointer to the RM Message               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                     */
/*****************************************************************************/
PUBLIC INT4
AclPortRmReleaseMemoryForMsg (UINT1 *pData)
{
    INT4                i4RetVal = RM_SUCCESS;
#ifdef L2RED_WANTED
    i4RetVal = RmReleaseMemoryForMsg (pData);
#else
    UNUSED_PARAM (pData);
#endif
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : AclPortRmApiHandleProtocolEvent                         */
/*                                                                           */
/* Description        : This function posts the event to the RM module         */
/*            form ACL module                         */
/*                                                                           */
/* Input(s)           : pEvt - Pointer to the strucutre for RM protocol evt  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                     */
/*****************************************************************************/
PUBLIC INT4
AclPortRmApiHandleProtocolEvent (tRmProtoEvt * pEvt)
{
    INT4                i4RetVal = RM_SUCCESS;
#ifdef L2RED_WANTED
    i4RetVal = RmApiHandleProtocolEvent (pEvt);
#else
    UNUSED_PARAM (pEvt);
#endif
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : AclPortRmApiSendProtoAckToRM                         */
/*                                                                           */
/* Description        : This function posts the acknowledgement to the         */
/*            RM module                         */
/*                                                                           */
/* Input(s)           : pProtoAck-Pointer to the struct for RM protocol Ack  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                     */
/*****************************************************************************/
PUBLIC INT4
AclPortRmApiSendProtoAckToRM (tRmProtoAck * pProtoAck)
{
    INT4                i4RetVal = RM_SUCCESS;
#ifdef L2RED_WANTED
    i4RetVal = RmApiSendProtoAckToRM (pProtoAck);
#else
    UNUSED_PARAM (pProtoAck);
#endif
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : AclPortRmEnqMsgToRm                               */
/*                                                                           */
/* Description        : This function posts the RM message to the RM Module  */
/*                                                                           */
/* Input(s)           : pRmMsg - Pointer to the RM Message             */
/*            u2DataLen - Length of the data                  */
/*            u4SrcEntId - Source Application ID.             */
/*            u4DestEntId - Destination Application ID.          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                     */
/*****************************************************************************/
UINT4
AclPortRmEnqMsgToRm (tRmMsg * pRmMsg, UINT2 u2DataLen, UINT4 u4SrcEntId,
                     UINT4 u4DestEntId)
{
    UINT4               u4RetVal = RM_SUCCESS;
#ifdef L2RED_WANTED
    u4RetVal =
        RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen, u4SrcEntId, u4DestEntId);
#else
    UNUSED_PARAM (pRmMsg);
    UNUSED_PARAM (u2DataLen);
    UNUSED_PARAM (u4SrcEntId);
    UNUSED_PARAM (u4DestEntId);
#endif
    return u4RetVal;
}

/*****************************************************************************/
/* Function Name      : AclPortRmGetNodeState                     */
/*                                                                           */
/* Description        : This function gets the RM node state.                 */
/*                                                                           */
/* Input(s)           : None                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                     */
/*****************************************************************************/
PUBLIC INT4
AclPortRmGetNodeState (VOID)
{
#ifdef L2RED_WANTED
    return (RmGetNodeState ());
#else
    return RM_ACTIVE;
#endif
}

/*****************************************************************************/
/* Function Name      : AclPortRmGetStandbyNodeCount                  */
/*                                                                           */
/* Description        : This function gets the RM Standby Count.             */
/*                                                                           */
/* Input(s)           : None                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                     */
/*****************************************************************************/
PUBLIC UINT1
AclPortRmGetStandbyNodeCount (VOID)
{
#ifdef L2RED_WANTED
    return (RmGetStandbyNodeCount ());
#else
    return ISS_ZERO_ENTRY;
#endif
}

/*****************************************************************************/
/* Function Name      : AclPortRmSetBulkUpdatesStatus                 */
/*                                                                           */
/* Description        : This function sets the RM Bul Update Status in RM    */
/*                                                                           */
/* Input(s)           : None                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                             */
/*                                                                           */
/* Return Value(s)    : None                             */
/*****************************************************************************/
PUBLIC VOID
AclPortRmSetBulkUpdatesStatus (VOID)
{
#ifdef L2RED_WANTED
    RmSetBulkUpdatesStatus (RM_ACL_APP_ID);
    return;
#else
    return;
#endif
}

/*****************************************************************************
**
**     FUNCTION NAME    : AclUpdateOverPortChannel
**
**     DESCRIPTION      : This function removes any L2/L3 ACL that is
**                        applied on a given port-channel interface.
**
**     INPUT            : u4IfIndex - Interface Index
**
**     OUTPUT           : NONE
**
**     RETURNS          : CFA_SUCCESS or CFA_FAILURE
**
*****************************************************************************/
INT4
AclUpdateOverPortChannel (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CFA_SUCCESS;
}
/*****************************************************************************
 **
 **     FUNCTION NAME    : AclIpFilterEnabledForPort
 **
 **     DESCRIPTION      : This function checks whether
 **                       IP any ACL is associated with this port
 **
 **
 **     INPUT            : u4IfIndex - Interface Index
 **
 **     OUTPUT           : NONE
 **
 **     RETURNS          :  CLI_SUCCESS / CLI_FAILURE
 **
 *****************************************************************************/
INT4
AclIpFilterEnabledForPort (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CLI_FAILURE;
}
/*****************************************************************************
 **
 **     FUNCTION NAME    : AclMacFilterEnabledForPort
 **
 **     DESCRIPTION      : This function checks whether
 **                       any MAC ACL is associated with this port
 **
 **
 **     INPUT            : u4IfIndex - Interface Index
 **
 **     OUTPUT           : NONE
 **
 **     RETURNS          :  CLI_SUCCESS / CLI_FAILURE
 **
 *****************************************************************************/
INT4
AclMacFilterEnabledForPort (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CLI_FAILURE;
}
/*****************************************************************************
 **
 **     FUNCTION NAME    : AclUserDefinedFilterEnabledForPort
 **
 **     DESCRIPTION      : This function checks whether
 **                       any UserDefined ACL is associated with this port
 **
 **
 **     INPUT            : u4IfIndex - Interface Index
 **
 **     OUTPUT           : NONE
 **
 **     RETURNS          :  CLI_SUCCESS / CLI_FAILURE
 **
 *****************************************************************************/
INT4
AclUserDefinedFilterEnabledForPort (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CLI_FAILURE;
}
/*****************************************************************************
 **
 **     FUNCTION NAME    : AclEnabledForPort
 **
 **     DESCRIPTION      : This function checks whether
 **                       any ACL is associated with this port
 **
 **
 **     INPUT            : u4IfIndex - Interface Index
 **
 **     OUTPUT           : NONE
 **
 **     RETURNS          : CLI_SUCCESS / CLI_FAILURE
 **
 *****************************************************************************/

INT4
AclEnabledForPort (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CLI_FAILURE;
}
