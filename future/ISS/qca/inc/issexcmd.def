/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved               
*                                                                    
* $Id: issexcmd.def,v 1.3 2014/11/04 13:16:41 siva Exp $                                                         
*                                                                    
*********************************************************************/

DEFINE GROUP: ISS_EXT_INT_CMDS 

#if defined (NPAPI_WANTED)

COMMAND : storm-control { broadcast |multicast | dlf } level <integer(1-1024000)>
ACTION  : {
	    UINT4 u4Type =0;
	    
	    if ($1 != NULL)
	    {
	      u4Type = ISS_RATE_BCAST_LIMIT;
	    }
	    else if ($2 != NULL)
	    {
	      u4Type = ISS_RATE_MCAST_LIMIT;
	    }
            else if ($3 != NULL) 
	    {
	      u4Type = ISS_RATE_DLF_LIMIT ;
	    }

	    cli_process_iss_ext_cmd ( CliHandle,  CLI_ISS_STORM_CONTROL, NULL, 
                                                    u4Type, $5);
	 }

SYNTAX  : storm-control { broadcast |multicast | dlf } level <rate-value>
PRVID   : 15
HELP    : Sets storm control rate for broadcast , multicast and DLF packets.
CXT_HELP: storm-control Configures storm control related configuration|
         broadcast Broadcast packet storm control related configuration|
         multicast Multicast packet storm control related configuration|
         dlf Unicast packet storm control related configuration|
         level Storm-control suppression level|
         (1-1024000) Storm control rate value|
         <CR> Sets storm control rate for broadcast , multicast and DLF packets.

COMMAND : no storm-control { broadcast |multicast | dlf } level 
ACTION  : {	   
             UINT4 u4Type =0;
	    
      	    if ($2 != NULL)
             {
                 u4Type = ISS_RATE_BCAST_LIMIT;
             }
             else if ($3 != NULL)
             {
                 u4Type = ISS_RATE_MCAST_LIMIT;
             }
             else if ($4 != NULL) 
             {
                 u4Type = ISS_RATE_DLF_LIMIT;
             }
             cli_process_iss_ext_cmd ( CliHandle,  CLI_ISS_NO_STORM_CONTROL,                                             NULL, u4Type);
         }
 
SYNTAX  : no storm-control { broadcast |multicast | dlf } level 
PRVID   : 15
HELP    : Sets storm control rate for broadcast , multicast and DLF packets to the default value
CXT_HELP: no Disables the configuration / deletes the entry / resets to default value|
          storm-control Configures strom control related configuration|
          broadcast Broadcast packet storm control related configuration|
          multicast Multicast packet storm control related configuration|
          dlf Unicast packet storm control related configuration|
          level Storm-control suppression level|
          <CR> Sets storm control rate for broadcast , multicast and DLF packets to the default cast packet.

COMMAND : rate-limit output [<integer(1-80000000)>] [<integer(1-80000000)>]
ACTION  : cli_process_iss_ext_cmd(CliHandle, CLI_ISS_PORT_RATE_LIMIT, NULL, $2, $3);
SYNTAX  : rate-limit output [<rate-value>] [<burst-value>] 
PRVID   : 15
HELP    : Enables the rate limiting and burst size rate limiting by configuring the egress packet rate of an interface.
CXT_HELP: rate-limit Configures burst size rate limiting related configuration|
          output Burst size rate limiting output|
          (1-80000000) Rate limit value|
          (1-80000000) Burst limit value|
          <CR> Enables the rate limiting and burst size rate limiting by configuring the egress packet rate of an interface.

COMMAND : no rate-limit output [rate-limit] [burst-limit]
ACTION  : cli_process_iss_ext_cmd(CliHandle, CLI_ISS_NO_PORT_RATE_LIMIT, NULL, $3, $4);
SYNTAX  : no rate-limit output [rate-limit] [burst-limit]
PRVID   : 15
HELP    : Disables the rate limiting and burst size rate limit on an egress port.
CXT_HELP: no Disables the configuration / deletes the entry / resets to default value|
          rate-limit Configures burst size rate limiting related configuration|
          output Burst size rate limiting output|
          rate-limit  Rate limit value|
          burst-limit Burst limit value|
          <CR> Disables the rate limiting and burst size rate limit on an egress port.

#endif

END GROUP   
