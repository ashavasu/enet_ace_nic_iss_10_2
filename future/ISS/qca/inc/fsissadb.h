/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissadb.h,v 1.1 2013/09/28 11:44:16 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSISSADB_H
#define _FSISSADB_H

UINT1 IssAclRateCtrlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 IssAclL2FilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 IssAclL3FilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 IssAclUserDefinedFilterTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 IssRedirectInterfaceGrpTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fsissa [] ={1,3,6,1,4,1,29601,2,21};
tSNMP_OID_TYPE fsissaOID = {9, fsissa};


UINT4 IssAclRateCtrlIndex [ ] ={1,3,6,1,4,1,29601,2,21,1,1,1,1};
UINT4 IssAclRateCtrlDLFLimitValue [ ] ={1,3,6,1,4,1,29601,2,21,1,1,1,2};
UINT4 IssAclRateCtrlBCASTLimitValue [ ] ={1,3,6,1,4,1,29601,2,21,1,1,1,3};
UINT4 IssAclRateCtrlMCASTLimitValue [ ] ={1,3,6,1,4,1,29601,2,21,1,1,1,4};
UINT4 IssAclRateCtrlPortRateLimit [ ] ={1,3,6,1,4,1,29601,2,21,1,1,1,5};
UINT4 IssAclRateCtrlPortBurstSize [ ] ={1,3,6,1,4,1,29601,2,21,1,1,1,6};
UINT4 IssAclL2FilterNo [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,1};
UINT4 IssAclL2FilterPriority [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,2};
UINT4 IssAclL2FilterEtherType [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,3};
UINT4 IssAclL2FilterProtocolType [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,4};
UINT4 IssAclL2FilterDstMacAddr [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,5};
UINT4 IssAclL2FilterSrcMacAddr [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,6};
UINT4 IssAclL2FilterVlanId [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,7};
UINT4 IssAclL2FilterInPortList [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,8};
UINT4 IssAclL2FilterAction [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,9};
UINT4 IssAclL2FilterMatchCount [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,10};
UINT4 IssAclL2FilterStatus [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,11};
UINT4 IssAclL2FilterOutPortList [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,12};
UINT4 IssAclL2FilterDirection [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,13};
UINT4 IssAclL2FilterSubAction [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,14};
UINT4 IssAclL2FilterSubActionId [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,15};
UINT4 IssAclL2FilterRedirectId [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,16};
UINT4 IssAclL2NextFilterNo [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,17};
UINT4 IssAclL2NextFilterType [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,18};
UINT4 IssAclL2FilterCreationMode [ ] ={1,3,6,1,4,1,29601,2,21,2,1,1,19};
UINT4 IssAclL3FilterNo [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,1};
UINT4 IssAclL3FilterPriority [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,2};
UINT4 IssAclL3FilterProtocol [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,3};
UINT4 IssAclL3FilterMessageType [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,4};
UINT4 IssAclL3FilterMessageCode [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,5};
UINT4 IssAclL3FilteAddrType [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,6};
UINT4 IssAclL3FilterDstIpAddr [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,7};
UINT4 IssAclL3FilterSrcIpAddr [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,8};
UINT4 IssAclL3FilterDstIpAddrPrefixLength [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,9};
UINT4 IssAclL3FilterSrcIpAddrPrefixLength [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,10};
UINT4 IssAclL3FilterMinDstProtPort [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,11};
UINT4 IssAclL3FilterMaxDstProtPort [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,12};
UINT4 IssAclL3FilterMinSrcProtPort [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,13};
UINT4 IssAclL3FilterMaxSrcProtPort [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,14};
UINT4 IssAclL3FilterInPortList [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,15};
UINT4 IssAclL3FilterOutPortList [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,16};
UINT4 IssAclL3FilterAckBit [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,17};
UINT4 IssAclL3FilterRstBit [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,18};
UINT4 IssAclL3FilterTos [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,19};
UINT4 IssAclL3FilterDscp [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,20};
UINT4 IssAclL3FilterDirection [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,21};
UINT4 IssAclL3FilterAction [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,22};
UINT4 IssAclL3FilterMatchCount [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,23};
UINT4 IssAclL3FilterFlowId [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,24};
UINT4 IssAclL3FilterStatus [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,25};
UINT4 IssAclL3FilterSubAction [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,26};
UINT4 IssAclL3FilterSubActionId [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,27};
UINT4 IssAclL3FilterRedirectId [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,28};
UINT4 IssAclL3FilterCreationMode [ ] ={1,3,6,1,4,1,29601,2,21,3,1,1,29};
UINT4 IssAclUserDefinedFilterId [ ] ={1,3,6,1,4,1,29601,2,21,4,1,1,1};
UINT4 IssAclUserDefinedFilterPktType [ ] ={1,3,6,1,4,1,29601,2,21,4,1,1,2};
UINT4 IssAclUserDefinedFilterOffSetBase [ ] ={1,3,6,1,4,1,29601,2,21,4,1,1,3};
UINT4 IssAclUserDefinedFilterOffSetValue [ ] ={1,3,6,1,4,1,29601,2,21,4,1,1,4};
UINT4 IssAclUserDefinedFilterOffSetMask [ ] ={1,3,6,1,4,1,29601,2,21,4,1,1,5};
UINT4 IssAclUserDefinedFilterPriority [ ] ={1,3,6,1,4,1,29601,2,21,4,1,1,6};
UINT4 IssAclUserDefinedFilterAction [ ] ={1,3,6,1,4,1,29601,2,21,4,1,1,7};
UINT4 IssAclUserDefinedFilterInPortList [ ] ={1,3,6,1,4,1,29601,2,21,4,1,1,8};
UINT4 IssAclUserDefinedFilterIdOneType [ ] ={1,3,6,1,4,1,29601,2,21,4,1,1,9};
UINT4 IssAclUserDefinedFilterIdOne [ ] ={1,3,6,1,4,1,29601,2,21,4,1,1,10};
UINT4 IssAclUserDefinedFilterIdTwoType [ ] ={1,3,6,1,4,1,29601,2,21,4,1,1,11};
UINT4 IssAclUserDefinedFilterIdTwo [ ] ={1,3,6,1,4,1,29601,2,21,4,1,1,12};
UINT4 IssAclUserDefinedFilterSubAction [ ] ={1,3,6,1,4,1,29601,2,21,4,1,1,13};
UINT4 IssAclUserDefinedFilterSubActionId [ ] ={1,3,6,1,4,1,29601,2,21,4,1,1,14};
UINT4 IssAclUserDefinedFilterRedirectId [ ] ={1,3,6,1,4,1,29601,2,21,4,1,1,15};
UINT4 IssAclUserDefinedFilterStatus [ ] ={1,3,6,1,4,1,29601,2,21,4,1,1,16};
UINT4 IssRedirectInterfaceGrpId [ ] ={1,3,6,1,4,1,29601,2,21,5,1,1,1};
UINT4 IssRedirectInterfaceGrpFilterType [ ] ={1,3,6,1,4,1,29601,2,21,5,1,1,2};
UINT4 IssRedirectInterfaceGrpFilterId [ ] ={1,3,6,1,4,1,29601,2,21,5,1,1,3};
UINT4 IssRedirectInterfaceGrpDistByte [ ] ={1,3,6,1,4,1,29601,2,21,5,1,1,4};
UINT4 IssRedirectInterfaceGrpPortList [ ] ={1,3,6,1,4,1,29601,2,21,5,1,1,5};
UINT4 IssRedirectInterfaceGrpType [ ] ={1,3,6,1,4,1,29601,2,21,5,1,1,6};
UINT4 IssRedirectInterfaceGrpUdbPosition [ ] ={1,3,6,1,4,1,29601,2,21,5,1,1,7};
UINT4 IssRedirectInterfaceGrpStatus [ ] ={1,3,6,1,4,1,29601,2,21,5,1,1,8};
UINT4 IssRedirectInterfaceGrpIdNextFree [ ] ={1,3,6,1,4,1,29601,2,21,6,1};




tMbDbEntry fsissaMibEntry[]= {

{{13,IssAclRateCtrlIndex}, GetNextIndexIssAclRateCtrlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssAclRateCtrlTableINDEX, 1, 0, 0, NULL},

{{13,IssAclRateCtrlDLFLimitValue}, GetNextIndexIssAclRateCtrlTable, IssAclRateCtrlDLFLimitValueGet, IssAclRateCtrlDLFLimitValueSet, IssAclRateCtrlDLFLimitValueTest, IssAclRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclRateCtrlTableINDEX, 1, 0, 0, "0"},

{{13,IssAclRateCtrlBCASTLimitValue}, GetNextIndexIssAclRateCtrlTable, IssAclRateCtrlBCASTLimitValueGet, IssAclRateCtrlBCASTLimitValueSet, IssAclRateCtrlBCASTLimitValueTest, IssAclRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclRateCtrlTableINDEX, 1, 0, 0, "0"},

{{13,IssAclRateCtrlMCASTLimitValue}, GetNextIndexIssAclRateCtrlTable, IssAclRateCtrlMCASTLimitValueGet, IssAclRateCtrlMCASTLimitValueSet, IssAclRateCtrlMCASTLimitValueTest, IssAclRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclRateCtrlTableINDEX, 1, 0, 0, "0"},

{{13,IssAclRateCtrlPortRateLimit}, GetNextIndexIssAclRateCtrlTable, IssAclRateCtrlPortRateLimitGet, IssAclRateCtrlPortRateLimitSet, IssAclRateCtrlPortRateLimitTest, IssAclRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclRateCtrlTableINDEX, 1, 0, 0, NULL},

{{13,IssAclRateCtrlPortBurstSize}, GetNextIndexIssAclRateCtrlTable, IssAclRateCtrlPortBurstSizeGet, IssAclRateCtrlPortBurstSizeSet, IssAclRateCtrlPortBurstSizeTest, IssAclRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclRateCtrlTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL2FilterNo}, GetNextIndexIssAclL2FilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssAclL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL2FilterPriority}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterPriorityGet, IssAclL2FilterPrioritySet, IssAclL2FilterPriorityTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 0, "1"},

{{13,IssAclL2FilterEtherType}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterEtherTypeGet, IssAclL2FilterEtherTypeSet, IssAclL2FilterEtherTypeTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssAclL2FilterProtocolType}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterProtocolTypeGet, IssAclL2FilterProtocolTypeSet, IssAclL2FilterProtocolTypeTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssAclL2FilterDstMacAddr}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterDstMacAddrGet, IssAclL2FilterDstMacAddrSet, IssAclL2FilterDstMacAddrTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL2FilterSrcMacAddr}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterSrcMacAddrGet, IssAclL2FilterSrcMacAddrSet, IssAclL2FilterSrcMacAddrTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL2FilterVlanId}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterVlanIdGet, IssAclL2FilterVlanIdSet, IssAclL2FilterVlanIdTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssAclL2FilterInPortList}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterInPortListGet, IssAclL2FilterInPortListSet, IssAclL2FilterInPortListTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL2FilterAction}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterActionGet, IssAclL2FilterActionSet, IssAclL2FilterActionTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 0, "1"},

{{13,IssAclL2FilterMatchCount}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterMatchCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IssAclL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL2FilterStatus}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterStatusGet, IssAclL2FilterStatusSet, IssAclL2FilterStatusTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 1, NULL},

{{13,IssAclL2FilterOutPortList}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterOutPortListGet, IssAclL2FilterOutPortListSet, IssAclL2FilterOutPortListTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL2FilterDirection}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterDirectionGet, IssAclL2FilterDirectionSet, IssAclL2FilterDirectionTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 0, "1"},

{{13,IssAclL2FilterSubAction}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterSubActionGet, IssAclL2FilterSubActionSet, IssAclL2FilterSubActionTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssAclL2FilterSubActionId}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterSubActionIdGet, IssAclL2FilterSubActionIdSet, IssAclL2FilterSubActionIdTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssAclL2FilterRedirectId}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterRedirectIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IssAclL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL2NextFilterNo}, GetNextIndexIssAclL2FilterTable, IssAclL2NextFilterNoGet, IssAclL2NextFilterNoSet, IssAclL2NextFilterNoTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL2NextFilterType}, GetNextIndexIssAclL2FilterTable, IssAclL2NextFilterTypeGet, IssAclL2NextFilterTypeSet, IssAclL2NextFilterTypeTest, IssAclL2FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL2FilterCreationMode}, GetNextIndexIssAclL2FilterTable, IssAclL2FilterCreationModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IssAclL2FilterTableINDEX, 1, 0, 0, "2"},

{{13,IssAclL3FilterNo}, GetNextIndexIssAclL3FilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssAclL3FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL3FilterPriority}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterPriorityGet, IssAclL3FilterPrioritySet, IssAclL3FilterPriorityTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "1"},

{{13,IssAclL3FilterProtocol}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterProtocolGet, IssAclL3FilterProtocolSet, IssAclL3FilterProtocolTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "255"},

{{13,IssAclL3FilterMessageType}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterMessageTypeGet, IssAclL3FilterMessageTypeSet, IssAclL3FilterMessageTypeTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "255"},

{{13,IssAclL3FilterMessageCode}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterMessageCodeGet, IssAclL3FilterMessageCodeSet, IssAclL3FilterMessageCodeTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "255"},

{{13,IssAclL3FilteAddrType}, GetNextIndexIssAclL3FilterTable, IssAclL3FilteAddrTypeGet, IssAclL3FilteAddrTypeSet, IssAclL3FilteAddrTypeTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL3FilterDstIpAddr}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterDstIpAddrGet, IssAclL3FilterDstIpAddrSet, IssAclL3FilterDstIpAddrTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL3FilterSrcIpAddr}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterSrcIpAddrGet, IssAclL3FilterSrcIpAddrSet, IssAclL3FilterSrcIpAddrTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL3FilterDstIpAddrPrefixLength}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterDstIpAddrPrefixLengthGet, IssAclL3FilterDstIpAddrPrefixLengthSet, IssAclL3FilterDstIpAddrPrefixLengthTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssAclL3FilterSrcIpAddrPrefixLength}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterSrcIpAddrPrefixLengthGet, IssAclL3FilterSrcIpAddrPrefixLengthSet, IssAclL3FilterSrcIpAddrPrefixLengthTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssAclL3FilterMinDstProtPort}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterMinDstProtPortGet, IssAclL3FilterMinDstProtPortSet, IssAclL3FilterMinDstProtPortTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssAclL3FilterMaxDstProtPort}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterMaxDstProtPortGet, IssAclL3FilterMaxDstProtPortSet, IssAclL3FilterMaxDstProtPortTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "65535"},

{{13,IssAclL3FilterMinSrcProtPort}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterMinSrcProtPortGet, IssAclL3FilterMinSrcProtPortSet, IssAclL3FilterMinSrcProtPortTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssAclL3FilterMaxSrcProtPort}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterMaxSrcProtPortGet, IssAclL3FilterMaxSrcProtPortSet, IssAclL3FilterMaxSrcProtPortTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "65535"},

{{13,IssAclL3FilterInPortList}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterInPortListGet, IssAclL3FilterInPortListSet, IssAclL3FilterInPortListTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL3FilterOutPortList}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterOutPortListGet, IssAclL3FilterOutPortListSet, IssAclL3FilterOutPortListTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL3FilterAckBit}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterAckBitGet, IssAclL3FilterAckBitSet, IssAclL3FilterAckBitTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "3"},

{{13,IssAclL3FilterRstBit}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterRstBitGet, IssAclL3FilterRstBitSet, IssAclL3FilterRstBitTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "3"},

{{13,IssAclL3FilterTos}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterTosGet, IssAclL3FilterTosSet, IssAclL3FilterTosTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "-1"},

{{13,IssAclL3FilterDscp}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterDscpGet, IssAclL3FilterDscpSet, IssAclL3FilterDscpTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "-1"},

{{13,IssAclL3FilterDirection}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterDirectionGet, IssAclL3FilterDirectionSet, IssAclL3FilterDirectionTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "1"},

{{13,IssAclL3FilterAction}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterActionGet, IssAclL3FilterActionSet, IssAclL3FilterActionTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "1"},

{{13,IssAclL3FilterMatchCount}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterMatchCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IssAclL3FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL3FilterFlowId}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterFlowIdGet, IssAclL3FilterFlowIdSet, IssAclL3FilterFlowIdTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL3FilterStatus}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterStatusGet, IssAclL3FilterStatusSet, IssAclL3FilterStatusTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 1, NULL},

{{13,IssAclL3FilterSubAction}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterSubActionGet, IssAclL3FilterSubActionSet, IssAclL3FilterSubActionTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssAclL3FilterSubActionId}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterSubActionIdGet, IssAclL3FilterSubActionIdSet, IssAclL3FilterSubActionIdTest, IssAclL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclL3FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssAclL3FilterRedirectId}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterRedirectIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IssAclL3FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclL3FilterCreationMode}, GetNextIndexIssAclL3FilterTable, IssAclL3FilterCreationModeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IssAclL3FilterTableINDEX, 1, 0, 0, "2"},

{{13,IssAclUserDefinedFilterId}, GetNextIndexIssAclUserDefinedFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, IssAclUserDefinedFilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclUserDefinedFilterPktType}, GetNextIndexIssAclUserDefinedFilterTable, IssAclUserDefinedFilterPktTypeGet, IssAclUserDefinedFilterPktTypeSet, IssAclUserDefinedFilterPktTypeTest, IssAclUserDefinedFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclUserDefinedFilterTableINDEX, 1, 0, 0, "0"},

{{13,IssAclUserDefinedFilterOffSetBase}, GetNextIndexIssAclUserDefinedFilterTable, IssAclUserDefinedFilterOffSetBaseGet, IssAclUserDefinedFilterOffSetBaseSet, IssAclUserDefinedFilterOffSetBaseTest, IssAclUserDefinedFilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclUserDefinedFilterTableINDEX, 1, 0, 0, "0"},

{{13,IssAclUserDefinedFilterOffSetValue}, GetNextIndexIssAclUserDefinedFilterTable, IssAclUserDefinedFilterOffSetValueGet, IssAclUserDefinedFilterOffSetValueSet, IssAclUserDefinedFilterOffSetValueTest, IssAclUserDefinedFilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssAclUserDefinedFilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclUserDefinedFilterOffSetMask}, GetNextIndexIssAclUserDefinedFilterTable, IssAclUserDefinedFilterOffSetMaskGet, IssAclUserDefinedFilterOffSetMaskSet, IssAclUserDefinedFilterOffSetMaskTest, IssAclUserDefinedFilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssAclUserDefinedFilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclUserDefinedFilterPriority}, GetNextIndexIssAclUserDefinedFilterTable, IssAclUserDefinedFilterPriorityGet, IssAclUserDefinedFilterPrioritySet, IssAclUserDefinedFilterPriorityTest, IssAclUserDefinedFilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclUserDefinedFilterTableINDEX, 1, 0, 0, "1"},

{{13,IssAclUserDefinedFilterAction}, GetNextIndexIssAclUserDefinedFilterTable, IssAclUserDefinedFilterActionGet, IssAclUserDefinedFilterActionSet, IssAclUserDefinedFilterActionTest, IssAclUserDefinedFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclUserDefinedFilterTableINDEX, 1, 0, 0, "1"},

{{13,IssAclUserDefinedFilterInPortList}, GetNextIndexIssAclUserDefinedFilterTable, IssAclUserDefinedFilterInPortListGet, IssAclUserDefinedFilterInPortListSet, IssAclUserDefinedFilterInPortListTest, IssAclUserDefinedFilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssAclUserDefinedFilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclUserDefinedFilterIdOneType}, GetNextIndexIssAclUserDefinedFilterTable, IssAclUserDefinedFilterIdOneTypeGet, IssAclUserDefinedFilterIdOneTypeSet, IssAclUserDefinedFilterIdOneTypeTest, IssAclUserDefinedFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclUserDefinedFilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclUserDefinedFilterIdOne}, GetNextIndexIssAclUserDefinedFilterTable, IssAclUserDefinedFilterIdOneGet, IssAclUserDefinedFilterIdOneSet, IssAclUserDefinedFilterIdOneTest, IssAclUserDefinedFilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssAclUserDefinedFilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclUserDefinedFilterIdTwoType}, GetNextIndexIssAclUserDefinedFilterTable, IssAclUserDefinedFilterIdTwoTypeGet, IssAclUserDefinedFilterIdTwoTypeSet, IssAclUserDefinedFilterIdTwoTypeTest, IssAclUserDefinedFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclUserDefinedFilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclUserDefinedFilterIdTwo}, GetNextIndexIssAclUserDefinedFilterTable, IssAclUserDefinedFilterIdTwoGet, IssAclUserDefinedFilterIdTwoSet, IssAclUserDefinedFilterIdTwoTest, IssAclUserDefinedFilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssAclUserDefinedFilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclUserDefinedFilterSubAction}, GetNextIndexIssAclUserDefinedFilterTable, IssAclUserDefinedFilterSubActionGet, IssAclUserDefinedFilterSubActionSet, IssAclUserDefinedFilterSubActionTest, IssAclUserDefinedFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclUserDefinedFilterTableINDEX, 1, 0, 0, "0"},

{{13,IssAclUserDefinedFilterSubActionId}, GetNextIndexIssAclUserDefinedFilterTable, IssAclUserDefinedFilterSubActionIdGet, IssAclUserDefinedFilterSubActionIdSet, IssAclUserDefinedFilterSubActionIdTest, IssAclUserDefinedFilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssAclUserDefinedFilterTableINDEX, 1, 0, 0, "0"},

{{13,IssAclUserDefinedFilterRedirectId}, GetNextIndexIssAclUserDefinedFilterTable, IssAclUserDefinedFilterRedirectIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, IssAclUserDefinedFilterTableINDEX, 1, 0, 0, NULL},

{{13,IssAclUserDefinedFilterStatus}, GetNextIndexIssAclUserDefinedFilterTable, IssAclUserDefinedFilterStatusGet, IssAclUserDefinedFilterStatusSet, IssAclUserDefinedFilterStatusTest, IssAclUserDefinedFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssAclUserDefinedFilterTableINDEX, 1, 0, 1, NULL},

{{13,IssRedirectInterfaceGrpId}, GetNextIndexIssRedirectInterfaceGrpTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, IssRedirectInterfaceGrpTableINDEX, 1, 0, 0, NULL},

{{13,IssRedirectInterfaceGrpFilterType}, GetNextIndexIssRedirectInterfaceGrpTable, IssRedirectInterfaceGrpFilterTypeGet, IssRedirectInterfaceGrpFilterTypeSet, IssRedirectInterfaceGrpFilterTypeTest, IssRedirectInterfaceGrpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssRedirectInterfaceGrpTableINDEX, 1, 0, 0, NULL},

{{13,IssRedirectInterfaceGrpFilterId}, GetNextIndexIssRedirectInterfaceGrpTable, IssRedirectInterfaceGrpFilterIdGet, IssRedirectInterfaceGrpFilterIdSet, IssRedirectInterfaceGrpFilterIdTest, IssRedirectInterfaceGrpTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssRedirectInterfaceGrpTableINDEX, 1, 0, 0, NULL},

{{13,IssRedirectInterfaceGrpDistByte}, GetNextIndexIssRedirectInterfaceGrpTable, IssRedirectInterfaceGrpDistByteGet, IssRedirectInterfaceGrpDistByteSet, IssRedirectInterfaceGrpDistByteTest, IssRedirectInterfaceGrpTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssRedirectInterfaceGrpTableINDEX, 1, 0, 0, NULL},

{{13,IssRedirectInterfaceGrpPortList}, GetNextIndexIssRedirectInterfaceGrpTable, IssRedirectInterfaceGrpPortListGet, IssRedirectInterfaceGrpPortListSet, IssRedirectInterfaceGrpPortListTest, IssRedirectInterfaceGrpTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssRedirectInterfaceGrpTableINDEX, 1, 0, 0, NULL},

{{13,IssRedirectInterfaceGrpType}, GetNextIndexIssRedirectInterfaceGrpTable, IssRedirectInterfaceGrpTypeGet, IssRedirectInterfaceGrpTypeSet, IssRedirectInterfaceGrpTypeTest, IssRedirectInterfaceGrpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssRedirectInterfaceGrpTableINDEX, 1, 0, 0, NULL},

{{13,IssRedirectInterfaceGrpUdbPosition}, GetNextIndexIssRedirectInterfaceGrpTable, IssRedirectInterfaceGrpUdbPositionGet, IssRedirectInterfaceGrpUdbPositionSet, IssRedirectInterfaceGrpUdbPositionTest, IssRedirectInterfaceGrpTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssRedirectInterfaceGrpTableINDEX, 1, 0, 0, "0"},

{{13,IssRedirectInterfaceGrpStatus}, GetNextIndexIssRedirectInterfaceGrpTable, IssRedirectInterfaceGrpStatusGet, IssRedirectInterfaceGrpStatusSet, IssRedirectInterfaceGrpStatusTest, IssRedirectInterfaceGrpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssRedirectInterfaceGrpTableINDEX, 1, 0, 1, NULL},

{{11,IssRedirectInterfaceGrpIdNextFree}, NULL, IssRedirectInterfaceGrpIdNextFreeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

};
tMibData fsissaEntry = { 80, fsissaMibEntry };

#endif /* _FSISSADB_H */

