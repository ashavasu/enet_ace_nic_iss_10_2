/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissedb.h,v 1.1 2013/09/28 11:44:16 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSISSEDB_H
#define _FSISSEDB_H

UINT1 IssExtRateCtrlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 IssExtL2FilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 IssExtL3FilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fsisse [10] ={1,3,6,1,4,1,2076,81,8};
tSNMP_OID_TYPE fsisseOID = {10, fsisse};


UINT4 IssExtRateCtrlIndex [ ] ={1,3,6,1,4,1,2076,81,8,1,1,1,1};
UINT4 IssExtRateCtrlDLFLimitValue [ ] ={1,3,6,1,4,1,2076,81,8,1,1,1,2};
UINT4 IssExtRateCtrlBCASTLimitValue [ ] ={1,3,6,1,4,1,2076,81,8,1,1,1,3};
UINT4 IssExtRateCtrlMCASTLimitValue [ ] ={1,3,6,1,4,1,2076,81,8,1,1,1,4};
UINT4 IssExtRateCtrlPortRateLimit [ ] ={1,3,6,1,4,1,2076,81,8,1,1,1,5};
UINT4 IssExtRateCtrlPortBurstSize [ ] ={1,3,6,1,4,1,2076,81,8,1,1,1,6};
UINT4 IssExtL2FilterNo [ ] ={1,3,6,1,4,1,2076,81,8,2,1,1,1};
UINT4 IssExtL2FilterPriority [ ] ={1,3,6,1,4,1,2076,81,8,2,1,1,2};
UINT4 IssExtL2FilterEtherType [ ] ={1,3,6,1,4,1,2076,81,8,2,1,1,3};
UINT4 IssExtL2FilterProtocolType [ ] ={1,3,6,1,4,1,2076,81,8,2,1,1,4};
UINT4 IssExtL2FilterDstMacAddr [ ] ={1,3,6,1,4,1,2076,81,8,2,1,1,5};
UINT4 IssExtL2FilterSrcMacAddr [ ] ={1,3,6,1,4,1,2076,81,8,2,1,1,6};
UINT4 IssExtL2FilterVlanId [ ] ={1,3,6,1,4,1,2076,81,8,2,1,1,7};
UINT4 IssExtL2FilterInPortList [ ] ={1,3,6,1,4,1,2076,81,8,2,1,1,8};
UINT4 IssExtL2FilterAction [ ] ={1,3,6,1,4,1,2076,81,8,2,1,1,9};
UINT4 IssExtL2FilterMatchCount [ ] ={1,3,6,1,4,1,2076,81,8,2,1,1,10};
UINT4 IssExtL2FilterStatus [ ] ={1,3,6,1,4,1,2076,81,8,2,1,1,11};
UINT4 IssExtL2FilterOutPortList [ ] ={1,3,6,1,4,1,2076,81,8,2,1,1,12};
UINT4 IssExtL2FilterDirection [ ] ={1,3,6,1,4,1,2076,81,8,2,1,1,13};
UINT4 IssExtL3FilterNo [ ] ={1,3,6,1,4,1,2076,81,8,3,1,1,1};
UINT4 IssExtL3FilterPriority [ ] ={1,3,6,1,4,1,2076,81,8,3,1,1,2};
UINT4 IssExtL3FilterProtocol [ ] ={1,3,6,1,4,1,2076,81,8,3,1,1,3};
UINT4 IssExtL3FilterMessageType [ ] ={1,3,6,1,4,1,2076,81,8,3,1,1,4};
UINT4 IssExtL3FilterMessageCode [ ] ={1,3,6,1,4,1,2076,81,8,3,1,1,5};
UINT4 IssExtL3FilterDstIpAddr [ ] ={1,3,6,1,4,1,2076,81,8,3,1,1,6};
UINT4 IssExtL3FilterSrcIpAddr [ ] ={1,3,6,1,4,1,2076,81,8,3,1,1,7};
UINT4 IssExtL3FilterDstIpAddrMask [ ] ={1,3,6,1,4,1,2076,81,8,3,1,1,8};
UINT4 IssExtL3FilterSrcIpAddrMask [ ] ={1,3,6,1,4,1,2076,81,8,3,1,1,9};
UINT4 IssExtL3FilterMinDstProtPort [ ] ={1,3,6,1,4,1,2076,81,8,3,1,1,10};
UINT4 IssExtL3FilterMaxDstProtPort [ ] ={1,3,6,1,4,1,2076,81,8,3,1,1,11};
UINT4 IssExtL3FilterMinSrcProtPort [ ] ={1,3,6,1,4,1,2076,81,8,3,1,1,12};
UINT4 IssExtL3FilterMaxSrcProtPort [ ] ={1,3,6,1,4,1,2076,81,8,3,1,1,13};
UINT4 IssExtL3FilterInPortList [ ] ={1,3,6,1,4,1,2076,81,8,3,1,1,14};
UINT4 IssExtL3FilterOutPortList [ ] ={1,3,6,1,4,1,2076,81,8,3,1,1,15};
UINT4 IssExtL3FilterAckBit [ ] ={1,3,6,1,4,1,2076,81,8,3,1,1,16};
UINT4 IssExtL3FilterRstBit [ ] ={1,3,6,1,4,1,2076,81,8,3,1,1,17};
UINT4 IssExtL3FilterTos [ ] ={1,3,6,1,4,1,2076,81,8,3,1,1,18};
UINT4 IssExtL3FilterDscp [ ] ={1,3,6,1,4,1,2076,81,8,3,1,1,19};
UINT4 IssExtL3FilterDirection [ ] ={1,3,6,1,4,1,2076,81,8,3,1,1,20};
UINT4 IssExtL3FilterAction [ ] ={1,3,6,1,4,1,2076,81,8,3,1,1,21};
UINT4 IssExtL3FilterMatchCount [ ] ={1,3,6,1,4,1,2076,81,8,3,1,1,22};
UINT4 IssExtL3FilterStatus [ ] ={1,3,6,1,4,1,2076,81,8,3,1,1,23};


tMbDbEntry fsisseRateMibEntry[]= {

{{13,IssExtRateCtrlIndex}, GetNextIndexIssExtRateCtrlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssExtRateCtrlTableINDEX, 1, 0, 0, NULL},

{{13,IssExtRateCtrlDLFLimitValue}, GetNextIndexIssExtRateCtrlTable, IssExtRateCtrlDLFLimitValueGet, IssExtRateCtrlDLFLimitValueSet, IssExtRateCtrlDLFLimitValueTest, IssExtRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssExtRateCtrlTableINDEX, 1, 0, 0, "0"},

{{13,IssExtRateCtrlBCASTLimitValue}, GetNextIndexIssExtRateCtrlTable, IssExtRateCtrlBCASTLimitValueGet, IssExtRateCtrlBCASTLimitValueSet, IssExtRateCtrlBCASTLimitValueTest, IssExtRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssExtRateCtrlTableINDEX, 1, 0, 0, "0"},

{{13,IssExtRateCtrlMCASTLimitValue}, GetNextIndexIssExtRateCtrlTable, IssExtRateCtrlMCASTLimitValueGet, IssExtRateCtrlMCASTLimitValueSet, IssExtRateCtrlMCASTLimitValueTest, IssExtRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssExtRateCtrlTableINDEX, 1, 0, 0, "0"},

{{13,IssExtRateCtrlPortRateLimit}, GetNextIndexIssExtRateCtrlTable, IssExtRateCtrlPortRateLimitGet, IssExtRateCtrlPortRateLimitSet, IssExtRateCtrlPortRateLimitTest, IssExtRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssExtRateCtrlTableINDEX, 1, 0, 0, NULL},

{{13,IssExtRateCtrlPortBurstSize}, GetNextIndexIssExtRateCtrlTable, IssExtRateCtrlPortBurstSizeGet, IssExtRateCtrlPortBurstSizeSet, IssExtRateCtrlPortBurstSizeTest, IssExtRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssExtRateCtrlTableINDEX, 1, 0, 0, NULL},
};
tMibData fsisseRateEntry = { 6, fsisseRateMibEntry };

tMbDbEntry fsisseL2MibEntry[]= {

{{13,IssExtL2FilterNo}, GetNextIndexIssExtL2FilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssExtL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssExtL2FilterPriority}, GetNextIndexIssExtL2FilterTable, IssExtL2FilterPriorityGet, IssExtL2FilterPrioritySet, IssExtL2FilterPriorityTest, IssExtL2FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssExtL2FilterTableINDEX, 1, 0, 0, "1"},

{{13,IssExtL2FilterEtherType}, GetNextIndexIssExtL2FilterTable, IssExtL2FilterEtherTypeGet, IssExtL2FilterEtherTypeSet, IssExtL2FilterEtherTypeTest, IssExtL2FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssExtL2FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssExtL2FilterProtocolType}, GetNextIndexIssExtL2FilterTable, IssExtL2FilterProtocolTypeGet, IssExtL2FilterProtocolTypeSet, IssExtL2FilterProtocolTypeTest, IssExtL2FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssExtL2FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssExtL2FilterDstMacAddr}, GetNextIndexIssExtL2FilterTable, IssExtL2FilterDstMacAddrGet, IssExtL2FilterDstMacAddrSet, IssExtL2FilterDstMacAddrTest, IssExtL2FilterTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, IssExtL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssExtL2FilterSrcMacAddr}, GetNextIndexIssExtL2FilterTable, IssExtL2FilterSrcMacAddrGet, IssExtL2FilterSrcMacAddrSet, IssExtL2FilterSrcMacAddrTest, IssExtL2FilterTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, IssExtL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssExtL2FilterVlanId}, GetNextIndexIssExtL2FilterTable, IssExtL2FilterVlanIdGet, IssExtL2FilterVlanIdSet, IssExtL2FilterVlanIdTest, IssExtL2FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssExtL2FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssExtL2FilterInPortList}, GetNextIndexIssExtL2FilterTable, IssExtL2FilterInPortListGet, IssExtL2FilterInPortListSet, IssExtL2FilterInPortListTest, IssExtL2FilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssExtL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssExtL2FilterAction}, GetNextIndexIssExtL2FilterTable, IssExtL2FilterActionGet, IssExtL2FilterActionSet, IssExtL2FilterActionTest, IssExtL2FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssExtL2FilterTableINDEX, 1, 0, 0, "1"},

{{13,IssExtL2FilterMatchCount}, GetNextIndexIssExtL2FilterTable, IssExtL2FilterMatchCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IssExtL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssExtL2FilterStatus}, GetNextIndexIssExtL2FilterTable, IssExtL2FilterStatusGet, IssExtL2FilterStatusSet, IssExtL2FilterStatusTest, IssExtL2FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssExtL2FilterTableINDEX, 1, 0, 1, NULL},

{{13,IssExtL2FilterOutPortList}, GetNextIndexIssExtL2FilterTable, IssExtL2FilterOutPortListGet, IssExtL2FilterOutPortListSet, IssExtL2FilterOutPortListTest, IssExtL2FilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssExtL2FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssExtL2FilterDirection}, GetNextIndexIssExtL2FilterTable, IssExtL2FilterDirectionGet, IssExtL2FilterDirectionSet, IssExtL2FilterDirectionTest, IssExtL2FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssExtL2FilterTableINDEX, 1, 0, 0, "1"},
};
tMibData fsisseL2Entry = { 13, fsisseL2MibEntry };

tMbDbEntry fsisseL3MibEntry[]= {

{{13,IssExtL3FilterNo}, GetNextIndexIssExtL3FilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssExtL3FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssExtL3FilterPriority}, GetNextIndexIssExtL3FilterTable, IssExtL3FilterPriorityGet, IssExtL3FilterPrioritySet, IssExtL3FilterPriorityTest, IssExtL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssExtL3FilterTableINDEX, 1, 0, 0, "1"},

{{13,IssExtL3FilterProtocol}, GetNextIndexIssExtL3FilterTable, IssExtL3FilterProtocolGet, IssExtL3FilterProtocolSet, IssExtL3FilterProtocolTest, IssExtL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssExtL3FilterTableINDEX, 1, 0, 0, "255"},

{{13,IssExtL3FilterMessageType}, GetNextIndexIssExtL3FilterTable, IssExtL3FilterMessageTypeGet, IssExtL3FilterMessageTypeSet, IssExtL3FilterMessageTypeTest, IssExtL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssExtL3FilterTableINDEX, 1, 0, 0, "255"},

{{13,IssExtL3FilterMessageCode}, GetNextIndexIssExtL3FilterTable, IssExtL3FilterMessageCodeGet, IssExtL3FilterMessageCodeSet, IssExtL3FilterMessageCodeTest, IssExtL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssExtL3FilterTableINDEX, 1, 0, 0, "255"},

{{13,IssExtL3FilterDstIpAddr}, GetNextIndexIssExtL3FilterTable, IssExtL3FilterDstIpAddrGet, IssExtL3FilterDstIpAddrSet, IssExtL3FilterDstIpAddrTest, IssExtL3FilterTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IssExtL3FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssExtL3FilterSrcIpAddr}, GetNextIndexIssExtL3FilterTable, IssExtL3FilterSrcIpAddrGet, IssExtL3FilterSrcIpAddrSet, IssExtL3FilterSrcIpAddrTest, IssExtL3FilterTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IssExtL3FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssExtL3FilterDstIpAddrMask}, GetNextIndexIssExtL3FilterTable, IssExtL3FilterDstIpAddrMaskGet, IssExtL3FilterDstIpAddrMaskSet, IssExtL3FilterDstIpAddrMaskTest, IssExtL3FilterTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IssExtL3FilterTableINDEX, 1, 0, 0, "4294967295"},

{{13,IssExtL3FilterSrcIpAddrMask}, GetNextIndexIssExtL3FilterTable, IssExtL3FilterSrcIpAddrMaskGet, IssExtL3FilterSrcIpAddrMaskSet, IssExtL3FilterSrcIpAddrMaskTest, IssExtL3FilterTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IssExtL3FilterTableINDEX, 1, 0, 0, "4294967295"},

{{13,IssExtL3FilterMinDstProtPort}, GetNextIndexIssExtL3FilterTable, IssExtL3FilterMinDstProtPortGet, IssExtL3FilterMinDstProtPortSet, IssExtL3FilterMinDstProtPortTest, IssExtL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssExtL3FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssExtL3FilterMaxDstProtPort}, GetNextIndexIssExtL3FilterTable, IssExtL3FilterMaxDstProtPortGet, IssExtL3FilterMaxDstProtPortSet, IssExtL3FilterMaxDstProtPortTest, IssExtL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssExtL3FilterTableINDEX, 1, 0, 0, "65535"},

{{13,IssExtL3FilterMinSrcProtPort}, GetNextIndexIssExtL3FilterTable, IssExtL3FilterMinSrcProtPortGet, IssExtL3FilterMinSrcProtPortSet, IssExtL3FilterMinSrcProtPortTest, IssExtL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssExtL3FilterTableINDEX, 1, 0, 0, "0"},

{{13,IssExtL3FilterMaxSrcProtPort}, GetNextIndexIssExtL3FilterTable, IssExtL3FilterMaxSrcProtPortGet, IssExtL3FilterMaxSrcProtPortSet, IssExtL3FilterMaxSrcProtPortTest, IssExtL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssExtL3FilterTableINDEX, 1, 0, 0, "65535"},

{{13,IssExtL3FilterInPortList}, GetNextIndexIssExtL3FilterTable, IssExtL3FilterInPortListGet, IssExtL3FilterInPortListSet, IssExtL3FilterInPortListTest, IssExtL3FilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssExtL3FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssExtL3FilterOutPortList}, GetNextIndexIssExtL3FilterTable, IssExtL3FilterOutPortListGet, IssExtL3FilterOutPortListSet, IssExtL3FilterOutPortListTest, IssExtL3FilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssExtL3FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssExtL3FilterAckBit}, GetNextIndexIssExtL3FilterTable, IssExtL3FilterAckBitGet, IssExtL3FilterAckBitSet, IssExtL3FilterAckBitTest, IssExtL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssExtL3FilterTableINDEX, 1, 0, 0, "3"},

{{13,IssExtL3FilterRstBit}, GetNextIndexIssExtL3FilterTable, IssExtL3FilterRstBitGet, IssExtL3FilterRstBitSet, IssExtL3FilterRstBitTest, IssExtL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssExtL3FilterTableINDEX, 1, 0, 0, "3"},

{{13,IssExtL3FilterTos}, GetNextIndexIssExtL3FilterTable, IssExtL3FilterTosGet, IssExtL3FilterTosSet, IssExtL3FilterTosTest, IssExtL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssExtL3FilterTableINDEX, 1, 0, 0, "1"},

{{13,IssExtL3FilterDscp}, GetNextIndexIssExtL3FilterTable, IssExtL3FilterDscpGet, IssExtL3FilterDscpSet, IssExtL3FilterDscpTest, IssExtL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssExtL3FilterTableINDEX, 1, 0, 0, "1"},

{{13,IssExtL3FilterDirection}, GetNextIndexIssExtL3FilterTable, IssExtL3FilterDirectionGet, IssExtL3FilterDirectionSet, IssExtL3FilterDirectionTest, IssExtL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssExtL3FilterTableINDEX, 1, 0, 0, "1"},

{{13,IssExtL3FilterAction}, GetNextIndexIssExtL3FilterTable, IssExtL3FilterActionGet, IssExtL3FilterActionSet, IssExtL3FilterActionTest, IssExtL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssExtL3FilterTableINDEX, 1, 0, 0, "1"},

{{13,IssExtL3FilterMatchCount}, GetNextIndexIssExtL3FilterTable, IssExtL3FilterMatchCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IssExtL3FilterTableINDEX, 1, 0, 0, NULL},

{{13,IssExtL3FilterStatus}, GetNextIndexIssExtL3FilterTable, IssExtL3FilterStatusGet, IssExtL3FilterStatusSet, IssExtL3FilterStatusTest, IssExtL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssExtL3FilterTableINDEX, 1, 0, 1, NULL},
};
tMibData fsisseL3Entry = { 23, fsisseL3MibEntry };
#endif /* _FSISSEDB_H */

/* Reason for Splitting the registrationa into groups
 * -------------------------------------------------
 * Static Sync up for ACL Metro OID is not happening even though the MIB 
 * is registered with MSR Trigger True. The problme is because, ACL Metro 
 * MIB registers its OID as (...81.8.4). But the Acl extension MIB has 
 * registered its OID as (...81.8) with MSR trigger as False. During the  
 * static sync up, framework is searching for the parent OID and ending up 
 * in getting the entry of (...81.8) which has MSR trigger as False. This 
 * results in not sending syncup to the Standby for the MEtro ACL OID which 
 * has (..81.8.4). 
 *
 * Solution
 * --------
 * ACL extension been has been splitted into three and registered individualy.
 * (..81.8.1), (..81.8.2) and (..81.8.3). This solves the problem of getting 
 * the parent entry of Metro OID.
 *
 * Note: This is not going to cause any problem because the MIB fsissext.mib 
 * is an obsolete one as we are using fsissacl.mib and not going to be updated
 * with new objects, hence generation of the corresponding wrapper and db 
 * files will not happen. */

tMibData *fsisseEntry[]= {
    NULL,
    &fsisseRateEntry,
    &fsisseL2Entry,
    &fsisseL3Entry
};
