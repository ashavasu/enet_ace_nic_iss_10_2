/* $Id: aclcli.c,v 1.7 2013/01/18 12:04:48 siva Exp $ */
/************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                 */
/* Licensee Aricent Inc., 2004-2005       */
/*                                                          */
/*  FILE NAME             : aclcli.c                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                 */
/*  SUBSYSTEM NAME        : ISS                             */
/*  MODULE NAME           : CLI                             */
/*  LANGUAGE              : C                               */
/*  TARGET ENVIRONMENT    :                                 */
/*  DATE OF FIRST RELEASE :                                 */
/*  AUTHOR                : Aricent Inc.                 */
/*  DESCRIPTION           : This file contains CLI routines */
/*                          related to system commands      */
/*                                                          */
/************************************************************/
#ifndef __ACLCLI_C__
#define __ACLCLI_C__

#include "lr.h"
#include "issexinc.h"
#include "fsisselw.h"
#include "aclcli.h"
#include "aclmcli.h"
#include "isscli.h"
#include "fsissecli.h"
#include "fsissewr.h"

/***************************************************************/
/*  Function Name   : cli_process_acl_cmd                      */
/*  Description     : This function servers as the handler for */
/*                    all system related CLI commands          */
/*  Input(s)        :                                          */
/*                    CliHandle - CLI Handle                   */
/*                    u4Command - Command given by user        */
/*                    Variable set of inputs depending on      */
/*                    the user command                         */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : None                                     */
/***************************************************************/
INT4
cli_process_acl_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[CLI_ACL_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4SrcIpMask = 0;
    UINT4               u4DestIpAddr = 0;
    UINT4               u4DestIpMask = 0;
    UINT4               u4MaxSrcPort = 65535;
    UINT4               u4MinSrcPort = 1;
    UINT4               u4MinDstPort = 1;
    UINT4               u4MaxDstPort = 65535;
    INT4                i4Tos = 0;
    UINT4               u4BitType = 0;
    INT4                i4Priority = 0;
    INT4                i4Action;
    INT4                i4Protocol = ISS_ANY;
    INT4                i4MsgType = ISS_ANY;
    INT4                i4MsgCode = ISS_ANY;
    INT4                i4FilterNo = 0;
    INT4                i4FilterType = 0;
    INT4                i4SVlan = 0;
    INT4                i4SVlanPrio = -1;
    INT4                i4CVlan = 0;
    INT4                i4CVlanPrio = -1;
    INT4                i4OuterEType = 0;
    INT4                i4EtherType = 0;
    INT4                i4TagType = 0;
    tMacAddr            SrcMacAddr;
    tMacAddr            DestMacAddr;
    UINT4               u4IsValidSrcPort;
    UINT4               u4IsValidDestPort;

    va_start (ap, u4Command);

    /* Third arguement is always interface name/index */
    va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguements and store in args array.  */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == CLI_ACL_MAX_ARGS)
            break;
    }

    va_end (ap);

    CLI_SET_ERR (0);

    CliRegisterLock (CliHandle, IssLock, IssUnLock);

    ISS_LOCK ();

    switch (u4Command)
    {
        case CLI_IP_ACL:
            i4RetStatus =
                AclCreateIPFilter (CliHandle, (UINT4) (args[0]),
                                   (INT4) (args[1]));
            break;

        case CLI_NO_IP_ACL:
            i4RetStatus = AclDestroyIPFilter (CliHandle, (INT4) (args[1]));
            break;

        case CLI_MAC_ACL:
            i4RetStatus = AclCreateMacFilter (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_NO_MAC_ACL:
            i4RetStatus = AclDestroyMacFilter (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_ACL_PERMIT:
        case CLI_ACL_DENY:
            /* Get the source IP,source mask, destination IP and destination mask
             */

            if ((UINT4 *) (args[1]) != NULL)
            {
                u4SrcIpAddr = *(UINT4 *) (args[1]);
            }
            if ((UINT4 *) (args[2]) != NULL)
            {
                u4SrcIpMask = *(UINT4 *) (args[2]);
            }
            if ((UINT4 *) (args[4]) != NULL)
            {
                u4DestIpAddr = *(UINT4 *) (args[4]);
            }
            if ((UINT4 *) (args[5]) != NULL)
            {
                u4DestIpMask = *(UINT4 *) (args[5]);
            }
            if (u4Command == CLI_ACL_DENY)
            {
                i4Action = ISS_DROP;
            }
            else
            {
                i4Action = ISS_ALLOW;
            }

            i4RetStatus =
                AclStdIpFilterConfig (CliHandle, i4Action, (UINT4) (args[0]),
                                      u4SrcIpAddr, u4SrcIpMask,
                                      (UINT4) (args[3]), u4DestIpAddr,
                                      u4DestIpMask);
            break;

        case CLI_ACL_PERMIT_PROTO:
        case CLI_ACL_DENY_PROTO:
        case CLI_ACL_PB_PERMIT_PROTO:
        case CLI_ACL_PB_DENY_PROTO:

            /* Get the source IP,source mask, destination IP and destination mask
             */
            if ((UINT4 *) (args[2]) != NULL)
            {
                u4SrcIpAddr = *(UINT4 *) (args[2]);
            }
            if ((UINT4 *) (args[3]) != NULL)
            {
                u4SrcIpMask = *(UINT4 *) (args[3]);
            }

            if ((UINT4 *) (args[5]) != NULL)
            {
                u4DestIpAddr = *(UINT4 *) (args[5]);
            }
            if ((UINT4 *) (args[6]) != NULL)
            {
                u4DestIpMask = *(UINT4 *) (args[6]);
            }

            /* Get the protocol value and priority values */

            if ((UINT4 *) (args[9]) != NULL)
            {
                i4Priority = (*(UINT4 *) (args[9]));
            }
            else
            {
                i4Priority = ISS_DEFAULT_FILTER_PRIORITY;
            }

            if ((u4Command == CLI_ACL_PB_PERMIT_PROTO) ||
                (u4Command == CLI_ACL_PB_DENY_PROTO))
            {
                if ((INT4 *) (args[10]) != NULL)
                {
                    i4SVlan = *(INT4 *) (args[10]);
                }
                if ((INT4 *) (args[11]) != NULL)
                {
                    i4SVlanPrio = *(INT4 *) (args[11]);
                }

                if ((INT4 *) (args[12]) != NULL)
                {
                    i4CVlan = *(INT4 *) (args[12]);
                }
                if ((INT4 *) (args[13]) != NULL)
                {
                    i4CVlanPrio = *(INT4 *) (args[13]);
                }
                i4TagType = (INT4) (args[14]);
            }

            if ((u4Command == CLI_ACL_DENY_PROTO) ||
                (u4Command == CLI_ACL_PB_DENY_PROTO))
            {
                i4Action = ISS_DROP;
            }
            else
            {
                i4Action = ISS_ALLOW;
            }
            i4RetStatus =
                AclExtIpFilterConfig
                (CliHandle, i4Action, (INT4) (args[0]), (UINT4) (args[1]),
                 u4SrcIpAddr, u4SrcIpMask, (UINT4) (args[4]),
                 u4DestIpAddr, u4DestIpMask, (INT4) (args[7]),
                 (INT4) (args[8]), i4Priority);

            if (i4RetStatus == CLI_SUCCESS)
            {
                i4RetStatus =
                    AclExtPbL3FilterConfig (CliHandle, i4SVlan,
                                            i4SVlanPrio, i4CVlan,
                                            i4CVlanPrio, i4TagType);
            }
            break;

        case CLI_ACL_PERMIT_TCP:
        case CLI_ACL_DENY_TCP:
        case CLI_ACL_PERMIT_UDP:
        case CLI_ACL_DENY_UDP:
        case CLI_ACL_PB_PERMIT_TCP:
        case CLI_ACL_PB_DENY_TCP:
        case CLI_ACL_PB_PERMIT_UDP:
        case CLI_ACL_PB_DENY_UDP:

            if ((u4Command == CLI_ACL_DENY_TCP) ||
                (u4Command == CLI_ACL_DENY_UDP) ||
                (u4Command == CLI_ACL_PB_DENY_TCP) ||
                (u4Command == CLI_ACL_PB_DENY_UDP))
            {
                i4Action = ISS_DROP;
            }
            else
            {
                i4Action = ISS_ALLOW;
            }
            /* Get the protocol type - This can be TCP or UDP only */

            if ((u4Command == CLI_ACL_PERMIT_TCP) ||
                (u4Command == CLI_ACL_DENY_TCP) ||
                (u4Command == CLI_ACL_PB_PERMIT_TCP) ||
                (u4Command == CLI_ACL_PB_DENY_TCP))
            {
                i4Protocol = ISS_PROT_TCP;
            }
            else
            {
                i4Protocol = ISS_PROT_UDP;
            }

            /* Get the source IP,source mask, destination IP and destination mask
             */

            if ((UINT4 *) (args[1]) != NULL)
            {
                u4SrcIpAddr = *(UINT4 *) (args[1]);
            }
            if ((UINT4 *) (args[2]) != NULL)
            {
                u4SrcIpMask = *(UINT4 *) (args[2]);
            }

            if ((UINT4 *) (args[7]) != NULL)
            {
                u4DestIpAddr = *(UINT4 *) (args[7]);
            }
            if ((UINT4 *) (args[8]) != NULL)
            {
                u4DestIpMask = *(UINT4 *) (args[8]);
            }

            u4IsValidSrcPort = FALSE;
            /* Processing for Source ports */

            if ((UINT4) (args[3]) == ACL_GREATER_THAN_PORT)
            {
                u4MinSrcPort = (*(UINT4 *) (args[4])) + 1;
                u4MaxSrcPort = ISS_MAX_PORT_VALUE;
                u4IsValidSrcPort = TRUE;
            }
            else if ((UINT4) (args[3]) == ACL_LESSER_THAN_PORT)
            {
                u4MinSrcPort = 1;
                u4MaxSrcPort = (*(UINT4 *) (args[4])) - 1;
                u4IsValidSrcPort = TRUE;
            }
            else if ((UINT4) (args[3]) == ACL_EQUAL_TO_PORT)
            {
                u4MinSrcPort = *(UINT4 *) (args[4]);
                u4MaxSrcPort = *(UINT4 *) (args[4]);
                u4IsValidSrcPort = TRUE;
            }
            else if ((UINT4) (args[3]) == ACL_RANGE_PORT)
            {
                u4MinSrcPort = *(UINT4 *) (args[4]);
                u4MaxSrcPort = *(UINT4 *) (args[5]);
                u4IsValidSrcPort = TRUE;
            }

            /* Processing for destination ports */

            u4IsValidDestPort = FALSE;

            if ((UINT4) (args[9]) == ACL_GREATER_THAN_PORT)
            {
                u4MinDstPort = (*(UINT4 *) (args[10])) + 1;
                u4MaxDstPort = ISS_MAX_PORT_VALUE;
                u4IsValidDestPort = TRUE;
            }
            else if ((UINT4) (args[9]) == ACL_LESSER_THAN_PORT)
            {
                u4MinDstPort = 1;
                u4MaxDstPort = (*(UINT4 *) (args[10])) - 1;
                u4IsValidDestPort = TRUE;
            }
            else if ((UINT4) (args[9]) == ACL_EQUAL_TO_PORT)
            {
                u4MinDstPort = *(UINT4 *) (args[10]);
                u4MaxDstPort = *(UINT4 *) (args[10]);
                u4IsValidDestPort = TRUE;
            }
            else if ((UINT4) (args[9]) == ACL_RANGE_PORT)
            {
                u4MinDstPort = *(UINT4 *) (args[10]);
                u4MaxDstPort = *(UINT4 *) (args[11]);
                u4IsValidDestPort = TRUE;
            }

            if ((u4IsValidSrcPort == TRUE) && (u4IsValidDestPort == TRUE))
            {

                CliUnRegisterLock (CliHandle);

                ISS_UNLOCK ();
                CliPrintf (CliHandle, "\r%% Cannot specify both source "
                           " and destination ports\r\n");
                return (CLI_FAILURE);
            }

            /* Processing for RST/ACK BIT */

            if ((UINT4 *) (args[12]) != NULL)
            {
                u4BitType = (UINT4) (args[12]);
            }

            i4Tos = (INT4) (args[13]);

            if ((UINT4 *) (args[15]) != NULL)
            {
                i4Priority = *(INT4 *) (args[15]);
            }
            else
            {
                i4Priority = ISS_DEFAULT_FILTER_PRIORITY;
            }

            if ((u4Command == CLI_ACL_PB_PERMIT_TCP) ||
                (u4Command == CLI_ACL_PB_DENY_TCP) ||
                (u4Command == CLI_ACL_PB_PERMIT_UDP) ||
                (u4Command == CLI_ACL_PB_DENY_UDP))
            {
                if ((INT4 *) (args[16]) != NULL)
                {
                    i4SVlan = *(INT4 *) (args[16]);
                }
                if ((INT4 *) (args[17]) != NULL)
                {
                    i4SVlanPrio = *(INT4 *) (args[17]);
                }

                if ((INT4 *) (args[18]) != NULL)
                {
                    i4CVlan = *(INT4 *) (args[18]);
                }
                if ((INT4 *) (args[19]) != NULL)
                {
                    i4CVlanPrio = *(INT4 *) (args[19]);
                }
                i4TagType = (INT4) (args[20]);
            }
            i4RetStatus = AclExtIpFilterTcpUdpConfig
                (CliHandle, i4Action, i4Protocol,
                 (UINT4) (args[0]), u4SrcIpAddr,
                 u4SrcIpMask, u4IsValidSrcPort,
                 u4MinSrcPort, u4MaxSrcPort,
                 (UINT4) (args[6]), u4DestIpAddr,
                 u4DestIpMask, u4IsValidDestPort,
                 u4MinDstPort, u4MaxDstPort, u4BitType, i4Tos,
                 (INT4) (args[14]), i4Priority);
            if (i4RetStatus == CLI_SUCCESS)
            {
                i4RetStatus =
                    AclExtPbL3FilterConfig (CliHandle, i4SVlan,
                                            i4SVlanPrio, i4CVlan,
                                            i4CVlanPrio, i4TagType);

            }
            break;

        case CLI_ACL_PERMIT_ICMP:
        case CLI_ACL_DENY_ICMP:
        case CLI_ACL_PB_PERMIT_ICMP:
        case CLI_ACL_PB_DENY_ICMP:

            /* Get the source IP,source mask, destination IP and destination mask
             */
            if ((UINT4 *) (args[1]) != NULL)
            {
                u4SrcIpAddr = *(UINT4 *) (args[1]);
            }
            if ((UINT4 *) (args[2]) != NULL)
            {
                u4SrcIpMask = *(UINT4 *) (args[2]);
            }

            if ((UINT4 *) (args[4]) != NULL)
            {
                u4DestIpAddr = *(UINT4 *) (args[4]);
            }
            if ((UINT4 *) (args[5]) != NULL)
            {
                u4DestIpMask = *(UINT4 *) (args[5]);
            }

            if ((u4Command == CLI_ACL_DENY_ICMP) ||
                (u4Command == CLI_ACL_PB_DENY_ICMP))
            {
                i4Action = ISS_DROP;
            }
            else
            {
                i4Action = ISS_ALLOW;
            }

            /* Get the ICMP Message Type and code */

            if ((UINT4 *) (args[6]) != NULL)
            {
                i4MsgType = *(UINT4 *) (args[6]);
            }
            if ((UINT4 *) (args[7]) != NULL)
            {
                i4MsgCode = *(UINT4 *) (args[7]);
            }

            if ((INT4 *) (args[8]) != NULL)
            {
                i4Priority = *(INT4 *) (args[8]);
            }
            else
            {
                i4Priority = ISS_DEFAULT_FILTER_PRIORITY;
            }
            if ((u4Command == CLI_ACL_PB_PERMIT_ICMP) ||
                (u4Command == CLI_ACL_PB_DENY_ICMP))
            {
                if ((INT4 *) (args[9]) != NULL)
                {
                    i4SVlan = *(INT4 *) (args[9]);
                }
                if ((INT4 *) (args[10]) != NULL)
                {
                    i4SVlanPrio = *(INT4 *) (args[10]);
                }

                if ((INT4 *) (args[11]) != NULL)
                {
                    i4CVlan = *(INT4 *) (args[11]);
                }
                if ((INT4 *) (args[12]) != NULL)
                {
                    i4CVlanPrio = *(INT4 *) (args[12]);
                }
                i4TagType = (INT4) (args[13]);
            }
            i4RetStatus =
                AclExtIpFilterIcmpConfig
                (CliHandle, i4Action,
                 (UINT4) (args[0]), u4SrcIpAddr, u4SrcIpMask,
                 (UINT4) (args[3]), u4DestIpAddr, u4DestIpMask,
                 i4MsgType, i4MsgCode, i4Priority);

            if (i4RetStatus == CLI_SUCCESS)
            {
                i4RetStatus =
                    AclExtPbL3FilterConfig (CliHandle, i4SVlan,
                                            i4SVlanPrio, i4CVlan,
                                            i4CVlanPrio, i4TagType);
            }
            break;

        case CLI_PERMIT_MAC_ACL:
        case CLI_DENY_MAC_ACL:
        case CLI_PB_PERMIT_MAC_ACL:
        case CLI_PB_DENY_MAC_ACL:

            if ((u4Command == CLI_PERMIT_MAC_ACL) ||
                (u4Command == CLI_PB_PERMIT_MAC_ACL))
            {
                i4Action = ISS_ALLOW;
            }
            else
            {
                i4Action = ISS_DROP;
            }

            /* Get the Source and destination MAC address */

            if ((UINT1 *) (args[1]) != NULL)
            {
                StrToMac (args[1], SrcMacAddr);
            }
            if ((UINT1 *) (args[3]) != NULL)
            {
                StrToMac (args[3], DestMacAddr);
            }

            if ((INT4 *) (args[5]) != NULL)
            {
                i4EtherType = *(INT4 *) (args[5]);
            }

            if ((args[6]) != 0)
            {
                i4CVlan = (INT4) (args[6]);
            }

            if ((INT4 *) (args[7]) != NULL)
            {
                i4Priority = *(INT4 *) (args[7]);
            }
            else
            {
                i4Priority = ISS_DEFAULT_FILTER_PRIORITY;
            }

            if ((u4Command == CLI_PB_PERMIT_MAC_ACL) ||
                (u4Command == CLI_PB_DENY_MAC_ACL))
            {
                if ((INT4 *) (args[8]) != NULL)
                {
                    i4OuterEType = *(INT4 *) (args[8]);
                }
                if ((INT4 *) (args[9]) != NULL)
                {
                    i4SVlan = *(INT4 *) (args[9]);
                }
                if ((INT4 *) (args[10]) != NULL)
                {
                    i4CVlanPrio = *(INT4 *) (args[10]);
                }
                if ((INT4 *) (args[11]) != NULL)
                {
                    i4SVlanPrio = *(INT4 *) (args[11]);
                }
                i4TagType = (INT4) (args[12]);
            }
            i4RetStatus =
                AclExtMacFilterConfig (CliHandle, i4Action, (UINT4) (args[0]),
                                       SrcMacAddr, (UINT4) (args[2]),
                                       DestMacAddr, (INT4) (args[4]),
                                       i4EtherType, i4CVlan, i4Priority);

            if (i4RetStatus == CLI_SUCCESS)
            {
                i4RetStatus =
                    AclExtPbL2FilterConfig (CliHandle, i4OuterEType,
                                            i4SVlan, i4SVlanPrio,
                                            i4CVlanPrio, i4TagType);

            }
            break;

        case CLI_ACL_IP_ACCESS_GRP:

            i4RetStatus =
                AclIpAccessGroup (CliHandle, *(INT4 *) (args[0]),
                                  (INT4) (args[1]));
            break;

        case CLI_ACL_IP_NO_ACCESS_GRP:

            if ((INT4 *) (args[0]) != NULL)
            {
                i4FilterNo = (*(INT4 *) (args[0]));
            }
            i4RetStatus =
                AclNoIpAccessGroup (CliHandle, i4FilterNo, (INT4) (args[1]));
            break;

        case CLI_ACL_MAC_ACCESS_GRP:

            i4RetStatus = AclMacAccessGroup (CliHandle, *(INT4 *) (args[0]));
            break;

        case CLI_ACL_MAC_NO_ACCESS_GRP:

            if ((INT4 *) (args[0]) != NULL)
            {
                i4FilterNo = (*(INT4 *) (args[0]));
            }
            i4RetStatus = AclNoMacAccessGroup (CliHandle, i4FilterNo);
            break;

        case CLI_ACL_SHOW:
            i4FilterType = (INT4) (args[0]);
            if ((INT4 *) (args[1]) != NULL)
            {
                i4FilterNo = (*(INT4 *) (args[1]));
            }
            i4RetStatus =
                AclShowAccessLists (CliHandle, i4FilterType, i4FilterNo);
            break;

    }

    if ((CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_ACL_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", AclCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);

    ISS_UNLOCK ();

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclCreateIPFilter                                  */
/*                                                                           */
/*     DESCRIPTION      : This function creates an IP ACL filter and enters  */
/*                        the corresponding configuration mode               */
/*                                                                           */
/*     INPUT            : u4Type  - Standard /Extended ACL                   */
/*                        i4Filterno - IP ACL number                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclCreateIPFilter (tCliHandle CliHandle, UINT4 u4Type, INT4 i4FilterNo)
{
    INT4                i4Status = 0;
    UINT4               u4ErrCode;
    UINT1               au1IfName[ACL_MAX_NAME_LENGTH];

    /* if filter already exists, do nothing and enter the configuration mode */

    if (nmhGetIssExtL3FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        /* Create a filter */
        if (nmhTestv2IssExtL3FilterStatus (&u4ErrCode,
                                           i4FilterNo,
                                           ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

    }

    /* Enter IP ACL configuration mode */

    if (u4Type == ACL_STANDARD)
    {
        /* Set the mode information for CLI */
        CLI_SET_STDACLID (i4FilterNo);
        SNPRINTF ((CHR1 *) au1IfName, ACL_MAX_NAME_LENGTH,
                  "%s%d", CLI_STDACL_MODE, i4FilterNo);
    }

    else

    {
        /* Set the mode information for CLI */
        CLI_SET_EXTACL (i4FilterNo);
        SNPRINTF ((CHR1 *) au1IfName, ACL_MAX_NAME_LENGTH,
                  "%s%d", CLI_EXTACL_MODE, i4FilterNo);
    }

    /* Return ACL configuration prompt */
    CliChangePath ((CHR1 *) au1IfName);

    UNUSED_PARAM (CliHandle);
    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclDestroyIPFilter                                 */
/*                                                                           */
/*     DESCRIPTION      : This function destroys a IP ACL filter and enters  */
/*                        the corresponding configuration mode               */
/*                                                                           */
/*     INPUT            : i4Filterno - IP ACL number                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclDestroyIPFilter (tCliHandle CliHandle, INT4 i4FilterNo)
{
    INT4                i4Status;
    UINT4               u4ErrCode;

    if (nmhGetIssExtL3FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        /* Filter does not exist */
        CliPrintf (CliHandle, "\r%% Invalid Filter number \r\n");
        return (CLI_FAILURE);
    }

    else

    {
        if (nmhTestv2IssExtL3FilterStatus (&u4ErrCode,
                                           i4FilterNo,
                                           ISS_DESTROY) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclCreateMacFilter                                 */
/*                                                                           */
/*     DESCRIPTION      : This function creates an MAC ACL filter and enters */
/*                        the corresponding configuration mode               */
/*                                                                           */
/*     INPUT            : i4Filterno - MAC ACL number                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclCreateMacFilter (tCliHandle CliHandle, INT4 i4FilterNo)
{
    INT4                i4Status;
    UINT4               u4ErrCode;
    UINT1               au1IfName[ACL_MAX_NAME_LENGTH];

    /* if filter already exists, do nothing and enter the configuration mode */

    if (nmhGetIssExtL2FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        /* Create a filter */
        if (nmhTestv2IssExtL2FilterStatus (&u4ErrCode,
                                           i4FilterNo,
                                           ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

    }

    /* Enter MAC ACL configuration mode */

    /* Set the mode information for CLI */
    CLI_SET_MACACL (i4FilterNo);
    SNPRINTF ((CHR1 *) au1IfName, ACL_MAX_NAME_LENGTH,
              "%s%d", CLI_MACACL_MODE, i4FilterNo);
    /* Return ACL configuration prompt */
    CliChangePath ((CHR1 *) au1IfName);

    UNUSED_PARAM (CliHandle);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclDestroyMacFilter                                */
/*                                                                           */
/*     DESCRIPTION      : This function destroys a MAC ACL filter and enters */
/*                        the corresponding configuration mode               */
/*                                                                           */
/*     INPUT            : i4Filterno - MAC ACL number                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclDestroyMacFilter (tCliHandle CliHandle, INT4 i4FilterNo)
{
    INT4                i4Status;
    UINT4               u4ErrCode;

    if (nmhGetIssExtL2FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        /* Filter does not exist */
        CliPrintf (CliHandle, "\r%% Invalid Filter number \r\n");
        return (CLI_FAILURE);
    }

    else

    {
        if (nmhTestv2IssExtL2FilterStatus (&u4ErrCode,
                                           i4FilterNo,
                                           ISS_DESTROY) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclStdIpFilterConfig                               */
/*                                                                           */
/*     DESCRIPTION      : This function configures a standard IP ACL filter  */
/*                         and enters the corresponding configuration mode   */
/*                                                                           */
/*     INPUT            :  u4Action -Permit/Deny                             */
/*                         u4SrcType - any/source network/ source host       */
/*                         u4SrcIp - Source IP Address                       */
/*                         u4SrcMask - Source IP Mask                        */
/*                         u4DestType - any/dest network/dest host           */
/*                         u4DestIp - Dest IP Address                        */
/*                         u4DestMask - Dest IP Mask                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/

INT4
AclStdIpFilterConfig (tCliHandle CliHandle, INT4 i4Action, UINT4 u4SrcType,
                      UINT4 u4SrcIpAddr, UINT4 u4SrcMask, UINT4 u4DestType,
                      UINT4 u4DestIpAddr, UINT4 u4DestMask)
{
    INT4                i4FilterNo;
    INT4                i4Status;
    UINT4               u4ErrCode;

    /*Get the IP Access list number from the current mode in CLI */
    i4FilterNo = CLI_GET_STDACLID ();
    if (nmhGetIssExtL3FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_ACL_NO_FILTER);
        return (CLI_FAILURE);
    }

    if (i4Status != ISS_NOT_READY)
    {
        /* Filter has previously been configured. This must be over-written 
         * First destroy the filter and create again 
         */

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

    }

    if (nmhTestv2IssExtL3FilterAction (&u4ErrCode,
                                       i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /*Test the user input for destination IP and mask */
    if (AclTestIpParams (ACL_SRC, i4FilterNo, u4SrcType, u4SrcIpAddr, u4SrcMask)
        == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /*Test the user input for destination IP and mask */
    if (AclTestIpParams
        (ACL_DST, i4FilterNo, u4DestType, u4DestIpAddr,
         u4DestMask) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (AclSetIpParams (ACL_SRC, i4FilterNo, u4SrcType, u4SrcIpAddr, u4SrcMask)
        == CLI_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (AclSetIpParams
        (ACL_DST, i4FilterNo, u4DestType, u4DestIpAddr,
         u4DestMask) == CLI_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtIpFilterConfig                               */
/*                                                                           */
/*     DESCRIPTION      : This function configures a extended IP ACL filter  */
/*                         and enters the corresponding configuration mode   */
/*                                                                           */
/*     INPUT            :  i4Action -Permit/Deny                             */
/*                         i4Protocol - Protocol value                       */
/*                         u4SrcType - any/source network/ source host       */
/*                         u4SrcIp - Source IP Address                       */
/*                         u4SrcMask - Source IP Mask                        */
/*                         u4DestType - any/dest network/dest host           */
/*                         u4DestIp - Dest IP Address                        */
/*                         u4DestMask - Dest IP Mask                         */
/*                         i4Priority - Filter priority                      */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclExtIpFilterConfig (tCliHandle CliHandle, INT4 i4Action,
                      INT4 i4Protocol, UINT4 u4SrcType,
                      UINT4 u4SrcIpAddr, UINT4 u4SrcMask,
                      UINT4 u4DestType, UINT4 u4DestIpAddr,
                      UINT4 u4DestMask, INT4 i4Tos, INT4 i4Dscp,
                      INT4 i4Priority)
{
    INT4                i4FilterNo;
    INT4                i4Status;
    UINT4               u4ErrCode;

    /*Get the IP Access list number from the current mode in CLI */
    i4FilterNo = CLI_GET_EXTACL ();
    nmhGetIssExtL3FilterStatus (i4FilterNo, &i4Status);

    if (i4Status != ISS_NOT_READY)
    {
        /* Filter has previously been configured. This must be over-written 
         * First destroy the filter and create again */

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

    }
    if (nmhTestv2IssExtL3FilterAction (&u4ErrCode,
                                       i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssExtL3FilterProtocol (&u4ErrCode,
                                         i4FilterNo,
                                         i4Protocol) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /*Test the user input for source IP, mask */
    if (AclTestIpParams (ACL_SRC, i4FilterNo, u4SrcType, u4SrcIpAddr, u4SrcMask)
        == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /*Test the user input for destination IP, mask */
    if (AclTestIpParams
        (ACL_DST, i4FilterNo, u4DestType, u4DestIpAddr,
         u4DestMask) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (i4Tos != ISS_TOS_INVALID)
    {
        if (nmhTestv2IssExtL3FilterTos (&u4ErrCode, i4FilterNo, i4Tos)
            == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (i4Dscp != ISS_DSCP_INVALID)
    {
        if (nmhTestv2IssExtL3FilterDscp (&u4ErrCode, i4FilterNo, i4Dscp)
            == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (nmhTestv2IssExtL3FilterPriority (&u4ErrCode, i4FilterNo, i4Priority)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterProtocol (i4FilterNo, i4Protocol) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (AclSetIpParams (ACL_SRC, i4FilterNo, u4SrcType, u4SrcIpAddr, u4SrcMask)
        == CLI_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (AclSetIpParams
        (ACL_DST, i4FilterNo, u4DestType, u4DestIpAddr,
         u4DestMask) == CLI_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (i4Tos != ISS_TOS_INVALID)
    {
        if (nmhSetIssExtL3FilterTos (i4FilterNo, i4Tos) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4Dscp != ISS_DSCP_INVALID)
    {
        if (nmhSetIssExtL3FilterDscp (i4FilterNo, i4Dscp) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (nmhSetIssExtL3FilterPriority (i4FilterNo, i4Priority) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtIpFilterTcpUdpConfig                         */
/*                                                                           */
/*     DESCRIPTION      : This function configures a extended IP ACL filter  */
/*                         and enters the corresponding configuration mode   */
/*                                                                           */
/*     INPUT            :  i4Action -Permit/Deny                             */
/*                         i4Protocol - Protocol value                       */
/*                         u4SrcType - any/source network/ source host       */
/*                         u4SrcIp - Source IP Address                       */
/*                         u4SrcMask - Source IP Mask                        */
/*                         u4DestType - any/dest network/dest host           */
/*                         u4DestIp - Dest IP Address                        */
/*                         u4DestMask - Dest IP Mask                         */
/*                         i4Priority - Filter priority                      */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclExtIpFilterTcpUdpConfig (tCliHandle CliHandle, INT4 i4Action,
                            INT4 i4Protocol, UINT4 u4SrcType, UINT4 u4SrcIpAddr,
                            UINT4 u4SrcMask, UINT4 u4SrcPortFlag,
                            UINT4 u4SrcMinPort, UINT4 u4SrcMaxPort,
                            UINT4 u4DestType, UINT4 u4DestIpAddr,
                            UINT4 u4DestMask, UINT4 u4DestPortFlag,
                            UINT4 u4DestMinPort, UINT4 u4DestMaxPort,
                            UINT4 u4BitType, INT4 i4Tos, INT4 i4Dscp,
                            INT4 i4Priority)
{
    INT4                i4FilterNo;
    INT4                i4Status;
    UINT4               u4ErrCode;

    /*Get the IP Access list number from the current mode in CLI */
    i4FilterNo = CLI_GET_EXTACL ();

    if (nmhGetIssExtL3FilterStatus (i4FilterNo, &i4Status) == SNMP_FAILURE)
    {
        /* Filter does not exist */
        CliPrintf (CliHandle, "\r%% Invalid Filter number \r\n");
        return (CLI_FAILURE);
    }

    if (i4Status != ISS_NOT_READY)
    {
        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

    }
    if (nmhTestv2IssExtL3FilterAction (&u4ErrCode,
                                       i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssExtL3FilterProtocol (&u4ErrCode,
                                         i4FilterNo,
                                         i4Protocol) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /*Test the user input for source IP, mask */
    if (AclTestIpParams (ACL_SRC, i4FilterNo, u4SrcType, u4SrcIpAddr, u4SrcMask)
        == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (u4SrcPortFlag == TRUE)
    {
        if (nmhTestv2IssExtL3FilterMinSrcProtPort
            (&u4ErrCode, i4FilterNo, u4SrcMinPort) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhTestv2IssExtL3FilterMaxSrcProtPort
            (&u4ErrCode, i4FilterNo, u4SrcMaxPort) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    /*Test the user input for destination IP, mask */
    if (AclTestIpParams
        (ACL_DST, i4FilterNo, u4DestType, u4DestIpAddr,
         u4DestMask) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (u4DestPortFlag == TRUE)
    {
        if (nmhTestv2IssExtL3FilterMinDstProtPort
            (&u4ErrCode, i4FilterNo, u4DestMinPort) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhTestv2IssExtL3FilterMaxDstProtPort
            (&u4ErrCode, i4FilterNo, u4DestMinPort) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (u4BitType == ACL_ACK)
    {
        /* filter TCP ACK bits */
        if (nmhTestv2IssExtL3FilterAckBit (&u4ErrCode, i4FilterNo,
                                           ISS_ACK_ESTABLISH) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }
    else if (u4BitType == ACL_RST)
    {
        /* filter TCP RST bits */
        if (nmhTestv2IssExtL3FilterRstBit (&u4ErrCode, i4FilterNo, ISS_RST_SET)
            == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (i4Tos != ISS_TOS_INVALID)
    {
        if (nmhTestv2IssExtL3FilterTos (&u4ErrCode, i4FilterNo, i4Tos)
            == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }
    if (i4Dscp != ISS_DSCP_INVALID)
    {
        if (nmhTestv2IssExtL3FilterDscp (&u4ErrCode, i4FilterNo, i4Dscp)
            == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (nmhTestv2IssExtL3FilterPriority (&u4ErrCode, i4FilterNo, i4Priority)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterProtocol (i4FilterNo, i4Protocol) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (AclSetIpParams (ACL_SRC, i4FilterNo, u4SrcType, u4SrcIpAddr, u4SrcMask)
        == CLI_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (u4SrcPortFlag == TRUE)
    {
        if (nmhSetIssExtL3FilterMinSrcProtPort (i4FilterNo, u4SrcMinPort)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        if (nmhSetIssExtL3FilterMaxSrcProtPort (i4FilterNo, u4SrcMaxPort)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (AclSetIpParams
        (ACL_DST, i4FilterNo, u4DestType, u4DestIpAddr,
         u4DestMask) == CLI_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (u4DestPortFlag == TRUE)
    {
        if (nmhSetIssExtL3FilterMinDstProtPort (i4FilterNo, u4DestMinPort)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        if (nmhSetIssExtL3FilterMaxDstProtPort (i4FilterNo, u4DestMaxPort)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4Tos != ISS_TOS_INVALID)
    {
        if (nmhSetIssExtL3FilterTos (i4FilterNo, i4Tos) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4Dscp != ISS_DSCP_INVALID)
    {
        if (nmhSetIssExtL3FilterDscp (i4FilterNo, i4Dscp) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (nmhSetIssExtL3FilterPriority (i4FilterNo, i4Priority) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (u4BitType == ACL_ACK)
    {
        /* Filter TCP ACK bits */
        if (nmhSetIssExtL3FilterAckBit (i4FilterNo, ISS_ACK_ESTABLISH) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    else if (u4BitType == ACL_RST)
    {
        /* Filter TCP RST bits */
        if (nmhSetIssExtL3FilterRstBit (i4FilterNo, ISS_RST_SET) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtIpFilterIcmpConfig                           */
/*                                                                           */
/*     DESCRIPTION      : This function configures a extended IP ACL filter  */
/*                         and enters the corresponding configuration mode   */
/*                                                                           */
/*     INPUT            :  i4Action -Permit/Deny                             */
/*                         u4SrcType - any/source network/ source host       */
/*                         u4SrcIp - Source IP Address                       */
/*                         u4SrcMask - Source IP Mask                        */
/*                         u4DestType - any/dest network/dest host           */
/*                         u4DestIp - Dest IP Address                        */
/*                         u4DestMask - Dest IP Mask                         */
/*                         u4MesageType, u4MessageCode - ICMP details        */
/*                         i4Priority - Filter priority                      */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclExtIpFilterIcmpConfig (tCliHandle CliHandle, INT4 i4Action,
                          UINT4 u4SrcType, UINT4 u4SrcIpAddr, UINT4 u4SrcMask,
                          UINT4 u4DestType, UINT4 u4DestIpAddr,
                          UINT4 u4DestMask, INT4 i4MessageType,
                          INT4 i4MessageCode, INT4 i4Priority)
{
    INT4                i4FilterNo;
    INT4                i4Status;
    UINT4               u4ErrCode;

    /*Get the IP Access list number from the current mode in CLI */
    i4FilterNo = CLI_GET_EXTACL ();

    nmhGetIssExtL3FilterStatus (i4FilterNo, &i4Status);

    if (i4Status != ISS_NOT_READY)
    {
        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

    }

    if (nmhTestv2IssExtL3FilterAction (&u4ErrCode,
                                       i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /*Test the user input for source IP, mask */

    if (AclTestIpParams (ACL_SRC, i4FilterNo, u4SrcType, u4SrcIpAddr, u4SrcMask)
        == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    /*Test the user input for destination IP, mask */
    if (AclTestIpParams
        (ACL_DST, i4FilterNo, u4DestType, u4DestIpAddr,
         u4DestMask) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssExtL3FilterProtocol (&u4ErrCode,
                                         i4FilterNo,
                                         ISS_PROT_ICMP) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssExtL3FilterMessageType
        (&u4ErrCode, i4FilterNo, i4MessageType) == SNMP_FAILURE)

    {
        /* ICMP Message Type */
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssExtL3FilterMessageCode
        (&u4ErrCode, i4FilterNo, i4MessageCode) == SNMP_FAILURE)
    {
        /* ICMP Message Code */
        return (CLI_FAILURE);
    }

    /* Filter Priority */
    if (nmhTestv2IssExtL3FilterPriority (&u4ErrCode, i4FilterNo, i4Priority)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (AclSetIpParams (ACL_SRC, i4FilterNo, u4SrcType, u4SrcIpAddr, u4SrcMask)
        == CLI_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (AclSetIpParams
        (ACL_DST, i4FilterNo, u4DestType, u4DestIpAddr,
         u4DestMask) == CLI_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterProtocol (i4FilterNo, ISS_PROT_ICMP) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (nmhSetIssExtL3FilterMessageType (i4FilterNo, i4MessageType)
        == SNMP_FAILURE)
    {
        /* ICMP Message Type */
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterMessageCode (i4FilterNo, i4MessageCode)
        == SNMP_FAILURE)
    {
        /* ICMP Message Code */
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL3FilterPriority (i4FilterNo, i4Priority) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclIpAccessGroup                                   */
/*                                                                           */
/*     DESCRIPTION      : This function associated an IP acl to an interface */
/*                                                                           */
/*     INPUT            : i4FilterNo - IP ACL number                         */
/*                        i4Direction - In or out                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclIpAccessGroup (tCliHandle CliHandle, INT4 i4FilterNo, INT4 i4Direction)
{
    UINT4               u4IfIndex;
    UINT1               au1PortList[ISS_PORTLIST_LEN];
    UINT1               au1OldPortList[ISS_PORTLIST_LEN];
    tSNMP_OCTET_STRING_TYPE PortList;
    tSNMP_OCTET_STRING_TYPE OldPortList;
    UINT4               u4ErrCode;
    INT4                i4Status;

    MEMSET (au1PortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1OldPortList, 0, ISS_PORTLIST_LEN);

    PortList.i4_Length = ISS_PORTLIST_LEN;
    PortList.pu1_OctetList = &au1PortList[0];

    OldPortList.i4_Length = ISS_PORTLIST_LEN;
    OldPortList.pu1_OctetList = &au1OldPortList[0];

    /*Get the interface index from the current mode in CLI */
    u4IfIndex = CLI_GET_IFINDEX ();

    if ((nmhGetIssExtL3FilterStatus (i4FilterNo, &i4Status)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid IP Filter \r\n");
        return (CLI_FAILURE);
    }

    /* Based on the direction we need to set the OutPortlist or the InPortlist
     * The status of the filter entry is set to NOT_IN_SERVICE and the port-list
     * should be updated.
     */

    if (i4Direction == ACL_ACCESS_IN)

    {
        nmhGetIssExtL3FilterInPortList (i4FilterNo, &PortList);
    }
    else
    {
        nmhGetIssExtL3FilterOutPortList (i4FilterNo, &PortList);
    }
    MEMCPY (OldPortList.pu1_OctetList, PortList.pu1_OctetList,
            PortList.i4_Length);
    OldPortList.i4_Length = PortList.i4_Length;

    ACL_ADD_PORT_LIST (au1PortList, u4IfIndex);

    if (nmhTestv2IssExtL3FilterStatus
        (&u4ErrCode, i4FilterNo, ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (i4Direction == ACL_ACCESS_IN)
    {
        if (nmhTestv2IssExtL3FilterInPortList
            (&u4ErrCode, i4FilterNo, &PortList) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    else

    {
        if (nmhTestv2IssExtL3FilterOutPortList
            (&u4ErrCode, i4FilterNo, &PortList) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (i4Status == ISS_ACTIVE)
    {
        if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (i4Direction == ACL_ACCESS_IN)
    {
        if (nmhSetIssExtL3FilterInPortList (i4FilterNo, &PortList) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else
    {
        if (nmhSetIssExtL3FilterOutPortList (i4FilterNo, &PortList)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* Activate the filter entry after configuring all the necessary parameters */

    if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        if (i4Direction == ACL_ACCESS_IN)
        {
            nmhSetIssExtL3FilterInPortList (i4FilterNo, &OldPortList);
        }
        else
        {
            nmhSetIssExtL3FilterOutPortList (i4FilterNo, &OldPortList);
        }
        nmhSetIssExtL3FilterStatus (i4FilterNo, i4Status);
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        return (CLI_FAILURE);

    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
    /*                                                                           */
/*     FUNCTION NAME    : AclNoIpAccessGroup                                 */
/*                                                                           */
/*     DESCRIPTION      : This function remove an IP acls from  an interface */
/*                                                                           */
/*     INPUT            : i4FilterNo - IP ACL number                         */
/*                        i4Direction - In or out                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclNoIpAccessGroup (tCliHandle CliHandle, INT4 i4FilterNo, INT4 i4Direction)
{
    tSNMP_OCTET_STRING_TYPE PortList;
    UINT1               au1PortList[ISS_PORTLIST_LEN];
    UINT1               au1NullPortList[ISS_PORTLIST_LEN];
    UINT4               u4IfIndex;
    INT4                i4CurrentFilter = 0;
    UINT1               u1Flag = 1;
    UINT4               u4ErrCode;

    MEMSET (au1PortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1NullPortList, 0, ISS_PORTLIST_LEN);

    PortList.i4_Length = ISS_PORTLIST_LEN;
    PortList.pu1_OctetList = &au1PortList[0];

    /*Get the interface index from the current mode in CLI */
    u4IfIndex = CLI_GET_IFINDEX ();

    if (i4FilterNo == 0)
    {
        /* The boolean u1Flag is used to specify whether the operation 
         * needs to be performed for all filters or for specific filter
         * only.
         */

        u1Flag = 0;

        if (nmhGetFirstIndexIssExtL3FilterTable (&i4FilterNo) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n No IP ACLs have been configured\r\n");
            return (CLI_SUCCESS);
        }
    }

    do
    {
        if (i4Direction == ACL_ACCESS_IN)
        {
            nmhGetIssExtL3FilterInPortList (i4FilterNo, &PortList);
        }

        else
        {
            nmhGetIssExtL3FilterOutPortList (i4FilterNo, &PortList);
        }

        if (CliIsMemberPort (PortList.pu1_OctetList,
                             ISS_PORTLIST_LEN, u4IfIndex) != CLI_SUCCESS)

        {
            if (u1Flag == 1)
            {

                CliPrintf (CliHandle,
                           "\r%% This filter has not been configured for this "
                           "interface or for this direction in this interface"
                           "\r\n");
                return (CLI_SUCCESS);
            }

        }
        else
        {

            ACL_DEL_PORT_LIST (au1PortList, u4IfIndex);

            /* Filter exists for this interface */

            if (nmhTestv2IssExtL3FilterStatus
                (&u4ErrCode, i4FilterNo, ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (i4Direction == ACL_ACCESS_IN)
            {
                if (nmhTestv2IssExtL3FilterInPortList (&u4ErrCode, i4FilterNo,
                                                       &PortList) ==
                    SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
            }
            else
            {
                if (nmhTestv2IssExtL3FilterOutPortList (&u4ErrCode, i4FilterNo,
                                                        &PortList) ==
                    SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
            }

            if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (i4Direction == ACL_ACCESS_IN)
            {
                if (nmhSetIssExtL3FilterInPortList (i4FilterNo, &PortList)
                    == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }
            }
            else
            {
                if (nmhSetIssExtL3FilterOutPortList (i4FilterNo, &PortList)
                    == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return (CLI_FAILURE);
                }
            }

            /* If there are any ports in the port list,
             * then set the filter to ACTIVE state   */

            if ((MEMCMP (au1PortList, au1NullPortList, ISS_PORTLIST_LEN)) != 0)
            {
                if (nmhSetIssExtL3FilterStatus (i4FilterNo, ISS_ACTIVE) ==
                    SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
            }
        }

        i4CurrentFilter = i4FilterNo;

    }
    while ((nmhGetNextIndexIssExtL3FilterTable (i4CurrentFilter, &i4FilterNo) ==
            SNMP_SUCCESS) && (u1Flag == 0));

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtPbL3FilterConfig                             */
/*                                                                           */
/*     DESCRIPTION      : This function configures a extended IP ACL filter  */
/*                                                                           */
/*     INPUT            :  i4SVlan  - Service vlan Id                        */
/*                         i4SVlanPrio - Service Vlan Priority               */
/*                         i4CVlan - Customer Vlan Id                        */
/*                         i4CVlanPrio - Customer Vlan Priority              */
/*                         i4TagType - Packet Tag type on which the filter   */
/*                                     will be applied                       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclExtPbL3FilterConfig (tCliHandle CliHandle, INT4 i4SVlan,
                        INT4 i4SVlanPrio, INT4 i4CVlan,
                        INT4 i4CVlanPrio, INT4 i4TagType)
{

    INT4                i4FilterNo;
    UINT4               u4ErrCode;

    i4FilterNo = CLI_GET_EXTACL ();

    if (AclPbTestL3Filter (&u4ErrCode, CliHandle, i4FilterNo, i4SVlan,
                           i4SVlanPrio, i4CVlan, i4CVlanPrio) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (AclPbSetL3Filter (CliHandle, i4FilterNo, i4SVlan, i4SVlanPrio, i4CVlan,
                          i4CVlanPrio) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (i4TagType != ISS_ZERO_ENTRY)
    {
        if (AclPbTestSetL3TagType (&u4ErrCode, CliHandle, i4FilterNo,
                                   i4TagType) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtMacFilterConfig                              */
/*                                                                           */
/*     DESCRIPTION      : This function to configure MAC acls                */
/*                                                                           */
/*     INPUT            : u4SrcType - ANY/ Source HOST MAC                   */
/*                        SrcMacAddr - Source Mac Address                    */
/*                        u4DestType - ANY/ Dest Source MAC                  */
/*                        i4Protocol - Protocol value                        */
/*                        i4EtherType - Ethernet type                        */
/*                        u4VlanId - Vlan Id                                 */
/*                        i4Priority - Priority value                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclExtMacFilterConfig (tCliHandle CliHandle, INT4 i4Action,
                       UINT4 u4SrcType, tMacAddr SrcMacAddr,
                       UINT4 u4DestType, tMacAddr DestMacAddr,
                       INT4 i4Protocol, INT4 i4EtherType, UINT4 u4VlanId,
                       INT4 i4Priority)
{
    UINT4               u4ErrCode = 0;
    INT4                i4FilterNo;
    INT4                i4Status;

    /*Get the MAC access list number from the current mode in CLI */
    i4FilterNo = CLI_GET_MACACL ();

    nmhGetIssExtL2FilterStatus (i4FilterNo, &i4Status);

    if (i4Status != ISS_NOT_READY)
    {
        /* Filter has previously been configured, it must be overwriten
         */
        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_DESTROY) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

    }
    if (nmhTestv2IssExtL2FilterAction (&u4ErrCode,
                                       i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (u4SrcType != ACL_ANY)
    {
        /* Source MAC has been specified */

        if (nmhTestv2IssExtL2FilterSrcMacAddr (&u4ErrCode, i4FilterNo,
                                               SrcMacAddr) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (u4DestType != ACL_ANY)
    {
        /* Destination MAC has been specified */

        if (nmhTestv2IssExtL2FilterDstMacAddr (&u4ErrCode, i4FilterNo,
                                               DestMacAddr) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (i4Protocol != 0)
    {
        if (nmhTestv2IssExtL2FilterProtocolType (&u4ErrCode, i4FilterNo,
                                                 i4Protocol) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (i4EtherType != 0)
    {
        if (nmhTestv2IssExtL2FilterEtherType (&u4ErrCode, i4FilterNo,
                                              i4EtherType) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (u4VlanId != 0)
    {
        if (nmhTestv2IssExtL2FilterVlanId (&u4ErrCode,
                                           i4FilterNo,
                                           u4VlanId) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (nmhTestv2IssExtL2FilterPriority (&u4ErrCode, i4FilterNo, i4Priority)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL2FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (u4SrcType != ACL_ANY)
    {

        if (nmhSetIssExtL2FilterSrcMacAddr (i4FilterNo, SrcMacAddr)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (u4DestType != ACL_ANY)
    {

        if (nmhSetIssExtL2FilterDstMacAddr (i4FilterNo, DestMacAddr)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4Protocol != 0)
    {
        if (nmhSetIssExtL2FilterProtocolType (i4FilterNo, i4Protocol) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (i4EtherType != 0)
    {
        if (nmhSetIssExtL2FilterEtherType (i4FilterNo, i4EtherType) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (u4VlanId != 0)
    {
        if (nmhSetIssExtL2FilterVlanId (i4FilterNo, u4VlanId) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    if (nmhSetIssExtL2FilterPriority (i4FilterNo, i4Priority) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclExtPbL2FilterConfig                             */
/*                                                                           */
/*     DESCRIPTION      : This function configures a extended IP ACL filter  */
/*                                                                           */
/*     INPUT            :  i4SVlan  - Service vlan Id                        */
/*                         i4SVlanPrio - Outer Vlan Priority                 */
/*                         i4CVlan - Customer Vlan Id                        */
/*                         i4CVlanPrio - Customer Vlan Priority              */
/*                         i4TagType - Packet Tag type on which the filter   */
/*                                     will be applied                       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
INT4
AclExtPbL2FilterConfig (tCliHandle CliHandle, INT4 i4OuterEType,
                        INT4 i4SVlan, INT4 i4SVlanPrio,
                        INT4 i4CVlanPrio, INT4 i4TagType)
{

    INT4                i4FilterNo;
    UINT4               u4ErrCode;

    i4FilterNo = CLI_GET_MACACL ();

    if (AclPbTestL2Filter (&u4ErrCode, CliHandle, i4FilterNo, i4OuterEType,
                           i4SVlan, i4SVlanPrio, i4CVlanPrio) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (AclPbSetL2Filter (CliHandle, i4FilterNo, i4OuterEType,
                          i4SVlan, i4SVlanPrio, i4CVlanPrio) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (i4TagType != 0)
    {
        if (AclPbTestSetL2TagType (&u4ErrCode, CliHandle, i4FilterNo,
                                   i4TagType) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclMacAccessGroup                                  */
/*                                                                           */
/*     DESCRIPTION      : This function associates a MAC acl to an interface */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handle                            */
/*                        i4FilterNo - MAC filter number                     */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclMacAccessGroup (tCliHandle CliHandle, INT4 i4FilterNo)
{
    UINT4               u4IfIndex;
    UINT1               au1PortList[ISS_PORTLIST_LEN];
    UINT1               au1OldPortList[ISS_PORTLIST_LEN];
    tSNMP_OCTET_STRING_TYPE PortList;
    tSNMP_OCTET_STRING_TYPE OldPortList;
    UINT4               u4ErrCode;
    INT4                i4Status;

    MEMSET (au1PortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1OldPortList, 0, ISS_PORTLIST_LEN);

    PortList.i4_Length = ISS_PORTLIST_LEN;
    PortList.pu1_OctetList = &au1PortList[0];

    OldPortList.i4_Length = ISS_PORTLIST_LEN;
    OldPortList.pu1_OctetList = &au1OldPortList[0];

    /*Get the interface index from the current mode in CLI */
    u4IfIndex = CLI_GET_IFINDEX ();

    if ((nmhGetIssExtL2FilterStatus (i4FilterNo, &i4Status)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid MAC Filter \r\n");
        return (CLI_FAILURE);
    }

    nmhGetIssExtL2FilterInPortList (i4FilterNo, &PortList);
    MEMCPY (OldPortList.pu1_OctetList, PortList.pu1_OctetList,
            PortList.i4_Length);
    OldPortList.i4_Length = PortList.i4_Length;

    ACL_ADD_PORT_LIST (au1PortList, u4IfIndex);

    if (nmhTestv2IssExtL2FilterStatus
        (&u4ErrCode, i4FilterNo, ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssExtL2FilterInPortList (&u4ErrCode, i4FilterNo, &PortList)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (i4Status == ISS_ACTIVE)
    {
        if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (nmhSetIssExtL2FilterInPortList (i4FilterNo, &PortList) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        nmhSetIssExtL2FilterInPortList (i4FilterNo, &OldPortList);
        nmhSetIssExtL2FilterStatus (i4FilterNo, i4Status);
        CLI_SET_ERR (CLI_ACL_FILTER_CREATION_FAILED);
        return (CLI_FAILURE);

    }

    UNUSED_PARAM (CliHandle);
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclNoMacAccessGroup                                */
/*                                                                           */
/*     DESCRIPTION      : This function removes a MAC access filter from     */
/*                        an interface                                       */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handle                            */
/*                        i4FilterNo - MAC filter number                     */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclNoMacAccessGroup (tCliHandle CliHandle, INT4 i4FilterNo)
{
    tSNMP_OCTET_STRING_TYPE PortList;
    UINT1               au1PortList[ISS_PORTLIST_LEN];
    UINT1               au1NullPortList[ISS_PORTLIST_LEN];
    UINT4               u4IfIndex;
    INT4                i4CurrentFilter;
    UINT1               u1Flag = 1;
    UINT4               u4ErrCode;

    MEMSET (au1PortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1NullPortList, 0, ISS_PORTLIST_LEN);

    PortList.i4_Length = ISS_PORTLIST_LEN;
    PortList.pu1_OctetList = &au1PortList[0];

    /*Get the interface index from the current mode in CLI */
    u4IfIndex = CLI_GET_IFINDEX ();

    if (i4FilterNo == 0)
    {
        u1Flag = 0;
        /* to delete all IP ACL 's in a given interface */

        if (nmhGetFirstIndexIssExtL2FilterTable (&i4FilterNo) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n No MAC ACLs have been configured\r\n");
            return (CLI_SUCCESS);
        }
    }

    do
    {
        nmhGetIssExtL2FilterInPortList (i4FilterNo, &PortList);

        if (CliIsMemberPort (PortList.pu1_OctetList,
                             ISS_PORTLIST_LEN, u4IfIndex) == CLI_SUCCESS)

        {
            ACL_DEL_PORT_LIST (au1PortList, u4IfIndex);

            /* Filter exists for this interface */
            if (nmhTestv2IssExtL2FilterStatus
                (&u4ErrCode, i4FilterNo, ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }
            if (nmhTestv2IssExtL2FilterInPortList
                (&u4ErrCode, i4FilterNo, &PortList) == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }
            if (nmhSetIssExtL2FilterInPortList (i4FilterNo, &PortList)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }

            /* If there are any ports in the port list,
             * then set the filter to ACTIVE state   */

            if ((MEMCMP (au1PortList, au1NullPortList, ISS_PORTLIST_LEN)) != 0)
            {
                if (nmhSetIssExtL2FilterStatus (i4FilterNo, ISS_ACTIVE) ==
                    SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
            }

            return (CLI_SUCCESS);
        }

        else if (u1Flag == 1)
        {
            CliPrintf (CliHandle,
                       "\r%% This filter has not been configured for this interface\r\n");
            return (CLI_SUCCESS);
        }
        i4CurrentFilter = i4FilterNo;

    }
    while ((nmhGetNextIndexIssExtL2FilterTable (i4CurrentFilter, &i4FilterNo) ==
            SNMP_SUCCESS) && (u1Flag == 0));

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclShowAccessLists                                 */
/*                                                                           */
/*     DESCRIPTION      : This function displays all or selected ACLs        */
/*                        conifgured in switch                               */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4FilterType - IP or MAC  filter                   */
/*                        i4FilterNo   - Filter number                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclShowAccessLists (tCliHandle CliHandle, INT4 i4FilterType, INT4 i4FilterNo)
{
    INT4                i4NextFilter;
    INT4                i4PrevFilter;
    UINT1               u1Quit = CLI_SUCCESS;

    if (i4FilterNo != 0)
    {
        if (i4FilterType == CLI_IP_ACL)
        {
            if (AclShowL3Filter (CliHandle, i4FilterNo) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nInvalid IP Access List \r\n");
                return CLI_FAILURE;
            }
        }
        else if (i4FilterType == CLI_MAC_ACL)
        {
            if (AclShowL2Filter (CliHandle, i4FilterNo) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nInvalid Mac Access List \r\n");
                return CLI_FAILURE;
            }
        }
        else
        {
            if (AclShowL3Filter (CliHandle, i4FilterNo) == CLI_SUCCESS)
            {
                u1Quit = CLI_SUCCESS;
            }
            if (AclShowL2Filter (CliHandle, i4FilterNo) == CLI_SUCCESS)
            {
                u1Quit = CLI_SUCCESS;
            }
            if (u1Quit == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r\nInvalid Access List \r\n");
                return CLI_FAILURE;
            }
        }
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\nIP ACCESS LISTS \r\n");
    CliPrintf (CliHandle, "-----------------\r\n");

    if (nmhGetFirstIndexIssExtL3FilterTable (&i4NextFilter) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r No IP Access Lists have been configured \r\n");
    }
    else
    {
        do
        {
            AclShowL3Filter (CliHandle, i4NextFilter);
            i4PrevFilter = i4NextFilter;
            u1Quit = CliPrintf (CliHandle, "\r\n");
        }
        while ((INT4) (nmhGetNextIndexIssExtL3FilterTable
                       (i4PrevFilter, &i4NextFilter) != SNMP_FAILURE)
               && (u1Quit != CLI_FAILURE));

        if (u1Quit == CLI_FAILURE)
        {
            return (u1Quit);
        }
    }

    CliPrintf (CliHandle, "\r\nMAC ACCESS LISTS\r\n");
    CliPrintf (CliHandle, "-----------------\r\n");

    if (nmhGetFirstIndexIssExtL2FilterTable (&i4NextFilter) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n No MAC Access Lists have been configured\r\n\r\n");
    }
    else
    {
        do
        {
            AclShowL2Filter (CliHandle, i4NextFilter);
            i4PrevFilter = i4NextFilter;
            u1Quit = CliPrintf (CliHandle, "\r\n\r\n");
        }
        while ((INT4) (nmhGetNextIndexIssExtL2FilterTable
                       (i4PrevFilter, &i4NextFilter) != SNMP_FAILURE)
               && (u1Quit != CLI_FAILURE));

        if (u1Quit == CLI_FAILURE)
        {
            return (u1Quit);
        }
    }
    return (u1Quit);
}

INT4
AclShowL3Filter (tCliHandle CliHandle, INT4 i4NextFilter)
{
    INT4                i4FltrTos = 0;
    INT4                i4FltrPrio = 0;
    INT4                i4MsgCode = 0;
    INT4                i4MsgType = 0;
    INT4                i4Protocol = 0;
    INT4                i4FltrAckBit = 0;
    INT4                i4FltrRstBit = 0;
    INT4                i4FltrAction = 0;
    INT4                i4SVlan = 0;
    INT4                i4SVlanPrio = 0;
    INT4                i4CVlan = 0;
    INT4                i4CVlanPrio = 0;
    INT4                i4TagType = 0;
    INT4                i4RowStatus = 0;
    UINT4               u4SrcIpAddr = 0;
    UINT4               u4SrcIpMask = 0;
    UINT4               u4DstIpAddr = 0;
    UINT4               u4DstIpMask = 0;
    UINT4               u4MinSrcProtPort = 0;
    UINT4               u4MaxSrcProtPort = 0;
    UINT4               u4MinDstProtPort = 0;
    UINT4               u4MaxDstProtPort = 0;
    INT1               *piIfName;
    INT4                i4Count;
    INT4                i4CommaCount;
    UINT1               u1StdAclFlag;
    INT4                i4FltrDscp = 0;
    tSNMP_OCTET_STRING_TYPE InPortList;
    tSNMP_OCTET_STRING_TYPE OutPortList;
    CHR1               *pu1String;
    UINT1               au1String[ISS_MAX_LEN];
    UINT1               au1InPortList[ISS_PORTLIST_LEN];
    UINT1               au1OutPortList[ISS_PORTLIST_LEN];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    InPortList.i4_Length = ISS_PORTLIST_LEN;
    OutPortList.i4_Length = ISS_PORTLIST_LEN;

    InPortList.pu1_OctetList = &au1InPortList[0];
    OutPortList.pu1_OctetList = &au1OutPortList[0];
    pu1String = (CHR1 *) & au1String[0];
    piIfName = (INT1 *) &au1IfName[0];

    MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1String, 0, ISS_MAX_LEN);

    if (nmhValidateIndexInstanceIssExtL3FilterTable (i4NextFilter) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    u1StdAclFlag = 0;
    /* Standard ACLs can have numbers upto 1000 */
    if (i4NextFilter < 1001)
    {
        u1StdAclFlag = 1;
    }

    if (!u1StdAclFlag)
    {
        nmhGetIssExtL3FilterPriority (i4NextFilter, &i4FltrPrio);
        nmhGetIssExtL3FilterProtocol (i4NextFilter, &i4Protocol);

        AclPbGetL3Filter (i4NextFilter, &i4SVlan, &i4SVlanPrio,
                          &i4CVlan, &i4CVlanPrio, &i4TagType);
    }

    if (i4Protocol == ISS_PROT_ICMP)
    {
        nmhGetIssExtL3FilterMessageType (i4NextFilter, &i4MsgType);
        nmhGetIssExtL3FilterMessageCode (i4NextFilter, &i4MsgCode);
    }

    nmhGetIssExtL3FilterSrcIpAddr (i4NextFilter, &u4SrcIpAddr);
    nmhGetIssExtL3FilterSrcIpAddrMask (i4NextFilter, &u4SrcIpMask);
    nmhGetIssExtL3FilterDstIpAddr (i4NextFilter, &u4DstIpAddr);
    nmhGetIssExtL3FilterDstIpAddrMask (i4NextFilter, &u4DstIpMask);
    nmhGetIssExtL3FilterInPortList (i4NextFilter, &InPortList);
    nmhGetIssExtL3FilterOutPortList (i4NextFilter, &OutPortList);

    if (i4Protocol == ISS_PROT_TCP)
    {
        nmhGetIssExtL3FilterAckBit (i4NextFilter, &i4FltrAckBit);
        nmhGetIssExtL3FilterRstBit (i4NextFilter, &i4FltrRstBit);
    }

    if ((i4NextFilter > ACL_STANDARD_MAX_VALUE) &&
        (i4Protocol != ISS_PROT_ICMP))
    {
        /* TOS bits */
        nmhGetIssExtL3FilterTos (i4NextFilter, &i4FltrTos);
        nmhGetIssExtL3FilterDscp (i4NextFilter, &i4FltrDscp);
    }

    /* Source and destination protocol port ranges for TCP/UDP */
    if ((i4Protocol == ISS_PROT_TCP) || (i4Protocol == ISS_PROT_UDP))
    {
        nmhGetIssExtL3FilterMinSrcProtPort (i4NextFilter, &u4MinSrcProtPort);
        nmhGetIssExtL3FilterMaxSrcProtPort (i4NextFilter, &u4MaxSrcProtPort);
        nmhGetIssExtL3FilterMinDstProtPort (i4NextFilter, &u4MinDstProtPort);
        nmhGetIssExtL3FilterMaxDstProtPort (i4NextFilter, &u4MaxDstProtPort);
    }

    nmhGetIssExtL3FilterAction (i4NextFilter, &i4FltrAction);
    nmhGetIssExtL3FilterStatus (i4NextFilter, &i4RowStatus);

    /* Standard ACLs can have numbers upto 1000 */
    if (i4NextFilter < 1001)
    {
        CliPrintf (CliHandle,
                   "\r\nStandard IP Access List %d\r\n", i4NextFilter);
        CliPrintf (CliHandle, "----------------------------\r\n");
    }
    /* Extended ACLs can have numbers in the range 1001-65535 */
    else
    {
        CliPrintf (CliHandle,
                   "\r\nExtended IP Access List %d\r\n", i4NextFilter);
        CliPrintf (CliHandle, "-----------------------------\r\n");
    }

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1String, 0, ISS_MAX_LEN);

    /* Filter priority and protocol type fields are not
     * configurable for Standard ACL's */
    if (!u1StdAclFlag)
    {
        CliPrintf (CliHandle, "%-33s : %d\r\n", " Filter Priority", i4FltrPrio);

        CliPrintf (CliHandle, "%-33s : ", " Filter Protocol Type");
        switch (i4Protocol)
        {
            case ISS_PROT_ICMP:
                CliPrintf (CliHandle, "ICMP\r\n");
                break;
            case ISS_PROT_IGMP:
                CliPrintf (CliHandle, "IGMP\r\n");
                break;
            case ISS_PROT_GGP:
                CliPrintf (CliHandle, "GGP\r\n");
                break;
            case ISS_PROT_IP:
                CliPrintf (CliHandle, "IP\r\n");
                break;
            case ISS_PROT_TCP:
                CliPrintf (CliHandle, "TCP\r\n");
                break;
            case ISS_PROT_EGP:
                CliPrintf (CliHandle, "EGP\r\n");
                break;
            case ISS_PROT_IGP:
                CliPrintf (CliHandle, "IGP\r\n");
                break;
            case ISS_PROT_NVP:
                CliPrintf (CliHandle, "NVP\r\n");
                break;
            case ISS_PROT_UDP:
                CliPrintf (CliHandle, "UDP\r\n");
                break;
            case ISS_PROT_IRTP:
                CliPrintf (CliHandle, "IRTP\r\n");
                break;
            case ISS_PROT_IDPR:
                CliPrintf (CliHandle, "IDPR\r\n");
                break;
            case ISS_PROT_RSVP:
                CliPrintf (CliHandle, "RSVP\r\n");
                break;
            case ISS_PROT_MHRP:
                CliPrintf (CliHandle, "MHRP\r\n");
                break;
            case ISS_PROT_IGRP:
                CliPrintf (CliHandle, "IGRP\r\n");
                break;
            case ISS_PROT_OSPFIGP:
                CliPrintf (CliHandle, "OSPF\r\n");
                break;
            case ISS_PROT_PIM:
                CliPrintf (CliHandle, "PIM\r\n");
                break;
            case ISS_PROT_ANY:
                CliPrintf (CliHandle, "ANY\r\n");
                break;
            default:
                CliPrintf (CliHandle, "%d\r\n", i4Protocol);
                break;
        }
        if (i4Protocol == ISS_PROT_ICMP)
        {

            switch (i4MsgType)
            {
                case ISS_ECHO_REPLY:
                    CliPrintf (CliHandle,
                               "%-33s : Echo reply\r\n", " ICMP type");
                    break;
                case ISS_DEST_UNREACH:
                    CliPrintf (CliHandle,
                               "%-33s : Destination unreachable\r\n",
                               " ICMP type");
                    break;
                case ISS_SRC_QUENCH:
                    CliPrintf (CliHandle,
                               "%-33s : Source quench\r\n", " ICMP type");
                    break;
                case ISS_REDIRECT:
                    CliPrintf (CliHandle, "%-33s : Redirect\r\n", " ICMP type");
                    break;
                case ISS_ECHO_REQ:
                    CliPrintf (CliHandle,
                               "%-33s : Echo request\r\n", " ICMP type");
                    break;
                case ISS_TIME_EXCEED:
                    CliPrintf (CliHandle,
                               "%-33s : Time exceeded\r\n", " ICMP type");
                    break;
                case ISS_PARAM_PROB:
                    CliPrintf (CliHandle,
                               "%-33s : Parameter problem\r\n", " ICMP type");
                    break;
                case ISS_TIMEST_REQ:
                    CliPrintf (CliHandle,
                               "%-33s : Timestamp request\r\n", " ICMP type");
                    break;
                case ISS_TIMEST_REP:
                    CliPrintf (CliHandle,
                               "%-33s : Timestamp reply\r\n", " ICMP type");
                    break;
                case ISS_INFO_REQ:
                    CliPrintf (CliHandle,
                               "%-33s : Information request\r\n", " ICMP type");
                    break;
                case ISS_INTO_REP:
                    CliPrintf (CliHandle,
                               "%-33s : Information reply\r\n", " ICMP type");
                    break;
                case ISS_ADD_MK_REQ:
                    CliPrintf (CliHandle,
                               "%-33s : Address mask request\r\n",
                               " ICMP type");
                    break;
                case ISS_ADD_MK_REP:
                    CliPrintf (CliHandle,
                               "%-33s : Address mask reply\r\n", " ICMP type");
                    break;
                case ISS_NO_ICMP_TYPE:
                    CliPrintf (CliHandle,
                               "%-33s : No ICMP types to be filtered\r\n",
                               " ICMP type");
                    break;
                default:
                    CliPrintf (CliHandle,
                               "%-33s : %d\r\n", " ICMP type", i4MsgType);
                    break;
            }

            switch (i4MsgCode)
            {
                case ISS_NET_UNREACH:
                    CliPrintf (CliHandle,
                               "%-33s : Network unreachable\r\n", " ICMP code");
                    break;
                case ISS_HOST_UNREACH:
                    CliPrintf (CliHandle,
                               "%-33s : Host unreachable\r\n", " ICMP code");
                    break;
                case ISS_PROT_UNREACH:
                    CliPrintf (CliHandle,
                               "%-33s : Protocol unreachable\r\n",
                               " ICMP code");
                    break;
                case ISS_PORT_UNREACH:
                    CliPrintf (CliHandle,
                               "%-33s : Port unreachable\r\n", " ICMP code");
                    break;
                case ISS_FRG_NEED:
                    CliPrintf (CliHandle,
                               "%-33s : Fragment need\r\n", " ICMP code");
                    break;
                case ISS_SRC_RT_FAIL:
                    CliPrintf (CliHandle,
                               "%-33s : Source route failed\r\n", " ICMP code");
                    break;
                case ISS_DST_NET_UNK:
                    CliPrintf (CliHandle,
                               "%-33s : Destination network unknown\r\n",
                               " ICMP code");
                    break;
                case ISS_DST_HOST_UNK:
                    CliPrintf (CliHandle,
                               "%-33s : Destination host unknown\r\n",
                               " ICMP code");
                    break;
                case ISS_SRC_HOST_ISO:
                    CliPrintf (CliHandle,
                               "%-33s : Source host isolated\r\n",
                               " ICMP code");
                    break;
                case ISS_DST_NET_ADMP:
                    CliPrintf (CliHandle,
                               "%-33s : Destination network "
                               "administratively prohibited\r\n", " ICMP code");
                    break;
                case ISS_DST_HOST_ADMP:
                    CliPrintf (CliHandle,
                               "%-33s : Destination host"
                               "administratively prohibited\r\n", " ICMP code");
                    break;
                case ISS_NET_UNRE_TOS:
                    CliPrintf (CliHandle,
                               "%-33s : Network unreachable TOS\r\n",
                               " ICMP code");
                    break;
                case ISS_HOST_UNRE_TOS:
                    CliPrintf (CliHandle,
                               "%-33s : Host unreachable TOS\r\n",
                               " ICMP code");
                    break;
                case ISS_NO_ICMP_CODE:
                    CliPrintf (CliHandle,
                               "%-33s : No ICMP codes to be filtered\r\n",
                               " ICMP code");
                    break;
                default:
                    CliPrintf (CliHandle,
                               "%-33s : %d\r\n", " ICMP code", i4MsgCode);
                    break;
            }
        }
    }

    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcIpAddr);
    CliPrintf (CliHandle, "%-33s : %-17s\r\n", " Source IP address", pu1String);
    MEMSET (au1String, 0, ISS_MAX_LEN);

    /*filter source IP address mask */
    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcIpMask);
    CliPrintf (CliHandle,
               "%-33s : %-17s\r\n", " Source IP address mask", pu1String);
    MEMSET (au1String, 0, ISS_MAX_LEN);

    /*filter destination IP address */
    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4DstIpAddr);
    CliPrintf (CliHandle,
               "%-33s : %-17s\r\n", " Destination IP address", pu1String);
    MEMSET (au1String, 0, ISS_MAX_LEN);

    /*filter destination IP address */
    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4DstIpMask);
    CliPrintf (CliHandle,
               "%-33s : %-17s\r\n", " Destination IP address mask", pu1String);
    MEMSET (au1String, 0, ISS_MAX_LEN);

    /* in ports */

    CliPrintf (CliHandle, "%-33s : ", " In Port List");

    i4Count = 0;
    i4CommaCount = 0;
    do
    {
        if (CliIsMemberPort (InPortList.pu1_OctetList,
                             ISS_PORTLIST_LEN, i4Count) == CLI_SUCCESS)
        {
            i4CommaCount++;
            CfaCliGetIfName ((UINT4) i4Count, piIfName);
            /* This function will format the display of the port list */
            AclCliPrintPortList (CliHandle, i4CommaCount, (UINT1 *) piIfName);
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        }

        i4Count++;
    }
    while (i4Count <= SYS_DEF_MAX_PHYSICAL_INTERFACES);
    /*If no port list has been configured */
    if (i4CommaCount == 0)
    {
        CliPrintf (CliHandle, "NIL");
    }

    MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
    CliPrintf (CliHandle, "\r\n");

    /* out ports */

    CliPrintf (CliHandle, "%-33s : ", " Out Port List");
    i4Count = 0;
    i4CommaCount = 0;
    do
    {
        if (CliIsMemberPort (OutPortList.pu1_OctetList,
                             ISS_PORTLIST_LEN, i4Count) == CLI_SUCCESS)
        {
            i4CommaCount++;
            CfaCliGetIfName ((UINT4) i4Count, piIfName);
            AclCliPrintPortList (CliHandle, i4CommaCount, (UINT1 *) piIfName);
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        }

        i4Count++;
    }
    while (i4Count <= SYS_DEF_MAX_PHYSICAL_INTERFACES);
    /*If no port list has been configured */
    if (i4CommaCount == 0)
    {
        CliPrintf (CliHandle, "NIL");
    }

    MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);
    CliPrintf (CliHandle, "\r\n");

    if (i4Protocol == ISS_PROT_TCP)
    {
        /* ACK bit */
        if (i4FltrAckBit == ISS_ACK_ESTABLISH)
        {
            CliPrintf (CliHandle, " Filtering ACK bit\r\n");
        }

        /* RST bit */
        if (i4FltrRstBit == ISS_RST_SET)
        {
            CliPrintf (CliHandle, " Filtering RST bit\r\n");
        }

    }
    /* Display TOS if  access list is extended and protocol
     * is not ICMP 
     */
    if ((i4NextFilter > ACL_STANDARD_MAX_VALUE) &&
        (i4Protocol != ISS_PROT_ICMP))
    {

        switch (i4FltrTos)
        {
            case ISS_TOS_NONE:
                CliPrintf (CliHandle, "%-33s : None\r\n", " Filter TOS");
                break;
            case ISS_TOS_HI_REL:
                CliPrintf (CliHandle, "%-33s : High reliablity\r\n",
                           " Filter TOS");
                break;
            case ISS_TOS_HI_THR:
                CliPrintf (CliHandle, "%-33s : High throughput\r\n",
                           " Filter TOS");
                break;
            case ISS_TOS_HI_REL_HI_THR:
                CliPrintf (CliHandle, "%-33s : High reliability"
                           " and high throughput\r\n", " Filter TOS");
                break;
            case ISS_TOS_LO_DEL:
                CliPrintf (CliHandle, "%-33s : Low delay\r\n", " Filter TOS");
                break;
            case ISS_TOS_LO_DEL_HI_REL:
                CliPrintf (CliHandle, "%-33s : Low delay and high"
                           " reliability\r\n", " Filter TOS");
                break;
            case ISS_TOS_LO_DEL_HI_THR:
                CliPrintf (CliHandle, "%-33s : Low delay and high"
                           " throughput\r\n", " Filter TOS");
                break;
            case ISS_TOS_LO_DEL_HI_THR_HI_REL:
                CliPrintf (CliHandle, "%-33s : Low delay, high"
                           " throughput and high reliability\r\n",
                           " Filter TOS");
                break;
            default:
                CliPrintf (CliHandle, "%-33s : Invalid combination\r\n",
                           " Filter TOS");
                break;
        }

        if (i4FltrDscp == ISS_DSCP_INVALID)
        {
            CliPrintf (CliHandle, "%-33s : NIL\r\n", " Filter DSCP");
        }
        else
        {
            CliPrintf (CliHandle, "%-33s : %d\r\n", " Filter DSCP", i4FltrDscp);
        }
    }

    /* Source and destination protocol port ranges for TCP/UDP */
    if ((i4Protocol == ISS_PROT_TCP) || (i4Protocol == ISS_PROT_UDP))
    {
        CliPrintf (CliHandle,
                   "%-33s : %d\r\n",
                   " Filter Source Ports From", u4MinSrcProtPort);

        CliPrintf (CliHandle, "%-33s : %d\r\n",
                   " Filter Source Ports Till", u4MaxSrcProtPort);

        CliPrintf (CliHandle, "%-33s : %d\r\n",
                   " Filter Destination Ports From", u4MinDstProtPort);

        CliPrintf (CliHandle, "%-33s : %d\r\n",
                   " Filter Destination Ports Till", u4MaxDstProtPort);
    }
    if (!u1StdAclFlag)
    {
        AclPbShowL3Filter (CliHandle, i4SVlan, i4SVlanPrio, i4CVlan,
                           i4CVlanPrio, i4TagType);
    }
    /* filter action */
    if (i4FltrAction == ISS_ALLOW)
    {
        CliPrintf (CliHandle, "%-33s : Permit\r\n", " Filter Action");
    }
    else if (i4FltrAction == ISS_DROP)
    {
        CliPrintf (CliHandle, "%-33s : Deny\r\n", " Filter Action");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Unknown\r\n", " Filter Action");
    }

    if (i4RowStatus == ISS_ACTIVE)
    {
        CliPrintf (CliHandle, "%-33s : Active", " Status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : InActive", " Status");
    }
    CliPrintf (CliHandle, "\r\n\r\n");
    return CLI_SUCCESS;
}

INT4
AclShowL2Filter (tCliHandle CliHandle, INT4 i4NextFilter)
{
    tMacAddr            DstMacAddr;
    tMacAddr            SrcMacAddr;
    INT4                i4FltrPrio;
    INT4                i4EtherType;
    INT4                i4Protocol;
    INT4                i4VlanId;
    INT4                i4OuterEType;
    INT4                i4SVlan;
    INT4                i4SVlanPrio;
    INT4                i4CVlanPrio;
    INT4                i4TagType;
    INT4                i4FltrAction;
    INT4                i4RowStatus;
    INT4                i4Count;
    INT4                i4CommaCount;
    CHR1               *pu1String;
    INT1               *piIfName;
    UINT1               au1String[ISS_MAX_LEN];
    UINT1               au1InPortList[ISS_PORTLIST_LEN];
    UINT1               au1OutPortList[ISS_PORTLIST_LEN];
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    tSNMP_OCTET_STRING_TYPE InPortList;
    tSNMP_OCTET_STRING_TYPE OutPortList;

    InPortList.i4_Length = ISS_PORTLIST_LEN;
    OutPortList.i4_Length = ISS_PORTLIST_LEN;

    InPortList.pu1_OctetList = &au1InPortList[0];
    OutPortList.pu1_OctetList = &au1OutPortList[0];
    pu1String = (CHR1 *) & au1String[0];
    piIfName = (INT1 *) &au1IfName[0];

    MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);
    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1String, 0, ISS_MAX_LEN);

    if (nmhValidateIndexInstanceIssExtL2FilterTable (i4NextFilter) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    nmhGetIssExtL2FilterProtocolType (i4NextFilter, (UINT4 *) &i4Protocol);
    nmhGetIssExtL2FilterPriority (i4NextFilter, &i4FltrPrio);
    nmhGetIssExtL2FilterEtherType (i4NextFilter, &i4EtherType);
    nmhGetIssExtL2FilterVlanId (i4NextFilter, &i4VlanId);
    nmhGetIssExtL2FilterDstMacAddr (i4NextFilter, (tMacAddr *) & DstMacAddr);
    nmhGetIssExtL2FilterSrcMacAddr (i4NextFilter, (tMacAddr *) & SrcMacAddr);
    nmhGetIssExtL2FilterInPortList (i4NextFilter, &InPortList);

    AclPbGetL2Filter (i4NextFilter, &i4OuterEType, &i4SVlan,
                      &i4SVlanPrio, &i4CVlanPrio, &i4TagType);

    nmhGetIssExtL2FilterAction (i4NextFilter, &i4FltrAction);
    nmhGetIssExtL2FilterStatus (i4NextFilter, &i4RowStatus);
    CliPrintf (CliHandle, "\r\nExtended MAC Access List %d\r\n", i4NextFilter);
    CliPrintf (CliHandle, "-----------------------------\r\n");

    CliPrintf (CliHandle, "%-33s : %d\r\n", " Filter Priority", i4FltrPrio);

    CliPrintf (CliHandle, "%-33s : ", " Protocol Type");
    switch (i4Protocol)
    {
        case AARP:
            CliPrintf (CliHandle, "AARP\r\n");
            break;

        case AMBER:
            CliPrintf (CliHandle, "AMBER\r\n");
            break;

        case DEC_SPANNING:
            CliPrintf (CliHandle, "DEC_SPANNING\r\n");
            break;

        case DIAGNOSTIC:
            CliPrintf (CliHandle, "DIAGNOSTIC\r\n");
            break;

        case DSM:
            CliPrintf (CliHandle, "DSM\r\n");
            break;

        case ETYPE_6000:
            CliPrintf (CliHandle, "ETYPE_6000\r\n");
            break;

        case ETYPE_8042:
            CliPrintf (CliHandle, "ETYPE_8042\r\n");
            break;

        case LAT:
            CliPrintf (CliHandle, "LAT\r\n");
            break;

        case LAVC_SCA:
            CliPrintf (CliHandle, "LAVC_SCA\r\n");
            break;

        case MOP_CONSOLE:
            CliPrintf (CliHandle, "MOP_CONSOLE\r\n");
            break;

        case MSDOS:
            CliPrintf (CliHandle, "MSDOS\r\n");
            break;

        case MUMPS:
            CliPrintf (CliHandle, "MUMPS\r\n");
            break;

        case NET_BIOS:
            CliPrintf (CliHandle, "NET_BIOS\r\n");
            break;

        case VINES_ECHO:
            CliPrintf (CliHandle, "VINES_ECHO\r\n");
            break;

        case VINES_IP:
            CliPrintf (CliHandle, "VINES_IP\r\n");
            break;

        case XNS_ID:
            CliPrintf (CliHandle, "XNS_ID\r\n");
            break;

        default:
            CliPrintf (CliHandle, "%d\r\n", i4Protocol);
            break;
    }

    CliPrintf (CliHandle, "%-33s : %d\r\n", " EtherType", i4EtherType);
    CliPrintf (CliHandle, "%-33s : %d\r\n", " Vlan Id", i4VlanId);

    CliPrintf (CliHandle, "%-33s : ", " Destination MAC Address");
    PrintMacAddress (DstMacAddr, (UINT1 *) pu1String);
    CliPrintf (CliHandle, "%s", pu1String);
    MEMSET (au1String, 0, ISS_MAX_LEN);
    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "%-33s : ", " Source MAC Address");
    PrintMacAddress (SrcMacAddr, (UINT1 *) pu1String);
    CliPrintf (CliHandle, "%s", pu1String);
    MEMSET (au1String, 0, ISS_MAX_LEN);
    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "%-33s : ", " In Port List");
    i4Count = 0;
    i4CommaCount = 0;
    do
    {
        if (CliIsMemberPort (InPortList.pu1_OctetList,
                             ISS_PORTLIST_LEN, i4Count) == CLI_SUCCESS)
        {
            i4CommaCount++;
            CfaCliGetIfName ((UINT4) i4Count, piIfName);
            /* This function will format the display of the port list */
            AclCliPrintPortList (CliHandle, i4CommaCount, (UINT1 *) piIfName);
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        }

        i4Count++;
    }
    while (i4Count <= SYS_DEF_MAX_PHYSICAL_INTERFACES);

    /*If no port list has been configured */
    if (i4CommaCount == 0)
    {
        CliPrintf (CliHandle, "NIL");
    }

    MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
    CliPrintf (CliHandle, "\r\n");

    AclPbShowL2Filter (CliHandle, i4OuterEType, i4SVlan, i4SVlanPrio,
                       i4CVlanPrio, i4TagType);
    if (i4FltrAction == ISS_ALLOW)
    {
        CliPrintf (CliHandle, "%-33s : Permit\r\n", " Filter Action");
    }
    else if (i4FltrAction == ISS_DROP)
    {
        CliPrintf (CliHandle, "%-33s : Deny\r\n", " Filter Action");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Unknown\r\n", " Filter Action");
    }

    if (i4RowStatus == ISS_ACTIVE)
    {
        CliPrintf (CliHandle, "%-33s : Active", " Status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : InActive", " Status");
    }
    CliPrintf (CliHandle, "\r\n\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclShowRunningConfig                               */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the ACL Configuration      */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
AclShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{

    INT4                i4Index = -1;

    AclShowRunningConfigTables (CliHandle);

    if (u4Module == ISS_ACL_SHOW_RUNNING_CONFIG)
    {
        AclShowRunningConfigInterfaceDetails (CliHandle, i4Index);

    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*     FUNCTION NAME    : AclShowRunningConfigTables                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays table  objects in ACL  for  */
/*                        show running configuration.                        */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
AclShowRunningConfigTables (tCliHandle CliHandle)
{

    tMacAddr            DstMacAddr;
    tMacAddr            SrcMacAddr;
    tMacAddr            zeroAddr;
    INT4                i4NextFilter;
    INT4                i4PrevFilter;
    INT4                i4FltrAction;
    INT4                i4Tos = 0;
    INT4                i4Dscp = 0;
    INT4                i4MsgType;
    INT4                i4MsgCode;
    INT4                i4FltrAckBit;
    INT4                i4FltrRstBit;
    INT4                i4EtherType;
    INT4                i4FltrPrio;
    INT4                i4Protocol = 0;
    INT4                i4SVlan = ACL_DEF_SVLAN;
    INT4                i4SVlanPrio = ACL_DEF_SVLAN_PRIO;
    INT4                i4VlanId = ACL_DEF_CVLAN;
    INT4                i4CVlanPrio = ACL_DEF_CVLAN_PRIO;
    INT4                i4TagType = ISS_FILTER_SINGLE_TAG;
    INT1                i1OutCome;
    UINT4               u4SrcIpAddr;
    UINT4               u4SrcIpMask;
    UINT4               u4DstIpAddr;
    UINT4               u4DstIpMask;
    UINT4               u4MinSrcProtPort = 0;
    UINT4               u4MaxSrcProtPort = 0;
    UINT4               u4MinDstProtPort = 0;
    UINT4               u4MaxDstProtPort = 0;
    INT4                i4OuterEType = 0;
    UINT1              *pu1String;
    UINT1               au1SrcAddr[ISS_ADDR_LEN];
    UINT1               au1SrcMask[ISS_ADDR_LEN];
    UINT1               au1DstAddr[ISS_ADDR_LEN];
    UINT1               au1DstMask[ISS_ADDR_LEN];
    UINT1               u1Quit = CLI_SUCCESS;
    UINT1               au1String[ISS_ADDR_LEN];
    UINT1               u1StdAclFlag = 0;
    pu1String = &au1String[0];

    CLI_MEMSET (au1String, 0, ISS_ADDR_LEN);
    CLI_MEMSET (zeroAddr, 0, MAC_ADDR_LEN);

    CliRegisterLock (CliHandle, IssLock, IssUnLock);

    ISS_LOCK ();
    /*Ip Access lists */

    i1OutCome = nmhGetFirstIndexIssExtL3FilterTable (&i4NextFilter);

    CliPrintf (CliHandle, "\r\n");

    while (i1OutCome != SNMP_FAILURE)
    {

        u1StdAclFlag = 0;

        if (i4NextFilter < 1001)
        {
            CliPrintf (CliHandle, "ip access-list standard %d\r\n",
                       i4NextFilter);
            u1StdAclFlag = 1;
        }
        else
        {
            CliPrintf (CliHandle, "\nip  access-list extended %d\r\n",
                       i4NextFilter);
        }

        if (!u1StdAclFlag)
        {
            nmhGetIssExtL3FilterProtocol (i4NextFilter, &i4Protocol);

            AclPbGetL3Filter (i4NextFilter, &i4SVlan, &i4SVlanPrio,
                              &i4VlanId, &i4CVlanPrio, &i4TagType);

            nmhGetIssExtL3FilterPriority (i4NextFilter, &i4FltrPrio);
        }

        if (i4Protocol == ISS_PROT_ICMP)
        {
            nmhGetIssExtL3FilterMessageType (i4NextFilter, &i4MsgType);
            nmhGetIssExtL3FilterMessageCode (i4NextFilter, &i4MsgCode);
        }

        /* Source and destination protocol port ranges for TCP/UDP */
        if ((i4Protocol == ISS_PROT_TCP) || (i4Protocol == ISS_PROT_UDP))
        {
            nmhGetIssExtL3FilterMinSrcProtPort (i4NextFilter,
                                                &u4MinSrcProtPort);
            nmhGetIssExtL3FilterMaxSrcProtPort (i4NextFilter,
                                                &u4MaxSrcProtPort);
            nmhGetIssExtL3FilterMinDstProtPort (i4NextFilter,
                                                &u4MinDstProtPort);
            nmhGetIssExtL3FilterMaxDstProtPort (i4NextFilter,
                                                &u4MaxDstProtPort);
        }

        nmhGetIssExtL3FilterAction (i4NextFilter, &i4FltrAction);
        nmhGetIssExtL3FilterSrcIpAddr (i4NextFilter, &u4SrcIpAddr);
        nmhGetIssExtL3FilterSrcIpAddrMask (i4NextFilter, &u4SrcIpMask);
        nmhGetIssExtL3FilterDstIpAddr (i4NextFilter, &u4DstIpAddr);
        nmhGetIssExtL3FilterDstIpAddrMask (i4NextFilter, &u4DstIpMask);

        if (!u1StdAclFlag)
        {
            if ((i4NextFilter > ACL_STANDARD_MAX_VALUE) &&
                (i4Protocol != ISS_PROT_ICMP))
            {
                nmhGetIssExtL3FilterTos (i4NextFilter, &i4Tos);

                nmhGetIssExtL3FilterDscp (i4NextFilter, &i4Dscp);
            }
        }

        if (i4Protocol == ISS_PROT_TCP)
        {
            nmhGetIssExtL3FilterAckBit (i4NextFilter, &i4FltrAckBit);
            nmhGetIssExtL3FilterRstBit (i4NextFilter, &i4FltrRstBit);
        }

        if (i4FltrAction == ISS_ALLOW)
        {
            CliPrintf (CliHandle, " permit ");
        }
        else if (i4FltrAction == ISS_DROP)
        {
            CliPrintf (CliHandle, " deny ");
        }

        if (!u1StdAclFlag)
        {
            if (i4Protocol == ISS_PROTO_ANY)
            {
                CliPrintf (CliHandle, " ip ");
            }
            else if (i4Protocol == ISS_PROT_OSPFIGP)
            {
                CliPrintf (CliHandle, " ospf ");

            }
            else if (i4Protocol == ISS_PROT_PIM)
            {
                CliPrintf (CliHandle, " pim ");
            }
            else if (i4Protocol == ISS_PROT_TCP)
            {
                CliPrintf (CliHandle, " tcp ");
            }
            else if (i4Protocol == ISS_PROT_UDP)
            {
                CliPrintf (CliHandle, " udp ");
            }
            else if (i4Protocol == ISS_PROT_ICMP)
            {
                CliPrintf (CliHandle, " icmp ");
            }
            else
            {
                CliPrintf (CliHandle, "%d ", i4Protocol);
            }

        }

        /*Source Ip Address */
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcIpAddr);
        CLI_MEMCPY (au1SrcAddr, pu1String, ISS_ADDR_LEN);

        CLI_MEMSET (au1String, 0, ISS_ADDR_LEN);
        /*Source Ip Mask */
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcIpMask);
        CLI_MEMCPY (au1SrcMask, pu1String, ISS_ADDR_LEN);

        CLI_MEMSET (au1String, 0, ISS_ADDR_LEN);
        /*filter destination IP address */
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4DstIpAddr);
        CLI_MEMCPY (au1DstAddr, pu1String, ISS_ADDR_LEN);

        CLI_MEMSET (au1String, 0, ISS_ADDR_LEN);

        /*filter destination IP address */
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4DstIpMask);
        CLI_MEMCPY (au1DstMask, pu1String, ISS_ADDR_LEN);

        if (u4SrcIpAddr == 0)
        {
            CliPrintf (CliHandle, " any  ");
        }

        else if ((u4SrcIpAddr != 0) && (CLI_STRCMP (au1SrcMask,
                                                    "255.255.255.255") == 0))
        {
            CliPrintf (CliHandle, "host %s ", au1SrcAddr);
        }
        else if ((u4SrcIpAddr != 0) && (CLI_STRCMP (au1SrcMask,
                                                    "255.255.255.255") != 0))
        {
            CliPrintf (CliHandle, "%s %s ", au1SrcAddr, au1SrcMask);
        }

        if ((!u1StdAclFlag) && ((i4Protocol == ISS_PROT_TCP) ||
                                (i4Protocol == ISS_PROT_UDP)))
        {
            if ((u4MinSrcProtPort != 0) && (u4MaxSrcProtPort != 0))
            {
                if (u4MinSrcProtPort == u4MaxSrcProtPort)
                {
                    CliPrintf (CliHandle, "eq %d ", u4MinSrcProtPort);
                }
                else if (u4MinSrcProtPort == 1 && u4MaxSrcProtPort != 0xffff)
                {
                    CliPrintf (CliHandle, "lt %d ", u4MaxSrcProtPort + 1);
                }
                else if (u4MaxSrcProtPort == 0xffff && u4MinSrcProtPort != 1)
                {
                    CliPrintf (CliHandle, "gt %d ", u4MinSrcProtPort - 1);
                }
                else
                {
                    CliPrintf (CliHandle, " range %d  %d ", u4MinSrcProtPort,
                               u4MaxSrcProtPort);
                }
            }

        }
        if (u4DstIpAddr == 0)
        {
            CliPrintf (CliHandle, " any ");
        }
        else if ((u4DstIpAddr != 0) && (CLI_STRCMP (au1DstMask,
                                                    "255.255.255.255") == 0))
        {
            CliPrintf (CliHandle, "host %s ", au1DstAddr);
        }
        else if ((u4DstIpAddr != 0) && (CLI_STRCMP (au1DstMask,
                                                    "255.255.255.255") != 0))
        {
            CliPrintf (CliHandle, "%s %s ", au1DstAddr, au1DstMask);
        }

        if ((!u1StdAclFlag) && ((i4Protocol == ISS_PROT_TCP) ||
                                (i4Protocol == ISS_PROT_UDP)))
        {
            if ((u4MinDstProtPort != 0) && (u4MaxDstProtPort != 0))
            {
                CliPrintf (CliHandle, " range %d %d ", u4MinDstProtPort,
                           u4MaxDstProtPort);
                if (u4MinDstProtPort == u4MaxDstProtPort)
                {
                    CliPrintf (CliHandle, "eq %d ", u4MinDstProtPort);
                }
                else if (u4MinDstProtPort == 1 && u4MaxDstProtPort != 0xffff)
                {
                    CliPrintf (CliHandle, "lt %d ", u4MaxDstProtPort + 1);
                }
                else if (u4MaxDstProtPort == 0xffff && u4MinDstProtPort != 1)
                {
                    CliPrintf (CliHandle, "gt %d ", u4MinDstProtPort - 1);
                }
                else
                {
                    CliPrintf (CliHandle, " range %d  %d ", u4MinDstProtPort,
                               u4MaxDstProtPort);
                }
            }
        }

        if (i4Protocol == ISS_PROT_TCP)
        {
            /* ACK bit */
            if (i4FltrAckBit == ISS_ACK_ESTABLISH)
            {
                CliPrintf (CliHandle, " ack");
            }

            /* RST bit */
            if (i4FltrRstBit == ISS_RST_SET)
            {
                CliPrintf (CliHandle, " rst");
            }

        }

        if (i4Protocol == ISS_PROT_ICMP)
        {
            if (i4MsgType != 255)
            {
                CliPrintf (CliHandle, " %d ", i4MsgType);
            }
            if (i4MsgCode != 255)
            {
                CliPrintf (CliHandle, "%d", i4MsgCode);
            }

        }

        if (!u1StdAclFlag)
        {
            if (i4Protocol != ISS_PROT_ICMP)
            {
                if (i4Tos != 8)
                {
                    if (i4Tos == ISS_TOS_HI_REL)
                    {
                        CliPrintf (CliHandle, " tos max-reliability");
                    }
                    else if (i4Tos == ISS_TOS_HI_THR)
                    {

                        CliPrintf (CliHandle, " tos max-throughput");
                    }
                    else if (i4Tos == ISS_TOS_LO_DEL)
                    {

                        CliPrintf (CliHandle, " tos min-delay");
                    }
                    else if (i4Tos == ISS_TOS_NONE)
                    {

                        /* tos normal */
                    }
                    else
                    {
                        CliPrintf (CliHandle, " tos %d", i4Tos);
                    }
                }

                if (i4Dscp != ISS_DSCP_INVALID)
                {
                    CliPrintf (CliHandle, " dscp %d", i4Dscp);

                }
            }

            if ((i4FltrPrio != 0) && (i4FltrPrio != 1))
            {
                CliPrintf (CliHandle, " priority %d", i4FltrPrio);

            }

            if (i4SVlan != ACL_DEF_SVLAN)
            {
                CliPrintf (CliHandle, " svlan-id %d", i4SVlan);
            }
            if (i4SVlanPrio != ACL_DEF_SVLAN_PRIO)
            {
                CliPrintf (CliHandle, " svlan-priority %d", i4SVlanPrio);
            }
            if (i4VlanId != ACL_DEF_CVLAN)
            {
                CliPrintf (CliHandle, " cvlan-id %d", i4VlanId);
            }
            if (i4CVlanPrio != ACL_DEF_CVLAN_PRIO)
            {
                CliPrintf (CliHandle, " cvlan-priority %d", i4CVlanPrio);
            }
            if (i4TagType != ISS_FILTER_SINGLE_TAG)
            {
                CliPrintf (CliHandle, " double-tag");
            }
        }

        u1Quit = (UINT1) CliPrintf (CliHandle, "\n!\r\n");
        if (u1Quit == CLI_FAILURE)
        {
            ISS_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return;
        }

        i4PrevFilter = i4NextFilter;

        i1OutCome =
            nmhGetNextIndexIssExtL3FilterTable (i4PrevFilter, &i4NextFilter);

    }

    /*MacAccessLists */

    CliPrintf (CliHandle, "\r\n");

    i1OutCome = nmhGetFirstIndexIssExtL2FilterTable (&i4NextFilter);

    while (i1OutCome != SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "mac access-list extended %d\r\n", i4NextFilter);

        nmhGetIssExtL2FilterPriority (i4NextFilter, &i4FltrPrio);
        nmhGetIssExtL2FilterEtherType (i4NextFilter, &i4EtherType);
        nmhGetIssExtL2FilterProtocolType (i4NextFilter, (UINT4 *) &i4Protocol);
        nmhGetIssExtL2FilterVlanId (i4NextFilter, &i4VlanId);
        nmhGetIssExtL2FilterDstMacAddr (i4NextFilter,
                                        (tMacAddr *) & DstMacAddr);
        nmhGetIssExtL2FilterSrcMacAddr (i4NextFilter,
                                        (tMacAddr *) & SrcMacAddr);
        AclPbGetL2Filter (i4NextFilter, &i4OuterEType, &i4SVlan, &i4SVlanPrio,
                          &i4CVlanPrio, &i4TagType);

        nmhGetIssExtL2FilterAction (i4NextFilter, &i4FltrAction);

        if (i4FltrAction == ISS_ALLOW)

        {
            CliPrintf (CliHandle, " permit ");
        }
        else if (i4FltrAction == ISS_DROP)
        {
            CliPrintf (CliHandle, " deny ");
        }

        CLI_MEMSET (au1String, 0, ISS_ADDR_LEN);

        if (CLI_MEMCMP (SrcMacAddr, zeroAddr, MAC_ADDR_LEN) != 0)
        {
            PrintMacAddress (SrcMacAddr, (UINT1 *) pu1String);
            CliPrintf (CliHandle, "host %s", pu1String);
        }
        else
        {
            CliPrintf (CliHandle, " any ");
        }

        CLI_MEMSET (au1String, 0, ISS_ADDR_LEN);

        if (CLI_MEMCMP (DstMacAddr, zeroAddr, MAC_ADDR_LEN) != 0)
        {
            PrintMacAddress (DstMacAddr, (UINT1 *) pu1String);
            CliPrintf (CliHandle, "host %s", pu1String);
        }
        else
        {
            CliPrintf (CliHandle, "any ");
        }

        switch (i4Protocol)
        {
            case AARP:
                CliPrintf (CliHandle, "aarp ");
                break;

            case AMBER:
                CliPrintf (CliHandle, "amber ");
                break;

            case DEC_SPANNING:
                CliPrintf (CliHandle, "dec-spanning ");
                break;

            case DIAGNOSTIC:
                CliPrintf (CliHandle, "diagnostic ");
                break;

            case DSM:
                CliPrintf (CliHandle, "dsm ");
                break;

            case ETYPE_6000:
                CliPrintf (CliHandle, "etype-6000 ");
                break;
            case ETYPE_8042:
                CliPrintf (CliHandle, "etype-8042");
                break;

            case LAT:
                CliPrintf (CliHandle, "lat ");
                break;

            case LAVC_SCA:
                CliPrintf (CliHandle, "lavc-sca ");
                break;

            case MOP_CONSOLE:
                CliPrintf (CliHandle, "mop-console ");
                break;

            case MSDOS:
                CliPrintf (CliHandle, "msdos ");
                break;

            case MUMPS:
                CliPrintf (CliHandle, "mumps ");
                break;

            case NET_BIOS:
                CliPrintf (CliHandle, "net-bios ");
                break;

            case VINES_ECHO:
                CliPrintf (CliHandle, "vines-echo ");
                break;

            case VINES_IP:
                CliPrintf (CliHandle, "vines-ip ");
                break;
            case XNS_ID:
                CliPrintf (CliHandle, "xns-id ");
                break;

            default:
                if (i4Protocol != 0)
                {
                    CliPrintf (CliHandle, "%d ", i4Protocol);
                }

                break;
        }

        if (i4EtherType != 0)
        {
            CliPrintf (CliHandle, "encaptype %d ", i4EtherType);
        }

        if (i4VlanId != 0)
        {
            CliPrintf (CliHandle, "vlan %d ", i4VlanId);
        }

        if (i4FltrPrio != 1)
        {
            CliPrintf (CliHandle, "priority %d ", i4FltrPrio);
        }
        if (i4OuterEType != ACL_DEF_ETYPE)
        {
            CliPrintf (CliHandle, " outerEtherType %d", i4OuterEType);
        }
        if (i4SVlan != ACL_DEF_SVLAN)
        {
            CliPrintf (CliHandle, " svlan-id %d", i4SVlan);
        }

        if (i4CVlanPrio != ACL_DEF_CVLAN_PRIO)
        {
            CliPrintf (CliHandle, " cvlan-priority %d", i4CVlanPrio);
        }
        if (i4SVlanPrio != ACL_DEF_SVLAN_PRIO)
        {
            CliPrintf (CliHandle, " svlan-priority %d", i4SVlanPrio);
        }

        if (i4TagType != ISS_FILTER_SINGLE_TAG)
        {
            CliPrintf (CliHandle, " double-tag");
        }

        u1Quit = (UINT1) CliPrintf (CliHandle, "\n!\r\n");

        if (u1Quit == CLI_FAILURE)
        {
            ISS_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            return;
        }

        i4PrevFilter = i4NextFilter;

        i1OutCome =
            nmhGetNextIndexIssExtL2FilterTable (i4PrevFilter, &i4NextFilter);

    }
    ISS_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return;

}

/*****************************************************************************/
/*     FUNCTION NAME    : AclShowRunningConfigInterfaceDetails               */
/*                                                                           */
/*     DESCRIPTION      : This function displays the  Interface objects in ACL*/
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
AclShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index)
{

    tSNMP_OCTET_STRING_TYPE InPortList;
    tSNMP_OCTET_STRING_TYPE OutPortList;

    UINT1               au1InPortList[ISS_PORTLIST_LEN];
    UINT1               au1OutPortList[ISS_PORTLIST_LEN];
    INT4                i4Direction;
    INT4                i4Count;
    INT4                i4NextFilter;
    INT4                i4PrevFilter;
    INT4                i4RowStatus;
    INT1                i1OutCome;

    InPortList.pu1_OctetList = &au1InPortList[0];
    InPortList.i4_Length = ISS_PORTLIST_LEN;

    OutPortList.pu1_OctetList = &au1OutPortList[0];
    OutPortList.i4_Length = ISS_PORTLIST_LEN;

    CliRegisterLock (CliHandle, IssLock, IssUnLock);
    ISS_LOCK ();

    i4Count = 1;
    do
    {
        if (i4Index == -1)
        {
            CliPrintf (CliHandle, "interface gigabitethernet 0/%d\r\n",
                       i4Count);
        }
        else
        {
            i4Count = i4Index;
        }
        /*Ip Access Lists */
        i1OutCome = nmhGetFirstIndexIssExtL3FilterTable (&i4NextFilter);

        while (i1OutCome != SNMP_FAILURE)
        {

            MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
            MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);

            nmhGetIssExtL3FilterStatus (i4NextFilter, &i4RowStatus);

            if (i4RowStatus == ISS_ACTIVE)
            {
                nmhGetIssExtL3FilterDirection (i4NextFilter, &i4Direction);
                nmhGetIssExtL3FilterInPortList (i4NextFilter, &InPortList);
                nmhGetIssExtL3FilterOutPortList (i4NextFilter, &OutPortList);

                if (i4Direction == ACL_ACCESS_IN)
                {
                    if (CliIsMemberPort (InPortList.pu1_OctetList,
                                         ISS_PORTLIST_LEN,
                                         i4Count) == CLI_SUCCESS)
                    {
                        CliPrintf (CliHandle, " ip access-group %d in\r\n",
                                   i4NextFilter);
                    }

                }
                if (i4Direction == ACL_ACCESS_OUT)
                {

                    if (CliIsMemberPort (OutPortList.pu1_OctetList,
                                         ISS_PORTLIST_LEN,
                                         i4Count) == CLI_SUCCESS)
                    {
                        CliPrintf (CliHandle, " ip access-group %d out\r\n",
                                   i4NextFilter);
                    }

                }
            }

            i4PrevFilter = i4NextFilter;
            i1OutCome =
                nmhGetNextIndexIssExtL3FilterTable (i4PrevFilter,
                                                    &i4NextFilter);
        }

        /*MacAccessLists */

        i1OutCome = nmhGetFirstIndexIssExtL2FilterTable (&i4NextFilter);

        while (i1OutCome != SNMP_FAILURE)
        {

            MEMSET (au1InPortList, 0, ISS_PORTLIST_LEN);
            MEMSET (au1OutPortList, 0, ISS_PORTLIST_LEN);

            nmhGetIssExtL2FilterStatus (i4NextFilter, &i4RowStatus);

            if (i4RowStatus == ISS_ACTIVE)
            {
                nmhGetIssExtL2FilterInPortList (i4NextFilter, &InPortList);

                if (CliIsMemberPort (InPortList.pu1_OctetList,
                                     ISS_PORTLIST_LEN, i4Count) == CLI_SUCCESS)
                {
                    CliPrintf (CliHandle, " mac access-group %d in\r\n",
                               i4NextFilter);
                }
            }
            else
            {
                if (CliIsMemberPort (InPortList.pu1_OctetList,
                                     ISS_PORTLIST_LEN, i4Index) == CLI_SUCCESS)
                {
                    CliPrintf (CliHandle, " mac access-group %d in\r\n",
                               i4NextFilter);
                }

            }
            i4PrevFilter = i4NextFilter;

            i1OutCome =
                nmhGetNextIndexIssExtL2FilterTable (i4PrevFilter,
                                                    &i4NextFilter);
        }
        i4Count++;
        if (i4Index == -1)
        {
            CliPrintf (CliHandle, "!\r\n");
        }

    }
    while ((i4Count <= SYS_DEF_MAX_PHYSICAL_INTERFACES) && (i4Index == -1));
    ISS_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : StdAclGetCfgPrompt                                 */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
StdAclGetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    INT4                i4StdAclIndex;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    /* NULL is passed to return "config-std-nacl" as the prompt 
     * for the mode  */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-std-nacl");
        return TRUE;
    }

    u4Len = STRLEN (CLI_STDACL_MODE);

    if (STRNCMP (pi1ModeName, CLI_STDACL_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    i4StdAclIndex = CLI_ATOI (pi1ModeName);

    /* Set the mode information for CLI */
    CLI_SET_STDACLID (i4StdAclIndex);

    STRCPY (pi1DispStr, "(config-std-nacl)#");
    return TRUE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ExtAclGetCfgPrompt                                 */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
ExtAclGetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    INT4                i4ExtAclIndex;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    /* NULL is passed to return "config-std-nacl" as the prompt 
     * for the mode  */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-ext-nacl");
        return TRUE;
    }

    u4Len = STRLEN (CLI_EXTACL_MODE);
    if (STRNCMP (pi1ModeName, CLI_EXTACL_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    i4ExtAclIndex = CLI_ATOI (pi1ModeName);

    /* Set the mode information for CLI */
    CLI_SET_EXTACL (i4ExtAclIndex);

    STRCPY (pi1DispStr, "(config-ext-nacl)#");
    return TRUE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : MacAclGetCfgPrompt                                 */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
MacAclGetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    INT4                i4MacAclIndex;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    /* NULL is passed to return "config-std-nacl" as the prompt 
     * for the mode  */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-ext-macl");
        return TRUE;
    }

    u4Len = STRLEN (CLI_MACACL_MODE);
    if (STRNCMP (pi1ModeName, CLI_MACACL_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    i4MacAclIndex = CLI_ATOI (pi1ModeName);

    /* Set the mode information for CLI */
    CLI_SET_MACACL (i4MacAclIndex);

    STRCPY (pi1DispStr, "(config-ext-macl)#");
    return TRUE;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclTestIpParams                                    */
/*                                                                           */
/*     DESCRIPTION      : This function tests the IP address, mask for a     */
/*                        filter                                             */
/*                                                                           */
/*     INPUT            : CliHandle - CLI HAndler                            */
/*                        u1Type - whether Source/Destination Ip address     */
/*                        information must be tested                         */
/*                        i4FilterNo - IP Filter Number                      */
/*                        i4IpType - Whether IPaddress and/or mask is user   */
/*                        input                                              */
/*                        u4IpAddr - Ip Address to be tested                 */
/*                        u4IpMask - Ip Mask to be tested                    */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclTestIpParams (UINT1 u1Type, INT4 i4FilterNo, UINT4 u4IpType, UINT4 u4IpAddr,
                 UINT4 u4IpMask)
{
    UINT4               u4ErrCode;

    if (u1Type == ACL_SRC)
    {
        if (u4IpType != ACL_ANY)
        {
            /* Source IP address has been specified */
            if (nmhTestv2IssExtL3FilterSrcIpAddr
                (&u4ErrCode, i4FilterNo, u4IpAddr) == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (u4IpType != ACL_HOST_IP)
            {
                /*Subnet Mask has been specified */
                if (nmhTestv2IssExtL3FilterSrcIpAddrMask (&u4ErrCode,
                                                          i4FilterNo,
                                                          u4IpMask) ==
                    SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
            }
        }
    }
    else if (u1Type == ACL_DST)
    {
        if (u4IpType != ACL_ANY)
        {
            /* Destination IP address has been specified */
            if (nmhTestv2IssExtL3FilterDstIpAddr
                (&u4ErrCode, i4FilterNo, u4IpAddr) == SNMP_FAILURE)
            {
                return (CLI_FAILURE);
            }

            if (u4IpType != ACL_HOST_IP)
            {
                if (nmhTestv2IssExtL3FilterDstIpAddrMask
                    (&u4ErrCode, i4FilterNo, u4IpMask) == SNMP_FAILURE)
                {
                    /*Subnet Mask has been specified */
                    return (CLI_FAILURE);
                }
            }
        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclSetIpParams                                     */
/*                                                                           */
/*     DESCRIPTION      : This function sets the IP address, mask for a      */
/*                        filter                                             */
/*                                                                           */
/*     INPUT            : CliHandle - CLI HAndler                            */
/*                        u1Type - whether Source/Destination Ip address     */
/*                        information must                                   */
/*                        be set                                             */
/*                        i4FilterNo - IP Filter Number                      */
/*                        i4IpType - Whether IPaddress and/or mask is user   */
/*                        input                                              */
/*                        u4IpAddr - Ip Address to be set                    */
/*                        u4IpMask - Ip Maxk to be set                       */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/

INT4
AclSetIpParams (UINT1 u1Type, INT4 i4FilterNo, UINT4 u4IpType, UINT4 u4IpAddr,
                UINT4 u4IpMask)
{
    UINT4               u4IpSubnet;

    if (u1Type == ACL_SRC)
    {
        if (u4IpType != ACL_ANY)
        {
            /* Source IP address has been specified */
            if (u4IpType != ACL_HOST_IP)
            {
                u4IpSubnet = (u4IpAddr & u4IpMask);

                if (nmhSetIssExtL3FilterSrcIpAddr (i4FilterNo, u4IpSubnet)
                    == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                /*Subnet Mask has been specified */
                if (nmhSetIssExtL3FilterSrcIpAddrMask (i4FilterNo,
                                                       u4IpMask) ==
                    SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
            }
            else
            {
                if (nmhSetIssExtL3FilterSrcIpAddr (i4FilterNo, u4IpAddr)
                    == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                if (nmhSetIssExtL3FilterSrcIpAddrMask (i4FilterNo,
                                                       (UINT4)
                                                       CLI_INET_ADDR
                                                       (HOST_MASK)) ==
                    SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
            }
        }
    }
    else if (u1Type == ACL_DST)
    {
        if (u4IpType != ACL_ANY)
        {
            /* Destination IP address has been specified */
            if (u4IpType != ACL_HOST_IP)
            {
                u4IpSubnet = (u4IpAddr & u4IpMask);

                if (nmhSetIssExtL3FilterDstIpAddr (i4FilterNo, u4IpSubnet)
                    == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                if (nmhSetIssExtL3FilterDstIpAddrMask (i4FilterNo,
                                                       u4IpMask) ==
                    SNMP_FAILURE)
                {
                    /*Subnet Mask has been specified */
                    return (CLI_FAILURE);
                }
            }
            else
            {
                if (nmhSetIssExtL3FilterDstIpAddr (i4FilterNo, u4IpAddr)
                    == SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
                if (nmhSetIssExtL3FilterDstIpAddrMask (i4FilterNo,
                                                       (UINT4)
                                                       CLI_INET_ADDR
                                                       (HOST_MASK)) ==
                    SNMP_FAILURE)
                {
                    return (CLI_FAILURE);
                }
            }

        }
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclCliPrintPortList                                */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Port list               */
/*                                                                           */
/*     INPUT            :  piIfName - Port Name                              */
/*                         CliHandle-CLI Handler                             */
/*                         i4CommaCount- position of port in port list       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

VOID
AclCliPrintPortList (tCliHandle CliHandle, INT4 i4CommaCount, UINT1 *piIfName)
{
    INT4                i4Times;
    INT1                i1Loop;

    i4Times = ISS_PORTLIST_LEN * 2;

    if (i4CommaCount == 1)
    {
        CliPrintf (CliHandle, "%s", piIfName);
        return;
    }
    for (i1Loop = 1; i1Loop < i4Times; i1Loop++)
    {
        /* Only 4 ports per line will be displayed */
        if (i4CommaCount <= (4 * i1Loop))
        {
            if (i4CommaCount == (4 * (i1Loop - 1)) + 1)
            {
                CliPrintf (CliHandle, " \r\n%-33s", " ");
            }
            CliPrintf (CliHandle, " , %s", piIfName);
            break;
        }
    }
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : AclIPv6GetCfgPrompt                                */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/
INT1
AclIPv6GetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = 0;
    INT4                i4IPv6AclIndex = 0;

    if (pi1DispStr == NULL)
    {
        return FALSE;
    }

    /* NULL is passed to return "config-ipv6-acl" as the prompt
       for the mode  */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-ipv6-acl");
        return TRUE;
    }

    u4Len = STRLEN (CLI_IPV6ACL_MODE);
    if (STRNCMP (pi1ModeName, CLI_IPV6ACL_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;

    i4IPv6AclIndex = CLI_ATOI (pi1ModeName);

    /* Set the mode information for CLI */
    CLI_SET_IPV6ACL (i4IPv6AclIndex);
    STRCPY (pi1DispStr, "(config-ipv6-acl)#");
    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UserDefAclGetCfgPrompt                             */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the Class map prompt in pi1DispStr if  */
/*                        valid.                                             */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/

INT1
UserDefAclGetCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    INT4                i4UserDefAclIndex;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    /* NULL is passed to return "config-std-nacl" as the prompt 
     * for the mode  */
    if (pi1ModeName == NULL)
    {
        STRCPY (pi1DispStr, "config-userdef-acl");
        return TRUE;
    }

    u4Len = STRLEN (CLI_USRDEFACL_MODE);
    if (STRNCMP (pi1ModeName, CLI_USRDEFACL_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    i4UserDefAclIndex = CLI_ATOI (pi1ModeName);

    /* Set the mode information for CLI */
    CLI_SET_USERDEFFACL (i4UserDefAclIndex);

    STRCPY (pi1DispStr, "(config-userdef-acl)#");
    return TRUE;
}
#endif
