/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: aclmcli.c,v 1.1.1.1 2008/06/13 12:16:16 premap-iss Exp $
 *
 * Description: This file contains Metro ACL configuration routines
 *********************************************************************/
#ifndef __ACLMCLI_C__
#define __ACLMCLI_C__

#include "lr.h"
#include "issexinc.h"
#include "aclmcli.h"
#include "fsissmlw.h"

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclPbTestL3Filter                                  */
/*                                                                           */
/*     DESCRIPTION      : This function Tests Provider Bribdge L3 Filter     */
/*                        objects                                            */
/*                                                                           */
/*     INPUT            : i4Filter - MAC ACL number                          */
/*                        i4SVlan - service vlan id                          */
/*                        i4SVlanPrio - Service Vlan Priority                */
/*                        i4CVlan     - Customer vlan id                     */
/*                        i4CVlanPrio - Customer Vlan Priority               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclPbTestL3Filter (UINT4 *pu4ErrCode, tCliHandle CliHandle, INT4 i4Filter,
                   INT4 i4SVlan, INT4 i4SVlanPrio, INT4 i4CVlan,
                   INT4 i4CVlanPrio)
{
    UNUSED_PARAM (CliHandle);

    if (i4SVlan != ISS_ZERO_ENTRY)
    {
        if (nmhTestv2IssMetroL3FilterSVlanId (pu4ErrCode, i4Filter, i4SVlan)
            == SNMP_FAILURE)
        {
            return (SNMP_FAILURE);
        }
    }

    if (i4SVlanPrio != ACL_DEF_SVLAN_PRIO)
    {
        if (nmhTestv2IssMetroL3FilterSVlanPriority
            (pu4ErrCode, i4Filter, i4SVlanPrio) == SNMP_FAILURE)
        {
            return (SNMP_FAILURE);
        }
    }

    if (i4CVlan != ISS_ZERO_ENTRY)
    {
        if (nmhTestv2IssMetroL3FilterCVlanId (pu4ErrCode, i4Filter, i4CVlan)
            == SNMP_FAILURE)
        {
            return (SNMP_FAILURE);
        }
    }

    if (i4CVlanPrio != ACL_DEF_CVLAN_PRIO)
    {
        if (nmhTestv2IssMetroL3FilterCVlanPriority
            (pu4ErrCode, i4Filter, i4CVlanPrio) == SNMP_FAILURE)
        {
            return (SNMP_FAILURE);
        }
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclPbGetL3Filter                                   */
/*                                                                           */
/*     DESCRIPTION      : This function Get Provider Bribdge L3 Filter       */
/*                        objects                                            */
/*                                                                           */
/*     INPUT            : i4Filter - MAC ACL number                          */
/*                                                                           */
/*     OUTPUT           : pi4SVlan - service vlan id                         */
/*                        pi4SVlanPrio - Service Vlan Priority               */
/*                        pi4CVlan     - Customer vlan id                    */
/*                        pi4CVlanPrio - Customer Vlan Priority              */
/*                        pi4TagType - Packet tag type on which the filter   */
/*                                     will be applied                       */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclPbGetL3Filter (INT4 i4Filter, INT4 *pi4SVlan, INT4 *pi4SVlanPrio,
                  INT4 *pi4CVlan, INT4 *pi4CVlanPrio, INT4 *pi4TagType)
{
    nmhGetIssMetroL3FilterSVlanId (i4Filter, pi4SVlan);
    nmhGetIssMetroL3FilterSVlanPriority (i4Filter, pi4SVlanPrio);
    nmhGetIssMetroL3FilterCVlanId (i4Filter, pi4CVlan);
    nmhGetIssMetroL3FilterCVlanPriority (i4Filter, pi4CVlanPrio);
    nmhGetIssMetroL3FilterPacketTagType (i4Filter, pi4TagType);

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclPbGetL2Filter                                   */
/*                                                                           */
/*     DESCRIPTION      : This function Get Provider Bribdge L2 Filter       */
/*                        objects                                            */
/*                                                                           */
/*     INPUT            : i4Filter - MAC ACL number                          */
/*                                                                           */
/*     OUTPUT             pu4OuterEType - Outer EtherType                    */
/*                        pi4SVlan - service vlan id                         */
/*                        pi4SVlanPrio - Service Vlan Priority               */
/*                        pi4CVlanPrio - Customer Vlan Priority              */
/*                        pi4TagType - Packet tag type on which the filter   */
/*                                     will be applied                       */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclPbGetL2Filter (INT4 i4Filter, INT4 *pi4OuterEType, INT4 *pi4SVlan,
                  INT4 *pi4SVlanPrio, INT4 *pi4CVlanPrio, INT4 *pi4TagType)
{
    nmhGetIssMetroL2FilterOuterEtherType (i4Filter, pi4OuterEType);
    nmhGetIssMetroL2FilterSVlanId (i4Filter, pi4SVlan);
    nmhGetIssMetroL2FilterSVlanPriority (i4Filter, pi4SVlanPrio);
    nmhGetIssMetroL2FilterCVlanPriority (i4Filter, pi4CVlanPrio);
    nmhGetIssMetroL2FilterPacketTagType (i4Filter, pi4TagType);

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclPbSetL3Filter                                   */
/*                                                                           */
/*     DESCRIPTION      : This function Set Provider Bribdge L3 Filter       */
/*                        objects                                            */
/*                                                                           */
/*     INPUT            : i4Filter - MAC ACL number                          */
/*                        i4SVlan - service vlan id                          */
/*                        i4SVlanPrio - Service Vlan Priority                */
/*                        i4CVlan     - Customer vlan id                     */
/*                        i4CVlanPrio - Customer Vlan Priority               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclPbSetL3Filter (tCliHandle CliHandle, INT4 i4Filter, INT4 i4SVlan,
                  INT4 i4SVlanPrio, INT4 i4CVlan, INT4 i4CVlanPrio)
{
    if (i4SVlan != ISS_ZERO_ENTRY)
    {
        if (nmhSetIssMetroL3FilterSVlanId (i4Filter, i4SVlan) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (SNMP_FAILURE);
        }
    }

    if (i4SVlanPrio != ACL_DEF_SVLAN_PRIO)
    {
        if (nmhSetIssMetroL3FilterSVlanPriority (i4Filter, i4SVlanPrio)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (SNMP_FAILURE);
        }
    }
    if (i4CVlan != ISS_ZERO_ENTRY)
    {
        if (nmhSetIssMetroL3FilterCVlanId (i4Filter, i4CVlan) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (SNMP_FAILURE);
        }
    }

    if (i4CVlanPrio != ACL_DEF_CVLAN_PRIO)
    {
        if (nmhSetIssMetroL3FilterCVlanPriority (i4Filter, i4CVlanPrio)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (SNMP_FAILURE);
        }
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclPbTestSetL3TagType                              */
/*                                                                           */
/*     DESCRIPTION      : This function validates and sets L3 filter packet  */
/*                        Tag Type for which the filter will be applied      */
/*                                                                           */
/*     INPUT            : i4Filter - MAC ACL number                          */
/*                        i4TagType - Packet Tag Type on which the filter    */
/*                                    be applied                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclPbTestSetL3TagType (UINT4 *pu4ErrCode, tCliHandle CliHandle,
                       INT4 i4Filter, INT4 i4TagType)
{
    if (nmhTestv2IssMetroL3FilterPacketTagType (pu4ErrCode, i4Filter,
                                                i4TagType) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }

    if (nmhSetIssMetroL3FilterPacketTagType (i4Filter, i4TagType)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclPbTestL2Filter                                  */
/*                                                                           */
/*     DESCRIPTION      : This function Test Provider Bribdge L2 Filter      */
/*                        objects                                            */
/*                                                                           */
/*     INPUT            : i4Filter - MAC ACL number                          */
/*                        i4OuterEType - Outer EtherType                     */
/*                        i4SVlan - service vlan id                          */
/*                        i4SVlanPrio - Service Vlan Priority                */
/*                        i4CVlanPrio - Customer Vlan Priority               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclPbTestL2Filter (UINT4 *pu4ErrCode, tCliHandle CliHandle, INT4 i4Filter,
                   INT4 i4OuterEType, INT4 i4SVlan, INT4 i4SVlanPrio,
                   INT4 i4CVlanPrio)
{
    UNUSED_PARAM (CliHandle);

    if (i4OuterEType != 0)
    {
        if (nmhTestv2IssMetroL2FilterOuterEtherType
            (pu4ErrCode, i4Filter, i4OuterEType) == SNMP_FAILURE)
        {
            return (SNMP_FAILURE);
        }
    }
    if (i4SVlan != 0)
    {
        if (nmhTestv2IssMetroL2FilterSVlanId (pu4ErrCode, i4Filter, i4SVlan)
            == SNMP_FAILURE)
        {
            return (SNMP_FAILURE);
        }
    }
    if (i4SVlanPrio != ACL_DEF_SVLAN_PRIO)
    {
        if (nmhTestv2IssMetroL2FilterSVlanPriority
            (pu4ErrCode, i4Filter, i4SVlanPrio) == SNMP_FAILURE)
        {
            return (SNMP_FAILURE);
        }
    }

    if (i4CVlanPrio != ACL_DEF_CVLAN_PRIO)
    {
        if (nmhTestv2IssMetroL2FilterCVlanPriority
            (pu4ErrCode, i4Filter, i4CVlanPrio) == SNMP_FAILURE)
        {
            return (SNMP_FAILURE);
        }
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclPbSetL2Filter                                   */
/*                                                                           */
/*     DESCRIPTION      : This function Set Provider Bribdge L2 Filter       */
/*                        objects                                            */
/*                                                                           */
/*     INPUT            : i4Filter - MAC ACL number                          */
/*                        i4OuterEType - Outer EtherType                     */
/*                        i4SVlan - service vlan id                          */
/*                        i4SVlanPrio - Service Vlan Priority                */
/*                        i4CVlanPrio - Customer Vlan Priority               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclPbSetL2Filter (tCliHandle CliHandle, INT4 i4Filter, INT4 i4OuterEType,
                  INT4 i4SVlan, INT4 i4SVlanPrio, INT4 i4CVlanPrio)
{
    if (i4OuterEType != 0)
    {
        if (nmhSetIssMetroL2FilterOuterEtherType (i4Filter, i4OuterEType) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (SNMP_FAILURE);
        }
    }
    if (i4SVlan != 0)
    {
        if (nmhSetIssMetroL2FilterSVlanId (i4Filter, i4SVlan) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (SNMP_FAILURE);
        }
    }
    if (i4SVlanPrio != ACL_DEF_SVLAN_PRIO)
    {
        if (nmhSetIssMetroL2FilterSVlanPriority (i4Filter, i4SVlanPrio) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (SNMP_FAILURE);
        }
    }

    if (i4CVlanPrio != ACL_DEF_CVLAN_PRIO)
    {
        if (nmhSetIssMetroL2FilterCVlanPriority (i4Filter, i4CVlanPrio) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (SNMP_FAILURE);
        }
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclPbTestSetL2TagType                              */
/*                                                                           */
/*     DESCRIPTION      : This function tests and set Provider Bribdge L2    */
/*                        Filter Tag Type                                    */
/*                                                                           */
/*     INPUT            : i4Filter - MAC ACL number                          */
/*                        i4TagType - Packet Tag Type on which the filter    */
/*                                    be applied                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclPbTestSetL2TagType (UINT4 *pu4ErrCode, tCliHandle CliHandle,
                       INT4 i4Filter, INT4 i4TagType)
{
    if (nmhTestv2IssMetroL2FilterPacketTagType
        (pu4ErrCode, i4Filter, i4TagType) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    if (nmhSetIssMetroL2FilterPacketTagType (i4Filter, i4TagType) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclPbShowL3Filter                                  */
/*                                                                           */
/*     DESCRIPTION      : This function Disaplys Provider Bribdge L3 Filter  */
/*                        objects                                            */
/*                                                                           */
/*     INPUT            : i4SVlan - service vlan id                          */
/*                        i4SVlanPrio - Service Vlan Priority                */
/*                        i4CVlan     - Customer vlan id                     */
/*                        i4CVlanPrio - Customer Vlan Priority               */
/*                        i4TagType - Packet Tag Type on which the filter    */
/*                                    be applied                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclPbShowL3Filter (tCliHandle CliHandle, INT4 i4SVlan, INT4 i4SVlanPrio,
                   INT4 i4CVlan, INT4 i4CVlanPrio, INT4 i4TagType)
{
    CliPrintf (CliHandle, " %-33s: %-17d\r\n", "Service Vlan", i4SVlan);

    if (i4SVlanPrio != -1)
    {
        CliPrintf (CliHandle, " %-33s: %-17d\r\n", "Service Vlan Priority",
                   i4SVlanPrio);
    }
    else
    {
        CliPrintf (CliHandle, " %-33s: %-17s\r\n", "Service Vlan Priority",
                   "None");
    }
    CliPrintf (CliHandle, " %-33s: %-17d\r\n", "Customer Vlan", i4CVlan);

    if (i4CVlanPrio != -1)
    {
        CliPrintf (CliHandle, " %-33s: %-17d\r\n", "Customer Vlan Priority",
                   i4CVlanPrio);
    }
    else
    {
        CliPrintf (CliHandle, " %-33s: %-17s\r\n", "Customer Vlan Priority",
                   "None");
    }
    CliPrintf (CliHandle, " %-33s: ", "Packet Tag Type");
    if (i4TagType == ISS_FILTER_SINGLE_TAG)
    {
        CliPrintf (CliHandle, "%-17s\r\n", "Single-tag");
    }
    else if (i4TagType == ISS_FILTER_DOUBLE_TAG)
    {
        CliPrintf (CliHandle, "%-17s\r\n", "Double-tag");
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : AclPbShowL2Filter                                  */
/*                                                                           */
/*     DESCRIPTION      : This function Displays Provider Bribdge L2 Filter  */
/*                        objects                                            */
/*                                                                           */
/*     INPUT            : i4Filter - MAC ACL number                          */
/*                                                                           */
/*     OUTPUT             i4OuterEType - Outer EtherType                     */
/*                        i4SVlan - service vlan id                          */
/*                        i4SVlanPrio - Service Vlan Priority                */
/*                        i4CVlanPrio - Customer Vlan Priority               */
/*                        i4TagType - Packet Tag Type on which the filter    */
/*                                    be applied                             */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
AclPbShowL2Filter (tCliHandle CliHandle, INT4 i4OuterEType, INT4 i4SVlan,
                   INT4 i4SVlanPrio, INT4 i4CVlanPrio, INT4 i4TagType)
{
    CliPrintf (CliHandle, " %-33s: %-17x\r\n", "Outer EtherType", i4OuterEType);
    CliPrintf (CliHandle, " %-33s: %-17d\r\n", "Service Vlan", i4SVlan);

    if (i4SVlanPrio != -1)
    {
        CliPrintf (CliHandle, " %-33s: %-17d\r\n", "Service Vlan Priority",
                   i4SVlanPrio);
    }
    else
    {
        CliPrintf (CliHandle, " %-33s: %-17s\r\n", "Service Vlan Priority",
                   "None");
    }

    if (i4CVlanPrio != -1)
    {
        CliPrintf (CliHandle, " %-33s: %-17d\r\n", "Customer Vlan Priority",
                   i4CVlanPrio);
    }
    else
    {
        CliPrintf (CliHandle, " %-33s: %-17s\r\n", "Customer Vlan Priority",
                   "None");
    }
    CliPrintf (CliHandle, " %-33s: ", "Packet Tag Type");
    if (i4TagType == ISS_FILTER_SINGLE_TAG)
    {
        CliPrintf (CliHandle, "%-17s\r\n", "Single-tag");
    }
    else if (i4TagType == ISS_FILTER_DOUBLE_TAG)
    {
        CliPrintf (CliHandle, "%-17s\r\n", "Double-tag");
    }

    return SNMP_SUCCESS;
}

#endif
