/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsisselw.c,v 1.10 2016/03/19 12:59:56 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "iss.h"
# include  "issexinc.h"
# include  "fsisselw.h"
# include  "isscli.h"
# include  "aclcli.h"
# include  "diffsrv.h"
# include  "qosxtd.h"

/* LOW LEVEL Routines for Table : IssExtRateCtrlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssExtRateCtrlTable
 Input       :  The Indices
                IssExtRateCtrlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssExtRateCtrlTable (INT4 i4IssExtRateCtrlIndex)
{

    if (IssExtSnmpLowValidateIndexPortRateTable (i4IssExtRateCtrlIndex) ==
        ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssExtRateCtrlTable
 Input       :  The Indices
                IssExtRateCtrlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssExtRateCtrlTable (INT4 *pi4IssExtRateCtrlIndex)
{
    return (nmhGetNextIndexIssExtRateCtrlTable (0, pi4IssExtRateCtrlIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssExtRateCtrlTable
 Input       :  The Indices
                IssExtRateCtrlIndex
                nextIssRateCtrlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssExtRateCtrlTable (INT4 i4IssExtRateCtrlIndex,
                                    INT4 *pi4NextIssRateCtrlIndex)
{
    INT4                i4Count = 0;

    if (i4IssExtRateCtrlIndex < 0)
    {
        return SNMP_FAILURE;
    }

    for (i4Count = i4IssExtRateCtrlIndex + 1; i4Count <= ISS_MAX_PORTS;
         i4Count++)
    {
        if ((gIssExGlobalInfo.apIssRateCtrlEntry[i4Count]) != NULL)
        {
            *pi4NextIssRateCtrlIndex = i4Count;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIssExtRateCtrlDLFLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                retValIssExtRateCtrlDLFLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtRateCtrlDLFLimitValue (INT4 i4IssExtRateCtrlIndex,
                                   INT4 *pi4RetValIssExtRateCtrlDLFLimitValue)
{
    tIssRateCtrlEntry  *pIssExtRateCtrlEntry;

    if (IssExtValidateRateCtrlEntry (i4IssExtRateCtrlIndex) != ISS_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pIssExtRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssExtRateCtrlIndex];

    *pi4RetValIssExtRateCtrlDLFLimitValue =
        (INT4) pIssExtRateCtrlEntry->u4IssRateCtrlDLFLimitValue;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssExtRateCtrlBCASTLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                retValIssExtRateCtrlBCASTLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtRateCtrlBCASTLimitValue (INT4 i4IssExtRateCtrlIndex,
                                     INT4
                                     *pi4RetValIssExtRateCtrlBCASTLimitValue)
{
    tIssRateCtrlEntry  *pIssExtRateCtrlEntry;

    if (IssExtValidateRateCtrlEntry (i4IssExtRateCtrlIndex) != ISS_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pIssExtRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssExtRateCtrlIndex];

    *pi4RetValIssExtRateCtrlBCASTLimitValue =
        (INT4) pIssExtRateCtrlEntry->u4IssRateCtrlBCASTLimitValue;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssExtRateCtrlMCASTLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                retValIssExtRateCtrlMCASTLimitValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtRateCtrlMCASTLimitValue (INT4 i4IssExtRateCtrlIndex,
                                     INT4
                                     *pi4RetValIssExtRateCtrlMCASTLimitValue)
{
    tIssRateCtrlEntry  *pIssExtRateCtrlEntry;

    if (IssExtValidateRateCtrlEntry (i4IssExtRateCtrlIndex) != ISS_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pIssExtRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssExtRateCtrlIndex];

    *pi4RetValIssExtRateCtrlMCASTLimitValue =
        (INT4) pIssExtRateCtrlEntry->u4IssRateCtrlMCASTLimitValue;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssExtRateCtrlPortRateLimit
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                retValIssExtRateCtrlPortRateLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtRateCtrlPortRateLimit (INT4 i4IssExtRateCtrlIndex,
                                   INT4 *pi4RetValIssExtRateCtrlPortRateLimit)
{
    tIssRateCtrlEntry  *pIssExtRateCtrlEntry = NULL;

    pIssExtRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssExtRateCtrlIndex];

    if (pIssExtRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIssExtRateCtrlPortRateLimit =
        pIssExtRateCtrlEntry->i4IssRateCtrlPortLimitRate;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssExtRateCtrlPortBurstSize
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                retValIssExtRateCtrlPortBurstSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtRateCtrlPortBurstSize (INT4 i4IssExtRateCtrlIndex,
                                   INT4 *pi4RetValIssExtRateCtrlPortBurstSize)
{
    tIssRateCtrlEntry  *pIssExtRateCtrlEntry = NULL;

    pIssExtRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssExtRateCtrlIndex];

    if (pIssExtRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIssExtRateCtrlPortBurstSize =
        pIssExtRateCtrlEntry->i4IssRateCtrlPortBurstRate;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssExtDefaultRateCtrlStatus
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                retValIssExtDefaultRateCtrlStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetIssExtDefaultRateCtrlStatus(INT4 i4IssExtRateCtrlIndex , INT4 *pi4RetValIssExtDefaultRateCtrlStatus)
{
  UNUSED_PARAM(i4IssExtRateCtrlIndex);
  UNUSED_PARAM (*pi4RetValIssExtDefaultRateCtrlStatus);
  return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIssExtRateCtrlDLFLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                setValIssExtRateCtrlDLFLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtRateCtrlDLFLimitValue (INT4 i4IssExtRateCtrlIndex,
                                   INT4 i4SetValIssExtRateCtrlDLFLimitValue)
{
    tIssRateCtrlEntry  *pIssExtRateCtrlEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    pIssExtRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssExtRateCtrlIndex];

    if (pIssExtRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssExtRateCtrlEntry->u4IssRateCtrlDLFLimitValue == (UINT4)
        i4SetValIssExtRateCtrlDLFLimitValue)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    i4RetVal = IsssysIssHwSetRateLimitingValue ((UINT4) i4IssExtRateCtrlIndex,
                                                ISS_RATE_DLF,
                                                i4SetValIssExtRateCtrlDLFLimitValue);

    if (i4RetVal != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif

    pIssExtRateCtrlEntry->u4IssRateCtrlDLFLimitValue = (UINT4)
        i4SetValIssExtRateCtrlDLFLimitValue;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssExtRateCtrlBCASTLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                setValIssExtRateCtrlBCASTLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtRateCtrlBCASTLimitValue (INT4 i4IssExtRateCtrlIndex,
                                     INT4 i4SetValIssExtRateCtrlBCASTLimitValue)
{
    tIssRateCtrlEntry  *pIssExtRateCtrlEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    pIssExtRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssExtRateCtrlIndex];

    if (pIssExtRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssExtRateCtrlEntry->u4IssRateCtrlBCASTLimitValue == (UINT4)
        i4SetValIssExtRateCtrlBCASTLimitValue)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    i4RetVal = IsssysIssHwSetRateLimitingValue ((UINT4) i4IssExtRateCtrlIndex,
                                                ISS_RATE_BCAST,
                                                i4SetValIssExtRateCtrlBCASTLimitValue);

    if (i4RetVal != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif

    pIssExtRateCtrlEntry->u4IssRateCtrlBCASTLimitValue = (UINT4)
        i4SetValIssExtRateCtrlBCASTLimitValue;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtRateCtrlMCASTLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                setValIssExtRateCtrlMCASTLimitValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtRateCtrlMCASTLimitValue (INT4 i4IssExtRateCtrlIndex,
                                     INT4 i4SetValIssExtRateCtrlMCASTLimitValue)
{
    tIssRateCtrlEntry  *pIssExtRateCtrlEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    pIssExtRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssExtRateCtrlIndex];

    if (pIssExtRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssExtRateCtrlEntry->u4IssRateCtrlMCASTLimitValue == (UINT4)
        i4SetValIssExtRateCtrlMCASTLimitValue)
    {
        return SNMP_SUCCESS;
    }

#ifdef NPAPI_WANTED
    i4RetVal = IsssysIssHwSetRateLimitingValue ((UINT4) i4IssExtRateCtrlIndex,
                                                ISS_RATE_MCAST,
                                                i4SetValIssExtRateCtrlMCASTLimitValue);

    if (i4RetVal != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif

    pIssExtRateCtrlEntry->u4IssRateCtrlMCASTLimitValue = (UINT4)
        i4SetValIssExtRateCtrlMCASTLimitValue;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssExtRateCtrlPortRateLimit
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                setValIssExtRateCtrlPortRateLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtRateCtrlPortRateLimit (INT4 i4IssExtRateCtrlIndex,
                                   INT4 i4SetValIssExtRateCtrlPortRateLimit)
{
    tIssRateCtrlEntry  *pIssExtRateCtrlEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif
    INT4                i4SetValIssExtRateCtrlPortBurstSize = 0;

    pIssExtRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssExtRateCtrlIndex];

    if (pIssExtRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((INT4) (pIssExtRateCtrlEntry->i4IssRateCtrlPortLimitRate) ==
        i4SetValIssExtRateCtrlPortRateLimit)
    {
        return SNMP_SUCCESS;
    }

    i4SetValIssExtRateCtrlPortBurstSize =
        pIssExtRateCtrlEntry->i4IssRateCtrlPortBurstRate;

#ifdef NPAPI_WANTED
    i4RetVal = IsssysIssHwSetPortEgressPktRate
        ((UINT4) i4IssExtRateCtrlIndex, i4SetValIssExtRateCtrlPortRateLimit,
         i4SetValIssExtRateCtrlPortBurstSize);

    if (i4RetVal != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif
    pIssExtRateCtrlEntry->i4IssRateCtrlPortLimitRate =
        i4SetValIssExtRateCtrlPortRateLimit;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetIssExtRateCtrlPortBurstSize
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                setValIssExtRateCtrlPortBurstSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtRateCtrlPortBurstSize (INT4 i4IssExtRateCtrlIndex,
                                   INT4 i4SetValIssExtRateCtrlPortBurstSize)
{
    tIssRateCtrlEntry  *pIssExtRateCtrlEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif
    INT4                i4SetValIssExtRateCtrlPortRateLimit = 0;

    pIssExtRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssExtRateCtrlIndex];

    if (pIssExtRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((INT4) (pIssExtRateCtrlEntry->i4IssRateCtrlPortBurstRate) ==
        i4SetValIssExtRateCtrlPortBurstSize)
    {
        return SNMP_SUCCESS;
    }

    i4SetValIssExtRateCtrlPortRateLimit =
        pIssExtRateCtrlEntry->i4IssRateCtrlPortLimitRate;

#ifdef NPAPI_WANTED
    i4RetVal = IsssysIssHwSetPortEgressPktRate
        ((UINT4) i4IssExtRateCtrlIndex, i4SetValIssExtRateCtrlPortRateLimit,
         i4SetValIssExtRateCtrlPortBurstSize);

    if (i4RetVal != FNP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif

    pIssExtRateCtrlEntry->i4IssRateCtrlPortBurstRate =
        i4SetValIssExtRateCtrlPortBurstSize;
    return SNMP_SUCCESS;
}


/****************************************************************************
 Function    :  nmhSetIssExtDefaultRateCtrlStatus
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                setValIssExtDefaultRateCtrlStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetIssExtDefaultRateCtrlStatus(INT4 i4IssExtRateCtrlIndex , INT4 i4SetValIssExtDefaultRateCtrlStatus)
{
  UNUSED_PARAM(i4IssExtRateCtrlIndex);
  UNUSED_PARAM (i4SetValIssExtDefaultRateCtrlStatus);
  return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2IssExtRateCtrlDLFLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                testValIssExtRateCtrlDLFLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtRateCtrlDLFLimitValue (UINT4 *pu4ErrorCode,
                                      INT4 i4IssExtRateCtrlIndex,
                                      INT4 i4TestValIssExtRateCtrlDLFLimitValue)
{

    if (IssExtValidateRateCtrlEntry (i4IssExtRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssExtRateCtrlDLFLimitValue < 0)
        || (i4TestValIssExtRateCtrlDLFLimitValue > RATE_LIMIT_MAX_VALUE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtRateCtrlBCASTLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                testValIssExtRateCtrlBCASTLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtRateCtrlBCASTLimitValue (UINT4 *pu4ErrorCode,
                                        INT4 i4IssExtRateCtrlIndex,
                                        INT4
                                        i4TestValIssExtRateCtrlBCASTLimitValue)
{

    if (IssExtValidateRateCtrlEntry (i4IssExtRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssExtRateCtrlBCASTLimitValue < 0)
        || (i4TestValIssExtRateCtrlBCASTLimitValue > RATE_LIMIT_MAX_VALUE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtRateCtrlMCASTLimitValue
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object 
                testValIssExtRateCtrlMCASTLimitValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtRateCtrlMCASTLimitValue (UINT4 *pu4ErrorCode,
                                        INT4 i4IssExtRateCtrlIndex,
                                        INT4
                                        i4TestValIssExtRateCtrlMCASTLimitValue)
{

    if (IssExtValidateRateCtrlEntry (i4IssExtRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssExtRateCtrlMCASTLimitValue < 0)
        || (i4TestValIssExtRateCtrlMCASTLimitValue > RATE_LIMIT_MAX_VALUE))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2IssExtRateCtrlPortRateLimit
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                testValIssExtRateCtrlPortRateLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtRateCtrlPortRateLimit (UINT4 *pu4ErrorCode,
                                      INT4 i4IssExtRateCtrlIndex,
                                      INT4 i4TestValIssExtRateCtrlPortRateLimit)
{
    tIssRateCtrlEntry  *pIssExtRateCtrlEntry;

    if (IssExtValidateRateCtrlEntry (i4IssExtRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pIssExtRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssExtRateCtrlIndex];

    if (pIssExtRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValIssExtRateCtrlPortRateLimit < 0) ||
        (i4TestValIssExtRateCtrlPortRateLimit > 80000000))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtRateCtrlPortBurstSize
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                testValIssExtRateCtrlPortBurstSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtRateCtrlPortBurstSize (UINT4 *pu4ErrorCode,
                                      INT4 i4IssExtRateCtrlIndex,
                                      INT4 i4TestValIssExtRateCtrlPortBurstSize)
{
    tIssRateCtrlEntry  *pIssExtRateCtrlEntry;

    if (IssExtValidateRateCtrlEntry (i4IssExtRateCtrlIndex) != ISS_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pIssExtRateCtrlEntry =
        gIssExGlobalInfo.apIssRateCtrlEntry[i4IssExtRateCtrlIndex];

    if (pIssExtRateCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValIssExtRateCtrlPortBurstSize < 0) ||
        (i4TestValIssExtRateCtrlPortBurstSize > 80000000))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}


/****************************************************************************
 Function    :  nmhTestv2IssExtDefaultRateCtrlStatus
 Input       :  The Indices
                IssExtRateCtrlIndex

                The Object
                testValIssExtDefaultRateCtrlStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2IssExtDefaultRateCtrlStatus(UINT4 *pu4ErrorCode , INT4 i4IssExtRateCtrlIndex , INT4 i4TestValIssExtDefaultRateCtrlStatus)
{
    UNUSED_PARAM(*pu4ErrorCode);
    UNUSED_PARAM(i4IssExtRateCtrlIndex);
    UNUSED_PARAM (i4TestValIssExtDefaultRateCtrlStatus);
    return SNMP_SUCCESS;
}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssExtRateCtrlTable
 Input       :  The Indices
                IssExtRateCtrlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssExtRateCtrlTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IssExtL2FilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssExtL2FilterTable
 Input       :  The Indices
                IssExtL2FilterNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssExtL2FilterTable (INT4 i4IssExtL2FilterNo)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssExtL2FilterNo) != ISS_TRUE)
    {
        return SNMP_FAILURE;
    }
    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_LIST);
    while (pSllNode != NULL)
    {
        pIssExtL2FilterEntry = (tIssL2FilterEntry *) pSllNode;
        if (i4IssExtL2FilterNo == pIssExtL2FilterEntry->i4IssL2FilterNo)
        {
            return SNMP_SUCCESS;
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_LIST, pSllNode);
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterPriority
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterPriority (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                                 INT4 i4Element)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssExtL2FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL &&
        pIssExtL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        return SNMP_FAILURE;
    }

    if ((i4Element <= ISS_MAX_FILTER_PRIORITY) &&
        (i4Element >= ISS_DEFAULT_FILTER_PRIORITY))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterPriority
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterPriority (INT4 i4IssExtL2FilterNo, INT4 i4Element)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        pIssExtL2FilterEntry->i4IssL2FilterPriority = i4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterPriority
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterPriority (INT4 i4IssExtL2FilterNo, INT4 *pElement)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        *pElement = pIssExtL2FilterEntry->i4IssL2FilterPriority;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterEtherType
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                testValIssExtL2FilterEtherType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterEtherType (UINT4 *pu4ErrorCode,
                                  INT4 i4IssExtL2FilterNo,
                                  INT4 i4TestValIssExtL2FilterEtherType)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (ISS_IS_L2FILTER_ID_VALID (i4IssExtL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssExtL2FilterEtherType < 0) ||
        (i4TestValIssExtL2FilterEtherType > 65535))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterEtherType
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                setValIssExtL2FilterEtherType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterEtherType (INT4 i4IssExtL2FilterNo,
                               INT4 i4SetValIssExtL2FilterEtherType)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        pIssExtL2FilterEntry->u2InnerEtherType = (UINT2)
            i4SetValIssExtL2FilterEtherType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterEtherType
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                retValIssExtL2FilterEtherType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterEtherType (INT4 i4IssExtL2FilterNo,
                               INT4 *pi4RetValIssExtL2FilterEtherType)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        *pi4RetValIssExtL2FilterEtherType =
            pIssExtL2FilterEntry->u2InnerEtherType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterProtocolType
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterProtocolType (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                                     UINT4 u4Element)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssExtL2FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((u4Element < ISS_MIN_PROTOCOL_ID) && (u4Element > ISS_MAX_PROTOCOL_ID))
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterProtocolType
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterProtocolType (INT4 i4IssExtL2FilterNo, UINT4 u4Element)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        pIssExtL2FilterEntry->u4IssL2FilterProtocolType = u4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterProtocolType
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterProtocolType (INT4 i4IssExtL2FilterNo, UINT4 *pElement)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        *pElement = pIssExtL2FilterEntry->u4IssL2FilterProtocolType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterDstMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterDstMacAddr (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                                   tMacAddr pElement)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    UNUSED_PARAM (pElement);

    if (ISS_IS_L2FILTER_ID_VALID (i4IssExtL2FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterDstMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterDstMacAddr (INT4 i4IssExtL2FilterNo, tMacAddr MacAddress)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        ISS_MEMSET (pIssExtL2FilterEntry->IssL2FilterDstMacAddr, 0,
                    ISS_ETHERNET_ADDR_SIZE);
        ISS_MEMCPY (pIssExtL2FilterEntry->IssL2FilterDstMacAddr,
                    MacAddress, ISS_ETHERNET_ADDR_SIZE);

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterDstMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterDstMacAddr (INT4 i4IssExtL2FilterNo, tMacAddr * pMacAddress)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        ISS_MEMCPY ((UINT1 *) pMacAddress,
                    pIssExtL2FilterEntry->IssL2FilterDstMacAddr,
                    ISS_ETHERNET_ADDR_SIZE);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterSrcMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterSrcMacAddr (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                                   tMacAddr pElement)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    UNUSED_PARAM (pElement);

    if (ISS_IS_L2FILTER_ID_VALID (i4IssExtL2FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterSrcMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterSrcMacAddr (INT4 i4IssExtL2FilterNo, tMacAddr pElement)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        ISS_MEMSET (pIssExtL2FilterEntry->IssL2FilterSrcMacAddr, 0,
                    ISS_ETHERNET_ADDR_SIZE);
        ISS_MEMCPY (pIssExtL2FilterEntry->IssL2FilterSrcMacAddr,
                    pElement, ISS_ETHERNET_ADDR_SIZE);

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterSrcMacAddr
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterSrcMacAddr (INT4 i4IssExtL2FilterNo, tMacAddr * pElement)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        ISS_MEMCPY ((UINT1 *) pElement,
                    pIssExtL2FilterEntry->IssL2FilterSrcMacAddr,
                    ISS_ETHERNET_ADDR_SIZE);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterVlanId
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterVlanId (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                               INT4 i4TestValIssExtL2FilterCustomerVlanId)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssExtL2FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssExtL2FilterCustomerVlanId <= 0) ||
        (i4TestValIssExtL2FilterCustomerVlanId > VLAN_DEV_MAX_VLAN_ID))
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterVlanId
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterVlanId (INT4 i4IssExtL2FilterNo, INT4 i4Element)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        pIssExtL2FilterEntry->u4IssL2FilterCustomerVlanId = (UINT4) i4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterVlanId
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterVlanId (INT4 i4IssExtL2FilterNo, INT4 *pElement)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        *pElement = (INT4) pIssExtL2FilterEntry->u4IssL2FilterCustomerVlanId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterInPortList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterInPortList (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                                   tSNMP_OCTET_STRING_TYPE * pElement)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssExtL2FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pElement->i4_Length <= 0) ||
        (pElement->i4_Length > ISS_PORT_LIST_SIZE))
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    /* This function checks if any port greater than the maximum number of
     * ports in the system is in the port list.*/

    if (IssIsPortListValid (pElement->pu1_OctetList, pElement->i4_Length)
        == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterInPortList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterInPortList (INT4 i4IssExtL2FilterNo,
                                tSNMP_OCTET_STRING_TYPE * pElement)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        ISS_MEMSET (pIssExtL2FilterEntry->IssL2FilterInPortList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_MEMCPY (pIssExtL2FilterEntry->IssL2FilterInPortList,
                    pElement->pu1_OctetList, pElement->i4_Length);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterInPortList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterInPortList (INT4 i4IssExtL2FilterNo,
                                tSNMP_OCTET_STRING_TYPE * pElement)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        ISS_MEMSET (pElement->pu1_OctetList, 0, ISS_PORT_LIST_SIZE);
        ISS_ADD_PORT_LIST (pElement->pu1_OctetList,
                           pIssExtL2FilterEntry->IssL2FilterInPortList);
        pElement->i4_Length = ISS_PORT_LIST_SIZE;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterOutPortList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                testValIssExtL2FilterOutPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterOutPortList (UINT4 *pu4ErrorCode,
                                    INT4 i4IssExtL2FilterNo,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValOutPortList)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssExtL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((pTestValOutPortList->i4_Length <= 0) ||
        (pTestValOutPortList->i4_Length > ISS_PORT_LIST_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    /* This function checks if any port greater than the maximum number of
     * ports in the system is in the port list.*/

    if (IssIsPortListValid (pTestValOutPortList->pu1_OctetList,
                            pTestValOutPortList->i4_Length) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterOutPortList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                setValIssExtL2FilterOutPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterOutPortList (INT4 i4IssExtL2FilterNo,
                                 tSNMP_OCTET_STRING_TYPE * pSetValOutPortList)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        ISS_MEMSET (pIssExtL2FilterEntry->IssL2FilterOutPortList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_MEMCPY (pIssExtL2FilterEntry->IssL2FilterOutPortList,
                    pSetValOutPortList->pu1_OctetList,
                    pSetValOutPortList->i4_Length);

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterOutPortList
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                retValIssExtL2FilterOutPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterOutPortList (INT4 i4IssExtL2FilterNo,
                                 tSNMP_OCTET_STRING_TYPE * pGetValOutPortList)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        ISS_MEMSET (pGetValOutPortList->pu1_OctetList, 0, ISS_PORT_LIST_SIZE);
        ISS_ADD_PORT_LIST (pGetValOutPortList->pu1_OctetList,
                           pIssExtL2FilterEntry->IssL2FilterInPortList);
        pGetValOutPortList->i4_Length = ISS_PORT_LIST_SIZE;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterDirection
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                testValIssExtL2FilterDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterDirection (UINT4 *pu4ErrorCode,
                                  INT4 i4IssExtL2FilterNo,
                                  INT4 i4TestValIssExtL2FilterDirection)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssExtL2FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValIssExtL2FilterDirection != ISS_DIRECTION_IN &&
        i4TestValIssExtL2FilterDirection != ISS_DIRECTION_OUT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterDirection
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                setValIssExtL2FilterDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterDirection (INT4 i4IssExtL2FilterNo,
                               INT4 i4SetValIssExtL2FilterDirection)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        pIssExtL2FilterEntry->u1FilterDirection =
            (UINT1) i4SetValIssExtL2FilterDirection;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterDirection
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                retValIssExtL2FilterDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterDirection (INT4 i4IssExtL2FilterNo,
                               INT4 *pi4RetValIssExtL2FilterDirection)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        *pi4RetValIssExtL2FilterDirection =
            (INT4) pIssExtL2FilterEntry->u1FilterDirection;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterAction
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterAction (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                               INT4 i4Element)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssExtL2FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4Element == ISS_ALLOW) || (i4Element == ISS_DROP) ||
        (i4Element == ISS_FORQOS))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterAction
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterAction (INT4 i4IssExtL2FilterNo, INT4 i4Element)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        pIssExtL2FilterEntry->IssL2FilterAction = i4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterAction
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterAction (INT4 i4IssExtL2FilterNo, INT4 *pElement)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        *pElement = pIssExtL2FilterEntry->IssL2FilterAction;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterMatchCount
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterMatchCount (INT4 i4IssExtL2FilterNo, UINT4 *pElement)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        *pElement = pIssExtL2FilterEntry->u4IssL2FilterMatchCount;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL2FilterStatus
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL2FilterStatus (UINT4 *pu4Error, INT4 i4IssExtL2FilterNo,
                               INT4 i4Element)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    if (ISS_IS_L2FILTER_ID_VALID (i4IssExtL2FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4Element < ISS_ACTIVE) || (i4Element > ISS_DESTROY))
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry == NULL &&
        (i4Element != ISS_CREATE_AND_WAIT && i4Element != ISS_CREATE_AND_GO))
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssExtL2FilterEntry != NULL &&
        (i4Element == ISS_CREATE_AND_WAIT || i4Element == ISS_CREATE_AND_GO))
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssExtL2FilterEntry != NULL &&
        (pIssExtL2FilterEntry->u1IssL2FilterStatus == ISS_NOT_READY &&
         i4Element == ISS_NOT_IN_SERVICE))
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssExtL2FilterEntry != NULL &&
        (pIssExtL2FilterEntry->u1IssL2FilterStatus != ISS_ACTIVE &&
         i4Element == ISS_NOT_IN_SERVICE))
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

#if defined (DIFFSRV_WANTED)
    if ((i4Element == ISS_DESTROY) || (i4Element == ISS_NOT_IN_SERVICE))
    {
        ISS_UNLOCK ();

        if (DsCheckMFClfrTable (i4IssExtL2FilterNo, DS_MAC_FILTER) ==
            DS_FAILURE)
        {
            CLI_SET_ERR (CLI_ACL_FILTER_NO_CHANGE);
            ISS_LOCK ();
            return SNMP_FAILURE;
        }

        ISS_LOCK ();
    }
#endif

#ifdef QOSX_WANTED
    /* Check the RefCount in IssExtL2Filter node -how many times it is used 
     * in the qosxtd Module */
    if ((i4Element == ISS_DESTROY) || (i4Element == ISS_NOT_IN_SERVICE))
    {
        if (pIssExtL2FilterEntry->u4RefCount > 0)
        {
            CLI_SET_ERR (CLI_ACL_FILTER_NO_CHANGE);
            return (SNMP_FAILURE);
        }
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtL2FilterStatus
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL2FilterStatus (INT4 i4IssExtL2FilterNo, INT4 i4Element)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        if (pIssExtL2FilterEntry->u1IssL2FilterStatus == (UINT1) i4Element)
        {
            return SNMP_SUCCESS;
        }

    }
    else
    {
        /* This Filter Entry is not there in the Database */
        if ((i4Element != ISS_CREATE_AND_WAIT) &&
            (i4Element != ISS_CREATE_AND_GO))
        {
            return SNMP_FAILURE;
        }
    }

    switch (i4Element)
    {

        case ISS_CREATE_AND_WAIT:
        case ISS_CREATE_AND_GO:

            if ((ISS_L2FILTERENTRY_ALLOC_MEM_BLOCK (pIssExtL2FilterEntry)) ==
                NULL)
            {
                ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "No Free L2 Filter Entry"
                         "or h/w init failed at startup\n");
                return SNMP_FAILURE;
            }

            ISS_MEMSET (pIssExtL2FilterEntry, 0, sizeof (tIssL2FilterEntry));

            /* Setting all the default values for the objects */
            pIssExtL2FilterEntry->i4IssL2FilterNo = i4IssExtL2FilterNo;
            pIssExtL2FilterEntry->i4IssL2FilterPriority =
                ISS_DEFAULT_FILTER_PRIORITY;

            pIssExtL2FilterEntry->u4IssL2FilterProtocolType =
                ISS_DEFAULT_PROTOCOL_TYPE;

            pIssExtL2FilterEntry->i1IssL2FilterCVlanPriority =
                ISS_DEFAULT_VLAN_PRIORITY;
            pIssExtL2FilterEntry->i1IssL2FilterSVlanPriority =
                ISS_DEFAULT_VLAN_PRIORITY;

            pIssExtL2FilterEntry->u4IssL2FilterCustomerVlanId = 0;
            pIssExtL2FilterEntry->u4IssL2FilterServiceVlanId = 0;

            pIssExtL2FilterEntry->u2InnerEtherType = 0;
            pIssExtL2FilterEntry->u2OuterEtherType = 0;
            pIssExtL2FilterEntry->u1IssL2FilterTagType = ISS_FILTER_SINGLE_TAG;

            pIssExtL2FilterEntry->u1FilterDirection = ISS_DIRECTION_IN;

            pIssExtL2FilterEntry->IssL2FilterAction = ISS_ALLOW;

            pIssExtL2FilterEntry->u4IssL2FilterMatchCount = 0;

            /* Adding the Entry to the Filter List */
            ISS_SLL_ADD (&(ISS_L2FILTER_LIST),
                         &(pIssExtL2FilterEntry->IssNextNode));

            /* Setting the RowStatus to NOT_READY if the
             * filter if i4Element is ISS_CREATE_AND_WAIT*/

            pIssExtL2FilterEntry->u1IssL2FilterStatus = ISS_NOT_READY;

            if (i4Element == ISS_CREATE_AND_GO)
            {
#ifdef NPAPI_WANTED
                if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                {
                    i4RetVal =
                        IsssysIssHwUpdateL2Filter (pIssExtL2FilterEntry,
                                                   ISS_L2FILTER_ADD);

                    if (i4RetVal != FNP_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                }
#endif

                pIssExtL2FilterEntry->u1IssL2FilterStatus = ISS_ACTIVE;
            }

            break;

        case ISS_NOT_IN_SERVICE:

#ifdef QOSX_WANTED
            /* This Entry will be deleted from the HW using Delete
             * Classification in the qosxtd Module */
            if (pIssExtL2FilterEntry->IssL2FilterAction == ISS_FORQOS)
            {
                /*This will be deleted in UnmapClass */
                pIssExtL2FilterEntry->u1IssL2FilterStatus = ISS_NOT_IN_SERVICE;

                return (SNMP_SUCCESS);
            }
#endif

#ifdef NPAPI_WANTED
            if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
            {
                /* Call the hardware API to update the filter */
                i4RetVal =
                    IsssysIssHwUpdateL2Filter (pIssExtL2FilterEntry,
                                               ISS_L2FILTER_DELETE);

                if (i4RetVal != FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
#endif

            pIssExtL2FilterEntry->u1IssL2FilterStatus = ISS_NOT_IN_SERVICE;

            break;

        case ISS_ACTIVE:

#ifdef QOSX_WANTED
            /* This Entry will be Added into the HW using Add Policy or 
             * Classification in the qosxtd Module */
            if (pIssExtL2FilterEntry->IssL2FilterAction == ISS_FORQOS)
            {
                pIssExtL2FilterEntry->u1IssL2FilterStatus = ISS_ACTIVE;
                return (SNMP_SUCCESS);
            }
#endif

#ifdef NPAPI_WANTED
            if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
            {
                /* Call the hardware API to add the L2 filter */
                i4RetVal =
                    IsssysIssHwUpdateL2Filter (pIssExtL2FilterEntry,
                                               ISS_L2FILTER_ADD);

                if (i4RetVal != FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
#endif

            pIssExtL2FilterEntry->u1IssL2FilterStatus = ISS_ACTIVE;

            break;

        case ISS_DESTROY:

            if (pIssExtL2FilterEntry->u1IssL2FilterStatus == ISS_ACTIVE)
            {
                if (pIssExtL2FilterEntry->IssL2FilterAction != ISS_FORQOS)
                {
#ifdef NPAPI_WANTED
                    if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                    {
                        /* Call the hardware API to delete the filter */
                        i4RetVal =
                            IsssysIssHwUpdateL2Filter (pIssExtL2FilterEntry,
                                                       ISS_L2FILTER_DELETE);
                        if (i4RetVal != FNP_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                    }
#endif
                }
                else
                {
#ifdef QOSX_WANTED
                    /* This Entry will be deleted from the HW using Delete
                     * Classification in the qosxtd Module */
                    if (pIssExtL2FilterEntry->IssL2FilterAction == ISS_FORQOS)
                    {
                        /*This will be deleted in UnmapClass */
                    }
#endif
                }                /* End of  if - forQoS */
            }
            /* Deleting the node from the list */
            ISS_SLL_DELETE (&(ISS_L2FILTER_LIST),
                            &(pIssExtL2FilterEntry->IssNextNode));
            /* Delete the Entry from the Software */
            if (ISS_L2FILTERENTRY_FREE_MEM_BLOCK (pIssExtL2FilterEntry)
                != MEM_SUCCESS)
            {
                ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "L2 Filter Entry Free Failure\n");
                return SNMP_FAILURE;
            }

            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssExtL2FilterStatus
 Input       :  The Indices
                IssExtL2FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL2FilterStatus (INT4 i4IssExtL2FilterNo, INT4 *pElement)
{
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pIssExtL2FilterEntry = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    if (pIssExtL2FilterEntry != NULL)
    {
        *pElement = (INT4) pIssExtL2FilterEntry->u1IssL2FilterStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssExtL2FilterTable
 Input       :  The Indices
                IssExtL2FilterNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssExtL2FilterTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : IssExtL3FilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssExtL3FilterTable
 Input       :  The Indices
                IssExtL3FilterNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssExtL3FilterTable (INT4 i4IssExtL3FilterNo)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pSllNode = ISS_SLL_FIRST (&ISS_L3FILTER_LIST);

    if (ISS_IS_L3FILTER_ID_VALID (i4IssExtL3FilterNo) != ISS_TRUE)
    {
        return SNMP_FAILURE;
    }
    while (pSllNode != NULL)
    {
        pIssExtL3FilterEntry = (tIssL3FilterEntry *) pSllNode;
        if (pIssExtL3FilterEntry->i4IssL3FilterNo == i4IssExtL3FilterNo)
        {
            return SNMP_SUCCESS;
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L3FILTER_LIST, pSllNode);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterPriority
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterPriority (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                 INT4 i4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssExtL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4Element <= ISS_MAX_FILTER_PRIORITY) &&
        (i4Element >= ISS_DEFAULT_FILTER_PRIORITY))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterPriority
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterPriority (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        pIssExtL3FilterEntry->i4IssL3FilterPriority = i4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterPriority
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterPriority (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        *pElement = pIssExtL3FilterEntry->i4IssL3FilterPriority;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterProtocol
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterProtocol (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                 INT4 i4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssExtL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4Element < 0) || (i4Element > 255))
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterProtocol
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterProtocol (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        pIssExtL3FilterEntry->IssL3FilterProtocol = i4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterProtocol
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterProtocol (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        *pElement = pIssExtL3FilterEntry->IssL3FilterProtocol;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterMessageType
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterMessageType (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                    INT4 i4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssExtL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4Element < 0) || (i4Element > 255))
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterMessageType
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterMessageType (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        pIssExtL3FilterEntry->i4IssL3FilterMessageType = i4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMessageType
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMessageType (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        *pElement = pIssExtL3FilterEntry->i4IssL3FilterMessageType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterMessageCode
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterMessageCode (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                    INT4 i4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssExtL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4Element < 0) || (i4Element > 255))
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterMessageCode
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterMessageCode (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        pIssExtL3FilterEntry->i4IssL3FilterMessageCode = i4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMessageCode
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMessageCode (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        *pElement = pIssExtL3FilterEntry->i4IssL3FilterMessageCode;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterDstIpAddr
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterDstIpAddr (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                  UINT4 u4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssExtL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Allow IP address which are Class A, B, C or D */
    if (!((ISS_IS_ADDR_CLASS_A (u4Element)) ||
          (ISS_IS_ADDR_CLASS_B (u4Element)) ||
          (ISS_IS_ADDR_CLASS_C (u4Element)) ||
          (ISS_IS_ADDR_CLASS_D (u4Element))))
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterDstIpAddr
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterDstIpAddr (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        pIssExtL3FilterEntry->u4IssL3FilterDstIpAddr = u4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterDstIpAddr
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterDstIpAddr (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        *pElement = pIssExtL3FilterEntry->u4IssL3FilterDstIpAddr;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterSrcIpAddr
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterSrcIpAddr (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                  UINT4 u4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssExtL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Allow IP address which are Class A, B or C */
    if (!((ISS_IS_ADDR_CLASS_A (u4Element)) ||
          (ISS_IS_ADDR_CLASS_B (u4Element)) ||
          (ISS_IS_ADDR_CLASS_C (u4Element))))
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterSrcIpAddr
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterSrcIpAddr (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        pIssExtL3FilterEntry->u4IssL3FilterSrcIpAddr = u4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterSrcIpAddr
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterSrcIpAddr (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        *pElement = pIssExtL3FilterEntry->u4IssL3FilterSrcIpAddr;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterDstIpAddrMask
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterDstIpAddrMask (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                      UINT4 u4Element)
{
    UINT1               u1Counter;
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssExtL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* The valid subnet address is from 0 to 32 */
    for (u1Counter = 0; u1Counter <= ISS_MAX_CIDR; ++u1Counter)
    {
        if (gu4IssCidrSubnetMask[u1Counter] == u4Element)
        {
            break;
        }
    }

    if (u1Counter > ISS_MAX_CIDR)
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterDstIpAddrMask
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterDstIpAddrMask (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        pIssExtL3FilterEntry->u4IssL3FilterDstIpAddrMask = u4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterDstIpAddrMask
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterDstIpAddrMask (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        *pElement = pIssExtL3FilterEntry->u4IssL3FilterDstIpAddrMask;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterSrcIpAddrMask
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterSrcIpAddrMask (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                      UINT4 u4Element)
{
    UINT1               u1Counter;
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssExtL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* The valid subnet address is from 0 to 32 */
    for (u1Counter = 0; u1Counter <= ISS_MAX_CIDR; ++u1Counter)
    {
        if (gu4IssCidrSubnetMask[u1Counter] == u4Element)
        {
            break;
        }
    }

    if (u1Counter > ISS_MAX_CIDR)
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterSrcIpAddrMask
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterSrcIpAddrMask (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        pIssExtL3FilterEntry->u4IssL3FilterSrcIpAddrMask = u4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterSrcIpAddrMask
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterSrcIpAddrMask (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        *pElement = pIssExtL3FilterEntry->u4IssL3FilterSrcIpAddrMask;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterMinDstProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterMinDstProtPort (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                       UINT4 u4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pIssExtL3FilterEntry->u4IssL3FilterMaxDstProtPort != 0) &&
        (pIssExtL3FilterEntry->u4IssL3FilterMaxDstProtPort < u4Element))
    {
        return SNMP_FAILURE;
    }

    if (ISS_IS_L3FILTER_ID_VALID (i4IssExtL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4Element <= 65535)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterMinDstProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterMinDstProtPort (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        pIssExtL3FilterEntry->u4IssL3FilterMinDstProtPort = u4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMinDstProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMinDstProtPort (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        *pElement = pIssExtL3FilterEntry->u4IssL3FilterMinDstProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterMaxDstProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterMaxDstProtPort (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                       UINT4 u4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry->u4IssL3FilterMinDstProtPort > u4Element)
    {
        return SNMP_FAILURE;
    }

    if (ISS_IS_L3FILTER_ID_VALID (i4IssExtL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4Element <= 65535)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterMaxDstProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterMaxDstProtPort (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        pIssExtL3FilterEntry->u4IssL3FilterMaxDstProtPort = u4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMaxDstProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMaxDstProtPort (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        *pElement = pIssExtL3FilterEntry->u4IssL3FilterMaxDstProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterMinSrcProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterMinSrcProtPort (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                       UINT4 u4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pIssExtL3FilterEntry->u4IssL3FilterMaxSrcProtPort != 0) &&
        (pIssExtL3FilterEntry->u4IssL3FilterMaxSrcProtPort < u4Element))
    {
        return SNMP_FAILURE;
    }

    if (ISS_IS_L3FILTER_ID_VALID (i4IssExtL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4Element <= 65535)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterMinSrcProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterMinSrcProtPort (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        pIssExtL3FilterEntry->u4IssL3FilterMinSrcProtPort = u4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMinSrcProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMinSrcProtPort (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        *pElement = pIssExtL3FilterEntry->u4IssL3FilterMinSrcProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterMaxSrcProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterMaxSrcProtPort (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                       UINT4 u4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry->u4IssL3FilterMinSrcProtPort > u4Element)
    {
        return SNMP_FAILURE;
    }

    if (ISS_IS_L3FILTER_ID_VALID (i4IssExtL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4Element <= 65535)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterMaxSrcProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterMaxSrcProtPort (INT4 i4IssExtL3FilterNo, UINT4 u4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        pIssExtL3FilterEntry->u4IssL3FilterMaxSrcProtPort = u4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMaxSrcProtPort
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMaxSrcProtPort (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        *pElement = pIssExtL3FilterEntry->u4IssL3FilterMaxSrcProtPort;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterInPortList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterInPortList (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                   tSNMP_OCTET_STRING_TYPE * pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssExtL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pElement->i4_Length <= 0) ||
        (pElement->i4_Length > ISS_PORT_LIST_SIZE))
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (IssIsPortListValid (pElement->pu1_OctetList, pElement->i4_Length)
        == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterInPortList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterInPortList (INT4 i4IssExtL3FilterNo,
                                tSNMP_OCTET_STRING_TYPE * pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        ISS_MEMSET (pIssExtL3FilterEntry->IssL3FilterInPortList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_MEMCPY (pIssExtL3FilterEntry->IssL3FilterInPortList,
                    pElement->pu1_OctetList, pElement->i4_Length);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterInPortList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterInPortList (INT4 i4IssExtL3FilterNo,
                                tSNMP_OCTET_STRING_TYPE * pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        ISS_MEMSET (pElement->pu1_OctetList, 0, ISS_PORT_LIST_SIZE);
        ISS_ADD_PORT_LIST (pElement->pu1_OctetList,
                           pIssExtL3FilterEntry->IssL3FilterInPortList);
        pElement->i4_Length = ISS_PORT_LIST_SIZE;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterOutPortList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterOutPortList (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                    tSNMP_OCTET_STRING_TYPE * pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssExtL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pElement->i4_Length <= 0) ||
        (pElement->i4_Length > ISS_PORT_LIST_SIZE))
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (IssIsPortListValid (pElement->pu1_OctetList, pElement->i4_Length)
        == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterOutPortList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterOutPortList (INT4 i4IssExtL3FilterNo,
                                 tSNMP_OCTET_STRING_TYPE * pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        ISS_MEMSET (pIssExtL3FilterEntry->IssL3FilterOutPortList, 0,
                    ISS_PORT_LIST_SIZE);
        ISS_MEMCPY (pIssExtL3FilterEntry->IssL3FilterOutPortList,
                    pElement->pu1_OctetList, pElement->i4_Length);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterOutPortList
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterOutPortList (INT4 i4IssExtL3FilterNo,
                                 tSNMP_OCTET_STRING_TYPE * pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        ISS_MEMSET (pElement->pu1_OctetList, 0, ISS_PORT_LIST_SIZE);
        ISS_ADD_PORT_LIST (pElement->pu1_OctetList,
                           pIssExtL3FilterEntry->IssL3FilterOutPortList);
        pElement->i4_Length = ISS_PORT_LIST_SIZE;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterAckBit
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterAckBit (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                               INT4 i4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssExtL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4Element >= ISS_ACK_ESTABLISH) && (i4Element <= ISS_ACK_ANY))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterAckBit
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterAckBit (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        pIssExtL3FilterEntry->IssL3FilterAckBit = i4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterAckBit
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterAckBit (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        *pElement = pIssExtL3FilterEntry->IssL3FilterAckBit;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterRstBit
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterRstBit (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                               INT4 i4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssExtL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4Element >= ISS_RST_SET) && (i4Element <= ISS_RST_ANY))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterRstBit
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterRstBit (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        pIssExtL3FilterEntry->IssL3FilterRstBit = i4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterRstBit
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterRstBit (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        *pElement = pIssExtL3FilterEntry->IssL3FilterRstBit;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterTos
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterTos (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                            INT4 i4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssExtL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry->i4IssL3FilterDscp != ISS_DSCP_INVALID)
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4Element >= ISS_TOS_NONE) && (i4Element < ISS_TOS_INVALID))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterTos
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterTos (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        pIssExtL3FilterEntry->IssL3FilterTos = i4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterTos
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterTos (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        *pElement = pIssExtL3FilterEntry->IssL3FilterTos;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterDscp
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                testValIssExtL3FilterDscp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterDscp (UINT4 *pu4ErrorCode, INT4 i4IssExtL3FilterNo,
                             INT4 i4TestValIssExtL3FilterDscp)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;
    if (ISS_IS_L3FILTER_ID_VALID (i4IssExtL3FilterNo) == ISS_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry->IssL3FilterTos != ISS_TOS_INVALID)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (((i4TestValIssExtL3FilterDscp >= ISS_MIN_DSCP_VALUE) &&
         (i4TestValIssExtL3FilterDscp <= ISS_MAX_DSCP_VALUE)) ||
        (i4TestValIssExtL3FilterDscp == -1))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterDscp
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                setValIssExtL3FilterDscp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterDscp (INT4 i4IssExtL3FilterNo,
                          INT4 i4SetValIssExtL3FilterDscp)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        pIssExtL3FilterEntry->i4IssL3FilterDscp = i4SetValIssExtL3FilterDscp;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterDscp
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                retValIssExtL3FilterDscp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterDscp (INT4 i4IssExtL3FilterNo,
                          INT4 *pi4RetValIssExtL3FilterDscp)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        *pi4RetValIssExtL3FilterDscp = pIssExtL3FilterEntry->i4IssL3FilterDscp;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterDirection
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterDirection (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                                  INT4 i4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssExtL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4Element == ISS_DIRECTION_IN) || (i4Element == ISS_DIRECTION_OUT))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterDirection
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterDirection (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        pIssExtL3FilterEntry->IssL3FilterDirection = i4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterDirection
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterDirection (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        *pElement = pIssExtL3FilterEntry->IssL3FilterDirection;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterAction
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterAction (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                               INT4 i4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssExtL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4Element == ISS_ALLOW) || (i4Element == ISS_DROP) ||
        (i4Element == ISS_FORQOS))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterAction
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterAction (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        pIssExtL3FilterEntry->IssL3FilterAction = i4Element;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterAction
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterAction (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        *pElement = pIssExtL3FilterEntry->IssL3FilterAction;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterMatchCount
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterMatchCount (INT4 i4IssExtL3FilterNo, UINT4 *pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        *pElement = pIssExtL3FilterEntry->u4IssL3FilterMatchCount;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2IssExtL3FilterStatus
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssExtL3FilterStatus (UINT4 *pu4Error, INT4 i4IssExtL3FilterNo,
                               INT4 i4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    if (ISS_IS_L3FILTER_ID_VALID (i4IssExtL3FilterNo) == ISS_FALSE)
    {
        *pu4Error = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4Element < ISS_ACTIVE) || (i4Element > ISS_DESTROY))
    {
        *pu4Error = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry == NULL &&
        (i4Element != ISS_CREATE_AND_WAIT && i4Element != ISS_CREATE_AND_GO))
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry != NULL &&
        (i4Element == ISS_CREATE_AND_WAIT || i4Element == ISS_CREATE_AND_GO))
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry != NULL &&
        pIssExtL3FilterEntry->u1IssL3FilterStatus == ISS_NOT_READY &&
        i4Element == ISS_NOT_IN_SERVICE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pIssExtL3FilterEntry != NULL &&
        pIssExtL3FilterEntry->u1IssL3FilterStatus != ISS_ACTIVE &&
        i4Element == ISS_NOT_IN_SERVICE)
    {
        *pu4Error = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
#if defined (DIFFSRV_WANTED)
    if ((i4Element == ISS_DESTROY) || (i4Element == ISS_NOT_IN_SERVICE))
    {
        ISS_UNLOCK ();

        if (DsCheckMFClfrTable (i4IssExtL3FilterNo, DS_IP_FILTER) == DS_FAILURE)
        {
            ISS_LOCK ();
            CLI_SET_ERR (CLI_ACL_FILTER_NO_CHANGE);
            return SNMP_FAILURE;
        }

        ISS_LOCK ();
    }
#endif

#ifdef QOSX_WANTED
    /* Check the RefCount in IssExtL3Filter node -how many times it is used 
     * in the qosxtd Module */
    if ((i4Element == ISS_DESTROY) || (i4Element == ISS_NOT_IN_SERVICE))
    {
        if (pIssExtL3FilterEntry->u4RefCount > 0)
        {
            CLI_SET_ERR (CLI_ACL_FILTER_NO_CHANGE);
            return (SNMP_FAILURE);
        }
    }
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssExtL3FilterStatus
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssExtL3FilterStatus (INT4 i4IssExtL3FilterNo, INT4 i4Element)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;
#ifdef NPAPI_WANTED
    INT4                i4RetVal;
#endif

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        if (pIssExtL3FilterEntry->u1IssL3FilterStatus == (UINT1) i4Element)
        {
            return SNMP_SUCCESS;
        }
    }
    else
    {
        /* This Filter Entry is not there in the Database */
        if ((i4Element != ISS_CREATE_AND_WAIT) &&
            (i4Element != ISS_CREATE_AND_GO))
        {
            return SNMP_FAILURE;
        }
    }

    switch (i4Element)
    {

        case ISS_CREATE_AND_WAIT:
        case ISS_CREATE_AND_GO:

            if ((ISS_L3FILTERENTRY_ALLOC_MEM_BLOCK (pIssExtL3FilterEntry)) ==
                NULL)
            {
                ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "No Free L3 Filter Entry"
                         "or h/w init failed at startup\n");
                return SNMP_FAILURE;
            }

            ISS_MEMSET (pIssExtL3FilterEntry, 0, sizeof (tIssL3FilterEntry));

            /* Setting all the default values for the objects */
            pIssExtL3FilterEntry->i4IssL3FilterNo = i4IssExtL3FilterNo;

            pIssExtL3FilterEntry->i4IssL3FilterPriority =
                ISS_DEFAULT_FILTER_PRIORITY;

            pIssExtL3FilterEntry->IssL3FilterProtocol = ISS_PROTO_ANY;
            pIssExtL3FilterEntry->u4SVlanId = 0;
            pIssExtL3FilterEntry->i1SVlanPriority = ISS_DEFAULT_VLAN_PRIORITY;
            pIssExtL3FilterEntry->u4CVlanId = 0;
            pIssExtL3FilterEntry->i1CVlanPriority = ISS_DEFAULT_VLAN_PRIORITY;
            pIssExtL3FilterEntry->u1IssL3FilterTagType = ISS_FILTER_SINGLE_TAG;
            pIssExtL3FilterEntry->i4IssL3FilterMessageType = ISS_ANY;
            pIssExtL3FilterEntry->i4IssL3FilterMessageCode = ISS_ANY;
            pIssExtL3FilterEntry->u4IssL3FilterDstIpAddr = ISS_ZERO_ENTRY;
            pIssExtL3FilterEntry->u4IssL3FilterSrcIpAddr = ISS_ZERO_ENTRY;
            pIssExtL3FilterEntry->u4IssL3FilterDstIpAddrMask =
                ISS_DEF_FILTER_MASK;
            pIssExtL3FilterEntry->u4IssL3FilterSrcIpAddrMask =
                ISS_DEF_FILTER_MASK;
            pIssExtL3FilterEntry->u4IssL3FilterMinDstProtPort = ISS_ZERO_ENTRY;
            pIssExtL3FilterEntry->u4IssL3FilterMaxDstProtPort =
                ISS_MAX_PORT_VALUE;
            pIssExtL3FilterEntry->u4IssL3FilterMinSrcProtPort = ISS_ZERO_ENTRY;
            pIssExtL3FilterEntry->u4IssL3FilterMaxSrcProtPort =
                ISS_MAX_PORT_VALUE;
            pIssExtL3FilterEntry->IssL3FilterAckBit = ISS_ACK_ANY;
            pIssExtL3FilterEntry->IssL3FilterRstBit = ISS_ACK_ANY;
            pIssExtL3FilterEntry->IssL3FilterTos = ISS_TOS_INVALID;
            pIssExtL3FilterEntry->i4IssL3FilterDscp = ISS_DSCP_INVALID;
            pIssExtL3FilterEntry->IssL3FilterDirection = ISS_DIRECTION_IN;
            pIssExtL3FilterEntry->IssL3FilterAction = ISS_ALLOW;
            pIssExtL3FilterEntry->u4IssL3FilterMatchCount = ISS_ZERO_ENTRY;
            pIssExtL3FilterEntry->u1IssL3FilterStatus = 0;

            /* Adding the Entry to the Filter List */
            ISS_SLL_ADD (&(ISS_L3FILTER_LIST),
                         &(pIssExtL3FilterEntry->IssNextNode));

            /* Setting the RowStatus to NOT_READY as all 
             * mandatory objects are not set taken. */
            pIssExtL3FilterEntry->u1IssL3FilterStatus = ISS_NOT_READY;

            if (i4Element == ISS_CREATE_AND_GO)
            {
#ifdef NPAPI_WANTED
                if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                {
                    /* Call the hardware API to add the L3 filter */
                    i4RetVal =
                        IsssysIssHwUpdateL3Filter (pIssExtL3FilterEntry,
                                                   ISS_L3FILTER_ADD);

                    if (i4RetVal != FNP_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                }
#endif
                pIssExtL3FilterEntry->u1IssL3FilterStatus = ISS_ACTIVE;

            }

            break;

        case ISS_NOT_IN_SERVICE:

#ifdef QOSX_WANTED
            /* This Entry will be deleted from the HW using Delete
             * Classification in the qosxtd Module */
            if (pIssExtL3FilterEntry->IssL3FilterAction == ISS_FORQOS)
            {
                /*This will be deleted in UnmapClass */
                pIssExtL3FilterEntry->u1IssL3FilterStatus = ISS_NOT_IN_SERVICE;
                return (SNMP_SUCCESS);
            }
#endif

#ifdef NPAPI_WANTED
            if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
            {
                /* Call the hardware API to update the filter */
                i4RetVal =
                    IsssysIssHwUpdateL3Filter (pIssExtL3FilterEntry,
                                               ISS_L3FILTER_DELETE);
                if (i4RetVal != FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
#endif

            pIssExtL3FilterEntry->u1IssL3FilterStatus = ISS_NOT_IN_SERVICE;

            return SNMP_SUCCESS;

            break;

        case ISS_ACTIVE:

#ifdef QOSX_WANTED
            /* This Entry will be Added into the HW using Add Policy or 
             * Classification in the qosxtd Module */
            if (pIssExtL3FilterEntry->IssL3FilterAction == ISS_FORQOS)
            {
                pIssExtL3FilterEntry->u1IssL3FilterStatus = ISS_ACTIVE;
                return (SNMP_SUCCESS);
            }
#endif

#ifdef NPAPI_WANTED
            if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
            {
                /* Call the hardware API to add the L3 filter */
                i4RetVal =
                    IsssysIssHwUpdateL3Filter (pIssExtL3FilterEntry,
                                               ISS_L3FILTER_ADD);

                if (i4RetVal != FNP_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
            }
#endif

            pIssExtL3FilterEntry->u1IssL3FilterStatus = ISS_ACTIVE;

            break;

        case ISS_DESTROY:

            if (pIssExtL3FilterEntry->u1IssL3FilterStatus == ISS_ACTIVE)
            {
                if (pIssExtL3FilterEntry->IssL3FilterAction != ISS_FORQOS)
                {
#ifdef NPAPI_WANTED
                    if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                    {
                        /* Call the hardware API to delete the filter */
                        i4RetVal =
                            IsssysIssHwUpdateL3Filter (pIssExtL3FilterEntry,
                                                       ISS_L3FILTER_DELETE);
                        if (i4RetVal != FNP_SUCCESS)
                        {
                            return SNMP_FAILURE;
                        }
                    }
#endif
                }
                else
                {
#ifdef QOSX_WANTED
                    /* This Entry will be deleted from the HW using Delete
                     * Classification in the qosxtd Module */
                    if (pIssExtL3FilterEntry->IssL3FilterAction == ISS_FORQOS)
                    {
                        /*This will be deleted in UnmapClass */
                    }
#endif
                }                /* End of  if - forQoS */

            }
            /* Deleting the node from the list */
            ISS_SLL_DELETE (&(ISS_L3FILTER_LIST),
                            &(pIssExtL3FilterEntry->IssNextNode));
            /* Delete the Entry from the Software */
            if (ISS_L3FILTERENTRY_FREE_MEM_BLOCK (pIssExtL3FilterEntry)
                != MEM_SUCCESS)
            {
                ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                         "L3 Filter Entry Free Failure\n");
                return SNMP_FAILURE;
            }

            break;

        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssExtL3FilterStatus
 Input       :  The Indices
                IssExtL3FilterNo

                The Object 
                Element
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssExtL3FilterStatus (INT4 i4IssExtL3FilterNo, INT4 *pElement)
{
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pIssExtL3FilterEntry = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    if (pIssExtL3FilterEntry != NULL)
    {
        *pElement = (INT4) pIssExtL3FilterEntry->u1IssL3FilterStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssExtL3FilterTable
 Input       :  The Indices
                IssExtFilterL3No
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssExtL3FilterTable (INT4 *pi4IssExtL3FilterNo)
{
    if ((IssExtSnmpLowGetFirstValidL3FilterTableIndex (pi4IssExtL3FilterNo)) ==
        ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssExtL3FilterTable
 Input       :  The Indices
                IssExtL3FilterNo
                nextIssL3FilterNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssExtL3FilterTable (INT4 i4IssExtL3FilterNo,
                                    INT4 *pi4IssExtL3FilterNoOUT)
{

    if (i4IssExtL3FilterNo < 0)
    {
        return SNMP_FAILURE;
    }

    if ((IssExtSnmpLowGetNextValidL3FilterTableIndex
         (i4IssExtL3FilterNo, pi4IssExtL3FilterNoOUT)) == ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssExtL2FilterTable
 Input       :  The Indices
                IssExtL2FilterNo
                nextIssL2FilterNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssExtL2FilterTable (INT4 i4IssExtL2FilterNo,
                                    INT4 *pi4IssExtL2FilterNoOUT)
{

    if (i4IssExtL2FilterNo < 0)
    {
        return SNMP_FAILURE;
    }

    if ((IssExtSnmpLowGetNextValidL2FilterTableIndex
         (i4IssExtL2FilterNo, pi4IssExtL2FilterNoOUT)) == ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssExtL2FilterTable
 Input       :  The Indices
                IssExtFilterL2No
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssExtL2FilterTable (INT4 *pi4IssExtL2FilterNo)
{
    if ((IssExtSnmpLowGetFirstValidL2FilterTableIndex (pi4IssExtL2FilterNo)) ==
        ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
* Function    :  IssExtSnmpLowValidateIndexPortRateTable
* Input       :  i4CtrlIndex(PortCtrl Table Index or Rate Ctrl Table Index)
* Output      :  None
* Returns     :  ISS_SUCCESS/ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowValidateIndexPortRateTable (INT4 i4PortIndex)
{
    /* This routine is only for Port Ctrl Table */
    if (i4PortIndex > ISS_ZERO_ENTRY && i4PortIndex <= ISS_MAX_PORTS)
    {
        return ISS_SUCCESS;
    }
    return ISS_FAILURE;
}

/****************************************************************************
* Function    :  IssExtValidateRateCtrlEntry
* Input       :  i4RateCtrlIndex
* Output      :  None
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtValidateRateCtrlEntry (INT4 i4RateCtrlIndex)
{
    /* Validating the entry */
    if ((i4RateCtrlIndex <= ISS_ZERO_ENTRY) ||
        (i4RateCtrlIndex > ISS_MAX_PORTS))
    {
        return ISS_FAILURE;
    }

    if ((gIssExGlobalInfo.apIssRateCtrlEntry[i4RateCtrlIndex] == NULL))
    {
        ISS_TRC_ARG1 (DATA_PATH_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                      "ISS: RATE CTRL ENTRY %d DOES NOT EXIST\n",
                      i4RateCtrlIndex);
        return ISS_FAILURE;
    }
    return ISS_SUCCESS;
}

/****************************************************************************
* Function    :  IssExtSnmpLowGetFirstValidL3FilterTableIndex
* Input       :  None
* Output      :  INT4 *pi4FirstL3FilterIndex
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowGetFirstValidL3FilterTableIndex (INT4 *pi4FirstL3FilterIndex)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;
    tIssL3FilterEntry  *pFirstL3FilterEntry = NULL;
    UINT1               u1FoundFlag = ISS_FALSE;

    pSllNode = ISS_SLL_FIRST (&ISS_L3FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssExtL3FilterEntry = (tIssL3FilterEntry *) pSllNode;
        if (u1FoundFlag == ISS_FALSE)
        {
            pFirstL3FilterEntry = pIssExtL3FilterEntry;
            u1FoundFlag = ISS_TRUE;
        }
        else
        {
            if (pFirstL3FilterEntry->i4IssL3FilterNo >
                pIssExtL3FilterEntry->i4IssL3FilterNo)
            {
                pFirstL3FilterEntry = pIssExtL3FilterEntry;
            }
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L3FILTER_LIST, pSllNode);
    }

    if (u1FoundFlag == ISS_TRUE)
    {
        *pi4FirstL3FilterIndex = pFirstL3FilterEntry->i4IssL3FilterNo;
        return ISS_SUCCESS;
    }

    return ISS_FAILURE;
}

/****************************************************************************
* Function    :  IssExtSnmpLowGetFirstValidL3FilterTableIndex
* Input       :  INT4 i4IssExt3FilterNo
* Output      :  INT4 *pi4NextL3FilterNo
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowGetNextValidL3FilterTableIndex (INT4 i4IssExtL3FilterNo,
                                             INT4 *pi4NextL3FilterNo)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;
    tIssL3FilterEntry  *pNextL3FilterEntry = NULL;
    UINT1               u1FoundFlag = ISS_FALSE;

    pSllNode = ISS_SLL_FIRST (&ISS_L3FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssExtL3FilterEntry = (tIssL3FilterEntry *) pSllNode;

        if (i4IssExtL3FilterNo < pIssExtL3FilterEntry->i4IssL3FilterNo)
        {
            if (u1FoundFlag == ISS_FALSE)
            {
                pNextL3FilterEntry = pIssExtL3FilterEntry;
                u1FoundFlag = ISS_TRUE;
            }
            else
            {
                if (pNextL3FilterEntry->i4IssL3FilterNo >
                    pIssExtL3FilterEntry->i4IssL3FilterNo)
                {
                    pNextL3FilterEntry = pIssExtL3FilterEntry;
                }
            }
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L3FILTER_LIST, pSllNode);
    }

    if (u1FoundFlag == ISS_TRUE)
    {
        *pi4NextL3FilterNo = pNextL3FilterEntry->i4IssL3FilterNo;
        return ISS_SUCCESS;
    }
    else
    {
        return ISS_FAILURE;
    }
}

/****************************************************************************
* Function    :  IssExtSnmpLowGetFirstValidL2FilterTableIndex
* Input       :  None
* Output      :  INT4 *pi4FirstL2FilterIndex
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowGetFirstValidL2FilterTableIndex (INT4 *pi4FirstL2FilterIndex)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;
    tIssL2FilterEntry  *pFirstL2FilterEntry = NULL;
    UINT1               u1FoundFlag = ISS_FALSE;

    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssExtL2FilterEntry = (tIssL2FilterEntry *) pSllNode;
        if (u1FoundFlag == ISS_FALSE)
        {
            pFirstL2FilterEntry = pIssExtL2FilterEntry;
            u1FoundFlag = ISS_TRUE;
        }
        else
        {
            if (pFirstL2FilterEntry->i4IssL2FilterNo >
                pIssExtL2FilterEntry->i4IssL2FilterNo)
            {
                pFirstL2FilterEntry = pIssExtL2FilterEntry;
            }
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_LIST, pSllNode);
    }

    if (u1FoundFlag == ISS_TRUE)
    {
        *pi4FirstL2FilterIndex = pFirstL2FilterEntry->i4IssL2FilterNo;
        return ISS_SUCCESS;
    }

    return ISS_FAILURE;
}

/****************************************************************************
* Function    :  IssExtSnmpLowGetFirstValidL2FilterTableIndex
* Input       :  INT4 i4IssExtL2FilterNo
* Output      :  INT4 *pi4NextL2FilterNo
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtSnmpLowGetNextValidL2FilterTableIndex (INT4 i4IssExtL2FilterNo,
                                             INT4 *pi4NextL2FilterNo)
{
    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;
    tIssL2FilterEntry  *pNextL2FilterEntry = NULL;
    UINT1               u1FoundFlag = ISS_FALSE;

    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssExtL2FilterEntry = (tIssL2FilterEntry *) pSllNode;

        if (i4IssExtL2FilterNo < pIssExtL2FilterEntry->i4IssL2FilterNo)
        {
            if (u1FoundFlag == ISS_FALSE)
            {
                pNextL2FilterEntry = pIssExtL2FilterEntry;
                u1FoundFlag = ISS_TRUE;
            }
            else
            {
                if (pNextL2FilterEntry->i4IssL2FilterNo >
                    pIssExtL2FilterEntry->i4IssL2FilterNo)
                {
                    pNextL2FilterEntry = pIssExtL2FilterEntry;
                }
            }
        }

        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_LIST, pSllNode);
    }

    if (u1FoundFlag == ISS_TRUE)
    {
        *pi4NextL2FilterNo = pNextL2FilterEntry->i4IssL2FilterNo;
        return ISS_SUCCESS;
    }
    else
    {
        return ISS_FAILURE;
    }
}

/****************************************************************************
* Function    :  IssExtGetL2FilterEntry
* Input       :  INT4 i4IssExtL2FilterNo
* Output      :  None
* Returns     :  tIssL2FilterEntry * 
*****************************************************************************/
tIssL2FilterEntry  *
IssExtGetL2FilterEntry (INT4 i4IssExtL2FilterNo)
{

    tIssSllNode        *pSllNode = NULL;
    tIssL2FilterEntry  *pIssExtL2FilterEntry = NULL;

    pSllNode = ISS_SLL_FIRST (&ISS_L2FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssExtL2FilterEntry = (tIssL2FilterEntry *) pSllNode;

        if (pIssExtL2FilterEntry->i4IssL2FilterNo == i4IssExtL2FilterNo)
        {
            return pIssExtL2FilterEntry;
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L2FILTER_LIST, pSllNode);
    }
    return NULL;
}

/****************************************************************************
* Function    :  IssExtQualifyL2FilterData 
* Input       :  tIssL2FilterEntry **ppIssExtL2FilterEntry
* Output      :  None
* Returns     :  INT4
*****************************************************************************/
INT4
IssExtQualifyL2FilterData (tIssL2FilterEntry ** ppIssExtL2FilterEntry)
{
    if ((*ppIssExtL2FilterEntry)->IssL2FilterAction != 0)
    {
        (*ppIssExtL2FilterEntry)->u1IssL2FilterStatus = ISS_NOT_IN_SERVICE;
        return ISS_SUCCESS;
    }
    else
    {
        (*ppIssExtL2FilterEntry)->u1IssL2FilterStatus = ISS_NOT_READY;
        return ISS_FAILURE;
    }
}

/****************************************************************************
* Function    :  IssExtGetL3FilterEntry
* Input       :  INT4 i4IssExtL3FilterNo
* Output      :  None
* Returns     :  tIssL3FilterEntry * 
*****************************************************************************/
tIssL3FilterEntry  *
IssExtGetL3FilterEntry (INT4 i4IssExtL3FilterNo)
{

    tIssSllNode        *pSllNode = NULL;
    tIssL3FilterEntry  *pIssExtL3FilterEntry = NULL;

    pSllNode = ISS_SLL_FIRST (&ISS_L3FILTER_LIST);

    while (pSllNode != NULL)
    {
        pIssExtL3FilterEntry = (tIssL3FilterEntry *) pSllNode;

        if (pIssExtL3FilterEntry->i4IssL3FilterNo == i4IssExtL3FilterNo)
        {
            return pIssExtL3FilterEntry;
        }
        pSllNode = ISS_SLL_NEXT (&ISS_L3FILTER_LIST, pSllNode);
    }
    return NULL;
}

/****************************************************************************
* Function    :  IssExtQualifyL3FilterData 
* Input       :  tIssL3FilterEntry **ppIssExtL3FilterEntry
* Output      :  None
* Returns     :  INT4
*****************************************************************************/
INT4
IssExtQualifyL3FilterData (tIssL3FilterEntry ** ppIssExtL3FilterEntry)
{
    if (((*ppIssExtL3FilterEntry)->IssL3FilterAction != 0) &&
        ((*ppIssExtL3FilterEntry)->IssL3FilterDirection != 0))
    {
        (*ppIssExtL3FilterEntry)->u1IssL3FilterStatus = ISS_NOT_IN_SERVICE;
        return ISS_SUCCESS;
    }
    else
    {
        (*ppIssExtL3FilterEntry)->u1IssL3FilterStatus = ISS_NOT_READY;
        return ISS_FAILURE;
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2IssExtL3FilterTable
 Input       :  The Indices
                IssExtL3FilterNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssExtL3FilterTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

tIssL2FilterEntry  *
IssExtGetL2FilterTableEntry (INT4 i4IssExtL2FilterNo)
{
    tIssL2FilterEntry  *pL2Filter = NULL;

    ISS_LOCK ();

    pL2Filter = IssExtGetL2FilterEntry (i4IssExtL2FilterNo);

    ISS_UNLOCK ();

    return (pL2Filter);
}

tIssL3FilterEntry  *
IssExtGetL3FilterTableEntry (INT4 i4IssExtL3FilterNo)
{
    tIssL3FilterEntry  *pL3Filter = NULL;

    ISS_LOCK ();

    pL3Filter = IssExtGetL3FilterEntry (i4IssExtL3FilterNo);

    ISS_UNLOCK ();

    return (pL3Filter);
}

/****************************************************************************
* Function    :  IssExtUpdateFilterRefCount
* Description :  This function is used to Incremet or Decrement the Referenc 
*                Conunt of the Given L2 or L3 Filter Entry
* Input       :  u4FilterId     - L2 / L3 Filter Entry Id
*             :  u4FilterType   - L2 (ISS_L2FILTER) / L3 (ISS_L3FILTER)        
*             :  u4Action       - ISS_INCR (Incremet) / ISS_DECR (Decrement)
* Output      :  None
* Returns     :  ISS_SUCCESS / ISS_FAILURE
*****************************************************************************/
INT4
IssExtUpdateFilterRefCount (UINT4 u4FilterId, UINT4 u4FilterType,
                            UINT4 u4Action)
{
    tIssL2FilterEntry  *pL2FilterEntry = NULL;
    tIssL3FilterEntry  *pL3FilterEntry = NULL;

    ISS_LOCK ();

    if (u4FilterType == ISS_L2FILTER)
    {
        /* Check the L2 Filter Id */
        pL2FilterEntry = IssExtGetL2FilterEntry (u4FilterId);
        if (pL2FilterEntry == NULL)
        {
            ISS_UNLOCK ();
            return (ISS_FAILURE);
        }

        if (u4Action == ISS_INCR)
        {
            /* Incremnet the RefCount of L2Filter Entry in the L2Filter Table */
            pL2FilterEntry->u4RefCount = (pL2FilterEntry->u4RefCount) + 1;
        }
        else
        {
            /* Decrement the RefCount of L2Filter Entry in the L2Filter Table */
            pL2FilterEntry->u4RefCount = (pL2FilterEntry->u4RefCount) - 1;
        }
    }
    else
    {
        /* Check the L3 Filter Id */
        pL3FilterEntry = IssExtGetL3FilterEntry (u4FilterId);
        if (pL3FilterEntry == NULL)
        {
            ISS_UNLOCK ();
            return (ISS_FAILURE);
        }

        if (u4Action == ISS_INCR)
        {
            /* Incremnet the RefCount of L3Filter Entry in the L3Filter Table */
            pL3FilterEntry->u4RefCount = (pL3FilterEntry->u4RefCount) + 1;
        }
        else
        {
            /* Decrement the RefCount of L3Filter Entry in the L3Filter Table */
            pL3FilterEntry->u4RefCount = (pL3FilterEntry->u4RefCount) - 1;
        }

    }                            /* End of if - u4FilterType */

    ISS_UNLOCK ();
    return (ISS_SUCCESS);

}
