
#ifndef _ISSEXMACR_H
#define _ISSEXMACR_H

#define ISS_MAX_RATE_VALUE              65535
#define ISS_DEF_RATE_VALUE              500
#define ISS_MIN_RATE_VALUE              1

/* Macros for validattion */
#define ISS_MIN_PROTOCOL_ID             1536
#define ISS_MAX_PROTOCOL_ID             65535
#define ISS_MIN_FILTER_ID               0
#define ISS_MAX_FILTER_ID               65535
#define ISS_MAX_FILTER_PRIORITY         255
#define ISS_DEFAULT_FILTER_PRIORITY     1
#define ISS_DEFAULT_PROTOCOL_TYPE       0
#define ISS_DEF_FILTER_MASK             0
#define ISS_MIN_DSCP_VALUE              0
#define ISS_MAX_DSCP_VALUE              63
/* ISS l2  ACL */
#define ISS_DEFAULT_VLAN_PRIORITY       -1
#define ISS_SERVICE_VLAN_ETHER_TYPE     0x88a8
#define ISS_CUSTOMER_VLAN_ETHER_TYPE    0x8100
/* MEM Pool Id Definitions */
#define ISS_RATEENTRY_POOL_ID            gIssExGlobalInfo.IssRateCtrlPoolId
#define ISS_L2FILTERENTRY_POOL_ID        gIssExGlobalInfo.IssL2FilterPoolId
#define ISS_L3FILTERENTRY_POOL_ID        gIssExGlobalInfo.IssL3FilterPoolId

/* Filter List */
#define ISS_L2FILTER_LIST               gIssExGlobalInfo.IssL2FilterListHead
#define ISS_L2FILTER_LIST_HEAD          (&ISS_L2FILTER_LIST)->Tail
#define ISS_IS_L2FILTER_ID_VALID(ISSId) \
        (((ISSId <= ISS_MIN_FILTER_ID) || \
        (ISSId > ISS_MAX_FILTER_ID)) ? ISS_FALSE : ISS_TRUE)

#define ISS_L3FILTER_LIST               gIssExGlobalInfo.IssL3FilterListHead
#define ISS_L3FILTER_LIST_HEAD          (&ISS_L3FILTER_LIST)->Tail
#define ISS_IS_L3FILTER_ID_VALID(ISSId) \
        (((ISSId <= ISS_MIN_FILTER_ID) || \
        (ISSId > ISS_MAX_FILTER_ID)) ? ISS_FALSE : ISS_TRUE)

#define ISS_L2FILTERENTRY_MEMBLK_COUNT  ISS_MAX_L2_FILTERS
#define ISS_L3FILTERENTRY_MEMBLK_COUNT  ISS_MAX_L3_FILTERS

#define ISS_RATEENTRY_MEMBLK_COUNT      (ISS_MAX_PORTS + 1)

#define ISS_RATEENTRY_MEMBLK_SIZE       sizeof(tIssRateCtrlEntry)

#define ISS_L2FILTERENTRY_MEMBLK_SIZE   sizeof(tIssL2FilterEntry)

#define  ISS_CREATE_RATEENTRY_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_CREATE_L2FILTERENTRY_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_CREATE_L3FILTERENTRY_MEM_POOL(x,y,pIssPoolId)\
         MemCreateMemPool(x,y,ISS_MEMORY_TYPE,(tMemPoolId*)pIssPoolId)

#define  ISS_DELETE_RATEENTRY_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_DELETE_L2FILTERENTRY_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_DELETE_L3FILTERENTRY_MEM_POOL(IssPoolId)\
         MemDeleteMemPool((tMemPoolId) IssPoolId)

#define  ISS_RATEENTRY_ALLOC_MEM_BLOCK(pu1Block)\
         (pu1Block = \
          (tIssRateCtrlEntry *)(MemAllocMemBlk (ISS_RATEENTRY_POOL_ID)))

#define  ISS_L2FILTERENTRY_ALLOC_MEM_BLOCK(pu1Block)\
         (pu1Block = \
          (tIssL2FilterEntry *)(MemAllocMemBlk (ISS_L2FILTERENTRY_POOL_ID)))

#define  ISS_L3FILTERENTRY_ALLOC_MEM_BLOCK(pu1Block)\
         (pu1Block = \
          (tIssL3FilterEntry *)(MemAllocMemBlk (ISS_L3FILTERENTRY_POOL_ID)))

#define  ISS_RATEENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_RATEENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define  ISS_L2FILTERENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_L2FILTERENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define  ISS_L3FILTERENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_L3FILTERENTRY_POOL_ID, (UINT1 *)pu1Msg)

#endif  /* _ISSEXMACR_H */
