
#ifndef _ISSEXTDFS_H
#define _ISSEXTDFS_H

typedef tTMO_SLL            tIssExSll;

typedef struct IssRateCtrlEntry {

   UINT4                u4IssRateCtrlDLFLimitValue;
   UINT4                u4IssRateCtrlBCASTLimitValue;
   UINT4                u4IssRateCtrlMCASTLimitValue;
   INT4                 i4IssRateCtrlPortLimitRate;
   INT4                 i4IssRateCtrlPortBurstRate;

}tIssRateCtrlEntry;

/* Iss Extension Global Info */
typedef struct { 

   tMemPoolId              IssRateCtrlPoolId;
   tMemPoolId              IssL2FilterPoolId;
   tMemPoolId              IssL3FilterPoolId;
   tIssRateCtrlEntry      *apIssRateCtrlEntry[ISS_RATEENTRY_MEMBLK_COUNT];
   tIssExSll               IssL2FilterListHead;
   tIssExSll               IssL3FilterListHead;

}tIssExtGlobalInfo;


#endif
