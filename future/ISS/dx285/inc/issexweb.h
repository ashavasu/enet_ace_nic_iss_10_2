#ifndef _DX285ISSWEB_H
#define _DX285ISSWEB_H

#include "webiss.h"
#include "isshttp.h"
#include "aclcli.h"
#include "iss.h"
#include "fsisselw.h"
#include "diffsrv.h"


#define   ACL_EXTENDED_START 1001

/* Prototypes for specific pages processing */
INT4 IssProcessCustomPages (tHttp * pHttp);
VOID IssRedirectMacFilterPage (tHttp *pHttp);
VOID IssRedirectDiffSrvPage (tHttp *pHttp);
VOID IssDxProcessIPFilterConfPage (tHttp * pHttp);
VOID IssDxProcessIPStdFilterConfPage (tHttp * pHttp);
VOID IssDxProcessMACFilterConfPage (tHttp * pHttp);
VOID IssDxProcessIPFilterConfPageSet (tHttp * pHttp);
VOID IssDxProcessIPFilterConfPageGet (tHttp * pHttp);
VOID IssDxProcessIPStdFilterConfPageGet (tHttp * pHttp);
VOID IssDxProcessIPStdFilterConfPageSet (tHttp * pHttp);
VOID IssDxProcessMACFilterConfPageGet (tHttp * pHttp);
VOID IssDxProcessMACFilterConfPageSet (tHttp * pHttp);
VOID IssDxProcessDfsPolicyMapPage (tHttp * pHttp);
VOID IssDxProcessDfsPolicyMapPageGet (tHttp * pHttp);
VOID IssDxProcessDfsPolicyMapPageSet (tHttp * pHttp);
VOID IssDxProcessSchdAlgoPage (tHttp * pHttp);
VOID IssDxProcessSchdAlgoPageGet (tHttp * pHttp);
VOID IssDxProcessSchdAlgoPageSet (tHttp * pHttp);
VOID IssDxProcessDfsCosqWeightBWConfPage (tHttp * pHttp);
VOID IssDxProcessDfsCosqWeightBWConfPageGet (tHttp * pHttp);
VOID IssDxProcessDfsCosqWeightBWConfPageSet (tHttp * pHttp); 

/* nmh routines used for scheduling */
extern INT4 nmhGetFirstIndexFsDiffServCoSqAlgorithmTable (INT4 *);
extern INT4 nmhGetNextIndexFsDiffServCoSqAlgorithmTable (INT4, INT4 *);
extern INT4 nmhTestv2FsDiffServCoSqAlgorithm (UINT4 *, INT4, INT4);
extern INT4 nmhSetFsDiffServCoSqAlgorithm (INT4, INT4);
extern INT4 nmhGetFsDiffServCoSqAlgorithm (INT4, INT4 *);
extern INT4 nmhGetFirstIndexFsDiffServCoSqWeightBwTable (INT4 *, INT4 *);
extern INT4 nmhGetFsDiffServCoSqWeight (INT4, INT4, UINT4 *);
extern INT4 nmhGetFsDiffServCoSqBwMin (INT4, INT4, UINT4 *); 
extern INT4 nmhGetFsDiffServCoSqBwMax (INT4, INT4, UINT4 *);
extern INT4 nmhGetFsDiffServCoSqBwFlags (INT4, INT4, INT4 *); 
extern INT4 nmhTestv2FsDiffServCoSqWeight (UINT4 *, INT4, INT4, UINT4);
extern INT4 nmhTestv2FsDiffServCoSqBwMin (UINT4 *, INT4, INT4, UINT4);
extern INT4 nmhTestv2FsDiffServCoSqBwMax (UINT4 *, INT4, INT4, UINT4);
extern INT4 nmhTestv2FsDiffServCoSqBwFlags (UINT4 *, INT4, INT4, INT4);
extern INT4 nmhSetFsDiffServCoSqWeight (INT4, INT4, UINT4);
extern INT4 nmhSetFsDiffServCoSqBwMin (INT4, INT4, UINT4);
extern INT4 nmhSetFsDiffServCoSqBwMax(INT4, INT4, UINT4);
extern INT4 nmhSetFsDiffServCoSqBwFlags(INT4, INT4, INT4);


extern INT4
DsWebnmGetPolicyMapEntry (INT4 i4PolicyMapId, tDiffServWebClfrData * pClfrData);
extern INT4
DsWebnmSetPolicyMapEntry (tDiffServWebSetClfrEntry * pClfrEntry,
                          UINT1 u1RowStatus, UINT1 *pu1ErrString);

tSpecificPage       asIssTargetpages[] = {
    {"dx285_ip_filterconf.html", IssDxProcessIPFilterConfPage},
    {"dx285_ip_stdfilterconf.html", IssDxProcessIPStdFilterConfPage},
    {"dx285_mac_filterconf.html", IssDxProcessMACFilterConfPage},
#ifdef DIFFSRV_WANTED
    {"dx285_dfs_policymapconf.html", IssDxProcessDfsPolicyMapPage},
	{"dx285_dfs_cosqschdalgorithm.html", IssDxProcessSchdAlgoPage},
	{"dx285_dfs_cosqweightbandwd.html", IssDxProcessDfsCosqWeightBWConfPage},
#endif
    {"", NULL}
};

#endif /* _ISSWEB_H */
