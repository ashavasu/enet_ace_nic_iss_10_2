/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: issexprot.h,v 1.6 2015/05/26 13:14:02 siva Exp $
 *
 * Description: This file include all the prototypes required
 *              for the operation of the ACL module
 ****************************************************************************/

#ifndef _ISSEXPROT_H
#define _ISSEXPROT_H

INT4 IssExInit (VOID);
VOID IssSetDefaultRateCtrlValues (UINT2 u2PortIndex,
                                  tIssRateCtrlEntry * pIssRateCtrlEntry);
INT4 IssExCreatePortRateCtrl (UINT2 u2PortIndex, tIssTableName IssTableFlag);
INT4 IssExDeletePortRateCtrl (UINT2 u2PortIndex);

tIssL2FilterEntry *IssGetL2FilterEntry (INT4 i4IssL2FilterNo);
tIssL3FilterEntry *IssGetL3FilterEntry (INT4 i4IssL3FilterNo);

INT4 IssQualifyL2FilterData (tIssL2FilterEntry ** ppIssL2FilterEntry);

INT4 IssQualifyL3FilterData (tIssL3FilterEntry ** ppIssL3FilterEntry);

VOID AclProcessQMsgEvent(VOID);
VOID RegisterFSISSM (VOID);
INT4 IssExPrgAclsToNpWithPriority(VOID);
INT4  AclCreateIPFilter PROTO ((tCliHandle, UINT4, INT4 ));

INT4  AclDestroyIPFilter PROTO ((tCliHandle,  INT4 ));

INT4 AclCreateMacFilter PROTO ((tCliHandle , INT4 ));

INT4 AclDestroyMacFilter PROTO ((tCliHandle , INT4 ));

INT4 AclMacAccessGroup PROTO ((tCliHandle CliHandle, INT4 i4FilterNo ));

INT4 AclNoMacAccessGroup PROTO ((tCliHandle CliHandle, INT4 i4FilterNo));
    
 INT4 AclMacExtAccessGroup PROTO ((tCliHandle, INT4, INT4));

 INT4 AclNoMacExtAccessGroup PROTO ((tCliHandle, INT4, INT4));

INT4 AclShowAccessLists PROTO ((tCliHandle CliHandle,INT4 i4FilterType,INT4 i4FilterNo));
INT4 AclShowL2Filter PROTO ((tCliHandle CliHandle,INT4 i4NextFilter));
INT4 AclShowL3Filter PROTO ((tCliHandle CliHandle,INT4 i4NextFilter));

INT4 AclShowRunningConfig(tCliHandle,UINT4);
VOID AclShowRunningConfigTables(tCliHandle);
VOID AclShowRunningConfigInterfaceDetails(tCliHandle,INT4);

INT4 AclTestIpParams PROTO ((UINT1 ,INT4 i4FilterNo,UINT4 u4SrcType,UINT4 u4SrcIpAddr, UINT4 u4SrcMask ));
                                           
INT4 AclSetIpParams PROTO ((UINT1 ,INT4 i4FilterNo,UINT4 u4SrcType,UINT4 u4SrcIpAddr, UINT4 u4SrcMask ));

VOID AclCliPrintPortList(tCliHandle CliHandle,
                         INT4 i4CommaCount, UINT1 * piIfName);
INT4 AclExtPbL3FilterConfig PROTO ((tCliHandle, INT4, INT4, INT4, INT4, INT4));
INT4 AclExtPbL2FilterConfig PROTO ((tCliHandle, INT4, INT4, INT4, INT4, INT4));

INT4 
AclTrafficSeprtnCliSetControl (tCliHandle CliHandle, INT4 i4TrafficControl);
INT4 AclTrafficSeprtnShowControl (tCliHandle CliHandle);
INT4 AclStdIpFilterConfig PROTO 
((tCliHandle, INT4,UINT4, UINT4 ,UINT4 ,UINT4, UINT4, UINT4));

INT4 AclExtIpFilterConfig PROTO 
((tCliHandle, INT4, INT4, UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, INT4, INT4,
INT4));

INT4
AclExtIp6FilterConfig PROTO ((tCliHandle CliHandle, UINT4 u4SrcIpAddr,
                       tIp6Addr SrcIp6Addr, UINT4 u4SrcIpMask,
                       UINT4 u4DestIpAddr, tIp6Addr DstIp6Addr,
         UINT4 u4DestIpMask, UINT4 u4FlowId, INT4 i4Action));

INT4 AclExtIpFilterTcpUdpConfig PROTO ((tCliHandle, INT4 i4Action, INT4 i4Protocol,
  UINT4 u4SrcType , UINT4 u4SrcIpAddr,  UINT4 u4SrcIpMask, 
  UINT4 u4SrcFlag, UINT4 u4SrcMinPort,  UINT4 u4SrcMaxRangePort,
  UINT4 u4DestType, UINT4  u4DestIpAddr,UINT4 u4DestIpMask,
  UINT4 u4DestFlag, UINT4 u4DestMinPort ,UINT4  u4DestMaxRangePort, 
  UINT4 u4BitType , INT4 i4Tos, INT4 i4Dscp, INT4 i4Priority ));

 INT4 AclExtIpFilterIcmpConfig PROTO
 ((tCliHandle, INT4 i4Action, UINT4,UINT4 , UINT4, UINT4, UINT4, UINT4 , INT4, INT4, INT4));

 INT4  AclIpAccessGroup PROTO ((tCliHandle, INT4 , INT4));
  
 INT4 AclNoIpAccessGroup PROTO ((tCliHandle, INT4 i4FilterNo, INT4 ));
 
 INT4 AclExtMacFilterConfig PROTO ((tCliHandle CliHandle, INT4 i4Action,
                       UINT4 u4SrcType, tMacAddr SrcMacAddr,
                       UINT4 u4DestType ,tMacAddr DestMacAddr,
                       INT4 i4Protocol, INT4 i4Encap,
                       UINT4 u4VlanId, INT4 u4Priority));

INT4 AclUpdateOverPortChannel PROTO ((UINT4 u4IfIndex));
INT4 AclEnabledForPort (UINT4 u4IfIndex);
INT4 AclIpFilterEnabledForPort (UINT4 u4IfIndex);
INT4 AclMacFilterEnabledForPort (UINT4 u4IfIndex);
INT4 AclUserDefinedFilterEnabledForPort  (UINT4 u4IfIndex);
#endif /* _ISSEXPROT_H */

