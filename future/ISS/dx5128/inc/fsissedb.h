/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissedb.h,v 1.1.1.1 2008/09/04 14:34:29 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSISSEDB_H
#define _FSISSEDB_H

UINT1 IssRateCtrlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 IssL2FilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 IssL3FilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fsisse [] ={1,3,6,1,4,1,2076,81,4,1};
tSNMP_OID_TYPE fsisseOID = {10, fsisse};


UINT4 IssRateCtrlIndex [ ] ={1,3,6,1,4,1,2076,81,4,1,1,1};
UINT4 IssRateCtrlDLFLimitValue [ ] ={1,3,6,1,4,1,2076,81,4,1,1,2};
UINT4 IssRateCtrlBCASTLimitValue [ ] ={1,3,6,1,4,1,2076,81,4,1,1,3};
UINT4 IssRateCtrlMCASTLimitValue [ ] ={1,3,6,1,4,1,2076,81,4,1,1,4};
UINT4 IssRateCtrlPortRateLimit [ ] ={1,3,6,1,4,1,2076,81,4,1,1,5};
UINT4 IssRateCtrlPortBurstSize [ ] ={1,3,6,1,4,1,2076,81,4,1,1,6};
UINT4 IssL2FilterNo [ ] ={1,3,6,1,4,1,2076,81,5,1,1,1};
UINT4 IssL2FilterPriority [ ] ={1,3,6,1,4,1,2076,81,5,1,1,2};
UINT4 IssL2FilterEtherType [ ] ={1,3,6,1,4,1,2076,81,5,1,1,3};
UINT4 IssL2FilterProtocolType [ ] ={1,3,6,1,4,1,2076,81,5,1,1,4};
UINT4 IssL2FilterDstMacAddr [ ] ={1,3,6,1,4,1,2076,81,5,1,1,5};
UINT4 IssL2FilterSrcMacAddr [ ] ={1,3,6,1,4,1,2076,81,5,1,1,6};
UINT4 IssL2FilterVlanId [ ] ={1,3,6,1,4,1,2076,81,5,1,1,7};
UINT4 IssL2FilterInPortList [ ] ={1,3,6,1,4,1,2076,81,5,1,1,8};
UINT4 IssL2FilterAction [ ] ={1,3,6,1,4,1,2076,81,5,1,1,9};
UINT4 IssL2FilterMatchCount [ ] ={1,3,6,1,4,1,2076,81,5,1,1,10};
UINT4 IssL2FilterStatus [ ] ={1,3,6,1,4,1,2076,81,5,1,1,11};
UINT4 IssL3FilterNo [ ] ={1,3,6,1,4,1,2076,81,6,1,1,1};
UINT4 IssL3FilterPriority [ ] ={1,3,6,1,4,1,2076,81,6,1,1,2};
UINT4 IssL3FilterProtocol [ ] ={1,3,6,1,4,1,2076,81,6,1,1,3};
UINT4 IssL3FilterMessageType [ ] ={1,3,6,1,4,1,2076,81,6,1,1,4};
UINT4 IssL3FilterMessageCode [ ] ={1,3,6,1,4,1,2076,81,6,1,1,5};
UINT4 IssL3FilterDstIpAddr [ ] ={1,3,6,1,4,1,2076,81,6,1,1,6};
UINT4 IssL3FilterSrcIpAddr [ ] ={1,3,6,1,4,1,2076,81,6,1,1,7};
UINT4 IssL3FilterDstIpAddrMask [ ] ={1,3,6,1,4,1,2076,81,6,1,1,8};
UINT4 IssL3FilterSrcIpAddrMask [ ] ={1,3,6,1,4,1,2076,81,6,1,1,9};
UINT4 IssL3FilterMinDstProtPort [ ] ={1,3,6,1,4,1,2076,81,6,1,1,10};
UINT4 IssL3FilterMaxDstProtPort [ ] ={1,3,6,1,4,1,2076,81,6,1,1,11};
UINT4 IssL3FilterMinSrcProtPort [ ] ={1,3,6,1,4,1,2076,81,6,1,1,12};
UINT4 IssL3FilterMaxSrcProtPort [ ] ={1,3,6,1,4,1,2076,81,6,1,1,13};
UINT4 IssL3FilterInPortList [ ] ={1,3,6,1,4,1,2076,81,6,1,1,14};
UINT4 IssL3FilterOutPortList [ ] ={1,3,6,1,4,1,2076,81,6,1,1,15};
UINT4 IssL3FilterAckBit [ ] ={1,3,6,1,4,1,2076,81,6,1,1,16};
UINT4 IssL3FilterRstBit [ ] ={1,3,6,1,4,1,2076,81,6,1,1,17};
UINT4 IssL3FilterTos [ ] ={1,3,6,1,4,1,2076,81,6,1,1,18};
UINT4 IssL3FilterDscp [ ] ={1,3,6,1,4,1,2076,81,6,1,1,19};
UINT4 IssL3FilterDirection [ ] ={1,3,6,1,4,1,2076,81,6,1,1,20};
UINT4 IssL3FilterAction [ ] ={1,3,6,1,4,1,2076,81,6,1,1,21};
UINT4 IssL3FilterMatchCount [ ] ={1,3,6,1,4,1,2076,81,6,1,1,22};
UINT4 IssL3FilterStatus [ ] ={1,3,6,1,4,1,2076,81,6,1,1,23};


tMbDbEntry fsisseMibEntry[]= {

{{12,IssRateCtrlIndex}, GetNextIndexIssRateCtrlTable, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssRateCtrlTableINDEX, 1},

{{12,IssRateCtrlDLFLimitValue}, GetNextIndexIssRateCtrlTable, IssRateCtrlDLFLimitValueGet, IssRateCtrlDLFLimitValueSet, IssRateCtrlDLFLimitValueTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssRateCtrlTableINDEX, 1},

{{12,IssRateCtrlBCASTLimitValue}, GetNextIndexIssRateCtrlTable, IssRateCtrlBCASTLimitValueGet, IssRateCtrlBCASTLimitValueSet, IssRateCtrlBCASTLimitValueTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssRateCtrlTableINDEX, 1},

{{12,IssRateCtrlMCASTLimitValue}, GetNextIndexIssRateCtrlTable, IssRateCtrlMCASTLimitValueGet, IssRateCtrlMCASTLimitValueSet, IssRateCtrlMCASTLimitValueTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssRateCtrlTableINDEX, 1},

{{12,IssRateCtrlPortRateLimit}, GetNextIndexIssRateCtrlTable, IssRateCtrlPortRateLimitGet, IssRateCtrlPortRateLimitSet, IssRateCtrlPortRateLimitTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssRateCtrlTableINDEX, 1},

{{12,IssRateCtrlPortBurstSize}, GetNextIndexIssRateCtrlTable, IssRateCtrlPortBurstSizeGet, IssRateCtrlPortBurstSizeSet, IssRateCtrlPortBurstSizeTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssRateCtrlTableINDEX, 1},

{{12,IssL2FilterNo}, GetNextIndexIssL2FilterTable, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssL2FilterTableINDEX, 1},

{{12,IssL2FilterPriority}, GetNextIndexIssL2FilterTable, IssL2FilterPriorityGet, IssL2FilterPrioritySet, IssL2FilterPriorityTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL2FilterTableINDEX, 1},

{{12,IssL2FilterEtherType}, GetNextIndexIssL2FilterTable, IssL2FilterEtherTypeGet, IssL2FilterEtherTypeSet, IssL2FilterEtherTypeTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL2FilterTableINDEX, 1},

{{12,IssL2FilterProtocolType}, GetNextIndexIssL2FilterTable, IssL2FilterProtocolTypeGet, IssL2FilterProtocolTypeSet, IssL2FilterProtocolTypeTest, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssL2FilterTableINDEX, 1},

{{12,IssL2FilterDstMacAddr}, GetNextIndexIssL2FilterTable, IssL2FilterDstMacAddrGet, IssL2FilterDstMacAddrSet, IssL2FilterDstMacAddrTest, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssL2FilterTableINDEX, 1},

{{12,IssL2FilterSrcMacAddr}, GetNextIndexIssL2FilterTable, IssL2FilterSrcMacAddrGet, IssL2FilterSrcMacAddrSet, IssL2FilterSrcMacAddrTest, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssL2FilterTableINDEX, 1},

{{12,IssL2FilterVlanId}, GetNextIndexIssL2FilterTable, IssL2FilterVlanIdGet, IssL2FilterVlanIdSet, IssL2FilterVlanIdTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL2FilterTableINDEX, 1},

{{12,IssL2FilterInPortList}, GetNextIndexIssL2FilterTable, IssL2FilterInPortListGet, IssL2FilterInPortListSet, IssL2FilterInPortListTest, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssL2FilterTableINDEX, 1},

{{12,IssL2FilterAction}, GetNextIndexIssL2FilterTable, IssL2FilterActionGet, IssL2FilterActionSet, IssL2FilterActionTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssL2FilterTableINDEX, 1},

{{12,IssL2FilterMatchCount}, GetNextIndexIssL2FilterTable, IssL2FilterMatchCountGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IssL2FilterTableINDEX, 1},

{{12,IssL2FilterStatus}, GetNextIndexIssL2FilterTable, IssL2FilterStatusGet, IssL2FilterStatusSet, IssL2FilterStatusTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssL2FilterTableINDEX, 1},

{{12,IssL3FilterNo}, GetNextIndexIssL3FilterTable, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssL3FilterTableINDEX, 1},

{{12,IssL3FilterPriority}, GetNextIndexIssL3FilterTable, IssL3FilterPriorityGet, IssL3FilterPrioritySet, IssL3FilterPriorityTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL3FilterTableINDEX, 1},

{{12,IssL3FilterProtocol}, GetNextIndexIssL3FilterTable, IssL3FilterProtocolGet, IssL3FilterProtocolSet, IssL3FilterProtocolTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL3FilterTableINDEX, 1},

{{12,IssL3FilterMessageType}, GetNextIndexIssL3FilterTable, IssL3FilterMessageTypeGet, IssL3FilterMessageTypeSet, IssL3FilterMessageTypeTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL3FilterTableINDEX, 1},

{{12,IssL3FilterMessageCode}, GetNextIndexIssL3FilterTable, IssL3FilterMessageCodeGet, IssL3FilterMessageCodeSet, IssL3FilterMessageCodeTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL3FilterTableINDEX, 1},

{{12,IssL3FilterDstIpAddr}, GetNextIndexIssL3FilterTable, IssL3FilterDstIpAddrGet, IssL3FilterDstIpAddrSet, IssL3FilterDstIpAddrTest, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IssL3FilterTableINDEX, 1},

{{12,IssL3FilterSrcIpAddr}, GetNextIndexIssL3FilterTable, IssL3FilterSrcIpAddrGet, IssL3FilterSrcIpAddrSet, IssL3FilterSrcIpAddrTest, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IssL3FilterTableINDEX, 1},

{{12,IssL3FilterDstIpAddrMask}, GetNextIndexIssL3FilterTable, IssL3FilterDstIpAddrMaskGet, IssL3FilterDstIpAddrMaskSet, IssL3FilterDstIpAddrMaskTest, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IssL3FilterTableINDEX, 1},

{{12,IssL3FilterSrcIpAddrMask}, GetNextIndexIssL3FilterTable, IssL3FilterSrcIpAddrMaskGet, IssL3FilterSrcIpAddrMaskSet, IssL3FilterSrcIpAddrMaskTest, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IssL3FilterTableINDEX, 1},

{{12,IssL3FilterMinDstProtPort}, GetNextIndexIssL3FilterTable, IssL3FilterMinDstProtPortGet, IssL3FilterMinDstProtPortSet, IssL3FilterMinDstProtPortTest, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssL3FilterTableINDEX, 1},

{{12,IssL3FilterMaxDstProtPort}, GetNextIndexIssL3FilterTable, IssL3FilterMaxDstProtPortGet, IssL3FilterMaxDstProtPortSet, IssL3FilterMaxDstProtPortTest, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssL3FilterTableINDEX, 1},

{{12,IssL3FilterMinSrcProtPort}, GetNextIndexIssL3FilterTable, IssL3FilterMinSrcProtPortGet, IssL3FilterMinSrcProtPortSet, IssL3FilterMinSrcProtPortTest, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssL3FilterTableINDEX, 1},

{{12,IssL3FilterMaxSrcProtPort}, GetNextIndexIssL3FilterTable, IssL3FilterMaxSrcProtPortGet, IssL3FilterMaxSrcProtPortSet, IssL3FilterMaxSrcProtPortTest, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssL3FilterTableINDEX, 1},

{{12,IssL3FilterInPortList}, GetNextIndexIssL3FilterTable, IssL3FilterInPortListGet, IssL3FilterInPortListSet, IssL3FilterInPortListTest, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssL3FilterTableINDEX, 1},

{{12,IssL3FilterOutPortList}, GetNextIndexIssL3FilterTable, IssL3FilterOutPortListGet, IssL3FilterOutPortListSet, IssL3FilterOutPortListTest, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssL3FilterTableINDEX, 1},

{{12,IssL3FilterAckBit}, GetNextIndexIssL3FilterTable, IssL3FilterAckBitGet, IssL3FilterAckBitSet, IssL3FilterAckBitTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssL3FilterTableINDEX, 1},

{{12,IssL3FilterRstBit}, GetNextIndexIssL3FilterTable, IssL3FilterRstBitGet, IssL3FilterRstBitSet, IssL3FilterRstBitTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssL3FilterTableINDEX, 1},

{{12,IssL3FilterTos}, GetNextIndexIssL3FilterTable, IssL3FilterTosGet, IssL3FilterTosSet, IssL3FilterTosTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssL3FilterTableINDEX, 1},

{{12,IssL3FilterDscp}, GetNextIndexIssL3FilterTable, IssL3FilterDscpGet, IssL3FilterDscpSet, IssL3FilterDscpTest, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL3FilterTableINDEX, 1},

{{12,IssL3FilterDirection}, GetNextIndexIssL3FilterTable, IssL3FilterDirectionGet, IssL3FilterDirectionSet, IssL3FilterDirectionTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssL3FilterTableINDEX, 1},

{{12,IssL3FilterAction}, GetNextIndexIssL3FilterTable, IssL3FilterActionGet, IssL3FilterActionSet, IssL3FilterActionTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssL3FilterTableINDEX, 1},

{{12,IssL3FilterMatchCount}, GetNextIndexIssL3FilterTable, IssL3FilterMatchCountGet, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IssL3FilterTableINDEX, 1},

{{12,IssL3FilterStatus}, GetNextIndexIssL3FilterTable, IssL3FilterStatusGet, IssL3FilterStatusSet, IssL3FilterStatusTest, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssL3FilterTableINDEX, 1},
};
tMibData fsisseEntry = { 40, fsisseMibEntry };
#endif /* _FSISSEDB_H */

