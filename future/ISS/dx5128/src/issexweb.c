#ifdef WEBNM_WANTED
#include "issexweb.h"

/*********************************************************************
*  Function Name : IssProcessCustomPages
*  Description   : This function processes the Pages specific to
*                  target
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
INT4
IssProcessCustomPages (tHttp * pHttp)
{
    UINT4               u4Count;

    for (u4Count = 0; asIssTargetpages[u4Count].au1Page[0] != '\0'; u4Count++)
    {
        if (STRCMP (pHttp->ai1HtmlName, asIssTargetpages[u4Count].au1Page) == 0)
        {
            asIssTargetpages[u4Count].pfunctPtr (pHttp);
            return ISS_SUCCESS;
        }
    }
    return ISS_FAILURE;
}

/************************************************************************
*  Function Name   : IssRedirectMacFilterPage
*  Description     : This function will redirect Mac Filter page
*  Input           : pHttp - Pointer to http entry where the html file
*                    name needs to be changed(copied)
*  Output          : None
*  Returns         : None
************************************************************************/
VOID
IssRedirectMacFilterPage (tHttp * pHttp)
{
    STRCPY (pHttp->ai1HtmlName, "dx5128_mac_filterconf.html");
}

/************************************************************************
*  Function Name   : IssRedirectDiffSrvPage
*  Description     : This function will redirect Diffsrv page
*  Input           : pHttp - Pointer to http entry where the html file
*                    name needs to be changed(copied)
*  Output          : None
*  Returns         : None
************************************************************************/
VOID
IssRedirectDiffSrvPage (tHttp * pHttp)
{
    STRCPY (pHttp->ai1HtmlName, "dx5128_dfs_globalconf.html");
}

/*********************************************************************
*  Function Name : IssDxProcessIPFilterConfPage
*  Description   : This function processes the request coming for the IP
*                  Filter Configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssDxProcessIPFilterConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssDxProcessIPFilterConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssDxProcessIPFilterConfPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssDxProcessIPStdFilterConfPage
*  Description   : This function processes the request coming for the IP
*                  standard Filter Configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssDxProcessIPStdFilterConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssDxProcessIPStdFilterConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssDxProcessIPStdFilterConfPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssDxProcessMACFilterConfPage
*  Description   : This function processes the request coming for the MAC
*                  Filter Configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
********************************************************************/
VOID
IssDxProcessMACFilterConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssDxProcessMACFilterConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssDxProcessMACFilterConfPageSet (pHttp);
    }
}

/*********************************************************************
*  Function Name : IssDxProcessIPFilterConfPageSet
*  Description   : This function processes the set request for the IP
*                 filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssDxProcessIPFilterConfPageSet (tHttp * pHttp)
{
    INT4                i4AckBit = 0, i4RstBit = 0, i4TOS = 0;
    UINT4               u4SrcIpAddr = 0, u4SrcAddrMask = 0, u4DestIpAddr =
        0, u4DestAddrMask = 0;
    UINT4               u4SrcPortFrom = 0, u4SrcPortTo = 0, u4DstPortFrom = 0;
    UINT4               u4DstPortTo = 0, u4Priority = 0, u4Code = 0;
    UINT4               u4ErrorCode = 0, u4Protocol = 0, u4Type = 0;
    INT4                i4Action = 0, i4FilterNo = 0;
    INT4                i4Dscp = 0;

    tSNMP_OCTET_STRING_TYPE InPortList, OutPortList;
    UINT1               u1InPortList[ISS_PORTLIST_LEN],
        u1OutPortList[ISS_PORTLIST_LEN];
    UINT1               u1Apply = 0, u1RowStatus = ISS_CREATE_AND_WAIT;

    HttpGetValuebyName ((UINT1 *) "FltNo", pHttp->au1Value,
                        pHttp->au1PostQuery);
    i4FilterNo = ATOI (pHttp->au1Value);

    /* Check whether the request is for ADD/DELETE */
    HttpGetValuebyName (ISS_ACTION, pHttp->au1Value, pHttp->au1PostQuery);

    ISS_LOCK ();

    /* check for delete */
    if (STRCMP (pHttp->au1Value, ISS_DELETE) == 0)
    {
        if (nmhTestv2IssL3FilterStatus (&u4ErrorCode, i4FilterNo, ISS_DESTROY)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "IP filter not available.");
            return;
        }

        if (nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "IP filter not available.");
            return;
        }

        ISS_UNLOCK ();

        IssDxProcessIPFilterConfPageGet (pHttp);
        return;
    }

    if (STRCMP (pHttp->au1Value, ISS_APPLY) == 0)
    {
        u1Apply = 1;
        u1RowStatus = ISS_NOT_IN_SERVICE;
    }

    HttpGetValuebyName ((UINT1 *) "FltOption", pHttp->au1Value,
                        pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    HttpGetValuebyName ((UINT1 *) "SrcIp", pHttp->au1Value,
                        pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        u4SrcIpAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    }

    HttpGetValuebyName ((UINT1 *) "SrcMask", pHttp->au1Value,
                        pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        u4SrcAddrMask = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    }

    HttpGetValuebyName ((UINT1 *) "DestIp", pHttp->au1Value,
                        pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        u4DestIpAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    }

    HttpGetValuebyName ((UINT1 *) "DestMask", pHttp->au1Value,
                        pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        u4DestAddrMask = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    }

    HttpGetValuebyName ((UINT1 *) "InPrtList", pHttp->au1Value,
                        pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        MEMSET (u1InPortList, 0, ISS_PORTLIST_LEN);
        issDecodeSpecialChar (pHttp->au1Value);
        if (STRCMP (pHttp->au1Value, "0") != 0)
        {
            if (ConvertStrToPortList (pHttp->au1Value, u1InPortList,
                                      ISS_PORTLIST_LEN,
                                      SYS_DEF_MAX_PHYSICAL_INTERFACES) ==
                OSIX_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp, (INT1 *) "Check the allowed in port list");
                return;
            }
        }
        InPortList.i4_Length = ISS_PORTLIST_LEN;
        InPortList.pu1_OctetList = MEM_CALLOC (InPortList.i4_Length, 1, UINT1);
        if (InPortList.pu1_OctetList == NULL)
        {
            ISS_UNLOCK ();
            return;
        }
        MEMCPY (InPortList.pu1_OctetList, u1InPortList, InPortList.i4_Length);
    }

    HttpGetValuebyName ((UINT1 *) "OutPrtList", pHttp->au1Value,
                        pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        MEMSET (u1OutPortList, 0, ISS_PORTLIST_LEN);
        issDecodeSpecialChar (pHttp->au1Value);

        if (STRCMP (pHttp->au1Value, "0") != 0)
        {
            if (ConvertStrToPortList (pHttp->au1Value, u1OutPortList,
                                      ISS_PORTLIST_LEN,
                                      SYS_DEF_MAX_PHYSICAL_INTERFACES) ==
                OSIX_FAILURE)
            {
                ISS_UNLOCK ();
                MEM_FREE (InPortList.pu1_OctetList);
                IssSendError (pHttp,
                              (INT1 *) "Check the allowed out port list");
                return;
            }
        }
        OutPortList.i4_Length = ISS_PORTLIST_LEN;
        OutPortList.pu1_OctetList =
            MEM_CALLOC (OutPortList.i4_Length, 1, UINT1);
        if (OutPortList.pu1_OctetList == NULL)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            return;
        }
        MEMCPY (OutPortList.pu1_OctetList, u1OutPortList,
                OutPortList.i4_Length);
    }

    /* Determine the type of ACL from the filter number and process the SET
     * request
     */

    HttpGetValuebyName ((UINT1 *) "Protocol", pHttp->au1Value,
                        pHttp->au1PostQuery);
    u4Protocol = ATOI (pHttp->au1Value);
    if (u4Protocol == ISS_PROT_OTHER)
    {
        HttpGetValuebyName ((UINT1 *) "Other", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Protocol = ATOI (pHttp->au1Value);
    }
    /* For Processing ICMP options. */
    if (u4Protocol == 1)
    {
        HttpGetValuebyName ((UINT1 *) "MsgCode", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Code = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "MsgType", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Type = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "Priority", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Priority = ATOI (pHttp->au1Value);
    }

    else
    {

        HttpGetValuebyName ((UINT1 *) "Priority", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Priority = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "Dscp", pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4Dscp = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "Tos", pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4TOS = ATOI (pHttp->au1Value);

        /* Configure either Dscp or TOS */
        if ((i4TOS > 0) && (i4Dscp > 0))
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *)
                          "Unable to configure both Dscp and TOS, configure one at a time");
            return;
        }
        else if (i4Dscp > 0)
        {
            i4TOS = ISS_TOS_INVALID;
        }
        else
        {
            i4Dscp = ISS_DSCP_INVALID;
        }
    }

    /* For Processing TCP options. */
    if (u4Protocol == 6)
    {
        HttpGetValuebyName ((UINT1 *) "AckBit", pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4AckBit = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "RstBit", pHttp->au1Value,
                            pHttp->au1PostQuery);
        i4RstBit = ATOI (pHttp->au1Value);

    }

    /* For Processing TCP/UDP options. */
    if (u4Protocol == 17 || u4Protocol == 6)
    {
        HttpGetValuebyName ((UINT1 *) "SrcPortMin", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4SrcPortFrom = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "SrcPortMax", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4SrcPortTo = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "DstPortMin", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4DstPortFrom = ATOI (pHttp->au1Value);

        HttpGetValuebyName ((UINT1 *) "DstPortMax", pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4DstPortTo = ATOI (pHttp->au1Value);
    }

    if (nmhTestv2IssL3FilterStatus (&u4ErrorCode, i4FilterNo,
                                    u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        IssSendError (pHttp, (INT1 *) "Invalid IP filter number.");
        return;
    }

    if ((i4Action != 0) && (nmhTestv2IssL3FilterAction
                            (&u4ErrorCode, i4FilterNo,
                             i4Action)) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the l3 filter action.");
        return;
    }
    if ((u4SrcIpAddr > 0) && (nmhTestv2IssL3FilterSrcIpAddr
                              (&u4ErrorCode, i4FilterNo, u4SrcIpAddr))
        == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *)
                      "Unable to set the filter with this source IP Address.");
        return;
    }

    if ((u4SrcAddrMask > 0) && (nmhTestv2IssL3FilterSrcIpAddrMask
                                (&u4ErrorCode, i4FilterNo, u4SrcAddrMask)
                                == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *)
                      "Unable to set the filter with this source Address Mask.");
        return;
    }

    if ((u4DestIpAddr > 0) && (nmhTestv2IssL3FilterDstIpAddr
                               (&u4ErrorCode, i4FilterNo, u4DestIpAddr)
                               == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (INT1 *)
                      "Unable to set the filter with this Destination IP Address.");
        return;
    }

    if ((u4DestAddrMask > 0) && (nmhTestv2IssL3FilterDstIpAddrMask
                                 (&u4ErrorCode, i4FilterNo, u4DestAddrMask)
                                 == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (INT1 *)
                      "Unable to set the filter with this Destination Address Mask.");
        return;
    }

    if ((InPortList.pu1_OctetList != NULL) && (nmhTestv2IssL3FilterInPortList
                                               (&u4ErrorCode, i4FilterNo,
                                                &InPortList) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set this in port list.");
        return;
    }

    if ((OutPortList.pu1_OctetList != NULL) && (nmhTestv2IssL3FilterOutPortList
                                                (&u4ErrorCode, i4FilterNo,
                                                 &OutPortList) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set this Out port list.");
        return;
    }

    if ((u4Protocol != 0) && (nmhTestv2IssL3FilterProtocol
                              (&u4ErrorCode, i4FilterNo, u4Protocol))
        == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the l3 filter protocol.");
        return;
    }
    if (u4Protocol == 1)
    {
        if (nmhTestv2IssL3FilterMessageType (&u4ErrorCode, i4FilterNo, u4Type)
            == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set ICMP MessageType.");
            return;
        }

        if (nmhTestv2IssL3FilterMessageCode (&u4ErrorCode, i4FilterNo, u4Code)
            == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set ICMP MessageCode.");
            return;
        }
    }
    else
    {
        if (i4Dscp != ISS_DSCP_INVALID)
        {
            if (nmhTestv2IssL3FilterDscp (&u4ErrorCode, i4FilterNo, i4Dscp)
                == SNMP_FAILURE)
            {
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                ISS_UNLOCK ();
                IssSendError (pHttp, (INT1 *) "Unable to set Filter Dscp.");
                return;
            }
        }

        if (i4TOS != ISS_TOS_INVALID)
        {
            if (nmhTestv2IssL3FilterTos (&u4ErrorCode, i4FilterNo, i4TOS)
                == SNMP_FAILURE)
            {
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                ISS_UNLOCK ();
                IssSendError (pHttp, (INT1 *) "Unable to set TCP TOS.");
                return;
            }
        }
    }

    if ((u4Priority != 0) && (nmhTestv2IssL3FilterPriority
                              (&u4ErrorCode, i4FilterNo, u4Priority)
                              == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter priority.");
        return;
    }

    if (u4Protocol == 6)
    {
        if (nmhTestv2IssL3FilterAckBit (&u4ErrorCode, i4FilterNo, i4AckBit)
            == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set TCP Ack Bit.");
            return;
        }

        if (nmhTestv2IssL3FilterRstBit (&u4ErrorCode, i4FilterNo, i4RstBit)
            == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Unable to set TCP Rst Bit.");
            return;
        }
    }
    if (u4Protocol == 17 || u4Protocol == 6)
    {
        if (nmhTestv2IssL3FilterMinSrcProtPort (&u4ErrorCode, i4FilterNo,
                                                u4SrcPortFrom) == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set TCP/UDP Src Port Start.");
            return;
        }

        if (nmhTestv2IssL3FilterMaxSrcProtPort (&u4ErrorCode, i4FilterNo,
                                                u4SrcPortTo) == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set TCP/UDP Src Port End.");
            return;
        }

        if (nmhTestv2IssL3FilterMinDstProtPort (&u4ErrorCode, i4FilterNo,
                                                u4DstPortFrom) == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set TCP/UDP Dst Port Start.");
            return;
        }

        if (nmhTestv2IssL3FilterMaxDstProtPort (&u4ErrorCode, i4FilterNo,
                                                u4DstPortTo) == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set TCP/UDP Dst Port End.");
            return;
        }
    }

    if (nmhSetIssL3FilterStatus (i4FilterNo, u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        IssSendError (pHttp, (INT1 *) "Entry already exists for this IP"
                      " filter number");
        return;
    }

    if ((i4Action != 0) && (nmhSetIssL3FilterAction (i4FilterNo, i4Action)
                            == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the l3 filter action.");
        return;
    }

    if ((u4SrcIpAddr > 0) && (nmhSetIssL3FilterSrcIpAddr
                              (i4FilterNo, u4SrcIpAddr) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (INT1 *) "Unable to set the l3 Source Ip Address.");
        return;
    }

    if ((u4SrcAddrMask > 0) && (nmhSetIssL3FilterSrcIpAddrMask
                                (i4FilterNo, u4SrcAddrMask) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (INT1 *) "Unable to set the l3 Source Address Mask.");
        return;
    }

    if ((u4DestIpAddr > 0) && (nmhSetIssL3FilterDstIpAddr
                               (i4FilterNo, u4DestIpAddr) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (INT1 *) "Unable to set the l3 Destination Ip Address.");
        return;
    }

    if ((u4DestAddrMask > 0) && (nmhSetIssL3FilterDstIpAddrMask
                                 (i4FilterNo, u4DestAddrMask) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp,
                      (INT1 *)
                      "Unable to set the l3 Destination Address Mask.");
        return;
    }
    if ((InPortList.pu1_OctetList != NULL) && (nmhSetIssL3FilterInPortList
                                               (i4FilterNo, &InPortList)
                                               == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set In Port List.");
        return;
    }

    if ((OutPortList.pu1_OctetList != NULL) && (nmhSetIssL3FilterOutPortList
                                                (i4FilterNo, &OutPortList)
                                                == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set Out Port List.");
        return;
    }

    if ((u4Protocol != 0) && (nmhSetIssL3FilterProtocol
                              (i4FilterNo, u4Protocol) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter priority.");
        return;
    }
    if (u4Protocol == 1)
    {
        if (nmhSetIssL3FilterMessageType (i4FilterNo, u4Type) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            IssSendError (pHttp, (INT1 *) "Unable to set ICMP MessageType.");
            return;
        }

        if (nmhSetIssL3FilterMessageCode (i4FilterNo, u4Code) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            IssSendError (pHttp, (INT1 *) "Unable to set ICMP MessageCode.");
            return;
        }
    }
    else
    {
        if (i4Dscp != ISS_DSCP_INVALID)
        {
            if (nmhSetIssL3FilterDscp (i4FilterNo, i4Dscp) == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                IssSendError (pHttp, "Unable to set Filter Dscp.");
                return;
            }
        }

        if (i4TOS != ISS_TOS_INVALID)
        {
            if (nmhSetIssL3FilterTos (i4FilterNo, i4TOS) == SNMP_FAILURE)
            {
                ISS_UNLOCK ();
                MEM_FREE (InPortList.pu1_OctetList);
                MEM_FREE (OutPortList.pu1_OctetList);
                IssSendError (pHttp, "Unable to set TCP TOS.");
                return;
            }
        }
    }

    if ((u4Priority != 0) && (nmhSetIssL3FilterPriority
                              (i4FilterNo, u4Priority) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter priority.");
        return;
    }

    if (u4Protocol == 6)
    {
        if (nmhSetIssL3FilterAckBit (i4FilterNo, i4AckBit) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            IssSendError (pHttp, (INT1 *) "Unable to set TCP Ack Bit.");
            return;
        }
        if (nmhSetIssL3FilterRstBit (i4FilterNo, i4RstBit) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            IssSendError (pHttp, (INT1 *) "Unable to set TCP Rst Bit.");
            return;
        }
    }
    if (u4Protocol == 17 || u4Protocol == 6)
    {
        if (nmhSetIssL3FilterMinSrcProtPort (i4FilterNo, u4SrcPortFrom)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            IssSendError (pHttp,
                          (INT1 *) "Unable to set TCP/UDP Src Port Start.");
            return;
        }

        if (nmhSetIssL3FilterMaxSrcProtPort (i4FilterNo, u4SrcPortTo)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            IssSendError (pHttp,
                          (INT1 *) "Unable to set TCP/UDP Src Port End.");
            return;
        }

        if (nmhSetIssL3FilterMinDstProtPort (i4FilterNo, u4DstPortFrom)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            IssSendError (pHttp,
                          (INT1 *) "Unable to set TCP/UDP Dst Port Start.");
            return;
        }

        if (nmhSetIssL3FilterMaxDstProtPort (i4FilterNo, u4DstPortTo)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            MEM_FREE (OutPortList.pu1_OctetList);
            IssSendError (pHttp,
                          (INT1 *) "Unable to set TCP/UDP Dst Port End.");
            return;
        }
    }
    if (nmhSetIssL3FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        IssSendError (pHttp, "Unable to set Filter Status");
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        return;
    }

    ISS_UNLOCK ();

    MEM_FREE (InPortList.pu1_OctetList);
    MEM_FREE (OutPortList.pu1_OctetList);
    IssDxProcessIPFilterConfPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssDxProcessIPFilterConfPageGet
*  Description   : This function processes the get request for the IP
*                  filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssDxProcessIPFilterConfPageGet (tHttp * pHttp)
{
    INT4                i4FilterNo, i4CurrentFilterNo, i4RetVal, i4OutCome;
    INT4                i4Protocol;
    UINT4               u4RetVal, u4Temp;
    tSNMP_OCTET_STRING_TYPE *InPortList = NULL, *OutPortList = NULL;
    UINT1              *pu1String;

    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = pHttp->i4Write;

    WebnmRegisterLock (pHttp, IssLock, IssUnLock);

    ISS_LOCK ();

    i4OutCome = (INT4) (nmhGetFirstIndexIssL3FilterTable (&i4FilterNo));
    if (i4OutCome == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    InPortList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);

    if (InPortList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    OutPortList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);
    if (OutPortList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (InPortList);
        return;
    }

    do
    {
        pHttp->i4Write = u4Temp;

        /* Display only extended lists */
        if (i4FilterNo >= ACL_EXTENDED_START)
        {
            STRCPY (pHttp->au1KeyString, "FILTER_NUMBER");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4FilterNo);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            STRCPY (pHttp->au1KeyString, "ACTION");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssL3FilterAction (i4FilterNo, &i4RetVal);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "SOURCE_IP_ADDRESS");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssL3FilterSrcIpAddr (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "SOURCE_MASK");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssL3FilterSrcIpAddrMask (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "DESTINATION_IP_ADDRESS");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssL3FilterDstIpAddr (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "DESTINATION_MASK");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssL3FilterDstIpAddrMask (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "IN_PORTS_LIST");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMSET (InPortList->pu1_OctetList, 0, ISS_PORTLIST_LEN);
            nmhGetIssL3FilterInPortList (i4FilterNo, InPortList);
            IssConvertOctetToPort (InPortList->pu1_OctetList,
                                   InPortList->i4_Length, pHttp->au1DataString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "OUT_PORTS_LIST");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMSET (OutPortList->pu1_OctetList, 0, ISS_PORTLIST_LEN);
            nmhGetIssL3FilterOutPortList (i4FilterNo, OutPortList);
            IssConvertOctetToPort (OutPortList->pu1_OctetList,
                                   OutPortList->i4_Length,
                                   pHttp->au1DataString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "PROTOCOL");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssL3FilterProtocol (i4FilterNo, &i4Protocol);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4Protocol);

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "OTHER");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4Protocol);

            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            if (i4Protocol == 1)
            {
                STRCPY (pHttp->au1KeyString, "CODE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssL3FilterMessageCode (i4FilterNo, &i4RetVal);
                SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "TYPE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssL3FilterMessageType (i4FilterNo, &i4RetVal);
                SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "PRIORITY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssL3FilterPriority (i4FilterNo, &i4RetVal);
                SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            }
            else
            {
                STRCPY (pHttp->au1KeyString, "CODE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                STRCPY (pHttp->au1DataString, "");
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "TYPE");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                STRCPY (pHttp->au1DataString, "");
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "PRIORITY");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssL3FilterPriority (i4FilterNo, &i4RetVal);
                SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DSCP");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssL3FilterDscp (i4FilterNo, &i4RetVal);
                SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "TOS");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssL3FilterTos (i4FilterNo, &i4RetVal);
                SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            }

            if (i4Protocol == 6)
            {
                STRCPY (pHttp->au1KeyString, "ACK_BIT");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssL3FilterAckBit (i4FilterNo, &i4RetVal);
                SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "RST_BIT");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssL3FilterRstBit (i4FilterNo, &i4RetVal);
                SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }

            if (i4Protocol == 6 || i4Protocol == 17)
            {
                STRCPY (pHttp->au1KeyString, "SOURCE_PORT_START");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssL3FilterMinSrcProtPort (i4FilterNo, &u4RetVal);
                SPRINTF ((INT1 *) pHttp->au1DataString, "%u", u4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "SOURCE_PORT_END");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssL3FilterMaxSrcProtPort (i4FilterNo, &u4RetVal);
                SPRINTF ((INT1 *) pHttp->au1DataString, "%u", u4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DESTINATION_PORT_START");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssL3FilterMinDstProtPort (i4FilterNo, &u4RetVal);
                SPRINTF ((INT1 *) pHttp->au1DataString, "%u", u4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

                STRCPY (pHttp->au1KeyString, "DESTINATION_PORT_END");
                WebnmSendString (pHttp, pHttp->au1KeyString);
                nmhGetIssL3FilterMaxDstProtPort (i4FilterNo, &u4RetVal);
                SPRINTF ((INT1 *) pHttp->au1DataString, "%u", u4RetVal);
                WebnmSockWrite (pHttp, pHttp->au1DataString,
                                STRLEN (pHttp->au1DataString));
                WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            }

            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        }
        i4CurrentFilterNo = i4FilterNo;
    }
    while (nmhGetNextIndexIssL3FilterTable
           (i4CurrentFilterNo, &i4FilterNo) == SNMP_SUCCESS);

    ISS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    free_octetstring (InPortList);
    free_octetstring (OutPortList);
    if (i4FilterNo >= ACL_EXTENDED_START)
    {
        WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                        (pHttp->i4HtmlSize - pHttp->i4Write));
    }
}

/*********************************************************************
*  Function Name : IssDxProcessIPStdFilterConfPageGet
*  Description   : This function processes the get request for the IP
*                  standard filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssDxProcessIPStdFilterConfPageGet (tHttp * pHttp)
{
    INT4                i4FilterNo, i4CurrentFilterNo, i4RetVal, i4OutCome;
    UINT4               u4RetVal, u4Temp;
    tSNMP_OCTET_STRING_TYPE *InPortList = NULL, *OutPortList = NULL;
    UINT1              *pu1String;

    pHttp->i4Write = 0;

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = pHttp->i4Write;
    WebnmRegisterLock (pHttp, IssLock, IssUnLock);

    ISS_LOCK ();

    i4OutCome = (INT4) (nmhGetFirstIndexIssL3FilterTable (&i4FilterNo));
    if ((i4OutCome == SNMP_FAILURE) || (i4FilterNo >= ACL_EXTENDED_START))
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    InPortList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);

    if (InPortList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    OutPortList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);

    if (OutPortList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        free_octetstring (InPortList);
        return;
    }

    do
    {

        /* Display only if the ACL number is in the range 1 to 1000 */

        if (i4FilterNo < ACL_EXTENDED_START)
        {
            pHttp->i4Write = u4Temp;
            STRCPY (pHttp->au1KeyString, "FILTER_NUMBER");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4FilterNo);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, pHttp->au1KeyString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "ACTION");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssL3FilterAction (i4FilterNo, &i4RetVal);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%d", i4RetVal);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "SOURCE_IP_ADDRESS");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssL3FilterSrcIpAddr (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "SOURCE_MASK");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssL3FilterSrcIpAddrMask (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "DESTINATION_IP_ADDRESS");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssL3FilterDstIpAddr (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "DESTINATION_MASK");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            nmhGetIssL3FilterDstIpAddrMask (i4FilterNo, &u4RetVal);
            WEB_CONVERT_IPADDR_TO_STR (pu1String, u4RetVal);
            SPRINTF ((INT1 *) pHttp->au1DataString, "%s", pu1String);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "IN_PORTS_LIST");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMSET (InPortList->pu1_OctetList, 0, ISS_PORTLIST_LEN);
            nmhGetIssL3FilterInPortList (i4FilterNo, InPortList);
            IssConvertOctetToPort (InPortList->pu1_OctetList,
                                   InPortList->i4_Length, pHttp->au1DataString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            STRCPY (pHttp->au1KeyString, "OUT_PORTS_LIST");
            WebnmSendString (pHttp, pHttp->au1KeyString);
            MEMSET (OutPortList->pu1_OctetList, 0, ISS_PORTLIST_LEN);
            nmhGetIssL3FilterOutPortList (i4FilterNo, OutPortList);
            IssConvertOctetToPort (OutPortList->pu1_OctetList,
                                   OutPortList->i4_Length,
                                   pHttp->au1DataString);
            WebnmSockWrite (pHttp, pHttp->au1DataString,
                            STRLEN (pHttp->au1DataString));
            WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        }

        i4CurrentFilterNo = i4FilterNo;
    }
    while (nmhGetNextIndexIssL3FilterTable
           (i4CurrentFilterNo, &i4FilterNo) == SNMP_SUCCESS);

    ISS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    free_octetstring (InPortList);
    free_octetstring (OutPortList);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : IssDxProcessIPStdFilterConfPageSet
*  Description   : This function processes the set request for the IP
*                 standard filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssDxProcessIPStdFilterConfPageSet (tHttp * pHttp)
{
    UINT4               u4SrcIpAddr = 0, u4SrcAddrMask = 0, u4DestIpAddr =
        0, u4DestAddrMask = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4Action = 0, i4FilterNo = 0;
    tSNMP_OCTET_STRING_TYPE InPortList, OutPortList;
    UINT1               u1InPortList[ISS_PORTLIST_LEN],
        u1OutPortList[ISS_PORTLIST_LEN];
    UINT1               u1Apply = 0, u1RowStatus = ISS_CREATE_AND_WAIT;

    STRCPY (pHttp->au1Name, "FILTER_NUMBER");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4FilterNo = ATOI (pHttp->au1Value);

    /* Check whether the request is for ADD/DELETE */
    STRCPY (pHttp->au1Name, "DELETE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    ISS_LOCK ();
    /* check for delete */
    if (STRCMP (pHttp->au1Value, "DELETE") == 0)
    {
        if (nmhTestv2IssL3FilterStatus (&u4ErrorCode, i4FilterNo, ISS_DESTROY)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, "ACL with this number not available.");
            return;
        }

        if (nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, "ACL with this number not available.");
            return;
        }
        ISS_UNLOCK ();
        IssDxProcessIPStdFilterConfPageGet (pHttp);
        return;
    }

    if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {
        u1Apply = 1;
        u1RowStatus = ISS_NOT_IN_SERVICE;
    }
    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "SOURCE_ADDRESS");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        u4SrcIpAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));

    STRCPY (pHttp->au1Name, "SOURCE_MASK");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        u4SrcAddrMask = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));

    STRCPY (pHttp->au1Name, "DESTINATION_ADDRESS");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        u4DestIpAddr = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));

    STRCPY (pHttp->au1Name, "DESTINATION_MASK");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_SUCCESS)
        u4DestAddrMask = OSIX_NTOHL (INET_ADDR (pHttp->au1Value));
    STRCPY (pHttp->au1Name, "IN_PORTS_LIST");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    if (STRLEN (pHttp->au1Value) != 0)
    {
        MEMSET (u1InPortList, 0, ISS_PORTLIST_LEN);
        issDecodeSpecialChar (pHttp->au1Value);
        if (STRCMP (pHttp->au1Value, "0") != 0)
        {
            if (ConvertStrToPortList (pHttp->au1Value, u1InPortList,
                                      ISS_PORTLIST_LEN,
                                      SYS_DEF_MAX_PHYSICAL_INTERFACES) ==
                OSIX_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp, "Check the allowed in port list");
                return;
            }
        }

        InPortList.i4_Length = ISS_PORTLIST_LEN;
        InPortList.pu1_OctetList = MEM_CALLOC (InPortList.i4_Length, 1, UINT1);
        if (InPortList.pu1_OctetList == NULL)
        {
            ISS_UNLOCK ();
            return;
        }
        MEMCPY (InPortList.pu1_OctetList, u1InPortList, InPortList.i4_Length);
    }

    STRCPY (pHttp->au1Name, "OUT_PORTS_LIST");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        MEMSET (u1OutPortList, 0, ISS_PORTLIST_LEN);
        issDecodeSpecialChar (pHttp->au1Value);

        if (STRCMP (pHttp->au1Value, "0") != 0)
        {
            if (ConvertStrToPortList (pHttp->au1Value, u1OutPortList,
                                      ISS_PORTLIST_LEN,
                                      SYS_DEF_MAX_PHYSICAL_INTERFACES) ==
                OSIX_FAILURE)
            {
                ISS_UNLOCK ();
                MEM_FREE (InPortList.pu1_OctetList);
                IssSendError (pHttp, "Check the allowed out port list");
                return;
            }
        }
        OutPortList.i4_Length = ISS_PORTLIST_LEN;
        OutPortList.pu1_OctetList =
            MEM_CALLOC (OutPortList.i4_Length, 1, UINT1);
        if (OutPortList.pu1_OctetList == NULL)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            return;
        }
        MEMCPY (OutPortList.pu1_OctetList, u1OutPortList,
                OutPortList.i4_Length);
    }
    if (nmhTestv2IssL3FilterStatus (&u4ErrorCode, i4FilterNo,
                                    u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        IssSendError (pHttp, "Invalid ACL number.");
        return;
    }

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    if ((i4Action != 0) && (nmhTestv2IssL3FilterAction
                            (&u4ErrorCode, i4FilterNo,
                             i4Action)) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        IssSendError (pHttp, "Unable to set the standard ACL filter action.");
        return;
    }

    if ((u4SrcIpAddr > 0) && (nmhTestv2IssL3FilterSrcIpAddr
                              (&u4ErrorCode, i4FilterNo, u4SrcIpAddr))
        == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        IssSendError (pHttp, (INT1 *) "Unable to set TCP/UDP Dst Port End.");
        return;
    }

    if ((u4SrcAddrMask > 0) && (nmhTestv2IssL3FilterSrcIpAddrMask
                                (&u4ErrorCode, i4FilterNo, u4SrcAddrMask)
                                == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        IssSendError (pHttp,
                      "Unable to set ACL with this source IP address Mask.");
        return;
    }
    if ((u4DestIpAddr > 0) && (nmhTestv2IssL3FilterDstIpAddr
                               (&u4ErrorCode, i4FilterNo, u4DestIpAddr)
                               == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        IssSendError (pHttp,
                      "Unable to set ACL with this destination IP Address.");
        return;
    }

    if ((u4DestAddrMask > 0) && (nmhTestv2IssL3FilterDstIpAddrMask
                                 (&u4ErrorCode, i4FilterNo, u4DestAddrMask)
                                 == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        IssSendError (pHttp,
                      "Unable to set ACL with this destination IP address mask");
        return;
    }

    if ((InPortList.pu1_OctetList != NULL) && (nmhTestv2IssL3FilterInPortList
                                               (&u4ErrorCode, i4FilterNo,
                                                &InPortList) == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        IssSendError (pHttp, "Unable to set this in port list.");
        return;
    }

    if ((OutPortList.pu1_OctetList != NULL) && (nmhTestv2IssL3FilterOutPortList
                                                (&u4ErrorCode, i4FilterNo,
                                                 &OutPortList) == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        IssSendError (pHttp, "Unable to set this Out port list.");
        return;
    }

    if (nmhSetIssL3FilterStatus (i4FilterNo, u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        IssSendError (pHttp, "Entry already exists for this IP"
                      " filter number");
        return;
    }
    if ((i4Action != 0) && (nmhSetIssL3FilterAction (i4FilterNo, i4Action)
                            == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        IssSendError (pHttp, "Unable to set the Standard ACL  filter action.");
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        return;
    }

    if ((u4SrcIpAddr > 0) && (nmhSetIssL3FilterSrcIpAddr
                              (i4FilterNo, u4SrcIpAddr) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, "Unable to set the ACL source IP address.");
        return;
    }
    if ((u4SrcAddrMask > 0) && (nmhSetIssL3FilterSrcIpAddrMask
                                (i4FilterNo, u4SrcAddrMask) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, "Unable to set ACL source address mask.");
        return;
    }

    if ((u4DestIpAddr > 0) && (nmhSetIssL3FilterDstIpAddr
                               (i4FilterNo, u4DestIpAddr) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, "Unable to set ACL destination IP address.");
        return;
    }
    if ((u4DestAddrMask > 0) && (nmhSetIssL3FilterDstIpAddrMask
                                 (i4FilterNo, u4DestAddrMask) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, "Unable to set the destination IP address mask.");
        return;
    }

    if ((InPortList.pu1_OctetList != NULL) && (nmhSetIssL3FilterInPortList
                                               (i4FilterNo, &InPortList)
                                               == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, "Unable to set In Port List.");
        return;
    }
    if ((OutPortList.pu1_OctetList != NULL) && (nmhSetIssL3FilterOutPortList
                                                (i4FilterNo, &OutPortList)
                                                == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, "Unable to set Out Port List.");
        return;
    }

    if (nmhSetIssL3FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        MEM_FREE (OutPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssL3FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set Filter Status");
        return;
    }

    ISS_UNLOCK ();
    MEM_FREE (InPortList.pu1_OctetList);
    MEM_FREE (OutPortList.pu1_OctetList);
    IssDxProcessIPStdFilterConfPageGet (pHttp);
}

/*********************************************************************
*  Function Name : IssDxProcessMACFilterConfPageGet
*  Description   : This function processes the get request for the MAC
*                  filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssDxProcessMACFilterConfPageGet (tHttp * pHttp)
{
    INT4                i4CurrentFilterNo, i4FilterNo, i4RetVal, i4OutCome;
    UINT4               u4RetVal, u4Temp;
    UINT1               au1String[20];
    tMacAddr            SrcMacAddr, DestMacAddr;
    tSNMP_OCTET_STRING_TYPE *InPortList = NULL;

    pHttp->i4Write = 0;

    IssPrintAvailableVlans (pHttp);

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = pHttp->i4Write;

    WebnmRegisterLock (pHttp, IssLock, IssUnLock);

    ISS_LOCK ();
    i4OutCome = (INT4) (nmhGetFirstIndexIssL2FilterTable (&i4FilterNo));
    if (i4OutCome == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    InPortList = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (ISS_PORTLIST_LEN);

    if (InPortList == NULL)
    {
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        return;
    }

    do
    {
        pHttp->i4Write = u4Temp;
        STRCPY (pHttp->au1KeyString, "FILTER_NUMBER");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4FilterNo);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "SOURCE_MAC");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssL2FilterSrcMacAddr (i4FilterNo, &SrcMacAddr);
        IssPrintMacAddress (SrcMacAddr, au1String);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "DESTINATION_MAC");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssL2FilterDstMacAddr (i4FilterNo, &DestMacAddr);
        IssPrintMacAddress (DestMacAddr, au1String);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s", au1String);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "ACTION_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssL2FilterAction (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "PRIORITY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssL2FilterPriority (i4FilterNo, &i4RetVal);
        ISS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        IssPrintAvailableVlans (pHttp);
        STRCPY (pHttp->au1KeyString, "VLAN_ID");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        WebnmRegisterLock (pHttp, IssLock, IssUnLock);
        ISS_LOCK ();
        nmhGetIssL2FilterVlanId (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "IN_PORT_LIST");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        MEMSET (InPortList->pu1_OctetList, 0, ISS_PORTLIST_LEN);
        nmhGetIssL2FilterInPortList (i4FilterNo, InPortList);
        IssConvertOctetToPort (InPortList->pu1_OctetList,
                               InPortList->i4_Length, pHttp->au1DataString);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "ENCAPTYPE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssL2FilterEtherType (i4FilterNo, &i4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "PROTOCOL");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        nmhGetIssL2FilterProtocolType (i4FilterNo, &u4RetVal);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OTHER");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4RetVal);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        i4CurrentFilterNo = i4FilterNo;
    }
    while (nmhGetNextIndexIssL2FilterTable
           (i4CurrentFilterNo, &i4FilterNo) == SNMP_SUCCESS);

    ISS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    free_octetstring (InPortList);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
*  Function Name : IssDxProcessMACFilterConfPageSet
*  Description   : This function processes the set request for the MAC
*                  filter configuration page.
*
*  Input(s)      : pHttp - Pointer to the global HTTP data structure.
*  Output(s)     : None.
*  Return Values : None
*********************************************************************/

VOID
IssDxProcessMACFilterConfPageSet (tHttp * pHttp)
{
    INT4                i4Action, i4FilterNo, i4Priority, i4EtherType;
    INT4                i4VlanId;
    UINT4               u4ErrorCode, u4Protocol;
    tMacAddr            SrcMacAddr, DestMacAddr;
    tSNMP_OCTET_STRING_TYPE InPortList;
    UINT1               u1PortList[ISS_PORTLIST_LEN], u1Apply = 0;
    UINT1               u1RowStatus = ISS_CREATE_AND_WAIT;
    UINT1               au1ZeroMac[ISS_MAC_LEN];

    MEMSET (au1ZeroMac, 0, ISS_MAC_LEN);

    STRCPY (pHttp->au1Name, "FILTER_NUMBER");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4FilterNo = ATOI (pHttp->au1Value);

    /* Check whether the request is for ADD/DELETE */
    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    ISS_LOCK ();
    /* check for delete */
    if (STRCMP (pHttp->au1Value, "DELETE") == 0)
    {
        if (nmhTestv2IssL2FilterStatus (&u4ErrorCode, i4FilterNo, ISS_DESTROY)
            == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "MAC filter not available.");
            return;
        }

        if (nmhSetIssL2FilterStatus (i4FilterNo, ISS_DESTROY) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "MAC filter not available.");
            return;
        }
        ISS_UNLOCK ();
        IssDxProcessMACFilterConfPageGet (pHttp);
        return;
    }

    if (STRCMP (pHttp->au1Value, "Apply") == 0)
    {
        u1Apply = 1;
        u1RowStatus = ISS_NOT_IN_SERVICE;
    }

    STRCPY (pHttp->au1Name, "SOURCE_MAC");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    StrToMac (pHttp->au1Value, SrcMacAddr);

    STRCPY (pHttp->au1Name, "DESTINATION_MAC");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    issDecodeSpecialChar (pHttp->au1Value);
    StrToMac (pHttp->au1Value, DestMacAddr);

    STRCPY (pHttp->au1Name, "ACTION_KEY");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Action = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PRIORITY");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4Priority = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "VLAN_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4VlanId = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "IN_PORT_LIST");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRLEN (pHttp->au1Value) != 0)
    {
        MEMSET (u1PortList, 0, ISS_PORTLIST_LEN);
        issDecodeSpecialChar (pHttp->au1Value);

        if (STRCMP (pHttp->au1Value, "0") != 0)
        {
            if (ConvertStrToPortList (pHttp->au1Value, u1PortList,
                                      ISS_PORTLIST_LEN,
                                      SYS_DEF_MAX_PHYSICAL_INTERFACES) ==
                OSIX_FAILURE)
            {
                ISS_UNLOCK ();
                IssSendError (pHttp, (INT1 *) "Check the allowed port list");
                return;
            }
        }
        InPortList.i4_Length = ISS_PORTLIST_LEN;
        InPortList.pu1_OctetList = MEM_CALLOC (InPortList.i4_Length, 1, UINT1);
        if (InPortList.pu1_OctetList == NULL)
        {
            ISS_UNLOCK ();
            return;
        }
        MEMCPY (InPortList.pu1_OctetList, u1PortList, InPortList.i4_Length);
    }
    STRCPY (pHttp->au1Name, "ENCAPTYPE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4EtherType = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "PROTOCOL");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4Protocol = ATOI (pHttp->au1Value);
    if (u4Protocol == OTHER)
    {
        STRCPY (pHttp->au1Name, "OTHER");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4Protocol = ATOI (pHttp->au1Value);
    }
    if (nmhTestv2IssL2FilterStatus (&u4ErrorCode, i4FilterNo,
                                    u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        IssSendError (pHttp, (INT1 *) "Invalid MAC filter number.");
        return;
    }

    if (nmhTestv2IssL2FilterAction (&u4ErrorCode, i4FilterNo, i4Action)
        == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        IssSendError (pHttp, (INT1 *) "Unable to set the l2 filter action.");
        return;
    }

    if (MEMCMP (au1ZeroMac, SrcMacAddr, ISS_MAC_LEN))
    {
        if (nmhTestv2IssL2FilterSrcMacAddr (&u4ErrorCode, i4FilterNo,
                                            SrcMacAddr) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            IssSendError (pHttp, (INT1 *) "Unable to set the filter with "
                          "this source MAC Address.");
            return;
        }
    }

    if (MEMCMP (au1ZeroMac, DestMacAddr, ISS_MAC_LEN))
    {
        if (nmhTestv2IssL2FilterDstMacAddr (&u4ErrorCode, i4FilterNo,
                                            DestMacAddr) == SNMP_FAILURE)
        {
            ISS_UNLOCK ();
            MEM_FREE (InPortList.pu1_OctetList);
            IssSendError (pHttp, (INT1 *) "Unable to set the filter with this"
                          " destination MAC Address.");
            return;
        }
    }

    if (nmhTestv2IssL2FilterPriority (&u4ErrorCode, i4FilterNo, i4Priority)
        == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        IssSendError (pHttp, (INT1 *) "Unable to set filter priority");
        return;
    }

    if ((u4Protocol != 0) && (nmhTestv2IssL2FilterProtocolType
                              (&u4ErrorCode, i4FilterNo, u4Protocol)
                              == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        IssSendError (pHttp,
                      (INT1 *) "Unable to set MAC filter protocol type!");
        return;
    }

    if ((i4EtherType != 0) && (nmhTestv2IssL2FilterEtherType
                               (&u4ErrorCode, i4FilterNo, i4EtherType)
                               == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        IssSendError (pHttp,
                      (INT1 *) "Unable to set MAC the filter ether type");
        return;
    }

    if ((i4VlanId != 0) && (nmhTestv2IssL2FilterVlanId
                            (&u4ErrorCode, i4FilterNo,
                             i4VlanId) == SNMP_FAILURE))
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        IssSendError (pHttp, (INT1 *) "Unable to set MAC filter VLAN ID.");
        return;
    }

    if ((InPortList.pu1_OctetList != NULL) && (nmhTestv2IssL2FilterInPortList
                                               (&u4ErrorCode, i4FilterNo,
                                                &InPortList)) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        IssSendError (pHttp, (INT1 *) "Unable to set this in port list.");
        return;
    }

    if (nmhSetIssL2FilterStatus (i4FilterNo, u1RowStatus) == SNMP_FAILURE)
    {
        ISS_UNLOCK ();
        MEM_FREE (InPortList.pu1_OctetList);
        IssSendError (pHttp, (INT1 *) "Entry already exists for this MAC"
                      " filter number");
        return;
    }

    if (nmhSetIssL2FilterAction (i4FilterNo, i4Action) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the l2 filter action.");
        return;
    }

    if (MEMCMP (au1ZeroMac, SrcMacAddr, ISS_MAC_LEN))
    {
        if (nmhSetIssL2FilterSrcMacAddr (i4FilterNo, SrcMacAddr)
            == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssL2FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set the source MAC Address.");
            return;
        }
    }

    if (MEMCMP (au1ZeroMac, DestMacAddr, ISS_MAC_LEN))
    {
        if (nmhSetIssL2FilterDstMacAddr
            (i4FilterNo, DestMacAddr) == SNMP_FAILURE)
        {
            MEM_FREE (InPortList.pu1_OctetList);
            if (u1Apply != 1)
            {
                nmhSetIssL2FilterStatus (i4FilterNo, ISS_DESTROY);
            }
            ISS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *)
                          "Unable to set the Destination MAC Address.");
            return;
        }
    }

    if (nmhSetIssL2FilterPriority (i4FilterNo, i4Priority) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter priority.");
        return;
    }

    if ((u4Protocol != 0) && (nmhSetIssL2FilterProtocolType
                              (i4FilterNo, u4Protocol) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter protocol type.");
        return;
    }

    if ((i4EtherType != 0) && (nmhSetIssL2FilterEtherType
                               (i4FilterNo, i4EtherType) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter ether type.");
        return;
    }

    if ((i4VlanId != 0) && (nmhSetIssL2FilterVlanId
                            (i4FilterNo, i4VlanId) == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set filter VLAN ID.");
        return;
    }

    if ((InPortList.pu1_OctetList != NULL) && (nmhSetIssL2FilterInPortList
                                               (i4FilterNo, &InPortList)
                                               == SNMP_FAILURE))
    {
        MEM_FREE (InPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set In Port List.");
        return;
    }

    if (nmhSetIssL2FilterStatus (i4FilterNo, ISS_ACTIVE) == SNMP_FAILURE)
    {
        MEM_FREE (InPortList.pu1_OctetList);
        if (u1Apply != 1)
        {
            nmhSetIssL2FilterStatus (i4FilterNo, ISS_DESTROY);
        }
        ISS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the filter status.");
        return;
    }
    ISS_UNLOCK ();
    MEM_FREE (InPortList.pu1_OctetList);
    IssDxProcessMACFilterConfPageGet (pHttp);
}

#ifdef DIFFSRV_WANTED

/*********************************************************************
 *  Function Name : IssDxProcessDfsPolicyMapPage
 *  Description   : This function processes the request coming for the
 *                  DiffSrv Policy Map page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *********************************************************************/

VOID
IssDxProcessDfsPolicyMapPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssDxProcessDfsPolicyMapPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssDxProcessDfsPolicyMapPageSet (pHttp);
    }
    return;
}

/***************************************************************************
 *  Function Name : IssDxProcessDfsPolicyMapPageGet
 *  Description   : This function processes the Get request coming for the
 *                  Diff Server Policy Map Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 *****************************************************************************/
VOID
IssDxProcessDfsPolicyMapPageGet (tHttp * pHttp)
{
    tDiffServWebClfrData PolicyMapData;
    INT4                i4ClfrId;
    INT4                i4OutCome = 0;
    INT4                i4PolicyMapId, i4NextPolicyMapId;
    INT4                i4OutProfActionId = 0;
    INT4                i4MeterId;
    INT4                i4InProfActionType = 0;
    INT4                i4OutProfActionType = 0;
    UINT4               u4TrafficRate;
    UINT4               u4Temp = 0;

    pHttp->i4Write = 0;
    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    WebnmRegisterLock (pHttp, DfsLock, DfsUnLock);

    DFS_LOCK ();

    i4OutCome = (INT4) nmhGetFirstIndexFsDiffServClfrTable (&i4PolicyMapId);

    if (i4OutCome == SNMP_FAILURE)
    {
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        DFS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        return;
    }
    u4Temp = pHttp->i4Write;
    do
    {
        pHttp->i4Write = u4Temp;
        STRCPY (pHttp->au1KeyString, "POLICY_MAPID");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4PolicyMapId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        i4OutCome = nmhGetFsDiffServClfrMFClfrId (i4PolicyMapId, &i4ClfrId);
        STRCPY (pHttp->au1KeyString, "CLFR_ID");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (i4OutCome != SNMP_FAILURE)
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4ClfrId);
        }
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));

        i4OutCome = nmhGetFsDiffServClfrOutProActionId (i4PolicyMapId,
                                                        &i4OutProfActionId);
        i4OutCome &= nmhGetFsDiffServOutProfileActionMID (i4OutProfActionId,
                                                          &(i4MeterId));
        u4TrafficRate = 0;
        i4OutCome &= nmhGetFsDiffServMeterRefreshCount (i4MeterId,
                                                        &u4TrafficRate);
        STRCPY (pHttp->au1KeyString, "TRAFFIC_RATE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if ((i4OutProfActionId != 0) && (i4MeterId != 0) &&
            (i4OutCome != SNMP_FAILURE))
        {
            if (u4TrafficRate > 0)
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4TrafficRate);
            }
            else
            {
                SPRINTF ((CHR1 *) pHttp->au1DataString, " ");
            }
        }
        else
        {
            SPRINTF ((CHR1 *) pHttp->au1DataString, " ");
        }

        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        if (DsWebnmGetPolicyMapEntry (i4PolicyMapId, &PolicyMapData)
            == SNMP_FAILURE)
        {
            WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
            i4NextPolicyMapId = i4PolicyMapId;
            continue;
        }

        STRCPY (pHttp->au1KeyString, "INPROF_ACT_TYPE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (STRCMP (PolicyMapData.au1DsClfrInProfActionType, "Policed-Dscp") ==
            0)
        {
            i4InProfActionType = 1;
        }
        else if (STRCMP
                 (PolicyMapData.au1DsClfrInProfActionType,
                  "Policed-Precedence") == 0)
        {
            i4InProfActionType = 2;
        }
        else if (STRCMP
                 (PolicyMapData.au1DsClfrInProfActionType,
                  "Policed-Priority") == 0)
        {
            i4InProfActionType = 3;
        }
        else
        {
            i4InProfActionType = 0;
        }
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4InProfActionType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "INPROF_ACT_VALUE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                 PolicyMapData.au1DsClfrInProfActionValue);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OUTPROF_ACT_TYPE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        if (STRCMP (PolicyMapData.au1DsClfrOutProfActionType, "Drop") == 0)
        {
            i4OutProfActionType = 1;
        }
        else if (STRCMP
                 (PolicyMapData.au1DsClfrOutProfActionType,
                  "Policed-Dscp") == 0)
        {
            i4OutProfActionType = 2;
        }
        else
        {
            i4OutProfActionType = 0;
        }
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4OutProfActionType);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        STRCPY (pHttp->au1KeyString, "OUTPROF_ACT_VALUE");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%s",
                 PolicyMapData.au1DsClfrOutProfActionValue);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
        i4NextPolicyMapId = i4PolicyMapId;
    }
    while (nmhGetNextIndexFsDiffServClfrTable (i4NextPolicyMapId,
                                               &i4PolicyMapId) == SNMP_SUCCESS);

    DFS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
}

/*********************************************************************
 *  Function Name : IssDxProcessDfsPolicyMapPageSet
 *  Description   : This function processes the request coming for the
 *                  Diff Server Policy Map Page.
 *
 *  Input(s)      : pHttp - Pointer to the global HTTP data structure.
 *  Output(s)     : None.
 *  Return Values : None
 ***********************************************************************/
VOID
IssDxProcessDfsPolicyMapPageSet (tHttp * pHttp)
{
    tDiffServWebSetClfrEntry ClfrEntry;
    UINT1               au1ErrStr[255];
    UINT1               u1Apply = 0;
    INT4                i4PolicyMapId;
    INT4                i4ClassMapId;
    INT4                i4InProfActionId;
    INT4                i4OutProfActionId;
    INT4                i4MeterId;
    INT4                i4FirstClassMapId;
    INT4                i4NextClassMapId;
    INT4                i4MFCId;
    INT4                i4Status;
    INT4                i4DsStatus;
    UINT4               u4ErrorCode;
    UINT4               u4TrafficRate;
    UINT4               u4OutProfActType;
    UINT4               u4InDscp = 0;
    UINT4               u4InPrec = 0;
    UINT4               u4OutDscp = 0;
    UINT4               u4CosValue = 0;
    UINT4               u4InProfActType = 0;
    UINT1               u1RowStatus = ISS_CREATE_AND_WAIT;

    /* Get the Policy-Map ID */
    STRCPY (pHttp->au1Name, "POLICY_MAPID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PolicyMapId = ATOI (pHttp->au1Value);

    DFS_LOCK ();

    nmhGetFsDsStatus (&i4DsStatus);
    if (i4DsStatus == 2)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "DiffServer not enabled.");
        return;
    }
    /* Check whether the request is for ADD/DELETE */
    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);

    /* check for delete */
    if (STRCMP (pHttp->au1Value, "DELETE") == 0)
    {
        if (nmhGetFsDiffServClfrStatus
            (i4PolicyMapId, &i4Status) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Policy Map entry not available.");
            return;
        }

        if ((i4Status == ISS_ACTIVE) &&
            (nmhSetFsDiffServClfrStatus (i4PolicyMapId,
                                         ISS_NOT_IN_SERVICE) == SNMP_FAILURE))
        {
            DFS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Cannot modify Policy Map entry status.");
            return;
        }

        nmhGetFsDiffServClfrInProActionId (i4PolicyMapId, &i4InProfActionId);
        if (i4InProfActionId != 0)
        {
            if (nmhGetFsDiffServInProfileActionStatus
                (i4InProfActionId, &i4Status) != SNMP_FAILURE)
            {

                if (nmhSetFsDiffServInProfileActionStatus
                    (i4InProfActionId, ISS_DESTROY) == SNMP_FAILURE)
                {
                    DFS_UNLOCK ();
                    IssSendError (pHttp,
                                  (INT1 *) "In-Profile Id does not exists");
                    return;
                }
            }
        }

        nmhGetFsDiffServClfrOutProActionId (i4PolicyMapId, &i4OutProfActionId);
        nmhGetFsDiffServOutProfileActionMID (i4OutProfActionId, &i4MeterId);
        if (i4OutProfActionId != 0)
        {
            if (nmhGetFsDiffServOutProfileActionStatus
                (i4OutProfActionId, &i4Status) != SNMP_FAILURE)
            {
                if (nmhSetFsDiffServOutProfileActionStatus
                    (i4OutProfActionId, ISS_DESTROY) == SNMP_FAILURE)
                {
                    DFS_UNLOCK ();
                    IssSendError (pHttp,
                                  (INT1 *) "Out-Profile Id does not exists");
                    return;
                }
            }
            if (i4MeterId != 0)
            {
                if (nmhGetFsDiffServMeterStatus
                    (i4MeterId, &i4Status) != SNMP_FAILURE)
                {
                    if (nmhSetFsDiffServMeterStatus
                        (i4MeterId, ISS_DESTROY) == SNMP_FAILURE)
                    {
                        DFS_UNLOCK ();
                        IssSendError (pHttp,
                                      (INT1 *) "Meter Id does not exists");
                        return;
                    }
                }
            }
        }
        if (nmhGetFsDiffServClfrStatus
            (i4PolicyMapId, &i4Status) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            return;
        }

        if (nmhSetFsDiffServClfrStatus (i4PolicyMapId,
                                        ISS_DESTROY) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Cannot delete Policy Map entry.");
            return;
        }
        DFS_UNLOCK ();
        IssDxProcessDfsPolicyMapPageGet (pHttp);
        return;
    }

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "apply") == 0)
    {
        u1Apply = 1;
        u1RowStatus = ISS_NOT_IN_SERVICE;
    }

    STRCPY (pHttp->au1Name, "CLFR_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4ClassMapId = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "ACTION");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    if (STRCMP (pHttp->au1Value, "Add") == 0)
    {
        /* Checking with Policy-Map ID */
        STRCPY (pHttp->au1Name, "POLICY_MAPID");
        if (nmhTestv2FsDiffServClfrStatus
            (&u4ErrorCode, i4PolicyMapId, u1RowStatus) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Invalid Policy Map ID.");
            return;
        }
        /* Check if class map exists */
        if (nmhGetFsDiffServMultiFieldClfrStatus
            (i4ClassMapId, &i4Status) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp, (INT1 *) "Invalid Class Map ID");
            return;
        }

        /* Check if the class-map already allocated to another policy-map.
         * We can't allocate the same class-map to different policy-map, because
         * Class-Map id == Datapath id */
        if (nmhGetFirstIndexFsDiffServClfrTable (&i4FirstClassMapId)
            != SNMP_FAILURE)
        {
            /* For the first time */
            i4NextClassMapId = i4FirstClassMapId;
            do
            {
                nmhGetFsDiffServClfrMFClfrId (i4NextClassMapId, &i4MFCId);
                if (i4MFCId == i4ClassMapId)
                {
                    DFS_UNLOCK ();
                    IssSendError (pHttp,
                                  "Class map already allocated to another policy-map");
                    return;
                }

                i4FirstClassMapId = i4NextClassMapId;
            }
            while (nmhGetNextIndexFsDiffServClfrTable
                   (i4FirstClassMapId, &i4NextClassMapId) != SNMP_FAILURE);
        }
    }

    STRCPY (pHttp->au1Name, "TRAFFIC_RATE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4TrafficRate = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "INPROF_ACT_TYPE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4InProfActType = ATOI (pHttp->au1Value);

    if (u4InProfActType == 1)
    {
        STRCPY (pHttp->au1Name, "INPROF_ACT_VALUE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4InDscp = ATOI (pHttp->au1Value);
    }
    else if (u4InProfActType == 2)
    {
        STRCPY (pHttp->au1Name, "INPROF_ACT_VALUE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4InPrec = ATOI (pHttp->au1Value);
    }
    else if (u4InProfActType == 3)
    {
        STRCPY (pHttp->au1Name, "INPROF_ACT_VALUE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4CosValue = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1Name, "OUTPROF_ACT_TYPE");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4OutProfActType = ATOI (pHttp->au1Value);

    if (u4OutProfActType == 2)
    {
        STRCPY (pHttp->au1Name, "OUTPROF_ACT_VALUE");
        HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value,
                            pHttp->au1PostQuery);
        u4OutDscp = ATOI (pHttp->au1Value);
    }

    MEMSET (&ClfrEntry, 0, sizeof (tDiffServWebSetClfrEntry));
    ClfrEntry.i4DsClfrId = i4PolicyMapId;
    ClfrEntry.i4DsClfrMFClfrId = i4ClassMapId;
    ClfrEntry.i4DsClfrInProActionId = i4PolicyMapId;
    ClfrEntry.u4InProfActType = u4InProfActType;
    ClfrEntry.u4InDscp = u4InDscp;
    ClfrEntry.u4InPrec = u4InPrec;
    ClfrEntry.u4CosValue = u4CosValue;
    ClfrEntry.i4DsClfrOutProActionId = i4PolicyMapId;
    ClfrEntry.i4MeterId = i4PolicyMapId;
    ClfrEntry.u4OutProfActType = u4OutProfActType;
    ClfrEntry.u4OutDscp = u4OutDscp;
    ClfrEntry.u4TrafficRate = u4TrafficRate;

    if (DsWebnmSetPolicyMapEntry (&ClfrEntry, u1RowStatus, au1ErrStr) ==
        SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) au1ErrStr);
        IssDxProcessDfsPolicyMapPageGet (pHttp);
        return;
    }

    DFS_UNLOCK ();
    IssDxProcessDfsPolicyMapPageGet (pHttp);
}

/*********************************************************************
 *  function name : IssDxProcessSchdAlgoPage
 *  description   : This function processes the request coming for the
 *                  diffsrv Cosq Schedule Algorithm configuration page.
 *
 *  input(s)      : phttp - pointer to the global http data structure.
 *  output(s)     : none.
 *  return values : none
 *********************************************************************/

VOID
IssDxProcessSchdAlgoPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssDxProcessSchdAlgoPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssDxProcessSchdAlgoPageSet (pHttp);
    }
    return;
}

/*********************************************************************
 *  function name : IssDxProcessSchdAlgoPageGet
 *  description   : This function processes the get request coming for the
 *                  diffsrv Cosq Schedule Algorithm configuration page.
 *                  Get
 *  input(s)      : phttp - pointer to the global http data structure.
 *  output(s)     : none.
 *  return values : none
 *********************************************************************/

VOID
IssDxProcessSchdAlgoPageGet (tHttp * pHttp)
{
    INT4                i4NextIndex = 0;
    INT4                i4PreIndex = 0;
    INT4                i4CoSqAlg = 0;
    INT4                i4OutCome = 0;
    UINT4               u4Temp = 0;

    WebnmRegisterLock (pHttp, DfsLock, DfsUnLock);
    DFS_LOCK ();

    i4OutCome = nmhGetFirstIndexFsDiffServCoSqAlgorithmTable (&i4NextIndex);
    if (i4OutCome == SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        WebnmUnRegisterLock (pHttp);
        IssSkipString (pHttp, ISS_TABLE_STOP_FLAG);
        return;
    }

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = pHttp->i4Write;

    do
    {
        pHttp->i4Write = u4Temp;

        STRCPY (pHttp->au1KeyString, "PORT_NUM_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4NextIndex);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDiffServCoSqAlgorithm (i4NextIndex, &i4CoSqAlg);
        STRCPY (pHttp->au1KeyString, "COSQ_ALGO_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4CoSqAlg);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);

        i4PreIndex = i4NextIndex;
    }
    while (nmhGetNextIndexFsDiffServCoSqAlgorithmTable
           (i4PreIndex, &i4NextIndex) == SNMP_SUCCESS);
    DFS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;

}

/*********************************************************************
 *  function name : IssDxProcessSchdAlgoPageSet
 *  description   : This function processes the set request coming for the
 *                  diffsrv Cosq Schedule Algorithm configuration page.
 *                  Set
 *  input(s)      : phttp - pointer to the global http data structure.
 *  output(s)     : none.
 *  return values : none
 *********************************************************************/

VOID
IssDxProcessSchdAlgoPageSet (tHttp * pHttp)
{
    INT4                i4PortNum = 0;
    INT4                i4CoSqAlg = 0;
    UINT4               u4ErrCode = 0;

    STRCPY (pHttp->au1Name, "PORT_NUM");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PortNum = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "COSQ_ALGO");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4CoSqAlg = ATOI (pHttp->au1Value);

    WebnmRegisterLock (pHttp, DfsLock, DfsUnLock);
    DFS_LOCK ();

    if (nmhTestv2FsDiffServCoSqAlgorithm
        (&u4ErrCode, i4PortNum, i4CoSqAlg) == SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the CoSq Algorithm");
        return;
    }

    if (nmhSetFsDiffServCoSqAlgorithm (i4PortNum, i4CoSqAlg) == SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the CoSq Algorithm");
        return;
    }

    DFS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    IssDxProcessSchdAlgoPageGet (pHttp);
}

/*********************************************************************
 *  function name : IssDxProcessDfsCosqWeightBWConfPage
 *  description   : This function processes the request coming for the
 *                  diffsrv Cosq Weight and Bandwidth configuration page.
 *
 *  input(s)      : phttp - pointer to the global http data structure.
 *  output(s)     : none.
 *  return values : none
 *********************************************************************/

VOID
IssDxProcessDfsCosqWeightBWConfPage (tHttp * pHttp)
{
    if (pHttp->i4Method == ISS_GET)
    {
        IssDxProcessDfsCosqWeightBWConfPageGet (pHttp);
    }
    else if (pHttp->i4Method == ISS_SET)
    {
        IssDxProcessDfsCosqWeightBWConfPageSet (pHttp);
    }
    return;
}

/*********************************************************************
 *  function name : IssDxProcessDfsCosqWeightBWConfPageGet
 *  description   : This function processes the get request coming for the
 *                  diffsrv Cosq Weight and Bandwidth configuration page.
 *                  Get
 *  input(s)      : phttp - pointer to the global http data structure.
 *  output(s)     : none.
 *  return values : none
 *********************************************************************/

VOID
IssDxProcessDfsCosqWeightBWConfPageGet (tHttp * pHttp)
{
    INT4                i4PortNum = 0;
    INT4                i4CosqId = 0;
    UINT4               u4CosqMaxBw = 0;
    UINT4               u4CosqMinBw = 0;
    UINT4               u4CosqWt = 0;
    UINT4               i4CosqFlag = 0;
    UINT4               u4Temp = 0;

    IssPrintAvailableL2Ports (pHttp);

    WebnmRegisterLock (pHttp, DfsLock, DfsUnLock);
    DFS_LOCK ();

    STRCPY (pHttp->au1Name, "PORT_NUM");
    if (HttpGetValuebyName
        (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery) == ENM_FAILURE)
    {
        nmhGetFirstIndexFsDiffServCoSqWeightBwTable (&i4PortNum, &i4CosqId);
    }
    else
    {
        i4PortNum = ATOI (pHttp->au1Value);
    }

    STRCPY (pHttp->au1KeyString, "PORT_NUM_KEY");
    WebnmSendString (pHttp, pHttp->au1KeyString);
    SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4PortNum);
    WebnmSockWrite (pHttp, pHttp->au1DataString, STRLEN (pHttp->au1DataString));

    WebnmSendString (pHttp, ISS_TABLE_FLAG);
    u4Temp = pHttp->i4Write;

    for (; i4CosqId <= 7; i4CosqId++)
    {
        pHttp->i4Write = u4Temp;

        STRCPY (pHttp->au1KeyString, "COSQ_ID_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4CosqId);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDiffServCoSqWeight (i4PortNum, i4CosqId, &u4CosqWt);
        STRCPY (pHttp->au1KeyString, "COSQ_WEIGHT_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4CosqWt);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDiffServCoSqBwMin (i4PortNum, i4CosqId, &u4CosqMinBw);
        STRCPY (pHttp->au1KeyString, "COSQ_MIN_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4CosqMinBw);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDiffServCoSqBwMax (i4PortNum, i4CosqId, &u4CosqMaxBw);
        STRCPY (pHttp->au1KeyString, "COSQ_MAX_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", u4CosqMaxBw);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);

        nmhGetFsDiffServCoSqBwFlags (i4PortNum, i4CosqId, &i4CosqFlag);
        STRCPY (pHttp->au1KeyString, "COSQ_FLAG_KEY");
        WebnmSendString (pHttp, pHttp->au1KeyString);
        SPRINTF ((CHR1 *) pHttp->au1DataString, "%d", i4CosqFlag);
        WebnmSockWrite (pHttp, pHttp->au1DataString,
                        STRLEN (pHttp->au1DataString));
        WebnmSendString (pHttp, ISS_PARAM_STOP_FLAG);
        WebnmSendString (pHttp, ISS_TABLE_STOP_FLAG);
    }
    DFS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    WebnmSockWrite (pHttp, (UINT1 *) (pHttp->pi1Html + pHttp->i4Write),
                    (pHttp->i4HtmlSize - pHttp->i4Write));
    return;

}

/*********************************************************************
 *  function name : IssDxProcessDfsCosqWeightBWConfPageSet
 *  description   : This function processes the request coming for the
 *                  diffsrv Cosq Weight and Bandwidth configuration page.
 *                  Set
 *  input(s)      : phttp - pointer to the global http data structure.
 *  output(s)     : none.
 *  return values : none
 *********************************************************************/

VOID
IssDxProcessDfsCosqWeightBWConfPageSet (tHttp * pHttp)
{
    INT4                i4PortNum = 0;
    INT4                i4CosqId = 0;
    UINT4               u4CosqMinBw = 0;
    UINT4               u4CosqWt = 0;
    UINT4               u4ErrCode = 0;

    STRCPY (pHttp->au1Name, "PORT_NUM");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4PortNum = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "COSQ_ID");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    i4CosqId = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "COSQ_WEIGHT");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4CosqWt = ATOI (pHttp->au1Value);

    STRCPY (pHttp->au1Name, "COSQ_MIN");
    HttpGetValuebyName (pHttp->au1Name, pHttp->au1Value, pHttp->au1PostQuery);
    u4CosqMinBw = ATOI (pHttp->au1Value);

    WebnmRegisterLock (pHttp, DfsLock, DfsUnLock);
    DFS_LOCK ();

    if (nmhTestv2FsDiffServCoSqWeight
        (&u4ErrCode, i4PortNum, i4CosqId, u4CosqWt) == SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the CoSq Weight");
        return;
    }

    if (nmhSetFsDiffServCoSqWeight (i4PortNum, i4CosqId, u4CosqWt) ==
        SNMP_FAILURE)
    {
        DFS_UNLOCK ();
        IssSendError (pHttp, (INT1 *) "Unable to set the CoSq Weight");
        return;
    }

    if (u4CosqMinBw != 0)
    {
        if (nmhTestv2FsDiffServCoSqBwMin
            (&u4ErrCode, i4PortNum, i4CosqId, u4CosqMinBw) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set the CoSq Minimum Bandwidth");
            return;
        }

        if (nmhSetFsDiffServCoSqBwMin
            (i4PortNum, i4CosqId, u4CosqMinBw) == SNMP_FAILURE)
        {
            DFS_UNLOCK ();
            IssSendError (pHttp,
                          (INT1 *) "Unable to set the CoSq Minimum Bandwidth");
            return;
        }
    }

    DFS_UNLOCK ();
    WebnmUnRegisterLock (pHttp);
    IssDxProcessDfsCosqWeightBWConfPageGet (pHttp);
}
#endif
#endif
