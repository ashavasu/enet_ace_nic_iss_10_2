Steps to compile ISS in VxWorks environment:
============================================
1. Create a build directory, say,  "IssBuild"
2. Go to the directory "IssBuild"
3. untar sdk in that directory ( may have to Untar the <sdkxxx.tgz> ) 
4. untar ISS source code also in that directory (may have to untar <ISSxxx.tgz>" )
5. Copy the "future" directory from ISS source code to "sdk-4.1.3/firmware/future" as below:

   IssBuild
      |
       - sdk-x.x.x
            |
            - Firmware
                |
                 - future


6. In the directory sdk-4.1.3/firmware/future/LR,
   - Edit line number 15 of make.h to specify the TARGET_OS to be VXWORKS.
     "TARGET_OS = OS_VXWORKS"
7. In directory "sdk-4.1.3/firmware/future" do the following
   - move all the files from "future/npapi/bcm5690" to 
     "future/npapi"
8. Go to sdk-4.1.3/firmware/systems/bmw
  - Edit the batch file "envset.bat" with appropriate path for sdk and tornado
  - Run the batch file "envset.bat". This will pop-up a command window.

9. Issue 'make' from that command window in "sdk-4.1.3/firmware/systems/bmw"
10. After a successful make, "bcm.bmw_p_spt"( the Image to be downloaded)
    will be created in sdk-4.1.3/firmware/systems/bmw

Note:
-----
 - While building ISS in vxworks environment, if build stops at "wtxtcl>" prompt, 
   give "exit" and again give a make

 - The ISS cli commands scan tool is a Linux based tool. 
   Hence whenever any ISS module is disabled in the build or any cli command is 
   modified, new "clicmds.c" file has to be generated in Linux environment  and is linked in vxworks build environment.

How to create clicmds.c in Linux Environment:
=============================================
-code/future/cli/src/clicmds.c has to be generated in a Linux environment through the below steps::
  a) Copy the ISS  source code in a linux system
  b) Go to code/future/LR
  c) Edit make.h & provide appropriate path for "future" directory in "BASE_DIR"
  d) Ensure that "TARGET_OS    = OS_TMO" in Line number 15.
  e) Change the following lines starting from line number 300 in make.h, to

       ifeq (${TARGET_OS}, OS_TMO)
               NPAPI                        = YES
               MFWD                         = NO
       endif 

  f) Issue 'make clean' and 'make' from code/future/cli 
  g) clicmds.c file will be generated in code/future/cli/src

- This clicmds.c file should be copied to
   "sdk-4.1.3/firmware/future/cli/src/clicmds.c" in vxworks build environment
   if the modifed cli commands have to be included in the build


