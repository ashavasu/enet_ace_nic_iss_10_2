/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsiss.h,v 1.29 2017/12/07 10:07:31 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/

#ifndef _SNMP_MIB_H
#define _SNMP_MIB_H

/* SNMP-MIB translation table. */

static struct MIB_OID fs_iss_mib_oid_table[] = {
    {"ccitt",        "0"},
    {"iso",        "1"},
    {"lldpExtensions",        "1.0.8802.1.1.2.1.5"},
    {"org",        "1.3"},
    {"dod",        "1.3.6"},
    {"internet",        "1.3.6.1"},
    {"directory",        "1.3.6.1.1"},
    {"mgmt",        "1.3.6.1.2"},
    {"mib-2",        "1.3.6.1.2.1"},
    {"ip",        "1.3.6.1.2.1.4"},
    {"transmission",        "1.3.6.1.2.1.10"},
    {"mplsStdMIB",        "1.3.6.1.2.1.10.166"},
    {"dot1dBridge",        "1.3.6.1.2.1.17"},
    {"dot1dStp",        "1.3.6.1.2.1.17.2"},
    {"dot1dTp",        "1.3.6.1.2.1.17.4"},
    {"vrrpOperEntry",        "1.3.6.1.2.1.18.1.3.1"},
    {"experimental",        "1.3.6.1.3"},
    {"private",        "1.3.6.1.4"},
    {"enterprises",        "1.3.6.1.4.1"},
    {"iss",        "1.3.6.1.4.1.2076.81"},
    {"issNotifications",        "1.3.6.1.4.1.2076.81.0"},
    {"issSystem",        "1.3.6.1.4.1.2076.81.1"},
    {"issSwitchName",        "1.3.6.1.4.1.2076.81.1.1"},
    {"issHardwareVersion",        "1.3.6.1.4.1.2076.81.1.2"},
    {"issFirmwareVersion",        "1.3.6.1.4.1.2076.81.1.3"},
    {"issDefaultIpAddrCfgMode",        "1.3.6.1.4.1.2076.81.1.4"},
    {"issDefaultIpAddr",        "1.3.6.1.4.1.2076.81.1.5"},
    {"issDefaultIpSubnetMask",        "1.3.6.1.4.1.2076.81.1.6"},
    {"issEffectiveIpAddr",        "1.3.6.1.4.1.2076.81.1.7"},
    {"issDefaultInterface",        "1.3.6.1.4.1.2076.81.1.8"},
    {"issRestart",        "1.3.6.1.4.1.2076.81.1.9"},
    {"issConfigSaveOption",        "1.3.6.1.4.1.2076.81.1.10"},
    {"issConfigSaveIpAddr",        "1.3.6.1.4.1.2076.81.1.11"},
    {"issConfigSaveFileName",        "1.3.6.1.4.1.2076.81.1.12"},
    {"issInitiateConfigSave",        "1.3.6.1.4.1.2076.81.1.13"},
    {"issConfigSaveStatus",        "1.3.6.1.4.1.2076.81.1.14"},
    {"issConfigRestoreOption",        "1.3.6.1.4.1.2076.81.1.15"},
    {"issConfigRestoreIpAddr",        "1.3.6.1.4.1.2076.81.1.16"},
    {"issConfigRestoreFileName",        "1.3.6.1.4.1.2076.81.1.17"},
    {"issInitiateConfigRestore",        "1.3.6.1.4.1.2076.81.1.18"},
    {"issConfigRestoreStatus",        "1.3.6.1.4.1.2076.81.1.19"},
    {"issDlImageFromIp",        "1.3.6.1.4.1.2076.81.1.20"},
    {"issDlImageName",        "1.3.6.1.4.1.2076.81.1.21"},
    {"issInitiateDlImage",        "1.3.6.1.4.1.2076.81.1.22"},
    {"issLoggingOption",        "1.3.6.1.4.1.2076.81.1.23"},
    {"issUploadLogFileToIp",        "1.3.6.1.4.1.2076.81.1.24"},
    {"issLogFileName",        "1.3.6.1.4.1.2076.81.1.25"},
    {"issInitiateUlLogFile",        "1.3.6.1.4.1.2076.81.1.26"},
    {"issRemoteSaveStatus",        "1.3.6.1.4.1.2076.81.1.27"},
    {"issDownloadStatus",        "1.3.6.1.4.1.2076.81.1.28"},
    {"issSysContact",        "1.3.6.1.4.1.2076.81.1.29"},
    {"issSysLocation",        "1.3.6.1.4.1.2076.81.1.30"},
    {"issLoginAuthentication",        "1.3.6.1.4.1.2076.81.1.31"},
    {"issSwitchBaseMacAddress",        "1.3.6.1.4.1.2076.81.1.32"},
    {"issOOBInterface",        "1.3.6.1.4.1.2076.81.1.33"},
    {"issSwitchDate",        "1.3.6.1.4.1.2076.81.1.34"},
    {"issNoCliConsole",        "1.3.6.1.4.1.2076.81.1.35"},
    {"issDefaultIpAddrAllocProtocol",        "1.3.6.1.4.1.2076.81.1.36"},
    {"issHttpPort",        "1.3.6.1.4.1.2076.81.1.37"},
    {"issHttpStatus",        "1.3.6.1.4.1.2076.81.1.38"},
    {"issConfigRestoreFileVersion",        "1.3.6.1.4.1.2076.81.1.39"},
    {"issDefaultRmIfName",        "1.3.6.1.4.1.2076.81.1.40"},
    {"issDefaultVlanId",        "1.3.6.1.4.1.2076.81.1.41"},
    {"issNpapiMode",        "1.3.6.1.4.1.2076.81.1.42"},
    {"issConfigAutoSaveTrigger",        "1.3.6.1.4.1.2076.81.1.43"},
    {"issConfigIncrSaveFlag",        "1.3.6.1.4.1.2076.81.1.44"},
    {"issConfigRollbackFlag",        "1.3.6.1.4.1.2076.81.1.45"},
    {"issConfigSyncUpOperation",        "1.3.6.1.4.1.2076.81.1.46"},
    {"issFrontPanelPortCount",        "1.3.6.1.4.1.2076.81.1.47"},
    {"issAuditLogStatus",        "1.3.6.1.4.1.2076.81.1.48"},
    {"issAuditLogFileName",        "1.3.6.1.4.1.2076.81.1.49"},
    {"issAuditLogFileSize",        "1.3.6.1.4.1.2076.81.1.50"},
    {"issAuditLogReset",        "1.3.6.1.4.1.2076.81.1.51"},
    {"issAuditLogRemoteIpAddr",        "1.3.6.1.4.1.2076.81.1.52"},
    {"issAuditLogInitiateTransfer",        "1.3.6.1.4.1.2076.81.1.53"},
    {"issAuditTransferFileName",        "1.3.6.1.4.1.2076.81.1.54"},
    {"issDownLoadTransferMode",        "1.3.6.1.4.1.2076.81.1.55"},
    {"issDownLoadUserName",        "1.3.6.1.4.1.2076.81.1.56"},
    {"issDownLoadPassword",        "1.3.6.1.4.1.2076.81.1.57"},
    {"issUploadLogTransferMode",        "1.3.6.1.4.1.2076.81.1.58"},
    {"issUploadLogUserName",        "1.3.6.1.4.1.2076.81.1.59"},
    {"issUploadLogPasswd",        "1.3.6.1.4.1.2076.81.1.60"},
    {"issConfigSaveTransferMode",        "1.3.6.1.4.1.2076.81.1.61"},
    {"issConfigSaveUserName",        "1.3.6.1.4.1.2076.81.1.62"},
    {"issConfigSavePassword",        "1.3.6.1.4.1.2076.81.1.63"},
    {"issSwitchMinThresholdTemperature",        "1.3.6.1.4.1.2076.81.1.64"},
    {"issSwitchMaxThresholdTemperature",        "1.3.6.1.4.1.2076.81.1.65"},
    {"issSwitchCurrentTemperature",        "1.3.6.1.4.1.2076.81.1.66"},
    {"issSwitchMaxCPUThreshold",        "1.3.6.1.4.1.2076.81.1.67"},
    {"issSwitchCurrentCPUThreshold",        "1.3.6.1.4.1.2076.81.1.68"},
    {"issSwitchPowerSurge",        "1.3.6.1.4.1.2076.81.1.69"},
    {"issSwitchPowerFailure",        "1.3.6.1.4.1.2076.81.1.70"},
    {"issSwitchCurrentPowerSupply",        "1.3.6.1.4.1.2076.81.1.71"},
    {"issSwitchMaxRAMUsage",        "1.3.6.1.4.1.2076.81.1.72"},
    {"issSwitchCurrentRAMUsage",        "1.3.6.1.4.1.2076.81.1.73"},
    {"issSwitchMaxFlashUsage",        "1.3.6.1.4.1.2076.81.1.74"},
    {"issSwitchCurrentFlashUsage",        "1.3.6.1.4.1.2076.81.1.75"},
    {"issConfigRestoreFileFormatVersion", "1.3.6.1.4.1.2076.81.1.76"},
    {"issDebugOption",        "1.3.6.1.4.1.2076.81.1.77"},
    {"issConfigDefaultValueSaveOption",      "1.3.6.1.4.1.2076.81.1.78"},
    {"issConfigSaveIpAddrType",        "1.3.6.1.4.1.2076.81.1.79"},
    {"issConfigSaveIpvxAddr",        "1.3.6.1.4.1.2076.81.1.80"},
    {"issConfigRestoreIpAddrType",        "1.3.6.1.4.1.2076.81.1.81"},
    {"issConfigRestoreIpvxAddr",        "1.3.6.1.4.1.2076.81.1.82"},
    {"issDlImageFromIpAddrType",        "1.3.6.1.4.1.2076.81.1.83"},
    {"issDlImageFromIpvx",        "1.3.6.1.4.1.2076.81.1.84"},
    {"issUploadLogFileToIpAddrType",        "1.3.6.1.4.1.2076.81.1.85"},
    {"issUploadLogFileToIpvx",        "1.3.6.1.4.1.2076.81.1.86"},
    {"issAuditLogRemoteIpAddrType",        "1.3.6.1.4.1.2076.81.1.87"},
    {"issAuditLogRemoteIpvxAddr",        "1.3.6.1.4.1.2076.81.1.88"},
    {"issSystemTimerSpeed",        "1.3.6.1.4.1.2076.81.1.89"},
    {"IssMgmtInterfaceRouting",        "1.3.6.1.4.1.2076.81.1.90"},
    {"issMacLearnRateLimit",      "1.3.6.1.4.1.2076.81.1.91"},
    {"issMacLearnRateLimitInterval",      "1.3.6.1.4.1.2076.81.1.92"},
    {"issVrfUnqMacFlag",        "1.3.6.1.4.1.2076.81.1.93"},
    {"issLoginAttempts",        "1.3.6.1.4.1.2076.81.1.94"},
    {"issLoginLockTime",        "1.3.6.1.4.1.2076.81.1.95"},
    {"issAuditLogSizeThreshold",        "1.3.6.1.4.1.2076.81.1.96"},
    {"issTelnetStatus",        "1.3.6.1.4.1.2076.81.1.97"},
    {"issWebSessionTimeOut",    "1.3.6.1.4.1.2076.81.1.98"},
    {"issWebSessionMaxUsers",    "1.3.6.1.4.1.2076.81.1.99"},
    {"issHeartBeatMode",        "1.3.6.1.4.1.2076.81.1.100"},
    {"issRedundancyType",        "1.3.6.1.4.1.2076.81.1.101"},
    {"issRedDataPlaneType",        "1.3.6.1.4.1.2076.81.1.102"},
    {"issClearConfig",        "1.3.6.1.4.1.2076.81.1.103"},
    {"issClearConfigFileName",        "1.3.6.1.4.1.2076.81.1.104"},
    {"issTelnetClientStatus",        "1.3.6.1.4.1.2076.81.1.105"},
    {"issSshClientStatus",        "1.3.6.1.4.1.2076.81.1.106"},
    {"issActiveTelnetClientSessions",        "1.3.6.1.4.1.2076.81.1.107"},
    {"issActiveSshClientSessions",        "1.3.6.1.4.1.2076.81.1.108"},
    {"issLogFileSize",        "1.3.6.1.4.1.2076.81.1.109"},
    {"issLogReset",        "1.3.6.1.4.1.2076.81.1.110"},
    {"issLogSizeThreshold",        "1.3.6.1.4.1.2076.81.1.111"},
    {"issAutomaticPortCreate",        "1.3.6.1.4.1.2076.81.1.112"},
    {"issUlRemoteLogFileName",        "1.3.6.1.4.1.2076.81.1.113"},
    {"issDefaultExecTimeOut",        "1.3.6.1.4.1.2076.81.1.114"},
    {"issRmStackingInterfaceType",   "1.3.6.1.4.1.2076.81.1.115"},
    {"issPeerLoggingOption",        "1.3.6.1.4.1.2076.81.1.116"},
    {"issStandbyRestart",        "1.3.6.1.4.1.2076.81.1.117"},
    {"issRestoreType",        "1.3.6.1.4.1.2076.81.1.118"},
    {"issDebugTimeStampOption",        "1.3.6.1.4.1.2076.81.1.121"},
    {"issConfigControl",        "1.3.6.1.4.1.2076.81.2"},
    {"issConfigCtrlTable",        "1.3.6.1.4.1.2076.81.2.1"},
    {"issConfigCtrlEntry",        "1.3.6.1.4.1.2076.81.2.1.1"},
    {"issConfigCtrlIndex",        "1.3.6.1.4.1.2076.81.2.1.1.1"},
    {"issConfigCtrlEgressStatus",        "1.3.6.1.4.1.2076.81.2.1.1.2"},
    {"issConfigCtrlStatsCollection",        "1.3.6.1.4.1.2076.81.2.1.1.3"},
    {"issConfigCtrlStatus",        "1.3.6.1.4.1.2076.81.2.1.1.4"},
    {"issPortCtrlTable",        "1.3.6.1.4.1.2076.81.2.2"},
    {"issPortCtrlEntry",        "1.3.6.1.4.1.2076.81.2.2.1"},
    {"issPortCtrlIndex",        "1.3.6.1.4.1.2076.81.2.2.1.1"},
    {"issPortCtrlMode",        "1.3.6.1.4.1.2076.81.2.2.1.2"},
    {"issPortCtrlDuplex",        "1.3.6.1.4.1.2076.81.2.2.1.3"},
    {"issPortCtrlSpeed",        "1.3.6.1.4.1.2076.81.2.2.1.4"},
    {"issPortCtrlFlowControl",        "1.3.6.1.4.1.2076.81.2.2.1.5"},
    {"issPortCtrlRenegotiate",        "1.3.6.1.4.1.2076.81.2.2.1.6"},
    {"issPortCtrlMaxMacAddr",        "1.3.6.1.4.1.2076.81.2.2.1.7"},
    {"issPortCtrlMaxMacAction",        "1.3.6.1.4.1.2076.81.2.2.1.8"},
    {"issPortHOLBlockPrevention",        "1.3.6.1.4.1.2076.81.2.2.1.9"},
    {"issPortAutoNegAdvtCapBits",        "1.3.6.1.4.1.2076.81.2.2.1.10"},
    {"issPortCpuControlledLearning",        "1.3.6.1.4.1.2076.81.2.2.1.11"},
    {"issPortMdiOrMdixCap",        "1.3.6.1.4.1.2076.81.2.2.1.12"},
    {"issPortCtrlFlowControlMaxRate",        "1.3.6.1.4.1.2076.81.2.2.1.13"},
    {"issPortCtrlFlowControlMinRate",        "1.3.6.1.4.1.2076.81.2.2.1.14"},
    {"issMirror",        "1.3.6.1.4.1.2076.81.3"},
    {"issMirrorStatus",        "1.3.6.1.4.1.2076.81.3.1"},
    {"issMirrorToPort",        "1.3.6.1.4.1.2076.81.3.2"},
    {"issMirrorCtrlTable",        "1.3.6.1.4.1.2076.81.3.3"},
    {"issMirrorCtrlEntry",        "1.3.6.1.4.1.2076.81.3.3.1"},
    {"issMirrorCtrlIndex",        "1.3.6.1.4.1.2076.81.3.3.1.1"},
    {"issMirrorCtrlIngressMirroring",        "1.3.6.1.4.1.2076.81.3.3.1.2"},
    {"issMirrorCtrlEgressMirroring",        "1.3.6.1.4.1.2076.81.3.3.1.3"},
    {"issMirrorCtrlStatus",        "1.3.6.1.4.1.2076.81.3.3.1.4"},
    {"issMirrorCtrlRemainingSrcRcrds",        "1.3.6.1.4.1.2076.81.3.4"},
    {"issMirrorCtrlRemainingDestRcrds",        "1.3.6.1.4.1.2076.81.3.5"},
    {"issMirrorCtrlExtnTable",        "1.3.6.1.4.1.2076.81.3.6"},
    {"issMirrorCtrlExtnEntry",        "1.3.6.1.4.1.2076.81.3.6.1"},
    {"issMirrorCtrlExtnSessionIndex",        "1.3.6.1.4.1.2076.81.3.6.1.1"},
    {"issMirrorCtrlExtnMirrType",        "1.3.6.1.4.1.2076.81.3.6.1.2"},
    {"issMirrorCtrlExtnRSpanStatus",        "1.3.6.1.4.1.2076.81.3.6.1.3"},
    {"issMirrorCtrlExtnRSpanVlanId",        "1.3.6.1.4.1.2076.81.3.6.1.4"},
    {"issMirrorCtrlExtnRSpanContext",        "1.3.6.1.4.1.2076.81.3.6.1.5"},
    {"issMirrorCtrlExtnStatus",        "1.3.6.1.4.1.2076.81.3.6.1.6"},
    {"issMirrorCtrlExtnSrcTable",        "1.3.6.1.4.1.2076.81.3.7"},
    {"issMirrorCtrlExtnSrcEntry",        "1.3.6.1.4.1.2076.81.3.7.1"},
    {"issMirrorCtrlExtnSrcId",        "1.3.6.1.4.1.2076.81.3.7.1.1"},
    {"issMirrorCtrlExtnSrcCfg",        "1.3.6.1.4.1.2076.81.3.7.1.2"},
    {"issMirrorCtrlExtnSrcMode",        "1.3.6.1.4.1.2076.81.3.7.1.3"},
    {"issMirrorCtrlExtnSrcVlanTable",        "1.3.6.1.4.1.2076.81.3.8"},
    {"issMirrorCtrlExtnSrcVlanEntry",        "1.3.6.1.4.1.2076.81.3.8.1"},
    {"issMirrorCtrlExtnSrcVlanContext",        "1.3.6.1.4.1.2076.81.3.8.1.1"},
    {"issMirrorCtrlExtnSrcVlanId",        "1.3.6.1.4.1.2076.81.3.8.1.2"},
    {"issMirrorCtrlExtnSrcVlanCfg",        "1.3.6.1.4.1.2076.81.3.8.1.3"},
    {"issMirrorCtrlExtnSrcVlanMode",        "1.3.6.1.4.1.2076.81.3.8.1.4"},
    {"issMirrorCtrlExtnDestinationTable",        "1.3.6.1.4.1.2076.81.3.9"},
    {"issMirrorCtrlExtnDestinationEntry",        "1.3.6.1.4.1.2076.81.3.9.1"},
    {"issMirrorCtrlExtnDestination",        "1.3.6.1.4.1.2076.81.3.9.1.1"},
    {"issMirrorCtrlExtnDestCfg",        "1.3.6.1.4.1.2076.81.3.9.1.2"},
    {"issCpuMirrorType",        "1.3.6.1.4.1.2076.81.3.10"},
    {"issCpuMirrorToPort",        "1.3.6.1.4.1.2076.81.3.11"},
    {"issRateControl",        "1.3.6.1.4.1.2076.81.4"},
    {"issRateCtrlTable",        "1.3.6.1.4.1.2076.81.4.1"},
    {"issRateCtrlEntry",        "1.3.6.1.4.1.2076.81.4.1.1"},
    {"issRateCtrlIndex",        "1.3.6.1.4.1.2076.81.4.1.1.1"},
    {"issRateCtrlDLFLimitValue",        "1.3.6.1.4.1.2076.81.4.1.1.2"},
    {"issRateCtrlBCASTLimitValue",        "1.3.6.1.4.1.2076.81.4.1.1.3"},
    {"issRateCtrlMCASTLimitValue",        "1.3.6.1.4.1.2076.81.4.1.1.4"},
    {"issRateCtrlPortRateLimit",        "1.3.6.1.4.1.2076.81.4.1.1.5"},
    {"issRateCtrlPortBurstSize",        "1.3.6.1.4.1.2076.81.4.1.1.6"},
    {"issL2Filter",        "1.3.6.1.4.1.2076.81.5"},
    {"issL2FilterTable",        "1.3.6.1.4.1.2076.81.5.1"},
    {"issL2FilterEntry",        "1.3.6.1.4.1.2076.81.5.1.1"},
    {"issL2FilterNo",        "1.3.6.1.4.1.2076.81.5.1.1.1"},
    {"issL2FilterPriority",        "1.3.6.1.4.1.2076.81.5.1.1.2"},
    {"issL2FilterEtherType",        "1.3.6.1.4.1.2076.81.5.1.1.3"},
    {"issL2FilterProtocolType",        "1.3.6.1.4.1.2076.81.5.1.1.4"},
    {"issL2FilterDstMacAddr",        "1.3.6.1.4.1.2076.81.5.1.1.5"},
    {"issL2FilterSrcMacAddr",        "1.3.6.1.4.1.2076.81.5.1.1.6"},
    {"issL2FilterVlanId",        "1.3.6.1.4.1.2076.81.5.1.1.7"},
    {"issL2FilterInPortList",        "1.3.6.1.4.1.2076.81.5.1.1.8"},
    {"issL2FilterAction",        "1.3.6.1.4.1.2076.81.5.1.1.9"},
    {"issL2FilterMatchCount",        "1.3.6.1.4.1.2076.81.5.1.1.10"},
    {"issL2FilterStatus",        "1.3.6.1.4.1.2076.81.5.1.1.11"},
    {"issL2FilterOutPortList",        "1.3.6.1.4.1.2076.81.5.1.1.12"},
    {"issL2FilterDirection",        "1.3.6.1.4.1.2076.81.5.1.1.13"},
    {"issL3Filter",        "1.3.6.1.4.1.2076.81.6"},
    {"issL3FilterTable",        "1.3.6.1.4.1.2076.81.6.1"},
    {"issL3FilterEntry",        "1.3.6.1.4.1.2076.81.6.1.1"},
    {"issL3FilterNo",        "1.3.6.1.4.1.2076.81.6.1.1.1"},
    {"issL3FilterPriority",        "1.3.6.1.4.1.2076.81.6.1.1.2"},
    {"issL3FilterProtocol",        "1.3.6.1.4.1.2076.81.6.1.1.3"},
    {"issL3FilterMessageType",        "1.3.6.1.4.1.2076.81.6.1.1.4"},
    {"issL3FilterMessageCode",        "1.3.6.1.4.1.2076.81.6.1.1.5"},
    {"issL3FilterDstIpAddr",        "1.3.6.1.4.1.2076.81.6.1.1.6"},
    {"issL3FilterSrcIpAddr",        "1.3.6.1.4.1.2076.81.6.1.1.7"},
    {"issL3FilterDstIpAddrMask",        "1.3.6.1.4.1.2076.81.6.1.1.8"},
    {"issL3FilterSrcIpAddrMask",        "1.3.6.1.4.1.2076.81.6.1.1.9"},
    {"issL3FilterMinDstProtPort",        "1.3.6.1.4.1.2076.81.6.1.1.10"},
    {"issL3FilterMaxDstProtPort",        "1.3.6.1.4.1.2076.81.6.1.1.11"},
    {"issL3FilterMinSrcProtPort",        "1.3.6.1.4.1.2076.81.6.1.1.12"},
    {"issL3FilterMaxSrcProtPort",        "1.3.6.1.4.1.2076.81.6.1.1.13"},
    {"issL3FilterInPortList",        "1.3.6.1.4.1.2076.81.6.1.1.14"},
    {"issL3FilterOutPortList",        "1.3.6.1.4.1.2076.81.6.1.1.15"},
    {"issL3FilterAckBit",        "1.3.6.1.4.1.2076.81.6.1.1.16"},
    {"issL3FilterRstBit",        "1.3.6.1.4.1.2076.81.6.1.1.17"},
    {"issL3FilterTos",        "1.3.6.1.4.1.2076.81.6.1.1.18"},
    {"issL3FilterDscp",        "1.3.6.1.4.1.2076.81.6.1.1.19"},
    {"issL3FilterDirection",        "1.3.6.1.4.1.2076.81.6.1.1.20"},
    {"issL3FilterAction",        "1.3.6.1.4.1.2076.81.6.1.1.21"},
    {"issL3FilterMatchCount",        "1.3.6.1.4.1.2076.81.6.1.1.22"},
    {"issL3FilterStatus",        "1.3.6.1.4.1.2076.81.6.1.1.23"},
    {"issIpAuthMgr",        "1.3.6.1.4.1.2076.81.7"},
    {"issIpAuthMgrTable",        "1.3.6.1.4.1.2076.81.7.1"},
    {"issIpAuthMgrEntry",        "1.3.6.1.4.1.2076.81.7.1.1"},
    {"issIpAuthMgrIpAddr",        "1.3.6.1.4.1.2076.81.7.1.1.1"},
    {"issIpAuthMgrIpMask",        "1.3.6.1.4.1.2076.81.7.1.1.2"},
    {"issIpAuthMgrPortList",        "1.3.6.1.4.1.2076.81.7.1.1.3"},
    {"issIpAuthMgrVlanList",        "1.3.6.1.4.1.2076.81.7.1.1.4"},
    {"issIpAuthMgrOOBPort",        "1.3.6.1.4.1.2076.81.7.1.1.5"},
    {"issIpAuthMgrAllowedServices",        "1.3.6.1.4.1.2076.81.7.1.1.6"},
    {"issIpAuthMgrRowStatus",        "1.3.6.1.4.1.2076.81.7.1.1.7"},
    {"issExt",        "1.3.6.1.4.1.2076.81.8"},
    {"issL4Switching",        "1.3.6.1.4.1.2076.81.9"},
    {"issL4SwitchingFilterTable",        "1.3.6.1.4.1.2076.81.9.1"},
    {"issL4SwitchingFilterEntry",        "1.3.6.1.4.1.2076.81.9.1.1"},
    {"issL4SwitchingFilterNo",        "1.3.6.1.4.1.2076.81.9.1.1.1"},
    {"issL4SwitchingProtocol",        "1.3.6.1.4.1.2076.81.9.1.1.2"},
    {"issL4SwitchingPortNo",        "1.3.6.1.4.1.2076.81.9.1.1.3"},
    {"issL4SwitchingCopyToPort",        "1.3.6.1.4.1.2076.81.9.1.1.4"},
    {"issL4SwitchingFilterStatus",        "1.3.6.1.4.1.2076.81.9.1.1.5"},
    {"issSystemTrap",        "1.3.6.1.4.1.2076.81.10"},
    {"issMsrFailedOid",        "1.3.6.1.4.1.2076.81.10.1"},
    {"issMsrFailedValue",        "1.3.6.1.4.1.2076.81.10.2"},
    {"issAuditTrap",        "1.3.6.1.4.1.2076.81.11"},
    {"issAuditTrapEvent",        "1.3.6.1.4.1.2076.81.11.1"},
    {"issAuditTrapEventTime",        "1.3.6.1.4.1.2076.81.11.2"},
    {"issAuditTrapFileName",        "1.3.6.1.4.1.2076.81.11.3"},
    {"issModule",        "1.3.6.1.4.1.2076.81.12"},
    {"issModuleTable",        "1.3.6.1.4.1.2076.81.12.1"},
    {"issModuleEntry",        "1.3.6.1.4.1.2076.81.12.1.1"},
    {"issModuleId",        "1.3.6.1.4.1.2076.81.12.1.1.1"},
    {"issModuleSystemControl",        "1.3.6.1.4.1.2076.81.12.1.1.2"},
    {"issSwitchFan",        "1.3.6.1.4.1.2076.81.13"},
    {"issSwitchFanTable",        "1.3.6.1.4.1.2076.81.13.1"},
    {"issSwitchFanEntry",        "1.3.6.1.4.1.2076.81.13.1.1"},
    {"issSwitchFanIndex",        "1.3.6.1.4.1.2076.81.13.1.1.1"},
    {"issSwitchFanStatus",        "1.3.6.1.4.1.2076.81.13.1.1.2"},
    {"issAclNp",        "1.3.6.1.4.1.2076.81.14"},
    {"issAclNpCommitSupport",        "1.3.6.1.4.1.2076.81.14.1"},
    {"issAclNpCommitAction",        "1.3.6.1.4.1.2076.81.14.2"},
    {"issAclTrafficControl",        "1.3.6.1.4.1.2076.81.15"},
    {"issAclTrafficSeperationCtrl",        "1.3.6.1.4.1.2076.81.15.1"},
    {"issLogTrap",        "1.3.6.1.4.1.2076.81.16"},
    {"issLogTrapEvent",        "1.3.6.1.4.1.2076.81.16.1"},
    {"issLogTrapEventTime",        "1.3.6.1.4.1.2076.81.16.2"},
    {"issLogTrapFileName",        "1.3.6.1.4.1.2076.81.16.3"},
    {"issHealthCheckGroup",        "1.3.6.1.4.1.2076.81.17"},
    {"issHealthChkStatus",        "1.3.6.1.4.1.2076.81.17.1"},
    {"issHealthChkErrorReason",        "1.3.6.1.4.1.2076.81.17.2"},
    {"issHealthChkMemAllocErrPoolId",        "1.3.6.1.4.1.2076.81.17.3"},
    {"issHealthChkConfigRestoreStatus",        "1.3.6.1.4.1.2076.81.17.4"},
    {"issHealthChkClearCtr",        "1.3.6.1.4.1.2076.81.17.5"},
    {"fsDot1dBridge",        "1.3.6.1.4.1.2076.116"},
    {"fsDot1dStp",        "1.3.6.1.4.1.2076.116.2"},
    {"fsDot1dTp",        "1.3.6.1.4.1.2076.116.4"},
    {"security",        "1.3.6.1.5"},
    {"snmpV2",        "1.3.6.1.6"},
    {"snmpDomains",        "1.3.6.1.6.1"},
    {"snmpProxys",        "1.3.6.1.6.2"},
    {"snmpModules",        "1.3.6.1.6.3"},
    {"snmpAuthProtocols",        "1.3.6.1.6.3.10.1.1"},
    {"snmpPrivProtocols",        "1.3.6.1.6.3.10.1.2"},
    {"ieee802dot1mibs",        "1.111.2.802.1"},
    {"joint-iso-ccitt",        "2"},
    {0,0}
};

#endif /* _SNMP_MIB_H */
