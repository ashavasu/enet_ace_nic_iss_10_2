/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: issmacro.h,v 1.48 2016/10/07 13:26:20 siva Exp $
 *
 * Description           : This file contains all macros used in ISS module.
 *****************************************************************************/

/*****************************************************************************/
/*    FILE  NAME            : issmacro.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISSTeam                                        */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#ifndef _ISSMACRO_H
#define _ISSMACRO_H

#define ISS_PROTOCOL_SEM                ((const UINT1 *)"ISEM") 
/* Trace and debug flags */
#define   ISS_TRC_FLAG               gu4IssDebugFlags
/* Module name */
#define   ISS_MOD_NAME              ((const char *)"ISS  ")
#define   ISS_TRC_CONTEXT(x)  x

#define ISS_MEMORY_TYPE                 MEM_DEFAULT_MEMORY_TYPE

#define ISS_CONFIG_USER_NAME_LEN        20
#define ISS_CONFIG_PASSWD_LEN           20
#define ISS_MAX_IPADDR_LEN              32
#define ISS_AUTO_NEG_BITS_SIZE           4
#define ISS_MAX_NUM_BYTES               8

#define ISS_CFGCTRL_MASK                0x01
#define ISS_MRRCTRL_MASK                0x02
#define ISS_PORTCTRL_MASK               0x04

#define ISS_ENET_PORT                   CFA_ENET

/* Macros for validattion */
#define DEF_FTP_FILE_NAME               ((const char *)"IssCfg.txt")
#define ISS_ETHERNET_ADDR_SIZE          6
#define ISS_MIN_PORTCTRL_MACADDR        0
#define ISS_MAX_PORTCTRL_MACADDR        2147483647
#define ISS_MIN_L4SFILTER_ID            0
#define ISS_MAX_L4SFILTER_ID            20
#define ISS_TMP_CONF_FILE               "iss_tmp.conf"
#define ISS_BKP_CONF_FILE               "iss_bkp.conf"
#define ISS_CONF_FILE                   ".conf"

#define ISS_SYS_DEF_CONTACT  "Aricent Ltd, India"
#define ISS_SYS_DEF_LOCATION  "Aricent Ltd, India"
#define ISS_SYS_DEF_NAME      "Aricent Linux Router Ver 1.0"

#define ISS_IS_L4SFILTER_ID_VALID(ISSId) \
        (((ISSId <= ISS_MIN_L4SFILTER_ID) || \
        (ISSId > ISS_MAX_L4SFILTER_ID)) ? ISS_FALSE : ISS_TRUE)

/* Macros for List Operations */
#define ISS_SLL_INIT(pList)             TMO_SLL_Init(pList)
#define ISS_SLL_INIT_NODE(pNode)        TMO_SLL_Init_Node(pNode)
#define ISS_SLL_ADD(pList, pNode)       TMO_SLL_Add(pList, pNode)
#define ISS_SLL_INSERT(pList, pPrev, pNode) TMO_SLL_Insert (pList, pPrev, pNode)
#define ISS_SLL_DELETE(pList, pNode)    TMO_SLL_Delete (pList, pNode)
#define ISS_SLL_COUNT(pList)            TMO_SLL_Count(pList)
#define ISS_SLL_NEXT(pList, pNode)      TMO_SLL_Next(pList,pNode)
#define ISS_SLL_FIRST(pList)            TMO_SLL_First(pList)
#define ISS_SLL_GET(pList)              TMO_SLL_Get(pList)
#define ISS_SLL_LAST(pList)             TMO_SLL_Last(pList)
#define ISS_SLL_IS_NODE_IN_LIST(pNode)  TMO_SLL_Is_Node_In_List(pNode)

#define ISS_SLL_SCAN(pList,pNode,TypeCast)  \
                                        TMO_SLL_Scan(pList,pNode,TypeCast)

/* Memory Block Count */

#define GET_MIRR_SESSION_INFO(SessionNum) \
   (&(gIssGlobalInfo.SessionInfo[SessionNum - 1]))

#define GET_MIRR_SESSION_DEST_HDR(SessionNum) \
    (gIssGlobalInfo.SessionInfo[SessionNum - 1].DestRecrdHeader)

#define GET_MIRR_SESSION_SRC_HDR(SessionNum) \
    (gIssGlobalInfo.SessionInfo[SessionNum - 1].SrcRecrdHeader)

#define ISS_L3FILTERENTRY_MEMBLK_SIZE   sizeof(tIssL3FilterEntry)
/* According to FirmwareImage size need to update this MEMBLK size */
#define ISS_FIRMWARE_MEMBLK_SIZE        255

/* MEM Pool Id Definitions */
#define ISS_CONFIGENTRY_POOL_ID        gIssGlobalInfo.IssConfigCtrlPoolId
#define ISS_PORTENTRY_POOL_ID          gIssGlobalInfo.IssPortCtrlPoolId
#define ISS_MIRR_DEST_RECORD_POOL_ID   gIssGlobalInfo.IssMirrDestRecordPoolId
#define ISS_MIRR_SRC_RECORD_POOL_ID    gIssGlobalInfo.IssMirrSrcRecordPoolId
#define ISS_L4SFILTERENTRY_POOL_ID     gIssGlobalInfo.IssL4SFilterPoolId
#define ISS_FIRMWARE_POOL_ID           gIssFirmwarePoolId
#define ISS_ENT_PHY_POOL_ID            gIssGlobalInfo.IssEntPhyPoolId

/* Macros for Memory Block Allocation from Memory Pool */

#define  ISS_CONFIGENTRY_ALLOC_MEM_BLOCK(pu1Msg)\
         (pu1Msg = (tIssConfigCtrlEntry *)\
          MemAllocMemBlk(ISS_CONFIGENTRY_POOL_ID))

#define  ISS_PORTENTRY_ALLOC_MEM_BLOCK(pu1Msg)\
         (pu1Msg = (tIssPortCtrlEntry *)MemAllocMemBlk\
          (ISS_PORTENTRY_POOL_ID))

#define  ISS_MIRR_DEST_RECORD_ALLOC_MEM_BLOCK(pu1Msg)\
         (pu1Msg = (tIssMirrDestRecordInfo *)MemAllocMemBlk\
          (ISS_MIRR_DEST_RECORD_POOL_ID))

#define  ISS_MIRR_SRC_RECORD_ALLOC_MEM_BLOCK(pu1Msg)\
         (pu1Msg = (tIssMirrSrcRecordInfo *)MemAllocMemBlk\
          (ISS_MIRR_SRC_RECORD_POOL_ID))

#define  ISS_GET_MIRR_FREE_DEST_RECORDS \
             MemGetFreeUnits(ISS_MIRR_DEST_RECORD_POOL_ID)

#define  ISS_GET_MIRR_FREE_SRC_RECORDS \
             MemGetFreeUnits(ISS_MIRR_SRC_RECORD_POOL_ID)

#define  ISS_L4SFILTERENTRY_ALLOC_MEM_BLOCK(pu1Msg)\
         (pu1Msg = (tIssL4SFilterEntry *)\
          MemAllocMemBlk(ISS_L4SFILTERENTRY_POOL_ID))

#define  ISS_FIRMWARE_ALLOC_MEM_BLOCK(pu1Msg)\
         (pu1Msg = (UINT1 *)\
          MemAllocMemBlk(ISS_FIRMWARE_POOL_ID))

#define  ISS_ENTPHY_ALLOC_MEM_BLOCK(pu1Msg)\
         (pu1Msg = (tIssEntPhyInfo *)\
          MemAllocMemBlk(ISS_ENT_PHY_POOL_ID))

/* Macros for Freeing Memory Blocks from Memory Pools */

#define  ISS_CONFIGENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_CONFIGENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define  ISS_PORTENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_PORTENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define  ISS_MIRR_RECORD_FREE_DEST_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_MIRR_DEST_RECORD_POOL_ID,(UINT1 *)pu1Msg)

#define  ISS_MIRR_RECORD_FREE_SRC_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_MIRR_SRC_RECORD_POOL_ID,(UINT1 *)pu1Msg)

#define  ISS_L4SFILTERENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_L4SFILTERENTRY_POOL_ID, (UINT1 *)pu1Msg)

#define  ISS_FIRMWARE_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_FIRMWARE_POOL_ID, (UINT1 *)pu1Msg)

#define  ISS_ENTPHY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(ISS_ENT_PHY_POOL_ID, (UINT1 *)pu1Msg)

/* Macros to get the status of the 0th entry */

#define  ISS_GET_CONFIGCTRL_DENTRY_STATUS \
  gIssGlobalInfo.apIssConfigCtrlEntry[ISS_ZERO_ENTRY]->IssConfigCtrlStatus

#define  ISS_GET_RATECTRL_DENTRY_STATUS \
  gIssGlobalInfo.apIssRateCtrlEntry[ISS_ZERO_ENTRY]->IssRateCtrlStatus

#define   ISS_CFA_GET_PORT_INFO(u2PortNum, pCfaIfInfo) \
          CfaGetIfInfo(u2PortNum, pCfaIfInfo)

/* Timer Related Definitions */
#define   ISS_SYS_TIMER_EXP_EVENT        0x01
#define   ISS_ACL_MSG_EVENT          0x02
/* Event identifier for redundancy message */
#define   ISS_RED_MSG_EVENT          0x03
#define   ISS_MAC_LEARN_RATE_LIMIT_EXP_EVENT        0x04
#define   ISS_PEER_INFO_EVENT        0x08

/* Task related macros, OSIX and Timer related macros */
#define   ISS_SYS_GET_TASK_ID         OsixTskIdSelf
#define   ISS_SYS_RECEIVE_EVENT       OsixEvtRecv
#define   ISS_SYS_TASK_ID             gIssGlobalInfo.IssSystMonitorTaskId
#define   ISS_SYS_TMR_LIST_ID         gIssGlobalInfo.IssTmrListId

/* Max number of Events for which callbacks are refered from application */
typedef enum {
    ISS_EVT_SYS_INIT,
    ISS_EVT_SYS_DEINIT,
    ISS_EVT_CUST_FIRM_UPG,
    WSS_EVT_READ_NVRAM,
    ISS_EVT_HOSTNME_UPDT,
    ISS_MAX_CALLBACK_EVENTS
}tIssCallBackEvents;

#define ISS_CALLBACK_FN(u4Event)  gIssCallback[u4Event]

#define   ISS_MEMCPY              MEMCPY
#define   ISS_STRCPY              STRCPY
#define   ISS_STRNCPY             STRNCPY
#define   ISS_STRCMP              STRCMP
#define   ISS_MEMCMP              MEMCMP
#define   ISS_MEMSET              MEMSET
#define   ISS_STRLEN              STRLEN
#define   ISS_SPRINTF             SPRINTF

#define ISS_ADD_PORT_LIST(au1List1, au1List2) \
              {\
                 UINT2 u2ByteIndex;\
                 \
                 for (u2ByteIndex = 0;\
                      u2ByteIndex < ISS_PORT_LIST_SIZE;\
                      u2ByteIndex++) {\
                    au1List1[u2ByteIndex] |= au1List2[u2ByteIndex];\
                 }\
              }

#define ISS_ADD_PORT_CHANNEL_LIST(au1List1, au1List2) \
              {\
                 UINT2 u2ByteIndex;\
                 \
                 for (u2ByteIndex = 0;\
                      u2ByteIndex < ISS_PORT_CHANNEL_LIST_SIZE;\
                      u2ByteIndex++) {\
                    au1List1[u2ByteIndex] |= au1List2[u2ByteIndex];\
                 }\
              }

#define ISS_AND_PORT_LIST(au1List1, au1List2) \
              {\
                 UINT2 u2ByteIndex;\
                 \
                 for (u2ByteIndex = 0;\
                      u2ByteIndex < ISS_PORT_LIST_SIZE;\
                      u2ByteIndex++) {\
                    au1List1[u2ByteIndex] &= au1List2[u2ByteIndex];\
                 }\
              }

#ifdef  TRACE_WANTED
#ifdef __GNUC__
/* Function Entry/Exit Trace for CC Task*/
#define  ISS_TRC_FN_ENTRY()  \
        if ((ISS_TRC_FLAG & ISS_TRC_FUNC_ENTRY_EXIT) || (ISS_TRC_FLAG & ISS_TRC_ALL)) \
        {\
         printf ("ISS  : %s Entered %s\n", __FILE__, __FUNCTION__);\
        }

#define  ISS_TRC_FN_EXIT()   \
        if ((ISS_TRC_FLAG & ISS_TRC_FUNC_ENTRY_EXIT) || (ISS_TRC_FLAG & ISS_TRC_ALL)) \
        {\
         printf ("ISS  : %s Exiting %s\n", __FILE__, __FUNCTION__);\
        }
#else
/* Function Entry/Exit Trace for CC Task*/
#define  ISS_TRC_FN_ENTRY()  \
        if ((ISS_TRC_FLAG & ISS_TRC_FUNC_ENTRY_EXIT) || (ISS_TRC_FLAG & ISS_TRC_ALL)) \
        {\
         printf ("ISS  : %s Entered %s at line %d\n", __FILE__,  __LINE__);\
        }

#define  ISS_TRC_FN_EXIT()   \
        if ((ISS_TRC_FLAG & ISS_TRC_FUNC_ENTRY_EXIT) || (ISS_TRC_FLAG & ISS_TRC_ALL)) \
        {\
         printf ("ISS  : %s Exiting %s at %d\n", __FILE__,  __LINE__);\
        }
#endif  /* __GNUC__ */

#define ISS_TRC(TraceType, Str)                                              \
        MOD_TRC(ISS_TRC_FLAG, TraceType, ISS_MOD_NAME, ISS_TRC_CONTEXT(Str))
   
#define ISS_TRC_ARG1(TraceType, Str, Arg1)                                   \
        MOD_TRC_ARG1(ISS_TRC_FLAG, TraceType, ISS_MOD_NAME,\
        ISS_TRC_CONTEXT(Str),Arg1)

#define ISS_TRC_ARG2(TraceType, Str, Arg1, Arg2)                             \
        MOD_TRC_ARG2(ISS_TRC_FLAG, TraceType, ISS_MOD_NAME, \
        ISS_TRC_CONTEXT(Str),Arg1, Arg2)

#define ISS_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)                       \
        MOD_TRC_ARG3(ISS_TRC_FLAG, TraceType, ISS_MOD_NAME,\
        ISS_TRC_CONTEXT(Str), Arg1, Arg2, Arg3)

#define ISS_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3, Arg4)                 \
        MOD_TRC_ARG4(ISS_TRC_FLAG, TraceType, ISS_MOD_NAME,\
        ISS_TRC_CONTEXT(Str), Arg1, Arg2, Arg3, Arg4)

#define ISS_TRC_ARG5(TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5)           \
        MOD_TRC_ARG5(ISS_TRC_FLAG, TraceType, ISS_MOD_NAME,\
        ISS_TRC_CONTEXT(Str), Arg1, Arg2, Arg3, Arg4, Arg5)

#define ISS_TRC_ARG6(TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)     \
        MOD_TRC_ARG6(ISS_TRC_FLAG, TraceType, ISS_MOD_NAME,\
        ISS_TRC_CONTEXT(Str), Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)

#define ISS_TRC_ARG7(TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)  \
        MOD_TRC_ARG7(ISS_TRC_FLAG, TraceType, ISS_MOD_NAME,\
        ISS_TRC_CONTEXT(Str), Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)

#else

#define  ISS_TRC_FN_ENTRY()
#define  ISS_TRC_FN_EXIT()
#define ISS_TRC(TraceType, Str)
#define ISS_TRC_ARG1(TraceType, Str, Arg1)
#define ISS_TRC_ARG2(TraceType, Str, Arg1, Arg2)
#define ISS_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)
#define ISS_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3, Arg4)
#define ISS_TRC_ARG5(TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5)
#define ISS_TRC_ARG6(TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)
#define ISS_TRC_ARG7(TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)


#endif  /* TRACE_WANTED*/

/* Constants for reflecting the status of the TFTP 
 * download/Remote Save Operation */
#define  ISS_TFTP_IDLE                        1
#define  ISS_TFTP_READ_IN_PROGRESS            2
#define  ISS_TFTP_WRITE_IN_PROGRESS           3
#define  ISS_TFTP_REQUEST_FAILED              4
#define  ISS_TFTP_TIMEOUT_ERROR               5
#define  ISS_TFTP_DOWNLOAD_FAILED             7
#define ISS_IS_MAC_MULTICAST(pMacAddr)\
            (pMacAddr[0] & 0x01)

#define ISS_NTOHL                        OSIX_NTOHL
#define ISS_HTONL                        OSIX_HTONL
#define ISS_HTONS                        OSIX_HTONS
#define ISS_ATOI                         ATOI
#define ISS_BZERO(ptr,len)               MEMSET(ptr,0,len)

#define ISS_CONVERT_IPADDR_TO_STR(pString, u4Value)\
{\
    struct in_addr      IpAddr;\
\
         IpAddr.s_addr = ISS_NTOHL (u4Value);\
\
         strcpy ((CHR1 *)pString,((CHR1 *) inet_ntoa (IpAddr)));\
\
}

#define ISS_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)
#define ISS_MIN_HTTP_PORT   0
#define ISS_MAX_HTTP_PORT   65535

#define ISS_TFTP_TRANSFER_MODE   1
#define ISS_SFTP_TRANSFER_MODE   2
/* Macros for MAC Learn Limit Rate Feature */
#define ISS_SWITCH_MAC_MAX_LEARN_LIMIT\
             gIssSwitchMacLearnLimitRate.i4IssSwitchMacMaxLearnLimit
#define ISS_SWITCH_MAC_LEARN_LIMIT_DURATION\
             gIssSwitchMacLearnLimitRate.u4IssSwitchMacLearnRateLimitInterval
#define ISS_SWITCH_MAC_LEARN_LIMIT_RATE\
             gIssSwitchMacLearnLimitRate.i4IssSwitchMacLearnLimitRate
#define ISS_MAC_LEARN_LIMIT_TMRID   gIssGlobalInfo.IssMacLearnRateTmrListId
#define ISS_DEF_SWITCH_MAC_LEARN_LIMIT_RATE        1000  
#define ISS_DEF_SWITCH_MAC_LEARN_LIMIT_INTERVAL    1000  /* one second */
#define ISS_MAX_LEARN_RATE_LIMIT_VAL               2147483647
#define ISS_MAX_LEARN_RATE_LIMIT_IN                100000
#define ISS_MAC_LEARN_LIMIT_DUR_SEC\
    (ISS_SWITCH_MAC_LEARN_LIMIT_DURATION/1000)
#define ISS_DEF_UNICAST_LEARNING_LIMIT \
            (VLAN_DEV_MAX_L2_TABLE_SIZE - VLAN_DEV_MAX_ST_UCAST_ENTRIES)

#define ISS_PORT_ISOLATION_ALLOWED(u1IfType) \
           ((u1IfType == CFA_PSEUDO_WIRE) ? ISS_TRUE : ISS_FALSE)

/* Rate limiting Macros */
/* Max rate limit value in kbps */
#define ISS_PORT_MAX_RATE_LIMIT  80000000
#endif /* _ISSMACRO_H */
