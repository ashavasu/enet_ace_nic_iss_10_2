/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* $Id: issglob.h,v 1.38 2017/12/28 10:40:13 siva Exp $ */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : issglob.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*    DESCRIPTION           : This file has declaration for Iss globals.     */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
#ifndef _ISSGLOB_H        
#define _ISSGLOB_H        
#ifdef _ISSSYS_C
tIssGlobalInfo                gIssGlobalInfo;
tIssSysGroupInfo              gIssSysGroupInfo;
tIssSysCapSupport             gIssSysCapSupport; /* To check whether system Capabilities Supported */
tIssDlImageInfo               gIssDlImageInfo;
tIsslogFileUlInfo             gIssUlLogFileInfo;
tConfigSaveInfo               gIssConfigSaveInfo;
tConfigRestoreInfo            gIssConfigRestoreInfo;
tIssRestart                   gIssRestart;
tIssRestart                   gIssStandbyRestart;
tIssMirrorStatus              gIssMirrorStatus;
tIssHwCapabilities            gIssHwCapabilities;
tIssuStartupInfo              gIssuStartupInfo;
UINT2                         gu2IssMirrorToPort;
tIssSwitchMacLearnLimitRate   gIssSwitchMacLearnLimitRate;
UINT4                         gu4IssDebugFlags;
UINT1                         gu1IssDlImageFlag;
UINT1                         gu1IssUlLogFlag;
UINT1                         gu1IssLoginAuthMode;
UINT1                         gu1IssPamDefaultPrivilege;
UINT1                         gu1IsIssInitCompleted = ISS_FALSE;
UINT1 gu1FirmwareImageName[ISS_STATIC_MAX_NAME_LEN];
INT4         gi4IssSwitchModeType = ISS_STORE_FORWARD;

INT4                          gi4DefaultExecTimeOut = CLI_SESSION_TIMEOUT; 
INT4                          gi4AuditStatus = ISS_DISABLE;
INT4                          gi4AuditLogReset = ISS_FALSE;
INT4                          gi4AuditInitiateTransfer = ISS_FALSE;
tIPvXAddr                     gAuditLogRemoteIpAddr ;
UINT4                         gu4AuditLogSize = AUDIT_MAX_FILE_SIZE;
UINT4                         gu4AuditLogSizeThreshold = 
                                        AUDIT_DEFAULT_THRESHOLD_VALUE;
UINT1                         gu1LoggingCmd = 0;
UINT1                         gau1IssAuditLogFileName[ISS_AUDIT_FILE_NAME_LENGTH];
UINT1                         gau1IssAuditTransferFileName[ISS_CONFIG_FILE_NAME_LEN];
UINT1                         gau1IssSyslog1FileName[ISS_CONFIG_FILE_NAME_LEN];
UINT1                         gau1IssSyslog2FileName[ISS_CONFIG_FILE_NAME_LEN];
UINT1                         gau1IssSyslog3FileName[ISS_CONFIG_FILE_NAME_LEN];
INT4                          gi4ClearConfig = ISS_FALSE;
INT4                          gi4ClrConfigInProgress = ISS_FALSE;
INT4                          gi4ClearConfigSupport = 0;
UINT1                         gau1IssClearConfigFileName[ISS_CLR_CONFIG_FILE_NAME_LEN + 1];
#ifdef ISS_TEST_WANTED
INT4                          gi4CentralizedIPSupport = ISS_FALSE;
#endif
/* table for mapping CIDR numbers to IP subnet masks - from RFC 1878*/
UINT4               gu4IssCidrSubnetMask[ISS_MAX_CIDR + 1] = {
    0x00000000, 0x80000000, 0xC0000000, 0xE0000000, 0xF0000000, 0xF8000000,
    0xFC000000, 0xFE000000, 0xFF000000, 0xFF800000, 0xFFC00000, 0xFFE00000,
    0xFFF00000, 0xFFF80000, 0xFFFC0000, 0xFFFE0000, 0xFFFF0000, 0xFFFF8000,
    0xFFFFC000, 0xFFFFE000, 0xFFFFF000, 0xFFFFF800, 0xFFFFFC00, 0xFFFFFE00,
    0xFFFFFF00, 0xFFFFFF80, 0xFFFFFFC0, 0xFFFFFFE0, 0xFFFFFFF0, 0xFFFFFFF8,
    0xFFFFFFFC, 0xFFFFFFFE, 0xFFFFFFFF
};

tIssCallbackEntry             gIssCallback[ISS_MAX_CALLBACK_EVENTS]; 

INT1 gi1SnmpEngineID[SNMP_MAX_STR_ENGINEID_LEN + 1] = "80.00.08.1c.04.46.53";
UINT4 gu4SnmpEngineBoots = 0;
INT4                          gi4IssCliTlntSrvrPrimaryBindPort = CLI_TELNET_SERVER_PORT;
INT4                          gi4IssCliTlntSrvrAlternateBindPort = CLI_TELNET_SERVER_PORT1;
UINT4                         gu4IssCliTlntSrvrBindAddr = INADDR_ANY;
INT4                          gi4WebSessions = ISS_MAX_WEB_SESSIONS;
INT4                          gi4WebSessionTimeOut = DEFAULT_WEB_SESSION_TIME_OUT;
INT4                          gi4TelnetClientStatus = ISS_TELNET_CLIENT_ENABLE;
INT4                          gi4SshClientStatus = ISS_SSH_CLIENT_ENABLE;
INT4                          gi4ActiveTelnetClientSessions = 0;
INT4                          gi4ActiveSshClientSessions = 0;
UINT1               gau1IssLogFileName[LOG_FILE_NAME_LENGTH];
UINT4               gu4LogSizeThreshold = LOG_DEFAULT_THRESHOLD_VALUE;
UINT4               gu4LogSize = LOG_MAX_FILE_SIZE;
UINT4           gu4CurrLogSize = 0;
INT4                          gi4LogReset = ISS_FALSE;
UINT1           gu1FileCopyingStatus = SFTP_PROCESS_COMPLETE;
#else
extern tIssGlobalInfo         gIssGlobalInfo;
extern tIssSysCapSupport      gIssSysCapSupport; /* To check whether system Capabilities Supported */
extern tIssDlImageInfo        gIssDlImageInfo;
extern tIsslogFileUlInfo      gIssUlLogFileInfo;
extern tConfigSaveInfo        gIssConfigSaveInfo;
extern tConfigRestoreInfo     gIssConfigRestoreInfo;
extern tIssRestart            gIssRestart;
extern tIssRestart            gIssStandbyRestart;
extern tIssMirrorStatus       gIssMirrorStatus;
extern UINT2                  gu2IssMirrorToPort;
extern tIssSwitchMacLearnLimitRate   gIssSwitchMacLearnLimitRate;
extern UINT4                  gu4IssDebugFlags;
extern UINT1                  gu1IssDlImageFlag;
extern UINT1                  gu1IssUlLogFlag;
extern UINT1                  gu1IssLoginAuthMode;
extern UINT1                  gu1IssPamDefaultPrivilege;
extern UINT4                  gu4IssCidrSubnetMask[ISS_MAX_CIDR + 1];
extern UINT1                  gu1IsIssInitCompleted;
extern INT4                   gi4DefaultExecTimeOut;
extern INT4                   gi4IssSwitchModeType;
extern tIssCallbackEntry      gIssCallback[ISS_MAX_CALLBACK_EVENTS]; 
extern INT1                   gi1SnmpEngineID[SNMP_MAX_STR_ENGINEID_LEN + 1];
extern UINT4                  gu4SnmpEngineBoots;
extern UINT1 gu1FirmwareImageName[ISS_STATIC_MAX_NAME_LEN];
extern INT4                   gi4IssCliTlntSrvrPrimaryBindPort;
extern INT4                   gi4IssCliTlntSrvrAlternateBindPort;
extern UINT4                  gu4IssCliTlntSrvrBindAddr;
extern INT4                   gi4WebSessionTimeOut;
extern INT4                   gi4WebSessions;
extern INT4                   gi4TelnetClientStatus;
extern INT4                   gi4SshClientStatus;
extern INT4                   gi4ActiveTelnetClientSessions;
extern INT4                   gi4ActiveSshClientSessions;
extern UINT1                 gu1FileCopyingStatus;
#endif /* _ISSSYS_C */

#endif /* _ISSGLOB_H */
