/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/*                                                                           */
/* $Id: issdefs.h,v 1.20 2015/06/03 02:58:04 siva Exp $                    */
/*                                                                           */
/* Licensee Aricent Inc., 2001-2002                                          */
/*****************************************************************************/
/*    FILE  NAME            : issdefs.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISSTeam                                        */
/*    DESCRIPTION           : This file contains all macros used in          */
/*                            ISS Module.                                    */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#ifndef _ISSDEFS_H
#define _ISSDEFS_H

#define MAX_USRN_LEN                         256
#define MAX_PASSW_LEN                        256
#define MAX_OPTION_STRING                    50
#define MAX_OPTION_VALUE                     50
#define ISS_SPL_CHAR_LIST             "!@#$%^&*()>`~_- +=|\\{}[]:;\"\'<,?"

#define WEB_SESSION_MIN_TIME_OUT             1
#define WEB_SESSION_MAX_TIME_OUT      DEFAULT_WEB_SESSION_TIME_OUT
#define WEB_SESSION_MIN_USERS                1
#define DEFAULT_WEB_SESSION_TIME_OUT         300
#define ISS_MAX_LOG_STR_LEN             200
#define ISS_SYS_OBJ_NAME_LEN            257
#define ISS_TRAP_TYPE_MAX_STR_LEN       32
#define ISS_ROW_STATUS_ACTIVE              ACTIVE
#define ISS_ROW_STATUS_NOT_IN_SERVICE      NOT_IN_SERVICE
#define ISS_ROW_STATUS_NOT_READY           NOT_READY
#define ISS_ROW_STATUS_CREATE_AND_GO       CREATE_AND_GO
#define ISS_ROW_STATUS_CREATE_AND_WAIT     CREATE_AND_WAIT
#define ISS_ROW_STATUS_DESTROY             DESTROY

#define ISS_MIRR_DEFAULT_SESSION           1
#define ISS_DEFAULT_CONTEXT                L2IWF_DEFAULT_CONTEXT

/*CLI*/
#define ISS_AUDIT_FILE_ACTIVE "/tmp/iss_output_file_active"
#define ISS_AUDIT_FILE_STDBY "/tmp/iss_output_file_stdby"
#define ISS_AUDIT_SHOW_CMD    "show port-isolation > "
#define ISS_CLI_EOF                  2
#define ISS_CLI_NO_EOF                 1
#define ISS_CLI_RDONLY               OSIX_FILE_RO
#define ISS_CLI_WRONLY               OSIX_FILE_WO
#define ISS_CLI_MAX_GROUPS_LINE_LEN  200

/* Trace String */
#define ISS_TRC_INIT_SHUT            0x80000000
#define ISS_TRC_ALL_FAIL             0x50000000
#define ISS_TRC_FUNC_ENTRY_EXIT      0x20000000
#define ISS_TRC_ALL                  0x10000000
#define ISS_MAX_TRC_COUNT            8 
#define ISS_TRC_MIN_SIZE             1
#define ISS_INVALID_TRC              0
#define L2_ACL_TRAP         1
#define L3_ACL_TRAP         2

#ifdef L2RED_WANTED
#define ISS_GET_RMNODE_STATUS()  RmGetNodeState() 
#else
#define ISS_GET_RMNODE_STATUS()  RM_ACTIVE
#endif

/* 10 of seconds polling is enabled for checking system features */
#define ISS_SYSTEM_POLL_DURATION        10

#define ISS_SYS_TRAP_OID_LEN            12
#define ISS_SYS_TASK_NAME              (const UINT1 *) "SMGT"

enum {
    ISS_SYS_TRAP_RAM_THRESHOLD = 1,
    ISS_SYS_TRAP_FAN_STATUS = 2,
    ISS_SYS_TRAP_CPU_THRESHOLD = 3,
    ISS_SYS_TRAP_TEMPERATURE_THRES = 4,
    ISS_SYS_TRAP_POWER_STATUS = 5,
    ISS_SYS_TRAP_FLASH_THRESHOLD = 6 
};
#define ISS_DEFAULT_DESCR       "Network Element"
#define ISS_DEFAULT_VENDOR_TYPE  "Aricent"
#define ISS_DEFAULT_SW_REV_NUM   "6.2.0"
#define ISS_DEFAULT_MFG_NAME     "Aricent"
#define ISS_DEFAULT_ALIAS_NAME   "DummyName"
#define ISS_DEFAULT_ASSET_ID     "DummyId"
/* Mfg Date : 2009-8-6,13:30:30.1*/
#define ISS_DEFAULT_MFG_DATE     "\x07\xD9\x08\x06\x0D\x1E\x1E\x01"

#define ISS_ENT_PHY_SERIAL_NUM   1
#define ISS_ENT_PHY_ASSET_ID     2
#define ISS_ENT_PHY_URIS         3
#define ISS_ENT_PHY_ALIAS_NAME   4

/* Timer Speed Values */
#define ISS_SYS_DEF_TMR_SPEED    1     /* Minimum/Default Timer Speed Value */
#define ISS_SYS_MAX_TMR_SPEED    1000  /* Maximum Timer Speed Value */

/*Default port type*/
#define ISS_DEFAULT_AUTO_MDI_OR_MDIX_CAP ISS_AUTO_MDIX

#define ISS_EOF 2
#endif /* _ISSDEFS_H */
