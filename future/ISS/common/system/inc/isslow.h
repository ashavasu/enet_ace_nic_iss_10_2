/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/*              */
/* $Id: isslow.h,v 1.58 2017/12/20 11:06:48 siva Exp $       */
/*              */
/* Licensee Aricent Inc., 2001-2002                                          */
/*****************************************************************************/
/*    FILE  NAME            : isslow.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Tool Generated                                 */
/*    DESCRIPTION           : This file has protype for low level snmp code  */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#ifndef _ISSLOW_H
#define _ISSLOW_H

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssSwitchName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssHardwareVersion ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssFirmwareVersion ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssDefaultIpAddrCfgMode ARG_LIST((INT4 *));

INT1
nmhGetIssDefaultIpAddr ARG_LIST((UINT4 *));

INT1
nmhGetIssDefaultIpSubnetMask ARG_LIST((UINT4 *));

INT1
nmhGetIssEffectiveIpAddr ARG_LIST((UINT4 *));

INT1
nmhGetIssDefaultInterface ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssRestart ARG_LIST((INT4 *));

INT1
nmhGetIssConfigSaveOption ARG_LIST((INT4 *));

INT1
nmhGetIssConfigSaveIpAddr ARG_LIST((UINT4 *));

INT1
nmhGetIssConfigSaveFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssInitiateConfigSave ARG_LIST((INT4 *));

INT1
nmhGetIssConfigSaveStatus ARG_LIST((INT4 *));

INT1
nmhGetIssConfigRestoreOption ARG_LIST((INT4 *));

INT1
nmhGetIssConfigRestoreIpAddr ARG_LIST((UINT4 *));

INT1
nmhGetIssConfigRestoreFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssInitiateConfigRestore ARG_LIST((INT4 *));

INT1
nmhGetIssConfigRestoreStatus ARG_LIST((INT4 *));

INT1
nmhGetIssDlImageFromIp ARG_LIST((UINT4 *));

INT1
nmhGetIssDlImageName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssInitiateDlImage ARG_LIST((INT4 *));

INT1
nmhGetIssLoggingOption ARG_LIST((INT4 *));

INT1
nmhGetIssUploadLogFileToIp ARG_LIST((UINT4 *));

INT1
nmhGetIssLogFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssInitiateUlLogFile ARG_LIST((INT4 *));

INT1
nmhGetIssRemoteSaveStatus ARG_LIST((INT4 *));

INT1
nmhGetIssDownloadStatus ARG_LIST((INT4 *));

INT1
nmhGetIssSysContact ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssSysLocation ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssLoginAuthentication ARG_LIST((INT4 *));

INT1
nmhGetIssPamLoginPrivilege ARG_LIST((INT4 *));

INT1
nmhGetIssSwitchBaseMacAddress ARG_LIST((tMacAddr * ));

INT1
nmhGetIssOOBInterface ARG_LIST((INT4 *));

INT1
nmhGetIssSwitchDate ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssNoCliConsole ARG_LIST((INT4 *));

INT1
nmhGetIssDefaultIpAddrAllocProtocol ARG_LIST((INT4 *));

INT1
nmhGetIssHttpPort ARG_LIST((INT4 *));

INT1
nmhGetIssHttpStatus ARG_LIST((INT4 *));

INT1
nmhGetIssConfigRestoreFileVersion ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssDefaultRmIfName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssDefaultVlanId ARG_LIST((INT4 *));

INT1
nmhGetIssNpapiMode ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssConfigAutoSaveTrigger ARG_LIST((INT4 *));

INT1
nmhGetIssConfigIncrSaveFlag ARG_LIST((INT4 *));

INT1
nmhGetIssConfigRollbackFlag ARG_LIST((INT4 *));

INT1
nmhGetIssConfigSyncUpOperation ARG_LIST((INT4 *));

INT1
nmhGetIssFrontPanelPortCount ARG_LIST((INT4 *));

INT1
nmhGetIssAuditLogStatus ARG_LIST((INT4 *));

INT1
nmhGetIssAuditLogFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssAuditLogFileSize ARG_LIST((UINT4 *));

INT1
nmhGetIssAuditLogReset ARG_LIST((INT4 *));

INT1
nmhGetIssAuditLogRemoteIpAddr ARG_LIST((UINT4 *));

INT1
nmhGetIssAuditLogInitiateTransfer ARG_LIST((INT4 *));

INT1
nmhGetIssAuditTransferFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssDownLoadTransferMode ARG_LIST((INT4 *));

INT1
nmhGetIssDownLoadUserName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssDownLoadPassword ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssUploadLogTransferMode ARG_LIST((INT4 *));

INT1
nmhGetIssUploadLogUserName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssUploadLogPasswd ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssConfigSaveTransferMode ARG_LIST((INT4 *));

INT1
nmhGetIssConfigSaveUserName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssConfigSavePassword ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssSwitchMinThresholdTemperature ARG_LIST((INT4 *));

INT1
nmhGetIssSwitchMaxThresholdTemperature ARG_LIST((INT4 *));

INT1
nmhGetIssSwitchCurrentTemperature ARG_LIST((INT4 *));

INT1
nmhGetIssSwitchMaxCPUThreshold ARG_LIST((INT4 *));

INT1
nmhGetIssSwitchCurrentCPUThreshold ARG_LIST((INT4 *));

INT1
nmhGetIssSwitchPowerSurge ARG_LIST((INT4 *));

INT1
nmhGetIssSwitchPowerFailure ARG_LIST((INT4 *));

INT1
nmhGetIssSwitchCurrentPowerSupply ARG_LIST((INT4 *));

INT1
nmhGetIssSwitchMaxRAMUsage ARG_LIST((INT4 *));

INT1
nmhGetIssSwitchCurrentRAMUsage ARG_LIST((INT4 *));

INT1
nmhGetIssSwitchMaxFlashUsage ARG_LIST((INT4 *));

INT1
nmhGetIssSwitchCurrentFlashUsage ARG_LIST((INT4 *));

INT1
nmhGetIssConfigRestoreFileFormatVersion ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssDebugOption ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssConfigDefaultValueSaveOption ARG_LIST((INT4 *));

INT1
nmhGetIssConfigSaveIpAddrType ARG_LIST((INT4 *));

INT1
nmhGetIssConfigSaveIpvxAddr ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssConfigRestoreIpAddrType ARG_LIST((INT4 *));

INT1
nmhGetIssConfigRestoreIpvxAddr ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssDlImageFromIpAddrType ARG_LIST((INT4 *));

INT1
nmhGetIssDlImageFromIpvx ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssUploadLogFileToIpAddrType ARG_LIST((INT4 *));

INT1
nmhGetIssUploadLogFileToIpvx ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssAuditLogRemoteIpAddrType ARG_LIST((INT4 *));

INT1
nmhGetIssAuditLogRemoteIpvxAddr ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssSystemTimerSpeed ARG_LIST((UINT4 *));

INT1
nmhGetIssMgmtInterfaceRouting ARG_LIST((INT4 *));

INT1
nmhGetIssMacLearnRateLimit ARG_LIST((INT4 *));

INT1
nmhGetIssMacLearnRateLimitInterval ARG_LIST((UINT4 *));

INT1
nmhGetIssVrfUnqMacFlag ARG_LIST((INT4 *));

INT1
nmhGetIssLoginAttempts ARG_LIST((INT4 *));

INT1
nmhGetIssLoginLockTime ARG_LIST((INT4 *));

INT1
nmhGetIssAuditLogSizeThreshold ARG_LIST((UINT4 *));

INT1
nmhGetIssTelnetStatus ARG_LIST((INT4 *));

INT1
nmhGetIssWebSessionTimeOut ARG_LIST((INT4 *));

INT1
nmhGetIssWebSessionMaxUsers ARG_LIST((INT4 *));

INT1
nmhGetIssHeartBeatMode ARG_LIST((INT4 *));

INT1
nmhGetIssRmRType ARG_LIST((INT4 *));

INT1
nmhGetIssRmDType ARG_LIST((INT4 *));

INT1
nmhGetIssClearConfig ARG_LIST((INT4 *));

INT1
nmhGetIssClearConfigFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssTelnetClientStatus ARG_LIST((INT4 *));

INT1
nmhGetIssSshClientStatus ARG_LIST((INT4 *));

INT1
nmhGetIssActiveTelnetClientSessions ARG_LIST((INT4 *));

INT1
nmhGetIssActiveSshClientSessions ARG_LIST((INT4 *));

INT1
nmhGetIssLogFileSize ARG_LIST((UINT4 *));

INT1
nmhGetIssLogReset ARG_LIST((INT4 *));

INT1
nmhGetIssLogSizeThreshold ARG_LIST((UINT4 *));

INT1
nmhGetIssAutomaticPortCreate ARG_LIST((INT4 *));

INT1
nmhGetIssUlRemoteLogFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssRmStackingInterfaceType ARG_LIST((INT4 *));

INT1
nmhGetIssPeerLoggingOption ARG_LIST((INT4 *));

INT1
nmhGetIssDefaultExecTimeOut ARG_LIST((INT4 *));

INT1
nmhGetIssStandbyRestart ARG_LIST((INT4 *));

INT1
nmhGetIssRestoreType ARG_LIST((INT4 *));

INT1 
nmhGetIssSwitchModeType ARG_LIST((INT4 *));
INT1
nmhGetIssDebugTimeStampOption ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssSwitchName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssDefaultIpAddrCfgMode ARG_LIST((INT4 ));

INT1
nmhSetIssDefaultIpAddr ARG_LIST((UINT4 ));

INT1
nmhSetIssDefaultIpSubnetMask ARG_LIST((UINT4 ));

INT1
nmhSetIssDefaultInterface ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssRestart ARG_LIST((INT4 ));

INT1
nmhSetIssConfigSaveOption ARG_LIST((INT4 ));

INT1
nmhSetIssConfigSaveIpAddr ARG_LIST((UINT4 ));

INT1
nmhSetIssConfigSaveFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssInitiateConfigSave ARG_LIST((INT4 ));

INT1
nmhSetIssConfigRestoreOption ARG_LIST((INT4 ));

INT1
nmhSetIssConfigRestoreIpAddr ARG_LIST((UINT4 ));

INT1
nmhSetIssConfigRestoreFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssInitiateConfigRestore ARG_LIST((INT4 ));

INT1
nmhSetIssDlImageFromIp ARG_LIST((UINT4 ));

INT1
nmhSetIssDlImageName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssInitiateDlImage ARG_LIST((INT4 ));

INT1
nmhSetIssLoggingOption ARG_LIST((INT4 ));

INT1
nmhSetIssUploadLogFileToIp ARG_LIST((UINT4 ));

INT1
nmhSetIssLogFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssInitiateUlLogFile ARG_LIST((INT4 ));

INT1
nmhSetIssSysContact ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssSysLocation ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssLoginAuthentication ARG_LIST((INT4 ));

INT1
nmhSetIssPamLoginPrivilege ARG_LIST((INT4 ));

INT1
nmhSetIssSwitchBaseMacAddress ARG_LIST((tMacAddr ));

INT1
nmhSetIssSwitchDate ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssNoCliConsole ARG_LIST((INT4 ));

INT1
nmhSetIssDefaultIpAddrAllocProtocol ARG_LIST((INT4 ));

INT1
nmhSetIssHttpPort ARG_LIST((INT4 ));

INT1
nmhSetIssHttpStatus ARG_LIST((INT4 ));

INT1
nmhSetIssDefaultRmIfName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssDefaultVlanId ARG_LIST((INT4 ));

INT1
nmhSetIssNpapiMode ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssConfigAutoSaveTrigger ARG_LIST((INT4 ));

INT1
nmhSetIssConfigIncrSaveFlag ARG_LIST((INT4 ));

INT1
nmhSetIssConfigRollbackFlag ARG_LIST((INT4 ));

INT1
nmhSetIssConfigSyncUpOperation ARG_LIST((INT4 ));

INT1
nmhSetIssAuditLogStatus ARG_LIST((INT4 ));

INT1
nmhSetIssAuditLogFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssAuditLogFileSize ARG_LIST((UINT4 ));

INT1
nmhSetIssAuditLogReset ARG_LIST((INT4 ));

INT1
nmhSetIssAuditLogRemoteIpAddr ARG_LIST((UINT4 ));

INT1
nmhSetIssAuditLogInitiateTransfer ARG_LIST((INT4 ));

INT1
nmhSetIssAuditTransferFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssDownLoadTransferMode ARG_LIST((INT4 ));

INT1
nmhSetIssDownLoadUserName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssDownLoadPassword ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssUploadLogTransferMode ARG_LIST((INT4 ));

INT1
nmhSetIssUploadLogUserName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssUploadLogPasswd ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssConfigSaveTransferMode ARG_LIST((INT4 ));

INT1
nmhSetIssConfigSaveUserName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssConfigSavePassword ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssSwitchMinThresholdTemperature ARG_LIST((INT4 ));

INT1
nmhSetIssSwitchMaxThresholdTemperature ARG_LIST((INT4 ));

INT1
nmhSetIssSwitchMaxCPUThreshold ARG_LIST((INT4 ));

INT1
nmhSetIssSwitchPowerSurge ARG_LIST((INT4 ));

INT1
nmhSetIssSwitchPowerFailure ARG_LIST((INT4 ));

INT1
nmhSetIssSwitchMaxRAMUsage ARG_LIST((INT4 ));

INT1
nmhSetIssSwitchMaxFlashUsage ARG_LIST((INT4 ));

INT1
nmhSetIssDebugOption ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssConfigDefaultValueSaveOption ARG_LIST((INT4 ));

INT1
nmhSetIssConfigSaveIpAddrType ARG_LIST((INT4 ));

INT1
nmhSetIssConfigSaveIpvxAddr ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssConfigRestoreIpAddrType ARG_LIST((INT4 ));

INT1
nmhSetIssConfigRestoreIpvxAddr ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssDlImageFromIpAddrType ARG_LIST((INT4 ));

INT1
nmhSetIssDlImageFromIpvx ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssUploadLogFileToIpAddrType ARG_LIST((INT4 ));

INT1
nmhSetIssUploadLogFileToIpvx ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssAuditLogRemoteIpAddrType ARG_LIST((INT4 ));

INT1
nmhSetIssAuditLogRemoteIpvxAddr ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssSystemTimerSpeed ARG_LIST((UINT4 ));

INT1
nmhSetIssMgmtInterfaceRouting ARG_LIST((INT4 ));

INT1
nmhSetIssMacLearnRateLimit ARG_LIST((INT4 ));

INT1
nmhSetIssMacLearnRateLimitInterval ARG_LIST((UINT4 ));

INT1
nmhSetIssVrfUnqMacFlag ARG_LIST((INT4 ));

INT1
nmhSetIssLoginAttempts ARG_LIST((INT4 ));

INT1
nmhSetIssLoginLockTime ARG_LIST((INT4 ));

INT1
nmhSetIssAuditLogSizeThreshold ARG_LIST((UINT4 ));

INT1
nmhSetIssTelnetStatus ARG_LIST((INT4 ));

INT1
nmhSetIssWebSessionTimeOut ARG_LIST((INT4 ));

INT1
nmhSetIssWebSessionMaxUsers ARG_LIST((INT4 ));

INT1
nmhSetIssHeartBeatMode ARG_LIST((INT4 ));

INT1
nmhSetIssRmRType ARG_LIST((INT4 ));

INT1
nmhSetIssRmDType ARG_LIST((INT4 ));

INT1
nmhSetIssClearConfig ARG_LIST((INT4 ));

INT1
nmhSetIssClearConfigFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssTelnetClientStatus ARG_LIST((INT4 ));

INT1
nmhSetIssSshClientStatus ARG_LIST((INT4 ));

INT1
nmhSetIssLogFileSize ARG_LIST((UINT4 ));

INT1
nmhSetIssLogReset ARG_LIST((INT4 ));

INT1
nmhSetIssLogSizeThreshold ARG_LIST((UINT4 ));

INT1
nmhSetIssAutomaticPortCreate ARG_LIST((INT4 ));

INT1
nmhSetIssUlRemoteLogFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssRmStackingInterfaceType ARG_LIST((INT4 ));

INT1
nmhSetIssPeerLoggingOption ARG_LIST((INT4 ));

INT1
nmhSetIssDefaultExecTimeOut ARG_LIST((INT4 ));

INT1
nmhSetIssStandbyRestart ARG_LIST((INT4 ));

INT1
nmhSetIssRestoreType ARG_LIST((INT4 ));

INT1 
nmhSetIssSwitchModeType ARG_LIST((INT4 ));
INT1
nmhSetIssDebugTimeStampOption ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssSwitchName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssDefaultIpAddrCfgMode ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssDefaultIpAddr ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2IssDefaultIpSubnetMask ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2IssDefaultInterface ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssRestart ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssConfigSaveOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssConfigSaveIpAddr ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2IssConfigSaveFileName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssInitiateConfigSave ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssConfigRestoreOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssConfigRestoreIpAddr ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2IssConfigRestoreFileName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssInitiateConfigRestore ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssDlImageFromIp ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2IssDlImageName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssInitiateDlImage ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssLoggingOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssUploadLogFileToIp ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2IssLogFileName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssInitiateUlLogFile ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssSysContact ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssSysLocation ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssLoginAuthentication ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssPamLoginPrivilege ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssSwitchBaseMacAddress ARG_LIST((UINT4 *  ,tMacAddr ));

INT1
nmhTestv2IssSwitchDate ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssNoCliConsole ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssDefaultIpAddrAllocProtocol ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssHttpPort ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssHttpStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssDefaultRmIfName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssDefaultVlanId ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssNpapiMode ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssConfigAutoSaveTrigger ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssConfigIncrSaveFlag ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssConfigRollbackFlag ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssConfigSyncUpOperation ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssAuditLogStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssAuditLogFileName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssAuditLogFileSize ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2IssAuditLogReset ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssAuditLogRemoteIpAddr ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2IssAuditLogInitiateTransfer ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssAuditTransferFileName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssDownLoadTransferMode ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssDownLoadUserName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssDownLoadPassword ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssUploadLogTransferMode ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssUploadLogUserName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssUploadLogPasswd ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssConfigSaveTransferMode ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssConfigSaveUserName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssConfigSavePassword ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssSwitchMinThresholdTemperature ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssSwitchMaxThresholdTemperature ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssSwitchMaxCPUThreshold ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssSwitchPowerSurge ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssSwitchPowerFailure ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssSwitchMaxRAMUsage ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssSwitchMaxFlashUsage ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssDebugOption ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssConfigDefaultValueSaveOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssConfigSaveIpAddrType ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssConfigSaveIpvxAddr ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssConfigRestoreIpAddrType ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssConfigRestoreIpvxAddr ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssDlImageFromIpAddrType ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssDlImageFromIpvx ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssUploadLogFileToIpAddrType ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssUploadLogFileToIpvx ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssAuditLogRemoteIpAddrType ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssAuditLogRemoteIpvxAddr ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssSystemTimerSpeed ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2IssMgmtInterfaceRouting ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssMacLearnRateLimit ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssMacLearnRateLimitInterval ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2IssVrfUnqMacFlag ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssLoginAttempts ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssLoginLockTime ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssAuditLogSizeThreshold ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2IssTelnetStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssWebSessionTimeOut ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssWebSessionMaxUsers ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssHeartBeatMode ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssRmRType ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssRmDType ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssClearConfig ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssClearConfigFileName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssTelnetClientStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssSshClientStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssLogFileSize ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2IssLogReset ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssLogSizeThreshold ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2IssAutomaticPortCreate ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssUlRemoteLogFileName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssRmStackingInterfaceType ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssPeerLoggingOption ARG_LIST((UINT4 *  ,INT4 ));


INT1
nmhTestv2IssDefaultExecTimeOut ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssStandbyRestart ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssRestoreType ARG_LIST((UINT4 *  ,INT4 ));

INT1 
nmhTestv2IssSwitchModeType ARG_LIST((UINT4 *  ,INT4 ));
INT1
nmhTestv2IssDebugTimeStampOption ARG_LIST((UINT4 *  ,INT4 ));


/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssSwitchName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssDefaultIpAddrCfgMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssDefaultIpAddr ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssDefaultIpSubnetMask ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssDefaultInterface ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssRestart ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssConfigSaveOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssConfigSaveIpAddr ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssConfigSaveFileName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssInitiateConfigSave ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssConfigRestoreOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssConfigRestoreIpAddr ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssConfigRestoreFileName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssInitiateConfigRestore ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssDlImageFromIp ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssDlImageName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssInitiateDlImage ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssLoggingOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssUploadLogFileToIp ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssLogFileName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssInitiateUlLogFile ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssSysContact ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssSysLocation ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssLoginAuthentication ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssPamLoginPrivilege ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssSwitchBaseMacAddress ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssSwitchDate ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssNoCliConsole ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssDefaultIpAddrAllocProtocol ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssHttpPort ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssHttpStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssDefaultRmIfName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssDefaultVlanId ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssNpapiMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssConfigAutoSaveTrigger ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssConfigIncrSaveFlag ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssConfigRollbackFlag ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssConfigSyncUpOperation ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssFrontPanelPortCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssAuditLogStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssAuditLogFileName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssAuditLogFileSize ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssAuditLogReset ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssAuditLogRemoteIpAddr ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssAuditLogInitiateTransfer ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssAuditTransferFileName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssDownLoadTransferMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssDownLoadUserName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssDownLoadPassword ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssUploadLogTransferMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssUploadLogUserName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssUploadLogPasswd ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssConfigSaveTransferMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssConfigSaveUserName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssConfigSavePassword ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssSwitchMinThresholdTemperature ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssSwitchMaxThresholdTemperature ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssSwitchMaxCPUThreshold ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssSwitchPowerSurge ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssSwitchPowerFailure ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssSwitchMaxRAMUsage ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssSwitchMaxFlashUsage ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssDebugOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssConfigDefaultValueSaveOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssConfigSaveIpAddrType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssConfigSaveIpvxAddr ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssConfigRestoreIpAddrType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssConfigRestoreIpvxAddr ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssDlImageFromIpAddrType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssDlImageFromIpvx ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssUploadLogFileToIpAddrType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssUploadLogFileToIpvx ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssAuditLogRemoteIpAddrType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssAuditLogRemoteIpvxAddr ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssSystemTimerSpeed ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssMgmtInterfaceRouting ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssMacLearnRateLimit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssMacLearnRateLimitInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssVrfUnqMacFlag ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssLoginAttempts ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssLoginLockTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssAuditLogSizeThreshold ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssTelnetStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssWebSessionTimeOut ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssWebSessionMaxUsers ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssHeartBeatMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssRmRType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssRmDType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssClearConfig ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssClearConfigFileName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssTelnetClientStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssSshClientStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssLogFileSize ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssLogReset ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssLogSizeThreshold ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssAutomaticPortCreate ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssUlRemoteLogFileName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


INT1
nmhDepv2IssRmStackingInterfaceType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssPeerLoggingOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


INT1
nmhDepv2IssDefaultExecTimeOut ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssStandbyRestart ARG_LIST((UINT4 *, tSnmpIndexList*,tSNMP_VAR_BIND*));

INT1
nmhDepv2IssRestoreType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1 
nmhDepv2IssSwitchModeType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
INT1
nmhDepv2IssDebugTimeStampOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


/* Proto Validate Index Instance for IssConfigCtrlTable. */
INT1
nmhValidateIndexInstanceIssConfigCtrlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssConfigCtrlTable  */

INT1
nmhGetFirstIndexIssConfigCtrlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssConfigCtrlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssConfigCtrlEgressStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssConfigCtrlStatsCollection ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssConfigCtrlStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssConfigCtrlEgressStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssConfigCtrlStatsCollection ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssConfigCtrlStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssConfigCtrlEgressStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssConfigCtrlStatsCollection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssConfigCtrlStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssConfigCtrlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IssPortCtrlTable. */
INT1
nmhValidateIndexInstanceIssPortCtrlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssPortCtrlTable  */

INT1
nmhGetFirstIndexIssPortCtrlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssPortCtrlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssPortCtrlMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssPortCtrlDuplex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssPortCtrlSpeed ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssPortCtrlFlowControl ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssPortCtrlRenegotiate ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssPortCtrlMaxMacAddr ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssPortCtrlMaxMacAction ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssPortHOLBlockPrevention ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssPortAutoNegAdvtCapBits ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssPortCpuControlledLearning ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssPortMdiOrMdixCap ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssPortCtrlFlowControlMaxRate ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssPortCtrlFlowControlMinRate ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssPortCtrlMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssPortCtrlDuplex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssPortCtrlSpeed ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssPortCtrlFlowControl ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssPortCtrlRenegotiate ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssPortCtrlMaxMacAddr ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssPortCtrlMaxMacAction ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssPortHOLBlockPrevention ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssPortAutoNegAdvtCapBits ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssPortCpuControlledLearning ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssPortMdiOrMdixCap ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssPortCtrlFlowControlMaxRate ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssPortCtrlFlowControlMinRate ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssPortCtrlMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssPortCtrlDuplex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssPortCtrlSpeed ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssPortCtrlFlowControl ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssPortCtrlRenegotiate ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssPortCtrlMaxMacAddr ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssPortCtrlMaxMacAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssPortHOLBlockPrevention ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssPortAutoNegAdvtCapBits ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssPortCpuControlledLearning ARG_LIST((UINT4 * ,INT4  ,INT4 ));

INT1
nmhTestv2IssPortMdiOrMdixCap ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssPortCtrlFlowControlMaxRate ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssPortCtrlFlowControlMinRate ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssPortCtrlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IssPortIsolationTable. */
INT1
nmhValidateIndexInstanceIssPortIsolationTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssPortIsolationTable  */

INT1
nmhGetFirstIndexIssPortIsolationTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssPortIsolationTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssPortIsolationStorageType ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetIssPortIsolationRowStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssPortIsolationRowStatus ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssPortIsolationRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssPortIsolationTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssMirrorStatus ARG_LIST((INT4 *));

INT1
nmhGetIssMirrorToPort ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssMirrorStatus ARG_LIST((INT4 ));

INT1
nmhSetIssMirrorToPort ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssMirrorStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssMirrorToPort ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssMirrorStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssMirrorToPort ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IssMirrorCtrlTable. */
INT1
nmhValidateIndexInstanceIssMirrorCtrlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssMirrorCtrlTable  */

INT1
nmhGetFirstIndexIssMirrorCtrlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssMirrorCtrlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssMirrorCtrlIngressMirroring ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssMirrorCtrlEgressMirroring ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssMirrorCtrlStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssMirrorCtrlIngressMirroring ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssMirrorCtrlEgressMirroring ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssMirrorCtrlStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssMirrorCtrlIngressMirroring ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssMirrorCtrlEgressMirroring ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssMirrorCtrlStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssMirrorCtrlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssMirrorCtrlRemainingSrcRcrds ARG_LIST((INT4 *));

INT1
nmhGetIssMirrorCtrlRemainingDestRcrds ARG_LIST((INT4 *));

/* Proto Validate Index Instance for IssMirrorCtrlExtnTable. */
INT1
nmhValidateIndexInstanceIssMirrorCtrlExtnTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssMirrorCtrlExtnTable  */

INT1
nmhGetFirstIndexIssMirrorCtrlExtnTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssMirrorCtrlExtnTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssMirrorCtrlExtnMirrType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssMirrorCtrlExtnRSpanStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssMirrorCtrlExtnRSpanVlanId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssMirrorCtrlExtnRSpanContext ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssMirrorCtrlExtnStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssMirrorCtrlExtnMirrType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssMirrorCtrlExtnRSpanStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssMirrorCtrlExtnRSpanVlanId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssMirrorCtrlExtnRSpanContext ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssMirrorCtrlExtnStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssMirrorCtrlExtnMirrType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssMirrorCtrlExtnRSpanStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssMirrorCtrlExtnRSpanVlanId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssMirrorCtrlExtnRSpanContext ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssMirrorCtrlExtnStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssMirrorCtrlExtnTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IssMirrorCtrlExtnSrcTable. */
INT1
nmhValidateIndexInstanceIssMirrorCtrlExtnSrcTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssMirrorCtrlExtnSrcTable  */

INT1
nmhGetFirstIndexIssMirrorCtrlExtnSrcTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssMirrorCtrlExtnSrcTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssMirrorCtrlExtnSrcCfg ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetIssMirrorCtrlExtnSrcMode ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssMirrorCtrlExtnSrcCfg ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetIssMirrorCtrlExtnSrcMode ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssMirrorCtrlExtnSrcCfg ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IssMirrorCtrlExtnSrcMode ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssMirrorCtrlExtnSrcTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IssMirrorCtrlExtnSrcVlanTable. */
INT1
nmhValidateIndexInstanceIssMirrorCtrlExtnSrcVlanTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssMirrorCtrlExtnSrcVlanTable  */

INT1
nmhGetFirstIndexIssMirrorCtrlExtnSrcVlanTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssMirrorCtrlExtnSrcVlanTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssMirrorCtrlExtnSrcVlanCfg ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetIssMirrorCtrlExtnSrcVlanMode ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssMirrorCtrlExtnSrcVlanCfg ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetIssMirrorCtrlExtnSrcVlanMode ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssMirrorCtrlExtnSrcVlanCfg ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2IssMirrorCtrlExtnSrcVlanMode ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssMirrorCtrlExtnSrcVlanTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IssMirrorCtrlExtnDestinationTable. */
INT1
nmhValidateIndexInstanceIssMirrorCtrlExtnDestinationTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssMirrorCtrlExtnDestinationTable  */

INT1
nmhGetFirstIndexIssMirrorCtrlExtnDestinationTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssMirrorCtrlExtnDestinationTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssMirrorCtrlExtnDestCfg ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssMirrorCtrlExtnDestCfg ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssMirrorCtrlExtnDestCfg ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssMirrorCtrlExtnDestinationTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssCpuMirrorType ARG_LIST((INT4 *));

INT1
nmhGetIssCpuMirrorToPort ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssCpuMirrorType ARG_LIST((INT4 ));

INT1
nmhSetIssCpuMirrorToPort ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssCpuMirrorType ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssCpuMirrorToPort ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssCpuMirrorType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssCpuMirrorToPort ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IssIpAuthMgrTable. */
INT1
nmhValidateIndexInstanceIssIpAuthMgrTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for IssIpAuthMgrTable  */

INT1
nmhGetFirstIndexIssIpAuthMgrTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssIpAuthMgrTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssIpAuthMgrPortList ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssIpAuthMgrVlanList ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssIpAuthMgrOOBPort ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIssIpAuthMgrAllowedServices ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIssIpAuthMgrRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssIpAuthMgrPortList ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssIpAuthMgrVlanList ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssIpAuthMgrOOBPort ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIssIpAuthMgrAllowedServices ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIssIpAuthMgrRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssIpAuthMgrPortList ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssIpAuthMgrVlanList ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssIpAuthMgrOOBPort ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2IssIpAuthMgrAllowedServices ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2IssIpAuthMgrRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssIpAuthMgrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IssRateCtrlTable. */
INT1
nmhValidateIndexInstanceIssRateCtrlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssRateCtrlTable  */

INT1
nmhGetFirstIndexIssRateCtrlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssRateCtrlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssRateCtrlDLFLimitValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssRateCtrlBCASTLimitValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssRateCtrlMCASTLimitValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssRateCtrlPortRateLimit ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssRateCtrlPortBurstSize ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssRateCtrlDLFLimitValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssRateCtrlBCASTLimitValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssRateCtrlMCASTLimitValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssRateCtrlPortRateLimit ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssRateCtrlPortBurstSize ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssRateCtrlDLFLimitValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssRateCtrlBCASTLimitValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssRateCtrlMCASTLimitValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssRateCtrlPortRateLimit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssRateCtrlPortBurstSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssRateCtrlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IssL2FilterTable. */
INT1
nmhValidateIndexInstanceIssL2FilterTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssL2FilterTable  */

INT1
nmhGetFirstIndexIssL2FilterTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssL2FilterTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssL2FilterPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL2FilterEtherType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL2FilterProtocolType ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssL2FilterDstMacAddr ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetIssL2FilterSrcMacAddr ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetIssL2FilterVlanId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL2FilterInPortList ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssL2FilterAction ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL2FilterMatchCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssL2FilterStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL2FilterOutPortList ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssL2FilterDirection ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssL2FilterPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL2FilterEtherType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL2FilterProtocolType ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssL2FilterDstMacAddr ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetIssL2FilterSrcMacAddr ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetIssL2FilterVlanId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL2FilterInPortList ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssL2FilterAction ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL2FilterStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL2FilterOutPortList ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssL2FilterDirection ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssL2FilterPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL2FilterEtherType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL2FilterProtocolType ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssL2FilterDstMacAddr ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2IssL2FilterSrcMacAddr ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2IssL2FilterVlanId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL2FilterInPortList ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssL2FilterAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL2FilterStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL2FilterOutPortList ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssL2FilterDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssL2FilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IssL3FilterTable. */
INT1
nmhValidateIndexInstanceIssL3FilterTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssL3FilterTable  */

INT1
nmhGetFirstIndexIssL3FilterTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssL3FilterTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssL3FilterPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL3FilterProtocol ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL3FilterMessageType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL3FilterMessageCode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL3FilterDstIpAddr ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssL3FilterSrcIpAddr ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssL3FilterDstIpAddrMask ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssL3FilterSrcIpAddrMask ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssL3FilterMinDstProtPort ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssL3FilterMaxDstProtPort ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssL3FilterMinSrcProtPort ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssL3FilterMaxSrcProtPort ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssL3FilterInPortList ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssL3FilterOutPortList ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssL3FilterAckBit ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL3FilterRstBit ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL3FilterTos ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL3FilterDscp ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL3FilterDirection ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL3FilterAction ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL3FilterMatchCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssL3FilterStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssL3FilterPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL3FilterProtocol ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL3FilterMessageType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL3FilterMessageCode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL3FilterDstIpAddr ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssL3FilterSrcIpAddr ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssL3FilterDstIpAddrMask ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssL3FilterSrcIpAddrMask ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssL3FilterMinDstProtPort ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssL3FilterMaxDstProtPort ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssL3FilterMinSrcProtPort ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssL3FilterMaxSrcProtPort ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssL3FilterInPortList ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssL3FilterOutPortList ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIssL3FilterAckBit ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL3FilterRstBit ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL3FilterTos ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL3FilterDscp ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL3FilterDirection ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL3FilterAction ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL3FilterStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssL3FilterPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL3FilterProtocol ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL3FilterMessageType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL3FilterMessageCode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL3FilterDstIpAddr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssL3FilterSrcIpAddr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssL3FilterDstIpAddrMask ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssL3FilterSrcIpAddrMask ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssL3FilterMinDstProtPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssL3FilterMaxDstProtPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssL3FilterMinSrcProtPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssL3FilterMaxSrcProtPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssL3FilterInPortList ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssL3FilterOutPortList ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2IssL3FilterAckBit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL3FilterRstBit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL3FilterTos ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL3FilterDscp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL3FilterDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL3FilterAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL3FilterStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssL3FilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IssL4SwitchingFilterTable. */
INT1
nmhValidateIndexInstanceIssL4SwitchingFilterTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssL4SwitchingFilterTable  */

INT1
nmhGetFirstIndexIssL4SwitchingFilterTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssL4SwitchingFilterTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssL4SwitchingProtocol ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL4SwitchingPortNo ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetIssL4SwitchingCopyToPort ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetIssL4SwitchingFilterStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssL4SwitchingProtocol ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL4SwitchingPortNo ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetIssL4SwitchingCopyToPort ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetIssL4SwitchingFilterStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssL4SwitchingProtocol ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL4SwitchingPortNo ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2IssL4SwitchingCopyToPort ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2IssL4SwitchingFilterStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssL4SwitchingFilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IssModuleTable. */
INT1
nmhValidateIndexInstanceIssModuleTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssModuleTable  */

INT1
nmhGetFirstIndexIssModuleTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssModuleTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssModuleSystemControl ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssModuleSystemControl ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssModuleSystemControl ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssModuleTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IssSwitchFanTable. */
INT1
nmhValidateIndexInstanceIssSwitchFanTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssSwitchFanTable  */

INT1
nmhGetFirstIndexIssSwitchFanTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIssSwitchFanTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssSwitchFanStatus ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssAuditTrapFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIssLogTrapFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssAclProvisionMode ARG_LIST((INT4 *));

INT1
nmhGetIssAclTriggerCommit ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssAclProvisionMode ARG_LIST((INT4 ));

INT1
nmhSetIssAclTriggerCommit ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssAclProvisionMode ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2IssAclTriggerCommit ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssAclProvisionMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2IssAclTriggerCommit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssAclTrafficSeperationCtrl ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssAclTrafficSeperationCtrl ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssAclTrafficSeperationCtrl ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssAclTrafficSeperationCtrl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIssHealthChkStatus ARG_LIST((INT4 *));

INT1
nmhGetIssHealthChkErrorReason ARG_LIST((INT4 *));

INT1
nmhGetIssHealthChkMemAllocErrPoolId ARG_LIST((INT4 *));

INT1
nmhGetIssHealthChkConfigRestoreStatus ARG_LIST((INT4 *));

INT1
nmhGetIssHealthChkClearCtr ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIssHealthChkClearCtr ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2IssHealthChkClearCtr ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2IssHealthChkClearCtr ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
#endif
