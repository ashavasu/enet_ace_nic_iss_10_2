/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  
 *
 * $Id: issproto.h,v 1.57 2017/12/07 10:07:32 siva Exp $            
 *
 * Description : This function contains all the prototype declarations in ISS.
 *
 *****************************************************************************/
/*****************************************************************************/
/*    FILE  NAME            : issproto.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#ifndef _ISSPROTO_H
#define _ISSPROTO_H


/* Reading the System Information */
INT4 IssReadSystemInfoFromNvRam PROTO ((VOID));
INT1 IssSetImageDumpLocation PROTO ((UINT1* ));

VOID IssInitialiseNvRamDefVal PROTO ((VOID));

/* Function for getting Nvram Data */
INT1 *IssGetRestoreFileNameFromNvRam PROTO ((VOID));
INT1 *IssGetBgpConfRestoreFileNameFromNvRam PROTO ((VOID));
INT1 *IssGetCsrConfRestoreFileNameFromNvRam PROTO ((VOID));
INT1 *IssGetOspfConfRestoreFileNameFromNvRam PROTO ((VOID));

INT1 *IssGetIncrSaveFileNameFromNvRam PROTO ((VOID));
tIPvXAddr *IssGetRestoreIpAddrFromNvRam PROTO ((VOID));
UINT4 IssGetMgmtPortFromNvRam PROTO ((VOID));
INT1  IssGetSaveFlagFromNvRam PROTO ((VOID));
UINT4 IssGetCliSerialConsoleFromNvRam PROTO ((VOID));
INT4 IssGetAutoFlagnFromNvRam PROTO ((VOID));
INT4 IssGetIncrSaveFlagnFromNvRam PROTO ((VOID));
VOID IssSetIncrSaveToNvRam (INT4 i4IncrSaveFlag);
INT4 IssGetRollbackFlagnFromNvRam PROTO ((VOID));
UINT4 IssGetColdStdbyState PROTO ((VOID));
INT4 IssGetAutoPortCreateFlagFromNvRam PROTO ((VOID));

/* Function for setting Nvram Data */
VOID IssSetSaveFlagToNvRam  PROTO ((INT1 ));
VOID IssSetIpAddrToNvRam PROTO((UINT4 ));
VOID IssSetSubnetMaskToNvRam PROTO ((UINT4 )); 
VOID IssSetInterfaceToNvRam PROTO ((INT1 *));
VOID IssSetRmIfNameToNvRam PROTO ((INT1 *));
VOID IssSetSoftwareVersionToNvRam PROTO ((INT1 *));
VOID IssSetRestoreIpAddrToNvRam PROTO((tIPvXAddr *));
VOID IssSetRestoreFlagToNvRam      PROTO ((INT1 ));
VOID IssSetConfigModeToNvRam PROTO ((UINT4));
VOID IssSetRestoreFileNameToNvRam PROTO ((INT1 *));
VOID IssSetSwitchBaseMacAddressToNvRam PROTO ((tMacAddr));
VOID IssSetCliSerialConsoleToNvRam PROTO ((UINT4));
VOID IssSetAutoSaveToNvRam (INT4 i4AutoSaveFlag);
VOID IssSetRollbackFlagToNvRam (INT4 i4RollbackFlag);
VOID IssSetHeartBeatModeToNvRam PROTO ((UINT4 )); 
VOID IssSetRedundancyTypeToNvRam PROTO ((UINT4 )); 
VOID IssSetRmDataPlaneToNvRam PROTO ((UINT4 )); 
VOID IssSetAutoPortCreateFlagToNvRam PROTO ((INT4));
UINT1 IssValidateFileName PROTO ((UINT1 *));

VOID IssSetFlashLoggingFileNameToNvRam PROTO ((UINT1 *));
INT1 IssSetFlashLoggingLocation PROTO ((UINT1 *));
INT1 IssGetFlashLoggingLocation PROTO ((UINT1 *));

/*  Iss Init Routine */
INT4 IssCreateDefaultPort PROTO ((UINT2));

INT4 IssCreateAllPorts PROTO ((tIssTableName ));
INT4 IssReleaseAllPorts PROTO ((tIssTableName ));

INT4 IssHandlePortCreateFailure PROTO ((tIssConfigCtrlEntry *,
                                        tIssPortCtrlEntry *));

VOID IssSetDefaultConfigCtrlValues PROTO ((UINT2, tIssConfigCtrlEntry *));
VOID IssSetDefaultPortCtrlValues PROTO ((UINT2 ,tIssPortCtrlEntry *));

INT4 IssUtilCreatePort PROTO ((UINT2 u2PortIndex, tIssTableName IssTableFlag));
    
INT4 IssSnmpLowValidateIndex PROTO ((INT4 ));
INT4 IssSnmpLowValidateIndexPortCtrlTable PROTO ((INT4 ));

INT4 IssSnmpLowGetFirstValidConfigCtrlIndex  PROTO ((INT4 *));
INT4 IssSnmpLowGetNextValidConfigCtrlIndex  PROTO ((INT4, INT4 *));

INT4 IssSnmpLowGetFirstValidMirrorCtrlIndex  PROTO ((INT4 *));
INT4 IssSnmpLowGetNextValidMirrorCtrlIndex  PROTO ((INT4, INT4 *));


INT4 IssValidateConfigCtrlEntry PROTO ((INT4 ));
INT4 IssValidatePortCtrlEntry PROTO ((INT4 ));
INT4 IssValidateMirrorCtrlEntry PROTO ((INT4 ));
INT4 IssRemoveMirrorCtrlEntry PROTO ((UINT4 *, INT4, UINT4, UINT4));
INT4 IssValidateRateCtrlEntry PROTO ((INT4 ));

INT4 IssSnmpLowGetFirstValidL2FilterTableIndex PROTO ((INT4 *));
INT4 IssSnmpLowGetNextValidL2FilterTableIndex PROTO ((INT4, INT4 *));

INT4 IssSnmpLowGetFirstValidL3FilterTableIndex PROTO ((INT4 *));
INT4 IssSnmpLowGetNextValidL3FilterTableIndex PROTO ((INT4, INT4 *));

tIssL2FilterEntry *IssGetL2FilterEntry PROTO ((INT4));
INT4 IssQualifyL2FilterData PROTO ((tIssL2FilterEntry **));

tIssL3FilterEntry *IssGetL3FilterEntry PROTO ((INT4));
tIssL4SFilterEntry *IssGetL4SFilterEntry (INT4 i4IssL4SFilterNo);

INT4 IssQualifyL3FilterData PROTO ((tIssL3FilterEntry **));
VOID IssStackingSystemRestart (VOID);
INT4 issStatStartupFile (UINT1 *pu1Src);
VOID IssEnableAllPortsInPortList PROTO ((UINT1 *, UINT4));
UINT4 issSetSwitchDate(tSNMP_OCTET_STRING_TYPE *);
UINT4  issGetSwitchDate(tSNMP_OCTET_STRING_TYPE *);
INT4 IssValidateCidrSubnetMask PROTO ((UINT4 u4SubnetMask));
INT1 IssSendCtrlMsg PROTO ((tISSCtrlMsg *));
UINT4 IssGetPortSpeedDuplex (INT4 i4IssPortCtrlIndex,
                             INT4 *pi4RetValIssPortCtrlSpeed,
                             INT4 *pi4RetValIssPortCtrlDuplex);

INT4 IssSnmpLowGetPortCtrlDuplex (INT4 i4Index, 
                                  INT4 *pi4PortCtrlDuplex);
INT4 IssInitMirrSession (INT4 u4SessionNo);
INT4 IssMirrGetNextDestRecrd(UINT4 u4SessionNo, UINT4 u4DestNum, UINT4 *pu4NextDestNum);
INT4 IssMirrAddDestRecrd(UINT4 u4SessionNo, UINT4 u4DestNum);
INT4 IssMirrRemoveDestRecrd(UINT4 u4SessionNo, UINT4 u4DestNum);
INT4 IssMirrGetSrcRecrdInfo ( UINT4 u4SessionNo, UINT4 u4ContextId, UINT4 u4SrcNum,
                      UINT1 *pu1Mode, UINT1 *pu1ControlStatus);
INT4 IssMirrGetNextSrcRecrd(UINT4 u4SessionNo, UINT4 u4ContextId, UINT4 u4SrcNum,
                    UINT4 *pu4NextContextNum, UINT4 *pu4NextSrcNum, UINT1
                    *pu1Mode, UINT1 *pu1ControlStatus);
INT4 IssMirrAddSrcRecrd(UINT4 u4SessionNo, UINT4 u4ContextId, UINT4 u4SrcNum, UINT1
                    u1Mode, UINT1 u1ControlStatus);
INT4 IssMirrRemoveSrcRecrd(UINT4 u4SessionNo, UINT4 u4ContextId, UINT4 u4SrcNum);

INT4 IssInitMirrDataBase(VOID);
INT4 IssMirrConfigHw(UINT2 u2SessionNo,UINT1 u1ConfigMode);
VOID IssMirrHandleCreatePort PROTO ((UINT2 u2PortIndex));
VOID IssMirrDelDefaultSessionDB(VOID);
INT4 IssL2IwfIsPortInPortChannel PROTO((UINT4 u4IfIndex));
INT4 IssL2IwfGetPortChannelForPort PROTO((UINT4 u4IfIndex, UINT2 *pu2AggId));
/* issfm.c */
PUBLIC INT4 IssSystTmrInit PROTO ((VOID));
PUBLIC VOID IssSysTmrExpHandler PROTO ((VOID));

PUBLIC VOID
IssSendTrapAndLog PROTO ((tIssSystTrapInfo *, UINT1));
PUBLIC tSNMP_VAR_BIND *
IssFormRAMThresholdTrapMsg PROTO ((UINT4, UINT4));
PUBLIC tSNMP_VAR_BIND *
IssFormFanStatusTrapMsg PROTO ((UINT4, UINT4));
PUBLIC tSNMP_VAR_BIND *
IssFormCPUThresholdTrapMsg PROTO ((UINT4, UINT4));
PUBLIC tSNMP_VAR_BIND *
IssFormTemperatureThresTrapMsg PROTO ((INT4, INT4, INT4));
PUBLIC tSNMP_VAR_BIND *
IssFormPowerSupplyTrapMsg PROTO ((UINT4, UINT4, UINT4));
PUBLIC tSNMP_VAR_BIND *
IssFormFlashThresholdTrapMsg PROTO ((UINT4, UINT4));

/* Prototypes for MAC Learn Limit Rate Feature */
VOID IssInitMacLearnLimitParams PROTO ((VOID));
VOID IssDeInitMacLearnLimitParams PROTO ((VOID));
VOID IssMacLearnLimitStartTmr PROTO ((VOID));
VOID IssMacLearnLimitProcessTimeout PROTO ((VOID));

INT1 IssGetPsPhyInfoInStack PROTO ((UINT4 , tIssEntPhyInfo *));
INT1 IssGetCpuPhyInfoInStack PROTO ((UINT4 , tIssEntPhyInfo *));
INT1 IssGetFanPhyInfoInStack PROTO ((UINT4 , tIssEntPhyInfo *));
INT1 IssGetChassisPhyInfo PROTO ((UINT4 , tIssEntPhyInfo *));
INT1 IssGetCpuPhyInfoInChassis PROTO ((UINT4 , UINT2 , tIssEntPhyInfo *));
INT1 IssGetPsPhyInfoInChassis PROTO ((UINT4 , UINT2 , tIssEntPhyInfo *));
INT1 IssGetFanPhyInfoInChassis PROTO ((UINT4 , UINT2 , tIssEntPhyInfo *));
INT1 IssGetIfPhyInfo PROTO ((UINT4 , UINT4 , tIssEntPhyInfo *));
INT1 IssGetStackPhyInfo PROTO ((tIssEntPhyInfo *));

VOID IssGetPsCountInChassis PROTO ((UINT4, UINT2 *));
VOID IssGetFanCountInChassis PROTO ((UINT4 , UINT2 *));
VOID IssGetCpuCountInChassis PROTO ((UINT4, UINT2 *));
VOID IssGetChassisCount PROTO ((UINT2 *));
VOID IssGetCpuCountInStack PROTO ((UINT2 *));
VOID IssGetFanCountInStack PROTO ((UINT2 *));
VOID IssGetPsCountInStack PROTO ((UINT2 *));
INT1 IssSetPsPhyInfoInStack PROTO ((UINT4 , UINT1, UINT1 *));
INT1 IssSetCpuPhyInfoInStack PROTO ((UINT4 , UINT1, UINT1 *));
INT1 IssSetFanPhyInfoInStack PROTO ((UINT4 , UINT1, UINT1 *));
INT1 IssSetChassisPhyInfo PROTO ((UINT4 , UINT1, UINT1 *));
INT1 IssSetCpuPhyInfoInChassis PROTO ((UINT4 , UINT2 , UINT1, UINT1 *));
INT1 IssSetPsPhyInfoInChassis PROTO ((UINT4 , UINT2 , UINT1, UINT1 *));
INT1 IssSetFanPhyInfoInChassis PROTO ((UINT4 , UINT2 , UINT1, UINT1 *));
INT1 IssSetIfPhyInfo PROTO ((UINT4 , UINT4 , UINT1, UINT1 *));
INT1 IssSetStackPhyInfo PROTO ((UINT1, UINT1 *));
UINT4 IssPortRmRegisterProtocols PROTO((tRmRegParams *));
INT4 IssPortRmReleaseMemoryForMsg PROTO((UINT1 *));
UINT4 IssPortRmEnqMsgToRm PROTO((tRmMsg *, UINT2 ,UINT4 ,UINT4));
INT4 IssPortRmApiSendProtoAckToRM PROTO((tRmProtoAck *));
INT4 IssPortRmApiHandleProtocolEvent PROTO((tRmProtoEvt *));
INT4 IssPortRmDeRegisterProtocols PROTO((UINT4));
UINT1 IssPortRmGetStandbyNodeCount PROTO((VOID));
INT4 IssPortRmGetNodeState PROTO((VOID));

INT4 IssSnmpLowValidateIndexPortRateTable (INT4 i4PortIndex);

INT4 IssShowRunningConfigInd(tCliHandle ,UINT4);
INT4 IssShowRunningConfigRegHL PROTO ((tShowRunningConfigRegParams *));

INT4 CliIssWebSessionTimeOut PROTO ((tCliHandle, INT4 * ));


INT4 CliIssDefaultExecTimeOut PROTO ((tCliHandle, INT4 * ));
INT4 CliIssNoDefaultExecTimeOut PROTO ((tCliHandle CliHandle));
INT4 VpnShowRunningConfig ( tCliHandle CliHandle );
INT4 CliIssDebugEnable PROTO((tCliHandle, UINT1 *));
INT4  CliIssConfigDebugTimeStamp PROTO ((tCliHandle , INT4 ));
INT4
IssRmEnqChkSumMsgToRm PROTO ((UINT2, UINT2 ));

INT4
IssGetShowCmdOutputAndCalcChkSum (UINT2 *pu2SwAudChkSum);
INT4
IssCliGetShowCmdOutputToFile (UINT1 *);

INT4
IssCliCalcSwAudCheckSum (UINT1 *, UINT2 *);

INT1
IssCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen, INT2 *pi2ReadLen);

INT1
IssIsMirrorVaildActiveStatus (INT4);

INT1
IssPortIcchIsIcclVlan (UINT2);
#endif /* _ISSPROTO_H */


