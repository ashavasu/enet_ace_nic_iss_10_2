/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: issred.h,v 1.2 2014/06/27 11:16:09 siva Exp $
 *
 * DESCRIPTION           : This file contains data structures and macros defined 
 *                         for HA support in ISS. 
 *****************************************************************************/

/*****************************************************************************/
/*    FILE  NAME            : issred.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    MODULE NAME           : ISS                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Linux                                          */
/*    DATE OF FIRST RELEASE : 21 July 2010                                   */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           :  This header file contains                     */
/*                             (i) data structure definition,                */
/*                             (ii) Macros definition,                       */
/*                             (iii) prototype declarations,                 */
/*                             (iv) extern declarations                      */
/*                             required for ISS HA Support.                  */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    21 July 2010           Initial Creation                        */
/*---------------------------------------------------------------------------*/

#ifndef ISSRED_H
#define ISSRED_H

#include "hwaud.h"

/* Macro for redundancy message */
#define ISS_RED_QUEUE                (UINT1*) "ISSRED"
#define ISS_RED_MESSAGE               1
#define ISS_RED_QUEUE_SIZE           32

/* Macro for Maximum NpSync entries suppported */
#define ISS_MAX_NPSYNC_BUF_ENTRIES    32

/* Macros for Hardware Audit Np Sync */
#define ISS_NPSYNC_BLK() \
                (gIssGlobalInfo.IssRedInfo.u1NpSyncBlockCount)

/* Data Structure for NP-Sync Buffer Table */
typedef tNpSyncBufferEntry tIssRedNpSyncEntry;

typedef struct
{
   /* Memory Pool for creation of queue messages for ISS redundancy*/
   tMemPoolId           IssRedQueuePoolId;
   /* Memory Pool for creation of NpSync-up nodes for Iss h/w audit. */
   tMemPoolId           HwAuditTablePoolId;
   /* Queue Identifier for Port Isolation redundancy. */
   tOsixQId             IssRedQueId;
   /* SLL header of Iss Hardware Audit  Np-Sync buffer table. */ 
   tTMO_SLL             HwAuditTable; 
   /* Node Status - Idle/Standby/Active */
   UINT1                u1NodeStatus;
   /* Counter to Block the NP Sync Message from Active to Standby Node.*/
   UINT1                u1NpSyncBlockCount; 
   /* Reserved variable. Currently not in use.*/
   UINT1                au1Reserved[2];
}tIssRedInfo;

typedef struct{
   /* Pointer to received message info from RM */
   tRmMsg               *pData;
   /* Length of the message received from RM */
   UINT2                u2DataLen;
   /* Event received from RM */
   UINT1                u1Event;
   /* Padding bytes */
   UINT1                u1Reserved;
}tIssRmMsg;

typedef struct{
   /* Message Type of the Queue  */
   UINT4                u4MsgType;
   /* Structure to store the RM Message and
    * Events from the RM module */
   tIssRmMsg          IssRmMsg;
}tIssRedQMsg;

/*Prototype declarations for Port Isolation Redundancy support. */

INT4 IssRedRegisterWithRM PROTO ((VOID));

INT4
IssRedHandleUpdateEvents PROTO((UINT1, tRmMsg *, UINT2));

VOID
IssRedProcessQMsgEvent PROTO((VOID));

INT4
IssRedQueEnqMsg PROTO((tIssRedQMsg *));

VOID
IssRedHandleRmEvents PROTO((tIssRedQMsg *));

VOID
IssRedInitHardwareAudit PROTO((VOID));

VOID IssRedHandleGoActive PROTO((VOID));

VOID
IssRedHandleGoStandby PROTO((VOID));

VOID
IssRedUpdateNpSyncBufferTable PROTO((unNpSync *,UINT4 ,UINT4));

VOID
IssProcessNpSyncMsg PROTO((tRmMsg *, UINT2 *));
VOID
IssRedProcessPeerMsgAtStandby PROTO((tRmMsg *, UINT2));
VOID
IssPIRedHandleHwAudit PROTO((tIssHwUpdtPortIsolation *));
VOID
IssRedHandleStandbyToActive PROTO((VOID));
VOID
IssRedHandleActiveToStandby PROTO((VOID));
VOID
IssRedHandleIdleToStandby PROTO((VOID));
VOID
IssRedHwAuditIncBlkCounter PROTO((VOID));
VOID
IssRedHwAuditDecBlkCounter PROTO((VOID));
VOID
IssRedHandleIdleToActive PROTO((VOID));
VOID
IssRedInitNpSyncBufferTable PROTO((VOID));
VOID IssRedHandleDynSyncAudit (VOID);
VOID IssExecuteCmdAndCalculateChkSum  (VOID);
#endif /* ISSRED_H */

