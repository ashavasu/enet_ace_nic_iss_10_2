/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : issinc.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*    DESCRIPTION           : This file contains all the include files for   */
/*                            ISS Module.                                    */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#ifndef _ISSWINC_H
#define _ISSWINC_H

/*Kernel Includes */
#include <asm/uaccess.h>        /* for copy_to_user and copy_from_user */

/* Common Includes */
#include "lr.h"
#include "iss.h"
#include "ip.h"
#include "fssnmp.h"
#include "cli.h"

/* ISS includes */
#include "nmhtdfs.h"
#include "isswdefs.h"
#include "issipmgr.h"
#include "chrdev.h"

#endif /* _ISSWINC_H */
