/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/*****************************************************************************/
/*    FILE  NAME            : isspiinc.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Port Isolation Feature                         */
/*    MODULE NAME           : ISS                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Linux                                          */
/*    DATE OF FIRST RELEASE : 21 July 2010                                   */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           :  This header file contains                     */
/*                             (i) data structure definition,                */
/*                             (ii) Macros definition,                       */
/*                             (iii) prototype declarations,                 */
/*                             (iv) extern declarations                      */
/*                             required for Port Isolation feature.          */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*$Id: isspiinc.h,v 1.5 2011/10/25 11:17:50 siva Exp $                       */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    21 July 2010           Initial Creation                        */
/*---------------------------------------------------------------------------*/

#ifndef ISSPIINC_H
#define ISSPIINC_H

#include "fsvlan.h"
#include "isspi.h"
#include "cli.h"

/* Macros to identify the different values returned by the
 * RB Tree Compare function for Port Isolation table.
 */
#define ISS_PI_RB_GREATER         1
#define ISS_PI_RB_EQUAL           0
#define ISS_PI_RB_LESS           -1


/* Port Isolation Storage type values */
#define ISS_PI_VOLATILE     2   /* Object is not restored on reboot */
#define ISS_PI_NON_VOLATILE 3   /* Object restored on reboot */

#define ISS_PI_INVALID_ID         0

typedef struct 
{
   /* Memory Pool for Port Isolation entry node creation. */
   tMemPoolId          IssPIEntryCtrlPoolId;
   /* Memory Pool for creation of Egress Port List Array */
   tMemPoolId          IssPIEgressPortPoolId;
   /* RB Tree for Port Isolation Table. */
   tRBTree             PortIsolationTable; 
                                          
}tIssPIGlobalInfo;

/* Data structure for Port Isolation Feature 
 * For each ingress port of the Port isolation table, a RB tree node will be 
 * maintained and the index will be (IfIndex, InVlanId) pair.  
 */

typedef struct _PortIsolationEntry
{
   tRBNodeEmbd    PortIsolationNode;     /* Node for Port Isolation */
   UINT4          u4IngressIfIndex;      /* Ingress Interface index */
   UINT4          *pu4EgressPorts;       /* List of ports to which egress 
                                          * traffic is allowed. Egress ports
                                          * are maintained in ascending order in 
                                          * this array. */
   tVlanId        InVlanId;              /* Ingress Vlan Id */
   UINT1          u1StorageClass;        /* Storage type of the Port Isolation
                                          * entry */
   UINT1          u1Reserved;            /* Reserved variable. Currently not
                                          * in use */
}tIssPortIsolationEntry;

/***************** Port Isolation Access Routines*********************/

tIssPortIsolationEntry *  
IssPIInitPortIsolationEntry PROTO ((UINT4, tVlanId));
INT4 
IssPIUpdtPortIsolationEntry PROTO ((tIssUpdtPortIsolation *));
tIssPortIsolationEntry * 
IssPIGetPortIsolationNode PROTO ((UINT4, tVlanId));
VOID 
IssPIDeletePortIsolationNode PROTO ((tIssPortIsolationEntry *));
VOID 
IssPIDeletePortIsolationTable PROTO ((VOID));
INT1
IssPIGetFirstPortIsolationNode PROTO ((UINT4 *,UINT4 *, UINT4 *));
INT1
IssPIGetNextPortIsolationNode PROTO ((UINT4 ,
                               tVlanId, 
                               UINT4 ,
                               UINT4 *,
                               tVlanId *, 
                               UINT4 *));
INT1 
IssPIGetPortIsolationEntry PROTO ((UINT4 , tVlanId, UINT4));
INT1 
IssPISetPortIsolationStorageType PROTO ((UINT4 , tVlanId,UINT4));
INT1 
IssPIGetPortIsolationStorageType PROTO ((UINT4 , tVlanId, UINT1 *));
INT1
IssPIAddPIEntryEgressPorts PROTO ((UINT4 *, UINT4));
INT1 
IssPIDelPIEntryEgressPorts PROTO ((UINT4 *, UINT4, INT1 * ));
INT4
IssPIUtilPITblCmpFn PROTO ((tRBElem *, tRBElem *));
INT4
IssPIUtilFreePIEntry PROTO((tRBElem *, UINT4 ));

/****************Port Isolation MBSM routines***********************/
VOID 
IssPIMbsmAddPortIsolationNode PROTO ((tIssPortIsolationEntry *, 
                                    tMbsmSlotInfo *));
VOID 
IssPIMbsmDelPortIsolationEntry PROTO ((tIssPortIsolationEntry *, 
                                        UINT4, 
                                        tMbsmSlotInfo *));
VOID 
IssPIMbsmAddPortIsolationEntry PROTO ((tIssPortIsolationEntry *, 
                                     UINT4, 
                                     tMbsmSlotInfo *));
INT4
IssPIConvertStrToPortArray (tCliHandle, UINT1 *, UINT4 *, UINT4*,
                            UINT4 , UINT4);

INT1
IssPIMbsmCheckPortRange PROTO ((UINT4, UINT4, UINT4));
#endif /* ISSPIINC_H */

