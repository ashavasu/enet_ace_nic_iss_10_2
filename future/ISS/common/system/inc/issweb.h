/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/* $Id: issweb.h,v 1.5 2014/01/30 12:22:57 siva Exp $ */
/*****************************************************************************/
/*    FILE  NAME            : issweb.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*    DESCRIPTION           : This file contains defines for webconfig.      */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/** ISSWEB.H ***/

#ifndef __ISSWEB_H__
#define __ISSWEB_H__ 


#define ISS_AUTO_CONF                         0 
#define ISS_WIZARD_CONF                       1
#define ISS_INTERFACE_CONF                    2
#define ISS_CUSTOMER_CONF                     3
#define ISS_STATUS                            6

#define ISSWEB_WZBRIDGE_LA_INDEX              0
#define ISSWEB_WZBRIDGE_LA_PARAMS_NUM         2 
#define ISSWEB_WZBRIDGE_PNAC_INDEX            2 
#define ISSWEB_WZBRIDGE_PNAC_PARAMS_NUM       3 
#define ISSWEB_WZBRIDGE_VLAN_INDEX            5 
#define ISSWEB_WZBRIDGE_VLAN_PARAMS_NUM       3
#define ISSWEB_MAX_NAME_LENGTH                200 
#define ISS_URL_ERROR                        -1
#define ISSWEB_MAX_STRING_LENGTH              100 
#define ISSWEB_MAX_PAIR_ELEMENTS              200
#define ISSWEB_MAX_NUM_ELEMENTS               10
#define ISSWEB_MAX_LENGTH                     25

#define INTF_FAILURE                         -1 

#define INT_TYPE                              ISS_UP
#define IPADDR_TYPE                           ISS_DOWN

/* Bridge */
#define ISS_BRIDGE_UP                       ISS_UP
#define ISS_BRIDGE_WITHOUTCRC               2 
#define ISS_BRIDGE_DOWN                       ISS_DOWN

/* STAP */
#define ISS_STAP_UP                           ISS_UP 
#define ISS_STAP_DOWN                       ISS_DOWN

/* LA */ 
#define ISS_LA_UP                       ISS_UP
#define ISS_LA_DOWN                       ISS_DOWN 

/* PNAC */ 
#define ISS_PNAC_UP                       ISS_UP
#define ISS_PNAC_DOWN                       ISS_DOWN
#define ISS_PNAC_AUTH_REMOTE               1

/* VLAN */
#define ISS_VLAN_PORTBASED               1
#define ISS_VLAN_MACBASED               2
#define ISS_VLAN_INDEP_LEARNING        1
#define ISS_VLAN_SHARED_LEARNING       2
#define ISS_VLAN_UP                       ISS_UP
#define ISS_VLAN_DOWN                       ISS_DOWN

/* GMRP */
#define ISS_GMRP_UP                       ISS_UP
#define ISS_GMRP_DOWN                       ISS_DOWN

/* IGS */ 
#define ISS_IGS_UP                       ISS_UP
#define ISS_IGS_DOWN                       ISS_DOWN

/* GVRP */
#define ISS_GVRP_UP                       ISS_UP
#define ISS_GVRP_DOWN                       ISS_DOWN 

/* GARP */
#define ISS_GARP_START                        ISS_DOWN
#define ISS_GARP_SHUTDOWN                     ISS_UP 

/* IP */
#define ISS_IP_FWD_DISABLE                    IP_FORW_DISABLE

#define ISS_INV_IPADDR                        (0xffffffff)
/* Page types associated with interface wizard */

#define ISS_INTF_ETHERNET_CREATE              1
#define ISS_INTF_ETHERNET_MODIFY              2
#define ISS_INTF_CONFIGURE                    11
#define ISS_INTF_DUMMY                        12
#define ISS_INTF_DISPLAY                      13

/* Constants for wizard interface */ 
#define ISS_INTF_MAX_LEN                      200 

/* Included for system sizing */
#define SIZ_MAX_LEN                           50

/* SNMP Related defines. */
# define NULL_STRING                          "\0" 
# define MAX_OCTETLEN                         255 
# define ADDR_LEN                             4 
# define INTEGER_LEN                          1 

#endif  /* __ISSWEB_H__  */

