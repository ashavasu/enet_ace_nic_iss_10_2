/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: isstdfs.h,v 1.57 2017/09/20 11:39:16 siva Exp $
 *
 * Description : This file contains typedefs for the ISS Module. 
 *                         
 *****************************************************************************/

/*****************************************************************************/
/*    FILE  NAME            : isstdfs.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#ifndef _ISSTDFS_H
#define _ISSTDFS_H

#include "issdefs.h"
#ifdef L2RED_WANTED
#include "issred.h"
#endif /* L2RED_WANTED */

/*Datastructures used*/
typedef tMemPoolId          tIssMemPoolId;
typedef tTMO_SLL            tIssSll;


/***********************************************************
*Data Structure related to MSR module -ISS CONFIG SAVE     *
************************************************************/

typedef tIssBool tInitiate;

typedef struct Sftp_Info{
    UINT1 au1UserName[ISS_CONFIG_USER_NAME_LEN+4];
    UINT1 au1Passwd[ISS_CONFIG_USER_NAME_LEN+4];
}tSftp_Info;
typedef struct ConfigSaveInfo {

   UINT1                 au1IssConfigSaveFileName[ISS_CONFIG_FILE_NAME_LEN+4]; 
   UINT1                 au1IssIncrSaveFileName[ISS_CONFIG_FILE_NAME_LEN+4]; 
   tSftp_Info            IssSftpConfigSaveInfo;
   tIPvXAddr             IssConfigSaveIpAddr;
   tIssConfigSaveOption  IssConfigSaveOption;              
   tInitiate             IssInitiateConfigSave;
   tInitiate             IssAutoConfigSave;  
   tIssBool              IssIncrSaveFlag; 
   tIssBool              IssRollbackFlag;
   INT4                  i4IssConfigSyncUp;
   UINT4                 u4IssConfigSaveTransferMode;
   tIssBool              IssDefValSaveFlag;
}tConfigSaveInfo;


/***********************************************************
*Data Structure related to MSR module -ISS CONFIG RESTORE  *
************************************************************/

typedef struct ConfigRestoreInfo {

   UINT1                   au1IssConfigRestoreFileName[ISS_CONFIG_FILE_NAME_LEN];
   tIPvXAddr               IssConfigRestoreIpAddr;
   tIssConfigRestoreOption IssConfigRestoreOption;
   tInitiate               IssInitiateConfigRestore;
}tConfigRestoreInfo;

/* Image Download Info for the switch */
typedef struct ImageDlInfo {

   tIPvXAddr               IssDlImageFromIp;
   tInitiate               IssInitiateDlImage;
   UINT1                   au1IssDlImageName[ISS_CONFIG_FILE_NAME_LEN];
   tSftp_Info              IssSftpDlImageInfo;
   UINT4                   u4IssDlTransferMode;
   UINT4                   u4DownloadStatus; /* Indicates the download status */

}tIssDlImageInfo;

/* Log File Upload Info structure */
typedef struct logFileUlInfo {

   tSftp_Info              IssSftpUlLogInfo;
   tIPvXAddr               IssUlLogFileFromIp;
   tInitiate               IssInitiateUlLogFile;
   UINT1                   au1IssUlLogFileName[ISS_CONFIG_FILE_NAME_LEN+4];
   UINT1                   au1IssUlLogRemoteFileName[ISS_CONFIG_FILE_NAME_LEN+4];
   UINT4                   u4IssUlTransferMode;
   UINT1                   *pu1LogBuf;
   UINT4                   u4LogSize;

}tIsslogFileUlInfo;

/* Data Structure related to System Restart */
typedef tIssBool tIssRestart;

/* Validity Value */
typedef enum {

   ISS_VALID = 1,
   ISS_INVALID

}tValidity;



/* port control action */
typedef enum {

   ISS_DROPPKT = 1,
   ISS_PURGELRU

}tIssPortCtrlAction;
    
/***********************************************************
*Data Structure related to PORT CONTROL GROUP              *
************************************************************/

typedef struct IssConfigCtrlEntry {

  tIssStatus            IssConfigCtrlEgressStatus;
  tIssStatus            IssConfigCtrlStatsCollection;
  tValidity             IssConfigCtrlStatus;

}tIssConfigCtrlEntry;

typedef struct IssPortCtrlEntry {

  tIssPortCtrlMode      IssPortCtrlMode;
  tIssPortCtrlDuplex    IssPortCtrlDuplex;
  tIssPortCtrlSpeed     IssPortCtrlSpeed;
  tIssPortCtrlAction    IssPortCtrlMaxMacAction;
  INT4                  i4IssPortCtrlMaxMacAddr;
  INT4                  i4IssPortCtrlANCapBits;
  tIssBool              IssPortCtrlRenegotiate;
  tIssStatus            IssPortCtrlFlowControl;
  tIssPortHolStatus     IssPortHOLBlockPrevention;
  tIssPortCpuCntrlLearning IssPortCpuCntrlLearning;
  UINT1                 au1Uris[ISS_ENT_PHY_URIS_LEN+1];
  tIssPortCtrlMdiOrMdixCap      IssPortCtrlMdiOrMdixCap;
  INT4                  i4IssPortCtrlFlowCtrlMinRate;
  INT4                  i4IssPortCtrlFlowCtrlMaxRate;
}tIssPortCtrlEntry;

/***********************************************************
*Data Structure related to MIRROR GROUP                    *
************************************************************/
typedef struct tSessionInfo
{
   tTMO_SLL DestRecrdHeader;   /*Header for Destination Records link list.*/
   tTMO_SLL SrcRecrdHeader;    /*Header for Source Records link list. */
   UINT4    u4RSpanContextId;
   UINT2    RSpanVlanId;   /*Remote SPAN VLAN ID*/
   UINT1    u1RSpanSessionType; /*RSPAN Session type . Source/Destination*/
   UINT1    u1MirroringType; /* Port/VLAN/MAC/Flow based*/
   UINT1    u1SessionStatus;/*ACTIVE Status for the session*/
   UINT1    au1Pad[3];
}tIssMirrSessionInfo;

typedef struct tDestRecordInfo
{
   tTMO_SLL_NODE NextRecordInfo;    /*Information of next Record in the list*/
   UINT4    u4DestMaxId;                        /* Maximum value of Destination Id in the record*/
   UINT4    u4DestMinId;                         /* Minimum value of Destination Id in the record*/
}tIssMirrDestRecordInfo;

typedef struct tSrcRecordInfo
{
   tTMO_SLL_NODE NextRecordInfo;    /*Information of next Record in the list*/
   UINT4    u4SrcMaxId;                          /* Maximum value of Source Id in the record*/
   UINT4    u4SrcMinId;                           /* Minimum value of Source Id in the record*/
   UINT4    u4ContextId;                          /*Context Id used in case sources are VLAN Ids*/
   UINT1    u1SessionMode;                    /* Can be Ingress/Egress/Both*/
   UINT1    u1ControlStatus;                    /* Valid/Invalid, Added to support Old Mirroring MIB Objects*/
   UINT1    au1Pad[2];
}tIssMirrSrcRecordInfo;


/***********************************************************
* Data Structure related Available ISS Callbacks           *
************************************************************/

typedef INT4 (*tIssAppSysInitFn)(VOID);

typedef INT4 (*tWssReadNvRamFn)(VOID);

typedef INT4 (*tIssCustFirmUpgradeFn)(CONST UINT1* ,UINT1*);

typedef INT4 (*tIssHostNmeUpdtFn) (UINT1 *);
typedef struct IssCallbackEntry {

    union IssAppCbFn {
        tIssAppSysInitFn  pIssAppSysInitFn;
 tIssCustFirmUpgradeFn pIssCustFirmUpgradeFn;
        tWssReadNvRamFn       pWssReadNvRamFn;
        tIssHostNmeUpdtFn pIssHostNmeUpdtFn;
    }uIssAppCbFn;

} tIssCallbackEntry;

typedef struct IssSystTrapInfo
{
    tIssSwitchThreshold     ThresholdTrap;
    tIssSwitchInfo          InfoTrap;
    tIssSwitchFanState      FanStatusTrap;
}tIssSystTrapInfo;

/* Iss Global Info */
typedef struct { 

   tIssConfigCtrlEntry    *apIssConfigCtrlEntry[MAX_ISS_CONFIG_ENTRIES_LIMIT];
   tIssPortCtrlEntry      *apIssPortCtrlEntry[MAX_ISS_PORT_ENTRIES_LIMIT];
   tIssMirrSessionInfo    SessionInfo[ISS_MIRR_MAX_SESSIONS]; /*Stores information of 
                                                                Mirroring Session*/
   tIssMemPoolId          IssConfigCtrlPoolId;
   tIssMemPoolId          IssPortCtrlPoolId;
   tIssMemPoolId          IssMirrDestRecordPoolId;
   tIssMemPoolId          IssMirrSrcRecordPoolId;
   tIssMemPoolId          IssL4SFilterPoolId;
   tIssMemPoolId          IssEntPhyPoolId;
#ifdef L2RED_WANTED
   tIssRedInfo            IssRedInfo;
#endif /* L2RED_WANTED */
   UINT1                  au1IssNpapiMode[ISS_NPAPI_MODE_LEN+1];
   UINT1                  au1IssRmStackInterface[ISS_RM_INTERFACE_LEN];
   UINT4                  u4IssTraceOption;
   UINT4                  u4IssCliSerialConsole;
   UINT4                  u4IssSwitchId;
   UINT4                  u4IssConfigState; 
   UINT4                  u4IssRmIpAddress;
   UINT4                  u4IssRmSubnetMask;
   UINT4                  u4IssModuleCsrRestoreFlag;
   UINT4                  u4IssCpuMirrorToPort;
   INT4                   i4IssCpuMirrorType;
   tIssSll                IssL4SFilterListHead;
   UINT1                  au1IssModuleSystemControl[MAX_PROTOCOLS+3];
   UINT1                  au1IssModuleCsrRestoreFlag[MAX_PROTOCOLS+3];
   tTimerListId           IssTmrListId;
   tOsixTaskId            IssSystMonitorTaskId;
   tTimerListId           IssMacLearnRateTmrListId;
   UINT1                  u1MirrorStatus;   /*Stores mirroring global status*/
   UINT1                  u1MirrAccessMode; /*Indicates which MIB objects are beed used to configure Mirroring tables
                                             Configuration through issMirrorCtrlTable 
                                             objects is considered as Compatibility mode 
                                             Configuration through issMirrorCtrlExtnTable 
                                             objects is considered as Extended mode */
   UINT1                  u1IssHealthChkStatus;
   UINT1                  u1IssHealthChkErrorReason;
   UINT4                  u4IssHealthChkMemAllocErrPoolId;
   UINT4                  u4LocalSecIpAddr; /* Secondary IP Address */
   UINT4                  u4LocalSecSubnetMask; /* Secondary Mask */
   tIp6Addr               localSecIp6Addr; /* Secondar Ipv6 address */
   UINT1                  u1SecPrefixLen; /* Secondary prefix length */
   UINT1                  u1IssHealthChkConfigRestoreStatus;
   UINT1                  u1IssHealthChkClearCtr;
   UINT1                  au1Pad;
}tIssGlobalInfo;

typedef struct IssSysCapSupport
{
    UINT1 u1TempStatusSupport; /* Is Temperature Status Supporetd */
    UINT1 u1FanStatusSupport;  /* Is Fan Status Supporetd */
    UINT1 u1RAMStatusSupport;  /* Is Ram Status Supporetd */
    UINT1 u1CPUStatusSupport;  /* Is CPU Status Supporetd */
    UINT1 u1FlashStatusSupport;/* Is Flash Status Supporetd */
    UINT1 u1PowerStatusSupport;/* Is Power Status Supporetd */
    UINT1 au1Pad[2];
}tIssSysCapSupport;

typedef struct _LaParams {

   UINT1  u1LaSystemControl;
   UINT1  u1LaStatus;
   UINT1  u1LaPortNo;
   UINT1  u1Dummy;

} tLaParams;

typedef struct PNACParams {

   UINT2  u1PnacAuthMode;
   UINT1  u1PnacSystemControl;
   UINT1  u1PnacStatus;
   UINT1  u1PnacAuthentic;
   UINT1  u1PnacPortNo;
   UINT1  u1Pad[2];
} tPnacParams;

typedef struct VlanParams {

   UINT1 u1VlanStatus;
   UINT1 u1VlanType;
   UINT1 u1VlanLearningType;
   UINT1 u1GvrpStatus;
   INT1 u1GmrpStatus;
   UINT1 u1Pad[3];

} tVlanParams;


typedef struct BridgeParams {
 UINT4 u4StpPriority;       
 UINT1 u1BaseBridgeStatus;
 UINT1 u1BaseBridgeCRCStatus;
 UINT1 u1StpStatus;          
 UINT1 u1IgmpStatus;      
} tBridgeParams;


typedef struct ConfigBridge {

 tBridgeParams BridgeParams;
 tLaParams LaParams;
 tPnacParams PnacParams;  
 tVlanParams VlanParams;

} tConfigBridge;


typedef struct DhcpParams {

 UINT1   u1DhcpSrvEnable;
 UINT1 u1Dummy;
 UINT2 u2Dummy;
 UINT4   u4DhcpStartIpAddr;
 UINT4   u4DhcpEndIpAddr;

} tDhcpParams;

typedef struct DnsParams {

   UINT4 u4DnsRelayEnable;
   UINT4 u4DnsServerAddr;
   UINT4 u4DnsServerEnable;

}tDnsParams;


typedef struct {

   INT1    i1Pname[MAX_OPTION_STRING];
   INT1    i1Pvalue[MAX_OPTION_VALUE];

}tPair;
typedef struct {
    UINT1    au1Descr[ISS_ENT_PHY_DESCR_LEN+1];
    UINT1    au1VendorType[ISS_ENT_PHY_TYPE_LEN+1];
    UINT1    au1Name[ISS_ENT_PHY_NAME_LEN+1];
    UINT1    au1HardwareRev[ISS_ENT_PHY_HW_REV_LEN+1];
    UINT1    au1FirmwareRev[ISS_ENT_PHY_FW_REV_LEN+1];
    UINT1    au1SoftwareRev[ISS_ENT_PHY_SW_REV_LEN+1];
    UINT1    au1MfgName[ISS_ENT_PHY_MFG_NAME_LEN+1];
    UINT1    au1ModelName[ISS_ENT_PHY_MODEL_NAME_LEN+1];
    UINT1    au1MfgDate[ISS_ENT_PHY_MFG_DATE_LEN+1];
    UINT1    au1Uris[ISS_ENT_PHY_URIS_LEN+1];
    UINT1    au1SerialNum[ISS_ENT_PHY_SER_NUM_LEN];
    UINT1    au1Alias[ISS_ENT_PHY_ALIAS_LEN];
    UINT1    au1AssetId[ISS_ENT_PHY_ASSET_ID_LEN];
    INT4     i4ParentRelPos;
    INT4     i4ContainedIn;
    INT1     i1PhyClass;
    BOOL1    b1IsFRU;
    UINT2    u2Padding;
} tIssEntPhyInfo;


typedef struct {
    UINT4 u4IssuMode;       /*Issu Mode*/
    UINT4 u4LastUpgradedTime; /*ISSU last upgraded time*/ 
   /* Roll back software Path */
    UINT1 au1RollBackSwPath[ISS_ISSU_MAX_PATH_LEN+1];
   /* Current Software Path */
    UINT1 au1CurrentSwPath[ISS_ISSU_MAX_PATH_LEN+1];  
    /* Roll back software version */
    UINT1 au1RollBackIssVersion[ISS_ISSU_IMAGE_NAME_MAX_SIZE+1]; 
    UINT1   u1StartUpMode; /*State in which image will boot up ISSU/NON-ISSU*/
    UINT1   u1NodeRole;   /*Node Role while boot up */
    UINT1 au1Pad[3];
}tIssuStartupInfo;

/***********************************************************
* Data Structure for MAC Learn Limit Rate Feature          *
************************************************************/
typedef struct _tIssSwitchMacLearnLimitRate {
    INT4      i4IssSwitchMacPresentCount;  /* Stores Current MAC count */
    INT4      i4IssSwitchMacMaxLearnLimit; /* Stores the MAX Learn limit 
                                              of ISS */
    INT4      i4IssSwitchMacLearnLimitRate; /* Stores the Learn limit rate set
                                               in Scalar object issMacLearnRateLimit*/
                                               
    UINT4     u4IssSwitchMacLearnRateLimitInterval; /* Stores the Learn limit 
                                                       rate Interval set in scalar object 
                                                       issMacLearnRateLimitInterval */
    tTmrAppTimer  IssMacLearnRateLimitTimer; /* Pointer to store the 
                                                MAC Learn Rate Limit timer*/
} tIssSwitchMacLearnLimitRate;


typedef struct _tIssFirmwareBlock
{
    UINT1    au1IssFirmwareBlock[ISS_FIRMWARE_MEMBLK_SIZE];
}tIssFirmwareBlock;

typedef struct _tIssPIEgressPortBlock
{
    UINT4   au4IssPIEgressPortBlock[ISS_MAX_UPLINK_PORTS];
}tIssPIEgressPortBlock;

typedef struct _tIssHwCapabilities
{
    UINT1              u1HwKnetIntf;
    UINT1              u1HwLaWithDiffPortSpeed;
    UINT1              u1HwUntaggedPortsForVlans;
    UINT1              u1HwUnicastMacLearningLimit;
    UINT1              u1HwQueueConfigOnLaPort;
    UINT1              u1HwLagOnAllPorts;  
    UINT1              u1HwLagOnCep;  
    UINT1              u1HwSelfMacInMacTable;  
    UINT1              u1HwMoreScheduler; 
    UINT1              u1HwDwrrSupport;
    UINT1              u1HwHighCapacityCntr;
    UINT1              u1HwCustomerTpidAllow;
    UINT1              u1ShapeParamEIR;
    UINT1              u1ShapeParamEBS;
    UINT1              u1HwMeterColorBlind;
    UINT1              u1HwUntagCepPep;
    UINT1              u1HwQoSClassToPolicerAndQ;
    UINT1              u1Pad[3];
}tIssHwCapabilities;


#endif /* _ISSTDFS_H */
