  /*************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: l4scmd.def,v 1.4 2013/09/27 07:24:54 siva Exp $
****************************************************************************/ 
/********** Layer 4 Switching COMMANDS ********************/

DEFINE GROUP: L4S_GLBCFG_GRP

#if defined (NPAPI_WANTED)

   COMMAND : layer4 switch <short (1-20)>  protocol { any | tcp | udp | <short (1-255)>} port { any | <short (1-65535)>} [interface <ifXtype> <ifnum>]

   ACTION :
   {
	UINT4 u4Protocol = 0;
	INT4 i4L4Port = 0;
	UINT4 u4PortId =0;
	INT4 i4FilterNo = 1;

	if( $2 != NULL)
	{
		i4FilterNo = *(INT4 *)$2;
	}
	if ($4 != NULL)
	{
		u4Protocol = 0;
	}
	else if ($5 != NULL)
	{
		u4Protocol = ISS_PROT_TCP;
	}
	else if ($6 != NULL)
	{
		u4Protocol = ISS_PROT_UDP;
	}
	else if ($7 != NULL)
	{
		u4Protocol = *(INT4 *)$7;
	}
	
	if ($9 != NULL)
	{
		i4L4Port = 0;
	}
	else if ($10 != NULL)
	{
		i4L4Port = *(INT4 *)$10;
	}
	
	if (CfaCliGetIfIndex ($12, $13, &u4PortId) == CLI_FAILURE)
        {
        	CliPrintf (CliHandle, "%% Invalid Interface \r\n");
        	return (CLI_FAILURE);
        }

	cli_process_l4s_command(CliHandle, CLI_L4S_ADD,NULL,i4FilterNo,u4Protocol,i4L4Port,u4PortId);
   }
	
   SYNTAX : layer4 switch <FilterNo (1-20)>  protocol { any | tcp | udp | <Protocol No (1-255)>} port { any | <PortNo (1-65535)>} interface { <interface type> <interface id> } 
   PRVID   : 15
   HELP   : Adds Layer 4 Switching entry in to the Table
   CXT_HELP : layer4 Configures layer 4 related details | 
              switch Switch related configuration | 
              (1-20) Filter number | 
              protocol Protocol related configuration | 
              any All kind of protocols | 
              tcp Transmission control protocol | 
              udp User datagram protocol | 
              (1-255) Number representing other protocols | 
              port Port related configuration | 
              any Any port | 
              (1-65535) Port number | 
              interface Interface related configuration | 
              (gigabitethernet/fastethernet/extreme-ethernet/port-channel/tunnel/vlan) Interface type | 
              <ifnum> Interface number (Example: 0/1) or port channel / tunnel / VLAN ID | 
              <CR> Adds Layer 4 Switching entry in to the Table


   COMMAND : no layer4 switch <short (1-20)>

   ACTION  :
   {
	INT4 i4FilterNo = 1;

	if( $3 != NULL)
	{
		i4FilterNo = *(INT4 *)$3;
	}

	cli_process_l4s_command(CliHandle, CLI_L4S_DELETE, NULL ,i4FilterNo);
   }	
	
   SYNTAX  : no layer4 switch <FilterNo (1-20)>
   PRVID   : 15
   HELP    : Deletes Layer 4 Switch entry in the table
   CXT_HELP : no Disables the configuration / deletes the entry / resets to default value | 
              layer4 Layer 4 related configuration | 
              switch Switch related configuration | 
              (1-20) Filter number | 
              <CR> Deletes Layer 4 Switch entry in the table

#endif

END GROUP

DEFINE GROUP: L4S_USEREXEC_CMDS

#if defined (NPAPI_WANTED)

   COMMAND : show layer4 switch { all | <short (1-20)> }

   ACTION  :
   {
	INT4 i4FilterNo = 1;
        
	if( $3 != NULL)
	{
		i4FilterNo = 0;
	}
	if( $4 != NULL)
	{
		i4FilterNo = *(INT4 *)$4;
	}

	cli_process_l4s_command(CliHandle, CLI_L4S_SHOW, NULL,i4FilterNo);
   }	
	
   SYNTAX  : show layer4 switch { all | <FilterNo (1-20)> }
   PRVID   : 1
   HELP    : Shows Layer 4 Switch entry in the table
   CXT_HELP : show Displays the configuration / statistics / general information | 
              layer4 Layer 4 related configuration | 
              switch Switch related configuration | 
              all All configurations | 
              (1-20) Filter number | 
              <CR> Shows Layer 4 Switch entry in the table
#endif

END GROUP
