/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsissdb.h,v 1.70 2017/12/07 10:07:31 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSISSDB_H
#define _FSISSDB_H

UINT1 IssConfigCtrlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 IssPortCtrlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 IssPortIsolationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 IssMirrorCtrlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 IssMirrorCtrlExtnTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 IssMirrorCtrlExtnSrcTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 IssMirrorCtrlExtnSrcVlanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 IssMirrorCtrlExtnDestinationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 IssIpAuthMgrTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 IssRateCtrlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 IssL2FilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 IssL3FilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 IssL4SwitchingFilterTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 IssModuleTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 IssSwitchFanTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fsiss [] ={1,3,6,1,4,1,2076,81};
tSNMP_OID_TYPE fsissOID = {8, fsiss};


/* Generated OID's for tables */
UINT4 IssConfigCtrlTable [] ={1,3,6,1,4,1,2076,81,2,1};
tSNMP_OID_TYPE IssConfigCtrlTableOID = {10, IssConfigCtrlTable};


UINT4 IssPortCtrlTable [] ={1,3,6,1,4,1,2076,81,2,2};
tSNMP_OID_TYPE IssPortCtrlTableOID = {10, IssPortCtrlTable};


UINT4 IssPortIsolationTable [] ={1,3,6,1,4,1,2076,81,2,3};
tSNMP_OID_TYPE IssPortIsolationTableOID = {10, IssPortIsolationTable};


UINT4 IssMirrorCtrlTable [] ={1,3,6,1,4,1,2076,81,3,3};
tSNMP_OID_TYPE IssMirrorCtrlTableOID = {10, IssMirrorCtrlTable};


UINT4 IssMirrorCtrlExtnTable [] ={1,3,6,1,4,1,2076,81,3,6};
tSNMP_OID_TYPE IssMirrorCtrlExtnTableOID = {10, IssMirrorCtrlExtnTable};


UINT4 IssMirrorCtrlExtnSrcTable [] ={1,3,6,1,4,1,2076,81,3,7};
tSNMP_OID_TYPE IssMirrorCtrlExtnSrcTableOID = {10, IssMirrorCtrlExtnSrcTable};


UINT4 IssMirrorCtrlExtnSrcVlanTable [] ={1,3,6,1,4,1,2076,81,3,8};
tSNMP_OID_TYPE IssMirrorCtrlExtnSrcVlanTableOID = {10, IssMirrorCtrlExtnSrcVlanTable};


UINT4 IssMirrorCtrlExtnDestinationTable [] ={1,3,6,1,4,1,2076,81,3,9};
tSNMP_OID_TYPE IssMirrorCtrlExtnDestinationTableOID = {10, IssMirrorCtrlExtnDestinationTable};


UINT4 IssIpAuthMgrTable [] ={1,3,6,1,4,1,2076,81,7,1};
tSNMP_OID_TYPE IssIpAuthMgrTableOID = {10, IssIpAuthMgrTable};


UINT4 IssRateCtrlTable [] ={1,3,6,1,4,1,2076,81,4,1};
tSNMP_OID_TYPE IssRateCtrlTableOID = {10, IssRateCtrlTable};


UINT4 IssL2FilterTable [] ={1,3,6,1,4,1,2076,81,5,1};
tSNMP_OID_TYPE IssL2FilterTableOID = {10, IssL2FilterTable};


UINT4 IssL3FilterTable [] ={1,3,6,1,4,1,2076,81,6,1};
tSNMP_OID_TYPE IssL3FilterTableOID = {10, IssL3FilterTable};


UINT4 IssL4SwitchingFilterTable [] ={1,3,6,1,4,1,2076,81,9,1};
tSNMP_OID_TYPE IssL4SwitchingFilterTableOID = {10, IssL4SwitchingFilterTable};


UINT4 IssModuleTable [] ={1,3,6,1,4,1,2076,81,12,1};
tSNMP_OID_TYPE IssModuleTableOID = {10, IssModuleTable};


UINT4 IssSwitchFanTable [] ={1,3,6,1,4,1,2076,81,13,1};
tSNMP_OID_TYPE IssSwitchFanTableOID = {10, IssSwitchFanTable};




UINT4 IssSwitchName [ ] ={1,3,6,1,4,1,2076,81,1,1};
UINT4 IssHardwareVersion [ ] ={1,3,6,1,4,1,2076,81,1,2};
UINT4 IssFirmwareVersion [ ] ={1,3,6,1,4,1,2076,81,1,3};
UINT4 IssDefaultIpAddrCfgMode [ ] ={1,3,6,1,4,1,2076,81,1,4};
UINT4 IssDefaultIpAddr [ ] ={1,3,6,1,4,1,2076,81,1,5};
UINT4 IssDefaultIpSubnetMask [ ] ={1,3,6,1,4,1,2076,81,1,6};
UINT4 IssEffectiveIpAddr [ ] ={1,3,6,1,4,1,2076,81,1,7};
UINT4 IssDefaultInterface [ ] ={1,3,6,1,4,1,2076,81,1,8};
UINT4 IssRestart [ ] ={1,3,6,1,4,1,2076,81,1,9};
UINT4 IssConfigSaveOption [ ] ={1,3,6,1,4,1,2076,81,1,10};
UINT4 IssConfigSaveIpAddr [ ] ={1,3,6,1,4,1,2076,81,1,11};
UINT4 IssConfigSaveFileName [ ] ={1,3,6,1,4,1,2076,81,1,12};
UINT4 IssInitiateConfigSave [ ] ={1,3,6,1,4,1,2076,81,1,13};
UINT4 IssConfigSaveStatus [ ] ={1,3,6,1,4,1,2076,81,1,14};
UINT4 IssConfigRestoreOption [ ] ={1,3,6,1,4,1,2076,81,1,15};
UINT4 IssConfigRestoreIpAddr [ ] ={1,3,6,1,4,1,2076,81,1,16};
UINT4 IssConfigRestoreFileName [ ] ={1,3,6,1,4,1,2076,81,1,17};
UINT4 IssInitiateConfigRestore [ ] ={1,3,6,1,4,1,2076,81,1,18};
UINT4 IssConfigRestoreStatus [ ] ={1,3,6,1,4,1,2076,81,1,19};
UINT4 IssDlImageFromIp [ ] ={1,3,6,1,4,1,2076,81,1,20};
UINT4 IssDlImageName [ ] ={1,3,6,1,4,1,2076,81,1,21};
UINT4 IssInitiateDlImage [ ] ={1,3,6,1,4,1,2076,81,1,22};
UINT4 IssLoggingOption [ ] ={1,3,6,1,4,1,2076,81,1,23};
UINT4 IssUploadLogFileToIp [ ] ={1,3,6,1,4,1,2076,81,1,24};
UINT4 IssLogFileName [ ] ={1,3,6,1,4,1,2076,81,1,25};
UINT4 IssInitiateUlLogFile [ ] ={1,3,6,1,4,1,2076,81,1,26};
UINT4 IssRemoteSaveStatus [ ] ={1,3,6,1,4,1,2076,81,1,27};
UINT4 IssDownloadStatus [ ] ={1,3,6,1,4,1,2076,81,1,28};
UINT4 IssSysContact [ ] ={1,3,6,1,4,1,2076,81,1,29};
UINT4 IssSysLocation [ ] ={1,3,6,1,4,1,2076,81,1,30};
UINT4 IssLoginAuthentication [ ] ={1,3,6,1,4,1,2076,81,1,31};
UINT4 IssSwitchBaseMacAddress [ ] ={1,3,6,1,4,1,2076,81,1,32};
UINT4 IssOOBInterface [ ] ={1,3,6,1,4,1,2076,81,1,33};
UINT4 IssSwitchDate [ ] ={1,3,6,1,4,1,2076,81,1,34};
UINT4 IssNoCliConsole [ ] ={1,3,6,1,4,1,2076,81,1,35};
UINT4 IssDefaultIpAddrAllocProtocol [ ] ={1,3,6,1,4,1,2076,81,1,36};
UINT4 IssHttpPort [ ] ={1,3,6,1,4,1,2076,81,1,37};
UINT4 IssHttpStatus [ ] ={1,3,6,1,4,1,2076,81,1,38};
UINT4 IssConfigRestoreFileVersion [ ] ={1,3,6,1,4,1,2076,81,1,39};
UINT4 IssDefaultRmIfName [ ] ={1,3,6,1,4,1,2076,81,1,40};
UINT4 IssDefaultVlanId [ ] ={1,3,6,1,4,1,2076,81,1,41};
UINT4 IssNpapiMode [ ] ={1,3,6,1,4,1,2076,81,1,42};
UINT4 IssConfigAutoSaveTrigger [ ] ={1,3,6,1,4,1,2076,81,1,43};
UINT4 IssConfigIncrSaveFlag [ ] ={1,3,6,1,4,1,2076,81,1,44};
UINT4 IssConfigRollbackFlag [ ] ={1,3,6,1,4,1,2076,81,1,45};
UINT4 IssConfigSyncUpOperation [ ] ={1,3,6,1,4,1,2076,81,1,46};
UINT4 IssFrontPanelPortCount [ ] ={1,3,6,1,4,1,2076,81,1,47};
UINT4 IssAuditLogStatus [ ] ={1,3,6,1,4,1,2076,81,1,48};
UINT4 IssAuditLogFileName [ ] ={1,3,6,1,4,1,2076,81,1,49};
UINT4 IssAuditLogFileSize [ ] ={1,3,6,1,4,1,2076,81,1,50};
UINT4 IssAuditLogReset [ ] ={1,3,6,1,4,1,2076,81,1,51};
UINT4 IssAuditLogRemoteIpAddr [ ] ={1,3,6,1,4,1,2076,81,1,52};
UINT4 IssAuditLogInitiateTransfer [ ] ={1,3,6,1,4,1,2076,81,1,53};
UINT4 IssAuditTransferFileName [ ] ={1,3,6,1,4,1,2076,81,1,54};
UINT4 IssDownLoadTransferMode [ ] ={1,3,6,1,4,1,2076,81,1,55};
UINT4 IssDownLoadUserName [ ] ={1,3,6,1,4,1,2076,81,1,56};
UINT4 IssDownLoadPassword [ ] ={1,3,6,1,4,1,2076,81,1,57};
UINT4 IssUploadLogTransferMode [ ] ={1,3,6,1,4,1,2076,81,1,58};
UINT4 IssUploadLogUserName [ ] ={1,3,6,1,4,1,2076,81,1,59};
UINT4 IssUploadLogPasswd [ ] ={1,3,6,1,4,1,2076,81,1,60};
UINT4 IssConfigSaveTransferMode [ ] ={1,3,6,1,4,1,2076,81,1,61};
UINT4 IssConfigSaveUserName [ ] ={1,3,6,1,4,1,2076,81,1,62};
UINT4 IssConfigSavePassword [ ] ={1,3,6,1,4,1,2076,81,1,63};
UINT4 IssSwitchMinThresholdTemperature [ ] ={1,3,6,1,4,1,2076,81,1,64};
UINT4 IssSwitchMaxThresholdTemperature [ ] ={1,3,6,1,4,1,2076,81,1,65};
UINT4 IssSwitchCurrentTemperature [ ] ={1,3,6,1,4,1,2076,81,1,66};
UINT4 IssSwitchMaxCPUThreshold [ ] ={1,3,6,1,4,1,2076,81,1,67};
UINT4 IssSwitchCurrentCPUThreshold [ ] ={1,3,6,1,4,1,2076,81,1,68};
UINT4 IssSwitchPowerSurge [ ] ={1,3,6,1,4,1,2076,81,1,69};
UINT4 IssSwitchPowerFailure [ ] ={1,3,6,1,4,1,2076,81,1,70};
UINT4 IssSwitchCurrentPowerSupply [ ] ={1,3,6,1,4,1,2076,81,1,71};
UINT4 IssSwitchMaxRAMUsage [ ] ={1,3,6,1,4,1,2076,81,1,72};
UINT4 IssSwitchCurrentRAMUsage [ ] ={1,3,6,1,4,1,2076,81,1,73};
UINT4 IssSwitchMaxFlashUsage [ ] ={1,3,6,1,4,1,2076,81,1,74};
UINT4 IssSwitchCurrentFlashUsage [ ] ={1,3,6,1,4,1,2076,81,1,75};
UINT4 IssConfigRestoreFileFormatVersion [ ] ={1,3,6,1,4,1,2076,81,1,76};
UINT4 IssDebugOption [ ] ={1,3,6,1,4,1,2076,81,1,77};
UINT4 IssConfigDefaultValueSaveOption [ ] ={1,3,6,1,4,1,2076,81,1,78};
UINT4 IssConfigSaveIpAddrType [ ] ={1,3,6,1,4,1,2076,81,1,79};
UINT4 IssConfigSaveIpvxAddr [ ] ={1,3,6,1,4,1,2076,81,1,80};
UINT4 IssConfigRestoreIpAddrType [ ] ={1,3,6,1,4,1,2076,81,1,81};
UINT4 IssConfigRestoreIpvxAddr [ ] ={1,3,6,1,4,1,2076,81,1,82};
UINT4 IssDlImageFromIpAddrType [ ] ={1,3,6,1,4,1,2076,81,1,83};
UINT4 IssDlImageFromIpvx [ ] ={1,3,6,1,4,1,2076,81,1,84};
UINT4 IssUploadLogFileToIpAddrType [ ] ={1,3,6,1,4,1,2076,81,1,85};
UINT4 IssUploadLogFileToIpvx [ ] ={1,3,6,1,4,1,2076,81,1,86};
UINT4 IssAuditLogRemoteIpAddrType [ ] ={1,3,6,1,4,1,2076,81,1,87};
UINT4 IssAuditLogRemoteIpvxAddr [ ] ={1,3,6,1,4,1,2076,81,1,88};
UINT4 IssSystemTimerSpeed [ ] ={1,3,6,1,4,1,2076,81,1,89};
UINT4 IssMgmtInterfaceRouting [ ] ={1,3,6,1,4,1,2076,81,1,90};
UINT4 IssMacLearnRateLimit [ ] ={1,3,6,1,4,1,2076,81,1,91};
UINT4 IssMacLearnRateLimitInterval [ ] ={1,3,6,1,4,1,2076,81,1,92};
UINT4 IssVrfUnqMacFlag [ ] ={1,3,6,1,4,1,2076,81,1,93};
UINT4 IssLoginAttempts [ ] ={1,3,6,1,4,1,2076,81,1,94};
UINT4 IssLoginLockTime [ ] ={1,3,6,1,4,1,2076,81,1,95};
UINT4 IssAuditLogSizeThreshold [ ] ={1,3,6,1,4,1,2076,81,1,96};
UINT4 IssTelnetStatus [ ] ={1,3,6,1,4,1,2076,81,1,97};
UINT4 IssWebSessionTimeOut [ ] ={1,3,6,1,4,1,2076,81,1,98};
UINT4 IssWebSessionMaxUsers [ ] ={1,3,6,1,4,1,2076,81,1,99};
UINT4 IssHeartBeatMode [ ] ={1,3,6,1,4,1,2076,81,1,100};
UINT4 IssRmRType [ ] ={1,3,6,1,4,1,2076,81,1,101};
UINT4 IssRmDType [ ] ={1,3,6,1,4,1,2076,81,1,102};
UINT4 IssClearConfig [ ] ={1,3,6,1,4,1,2076,81,1,103};
UINT4 IssClearConfigFileName [ ] ={1,3,6,1,4,1,2076,81,1,104};
UINT4 IssTelnetClientStatus [ ] ={1,3,6,1,4,1,2076,81,1,105};
UINT4 IssSshClientStatus [ ] ={1,3,6,1,4,1,2076,81,1,106};
UINT4 IssActiveTelnetClientSessions [ ] ={1,3,6,1,4,1,2076,81,1,107};
UINT4 IssActiveSshClientSessions [ ] ={1,3,6,1,4,1,2076,81,1,108};
UINT4 IssLogFileSize [ ] ={1,3,6,1,4,1,2076,81,1,109};
UINT4 IssLogReset [ ] ={1,3,6,1,4,1,2076,81,1,110};
UINT4 IssLogSizeThreshold [ ] ={1,3,6,1,4,1,2076,81,1,111};
UINT4 IssAutomaticPortCreate [ ] ={1,3,6,1,4,1,2076,81,1,112};
UINT4 IssUlRemoteLogFileName [ ] ={1,3,6,1,4,1,2076,81,1,113};
UINT4 IssDefaultExecTimeOut [ ] ={1,3,6,1,4,1,2076,81,1,114};
UINT4 IssRmStackingInterfaceType [ ] ={1,3,6,1,4,1,2076,81,1,115};
UINT4 IssPeerLoggingOption [ ] ={1,3,6,1,4,1,2076,81,1,116};
UINT4 IssStandbyRestart [ ] ={1,3,6,1,4,1,2076,81,1,117};
UINT4 IssRestoreType [ ] ={1,3,6,1,4,1,2076,81,1,118};
UINT4 IssSwitchModeType [ ] ={1,3,6,1,4,1,2076,81,1,119};
UINT4 IssDebugTimeStampOption [ ] ={1,3,6,1,4,1,2076,81,1,121};
UINT4 IssConfigCtrlIndex [ ] ={1,3,6,1,4,1,2076,81,2,1,1,1};
UINT4 IssConfigCtrlEgressStatus [ ] ={1,3,6,1,4,1,2076,81,2,1,1,2};
UINT4 IssConfigCtrlStatsCollection [ ] ={1,3,6,1,4,1,2076,81,2,1,1,3};
UINT4 IssConfigCtrlStatus [ ] ={1,3,6,1,4,1,2076,81,2,1,1,4};
UINT4 IssPortCtrlIndex [ ] ={1,3,6,1,4,1,2076,81,2,2,1,1};
UINT4 IssPortCtrlMode [ ] ={1,3,6,1,4,1,2076,81,2,2,1,2};
UINT4 IssPortCtrlDuplex [ ] ={1,3,6,1,4,1,2076,81,2,2,1,3};
UINT4 IssPortCtrlSpeed [ ] ={1,3,6,1,4,1,2076,81,2,2,1,4};
UINT4 IssPortCtrlFlowControl [ ] ={1,3,6,1,4,1,2076,81,2,2,1,5};
UINT4 IssPortCtrlRenegotiate [ ] ={1,3,6,1,4,1,2076,81,2,2,1,6};
UINT4 IssPortCtrlMaxMacAddr [ ] ={1,3,6,1,4,1,2076,81,2,2,1,7};
UINT4 IssPortCtrlMaxMacAction [ ] ={1,3,6,1,4,1,2076,81,2,2,1,8};
UINT4 IssPortHOLBlockPrevention [ ] ={1,3,6,1,4,1,2076,81,2,2,1,9};
UINT4 IssPortAutoNegAdvtCapBits [ ] ={1,3,6,1,4,1,2076,81,2,2,1,10};
UINT4 IssPortCpuControlledLearning [ ] ={1,3,6,1,4,1,2076,81,2,2,1,11};
UINT4 IssPortMdiOrMdixCap [ ] ={1,3,6,1,4,1,2076,81,2,2,1,12};
UINT4 IssPortCtrlFlowControlMaxRate [ ] ={1,3,6,1,4,1,2076,81,2,2,1,13};
UINT4 IssPortCtrlFlowControlMinRate [ ] ={1,3,6,1,4,1,2076,81,2,2,1,14};
UINT4 IssPortIsolationIngressPort [ ] ={1,3,6,1,4,1,2076,81,2,3,1,1};
UINT4 IssPortIsolationInVlanId [ ] ={1,3,6,1,4,1,2076,81,2,3,1,2};
UINT4 IssPortIsolationEgressPort [ ] ={1,3,6,1,4,1,2076,81,2,3,1,3};
UINT4 IssPortIsolationStorageType [ ] ={1,3,6,1,4,1,2076,81,2,3,1,4};
UINT4 IssPortIsolationRowStatus [ ] ={1,3,6,1,4,1,2076,81,2,3,1,5};
UINT4 IssMirrorStatus [ ] ={1,3,6,1,4,1,2076,81,3,1};
UINT4 IssMirrorToPort [ ] ={1,3,6,1,4,1,2076,81,3,2};
UINT4 IssMirrorCtrlIndex [ ] ={1,3,6,1,4,1,2076,81,3,3,1,1};
UINT4 IssMirrorCtrlIngressMirroring [ ] ={1,3,6,1,4,1,2076,81,3,3,1,2};
UINT4 IssMirrorCtrlEgressMirroring [ ] ={1,3,6,1,4,1,2076,81,3,3,1,3};
UINT4 IssMirrorCtrlStatus [ ] ={1,3,6,1,4,1,2076,81,3,3,1,4};
UINT4 IssMirrorCtrlRemainingSrcRcrds [ ] ={1,3,6,1,4,1,2076,81,3,4};
UINT4 IssMirrorCtrlRemainingDestRcrds [ ] ={1,3,6,1,4,1,2076,81,3,5};
UINT4 IssMirrorCtrlExtnSessionIndex [ ] ={1,3,6,1,4,1,2076,81,3,6,1,1};
UINT4 IssMirrorCtrlExtnMirrType [ ] ={1,3,6,1,4,1,2076,81,3,6,1,2};
UINT4 IssMirrorCtrlExtnRSpanStatus [ ] ={1,3,6,1,4,1,2076,81,3,6,1,3};
UINT4 IssMirrorCtrlExtnRSpanVlanId [ ] ={1,3,6,1,4,1,2076,81,3,6,1,4};
UINT4 IssMirrorCtrlExtnRSpanContext [ ] ={1,3,6,1,4,1,2076,81,3,6,1,5};
UINT4 IssMirrorCtrlExtnStatus [ ] ={1,3,6,1,4,1,2076,81,3,6,1,6};
UINT4 IssMirrorCtrlExtnSrcId [ ] ={1,3,6,1,4,1,2076,81,3,7,1,1};
UINT4 IssMirrorCtrlExtnSrcCfg [ ] ={1,3,6,1,4,1,2076,81,3,7,1,2};
UINT4 IssMirrorCtrlExtnSrcMode [ ] ={1,3,6,1,4,1,2076,81,3,7,1,3};
UINT4 IssMirrorCtrlExtnSrcVlanContext [ ] ={1,3,6,1,4,1,2076,81,3,8,1,1};
UINT4 IssMirrorCtrlExtnSrcVlanId [ ] ={1,3,6,1,4,1,2076,81,3,8,1,2};
UINT4 IssMirrorCtrlExtnSrcVlanCfg [ ] ={1,3,6,1,4,1,2076,81,3,8,1,3};
UINT4 IssMirrorCtrlExtnSrcVlanMode [ ] ={1,3,6,1,4,1,2076,81,3,8,1,4};
UINT4 IssMirrorCtrlExtnDestination [ ] ={1,3,6,1,4,1,2076,81,3,9,1,1};
UINT4 IssMirrorCtrlExtnDestCfg [ ] ={1,3,6,1,4,1,2076,81,3,9,1,2};
UINT4 IssCpuMirrorType [ ] ={1,3,6,1,4,1,2076,81,3,10};
UINT4 IssCpuMirrorToPort [ ] ={1,3,6,1,4,1,2076,81,3,11};
UINT4 IssIpAuthMgrIpAddr [ ] ={1,3,6,1,4,1,2076,81,7,1,1,1};
UINT4 IssIpAuthMgrIpMask [ ] ={1,3,6,1,4,1,2076,81,7,1,1,2};
UINT4 IssIpAuthMgrPortList [ ] ={1,3,6,1,4,1,2076,81,7,1,1,3};
UINT4 IssIpAuthMgrVlanList [ ] ={1,3,6,1,4,1,2076,81,7,1,1,4};
UINT4 IssIpAuthMgrOOBPort [ ] ={1,3,6,1,4,1,2076,81,7,1,1,5};
UINT4 IssIpAuthMgrAllowedServices [ ] ={1,3,6,1,4,1,2076,81,7,1,1,6};
UINT4 IssIpAuthMgrRowStatus [ ] ={1,3,6,1,4,1,2076,81,7,1,1,7};
UINT4 IssRateCtrlIndex [ ] ={1,3,6,1,4,1,2076,81,4,1,1,1};
UINT4 IssRateCtrlDLFLimitValue [ ] ={1,3,6,1,4,1,2076,81,4,1,1,2};
UINT4 IssRateCtrlBCASTLimitValue [ ] ={1,3,6,1,4,1,2076,81,4,1,1,3};
UINT4 IssRateCtrlMCASTLimitValue [ ] ={1,3,6,1,4,1,2076,81,4,1,1,4};
UINT4 IssRateCtrlPortRateLimit [ ] ={1,3,6,1,4,1,2076,81,4,1,1,5};
UINT4 IssRateCtrlPortBurstSize [ ] ={1,3,6,1,4,1,2076,81,4,1,1,6};
UINT4 IssL2FilterNo [ ] ={1,3,6,1,4,1,2076,81,5,1,1,1};
UINT4 IssL2FilterPriority [ ] ={1,3,6,1,4,1,2076,81,5,1,1,2};
UINT4 IssL2FilterEtherType [ ] ={1,3,6,1,4,1,2076,81,5,1,1,3};
UINT4 IssL2FilterProtocolType [ ] ={1,3,6,1,4,1,2076,81,5,1,1,4};
UINT4 IssL2FilterDstMacAddr [ ] ={1,3,6,1,4,1,2076,81,5,1,1,5};
UINT4 IssL2FilterSrcMacAddr [ ] ={1,3,6,1,4,1,2076,81,5,1,1,6};
UINT4 IssL2FilterVlanId [ ] ={1,3,6,1,4,1,2076,81,5,1,1,7};
UINT4 IssL2FilterInPortList [ ] ={1,3,6,1,4,1,2076,81,5,1,1,8};
UINT4 IssL2FilterAction [ ] ={1,3,6,1,4,1,2076,81,5,1,1,9};
UINT4 IssL2FilterMatchCount [ ] ={1,3,6,1,4,1,2076,81,5,1,1,10};
UINT4 IssL2FilterStatus [ ] ={1,3,6,1,4,1,2076,81,5,1,1,11};
UINT4 IssL2FilterOutPortList [ ] ={1,3,6,1,4,1,2076,81,5,1,1,12};
UINT4 IssL2FilterDirection [ ] ={1,3,6,1,4,1,2076,81,5,1,1,13};
UINT4 IssL3FilterNo [ ] ={1,3,6,1,4,1,2076,81,6,1,1,1};
UINT4 IssL3FilterPriority [ ] ={1,3,6,1,4,1,2076,81,6,1,1,2};
UINT4 IssL3FilterProtocol [ ] ={1,3,6,1,4,1,2076,81,6,1,1,3};
UINT4 IssL3FilterMessageType [ ] ={1,3,6,1,4,1,2076,81,6,1,1,4};
UINT4 IssL3FilterMessageCode [ ] ={1,3,6,1,4,1,2076,81,6,1,1,5};
UINT4 IssL3FilterDstIpAddr [ ] ={1,3,6,1,4,1,2076,81,6,1,1,6};
UINT4 IssL3FilterSrcIpAddr [ ] ={1,3,6,1,4,1,2076,81,6,1,1,7};
UINT4 IssL3FilterDstIpAddrMask [ ] ={1,3,6,1,4,1,2076,81,6,1,1,8};
UINT4 IssL3FilterSrcIpAddrMask [ ] ={1,3,6,1,4,1,2076,81,6,1,1,9};
UINT4 IssL3FilterMinDstProtPort [ ] ={1,3,6,1,4,1,2076,81,6,1,1,10};
UINT4 IssL3FilterMaxDstProtPort [ ] ={1,3,6,1,4,1,2076,81,6,1,1,11};
UINT4 IssL3FilterMinSrcProtPort [ ] ={1,3,6,1,4,1,2076,81,6,1,1,12};
UINT4 IssL3FilterMaxSrcProtPort [ ] ={1,3,6,1,4,1,2076,81,6,1,1,13};
UINT4 IssL3FilterInPortList [ ] ={1,3,6,1,4,1,2076,81,6,1,1,14};
UINT4 IssL3FilterOutPortList [ ] ={1,3,6,1,4,1,2076,81,6,1,1,15};
UINT4 IssL3FilterAckBit [ ] ={1,3,6,1,4,1,2076,81,6,1,1,16};
UINT4 IssL3FilterRstBit [ ] ={1,3,6,1,4,1,2076,81,6,1,1,17};
UINT4 IssL3FilterTos [ ] ={1,3,6,1,4,1,2076,81,6,1,1,18};
UINT4 IssL3FilterDscp [ ] ={1,3,6,1,4,1,2076,81,6,1,1,19};
UINT4 IssL3FilterDirection [ ] ={1,3,6,1,4,1,2076,81,6,1,1,20};
UINT4 IssL3FilterAction [ ] ={1,3,6,1,4,1,2076,81,6,1,1,21};
UINT4 IssL3FilterMatchCount [ ] ={1,3,6,1,4,1,2076,81,6,1,1,22};
UINT4 IssL3FilterStatus [ ] ={1,3,6,1,4,1,2076,81,6,1,1,23};
UINT4 IssL4SwitchingFilterNo [ ] ={1,3,6,1,4,1,2076,81,9,1,1,1};
UINT4 IssL4SwitchingProtocol [ ] ={1,3,6,1,4,1,2076,81,9,1,1,2};
UINT4 IssL4SwitchingPortNo [ ] ={1,3,6,1,4,1,2076,81,9,1,1,3};
UINT4 IssL4SwitchingCopyToPort [ ] ={1,3,6,1,4,1,2076,81,9,1,1,4};
UINT4 IssL4SwitchingFilterStatus [ ] ={1,3,6,1,4,1,2076,81,9,1,1,5};
UINT4 IssModuleId [ ] ={1,3,6,1,4,1,2076,81,12,1,1,1};
UINT4 IssModuleSystemControl [ ] ={1,3,6,1,4,1,2076,81,12,1,1,2};
UINT4 IssSwitchFanIndex [ ] ={1,3,6,1,4,1,2076,81,13,1,1,1};
UINT4 IssSwitchFanStatus [ ] ={1,3,6,1,4,1,2076,81,13,1,1,2};
UINT4 IssMsrFailedOid [ ] ={1,3,6,1,4,1,2076,81,10,1};
UINT4 IssMsrFailedValue [ ] ={1,3,6,1,4,1,2076,81,10,2};
UINT4 IssAuditTrapEvent [ ] ={1,3,6,1,4,1,2076,81,11,1};
UINT4 IssAuditTrapEventTime [ ] ={1,3,6,1,4,1,2076,81,11,2};
UINT4 IssAuditTrapFileName [ ] ={1,3,6,1,4,1,2076,81,11,3};
UINT4 IssAclProvisionMode [ ] ={1,3,6,1,4,1,2076,81,14,1};
UINT4 IssAclTriggerCommit [ ] ={1,3,6,1,4,1,2076,81,14,2};
UINT4 IssLogTrapEvent [ ] ={1,3,6,1,4,1,2076,81,16,1};
UINT4 IssLogTrapEventTime [ ] ={1,3,6,1,4,1,2076,81,16,2};
UINT4 IssLogTrapFileName [ ] ={1,3,6,1,4,1,2076,81,16,3};
UINT4 IssAclTrafficSeperationCtrl [ ] ={1,3,6,1,4,1,2076,81,15,1};
UINT4 IssHealthChkStatus [ ] ={1,3,6,1,4,1,2076,81,17,1};
UINT4 IssHealthChkErrorReason [ ] ={1,3,6,1,4,1,2076,81,17,2};
UINT4 IssHealthChkMemAllocErrPoolId [ ] ={1,3,6,1,4,1,2076,81,17,3};
UINT4 IssHealthChkConfigRestoreStatus [ ] ={1,3,6,1,4,1,2076,81,17,4};
UINT4 IssHealthChkClearCtr [ ] ={1,3,6,1,4,1,2076,81,17,5};

tSNMP_OID_TYPE IssSwitchNameOID = {10, IssSwitchName};


tSNMP_OID_TYPE IssHardwareVersionOID = {10, IssHardwareVersion};


tSNMP_OID_TYPE IssFirmwareVersionOID = {10, IssFirmwareVersion};


tSNMP_OID_TYPE IssDefaultIpAddrCfgModeOID = {10, IssDefaultIpAddrCfgMode};


tSNMP_OID_TYPE IssDefaultIpAddrOID = {10, IssDefaultIpAddr};


tSNMP_OID_TYPE IssDefaultIpSubnetMaskOID = {10, IssDefaultIpSubnetMask};


tSNMP_OID_TYPE IssEffectiveIpAddrOID = {10, IssEffectiveIpAddr};


tSNMP_OID_TYPE IssDefaultInterfaceOID = {10, IssDefaultInterface};


tSNMP_OID_TYPE IssRestartOID = {10, IssRestart};


tSNMP_OID_TYPE IssConfigSaveOptionOID = {10, IssConfigSaveOption};


tSNMP_OID_TYPE IssConfigSaveIpAddrOID = {10, IssConfigSaveIpAddr};


tSNMP_OID_TYPE IssConfigSaveFileNameOID = {10, IssConfigSaveFileName};


tSNMP_OID_TYPE IssInitiateConfigSaveOID = {10, IssInitiateConfigSave};


tSNMP_OID_TYPE IssConfigSaveStatusOID = {10, IssConfigSaveStatus};


tSNMP_OID_TYPE IssConfigRestoreOptionOID = {10, IssConfigRestoreOption};


tSNMP_OID_TYPE IssConfigRestoreIpAddrOID = {10, IssConfigRestoreIpAddr};


tSNMP_OID_TYPE IssConfigRestoreFileNameOID = {10, IssConfigRestoreFileName};


tSNMP_OID_TYPE IssInitiateConfigRestoreOID = {10, IssInitiateConfigRestore};


tSNMP_OID_TYPE IssConfigRestoreStatusOID = {10, IssConfigRestoreStatus};


tSNMP_OID_TYPE IssDlImageFromIpOID = {10, IssDlImageFromIp};


tSNMP_OID_TYPE IssDlImageNameOID = {10, IssDlImageName};


tSNMP_OID_TYPE IssInitiateDlImageOID = {10, IssInitiateDlImage};


tSNMP_OID_TYPE IssLoggingOptionOID = {10, IssLoggingOption};


tSNMP_OID_TYPE IssUploadLogFileToIpOID = {10, IssUploadLogFileToIp};


tSNMP_OID_TYPE IssLogFileNameOID = {10, IssLogFileName};


tSNMP_OID_TYPE IssInitiateUlLogFileOID = {10, IssInitiateUlLogFile};


tSNMP_OID_TYPE IssRemoteSaveStatusOID = {10, IssRemoteSaveStatus};


tSNMP_OID_TYPE IssDownloadStatusOID = {10, IssDownloadStatus};


tSNMP_OID_TYPE IssSysContactOID = {10, IssSysContact};


tSNMP_OID_TYPE IssSysLocationOID = {10, IssSysLocation};


tSNMP_OID_TYPE IssLoginAuthenticationOID = {10, IssLoginAuthentication};


tSNMP_OID_TYPE IssSwitchBaseMacAddressOID = {10, IssSwitchBaseMacAddress};


tSNMP_OID_TYPE IssOOBInterfaceOID = {10, IssOOBInterface};


tSNMP_OID_TYPE IssSwitchDateOID = {10, IssSwitchDate};


tSNMP_OID_TYPE IssNoCliConsoleOID = {10, IssNoCliConsole};


tSNMP_OID_TYPE IssDefaultIpAddrAllocProtocolOID = {10, IssDefaultIpAddrAllocProtocol};


tSNMP_OID_TYPE IssHttpPortOID = {10, IssHttpPort};


tSNMP_OID_TYPE IssHttpStatusOID = {10, IssHttpStatus};


tSNMP_OID_TYPE IssConfigRestoreFileVersionOID = {10, IssConfigRestoreFileVersion};


tSNMP_OID_TYPE IssDefaultRmIfNameOID = {10, IssDefaultRmIfName};


tSNMP_OID_TYPE IssDefaultVlanIdOID = {10, IssDefaultVlanId};


tSNMP_OID_TYPE IssNpapiModeOID = {10, IssNpapiMode};


tSNMP_OID_TYPE IssConfigAutoSaveTriggerOID = {10, IssConfigAutoSaveTrigger};


tSNMP_OID_TYPE IssConfigIncrSaveFlagOID = {10, IssConfigIncrSaveFlag};


tSNMP_OID_TYPE IssConfigRollbackFlagOID = {10, IssConfigRollbackFlag};


tSNMP_OID_TYPE IssConfigSyncUpOperationOID = {10, IssConfigSyncUpOperation};


tSNMP_OID_TYPE IssFrontPanelPortCountOID = {10, IssFrontPanelPortCount};


tSNMP_OID_TYPE IssAuditLogStatusOID = {10, IssAuditLogStatus};


tSNMP_OID_TYPE IssAuditLogFileNameOID = {10, IssAuditLogFileName};


tSNMP_OID_TYPE IssAuditLogFileSizeOID = {10, IssAuditLogFileSize};


tSNMP_OID_TYPE IssAuditLogResetOID = {10, IssAuditLogReset};


tSNMP_OID_TYPE IssAuditLogRemoteIpAddrOID = {10, IssAuditLogRemoteIpAddr};


tSNMP_OID_TYPE IssAuditLogInitiateTransferOID = {10, IssAuditLogInitiateTransfer};


tSNMP_OID_TYPE IssAuditTransferFileNameOID = {10, IssAuditTransferFileName};


tSNMP_OID_TYPE IssDownLoadTransferModeOID = {10, IssDownLoadTransferMode};


tSNMP_OID_TYPE IssDownLoadUserNameOID = {10, IssDownLoadUserName};


tSNMP_OID_TYPE IssDownLoadPasswordOID = {10, IssDownLoadPassword};


tSNMP_OID_TYPE IssUploadLogTransferModeOID = {10, IssUploadLogTransferMode};


tSNMP_OID_TYPE IssUploadLogUserNameOID = {10, IssUploadLogUserName};


tSNMP_OID_TYPE IssUploadLogPasswdOID = {10, IssUploadLogPasswd};


tSNMP_OID_TYPE IssConfigSaveTransferModeOID = {10, IssConfigSaveTransferMode};


tSNMP_OID_TYPE IssConfigSaveUserNameOID = {10, IssConfigSaveUserName};


tSNMP_OID_TYPE IssConfigSavePasswordOID = {10, IssConfigSavePassword};


tSNMP_OID_TYPE IssSwitchMinThresholdTemperatureOID = {10, IssSwitchMinThresholdTemperature};


tSNMP_OID_TYPE IssSwitchMaxThresholdTemperatureOID = {10, IssSwitchMaxThresholdTemperature};


tSNMP_OID_TYPE IssSwitchCurrentTemperatureOID = {10, IssSwitchCurrentTemperature};


tSNMP_OID_TYPE IssSwitchMaxCPUThresholdOID = {10, IssSwitchMaxCPUThreshold};


tSNMP_OID_TYPE IssSwitchCurrentCPUThresholdOID = {10, IssSwitchCurrentCPUThreshold};


tSNMP_OID_TYPE IssSwitchPowerSurgeOID = {10, IssSwitchPowerSurge};


tSNMP_OID_TYPE IssSwitchPowerFailureOID = {10, IssSwitchPowerFailure};


tSNMP_OID_TYPE IssSwitchCurrentPowerSupplyOID = {10, IssSwitchCurrentPowerSupply};


tSNMP_OID_TYPE IssSwitchMaxRAMUsageOID = {10, IssSwitchMaxRAMUsage};


tSNMP_OID_TYPE IssSwitchCurrentRAMUsageOID = {10, IssSwitchCurrentRAMUsage};


tSNMP_OID_TYPE IssSwitchMaxFlashUsageOID = {10, IssSwitchMaxFlashUsage};


tSNMP_OID_TYPE IssSwitchCurrentFlashUsageOID = {10, IssSwitchCurrentFlashUsage};


tSNMP_OID_TYPE IssConfigRestoreFileFormatVersionOID = {10, IssConfigRestoreFileFormatVersion};


tSNMP_OID_TYPE IssDebugOptionOID = {10, IssDebugOption};


tSNMP_OID_TYPE IssConfigDefaultValueSaveOptionOID = {10, IssConfigDefaultValueSaveOption};


tSNMP_OID_TYPE IssConfigSaveIpAddrTypeOID = {10, IssConfigSaveIpAddrType};


tSNMP_OID_TYPE IssConfigSaveIpvxAddrOID = {10, IssConfigSaveIpvxAddr};


tSNMP_OID_TYPE IssConfigRestoreIpAddrTypeOID = {10, IssConfigRestoreIpAddrType};


tSNMP_OID_TYPE IssConfigRestoreIpvxAddrOID = {10, IssConfigRestoreIpvxAddr};


tSNMP_OID_TYPE IssDlImageFromIpAddrTypeOID = {10, IssDlImageFromIpAddrType};


tSNMP_OID_TYPE IssDlImageFromIpvxOID = {10, IssDlImageFromIpvx};


tSNMP_OID_TYPE IssUploadLogFileToIpAddrTypeOID = {10, IssUploadLogFileToIpAddrType};


tSNMP_OID_TYPE IssUploadLogFileToIpvxOID = {10, IssUploadLogFileToIpvx};


tSNMP_OID_TYPE IssAuditLogRemoteIpAddrTypeOID = {10, IssAuditLogRemoteIpAddrType};


tSNMP_OID_TYPE IssAuditLogRemoteIpvxAddrOID = {10, IssAuditLogRemoteIpvxAddr};


tSNMP_OID_TYPE IssSystemTimerSpeedOID = {10, IssSystemTimerSpeed};


tSNMP_OID_TYPE IssMgmtInterfaceRoutingOID = {10, IssMgmtInterfaceRouting};


tSNMP_OID_TYPE IssMacLearnRateLimitOID = {10, IssMacLearnRateLimit};


tSNMP_OID_TYPE IssMacLearnRateLimitIntervalOID = {10, IssMacLearnRateLimitInterval};


tSNMP_OID_TYPE IssVrfUnqMacFlagOID = {10, IssVrfUnqMacFlag};


tSNMP_OID_TYPE IssLoginAttemptsOID = {10, IssLoginAttempts};


tSNMP_OID_TYPE IssLoginLockTimeOID = {10, IssLoginLockTime};


tSNMP_OID_TYPE IssAuditLogSizeThresholdOID = {10, IssAuditLogSizeThreshold};


tSNMP_OID_TYPE IssTelnetStatusOID = {10, IssTelnetStatus};


tSNMP_OID_TYPE IssWebSessionTimeOutOID = {10, IssWebSessionTimeOut};


tSNMP_OID_TYPE IssWebSessionMaxUsersOID = {10, IssWebSessionMaxUsers};


tSNMP_OID_TYPE IssHeartBeatModeOID = {10, IssHeartBeatMode};


tSNMP_OID_TYPE IssRmRTypeOID = {10, IssRmRType};


tSNMP_OID_TYPE IssRmDTypeOID = {10, IssRmDType};


tSNMP_OID_TYPE IssClearConfigOID = {10, IssClearConfig};


tSNMP_OID_TYPE IssClearConfigFileNameOID = {10, IssClearConfigFileName};

tSNMP_OID_TYPE IssTelnetClientStatusOID = {10, IssTelnetClientStatus};


tSNMP_OID_TYPE IssSshClientStatusOID = {10, IssSshClientStatus};


tSNMP_OID_TYPE IssActiveTelnetClientSessionsOID = {10, IssActiveTelnetClientSessions};


tSNMP_OID_TYPE IssActiveSshClientSessionsOID = {10, IssActiveSshClientSessions};

tSNMP_OID_TYPE IssLogFileSizeOID = {10, IssLogFileSize};

tSNMP_OID_TYPE IssLogResetOID = {10, IssLogReset};

tSNMP_OID_TYPE IssLogSizeThresholdOID = {10, IssLogSizeThreshold};

tSNMP_OID_TYPE IssAutomaticPortCreateOID = {10, IssAutomaticPortCreate};

tSNMP_OID_TYPE IssUlRemoteLogFileNameOID = {10, IssUlRemoteLogFileName};

tSNMP_OID_TYPE IssDefaultExecTimeOutOID = {10, IssDefaultExecTimeOut};

tSNMP_OID_TYPE IssRmStackingInterfaceTypeOID = {10, IssRmStackingInterfaceType};

tSNMP_OID_TYPE IssPeerLoggingOptionOID = {10, IssPeerLoggingOption};


tSNMP_OID_TYPE IssStandbyRestartOID = {10, IssStandbyRestart};


tSNMP_OID_TYPE IssRestoreTypeOID = {10, IssRestoreType};

tSNMP_OID_TYPE IssSwitchModeTypeOID = {10, IssSwitchModeType};
tSNMP_OID_TYPE IssDebugTimeStampOptionOID = {10, IssDebugTimeStampOption};


tSNMP_OID_TYPE IssMirrorStatusOID = {10, IssMirrorStatus};


tSNMP_OID_TYPE IssMirrorToPortOID = {10, IssMirrorToPort};


tSNMP_OID_TYPE IssMirrorCtrlRemainingSrcRcrdsOID = {10, IssMirrorCtrlRemainingSrcRcrds};


tSNMP_OID_TYPE IssMirrorCtrlRemainingDestRcrdsOID = {10, IssMirrorCtrlRemainingDestRcrds};

tSNMP_OID_TYPE IssCpuMirrorTypeOID = {10, IssCpuMirrorType};
tSNMP_OID_TYPE IssCpuMirrorToPortOID = {10, IssCpuMirrorToPort};

tSNMP_OID_TYPE IssAuditTrapFileNameOID = {10, IssAuditTrapFileName};


tSNMP_OID_TYPE IssAclProvisionModeOID = {10, IssAclProvisionMode};


tSNMP_OID_TYPE IssAclTriggerCommitOID = {10, IssAclTriggerCommit};

tSNMP_OID_TYPE IssAclTrafficSeperationCtrlOID = {10, IssAclTrafficSeperationCtrl};

tSNMP_OID_TYPE IssHealthChkStatusOID = {10, IssHealthChkStatus};

tSNMP_OID_TYPE IssHealthChkErrorReasonOID = {10, IssHealthChkErrorReason};

tSNMP_OID_TYPE IssHealthChkMemAllocErrPoolIdOID = {10, IssHealthChkMemAllocErrPoolId};

tSNMP_OID_TYPE IssHealthChkConfigRestoreStatusOID = {10, IssHealthChkConfigRestoreStatus};

tSNMP_OID_TYPE IssHealthChkClearCtrOID = {10, IssHealthChkClearCtr};


tMbDbEntry IssSwitchNameMibEntry[]= {

{{10,IssSwitchName}, NULL, IssSwitchNameGet, IssSwitchNameSet, IssSwitchNameTest, IssSwitchNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "ISS"},
};
tMibData IssSwitchNameEntry = { 1, IssSwitchNameMibEntry };

tMbDbEntry IssHardwareVersionMibEntry[]= {

{{10,IssHardwareVersion}, NULL, IssHardwareVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, ""},
};
tMibData IssHardwareVersionEntry = { 1, IssHardwareVersionMibEntry };

tMbDbEntry IssFirmwareVersionMibEntry[]= {

{{10,IssFirmwareVersion}, NULL, IssFirmwareVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, ""},
};
tMibData IssFirmwareVersionEntry = { 1, IssFirmwareVersionMibEntry };

tMbDbEntry IssDefaultIpAddrCfgModeMibEntry[]= {

{{10,IssDefaultIpAddrCfgMode}, NULL, IssDefaultIpAddrCfgModeGet, IssDefaultIpAddrCfgModeSet, IssDefaultIpAddrCfgModeTest, IssDefaultIpAddrCfgModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData IssDefaultIpAddrCfgModeEntry = { 1, IssDefaultIpAddrCfgModeMibEntry };

tMbDbEntry IssDefaultIpAddrMibEntry[]= {

{{10,IssDefaultIpAddr}, NULL, IssDefaultIpAddrGet, IssDefaultIpAddrSet, IssDefaultIpAddrTest, IssDefaultIpAddrDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssDefaultIpAddrEntry = { 1, IssDefaultIpAddrMibEntry };

tMbDbEntry IssDefaultIpSubnetMaskMibEntry[]= {

{{10,IssDefaultIpSubnetMask}, NULL, IssDefaultIpSubnetMaskGet, IssDefaultIpSubnetMaskSet, IssDefaultIpSubnetMaskTest, IssDefaultIpSubnetMaskDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssDefaultIpSubnetMaskEntry = { 1, IssDefaultIpSubnetMaskMibEntry };

tMbDbEntry IssEffectiveIpAddrMibEntry[]= {

{{10,IssEffectiveIpAddr}, NULL, IssEffectiveIpAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, NULL, 0, 1, 0, NULL},
};
tMibData IssEffectiveIpAddrEntry = { 1, IssEffectiveIpAddrMibEntry };

tMbDbEntry IssDefaultInterfaceMibEntry[]= {

{{10,IssDefaultInterface}, NULL, IssDefaultInterfaceGet, IssDefaultInterfaceSet, IssDefaultInterfaceTest, IssDefaultInterfaceDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "eth0"},
};
tMibData IssDefaultInterfaceEntry = { 1, IssDefaultInterfaceMibEntry };

tMbDbEntry IssRestartMibEntry[]= {

{{10,IssRestart}, NULL, IssRestartGet, IssRestartSet, IssRestartTest, IssRestartDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData IssRestartEntry = { 1, IssRestartMibEntry };

tMbDbEntry IssConfigSaveOptionMibEntry[]= {

{{10,IssConfigSaveOption}, NULL, IssConfigSaveOptionGet, IssConfigSaveOptionSet, IssConfigSaveOptionTest, IssConfigSaveOptionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData IssConfigSaveOptionEntry = { 1, IssConfigSaveOptionMibEntry };

tMbDbEntry IssConfigSaveIpAddrMibEntry[]= {

{{10,IssConfigSaveIpAddr}, NULL, IssConfigSaveIpAddrGet, IssConfigSaveIpAddrSet, IssConfigSaveIpAddrTest, IssConfigSaveIpAddrDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 1, 0, NULL},
};
tMibData IssConfigSaveIpAddrEntry = { 1, IssConfigSaveIpAddrMibEntry };

tMbDbEntry IssConfigSaveFileNameMibEntry[]= {

{{10,IssConfigSaveFileName}, NULL, IssConfigSaveFileNameGet, IssConfigSaveFileNameSet, IssConfigSaveFileNameTest, IssConfigSaveFileNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "iss.conf"},
};
tMibData IssConfigSaveFileNameEntry = { 1, IssConfigSaveFileNameMibEntry };

tMbDbEntry IssInitiateConfigSaveMibEntry[]= {

{{10,IssInitiateConfigSave}, NULL, IssInitiateConfigSaveGet, IssInitiateConfigSaveSet, IssInitiateConfigSaveTest, IssInitiateConfigSaveDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData IssInitiateConfigSaveEntry = { 1, IssInitiateConfigSaveMibEntry };

tMbDbEntry IssConfigSaveStatusMibEntry[]= {

{{10,IssConfigSaveStatus}, NULL, IssConfigSaveStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, "4"},
};
tMibData IssConfigSaveStatusEntry = { 1, IssConfigSaveStatusMibEntry };

tMbDbEntry IssConfigRestoreOptionMibEntry[]= {

{{10,IssConfigRestoreOption}, NULL, IssConfigRestoreOptionGet, IssConfigRestoreOptionSet, IssConfigRestoreOptionTest, IssConfigRestoreOptionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData IssConfigRestoreOptionEntry = { 1, IssConfigRestoreOptionMibEntry };

tMbDbEntry IssConfigRestoreIpAddrMibEntry[]= {

{{10,IssConfigRestoreIpAddr}, NULL, IssConfigRestoreIpAddrGet, IssConfigRestoreIpAddrSet, IssConfigRestoreIpAddrTest, IssConfigRestoreIpAddrDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 1, 0, NULL},
};
tMibData IssConfigRestoreIpAddrEntry = { 1, IssConfigRestoreIpAddrMibEntry };

tMbDbEntry IssConfigRestoreFileNameMibEntry[]= {

{{10,IssConfigRestoreFileName}, NULL, IssConfigRestoreFileNameGet, IssConfigRestoreFileNameSet, IssConfigRestoreFileNameTest, IssConfigRestoreFileNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "iss.conf"},
};
tMibData IssConfigRestoreFileNameEntry = { 1, IssConfigRestoreFileNameMibEntry };

tMbDbEntry IssInitiateConfigRestoreMibEntry[]= {

{{10,IssInitiateConfigRestore}, NULL, IssInitiateConfigRestoreGet, IssInitiateConfigRestoreSet, IssInitiateConfigRestoreTest, IssInitiateConfigRestoreDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData IssInitiateConfigRestoreEntry = { 1, IssInitiateConfigRestoreMibEntry };

tMbDbEntry IssConfigRestoreStatusMibEntry[]= {

{{10,IssConfigRestoreStatus}, NULL, IssConfigRestoreStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, "4"},
};
tMibData IssConfigRestoreStatusEntry = { 1, IssConfigRestoreStatusMibEntry };

tMbDbEntry IssDlImageFromIpMibEntry[]= {

{{10,IssDlImageFromIp}, NULL, IssDlImageFromIpGet, IssDlImageFromIpSet, IssDlImageFromIpTest, IssDlImageFromIpDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 1, 0, NULL},
};
tMibData IssDlImageFromIpEntry = { 1, IssDlImageFromIpMibEntry };

tMbDbEntry IssDlImageNameMibEntry[]= {

{{10,IssDlImageName}, NULL, IssDlImageNameGet, IssDlImageNameSet, IssDlImageNameTest, IssDlImageNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "iss.exe"},
};
tMibData IssDlImageNameEntry = { 1, IssDlImageNameMibEntry };

tMbDbEntry IssInitiateDlImageMibEntry[]= {

{{10,IssInitiateDlImage}, NULL, IssInitiateDlImageGet, IssInitiateDlImageSet, IssInitiateDlImageTest, IssInitiateDlImageDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssInitiateDlImageEntry = { 1, IssInitiateDlImageMibEntry };

tMbDbEntry IssLoggingOptionMibEntry[]= {

{{10,IssLoggingOption}, NULL, IssLoggingOptionGet, IssLoggingOptionSet, IssLoggingOptionTest, IssLoggingOptionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData IssLoggingOptionEntry = { 1, IssLoggingOptionMibEntry };

tMbDbEntry IssUploadLogFileToIpMibEntry[]= {

{{10,IssUploadLogFileToIp}, NULL, IssUploadLogFileToIpGet, IssUploadLogFileToIpSet, IssUploadLogFileToIpTest, IssUploadLogFileToIpDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 1, 0, NULL},
};
tMibData IssUploadLogFileToIpEntry = { 1, IssUploadLogFileToIpMibEntry };

tMbDbEntry IssLogFileNameMibEntry[]= {

{{10,IssLogFileName}, NULL, IssLogFileNameGet, IssLogFileNameSet, IssLogFileNameTest, IssLogFileNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "iss.log"},
};
tMibData IssLogFileNameEntry = { 1, IssLogFileNameMibEntry };

tMbDbEntry IssInitiateUlLogFileMibEntry[]= {

{{10,IssInitiateUlLogFile}, NULL, IssInitiateUlLogFileGet, IssInitiateUlLogFileSet, IssInitiateUlLogFileTest, IssInitiateUlLogFileDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssInitiateUlLogFileEntry = { 1, IssInitiateUlLogFileMibEntry };

tMbDbEntry IssRemoteSaveStatusMibEntry[]= {

{{10,IssRemoteSaveStatus}, NULL, IssRemoteSaveStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, "4"},
};
tMibData IssRemoteSaveStatusEntry = { 1, IssRemoteSaveStatusMibEntry };

tMbDbEntry IssDownloadStatusMibEntry[]= {

{{10,IssDownloadStatus}, NULL, IssDownloadStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, "4"},
};
tMibData IssDownloadStatusEntry = { 1, IssDownloadStatusMibEntry };

tMbDbEntry IssSysContactMibEntry[]= {

{{10,IssSysContact}, NULL, IssSysContactGet, IssSysContactSet, IssSysContactTest, IssSysContactDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 1, 0, NULL},
};
tMibData IssSysContactEntry = { 1, IssSysContactMibEntry };

tMbDbEntry IssSysLocationMibEntry[]= {

{{10,IssSysLocation}, NULL, IssSysLocationGet, IssSysLocationSet, IssSysLocationTest, IssSysLocationDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 1, 0, NULL},
};
tMibData IssSysLocationEntry = { 1, IssSysLocationMibEntry };

tMbDbEntry IssLoginAuthenticationMibEntry[]= {

{{10,IssLoginAuthentication}, NULL, IssLoginAuthenticationGet, IssLoginAuthenticationSet, IssLoginAuthenticationTest, IssLoginAuthenticationDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData IssLoginAuthenticationEntry = { 1, IssLoginAuthenticationMibEntry };

tMbDbEntry IssSwitchBaseMacAddressMibEntry[]= {

{{10,IssSwitchBaseMacAddress}, NULL, IssSwitchBaseMacAddressGet, IssSwitchBaseMacAddressSet, IssSwitchBaseMacAddressTest, IssSwitchBaseMacAddressDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, NULL, 0, 0, 0, "000102030405"},
};
tMibData IssSwitchBaseMacAddressEntry = { 1, IssSwitchBaseMacAddressMibEntry };

tMbDbEntry IssOOBInterfaceMibEntry[]= {

{{10,IssOOBInterface}, NULL, IssOOBInterfaceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData IssOOBInterfaceEntry = { 1, IssOOBInterfaceMibEntry };

tMbDbEntry IssSwitchDateMibEntry[]= {

{{10,IssSwitchDate}, NULL, IssSwitchDateGet, IssSwitchDateSet, IssSwitchDateTest, IssSwitchDateDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssSwitchDateEntry = { 1, IssSwitchDateMibEntry };

tMbDbEntry IssNoCliConsoleMibEntry[]= {

{{10,IssNoCliConsole}, NULL, IssNoCliConsoleGet, IssNoCliConsoleSet, IssNoCliConsoleTest, IssNoCliConsoleDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssNoCliConsoleEntry = { 1, IssNoCliConsoleMibEntry };

tMbDbEntry IssDefaultIpAddrAllocProtocolMibEntry[]= {

{{10,IssDefaultIpAddrAllocProtocol}, NULL, IssDefaultIpAddrAllocProtocolGet, IssDefaultIpAddrAllocProtocolSet, IssDefaultIpAddrAllocProtocolTest, IssDefaultIpAddrAllocProtocolDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData IssDefaultIpAddrAllocProtocolEntry = { 1, IssDefaultIpAddrAllocProtocolMibEntry };

tMbDbEntry IssHttpPortMibEntry[]= {

{{10,IssHttpPort}, NULL, IssHttpPortGet, IssHttpPortSet, IssHttpPortTest, IssHttpPortDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "80"},
};
tMibData IssHttpPortEntry = { 1, IssHttpPortMibEntry };

tMbDbEntry IssHttpStatusMibEntry[]= {

{{10,IssHttpStatus}, NULL, IssHttpStatusGet, IssHttpStatusSet, IssHttpStatusTest, IssHttpStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData IssHttpStatusEntry = { 1, IssHttpStatusMibEntry };

tMbDbEntry IssConfigRestoreFileVersionMibEntry[]= {

{{10,IssConfigRestoreFileVersion}, NULL, IssConfigRestoreFileVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData IssConfigRestoreFileVersionEntry = { 1, IssConfigRestoreFileVersionMibEntry };

tMbDbEntry IssDefaultRmIfNameMibEntry[]= {

{{10,IssDefaultRmIfName}, NULL, IssDefaultRmIfNameGet, IssDefaultRmIfNameSet, IssDefaultRmIfNameTest, IssDefaultRmIfNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "NONE"},
};
tMibData IssDefaultRmIfNameEntry = { 1, IssDefaultRmIfNameMibEntry };

tMbDbEntry IssDefaultVlanIdMibEntry[]= {

{{10,IssDefaultVlanId}, NULL, IssDefaultVlanIdGet, IssDefaultVlanIdSet, IssDefaultVlanIdTest, IssDefaultVlanIdDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData IssDefaultVlanIdEntry = { 1, IssDefaultVlanIdMibEntry };

tMbDbEntry IssNpapiModeMibEntry[]= {

{{10,IssNpapiMode}, NULL, IssNpapiModeGet, IssNpapiModeSet, IssNpapiModeTest, IssNpapiModeDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "Synchronous"},
};
tMibData IssNpapiModeEntry = { 1, IssNpapiModeMibEntry };

tMbDbEntry IssConfigAutoSaveTriggerMibEntry[]= {

{{10,IssConfigAutoSaveTrigger}, NULL, IssConfigAutoSaveTriggerGet, IssConfigAutoSaveTriggerSet, IssConfigAutoSaveTriggerTest, IssConfigAutoSaveTriggerDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData IssConfigAutoSaveTriggerEntry = { 1, IssConfigAutoSaveTriggerMibEntry };

tMbDbEntry IssConfigIncrSaveFlagMibEntry[]= {

{{10,IssConfigIncrSaveFlag}, NULL, IssConfigIncrSaveFlagGet, IssConfigIncrSaveFlagSet, IssConfigIncrSaveFlagTest, IssConfigIncrSaveFlagDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData IssConfigIncrSaveFlagEntry = { 1, IssConfigIncrSaveFlagMibEntry };

tMbDbEntry IssConfigRollbackFlagMibEntry[]= {

{{10,IssConfigRollbackFlag}, NULL, IssConfigRollbackFlagGet, IssConfigRollbackFlagSet, IssConfigRollbackFlagTest, IssConfigRollbackFlagDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData IssConfigRollbackFlagEntry = { 1, IssConfigRollbackFlagMibEntry };

tMbDbEntry IssConfigSyncUpOperationMibEntry[]= {

{{10,IssConfigSyncUpOperation}, NULL, IssConfigSyncUpOperationGet, IssConfigSyncUpOperationSet, IssConfigSyncUpOperationTest, IssConfigSyncUpOperationDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData IssConfigSyncUpOperationEntry = { 1, IssConfigSyncUpOperationMibEntry };

tMbDbEntry IssFrontPanelPortCountMibEntry[]= {

{{10,IssFrontPanelPortCount}, NULL, IssFrontPanelPortCountGet, IssFrontPanelPortCountSet, IssFrontPanelPortCountTest, IssFrontPanelPortCountDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssFrontPanelPortCountEntry = { 1, IssFrontPanelPortCountMibEntry };

tMbDbEntry IssAuditLogStatusMibEntry[]= {

{{10,IssAuditLogStatus}, NULL, IssAuditLogStatusGet, IssAuditLogStatusSet, IssAuditLogStatusTest, IssAuditLogStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData IssAuditLogStatusEntry = { 1, IssAuditLogStatusMibEntry };

tMbDbEntry IssAuditLogFileNameMibEntry[]= {

{{10,IssAuditLogFileName}, NULL, IssAuditLogFileNameGet, IssAuditLogFileNameSet, IssAuditLogFileNameTest, IssAuditLogFileNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "config.txt"},
};
tMibData IssAuditLogFileNameEntry = { 1, IssAuditLogFileNameMibEntry };

tMbDbEntry IssAuditLogFileSizeMibEntry[]= {

{{10,IssAuditLogFileSize}, NULL, IssAuditLogFileSizeGet, IssAuditLogFileSizeSet, IssAuditLogFileSizeTest, IssAuditLogFileSizeDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "10"},
};
tMibData IssAuditLogFileSizeEntry = { 1, IssAuditLogFileSizeMibEntry };

tMbDbEntry IssAuditLogResetMibEntry[]= {

{{10,IssAuditLogReset}, NULL, IssAuditLogResetGet, IssAuditLogResetSet, IssAuditLogResetTest, IssAuditLogResetDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData IssAuditLogResetEntry = { 1, IssAuditLogResetMibEntry };

tMbDbEntry IssAuditLogRemoteIpAddrMibEntry[]= {

{{10,IssAuditLogRemoteIpAddr}, NULL, IssAuditLogRemoteIpAddrGet, IssAuditLogRemoteIpAddrSet, IssAuditLogRemoteIpAddrTest, IssAuditLogRemoteIpAddrDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 1, 0, NULL},
};
tMibData IssAuditLogRemoteIpAddrEntry = { 1, IssAuditLogRemoteIpAddrMibEntry };

tMbDbEntry IssAuditLogInitiateTransferMibEntry[]= {

{{10,IssAuditLogInitiateTransfer}, NULL, IssAuditLogInitiateTransferGet, IssAuditLogInitiateTransferSet, IssAuditLogInitiateTransferTest, IssAuditLogInitiateTransferDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData IssAuditLogInitiateTransferEntry = { 1, IssAuditLogInitiateTransferMibEntry };

tMbDbEntry IssAuditTransferFileNameMibEntry[]= {

{{10,IssAuditTransferFileName}, NULL, IssAuditTransferFileNameGet, IssAuditTransferFileNameSet, IssAuditTransferFileNameTest, IssAuditTransferFileNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "config.txt"},
};
tMibData IssAuditTransferFileNameEntry = { 1, IssAuditTransferFileNameMibEntry };

tMbDbEntry IssDownLoadTransferModeMibEntry[]= {

{{10,IssDownLoadTransferMode}, NULL, IssDownLoadTransferModeGet, IssDownLoadTransferModeSet, IssDownLoadTransferModeTest, IssDownLoadTransferModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData IssDownLoadTransferModeEntry = { 1, IssDownLoadTransferModeMibEntry };

tMbDbEntry IssDownLoadUserNameMibEntry[]= {

{{10,IssDownLoadUserName}, NULL, IssDownLoadUserNameGet, IssDownLoadUserNameSet, IssDownLoadUserNameTest, IssDownLoadUserNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssDownLoadUserNameEntry = { 1, IssDownLoadUserNameMibEntry };

tMbDbEntry IssDownLoadPasswordMibEntry[]= {

{{10,IssDownLoadPassword}, NULL, IssDownLoadPasswordGet, IssDownLoadPasswordSet, IssDownLoadPasswordTest, IssDownLoadPasswordDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssDownLoadPasswordEntry = { 1, IssDownLoadPasswordMibEntry };

tMbDbEntry IssUploadLogTransferModeMibEntry[]= {

{{10,IssUploadLogTransferMode}, NULL, IssUploadLogTransferModeGet, IssUploadLogTransferModeSet, IssUploadLogTransferModeTest, IssUploadLogTransferModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData IssUploadLogTransferModeEntry = { 1, IssUploadLogTransferModeMibEntry };

tMbDbEntry IssUploadLogUserNameMibEntry[]= {

{{10,IssUploadLogUserName}, NULL, IssUploadLogUserNameGet, IssUploadLogUserNameSet, IssUploadLogUserNameTest, IssUploadLogUserNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssUploadLogUserNameEntry = { 1, IssUploadLogUserNameMibEntry };

tMbDbEntry IssUploadLogPasswdMibEntry[]= {

{{10,IssUploadLogPasswd}, NULL, IssUploadLogPasswdGet, IssUploadLogPasswdSet, IssUploadLogPasswdTest, IssUploadLogPasswdDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssUploadLogPasswdEntry = { 1, IssUploadLogPasswdMibEntry };

tMbDbEntry IssConfigSaveTransferModeMibEntry[]= {

{{10,IssConfigSaveTransferMode}, NULL, IssConfigSaveTransferModeGet, IssConfigSaveTransferModeSet, IssConfigSaveTransferModeTest, IssConfigSaveTransferModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData IssConfigSaveTransferModeEntry = { 1, IssConfigSaveTransferModeMibEntry };

tMbDbEntry IssConfigSaveUserNameMibEntry[]= {

{{10,IssConfigSaveUserName}, NULL, IssConfigSaveUserNameGet, IssConfigSaveUserNameSet, IssConfigSaveUserNameTest, IssConfigSaveUserNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssConfigSaveUserNameEntry = { 1, IssConfigSaveUserNameMibEntry };

tMbDbEntry IssConfigSavePasswordMibEntry[]= {

{{10,IssConfigSavePassword}, NULL, IssConfigSavePasswordGet, IssConfigSavePasswordSet, IssConfigSavePasswordTest, IssConfigSavePasswordDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssConfigSavePasswordEntry = { 1, IssConfigSavePasswordMibEntry };

tMbDbEntry IssSwitchMinThresholdTemperatureMibEntry[]= {

{{10,IssSwitchMinThresholdTemperature}, NULL, IssSwitchMinThresholdTemperatureGet, IssSwitchMinThresholdTemperatureSet, IssSwitchMinThresholdTemperatureTest, IssSwitchMinThresholdTemperatureDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "10"},
};
tMibData IssSwitchMinThresholdTemperatureEntry = { 1, IssSwitchMinThresholdTemperatureMibEntry };

tMbDbEntry IssSwitchMaxThresholdTemperatureMibEntry[]= {

{{10,IssSwitchMaxThresholdTemperature}, NULL, IssSwitchMaxThresholdTemperatureGet, IssSwitchMaxThresholdTemperatureSet, IssSwitchMaxThresholdTemperatureTest, IssSwitchMaxThresholdTemperatureDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "40"},
};
tMibData IssSwitchMaxThresholdTemperatureEntry = { 1, IssSwitchMaxThresholdTemperatureMibEntry };

tMbDbEntry IssSwitchCurrentTemperatureMibEntry[]= {

{{10,IssSwitchCurrentTemperature}, NULL, IssSwitchCurrentTemperatureGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData IssSwitchCurrentTemperatureEntry = { 1, IssSwitchCurrentTemperatureMibEntry };

tMbDbEntry IssSwitchMaxCPUThresholdMibEntry[]= {

{{10,IssSwitchMaxCPUThreshold}, NULL, IssSwitchMaxCPUThresholdGet, IssSwitchMaxCPUThresholdSet, IssSwitchMaxCPUThresholdTest, IssSwitchMaxCPUThresholdDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "95"},
};
tMibData IssSwitchMaxCPUThresholdEntry = { 1, IssSwitchMaxCPUThresholdMibEntry };

tMbDbEntry IssSwitchCurrentCPUThresholdMibEntry[]= {

{{10,IssSwitchCurrentCPUThreshold}, NULL, IssSwitchCurrentCPUThresholdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData IssSwitchCurrentCPUThresholdEntry = { 1, IssSwitchCurrentCPUThresholdMibEntry };

tMbDbEntry IssSwitchPowerSurgeMibEntry[]= {

{{10,IssSwitchPowerSurge}, NULL, IssSwitchPowerSurgeGet, IssSwitchPowerSurgeSet, IssSwitchPowerSurgeTest, IssSwitchPowerSurgeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "230"},
};
tMibData IssSwitchPowerSurgeEntry = { 1, IssSwitchPowerSurgeMibEntry };

tMbDbEntry IssSwitchPowerFailureMibEntry[]= {

{{10,IssSwitchPowerFailure}, NULL, IssSwitchPowerFailureGet, IssSwitchPowerFailureSet, IssSwitchPowerFailureTest, IssSwitchPowerFailureDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "100"},
};
tMibData IssSwitchPowerFailureEntry = { 1, IssSwitchPowerFailureMibEntry };

tMbDbEntry IssSwitchCurrentPowerSupplyMibEntry[]= {

{{10,IssSwitchCurrentPowerSupply}, NULL, IssSwitchCurrentPowerSupplyGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData IssSwitchCurrentPowerSupplyEntry = { 1, IssSwitchCurrentPowerSupplyMibEntry };

tMbDbEntry IssSwitchMaxRAMUsageMibEntry[]= {

{{10,IssSwitchMaxRAMUsage}, NULL, IssSwitchMaxRAMUsageGet, IssSwitchMaxRAMUsageSet, IssSwitchMaxRAMUsageTest, IssSwitchMaxRAMUsageDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "90"},
};
tMibData IssSwitchMaxRAMUsageEntry = { 1, IssSwitchMaxRAMUsageMibEntry };

tMbDbEntry IssSwitchCurrentRAMUsageMibEntry[]= {

{{10,IssSwitchCurrentRAMUsage}, NULL, IssSwitchCurrentRAMUsageGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData IssSwitchCurrentRAMUsageEntry = { 1, IssSwitchCurrentRAMUsageMibEntry };

tMbDbEntry IssSwitchMaxFlashUsageMibEntry[]= {

{{10,IssSwitchMaxFlashUsage}, NULL, IssSwitchMaxFlashUsageGet, IssSwitchMaxFlashUsageSet, IssSwitchMaxFlashUsageTest, IssSwitchMaxFlashUsageDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "95"},
};
tMibData IssSwitchMaxFlashUsageEntry = { 1, IssSwitchMaxFlashUsageMibEntry };

tMbDbEntry IssSwitchCurrentFlashUsageMibEntry[]= {

{{10,IssSwitchCurrentFlashUsage}, NULL, IssSwitchCurrentFlashUsageGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData IssSwitchCurrentFlashUsageEntry = { 1, IssSwitchCurrentFlashUsageMibEntry };

tMbDbEntry IssConfigRestoreFileFormatVersionMibEntry[]= {

{{10,IssConfigRestoreFileFormatVersion}, NULL, IssConfigRestoreFileFormatVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData IssConfigRestoreFileFormatVersionEntry = { 1, IssConfigRestoreFileFormatVersionMibEntry };

tMbDbEntry IssDebugOptionMibEntry[]= {

{{10,IssDebugOption}, NULL, IssDebugOptionGet, IssDebugOptionSet, IssDebugOptionTest, IssDebugOptionDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssDebugOptionEntry = { 1, IssDebugOptionMibEntry };

tMbDbEntry IssConfigDefaultValueSaveOptionMibEntry[]= {

{{10,IssConfigDefaultValueSaveOption}, NULL, IssConfigDefaultValueSaveOptionGet, IssConfigDefaultValueSaveOptionSet, IssConfigDefaultValueSaveOptionTest, IssConfigDefaultValueSaveOptionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData IssConfigDefaultValueSaveOptionEntry = { 1, IssConfigDefaultValueSaveOptionMibEntry };

tMbDbEntry IssConfigSaveIpAddrTypeMibEntry[]= {

{{10,IssConfigSaveIpAddrType}, NULL, IssConfigSaveIpAddrTypeGet, IssConfigSaveIpAddrTypeSet, IssConfigSaveIpAddrTypeTest, IssConfigSaveIpAddrTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssConfigSaveIpAddrTypeEntry = { 1, IssConfigSaveIpAddrTypeMibEntry };

tMbDbEntry IssConfigSaveIpvxAddrMibEntry[]= {

{{10,IssConfigSaveIpvxAddr}, NULL, IssConfigSaveIpvxAddrGet, IssConfigSaveIpvxAddrSet, IssConfigSaveIpvxAddrTest, IssConfigSaveIpvxAddrDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssConfigSaveIpvxAddrEntry = { 1, IssConfigSaveIpvxAddrMibEntry };

tMbDbEntry IssConfigRestoreIpAddrTypeMibEntry[]= {

{{10,IssConfigRestoreIpAddrType}, NULL, IssConfigRestoreIpAddrTypeGet, IssConfigRestoreIpAddrTypeSet, IssConfigRestoreIpAddrTypeTest, IssConfigRestoreIpAddrTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssConfigRestoreIpAddrTypeEntry = { 1, IssConfigRestoreIpAddrTypeMibEntry };

tMbDbEntry IssConfigRestoreIpvxAddrMibEntry[]= {

{{10,IssConfigRestoreIpvxAddr}, NULL, IssConfigRestoreIpvxAddrGet, IssConfigRestoreIpvxAddrSet, IssConfigRestoreIpvxAddrTest, IssConfigRestoreIpvxAddrDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssConfigRestoreIpvxAddrEntry = { 1, IssConfigRestoreIpvxAddrMibEntry };

tMbDbEntry IssDlImageFromIpAddrTypeMibEntry[]= {

{{10,IssDlImageFromIpAddrType}, NULL, IssDlImageFromIpAddrTypeGet, IssDlImageFromIpAddrTypeSet, IssDlImageFromIpAddrTypeTest, IssDlImageFromIpAddrTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssDlImageFromIpAddrTypeEntry = { 1, IssDlImageFromIpAddrTypeMibEntry };

tMbDbEntry IssDlImageFromIpvxMibEntry[]= {

{{10,IssDlImageFromIpvx}, NULL, IssDlImageFromIpvxGet, IssDlImageFromIpvxSet, IssDlImageFromIpvxTest, IssDlImageFromIpvxDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssDlImageFromIpvxEntry = { 1, IssDlImageFromIpvxMibEntry };

tMbDbEntry IssUploadLogFileToIpAddrTypeMibEntry[]= {

{{10,IssUploadLogFileToIpAddrType}, NULL, IssUploadLogFileToIpAddrTypeGet, IssUploadLogFileToIpAddrTypeSet, IssUploadLogFileToIpAddrTypeTest, IssUploadLogFileToIpAddrTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssUploadLogFileToIpAddrTypeEntry = { 1, IssUploadLogFileToIpAddrTypeMibEntry };

tMbDbEntry IssUploadLogFileToIpvxMibEntry[]= {

{{10,IssUploadLogFileToIpvx}, NULL, IssUploadLogFileToIpvxGet, IssUploadLogFileToIpvxSet, IssUploadLogFileToIpvxTest, IssUploadLogFileToIpvxDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssUploadLogFileToIpvxEntry = { 1, IssUploadLogFileToIpvxMibEntry };

tMbDbEntry IssAuditLogRemoteIpAddrTypeMibEntry[]= {

{{10,IssAuditLogRemoteIpAddrType}, NULL, IssAuditLogRemoteIpAddrTypeGet, IssAuditLogRemoteIpAddrTypeSet, IssAuditLogRemoteIpAddrTypeTest, IssAuditLogRemoteIpAddrTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssAuditLogRemoteIpAddrTypeEntry = { 1, IssAuditLogRemoteIpAddrTypeMibEntry };

tMbDbEntry IssAuditLogRemoteIpvxAddrMibEntry[]= {

{{10,IssAuditLogRemoteIpvxAddr}, NULL, IssAuditLogRemoteIpvxAddrGet, IssAuditLogRemoteIpvxAddrSet, IssAuditLogRemoteIpvxAddrTest, IssAuditLogRemoteIpvxAddrDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssAuditLogRemoteIpvxAddrEntry = { 1, IssAuditLogRemoteIpvxAddrMibEntry };

tMbDbEntry IssSystemTimerSpeedMibEntry[]= {

{{10,IssSystemTimerSpeed}, NULL, IssSystemTimerSpeedGet, IssSystemTimerSpeedSet, IssSystemTimerSpeedTest, IssSystemTimerSpeedDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssSystemTimerSpeedEntry = { 1, IssSystemTimerSpeedMibEntry };

tMbDbEntry IssMgmtInterfaceRoutingMibEntry[]= {

{{10,IssMgmtInterfaceRouting}, NULL, IssMgmtInterfaceRoutingGet, IssMgmtInterfaceRoutingSet, IssMgmtInterfaceRoutingTest, IssMgmtInterfaceRoutingDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData IssMgmtInterfaceRoutingEntry = { 1, IssMgmtInterfaceRoutingMibEntry };

tMbDbEntry IssMacLearnRateLimitMibEntry[]= {

{{10,IssMacLearnRateLimit}, NULL, IssMacLearnRateLimitGet, IssMacLearnRateLimitSet, IssMacLearnRateLimitTest, IssMacLearnRateLimitDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "1000"},
};
tMibData IssMacLearnRateLimitEntry = { 1, IssMacLearnRateLimitMibEntry };

tMbDbEntry IssMacLearnRateLimitIntervalMibEntry[]= {

{{10,IssMacLearnRateLimitInterval}, NULL, IssMacLearnRateLimitIntervalGet, IssMacLearnRateLimitIntervalSet, IssMacLearnRateLimitIntervalTest, IssMacLearnRateLimitIntervalDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "1000"},
};
tMibData IssMacLearnRateLimitIntervalEntry = { 1, IssMacLearnRateLimitIntervalMibEntry };

tMbDbEntry IssVrfUnqMacFlagMibEntry[]= {

{{10,IssVrfUnqMacFlag}, NULL, IssVrfUnqMacFlagGet, IssVrfUnqMacFlagSet, IssVrfUnqMacFlagTest, IssVrfUnqMacFlagDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},
};
tMibData IssVrfUnqMacFlagEntry = { 1, IssVrfUnqMacFlagMibEntry };

tMbDbEntry IssLoginAttemptsMibEntry[]= {

{{10,IssLoginAttempts}, NULL, IssLoginAttemptsGet, IssLoginAttemptsSet, IssLoginAttemptsTest, IssLoginAttemptsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "3"},
};
tMibData IssLoginAttemptsEntry = { 1, IssLoginAttemptsMibEntry };

tMbDbEntry IssLoginLockTimeMibEntry[]= {

{{10,IssLoginLockTime}, NULL, IssLoginLockTimeGet, IssLoginLockTimeSet, IssLoginLockTimeTest, IssLoginLockTimeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "30"},
};
tMibData IssLoginLockTimeEntry = { 1, IssLoginLockTimeMibEntry };

tMbDbEntry IssAuditLogSizeThresholdMibEntry[]= {

{{10,IssAuditLogSizeThreshold}, NULL, IssAuditLogSizeThresholdGet, IssAuditLogSizeThresholdSet, IssAuditLogSizeThresholdTest, IssAuditLogSizeThresholdDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "70"},
};
tMibData IssAuditLogSizeThresholdEntry = { 1, IssAuditLogSizeThresholdMibEntry };

tMbDbEntry IssTelnetStatusMibEntry[]= {

{{10,IssTelnetStatus}, NULL, IssTelnetStatusGet, IssTelnetStatusSet, IssTelnetStatusTest, IssTelnetStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData IssTelnetStatusEntry = { 1, IssTelnetStatusMibEntry };

tMbDbEntry IssWebSessionTimeOutMibEntry[]= {

{{10,IssWebSessionTimeOut}, NULL, IssWebSessionTimeOutGet, IssWebSessionTimeOutSet, IssWebSessionTimeOutTest, IssWebSessionTimeOutDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "300"},
};
tMibData IssWebSessionTimeOutEntry = { 1, IssWebSessionTimeOutMibEntry };

tMbDbEntry IssWebSessionMaxUsersMibEntry[]= {

{{10,IssWebSessionMaxUsers}, NULL, IssWebSessionMaxUsersGet, IssWebSessionMaxUsersSet, IssWebSessionMaxUsersTest, IssWebSessionMaxUsersDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "10"},
};
tMibData IssWebSessionMaxUsersEntry = { 1, IssWebSessionMaxUsersMibEntry };

tMbDbEntry IssHeartBeatModeMibEntry[]= {

{{10,IssHeartBeatMode}, NULL, IssHeartBeatModeGet, IssHeartBeatModeSet, IssHeartBeatModeTest, IssHeartBeatModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData IssHeartBeatModeEntry = { 1, IssHeartBeatModeMibEntry };

tMbDbEntry IssRmRTypeMibEntry[]= {

{{10,IssRmRType}, NULL, IssRmRTypeGet, IssRmRTypeSet, IssRmRTypeTest, IssRmRTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData IssRmRTypeEntry = { 1, IssRmRTypeMibEntry };

tMbDbEntry IssRmDTypeMibEntry[]= {

{{10,IssRmDType}, NULL, IssRmDTypeGet, IssRmDTypeSet, IssRmDTypeTest, IssRmDTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData IssRmDTypeEntry = { 1, IssRmDTypeMibEntry };

tMbDbEntry IssClearConfigMibEntry[]= {

{{10,IssClearConfig}, NULL, IssClearConfigGet, IssClearConfigSet, IssClearConfigTest, IssClearConfigDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData IssClearConfigEntry = { 1, IssClearConfigMibEntry };

tMbDbEntry IssClearConfigFileNameMibEntry[]= {

{{10,IssClearConfigFileName}, NULL, IssClearConfigFileNameGet, IssClearConfigFileNameSet, IssClearConfigFileNameTest, IssClearConfigFileNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "clear.conf"},
};
tMibData IssClearConfigFileNameEntry = { 1, IssClearConfigFileNameMibEntry };

tMbDbEntry IssTelnetClientStatusMibEntry[]= {

{{10,IssTelnetClientStatus}, NULL, IssTelnetClientStatusGet, IssTelnetClientStatusSet, IssTelnetClientStatusTest, IssTelnetClientStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData IssTelnetClientStatusEntry = { 1, IssTelnetClientStatusMibEntry };

tMbDbEntry IssSshClientStatusMibEntry[]= {

{{10,IssSshClientStatus}, NULL, IssSshClientStatusGet, IssSshClientStatusSet, IssSshClientStatusTest, IssSshClientStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData IssSshClientStatusEntry = { 1, IssSshClientStatusMibEntry };

tMbDbEntry IssActiveTelnetClientSessionsMibEntry[]= {

{{10,IssActiveTelnetClientSessions}, NULL, IssActiveTelnetClientSessionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData IssActiveTelnetClientSessionsEntry = { 1, IssActiveTelnetClientSessionsMibEntry };

tMbDbEntry IssActiveSshClientSessionsMibEntry[]= {

{{10,IssActiveSshClientSessions}, NULL, IssActiveSshClientSessionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData IssActiveSshClientSessionsEntry = { 1, IssActiveSshClientSessionsMibEntry };

tMbDbEntry IssLogFileSizeMibEntry[]= {

{{10,IssLogFileSize}, NULL, IssLogFileSizeGet, IssLogFileSizeSet, IssLogFileSizeTest, IssLogFileSizeDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "1048576"},
};
tMibData IssLogFileSizeEntry = { 1, IssLogFileSizeMibEntry };

tMbDbEntry IssLogResetMibEntry[]= {

{{10,IssLogReset}, NULL, IssLogResetGet, IssLogResetSet, IssLogResetTest, IssLogResetDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData IssLogResetEntry = { 1, IssLogResetMibEntry };

tMbDbEntry IssLogSizeThresholdMibEntry[]= {

{{10,IssLogSizeThreshold}, NULL, IssLogSizeThresholdGet, IssLogSizeThresholdSet, IssLogSizeThresholdTest, IssLogSizeThresholdDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "70"},
};
tMibData IssLogSizeThresholdEntry = { 1, IssLogSizeThresholdMibEntry };

tMbDbEntry IssAutomaticPortCreateMibEntry [] = {

{{10,IssAutomaticPortCreate}, NULL, IssAutomaticPortCreateGet, IssAutomaticPortCreateSet, IssAutomaticPortCreateTest, IssAutomaticPortCreateDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData IssAutomaticPortCreateEntry = { 1, IssAutomaticPortCreateMibEntry };

tMbDbEntry IssUlRemoteLogFileNameMibEntry [] = {

{{10,IssUlRemoteLogFileName}, NULL, IssUlRemoteLogFileNameGet, IssUlRemoteLogFileNameSet, IssUlRemoteLogFileNameTest, IssUlRemoteLogFileNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "iss.log"},
};
tMibData IssUlRemoteLogFileNameEntry = { 1, IssUlRemoteLogFileNameMibEntry };

tMbDbEntry IssDefaultExecTimeOutMibEntry [] = {

{{10,IssDefaultExecTimeOut}, NULL, IssDefaultExecTimeOutGet, IssDefaultExecTimeOutSet, IssDefaultExecTimeOutTest, IssDefaultExecTimeOutDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssDefaultExecTimeOutEntry = { 1, IssDefaultExecTimeOutMibEntry };

tMbDbEntry IssRmStackingInterfaceTypeMibEntry[]= {

{{10,IssRmStackingInterfaceType}, NULL, IssRmStackingInterfaceTypeGet, IssRmStackingInterfaceTypeSet, IssRmStackingInterfaceTypeTest, IssRmStackingInterfaceTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData IssRmStackingInterfaceTypeEntry = { 1, IssRmStackingInterfaceTypeMibEntry };

tMbDbEntry IssPeerLoggingOptionMibEntry[]= {

{{10,IssPeerLoggingOption}, NULL, IssPeerLoggingOptionGet, IssPeerLoggingOptionSet, IssPeerLoggingOptionTest, IssPeerLoggingOptionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData IssPeerLoggingOptionEntry = { 1, IssPeerLoggingOptionMibEntry };

tMbDbEntry IssStandbyRestartMibEntry[]= {
{{10,IssStandbyRestart}, NULL, IssStandbyRestartGet,IssStandbyRestartSet,IssStandbyRestartTest,IssStandbyRestartDep,SNMP_DATA_TYPE_INTEGER,SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData IssStandbyRestartEntry = {1, IssStandbyRestartMibEntry };

tMbDbEntry IssRestoreTypeMibEntry[]= {

{{10,IssRestoreType}, NULL, IssRestoreTypeGet, IssRestoreTypeSet, IssRestoreTypeTest, IssRestoreTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData IssRestoreTypeEntry = { 1, IssRestoreTypeMibEntry };


tMbDbEntry IssSwitchModeTypeMibEntry[]={

{{10,IssSwitchModeType}, NULL, IssSwitchModeTypeGet, IssSwitchModeTypeSet, IssSwitchModeTypeTest, IssSwitchModeTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};

tMibData IssSwitchModeTypeEntry = {1, IssSwitchModeTypeMibEntry };
tMbDbEntry IssDebugTimeStampOptionMibEntry[]= {

{{10,IssDebugTimeStampOption}, NULL, IssDebugTimeStampOptionGet, IssDebugTimeStampOptionSet, IssDebugTimeStampOptionTest, IssDebugTimeStampOptionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData IssDebugTimeStampOptionEntry = { 1, IssDebugTimeStampOptionMibEntry };

tMbDbEntry IssConfigCtrlTableMibEntry[]= {

{{12,IssConfigCtrlIndex}, GetNextIndexIssConfigCtrlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssConfigCtrlTableINDEX, 1, 0, 0, NULL},

{{12,IssConfigCtrlEgressStatus}, GetNextIndexIssConfigCtrlTable, IssConfigCtrlEgressStatusGet, IssConfigCtrlEgressStatusSet, IssConfigCtrlEgressStatusTest, IssConfigCtrlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssConfigCtrlTableINDEX, 1, 0, 0, "1"},

{{12,IssConfigCtrlStatsCollection}, GetNextIndexIssConfigCtrlTable, IssConfigCtrlStatsCollectionGet, IssConfigCtrlStatsCollectionSet, IssConfigCtrlStatsCollectionTest, IssConfigCtrlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssConfigCtrlTableINDEX, 1, 0, 0, "1"},

{{12,IssConfigCtrlStatus}, GetNextIndexIssConfigCtrlTable, IssConfigCtrlStatusGet, IssConfigCtrlStatusSet, IssConfigCtrlStatusTest, IssConfigCtrlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssConfigCtrlTableINDEX, 1, 0, 0, NULL},
};
tMibData IssConfigCtrlTableEntry = { 4, IssConfigCtrlTableMibEntry };

tMbDbEntry IssPortCtrlTableMibEntry[]= {

{{12,IssPortCtrlIndex}, GetNextIndexIssPortCtrlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssPortCtrlTableINDEX, 1, 0, 0, NULL},

{{12,IssPortCtrlMode}, GetNextIndexIssPortCtrlTable, IssPortCtrlModeGet, IssPortCtrlModeSet, IssPortCtrlModeTest, IssPortCtrlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssPortCtrlTableINDEX, 1, 0, 0, "1"},

{{12,IssPortCtrlDuplex}, GetNextIndexIssPortCtrlTable, IssPortCtrlDuplexGet, IssPortCtrlDuplexSet, IssPortCtrlDuplexTest, IssPortCtrlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssPortCtrlTableINDEX, 1, 0, 0, NULL},

{{12,IssPortCtrlSpeed}, GetNextIndexIssPortCtrlTable, IssPortCtrlSpeedGet, IssPortCtrlSpeedSet, IssPortCtrlSpeedTest, IssPortCtrlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssPortCtrlTableINDEX, 1, 0, 0, NULL},

{{12,IssPortCtrlFlowControl}, GetNextIndexIssPortCtrlTable, IssPortCtrlFlowControlGet, IssPortCtrlFlowControlSet, IssPortCtrlFlowControlTest, IssPortCtrlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssPortCtrlTableINDEX, 1, 1, 0, NULL},

{{12,IssPortCtrlRenegotiate}, GetNextIndexIssPortCtrlTable, IssPortCtrlRenegotiateGet, IssPortCtrlRenegotiateSet, IssPortCtrlRenegotiateTest, IssPortCtrlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssPortCtrlTableINDEX, 1, 0, 0, "2"},

{{12,IssPortCtrlMaxMacAddr}, GetNextIndexIssPortCtrlTable, IssPortCtrlMaxMacAddrGet, IssPortCtrlMaxMacAddrSet, IssPortCtrlMaxMacAddrTest, IssPortCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssPortCtrlTableINDEX, 1, 0, 0, NULL},

{{12,IssPortCtrlMaxMacAction}, GetNextIndexIssPortCtrlTable, IssPortCtrlMaxMacActionGet, IssPortCtrlMaxMacActionSet, IssPortCtrlMaxMacActionTest, IssPortCtrlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssPortCtrlTableINDEX, 1, 0, 0, NULL},

{{12,IssPortHOLBlockPrevention}, GetNextIndexIssPortCtrlTable, IssPortHOLBlockPreventionGet, IssPortHOLBlockPreventionSet, IssPortHOLBlockPreventionTest, IssPortCtrlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssPortCtrlTableINDEX, 1, 0, 0, "2"},

{{12,IssPortAutoNegAdvtCapBits}, GetNextIndexIssPortCtrlTable, IssPortAutoNegAdvtCapBitsGet, IssPortAutoNegAdvtCapBitsSet, IssPortAutoNegAdvtCapBitsTest, IssPortCtrlTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssPortCtrlTableINDEX, 1, 0, 0, NULL},

{{12,IssPortCpuControlledLearning}, GetNextIndexIssPortCtrlTable, IssPortCpuControlledLearningGet, IssPortCpuControlledLearningSet, IssPortCpuControlledLearningTest, IssPortCtrlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssPortCtrlTableINDEX, 1, 0, 0, "1"},

{{12,IssPortMdiOrMdixCap}, GetNextIndexIssPortCtrlTable, IssPortMdiOrMdixCapGet, IssPortMdiOrMdixCapSet, IssPortMdiOrMdixCapTest, IssPortCtrlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssPortCtrlTableINDEX, 1, 0, 0, "1"},

{{12,IssPortCtrlFlowControlMaxRate}, GetNextIndexIssPortCtrlTable, IssPortCtrlFlowControlMaxRateGet, IssPortCtrlFlowControlMaxRateSet, IssPortCtrlFlowControlMaxRateTest, IssPortCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssPortCtrlTableINDEX, 1, 0, 0, NULL},

{{12,IssPortCtrlFlowControlMinRate}, GetNextIndexIssPortCtrlTable, IssPortCtrlFlowControlMinRateGet, IssPortCtrlFlowControlMinRateSet, IssPortCtrlFlowControlMinRateTest, IssPortCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssPortCtrlTableINDEX, 1, 0, 0, NULL},
};
tMibData IssPortCtrlTableEntry = { 14, IssPortCtrlTableMibEntry };

tMbDbEntry IssPortIsolationTableMibEntry[]= {

{{12,IssPortIsolationIngressPort}, GetNextIndexIssPortIsolationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IssPortIsolationTableINDEX, 3, 0, 0, NULL},

{{12,IssPortIsolationInVlanId}, GetNextIndexIssPortIsolationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssPortIsolationTableINDEX, 3, 0, 0, NULL},

{{12,IssPortIsolationEgressPort}, GetNextIndexIssPortIsolationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, IssPortIsolationTableINDEX, 3, 0, 0, NULL},

{{12,IssPortIsolationStorageType}, GetNextIndexIssPortIsolationTable, IssPortIsolationStorageTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IssPortIsolationTableINDEX, 3, 0, 0, NULL},

{{12,IssPortIsolationRowStatus}, GetNextIndexIssPortIsolationTable, IssPortIsolationRowStatusGet, IssPortIsolationRowStatusSet, IssPortIsolationRowStatusTest, IssPortIsolationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssPortIsolationTableINDEX, 3, 0, 1, NULL},
};
tMibData IssPortIsolationTableEntry = { 5, IssPortIsolationTableMibEntry };

tMbDbEntry IssMirrorStatusMibEntry[]= {

{{10,IssMirrorStatus}, NULL, IssMirrorStatusGet, IssMirrorStatusSet, IssMirrorStatusTest, IssMirrorStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData IssMirrorStatusEntry = { 1, IssMirrorStatusMibEntry };

tMbDbEntry IssMirrorToPortMibEntry[]= {

{{10,IssMirrorToPort}, NULL, IssMirrorToPortGet, IssMirrorToPortSet, IssMirrorToPortTest, IssMirrorToPortDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssMirrorToPortEntry = { 1, IssMirrorToPortMibEntry };

tMbDbEntry IssCpuMirrorTypeMibEntry[]= {

{{10,IssCpuMirrorType}, NULL, IssCpuMirrorTypeGet, IssCpuMirrorTypeSet, IssCpuMirrorTypeTest, IssCpuMirrorTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "4"},
};
tMibData IssCpuMirrorTypeEntry = { 1, IssCpuMirrorTypeMibEntry };

tMbDbEntry IssCpuMirrorToPortMibEntry[]= {

{{10,IssCpuMirrorToPort}, NULL, IssCpuMirrorToPortGet, IssCpuMirrorToPortSet, IssCpuMirrorToPortTest, IssCpuMirrorToPortDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssCpuMirrorToPortEntry = { 1, IssCpuMirrorToPortMibEntry };




tMbDbEntry IssMirrorCtrlTableMibEntry[]= {

{{12,IssMirrorCtrlIndex}, GetNextIndexIssMirrorCtrlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssMirrorCtrlTableINDEX, 1, 0, 0, NULL},

{{12,IssMirrorCtrlIngressMirroring}, GetNextIndexIssMirrorCtrlTable, IssMirrorCtrlIngressMirroringGet, IssMirrorCtrlIngressMirroringSet, IssMirrorCtrlIngressMirroringTest, IssMirrorCtrlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssMirrorCtrlTableINDEX, 1, 0, 0, "2"},

{{12,IssMirrorCtrlEgressMirroring}, GetNextIndexIssMirrorCtrlTable, IssMirrorCtrlEgressMirroringGet, IssMirrorCtrlEgressMirroringSet, IssMirrorCtrlEgressMirroringTest, IssMirrorCtrlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssMirrorCtrlTableINDEX, 1, 0, 0, "2"},

{{12,IssMirrorCtrlStatus}, GetNextIndexIssMirrorCtrlTable, IssMirrorCtrlStatusGet, IssMirrorCtrlStatusSet, IssMirrorCtrlStatusTest, IssMirrorCtrlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssMirrorCtrlTableINDEX, 1, 0, 0, NULL},
};
tMibData IssMirrorCtrlTableEntry = { 4, IssMirrorCtrlTableMibEntry };

tMbDbEntry IssMirrorCtrlRemainingSrcRcrdsMibEntry[]= {

{{10,IssMirrorCtrlRemainingSrcRcrds}, NULL, IssMirrorCtrlRemainingSrcRcrdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData IssMirrorCtrlRemainingSrcRcrdsEntry = { 1, IssMirrorCtrlRemainingSrcRcrdsMibEntry };

tMbDbEntry IssMirrorCtrlRemainingDestRcrdsMibEntry[]= {

{{10,IssMirrorCtrlRemainingDestRcrds}, NULL, IssMirrorCtrlRemainingDestRcrdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData IssMirrorCtrlRemainingDestRcrdsEntry = { 1, IssMirrorCtrlRemainingDestRcrdsMibEntry };

tMbDbEntry IssMirrorCtrlExtnTableMibEntry[]= {

{{12,IssMirrorCtrlExtnSessionIndex}, GetNextIndexIssMirrorCtrlExtnTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssMirrorCtrlExtnTableINDEX, 1, 0, 0, NULL},

{{12,IssMirrorCtrlExtnMirrType}, GetNextIndexIssMirrorCtrlExtnTable, IssMirrorCtrlExtnMirrTypeGet, IssMirrorCtrlExtnMirrTypeSet, IssMirrorCtrlExtnMirrTypeTest, IssMirrorCtrlExtnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssMirrorCtrlExtnTableINDEX, 1, 0, 0, "4"},

{{12,IssMirrorCtrlExtnRSpanStatus}, GetNextIndexIssMirrorCtrlExtnTable, IssMirrorCtrlExtnRSpanStatusGet, IssMirrorCtrlExtnRSpanStatusSet, IssMirrorCtrlExtnRSpanStatusTest, IssMirrorCtrlExtnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssMirrorCtrlExtnTableINDEX, 1, 0, 0, "3"},

{{12,IssMirrorCtrlExtnRSpanVlanId}, GetNextIndexIssMirrorCtrlExtnTable, IssMirrorCtrlExtnRSpanVlanIdGet, IssMirrorCtrlExtnRSpanVlanIdSet, IssMirrorCtrlExtnRSpanVlanIdTest, IssMirrorCtrlExtnTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssMirrorCtrlExtnTableINDEX, 1, 0, 0, "0"},

{{12,IssMirrorCtrlExtnRSpanContext}, GetNextIndexIssMirrorCtrlExtnTable, IssMirrorCtrlExtnRSpanContextGet, IssMirrorCtrlExtnRSpanContextSet, IssMirrorCtrlExtnRSpanContextTest, IssMirrorCtrlExtnTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssMirrorCtrlExtnTableINDEX, 1, 0, 0, "0"},

{{12,IssMirrorCtrlExtnStatus}, GetNextIndexIssMirrorCtrlExtnTable, IssMirrorCtrlExtnStatusGet, IssMirrorCtrlExtnStatusSet, IssMirrorCtrlExtnStatusTest, IssMirrorCtrlExtnTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssMirrorCtrlExtnTableINDEX, 1, 0, 1, NULL},
};
tMibData IssMirrorCtrlExtnTableEntry = { 6, IssMirrorCtrlExtnTableMibEntry };

tMbDbEntry IssMirrorCtrlExtnSrcTableMibEntry[]= {

{{12,IssMirrorCtrlExtnSrcId}, GetNextIndexIssMirrorCtrlExtnSrcTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssMirrorCtrlExtnSrcTableINDEX, 2, 0, 0, NULL},

{{12,IssMirrorCtrlExtnSrcCfg}, GetNextIndexIssMirrorCtrlExtnSrcTable, IssMirrorCtrlExtnSrcCfgGet, IssMirrorCtrlExtnSrcCfgSet, IssMirrorCtrlExtnSrcCfgTest, IssMirrorCtrlExtnSrcTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssMirrorCtrlExtnSrcTableINDEX, 2, 0, 0, NULL},

{{12,IssMirrorCtrlExtnSrcMode}, GetNextIndexIssMirrorCtrlExtnSrcTable, IssMirrorCtrlExtnSrcModeGet, IssMirrorCtrlExtnSrcModeSet, IssMirrorCtrlExtnSrcModeTest, IssMirrorCtrlExtnSrcTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssMirrorCtrlExtnSrcTableINDEX, 2, 0, 0, "3"},
};
tMibData IssMirrorCtrlExtnSrcTableEntry = { 3, IssMirrorCtrlExtnSrcTableMibEntry };

tMbDbEntry IssMirrorCtrlExtnSrcVlanTableMibEntry[]= {

{{12,IssMirrorCtrlExtnSrcVlanContext}, GetNextIndexIssMirrorCtrlExtnSrcVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssMirrorCtrlExtnSrcVlanTableINDEX, 3, 0, 0, NULL},

{{12,IssMirrorCtrlExtnSrcVlanId}, GetNextIndexIssMirrorCtrlExtnSrcVlanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssMirrorCtrlExtnSrcVlanTableINDEX, 3, 0, 0, NULL},

{{12,IssMirrorCtrlExtnSrcVlanCfg}, GetNextIndexIssMirrorCtrlExtnSrcVlanTable, IssMirrorCtrlExtnSrcVlanCfgGet, IssMirrorCtrlExtnSrcVlanCfgSet, IssMirrorCtrlExtnSrcVlanCfgTest, IssMirrorCtrlExtnSrcVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssMirrorCtrlExtnSrcVlanTableINDEX, 3, 0, 0, NULL},

{{12,IssMirrorCtrlExtnSrcVlanMode}, GetNextIndexIssMirrorCtrlExtnSrcVlanTable, IssMirrorCtrlExtnSrcVlanModeGet, IssMirrorCtrlExtnSrcVlanModeSet, IssMirrorCtrlExtnSrcVlanModeTest, IssMirrorCtrlExtnSrcVlanTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssMirrorCtrlExtnSrcVlanTableINDEX, 3, 0, 0, "3"},
};
tMibData IssMirrorCtrlExtnSrcVlanTableEntry = { 4, IssMirrorCtrlExtnSrcVlanTableMibEntry };

tMbDbEntry IssMirrorCtrlExtnDestinationTableMibEntry[]= {

{{12,IssMirrorCtrlExtnDestination}, GetNextIndexIssMirrorCtrlExtnDestinationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssMirrorCtrlExtnDestinationTableINDEX, 2, 0, 0, NULL},

{{12,IssMirrorCtrlExtnDestCfg}, GetNextIndexIssMirrorCtrlExtnDestinationTable, IssMirrorCtrlExtnDestCfgGet, IssMirrorCtrlExtnDestCfgSet, IssMirrorCtrlExtnDestCfgTest, IssMirrorCtrlExtnDestinationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssMirrorCtrlExtnDestinationTableINDEX, 2, 0, 0, NULL},
};
tMibData IssMirrorCtrlExtnDestinationTableEntry = { 2, IssMirrorCtrlExtnDestinationTableMibEntry };

tMbDbEntry IssRateCtrlTableMibEntry[]= {

{{12,IssRateCtrlIndex}, GetNextIndexIssRateCtrlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssRateCtrlTableINDEX, 1, 1, 0, NULL},

{{12,IssRateCtrlDLFLimitValue}, GetNextIndexIssRateCtrlTable, IssRateCtrlDLFLimitValueGet, IssRateCtrlDLFLimitValueSet, IssRateCtrlDLFLimitValueTest, IssRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssRateCtrlTableINDEX, 1, 1, 0, "0"},

{{12,IssRateCtrlBCASTLimitValue}, GetNextIndexIssRateCtrlTable, IssRateCtrlBCASTLimitValueGet, IssRateCtrlBCASTLimitValueSet, IssRateCtrlBCASTLimitValueTest, IssRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssRateCtrlTableINDEX, 1, 1, 0, "0"},

{{12,IssRateCtrlMCASTLimitValue}, GetNextIndexIssRateCtrlTable, IssRateCtrlMCASTLimitValueGet, IssRateCtrlMCASTLimitValueSet, IssRateCtrlMCASTLimitValueTest, IssRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssRateCtrlTableINDEX, 1, 1, 0, "0"},

{{12,IssRateCtrlPortRateLimit}, GetNextIndexIssRateCtrlTable, IssRateCtrlPortRateLimitGet, IssRateCtrlPortRateLimitSet, IssRateCtrlPortRateLimitTest, IssRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssRateCtrlTableINDEX, 1, 1, 0, NULL},

{{12,IssRateCtrlPortBurstSize}, GetNextIndexIssRateCtrlTable, IssRateCtrlPortBurstSizeGet, IssRateCtrlPortBurstSizeSet, IssRateCtrlPortBurstSizeTest, IssRateCtrlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssRateCtrlTableINDEX, 1, 1, 0, NULL},
};
tMibData IssRateCtrlTableEntry = { 6, IssRateCtrlTableMibEntry };

tMbDbEntry IssL2FilterTableMibEntry[]= {

{{12,IssL2FilterNo}, GetNextIndexIssL2FilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssL2FilterTableINDEX, 1, 0, 0, NULL},

{{12,IssL2FilterPriority}, GetNextIndexIssL2FilterTable, IssL2FilterPriorityGet, IssL2FilterPrioritySet, IssL2FilterPriorityTest, IssL2FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL2FilterTableINDEX, 1, 0, 0, "1"},

{{12,IssL2FilterEtherType}, GetNextIndexIssL2FilterTable, IssL2FilterEtherTypeGet, IssL2FilterEtherTypeSet, IssL2FilterEtherTypeTest, IssL2FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL2FilterTableINDEX, 1, 1, 0, "0"},

{{12,IssL2FilterProtocolType}, GetNextIndexIssL2FilterTable, IssL2FilterProtocolTypeGet, IssL2FilterProtocolTypeSet, IssL2FilterProtocolTypeTest, IssL2FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssL2FilterTableINDEX, 1, 1, 0, "0"},

{{12,IssL2FilterDstMacAddr}, GetNextIndexIssL2FilterTable, IssL2FilterDstMacAddrGet, IssL2FilterDstMacAddrSet, IssL2FilterDstMacAddrTest, IssL2FilterTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, IssL2FilterTableINDEX, 1, 1, 0, NULL},

{{12,IssL2FilterSrcMacAddr}, GetNextIndexIssL2FilterTable, IssL2FilterSrcMacAddrGet, IssL2FilterSrcMacAddrSet, IssL2FilterSrcMacAddrTest, IssL2FilterTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, IssL2FilterTableINDEX, 1, 1, 0, NULL},

{{12,IssL2FilterVlanId}, GetNextIndexIssL2FilterTable, IssL2FilterVlanIdGet, IssL2FilterVlanIdSet, IssL2FilterVlanIdTest, IssL2FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL2FilterTableINDEX, 1, 1, 0, "0"},

{{12,IssL2FilterInPortList}, GetNextIndexIssL2FilterTable, IssL2FilterInPortListGet, IssL2FilterInPortListSet, IssL2FilterInPortListTest, IssL2FilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssL2FilterTableINDEX, 1, 1, 0, NULL},

{{12,IssL2FilterAction}, GetNextIndexIssL2FilterTable, IssL2FilterActionGet, IssL2FilterActionSet, IssL2FilterActionTest, IssL2FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssL2FilterTableINDEX, 1, 1, 0, "1"},

{{12,IssL2FilterMatchCount}, GetNextIndexIssL2FilterTable, IssL2FilterMatchCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IssL2FilterTableINDEX, 1, 1, 0, NULL},

{{12,IssL2FilterStatus}, GetNextIndexIssL2FilterTable, IssL2FilterStatusGet, IssL2FilterStatusSet, IssL2FilterStatusTest, IssL2FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssL2FilterTableINDEX, 1, 1, 1, NULL},

{{12,IssL2FilterOutPortList}, GetNextIndexIssL2FilterTable, IssL2FilterOutPortListGet, IssL2FilterOutPortListSet, IssL2FilterOutPortListTest, IssL2FilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssL2FilterTableINDEX, 1, 1, 0, NULL},

{{12,IssL2FilterDirection}, GetNextIndexIssL2FilterTable, IssL2FilterDirectionGet, IssL2FilterDirectionSet, IssL2FilterDirectionTest, IssL2FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssL2FilterTableINDEX, 1, 1, 0, "1"},
};
tMibData IssL2FilterTableEntry = { 13, IssL2FilterTableMibEntry };

tMbDbEntry IssL3FilterTableMibEntry[]= {

{{12,IssL3FilterNo}, GetNextIndexIssL3FilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssL3FilterTableINDEX, 1, 1, 0, NULL},

{{12,IssL3FilterPriority}, GetNextIndexIssL3FilterTable, IssL3FilterPriorityGet, IssL3FilterPrioritySet, IssL3FilterPriorityTest, IssL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 1, 0, "1"},

{{12,IssL3FilterProtocol}, GetNextIndexIssL3FilterTable, IssL3FilterProtocolGet, IssL3FilterProtocolSet, IssL3FilterProtocolTest, IssL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 1, 0, "255"},

{{12,IssL3FilterMessageType}, GetNextIndexIssL3FilterTable, IssL3FilterMessageTypeGet, IssL3FilterMessageTypeSet, IssL3FilterMessageTypeTest, IssL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 1, 0, "255"},

{{12,IssL3FilterMessageCode}, GetNextIndexIssL3FilterTable, IssL3FilterMessageCodeGet, IssL3FilterMessageCodeSet, IssL3FilterMessageCodeTest, IssL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 1, 0, "255"},

{{12,IssL3FilterDstIpAddr}, GetNextIndexIssL3FilterTable, IssL3FilterDstIpAddrGet, IssL3FilterDstIpAddrSet, IssL3FilterDstIpAddrTest, IssL3FilterTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 1, 0, "0"},

{{12,IssL3FilterSrcIpAddr}, GetNextIndexIssL3FilterTable, IssL3FilterSrcIpAddrGet, IssL3FilterSrcIpAddrSet, IssL3FilterSrcIpAddrTest, IssL3FilterTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 1, 0, "0"},

{{12,IssL3FilterDstIpAddrMask}, GetNextIndexIssL3FilterTable, IssL3FilterDstIpAddrMaskGet, IssL3FilterDstIpAddrMaskSet, IssL3FilterDstIpAddrMaskTest, IssL3FilterTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 1, 0, "4294967295"},

{{12,IssL3FilterSrcIpAddrMask}, GetNextIndexIssL3FilterTable, IssL3FilterSrcIpAddrMaskGet, IssL3FilterSrcIpAddrMaskSet, IssL3FilterSrcIpAddrMaskTest, IssL3FilterTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 1, 0, "4294967295"},

{{12,IssL3FilterMinDstProtPort}, GetNextIndexIssL3FilterTable, IssL3FilterMinDstProtPortGet, IssL3FilterMinDstProtPortSet, IssL3FilterMinDstProtPortTest, IssL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 1, 0, "0"},

{{12,IssL3FilterMaxDstProtPort}, GetNextIndexIssL3FilterTable, IssL3FilterMaxDstProtPortGet, IssL3FilterMaxDstProtPortSet, IssL3FilterMaxDstProtPortTest, IssL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 1, 0, "65535"},

{{12,IssL3FilterMinSrcProtPort}, GetNextIndexIssL3FilterTable, IssL3FilterMinSrcProtPortGet, IssL3FilterMinSrcProtPortSet, IssL3FilterMinSrcProtPortTest, IssL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 1, 0, "0"},

{{12,IssL3FilterMaxSrcProtPort}, GetNextIndexIssL3FilterTable, IssL3FilterMaxSrcProtPortGet, IssL3FilterMaxSrcProtPortSet, IssL3FilterMaxSrcProtPortTest, IssL3FilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 1, 0, "65535"},

{{12,IssL3FilterInPortList}, GetNextIndexIssL3FilterTable, IssL3FilterInPortListGet, IssL3FilterInPortListSet, IssL3FilterInPortListTest, IssL3FilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 1, 0, NULL},

{{12,IssL3FilterOutPortList}, GetNextIndexIssL3FilterTable, IssL3FilterOutPortListGet, IssL3FilterOutPortListSet, IssL3FilterOutPortListTest, IssL3FilterTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 1, 0, NULL},

{{12,IssL3FilterAckBit}, GetNextIndexIssL3FilterTable, IssL3FilterAckBitGet, IssL3FilterAckBitSet, IssL3FilterAckBitTest, IssL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 1, 0, "3"},

{{12,IssL3FilterRstBit}, GetNextIndexIssL3FilterTable, IssL3FilterRstBitGet, IssL3FilterRstBitSet, IssL3FilterRstBitTest, IssL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 1, 0, "3"},

{{12,IssL3FilterTos}, GetNextIndexIssL3FilterTable, IssL3FilterTosGet, IssL3FilterTosSet, IssL3FilterTosTest, IssL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 1, 0, "-1"},

{{12,IssL3FilterDscp}, GetNextIndexIssL3FilterTable, IssL3FilterDscpGet, IssL3FilterDscpSet, IssL3FilterDscpTest, IssL3FilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 1, 0, "-1"},

{{12,IssL3FilterDirection}, GetNextIndexIssL3FilterTable, IssL3FilterDirectionGet, IssL3FilterDirectionSet, IssL3FilterDirectionTest, IssL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 1, 0, "1"},

{{12,IssL3FilterAction}, GetNextIndexIssL3FilterTable, IssL3FilterActionGet, IssL3FilterActionSet, IssL3FilterActionTest, IssL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 1, 0, "1"},

{{12,IssL3FilterMatchCount}, GetNextIndexIssL3FilterTable, IssL3FilterMatchCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, IssL3FilterTableINDEX, 1, 1, 0, NULL},

{{12,IssL3FilterStatus}, GetNextIndexIssL3FilterTable, IssL3FilterStatusGet, IssL3FilterStatusSet, IssL3FilterStatusTest, IssL3FilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssL3FilterTableINDEX, 1, 1, 1, NULL},
};
tMibData IssL3FilterTableEntry = { 23, IssL3FilterTableMibEntry };

tMbDbEntry IssIpAuthMgrTableMibEntry[]= {

{{12,IssIpAuthMgrIpAddr}, GetNextIndexIssIpAuthMgrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IssIpAuthMgrTableINDEX, 2, 0, 0, NULL},

{{12,IssIpAuthMgrIpMask}, GetNextIndexIssIpAuthMgrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, IssIpAuthMgrTableINDEX, 2, 0, 0, NULL},

{{12,IssIpAuthMgrPortList}, GetNextIndexIssIpAuthMgrTable, IssIpAuthMgrPortListGet, IssIpAuthMgrPortListSet, IssIpAuthMgrPortListTest, IssIpAuthMgrTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssIpAuthMgrTableINDEX, 2, 0, 0, NULL},

{{12,IssIpAuthMgrVlanList}, GetNextIndexIssIpAuthMgrTable, IssIpAuthMgrVlanListGet, IssIpAuthMgrVlanListSet, IssIpAuthMgrVlanListTest, IssIpAuthMgrTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, IssIpAuthMgrTableINDEX, 2, 0, 0, NULL},

{{12,IssIpAuthMgrOOBPort}, GetNextIndexIssIpAuthMgrTable, IssIpAuthMgrOOBPortGet, IssIpAuthMgrOOBPortSet, IssIpAuthMgrOOBPortTest, IssIpAuthMgrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssIpAuthMgrTableINDEX, 2, 0, 0, "2"},

{{12,IssIpAuthMgrAllowedServices}, GetNextIndexIssIpAuthMgrTable, IssIpAuthMgrAllowedServicesGet, IssIpAuthMgrAllowedServicesSet, IssIpAuthMgrAllowedServicesTest, IssIpAuthMgrTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssIpAuthMgrTableINDEX, 2, 0, 0, "31"},

{{12,IssIpAuthMgrRowStatus}, GetNextIndexIssIpAuthMgrTable, IssIpAuthMgrRowStatusGet, IssIpAuthMgrRowStatusSet, IssIpAuthMgrRowStatusTest, IssIpAuthMgrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssIpAuthMgrTableINDEX, 2, 0, 1, NULL},
};
tMibData IssIpAuthMgrTableEntry = { 7, IssIpAuthMgrTableMibEntry };

tMbDbEntry IssL4SwitchingFilterTableMibEntry[]= {

{{12,IssL4SwitchingFilterNo}, GetNextIndexIssL4SwitchingFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssL4SwitchingFilterTableINDEX, 1, 0, 0, NULL},

{{12,IssL4SwitchingProtocol}, GetNextIndexIssL4SwitchingFilterTable, IssL4SwitchingProtocolGet, IssL4SwitchingProtocolSet, IssL4SwitchingProtocolTest, IssL4SwitchingFilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL4SwitchingFilterTableINDEX, 1, 0, 0, "255"},

{{12,IssL4SwitchingPortNo}, GetNextIndexIssL4SwitchingFilterTable, IssL4SwitchingPortNoGet, IssL4SwitchingPortNoSet, IssL4SwitchingPortNoTest, IssL4SwitchingFilterTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, IssL4SwitchingFilterTableINDEX, 1, 0, 0, "0"},

{{12,IssL4SwitchingCopyToPort}, GetNextIndexIssL4SwitchingFilterTable, IssL4SwitchingCopyToPortGet, IssL4SwitchingCopyToPortSet, IssL4SwitchingCopyToPortTest, IssL4SwitchingFilterTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, IssL4SwitchingFilterTableINDEX, 1, 0, 0, NULL},

{{12,IssL4SwitchingFilterStatus}, GetNextIndexIssL4SwitchingFilterTable, IssL4SwitchingFilterStatusGet, IssL4SwitchingFilterStatusSet, IssL4SwitchingFilterStatusTest, IssL4SwitchingFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssL4SwitchingFilterTableINDEX, 1, 0, 1, NULL},
};
tMibData IssL4SwitchingFilterTableEntry = { 5, IssL4SwitchingFilterTableMibEntry };

tMbDbEntry IssMsrFailedOidMibEntry[]= {

{{10,IssMsrFailedOid}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData IssMsrFailedOidEntry = { 1, IssMsrFailedOidMibEntry };

tMbDbEntry IssMsrFailedValueMibEntry[]= {

{{10,IssMsrFailedValue}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData IssMsrFailedValueEntry = { 1, IssMsrFailedValueMibEntry };

tMbDbEntry IssAuditTrapEventMibEntry[]= {

{{10,IssAuditTrapEvent}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData IssAuditTrapEventEntry = { 1, IssAuditTrapEventMibEntry };

tMbDbEntry IssAuditTrapEventTimeMibEntry[]= {

{{10,IssAuditTrapEventTime}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData IssAuditTrapEventTimeEntry = { 1, IssAuditTrapEventTimeMibEntry };

tMbDbEntry IssAuditTrapFileNameMibEntry[]= {

{{10,IssAuditTrapFileName}, NULL, IssAuditTrapFileNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData IssAuditTrapFileNameEntry = { 1, IssAuditTrapFileNameMibEntry };

tMbDbEntry IssModuleTableMibEntry[]= {

{{12,IssModuleId}, GetNextIndexIssModuleTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssModuleTableINDEX, 1, 0, 0, NULL},

{{12,IssModuleSystemControl}, GetNextIndexIssModuleTable, IssModuleSystemControlGet, IssModuleSystemControlSet, IssModuleSystemControlTest, IssModuleTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, IssModuleTableINDEX, 1, 0, 0, "2"},
};
tMibData IssModuleTableEntry = { 2, IssModuleTableMibEntry };

tMbDbEntry IssSwitchFanTableMibEntry[]= {

{{12,IssSwitchFanIndex}, GetNextIndexIssSwitchFanTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, IssSwitchFanTableINDEX, 1, 0, 0, NULL},

{{12,IssSwitchFanStatus}, GetNextIndexIssSwitchFanTable, IssSwitchFanStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, IssSwitchFanTableINDEX, 1, 0, 0, NULL},
};
tMibData IssSwitchFanTableEntry = { 2, IssSwitchFanTableMibEntry };

tMbDbEntry IssAclProvisionModeMibEntry[]= {

{{10,IssAclProvisionMode}, NULL, IssAclProvisionModeGet, IssAclProvisionModeSet, IssAclProvisionModeTest, IssAclProvisionModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData IssAclProvisionModeEntry = { 1, IssAclProvisionModeMibEntry };

tMbDbEntry IssAclTriggerCommitMibEntry[]= {

{{10,IssAclTriggerCommit}, NULL, IssAclTriggerCommitGet, IssAclTriggerCommitSet, IssAclTriggerCommitTest, IssAclTriggerCommitDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},
};
tMibData IssAclTriggerCommitEntry = { 1, IssAclTriggerCommitMibEntry };

tMbDbEntry IssLogTrapEventMibEntry[]= {

{{10,IssLogTrapEvent}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData IssLogTrapEventEntry = { 1, IssLogTrapEventMibEntry };

tMbDbEntry IssLogTrapEventTimeMibEntry[]= {

{{10,IssLogTrapEventTime}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData IssLogTrapEventTimeEntry = { 1, IssLogTrapEventTimeMibEntry };

tMbDbEntry IssLogTrapFileNameMibEntry[]= {

{{10,IssLogTrapFileName}, NULL, IssLogTrapFileNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData IssLogTrapFileNameEntry = { 1, IssLogTrapFileNameMibEntry };

tMbDbEntry IssAclTrafficSeperationCtrlMibEntry[]= {

{{10,IssAclTrafficSeperationCtrl}, NULL, IssAclTrafficSeperationCtrlGet, IssAclTrafficSeperationCtrlSet, IssAclTrafficSeperationCtrlTest, IssAclTrafficSeperationCtrlDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "3"},
};
tMibData IssAclTrafficSeperationCtrlEntry = { 1, IssAclTrafficSeperationCtrlMibEntry };

tMbDbEntry IssHealthChkStatusMibEntry[]= {

{{10,IssHealthChkStatus}, NULL, IssHealthChkStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData IssHealthChkStatusEntry = {1, IssHealthChkStatusMibEntry };

tMbDbEntry IssHealthChkErrorReasonMibEntry[]= {

{{10,IssHealthChkErrorReason}, NULL, IssHealthChkErrorReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData IssHealthChkErrorReasonEntry = {1, IssHealthChkErrorReasonMibEntry };

tMbDbEntry IssHealthChkMemAllocErrPoolIdMibEntry[]= {

{{10,IssHealthChkMemAllocErrPoolId}, NULL, IssHealthChkMemAllocErrPoolIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData IssHealthChkMemAllocErrPoolIdEntry = {1, IssHealthChkMemAllocErrPoolIdMibEntry };

tMbDbEntry IssHealthChkConfigRestoreStatusMibEntry[]= {

{{10,IssHealthChkConfigRestoreStatus}, NULL, IssHealthChkConfigRestoreStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData IssHealthChkConfigRestoreStatusEntry = {1, IssHealthChkConfigRestoreStatusMibEntry };

tMbDbEntry IssHealthChkClearCtrMibEntry[]= {

{{10,IssHealthChkClearCtr}, NULL, IssHealthChkClearCtrGet, IssHealthChkClearCtrSet, IssHealthChkClearCtrTest, IssHealthChkClearCtrDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData IssHealthChkClearCtrEntry = {1, IssHealthChkClearCtrMibEntry };

#endif /* _FSISSDB_H */

