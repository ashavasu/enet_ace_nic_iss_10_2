/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/* $Id: issnvram.h,v 1.83.10.1 2018/05/22 10:50:00 siva Exp $ */
/*****************************************************************************/
/*    FILE  NAME            : issnvram.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*    DESCRIPTION           : This file contains nvram related declarations. */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#ifndef _ISSNVRAM_H
#define _ISSNVRAM_H

#define NVRAM_SUCCESS                 0
#define NVRAM_FAILURE                 1
#define NVRAM_FILE_NOT_FOUND          2

#define MAX_COLUMN_LENGTH             120
#define INTERFACE_STR_SIZE            CFA_MAX_PORT_NAME_LENGTH
#define RESTORE_FILE_NAME_LEN         ISS_CONFIG_FILE_NAME_LEN
#define ISS_NPAPI_MODE_NAME_LEN        15
#define ISS_USER_PREFERENCE_LEN         2

/* This is the default interface alias name */
#ifdef MBSM_WANTED
#define ISS_DEFAULT_INTERFACE_NAME    ((const char *) "eth0")
#else
#define ISS_DEFAULT_INTERFACE_NAME    ((const char *) "Slot0/1")
#endif
typedef enum
{
    ISS_CLI_SERIAL_CONSOLE_ENABLE = 1,
    ISS_CLI_SERIAL_CONSOLE_DISABLE
}eIssCliConsole;

#if defined (MBSM_WANTED) && defined (RM_WANTED)
#define ISS_DEFAULT_RM_INTERFACE_NAME    ((const char *) "eth1")
#else
#if defined (RM_WANTED)
#define ISS_DEFAULT_RM_INTERFACE_NAME    ((const char *) "lo")
#else
#define ISS_DEFAULT_RM_INTERFACE_NAME    ((const char *) "NONE")
#endif
#endif

/* Macros related to file handling */
#ifdef BGP_WANTED
#define BGP4_GR_CONF                      (const CHR1 *)"bgpgr.txt"
#endif /* BGP_WANTED */
#define ISS_DEFAULT_IP_ADDRESS        ISS_CUST_SYS_DEF_IP_ADDRESS
#define ISS_DEFAULT_IPV6_ADDRESS      ISS_CUST_SYS_DEF_IPV6_ADDRESS
#define ISS_DEFAULT_SEC_IP_ADDRESS    0
#define ISS_DEFAULT_SEC_IPV6_ADDRESS  "0::0"
#define ISS_DEFAULT_RES_IP_ADDRESS    ISS_CUST_SYS_DEF_RES_IP_ADDRESS
#define ISS_DEFAULT_RES_STR_IP_ADDRESS   ISS_CUST_SYS_DEF_RES_STR_IP_ADDRESS
#define ISS_DEFAULT_RESTORE_FLAG      1
#define ISS_DEFAULT_SAVE_FLAG         1
#define ISS_DEFAULT_AUTO_SAVE_FLAG    2
#define ISS_DEFAULT_RES_FILE_NAME     "iss.conf"
#define ISS_DEFAULT_INCR_SAVE_FILE_NAME     "issinc.conf"
#define ISS_DEFAULT_IP_MASK           ISS_CUST_SYS_DEF_IP_MASK
#define ISS_DEFAULT_IPV6_PREFIX_LEN   ISS_CUST_SYS_DEF_IPV6_PREFIX_LEN
#define ISS_DEFAULT_SEC_IP_MASK           0xffffffff
#define ISS_DEFAULT_SEC_IPV6_PREFIX_LEN   128
#define ISS_DEFAULT_PIM_MODE          2 
#define ISS_DEFAULT_SNOOP_FWD_MODE      ISS_SNOOP_MAC
#define ISS_DEFAULT_CLI_SERIAL_CONSOLE  ISS_CLI_SERIAL_CONSOLE_ENABLE
/*By Default, ISS comes as Customer Bridge Mode*/
#define ISS_DEFAULT_BRIDGE_MODE       1 
#define ISS_DEFAULT_VLAN_ID           1
#define ISS_DEFAULT_FRONT_PANEL_PORT_COUNT    SYS_DEF_MAX_PHYSICAL_INTERFACES - SYS_DEF_MAX_RADIO_INTERFACES
#define ISS_DEFAULT_INCR_SAVE_FLAG    2
#define ISS_DEFAULT_ROLLBACK_FLAG    2
#define ISS_DEFAULT_NPAPI_MODE        "Synchronous"
#define ISS_DEFAULT_FIPS_OPER_MODE   2
/* HITLESS RESTART */
#define ISS_DEFAULT_HR_FLAG          ISS_HITLESS_RESTART_DISABLE
#define FLASH_STR_LEN                STRLEN (FLASH)
#define ISS_HARDWARE_VARSION     "5.9.1"
#define ISS_HARDWARE_PART_NUMBER "1-0-0"
#define ISS_SOFTWARE_VERSION "10.2.0"
#define ISS_SOFTWARE_SERIAL_NUMBER "1-0-0"
#define ISS_FIRMWARE_VERSION         ISS_CUST_FW_VERSION
#define ISS_SWITCH_NAME "ISS"
#define ISS_NVRAM_FILE        FLASH "issnvram.txt"
#define ISS_DEFAULT_DEBUG_LOG_FILE        FLASH
#define ISS_DEFAULT_IMG_DUMP_PATH         FLASH
#define ISS_DEFAULT_AUTO_PORT_CREATE 1
/* length of pre-string "RES_FILE_NAME   ="
 * + RESTORE_FILE_NAME_LEN + Byte alignment */
#define ISS_NVRAM_MAX_LEN 165

#define ISS_DEFAULT_RESTORE_TYPE     1

typedef union NvramCallBackEntry {

    INT4 (*pIssCustShowNvram) (INT4 CliHandle);
    INT4 (*pIssCustReadNvram) (VOID);
    INT4 (*pIssCustWriteNvram) (VOID);
    INT4 (*pIssCustInitNvRamDefVal) (VOID);
} tNvramCallBackEntry;

typedef enum {
    ISS_NVRAM_CLI_SHOW_EVENT = 1,
    ISS_NVRAM_CUST_READ_EVENT,
    ISS_NVRAM_CUST_WRITE_EVENT,
    ISS_NVRAM_CUST_INIT_EVENT,
    NVRAM_MAX_CALLBACK_EVENTS
}tNvramCallBackEvents;
/* Prototypes */
#ifdef __ISSCUST_C_
tNvramCallBackEntry     gNvramCallBack[NVRAM_MAX_CALLBACK_EVENTS];
#else
PUBLIC tNvramCallBackEntry     gNvramCallBack[NVRAM_MAX_CALLBACK_EVENTS];
#endif

#define ISS_NVRAM_CALLBACK       gNvramCallBack
typedef struct {
    char ai1RestoreFileName[RESTORE_FILE_NAME_LEN];
    char ai1BgpConfRestoreFileName[RESTORE_FILE_NAME_LEN];
    char ai1CsrConfRestoreFileName[RESTORE_FILE_NAME_LEN];
    char ai1OspfConfRestoreFileName[RESTORE_FILE_NAME_LEN];
    char ai1IncrSaveFileName[ISS_CONFIG_FILE_NAME_LEN];
    char ai1SnmpEngineID[SNMP_MAX_STR_ENGINEID_LEN + 1];
    char ai1InterfaceName[INTERFACE_STR_SIZE];
    char ai1RmIfName[INTERFACE_STR_SIZE]; 
    char au1NpapiMode[ISS_NPAPI_MODE_NAME_LEN+1];
    int i4FipsOperMode;
      char ai1HardwareVersion[ISS_STR_LEN];   
      char ai1SoftwareVersion[ISS_STR_LEN];   
      char ai1FirmwareVersion[ISS_STR_LEN];   
      char ai1HardwarePartNumber[ISS_STR_LEN];   
      char ai1SoftwareSerialNumber[ISS_STR_LEN];      
      char ai1SwitchName[ISS_STR_LEN];         
    unsigned int u4LocalIpAddr;
    unsigned int u4LocalSubnetMask;
    unsigned int u4CfgMode;
    unsigned int u4RestoreOption;
    tIPvXAddr    RestoreIpAddr;
    unsigned int u4PimMode;
    unsigned int u4SnoopFwdMode;
    unsigned int u4MgmtPort; /* Flag set to TRUE if MgmtPort exists */
    unsigned int u4CliSerialConsole; /* TRUE - Serial console enabled
                                      * FALSE - Serial console disabled
                                      */
    unsigned int u4IpAddrAllocProto;
    unsigned int u4BridgeMode;  /*Indicates the bridge comes as
                                 * CB/PB/PEB/PCB*/
    unsigned int u4ColdStandby;/* Indicates the cold stand by state */
    unsigned int u4SnmpEngineBoots;
    unsigned int u4FrontPanelPortCount;
    unsigned int u4RmHbMode; /* RM Heart Beat Mode */
    unsigned int u4RmRType; /* RM Redundancy Type */
    unsigned int u4RmDType; /* RM Data Plane */
    unsigned short u2DefaultVlan;
    unsigned short u2StackPortCount; /* Number of Stack Ports */
    unsigned short u4SwitchId;
    unsigned short u2TimeStampMethod;/*Time Stamping method to be used */
    char i1SaveFlag;   /* If Set to 1 MIB is saved */
    char i1ResFlag;    /* If Set to 1 MIB is restored*/
    char    i1AutoSaveFlag;    /* New flag to store the value ofauto save option*/
    char    i1IncrSaveFlag;    /* New flag to store the Incremental save enabledf or disabled value*/
    char    i1RollbackFlag;    /* New flag to store the ROllback enabled or disabled value*/
    tMacAddr au1SwitchMac; /* Base MAC address */
    char au1UserPreference[ISS_USER_PREFERENCE_LEN+2];
    char    i1DefValSaveFlag; /* New flag to Save Default values in
                                 incremental Save mode */
    BOOLEAN   bVrfUnqMacOption; /* TRUE - Assigns unique mac to each VR
                                 * FALSE - Assigns base mac to all VR */
/* HITLESS RESTART */
    char    i1RmHRFlag;  /* If set to 0 - Hitless restart feature is disabled
                          * If set to 1 - Hitless restart feature is enabled 
                          * and NPAPI init calls in CFA are blocked */
    char    i1AutomaticPortCreate; 
                            /* If set to 1 (TRUE) - Ports are Automatically created .
                             * If set to 2 (FALSE) - Ports are created through RSTP module */
    UINT1 u1PrefixLen; 
    tIp6Addr localIp6Addr;
    unsigned int u4RmStackingInterfaceType;  /* RM Stacking Interface Type. 
                               If set to 1 ,RM uses OOB port for communciation.
                               If set to 0, RM uses an Ethernet port for the same */
    UINT1 au1FlashLoggingFilePath[ISS_CONFIG_FILE_NAME_LEN];
    UINT1 au1ImageDumpFilePath[ISS_CONFIG_FILE_NAME_LEN];
    UINT1 u1DissRolePlayed;
    UINT1 u1RestoreType;   /* Restoration Type.
                              If set to 1 (MSR) - Save and Restore the configuration in the format of MIB OID format 
                              If set to 2 (CSR) - Save and Restore the configuration in the format of CLI commands */
    UINT1 au1Reserved[2];

}tNVRAM_DATA;

typedef struct {
    tMacAddr  au1AggregatorMac; /* Aggregator MAC address */
    UINT1     au1Reserved[2];
}tNVRAM_AGG_MAC;


int NvRamRead PROTO ((tNVRAM_DATA *));
int NvRamWrite PROTO ((tNVRAM_DATA *));
VOID NvRamReadAggregatorMac PROTO ((VOID));
UINT4 GetNvRamValue PROTO ((UINT1 *, UINT1 *, UINT1 *));
VOID IssStoreGraceContent (VOID*,UINT4); 
VOID IssRestoreGraceContent (VOID *, UINT4);
VOID IssRemoveGraceContent  (UINT4);
VOID IssBgpRestoreGraceContent (VOID *);
VOID IssBgpStoreGraceContent (VOID *);
VOID IsisStore PROTO ((VOID *));
VOID IsisRestore PROTO ((VOID *));

INT4
IssCustNvramCallBack (UINT4 u4Event, INT4 CliHandle);

VOID IssSetRMTypeToNvRam PROTO ((UINT4 u4RmStackingInterfaceType));

VOID IssSysGetIssPrtCtrlSpeed(INT4 i4IssPortCtrlIndex, tIssPortCtrlSpeed *pIssPrtCtrlSpeed);
#endif /*_ISSNVRAM_H */
