/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/* $Id: wlcnvram.h,v 1.2 2013/11/07 11:35:46 siva Exp $              */
/*****************************************************************************/
/*    FILE  NAME            : wlcnvram.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*    DESCRIPTION           : This file contains wlc nvram related           */
/*                            declarations.                                  */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#ifndef _WLCNVRAM_H
#define _WLCNVRAM_H

#define WLC_NVRAM_FILE        FLASH "wlcnvram.txt"
#define WLC_NVRAM_MAX_LEN     100
#define WLC_NVRAM_MAX_LEN_1   150

/* Default values macros */

#define WSS_DEFAULT_CAPWAP_UDP_SERVER_PORT  5246

/* DTLS Key Type */
#define WSS_DTLS_KEY_TYPE_PSK         1
#define WSS_DTLS_KEY_TYPE_CERT        2
#define WSS_DEFAULT_DTLS_KEY_TYPE     WSS_DTLS_KEY_TYPE_PSK

typedef struct {
    UINT4 u4CapwapUdpServerPort;    /* CAPWAP UDP Server Port */
    UINT4 u4DtlsKeyType;

}tWLCNVRAM_DATA;

int WlcNvRamRead PROTO ((tWLCNVRAM_DATA *));
int WlcNvRamWrite PROTO ((tWLCNVRAM_DATA *));

/* WLC NVRAM Get Functions */
UINT4 WssGetCapwapUdpSPortFromWlcNvRam PROTO ((VOID));
UINT4 WssGetDtlsKeyTypeFromWlcNvRam PROTO ((VOID));

/* WLC NVRAM Set Functions */
VOID WssSetCapwapUdpSPortToWlcNvRam PROTO ((UINT4));
VOID WssSetDtlsKeyTypeToWlcNvRam PROTO ((UINT4));

#endif /*_WLCNVRAM_H */

