/*****************************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: wssproto.h,v 1.3 2017/05/23 14:21:45 siva Exp $
*
* Description : This file contains all the prototype declarations in WSS.
*
******************************************************************************/

#ifndef _WSSPROTO_H
#define _WSSPROTO_H

/* Reading the Wss System Information */
VOID WssReadSystemInfoFromWssNvRam PROTO ((VOID));

#ifdef WTP_WANTED
VOID WssInitialiseWtpNvRamDefVal PROTO ((VOID));
VOID WssReadSwitchMac PROTO ((VOID));
#endif

#ifdef WLC_WANTED
VOID WssInitialiseWlcNvRamDefVal PROTO ((VOID));
#endif

#endif /* _WSSPROTO_H */
