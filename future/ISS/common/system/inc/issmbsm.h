
#ifndef _ISSMBSM_H
#define _ISSMBSM_H

PUBLIC INT4  IssMbsmL2FilterCardInsert PROTO  
((tMbsmPortInfo* pPortInfo, tMbsmSlotInfo* pSlotInfo));

PUBLIC INT4 IssMbsmL2FilterCardRemove PROTO 
((tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo));

PUBLIC INT4 IssMbsmL3FilterCardInsert PROTO 
((tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo));

PUBLIC INT4 IssMbsmL3FilterCardRemove PROTO 
((tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo));

PUBLIC INT4 IssMbsmCardInsertConfigCtrlTable 
PROTO ((tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo));

PUBLIC INT4 IssMbsmCardInsertRateCtrlTable 
PROTO ((tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo));

PUBLIC INT4 IssMbsmCardInsertPortCtrlTable 
PROTO ((tMbsmPortInfo * pPortInfo,tMbsmSlotInfo * pSlotInfo));

PUBLIC INT4 IssMbsmCardInsertMirrorCtrlTable 
PROTO ((tMbsmPortInfo * pPortInfo, tMbsmSlotInfo * pSlotInfo));



#endif
