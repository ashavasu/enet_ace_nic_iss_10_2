/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : issmdb.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Tool Generated                                 */
/*    DESCRIPTION           : The mib related file contains the Mib DataBase */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

/*****
  issmdb.h . This File Contains the Mib DataBase.
*****/

#ifndef _ISSMDB_H
#define _ISSMDB_H

/*  The NULL FUNCTION POINTER.  */
# define NULLF ARG_LIST(((INT4 (*)(tSNMP_OID_TYPE *,\
                                          tSNMP_OID_TYPE *,\
                                          UINT1,\
                                          tSNMP_MULTI_DATA_TYPE *)) NULL))
/*  NULL VARBIND FUNCTION POINTER . */
# define NULLVBF  ARG_LIST(((tSNMP_VAR_BIND* (*)(tSNMP_OID_TYPE *,\
                                           tSNMP_OID_TYPE *,\
                                           UINT1,\
                                           UINT1))NULL))
# include "issmid.h"
# include "isscon.h"
# include "issogi.h"

/*  The Declaration of the Group Arrays. */

UINT4 au4_iss_snmp_TABLE1[] = {1,3,6,1,4,1,2076,81,1};
UINT4 au4_iss_snmp_TABLE2[] = {1,3,6,1,4,1,2076,81,2};
UINT4 au4_iss_snmp_TABLE3[] = {1,3,6,1,4,1,2076,81,3};
UINT4 au4_iss_snmp_TABLE4[] = {1,3,6,1,4,1,2076,81,4};
UINT4 au4_iss_snmp_TABLE5[] = {1,3,6,1,4,1,2076,81,5};

/*  The Declaration of the SubGroup Arrays. */

UINT4  au4_SNMP_OGP_ISSSYSTEM_OID [] ={0};
UINT4  au4_SNMP_OGP_ISSCONFIGCTRLTABLE_OID [] ={1,1};
UINT4  au4_SNMP_OGP_ISSPORTCTRLTABLE_OID [] ={2,1};
UINT4  au4_SNMP_OGP_ISSMIRROR_OID [] ={0};
UINT4  au4_SNMP_OGP_ISSMIRRORCTRLTABLE_OID [] ={3,1};
UINT4  au4_SNMP_OGP_ISSRATECTRLTABLE_OID [] ={1,1};
UINT4  au4_SNMP_OGP_ISSFILTERTABLE_OID [] ={1,1};

/*  Declaration of Group OID Table. */
/* Each entry contains Length of Group OID, Pointer to the Group OID,
   Priority of the registering subagent, Timeout for response,
   and Number of Subgroups in that order */

 tSNMP_GroupOIDType iss_FMAS_GroupOIDTable[] =
{
   {9 , au4_iss_snmp_TABLE1 , 1 , 10 , 1},
   {9 , au4_iss_snmp_TABLE2 , 1 , 10 , 2},
   {9 , au4_iss_snmp_TABLE3 , 1 , 10 , 2},
   {9 , au4_iss_snmp_TABLE4 , 1 , 10 , 1},
   {9 , au4_iss_snmp_TABLE5 , 1 , 10 , 1}
};
/*  Declaration of Base OID Table. */
/* Each entry contains Length of Base OID, Pointer to the Base OID,
   Middle level get function pointer, Middle level test function pointer,
   Middle level set function pointer and Number of objects in that table
   in that order */

 tSNMP_BaseOIDType  iss_FMAS_BaseOIDTable[] = {
{
0,
au4_SNMP_OGP_ISSSYSTEM_OID,
issSystemGet,
issSystemTest,
issSystemSet,
27
},
{
2,
au4_SNMP_OGP_ISSCONFIGCTRLTABLE_OID,
issConfigCtrlEntryGet,
issConfigCtrlEntryTest,
issConfigCtrlEntrySet,
4
},
{
2,
au4_SNMP_OGP_ISSPORTCTRLTABLE_OID,
issPortCtrlEntryGet,
issPortCtrlEntryTest,
issPortCtrlEntrySet,
8
},
{
0,
au4_SNMP_OGP_ISSMIRROR_OID,
issMirrorGet,
issMirrorTest,
issMirrorSet,
2
},
{
2,
au4_SNMP_OGP_ISSMIRRORCTRLTABLE_OID,
issMirrorCtrlEntryGet,
issMirrorCtrlEntryTest,
issMirrorCtrlEntrySet,
4
},
{
2,
au4_SNMP_OGP_ISSRATECTRLTABLE_OID,
issRateCtrlEntryGet,
issRateCtrlEntryTest,
issRateCtrlEntrySet,
6
},
{
2,
au4_SNMP_OGP_ISSFILTERTABLE_OID,
issFilterEntryGet,
issFilterEntryTest,
issFilterEntrySet,
15
}
};
/* Declaration of MIB Object Table. */
/* Each entry contains Name of the table, Permissions for
   the object and Object name in that order */

 tSNMP_MIBObjectDescrType  iss_FMAS_MIBObjectTable[] = {
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_WRITE,
 ISSSWITCHNAME
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_ONLY,
 ISSHARDWAREVERSION
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_ONLY,
 ISSFIRMWAREVERSION
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_WRITE,
 ISSDEFAULTIPADDRCFGMODE
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_WRITE,
 ISSDEFAULTIPADDR
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_WRITE,
 ISSDEFAULTIPSUBNETMASK
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_ONLY,
 ISSEFFECTIVEIPADDR
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_WRITE,
 ISSDEFAULTINTERFACE
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_WRITE,
 ISSRESTART
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_WRITE,
 ISSCONFIGSAVEOPTION
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_WRITE,
 ISSCONFIGSAVEIPADDR
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_WRITE,
 ISSCONFIGSAVEFILENAME
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_WRITE,
 ISSINITIATECONFIGSAVE
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_ONLY,
 ISSCONFIGSAVESTATUS
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_WRITE,
 ISSCONFIGRESTOREOPTION
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_WRITE,
 ISSCONFIGRESTOREIPADDR
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_WRITE,
 ISSCONFIGRESTOREFILENAME
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_ONLY,
 ISSCONFIGRESTORESTATUS
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_WRITE,
 ISSDLIMAGEFROMIP
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_WRITE,
 ISSDLIMAGENAME
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_WRITE,
 ISSINITIATEDLIMAGE
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_WRITE,
 ISSLOGGINGOPTION
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_WRITE,
 ISSUPLOADLOGFILETOIP
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_WRITE,
 ISSLOGFILENAME
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_WRITE,
 ISSINITIATEULLOGFILE
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_ONLY,
 ISSREMOTESAVESTATUS
},
{
 SNMP_OGP_INDEX_ISSSYSTEM,
 READ_ONLY,
 ISSDOWNLOADSTATUS
},
{
 SNMP_OGP_INDEX_ISSCONFIGCTRLTABLE,
 NO_ACCESS,
 ISSCONFIGCTRLINDEX
},
{
 SNMP_OGP_INDEX_ISSCONFIGCTRLTABLE,
 READ_WRITE,
 ISSCONFIGCTRLEGRESSSTATUS
},
{
 SNMP_OGP_INDEX_ISSCONFIGCTRLTABLE,
 READ_WRITE,
 ISSCONFIGCTRLSTATSCOLLECTION
},
{
 SNMP_OGP_INDEX_ISSCONFIGCTRLTABLE,
 READ_WRITE,
 ISSCONFIGCTRLSTATUS
},
{
 SNMP_OGP_INDEX_ISSPORTCTRLTABLE,
 NO_ACCESS,
 ISSPORTCTRLINDEX
},
{
 SNMP_OGP_INDEX_ISSPORTCTRLTABLE,
 READ_WRITE,
 ISSPORTCTRLMODE
},
{
 SNMP_OGP_INDEX_ISSPORTCTRLTABLE,
 READ_WRITE,
 ISSPORTCTRLDUPLEX
},
{
 SNMP_OGP_INDEX_ISSPORTCTRLTABLE,
 READ_WRITE,
 ISSPORTCTRLSPEED
},
{
 SNMP_OGP_INDEX_ISSPORTCTRLTABLE,
 READ_WRITE,
 ISSPORTCTRLFLOWCONTROL
},
{
 SNMP_OGP_INDEX_ISSPORTCTRLTABLE,
 READ_WRITE,
 ISSPORTCTRLRENEGOTIATE
},
{
 SNMP_OGP_INDEX_ISSPORTCTRLTABLE,
 READ_WRITE,
 ISSPORTCTRLMAXMACADDR
},
{
 SNMP_OGP_INDEX_ISSPORTCTRLTABLE,
 READ_WRITE,
 ISSPORTCTRLMAXMACACTION
},
{
 SNMP_OGP_INDEX_ISSMIRROR,
 READ_WRITE,
 ISSMIRRORSTATUS
},
{
 SNMP_OGP_INDEX_ISSMIRROR,
 READ_WRITE,
 ISSMIRRORTOPORT
},
{
 SNMP_OGP_INDEX_ISSMIRRORCTRLTABLE,
 NO_ACCESS,
 ISSMIRRORCTRLINDEX
},
{
 SNMP_OGP_INDEX_ISSMIRRORCTRLTABLE,
 READ_WRITE,
 ISSMIRRORCTRLINGRESSMIRRORING
},
{
 SNMP_OGP_INDEX_ISSMIRRORCTRLTABLE,
 READ_WRITE,
 ISSMIRRORCTRLEGRESSMIRRORING
},
{
 SNMP_OGP_INDEX_ISSMIRRORCTRLTABLE,
 READ_WRITE,
 ISSMIRRORCTRLSTATUS
},
{
 SNMP_OGP_INDEX_ISSRATECTRLTABLE,
 NO_ACCESS,
 ISSRATECTRLINDEX
},
{
 SNMP_OGP_INDEX_ISSRATECTRLTABLE,
 READ_WRITE,
 ISSRATECTRLDLFLIMITSTATUS
},
{
 SNMP_OGP_INDEX_ISSRATECTRLTABLE,
 READ_WRITE,
 ISSRATECTRLBCASTLIMITSTATUS
},
{
 SNMP_OGP_INDEX_ISSRATECTRLTABLE,
 READ_WRITE,
 ISSRATECTRLMCASTLIMITSTATUS
},
{
 SNMP_OGP_INDEX_ISSRATECTRLTABLE,
 READ_WRITE,
 ISSRATECTRLLIMITVALUE
},
{
 SNMP_OGP_INDEX_ISSRATECTRLTABLE,
 READ_WRITE,
 ISSRATECTRLSTATUS
},
{
 SNMP_OGP_INDEX_ISSFILTERTABLE,
 NO_ACCESS,
 ISSFILTERNO
},
{
 SNMP_OGP_INDEX_ISSFILTERTABLE,
 READ_WRITE,
 ISSFILTERPRIORITY
},
{
 SNMP_OGP_INDEX_ISSFILTERTABLE,
 READ_WRITE,
 ISSFILTERTYPE
},
{
 SNMP_OGP_INDEX_ISSFILTERTABLE,
 READ_WRITE,
 ISSFILTERPROTOCOLTYPE
},
{
 SNMP_OGP_INDEX_ISSFILTERTABLE,
 READ_WRITE,
 ISSFILTERDSTMACADDR
},
{
 SNMP_OGP_INDEX_ISSFILTERTABLE,
 READ_WRITE,
 ISSFILTERSRCMACADDR
},
{
 SNMP_OGP_INDEX_ISSFILTERTABLE,
 READ_WRITE,
 ISSFILTERDSTIPADDR
},
{
 SNMP_OGP_INDEX_ISSFILTERTABLE,
 READ_WRITE,
 ISSFILTERSRCIPADDR
},
{
 SNMP_OGP_INDEX_ISSFILTERTABLE,
 READ_WRITE,
 ISSFILTERDSTIPADDRMASK
},
{
 SNMP_OGP_INDEX_ISSFILTERTABLE,
 READ_WRITE,
 ISSFILTERSRCIPADDRMASK
},
{
 SNMP_OGP_INDEX_ISSFILTERTABLE,
 READ_WRITE,
 ISSFILTERINPORTLIST
},
{
 SNMP_OGP_INDEX_ISSFILTERTABLE,
 READ_WRITE,
 ISSFILTEROUTPORTLIST
},
{
 SNMP_OGP_INDEX_ISSFILTERTABLE,
 READ_WRITE,
 ISSFILTERACTION
},
{
 SNMP_OGP_INDEX_ISSFILTERTABLE,
 READ_ONLY,
 ISSFILTERMATCHCOUNT
},
{
 SNMP_OGP_INDEX_ISSFILTERTABLE,
 READ_CREATE,
 ISSFILTERSTATUS
}
};

 tSNMP_GLOBAL_STRUCT iss_FMAS_Global_data =
{sizeof (iss_FMAS_GroupOIDTable) / sizeof (tSNMP_GroupOIDType),
 sizeof (iss_FMAS_BaseOIDTable) / sizeof (tSNMP_BaseOIDType)};

 int iss_MAX_OBJECTS =
(sizeof (iss_FMAS_MIBObjectTable) / sizeof (tSNMP_MIBObjectDescrType));

#endif  /* _ISSMDB_H */
