/*****************************************************************************/
/* Copyright (C) 2013 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2013                                          */
/* $Id: wtpnvram.h,v 1.4 2016/12/16 11:01:40 siva Exp $              */
/*****************************************************************************/
/*    FILE  NAME            : wtpnvram.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*    DESCRIPTION           : This file contains wtp nvram related           */
/*                            declarations.                                  */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#ifndef _WTPNVRAM_H
#define _WTPNVRAM_H


#define WTP_NVRAM_FILE        FLASH "wtpnvram.txt"
#define WTP_NVRAM_MAX_LEN     100
#define WTP_NVRAM_MAX_LEN_1   150

/* AC Referral Type */
#define WSS_AC_REFERRAL_TYPE_BROADCAST  1
#define WSS_AC_REFERRAL_TYPE_MULTICAST  2
#define WSS_DEFAULT_AC_REFERRAL_TYPE    WSS_AC_REFERRAL_TYPE_BROADCAST

/* WTP CAPWAP Mac Type */
#define WSS_CAPWAP_MAC_TYPE_LOCAL       0
#define WSS_CAPWAP_MAC_TYPE_SPLIT       1
#define WSS_CAPWAP_MAC_TYPE_BOTH        2
#define WSS_DEFAULT_CAPWAP_MAC_TYPE     WSS_CAPWAP_MAC_TYPE_LOCAL

/* WTP CAPWAP Tunnel Mode */
#define WSS_CAPWAP_TUNL_MODE_NATIVE         8
#define WSS_CAPWAP_TUNL_MODE_DOT3           4
#define WSS_CAPWAP_TUNL_MODE_LOCALBRIDGE    2
#define WSS_DEFAULT_CAPWAP_TUNL_MODE        WSS_CAPWAP_TUNL_MODE_DOT3 

/* DTLS Key Type */
#define WSS_DTLS_KEY_TYPE_PSK         1
#define WSS_DTLS_KEY_TYPE_CERT        2
#define WSS_DEFAULT_DTLS_KEY_TYPE     WSS_DTLS_KEY_TYPE_PSK

/*AC Discovery Type*/
#define WSS_UNKNOWN_DISC_TYPE               0
#define WSS_STATIC_CONF_DISC_TYPE           1
#define WSS_DHCP_DISC_TYPE                  2 
#define WSS_DNS_DISC_TYPE                   3
#define WSS_AC_REFERRAL_DISC_TYPE           4
#define WSS_DEFAULT_DISCOVERY_TYPE      WSS_AC_REFERRAL_DISC_TYPE

/* WTP Model Number */
#define WSS_DEFAULT_WTP_MODEL_NUMBER    "QCA100WTP200000"

/* WTP Serial Number */
#define WSS_DEFAULT_WTP_SERIAL_NUMBER    "WTP-200000"

/* WTP Board Id */
#define WSS_DEFAULT_WTP_BOARD_ID    "BID-300A"

/* WTP Board Revision */
#define WSS_DEFAULT_WTP_BOARD_REVISION    "BOARD-REV:1.0"

/* CAPWAP UDP Server Port */
#define WSS_DEFAULT_CAPWAP_UDP_SERVER_PORT  5246

/* Native VLAN */
#define WSS_DEFAULT_NATIVE_VLAN     100

/* WLC IP Addr */
#define WSS_DEFAULT_WLC_IP_ADDR     0xC0A80105

typedef struct {
    UINT4 u4CapwapUdpServerPort;    /* CAPWAP UDP Server Port */
    UINT4 u4NativeVlan;             /* Native VLAN */
    UINT4 u4WlcIpAddr;
    UINT2 u2AcReferralType;         /* AC Referral Type */
    UINT2 u2WtpCapwapMacType;       /* WTP Capwap MAC Type */
    UINT2 u2WtpCapwapTunnelMode;    /* WTP Capwap Tunnel Mode */
    UINT1 u1DiscoveryType;          /* Discovery Type of AP*/
    UINT1 au1Pad[1];
    char ai1WtpModelNumber[WTP_NVRAM_MAX_LEN];    /* WTP Model Number */
    char ai1WtpSerialNumber[WTP_NVRAM_MAX_LEN];    /* WTP Serial Number */
    char ai1WtpBoardId[WTP_NVRAM_MAX_LEN];        /* WTP Board ID */
    char ai1WtpBoardRevision[WTP_NVRAM_MAX_LEN];    /* WTP Board Revision */
    UINT4 u4DtlsKeyType;

}tWTPNVRAM_DATA;

int WtpNvRamRead PROTO ((tWTPNVRAM_DATA *));
int WtpNvRamWrite PROTO ((tWTPNVRAM_DATA *));

/* WTP NVRAM Get Functions */
UINT1 WssGetDiscoveryTypeFromWtpNvRam PROTO ((VOID));
UINT2 WssGetAcRefTypeFromWtpNvRam PROTO ((VOID));
UINT2 WssGetWtpMacTypeFromWtpNvRam PROTO ((VOID));
UINT2 WssGetWtpTunlModeFromWtpNvRam PROTO ((VOID));
INT1* WssGetWtpModelNumFromWtpNvRam PROTO ((VOID));
INT1* WssGetWtpSerialNumFromWtpNvRam PROTO ((VOID));
INT1* WssGetWtpBoardIdFromWtpNvRam PROTO ((VOID));
INT1* WssGetWtpBoardRevFromWtpNvRam PROTO ((VOID));
UINT4 WssGetCapwapUdpSPortFromWtpNvRam PROTO ((VOID));
UINT4 WssGetNativeVlanFromWtpNvRam PROTO ((VOID));
UINT4 WssGetWlcIpAddrFromWtpNvRam PROTO ((VOID));
UINT4 WssGetDtlsKeyTypeFromWtpNvRam PROTO ((VOID));

/* WTP NVRAM Set Functions */
VOID WssSetDiscoveryTypeToWtpNvRam PROTO ((UINT1));
VOID WssSetAcRefTypeToWtpNvRam PROTO ((UINT2));
VOID WssSetWtpMacTypeToWtpNvRam PROTO ((UINT2));
VOID WssSetWtpTunlModeToWtpNvRam PROTO ((UINT2));
VOID WssSetWtpModelNumToWtpNvRam PROTO ((INT1*));
VOID WssSetWtpSerialNumToWtpNvRam PROTO ((INT1*));
VOID WssSetWtpBoardIdToWtpNvRam PROTO ((INT1*));
VOID WssSetWtpBoardRevToWtpNvRam PROTO ((INT1*));
VOID WssSetCapwapUdpSPortToWtpNvRam PROTO ((UINT4));
VOID WssSetNativeVlanToWtpNvRam PROTO ((UINT4));
VOID WssSetWlcIpAddrToWtpNvRam PROTO ((UINT4));
VOID WssSetDtlsKeyTypeToWtpNvRam PROTO ((UINT4));

#endif /*_WTPNVRAM_H */
