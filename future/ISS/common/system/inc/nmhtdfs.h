typedef struct {
    int     cmd;
    UINT4   *pu4IssIpAuthMgrIpAddr;
    UINT4   *pu4IssIpAuthMgrIpMask;
    INT1    rval;
}tNmhGetFirstIndexIssIpAuthMgrTable;

typedef struct {
    int     cmd;
    UINT4   u4IssIpAuthMgrIpAddr;
    UINT4   *pu4NextIssIpAuthMgrIpAddr;
    UINT4   u4IssIpAuthMgrIpMask;
    UINT4   *pu4NextIssIpAuthMgrIpMask;
    INT1    rval;
}tNmhGetNextIndexIssIpAuthMgrTable;

typedef struct {
    int     cmd;
    UINT4   u4IssIpAuthMgrIpAddr;
    UINT4   u4IssIpAuthMgrIpMask;
    INT1    rval;
}tNmhValidateIndexInstanceIssIpAuthMgrTable;

typedef struct {
    int     cmd;
    UINT4   u4IssIpAuthMgrIpAddr;
    UINT4   u4IssIpAuthMgrIpMask;
    tSNMP_OCTET_STRING_TYPE *pRetValIssIpAuthMgrPortList;
    INT1    rval;
}tNmhGetIssIpAuthMgrPortList;

typedef struct {
    int     cmd;
    UINT4 u4IssIpAuthMgrIpAddr;
    UINT4 u4IssIpAuthMgrIpMask;
    tSNMP_OCTET_STRING_TYPE *pRetValIssIpAuthMgrVlanList;
    INT1    rval;
}tNmhGetIssIpAuthMgrVlanList;

typedef struct {
    int     cmd;
    UINT4 u4IssIpAuthMgrIpAddr;
    UINT4 u4IssIpAuthMgrIpMask;
    INT4 *pi4RetValIssIpAuthMgrOOBPort;
    INT1    rval;
}tNmhGetIssIpAuthMgrOOBPort;

typedef struct {
    int     cmd;
    UINT4 u4IssIpAuthMgrIpAddr;
    UINT4 u4IssIpAuthMgrIpMask;
    INT4 *pi4RetValIssIpAuthMgrAllowedServices;
    INT1    rval;
}tNmhGetIssIpAuthMgrAllowedServices;

typedef struct {
    int     cmd;
    UINT4 u4IssIpAuthMgrIpAddr;
    UINT4 u4IssIpAuthMgrIpMask;
    INT4 *pi4RetValIssIpAuthMgrRowStatus;
    INT1    rval;
}tNmhGetIssIpAuthMgrRowStatus;

typedef struct {
    int     cmd;
    UINT4 u4IssIpAuthMgrIpAddr;
    UINT4 u4IssIpAuthMgrIpMask;
    tSNMP_OCTET_STRING_TYPE *pSetValIssIpAuthMgrPortList;
    INT1    rval;
}tNmhSetIssIpAuthMgrPortList;

typedef struct {
    int     cmd;
    UINT4 u4IssIpAuthMgrIpAddr;
    UINT4 u4IssIpAuthMgrIpMask;
    tSNMP_OCTET_STRING_TYPE *pSetValIssIpAuthMgrVlanList;
    INT1    rval;
}tNmhSetIssIpAuthMgrVlanList;

typedef struct {
    int     cmd;
    UINT4 u4IssIpAuthMgrIpAddr;
    UINT4 u4IssIpAuthMgrIpMask;
    INT4 i4SetValIssIpAuthMgrOOBPort;
    INT1    rval;
}tNmhSetIssIpAuthMgrOOBPort;

typedef struct {
    int     cmd;
    UINT4 u4IssIpAuthMgrIpAddr;
    UINT4 u4IssIpAuthMgrIpMask;
    INT4 i4SetValIssIpAuthMgrAllowedServices;
    INT1    rval;
}tNmhSetIssIpAuthMgrAllowedServices;

typedef struct {
    int     cmd;
    UINT4 u4IssIpAuthMgrIpAddr;
    UINT4 u4IssIpAuthMgrIpMask;
    INT4 i4SetValIssIpAuthMgrRowStatus;
    INT1    rval;
}tNmhSetIssIpAuthMgrRowStatus;

typedef struct {
    int     cmd;
    UINT4   *pu4ErrorCode;
    UINT4 u4IssIpAuthMgrIpAddr;
    UINT4 u4IssIpAuthMgrIpMask;
    tSNMP_OCTET_STRING_TYPE *pTestValIssIpAuthMgrPortList;
    INT1    rval;
}tNmhTestv2IssIpAuthMgrPortList;

typedef struct {
    int     cmd;
    UINT4 *pu4ErrorCode;
    UINT4 u4IssIpAuthMgrIpAddr;
    UINT4 u4IssIpAuthMgrIpMask;
    tSNMP_OCTET_STRING_TYPE *pTestValIssIpAuthMgrVlanList;
    INT1    rval;
}tNmhTestv2IssIpAuthMgrVlanList;

typedef struct {
    int     cmd;
    UINT4 *pu4ErrorCode;
    UINT4 u4IssIpAuthMgrIpAddr;
    UINT4 u4IssIpAuthMgrIpMask;
    INT4 i4TestValIssIpAuthMgrOOBPort;
    INT1    rval;
}tNmhTestv2IssIpAuthMgrOOBPort;

typedef struct {
    int     cmd;
    UINT4   *pu4ErrorCode;
    UINT4 u4IssIpAuthMgrIpAddr;
    UINT4 u4IssIpAuthMgrIpMask;
    INT4 i4TestValIssIpAuthMgrAllowedServices;
    INT1    rval;
}tNmhTestv2IssIpAuthMgrAllowedServices;

typedef struct {
    int     cmd;
    UINT4 *pu4ErrorCode;
    UINT4 u4IssIpAuthMgrIpAddr;
    UINT4 u4IssIpAuthMgrIpMask;
    INT4 i4TestValIssIpAuthMgrRowStatus;
    INT1    rval;
}tNmhTestv2IssIpAuthMgrRowStatus;

union unNmh {
    tNmhGetFirstIndexIssIpAuthMgrTable GetFirstIndexIssIpAuthMgrTable;
    tNmhGetNextIndexIssIpAuthMgrTable  GetNextIndexIssIpAuthMgrTable;
    tNmhValidateIndexInstanceIssIpAuthMgrTable ValidateIdxInstIssIpAuthMgrTable;
    tNmhGetIssIpAuthMgrPortList        GetIssIpAuthMgrPortList; 
    tNmhGetIssIpAuthMgrVlanList        GetIssIpAuthMgrVlanList;
    tNmhGetIssIpAuthMgrOOBPort         GetIssIpAuthMgrOOBPort;
    tNmhGetIssIpAuthMgrAllowedServices GetIssIpAuthMgrAllowedServices;
    tNmhGetIssIpAuthMgrRowStatus       GetIssIpAuthMgrRowStatus; 
    tNmhSetIssIpAuthMgrPortList        SetIssIpAuthMgrPortList;
    tNmhSetIssIpAuthMgrVlanList        SetIssIpAuthMgrVlanList;
    tNmhSetIssIpAuthMgrOOBPort         SetIssIpAuthMgrOOBPort;
    tNmhSetIssIpAuthMgrAllowedServices SetIssIpAuthMgrAllowedServices;
    tNmhSetIssIpAuthMgrRowStatus       SetIssIpAuthMgrRowStatus;
    tNmhTestv2IssIpAuthMgrPortList     Testv2IssIpAuthMgrPortList;
    tNmhTestv2IssIpAuthMgrVlanList     Testv2IssIpAuthMgrVlanList;
    tNmhTestv2IssIpAuthMgrOOBPort      Testv2IssIpAuthMgrOOBPort;
    tNmhTestv2IssIpAuthMgrAllowedServices Testv2IssIpAuthMgrAllowedServices;
    tNmhTestv2IssIpAuthMgrRowStatus    Testv2IssIpAuthMgrRowStatus;
};
