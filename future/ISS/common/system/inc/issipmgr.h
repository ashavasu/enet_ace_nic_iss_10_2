/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* $Id: issipmgr.h,v 1.11 2013/11/29 10:58:33 siva Exp $                     */
/* Licensee Aricent Inc., 2001-2002                                         */
/*****************************************************************************/
/*    FILE  NAME            : issipmgr.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*    DESCRIPTION           : This file has header file definition required  */
/*                            for general features of IPAuth Mgr             */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*  1.0       July, 6, 2003           Initial Creation.                      */
/*---------------------------------------------------------------------------*/

#ifndef __ISSAUTHMGR_H__
#define __ISSAUTHMGR_H__

/************ IP Authorised Manager datastructures *********/
typedef struct _tIssIpMgrEntry {
   UINT4            u4IpAddr;         /* IP address of the manager. */
   UINT4            u4IpMask;         /* IP mask of the manager. */
   UINT4            u4AllowedServices;/* Services allowed for manager. */  
   INT4             i4Status;         /* Row status of the entry. */       
   tIssAuthPortList     IpAuthPortList;   /* Ports through which manager can 
                                       * access the system. */
   tIssVlanList     IpAuthVlanList;   /* Vlans from which manager can
                                       * access the system. */
   UINT1            u1IpAuthOobPort;  /* Set to True/False */
   UINT1            au1Pad[3];
} tIssIpMgrEntry;

typedef struct _tIssIpMgrTable {
   UINT4            u4Count;          /* Number of managers in the table. */
   tIssIpMgrEntry   *pIpMgrEntry;     /* Ip auth manager entry. */
} tIssIpMgrTable;

typedef struct _tIssIpMgrBlockTable {
   UINT4            u4IpAddr;          /* Number of managers in the table. */
   UINT4            u4Port;
} tIssIpMgrBlockTable;


/*********** IP Authorised Manager - Constant Definitions ****/

#define ISS_MAX_MGR_IP_ENTRIES          10

#define ISS_TCP_PROTOCOL                6
#define ISS_UDP_PROTOCOL                17

#define ISS_L4_DST_PORT_OFFSET          2  
/* Offset of Dest Port in TCP/UDP packet */

#define ISS_SNMP_PORT                   161
#define ISS_TELNET_PORT                 CliGetTelnetPort ()
#define ISS_HTTP_PORT                   HttpGetSourcePort ()
#define ISS_HTTPS_PORT                  443 
#define ISS_SSH_PORT                    22

#define ISS_IP_AUTH_INVALID             -1 
#define ISS_IP_AUTH_ACTIVE              1   
#define ISS_IP_AUTH_INACTIVE            2


#define ISS_IP_AUTH_MIN_PORTS           1

#define ISS_IP_MGR_TBL_SIZE             (sizeof (tIssIpMgrEntry) * (ISS_MAX_MGR_IP_ENTRIES + 1)) 

/********** IP Authorised Manager - Definitions ************/
#ifdef _ISSIPMGR_C  
tIssIpMgrTable   gIssIpMgrTable;
tIssIpMgrBlockTable gIssIpMgrBlockTable[CLI_NUM_SSH_CLIENT_WATCH];
UINT4            gu4IssIpAuthActiveManagers = 0;

/* table for mapping CIDR numbers to IP subnet masks - from RFC 1878*/
UINT4               gu4IssIpCidrSubnetMask[ISS_MAX_CIDR + 1] = {
    0x00000000, 0x80000000, 0xC0000000, 0xE0000000, 0xF0000000, 0xF8000000,
    0xFC000000, 0xFE000000, 0xFF000000, 0xFF800000, 0xFFC00000, 0xFFE00000,
    0xFFF00000, 0xFFF80000, 0xFFFC0000, 0xFFFE0000, 0xFFFF0000, 0xFFFF8000,
    0xFFFFC000, 0xFFFFE000, 0xFFFFF000, 0xFFFFF800, 0xFFFFFC00, 0xFFFFFE00,
    0xFFFFFF00, 0xFFFFFF80, 0xFFFFFFC0, 0xFFFFFFE0, 0xFFFFFFF0, 0xFFFFFFF8,
    0xFFFFFFFC, 0xFFFFFFFE, 0xFFFFFFFF
};
UINT1           gau1IssIpMgrArr[ISS_IP_MGR_TBL_SIZE];
#else
extern tIssIpMgrTable   gIssIpMgrTable;
extern UINT4            gu4IssIpAuthActiveManagers;
#endif /* _ISSIPMGR_C */
   
/************ IP AUthorised Manager - Macros ***************/

#define ISS_GET_IP_MGR_ENTRY(u4Index) (&(gIssIpMgrTable.pIpMgrEntry[u4Index]))

/* Macros for protocol filter */
#define ISS_IS_SNMP_FILTERED(u4Index)  \
        (((ISS_GET_IP_MGR_ENTRY(u4Index)->u4AllowedServices \
             & ISS_SNMP_MASK) == ISS_SNMP_MASK) \
         ? ISS_FALSE : ISS_TRUE)

#define ISS_IS_TELNET_FILTERED(u4Index)  \
        (((ISS_GET_IP_MGR_ENTRY(u4Index)->u4AllowedServices \
             & ISS_TELNET_MASK) == ISS_TELNET_MASK) \
         ? ISS_FALSE : ISS_TRUE)

#define ISS_IS_HTTP_FILTERED(u4Index)  \
        (((ISS_GET_IP_MGR_ENTRY(u4Index)->u4AllowedServices \
             & ISS_HTTP_MASK) == ISS_HTTP_MASK) \
         ? ISS_FALSE : ISS_TRUE)

#define ISS_IS_HTTPS_FILTERED(u4Index)  \
        (((ISS_GET_IP_MGR_ENTRY(u4Index)->u4AllowedServices \
             & ISS_HTTPS_MASK) == ISS_HTTPS_MASK) \
         ? ISS_FALSE : ISS_TRUE)
        
#define ISS_IS_SSH_FILTERED(u4Index)  \
        (((ISS_GET_IP_MGR_ENTRY(u4Index)->u4AllowedServices \
             & ISS_SSH_MASK) == ISS_SSH_MASK) \
         ? ISS_FALSE : ISS_TRUE)
               
/* Macros for identifying protocol type */
#define ISS_IS_PKT_SNMP(i4Result, u2L3Proto, u2L4DstPort) \
{ \
   if ((ISS_UDP_PROTOCOL == u2L3Proto) && (ISS_SNMP_PORT == u2L4DstPort)) \
   { \
      i4Result = ISS_TRUE;  \
   } \
   else \
   i4Result = ISS_FALSE; \
}

#define ISS_IS_PKT_TELNET(i4Result, u2L3Proto, u2L4DstPort) \
{ \
   if ((ISS_TCP_PROTOCOL == u2L3Proto) && (ISS_TELNET_PORT == u2L4DstPort)) \
   { \
      i4Result = ISS_TRUE;  \
   } \
   else \
   i4Result = ISS_FALSE; \
}

#define ISS_IS_PKT_HTTP(i4Result, u2L3Proto, u2L4DstPort) \
{ \
   if ((ISS_TCP_PROTOCOL == u2L3Proto) && (ISS_HTTP_PORT == u2L4DstPort)) \
   { \
      i4Result = ISS_TRUE;  \
   } \
   else \
   i4Result = ISS_FALSE; \
}

#define ISS_IS_PKT_HTTPS(i4Result, u2L3Proto, u2L4DstPort) \
{ \
   if ((ISS_TCP_PROTOCOL == u2L3Proto) && (ISS_HTTPS_PORT == u2L4DstPort)) \
   { \
      i4Result = ISS_TRUE;  \
   } \
   else \
   i4Result = ISS_FALSE; \
}

#define ISS_IS_PKT_SSH(i4Result, u2L3Proto, u2L4DstPort) \
{ \
   if ((ISS_TCP_PROTOCOL == u2L3Proto) && (ISS_SSH_PORT == u2L4DstPort)) \
   { \
      i4Result = ISS_TRUE;  \
   } \
  else \
   i4Result = ISS_FALSE; \
}

/* Macros for port List / Vlan List */

#define  ISS_IP_AUTH_IS_EXCEED_MAX_PORTS(au1List, len, bRetVal)\
         {\
             UINT2 u2BitCount = 0;\
             UINT2 u2PrtCnt = 0;\
             for (u2BitCount = 0; u2BitCount < ((len) * 8); u2BitCount++)\
             {\
                 if ((au1List)[u2BitCount / 8] &\
                            (0x80 >> (u2BitCount % 8)))\
                 {\
                     u2PrtCnt++;\
                     if (u2PrtCnt > ISS_MAX_PORTS) \
                     {\
                         bRetVal = ISS_TRUE;\
                         break;\
                     }\
                 }\
             }\
         }

#define  ISS_IP_AUTH_IS_EXCEED_MAX_VLANS(au1List, len, bRetVal)\
         {\
             UINT2 u2BitCount = 0;\
             UINT2 u2PrtCnt = 0;\
             for (u2BitCount = 0; u2BitCount < ((len) * 8) ; u2BitCount++)\
             {\
                 if ((au1List)[u2BitCount / 8] &\
                           (0x80 >> (u2BitCount % 8)))\
                 {\
                     u2PrtCnt++;\
                     if (u2PrtCnt > ISS_MAX_VLAN_ID)\
                     {\
                         bRetVal = ISS_TRUE;\
                         break;\
                     }\
                 }\
             }\
         }

/*IPAuth Manager Prototypes*/
VOID IssCheckForMgmtPkt PROTO ((UINT1, UINT2, UINT4*));
INT4 IssApplyPortFilter PROTO ((UINT4, UINT2));
INT4 IssApplyOobFilter PROTO  ((UINT4 u4Index));
INT4 IssApplyVlanFilter PROTO ((UINT4, UINT2));
INT4 IssApplyProtocolFilter PROTO ((UINT4, UINT4));
INT4 IssFindIpAuthManager PROTO ((UINT4, UINT4, tIssIpMgrEntry**));
INT4 IssFindFreeEntryInTable PROTO ((tIssIpMgrEntry**));
#endif  /* __ISSAUTHMGR_H__ */
