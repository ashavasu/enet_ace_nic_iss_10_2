
/* $Id: systemsz.h,v 1.4 2012/03/30 13:23:24 siva Exp $ */
enum {
    MAX_ISS_CONFIG_ENTRIES_SIZING_ID,
    MAX_ISS_ENT_PHY_ENTRIES_SIZING_ID,
    MAX_ISS_FIRMWARE_BLOCKS_SIZING_ID,
    MAX_ISS_L4SFILTERENTRY_BLOCKS_SIZING_ID,
    MAX_ISS_MIRR_DEST_RECORD_BLOCKS_SIZING_ID,
    MAX_ISS_MIRR_SRC_RECORD_BLOCKS_SIZING_ID,
    MAX_ISS_PI_EGRESS_PORT_ENTRIES_SIZING_ID,
    MAX_ISS_PORT_ENTRIES_SIZING_ID,
    MAX_ISS_PORT_ISOLATION_ENTRIES_SIZING_ID,
    SYSTEM_MAX_SIZING_ID
};

#ifdef  _SYSTEMSZ_C
tMemPoolId SYSTEMMemPoolIds[ SYSTEM_MAX_SIZING_ID];
INT4  SystemSizingMemCreateMemPools(VOID);
VOID  SystemSizingMemDeleteMemPools(VOID);
INT4  SystemSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _SYSTEMSZ_C  */
extern tMemPoolId SYSTEMMemPoolIds[ ];
extern INT4  SystemSizingMemCreateMemPools(VOID);
extern VOID  SystemSizingMemDeleteMemPools(VOID);
extern INT4  SystemSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _SYSTEMSZ_C  */


#ifdef  _SYSTEMSZ_C
tFsModSizingParams FsSYSTEMSizingParams [] = {
{ "tIssConfigCtrlEntry", "MAX_ISS_CONFIG_ENTRIES", sizeof(tIssConfigCtrlEntry),MAX_ISS_CONFIG_ENTRIES, MAX_ISS_CONFIG_ENTRIES,0 },
{ "tIssEntPhyInfo", "MAX_ISS_ENT_PHY_ENTRIES", sizeof(tIssEntPhyInfo),MAX_ISS_ENT_PHY_ENTRIES, MAX_ISS_ENT_PHY_ENTRIES,0 },
{ "tIssFirmwareBlock", "MAX_ISS_FIRMWARE_BLOCKS", sizeof(tIssFirmwareBlock),MAX_ISS_FIRMWARE_BLOCKS, MAX_ISS_FIRMWARE_BLOCKS,0 },
{ "tIssL4SFilterEntry", "MAX_ISS_L4SFILTERENTRY_BLOCKS", sizeof(tIssL4SFilterEntry),MAX_ISS_L4SFILTERENTRY_BLOCKS, MAX_ISS_L4SFILTERENTRY_BLOCKS,0 },
{ "tIssMirrDestRecordInfo", "MAX_ISS_MIRR_DEST_RECORD_BLOCKS", sizeof(tIssMirrDestRecordInfo),MAX_ISS_MIRR_DEST_RECORD_BLOCKS, MAX_ISS_MIRR_DEST_RECORD_BLOCKS,0 },
{ "tIssMirrSrcRecordInfo", "MAX_ISS_MIRR_SRC_RECORD_BLOCKS", sizeof(tIssMirrSrcRecordInfo),MAX_ISS_MIRR_SRC_RECORD_BLOCKS, MAX_ISS_MIRR_SRC_RECORD_BLOCKS,0 },
{ "tIssPIEgressPortBlock", "MAX_ISS_PI_EGRESS_PORT_ENTRIES", sizeof(tIssPIEgressPortBlock),MAX_ISS_PI_EGRESS_PORT_ENTRIES, MAX_ISS_PI_EGRESS_PORT_ENTRIES,0 },
{ "tIssPortCtrlEntry", "MAX_ISS_PORT_ENTRIES", sizeof(tIssPortCtrlEntry),MAX_ISS_PORT_ENTRIES, MAX_ISS_PORT_ENTRIES,0 },
{ "tIssPortIsolationEntry", "MAX_ISS_PORT_ISOLATION_ENTRIES", sizeof(tIssPortIsolationEntry),MAX_ISS_PORT_ISOLATION_ENTRIES, MAX_ISS_PORT_ISOLATION_ENTRIES,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _SYSTEMSZ_C  */
extern tFsModSizingParams FsSYSTEMSizingParams [];
#endif /*  _SYSTEMSZ_C  */


