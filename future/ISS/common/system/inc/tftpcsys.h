/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tftpcsys.h,v 1.15 2015/10/28 06:13:53 siva Exp $
 *
 * Description: This file has the routines which are customer
 *              specific
 *
 *******************************************************************/


/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/*****************************************************************************/
/*    FILE  NAME            : tftpcsys.h                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : TFTP Client system file                        */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*    DESCRIPTION           : This file contains the data structures and     */
/*                            prototypes for tftp client                     */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
#ifndef __TFTPCSYS_H__
#define __TFTPCSYS_H__
#include "tftpc.h"

UINT4 gu4TftpcTrcFlag=0; /* Critical 0x1, Mid 0x2, Low 0x4 */

typedef struct tftpSession
{
    union
    {
        struct sockaddr_in   cliAddr; /* Client Address */
        struct sockaddr_in6  cliAddr6; /* Client Address */
    }
    client_ip_addr_u;
    union
    {
        struct sockaddr_in   srvAddr ; /* Server Address */
        struct sockaddr_in6  srvAddr6; /* Server Address */
    }
    srv_ip_addr_u;

    INT4  i4SockFd;               /* SockFd for con */
    UINT4 u4FileSize;             /* File Size      */
    INT4  i4OutBufLen;            /* Tx Buf Len     */
    INT4  i4InBufLen;             /* Rx Buf Len     */
    UINT1 *pu1OutPktBuf;          /* Tx Buf         */
    UINT1 *pu1InPktBuf;           /* Rx Buf         */
    UINT1 *pu1Data;               /* Data Pointer   */
    INT4 i4DataLen;               /* Data Len       */
    UINT2 u2BlkSize;              /* 8-65464        */
    UINT2 u2TimeOut;              /* 1-255          */
    UINT2 u2BlkNo;                /* Current Blk No */
    UINT2 u2ReqType;              /* Type RRQ/WRQ   */
    UINT2 u2RetryCnt;             /* Max Retry      */
    UINT2 u2TransferType;         /* Read/ Write    */
    UINT1 u1AddrType;             /*IPv4 or IPv6*/
    UINT1 au1Padding[3];
}tTftpcSession;




typedef struct tftpcError
{
    INT4   i4ECode;
    const INT1  *pu1Msg;
}tTftpcErrorMsg;

PRIVATE tTftpcErrorMsg tftpEMsg[]=
{
    {TFTPC_E_SERVER,  (INT1 *) "Not defined, see error message (if any)."},/*0*/
    {TFTPC_E_NOFILE,  (INT1 *) "File not found."},                         /*1*/
    {TFTPC_E_ACCESS,  (INT1 *) "Access violation."},                       /*2*/
    {TFTPC_E_FULL  ,  (INT1 *) "Disk full or allocation exceeded."},       /*3*/
    {TFTPC_E_SERVER,  (INT1 *) "Illegal TFTP operation."},                 /*4*/
    {TFTPC_E_SERVER,  (INT1 *) "Unknown transfer ID."},                    /*5*/
    {TFTPC_E_EXISTS,  (INT1 *) "File already exists."},                    /*6*/
    {TFTPC_E_SERVER,  (INT1 *) "No such user."},                           /*7*/
    {TFTPC_E_SERVER,  (INT1 *) "Unable to negotiate RFC 2347 options"}     /*8*/
};

#define TFTPC_ERROR_INFO(a)   tftpEMsg[a].pu1Msg
#define TFTPC_ERROR_CODE(a)   tftpEMsg[a].i4ECode
#define TFTPC_MAX_BUFFER   1500
#define TFTPC_MEMSET       MEMSET
#define TFTPC_MEMCPY       MEMCPY
#define TFTPC_STAT         stat

#define TFTPC_STRCPY       STRCPY
#define TFTPC_STRLEN       STRLEN
#define TFTPC_STRCMP       STRCMP

#define TFTPC_HTONL        OSIX_HTONL
#define TFTPC_HTONS        OSIX_HTONS
#define TFTPC_NTOHL        OSIX_NTOHL
#define TFTPC_NTOHS        OSIX_NTOHS
#define TFTPC_ATOI         ATOI

#define TFTPC_FD_ZERO      FD_ZERO
#define TFTPC_FD_SET       FD_SET
#define TFTPC_FEOF         feof
#define TFTPC_FOPEN        FOPEN
#define TFTPC_FCLOSE       fclose
#define TFTPC_FREAD        fread
#define TFTPC_FWRITE       FWRITE


#define TFTPC_BLOCKSIZE_OPT  "blksize"
#define TFTPC_BLOCKSIZE     "1432"  /* Max UDP Payload, considering MTU as 1500 */
#define TFTPC_TIMEOUT_OPT   "timeout"

#if defined (DX260) || defined (DX285)
#define TFTPC_TIMEOUT       "20"
#else
#define TFTPC_TIMEOUT       "8"
#endif
#define TFTPC_TSIZE_OPT     "tsize"

#define TFTPC_OP_SIZE      2
#define TFTPC_OCTET_MODE    "octet"
#define TFTPC_SRV_PORT     69
#define TFTPC_LOCAL_PORT   6000

#define TFTPC_RRQ_PKT      1
#define TFTPC_WRQ_PKT      2
#define TFTPC_DATA_PKT     3
#define TFTPC_ACK_PKT      4
#define TFTPC_ERROR_PKT    5
#define TFTPC_OAK_PKT      6

#define TFTPC_BLK_NO_OFFSET        2
#define TFTPC_DATA_OFFSET          4

#define TFTPC_WRITE       TFTPC_WRQ_PKT
#define TFTPC_READ        TFTPC_RRQ_PKT

#define TFTPC_BLOCKSIZE_STD   512
#define TFTPC_BLOCKSIZE_MAX   1432
#ifndef DX260
#define TFTPC_MAX_RETRY       3
#else
#define TFTPC_MAX_RETRY       10
#endif

#define TFTPC_MOD_NAME  "TFTP"

#define TFTPC_CRI_LEVEL   0x1
#define TFTPC_MED_LEVEL   0x2
#define TFTPC_LOW_LEVEL   0x4

#define TFTPC_ZERO        0
#define TFTPC_HUNDRED     100
#define TFTPC_EOS         '\0'
#define TFTPC_ERR_UNKNOWN_ERR 0 
#define TFTPC_ERR_FILE_NOT_FOUND 1 
#define TFTPC_ERR_ACCESS_VIOLATION 2
#define TFTPC_ERR_DISK_FULL 3
#define TFTPC_ERR_ILLEGAL_OPERATION 4
#define TFTPC_ERR_UNKNOWN_TID 5

#define TFTPC_MAX_BLOCKS  65535

#ifdef TRACE_WANTED
#define TFTPC_TRC_LOW(Str) MOD_TRC(gu4TftpcTrcFlag, TFTPC_LOW_LEVEL, \
                                         TFTPC_MOD_NAME, (const char *)Str)
#define TFTPC_TRC_MED(Str) MOD_TRC(gu4TftpcTrcFlag, TFTPC_MED_LEVEL, \
                                         TFTPC_MOD_NAME, (const char *)Str)
#define TFTPC_TRC_CRI(Str) MOD_TRC(gu4TftpcTrcFlag, TFTPC_CRI_LEVEL, \
                                         TFTPC_MOD_NAME, (const char *)Str)
#define TFTPC_TRC_LOW_ARG1(Str, Arg1) MOD_TRC_ARG1(gu4TftpcTrcFlag, TFTPC_LOW_LEVEL, \
                                         TFTPC_MOD_NAME, (const char *)Str, Arg1)
#define TFTPC_TRC_MED_ARG1(Str, Arg1) MOD_TRC_ARG1(gu4TftpcTrcFlag, TFTPC_MED_LEVEL, \
                                         TFTPC_MOD_NAME, (const char *)Str, Arg1)
#define TFTPC_TRC_CRI_ARG1(Str, Arg1) MOD_TRC_ARG1(gu4TftpcTrcFlag, TFTPC_CRI_LEVEL, \
                                         TFTPC_MOD_NAME, (const char *)Str, Arg1)
#define TFTPC_TRC_LOW_ARG2(Str, Arg1, Arg2) MOD_TRC_ARG2(gu4TftpcTrcFlag, TFTPC_LOW_LEVEL, \
                                         TFTPC_MOD_NAME, (const char *)Str, Arg1, Arg2)
#define TFTPC_TRC_CRI_ARG2(Str, Arg1, Arg2) MOD_TRC_ARG2(gu4TftpcTrcFlag, TFTPC_CRI_LEVEL, \
                                         TFTPC_MOD_NAME, (const char *)Str, Arg1, Arg2)

#define TFTPC_ASSERT_ER(err)          
#else
#define TFTPC_TRC_LOW(Str)
#define TFTPC_TRC_MED(Str)
#define TFTPC_TRC_CRI(Str)
#define TFTPC_TRC_LOW_ARG1(Str, Arg1)
#define TFTPC_TRC_MED_ARG1(Str, Arg1)
#define TFTPC_TRC_CRI_ARG1(Str, Arg1)
#define TFTPC_TRC_LOW_ARG2(Str, Arg1, Arg2)
#define TFTPC_TRC_CRI_ARG2(Str, Arg1, Arg2)
#define TFTPC_ASSERT_ER(err)
#endif

typedef enum
{
 ISS_10_PERCENT = 10,
 ISS_20_PERCENT = 20,
 ISS_30_PERCENT = 30,
 ISS_40_PERCENT = 40,
 ISS_50_PERCENT = 50,
 ISS_60_PERCENT = 60,
 ISS_70_PERCENT = 70,
 ISS_80_PERCENT = 80,
 ISS_90_PERCENT = 90
}eTftpStatus;

typedef struct
{
 UINT2  u2OpCode;
 UINT2  u2BlkAckErr;
 UINT1  au1Data[TFTPC_BLOCKSIZE_MAX];
}tTftpcPkt;

VOID tftpcExtractOpt(tTftpcSession *tftpS);
INT4 tftpcProcessResponse(tTftpcSession *tftpS);
INT4 tftpcSendPkt(tTftpcSession *tftpS);
VOID tftpcTerminate(tTftpcSession *tftpS);
INT4 tftpcGetPkt(tTftpcSession *tftpS);
INT4 tftpcConfig(tTftpcSession *tftpS, UINT1 u1Action,UINT2 u2TimeOut, UINT2 u2RetryCnt);
INT4 tftpcConnect (tTftpcSession * tftpS, const UINT1 *pu1File,
                   UINT4 u4FileSize, tIPvXAddr SrvIp);
INT4 tftpcSendAck(tTftpcSession *tftpS);
INT4 tftpcSendErrPkt(tTftpcSession * tftpS,UINT1 u1ErrorCode);

#endif /* __TFTPCSYS_H__ */
