/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                     */
/* $Id: issextn.h,v 1.70 2017/06/29 13:40:45 siva Exp $       */
/* Licensee Aricent Inc., 2001-2002                               */
/*****************************************************************************/
/*    FILE  NAME            : issextn.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*    DESCRIPTION           : This file has all the externs used by ISS.     */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#ifndef _ISSEXTN_H
#define _ISSEXTN_H


extern INT4 gi4MibSaveStatus;

extern INT4 gi4RemoteSaveStatus;  
                         
extern INT4 gi4MibResStatus;

extern INT4 gi4MibResFailureReason;

extern INT4 gi4ClrConfigInProgress;

extern tMsrSavResOption gMsrSavResPriority;

/* Http */
extern INT1         nmhGetFsHttpRequestCount (INT4 *pi4RetValFsHttpRequestCount);
extern INT1         nmhGetFsHttpRequestDiscards (INT4 *pi4RetValFsHttpRequestDiscards);

/* Traffic Separtion and Protection Control */
extern UINT4 gu4TrafficSeprtnControl;

extern UINT4 gu4StackingModel;
/* Bridge */
extern INT1 nmhGetDot1dBaseBridgeStatus PROTO ((INT4 *));
/* LA */
extern INT1 nmhGetFsLaStatus PROTO ((UINT4 *));
extern INT1 nmhGetDot3adAggPortActorAdminState PROTO ((INT4, INT4 *));
extern INT1 nmhGetDot1xPaeSystemAuthControl PROTO ((UINT4 *));
extern INT1 nmhTestv2FsLaSystemControl PROTO ((UINT4 *, UINT4));
extern INT1 nmhSetFsLaSystemControl PROTO ((UINT4));
extern INT1 nmhSetFsLaStatus PROTO ((UINT4));
extern INT1 nmhTestv2Dot3adAggPortActorAdminState PROTO ((UINT4 *, UINT4,
                                                          UINT4));
extern INT1 nmhSetDot3adAggPortActorAdminState PROTO ((UINT4, UINT4));
extern INT1 nmhTestv2FsLaTraceOption PROTO ((UINT4 *, INT4));
extern INT1 nmhSetFsLaTraceOption PROTO ((INT4));
extern VOID IssLaShowDebugging (tCliHandle CliHandle);
extern VOID IssSynceShowDebugging (tCliHandle CliHandle);

/* Spanning Tree Protocol */
extern INT1 nmhTestv2Dot1dStpPortEnable PROTO ((UINT4 *, INT4, INT4));
extern INT1 nmhSetDot1dStpPortEnable PROTO ((INT4, INT4));


/* PNAC */
extern INT1 nmhGetFsPnacAuthenticServer PROTO ((UINT4 *));
extern INT1 nmhGetDot1xAuthAuthControlledPortControl PROTO ((INT4, INT4 *));
extern INT1 nmhTestv2FsPnacSystemControl PROTO ((UINT4 *, UINT4));
extern INT1 nmhSetFsPnacSystemControl PROTO ((UINT4));
extern INT1 nmhTestv2Dot1xPaeSystemAuthControl PROTO ((UINT4 *, UINT4));
extern INT1 nmhSetDot1xPaeSystemAuthControl PROTO ((UINT4));

extern INT1 nmhGetFsPnacNasId PROTO ((tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsPnacNasId PROTO ((UINT4 *, tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsPnacNasId PROTO ((tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsPnacAuthenticServer PROTO ((UINT4 *, UINT4));
extern INT1 nmhSetFsPnacAuthenticServer PROTO ((UINT4));
extern INT1 nmhTestv2FsPnacTraceOption PROTO ((UINT4 *, INT4));
extern INT1 nmhSetFsPnacTraceOption PROTO ((INT4));


/* PNAC Time related setting */
extern INT1 nmhTestv2Dot1xAuthTxPeriod PROTO ((UINT4 *, UINT4, UINT4));
extern INT1 nmhSetDot1xAuthTxPeriod PROTO ((UINT4, UINT4));
extern INT1 nmhTestv2Dot1xAuthQuietPeriod PROTO ((UINT4 *, UINT4, UINT4));
extern INT1 nmhSetDot1xAuthQuietPeriod PROTO ((UINT4, UINT4));
extern INT1 nmhTestv2Dot1xAuthSuppTimeout PROTO ((UINT4 *, UINT4, UINT4));
extern INT1 nmhSetDot1xAuthSuppTimeout PROTO ((UINT4, UINT4));
extern INT1 nmhTestv2Dot1xAuthReAuthPeriod PROTO ((UINT4 *, UINT4, UINT4));
extern INT1 nmhSetDot1xAuthReAuthPeriod PROTO ((UINT4, UINT4));
extern INT1 nmhTestv2Dot1xAuthReAuthEnabled PROTO ((UINT4 *, UINT4, UINT4));
extern INT1 nmhSetDot1xAuthReAuthEnabled PROTO ((UINT4, UINT4));
extern INT1 nmhTestv2FsPnacPaePortAuthMode PROTO ((UINT4 *, UINT4, UINT4));
extern INT1 nmhSetFsPnacPaePortAuthMode PROTO ((UINT4, UINT4));
extern INT1 nmhTestv2Dot1xAuthAuthControlledPortControl PROTO ((UINT4 *, UINT4, UINT4));
extern INT1 nmhSetDot1xAuthAuthControlledPortControl PROTO ((UINT4, UINT4));

/* Vlan */
extern INT1 nmhGetDot1qFutureVlanStatus PROTO ((UINT4 *));
extern INT1 nmhTestv2Dot1qFutureVlanStatus PROTO ((UINT4 *, UINT4));
extern INT1 nmhSetDot1qFutureVlanStatus PROTO ((UINT4));
extern INT1 nmhTestv2Dot1qFutureVlanDebug PROTO ((UINT4 *, INT4));
extern INT1 nmhSetDot1qFutureVlanDebug PROTO ((INT4));

/* Traffic Class */
extern INT1 nmhTestv2Dot1dTrafficClass PROTO ((UINT4 *, INT4, INT4, INT4));
extern INT1 nmhSetDot1dTrafficClass PROTO ((INT4, INT4, INT4));

/* Radius Client*/
extern INT1 nmhTestv2RadiusExtDebugMask PROTO ((UINT4 *, INT4));
extern INT1 nmhSetRadiusExtDebugMask PROTO ((INT4));

/* Bridge */
extern INT1 nmhTestv2Dot1dBaseBridgeStatus PROTO (( UINT4 *, INT4));
extern INT1 nmhSetDot1dBaseBridgeStatus  PROTO ((INT4));
extern INT1 nmhTestv2FsRstModuleStatus PROTO (( UINT4 *, INT4));
extern INT1 nmhSetFsRstModuleStatus PROTO ((INT4));
extern INT1 nmhGetFsRstModuleStatus PROTO ((INT4 *));
extern INT1 nmhTestv2Dot1dStpPriority PROTO ((UINT4 *, INT4));
extern INT1 nmhSetDot1dStpPriority PROTO ((INT4));
extern INT1 nmhGetDot1dStpPriority PROTO ((INT4 *));

/* IP */

extern INT1 nmhTestv2IpForwarding PROTO ((UINT4 *, INT4));
extern INT1 nmhSetIpForwarding PROTO ((INT4));

/* Rmon */
extern INT1 nmhGetRmonEnableStatus PROTO ((INT4 *));
extern INT1 nmhTestv2RmonDebugType PROTO ((UINT4 *, UINT4));
extern INT1 nmhSetRmonDebugType PROTO ((UINT4));

/* Radius */
extern INT1 nmhGetFirstIndexRadiusExtServerTable (INT4 *);

extern INT1 CliInitUsersGroups(VOID);
/* Extren ProtoType for Handling and processing the ACL Queue 
 * message posted to the ISS Task. */

/* Extern Prototypes for Enabling/disabling Routing over Management Interface */
 
extern INT1  LnxIpSetForwarding (UINT1 u1Status);

#ifdef NPAPI_WANTED
extern VOID NpSetTrace PROTO ((UINT1 u1TraceModule, UINT1 u1TraceLevel));
extern VOID NpGetTrace PROTO ((UINT1 u1TraceModule, UINT1 * pu1TraceLevel));
extern VOID NpSetTraceLevel PROTO((UINT2 ));
extern VOID NpGetTraceLevel PROTO((UINT2 *));
#endif

extern VOID EoamCliShowDebugging (tCliHandle);
extern VOID IssPnacShowDebugging (tCliHandle);
extern  VOID IssVlanShowDebugging (tCliHandle);
extern VOID IssGarpShowDebugging (tCliHandle);
extern  VOID IssRadiusShowDebugging (tCliHandle);
extern VOID IssRipShowDebugging (tCliHandle);
extern VOID IssTacacShowDebugging (tCliHandle);
extern VOID IssBgpShowDebugging (tCliHandle);
extern VOID LldpCliShowDebugging(tCliHandle);
extern VOID Rmon2CliShowTrace (tCliHandle CliHandle);
extern VOID DsmonCliShowTrace (tCliHandle CliHandle);
extern VOID IssVrrpShowDebugging (tCliHandle CliHandle);
extern INT4 VcmSispShowRunningConfig PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex));
extern VOID EoamCliShowRunningConfig (tCliHandle, UINT4);
extern INT4 LaShowRunningConfig(tCliHandle,UINT4);
extern INT4 PnacShowRunningConfig(tCliHandle CliHandle, UINT4 u4Module);
extern INT4 RsnaShowRunningConfig (tCliHandle CliHandle, UINT4 u4WlanIndex);
extern INT4 WpsShowRunningConfig (tCliHandle CliHandle, UINT4 u4WlanIndex);
extern INT4 RfmgmtShowRunningConfig (tCliHandle CliHandle);
extern INT4 RipShowRunningConfigCxt (tCliHandle CliHandle, UINT4 u4Module,
                                                                   UINT4 u4ContextId);
extern INT4 BgpShowRunningConfig (tCliHandle CliHandle, UINT1 *pu1ContextName);
extern INT4 VrrpShowRunningConfig (tCliHandle CliHandle);
extern INT4 Snmpv3ShowRunningConfig(tCliHandle CliHandle);
extern INT4 IpShowRunningConfigInCxt (tCliHandle CliHandle, UINT4);
extern INT4 FsIpShowRunningConfigInCxt(tCliHandle CliHandle, UINT4 u4Module, INT4);
extern INT4 RrdShowRunningConfig(tCliHandle CliHandle, INT4 i4ContextId, UINT4);
extern INT4 RadiusShowRunningConfig(tCliHandle CliHandle);
extern INT4 RmonShowRunningConfig(tCliHandle,UINT4);
extern INT4 RmonShowRunningConfigVlan (tCliHandle, UINT1, UINT4);
extern INT4 TacacsShowRunningConfig (tCliHandle CliHandle);
extern INT4 LldpShowRunningConfig(tCliHandle,UINT4);
extern INT4 VlanShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId,
                                    UINT4 u4VlanId, UINT4 u4Module);
extern INT4 VlanMplsShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId,
                                    UINT4 u4VlanId, UINT4 u4Module);
extern INT4 GarpShowRunningConfig(tCliHandle CliHandle, UINT4 u4ContextId);
extern VOID IssMrpShowDebugging (tCliHandle CliHandle);
extern VOID MrpSrcShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId);
extern INT4 RMapShowRunningConfig (tCliHandle CliHandle);
extern VOID EcfmCliShowDebug (tCliHandle CliHandle);
extern INT4 D6SrCliShowRunningConfig (tCliHandle CliHandle,UINT4 u4Module);
extern INT4 D6RlCliShowRunningConfig (tCliHandle CliHandle,UINT4 u4Module);
extern INT4 EcfmShowRunningConfig (tCliHandle, UINT4,UINT4);
extern INT4 D6ClCliShowRunningConfig (tCliHandle CliHandle,UINT4 u4Module);
extern INT4 EcfmGlobalShowRunningConfig (tCliHandle);
extern INT4 L2dsShowRunningConfig (tCliHandle);
#ifdef ERPS_WANTED
extern INT4 ErpsShowRunningConfig(tCliHandle CliHandle);
extern INT4 ErpsCliShowDebugging (tCliHandle CliHandle, UINT4 u4ContextId);
#endif
extern INT4 ElpsCliShowDebugging (tCliHandle CliHandle, UINT4 u4ContextId);
extern INT4 ElpsCliShowRunningConfig(tCliHandle,UINT4);
extern INT4 PbbGlobalShowRunningConfig (tCliHandle CliHandle);
extern INT4 PbbShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId, UINT4
                                  u4Module);
extern INT4 PbbCliShowDebugging (tCliHandle CliHandle);
#ifdef OSPFTE_WANTED
extern VOID IssOspfteCliShowDebugging (tCliHandle);
extern INT4 OspfTeShowRunningConfig (tCliHandle);
#endif

#ifdef TLM_WANTED
extern VOID IssTlmCliShowDebugging (tCliHandle CliHandle);
extern INT4 TlmCliShowRunningConfig (tCliHandle CliHandle);
#endif

#ifdef MPLS_WANTED
extern INT4 MplsShowRunningConfig (tCliHandle CliHandle);
extern INT4 MplsDsTeShowRunningConfig (tCliHandle CliHandle);
extern INT4 MplsVfiShowRunningConfig (tCliHandle CliHandle);
extern INT4 MplsXCShowRunningConfigInterface (tCliHandle CliHandle);
extern INT4 RpteCliRsvpTeShowRunnningConfig (tCliHandle CliHandle);
extern INT4 LdpCliShowRunningConfig (tCliHandle CliHandle);

extern VOID IssMplsCommonShowDebugging (tCliHandle CliHandle);
extern VOID IssMplsTeShowDebugging (tCliHandle CliHandle);
extern VOID IssMplsLdpShowDebugging (tCliHandle CliHandle);
extern VOID IssRsvpShowDebugging (tCliHandle CliHandle);
extern VOID IssMplsOamShowDebugging (tCliHandle CliHandle);
extern VOID IssMplsL2vpnShowDebugging (tCliHandle CliHandle);
extern VOID IssMplsOamEchoShowDebugging (tCliHandle CliHandle);
#endif
#ifdef RFC6374_WANTED
extern VOID IssMplsPmRFC6374ShowDebugging (tCliHandle CliHandle);
#endif
#ifdef BFD_WANTED
   extern VOID IssBfdShowDebugging (tCliHandle CliHandle, UINT4 u4ContextId);
   extern INT4 BfdShowRunningConfig (tCliHandle CliHandle,UINT4 u4ContextId);
#endif
#ifdef MEF_WANTED
extern INT4 MefShowRunningConfig (tCliHandle CliHandle);
#endif
#ifdef WSS_WANTED
extern INT4  WssBatchConfigShowRunningConfig (tCliHandle CliHandle);
extern INT4 WssShowRunningConfig (tCliHandle CliHandle);
#ifdef WSSUSER_WANTED
extern INT4 WssUserShowRunningConfig (tCliHandle CliHandle);
#endif
#endif
extern INT4 NatShowRunningConfig (tCliHandle CliHandle,UINT4 u4Module);
extern INT4 NatShowRunningConfigScalar (tCliHandle CliHandle);
extern INT4 NatShowRunningConfigInterface (tCliHandle CliHandle);
extern INT4 NatShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index);
extern INT4 FwlShowRunningConfig (tCliHandle CliHandle);
extern INT4 IpdbShowRunningConfig  (tCliHandle, UINT4,UINT4);
extern VOID FmCliShowDebugging (tCliHandle);
extern VOID FmCliShowRunningConfig(tCliHandle,UINT4);
extern VOID CliExecTimeoutShowRunningConfig (tCliHandle);
#ifdef DHCP6_CLNT_WANTED
extern VOID IssDhcp6ClientShowDebugging(tCliHandle);
#endif
#ifdef DHCP6_RLY_WANTED
extern VOID IssDhcp6RelayShowDebugging(tCliHandle);
#endif
#ifdef DHCP6_SRV_WANTED
extern VOID IssDhcp6ServerShowDebugging (tCliHandle);
#endif

#ifdef FM_WANTED
#ifdef MPLS_WANTED
extern VOID IssMplsFmShowDebugging (tCliHandle CliHandle);
#endif
#endif

extern INT4 DsmonShowRunningConfig(tCliHandle);
#ifdef RMON2_WANTED
extern INT4 Rmon2ShowRunningConfig(tCliHandle);
#endif
#ifdef CFA_WANTED
extern VOID IssDhcpSnoopingShowDebugging (tCliHandle CliHandle);
extern VOID IssInterfaceShowDebugging (tCliHandle CliHandle);
extern INT4 CfaShShowRunningConfig PROTO ((tCliHandle CliHandle, UINT4 u4Module));
extern INT4 CfaUfdShowRunningConfig PROTO ((tCliHandle CliHandle, UINT4 u4Module));
#endif

#ifdef MSDP_WANTED
extern VOID IssMsdpShowDebugging (tCliHandle);
#endif
#ifdef ISIS_WANTED
extern VOID IsisCliShowDbgInfo (tCliHandle CliHandle);
#endif

#ifdef PBBTE_WANTED
extern VOID IssPbbTeShowDebugging (tCliHandle CliHandle);
#endif

#ifdef MLD_WANTED
extern VOID IssMldShowDebugging (tCliHandle CliHandle);
#endif

#ifdef PPP_WANTED
extern INT4 PppShowRunningConfig(tCliHandle, UINT4);
#endif
#ifdef FSB_WANTED
extern INT4 FsbCliShowRunningConfig (tCliHandle CliHandle);
#endif
#ifdef EVB_WANTED
extern INT4  VlanEvbShowRunningConfig(tCliHandle CliHandle);

#endif
#ifdef ISSU_WANTED
extern VOID  IssuCliShowDebug (tCliHandle CliHandle);
#endif
#endif/* _ISSEXTN_H */
