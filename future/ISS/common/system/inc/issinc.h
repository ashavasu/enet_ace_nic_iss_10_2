/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: issinc.h,v 1.40 2016/05/19 10:57:31 siva Exp $
 *
 * Description           : This file contains all the include files 
 *                         for ISS module.
 *****************************************************************************/

/*****************************************************************************/
/*    FILE  NAME            : issinc.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#ifndef _ISSINC_H
#define _ISSINC_H

/* Common Includes */
#include "lr.h"
#include "cfa.h"
#include "tcp.h"
#include "fssocket.h"
#include "fswebnm.h"
#include "iss.h"
#include "msr.h"
#include "ip.h"
#include "ospf.h"
#include "ospf3.h"
#include "isis.h"
#include "rstp.h" 
#include "elm.h" 
#include "fsvlan.h"
#include "la.h"
#include "firewall.h"
#include "eoam.h"
#include "mpls.h"
#include "elps.h"
#include "bfd.h"
#include "cli.h"
#include "httpssl.h"
#include "cust.h"
#include "utlbit.h"
#include "tac.h"
#include "sshfs.h"
#include "snmccons.h"
#include "snmcdefn.h"
#include "fssnmp.h"
#include "utilrand.h"
#include "fips.h"
#include "snmputil.h"
#include "issnpwr.h"
#include "isscli.h"
#include "sftpc.h"
#ifdef KERNEL_WANTED
#include "chrdev.h"
#include "isswdefs.h"
#include "nmhtdfs.h"
#endif
#ifdef NPAPI_WANTED
#include "npapi.h"
#include "issnpwr.h"
#include "issnp.h"
#include "isspinp.h"
#include "cfanp.h"
#include "cfanpwr.h"
#ifdef MBSM_WANTED
#include "mbsnp.h"
#endif /* MBSM_WANTED */
#endif
#include "issu.h"
/* ISS includes */
#include "issipmgr.h"
#include "issmacro.h"
#include "issextn.h"
#include "issdefs.h"

#ifdef L2RED_WANTED
#include "issred.h"
#endif /* L2RED_WANTED */

#include "isstdfs.h"
#include "issglob.h"


#include "isslow.h"
#include "issproto.h"
#include "issnvram.h"
#include "issweb.h"

#ifndef _ENTINC_H
#include "entinc.h"
#endif
#include "bridge.h"
#include "l2iwf.h"

#ifdef MBSM_WANTED
#include "mbsm.h"
#include "issmbsm.h"
#endif

#ifdef QOSX_WANTED
#include "qosxcli.h"
#endif

#include "isspiinc.h"

#include "systemsz.h"
#include "csr.h"

#ifdef WSS_WANTED
#include "wssproto.h"

#ifdef WTP_WANTED
#include "wtpnvram.h"
#endif

#ifdef WLC_WANTED
#include "wlcnvram.h"
#endif
#endif

#ifdef ICCH_WANTED
#include "icch.h"
#endif
#endif /* _ISSINC_H */
