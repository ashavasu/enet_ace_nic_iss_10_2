####################################################
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
#
# $Id: make.h,v 1.12 2014/06/05 12:16:38 siva Exp $
#
####################################################

#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 10/05/2002                                    |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            including for building the FutureISS          |
# |                            product.                                      |
# |                                                                          |
# +--------------------------------------------------------------------------+
#
#     CHANGE RECORD :
# +--------------------------------------------------------------------------+
# | VERSION | AUTHOR/    | DESCRIPTION OF CHANGE                             |
# |         | DATE       |                                                   |
# +---------|------------|---------------------------------------------------+
# |   1     | ISS        | Creation of makefile                              |
# |         | 10/05/2002 |                                                   |
# +--------------------------------------------------------------------------+

#Compilation Switches

SYSTEM_COMPILATION_SWITCHES += -UISS_HEALTH_TEST_WANTED

# Set the PROJ_BASE_DIR as the directory where you untar the project files
PROJECT_NAME        = FutureISS
PROJECT_BASE_DIR    = ${ISS_COMMON_DIR}/system
PROJECT_SOURCE_DIR  = ${PROJECT_BASE_DIR}/src
PROJECT_INCLUDE_DIR = ${PROJECT_BASE_DIR}/inc
PROJECT_OBJECT_DIR  = ${PROJECT_BASE_DIR}/obj

CFA_INC_DIR         = -I$(BASE_DIR)/cfa2/inc
SSH_INC_DIR         = -I$(BASE_DIR)/ssh/inc
ENT_INC_DIR         = -I$(PROJECT_BASE_DIR)/ent/inc
EXT_INC_DIR         = -I$(ISS_EXT_DIR)/inc

ifeq (${WSSCFG}, YES)
WSSCFG_INC_DIR      = -I$(BASE_DIR)/wss/wsscfg/inc
ifeq (${RFMGMT}, YES)
RFMGMT_INC_DIR      = -I$(BASE_DIR)/wss/rfmgmt/inc
endif
endif

# Specify the project include directories and dependencies
PROJECT_INCLUDE_FILES  = $(PROJECT_INCLUDE_DIR)/isstdfs.h \
                         $(PROJECT_INCLUDE_DIR)/issmacro.h \
                         $(PROJECT_INCLUDE_DIR)/issipmgr.h \
                         $(PROJECT_INCLUDE_DIR)/issproto.h \
                         $(PROJECT_INCLUDE_DIR)/issglob.h \
                         $(PROJECT_INCLUDE_DIR)/issinc.h \
                         $(PROJECT_INCLUDE_DIR)/isslow.h \
                         $(PROJECT_INCLUDE_DIR)/issweb.h \
                         $(BASE_DIR)/cfa2/inc/ifmiblow.h \
                         $(BASE_DIR)/inc/cust.h \
                         $(BASE_DIR)/inc/iss.h

GLOBAL_INCLUDES  = -I$(PROJECT_INCLUDE_DIR)

PROJECT_FINAL_INCLUDES_DIRS=   ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS} ${EXT_INC_DIR} ${CFA_INC_DIR} ${SSH_INC_DIR} ${ENT_INC_DIR}


ifeq (${WSSCFG}, YES)
PROJECT_FINAL_INCLUDES_DIRS += $(WSSCFG_INC_DIR)
endif

PROJECT_FINAL_INCLUDE_FILES = $(PROJECT_INCLUDE_FILES)

PROJECT_DEPENDENCIES = $(COMMON_DEPENDENCIES) \
                       $(PROJECT_FINAL_INCLUDE_FILES) \
                       $(PROJECT_BASE_DIR)/Makefile \
                       $(PROJECT_BASE_DIR)/make.h


