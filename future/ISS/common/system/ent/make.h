#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 10/05/2002                                    |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            including for building the FutureISS          |
# |                            product.                                      |
# |                                                                          |
# +--------------------------------------------------------------------------+
#
#     CHANGE RECORD :
# +--------------------------------------------------------------------------+
# | VERSION | AUTHOR/    | DESCRIPTION OF CHANGE                             |
# |         | DATE       |                                                   |
# +---------|------------|---------------------------------------------------+
# |   1     | ISS        | Creation of makefile                              |
# |         | 10/05/2002 |                                                   |
# +--------------------------------------------------------------------------+


# Set the PROJ_BASE_DIR as the directory where you untar the project files
ENT_NAME         = FutureEnt
ENT_BASE_DIR     = ${ISS_COMMON_DIR}/system/ent
ENT_SOURCE_DIR   = ${ENT_BASE_DIR}/src
ENT_INCLUDE_DIR  = ${ENT_BASE_DIR}/inc
ENT_OBJECT_DIR   = ${ENT_BASE_DIR}/obj

CLI_INC_DIR      = -I$(COMN_INCL_DIR)/cli
CFA_INC_DIR      = -I$(BASE_DIR)/cfa2/inc
SYSTEM_INC_DIR   = -I${ISS_COMMON_DIR}/system/inc

# Specify the project include directories and dependencies
ENT_INCLUDE_FILES  = $(ENT_INCLUDE_DIR)/entdefs.h \
                     $(ENT_INCLUDE_DIR)/entinc.h \
                     $(ENT_INCLUDE_DIR)/entprot.h \
                     $(ENT_INCLUDE_DIR)/entport.h \
                     $(COMN_INCL_DIR)/cli/stdentcli.h \
                     $(ENT_INCLUDE_DIR)/stdentdb.h \
                     $(ENT_INCLUDE_DIR)/stdent.h \
                     $(ENT_INCLUDE_DIR)/stdentlw.h \
                     $(ENT_INCLUDE_DIR)/stdentwr.h

GLOBAL_INCLUDES   = -I$(ENT_INCLUDE_DIR)

ENT_FINAL_INCLUDES_DIRS =  $(GLOBAL_INCLUDES) $(COMMON_INCLUDE_DIRS) $(CLI_INC_DIR) $(CFA_INC_DIR) $(SYSTEM_INC_DIR)

ENT_FINAL_INCLUDE_FILES = $(ENT_INCLUDE_FILES)

ENT_DEPENDENCIES = $(COMMON_DEPENDENCIES) \
                   $(ENT_FINAL_INCLUDE_FILES) \
                   $(ENT_BASE_DIR)/Makefile \
                   $(ENT_BASE_DIR)/make.h
