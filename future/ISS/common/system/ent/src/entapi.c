/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 *$Id: entapi.c,v 1.1 2015/09/09 13:43:18 siva Exp $
 *
 * Description: This file contains the APIs to get Physical Entity Information 
 ********************************************************************/
#ifndef __ENTAPI_C
#define __ENTAPI_C

#include "entinc.h"

/*****************************************************************************
 * Function Name      : EntApiGetInventoryInfo                               *
 *                                                                           *
 * Description        : This function is called to get physical entity device*
 *                      information that has to be used in LLDP-MED Inventory*
 *                      TLV of the local device                              *
 *                                                                           *
 * Input(s)           : NONE                                                 *
 *                                                                           *
 * Output(s)          : pInventoryInfo - Inventory Info of the entity        *
 *                                                                           *
 * Return Value(s)    : SUCCESS/FAILURE                                      *
 ****************************************************************************/
PUBLIC INT1
EntApiGetInventoryInfo (tLldpMedLocInventoryInfo *pInventoryInfo)
{
    tSNMP_OCTET_STRING_TYPE PhyEntInfo;    
    UINT1                   au1PhyEntInfo[ISS_ENT_PHY_HW_REV_LEN];
    INT4                    i4PhyIndex = 0;

    MEMSET (&PhyEntInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1PhyEntInfo, 0, ISS_ENT_PHY_HW_REV_LEN);

    PhyEntInfo.pu1_OctetList = au1PhyEntInfo;
    PhyEntInfo.i4_Length = 0;
    i4PhyIndex = 1;
    /* Get Hardware Revision of the local device*/
    if (nmhGetEntPhysicalHardwareRev (i4PhyIndex, &PhyEntInfo) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    MEMCPY(pInventoryInfo->au1MedLocHardwareRev, PhyEntInfo.pu1_OctetList, PhyEntInfo.i4_Length);

    /* Reset the values before using it*/
    MEMSET (&PhyEntInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1PhyEntInfo, 0, ISS_ENT_PHY_HW_REV_LEN);

    PhyEntInfo.pu1_OctetList = au1PhyEntInfo;
    PhyEntInfo.i4_Length = 0;

    /* Get Software Revision of the local device*/
    if (nmhGetEntPhysicalSoftwareRev (i4PhyIndex, &PhyEntInfo) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    MEMCPY(pInventoryInfo->au1MedLocSoftwareRev, PhyEntInfo.pu1_OctetList, PhyEntInfo.i4_Length);

    /* Reset the values before using it*/
    MEMSET (&PhyEntInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1PhyEntInfo, 0, ISS_ENT_PHY_HW_REV_LEN);

    PhyEntInfo.pu1_OctetList = au1PhyEntInfo;
    PhyEntInfo.i4_Length = 0;

    /* Get the Firmware Revision of Local device */
    if (nmhGetEntPhysicalFirmwareRev (i4PhyIndex, &PhyEntInfo) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    MEMCPY(pInventoryInfo->au1MedLocFirmwareRev, PhyEntInfo.pu1_OctetList, PhyEntInfo.i4_Length);

    /* Reset the values before using it*/
    MEMSET (&PhyEntInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1PhyEntInfo, 0, ISS_ENT_PHY_HW_REV_LEN);

    PhyEntInfo.pu1_OctetList = au1PhyEntInfo;
    PhyEntInfo.i4_Length = 0;

    /* Get the Serial Number of Local device */
    if (nmhGetEntPhysicalSerialNum (i4PhyIndex, &PhyEntInfo) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    MEMCPY(pInventoryInfo->au1MedLocSerialNum, PhyEntInfo.pu1_OctetList, PhyEntInfo.i4_Length);

    /* Reset the values before using it*/
    MEMSET (&PhyEntInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1PhyEntInfo, 0, ISS_ENT_PHY_HW_REV_LEN);

    PhyEntInfo.pu1_OctetList = au1PhyEntInfo;
    PhyEntInfo.i4_Length = 0;

    /* Get the Manufacturer Name of Local device */
    if (nmhGetEntPhysicalMfgName (i4PhyIndex, &PhyEntInfo) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    MEMCPY(pInventoryInfo->au1MedLocMfgName, PhyEntInfo.pu1_OctetList, PhyEntInfo.i4_Length);

    /* Reset the values before using it*/
    MEMSET (&PhyEntInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1PhyEntInfo, 0, ISS_ENT_PHY_HW_REV_LEN);

    PhyEntInfo.pu1_OctetList = au1PhyEntInfo;
    PhyEntInfo.i4_Length = 0;

    /* Get the Model Name of Local device */
    if (nmhGetEntPhysicalModelName (i4PhyIndex, &PhyEntInfo) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    MEMCPY(pInventoryInfo->au1MedLocModelName, PhyEntInfo.pu1_OctetList, PhyEntInfo.i4_Length);

    /* Reset the values before using it*/
    MEMSET (&PhyEntInfo, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&au1PhyEntInfo, 0, ISS_ENT_PHY_HW_REV_LEN);

    PhyEntInfo.pu1_OctetList = au1PhyEntInfo;
    PhyEntInfo.i4_Length = 0;
   
    /* Get the Asset ID of Local device */
    if (nmhGetEntPhysicalAssetID (i4PhyIndex, &PhyEntInfo) == SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    MEMCPY(pInventoryInfo->au1MedLocAssetId, PhyEntInfo.pu1_OctetList, PhyEntInfo.i4_Length);

 return OSIX_SUCCESS;
}

#endif
