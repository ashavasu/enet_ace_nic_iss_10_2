/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 *$Id: entutil.c,v 1.19 2014/07/17 12:34:06 siva Exp $
 *
 * Description: This file contains the Utility Procedures used for
 *              Entity MIB
 ********************************************************************/

#include "entinc.h"

PRIVATE VOID EntUtilCalcPhyCount PROTO ((UINT4 *, UINT4));
PRIVATE INT1
    EntUtilProcessPhyIndex PROTO ((INT4 *, INT4, UINT4, INT4 *,
                                   INT1 *, INT4 *, INT4 *, UINT4 *));
PRIVATE INT1
    EntUtilIdentifyPhyComponent PROTO ((INT4, INT1 *, INT4 *,
                                        INT4 *, UINT4 *, UINT4 *));

/*****************************************************************************
 * Function Name      : EntUtilGetRegisteredMibEntry                         *
 *                                                                           *
 * Description        : This function is utility routine that is used to     *
 *                      fetch registered mib entry from the SNMP Agent       *
 *                                                                           *
 * Input(s)           : u4LogIndex - Identifying the logical component       * 
 *                                                                           *
 * Output(s)          : pMib - Mib correspondign to teh logical index        *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
 ****************************************************************************/
PUBLIC INT1
EntUtilGetRegisteredMibEntry (UINT4 u4LogIndex, UINT1 *pMib)
{

    UINT4               u4InstMibCount = 0;
    UINT4               u4NonInstMibCount = 0;
    UINT4               u4TotalMibCount = 0;
    UINT4               u4MibNumber = 0;
    UINT4               u4Temp = 0;

    EntSnmpGetMIBCountWithContextIdAndLock (&u4InstMibCount,
                                            &u4NonInstMibCount);

    u4TotalMibCount = u4InstMibCount + u4NonInstMibCount;

    if (u4LogIndex < u4NonInstMibCount)
    {
        u4MibNumber = u4LogIndex;
        EntSnmpGetMIBNameWithLock (u4MibNumber, pMib);
    }
    else if ((u4LogIndex > u4NonInstMibCount)
             && (u4LogIndex <= u4TotalMibCount))
    {
        u4MibNumber = u4LogIndex - u4NonInstMibCount;
        if (EntSnmpGetMIBNameWithContextIdAndLock (u4MibNumber, pMib) ==
            OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    else if (u4LogIndex > u4TotalMibCount)
    {
        u4Temp = u4LogIndex - u4TotalMibCount;
        if ((u4Temp % u4InstMibCount) == 0)
        {
            u4MibNumber = u4InstMibCount;
        }
        else
        {
            u4MibNumber = (u4Temp % u4InstMibCount);
        }
        if (EntSnmpGetMIBNameWithContextIdAndLock (u4MibNumber, pMib) ==
            OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntUtilIdentifyPhyComponent                          *
 *                                                                           *
 * Description        : This function is the utility function to identify    *
 *                      the physical component through traversal of all teh  *
 *                      physical components prsent                           *
 *                                                                           *
 * Input(s)           : i4PhyIndex - Identifying the physical component      *
 *                                                                           *
 * Output(s)          : pu4ChassisNum - Chassis number in which physical     *
 *                      component is present.                                *
 *                      pi1PhyClass - Type of Physical component             *
 *                      pi4ContainedIn - Contained In information for this   *
 *                        physical component                                   *
 *                        pu4LocalIndex - It is the index which identifies the *
 *                        position of the physical component.                  *
 *                                                                           *
 * Output(s)          : pPhyInfo - Physical Componet Information             *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
 ****************************************************************************/
PRIVATE INT1
EntUtilIdentifyPhyComponent (INT4 i4PhyIndex, INT1 *pi1PhyClass,
                             INT4 *pi4ContainedIn, INT4 *pi4ParentRelPos,
                             UINT4 *pu4ChassisNum, UINT4 *pu4LocalIndex)
{
    UINT4               u4ChassisNum = 0;
    INT4                i4LocalPhyIndex = 0;
    INT4                i4TraversedPI = 0;
    INT4                i4ChassisPI = 0;

#ifdef STACKING_WANTED

    /*This case is of pizza box stacking */
    UINT2               u2ChassisCount = 0;
    UINT2               u2PSCount = 0;
    UINT2               u2CpuCount = 0;
    UINT2               u2FanCount = 0;
    UINT2               u2LocalRelPos = 0;
    if (i4PhyIndex == 1)
    {
        *pi4ContainedIn = 0;
        *pi1PhyClass = ENT_PHY_CLASS_STACK;
        *pi4ParentRelPos = 0;
        *pu4ChassisNum = 0;
        *pu4LocalIndex = 0;
        return OSIX_SUCCESS;
    }
    else
    {
        i4LocalPhyIndex = i4PhyIndex - 1;

        EntIssGetPSCountInStack (&u2PSCount);
        if (i4LocalPhyIndex <= u2PSCount)
        {
            /*Find the PHInfo for i4LocalPhyIndex number power-supply */
            *pi4ContainedIn = 1;
            *pi1PhyClass = ENT_PHY_CLASS_POWER;
            *pi4ParentRelPos = i4LocalPhyIndex;
            *pu4ChassisNum = 0;
            *pu4LocalIndex = i4LocalPhyIndex;
            return OSIX_SUCCESS;
        }

        i4LocalPhyIndex = i4LocalPhyIndex - u2PSCount;
        u2LocalRelPos = u2LocalRelPos + u2PSCount;

        EntIssGetCpuCountInStack (&u2CpuCount);
        if (i4LocalPhyIndex <= u2CpuCount)
        {
            /*Find the PHInfo for i4LocalPhyIndex number power-supply */
            *pi4ContainedIn = 1;
            *pi1PhyClass = ENT_PHY_CLASS_CPU;
            *pi4ParentRelPos = i4LocalPhyIndex + u2LocalRelPos;
            *pu4ChassisNum = 0;
            *pu4LocalIndex = i4LocalPhyIndex;
            return OSIX_SUCCESS;
        }

        i4LocalPhyIndex = i4LocalPhyIndex - u2CpuCount;
        u2LocalRelPos = u2LocalRelPos + u2CpuCount;

        EntIssGetFanCountInStack (&u2FanCount);
        if (i4LocalPhyIndex <= u2FanCount)
        {
            /*Find the PHInfo for i4LocalPhyIndex number power-supply */
            *pi4ContainedIn = 1;
            *pi1PhyClass = ENT_PHY_CLASS_FAN;
            *pi4ParentRelPos = i4LocalPhyIndex + u2LocalRelPos;
            *pu4ChassisNum = 0;
            *pu4LocalIndex = i4LocalPhyIndex;
            return OSIX_SUCCESS;
        }

        i4LocalPhyIndex = i4LocalPhyIndex - u2FanCount;
        u2LocalRelPos = u2LocalRelPos + u2FanCount;

        EntIssGetChassisCount (&u2ChassisCount);
        if (i4LocalPhyIndex <= u2ChassisCount)
        {
            u4ChassisNum = i4LocalPhyIndex;
            *pi4ContainedIn = 1;
            *pi1PhyClass = ENT_PHY_CLASS_CHASSIS;
            *pi4ParentRelPos = i4LocalPhyIndex + u2LocalRelPos;
            *pu4ChassisNum = 0;
            *pu4LocalIndex = i4LocalPhyIndex;
            return OSIX_SUCCESS;
        }

        i4LocalPhyIndex = i4LocalPhyIndex - u2ChassisCount;

        u4ChassisNum = 0;
        i4TraversedPI = 1 + u2PSCount + u2CpuCount +
            u2FanCount + u2ChassisCount;

        /* To traverse the Fan, CPU, Power Supply, Slot, Card, 
         * Interface Physical Components*/
        while (i4LocalPhyIndex > 0)
        {
            u4ChassisNum = u4ChassisNum + 1;
            i4ChassisPI = 1 + u2PSCount + u2CpuCount +
                u2FanCount + u4ChassisNum;
            *pu4ChassisNum = u4ChassisNum;
            if (EntUtilProcessPhyIndex (&i4TraversedPI, i4ChassisPI,
                                        u4ChassisNum, &i4LocalPhyIndex,
                                        pi1PhyClass, pi4ContainedIn,
                                        pi4ParentRelPos, pu4LocalIndex) ==
                OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }
        }
    }                            /*end of else part */
#else
    /*This case is common for pizza box and chassis */
    if (i4PhyIndex == 1)
    {
        u4ChassisNum = 1;
        *pi4ContainedIn = 0;
        *pi1PhyClass = ENT_PHY_CLASS_CHASSIS;
        *pi4ParentRelPos = 0;
        *pu4ChassisNum = 1;
        *pu4LocalIndex = 0;

    }
    else
    {
        i4LocalPhyIndex = i4PhyIndex - 1;
        i4ChassisPI = 1;
        u4ChassisNum = 1;
        i4TraversedPI = 1;
        *pu4ChassisNum = 1;

        if (EntUtilProcessPhyIndex (&i4TraversedPI, i4ChassisPI, u4ChassisNum,
                                    &i4LocalPhyIndex, pi1PhyClass,
                                    pi4ContainedIn, pi4ParentRelPos,
                                    pu4LocalIndex) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
#endif

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntUtilGetPhysicalInfo                               *
 *                                                                           *
 * Description        : This function is the utility function to get the     *
 *                      physical component information from the CFA/MBSM/ISS *
 *                      modules                                              *
 *                                                                           *
 * Input(s)           : i4PhyIndex - Identifying the physical component      *
 *                                                                           *
 * Output(s)          : pPhyInfo - Physical Componet Information             *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
 ****************************************************************************/
PUBLIC INT1
EntUtilGetPhysicalInfo (INT4 i4PhyIndex, tIssEntPhyInfo * pPhyInfo)
{

    UINT4               u4ChassisNum = 0;
    UINT4               u4LocalIndex = 0;

    INT1                i1PhyClass = 0;
    INT4                i4ContainedIn = 0;
    INT4                i4ParentRelPos = 0;

    if (i4PhyIndex > ENT_MAX_PHY_ENTITY)
    {
        return OSIX_FAILURE;
    }

    pPhyInfo->b1IsFRU = OSIX_TRUE;

    EntUtilIdentifyPhyComponent (i4PhyIndex, &i1PhyClass, &i4ContainedIn,
                                 &i4ParentRelPos, &u4ChassisNum, &u4LocalIndex);

    pPhyInfo->i1PhyClass = i1PhyClass;
    pPhyInfo->i4ContainedIn = i4ContainedIn;
    pPhyInfo->i4ParentRelPos = i4ParentRelPos;

    /*This case is of pizza box stacking */

    switch (i1PhyClass)
    {
        case ENT_PHY_CLASS_STACK:
            EntIssGetStackInfo (pPhyInfo);
            break;

        case ENT_PHY_CLASS_POWER:
            if (u4ChassisNum == 0)
            {
                EntIssGetPSInfoInStack (u4LocalIndex, pPhyInfo);
            }
            else
            {
                EntIssGetPSInfoInChassis (u4ChassisNum, u4LocalIndex, pPhyInfo);
            }
            break;

        case ENT_PHY_CLASS_CPU:
            if (u4ChassisNum == 0)
            {
                EntIssGetCpuInfoInStack (u4LocalIndex, pPhyInfo);
            }
            else
            {
                EntIssGetCpuInfoInChassis (u4ChassisNum, u4LocalIndex,
                                           pPhyInfo);
            }
            break;

        case ENT_PHY_CLASS_FAN:
            if (u4ChassisNum == 0)
            {
                EntIssGetFanInfoInStack (u4LocalIndex, pPhyInfo);
            }
            else
            {
                EntIssGetFanInfoInChassis (u4ChassisNum, u4LocalIndex,
                                           pPhyInfo);
            }

            break;

        case ENT_PHY_CLASS_CHASSIS:
            EntIssGetChassisInfo (u4ChassisNum, pPhyInfo);
            break;

        case ENT_PHY_CLASS_CONTAINER:
            if (EntMbsmGetSlotPhyInfo (u4ChassisNum, u4LocalIndex,
                                       pPhyInfo) != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            break;

        case ENT_PHY_CLASS_MODULE:
            if (EntMbsmGetCardPhyInfo (u4ChassisNum, u4LocalIndex,
                                       pPhyInfo) != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            break;

        case ENT_PHY_CLASS_PORT:

            if (EntIssIfGetPhyInfo (u4ChassisNum, u4LocalIndex, pPhyInfo)
                != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }

            pPhyInfo->b1IsFRU = OSIX_FALSE;
            break;
    }

    return OSIX_SUCCESS;
}

 /****************************************************************************
 * Function Name      : EntUtilProcessPhyIndex                               *
 *                                                                           *
 * Description        : This function is the utility function to get the     *
 *                      physical component information from the CFA/MBSM/ISS *
 *                      modules                                              *
 *                                                                           *
 * Input(s)           : i4ChassisPI - Physical Index of teh chassis          *
 *                      u4ChassisNum - Chassis Number                        *
 *                                                                           *
 * Output(s)          : pi4PhyIndex - Updated Physical Index value used for  *
 *                      traversal                                            *
 *                      pPhyInfo - Physical Componet Information             *
 *                      i4TraversedPI - Traversed Physical Index             * 
 *                      pi1PhyClass - Type of Physical component             *
 *                      pi4ContainedIn - Contained In information for this   *
 *                        physical component                                   *
 *                        pu4LocalIndex - It is the index which identifies the *
 *                        position of the physical component.                  *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
 ****************************************************************************/

PRIVATE INT1
EntUtilProcessPhyIndex (INT4 *pi4TraversedPI, INT4 i4ChassisPI,
                        UINT4 u4ChassisNum, INT4 *pi4PhyIndex,
                        INT1 *pi1PhyClass, INT4 *pi4ContainedIn,
                        INT4 *pi4ParentRelPos, UINT4 *pu4LocalIndex)
{
#if defined (MBSM_WANTED)
    UINT4               u4LocalIfIndex = 0;
#endif
    INT4                i4PhyIndex = *pi4PhyIndex;
#if !defined (STACKING_WANTED)
    INT4                i4SlotId = 0;
    UINT4               u4Temp = 0;
#endif
#if ((!defined (STACKING_WANTED)) && \
    (defined (MBSM_WANTED)))
    UINT2               u2MaxSlot = 0;
    UINT2               u2LocalCardCount = 0;
    UINT2               u2Count = 0;
#endif

#if ((!defined (STACKING_WANTED)) && (!defined (MBSM_WANTED)))
    UINT4               u4IfIndex = 0;
#endif
    UINT2               u2PSCount = 0;
    UINT2               u2CpuCount = 0;
    UINT2               u2FanCount = 0;
    UINT2               u2LocalRelPos = 0;
#ifdef MBSM_WANTED
    tMbsmSlotInfo       MbsmSlotMapTable;
    INT4                i4IfContainedIn = 0;
    /*For calculating phy index of next chassis */
    UINT4               u4ActivePortCount = 0;
#endif
#if ((defined (STACKING_WANTED)) && (defined (MBSM_WANTED)))
    INT4                i4TotalTraversedPI = 0;
#endif

    EntIssGetCpuCountInChassis (u4ChassisNum, &u2CpuCount);
    EntIssGetPSCountInChassis (u4ChassisNum, &u2PSCount);
    EntIssGetFanCountInChassis (u4ChassisNum, &u2FanCount);

    if ((i4PhyIndex >= 1) && (i4PhyIndex <= (u2CpuCount)))
    {
        *pi4ContainedIn = i4ChassisPI;
        *pi1PhyClass = ENT_PHY_CLASS_CPU;
        *pi4ParentRelPos = i4PhyIndex;
        *pu4LocalIndex = i4PhyIndex;
        *pi4PhyIndex = 0;
        return OSIX_SUCCESS;
    }
    i4PhyIndex = i4PhyIndex - u2CpuCount;
    *pi4TraversedPI = *pi4TraversedPI + u2CpuCount;
    u2LocalRelPos = u2LocalRelPos + u2CpuCount;

    if ((i4PhyIndex >= 1) && (i4PhyIndex <= (u2PSCount)))
    {
        *pi4ContainedIn = i4ChassisPI;
        *pi1PhyClass = ENT_PHY_CLASS_POWER;
        *pi4ParentRelPos = (INT4) (i4PhyIndex + u2LocalRelPos);
        *pu4LocalIndex = i4PhyIndex;
        *pi4PhyIndex = 0;
        return OSIX_SUCCESS;
    }
    i4PhyIndex = i4PhyIndex - u2PSCount;
    *pi4TraversedPI = *pi4TraversedPI + u2PSCount;
    u2LocalRelPos = u2LocalRelPos + u2PSCount;

    if ((i4PhyIndex >= 1) && (i4PhyIndex <= (u2FanCount)))
    {
        *pi4ContainedIn = i4ChassisPI;
        *pi1PhyClass = ENT_PHY_CLASS_FAN;
        *pi4ParentRelPos = (INT4) (u2LocalRelPos + i4PhyIndex);
        *pu4LocalIndex = i4PhyIndex;
        *pi4PhyIndex = 0;
        return OSIX_SUCCESS;
    }
    i4PhyIndex = i4PhyIndex - u2FanCount;
    *pi4TraversedPI = *pi4TraversedPI + u2FanCount;
    u2LocalRelPos = u2LocalRelPos + u2FanCount;

#if ((!defined (STACKING_WANTED)) && \
    (defined (MBSM_WANTED)))

    /*This case is of chassis */

    u2MaxSlot = ENT_MAX_LC_SLOTS;

    /*physical component - Slots */
    if ((i4PhyIndex >= MBSM_SLOT_INDEX_START) &&
        (i4PhyIndex <= (ENT_MAX_LC_SLOTS + MBSM_SLOT_INDEX_START)))
    {
        i4SlotId = i4PhyIndex;
        *pi4ContainedIn = i4ChassisPI;
        *pi1PhyClass = ENT_PHY_CLASS_CONTAINER;
        *pi4ParentRelPos = (INT4) (i4SlotId + u2LocalRelPos);
        if (ENT_MAX_CC_SLOTS)
        {
            *pu4LocalIndex = i4SlotId + ENT_MAX_CC_SLOTS - 1;
        }
        *pi4PhyIndex = 0;
        return OSIX_SUCCESS;
    }
    /*physical components - Card or Interface */
    else if (i4PhyIndex > (u2MaxSlot))
    {
        u4Temp = i4PhyIndex - u2MaxSlot;
        u2Count = MBSM_SLOT_INDEX_START;
        i4IfContainedIn = *pi4TraversedPI + u2MaxSlot;
        while ((u4Temp > 0) && (u2Count <= u2MaxSlot))
        {
            MEMSET (&MbsmSlotMapTable, 0x00, sizeof (tMbsmSlotInfo));

            if ((EntMbsmGetSlotInfo
                 ((u2Count + ENT_MAX_CC_SLOTS),
                  &MbsmSlotMapTable) != OSIX_SUCCESS))
            {
                u2Count++;
                continue;
            }
            if (MbsmSlotMapTable.i1Status != MBSM_STATUS_ACTIVE)
            {
                u2Count++;
                continue;
            }
            u2LocalCardCount = u2LocalCardCount + 1;
            i4IfContainedIn = i4IfContainedIn + 1;

            u4ActivePortCount =
                EntUtilGetActivePortCount (MbsmSlotMapTable.u4NumPorts,
                                           MbsmSlotMapTable.u4StartIfIndex);

            /*physical component - Card */
            if (u4Temp == 1)
            {
                *pi1PhyClass = ENT_PHY_CLASS_MODULE;
                *pi4ParentRelPos = 1;
                /* u2Count specifies the slots and one is incremented
                 * considering the chassis */
                *pi4ContainedIn = *pi4TraversedPI + u2Count;
                if (ENT_MAX_CC_SLOTS)
                {
                    *pu4LocalIndex = (UINT4) (u2Count + ENT_MAX_CC_SLOTS - 1);
                }
                u4Temp = 0;
                *pi4PhyIndex = 0;
                return OSIX_SUCCESS;
            }
            /*physical component - interfaces */
            else if ((u4ActivePortCount > 0) &&
                     (u4Temp <= (1 + u4ActivePortCount)))
            {
                /*Decremented 1 for the card */
                u4Temp = u4Temp - 1;

                EntUtilGetIfIndexForPortNum (MbsmSlotMapTable.u4NumPorts,
                                             MbsmSlotMapTable.u4StartIfIndex,
                                             u4Temp, &u4LocalIfIndex);
                *pi1PhyClass = ENT_PHY_CLASS_PORT;
                *pi4ParentRelPos = u4Temp;
                *pi4ContainedIn = i4IfContainedIn;
                *pu4LocalIndex = u4LocalIfIndex;
                u4Temp = 0;
                *pi4PhyIndex = 0;
                return OSIX_SUCCESS;
            }
            else
            {
                u4Temp = u4Temp - u4ActivePortCount - 1;
                u2Count++;
                i4IfContainedIn = i4IfContainedIn + u4ActivePortCount;
            }
        }
        *pi4PhyIndex = u4Temp;
    }
    /*end of else part */

#elif ((defined (STACKING_WANTED)) && \
      (defined (MBSM_WANTED)))

    /*This case is of pizza box stacking */

    i4IfContainedIn = *pi4TraversedPI;

    /*physical component - Slots */
    MEMSET (&MbsmSlotMapTable, 0x00, sizeof (tMbsmSlotInfo));

    if ((EntMbsmGetSlotInfo (u4ChassisNum - 1, &MbsmSlotMapTable) !=
         OSIX_SUCCESS))
    {
        *pi4PhyIndex = i4PhyIndex;
        return OSIX_SUCCESS;
    }
    if (MbsmSlotMapTable.i1Status != MBSM_STATUS_ACTIVE)
    {
        *pi4PhyIndex = i4PhyIndex;
        return OSIX_SUCCESS;
    }
    i4IfContainedIn = i4IfContainedIn + 1;
    i4TotalTraversedPI = i4TotalTraversedPI + 1;

    u4ActivePortCount =
        EntUtilGetActivePortCount (MbsmSlotMapTable.u4NumPorts,
                                   MbsmSlotMapTable.u4StartIfIndex);

    /*physical component - Card */
    if (i4PhyIndex == 1)
    {
        *pi1PhyClass = ENT_PHY_CLASS_MODULE;
        *pi4ParentRelPos = 1;
        *pi4ContainedIn = i4ChassisPI;
        *pu4LocalIndex = (UINT4) (u4ChassisNum - 1);
        *pi4PhyIndex = 0;
        return OSIX_SUCCESS;
    }
    /*physical component - interfaces */
    else if ((u4ActivePortCount > 0) &&
             (i4PhyIndex <= (INT4) (1 + u4ActivePortCount)))
    {
        /*Decremented 1 for the card */
        i4PhyIndex = i4PhyIndex - 1;

        EntUtilGetIfIndexForPortNum (MbsmSlotMapTable.u4NumPorts,
                                     MbsmSlotMapTable.u4StartIfIndex,
                                     i4PhyIndex, &u4LocalIfIndex);

        *pi1PhyClass = ENT_PHY_CLASS_PORT;
        *pi4ParentRelPos = i4PhyIndex;
        *pi4ContainedIn = i4IfContainedIn;
        *pu4LocalIndex = u4LocalIfIndex;
        *pi4PhyIndex = 0;
        return OSIX_SUCCESS;
    }
    else
    {
        i4PhyIndex = i4PhyIndex - u4ActivePortCount - 1;
        i4IfContainedIn = i4IfContainedIn + u4ActivePortCount;
        i4TotalTraversedPI = i4TotalTraversedPI + u4ActivePortCount;
    }
    *pi4PhyIndex = i4PhyIndex;
    *pi4TraversedPI = *pi4TraversedPI + i4TotalTraversedPI;

#elif ((!defined (STACKING_WANTED)) && \
        (! defined (MBSM_WANTED)))
    /*This case is only of pizza box without stacking */
    if (i4PhyIndex == 1)
    {
        *pi1PhyClass = ENT_PHY_CLASS_MODULE;
        *pi4ParentRelPos = u2LocalRelPos + 1;
        *pi4ContainedIn = 1;
        *pu4LocalIndex = 1;
    }
    else
    {
        u4Temp = i4PhyIndex - 1;
        /*Get uTemp(th) number IfIndex */
        EntUtilGetIfIndex (u4ChassisNum, i4SlotId = 1, u4Temp, &u4IfIndex);
        *pi1PhyClass = ENT_PHY_CLASS_PORT;
        /*Decremented 1 for the slot */
        *pi4ParentRelPos = i4PhyIndex - 1;
        *pi4ContainedIn = *pi4TraversedPI + 1;
        *pu4LocalIndex = u4IfIndex;
    }
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntUtilSetPhyInfo                                    *
 *                                                                           *
 * Description        : This function is the utility function to set the     *
 *                      physical component information from the CFA/MBSM/ISS *
 *                      modules                                              *
 *                                                                           *
 * Input(s)           : i4PhyIndex - Identifying the physical component      *
 *                      pPhyString - String value to be set
 *                      u1Object - Object value for which string to be set
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
 ****************************************************************************/

PUBLIC INT1
EntUtilSetPhyInfo (INT4 i4PhyIndex, UINT1 *pPhyString, UINT1 u1Object)
{

    UINT4               u4ChassisNum = 0;
    UINT4               u4LocalIndex = 0;
    INT4                i4ContainedIn = 0;
    INT4                i4ParentRelPos = 0;
    INT1                i1PhyClass = 0;
    INT1                i1RetStatus = OSIX_SUCCESS;

    EntUtilIdentifyPhyComponent (i4PhyIndex, &i1PhyClass, &i4ContainedIn,
                                 &i4ParentRelPos, &u4ChassisNum, &u4LocalIndex);

    /* Global Stack Physical Index
     * Physical Info for Stack is set here*/

    if (i1PhyClass == ENT_PHY_CLASS_STACK)
    {
        switch (u1Object)
        {
            case ENT_PHY_SERIAL_NUM:
                EntIssStackSetSerialNum (pPhyString);
                break;
            case ENT_PHY_ALIAS_NAME:
                EntIssStackSetAlias (pPhyString);
                break;
            case ENT_PHY_ASSET_ID:
                EntIssStackSetAssetId (pPhyString);
                break;
            case ENT_PHY_URIS:
                EntIssStackSetUris (pPhyString);
                break;
            default:
                return OSIX_FAILURE;
        }
        return OSIX_SUCCESS;
    }
    else if (i1PhyClass == ENT_PHY_CLASS_POWER)
    {
        if (u4ChassisNum == 0)
        {
            /* Power Supply present 
             * for stack and setting its physical info*/
            switch (u1Object)
            {
                case ENT_PHY_SERIAL_NUM:
                    EntIssStackPSSetSerialNum (u4LocalIndex, pPhyString);
                    break;
                case ENT_PHY_ALIAS_NAME:
                    EntIssStackPSSetAlias (u4LocalIndex, pPhyString);
                    break;
                case ENT_PHY_ASSET_ID:
                    EntIssStackPSSetAssetId (u4LocalIndex, pPhyString);
                    break;
                case ENT_PHY_URIS:
                    EntIssStackPSSetUris (u4LocalIndex, pPhyString);
                    break;
                default:
                    return OSIX_FAILURE;
            }
            return OSIX_SUCCESS;
        }
        else
        {
            switch (u1Object)
            {
                case ENT_PHY_SERIAL_NUM:
                    EntIssChassisPSSetSerialNum (u4ChassisNum,
                                                 u4LocalIndex, pPhyString);
                    break;
                case ENT_PHY_ALIAS_NAME:
                    EntIssChassisPSSetAlias (u4ChassisNum,
                                             u4LocalIndex, pPhyString);
                    break;
                case ENT_PHY_ASSET_ID:
                    EntIssChassisPSSetAssetId (u4ChassisNum,
                                               u4LocalIndex, pPhyString);
                    break;
                case ENT_PHY_URIS:
                    EntIssChassisPSSetUris (u4ChassisNum,
                                            u4LocalIndex, pPhyString);
                    break;
                default:
                    return OSIX_FAILURE;
            }
        }
    }
    else if (i1PhyClass == ENT_PHY_CLASS_CPU)
    {
        if (u4ChassisNum == 0)
        {
            switch (u1Object)
            {
                case ENT_PHY_SERIAL_NUM:
                    EntIssStackCpuSetSerialNum (u4LocalIndex, pPhyString);
                    break;
                case ENT_PHY_ALIAS_NAME:
                    EntIssStackCpuSetAlias (u4LocalIndex, pPhyString);
                    break;
                case ENT_PHY_ASSET_ID:
                    EntIssStackCpuSetAssetId (u4LocalIndex, pPhyString);
                    break;
                case ENT_PHY_URIS:
                    EntIssStackCpuSetUris (u4LocalIndex, pPhyString);
                    break;
                default:
                    return OSIX_FAILURE;
            }
            return OSIX_SUCCESS;
        }
        else
        {
            switch (u1Object)
            {
                case ENT_PHY_SERIAL_NUM:
                    EntIssChassisCpuSetSerialNum (u4ChassisNum, u4LocalIndex,
                                                  pPhyString);
                    break;
                case ENT_PHY_ALIAS_NAME:
                    EntIssChassisCpuSetAlias (u4ChassisNum, u4LocalIndex,
                                              pPhyString);
                    break;
                case ENT_PHY_ASSET_ID:
                    EntIssChassisCpuSetAssetId (u4ChassisNum, u4LocalIndex,
                                                pPhyString);
                    break;
                case ENT_PHY_URIS:
                    EntIssChassisCpuSetUris (u4ChassisNum, u4LocalIndex,
                                             pPhyString);
                    break;
                default:
                    return OSIX_FAILURE;
            }
        }
    }
    else if (i1PhyClass == ENT_PHY_CLASS_FAN)
    {
        if (u4ChassisNum == 0)
        {
            switch (u1Object)
            {
                case ENT_PHY_SERIAL_NUM:
                    EntIssStackFanSetSerialNum (u4LocalIndex, pPhyString);
                    break;
                case ENT_PHY_ALIAS_NAME:
                    EntIssStackFanSetAlias (u4LocalIndex, pPhyString);
                    break;
                case ENT_PHY_ASSET_ID:
                    EntIssStackFanSetAssetId (u4LocalIndex, pPhyString);
                    break;
                case ENT_PHY_URIS:
                    EntIssStackFanSetUris (u4LocalIndex, pPhyString);
                    break;
                default:
                    return OSIX_FAILURE;
            }
            return OSIX_SUCCESS;
        }
        else
        {
            switch (u1Object)
            {
                case ENT_PHY_SERIAL_NUM:
                    EntIssChassisFanSetSerialNum (u4ChassisNum, u4LocalIndex,
                                                  pPhyString);
                    break;
                case ENT_PHY_ALIAS_NAME:
                    EntIssChassisFanSetAlias (u4ChassisNum, u4LocalIndex,
                                              pPhyString);
                    break;
                case ENT_PHY_ASSET_ID:
                    EntIssChassisFanSetAssetId (u4ChassisNum, u4LocalIndex,
                                                pPhyString);
                    break;
                case ENT_PHY_URIS:
                    EntIssChassisFanSetUris (u4ChassisNum, u4LocalIndex,
                                             pPhyString);
                    break;
                default:
                    return OSIX_FAILURE;
            }
        }
    }
    else if (i1PhyClass == ENT_PHY_CLASS_CHASSIS)
    {
        switch (u1Object)
        {
            case ENT_PHY_SERIAL_NUM:
                i1RetStatus = EntIssChassisSetSerialNum (u4ChassisNum,
                                                         pPhyString);
                break;
            case ENT_PHY_ALIAS_NAME:
                i1RetStatus = EntIssChassisSetAlias (u4ChassisNum, pPhyString);
                break;
            case ENT_PHY_ASSET_ID:
                i1RetStatus = EntIssChassisSetAssetId (u4ChassisNum,
                                                       pPhyString);
                break;
            case ENT_PHY_URIS:
                i1RetStatus = EntIssChassisSetUris (u4ChassisNum, pPhyString);
                break;
            default:
                return OSIX_FAILURE;
        }
        return i1RetStatus;
    }
    else if (i1PhyClass == ENT_PHY_CLASS_CONTAINER)
    {

        switch (u1Object)
        {
            case ENT_PHY_SERIAL_NUM:
                i1RetStatus =
                    EntMbsmSetSlotSerialNum (u4ChassisNum, u4LocalIndex,
                                             pPhyString);
                break;
            case ENT_PHY_ALIAS_NAME:
                i1RetStatus =
                    EntMbsmSetSlotAlias (u4ChassisNum, u4LocalIndex,
                                         pPhyString);
                break;
            case ENT_PHY_ASSET_ID:
                i1RetStatus =
                    EntMbsmSetSlotAssetId (u4ChassisNum, u4LocalIndex,
                                           pPhyString);
                break;
            case ENT_PHY_URIS:
                i1RetStatus =
                    EntMbsmSetSlotUris (u4ChassisNum, u4LocalIndex, pPhyString);
                break;
            default:
                return OSIX_FAILURE;
        }
        return i1RetStatus;
    }
    else if (i1PhyClass == ENT_PHY_CLASS_MODULE)
    {
        switch (u1Object)
        {
            case ENT_PHY_SERIAL_NUM:
                i1RetStatus =
                    EntMbsmSetSlotSerialNum (u4ChassisNum, u4LocalIndex,
                                             pPhyString);
                break;
            case ENT_PHY_ALIAS_NAME:
                i1RetStatus =
                    EntMbsmSetSlotAlias (u4ChassisNum, u4LocalIndex,
                                         pPhyString);
                break;
            case ENT_PHY_ASSET_ID:
                i1RetStatus =
                    EntMbsmSetSlotAssetId (u4ChassisNum, u4LocalIndex,
                                           pPhyString);
                break;
            case ENT_PHY_URIS:
                i1RetStatus =
                    EntMbsmSetSlotUris (u4ChassisNum, u4LocalIndex, pPhyString);
                break;
            default:
                return OSIX_FAILURE;
        }
        return i1RetStatus;
    }
    else if (i1PhyClass == ENT_PHY_CLASS_PORT)
    {
        switch (u1Object)
        {
            case ENT_PHY_URIS:
                i1RetStatus =
                    EntIssSetIfUris (u4ChassisNum, u4LocalIndex, pPhyString);
                break;
            default:
                return OSIX_FAILURE;
        }
        return i1RetStatus;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntUtilGetCntxtIdForLI                               *
 *                                                                           *
 * Description        : This function is utility routine to the contextId    *
 *                      value corresponding to the Logical Index             *
 *                                                                           *
 * Input(s)           : u4LogIndex - Identifying the logical component       *
 *                                                                           *
 * Output(s)          : pu4ContextId - Vlaue of context Id found             *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 ****************************************************************************/
PUBLIC VOID
EntUtilGetCntxtIdForLI (UINT4 u4LogIndex, UINT4 *pu4ContextId)
{
    UINT4               u4InstMibCount = 0;
    UINT4               u4NonInstMibCount = 0;
    UINT4               u4TotalMibCount = 0;
    UINT4               u4LocalIndex = 0;

    EntSnmpGetMIBCountWithContextIdAndLock (&u4InstMibCount,
                                            &u4NonInstMibCount);

    u4TotalMibCount = u4InstMibCount + u4NonInstMibCount;

    /*If the Logical Index is greater than Total Mib count of intantiable and
     * non - instantiable mibs than only Logical Index refers to a particular
     * context which can be found with the steps below */

    if (u4LogIndex <= u4TotalMibCount)
    {
        *pu4ContextId = 0;
    }
    else
    {
        u4LocalIndex = u4LogIndex - u4TotalMibCount;
        if ((u4LocalIndex % u4InstMibCount) == 0)
        {
            *pu4ContextId = u4LocalIndex / u4InstMibCount;
        }
        else
        {
            *pu4ContextId = (u4LocalIndex / u4InstMibCount) + 1;
        }
    }
    return;
}

/*****************************************************************************
 * Function Name      : EntUtilFirstIfInfoForCntxt                           *
 *                                                                           *
 * Description        : This function is utility routine to get the IfName   *
 *                      IfIndex of the first port mapped to the provided     * 
 *                      context                                              *
 *                                                                           *
 * Input(s)           : u4ContextId - Context Id                             *
 *                                                                           *
 * Output(s)          : pu1IfName - Name of the first interface mapped to 
 *                      this context                                         *
 *                      pu4IfIndex - Index of first interface mapped to
 *                      this context
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
 ****************************************************************************/
PUBLIC INT1
EntUtilFirstIfInfoForCntxt (UINT4 u4ContextId, UINT1 *pu1IfName,
                            UINT4 *pu4IfIndex)
{

    tPortList          *pIfPortList = NULL;
    tCfaIfInfo          IfInfo;
    UINT4               u4Port = 0;
    UINT2               u2ByteIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;
    pIfPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pIfPortList == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (*pIfPortList, 0, sizeof (tPortList));

    MEMSET (&(IfInfo), 0, sizeof (tCfaIfInfo));

    if (EntVcmGetContextPortList (u4ContextId, *pIfPortList) == OSIX_SUCCESS)
    {
        for (u2ByteIndex = 0; (u2ByteIndex < ENT_IFPORT_LIST_SIZE);
             u2ByteIndex++)
        {
            u1PortFlag = (*pIfPortList)[u2ByteIndex];
            for (u2BitIndex = 0; ((u2BitIndex < ENT_PORTS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
            {
                if ((u1PortFlag & ENT_BIT8) != 0)
                {
                    u4Port = (UINT4) ((u2ByteIndex * ENT_PORTS_PER_BYTE) +
                                      u2BitIndex + 1);
                    if (EntCfaCliGetIfName (u4Port, (INT1 *) pu1IfName) ==
                        CFA_FAILURE)
                    {
                        FsUtilReleaseBitList ((UINT1 *) pIfPortList);
                        return OSIX_FAILURE;
                    }
                    FsUtilReleaseBitList ((UINT1 *) pIfPortList);
                    *pu4IfIndex = u4Port;
                    return OSIX_SUCCESS;
                }
                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }                    /*end of for2 */
        }                        /*end of for1 */
    }
    FsUtilReleaseBitList ((UINT1 *) pIfPortList);

    return OSIX_FAILURE;
}

/*****************************************************************************
 * Function Name      : EntUtilGetNextIfIndex                                *
 *                                                                           *
 * Description        : This function is utility routine to get the next
 *                      interface mapped to the given context                *
 *                                                                           *
 * Input(s)           : u4IfIndex - Interface Index
 *                      u4ContextId - Context Id                             *
 *                                                                           *
 * Output(s)          : pu4NextIfIndex - Next IfIndex mapped to the given 
 *                      context                                              *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
 ****************************************************************************/
PUBLIC INT1
EntUtilGetNextIfIndex (UINT4 u4IfIndex, UINT4 u4ContextId,
                       UINT4 *pu4NextIfIndex)
{

    tPortList          *pIfPortList = NULL;
    tCfaIfInfo          IfInfo;
    UINT4               u4Port = 0;
    UINT2               u2ByteIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;
    UINT1               u1NextIndexFlag = 0;

    MEMSET (&(IfInfo), 0, sizeof (tCfaIfInfo));
    pIfPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pIfPortList == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (*pIfPortList, 0, sizeof (tPortList));

    if (EntVcmGetContextPortList (u4ContextId, *pIfPortList) != OSIX_SUCCESS)
    {
        FsUtilReleaseBitList ((UINT1 *) pIfPortList);
        return OSIX_FAILURE;
    }

    for (u2ByteIndex = 0; (u2ByteIndex < ENT_IFPORT_LIST_SIZE); u2ByteIndex++)
    {
        u1PortFlag = (*pIfPortList)[u2ByteIndex];
        for (u2BitIndex = 0; ((u2BitIndex < ENT_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & ENT_BIT8) != 0)
            {
                u4Port = (UINT4) ((u2ByteIndex * ENT_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);

                if (u1NextIndexFlag == 1)
                {
                    *pu4NextIfIndex = u4Port;
                    FsUtilReleaseBitList ((UINT1 *) pIfPortList);
                    return OSIX_SUCCESS;
                }

                if (u4Port == u4IfIndex)
                {
                    u1NextIndexFlag = 1;
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }                        /*end of for2 */
    }                            /*end of for1 */
    FsUtilReleaseBitList ((UINT1 *) pIfPortList);
    return OSIX_FAILURE;
}

/*****************************************************************************
 * Function Name      : EntUtilGetPhyIndexForIfName                          *
 *                                                                           *
 * Description        : This function is utility routine to get the 
 *                      Phy Index for input Interface                        *
 *                                                                           *
 * Input(s)           : pu1IfName - Interface Name                           *
 *                                                                           *
 * Output(s)          : pi4PhyIndex - Phy Index for input Interfrace         *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
 ****************************************************************************/
PUBLIC INT1
EntUtilGetPhyIndexForIfName (UINT1 *pu1IfName, INT4 *pi4PhyIndex)
{
    tIssEntPhyInfo     *pPhyInfo = NULL;
    INT4                i4LocalPhyIndex = 1;

    if (ISS_ENTPHY_ALLOC_MEM_BLOCK (pPhyInfo) == NULL)
    {
        return OSIX_FAILURE;
    }
    while (i4LocalPhyIndex <= (INT4) ENT_MAX_PHY_ENTITY)
    {
        MEMSET (pPhyInfo, 0, sizeof (tIssEntPhyInfo));
        if (EntUtilGetPhysicalInfo (i4LocalPhyIndex, pPhyInfo) != OSIX_SUCCESS)
        {
            ISS_ENTPHY_FREE_MEM_BLOCK (pPhyInfo);
            return OSIX_FAILURE;
        }

        if ((pPhyInfo->i1PhyClass == ENT_PHY_CLASS_PORT) &&
            (STRCMP (pPhyInfo->au1Name, pu1IfName) == 0))
        {
            *pi4PhyIndex = i4LocalPhyIndex;
            ISS_ENTPHY_FREE_MEM_BLOCK (pPhyInfo);
            return OSIX_SUCCESS;
        }
        i4LocalPhyIndex = i4LocalPhyIndex + 1;
    }
    ISS_ENTPHY_FREE_MEM_BLOCK (pPhyInfo);
    return OSIX_FAILURE;
}

/*****************************************************************************
 * Function Name      : EntUtilMaxPhyIndex                                   *
 *                                                                           *
 * Description        : This function is utility routine to get the maximum
 *                      physical components present                          *
 *                                                                           *
 * Input(s)           : None                                                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : Maximum Physical Count                               *
 ****************************************************************************/
PUBLIC INT4
EntUtilMaxPhyIndex ()
{

    UINT4               u4PhysicalCount = 0;
    UINT4               u4ChassisNum = 0;

#ifdef STACKING_WANTED

    /*This case is of pizza box stacking */
    UINT2               u2ChassisCount = 0;
    UINT2               u2PSCount = 0;
    UINT2               u2CpuCount = 0;
    UINT2               u2FanCount = 0;

    u4PhysicalCount = 1;

    EntIssGetPSCountInStack (&u2PSCount);
    u4PhysicalCount = u4PhysicalCount + u2PSCount;

    EntIssGetCpuCountInStack (&u2CpuCount);
    u4PhysicalCount = u4PhysicalCount + u2CpuCount;

    EntIssGetFanCountInStack (&u2FanCount);
    u4PhysicalCount = u4PhysicalCount + u2FanCount;

    EntIssGetChassisCount (&u2ChassisCount);
    u4PhysicalCount = u4PhysicalCount + u2ChassisCount;

    /* To traverse the Fan, CPU, Power Supply, Slot, Card, 
     * Interface Physical Components*/

    for (u4ChassisNum = 1; u4ChassisNum <= u2ChassisCount; u4ChassisNum++)
    {
        EntUtilCalcPhyCount (&u4PhysicalCount, u4ChassisNum);
    }

#else
    /*This case is common for pizza box and chassis */

    u4PhysicalCount = 1;

    u4ChassisNum = 1;
    EntUtilCalcPhyCount (&u4PhysicalCount, u4ChassisNum);
#endif

    return u4PhysicalCount;
}

 /****************************************************************************
 * Function Name      : EntUtilCalcPhyCount                                  *
 *                                                                           *
 * Description        : This function is the utility function to get         *
 *                      calculate the physical index in system               *            
 *                                                                           *
 * Input(s)           : u4ChassisNum - Chassis Number                        *
 *                                                                           *
 * Output(s)          : pu4PhysicalCount - Physical component count          *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 ****************************************************************************/

PRIVATE VOID
EntUtilCalcPhyCount (UINT4 *pu4PhysicalCount, UINT4 u4ChassisNum)
{
#if ((!defined (STACKING_WANTED)) && (!defined (MBSM_WANTED)))
    UINT4               u4IfIndex = 0;
    UINT4               u4LocalPort = 0;
#endif
    UINT2               u2PSCount = 0;
    UINT2               u2CpuCount = 0;
    UINT2               u2FanCount = 0;
#ifdef MBSM_WANTED
    tMbsmSlotInfo       MbsmSlotMapTable;
    UINT4               u4ActivePortCount = 0;
#endif

    EntIssGetCpuCountInChassis (u4ChassisNum, &u2CpuCount);
    EntIssGetPSCountInChassis (u4ChassisNum, &u2PSCount);
    EntIssGetFanCountInChassis (u4ChassisNum, &u2FanCount);

    *pu4PhysicalCount = *pu4PhysicalCount + u2CpuCount + u2PSCount + u2FanCount;

#if ((defined (STACKING_WANTED)) && \
      (defined (MBSM_WANTED)))

    /*This case is of pizza box stacking */

    /*physical components - Card or Interface */
    MEMSET (&MbsmSlotMapTable, 0x00, sizeof (tMbsmSlotInfo));

    if ((EntMbsmGetSlotInfo (u4ChassisNum - 1, &MbsmSlotMapTable) !=
         OSIX_SUCCESS))
    {
        return;
    }
    if (MbsmSlotMapTable.i1Status != MBSM_STATUS_ACTIVE)
    {
        return;
    }

    *pu4PhysicalCount = *pu4PhysicalCount + 1;

    u4ActivePortCount =
        EntUtilGetActivePortCount (MbsmSlotMapTable.u4NumPorts,
                                   MbsmSlotMapTable.u4StartIfIndex);
    *pu4PhysicalCount = *pu4PhysicalCount + u4ActivePortCount;

#endif

#if ((!defined (STACKING_WANTED)) && \
    (defined (MBSM_WANTED)))

    /*This case is of chassis and pizza box stacking */
    UINT2               u2MaxSlot = 0;
    UINT2               u2Count = 0;
    u2MaxSlot = ENT_MAX_LC_SLOTS + MBSM_SLOT_INDEX_START;

    *pu4PhysicalCount = *pu4PhysicalCount + ENT_MAX_LC_SLOTS;
    /*physical components - Card or Interface */
    u2Count = MBSM_SLOT_INDEX_START;
    while ((u2Count <= u2MaxSlot))
    {
        MEMSET (&MbsmSlotMapTable, 0x00, sizeof (tMbsmSlotInfo));

        if ((EntMbsmGetSlotInfo
             ((u2Count + ENT_MAX_CC_SLOTS), &MbsmSlotMapTable) != OSIX_SUCCESS))
        {
            u2Count++;
            continue;
        }
        if (MbsmSlotMapTable.i1Status != MBSM_STATUS_ACTIVE)
        {
            u2Count++;
            continue;
        }

        *pu4PhysicalCount = *pu4PhysicalCount + 1;

        u4ActivePortCount =
            EntUtilGetActivePortCount (MbsmSlotMapTable.u4NumPorts,
                                       MbsmSlotMapTable.u4StartIfIndex);
        *pu4PhysicalCount = *pu4PhysicalCount + u4ActivePortCount;

        u2Count++;
    }
#endif

#if ((!defined (STACKING_WANTED)) && \
        (! defined (MBSM_WANTED)))

    /*This case is only of pizza box without stacking */

    /*Card */
    *pu4PhysicalCount = *pu4PhysicalCount + 1;

    EntUtilGetIfIndex (u4ChassisNum, 1, 1, &u4LocalPort);

    if (u4LocalPort != 0)
    {
        *pu4PhysicalCount = *pu4PhysicalCount + 1;
        while (EntCfaGetNextActivePort (u4LocalPort, &u4IfIndex) ==
               OSIX_SUCCESS)
        {
            *pu4PhysicalCount = *pu4PhysicalCount + 1;
            u4LocalPort = u4IfIndex;
        }
    }
#endif

    return;

}

/*****************************************************************************
 * Function Name      : EntUtilMaxLogIndex                                   *
 *                                                                           *
 * Description        : This function is utility routine to get the          *
 *                      maximum logical indexes present                      *
 *                                                                           *
 * Input(s)           : None                                                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : Max Count                                            *
 ****************************************************************************/
PUBLIC INT4
EntUtilMaxLogIndex ()
{
    UINT4               u4ContextCount = 0;
    UINT4               u4LogicalCount = 0;
    UINT4               u4InstMibCount = 0;
    UINT4               u4NonInstMibCount = 0;

    EntSnmpGetMIBCountWithContextIdAndLock (&u4InstMibCount,
                                            &u4NonInstMibCount);

    EntUtilGetActiveContextCount (&u4ContextCount);

    if (u4ContextCount == 1)
    {
        u4LogicalCount = u4InstMibCount + u4NonInstMibCount;
    }
    else
    {
        u4LogicalCount = u4InstMibCount * u4ContextCount;
        u4LogicalCount = u4LogicalCount + u4NonInstMibCount;
    }

    return u4LogicalCount;
}

/*****************************************************************************
 * Function Name      : EntUtilGetIfIndex                                    *
 *                                                                           *
 * Description        : This function is utility routine to the IfIndex      *
 *                      for the PortNumber                                   *
 *                                                                           *
 * Input(s)           : u4ChassisNum - Chassis Number                        *
 *                      i4SlotId - Slot Id 
 *                      u4PortNum - Port Number for which index to be found
 *                                                                           *
 * Output(s)          : pu4IfIndex - Inetrface Index Value                   *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 ****************************************************************************/
PUBLIC VOID
EntUtilGetIfIndex (UINT4 u4ChassisNum, INT4 i4SlotId,
                   UINT4 u4PortNum, UINT4 *pu4IfIndex)
{
    UINT4               u4Count = 0;
    UINT4               u4LocalPort = 0;

    UNUSED_PARAM (u4ChassisNum);
    UNUSED_PARAM (i4SlotId);

    while (u4Count < u4PortNum)
    {
        if (EntCfaGetNextActivePort (u4LocalPort, pu4IfIndex) == OSIX_SUCCESS)
        {
            u4Count = u4Count + 1;
            u4LocalPort = *pu4IfIndex;
        }
        else
        {
            return;
        }
    }
    return;
}

/*****************************************************************************
 * Function Name      : EntUtilGetNextLogIndex                               *
 *                                                                           *
 * Description        : This function is utility routine to the get next     *
 *                      valid logical index                                  *
 *                                                                           *
 * Input(s)           : i4LogIndex - Logical Index                           *
 *                                                                           *
 * Output(s)          : pi4NextLogIndex - Next Valid Logical Index           *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
 ****************************************************************************/
PUBLIC INT1
EntUtilGetNextLogIndex (INT4 i4LogIndex, INT4 *pi4NextLogIndex)
{
    if (i4LogIndex < ENT_MAX_LOG_ENTITY)
    {
        *pi4NextLogIndex = i4LogIndex + 1;
        return OSIX_SUCCESS;
    }
    else
    {
        return OSIX_FAILURE;
    }
}

/*****************************************************************************
 * Function Name      : EntUtilGetNextPhyIndex                               *
 *                                                                           *
 * Description        : This function is utility routine to the get next     *
 *                      valid physical index                                 *
 *                                                                           *
 * Input(s)           : i4PhyIndex - Physical Index                          *
 *                                                                           *
 * Output(s)          : pi4NextPhyIndex - Next Valid Physical Index          *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
 ****************************************************************************/
PUBLIC INT1
EntUtilGetNextPhyIndex (INT4 i4PhyIndex, INT4 *pi4NextPhyIndex)
{
    if (i4PhyIndex < ENT_MAX_PHY_ENTITY)
    {
        *pi4NextPhyIndex = i4PhyIndex + 1;
        return OSIX_SUCCESS;
    }
    else
    {
        return OSIX_FAILURE;
    }
}

/*****************************************************************************
 * Function Name      : EntUtilGetActiveContextCount                         *
 *                                                                           *
 * Description        : This function is utility routine to the get the      *
 *                      count of Active context present in teh system        *
 *                                                                           *
 * Input(s)           : None                                                 *
 *                                                                           *
 * Output(s)          : pu4ContextCount - Number of Active contexts          *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 ****************************************************************************/
PUBLIC VOID
EntUtilGetActiveContextCount (UINT4 *pu4ContextCount)
{

    UINT4               u4ContextId = 0;
    UINT4               u4NextContextId = 0;

    if (EntVcmGetFirstActiveContext (&u4ContextId) == VCM_SUCCESS)
    {
        *pu4ContextCount = 1;

        while (EntVcmGetNextActiveContext (u4ContextId, &u4NextContextId) ==
               VCM_SUCCESS)
        {
            *pu4ContextCount = *pu4ContextCount + 1;
            u4ContextId = u4NextContextId;
        }
    }
    return;
}

/*****************************************************************************
 * Function Name      : EntUtilGetIfIndexForPhyIndex                         *
 *                                                                           *
 * Description        : This function is utility routine to the get the      *
 *                      IfIndex for teh Physical Index                       *
 *                                                                           *
 * Input(s)           : i4PhyIndex - Physical Index                          *
 *                                                                           *
 * Output(s)          : pu4IfIndex - IfIndex corresponding to PhyIndex       *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
 ****************************************************************************/
PUBLIC INT4
EntUtilGetIfIndexForPhyIndex (UINT4 i4PhyIndex, UINT4 *pu4IfIndex)
{
    tIssEntPhyInfo     *pPhyInfo = NULL;
    UINT4               u4LocalPort = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    if (ISS_ENTPHY_ALLOC_MEM_BLOCK (pPhyInfo) == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pPhyInfo, 0, sizeof (tIssEntPhyInfo));
    if (EntUtilGetPhysicalInfo (i4PhyIndex, pPhyInfo) != OSIX_SUCCESS)
    {
        ISS_ENTPHY_FREE_MEM_BLOCK (pPhyInfo);
        return OSIX_FAILURE;
    }
    do
    {
        if (EntCfaGetNextActivePort (u4LocalPort, pu4IfIndex) == OSIX_FAILURE)
        {
            ISS_ENTPHY_FREE_MEM_BLOCK (pPhyInfo);
            return OSIX_FAILURE;
        }

        if (EntCfaCliGetIfName (*pu4IfIndex, (INT1 *) au1IfName) ==
            OSIX_FAILURE)
        {
            ISS_ENTPHY_FREE_MEM_BLOCK (pPhyInfo);
            return OSIX_FAILURE;
        }
        u4LocalPort = *pu4IfIndex;
    }
    while (STRNCMP (pPhyInfo->au1Name, au1IfName,
                    STRLEN (pPhyInfo->au1Name)) != 0);

    ISS_ENTPHY_FREE_MEM_BLOCK (pPhyInfo);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntUtilGetActivePortCount                            *
 *                                                                           *
 * Description        : This function is utility routine to the get the      *
 *                      number of Ports Active form teh list of ports        *
 *                                                                           *
 * Input(s)           : u4PhyIndex - Starting Physical Index for teh card    *
 *                      inserted.                                            *
 *                      u4MaxPorts - Maximum number of ports that can be     *
 *                      present in the inserted card.                        *
 *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : Count of Active Ports                                *
 ****************************************************************************/
PUBLIC INT4
EntUtilGetActivePortCount (UINT4 u4MaxPorts, UINT4 u4IfIndex)
{

    UINT4               u4LocalCount = 0;
    UINT4               u4ActivePortCount = 0;

    while (u4LocalCount < u4MaxPorts)
    {
        if (EntCfaValidateIfIndex (u4IfIndex) == OSIX_SUCCESS)
        {
            u4ActivePortCount = u4ActivePortCount + 1;
        }
        u4LocalCount = u4LocalCount + 1;
        u4IfIndex = u4IfIndex + 1;
    }
    return u4ActivePortCount;
}

/*****************************************************************************
 * Function Name      : EntUtilGetIfIndexForPortNum                          *
 *                                                                           *
 * Description        : This function is utility routine to the get the      *
 *                      IfIndex of teh Active Port correspoding to teh       *
 *                      specified portnumber                                 *
 * Input(s)           : u4PhyIndex - Starting Physical Index for teh card    *
 *                      inserted.                                            *
 *                      u4MaxPorts - Maximum number of ports that can be     *
 *                      present in the inserted card.                        *
 *                      u4LocalPortNum - Port Number for which index to be   *
 *                      identified.                                          *
 *                                                                           *
 * Output(s)          : pu4IfIndex - IfIndex for teh portnumber              *
 *                                                                           *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            *
 ****************************************************************************/
PUBLIC INT4
EntUtilGetIfIndexForPortNum (UINT4 u4MaxPorts, UINT4 u4StartIfIndex,
                             UINT4 u4LocalPortNum, UINT4 *pu4IfIndex)
{

    UINT4               u4LocalCount = 1;
    UINT4               u4ActivePortCount = 0;

    *pu4IfIndex = u4StartIfIndex;
    while (u4LocalCount <= u4MaxPorts)
    {
        if (EntCfaValidateIfIndex (*pu4IfIndex) == OSIX_SUCCESS)
        {
            u4ActivePortCount = u4ActivePortCount + 1;
        }

        if (u4ActivePortCount == u4LocalPortNum)
        {
            return OSIX_SUCCESS;
        }
        u4LocalCount = u4LocalCount + 1;
        *pu4IfIndex = *pu4IfIndex + 1;
    }
    *pu4IfIndex = 0;
    return OSIX_FAILURE;
}

/****************************************************************************/
