/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 *$Id: entport.c,v 1.13 2014/03/09 13:35:06 siva Exp $
 *
 * Description: This file contains the wrapper function for all the
 *              external APIs called by Entity MIB.
 ********************************************************************/
#include "entinc.h"

UINT4               gau4EntSysLogId;
/*****************************************************************************
 * Function Name      : EntMbsmSetSerialNum                                  *
 *                                                                           *
 * Description        : This function is called to set the serial number     *
 *                      for container and module                             *
 *                                                                           *
 * Input(s)           : u4ChassisNum - Specifies the chassis number          *
 *                      i4SlotId - Slot Identifier to identify the slot      *
 *                                                                           *
 * Output(s)          : pSerialNum - Serial Number of Physical component     *
 *                                                                           *
 * Return Value(s)    : SUCCESS/FAILURE                                      *
 ****************************************************************************/
PUBLIC INT1
EntMbsmSetSlotSerialNum (UINT4 u4ChassisNum, INT4 i4SlotId, UINT1 *pSerialNum)
{
#ifdef MBSM_WANTED
    tMbsmSlotInfo       SlotInfo;
    UNUSED_PARAM (u4ChassisNum);
    MEMSET (&SlotInfo, 0xFF, sizeof (tMbsmSlotInfo));
    MEMSET (&SlotInfo.au1SerialNum, 0x00, MBSM_ENT_PHY_SER_NUM_LEN);
    MEMCPY (SlotInfo.au1SerialNum, pSerialNum, STRLEN (pSerialNum));
    /* Update the Serial Number in MBSM module corresponding to the SlotId */
    if (MbsmSetSlotInfo (i4SlotId, &SlotInfo) != MBSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (u4ChassisNum);
    UNUSED_PARAM (i4SlotId);
    UNUSED_PARAM (pSerialNum);
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntMbsmSetSlotAlias                                  *
 *                                                                           *
 * Description        : This function is called to set the Alias Name        *
 *                      for container and module                             *
 *
 * Input(s)           : u4ChassisNum - Specifies the chassis number          *
 *                      i4SlotId - Slot Identifier to identify the slot      *
 *                                                                           *
 * Output(s)          : pAlias - Alias of Physical component                 *
 *                                                                           *
 * Return Value(s)    : SUCCESS/FAILURE                                      *
 ****************************************************************************/
PUBLIC INT1
EntMbsmSetSlotAlias (UINT4 u4ChassisNum, INT4 i4SlotId, UINT1 *pAlias)
{
#ifdef MBSM_WANTED
    tMbsmSlotInfo       SlotInfo;
    UNUSED_PARAM (u4ChassisNum);
    MEMSET (&SlotInfo, 0xFF, sizeof (tMbsmSlotInfo));
    MEMSET (&SlotInfo.au1Alias, 0x00, MBSM_ENT_PHY_ALIAS_LEN);
    MEMCPY (SlotInfo.au1Alias, pAlias, STRLEN (pAlias));
    /* Update the Serial Number in MBSM module corresponding to the SlotId */
    if (MbsmSetSlotInfo (i4SlotId, &SlotInfo) != MBSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (u4ChassisNum);
    UNUSED_PARAM (i4SlotId);
    UNUSED_PARAM (pAlias);
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntMbsmSetSlotAssetId                                *
 *                                                                           *
 * Description        : This function is called to set the AssetId           *
 *                      for container and module                             *
 *
 * Input(s)           : u4ChassisNum - Specifies the chassis number          *
 *                      i4SlotId - Slot Identifier to identify the slot      *
 *                                                                           *
 * Output(s)          : pAssetId - AssetId of Physical component             *
 *                                                                           *
 * Return Value(s)    : SUCCESS/FAILURE                                      *
 ****************************************************************************/
PUBLIC INT1
EntMbsmSetSlotAssetId (UINT4 u4ChassisNum, INT4 i4SlotId, UINT1 *pAssetId)
{
#ifdef MBSM_WANTED
    tMbsmSlotInfo       SlotInfo;
    UNUSED_PARAM (u4ChassisNum);
    MEMSET (&SlotInfo, 0xFF, sizeof (tMbsmSlotInfo));
    MEMSET (&SlotInfo.au1AssetId, 0x00, MBSM_ENT_PHY_ASSET_ID_LEN);
    MEMCPY (SlotInfo.au1AssetId, pAssetId, STRLEN (pAssetId));
    /* Update the Serial Number in MBSM module corresponding to the SlotId */
    if (MbsmSetSlotInfo (i4SlotId, &SlotInfo) != MBSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (u4ChassisNum);
    UNUSED_PARAM (i4SlotId);
    UNUSED_PARAM (pAssetId);
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntMbsmSetSlotUris                                   *
 *                                                                           *
 * Description        : This function is called to set the Uris Identifier   *
 *                      for container and module                             *
 *
 * Input(s)           : u4ChassisNum - Specifies the chassis number 
 *                      i4SlotId - Slot Identifier to identify the slot      *
 *                                                                           *
 * Output(s)          : pUris - Uris of Physical component                   *
 *                                                                           *
 * Return Value(s)    : SUCCESS/FAILURE                                      *
 ****************************************************************************/
PUBLIC INT1
EntMbsmSetSlotUris (UINT4 u4ChassisNum, INT4 i4SlotId, UINT1 *pUris)
{
#ifdef MBSM_WANTED
    tMbsmSlotInfo       SlotInfo;
    UNUSED_PARAM (u4ChassisNum);
    MEMSET (&SlotInfo, 0xFF, sizeof (tMbsmSlotInfo));
    MEMSET (&SlotInfo.au1Uris, 0x00, MBSM_ENT_PHY_URIS_LEN);
    MEMCPY (SlotInfo.au1Uris, pUris, STRLEN (pUris));
    /* Update the Serial Number in MBSM module corresponding to the SlotId */
    if (MbsmSetSlotInfo (i4SlotId, &SlotInfo) != MBSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (u4ChassisNum);
    UNUSED_PARAM (i4SlotId);
    UNUSED_PARAM (pUris);
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntMbsmGetSlotInfo                                   *
 *                                                                           *
 * Description        : This function is called to get the information       *
 *                      related to physical componets                        *
 *                                                                           *
 * Input(s)           : u2SlotId -  Slot Identifier to identify the slot     *
 *                                                                           *
 * Output(s)          : pSlotInfo - Slot related Information                 *
 *                                                                           *
 * Return Value(s)    : SUCCESS/FAILURE                                      *
 ****************************************************************************/
PUBLIC INT1
EntMbsmGetSlotInfo (UINT2 u2SlotId, tMbsmSlotInfo * pSlotInfo)
{
#ifdef MBSM_WANTED
    /* Get the Slot Information related to SlotId from MBSM module */
    if (MbsmGetSlotInfo ((INT4) u2SlotId, pSlotInfo) != MBSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }
#else
    UNUSED_PARAM (u2SlotId);
    UNUSED_PARAM (pSlotInfo);
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntMbsmGetSlotPhyInfo                                *
 *                                                                           *
 * Description        : This function is called to get the information       *
 *                      related to physical componets                        *
 *                                                                           *
 * Input(s)           : u4ChassisNum - Specifies the chassis number 
 *                      i4SlotId - Slot Identifier to identify the slot      *
 *
 * Output(s)          : pPhyInfo - Physical Information                      *
 *                                                                           *
 * Return Value(s)    : SUCCESS/FAILURE                                      *
 ****************************************************************************/
PUBLIC INT1
EntMbsmGetSlotPhyInfo (UINT4 u4ChassisNum, INT4 i4SlotId,
                       tIssEntPhyInfo * pPhyInfo)
{
#ifdef MBSM_WANTED
    tMbsmSlotInfo       SlotInfo;
    UNUSED_PARAM (u4ChassisNum);
    /* Get the Physical Information related to SlotId from MBSM module */
    if (MbsmGetSlotInfo (i4SlotId, &SlotInfo) != MBSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    MEMCPY (pPhyInfo->au1SerialNum, SlotInfo.au1SerialNum,
            MBSM_ENT_PHY_SER_NUM_LEN);
    MEMCPY (pPhyInfo->au1Alias, SlotInfo.au1Alias, MBSM_ENT_PHY_ALIAS_LEN);
    MEMCPY (pPhyInfo->au1AssetId, SlotInfo.au1AssetId,
            MBSM_ENT_PHY_ASSET_ID_LEN);
    MEMCPY (pPhyInfo->au1Uris, SlotInfo.au1Uris, MBSM_ENT_PHY_URIS_LEN);
    MEMCPY (pPhyInfo->au1Descr, SlotInfo.au1SlotCardName,
            (MBSM_MAX_CARD_NAME_LEN + 1));
#else
    UNUSED_PARAM (u4ChassisNum);
    UNUSED_PARAM (i4SlotId);
    UNUSED_PARAM (pPhyInfo);
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntMbsmGetCardPhyInfo                                *
 *                                                                           *
 * Description        : This function is called to get the information       *
 *                      related to physical componets                        *
 *                                                                           *
 * Input(s)           : u4ChassisNum - Specifies the chassis number 
 *                      i4SlotId - Slot Identifier to identify the slot      *
 *
 * Output(s)          : pPhyInfo - Physical Information                      *
 *                                                                           *
 * Return Value(s)    : SUCCESS/FAILURE                                      *
 ****************************************************************************/
PUBLIC INT1
EntMbsmGetCardPhyInfo (UINT4 u4ChassisNum, INT4 i4SlotId,
                       tIssEntPhyInfo * pPhyInfo)
{
#ifdef MBSM_WANTED

    tMbsmSlotInfo       SlotInfo;
    UNUSED_PARAM (u4ChassisNum);
    /* Get the Physical Information related to SlotId from MBSM module */
    if (MbsmGetSlotInfo (i4SlotId, &SlotInfo) != MBSM_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    MEMCPY (pPhyInfo->au1SerialNum, SlotInfo.au1SerialNum,
            MBSM_ENT_PHY_SER_NUM_LEN);
    MEMCPY (pPhyInfo->au1Alias, SlotInfo.au1Alias, MBSM_ENT_PHY_ALIAS_LEN);
    MEMCPY (pPhyInfo->au1AssetId, SlotInfo.au1AssetId,
            MBSM_ENT_PHY_ASSET_ID_LEN);
    MEMCPY (pPhyInfo->au1Uris, SlotInfo.au1Uris, MBSM_ENT_PHY_URIS_LEN);
    MEMCPY (pPhyInfo->au1Descr, SlotInfo.au1SlotCardName,
            (MBSM_MAX_CARD_NAME_LEN + 1));
#else
    UNUSED_PARAM (u4ChassisNum);
    UNUSED_PARAM (i4SlotId);
    UNUSED_PARAM (pPhyInfo);
#endif
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntIssGetStackInfo                                   *
 *                                                                           *
 * Description        : This function is called to get the stack information *
 *                      form the ISS                           
 *                                                                           *
 * Input(s)           : None                                                 *
 *                                                                           *
 * Output(s)          : Physical Information                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 ****************************************************************************/
PUBLIC VOID
EntIssGetStackInfo (tIssEntPhyInfo * pPhyInfo)
{
    IssGetStackPhyInfo (pPhyInfo);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssGetPSCountInStack                          */
/*                                                                          */
/*    Description        : This function is used to get the Power Supply    */
/*                         count of the system                              */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : pu2PSCount - Power Supply Count in Stack         */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssGetPSCountInStack (UINT2 *pu2PSCount)
{
    IssGetPsCountInStack (pu2PSCount);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssGetCpuCountInStack                         */
/*                                                                          */
/*    Description        : This function is used to get the Cpu
 *                         count of the system                              */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : pu2CpuCount - Cpu Count                          */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssGetCpuCountInStack (UINT2 *pu2CpuCount)
{
    IssGetCpuCountInStack (pu2CpuCount);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssGetFanCountInStack                         */
/*                                                                          */
/*    Description        : This function is used to get the Fan
 *                         count of the system                              */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : pu2FanCount - Fan Count                          */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssGetFanCountInStack (UINT2 *pu2FanCount)
{
    IssGetFanCountInStack (pu2FanCount);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssGetChassisCount                            */
/*                                                                          */
/*    Description        : This function is used to get the Chassis
 *                         count of the system                              */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : Chassis Count                                    */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssGetChassisCount (UINT2 *pu2ChassisCount)
{
    IssGetChassisCount (pu2ChassisCount);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssGetPSInfoInStack                           */
/*                                                                          */
/*    Description        : This function is used to get the Power Supply 
 *                         Info of the system                               */
/*                                                                          */
/*    Input(s)          : u4PSNum - Power Supply Count for which info to be 
 *                        fetched                                           */
/*                                                                          */
/*    Output(s)          : pPhyInfo - Physical component information        */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssGetPSInfoInStack (UINT4 u4PSNum, tIssEntPhyInfo * pPhyInfo)
{

    IssGetPsPhyInfoInStack (u4PSNum, pPhyInfo);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssGetCpuInfoInStack                          */
/*                                                                          */
/*    Description        : This function is used to get the CPU Info        */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : u4CpuNum -  Count for which info to be           */
/*                         fetched                                          */
/*    Output(s)          : pPhyInfo - Physical component information        */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssGetCpuInfoInStack (UINT4 u4CpuNum, tIssEntPhyInfo * pPhyInfo)
{

    IssGetCpuPhyInfoInStack (u4CpuNum, pPhyInfo);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssGetFanInfoInStack                          */
/*                                                                          */
/*    Description        : This function is used to get the Fan Info        */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : u4FanNum - Fan Count                             */
/*                                                                          */
/*    Output(s)          : pPhyInfo - Physical component information        */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssGetFanInfoInStack (UINT4 u4FanNum, tIssEntPhyInfo * pPhyInfo)
{
    IssGetFanPhyInfoInStack (u4FanNum, pPhyInfo);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssGetChassisInfo                             */
/*                                                                          */
/*    Description        : This function is used to get the Chassis Info    */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : u4ChassisNum - Specifies the chassis number      */
/*                                                                          */
/*    Output(s)          : pPhyInfo - Physical component information        */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssGetChassisInfo (UINT4 u4ChassisNum, tIssEntPhyInfo * pPhyInfo)
{
    IssGetChassisPhyInfo (u4ChassisNum, pPhyInfo);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssGetCpuCountInChassis                       */
/*                                                                          */
/*    Description        : This function is used to get the Cpu count       */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : u4ChassisNum - Specifies the chassis number      */
/*                                                                          */
/*    Output(s)          : pu2CpuCount - Number of CPU present in chassis   */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssGetCpuCountInChassis (UINT4 u4ChassisNum, UINT2 *pu2CpuCount)
{
    IssGetCpuCountInChassis (u4ChassisNum, pu2CpuCount);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssGetPSCountInChassis                        */
/*                                                                          */
/*    Description        : This function is used to get the Power supply 
 *                         count of the system                              */
/*                                                                          */
/*    Input(s)           : u4ChassisNum - Specifies the chassis number      */
/*                                                                          */
/*    Output(s)          : pu2PSCount - Number of Power Supplies present in */
/*                         chassis                                          */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssGetPSCountInChassis (UINT4 u4ChassisNum, UINT2 *pu2PSCount)
{
    IssGetPsCountInChassis (u4ChassisNum, pu2PSCount);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssGetFanCountInChassis                       */
/*                                                                          */
/*    Description        : This function is used to get the fan count       */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : u4ChassisNum - Specifies the chassis number      */
/*                                                                          */
/*    Output(s)          : pu2FanCount - Number of Fans present in chassis  */

/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssGetFanCountInChassis (UINT4 u4ChassisNum, UINT2 *pu2FanCount)
{

    IssGetFanCountInChassis (u4ChassisNum, pu2FanCount);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssChassisGetCpuInfoInChassis                 */
/*                                                                          */
/*    Description        : This function is used to get the CPU Info        */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : u4ChassisNum - Specifies the chassis number      */
/*                                                                          */
/*    Output(s)          : pPhyInfo - Physical component information        */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssGetCpuInfoInChassis (UINT4 u4ChassisNum, UINT2 u2CpuNum,
                           tIssEntPhyInfo * pPhyInfo)
{
    IssGetCpuPhyInfoInChassis (u4ChassisNum, u2CpuNum, pPhyInfo);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssGetPSInfoInChassis                         */
/*                                                                          */
/*    Description        : This function is used to get the Power Supply    */
/*                         Info of the system                               */
/*                                                                          */
/*    Input(s)           : u4ChassisNum - Specifies the chassis number      */
/*                                                                          */
/*    Output(s)          : pPhyInfo - Physical component information        */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssGetPSInfoInChassis (UINT4 u4ChassisNum, UINT2 u2PSNum,
                          tIssEntPhyInfo * pPhyInfo)
{
    IssGetPsPhyInfoInChassis (u4ChassisNum, u2PSNum, pPhyInfo);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssGetFanInfoInChassis                        */
/*                                                                          */
/*    Description        : This function is used to get the Fan             */
/*                         Info of the system                               */
/*                                                                          */
/*    Input(s)           : u4ChassisNum - Specifies the chassis number      */
/*                                                                          */
/*    Output(s)          : pPhyInfo - Physical component information        */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssGetFanInfoInChassis (UINT4 u4ChassisNum, UINT2 u2FanNum,
                           tIssEntPhyInfo * pPhyInfo)
{
    IssGetFanPhyInfoInChassis (u4ChassisNum, u2FanNum, pPhyInfo);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssStackSetSerialNum                          */
/*                                                                          */
/*    Description        : This function is used to set the serial number   */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : pSerialNum - Serial Number of Physical component */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssStackSetSerialNum (UINT1 *pu1SerialNum)
{
    IssSetStackPhyInfo (ISS_ENT_PHY_SERIAL_NUM, pu1SerialNum);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssStackSetAlias                              */
/*                                                                          */
/*    Description        : This function is used to set the Alias number    */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : pu1Alias- Alias of Physical component            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssStackSetAlias (UINT1 *pu1Alias)
{
    IssSetStackPhyInfo (ISS_ENT_PHY_ALIAS_NAME, pu1Alias);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssStackSetAssetId                            */
/*                                                                          */
/*    Description        : This function is used to set the Asset Id        */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : pu1AssetId - AssetId of Physical component       */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssStackSetAssetId (UINT1 *pu1AssetId)
{
    IssSetStackPhyInfo (ISS_ENT_PHY_ASSET_ID, pu1AssetId);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssStackSetUris                               */
/*                                                                          */
/*    Description        : This function is used to set the Uris            */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : pu1Uris - Uris Info of Physical component        */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssStackSetUris (UINT1 *pu1Uris)
{
    IssSetStackPhyInfo (ISS_ENT_PHY_URIS, pu1Uris);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssStackPSSetSerialNum                        */
/*                                                                          */
/*    Description        : This function is used to set the serial number   */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           :u4PSNum - Power Supply Count  
 *                        pSerialNum - Serial Number of Physical component  */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssStackPSSetSerialNum (UINT4 u4PSNum, UINT1 *pu1SerialNum)
{
    IssSetPsPhyInfoInStack (u4PSNum, ISS_ENT_PHY_SERIAL_NUM, pu1SerialNum);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssStackPSSetAlias                            */
/*                                                                          */
/*    Description        : This function is used to set the Alias           */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           :u4PSNum - Power Supply Count                      */
/*                        pu1Alias- Alias of Physical component             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssStackPSSetAlias (UINT4 u4PSNum, UINT1 *pu1Alias)
{
    IssSetPsPhyInfoInStack (u4PSNum, ISS_ENT_PHY_ALIAS_NAME, pu1Alias);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssStackPSSetAssetId                          */
/*                                                                          */
/*    Description        : This function is used to set the Asset Id        */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           :u4PSNum - Power Supply Count                      */
/*                       : pu1AssetId - AssetId of Physical component       */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssStackPSSetAssetId (UINT4 u4PSNum, UINT1 *pu1AssetId)
{
    IssSetPsPhyInfoInStack (u4PSNum, ISS_ENT_PHY_ASSET_ID, pu1AssetId);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssStackPSSetUris                             */
/*                                                                          */
/*    Description        : This function is used to set the Uris            */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           :u4PSNum - Power Supply Count                      */
/*                       : pu1Uris - Uris Info of Physical component        */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssStackPSSetUris (UINT4 u4PSNum, UINT1 *pu1Uris)
{

    IssSetPsPhyInfoInStack (u4PSNum, ISS_ENT_PHY_URIS, pu1Uris);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssStackCpuSetSerialNum                       */
/*                                                                          */
/*    Description        : This function is used to set the serial number   */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : u4CpuNum - Cpu Count
 *                       : pSerialNum - Serial Number of Physical component */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssStackCpuSetSerialNum (UINT4 u4CpuNum, UINT1 *pu1SerialNum)
{
    IssSetCpuPhyInfoInStack (u4CpuNum, ISS_ENT_PHY_SERIAL_NUM, pu1SerialNum);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssStackCpuSetAlias                           */
/*                                                                          */
/*    Description        : This function is used to set the Alias           */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : pu1Alias- Alias of Physical component            */
/*                         u4CpuNum - Cpu Count                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssStackCpuSetAlias (UINT4 u4CpuNum, UINT1 *pu1Alias)
{
    IssSetCpuPhyInfoInStack (u4CpuNum, ISS_ENT_PHY_ALIAS_NAME, pu1Alias);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssStackCpuSetAssetId                         */
/*                                                                          */
/*    Description        : This function is used to set the Asset Id        */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : pu1AssetId - AssetId of Physical component       */
/*                         u4CpuNum - Cpu Count                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssStackCpuSetAssetId (UINT4 u4CpuNum, UINT1 *pu1AssetId)
{
    IssSetCpuPhyInfoInStack (u4CpuNum, ISS_ENT_PHY_ASSET_ID, pu1AssetId);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssStackCpuSetUris                            */
/*                                                                          */
/*    Description        : This function is used to set the Uris            */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : pu1Uris - Uris Info of Physical component        */
/*                         u4CpuNum - Cpu Count                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/

VOID
EntIssStackCpuSetUris (UINT4 u4CpuNum, UINT1 *pu1Uris)
{
    IssSetCpuPhyInfoInStack (u4CpuNum, ISS_ENT_PHY_URIS, pu1Uris);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssStackFanSetSerialNum                       */
/*                                                                          */
/*    Description        : This function is used to set the serial number   */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : pSerialNum - Serial Number of Physical component */
/*                         u4FanNum - Fan Count                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssStackFanSetSerialNum (UINT4 u4FanNum, UINT1 *pu1SerialNum)
{
    IssSetFanPhyInfoInStack (u4FanNum, ISS_ENT_PHY_SERIAL_NUM, pu1SerialNum);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssStackFanSetAlias                           */
/*                                                                          */
/*    Description        : This function is used to set the Alias           */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : pu1Alias- Alias of Physical component            */
/*                         u4FanNum - Fan Count                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssStackFanSetAlias (UINT4 u4FanNum, UINT1 *pu1Alias)
{
    IssSetFanPhyInfoInStack (u4FanNum, ISS_ENT_PHY_ALIAS_NAME, pu1Alias);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssStackFanSetAssetId                         */
/*                                                                          */
/*    Description        : This function is used to set the Asset Id        */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : pu1AssetId - AssetId of Physical component       */
/*                         u4FanNum - Fan Count                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssStackFanSetAssetId (UINT4 u4FanNum, UINT1 *pu1AssetId)
{
    IssSetFanPhyInfoInStack (u4FanNum, ISS_ENT_PHY_ASSET_ID, pu1AssetId);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssStackFanSetUris                            */
/*                                                                          */
/*    Description        : This function is used to set the Uris            */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : pu1Uris - Uris Info of Physical component        */
/*                         u4FanNum - Fan Count                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssStackFanSetUris (UINT4 u4FanNum, UINT1 *pu1Uris)
{
    IssSetFanPhyInfoInStack (u4FanNum, ISS_ENT_PHY_URIS, pu1Uris);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssChassisCpuSetSerialNum                     */
/*                                                                          */
/*    Description        : This function is used to set the serial number   */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : pSerialNum - Serial Number of Physical component */
/*                         u4CpuNum - Cpu Count                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssChassisCpuSetSerialNum (UINT4 u4ChassisNum, UINT4 u4CpuNum,
                              UINT1 *pu1SerialNum)
{
    IssSetCpuPhyInfoInChassis (u4ChassisNum, u4CpuNum, ISS_ENT_PHY_SERIAL_NUM,
                               pu1SerialNum);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssChassisCpuSetAlias                         */
/*                                                                          */
/*    Description        : This function is used to set the Alias           */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : pu1Alias- Alias of Physical component            */
/*                         u4CpuNum - Cpu Count                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssChassisCpuSetAlias (UINT4 u4ChassisNum, UINT4 u4CpuNum, UINT1 *pu1Alias)
{
    IssSetCpuPhyInfoInChassis (u4ChassisNum, u4CpuNum,
                               ISS_ENT_PHY_ALIAS_NAME, pu1Alias);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssChassisCpuSetAssetId                       */
/*                                                                          */
/*    Description        : This function is used to set the Asset Id        */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : u4ChassisNum - Specifies the chassis number      */
/*                         u4CpuNum - Cpu Count                             */
/*                         pu1AssetId - AssetId of Physical component       */

/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssChassisCpuSetAssetId (UINT4 u4ChassisNum, UINT4 u4CpuNum,
                            UINT1 *pu1AssetId)
{
    IssSetCpuPhyInfoInChassis (u4ChassisNum, u4CpuNum,
                               ISS_ENT_PHY_ASSET_ID, pu1AssetId);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssChassisCpuSetUris                          */
/*                                                                          */
/*    Description        : This function is used to set the Uris            */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : pu1Uris - Uris Info of Physical component        */
/*                         u4CpuNum - Cpu Count                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssChassisCpuSetUris (UINT4 u4ChassisNum, UINT4 u4CpuNum, UINT1 *pu1Uris)
{
    IssSetCpuPhyInfoInChassis (u4ChassisNum, u4CpuNum,
                               ISS_ENT_PHY_URIS, pu1Uris);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssChassisPSSetSerialNum                      */
/*                                                                          */
/*    Description        : This function is used to set the serial number   */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : u4ChassisNum - Specifies the chassis number      */
/*                       : u4PSNum - Power Supply Count                     */
/*                       : pSerialNum - Serial Number of Physical component */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssChassisPSSetSerialNum (UINT4 u4ChassisNum, UINT4 u4PSNum,
                             UINT1 *pu1SerialNum)
{
    IssSetPsPhyInfoInChassis (u4ChassisNum, u4PSNum, ISS_ENT_PHY_SERIAL_NUM,
                              pu1SerialNum);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssChassisPSSetAlias                          */
/*                                                                          */
/*    Description        : This function is used to set the Alias           */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : u4ChassisNum - Specifies the chassis number      */
/*                       : u4PSNum - Power Supply Count                     */
/*                       : pu1Alias- Alias of Physical component            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssChassisPSSetAlias (UINT4 u4ChassisNum, UINT4 u4PSNum, UINT1 *pu1Alias)
{
    IssSetPsPhyInfoInChassis (u4ChassisNum, u4PSNum,
                              ISS_ENT_PHY_ALIAS_NAME, pu1Alias);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssChassisPSSetAssetId                        */
/*                                                                          */
/*    Description        : This function is used to set the Asset Id        */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : u4ChassisNum - Specifies the chassis number      */
/*                       : u4PSNum - Power Supply Count                     */
/*                       : pu1AssetId - AssetId of Physical component       */

/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssChassisPSSetAssetId (UINT4 u4ChassisNum, UINT4 u4PSNum, UINT1 *pu1AssetId)
{
    IssSetPsPhyInfoInChassis (u4ChassisNum, u4PSNum,
                              ISS_ENT_PHY_ASSET_ID, pu1AssetId);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssChassisPSSetUris                           */
/*                                                                          */
/*    Description        : This function is used to set the Uris            */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : u4ChassisNum - Specifies the chassis number      */
/*                       : u4PSNum - Power Supply Count                     */
/*                       : pu1Uris - Uris Info of Physical component        */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssChassisPSSetUris (UINT4 u4ChassisNum, UINT4 u4PSNum, UINT1 *pu1Uris)
{
    IssSetPsPhyInfoInChassis (u4ChassisNum, u4PSNum, ISS_ENT_PHY_URIS, pu1Uris);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssChassisFanSetSerialNum                     */
/*                                                                          */
/*    Description        : This function is used to set the serial number   */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : u4ChassisNum - Specifies the chassis number      */
/*                         pSerialNum - Serial Number of Physical component */
/*                         u4FanNum - Fan Count                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssChassisFanSetSerialNum (UINT4 u4ChassisNum, UINT4 u4FanNum,
                              UINT1 *pu1SerialNum)
{

    IssSetFanPhyInfoInChassis (u4ChassisNum, u4FanNum,
                               ISS_ENT_PHY_SERIAL_NUM, pu1SerialNum);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssChassisFanSetAlias                         */
/*                                                                          */
/*    Description        : This function is used to set the Alias           */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : u4ChassisNum - Specifies the chassis number      */
/*                       : pu1Alias- Alias of Physical component            */
/*                         u4FanNum - Fan Count                             */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssChassisFanSetAlias (UINT4 u4ChassisNum, UINT4 u4FanNum, UINT1 *pu1Alias)
{
    IssSetFanPhyInfoInChassis (u4ChassisNum, u4FanNum,
                               ISS_ENT_PHY_ALIAS_NAME, pu1Alias);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssChassisFanSetAssetId                       */
/*                                                                          */
/*    Description        : This function is used to set the Asset Id        */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : u4ChassisNum - Specifies the chassis number      */
/*                         u4FanNum - Fan Count                             */
/*                         pu1AssetId - AssetId of Physical component       */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssChassisFanSetAssetId (UINT4 u4ChassisNum, UINT4 u4FanNum,
                            UINT1 *pu1AssetId)
{
    IssSetFanPhyInfoInChassis (u4ChassisNum, u4FanNum,
                               ISS_ENT_PHY_ASSET_ID, pu1AssetId);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : EntIssChassisFanSetUris                          */
/*                                                                          */
/*    Description        : This function is used to set the Uris            */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : u4ChassisNum - Specifies the chassis number      */
/*                         u4FanNum - Fan Count                             */
/*                       : pu1Uris - Uris Info of Physical component        */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
EntIssChassisFanSetUris (UINT4 u4ChassisNum, UINT4 u4FanNum, UINT1 *pu1Uris)
{
    IssSetFanPhyInfoInChassis (u4ChassisNum, u4FanNum,
                               ISS_ENT_PHY_URIS, pu1Uris);
    return;
}

/*****************************************************************************
 * Function Name      : EntIssChassisSetSerialNum                            *
 *                                                                           *
 * Description        : This function is called to set the serial number     *
 *                      for global physical entity                           *
 *                                                                           *
 * Input(s)           : u4ChassisNum - Specifies the chassis number          *
 *                      pSerialNum - Serial Number of Physical component     *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_FAILURE/ OSIX_SUCCESS                           *
 ****************************************************************************/
PUBLIC INT1
EntIssChassisSetSerialNum (UINT4 u4ChassisNum, UINT1 *pSerialNum)
{
    /* Update the Serial Number for Chassis */

    if (IssSetChassisPhyInfo (u4ChassisNum, ISS_ENT_PHY_SERIAL_NUM,
                              pSerialNum) != ISS_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntIssChassisSetAlias                                *
 *                                                                           *
 * Description        : This function is called to set the Alias             *
 *                      for global physical entity                           *
 *                                                                           *
 * Input(s)           : u4ChassisNum - Specifies the chassis number          *
 *                    : pAlias     -  Alias NAme of Physical component       *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : SUCCESS/FAILURE                                      *
 ****************************************************************************/
PUBLIC INT1
EntIssChassisSetAlias (UINT4 u4ChassisNum, UINT1 *pAlias)
{
    /* Update the Alias Number for Chassis */
    if (IssSetChassisPhyInfo (u4ChassisNum, ISS_ENT_PHY_ALIAS_NAME,
                              pAlias) != ISS_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntIssChassisSetAssetId                              *
 *                                                                           *
 * Description        : This function is called to set the AssetId           *
 *                      for global physical entity                           *
 *                                                                           *
 * Input(s)           : u4ChassisNum - Specifies the chassis number          *
 *                    : pAssetId  -    Asset Id of Physical component        *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_FAILURE/ OSIX_SUCCESS                           *
 ****************************************************************************/
PUBLIC INT1
EntIssChassisSetAssetId (UINT4 u4ChassisNum, UINT1 *pAssetId)
{
    /* Update the Asset ID for Chassis */
    if (IssSetChassisPhyInfo (u4ChassisNum, ISS_ENT_PHY_ASSET_ID,
                              pAssetId) != ISS_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntIssChassisSetUris                                 *
 *                                                                           *
 * Description        : This function is called to set the Uris              *
 *                      for global physical entity                           *
 *                                                                           *
 * Input(s)           : u4ChassisNum - Specifies the chassis number          *
 * Input(s)           : pUris -        Uris Id of Physical component         *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_FAILURE/ OSIX_SUCCESS                           *
 ****************************************************************************/
PUBLIC INT1
EntIssChassisSetUris (UINT4 u4ChassisNum, UINT1 *pUris)
{
    /* Update the Uris Number for Chassis */
    if (IssSetChassisPhyInfo (u4ChassisNum, ISS_ENT_PHY_URIS,
                              pUris) != ISS_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntIssIfGetPhyInfo                                   *
 *                                                                           *
 * Description        : This function is called to get the information       *
 *                      related to physical components (interface).          *
 *                                                                           *
 * Input(s)           : u4ChassisNum - Specifies the chassis number          *
 *                      u4ifIndex  - Interface Index                         *
 * Output(s)          : pPhyInfo - Physical component information            *
 *                                                                           *
 * Return Value(s)    : OSIX_FAILURE/ OSIX_SUCCESS                           *
 ****************************************************************************/
PUBLIC INT1
EntIssIfGetPhyInfo (UINT4 u4ChassisNum, UINT4 u4ifIndex,
                    tIssEntPhyInfo * pPhyInfo)
{

    if (IssGetIfPhyInfo (u4ChassisNum, u4ifIndex, pPhyInfo) != ISS_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntIssSetIfUris                                      *
 *                                                                           *
 * Description        : This function is called to set the Uris              *
 *                      for the interface.                                   *
 *                                                                           *
 * Input(s)           : u4ChassisNum - Specifies the chassis number          *
 *                      u4ifIndex  - Interface Index                         *
 *                      Uris - Interface Uris Identifier                     *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : OSIX_FAILURE/ OSIX_SUCCESS                           *
 ****************************************************************************/
PUBLIC INT1
EntIssSetIfUris (UINT4 u4ChassisNum, UINT4 u4IfIndex, UINT1 *pIfUris)
{
    /* Set the Physical Information */
    if (IssSetIfPhyInfo (u4ChassisNum, u4IfIndex, ISS_ENT_PHY_URIS,
                         pIfUris) != ISS_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntVcmGetFirstActiveContext                          *
 *                                                                           *
 * Description        : This function is used to get the first active        *
 *                      context present in the system.                       *
 *                                                                           *
 * Input(s)           : None                                                 *
 *                                                                           *
 * Output(s)          : pu4ContextCount -  Active Context count.             *
 *                                                                           *
 * Return Value(s)    : SUCCESS/FAILURE                                      *
 * Return Value(s)    : None                                                 *
 ****************************************************************************/
INT4
EntVcmGetFirstActiveContext (UINT4 *pu4ContextId)
{
    if (VcmGetFirstActiveContext (pu4ContextId) != VCM_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntVcmGetNextActiveContext                           *
 *                                                                           *
 * Description        : This function is used to get the next  active        *
 *                      context present in the system.                       *
 *                                                                           *
 * Input(s)           : u4ContextId  - Virtual Context Id                    *
 *                                                                           *
 * Output(s)          : pu4NextContextId -  Next Active Context Id           *
 *                                                                           *
 * Return Value(s)    : SUCCESS/FAILURE                                      *
 ****************************************************************************/
INT4
EntVcmGetNextActiveContext (UINT4 u4ContextId, UINT4 *pu4NextContextId)
{
    if (VcmGetNextActiveContext (u4ContextId, pu4NextContextId) != VCM_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EntVcmGetContextPortList                             */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      interfaces mapped to this context                    */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : Interface List                                       */
/*                                                                           */
/* Return Value(s)    : OSIX_FAILURE/ OSIX_SUCCESS                           */
/*                                                                           */
/*****************************************************************************/
INT1
EntVcmGetContextPortList (UINT4 u4ContextId, tPortList PortList)
{
    if (VcmGetContextPortList (u4ContextId, PortList) == VCM_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EntVcmGetAliasName                                   */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      the alias name of the given context                  */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : pu1Alias       - Switch Alias Name.                  */
/*                                                                           */
/* Return Value(s)    : OSIX_FAILURE/ OSIX_SUCCESS                           */
/*                                                                           */
/*****************************************************************************/
INT1
EntVcmGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    if (VcmGetAliasName (u4ContextId, pu1Alias) == VCM_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EntCfaGetNextActivePort                              */
/*                                                                           */
/* Description        : This function gets the next active port              */
/*                                                                           */
/* Input(s)           : u2Port   :Port Number                                */
/*                                                                           */
/* Output(s)          : pu4NextPort:Pointer to the next active port          */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/

INT1
EntCfaGetNextActivePort (UINT4 u4Port, UINT4 *pu4NextPort)
{
    if (CfaGetNextActivePort (u4Port, pu4NextPort) == CFA_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (CfaIsPhysicalInterface (*pu4NextPort) == CFA_FALSE)
    {
        *pu4NextPort = 0;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EntCfaIpIfGetIfInfo                                  */
/*                                                                           */
/* Description        : Providesthe IP related parameters for a interface    */
/*                                                                           */
/* Input(s)           : u2Port   :Port Number                                */
/*                                                                           */
/* Output(s)          : pIpInfo - Interface information filled               */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/

INT1
EntCfaIpIfGetIfInfo (UINT4 u4Port, tIpConfigInfo * pIpInfo)
{
    if (CfaIpIfGetIfInfo (u4Port, pIpInfo) == CFA_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : EntCfaGetFirstIpActivePort                           */
/*                                                                           */
/* Description        : This function gets the next active port              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu4Port  :Pointer to the First Ipactive port         */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/

INT1
EntCfaGetFirstIpActivePort (UINT4 *pu4Port)
{
    UINT4               u4Port = 0;
    UINT4               u4NextPort = 0;
    tCfaIfInfo          IfInfo;

    while (CfaGetNextActivePort (u4Port, &u4NextPort) == CFA_SUCCESS)
    {
        MEMSET (&IfInfo, 0x00, sizeof (tCfaIfInfo));
        if ((CfaGetIfInfo (u4NextPort, &IfInfo)) != CFA_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        if (IfInfo.u1IfType == CFA_L3IPVLAN)
        {
            *pu4Port = u4NextPort;
            return OSIX_SUCCESS;
        }
        u4Port = u4NextPort;
    }
    return OSIX_FAILURE;
}

/*****************************************************************************
 *                                                                           *
 *    Function Name     : EntCfaCliGetIfName                                 *
 *                                                                           *
 *    Description       : This function returns the interface related params *
 *                        assigned to this interface to the external modules.*
 *                                                                           *
 *    Input(s)          : u4IfIndex - Interface Index                        *
 *                                                                           *
 *    Output(s)         : pIfName - Name of teh interface                    *
 *                                                                           *
 *    Returns           : OSIX_SUCCESS/OSIX_FAILURE                          *
 *****************************************************************************/
INT1
EntCfaCliGetIfName (UINT4 u4IfIndex, INT1 *pIfName)
{
    if (CfaCliGetIfName (u4IfIndex, pIfName) == CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 *    Function Name        : EntCfaValidateIfIndex                           *
 *                                                                           *
 *    Description          : This function validates the IfIndex             *
 *                                                                           *
 *    Input(s)             : u4IfIndex - Interface Index                     *
 *                                                                           *
 *    Output(s)            : None                                            *
 *                                                                           *
 *    Returns              : OSIX_SUCCESS/OSIX_FAILURE                       *
 *****************************************************************************/
INT1
EntCfaValidateIfIndex (UINT4 u4IfIndex)
{
    if (CfaValidateIfIndex (u4IfIndex) == CFA_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntSnmpGetSysName                                    *
 *                                                                           *
 * Description        : This function is called to get the System Name from
 *                      the SNMP Agent  information                          *
 *                                                                           *
 * Input(s)           : None                                                 *
 *                                                                           *
 * Output(s)          : pSysName - get the System Name                       *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 ****************************************************************************/
PUBLIC VOID
EntSnmpGetSysName (UINT1 *pSysName)
{
#ifdef SNMP_3_WANTED
    SnmpGetSysName (pSysName);
#else
    UNUSED_PARAM (pSysName);
#endif
    return;
}

/*****************************************************************************
 * Function Name      : EntSnmpGetTransportAddr                              *
 *                                                                           *
 * Description        : This function is called to get the Transport Address *
 *                      information                                          *
 *                                                                           *
 * Input(s)           : None                                                 *
 *                                                                           *
 * Output(s)          : pTransportAddr - Transport Address                   *
 *                                                                           *
 * Return Value(s)    : OSIX_FAILURE/ OSIX_SUCCESS                           *
 ****************************************************************************/
PUBLIC INT1
EntSnmpGetTransportAddr (UINT1 *pTransportAddr)
{
    tSnmpAgentParam    *pSnmpAgentParam = NULL;
    if ((pSnmpAgentParam = alloc_agentparams ()) == NULL)
    {
        return OSIX_FAILURE;

    }
    MEMSET (pSnmpAgentParam, 0x00, sizeof (tSnmpAgentParam));
    SnmpGetAgentParam (0, pSnmpAgentParam);
    if (pSnmpAgentParam->u2TransportAddrLength != 0)
    {
        MEMCPY (pTransportAddr, pSnmpAgentParam->au1TransportAddr,
                pSnmpAgentParam->u2TransportAddrLength);
    }
    free_agentparams (pSnmpAgentParam);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntSnmpGetTransportDomain                            *
 *                                                                           *
 * Description        : This function is called to get the Transport Domain  *
 *                      information.                                         *
 *                                                                           *
 * Input(s)           : None                                                 *
 *                                                                           *
 * Output(s)          : pTransportDomain - Transport Domain                  *
 *                                                                           *
 * Return Value(s)    : OSIX_FAILURE/ OSIX_SUCCESS                           *
 ****************************************************************************/
PUBLIC INT1
EntSnmpGetTransportDomain (UINT4 *pTransportDomain)
{
    tSnmpAgentParam    *pSnmpAgentParam = NULL;
    if ((pSnmpAgentParam = alloc_agentparams ()) == NULL)
    {
        return OSIX_FAILURE;

    }
    MEMSET (pSnmpAgentParam, 0x00, sizeof (tSnmpAgentParam));
    SnmpGetAgentParam (0, pSnmpAgentParam);
    *pTransportDomain = pSnmpAgentParam->u4TransportDomain;
    free_agentparams (pSnmpAgentParam);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntSnmpGetContextEngineID                            *
 *                                                                           *
 * Description        : This function is called to get the Context           *
 *                      Engine Id information.                               *
 *                                                                           *
 * Input(s)           : None                                                 *
 *                                                                           *
 * Output(s)          : pContextEngineId                                     *
 *                                                                           *
 * Return Value(s)    : OSIX_FAILURE/ OSIX_SUCCESS                           *
 ****************************************************************************/
PUBLIC INT1
EntSnmpGetContextEngineId (tSNMP_OCTET_STRING_TYPE * pContextEngineId)
{
    tSnmpAgentParam    *pSnmpAgentParam = NULL;
    if ((pSnmpAgentParam = alloc_agentparams ()) == NULL)
    {
        return OSIX_FAILURE;

    }
    MEMSET (pSnmpAgentParam, 0x00, sizeof (tSnmpAgentParam));
    SnmpGetAgentParam (0, pSnmpAgentParam);
    if (pSnmpAgentParam->u2EngineIdLength != 0)
    {
        MEMCPY (pContextEngineId->pu1_OctetList, pSnmpAgentParam->au1EngineId,
                pSnmpAgentParam->u2EngineIdLength);
        pContextEngineId->i4_Length = pSnmpAgentParam->u2EngineIdLength;
    }
    free_agentparams (pSnmpAgentParam);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntSnmpGetMIBNameWithContextIdAndLock                *
 *                                                                           *
 * Description        : This function is called to get the registered        *
 *                      instantiable MIB from the SNMP Agent corresponding to*
 *                      the MibNumber.                                       *
 *                                                                           *
 * Input(s)           : MibNumber - Mib number to find Mib Name              *
 *                                                                           *
 * Output(s)          : pMibName - Mib Name                                  *
 *                                                                           *
 * Return Value(s)    : OSIX_FAILURE/ OSIX_SUCCESS                           *
 ****************************************************************************/
PUBLIC INT1
EntSnmpGetMIBNameWithContextIdAndLock (UINT4 MibNumber, UINT1 *pMibName)
{
    tSnmpAgentParam    *pSnmpAgentParam = NULL;
    if ((pSnmpAgentParam = alloc_agentparams ()) == NULL)
    {
        return OSIX_FAILURE;

    }
    MEMSET (pSnmpAgentParam, 0x00, sizeof (tSnmpAgentParam));
    SnmpGetAgentParam (MibNumber, pSnmpAgentParam);
    MEMCPY (pMibName, pSnmpAgentParam->au1NameWithContextIdAndLock,
            STRLEN (pSnmpAgentParam->au1NameWithContextIdAndLock));
    free_agentparams (pSnmpAgentParam);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntSnmpGetMIBNameWithLock                            *
 *                                                                           *
 * Description        : This function is called to get the registered        *
 *                      instantiable MIB from the SNMP Agent corresponding to*
 *                      the MibNumber.                                       *
 * Input(s)           : MibNumber - Mib number to find Mib Name              *
 *                                                                           *
 * Output(s)          : pMibName - Mib Name                                  *
 *                                                                           *
 * Return Value(s)    : OSIX_FAILURE/ OSIX_SUCCESS                           *
 ****************************************************************************/
PUBLIC INT1
EntSnmpGetMIBNameWithLock (UINT4 MibNumber, UINT1 *pMibName)
{
    tSnmpAgentParam    *pSnmpAgentParam = NULL;
    if ((pSnmpAgentParam = alloc_agentparams ()) == NULL)
    {
        return OSIX_FAILURE;

    }
    MEMSET (pSnmpAgentParam, 0x00, sizeof (tSnmpAgentParam));
    SnmpGetAgentParam (MibNumber, pSnmpAgentParam);
    MEMCPY (pMibName, pSnmpAgentParam->au1NameWithLock,
            STRLEN (pSnmpAgentParam->au1NameWithLock));
    free_agentparams (pSnmpAgentParam);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntSnmpGetMIBCountWithContextIdAndLock               *
 *                                                                           *
 * Description        : This function is called to get the count of          *
 *                      registered MIBs from the SNMP Agent                  *
 *                                                                           *
 * Input(s)           : None                                                 *
 *                                                                           *
 * Output(s)          : pu4InstMibCount - Instantiable Mib Number            *
 *                      pu4NonInstMibCount - Non Instantiable Mib Number     *
 *                                                                           *
 * Return Value(s)    : OSIX_FAILURE/ OSIX_SUCCESS                           *
 ****************************************************************************/
PUBLIC INT1
EntSnmpGetMIBCountWithContextIdAndLock (UINT4 *pu4InstMibCount,
                                        UINT4 *pu4NonInstMibCount)
{
    tSnmpAgentParam    *pSnmpAgentParam = NULL;
    if ((pSnmpAgentParam = alloc_agentparams ()) == NULL)
    {
        return OSIX_FAILURE;

    }
    MEMSET (pSnmpAgentParam, 0x00, sizeof (tSnmpAgentParam));
    if (SnmpGetAgentParam (0, pSnmpAgentParam) != SNMP_SUCCESS)
    {
        free_agentparams (pSnmpAgentParam);
        return OSIX_FAILURE;
    }
    *pu4InstMibCount = pSnmpAgentParam->u4NumWithContextIdAndLock;
    *pu4NonInstMibCount = pSnmpAgentParam->u4NumWithLock;

    free_agentparams (pSnmpAgentParam);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 * Function Name      : EntSnmpGetLastChangeTime                             *
 *                                                                           *
 * Description        : This function is called to get the Last Change Time 
 *                      value                                                *
 *                                                                           *
 * Input(s)           : None                                                 *
 *                                                                           *
 * Output(s)          : pu4LastChangeTime - Last Time Change Value           *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 ****************************************************************************/
PUBLIC VOID
EntSnmpGetLastChangeTime (UINT4 *pu4LastChangeTime)
{
    tSnmpAgentParam    *pSnmpAgentParam = NULL;
    if ((pSnmpAgentParam = alloc_agentparams ()) == NULL)
    {
        return;

    }
    MEMSET (pSnmpAgentParam, 0x00, sizeof (tSnmpAgentParam));
    SnmpGetAgentParam (0, pSnmpAgentParam);
    *pu4LastChangeTime = pSnmpAgentParam->u4LastChangeTime;
    free_agentparams (pSnmpAgentParam);
    return;
}

/*****************************************************************************
 * Function Name      : EntSnmpUpdateLastTimeChange                          *
 *                                                                           *
 * Description        : This function is called to update the Last Change    *
 *                      Time                                                 *
 *                                                                           *
 * Input(s)           : None                                                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 ****************************************************************************/
PUBLIC VOID
EntSnmpUpdateLastTimeChange (VOID)
{
#ifdef SNMP_3_WANTED
    /* Update the LastChangeTime */
    SnmpUpdateLastTimeChange ();
#endif
    return;
}

/*****************************************************************************
 * Function Name      : EntSnmpGetLastChangeTime                             *
 *                                                                           *
 * Description        : This function is called to update the Last Change 
 *                      Time                                                 *
 *                                                                           *
 * Input(s)           : None                                                 *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 ****************************************************************************/
PUBLIC INT1
EntityInit (VOID)
{
    UINT4               i4SysLogId = 0;
#ifdef SYSLOG_WANTED
    /* Register with SysLog */
    i4SysLogId = SYS_LOG_REGISTER (ENTITY_NAME, SYSLOG_CRITICAL_LEVEL);
    if (i4SysLogId <= 0)
    {
        return OSIX_FAILURE;
    }
#endif /* SYSLOG_WANTED */

    ENTITY_SYSLOG_ID = (UINT4) (i4SysLogId);
    return OSIX_SUCCESS;
}
