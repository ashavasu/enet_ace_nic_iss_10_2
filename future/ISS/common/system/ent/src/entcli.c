/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 * $Id: entcli.c,v 1.19 2015/02/04 11:40:42 siva Exp $
 *
 * Description:  This file contains CLI SET/GET/TEST and GETNEXT
 *               routines for the MIB objects specified in stdent.mib
 *******************************************************************/

#ifndef __ENTCLI_C
#define __ENTCLI_C

#include "issinc.h"
#include "isscli.h"
#include "stdentcli.h"

PRIVATE INT4 EntPrintPhyEntity PROTO ((tCliHandle, INT4));
PRIVATE INT4 EntPrintLogEntity PROTO ((tCliHandle, INT4));

#define ENT_CLI_MAX_ARGS        10    /* Max number of tokens in ENT CLI
                                       commands */

UINT1              *gapu1PhyClass[] = {
    (UINT1 *) "Other\0",
    (UINT1 *) "Unknown\0",
    (UINT1 *) "Chassis\0",
    (UINT1 *) "Backplane\0",
    (UINT1 *) "Container\0",
    (UINT1 *) "Power Supply\0",
    (UINT1 *) "Fan\0",
    (UINT1 *) "Sensor\0",
    (UINT1 *) "Module\0",
    (UINT1 *) "Port\0",
    (UINT1 *) "Stack\0",
    (UINT1 *) "Cpu\0"
};

/****************************************************************************/
/*                                                                          */
/*    Function Name      : cli_process_ent_cmd                              */
/*                                                                          */
/*    Description        : This function is the entry function for the      */
/*                         FS CLI module inside the Entity  module.         */
/*                                                                          */
/*    Input(s)           : Command argument list                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : CLI_SUCCESS/CLI_FAILURE                          */
/****************************************************************************/
INT4
cli_process_ent_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[ENT_CLI_MAX_ARGS];
    INT1                argno = 0;
    INT4                i4RetStatus = CLI_SUCCESS;

    va_start (ap, u4Command);

    /* Walk through the rest of the arguments and store in args array. 
     * In the variable argument list, there is a possibility of NULL existing
     * between valid arguments, hence we parse the arg list for a pre-defined
     * number of iterations.
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == ENT_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    /*LOCK (); */
    switch (u4Command)
    {
        case CLI_ENT_CFG_ASSET_ID:
            i4RetStatus =
                EntCliConfigPhyEntity (CliHandle, ISS_ENT_PHY_ASSET_ID,
                                       *(INT4 *) args[1], (UINT1 *) args[2]);
            break;
        case CLI_ENT_CFG_SER_NUM:
            i4RetStatus =
                EntCliConfigPhyEntity (CliHandle, ISS_ENT_PHY_SERIAL_NUM,
                                       *(INT4 *) args[1], (UINT1 *) args[2]);
            break;
        case CLI_ENT_CFG_ALIAS_NAME:
            i4RetStatus =
                EntCliConfigPhyEntity (CliHandle, ISS_ENT_PHY_ALIAS_NAME,
                                       *(INT4 *) args[1], (UINT1 *) args[2]);
            break;
        case CLI_ENT_CFG_URIS:
            i4RetStatus = EntCliConfigPhyEntity (CliHandle, ISS_ENT_PHY_URIS,
                                                 *(INT4 *) args[1],
                                                 (UINT1 *) args[2]);
            break;
        case CLI_ENT_RESET_URIS:
            i4RetStatus = EntCliResetPhyEntity (CliHandle, ISS_ENT_PHY_URIS,
                                                *(INT4 *) args[1]);
            break;
        case CLI_ENT_RESET_ALL:
            i4RetStatus = EntCliResetPhyEntity (CliHandle, ENT_PHY_RESET_ALL,
                                                *(INT4 *) args[1]);
            break;
        case CLI_ENT_RESET_ASSET_ID:
            i4RetStatus = EntCliResetPhyEntity (CliHandle, ISS_ENT_PHY_ASSET_ID,
                                                *(INT4 *) args[1]);
            break;
        case CLI_ENT_RESET_SER_NUM:
            i4RetStatus =
                EntCliResetPhyEntity (CliHandle, ISS_ENT_PHY_SERIAL_NUM,
                                      *(INT4 *) args[1]);
            break;
        case CLI_ENT_RESET_ALIAS_NAME:
            i4RetStatus =
                EntCliResetPhyEntity (CliHandle, ISS_ENT_PHY_ALIAS_NAME,
                                      *(INT4 *) args[1]);
            break;

        case CLI_ENT_SHOW_LOG_ENTITY:
            i4RetStatus = EntCliShowLogEntity (CliHandle, *(INT4 *) args[1]);
            break;

        case CLI_ENT_SHOW_LOG_ENTITY_ALL:
            i4RetStatus = EntCliShowAllLogEntities (CliHandle);
            break;

        case CLI_ENT_SHOW_PHY_ENTITY:
            i4RetStatus = EntCliShowPhyEntity (CliHandle, *(INT4 *) args[1]);
            break;

        case CLI_ENT_SHOW_PHY_ENTITY_ALL:
            i4RetStatus = EntCliShowAllPhyEntities (CliHandle);
            break;

        case CLI_ENT_SHOW_LP_MAPPING_ALL:
            i4RetStatus = EntCliShowAllLPMapEntries (CliHandle);
            break;

        case CLI_ENT_SHOW_ALIAS_MAPPING:
            i4RetStatus =
                EntCliShowAliasMapEntries (CliHandle, *(INT4 *) args[1]);
            break;

        case CLI_ENT_SHOW_ALIAS_MAPPING_ALL:
            i4RetStatus = EntCliShowAllAliasMapEntries (CliHandle);
            break;

        case CLI_ENT_SHOW_PHY_CONTAINMENT:
            i4RetStatus =
                EntCliShowPhyContainment (CliHandle, *(INT4 *) args[1]);
            break;

        case CLI_ENT_SHOW_PHY_CONTAINMENT_ALL:
            i4RetStatus = EntCliShowAllPhyContainment (CliHandle);
            break;

        default:
            CliPrintf (CliHandle, "\r\n%% Unknown command\r\n");
            break;
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    /* No failures will be returned from command Action routines as of now.
     * If in future, error code are handled in Lowlevel routines then 
     * check for i4RetStatus & ErrorCode and display the proper error string 
     */
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : EntCliShowAllLPMapEntries                          */
/*                                                                           */
/*     DESCRIPTION      : This function displays the logical and physical    */
/*                        mapping information                                */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4LogIndex -                                       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
EntCliShowAllLPMapEntries (tCliHandle CliHandle)
{
    INT4                i4LogIndex = 0;
    INT4                i4PhyIndex = 0;
    INT4                i4NextLogIndex = 0;
    INT4                i4NextPhyIndex = 0;

    INT4                i4PhyClass = 0;
    tSNMP_OID_TYPE      RetLogType;
    UINT4               au4LogType[ENT_MAX_TYPE_LEN];

    UINT1               au1Temp1[30] = { 0 };
    UINT1               au1Temp2[30] = { 0 };

    if (nmhGetNextIndexEntLPMappingTable (0, &i4NextLogIndex,
                                          0, &i4NextPhyIndex) != SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "%-30s%-30s",
                   "Logical Entity", "Mapped Physical Entity");
        CliPrintf (CliHandle, "\r\n");

        CliPrintf (CliHandle, "%-30s%-30s",
                   "--------------", "----------------------");

        CliPrintf (CliHandle, "\r\n\n");
        i4NextLogIndex = 0;
        i4NextPhyIndex = 0;
    }
    else
    {
        CliPrintf (CliHandle, "\n\r%% No Mapping exists \n\n\r");
        return CLI_SUCCESS;
    }

    while (nmhGetNextIndexEntLPMappingTable (i4LogIndex, &i4NextLogIndex,
                                             i4PhyIndex, &i4NextPhyIndex) !=
           SNMP_FAILURE)
    {

        MEMSET (au4LogType, 0, (sizeof (UINT4) * ENT_MAX_TYPE_LEN));
        MEMSET (&RetLogType, 0, sizeof (tSNMP_OID_TYPE));
        RetLogType.pu4_OidList = au4LogType;
        RetLogType.u4_Length = 0;

        i4PhyClass = 0;

        if (nmhGetEntLogicalType (i4NextLogIndex, &RetLogType) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhGetEntPhysicalClass (i4NextPhyIndex, &i4PhyClass) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (RetLogType.u4_Length == 0)
        {
            SPRINTF ((CHR1 *) au1Temp1, "%d (%s)", i4NextLogIndex,
                     "not available");
            SPRINTF ((CHR1 *) au1Temp2, "%d (%s)", i4NextPhyIndex,
                     gapu1PhyClass[i4PhyClass - 1]);
            CliPrintf (CliHandle, "%-30s%-30s\n\r", au1Temp1, au1Temp2);
        }
        else
        {
            SPRINTF ((CHR1 *) au1Temp1, "%d (%s)", i4NextLogIndex,
                     (char *) RetLogType.pu4_OidList);
            SPRINTF ((CHR1 *) au1Temp2, "%d (%s)", i4NextPhyIndex,
                     gapu1PhyClass[i4PhyClass - 1]);
            CliPrintf (CliHandle, "%-30s%-30s\n\r", au1Temp1, au1Temp2);

        }

        i4LogIndex = i4NextLogIndex;
        i4PhyIndex = i4NextPhyIndex;
    }

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : EntCliShowAliasMapEntries                          */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Alias Mapping  
 *                        information                                        */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4PhyIndex - Identifying the physical component    */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
EntCliShowAliasMapEntries (tCliHandle CliHandle, INT4 i4PhyIndex)
{
    INT4                i4PhyClass = 0;
    INT4                i4LogIndex = 0;

    tSNMP_OID_TYPE      RetAliasMapId;
    UINT4               au4AliasMap[ISS_ENT_PHY_ALIAS_LEN];
    UINT1               au1Temp1[30] = { 0 };

    MEMSET (au4AliasMap, 0, sizeof (au4AliasMap));
    MEMSET (&RetAliasMapId, 0, sizeof (tSNMP_OID_TYPE));
    RetAliasMapId.pu4_OidList = au4AliasMap;
    RetAliasMapId.u4_Length = 0;

    if (nmhValidateIndexInstanceEntPhysicalTable (i4PhyIndex) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhGetEntPhysicalClass (i4PhyIndex, &i4PhyClass);

    if (i4PhyClass != ENT_PHY_CLASS_PORT)
    {
        CliPrintf (CliHandle,
                   "\n\r%% No Mapping exist for this physical index\n\r");
    }
    else
    {
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "%-23s%-20s%-20s",
                   "Physical Entity", "Logical Entity",
                   "Mapped External Identifier");
        CliPrintf (CliHandle, "\r\n");

        CliPrintf (CliHandle, "%-23s%-20s%-20s",
                   "---------------", "--------------",
                   "--------------------------");

        CliPrintf (CliHandle, "\r\n");

        nmhGetEntAliasMappingIdentifier (i4PhyIndex, i4LogIndex,
                                         &RetAliasMapId);

        SPRINTF ((CHR1 *) au1Temp1, "%d (%s)", i4PhyIndex, "Port");
        CliPrintf (CliHandle,
                   "%-23s%-20s"
                   "%-20s\n\r", au1Temp1, "all", RetAliasMapId.pu4_OidList);
    }

    CliPrintf (CliHandle, "\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : EntCliShowAllAliasMapEntries                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays the alias mapping 
 *                        information for all the External Identifiers       */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
EntCliShowAllAliasMapEntries (tCliHandle CliHandle)
{
    INT4                i4LogIndex = 0;
    INT4                i4PhyIndex = 0;
    INT4                i4NextLogIndex = 0;
    INT4                i4NextPhyIndex = 0;
    tSNMP_OID_TYPE      RetAliasMapId;
    UINT4               au4AliasMap[ISS_ENT_PHY_ALIAS_LEN];
    UINT1               au1Temp1[30] = { 0 };

    if (nmhGetNextIndexEntAliasMappingTable (0, &i4NextPhyIndex,
                                             0, &i4NextLogIndex) !=
        SNMP_FAILURE)
    {

        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "%-23s%-20s%-20s",
                   "Physical Entity", "Logical Entity",
                   "Mapped External Identifier");
        CliPrintf (CliHandle, "\r\n");

        CliPrintf (CliHandle, "%-23s%-20s%-20s",
                   "---------------", "--------------",
                   "--------------------------");

        CliPrintf (CliHandle, "\r\n");
        i4NextPhyIndex = 0;
        i4NextLogIndex = 0;
    }
    else
    {
        CliPrintf (CliHandle, "\n\r%% No Mapping exists\n\n\r");
        return CLI_SUCCESS;
    }

    while (nmhGetNextIndexEntAliasMappingTable (i4PhyIndex, &i4NextPhyIndex,
                                                i4LogIndex, &i4NextLogIndex) !=
           SNMP_FAILURE)
    {
        MEMSET (au4AliasMap, 0, sizeof (au4AliasMap));
        MEMSET (&RetAliasMapId, 0, sizeof (tSNMP_OID_TYPE));
        RetAliasMapId.pu4_OidList = au4AliasMap;
        RetAliasMapId.u4_Length = 0;

        nmhGetEntAliasMappingIdentifier (i4NextPhyIndex, i4NextLogIndex,
                                         &RetAliasMapId);

        SPRINTF ((CHR1 *) au1Temp1, "%d (%s)", i4NextPhyIndex, "Port");
        CliPrintf (CliHandle,
                   "%-23s%-20s"
                   "%-20s\n\r", au1Temp1, "all", RetAliasMapId.pu4_OidList);

        i4PhyIndex = i4NextPhyIndex;
        i4LogIndex = i4NextLogIndex;
    }

    CliPrintf (CliHandle, "\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : EntCliShowPhyContainment                           */
/*                                                                           */
/*     DESCRIPTION      : This function displays the containment  
 *                        information                                        */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4PhyIndex - Identifying the physical component    */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
EntCliShowPhyContainment (tCliHandle CliHandle, INT4 i4PhyIndex)
{
    INT4                i4return = SNMP_SUCCESS;
    INT4                i4PrevPhyIndex = 0;
    INT4                i4NextPhyIndex = 0;
    INT4                i4PhyChildIndex = 0;
    INT4                i4PhyChildNextIndex = 0;
    INT4                i4PhyClass = 0;
    UINT4               u4Flag = 0;

    BOOL1               b1VisitedFlag = OSIX_FALSE;

    if (nmhValidateIndexInstanceEntPhysicalTable (i4PhyIndex) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    while ((i4return =
            nmhGetNextIndexEntPhysicalContainsTable (i4PrevPhyIndex,
                                                     &i4NextPhyIndex,
                                                     i4PhyChildIndex,
                                                     &i4PhyChildNextIndex)) !=
           SNMP_FAILURE)
    {
        if (i4NextPhyIndex == i4PhyIndex)
        {
            break;
        }
        else if (i4NextPhyIndex < i4PhyIndex)
        {
            i4PrevPhyIndex = i4NextPhyIndex;
            i4PhyChildIndex = i4PhyChildNextIndex;
            continue;
        }
        else
        {
            CliPrintf (CliHandle,
                       "\n\r%% No Member Physical Entities present for "
                       "physical index\n\n\r");
            return CLI_SUCCESS;
        }
    }

    if (i4return == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\n\r%% No Member Physical Entities present for "
                   "physical index\n\n\r");
        return CLI_SUCCESS;
    }

    while (nmhGetNextIndexEntPhysicalContainsTable (i4PrevPhyIndex,
                                                    &i4NextPhyIndex,
                                                    i4PhyChildIndex,
                                                    &i4PhyChildNextIndex) !=
           SNMP_FAILURE)
    {
        if ((i4PhyIndex == i4NextPhyIndex) && (b1VisitedFlag == OSIX_FALSE))
        {
            CliPrintf (CliHandle, "\n\rContainmaint Relationship");
            CliPrintf (CliHandle, "\n\r-------------------------");

            if (nmhGetEntPhysicalClass (i4NextPhyIndex, &i4PhyClass) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            CliPrintf (CliHandle, "\n\n\rPhysical Entity            : %d (%s) ",
                       i4NextPhyIndex, gapu1PhyClass[i4PhyClass - 1]);

            if (nmhGetEntPhysicalClass (i4PhyChildNextIndex, &i4PhyClass) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            CliPrintf (CliHandle, "\n\rMember Physical Entities   : %d (%s)",
                       i4PhyChildNextIndex, gapu1PhyClass[i4PhyClass - 1]);
            u4Flag = 2;

            b1VisitedFlag = OSIX_TRUE;
        }
        else if ((i4PhyIndex == i4NextPhyIndex) && (u4Flag == 4))
        {
            if (nmhGetEntPhysicalClass (i4PhyChildNextIndex, &i4PhyClass) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            CliPrintf (CliHandle, "\n\r                             %d (%s)",
                       i4PhyChildNextIndex, gapu1PhyClass[i4PhyClass - 1]);

            u4Flag = 2;
            i4PrevPhyIndex = i4NextPhyIndex;
            i4PhyChildIndex = i4PhyChildNextIndex;
            continue;
        }

        else if ((i4PhyIndex == i4NextPhyIndex) && ((u4Flag == 2) ||
                                                    (u4Flag == 3)))
        {
            if (nmhGetEntPhysicalClass (i4PhyChildNextIndex, &i4PhyClass) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            CliPrintf (CliHandle, ", %d (%s)", i4PhyChildNextIndex,
                       gapu1PhyClass[i4PhyClass - 1]);
            u4Flag = u4Flag + 1;
        }
        else
        {
            break;
        }
        i4PrevPhyIndex = i4NextPhyIndex;
        i4PhyChildIndex = i4PhyChildNextIndex;
    }

    CliPrintf (CliHandle, "\r\n-----------------------------"
               "----------------------------------\n\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : EntCliShowAllPhyContainment                        */
/*                                                                           */
/*     DESCRIPTION      : This function displays the containment  
 *                        information for all the physical components        */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
EntCliShowAllPhyContainment (tCliHandle CliHandle)
{
    INT4                i4PhyIndex = 0;
    INT4                i4NextPhyIndex = 0;
    INT4                i4PhyChildIndex = 0;
    INT4                i4PhyChildNextIndex = 0;
    INT4                i4PhyClass = 0;
    UINT4               u4Flag = 0;

    CliPrintf (CliHandle, "\n\rContainmaint Relationship");
    CliPrintf (CliHandle, "\n\r-------------------------");

    while (nmhGetNextIndexEntPhysicalContainsTable (i4PhyIndex, &i4NextPhyIndex,
                                                    i4PhyChildIndex,
                                                    &i4PhyChildNextIndex) !=
           SNMP_FAILURE)
    {
        if (i4PhyIndex != i4NextPhyIndex)
        {

            if (nmhGetEntPhysicalClass (i4NextPhyIndex, &i4PhyClass) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            CliPrintf (CliHandle, "\n\n\rPhysical Entity            : %d (%s) ",
                       i4NextPhyIndex, gapu1PhyClass[i4PhyClass - 1]);

            if (nmhGetEntPhysicalClass (i4PhyChildNextIndex, &i4PhyClass) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            CliPrintf (CliHandle, "\n\rMember Physical Entities   : %d (%s)",
                       i4PhyChildNextIndex, gapu1PhyClass[i4PhyClass - 1]);
            u4Flag = 2;

        }
        else if ((i4PhyIndex == i4NextPhyIndex) && (u4Flag == 4))
        {
            if (nmhGetEntPhysicalClass (i4PhyChildNextIndex, &i4PhyClass) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            CliPrintf (CliHandle, "\n\r                             %d (%s)",
                       i4PhyChildNextIndex, gapu1PhyClass[i4PhyClass - 1]);

            u4Flag = 2;
            i4PhyIndex = i4NextPhyIndex;
            i4PhyChildIndex = i4PhyChildNextIndex;
            continue;
        }

        else if ((i4PhyIndex == i4NextPhyIndex) && ((u4Flag == 2) ||
                                                    (u4Flag == 3)))
        {
            if (nmhGetEntPhysicalClass (i4PhyChildNextIndex, &i4PhyClass) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            CliPrintf (CliHandle, ", %d (%s)", i4PhyChildNextIndex,
                       gapu1PhyClass[i4PhyClass - 1]);
            u4Flag = u4Flag + 1;
        }

        i4PhyIndex = i4NextPhyIndex;
        i4PhyChildIndex = i4PhyChildNextIndex;
    }

    CliPrintf (CliHandle, "\r\n------------------------------"
               "------------------------------\n\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : EntCliShowLogEntity                                */
/*                                                                           */
/*     DESCRIPTION      : This function displays the logical entity 
 *                        information                                        */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4LogIndex - Identifying the logical component     */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
EntCliShowLogEntity (tCliHandle CliHandle, INT4 i4LogIndex)
{
    tSNMP_OID_TYPE     *pRetLogType = NULL;

    tSNMP_OCTET_STRING_TYPE RetLogTemp;
    UINT1               au1LogTemp[ENT_MAX_STR_LEN];
    UINT1               au1Log[ENT_MAX_STR_LEN];

    UINT4               u4TAddress = 0;
    UINT4               u4TempStrLen = 0;
    UINT2               u2TPort = 0;
    CHR1               *pu1TAddress = NULL;

    MEMSET (au1LogTemp, 0, ENT_MAX_STR_LEN);
    MEMSET (&RetLogTemp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RetLogTemp.pu1_OctetList = au1LogTemp;
    RetLogTemp.i4_Length = 0;

    if (nmhValidateIndexInstanceEntLogicalTable (i4LogIndex) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "Logical Index: %d\n\r", i4LogIndex);

    if (nmhGetEntLogicalDescr (i4LogIndex, &RetLogTemp) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (RetLogTemp.i4_Length != 0)
    {
        CliPrintf (CliHandle, "Logical Description: %s\n\r",
                   RetLogTemp.pu1_OctetList);
    }
    else
    {
        CliPrintf (CliHandle, "Logical Description: %s\n\r", "not available");
    }
    pRetLogType = (tSNMP_OID_TYPE *) alloc_oid (ENT_MAX_STR_LEN);
    if (pRetLogType == NULL)
    {
        return CLI_FAILURE;
    }
    MEMSET (pRetLogType->pu4_OidList, 0, (sizeof (UINT4) * ENT_MAX_STR_LEN));
    pRetLogType->u4_Length = 0;
    if (nmhGetEntLogicalType (i4LogIndex, pRetLogType) == SNMP_FAILURE)
    {
        free_oid (pRetLogType);
        return CLI_FAILURE;
    }

    if (pRetLogType->u4_Length != 0)
    {
        CliPrintf (CliHandle, "Logical Type: %s\n\r", pRetLogType->pu4_OidList);
    }
    else
    {
        CliPrintf (CliHandle, "Logical Type: %s\n\r", "not available");
    }
    free_oid (pRetLogType);
    MEMSET (au1LogTemp, 0, ENT_MAX_STR_LEN);
    MEMSET (&RetLogTemp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RetLogTemp.pu1_OctetList = au1LogTemp;
    RetLogTemp.i4_Length = 0;
    if (nmhGetEntLogicalCommunity (i4LogIndex, &RetLogTemp) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (RetLogTemp.i4_Length != 0)
    {
        CliPrintf (CliHandle, "Logical Community: %s\n\r",
                   RetLogTemp.pu1_OctetList);
    }
    else
    {
        CliPrintf (CliHandle, "Logical Community: %s\n\r", "not available");
    }
    MEMSET (au1LogTemp, 0, ENT_MAX_STR_LEN);
    MEMSET (&RetLogTemp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RetLogTemp.pu1_OctetList = au1LogTemp;
    RetLogTemp.i4_Length = 0;
    if (nmhGetEntLogicalTAddress (i4LogIndex, &RetLogTemp) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (RetLogTemp.i4_Length != 0)
    {
        pu1TAddress = (CHR1 *) au1Log;
        ENT_GET_4BYTE (u4TAddress, RetLogTemp.pu1_OctetList);
        CLI_CONVERT_IPADDR_TO_STR (pu1TAddress, u4TAddress);
        u4TempStrLen = STRLEN (pu1TAddress);
        SPRINTF ((CHR1 *) (pu1TAddress + u4TempStrLen), ":");
        u4TempStrLen++;

        ENT_GET_2BYTE (u2TPort,
                       &(RetLogTemp.
                         pu1_OctetList[ENT_TRANSPORT_ADDR_PORT_INDEX]));
        SPRINTF ((CHR1 *) (pu1TAddress + u4TempStrLen), "%d", u2TPort);

        CliPrintf (CliHandle, "Logical Transport Address: %s\n\r", pu1TAddress);
    }
    else
    {
        CliPrintf (CliHandle, "Logical Transport Address: %s\n\r",
                   "not available");
    }
    pRetLogType = (tSNMP_OID_TYPE *) alloc_oid (ENT_MAX_STR_LEN);
    if (pRetLogType == NULL)
    {
        return CLI_FAILURE;
    }
    MEMSET (pRetLogType->pu4_OidList, 0, (sizeof (UINT4) * ENT_MAX_STR_LEN));
    pRetLogType->u4_Length = 0;
    if (nmhGetEntLogicalTDomain (i4LogIndex, pRetLogType) == SNMP_FAILURE)
    {
        free_oid (pRetLogType);
        return CLI_FAILURE;
    }

    if (pRetLogType->u4_Length != 0)
    {
        CliPrintf (CliHandle, "Logical Transport Domain: %s\n\r",
                   pRetLogType->pu4_OidList);
    }
    else
    {
        CliPrintf (CliHandle, "Logical Transport Domain: %s\n\r",
                   "not available");
    }
    free_oid (pRetLogType);
    MEMSET (au1Log, 0, ENT_MAX_STR_LEN);
    MEMSET (au1LogTemp, 0, ENT_MAX_STR_LEN);
    MEMSET (&RetLogTemp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RetLogTemp.pu1_OctetList = au1LogTemp;
    RetLogTemp.i4_Length = 0;

    if (nmhGetEntLogicalContextEngineID (i4LogIndex, &RetLogTemp) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    CliOctetToStr (RetLogTemp.pu1_OctetList,
                   RetLogTemp.i4_Length, au1Log, ENT_MAX_STR_LEN);

    CliPrintf (CliHandle, "Logical Context Engine Id: %s\n\r", au1Log);
    MEMSET (au1LogTemp, 0, ENT_MAX_STR_LEN);
    MEMSET (&RetLogTemp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RetLogTemp.pu1_OctetList = au1LogTemp;
    RetLogTemp.i4_Length = 0;

    if (nmhGetEntLogicalContextName (i4LogIndex, &RetLogTemp) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (RetLogTemp.i4_Length != 0)
    {
        CliPrintf (CliHandle, "Logical Context Name: %s\n\r",
                   RetLogTemp.pu1_OctetList);
    }
    else
    {
        CliPrintf (CliHandle, "Logical Context Name: %s\n\r", "not available");
    }

    CliPrintf (CliHandle,
               "\n-------------------------------------------------\n\n\r");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : EntCliShowPhyEntity                                */
/*                                                                           */
/*     DESCRIPTION      : This function displays the physical entity 
 *                        information                                        */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4PhyIndex - Identifying the physical component    */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
EntCliShowPhyEntity (tCliHandle CliHandle, INT4 i4PhyIndex)
{

    tSNMP_OCTET_STRING_TYPE RetPhyTemp;
    tSNMP_OCTET_STRING_TYPE RetPhyNum;
    tSNMP_OID_TYPE     *pRetPhyType = NULL;

    INT4                i4PhyClass = 0;
    INT4                i4ParentRelPos = 0;
    INT4                i4ContainedIn = 0;

    UINT1               au1PhyTemp[ISS_ENT_PHY_DESCR_LEN];
    UINT1               au1PhyNum[ISS_ENT_PHY_NUM];

    INT4                i4FRUStatus = ENT_SNMP_FALSE;
    UINT1               au1Temp[ISS_ENT_PHY_MFG_DATE_LEN] = { 0 };
    UINT2               u2DecValue = 0;
    UINT1               u1DecValue = 0;
    UINT1               u1Index = 0;
    UINT1               u1Index2 = 0;

    MEMSET (au1PhyTemp, 0, ISS_ENT_PHY_DESCR_LEN);
    MEMSET (&RetPhyTemp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RetPhyTemp.pu1_OctetList = au1PhyTemp;
    RetPhyTemp.i4_Length = 0;

    if (nmhValidateIndexInstanceEntPhysicalTable (i4PhyIndex) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "\n\r");

    CliPrintf (CliHandle, "Physical Index: %d\n\r", i4PhyIndex);

    if (nmhGetEntPhysicalDescr (i4PhyIndex, &RetPhyTemp) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (RetPhyTemp.i4_Length != 0)
    {
        CliPrintf (CliHandle, "Physical Descr: %s\n\r",
                   RetPhyTemp.pu1_OctetList);
    }
    else
    {
        CliPrintf (CliHandle, "Physical Descr: %s\n\r", "not available");
    }
    pRetPhyType = (tSNMP_OID_TYPE *) alloc_oid (ISS_ENT_PHY_TYPE_LEN);
    if (pRetPhyType == NULL)
    {
        return CLI_FAILURE;
    }

    MEMSET (pRetPhyType->pu4_OidList, 0,
            (sizeof (UINT4) * ISS_ENT_PHY_TYPE_LEN));
    pRetPhyType->u4_Length = 0;

    if (nmhGetEntPhysicalVendorType (i4PhyIndex, pRetPhyType) == SNMP_FAILURE)
    {
        free_oid (pRetPhyType);
        return CLI_FAILURE;
    }
    if (pRetPhyType->u4_Length != 0)
    {
        CliPrintf (CliHandle, "Physical VendorType: %s\n\r",
                   pRetPhyType->pu4_OidList);
    }
    else
    {
        CliPrintf (CliHandle, "Physical VendorType: %s\n\r", "not available");
    }
    free_oid (pRetPhyType);

    if (nmhGetEntPhysicalContainedIn (i4PhyIndex, &i4ContainedIn) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "Physical ContainedIn: %d\n\r", i4ContainedIn);

    if (nmhGetEntPhysicalClass (i4PhyIndex, &i4PhyClass) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "Physical Class: %s\n\r",
               gapu1PhyClass[i4PhyClass - 1]);

    if (nmhGetEntPhysicalParentRelPos (i4PhyIndex, &i4ParentRelPos) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "Physical ParentRelPos: %d\n\r", i4ParentRelPos);
    MEMSET (au1PhyTemp, 0, ISS_ENT_PHY_DESCR_LEN);
    MEMSET (&RetPhyTemp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RetPhyTemp.pu1_OctetList = au1PhyTemp;
    RetPhyTemp.i4_Length = 0;

    if (nmhGetEntPhysicalName (i4PhyIndex, &RetPhyTemp) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (RetPhyTemp.i4_Length != 0)
    {
        CliPrintf (CliHandle, "Physical Name: %s\n\r",
                   RetPhyTemp.pu1_OctetList);
    }
    else
    {
        CliPrintf (CliHandle, "Physical Name: %s\n\r", "not available");
    }
    MEMSET (au1PhyTemp, 0, ISS_ENT_PHY_DESCR_LEN);
    MEMSET (&RetPhyTemp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RetPhyTemp.pu1_OctetList = au1PhyTemp;
    RetPhyTemp.i4_Length = 0;

    if (nmhGetEntPhysicalHardwareRev (i4PhyIndex, &RetPhyTemp) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (RetPhyTemp.i4_Length != 0)
    {
        CliPrintf (CliHandle, "Physical HardwareRev: %s\n\r",
                   RetPhyTemp.pu1_OctetList);
    }
    else
    {
        CliPrintf (CliHandle, "Physical HardwareRev: %s\n\r", "not available");
    }
    MEMSET (au1PhyTemp, 0, ISS_ENT_PHY_DESCR_LEN);
    MEMSET (&RetPhyTemp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RetPhyTemp.pu1_OctetList = au1PhyTemp;
    RetPhyTemp.i4_Length = 0;

    if (nmhGetEntPhysicalSoftwareRev (i4PhyIndex, &RetPhyTemp) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (RetPhyTemp.i4_Length != 0)
    {
        CliPrintf (CliHandle, "Physical SoftwareRev: %s\n\r",
                   RetPhyTemp.pu1_OctetList);
    }
    else
    {
        CliPrintf (CliHandle, "Physical SoftwareRev: %s\n\r", "not available");
    }
    MEMSET (au1PhyTemp, 0, ISS_ENT_PHY_DESCR_LEN);
    MEMSET (&RetPhyTemp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RetPhyTemp.pu1_OctetList = au1PhyTemp;
    RetPhyTemp.i4_Length = 0;

    if (nmhGetEntPhysicalFirmwareRev (i4PhyIndex, &RetPhyTemp) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (RetPhyTemp.i4_Length != 0)
    {
        CliPrintf (CliHandle, "Physical FirmwareRev: %s\n\r",
                   RetPhyTemp.pu1_OctetList);
    }
    else
    {
        CliPrintf (CliHandle, "Physical FirmwareRev: %s\n\r", "not available");
    }
    MEMSET (au1PhyNum, 0, ISS_ENT_PHY_NUM);
    MEMSET (&RetPhyNum, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RetPhyNum.pu1_OctetList = au1PhyNum;
    RetPhyNum.i4_Length = 0;

    if (nmhGetEntPhysicalSerialNum (i4PhyIndex, &RetPhyNum) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (RetPhyNum.i4_Length != 0)
    {
        CliPrintf (CliHandle, "Physical Serial Num: %s\n\r",
                   RetPhyNum.pu1_OctetList);
    }
    else
    {
        CliPrintf (CliHandle, "Physical Serial Num: %s\n\r", "not available");
    }
    MEMSET (au1PhyTemp, 0, ISS_ENT_PHY_DESCR_LEN);
    MEMSET (&RetPhyTemp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RetPhyTemp.pu1_OctetList = au1PhyTemp;
    RetPhyTemp.i4_Length = 0;

    if (nmhGetEntPhysicalMfgName (i4PhyIndex, &RetPhyTemp) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (RetPhyTemp.i4_Length != 0)
    {
        CliPrintf (CliHandle, "Physical MfgName: %s\n\r",
                   RetPhyTemp.pu1_OctetList);
    }
    else
    {
        CliPrintf (CliHandle, "Physical MfgName: %s\n\r", "not available");
    }
    MEMSET (au1PhyTemp, 0, ISS_ENT_PHY_DESCR_LEN);
    MEMSET (&RetPhyTemp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RetPhyTemp.pu1_OctetList = au1PhyTemp;
    RetPhyTemp.i4_Length = 0;

    if (nmhGetEntPhysicalModelName (i4PhyIndex, &RetPhyTemp) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (RetPhyTemp.i4_Length != 0)
    {
        CliPrintf (CliHandle, "Physical ModelName: %s\n\r",
                   RetPhyTemp.pu1_OctetList);
    }
    else
    {
        CliPrintf (CliHandle, "Physical ModelName: %s\n\r", "not available");
    }
    MEMSET (au1PhyNum, 0, ISS_ENT_PHY_NUM);
    MEMSET (&RetPhyNum, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RetPhyNum.pu1_OctetList = au1PhyNum;
    RetPhyNum.i4_Length = 0;

    if (nmhGetEntPhysicalAlias (i4PhyIndex, &RetPhyNum) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (RetPhyNum.i4_Length != 0)
    {
        CliPrintf (CliHandle, "Physical Alias: %s \n\r",
                   RetPhyNum.pu1_OctetList);
    }
    else
    {
        CliPrintf (CliHandle, "Physical Alias: %s\n\r", "not available");
    }
    MEMSET (au1PhyNum, 0, ISS_ENT_PHY_NUM);
    MEMSET (&RetPhyNum, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RetPhyNum.pu1_OctetList = au1PhyNum;
    RetPhyNum.i4_Length = 0;

    if (nmhGetEntPhysicalAssetID (i4PhyIndex, &RetPhyNum) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (RetPhyNum.i4_Length != 0)
    {
        CliPrintf (CliHandle, "Physical AssetID: %s \n\r",
                   RetPhyNum.pu1_OctetList);
    }
    else
    {
        CliPrintf (CliHandle, "Physical AssetID: %s\n\r", "not available");
    }

    MEMSET (au1PhyTemp, 0, ISS_ENT_PHY_DESCR_LEN);
    MEMSET (&RetPhyTemp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RetPhyTemp.pu1_OctetList = au1PhyTemp;
    RetPhyTemp.i4_Length = 0;

    if (nmhGetEntPhysicalMfgDate (i4PhyIndex, &RetPhyTemp) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (RetPhyTemp.i4_Length != 0)
    {
        ENT_GET_2BYTE (u2DecValue, RetPhyTemp.pu1_OctetList);
        SPRINTF ((CHR1 *) au1Temp, "%u", u2DecValue);
        u1Index = ENT_MFG_DAT_YEAR_INDEX;
        u1Index2 = ENT_MFG_DAT_TIME_SEP_INDEX;
        while ((u1Index < RetPhyTemp.i4_Length) &&
               (u1Index < sizeof (au1PhyTemp)))
        {
            if ((u1Index == ENT_MFG_DAT_YEAR_INDEX) ||
                (u1Index == ENT_MFG_DAT_YEAR_INDEX + 1))
            {
                SPRINTF ((CHR1 *) (au1Temp + u1Index2), "-");
                u1Index2++;
            }
            if ((u1Index == ENT_MFG_DAT_TIME_SEP_INDEX)
                || (u1Index == ENT_MFG_DAT_COMMA_INDEX))
            {
                SPRINTF ((CHR1 *) (au1Temp + u1Index2), ",");
                u1Index2++;
            }
            if ((u1Index == ENT_MFG_TIME_SEP_INDEX) ||
                (u1Index == ENT_MFG_TIME_SEP_INDEX + 1) ||
                (u1Index == ENT_MFG_DAT_COLEN_INDEX))
            {
                SPRINTF ((CHR1 *) (au1Temp + u1Index2), ":");
                u1Index2++;
            }
            if (u1Index == ENT_MFG_TIME_MSEC_SEP_INDEX)
            {
                SPRINTF ((CHR1 *) (au1Temp + u1Index2), ".");
                u1Index2++;
            }

            ENT_GET_1BYTE (u1DecValue,
                           (UINT1 *) &(RetPhyTemp.pu1_OctetList[u1Index++]));
            if ((u1DecValue == ENT_MFG_DAT_PLUS_ASCII_VALUE) ||
                (u1DecValue == ENT_MFG_DAT_MINUS_ASCII_VALUE))
            {
                SPRINTF ((CHR1 *) (au1Temp + u1Index2), "%c", u1DecValue);
                u1DecValue = 0;
            }
            else
            {
                SPRINTF ((CHR1 *) (au1Temp + u1Index2), "%d", u1DecValue);
            }
            if (u1DecValue > (ENT_MFG_DAT_COMMA_INDEX + 1))
            {
                u1Index2 = u1Index2 + ENT_MFG_DAT_YEAR_INDEX;
            }
            else
            {
                u1Index2++;
            }
        }
        CliPrintf (CliHandle, "Physical MfgDate: %s\n\r", au1Temp);
    }
    else
    {
        CliPrintf (CliHandle, "Physical MfgDate: %s\n\r", "not available");
    }
    MEMSET (au1PhyTemp, 0, ISS_ENT_PHY_DESCR_LEN);
    MEMSET (&RetPhyTemp, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RetPhyTemp.pu1_OctetList = au1PhyTemp;
    RetPhyTemp.i4_Length = 0;

    if (nmhGetEntPhysicalUris (i4PhyIndex, &RetPhyTemp) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (RetPhyTemp.i4_Length != 0)
    {
        CliPrintf (CliHandle, "Physical Uris: %s\n\r",
                   RetPhyTemp.pu1_OctetList);
    }
    else
    {
        CliPrintf (CliHandle, "Physical Uris: %s\n\r", "not available");
    }

    if (nmhGetEntPhysicalIsFRU (i4PhyIndex, &i4FRUStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (i4FRUStatus == ENT_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "Physical FRU Status: True\n\r");
    }
    else
    {
        CliPrintf (CliHandle, "Physical FRU Status: False\n\r");
    }

    CliPrintf (CliHandle, "\n------------------------------------\n\n\r");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : EntCliShowAllPhyEntities                           */
/*                                                                           */
/*     DESCRIPTION      : This function displays all the physical entities 
 *                        and their information                              */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
EntCliShowAllPhyEntities (tCliHandle CliHandle)
{

    INT4                i4PhyIndex = 0;
    INT4                i4NextPhyIndex = 0;

    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, "%-15s%-15s%-15s%-32s",
               "Index", "Class", "ParentRelPos", "Physical Alias");
    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "%-15s%-15s%-15s%-32s",
               "-----", "-----", "------------", "--------------");

    CliPrintf (CliHandle, "\r\n\n");
    while (nmhGetNextIndexEntPhysicalTable (i4PhyIndex, &i4NextPhyIndex) !=
           SNMP_FAILURE)
    {
        EntPrintPhyEntity (CliHandle, i4NextPhyIndex);
        i4PhyIndex = i4NextPhyIndex;
    }

    CliPrintf (CliHandle, "\n\r");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : EntPrintPhyEntity                                  */
/*                                                                           */
/*     DESCRIPTION      : This function print the physical entities 
 *                        and their information                              */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
EntPrintPhyEntity (tCliHandle CliHandle, INT4 i4PhyIndex)
{
    tSNMP_OCTET_STRING_TYPE RetPhyAlias;
    INT4                i4PhyClass = 0;
    INT4                i4ParentRelPos = 0;
    UINT1               au1PhyAlias[ISS_ENT_PHY_ALIAS_LEN];

    MEMSET (au1PhyAlias, 0, ISS_ENT_PHY_ALIAS_LEN);
    MEMSET (&RetPhyAlias, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    RetPhyAlias.pu1_OctetList = au1PhyAlias;
    RetPhyAlias.i4_Length = 0;

    if (nmhValidateIndexInstanceEntPhysicalTable (i4PhyIndex) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetEntPhysicalClass (i4PhyIndex, &i4PhyClass) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetEntPhysicalParentRelPos (i4PhyIndex, &i4ParentRelPos) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetEntPhysicalAlias (i4PhyIndex, &RetPhyAlias) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "%-15d", i4PhyIndex);
    CliPrintf (CliHandle, "%-15s", gapu1PhyClass[i4PhyClass - 1]);
    CliPrintf (CliHandle, "%-15d", i4ParentRelPos);
    if (RetPhyAlias.i4_Length == 0)
    {
        CliPrintf (CliHandle, "%-32s \n\r", "not available");
    }
    else
    {
        CliPrintf (CliHandle, "%-32s \n\r", RetPhyAlias.pu1_OctetList);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : EntPrintLogEntity                                  */
/*                                                                           */
/*     DESCRIPTION      : This function print the logical entity 
 *                        and their information                              */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
EntPrintLogEntity (tCliHandle CliHandle, INT4 i4LogIndex)
{

    tSNMP_OCTET_STRING_TYPE *pRetLogDescr = NULL;
    tSNMP_OID_TYPE      RetLogType;
    UINT4               au4LogType[ENT_MAX_TYPE_LEN];

    MEMSET (au4LogType, 0, (sizeof (UINT4) * ENT_MAX_TYPE_LEN));
    MEMSET (&RetLogType, 0, sizeof (tSNMP_OID_TYPE));
    RetLogType.pu4_OidList = au4LogType;
    RetLogType.u4_Length = 0;

    if (nmhValidateIndexInstanceEntLogicalTable (i4LogIndex) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    pRetLogDescr = allocmem_octetstring (ENT_MAX_STR_LEN);
    if (pRetLogDescr == NULL)
    {
        CliPrintf (CliHandle, "%% Unable to allocate memory\r\n");
        return CLI_FAILURE;
    }

    pRetLogDescr->i4_Length = 0;
    if (nmhGetEntLogicalDescr (i4LogIndex, pRetLogDescr) == SNMP_FAILURE)
    {
        free_octetstring (pRetLogDescr);
        return CLI_FAILURE;
    }
    if (nmhGetEntLogicalType (i4LogIndex, &RetLogType) == SNMP_FAILURE)
    {
        free_octetstring (pRetLogDescr);
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "%-15d", i4LogIndex);

    CliPrintf (CliHandle, "%-35s", pRetLogDescr->pu1_OctetList);
    free_octetstring (pRetLogDescr);

    if (RetLogType.u4_Length == 0)
    {
        CliPrintf (CliHandle, "%-15s \n\r", "not available");
    }
    else
    {
        CliPrintf (CliHandle, "%-15s \n\r", RetLogType.pu4_OidList);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : EntCliShowAllLogEntities                           */
/*                                                                           */
/*     DESCRIPTION      : This function displays the logical entity 
 *                        information                                        */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
EntCliShowAllLogEntities (tCliHandle CliHandle)
{

    INT4                i4LogIndex = 0;
    INT4                i4NextLogIndex = 0;

    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, "%-15s%-35s%-15s", "Index", "Description", "Type");
    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "%-15s%-35s%-15s", "-----", "------------", "-----");

    CliPrintf (CliHandle, "\r\n\n");

    while (nmhGetNextIndexEntLogicalTable (i4LogIndex, &i4NextLogIndex) !=
           SNMP_FAILURE)
    {
        EntPrintLogEntity (CliHandle, i4NextLogIndex);
        i4LogIndex = i4NextLogIndex;
    }

    CliPrintf (CliHandle, "\n\r");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : EntCliConfigPhyEntity                              */
/*                                                                           */
/*     DESCRIPTION      : This function is used to configure the read-write 
 *                        physical component objects                         */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        u1Object - Object Identifier whose value needs to 
 *                        be set.                                            */
/*                        i4PhyIndex - Identifying the physical component    */
/*                        pu1ObjectName -value of teh object                 */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
EntCliConfigPhyEntity (tCliHandle CliHandle, UINT1 u1Object,
                       INT4 i4PhyIndex, UINT1 *pu1ObjectName)
{
    UINT4               u4ErrorCode = 0;

    tSNMP_OCTET_STRING_TYPE PhyObjectName;
    UINT1               au1PhyObjectName[ENT_MAX_STR_LEN];

    MEMSET (au1PhyObjectName, 0, ENT_MAX_STR_LEN);
    MEMSET (&PhyObjectName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    PhyObjectName.pu1_OctetList = au1PhyObjectName;
    PhyObjectName.i4_Length = 0;

    if (STRLEN (pu1ObjectName) > ENT_MAX_STR_LEN)
    {
        return CLI_FAILURE;
    }
    if (nmhValidateIndexInstanceEntPhysicalTable (i4PhyIndex) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    MEMCPY (PhyObjectName.pu1_OctetList, pu1ObjectName, STRLEN (pu1ObjectName));
    PhyObjectName.i4_Length = STRLEN (pu1ObjectName);

    if (u1Object == ISS_ENT_PHY_SERIAL_NUM)
    {
        if (nmhTestv2EntPhysicalSerialNum (&u4ErrorCode,
                                           i4PhyIndex, &PhyObjectName)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to update the Serial Number\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetEntPhysicalSerialNum (i4PhyIndex, &PhyObjectName) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to update the Serial Number\r\n");
            return CLI_FAILURE;
        }
    }
    else if (u1Object == ISS_ENT_PHY_ASSET_ID)
    {
        if (nmhTestv2EntPhysicalAssetID (&u4ErrorCode,
                                         i4PhyIndex,
                                         &PhyObjectName) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to update the AssetID\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetEntPhysicalAssetID (i4PhyIndex, &PhyObjectName) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to update the AssetID\r\n");
            return CLI_FAILURE;
        }
    }
    else if (u1Object == ISS_ENT_PHY_ALIAS_NAME)
    {
        if (nmhTestv2EntPhysicalAlias (&u4ErrorCode,
                                       i4PhyIndex,
                                       &PhyObjectName) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to update the Alias\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetEntPhysicalAlias (i4PhyIndex, &PhyObjectName) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to update the Alias\r\n");
            return CLI_FAILURE;
        }
    }
    else if (u1Object == ISS_ENT_PHY_URIS)
    {
        if (nmhTestv2EntPhysicalUris (&u4ErrorCode,
                                      i4PhyIndex,
                                      &PhyObjectName) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Wrong Length\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetEntPhysicalUris (i4PhyIndex, &PhyObjectName) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to update the Uris\r\n");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : EntCliResetPhyEntity                               */
/*                                                                           */
/*     DESCRIPTION      : This function is used to reset the read-write 
 *                        physical component objects                         */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        u1Object - Object Identifier whose value needs to 
 *                        be reset.                                          */
/*                        i4PhyIndex - Identifying the physical component    */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
EntCliResetPhyEntity (tCliHandle CliHandle, UINT1 u1Object, INT4 i4PhyIndex)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE PhyObjectName;
    UINT1               au1PhyObjectName[ENT_MAX_STR_LEN];
    UINT4               i4Return = SNMP_SUCCESS;

    MEMSET (au1PhyObjectName, 0, ENT_MAX_STR_LEN);
    MEMSET (&PhyObjectName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    PhyObjectName.pu1_OctetList = au1PhyObjectName;
    PhyObjectName.i4_Length = 0;

    if ((u1Object == ISS_ENT_PHY_URIS) || (u1Object == ENT_PHY_RESET_ALL))
    {
        i4Return = nmhTestv2EntPhysicalUris (&u4ErrorCode,
                                             i4PhyIndex, &PhyObjectName);
        if (i4Return == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to reset the Uris\r\n");
        }
        else
        {
            if (nmhSetEntPhysicalUris (i4PhyIndex, &PhyObjectName) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to reset the Uris\r\n");
            }
        }
    }

    if ((u1Object == ISS_ENT_PHY_SERIAL_NUM) || (u1Object == ENT_PHY_RESET_ALL))
    {
        i4Return = nmhTestv2EntPhysicalSerialNum (&u4ErrorCode,
                                                  i4PhyIndex, &PhyObjectName);
        if (i4Return == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to reset the SerialNum\r\n");
        }
        else
        {
            if (nmhSetEntPhysicalSerialNum (i4PhyIndex, &PhyObjectName) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to reset the SerialNum\r\n");
            }
        }
    }

    if ((u1Object == ISS_ENT_PHY_ASSET_ID) || (u1Object == ENT_PHY_RESET_ALL))
    {
        i4Return = nmhTestv2EntPhysicalAssetID (&u4ErrorCode,
                                                i4PhyIndex, &PhyObjectName);

        if (i4Return == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to reset the Asset Id\r\n");
        }
        else
        {
            if (nmhSetEntPhysicalAssetID (i4PhyIndex, &PhyObjectName) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to reset the Asset Id\r\n");
            }
        }
    }
    if ((u1Object == ISS_ENT_PHY_ALIAS_NAME) || (u1Object == ENT_PHY_RESET_ALL))
    {
        i4Return = nmhTestv2EntPhysicalAlias (&u4ErrorCode,
                                              i4PhyIndex, &PhyObjectName);
        if (i4Return == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to reset the Alias Name\r\n");
        }
        else
        {
            if (nmhSetEntPhysicalAlias (i4PhyIndex, &PhyObjectName) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to reset the Alias Name\r\n");
            }
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : EntShowRunningConfig                               */
/*                                                                           */
/*     DESCRIPTION      : This function displays the physical entity 
 *                        information                                        */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
EntShowRunningConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE RetPhySerialNum;
    tSNMP_OCTET_STRING_TYPE RetPhyAlias;
    tSNMP_OCTET_STRING_TYPE RetPhyAssetId;
    tSNMP_OCTET_STRING_TYPE RetPhyUris;
    INT4                i4PhyIndex = 0;
    INT4                i4NextPhyIndex = 0;
    INT4                i4FRUStatus = 0;
    UINT1               au1PhySerialNum[ISS_ENT_PHY_SER_NUM_LEN];
    UINT1               au1PhyAlias[ISS_ENT_PHY_ALIAS_LEN];
    UINT1               au1PhyAssetId[ISS_ENT_PHY_ASSET_ID_LEN];
    UINT1               au1PhyUris[ISS_ENT_PHY_URIS_LEN];

    while (nmhGetNextIndexEntPhysicalTable (i4PhyIndex, &i4NextPhyIndex) !=
           SNMP_FAILURE)
    {
        MEMSET (au1PhySerialNum, 0, ISS_ENT_PHY_SER_NUM_LEN);
        MEMSET (&RetPhySerialNum, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        RetPhySerialNum.pu1_OctetList = au1PhySerialNum;
        RetPhySerialNum.i4_Length = 0;

        MEMSET (au1PhyAlias, 0, ISS_ENT_PHY_ALIAS_LEN);
        MEMSET (&RetPhyAlias, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        RetPhyAlias.pu1_OctetList = au1PhyAlias;
        RetPhyAlias.i4_Length = 0;

        MEMSET (au1PhyAssetId, 0, ISS_ENT_PHY_ASSET_ID_LEN);
        MEMSET (&RetPhyAssetId, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        RetPhyAssetId.pu1_OctetList = au1PhyAssetId;
        RetPhyAssetId.i4_Length = 0;

        MEMSET (au1PhyUris, 0, ISS_ENT_PHY_URIS_LEN);
        MEMSET (&RetPhyUris, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        RetPhyUris.pu1_OctetList = au1PhyUris;
        RetPhyUris.i4_Length = 0;

        if (i4NextPhyIndex > ENT_MAX_PHY_ENTITY)
        {
            return CLI_SUCCESS;
        }

        if (nmhGetEntPhysicalSerialNum (i4NextPhyIndex, &RetPhySerialNum) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhGetEntPhysicalAlias (i4NextPhyIndex, &RetPhyAlias) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhGetEntPhysicalAssetID (i4NextPhyIndex, &RetPhyAssetId) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhGetEntPhysicalUris (i4NextPhyIndex, &RetPhyUris) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhGetEntPhysicalIsFRU (i4NextPhyIndex, &i4FRUStatus) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (i4NextPhyIndex == 1)
        {
            if ((RetPhySerialNum.i4_Length != 0) ||
                MEMCMP (RetPhyAlias.pu1_OctetList, ISS_DEFAULT_ALIAS_NAME,
                        STRLEN (ISS_DEFAULT_ALIAS_NAME) != 0) ||
                MEMCMP (RetPhyAssetId.pu1_OctetList, ISS_DEFAULT_ASSET_ID,
                        STRLEN (ISS_DEFAULT_ASSET_ID) != 0) ||
                (RetPhyUris.i4_Length != 0))
            {
                CliPrintf (CliHandle, "!\r\n");
                CliPrintf (CliHandle, "\n\n\rset entity physical-index %d ",
                           i4NextPhyIndex);

                if (MEMCMP (RetPhyAssetId.pu1_OctetList, ISS_DEFAULT_ASSET_ID,
                            STRLEN (ISS_DEFAULT_ASSET_ID) != 0))
                {
                    CliPrintf (CliHandle, "asset-id %s ",
                               RetPhyAssetId.pu1_OctetList);
                }
                if (RetPhySerialNum.i4_Length != 0)
                {
                    CliPrintf (CliHandle, "serial-number %s ",
                               RetPhySerialNum.pu1_OctetList);
                }

                if (MEMCMP (RetPhyAlias.pu1_OctetList, ISS_DEFAULT_ALIAS_NAME,
                            STRLEN (ISS_DEFAULT_ALIAS_NAME) != 0))
                {
                    CliPrintf (CliHandle, "alias-name %s ",
                               RetPhyAlias.pu1_OctetList);
                }
                if (RetPhyUris.i4_Length != 0)
                {
                    CliPrintf (CliHandle, "uris %s ", RetPhyUris.pu1_OctetList);
                }
            }
        }
        else
        {

            if ((RetPhySerialNum.i4_Length != 0) || (RetPhyAlias.i4_Length != 0)
                || (RetPhyAssetId.i4_Length != 0)
                || (RetPhyUris.i4_Length != 0))
            {

                if (i4FRUStatus == ENT_SNMP_FALSE)
                {
                    if (RetPhyUris.i4_Length != 0)
                    {
                        CliPrintf (CliHandle,
                                   "\n\n\rset entity physical-index %d ",
                                   i4NextPhyIndex);
                        CliPrintf (CliHandle, "uris %s ",
                                   RetPhyUris.pu1_OctetList);
                    }
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\n\n\rset entity physical-index %d ",
                               i4NextPhyIndex);

                    if (RetPhyAssetId.i4_Length != 0)
                    {
                        CliPrintf (CliHandle, "asset-id %s ",
                                   RetPhyAssetId.pu1_OctetList);
                    }

                    if (RetPhySerialNum.i4_Length != 0)
                    {
                        CliPrintf (CliHandle, "serial-number %s ",
                                   RetPhySerialNum.pu1_OctetList);
                    }

                    if (RetPhyAlias.i4_Length != 0)
                    {
                        CliPrintf (CliHandle, "alias-name %s ",
                                   RetPhyAlias.pu1_OctetList);
                    }
                    if (RetPhyUris.i4_Length != 0)
                    {
                        CliPrintf (CliHandle, "uris %s ",
                                   RetPhyUris.pu1_OctetList);
                    }
                }
            }
        }
        i4PhyIndex = i4NextPhyIndex;
    }

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

#endif /*  */
