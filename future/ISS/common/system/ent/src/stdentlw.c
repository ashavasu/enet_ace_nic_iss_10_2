/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdentlw.c,v 1.20 2015/03/06 10:23:55 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "entinc.h"

extern UINT4        gau4EntSysLogId;
static tIssEntPhyInfo PhyInfo;

/* LOW LEVEL Routines for Table : EntPhysicalTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceEntPhysicalTable
 Input       :  The Indices
                EntPhysicalIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceEntPhysicalTable (INT4 i4EntPhysicalIndex)
{
    UINT4               u4MaxPhyIndex = ENT_MAX_PHY_ENTITY;

    if ((i4EntPhysicalIndex < ENT_MIN_PHYICAL_INDEX_VAL) ||
        (i4EntPhysicalIndex > (INT4) u4MaxPhyIndex))
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical Index Validation Failed"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexEntPhysicalTable
 Input       :  The Indices
                EntPhysicalIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexEntPhysicalTable (INT4 *pi4EntPhysicalIndex)
{
    return (nmhGetNextIndexEntPhysicalTable (0, pi4EntPhysicalIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexEntPhysicalTable
 Input       :  The Indices
                EntPhysicalIndex
                nextEntPhysicalIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexEntPhysicalTable (INT4 i4EntPhysicalIndex,
                                 INT4 *pi4NextEntPhysicalIndex)
{
    if (i4EntPhysicalIndex < ENT_MAX_PHY_ENTITY)
    {
        *pi4NextEntPhysicalIndex = i4EntPhysicalIndex + 1;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetEntPhysicalDescr
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                retValEntPhysicalDescr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntPhysicalDescr (INT4 i4EntPhysicalIndex,
                        tSNMP_OCTET_STRING_TYPE * pRetValEntPhysicalDescr)
{
    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));
    if (EntUtilGetPhysicalInfo (i4EntPhysicalIndex, &PhyInfo) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (STRLEN (PhyInfo.au1Descr) != 0)
    {
        MEMCPY (pRetValEntPhysicalDescr->pu1_OctetList, PhyInfo.au1Descr,
                STRLEN (PhyInfo.au1Descr));
        pRetValEntPhysicalDescr->i4_Length = STRLEN (PhyInfo.au1Descr);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetEntPhysicalVendorType
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                retValEntPhysicalVendorType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntPhysicalVendorType (INT4 i4EntPhysicalIndex,
                             tSNMP_OID_TYPE * pRetValEntPhysicalVendorType)
{

    /* Null Oid */
    UINT4               au4NullOid[] = { 0, 0 };

    UNUSED_PARAM (i4EntPhysicalIndex);

    MEMCPY (pRetValEntPhysicalVendorType->pu4_OidList, au4NullOid,
            sizeof (au4NullOid));
    pRetValEntPhysicalVendorType->u4_Length =
        sizeof (au4NullOid) / sizeof (UINT4);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetEntPhysicalContainedIn
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                retValEntPhysicalContainedIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntPhysicalContainedIn (INT4 i4EntPhysicalIndex,
                              INT4 *pi4RetValEntPhysicalContainedIn)
{
    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));

    if (EntUtilGetPhysicalInfo (i4EntPhysicalIndex, &PhyInfo) != OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical Contained In Get Operation Failed"));
        return SNMP_FAILURE;
    }

    *pi4RetValEntPhysicalContainedIn = PhyInfo.i4ContainedIn;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetEntPhysicalClass
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                retValEntPhysicalClass
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntPhysicalClass (INT4 i4EntPhysicalIndex,
                        INT4 *pi4RetValEntPhysicalClass)
{
    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));

    if (EntUtilGetPhysicalInfo (i4EntPhysicalIndex, &PhyInfo) != OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical Class Get Operation Failed"));
        return SNMP_FAILURE;
    }

    *pi4RetValEntPhysicalClass = (INT4) PhyInfo.i1PhyClass;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetEntPhysicalParentRelPos
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                retValEntPhysicalParentRelPos
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntPhysicalParentRelPos (INT4 i4EntPhysicalIndex,
                               INT4 *pi4RetValEntPhysicalParentRelPos)
{

    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));

    if (EntUtilGetPhysicalInfo (i4EntPhysicalIndex, &PhyInfo) != OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical Parent Relative Position Get"
                      "Operation Failed"));
        return SNMP_FAILURE;
    }

    *pi4RetValEntPhysicalParentRelPos = PhyInfo.i4ParentRelPos;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetEntPhysicalName
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                retValEntPhysicalName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntPhysicalName (INT4 i4EntPhysicalIndex,
                       tSNMP_OCTET_STRING_TYPE * pRetValEntPhysicalName)
{

    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));

    if (EntUtilGetPhysicalInfo (i4EntPhysicalIndex, &PhyInfo) != OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical Name Get Operation Failed"));
        return SNMP_FAILURE;
    }

    if (STRLEN (PhyInfo.au1Name) != 0)
    {
        MEMCPY (pRetValEntPhysicalName->pu1_OctetList, PhyInfo.au1Name,
                STRLEN (PhyInfo.au1Name));
        pRetValEntPhysicalName->i4_Length = STRLEN (PhyInfo.au1Name);
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetEntPhysicalHardwareRev
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                retValEntPhysicalHardwareRev
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntPhysicalHardwareRev (INT4 i4EntPhysicalIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValEntPhysicalHardwareRev)
{
    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));

    if (EntUtilGetPhysicalInfo (i4EntPhysicalIndex, &PhyInfo) != OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical Hardware Revision Get Operation Failed"));
        return SNMP_FAILURE;
    }

    if (STRLEN (PhyInfo.au1HardwareRev) != 0)
    {
        MEMCPY (pRetValEntPhysicalHardwareRev->pu1_OctetList,
                PhyInfo.au1HardwareRev, STRLEN (PhyInfo.au1HardwareRev));
        pRetValEntPhysicalHardwareRev->i4_Length =
            STRLEN (PhyInfo.au1HardwareRev);
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetEntPhysicalFirmwareRev
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                retValEntPhysicalFirmwareRev
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntPhysicalFirmwareRev (INT4 i4EntPhysicalIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValEntPhysicalFirmwareRev)
{
    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));

    if (EntUtilGetPhysicalInfo (i4EntPhysicalIndex, &PhyInfo) != OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical Firmware Revision Get Operation Failed"));
        return SNMP_FAILURE;
    }

    if (STRLEN (PhyInfo.au1FirmwareRev) != 0)
    {
        MEMCPY (pRetValEntPhysicalFirmwareRev->pu1_OctetList,
                PhyInfo.au1FirmwareRev, STRLEN (PhyInfo.au1FirmwareRev));
        pRetValEntPhysicalFirmwareRev->i4_Length =
            STRLEN (PhyInfo.au1FirmwareRev);
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetEntPhysicalSoftwareRev
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                retValEntPhysicalSoftwareRev
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntPhysicalSoftwareRev (INT4 i4EntPhysicalIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValEntPhysicalSoftwareRev)
{
    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));

    if (EntUtilGetPhysicalInfo (i4EntPhysicalIndex, &PhyInfo) != OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical Software Revision Get Operation Failed"));
        return SNMP_FAILURE;
    }

    if (STRLEN (PhyInfo.au1SoftwareRev) != 0)
    {
        MEMCPY (pRetValEntPhysicalSoftwareRev->pu1_OctetList,
                PhyInfo.au1SoftwareRev, STRLEN (PhyInfo.au1SoftwareRev));
        pRetValEntPhysicalSoftwareRev->i4_Length =
            STRLEN (PhyInfo.au1SoftwareRev);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetEntPhysicalSerialNum
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                retValEntPhysicalSerialNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntPhysicalSerialNum (INT4 i4EntPhysicalIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValEntPhysicalSerialNum)
{
    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));

    if (EntUtilGetPhysicalInfo (i4EntPhysicalIndex, &PhyInfo) != OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical Serial Number Get Operation Failed"));
        return SNMP_FAILURE;
    }

    if (STRLEN (PhyInfo.au1SerialNum) != 0)
    {
        MEMCPY (pRetValEntPhysicalSerialNum->pu1_OctetList,
                PhyInfo.au1SerialNum, STRLEN (PhyInfo.au1SerialNum));
        pRetValEntPhysicalSerialNum->i4_Length = STRLEN (PhyInfo.au1SerialNum);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetEntPhysicalMfgName
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                retValEntPhysicalMfgName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntPhysicalMfgName (INT4 i4EntPhysicalIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValEntPhysicalMfgName)
{
    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));

    if (EntUtilGetPhysicalInfo (i4EntPhysicalIndex, &PhyInfo) != OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical component Manufacturer's Name Get"
                      "Operation Failed"));
        return SNMP_FAILURE;
    }

    if (STRLEN (PhyInfo.au1MfgName) != 0)
    {
        MEMCPY (pRetValEntPhysicalMfgName->pu1_OctetList, PhyInfo.au1MfgName,
                STRLEN (PhyInfo.au1MfgName));
        pRetValEntPhysicalMfgName->i4_Length = STRLEN (PhyInfo.au1MfgName);
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetEntPhysicalModelName
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                retValEntPhysicalModelName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntPhysicalModelName (INT4 i4EntPhysicalIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValEntPhysicalModelName)
{
    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));

    if (EntUtilGetPhysicalInfo (i4EntPhysicalIndex, &PhyInfo) != OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical component's Model Name Get Operation Failed"));
        return SNMP_FAILURE;
    }

    if (STRLEN (PhyInfo.au1ModelName) != 0)
    {
        MEMCPY (pRetValEntPhysicalModelName->pu1_OctetList,
                PhyInfo.au1ModelName, STRLEN (PhyInfo.au1ModelName));
        pRetValEntPhysicalModelName->i4_Length = STRLEN (PhyInfo.au1ModelName);
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetEntPhysicalAlias
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                retValEntPhysicalAlias
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntPhysicalAlias (INT4 i4EntPhysicalIndex,
                        tSNMP_OCTET_STRING_TYPE * pRetValEntPhysicalAlias)
{
    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));

    if (EntUtilGetPhysicalInfo (i4EntPhysicalIndex, &PhyInfo) != OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical component's Alias Name Get Operation Failed"));
        return SNMP_FAILURE;
    }

    if (STRLEN (PhyInfo.au1Alias) != 0)
    {
        MEMCPY (pRetValEntPhysicalAlias->pu1_OctetList, PhyInfo.au1Alias,
                STRLEN (PhyInfo.au1Alias));
        pRetValEntPhysicalAlias->i4_Length = STRLEN (PhyInfo.au1Alias);
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetEntPhysicalAssetID
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                retValEntPhysicalAssetID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntPhysicalAssetID (INT4 i4EntPhysicalIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValEntPhysicalAssetID)
{

    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));

    if (EntUtilGetPhysicalInfo (i4EntPhysicalIndex, &PhyInfo) != OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical component's Asset Id Get Operation Failed"));
        return SNMP_FAILURE;
    }

    if (STRLEN (PhyInfo.au1AssetId) != 0)
    {
        MEMCPY (pRetValEntPhysicalAssetID->pu1_OctetList, PhyInfo.au1AssetId,
                STRLEN (PhyInfo.au1AssetId));
        pRetValEntPhysicalAssetID->i4_Length = STRLEN (PhyInfo.au1AssetId);
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetEntPhysicalIsFRU
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                retValEntPhysicalIsFRU
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntPhysicalIsFRU (INT4 i4EntPhysicalIndex,
                        INT4 *pi4RetValEntPhysicalIsFRU)
{
    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));

    if (EntUtilGetPhysicalInfo (i4EntPhysicalIndex, &PhyInfo) != OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical component's FRU status Get Operation Failed"));
        return SNMP_FAILURE;
    }
    if (PhyInfo.b1IsFRU == OSIX_TRUE)
    {
        *pi4RetValEntPhysicalIsFRU = ENT_SNMP_TRUE;
    }
    else
    {

        *pi4RetValEntPhysicalIsFRU = ENT_SNMP_FALSE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetEntPhysicalMfgDate
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                retValEntPhysicalMfgDate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntPhysicalMfgDate (INT4 i4EntPhysicalIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValEntPhysicalMfgDate)
{
    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));

    if (EntUtilGetPhysicalInfo (i4EntPhysicalIndex, &PhyInfo) != OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical component's Mfg Date Get Operation Failed"));
        return SNMP_FAILURE;
    }

    if (STRLEN (PhyInfo.au1MfgDate) != 0)
    {
        MEMCPY (pRetValEntPhysicalMfgDate->pu1_OctetList, PhyInfo.au1MfgDate,
                STRLEN (PhyInfo.au1MfgDate));
        pRetValEntPhysicalMfgDate->i4_Length = STRLEN (PhyInfo.au1MfgDate);
    }
    else
    {
        MEMCPY (pRetValEntPhysicalMfgDate->pu1_OctetList,
                ENT_DEFAULT_PHY_MFG_DATE, ENT_DEFAULT_PHY_MFG_DATE_LEN);
        pRetValEntPhysicalMfgDate->i4_Length = ENT_DEFAULT_PHY_MFG_DATE_LEN;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetEntPhysicalUris
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                retValEntPhysicalUris
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntPhysicalUris (INT4 i4EntPhysicalIndex,
                       tSNMP_OCTET_STRING_TYPE * pRetValEntPhysicalUris)
{
    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));

    if (EntUtilGetPhysicalInfo (i4EntPhysicalIndex, &PhyInfo) != OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical component's Uris Name Get Operation Failed"));
        return SNMP_FAILURE;
    }

    if (STRLEN (PhyInfo.au1Uris) != 0)
    {
        MEMCPY (pRetValEntPhysicalUris->pu1_OctetList, PhyInfo.au1Uris,
                STRLEN (PhyInfo.au1Uris));
        pRetValEntPhysicalUris->i4_Length = STRLEN (PhyInfo.au1Uris);
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetEntPhysicalSerialNum
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                setValEntPhysicalSerialNum
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetEntPhysicalSerialNum (INT4 i4EntPhysicalIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValEntPhysicalSerialNum)
{
    UINT1               au1SerialNum[ISS_ENT_PHY_SER_NUM_LEN];
    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));
    MEMSET (au1SerialNum, 0, ISS_ENT_PHY_SER_NUM_LEN);

    if (EntUtilGetPhysicalInfo (i4EntPhysicalIndex, &PhyInfo) != OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical component's Serial Num Set Operation Failed"));
        return SNMP_FAILURE;
    }

    if (PhyInfo.b1IsFRU == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (au1SerialNum, pSetValEntPhysicalSerialNum->pu1_OctetList,
            pSetValEntPhysicalSerialNum->i4_Length);

    au1SerialNum[pSetValEntPhysicalSerialNum->i4_Length] = '\0';

    if (EntUtilSetPhyInfo (i4EntPhysicalIndex,
                           au1SerialNum,
                           ISS_ENT_PHY_SERIAL_NUM) != OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical component's Serial Number Set Operation Failed"));
        return SNMP_FAILURE;
    }
    EntSnmpUpdateLastTimeChange ();

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetEntPhysicalAlias
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                setValEntPhysicalAlias
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetEntPhysicalAlias (INT4 i4EntPhysicalIndex,
                        tSNMP_OCTET_STRING_TYPE * pSetValEntPhysicalAlias)
{

    UINT1               au1Alias[ISS_ENT_PHY_ALIAS_LEN];
    MEMSET (au1Alias, 0, ISS_ENT_PHY_ALIAS_LEN);
    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));

    if (EntUtilGetPhysicalInfo (i4EntPhysicalIndex, &PhyInfo) != OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical component's Alias Name Set Operation Failed"));
        return SNMP_FAILURE;
    }

    if (PhyInfo.i1PhyClass == ENT_PHY_CLASS_PORT)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (au1Alias, pSetValEntPhysicalAlias->pu1_OctetList,
            pSetValEntPhysicalAlias->i4_Length);

    au1Alias[pSetValEntPhysicalAlias->i4_Length] = '\0';

    if (EntUtilSetPhyInfo (i4EntPhysicalIndex,
                           au1Alias, ISS_ENT_PHY_ALIAS_NAME) != OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical component's Alias Name Set Operation Failed"));
        return SNMP_FAILURE;
    }
    EntSnmpUpdateLastTimeChange ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetEntPhysicalAssetID
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                setValEntPhysicalAssetID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetEntPhysicalAssetID (INT4 i4EntPhysicalIndex,
                          tSNMP_OCTET_STRING_TYPE * pSetValEntPhysicalAssetID)
{

    UINT1               au1AssetID[ISS_ENT_PHY_ASSET_ID_LEN];
    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));
    MEMSET (au1AssetID, 0, ISS_ENT_PHY_ASSET_ID_LEN);
    MEMCPY (au1AssetID, pSetValEntPhysicalAssetID->pu1_OctetList,
            pSetValEntPhysicalAssetID->i4_Length);

    au1AssetID[pSetValEntPhysicalAssetID->i4_Length] = '\0';

    if (EntUtilGetPhysicalInfo (i4EntPhysicalIndex, &PhyInfo) != OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical component's Asset Id Set Operation Failed"));
        return SNMP_FAILURE;
    }

    if (PhyInfo.b1IsFRU == OSIX_FALSE)
    {
        return SNMP_FAILURE;
    }

    if (EntUtilSetPhyInfo (i4EntPhysicalIndex,
                           au1AssetID, ISS_ENT_PHY_ASSET_ID) != OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical component's Asset Id Set Operation Failed"));
        return SNMP_FAILURE;
    }
    EntSnmpUpdateLastTimeChange ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetEntPhysicalUris
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                setValEntPhysicalUris
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetEntPhysicalUris (INT4 i4EntPhysicalIndex,
                       tSNMP_OCTET_STRING_TYPE * pSetValEntPhysicalUris)
{

    UINT1               au1Uris[ISS_ENT_PHY_URIS_LEN];
    MEMSET (au1Uris, 0, ISS_ENT_PHY_URIS_LEN);

    MEMCPY (au1Uris, pSetValEntPhysicalUris->pu1_OctetList,
            pSetValEntPhysicalUris->i4_Length);

    au1Uris[pSetValEntPhysicalUris->i4_Length] = '\0';

    if (EntUtilSetPhyInfo (i4EntPhysicalIndex,
                           au1Uris, ISS_ENT_PHY_URIS) != OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical component's Uris Id Set Operation Failed"));
        return SNMP_FAILURE;
    }
    EntSnmpUpdateLastTimeChange ();
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2EntPhysicalSerialNum
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                testValEntPhysicalSerialNum
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2EntPhysicalSerialNum (UINT4 *pu4ErrorCode, INT4 i4EntPhysicalIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValEntPhysicalSerialNum)
{

    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));

    if ((i4EntPhysicalIndex < ENT_MIN_PHYICAL_INDEX_VAL) ||
        (i4EntPhysicalIndex > (INT4) ENT_MAX_PHY_ENTITY))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (EntUtilGetPhysicalInfo (i4EntPhysicalIndex, &PhyInfo) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (PhyInfo.b1IsFRU == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pTestValEntPhysicalSerialNum->i4_Length >= ISS_ENT_PHY_SER_NUM_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2EntPhysicalAlias
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                testValEntPhysicalAlias
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2EntPhysicalAlias (UINT4 *pu4ErrorCode, INT4 i4EntPhysicalIndex,
                           tSNMP_OCTET_STRING_TYPE * pTestValEntPhysicalAlias)
{
    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));

    if ((i4EntPhysicalIndex < ENT_MIN_PHYICAL_INDEX_VAL) ||
        (i4EntPhysicalIndex > (INT4) ENT_MAX_PHY_ENTITY))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (EntUtilGetPhysicalInfo (i4EntPhysicalIndex, &PhyInfo) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (PhyInfo.i1PhyClass == ENT_PHY_CLASS_PORT)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pTestValEntPhysicalAlias->i4_Length >= ISS_ENT_PHY_ALIAS_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2EntPhysicalAssetID
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                testValEntPhysicalAssetID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2EntPhysicalAssetID (UINT4 *pu4ErrorCode, INT4 i4EntPhysicalIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValEntPhysicalAssetID)
{
    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));

    if ((i4EntPhysicalIndex < ENT_MIN_PHYICAL_INDEX_VAL) ||
        (i4EntPhysicalIndex > (INT4) ENT_MAX_PHY_ENTITY))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (EntUtilGetPhysicalInfo (i4EntPhysicalIndex, &PhyInfo) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (PhyInfo.b1IsFRU == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pTestValEntPhysicalAssetID->i4_Length >= ISS_ENT_PHY_ASSET_ID_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2EntPhysicalUris
 Input       :  The Indices
                EntPhysicalIndex

                The Object 
                testValEntPhysicalUris
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2EntPhysicalUris (UINT4 *pu4ErrorCode, INT4 i4EntPhysicalIndex,
                          tSNMP_OCTET_STRING_TYPE * pTestValEntPhysicalUris)
{

    if ((i4EntPhysicalIndex < ENT_MIN_PHYICAL_INDEX_VAL) ||
        (i4EntPhysicalIndex > (INT4) ENT_MAX_PHY_ENTITY))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pTestValEntPhysicalUris->i4_Length >= ISS_ENT_PHY_URIS_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2EntPhysicalTable
 Input       :  The Indices
                EntPhysicalIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2EntPhysicalTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : EntLogicalTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceEntLogicalTable
 Input       :  The Indices
                EntLogicalIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceEntLogicalTable (INT4 i4EntLogicalIndex)
{
    UINT4               u4MaxLogIndex = EntUtilMaxLogIndex ();

    if ((i4EntLogicalIndex < ENT_MIN_LOGICAL_INDEX_VAL) ||
        (i4EntLogicalIndex > (INT4) u4MaxLogIndex))
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Logical Index Validation Failed"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexEntLogicalTable
 Input       :  The Indices
                EntLogicalIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexEntLogicalTable (INT4 *pi4EntLogicalIndex)
{
    return (nmhGetNextIndexEntLogicalTable (0, pi4EntLogicalIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexEntLogicalTable
 Input       :  The Indices
                EntLogicalIndex
                nextEntLogicalIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexEntLogicalTable (INT4 i4EntLogicalIndex,
                                INT4 *pi4NextEntLogicalIndex)
{
    if (i4EntLogicalIndex < ENT_MAX_LOG_ENTITY)
    {
        *pi4NextEntLogicalIndex = i4EntLogicalIndex + 1;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetEntLogicalDescr
 Input       :  The Indices
                EntLogicalIndex

                The Object 
                retValEntLogicalDescr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntLogicalDescr (INT4 i4EntLogicalIndex,
                       tSNMP_OCTET_STRING_TYPE * pRetValEntLogicalDescr)
{
    UNUSED_PARAM (i4EntLogicalIndex);

    EntSnmpGetSysName (pRetValEntLogicalDescr->pu1_OctetList);

    pRetValEntLogicalDescr->i4_Length =
        STRLEN (pRetValEntLogicalDescr->pu1_OctetList);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetEntLogicalType
 Input       :  The Indices
                EntLogicalIndex

                The Object 
                retValEntLogicalType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntLogicalType (INT4 i4EntLogicalIndex,
                      tSNMP_OID_TYPE * pRetValEntLogicalType)
{
    /* Null Oid */
    UINT4               au4NullOid[] = { 0, 0 };

    UNUSED_PARAM (i4EntLogicalIndex);

    MEMCPY (pRetValEntLogicalType->pu4_OidList, au4NullOid,
            sizeof (au4NullOid));
    pRetValEntLogicalType->u4_Length = sizeof (au4NullOid) / sizeof (UINT4);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetEntLogicalCommunity
 Input       :  The Indices
                EntLogicalIndex

                The Object 
                retValEntLogicalCommunity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntLogicalCommunity (INT4 i4EntLogicalIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValEntLogicalCommunity)
{
    UINT4               u4ContextId = 0;

    EntUtilGetCntxtIdForLI (i4EntLogicalIndex, &u4ContextId);

    if (EntVcmGetAliasName (u4ContextId,
                            pRetValEntLogicalCommunity->pu1_OctetList)
        == OSIX_FAILURE)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Logical Component's Community Get Operation Failed"));
        return SNMP_FAILURE;
    }
    pRetValEntLogicalCommunity->i4_Length =
        STRLEN (pRetValEntLogicalCommunity->pu1_OctetList);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetEntLogicalTAddress
 Input       :  The Indices
                EntLogicalIndex

                The Object 
                retValEntLogicalTAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntLogicalTAddress (INT4 i4EntLogicalIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValEntLogicalTAddress)
{
    tIpConfigInfo       IpIfInfo;
    UINT4               u4Port = 0;

    UNUSED_PARAM (i4EntLogicalIndex);

    MEMSET ((UINT1 *) &IpIfInfo, 0, sizeof (tIpConfigInfo));

    if (EntCfaGetFirstIpActivePort (&u4Port) == CFA_SUCCESS)
    {
        if (EntCfaIpIfGetIfInfo (u4Port, &IpIfInfo) == CFA_SUCCESS)
        {
            PTR_ASSIGN4 (pRetValEntLogicalTAddress->pu1_OctetList, IpIfInfo.u4Addr);
            pRetValEntLogicalTAddress->i4_Length = sizeof (UINT4);
            PTR_ASSIGN2 (&(pRetValEntLogicalTAddress->pu1_OctetList[4]),
                    (UINT2) ENT_SNMP_PORT);
            pRetValEntLogicalTAddress->i4_Length = sizeof (UINT4) + sizeof (UINT2);
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetEntLogicalTDomain
 Input       :  The Indices
                EntLogicalIndex

                The Object 
                retValEntLogicalTDomain
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntLogicalTDomain (INT4 i4EntLogicalIndex,
                         tSNMP_OID_TYPE * pRetValEntLogicalTDomain)
{
    UNUSED_PARAM (i4EntLogicalIndex);

    EntSnmpGetTransportDomain (pRetValEntLogicalTDomain->pu4_OidList);
    pRetValEntLogicalTDomain->u4_Length =
        STRLEN (pRetValEntLogicalTDomain->pu4_OidList);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetEntLogicalContextEngineID
 Input       :  The Indices
                EntLogicalIndex

                The Object 
                retValEntLogicalContextEngineID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntLogicalContextEngineID (INT4 i4EntLogicalIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValEntLogicalContextEngineID)
{
    UNUSED_PARAM (i4EntLogicalIndex);

    EntSnmpGetContextEngineId (pRetValEntLogicalContextEngineID);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetEntLogicalContextName
 Input       :  The Indices
                EntLogicalIndex

                The Object 
                retValEntLogicalContextName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntLogicalContextName (INT4 i4EntLogicalIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValEntLogicalContextName)
{
    UINT4               u4ContextId = 0;

    EntUtilGetCntxtIdForLI (i4EntLogicalIndex, &u4ContextId);

    if (EntVcmGetAliasName (u4ContextId,
                            pRetValEntLogicalContextName->pu1_OctetList)
        == OSIX_FAILURE)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Logical Component's Context Name Get"
                      "Operation Failed"));
        return SNMP_FAILURE;
    }
    pRetValEntLogicalContextName->i4_Length =
        STRLEN (pRetValEntLogicalContextName->pu1_OctetList);

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : EntLPMappingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceEntLPMappingTable
 Input       :  The Indices
                EntLogicalIndex
                EntLPPhysicalIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceEntLPMappingTable (INT4 i4EntLogicalIndex,
                                           INT4 i4EntLPPhysicalIndex)
{
    if ((i4EntLPPhysicalIndex < ENT_MIN_PHYICAL_INDEX_VAL) ||
        (i4EntLPPhysicalIndex > ENT_MAX_PHY_ENTITY) ||
        (i4EntLogicalIndex < ENT_MIN_LOGICAL_INDEX_VAL) ||
        (i4EntLogicalIndex > ENT_MAX_LOG_ENTITY))
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "LP Mapping Validation Failed"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexEntLPMappingTable
 Input       :  The Indices
                EntLogicalIndex
                EntLPPhysicalIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexEntLPMappingTable (INT4 *pi4EntLogicalIndex,
                                   INT4 *pi4EntLPPhysicalIndex)
{
    return (nmhGetNextIndexEntLPMappingTable
            (0, pi4EntLogicalIndex, 0, pi4EntLPPhysicalIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexEntLPMappingTable
 Input       :  The Indices
                EntLogicalIndex
                nextEntLogicalIndex
                EntLPPhysicalIndex
                nextEntLPPhysicalIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexEntLPMappingTable (INT4 i4EntLogicalIndex,
                                  INT4 *pi4NextEntLogicalIndex,
                                  INT4 i4EntLPPhysicalIndex,
                                  INT4 *pi4NextEntLPPhysicalIndex)
{

    UINT4               u4ContextId = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4NextIfIndex = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);

    if (i4EntLogicalIndex == 0)
    {
        if (EntUtilGetNextLogIndex (i4EntLogicalIndex,
                                    pi4NextEntLogicalIndex) == OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }
        i4EntLogicalIndex = *pi4NextEntLogicalIndex;
        EntUtilGetCntxtIdForLI (i4EntLogicalIndex, &u4ContextId);
        while (EntUtilFirstIfInfoForCntxt (u4ContextId, au1IfName, &u4IfIndex)
               == OSIX_FAILURE)
        {
            if (EntUtilGetNextLogIndex (i4EntLogicalIndex,
                                        pi4NextEntLogicalIndex) == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }

            i4EntLogicalIndex = *pi4NextEntLogicalIndex;
            EntUtilGetCntxtIdForLI (i4EntLogicalIndex, &u4ContextId);
        }

        if (EntCfaCliGetIfName (u4IfIndex, (INT1 *) au1IfName) == OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }

        if (EntUtilGetPhyIndexForIfName (au1IfName, pi4NextEntLPPhysicalIndex)
            == OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }

        i4EntLPPhysicalIndex = *pi4NextEntLPPhysicalIndex;
        return SNMP_SUCCESS;
    }
    else
    {
        if (EntUtilGetNextLogIndex (i4EntLogicalIndex,
                                    pi4NextEntLogicalIndex) == OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }
        EntUtilGetCntxtIdForLI (i4EntLogicalIndex, &u4ContextId);
        EntUtilGetIfIndexForPhyIndex (i4EntLPPhysicalIndex, &u4IfIndex);

        if (EntUtilGetNextIfIndex (u4IfIndex, u4ContextId, &u4NextIfIndex) ==
            OSIX_SUCCESS)
        {
            if (EntCfaCliGetIfName (u4NextIfIndex, (INT1 *) au1IfName) ==
                OSIX_FAILURE)
            {
                return OSIX_FAILURE;
            }

            if (EntUtilGetPhyIndexForIfName
                (au1IfName, pi4NextEntLPPhysicalIndex) == OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }

            i4EntLPPhysicalIndex = *pi4NextEntLPPhysicalIndex;
            return SNMP_SUCCESS;
        }
        else
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/* LOW LEVEL Routines for Table : EntAliasMappingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceEntAliasMappingTable
 Input       :  The Indices
                EntPhysicalIndex
                EntAliasLogicalIndexOrZero
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceEntAliasMappingTable (INT4 i4EntPhysicalIndex,
                                              INT4 i4EntAliasLogicalIndexOrZero)
{
    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));

    if ((i4EntPhysicalIndex < ENT_MIN_PHYICAL_INDEX_VAL) ||
        (i4EntPhysicalIndex > ENT_MAX_PHY_ENTITY) ||
        (i4EntAliasLogicalIndexOrZero > 0))
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Alias Mapping Validation Failed"));
        return SNMP_FAILURE;
    }

    if (EntUtilGetPhysicalInfo (i4EntPhysicalIndex, &PhyInfo) != OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Alias Mapping Validation Failed"));
        return SNMP_FAILURE;
    }

    /*If the Physical class is not Port than
     * Physical Index is invalid  as Alias mapping is present 
     * Interfaces only*/
    if (PhyInfo.i1PhyClass != ENT_PHY_CLASS_PORT)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Alias Mapping Validation Failed"));
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexEntAliasMappingTable
 Input       :  The Indices
                EntPhysicalIndex
                EntAliasLogicalIndexOrZero
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexEntAliasMappingTable (INT4 *pi4EntPhysicalIndex,
                                      INT4 *pi4EntAliasLogicalIndexOrZero)
{
    return
        (nmhGetNextIndexEntAliasMappingTable (0, pi4EntPhysicalIndex,
                                              0,
                                              pi4EntAliasLogicalIndexOrZero));

}

/****************************************************************************
 Function    :  nmhGetNextIndexEntAliasMappingTable
 Input       :  The Indices
                EntPhysicalIndex
                nextEntPhysicalIndex
                EntAliasLogicalIndexOrZero
                nextEntAliasLogicalIndexOrZero
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexEntAliasMappingTable (INT4 i4EntPhysicalIndex,
                                     INT4 *pi4NextEntPhysicalIndex,
                                     INT4 i4EntAliasLogicalIndexOrZero,
                                     INT4 *pi4NextEntAliasLogicalIndexOrZero)
{
    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));

    UNUSED_PARAM (i4EntAliasLogicalIndexOrZero);
    *pi4NextEntAliasLogicalIndexOrZero = 0;

    do
    {
        if (EntUtilGetNextPhyIndex (i4EntPhysicalIndex,
                                    pi4NextEntPhysicalIndex) == OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }

        if (EntUtilGetPhysicalInfo (*pi4NextEntPhysicalIndex, &PhyInfo) !=
            OSIX_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        i4EntPhysicalIndex = *pi4NextEntPhysicalIndex;
    }
    while (PhyInfo.i1PhyClass != ENT_PHY_CLASS_PORT);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetEntAliasMappingIdentifier
 Input       :  The Indices
                EntPhysicalIndex
                EntAliasLogicalIndexOrZero

                The Object 
                retValEntAliasMappingIdentifier
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntAliasMappingIdentifier (INT4 i4EntPhysicalIndex,
                                 INT4 i4EntAliasLogicalIndexOrZero,
                                 tSNMP_OID_TYPE *
                                 pRetValEntAliasMappingIdentifier)
{
    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));

    UNUSED_PARAM (i4EntAliasLogicalIndexOrZero);

    if (EntUtilGetPhysicalInfo (i4EntPhysicalIndex, &PhyInfo) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (STRLEN (PhyInfo.au1Alias) != 0)
    {
        MEMSET (pRetValEntAliasMappingIdentifier->pu4_OidList, 0,
                sizeof (UINT4) * STRLEN (PhyInfo.au1Alias));
        pRetValEntAliasMappingIdentifier->u4_Length = STRLEN (PhyInfo.au1Alias);
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : EntPhysicalContainsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceEntPhysicalContainsTable
 Input       :  The Indices
                EntPhysicalIndex
                EntPhysicalChildIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceEntPhysicalContainsTable (INT4 i4EntPhysicalIndex,
                                                  INT4 i4EntPhysicalChildIndex)
{
    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));

    if ((i4EntPhysicalIndex < ENT_MIN_PHYICAL_INDEX_VAL) ||
        (i4EntPhysicalIndex > ENT_MAX_PHY_ENTITY) ||
        (i4EntPhysicalChildIndex < ENT_MIN_PHYICAL_INDEX_VAL) ||
        (i4EntPhysicalChildIndex > ENT_MAX_PHY_ENTITY))
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical Contains Table validation Failed"));
        return SNMP_FAILURE;
    }

    if (EntUtilGetPhysicalInfo (i4EntPhysicalChildIndex, &PhyInfo) !=
        OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical Contains Table validation Failed"));
        return SNMP_FAILURE;
    }

    if (PhyInfo.i4ContainedIn != i4EntPhysicalIndex)
    {
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, ENTITY_SYSLOG_ID,
                      "Physical Contains Table validation Failed"));
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexEntPhysicalContainsTable
 Input       :  The Indices
                EntPhysicalIndex
                EntPhysicalChildIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexEntPhysicalContainsTable (INT4 *pi4EntPhysicalIndex,
                                          INT4 *pi4EntPhysicalChildIndex)
{
    return
        (nmhGetNextIndexEntPhysicalContainsTable (0, pi4EntPhysicalIndex,
                                                  0, pi4EntPhysicalChildIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexEntPhysicalContainsTable
 Input       :  The Indices
                EntPhysicalIndex
                nextEntPhysicalIndex
                EntPhysicalChildIndex
                nextEntPhysicalChildIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexEntPhysicalContainsTable (INT4 i4EntPhysicalIndex,
                                         INT4 *pi4NextEntPhysicalIndex,
                                         INT4 i4EntPhysicalChildIndex,
                                         INT4 *pi4NextEntPhysicalChildIndex)
{
    INT4                i4LocalPhyChildIndex = 0;
    MEMSET (&PhyInfo, 0, sizeof (tIssEntPhyInfo));

    UNUSED_PARAM (i4EntPhysicalIndex);
    if (i4EntPhysicalChildIndex == 0)
    {
        i4LocalPhyChildIndex = 1;
        EntUtilGetNextPhyIndex (i4LocalPhyChildIndex,
                                pi4NextEntPhysicalChildIndex);
    }
    else
    {
        if (EntUtilGetNextPhyIndex (i4EntPhysicalChildIndex,
                                    pi4NextEntPhysicalChildIndex)
            == OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    if (EntUtilGetPhysicalInfo (*pi4NextEntPhysicalChildIndex, &PhyInfo) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4NextEntPhysicalIndex = PhyInfo.i4ContainedIn;
    if (*pi4NextEntPhysicalIndex < i4EntPhysicalIndex)
    {
        return SNMP_FAILURE;
    }
    else if (*pi4NextEntPhysicalIndex == i4EntPhysicalIndex)
    {
        if (*pi4NextEntPhysicalChildIndex <= i4EntPhysicalChildIndex)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetEntLastChangeTime
 Input       :  The Indices

                The Object 
                retValEntLastChangeTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetEntLastChangeTime (UINT4 *pu4RetValEntLastChangeTime)
{
    EntSnmpGetLastChangeTime (pu4RetValEntLastChangeTime);
    return SNMP_SUCCESS;
}
