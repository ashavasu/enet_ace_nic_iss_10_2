/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: entport.h,v 1.6 2013/04/15 10:54:31 siva Exp $
 * 
 * Description: This file contains the prototypes for all the
 *              PUBLIC procedures.
 **********************************************************************/

#ifndef _ENT_PORT_H
#define _ENT_PORT_H

PUBLIC INT1 EntMbsmGetSlotInfo PROTO ((UINT2, tMbsmSlotInfo *));

PUBLIC INT1 EntMbsmGetSlotPhyInfo PROTO ((UINT4, INT4 , tIssEntPhyInfo *));
PUBLIC INT1 EntMbsmGetCardPhyInfo PROTO ((UINT4, INT4 , tIssEntPhyInfo *));

PUBLIC INT1 EntMbsmSetSlotSerialNum PROTO ((UINT4, INT4, UINT1 *));
PUBLIC INT1 EntMbsmSetSlotAlias PROTO ((UINT4, INT4, UINT1 *));
PUBLIC INT1 EntMbsmSetSlotAssetId PROTO ((UINT4, INT4, UINT1 *));
PUBLIC INT1 EntMbsmSetSlotUris PROTO ((UINT4, INT4, UINT1 *));

PUBLIC VOID EntIssGetStackInfo PROTO ((tIssEntPhyInfo *));
PUBLIC VOID EntIssStackSetSerialNum PROTO ((UINT1 *));
PUBLIC VOID EntIssStackSetAlias PROTO ((UINT1 *));
PUBLIC VOID EntIssStackSetAssetId PROTO ((UINT1 *));
PUBLIC VOID EntIssStackSetUris PROTO ((UINT1 *));

PUBLIC VOID EntIssGetCpuCountInStack PROTO ((UINT2 *));
PUBLIC VOID EntIssStackCpuSetSerialNum PROTO ((UINT4, UINT1 *));

PUBLIC VOID EntIssStackCpuSetAlias PROTO ((UINT4, UINT1 *));
PUBLIC VOID EntIssStackCpuSetAssetId PROTO ((UINT4, UINT1 *));
PUBLIC VOID EntIssStackCpuSetUris PROTO ((UINT4, UINT1 *));
PUBLIC VOID EntIssGetPSCountInStack PROTO ((UINT2 *));
PUBLIC VOID EntIssStackPSSetSerialNum PROTO ((UINT4, UINT1 *));
PUBLIC VOID EntIssStackPSSetAlias PROTO ((UINT4, UINT1 *));
PUBLIC VOID EntIssStackPSSetAssetId PROTO ((UINT4, UINT1 *));
PUBLIC VOID EntIssStackPSSetUris PROTO ((UINT4, UINT1 *));

PUBLIC VOID EntIssGetFanCountInStack PROTO ((UINT2 *));
PUBLIC VOID EntIssStackFanSetSerialNum PROTO ((UINT4, UINT1 *));
PUBLIC VOID EntIssStackFanSetAlias PROTO ((UINT4, UINT1 *));
PUBLIC VOID EntIssStackFanSetAssetId PROTO ((UINT4, UINT1 *));
PUBLIC VOID EntIssStackFanSetUris PROTO ((UINT4, UINT1 *));


PUBLIC VOID EntIssGetChassisCount PROTO ((UINT2 *));
PUBLIC INT1 EntIssChassisSetSerialNum PROTO ((UINT4, UINT1 *));
PUBLIC INT1 EntIssChassisSetAlias PROTO ((UINT4, UINT1 *));
PUBLIC INT1 EntIssChassisSetAssetId PROTO ((UINT4, UINT1 *));
PUBLIC INT1 EntIssChassisSetUris PROTO ((UINT4, UINT1 *));

PUBLIC VOID EntIssGetCpuCountInChassis PROTO ((UINT4, UINT2 *));
PUBLIC VOID EntIssChassisCpuSetSerialNum PROTO ((UINT4, UINT4, UINT1 *));
PUBLIC VOID EntIssChassisCpuSetAlias PROTO ((UINT4, UINT4, UINT1 *));
PUBLIC VOID EntIssChassisCpuSetAssetId PROTO ((UINT4, UINT4, UINT1 *));
PUBLIC VOID EntIssChassisCpuSetUris PROTO ((UINT4, UINT4, UINT1 *));

PUBLIC VOID EntIssGetPSCountInChassis PROTO ((UINT4, UINT2 *));
PUBLIC VOID EntIssChassisPSSetSerialNum PROTO ((UINT4, UINT4, UINT1 *));
PUBLIC VOID EntIssChassisPSSetAlias PROTO ((UINT4, UINT4, UINT1 *));
PUBLIC VOID EntIssChassisPSSetAssetId PROTO ((UINT4, UINT4, UINT1 *));
PUBLIC VOID EntIssChassisPSSetUris PROTO ((UINT4, UINT4, UINT1 *));

PUBLIC VOID EntIssGetFanCountInChassis PROTO ((UINT4, UINT2 *));
PUBLIC VOID EntIssChassisFanSetSerialNum PROTO ((UINT4, UINT4, UINT1 *));
PUBLIC VOID EntIssChassisFanSetAlias PROTO ((UINT4, UINT4, UINT1 *));
PUBLIC VOID EntIssChassisFanSetAssetId PROTO ((UINT4, UINT4, UINT1 *));
PUBLIC VOID EntIssChassisFanSetUris PROTO ((UINT4, UINT4, UINT1 *));

PUBLIC INT1 EntIssSetIfUris PROTO ((UINT4, UINT4, UINT1 *));

PUBLIC VOID EntIssGetCpuInfoInStack PROTO ((UINT4, tIssEntPhyInfo *));
PUBLIC VOID EntIssGetPSInfoInStack PROTO ((UINT4, tIssEntPhyInfo *));
PUBLIC VOID EntIssGetFanInfoInStack PROTO ((UINT4, tIssEntPhyInfo *));

PUBLIC VOID EntIssGetChassisInfo PROTO ((UINT4, tIssEntPhyInfo *));

PUBLIC VOID EntIssGetCpuInfoInChassis PROTO ((UINT4, UINT2, tIssEntPhyInfo *));
PUBLIC VOID EntIssGetPSInfoInChassis PROTO ((UINT4, UINT2, tIssEntPhyInfo *));
PUBLIC VOID EntIssGetFanInfoInChassis PROTO ((UINT4, UINT2, tIssEntPhyInfo *));

PUBLIC INT1 EntIssIfGetPhyInfo PROTO ((UINT4, UINT4 , tIssEntPhyInfo *));

INT4
EntVcmGetFirstActiveContext PROTO ((UINT4 *));
INT4
EntVcmGetNextActiveContext PROTO ((UINT4, UINT4 *));

PUBLIC INT1 EntVcmGetContextPortList PROTO ((UINT4, tPortList ));
PUBLIC INT1 EntVcmGetAliasName PROTO ((UINT4 , UINT1 *));

PUBLIC INT1 EntCfaCliGetIfName PROTO ((UINT4, INT1 *));
PUBLIC INT1 EntCfaGetNextActivePort PROTO ((UINT4, UINT4 *));
PUBLIC INT1 EntCfaValidateIfIndex PROTO ((UINT4 ));

PUBLIC INT1 EntSnmpGetMIBCountWithContextIdAndLock PROTO ((UINT4 *, UINT4 *));
PUBLIC INT1 EntSnmpGetMIBNameWithLock PROTO ((UINT4 , UINT1 *));
PUBLIC INT1 EntSnmpGetMIBNameWithContextIdAndLock PROTO ((UINT4 , UINT1 *));

PUBLIC VOID EntSnmpGetSysName PROTO ((UINT1 *));
PUBLIC VOID EntSnmpGetLastChangeTime PROTO ((UINT4 *));
PUBLIC INT1 EntSnmpGetTransportAddr PROTO ((UINT1 *));
PUBLIC INT1 EntSnmpGetTransportDomain PROTO ((UINT4 *));
PUBLIC INT1 EntSnmpGetContextEngineId PROTO ((tSNMP_OCTET_STRING_TYPE *));
PUBLIC VOID EntSnmpUpdateLastTimeChange PROTO ((VOID));

PUBLIC INT1 EntCfaGetFirstIpActivePort PROTO ((UINT4 *));
PUBLIC INT1 EntCfaIpIfGetIfInfo PROTO ((UINT4 , tIpConfigInfo *));
#endif /* _ENT_PORT_H */

