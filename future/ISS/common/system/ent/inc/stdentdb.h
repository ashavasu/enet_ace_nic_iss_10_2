/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdentdb.h,v 1.3 2009/09/26 10:15:08 prabuc Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDENTDB_H
#define _STDENTDB_H

UINT1 EntPhysicalTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 EntLogicalTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 EntLPMappingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 EntAliasMappingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 EntPhysicalContainsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 stdent [] ={1,3,6,1,2,1,47};
tSNMP_OID_TYPE stdentOID = {7, stdent};


UINT4 EntPhysicalIndex [ ] ={1,3,6,1,2,1,47,1,1,1,1,1};
UINT4 EntPhysicalDescr [ ] ={1,3,6,1,2,1,47,1,1,1,1,2};
UINT4 EntPhysicalVendorType [ ] ={1,3,6,1,2,1,47,1,1,1,1,3};
UINT4 EntPhysicalContainedIn [ ] ={1,3,6,1,2,1,47,1,1,1,1,4};
UINT4 EntPhysicalClass [ ] ={1,3,6,1,2,1,47,1,1,1,1,5};
UINT4 EntPhysicalParentRelPos [ ] ={1,3,6,1,2,1,47,1,1,1,1,6};
UINT4 EntPhysicalName [ ] ={1,3,6,1,2,1,47,1,1,1,1,7};
UINT4 EntPhysicalHardwareRev [ ] ={1,3,6,1,2,1,47,1,1,1,1,8};
UINT4 EntPhysicalFirmwareRev [ ] ={1,3,6,1,2,1,47,1,1,1,1,9};
UINT4 EntPhysicalSoftwareRev [ ] ={1,3,6,1,2,1,47,1,1,1,1,10};
UINT4 EntPhysicalSerialNum [ ] ={1,3,6,1,2,1,47,1,1,1,1,11};
UINT4 EntPhysicalMfgName [ ] ={1,3,6,1,2,1,47,1,1,1,1,12};
UINT4 EntPhysicalModelName [ ] ={1,3,6,1,2,1,47,1,1,1,1,13};
UINT4 EntPhysicalAlias [ ] ={1,3,6,1,2,1,47,1,1,1,1,14};
UINT4 EntPhysicalAssetID [ ] ={1,3,6,1,2,1,47,1,1,1,1,15};
UINT4 EntPhysicalIsFRU [ ] ={1,3,6,1,2,1,47,1,1,1,1,16};
UINT4 EntPhysicalMfgDate [ ] ={1,3,6,1,2,1,47,1,1,1,1,17};
UINT4 EntPhysicalUris [ ] ={1,3,6,1,2,1,47,1,1,1,1,18};
UINT4 EntLogicalIndex [ ] ={1,3,6,1,2,1,47,1,2,1,1,1};
UINT4 EntLogicalDescr [ ] ={1,3,6,1,2,1,47,1,2,1,1,2};
UINT4 EntLogicalType [ ] ={1,3,6,1,2,1,47,1,2,1,1,3};
UINT4 EntLogicalCommunity [ ] ={1,3,6,1,2,1,47,1,2,1,1,4};
UINT4 EntLogicalTAddress [ ] ={1,3,6,1,2,1,47,1,2,1,1,5};
UINT4 EntLogicalTDomain [ ] ={1,3,6,1,2,1,47,1,2,1,1,6};
UINT4 EntLogicalContextEngineID [ ] ={1,3,6,1,2,1,47,1,2,1,1,7};
UINT4 EntLogicalContextName [ ] ={1,3,6,1,2,1,47,1,2,1,1,8};
UINT4 EntLPPhysicalIndex [ ] ={1,3,6,1,2,1,47,1,3,1,1,1};
UINT4 EntAliasLogicalIndexOrZero [ ] ={1,3,6,1,2,1,47,1,3,2,1,1};
UINT4 EntAliasMappingIdentifier [ ] ={1,3,6,1,2,1,47,1,3,2,1,2};
UINT4 EntPhysicalChildIndex [ ] ={1,3,6,1,2,1,47,1,3,3,1,1};
UINT4 EntLastChangeTime [ ] ={1,3,6,1,2,1,47,1,4,1};


tMbDbEntry stdentMibEntry[]= {

{{12,EntPhysicalIndex}, GetNextIndexEntPhysicalTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, EntPhysicalTableINDEX, 1, 0, 0, NULL},

{{12,EntPhysicalDescr}, GetNextIndexEntPhysicalTable, EntPhysicalDescrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, EntPhysicalTableINDEX, 1, 0, 0, NULL},

{{12,EntPhysicalVendorType}, GetNextIndexEntPhysicalTable, EntPhysicalVendorTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, EntPhysicalTableINDEX, 1, 0, 0, NULL},

{{12,EntPhysicalContainedIn}, GetNextIndexEntPhysicalTable, EntPhysicalContainedInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, EntPhysicalTableINDEX, 1, 0, 0, NULL},

{{12,EntPhysicalClass}, GetNextIndexEntPhysicalTable, EntPhysicalClassGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, EntPhysicalTableINDEX, 1, 0, 0, NULL},

{{12,EntPhysicalParentRelPos}, GetNextIndexEntPhysicalTable, EntPhysicalParentRelPosGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, EntPhysicalTableINDEX, 1, 0, 0, NULL},

{{12,EntPhysicalName}, GetNextIndexEntPhysicalTable, EntPhysicalNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, EntPhysicalTableINDEX, 1, 0, 0, NULL},

{{12,EntPhysicalHardwareRev}, GetNextIndexEntPhysicalTable, EntPhysicalHardwareRevGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, EntPhysicalTableINDEX, 1, 0, 0, NULL},

{{12,EntPhysicalFirmwareRev}, GetNextIndexEntPhysicalTable, EntPhysicalFirmwareRevGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, EntPhysicalTableINDEX, 1, 0, 0, NULL},

{{12,EntPhysicalSoftwareRev}, GetNextIndexEntPhysicalTable, EntPhysicalSoftwareRevGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, EntPhysicalTableINDEX, 1, 0, 0, NULL},

{{12,EntPhysicalSerialNum}, GetNextIndexEntPhysicalTable, EntPhysicalSerialNumGet, EntPhysicalSerialNumSet, EntPhysicalSerialNumTest, EntPhysicalTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, EntPhysicalTableINDEX, 1, 0, 0, NULL},

{{12,EntPhysicalMfgName}, GetNextIndexEntPhysicalTable, EntPhysicalMfgNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, EntPhysicalTableINDEX, 1, 0, 0, NULL},

{{12,EntPhysicalModelName}, GetNextIndexEntPhysicalTable, EntPhysicalModelNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, EntPhysicalTableINDEX, 1, 0, 0, NULL},

{{12,EntPhysicalAlias}, GetNextIndexEntPhysicalTable, EntPhysicalAliasGet, EntPhysicalAliasSet, EntPhysicalAliasTest, EntPhysicalTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, EntPhysicalTableINDEX, 1, 0, 0, NULL},

{{12,EntPhysicalAssetID}, GetNextIndexEntPhysicalTable, EntPhysicalAssetIDGet, EntPhysicalAssetIDSet, EntPhysicalAssetIDTest, EntPhysicalTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, EntPhysicalTableINDEX, 1, 0, 0, NULL},

{{12,EntPhysicalIsFRU}, GetNextIndexEntPhysicalTable, EntPhysicalIsFRUGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, EntPhysicalTableINDEX, 1, 0, 0, NULL},

{{12,EntPhysicalMfgDate}, GetNextIndexEntPhysicalTable, EntPhysicalMfgDateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, EntPhysicalTableINDEX, 1, 0, 0, NULL},

{{12,EntPhysicalUris}, GetNextIndexEntPhysicalTable, EntPhysicalUrisGet, EntPhysicalUrisSet, EntPhysicalUrisTest, EntPhysicalTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, EntPhysicalTableINDEX, 1, 0, 0, NULL},

{{12,EntLogicalIndex}, GetNextIndexEntLogicalTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, EntLogicalTableINDEX, 1, 0, 0, NULL},

{{12,EntLogicalDescr}, GetNextIndexEntLogicalTable, EntLogicalDescrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, EntLogicalTableINDEX, 1, 0, 0, NULL},

{{12,EntLogicalType}, GetNextIndexEntLogicalTable, EntLogicalTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, EntLogicalTableINDEX, 1, 0, 0, NULL},

{{12,EntLogicalCommunity}, GetNextIndexEntLogicalTable, EntLogicalCommunityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, EntLogicalTableINDEX, 1, 1, 0, NULL},

{{12,EntLogicalTAddress}, GetNextIndexEntLogicalTable, EntLogicalTAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, EntLogicalTableINDEX, 1, 0, 0, NULL},

{{12,EntLogicalTDomain}, GetNextIndexEntLogicalTable, EntLogicalTDomainGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, EntLogicalTableINDEX, 1, 0, 0, NULL},

{{12,EntLogicalContextEngineID}, GetNextIndexEntLogicalTable, EntLogicalContextEngineIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, EntLogicalTableINDEX, 1, 0, 0, NULL},

{{12,EntLogicalContextName}, GetNextIndexEntLogicalTable, EntLogicalContextNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, EntLogicalTableINDEX, 1, 0, 0, NULL},

{{12,EntLPPhysicalIndex}, GetNextIndexEntLPMappingTable, EntLPPhysicalIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, EntLPMappingTableINDEX, 2, 0, 0, NULL},

{{12,EntAliasLogicalIndexOrZero}, GetNextIndexEntAliasMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, EntAliasMappingTableINDEX, 2, 0, 0, NULL},

{{12,EntAliasMappingIdentifier}, GetNextIndexEntAliasMappingTable, EntAliasMappingIdentifierGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, EntAliasMappingTableINDEX, 2, 0, 0, NULL},

{{12,EntPhysicalChildIndex}, GetNextIndexEntPhysicalContainsTable, EntPhysicalChildIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, EntPhysicalContainsTableINDEX, 2, 0, 0, NULL},

{{10,EntLastChangeTime}, NULL, EntLastChangeTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData stdentEntry = { 31, stdentMibEntry };
#endif /* _STDENTDB_H */

