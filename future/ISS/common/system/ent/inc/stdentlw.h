/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdentlw.h,v 1.3 2009/09/26 10:15:08 prabuc Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for EntPhysicalTable. */
INT1
nmhValidateIndexInstanceEntPhysicalTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for EntPhysicalTable  */

INT1
nmhGetFirstIndexEntPhysicalTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexEntPhysicalTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetEntPhysicalDescr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetEntPhysicalVendorType ARG_LIST((INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetEntPhysicalContainedIn ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetEntPhysicalClass ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetEntPhysicalParentRelPos ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetEntPhysicalName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetEntPhysicalHardwareRev ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetEntPhysicalFirmwareRev ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetEntPhysicalSoftwareRev ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetEntPhysicalSerialNum ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetEntPhysicalMfgName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetEntPhysicalModelName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetEntPhysicalAlias ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetEntPhysicalAssetID ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetEntPhysicalIsFRU ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetEntPhysicalMfgDate ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetEntPhysicalUris ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetEntPhysicalSerialNum ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetEntPhysicalAlias ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetEntPhysicalAssetID ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetEntPhysicalUris ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2EntPhysicalSerialNum ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2EntPhysicalAlias ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2EntPhysicalAssetID ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2EntPhysicalUris ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2EntPhysicalTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for EntLogicalTable. */
INT1
nmhValidateIndexInstanceEntLogicalTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for EntLogicalTable  */

INT1
nmhGetFirstIndexEntLogicalTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexEntLogicalTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetEntLogicalDescr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetEntLogicalType ARG_LIST((INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetEntLogicalCommunity ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetEntLogicalTAddress ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetEntLogicalTDomain ARG_LIST((INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetEntLogicalContextEngineID ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetEntLogicalContextName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for EntLPMappingTable. */
INT1
nmhValidateIndexInstanceEntLPMappingTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for EntLPMappingTable  */

INT1
nmhGetFirstIndexEntLPMappingTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexEntLPMappingTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

/* Proto Validate Index Instance for EntAliasMappingTable. */
INT1
nmhValidateIndexInstanceEntAliasMappingTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for EntAliasMappingTable  */

INT1
nmhGetFirstIndexEntAliasMappingTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexEntAliasMappingTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetEntAliasMappingIdentifier ARG_LIST((INT4  , INT4 ,tSNMP_OID_TYPE * ));

/* Proto Validate Index Instance for EntPhysicalContainsTable. */
INT1
nmhValidateIndexInstanceEntPhysicalContainsTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for EntPhysicalContainsTable  */

INT1
nmhGetFirstIndexEntPhysicalContainsTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexEntPhysicalContainsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetEntLastChangeTime ARG_LIST((UINT4 *));
