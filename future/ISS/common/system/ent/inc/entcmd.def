/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved               
*                                                                    
* $Id: entcmd.def,v 1.10 2014/11/04 13:19:33 siva Exp $                                                         
*                                                                    
*********************************************************************/

/**************************************************************************/
/*                      ENTITY MIB Configuration                                */
/**************************************************************************/

DEFINE GROUP: ENT_CFG_CMDS
COMMAND : set entity physical-index <integer (1-2147483647)> {[asset-id
<string(32)>] [serial-number  <string(32)>] [alias-name  <string(32)>]
[uris  <string(255)>]}
ACTION  : 
{

   if ($4 != NULL)
   {
       cli_process_ent_cmd ( CliHandle,  CLI_ENT_CFG_ASSET_ID, NULL, $3, $5);
   }
   if ($6 != NULL)
   {
       cli_process_ent_cmd ( CliHandle,  CLI_ENT_CFG_SER_NUM, NULL, $3, $7);
   }
   if ($8 != NULL)
   {
       cli_process_ent_cmd ( CliHandle,  CLI_ENT_CFG_ALIAS_NAME, NULL, $3, $9);
   }
   if ($10 != NULL)
   {
       cli_process_ent_cmd ( CliHandle,  CLI_ENT_CFG_URIS, NULL, $3, $11);
   }
   if (($4 == NULL) && ($6 == NULL) && ($8 == NULL) && ($10 == NULL))
   {
       CliPrintf (CliHandle, "\r%% Invalid List of Arguments\r\n");
   }

}
SYNTAX  : set entity physical-index <integer (1..2147483647)> {[asset-id
<SnmpAdminString (Size (1..32))>] [serial-number  <SnmpAdminString (Size
(1..32))>] [alias-name <SnmpAdminString (Size (1..32))>] [uris <OCTET-STRING
(Size (1..255))>]}
PRVID   : 15
HELP    : Configures the read-write objects of the physical components
present in the system
CXT_HELP : set Configures the parameters | 
           entity Entity MIB related configuration | 
           physical-index Physical components objects related configuration | 
           (1-2147483647) Index of the physical entity | 
           asset-id Asset tracking identifier configuration | 
           <string(32)> Asset tracking identifier for the physical entity | 
           serial-number Vendor-specific serial number string configuration | 
           <string(32)> Vendor-specific serial number string for the physical entity | 
           alias-name  Alias name configuration | 
           <string(32)> Alias name for the physical entity | 
           uris Uniform Resource Indicator configuration | 
           <string(255)> Uniform Resource Indicator for the physical entity | 
           <CR> Configures the read-write objects of the physical components
present in the system


COMMAND : no entity physical-index <integer (1-2147483647)> [asset-id] [serial-number][alias-name][uris]
ACTION  : 
{

   if (($4 == NULL) && ($5 == NULL) && ($6 == NULL) && ($7 == NULL))
   {
       cli_process_ent_cmd ( CliHandle,  CLI_ENT_RESET_ALL, NULL, $3);
   }
   if ($7 != NULL)
   {
       cli_process_ent_cmd ( CliHandle,  CLI_ENT_RESET_URIS, NULL, $3);
   }
   if ($4 != NULL)
   {
       cli_process_ent_cmd ( CliHandle,  CLI_ENT_RESET_ASSET_ID, NULL, $3);
   }
   if ($5 != NULL)
   {
       cli_process_ent_cmd ( CliHandle,  CLI_ENT_RESET_SER_NUM, NULL, $3);
   }
   if ($6 != NULL)
   {
       cli_process_ent_cmd ( CliHandle,  CLI_ENT_RESET_ALIAS_NAME, NULL, $3);
   }
}
SYNTAX  : no entity physical-index <integer (1-2147483647)> [asset-id] [serial-number][alias-name][uris]
PRVID   : 15
HELP    : Reset the read-write objects of the physical components present in the system
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value | 
           entity Entity MIB related configuration | 
           physical-index Physical components objects related configuration | 
           (1-2147483647) Index of the physical entity | 
           asset-id Asset tracking identifier configuration | 
           serial-number Vendor-specific serial number string configuration | 
           alias-name Alias name configuration | 
           uris Uniform Resource Indicator configuration | 
           <CR> Reset the read-write objects of the physical components present in the system

END GROUP   

/**************************************************************************/
/*                      ENTITY MIB Show Commands                          */
/**************************************************************************/


DEFINE GROUP: ENT_SHOW_CMDS

COMMAND : show entity logical [index <integer (1-2147483647)>]
ACTION  : 
{
   if ($3 != NULL)
   {
       cli_process_ent_cmd ( CliHandle,  CLI_ENT_SHOW_LOG_ENTITY, 
                             NULL, $4);
   }
   else
   {
       cli_process_ent_cmd ( CliHandle,  CLI_ENT_SHOW_LOG_ENTITY_ALL, 
                             NULL);
   }
}

SYNTAX  : show entity logical [index <integer (1..2147483647)>]
PRVID   : 15
HELP    : Displays the logical entities
CXT_HELP : show Displays the configuration / statistics / general information | 
           entity Entity MIB related configuration | 
           logical Logical entity configuration | 
           index Index of the logical entity | 
           (1-2147483647) Index value | 
           <CR> Displays the logical entities

COMMAND : show entity physical [index <integer (1-2147483647)>]
ACTION  : 
{
   if ($3 != NULL)
   {
       cli_process_ent_cmd ( CliHandle,  CLI_ENT_SHOW_PHY_ENTITY, 
                             NULL, $4);
   }
   else
   {
       cli_process_ent_cmd ( CliHandle,  CLI_ENT_SHOW_PHY_ENTITY_ALL, 
                             NULL);
   }
}

SYNTAX  : show entity physical [index <integer (1..2147483647)>]
PRVID   : 15
HELP    : Displays the physical entities
CXT_HELP : show Displays the configuration / statistics / general information | 
           entity Entity MIB related configuration | 
           physical Physical entity configuration | 
           index Index of the physical entity | 
           (1-2147483647) Index value | 
           <CR> Displays the physical entities

COMMAND : show entity lp-mapping 
ACTION  : 
{
   cli_process_ent_cmd ( CliHandle,  CLI_ENT_SHOW_LP_MAPPING_ALL, 
                         NULL);
}

SYNTAX  : show entity lp-mapping 
PRVID   : 15
HELP    : Displays the mapping of logical and physical entities
CXT_HELP : show Displays the configuration / statistics / general information | 
           entity Entity MIB related configuration | 
           lp-mapping Logical and physical entities mapping details | 
           <CR> Displays the mapping of logical and physical entities

COMMAND : show entity alias-mapping [index <integer (1-2147483647)>]
ACTION  : 
{
   if ($3 != NULL)
   {
       cli_process_ent_cmd ( CliHandle,  CLI_ENT_SHOW_ALIAS_MAPPING, 
                             NULL, $4);
   }
   else
   {
       cli_process_ent_cmd ( CliHandle,  CLI_ENT_SHOW_ALIAS_MAPPING_ALL, 
                             NULL);
   }
}

SYNTAX  : show entity alias-mapping [index <integer (1..2147483647)>]
PRVID   : 15
HELP    : Displays the mapping of logical and physical entity with external
identifiers
CXT_HELP : show Displays the configuration / statistics / general information | 
           entity Entity MIB related configuration | 
           alias-mapping Details regarding mapping of logical and physical entities with external identifiers | 
           index Index of the physical entity | 
           (1-2147483647) Index value | 
           <CR> Displays the mapping of logical and physical entity with external identifiers

COMMAND : show entity phy-containment [index <integer (1-2147483647)>]
ACTION  : 
{
   if ($3 != NULL)
   {
       cli_process_ent_cmd ( CliHandle,  CLI_ENT_SHOW_PHY_CONTAINMENT, 
                             NULL, $4);
   }
   else
   {
       cli_process_ent_cmd ( CliHandle,  CLI_ENT_SHOW_PHY_CONTAINMENT_ALL, 
                             NULL);
   }
}

SYNTAX  : show entity phy-containment [index <integer (1..2147483647)>]
PRVID   : 15
HELP    : Displays the containment relationship of physical components
CXT_HELP : show Displays the configuration / statistics / general information | 
           entity Entity MIB related configuration | 
           phy-containment Containment relationship details of physical components | 
           index Index of the physical entity | 
           (1-2147483647) Index value | 
           <CR> Displays the containment relationship of physical components

END GROUP   
