/**************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                   *
 *                                                                        *
 * $Id: entdefs.h,v 1.8 2012/03/30 13:22:47 siva Exp $                                                      *
 *                                                                        *
 * Description: This file contains the constant definations for Entity MIB*
 * ************************************************************************/


#ifndef _ENT_DEF_H
#define _ENT_DEF_H



/* Index specific constants */
#define ENT_MIN_PHYICAL_INDEX_VAL 1

#define ENT_MIN_LOGICAL_INDEX_VAL 1


#define ENT_MAX_PHY_ENTITY          EntUtilMaxPhyIndex ()
#define ENT_MAX_LOG_ENTITY          EntUtilMaxLogIndex () 


/* Physical Table Constants */


#define ENT_PHY_RESET_ALL         0
#define ENT_PHY_SERIAL_NUM        (ISS_ENT_PHY_SERIAL_NUM)
#define ENT_PHY_ALIAS_NAME        (ISS_ENT_PHY_ALIAS_NAME)
#define ENT_PHY_ASSET_ID          (ISS_ENT_PHY_ASSET_ID)
#define ENT_PHY_URIS              (ISS_ENT_PHY_URIS)



#define ENT_PHY_CLASS_OTHER        1
#define ENT_PHY_CLASS_UNKNOWN      2
#define ENT_PHY_CLASS_CHASSIS      3
#define ENT_PHY_CLASS_BACKPLANE    4        
#define ENT_PHY_CLASS_CONTAINER    5        
#define ENT_PHY_CLASS_POWER        6
#define ENT_PHY_CLASS_FAN          7        
#define ENT_PHY_CLASS_SENSOR       8
#define ENT_PHY_CLASS_MODULE       9
#define ENT_PHY_CLASS_PORT         10
#define ENT_PHY_CLASS_STACK        11
#define ENT_PHY_CLASS_CPU          12


#define ENT_SNMP_PORT          SNMP_PORT

/* System Specific Constants */

#define ENT_INCR_VAL                   1   /* Increment value */
#define ENT_DECR_VAL                   1   /* Decrement value */

#define ENT_IFPORT_LIST_SIZE  BRG_PORT_LIST_SIZE
#define ENT_PORTS_PER_BYTE         8

#define ENT_MAX_STR_LEN            255
#define ENT_MAX_TYPE_LEN           10
/* ENT 8 bit */
#define ENT_BIT8 0x80
#define ENT_DEFAULT_PHY_MFG_DATE  "\x00\x00\x00\x00\x00\x00\x00\x00"
#define ENT_DEFAULT_PHY_MFG_DATE_LEN  8

#ifdef MBSM_WANTED
#define ENT_MAX_LC_SLOTS    MBSM_MAX_LC_SLOTS
#define ENT_MAX_CC_SLOTS    MBSM_MAX_CC_SLOTS
#endif

#define ENTITY_NAME          (CONST UINT1 *)"ENTITY_MIB"
#define ENTITY_SYSLOG_ID      gau4EntSysLogId 

#define ENT_SNMP_TRUE      1
#define ENT_SNMP_FALSE      2
#define ENT_GET_1BYTE(u1Val, pu1Buf)\
do{\
    u1Val = *pu1Buf;\
}while(0)

#define ENT_GET_2BYTE(u2Val, pu1Buf)\
do{\
    MEMCPY (&u2Val, pu1Buf, 2);\
            u2Val = (UINT2) (OSIX_NTOHS(u2Val));\
}while(0)
#define ENT_GET_4BYTE(u4Val, pu1Buf)\
do{\
    MEMCPY (&u4Val, pu1Buf, 4);\
            u4Val = (UINT4) (OSIX_NTOHL(u4Val));\
}while(0)
#define ENT_MFG_DAT_COMMA_INDEX 8
#define ENT_MFG_DAT_COLEN_INDEX 10
#define ENT_MFG_DAT_YEAR_INDEX 2
#define ENT_MFG_DAT_TIME_SEP_INDEX 4
#define ENT_MFG_TIME_SEP_INDEX 5
#define ENT_MFG_TIME_MSEC_SEP_INDEX 7
#define ENT_MFG_DAT_PLUS_ASCII_VALUE 43
#define ENT_MFG_DAT_MINUS_ASCII_VALUE 45
#define ENT_TRANSPORT_ADDR_PORT_INDEX 4
#endif /* _ENTDEF_H */
