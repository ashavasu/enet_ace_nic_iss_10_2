/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved]
 *
 *$Id: entprot.h,v 1.4 2009/09/29 14:35:09 prabuc Exp $
 *
 * Description: This file contains the prototypes for all the
 *              PUBLIC procedures
 ********************************************************************/

#ifndef _ENT_PRT_H
#define _ENT_PRT_H


PUBLIC INT1
EntUtilGetRegisteredMibEntry PROTO ((UINT4 , UINT1 *));

PUBLIC INT1
EntUtilGetPhysicalInfo PROTO ((INT4 , tIssEntPhyInfo *));

PUBLIC INT1
EntUtilSetPhyInfo PROTO ((INT4 , UINT1 *, UINT1 ));

PUBLIC VOID
EntUtilGetCntxtIdForLI PROTO ((UINT4 , UINT4 *));

PUBLIC INT1
EntUtilFirstIfInfoForCntxt PROTO ((UINT4 , UINT1 *, UINT4 *));

PUBLIC INT1
EntUtilGetNextIfIndex PROTO ((UINT4 , UINT4 ,UINT4 *));

PUBLIC INT1
EntUtilGetPhyIndexForIfName PROTO ((UINT1 *, INT4 *));

PUBLIC INT4
EntUtilMaxLogIndex PROTO ((VOID));

PUBLIC INT4
EntUtilMaxPhyIndex PROTO ((VOID));

PUBLIC VOID
EntUtilGetIfIndex PROTO ((UINT4 , INT4 , UINT4 , UINT4 *));

PUBLIC INT1
EntUtilGetNextLogIndex PROTO ((INT4 , INT4 *));

PUBLIC INT1
EntUtilGetNextPhyIndex PROTO ((INT4 , INT4 *));

PUBLIC VOID
EntUtilGetActiveContextCount PROTO ((UINT4 *));

PUBLIC INT4
EntUtilGetIfIndexForPhyIndex PROTO ((UINT4 , UINT4 *));

PUBLIC INT4
EntUtilGetActivePortCount PROTO ((UINT4 , UINT4));

PUBLIC INT4
EntUtilGetIfIndexForPortNum PROTO ((UINT4 , UINT4 ,
                                    UINT4 , UINT4 *));


PUBLIC INT4 EntCliShowLogEntity PROTO ((tCliHandle, INT4));
PUBLIC INT4 EntCliShowPhyEntity PROTO ((tCliHandle, INT4));
PUBLIC INT4 EntCliShowAllPhyEntities PROTO ((tCliHandle));
PUBLIC INT4 EntCliShowAllLogEntities PROTO ((tCliHandle));
PUBLIC INT4 EntCliConfigPhyEntity PROTO ((tCliHandle, UINT1, INT4, UINT1 *));
PUBLIC INT4 EntCliResetPhyEntity PROTO ((tCliHandle, UINT1, INT4 ));
PUBLIC INT4 EntCliShowAllLPMapEntries PROTO ((tCliHandle));
PUBLIC INT4 EntCliShowAliasMapEntries PROTO ((tCliHandle, INT4));
PUBLIC INT4 EntCliShowAllAliasMapEntries PROTO ((tCliHandle));
PUBLIC INT4 EntCliShowPhyContainment PROTO ((tCliHandle, INT4));
PUBLIC INT4 EntCliShowAllPhyContainment PROTO ((tCliHandle));
PUBLIC INT1 EntityInit PROTO ((VOID));
#endif /* _ENT_PRT_H */
