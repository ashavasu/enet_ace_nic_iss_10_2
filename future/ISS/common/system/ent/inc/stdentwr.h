#ifndef _STDENTWR_H
#define _STDENTWR_H
INT4 GetNextIndexEntPhysicalTable(tSnmpIndex *, tSnmpIndex *);

INT4 EntPhysicalDescrGet(tSnmpIndex *, tRetVal *);
INT4 EntPhysicalVendorTypeGet(tSnmpIndex *, tRetVal *);
INT4 EntPhysicalContainedInGet(tSnmpIndex *, tRetVal *);
INT4 EntPhysicalClassGet(tSnmpIndex *, tRetVal *);
INT4 EntPhysicalParentRelPosGet(tSnmpIndex *, tRetVal *);
INT4 EntPhysicalNameGet(tSnmpIndex *, tRetVal *);
INT4 EntPhysicalHardwareRevGet(tSnmpIndex *, tRetVal *);
INT4 EntPhysicalFirmwareRevGet(tSnmpIndex *, tRetVal *);
INT4 EntPhysicalSoftwareRevGet(tSnmpIndex *, tRetVal *);
INT4 EntPhysicalSerialNumGet(tSnmpIndex *, tRetVal *);
INT4 EntPhysicalMfgNameGet(tSnmpIndex *, tRetVal *);
INT4 EntPhysicalModelNameGet(tSnmpIndex *, tRetVal *);
INT4 EntPhysicalAliasGet(tSnmpIndex *, tRetVal *);
INT4 EntPhysicalAssetIDGet(tSnmpIndex *, tRetVal *);
INT4 EntPhysicalIsFRUGet(tSnmpIndex *, tRetVal *);
INT4 EntPhysicalMfgDateGet(tSnmpIndex *, tRetVal *);
INT4 EntPhysicalUrisGet(tSnmpIndex *, tRetVal *);
INT4 EntPhysicalSerialNumSet(tSnmpIndex *, tRetVal *);
INT4 EntPhysicalAliasSet(tSnmpIndex *, tRetVal *);
INT4 EntPhysicalAssetIDSet(tSnmpIndex *, tRetVal *);
INT4 EntPhysicalUrisSet(tSnmpIndex *, tRetVal *);
INT4 EntPhysicalSerialNumTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 EntPhysicalAliasTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 EntPhysicalAssetIDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 EntPhysicalUrisTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 EntPhysicalTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexEntLogicalTable(tSnmpIndex *, tSnmpIndex *);
INT4 EntLogicalDescrGet(tSnmpIndex *, tRetVal *);
INT4 EntLogicalTypeGet(tSnmpIndex *, tRetVal *);
INT4 EntLogicalCommunityGet(tSnmpIndex *, tRetVal *);
INT4 EntLogicalTAddressGet(tSnmpIndex *, tRetVal *);
INT4 EntLogicalTDomainGet(tSnmpIndex *, tRetVal *);
INT4 EntLogicalContextEngineIDGet(tSnmpIndex *, tRetVal *);
INT4 EntLogicalContextNameGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexEntLPMappingTable(tSnmpIndex *, tSnmpIndex *);
INT4 EntLPPhysicalIndexGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexEntAliasMappingTable(tSnmpIndex *, tSnmpIndex *);
INT4 EntAliasMappingIdentifierGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexEntPhysicalContainsTable(tSnmpIndex *, tSnmpIndex *);
INT4 EntPhysicalChildIndexGet(tSnmpIndex *, tRetVal *);
INT4 EntLastChangeTimeGet(tSnmpIndex *, tRetVal *);
VOID RegisterSTDENT(VOID);
VOID UnRegisterSTDENT(VOID);
#endif /* _STDENTWR_H */
