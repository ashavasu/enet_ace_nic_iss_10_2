/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: issmgrlw.c,v 1.24 2017/12/21 10:16:32 siva Exp $
 *
 * *******************************************************************/
/* SOURCE FILE  :
 *
 *  ---------------------------------------------------------------------------
 * |  Copyright (C) 2006 Aricent Inc . All Rights Reserved                                 |
 * |  Copyright (C) 2006 Aricent Inc . All Rights Reserved                       |
 * |                                                                           |
 * |  FILE NAME                   :  issmgrlw.c                                |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR            :  Aricent Inc.                       |
 * |                                                                           |
 * |  SUBSYSTEM NAME              :  ISS                                       |
 * |                                                                           |
 * |  MODULE NAME                 :  ISS                                       |
 * |                                                                           |
 * |  LANGUAGE                    :  C                                         |
 * |                                                                           |
 * |  TARGET ENVIRONMENT          :  LINUX                                     |
 * |                                                                           |
 * |  AUTHOR                      :  ISS Team                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION                 :  this file has the low level code for the  |
 * |                                 fsiss mib                                 |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 * 
 */
#ifdef KERNEL_WANTED
#include  "isswinc.h"
#else
#include "issinc.h"
#endif

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssIpAuthMgrTable
 Input       :  The Indices
                IssIpAuthMgrIpAddr
                IssIpAuthMgrIpMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssIpAuthMgrTable (UINT4 u4IssIpAuthMgrIpAddr,
                                           UINT4 u4IssIpAuthMgrIpMask)
{
    if (u4IssIpAuthMgrIpAddr == 0)
    {
        if (u4IssIpAuthMgrIpMask != 0)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (!((ISS_IS_ADDR_CLASS_A (u4IssIpAuthMgrIpAddr)) ||
              (ISS_IS_ADDR_CLASS_B (u4IssIpAuthMgrIpAddr)) ||
              (ISS_IS_ADDR_CLASS_C (u4IssIpAuthMgrIpAddr))))
        {
            return SNMP_FAILURE;
        }
    }

    if ((u4IssIpAuthMgrIpAddr & u4IssIpAuthMgrIpMask) != u4IssIpAuthMgrIpAddr)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIssIpAuthMgrTable
 Input       :  The Indices
                IssIpAuthMgrIpAddr
                IssIpAuthMgrIpMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIssIpAuthMgrTable (UINT4 *pu4IssIpAuthMgrIpAddr,
                                   UINT4 *pu4IssIpAuthMgrIpMask)
{
    UINT4               u4Index;
    UINT4               u4Found = ISS_FALSE;
    tIssIpMgrEntry     *pIpMgrEntry;

    /* To include the generic manager in the list, u4Index starts from 0 */
    for (u4Index = 0; u4Index <= ISS_MAX_MGR_IP_ENTRIES; u4Index++)
    {
        pIpMgrEntry = ISS_GET_IP_MGR_ENTRY (u4Index);

        if (pIpMgrEntry->i4Status != ISS_IP_AUTH_INVALID)
        {
            if (u4Found == ISS_FALSE)
            {
                u4Found = ISS_TRUE;
                *pu4IssIpAuthMgrIpAddr = pIpMgrEntry->u4IpAddr;
                *pu4IssIpAuthMgrIpMask = pIpMgrEntry->u4IpMask;
            }
            else
            {
                /*For smallest IP address - compare the IP address and if both are equal, 
                   compare the mask address to get the smallest ip address */
                if ((*pu4IssIpAuthMgrIpAddr > pIpMgrEntry->u4IpAddr) ||
                    ((*pu4IssIpAuthMgrIpAddr == pIpMgrEntry->u4IpAddr) &&
                     (*pu4IssIpAuthMgrIpMask > pIpMgrEntry->u4IpMask)))
                {
                    *pu4IssIpAuthMgrIpAddr = pIpMgrEntry->u4IpAddr;
                    *pu4IssIpAuthMgrIpMask = pIpMgrEntry->u4IpMask;
                }
            }
        }
    }

    if (u4Found == ISS_TRUE)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIssIpAuthMgrTable
 Input       :  The Indices
                IssIpAuthMgrIpAddr
                nextIssIpAuthMgrIpAddr
                IssIpAuthMgrIpMask
                nextIssIpAuthMgrIpMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIssIpAuthMgrTable (UINT4 u4IssIpAuthMgrIpAddr,
                                  UINT4 *pu4NextIssIpAuthMgrIpAddr,
                                  UINT4 u4IssIpAuthMgrIpMask,
                                  UINT4 *pu4NextIssIpAuthMgrIpMask)
{
    UINT4               u4Index;
    UINT4               u4Found = ISS_FALSE;
    tIssIpMgrEntry     *pIpMgrEntry;

    /* To include the generic manager in the list, u4Index starts from 0 */
    for (u4Index = 0; u4Index <= ISS_MAX_MGR_IP_ENTRIES; u4Index++)
    {
        pIpMgrEntry = ISS_GET_IP_MGR_ENTRY (u4Index);

        if (pIpMgrEntry->i4Status != ISS_IP_AUTH_INVALID)
        {
            /* To get the next smallest IP address -  sort first for smallest IP 
               and in case both are equal then compare the mask IP address */
            if ((u4IssIpAuthMgrIpAddr < pIpMgrEntry->u4IpAddr) ||
                ((u4IssIpAuthMgrIpAddr == pIpMgrEntry->u4IpAddr) &&
                 (u4IssIpAuthMgrIpMask < pIpMgrEntry->u4IpMask)))
            {
                if (u4Found == ISS_FALSE)
                {
                    u4Found = ISS_TRUE;
                    *pu4NextIssIpAuthMgrIpAddr = pIpMgrEntry->u4IpAddr;
                    *pu4NextIssIpAuthMgrIpMask = pIpMgrEntry->u4IpMask;
                }
                else
                {
                    if ((*pu4NextIssIpAuthMgrIpAddr > pIpMgrEntry->u4IpAddr) ||
                        ((*pu4NextIssIpAuthMgrIpAddr == pIpMgrEntry->u4IpAddr)
                         && (*pu4NextIssIpAuthMgrIpMask >
                             pIpMgrEntry->u4IpMask)))
                    {
                        *pu4NextIssIpAuthMgrIpAddr = pIpMgrEntry->u4IpAddr;
                        *pu4NextIssIpAuthMgrIpMask = pIpMgrEntry->u4IpMask;
                    }
                }
            }
        }
    }

    if (u4Found == ISS_TRUE)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIssIpAuthMgrOOBPort
 Input       :  The Indices
                IssIpAuthMgrIpAddr
                IssIpAuthMgrIpMask

                The Object
                retValIssIpAuthMgrOOBPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssIpAuthMgrOOBPort (UINT4 u4IssIpAuthMgrIpAddr,
                           UINT4 u4IssIpAuthMgrIpMask,
                           INT4 *pi4RetValIssIpAuthMgrOOBPort)
{
    tIssIpMgrEntry     *pIpMgrEntry;

    if ((IssFindIpAuthManager (u4IssIpAuthMgrIpAddr, u4IssIpAuthMgrIpMask,
                               &pIpMgrEntry)) == ISS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIssIpAuthMgrOOBPort = (INT4) pIpMgrEntry->u1IpAuthOobPort;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssIpAuthMgrPortList
 Input       :  The Indices
                IssIpAuthMgrIpAddr
                IssIpAuthMgrIpMask

                The Object 
                retValIssIpAuthMgrPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssIpAuthMgrPortList (UINT4 u4IssIpAuthMgrIpAddr,
                            UINT4 u4IssIpAuthMgrIpMask,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValIssIpAuthMgrPortList)
{
    tIssIpMgrEntry     *pIpMgrEntry;

    if ((IssFindIpAuthManager (u4IssIpAuthMgrIpAddr, u4IssIpAuthMgrIpMask,
                               &pIpMgrEntry)) == ISS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pRetValIssIpAuthMgrPortList->pu1_OctetList, 0,
            ISS_AUTH_PORT_LIST_SIZE);

    MEMCPY (pRetValIssIpAuthMgrPortList->pu1_OctetList,
            pIpMgrEntry->IpAuthPortList, ISS_AUTH_PORT_LIST_SIZE);

    pRetValIssIpAuthMgrPortList->i4_Length = ISS_AUTH_PORT_LIST_SIZE;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetIssIpAuthMgrVlanList
 Input       :  The Indices
                IssIpAuthMgrIpAddr
                IssIpAuthMgrIpMask

                The Object 
                retValIssIpAuthMgrVlanList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssIpAuthMgrVlanList (UINT4 u4IssIpAuthMgrIpAddr,
                            UINT4 u4IssIpAuthMgrIpMask,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValIssIpAuthMgrVlanList)
{
    tIssIpMgrEntry     *pIpMgrEntry;

    if ((IssFindIpAuthManager (u4IssIpAuthMgrIpAddr, u4IssIpAuthMgrIpMask,
                               &pIpMgrEntry)) == ISS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pRetValIssIpAuthMgrVlanList->pu1_OctetList, 0, ISS_VLAN_LIST_SIZE);

    MEMCPY (pRetValIssIpAuthMgrVlanList->pu1_OctetList,
            pIpMgrEntry->IpAuthVlanList, ISS_VLAN_LIST_SIZE);

    pRetValIssIpAuthMgrVlanList->i4_Length = ISS_VLAN_LIST_SIZE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIssIpAuthMgrAllowedServices
 Input       :  The Indices
                IssIpAuthMgrIpAddr
                IssIpAuthMgrIpMask

                The Object 
                retValIssIpAuthMgrAllowedServices
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssIpAuthMgrAllowedServices (UINT4 u4IssIpAuthMgrIpAddr,
                                   UINT4 u4IssIpAuthMgrIpMask,
                                   INT4 *pi4RetValIssIpAuthMgrAllowedServices)
{
    tIssIpMgrEntry     *pIpMgrEntry;

    if ((IssFindIpAuthManager (u4IssIpAuthMgrIpAddr, u4IssIpAuthMgrIpMask,
                               &pIpMgrEntry)) == ISS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValIssIpAuthMgrAllowedServices =
        (INT4) pIpMgrEntry->u4AllowedServices;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetIssIpAuthMgrRowStatus
 Input       :  The Indices
                IssIpAuthMgrIpAddr
                IssIpAuthMgrIpMask

                The Object 
                retValIssIpAuthMgrRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssIpAuthMgrRowStatus (UINT4 u4IssIpAuthMgrIpAddr,
                             UINT4 u4IssIpAuthMgrIpMask,
                             INT4 *pi4RetValIssIpAuthMgrRowStatus)
{
    tIssIpMgrEntry     *pIpMgrEntry;

    if ((IssFindIpAuthManager (u4IssIpAuthMgrIpAddr, u4IssIpAuthMgrIpMask,
                               &pIpMgrEntry)) == ISS_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (pIpMgrEntry->i4Status == ISS_IP_AUTH_ACTIVE)
    {
        *pi4RetValIssIpAuthMgrRowStatus = ISS_ACTIVE;
    }
    else if (pIpMgrEntry->i4Status == ISS_IP_AUTH_INACTIVE)
    {
        *pi4RetValIssIpAuthMgrRowStatus = ISS_NOT_IN_SERVICE;
    }

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetIssIpAuthMgrOOBPort
 Input       :  The Indices
                IssIpAuthMgrIpAddr
                IssIpAuthMgrIpMask

                The Object
                setValIssIpAuthMgrOOBPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssIpAuthMgrOOBPort (UINT4 u4IssIpAuthMgrIpAddr,
                           UINT4 u4IssIpAuthMgrIpMask,
                           INT4 i4SetValIssIpAuthMgrOOBPort)
{
    tIssIpMgrEntry     *pIpMgrEntry;

    if ((IssFindIpAuthManager (u4IssIpAuthMgrIpAddr, u4IssIpAuthMgrIpMask,
                               &pIpMgrEntry)) == ISS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pIpMgrEntry->u1IpAuthOobPort = (UINT1) i4SetValIssIpAuthMgrOOBPort;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssIpAuthMgrPortList
 Input       :  The Indices
                IssIpAuthMgrIpAddr
                IssIpAuthMgrIpMask

                The Object 
                setValIssIpAuthMgrPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssIpAuthMgrPortList (UINT4 u4IssIpAuthMgrIpAddr,
                            UINT4 u4IssIpAuthMgrIpMask,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValIssIpAuthMgrPortList)
{
    tIssIpMgrEntry     *pIpMgrEntry;

    if ((IssFindIpAuthManager (u4IssIpAuthMgrIpAddr, u4IssIpAuthMgrIpMask,
                               &pIpMgrEntry)) == ISS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pIpMgrEntry->IpAuthPortList, 0, ISS_AUTH_PORT_LIST_SIZE);

    MEMCPY (pIpMgrEntry->IpAuthPortList,
            pSetValIssIpAuthMgrPortList->pu1_OctetList,
            pSetValIssIpAuthMgrPortList->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssIpAuthMgrVlanList
 Input       :  The Indices
                IssIpAuthMgrIpAddr
                IssIpAuthMgrIpMask

                The Object 
                setValIssIpAuthMgrVlanList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssIpAuthMgrVlanList (UINT4 u4IssIpAuthMgrIpAddr,
                            UINT4 u4IssIpAuthMgrIpMask,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValIssIpAuthMgrVlanList)
{
    tIssIpMgrEntry     *pIpMgrEntry;

    if ((IssFindIpAuthManager (u4IssIpAuthMgrIpAddr, u4IssIpAuthMgrIpMask,
                               &pIpMgrEntry)) == ISS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    MEMSET (pIpMgrEntry->IpAuthVlanList, 0, ISS_VLAN_LIST_SIZE);

    MEMCPY (pIpMgrEntry->IpAuthVlanList,
            pSetValIssIpAuthMgrVlanList->pu1_OctetList,
            pSetValIssIpAuthMgrVlanList->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssIpAuthMgrAllowedServices
 Input       :  The Indices
                IssIpAuthMgrIpAddr
                IssIpAuthMgrIpMask

                The Object 
                   setValIssIpAuthMgrAllowedServices
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssIpAuthMgrAllowedServices (UINT4 u4IssIpAuthMgrIpAddr,
                                   UINT4 u4IssIpAuthMgrIpMask,
                                   INT4 i4SetValIssIpAuthMgrAllowedServices)
{
    tIssIpMgrEntry     *pIpMgrEntry;

    if ((IssFindIpAuthManager (u4IssIpAuthMgrIpAddr, u4IssIpAuthMgrIpMask,
                               &pIpMgrEntry)) == ISS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pIpMgrEntry->u4AllowedServices =
        (UINT4) i4SetValIssIpAuthMgrAllowedServices;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIssIpAuthMgrRowStatus
 Input       :  The Indices
                IssIpAuthMgrIpAddr
                IssIpAuthMgrIpMask

                The Object 
                setValIssIpAuthMgrRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssIpAuthMgrRowStatus (UINT4 u4IssIpAuthMgrIpAddr,
                             UINT4 u4IssIpAuthMgrIpMask,
                             INT4 i4SetValIssIpAuthMgrRowStatus)
{
    tIssIpMgrEntry     *pIpMgrEntry = NULL;

    if (i4SetValIssIpAuthMgrRowStatus != ISS_CREATE_AND_WAIT)
    {
        if (IssFindIpAuthManager (u4IssIpAuthMgrIpAddr, u4IssIpAuthMgrIpMask,
                                  &pIpMgrEntry) == ISS_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (pIpMgrEntry->i4Status == i4SetValIssIpAuthMgrRowStatus)
        {
            return SNMP_SUCCESS;
        }
    }

    switch (i4SetValIssIpAuthMgrRowStatus)
    {
        case ISS_CREATE_AND_WAIT:

            if (u4IssIpAuthMgrIpAddr == 0 && u4IssIpAuthMgrIpMask == 0)
            {
                pIpMgrEntry = ISS_GET_IP_MGR_ENTRY (0);
            }
            else
            {
                if (IssFindFreeEntryInTable (&pIpMgrEntry) == ISS_FAILURE)
                {
                    return SNMP_FAILURE;
                }
            }

            MEMSET (pIpMgrEntry, 0, sizeof (tIssIpMgrEntry));

            pIpMgrEntry->u4IpAddr = u4IssIpAuthMgrIpAddr;
            pIpMgrEntry->u4IpMask = u4IssIpAuthMgrIpMask;

            MEMSET (pIpMgrEntry->IpAuthPortList, 0xff,
                    ISS_AUTH_PORT_LIST_SIZE - 1);
            ISS_LAST_BYTE_IN_PORTLIST (pIpMgrEntry->IpAuthPortList,
                                       ISS_MAX_PORTS, ISS_AUTH_PORT_LIST_SIZE);

            MEMSET (pIpMgrEntry->IpAuthVlanList, 0xff, ISS_VLAN_LIST_SIZE - 1);
            ISS_LAST_BYTE_IN_PORTLIST (pIpMgrEntry->IpAuthVlanList,
                                       ISS_MAX_VLAN_ID, ISS_VLAN_LIST_SIZE);

            pIpMgrEntry->u4AllowedServices = ISS_ALL_MASK;
            pIpMgrEntry->u1IpAuthOobPort = ISS_FALSE;
            pIpMgrEntry->i4Status = ISS_IP_AUTH_INACTIVE;
            gIssIpMgrTable.u4Count++;

            break;

        case ISS_NOT_IN_SERVICE:

            pIpMgrEntry->i4Status = ISS_IP_AUTH_INACTIVE;
            gu4IssIpAuthActiveManagers--;

            break;

        case ISS_ACTIVE:

            pIpMgrEntry->i4Status = ISS_IP_AUTH_ACTIVE;
            gu4IssIpAuthActiveManagers++;

            break;

        case ISS_DESTROY:

            if (pIpMgrEntry->i4Status == ISS_IP_AUTH_ACTIVE)
            {
                gu4IssIpAuthActiveManagers--;
            }
            gIssIpMgrTable.u4Count--;
            pIpMgrEntry->i4Status = ISS_IP_AUTH_INVALID;

            break;

        default:

            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2IssIpAuthMgrOOBPort
 Input       :  The Indices
                IssIpAuthMgrIpAddr
                IssIpAuthMgrIpMask

                The Object
                testValIssIpAuthMgrOOBPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssIpAuthMgrOOBPort (UINT4 *pu4ErrorCode,
                              UINT4 u4IssIpAuthMgrIpAddr,
                              UINT4 u4IssIpAuthMgrIpMask,
                              INT4 i4TestValIssIpAuthMgrOOBPort)
{
    tIssIpMgrEntry     *pIpMgrEntry;

    if (IssFindIpAuthManager (u4IssIpAuthMgrIpAddr, u4IssIpAuthMgrIpMask,
                              &pIpMgrEntry) == ISS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValIssIpAuthMgrOOBPort != ISS_TRUE) &&
        (i4TestValIssIpAuthMgrOOBPort != ISS_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssIpAuthMgrPortList
 Input       :  The Indices
                IssIpAuthMgrIpAddr
                IssIpAuthMgrIpMask

                The Object 
                testValIssIpAuthMgrPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssIpAuthMgrPortList (UINT4 *pu4ErrorCode, UINT4 u4IssIpAuthMgrIpAddr,
                               UINT4 u4IssIpAuthMgrIpMask,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValIssIpAuthMgrPortList)
{
    UINT1               bu1RetVal = ISS_FALSE;
    tIssIpMgrEntry     *pIpMgrEntry;

    if ((pTestValIssIpAuthMgrPortList->i4_Length <= 0) ||
        (pTestValIssIpAuthMgrPortList->i4_Length > ISS_AUTH_PORT_LIST_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceIssIpAuthMgrTable (u4IssIpAuthMgrIpAddr,
                                                   u4IssIpAuthMgrIpMask)
        == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    ISS_IP_AUTH_IS_EXCEED_MAX_PORTS (pTestValIssIpAuthMgrPortList->
                                     pu1_OctetList,
                                     pTestValIssIpAuthMgrPortList->
                                     i4_Length, bu1RetVal);
    if (bu1RetVal == ISS_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (IssFindIpAuthManager (u4IssIpAuthMgrIpAddr, u4IssIpAuthMgrIpMask,
                              &pIpMgrEntry) == ISS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pIpMgrEntry->i4Status != ISS_IP_AUTH_INACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

 /****************************************************************************
 Function    :  nmhTestv2IssIpAuthMgrVlanList
 Input       :  The Indices
                IssIpAuthMgrIpAddr
                IssIpAuthMgrIpMask

                The Object 
                testValIssIpAuthMgrVlanList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssIpAuthMgrVlanList (UINT4 *pu4ErrorCode, UINT4 u4IssIpAuthMgrIpAddr,
                               UINT4 u4IssIpAuthMgrIpMask,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValIssIpAuthMgrVlanList)
{
    UINT1               bu1RetVal = ISS_FALSE;
    tIssIpMgrEntry     *pIpMgrEntry;

    if ((pTestValIssIpAuthMgrVlanList->i4_Length <= 0) ||
        (pTestValIssIpAuthMgrVlanList->i4_Length > ISS_VLAN_LIST_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceIssIpAuthMgrTable (u4IssIpAuthMgrIpAddr,
                                                   u4IssIpAuthMgrIpMask)
        == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    ISS_IP_AUTH_IS_EXCEED_MAX_VLANS (pTestValIssIpAuthMgrVlanList->
                                     pu1_OctetList,
                                     pTestValIssIpAuthMgrVlanList->
                                     i4_Length, bu1RetVal);
    if (bu1RetVal == ISS_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (IssFindIpAuthManager (u4IssIpAuthMgrIpAddr, u4IssIpAuthMgrIpMask,
                              &pIpMgrEntry) == ISS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pIpMgrEntry->i4Status != ISS_IP_AUTH_INACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssIpAuthMgrAllowedServices
 Input       :  The Indices
                IssIpAuthMgrIpAddr
                IssIpAuthMgrIpMask

                The Object 
                testValIssIpAuthMgrAllowedServices
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssIpAuthMgrAllowedServices (UINT4 *pu4ErrorCode,
                                      UINT4 u4IssIpAuthMgrIpAddr,
                                      UINT4 u4IssIpAuthMgrIpMask,
                                      INT4 i4TestValIssIpAuthMgrAllowedServices)
{
    tIssIpMgrEntry     *pIpMgrEntry;

    if ((UINT4) i4TestValIssIpAuthMgrAllowedServices > ISS_ALL_MASK)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceIssIpAuthMgrTable (u4IssIpAuthMgrIpAddr,
                                                   u4IssIpAuthMgrIpMask)
        == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (IssFindIpAuthManager (u4IssIpAuthMgrIpAddr, u4IssIpAuthMgrIpMask,
                              &pIpMgrEntry) == ISS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pIpMgrEntry->i4Status != ISS_IP_AUTH_INACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2IssIpAuthMgrRowStatus
 Input       :  The Indices
                IssIpAuthMgrIpAddr
                IssIpAuthMgrIpMask

                The Object 
                testValIssIpAuthMgrRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssIpAuthMgrRowStatus (UINT4 *pu4ErrorCode, UINT4 u4IssIpAuthMgrIpAddr,
                                UINT4 u4IssIpAuthMgrIpMask,
                                INT4 i4TestValIssIpAuthMgrRowStatus)
{
    tIssIpMgrEntry     *pIpMgrEntry = NULL;

    if ((i4TestValIssIpAuthMgrRowStatus == ISS_CREATE_AND_WAIT)
        && (gIssIpMgrTable.u4Count == ISS_MAX_MGR_IP_ENTRIES))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (IssValidateSubnetMask (u4IssIpAuthMgrIpMask) == ISS_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;

    }

    if (nmhValidateIndexInstanceIssIpAuthMgrTable (u4IssIpAuthMgrIpAddr,
                                                   u4IssIpAuthMgrIpMask)
        == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (IssFindIpAuthManager (u4IssIpAuthMgrIpAddr, u4IssIpAuthMgrIpMask,
                              &pIpMgrEntry) == ISS_SUCCESS)
    {
        switch (i4TestValIssIpAuthMgrRowStatus)
        {
            case ISS_CREATE_AND_WAIT:

                if ((pIpMgrEntry != NULL) ||
                    (gIssIpMgrTable.u4Count == ISS_MAX_MGR_IP_ENTRIES))
                {
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    return SNMP_FAILURE;
                }
                break;

            case ISS_NOT_IN_SERVICE:
                /* Fall through */
            case ISS_DESTROY:
                /* Fall through */
            case ISS_ACTIVE:
                if (pIpMgrEntry == NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    return SNMP_FAILURE;
                }

                break;

            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}
