/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: isscust.c,v 1.82 2017/11/10 11:08:48 siva Exp $
 *
 * Description: This file has the routines which are customer
 *              specific
 *
 *******************************************************************/
/* SOURCE FILE  :
 *
 *  ---------------------------------------------------------------------------
 * |  Copyright (C) 2006 Aricent Inc . All Rights Reserved                                 |
 * |  Copyright (C) 2006 Aricent Inc . All Rights Reserved                       |
 * |                                                                           |
 * |  FILE NAME                   :  isscustom.c                               |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR            :  Aricent Inc.                       |
 * |                                                                           |
 * |  SUBSYSTEM NAME              :  ISS                                       |
 * |                                                                           |
 * |  MODULE NAME                 :  ISS                                       |
 * |                                                                           |
 * |  LANGUAGE                    :  C                                         |
 * |                                                                           |
 * |  TARGET ENVIRONMENT          :  LINUX                                     |
 * |                                                                           |
 * |  AUTHOR                      :  ISS Team                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION                 :  This file has the routines which are      |
 * |                                 customer specific.                        |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 * 
 */
#ifndef __ISSCUST_C_
#define __ISSCUST_C_
#include "issinc.h"
#ifdef NPAPI_WANTED
#include "cfanp.h"
#include "custnp.h"
#endif
#include <sys/stat.h>
#include <dirent.h>
#include "radius.h"
#include "fsike.h"
#include "vpn.h"
#include "utilalgo.h"
#include "fsclk.h"
#include "fm.h"
#include "fsb.h"
#include "snmputil.h"
#include "fips.h"
extern tNVRAM_DATA  sNvRamData;
extern tNVRAM_AGG_MAC sNvRamAggMac[LA_DEV_MAX_TRUNK_GROUP];
extern tIssMemPoolId gIssFirmwarePoolId;
#ifdef FULCRUM_WANTED
extern INT4         InitializeFm (INT1 *pi1Param);
#endif
INT4                IssCustRemoveCliCmd (VOID);

INT4 IssHwAdaptorInit(VOID);

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSysCapabilitiesSupported                   */
/*                                                                          */
/*    Description        : This function is invoked to Get the system       */
/*                         supported capabilities                           */
/*                                                                          */
/*    Input(s)           : pu1SysCapSupported - Capability supported bit    */
/*                                              list                        */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Nonr                                             */
/****************************************************************************/
VOID
IssGetSysCapabilitiesSupported (UINT1 *pu1SysCapSupported)
{
    /* Set Capability ISS_SYS_CAPAB_BRIDGE */
    OSIX_BITLIST_SET_BIT (pu1SysCapSupported, ISS_SYS_CAPAB_BRIDGE,
                          ISS_SYS_CAPABILITIES_LEN);
    /* Set Capability ISS_SYS_CAPAB_ROUTER */
    OSIX_BITLIST_SET_BIT (pu1SysCapSupported, ISS_SYS_CAPAB_ROUTER,
                          ISS_SYS_CAPABILITIES_LEN);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSysCapabilitiesEnabled                     */
/*                                                                          */
/*    Description        : This function is invoked to Get the              */
/*                         capabilities thoes are enabled in the system     */
/*                                                                          */
/*    Input(s)           : pu1SysCapEnabled - Capability enabled bit list   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Nonr                                             */
/****************************************************************************/
VOID
IssGetSysCapabilitiesEnabled (UINT1 *pu1SysCapEnabled)
{
    /* Set Capability ISS_SYS_CAPAB_BRIDGE */
    OSIX_BITLIST_SET_BIT (pu1SysCapEnabled, ISS_SYS_CAPAB_BRIDGE,
                          ISS_SYS_CAPABILITIES_LEN);
    /* Set Capability ISS_SYS_CAPAB_ROUTER */
    OSIX_BITLIST_SET_BIT (pu1SysCapEnabled, ISS_SYS_CAPAB_ROUTER,
                          ISS_SYS_CAPABILITIES_LEN);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetHardwareVersion                            */
/*                                                                          */
/*    Description        : This function is invoked to Get the Hardware     */
/*                         version.                                         */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : UINT1 *                                          */
/****************************************************************************/
const char         *
IssGetHardwareVersion ()
{
#ifdef FULCRUM_WANTED
    return IssHwGetHwVersion ();
#else
    return sNvRamData.ai1HardwareVersion;
#endif

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetFirmwareVersion                            */
/*                                                                          */
/*    Description        : This function is invoked to Get the firmware     */
/*                         version.                                         */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : UINT1 *                                          */
/****************************************************************************/
const char         *
IssGetFirmwareVersion ()
{
    return sNvRamData.ai1FirmwareVersion;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetHardwarePartNumber */
/*                                                                          */
/*    Description        : This function is invoked to Get the Hardware     */
/*                         part Number.                                         */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : UINT1 *                                          */
/****************************************************************************/
const char         *
IssGetHardwarePartNumber ()
{
    return sNvRamData.ai1HardwarePartNumber;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSoftwareSerialNumber */
/*                                                                          */
/*    Description        : This function is invoked to Get the Software     */
/*                         Serial Number.                                         */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : UINT1 *                                          */
/****************************************************************************/
const char         *
IssGetSoftwareSerialNumber ()
{
    return sNvRamData.ai1SoftwareSerialNumber;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSoftwareVersion */
/*                                                                          */
/*    Description        : This function is invoked to Get the Software     */
/*                         Version.                                         */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : UINT1 *                                          */
/****************************************************************************/
const char         *
IssGetSoftwareVersion ()
{
    return sNvRamData.ai1SoftwareVersion;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSwitchName                                 */
/*                                                                          */
/*    Description        : This function is invoked to Get the Switch Name  */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : UINT1 *                                          */
/****************************************************************************/
const char         *
IssGetSwitchName ()
{
    return sNvRamData.ai1SwitchName;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetCustomCliPrompt                            */
/*                                                                          */
/*    Description        : This function is invoked to Get the Cli Prompt   */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : Cli Prompt string                                */
/****************************************************************************/
const char         *
IssGetCustomCliPrompt ()
{
#ifdef VCPEMGR_WANTED
    return "vCPEMgr";
#elif VCPEINS_WANTED
    return "vCPE";
#endif
    return "iss";
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetCliDisplayBanner                           */
/*                                                                          */
/*    Description        : This function is invoked to Get the Cli Banner   */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : pu1BannerMsg                                     */
/*                                                                          */
/*    Returns            : Cli Banner string                                */
/****************************************************************************/
INT1
IssGetCliDisplayBanner (CHR1 * pu1BannerMsg)
{
    UINT4               u4BytesFilled = 0;
    INT4                i4FileId = -1;
    UINT1              *pu1FormatChar = (UINT1 *) "\r\n\t\t";
    UINT1               u1FormatCharLen = 0;
    UINT1               u1TempSize = 0;
    INT4                i4FipsOperMode = 0;

    MEMSET (pu1BannerMsg, 0, CLI_MAX_BANNER_LEN);
    u1FormatCharLen = (UINT1) STRLEN (pu1FormatChar);
    if ((i4FileId = FileOpen ((CONST UINT1 *) FLASH ISS_BANNER_FILE_NAME,
                              (OSIX_FILE_RO))) >= 0)
    {
        u4BytesFilled = 0;
        u1TempSize = (UINT1) MEM_MAX_BYTES (u1FormatCharLen,
                                            CLI_MAX_BANNER_LEN - 1 -
                                            u4BytesFilled);
        MEMCPY (pu1BannerMsg + u4BytesFilled, pu1FormatChar, u1TempSize);
        u4BytesFilled += u1TempSize;
        while (u4BytesFilled < CLI_MAX_BANNER_LEN - 1)
        {
            if (FileRead (i4FileId, pu1BannerMsg + u4BytesFilled, 1) <= 0)
            {
                break;
            }

            if (pu1BannerMsg[u4BytesFilled] == '\n')
            {
                u1TempSize = (UINT1) MEM_MAX_BYTES (u1FormatCharLen,
                                                    CLI_MAX_BANNER_LEN - 1 -
                                                    u4BytesFilled);
                MEMCPY (pu1BannerMsg + u4BytesFilled, pu1FormatChar,
                        u1TempSize);
                u4BytesFilled += (UINT4) (u1TempSize - 1);
            }
            u4BytesFilled++;
        }
        pu1BannerMsg[u4BytesFilled] = '\0';
        FileClose (i4FileId);
    }

    if (u4BytesFilled <= u1FormatCharLen)
    {
        i4FipsOperMode = FipsGetFipsCurrOperMode ();
        if ((FIPS_MODE == i4FipsOperMode) || (CNSA_MODE == i4FipsOperMode))
        {
            STRNCPY (pu1BannerMsg, "----"
                     "\r\n\t\t Unauthorized access to this system is "
                     "prohibited.\n"
                     "\r\n\t\t The administrator may monitor any activity"
                     "\r\n\t\t or communication on the system and retrieve"
                     "\r\n\t\t any information stored within the system.By"
                     "\r\n\t\t accessing this system,you are consenting to"
                     "\r\n\t\t such monitoring and information retrieval."
                     "\r\n\t\t Users should have no expectation of privacy"
                     "\r\n\t\t as to any communication on or information"
                     "\r\n\t\t stored within the system."
                     "\r\n\t\t ----", CLI_MAX_BANNER_LEN - 1);
        }
        else
        {
            STRNCPY (pu1BannerMsg, "Aricent Intelligent Switch Solution",
                     CLI_MAX_BANNER_LEN - 1);
        }
    }
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSaveFileName                               */
/*                                                                          */
/*    Description        : This function is invoked to Get the configuration*/
/*                         save file Name                                   */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : UINT1 *                                          */
/****************************************************************************/
UINT1              *
IssGetSaveFileName ()
{
    return (UINT1 *) "iss.conf";
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetBgpConfSaveFileName                        */
/*                                                                          */
/*    Description        : This function is invoked to Get the configuration*/
/*                         save file Name                                   */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : UINT1 *                                          */
/****************************************************************************/
UINT1              *
IssGetBgpConfSaveFileName ()
{
    return (UINT1 *) "Bgp4CSRrestore.conf";
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetCsrConfSaveFileName                        */
/*                                                                          */
/*    Description        : This function is invoked to Get the configuration*/
/*                         save file Name                                   */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : UINT1 *                                          */
/****************************************************************************/
UINT1              *
IssGetCsrConfSaveFileName ()
{
    return (UINT1 *) "OspfCSRrestore.conf";
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetOspfConfSaveFileName                        */
/*                                                                          */
/*    Description        : This function is invoked to Get the configuration*/
/*                         save file Name                                   */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : UINT1 *                                          */
/****************************************************************************/
UINT1              *
IssGetOspfConfSaveFileName ()
{
    return (UINT1 *) "Ospf3CSRrestore.conf";
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetDlImageName                                */
/*                                                                          */
/*    Description        : This function is invoked to Get the DownLoad     */
/*                         Image Name                                       */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : UINT1 *                                          */
/****************************************************************************/
const char         *
IssGetDlImageName ()
{
    return "iss.exe";
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetImageName                                  */
/*                                                                          */
/*    Description        : This function is invoked to Get the Current      */
/*                         Image Name                                       */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : UINT1 *                                          */
/****************************************************************************/
const char         *
IssGetImageName ()
{
    return "ISS.exe";
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetUlLogFileName                              */
/*                                                                          */
/*    Description        : This function is invoked to Get the Upload Log   */
/*                         File  Name                                       */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : UINT1 *                                          */
/****************************************************************************/
const char         *
IssGetUlLogFileName ()
{
    return "iss.log";
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSysLocation                                */
/*                                                                          */
/*    Description        : This function is invoked to Get the Sys Location */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : string containing switch Location                */
/****************************************************************************/
const char         *
IssGetSysLocation ()
{
    return "ARICENT";
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSysContact                                 */
/*                                                                          */
/*    Description        : This function is invoked to Get the Sys Contact  */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : String containing switch contact id              */
/****************************************************************************/

const char         *
IssGetSysContact ()
{
    return "info@aricent.com";
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : TgtCustomStartup                                 */
/*                                                                          */
/*    Description        : This function is invoked to perform any target   */
/*                         specific initialization / startup functions      */
/*                                                                          */
/*    Input(s)           : pi1Dummy - Dummy argument                        */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
TgtCustomStartup (INT1 *pi1Dummy)
{
    INT4                i4ErrCode = 0;
    UINT1               u1AlgoIndex = 0;
    CHR1               *pc1PbitResult = "PASS";
    DIR                *pLogDir = NULL;
    UINT1              *pu1FlashLogFilePath;
    UINT1               au1LogFilePath[ISS_CONFIG_FILE_NAME_LEN + 1];
    INT4                i4FipsOperMode = 0;

    UNUSED_PARAM (pc1PbitResult);
    UNUSED_PARAM (i4ErrCode);
    MEMSET (au1LogFilePath, 0, ISS_CONFIG_FILE_NAME_LEN + 1);
    pu1FlashLogFilePath = (UINT1 *) &au1LogFilePath[0];

    if ((pLogDir = opendir (ISS_LOG_DIRECTORY)) == NULL)
    {
        if (OsixCreateDir (ISS_LOG_DIRECTORY) == OSIX_FAILURE)
        {
            PRINTF ("Error in creation of LogDir\n");
        }
    }
    else
    {
        closedir (pLogDir);
    }

    /* Initial Call will be called with zero */
    if (*pi1Dummy == 0)
    {
        lrInitComplete (OSIX_SUCCESS);
#if defined (LNXIP4_WANTED) || defined (IP_WANTED)
        /* IpUp Event is given by IP Modules */
#else
        CfaNotifyIpUp ();
#endif
    }
    else if (*pi1Dummy == ISS_SPAWN_MSR_TASK)
    {
        /* Customer specific initialisations can be parked here.
         * This condition will hit after CFA processed IP_UP_EVENT
         * and h/w is initialised
         * CliExecuteAppCmd can be called if RM is not used.
         */
    }
    else if (*pi1Dummy == ISS_SPAWN_MSR_RM_TASK)
    {
        /* Customer specific initialisations can be parked here 
         * if RM is enabled. This flow will hit after node becomes 
         * active. So calling of CliExecuteAppCmd to be called only 
         * in this switch case (where RM is used) 
         */
        return;
    }

    /*Remove commands from the CLI framework */
    IssCustRemoveCliCmd ();

    /* Enabling the support for the security algorithm in hardware */
    for (u1AlgoIndex = 0; u1AlgoIndex < UTIL_MAX_ALGOS; u1AlgoIndex++)
    {
        if ((u1AlgoIndex == ISS_UTIL_ALGO_AES)
            || (u1AlgoIndex == ISS_UTIL_ALGO_AES_CBC_MAC96)
            || (u1AlgoIndex == ISS_UTIL_ALGO_AES_CBC_MAC128)
            || (u1AlgoIndex == ISS_UTIL_ALGO_MD5_ALGO))
        {
            UtilSetAlgoSupportInHw (u1AlgoIndex, OSIX_FALSE);
        }
        else
        {
            UtilSetAlgoSupportInHw (u1AlgoIndex, OSIX_TRUE);
        }
    }

    /*
     * powerup self-test is applicable for only FIPS mode. also to reduce
     * the boot-time in NonFIPS mode.
     */
    i4FipsOperMode = FipsGetFipsCurrOperMode ();
    if ((FIPS_MODE == i4FipsOperMode) || (CNSA_MODE == i4FipsOperMode))
    {
        /* Do FIPS Power up self-test */
#ifndef EXT_CRYPTO_WANTED
        if (FipsApiRunKnownAnsTest (FIPS_KNOWN_ANS_TEST_ALL, &i4ErrCode)
            == OSIX_FAILURE)
        {
            /* Update self-test status */
            pc1PbitResult = "FAIL";
            IssCustFipsErrHandler (ISS_FIPS_SELF_TEST, ISS_FAILURE);
        }
        IssCustFipsErrHandler (ISS_FIPS_SELF_TEST, ISS_SUCCESS);
        IssCustLedSet (pc1PbitResult);

        /* setting the global variable to indicate power-on self test completed */
        FipsSetKnownAnsTestFlags (FIPS_KAT_POWER_ON_STATUS);
#endif
    }
    /* Generate a random number during power-up and save it for 
     * comparison with next generate random number. Conditioanal
     * Test(CBIT) as described in FIPS PUB 140-2 */
    UtilRand ();

    if (STRCMP (sNvRamData.au1FlashLoggingFilePath, "") != 0)
    {
        if (nmhSetIssLoggingOption (FLASH_LOG_LOCATION) == SNMP_FAILURE)
        {
            return;
        }

        IssSetFlashLoggingLocation (pu1FlashLogFilePath);
    }

    if (STRCMP (sNvRamData.au1ImageDumpFilePath, "") != 0)
    {
        INT1                i1Count = 0;
        while (sNvRamData.au1ImageDumpFilePath[i1Count] != '\0')
        {
            if (!(ISALPHA (sNvRamData.au1ImageDumpFilePath[i1Count]) ||
                  ISDIGIT (sNvRamData.au1ImageDumpFilePath[i1Count]) ||
                  ISFWDSLASH (sNvRamData.au1ImageDumpFilePath[i1Count])))
            {
                return;
            }
            i1Count++;
        }
        OsixCreateDir ((const CHR1 *) sNvRamData.au1ImageDumpFilePath);

        OsixCoreDumpPathSetting ((UINT1 *) sNvRamData.au1ImageDumpFilePath);
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssCustRegisterEvtCallBk                         */
/*                                                                          */
/*    Description        : This function is to be invoked by application to */
/*                         register call back functions for required events */
/*                                                                          */
/*                         ISS protocols will call the function pointer     */
/*                         when this event occurs.                          */
/*                                                                          */
/*    Input(s)           : u4ModuleId - ISS module name                     */
/*                         u4Event - Event for which callback is registered */
/*                         pFsCbInfo - Pointer to call back information     */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ISS_FAILURE                          */
/****************************************************************************/
INT4
IssCustRegisterEvtCallBk (UINT4 u4ModuleId, UINT4 u4Event,
                          tFsCbInfo * pFsCbInfo)
{
    INT4                i4RetVal = ISS_FAILURE;

    switch (u4ModuleId)
    {
        case ISS_SYSTEM_MODULE:
            i4RetVal = IssCallBackRegister (u4Event, pFsCbInfo);
            break;

        case ISS_SNMP_MODULE:
#ifdef SNMP_3_WANTED
            i4RetVal = SnmpCallBackRegister (u4Event, pFsCbInfo);

            if (SNMP_FAILURE == i4RetVal)
            {
                i4RetVal = ISS_FAILURE;
            }
            else
            {
                i4RetVal = ISS_SUCCESS;
            }
#endif
            break;

        case ISS_LA_MODULE:
            i4RetVal = LaCallBackRegister (u4Event, pFsCbInfo);

            if (LA_FAILURE == i4RetVal)
            {
                i4RetVal = ISS_FAILURE;
            }
            else
            {
                i4RetVal = ISS_SUCCESS;
            }
            break;

#ifdef SYSLOG_WANTED
        case ISS_SYSLOG_MODULE:
            i4RetVal = SysLogCallBackRegister (u4Event, pFsCbInfo);

            if (SYSLOG_FAILURE == i4RetVal)
            {
                i4RetVal = ISS_FAILURE;
            }
            else
            {
                i4RetVal = ISS_SUCCESS;
            }
            break;
#endif

        case ISS_MSR_MODULE:
            i4RetVal = MsrUtilCallBackRegister (u4Event, pFsCbInfo);
            if (MSR_FAILURE == i4RetVal)
            {
                i4RetVal = ISS_FAILURE;
            }
            else
            {
                i4RetVal = ISS_SUCCESS;
            }
            break;
#ifdef CLKIWF_WANTED
        case ISS_CLKIWF_MODULE:
            i4RetVal = ClkIwfCallBackRegister (u4Event, pFsCbInfo);

            if (OSIX_FAILURE == i4RetVal)
            {
                i4RetVal = ISS_FAILURE;
            }
            else
            {
                i4RetVal = ISS_SUCCESS;
            }
            break;
#endif
        case ISS_FM_MODULE:
            i4RetVal = FmCallbackRegister (pFsCbInfo);
            if (OSIX_FAILURE == i4RetVal)
            {
                i4RetVal = ISS_FAILURE;
            }
            else
            {
                i4RetVal = ISS_SUCCESS;
            }
            break;
#ifdef FSB_WANTED
        case ISS_FSB_MODULE:
            i4RetVal = FsbCallBackRegister (u4Event, pFsCbInfo);

            if (FSB_FAILURE == i4RetVal)
            {
                i4RetVal = ISS_FAILURE;
            }
            else
            {
                i4RetVal = ISS_SUCCESS;
            }
            break;
#endif
        default:
            break;
    }

    return i4RetVal;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssCustInit                                      */
/*                                                                          */
/*    Description        : This function is invoked to update system        */
/*                         parameters                                       */
/*                                                                          */
/*    Input(s)           : argc, argv (command line arguments)              */
/*                                                                          */
/*    Output(s)          : VOID                                             */
/*                                                                          */
/*    Returns            : VOID                                             */
/****************************************************************************/
VOID
IssCustInit (int argc, char *argv[])
{
#ifdef FULCRUM_WANTED

    char               *pu1Insmod_str = NULL;
    char               *pu1Focalpoint_str = NULL;
    UINT4               u4EnvLen = 0;
    UINT4               u4MallocLen = 0;
#endif
    INT4                i4FipsOperMode = 0;

    UNUSED_PARAM (argc);
    UNUSED_PARAM (argv);


 	/* Enet adaptor init*/
	if (IssHwAdaptorInit() != ISS_SUCCESS)
	{
		printf("DPLInit failed!");
		return;
	}

    /* When the FIPS is supported, the status will be taken
     * from the appropriate Hardware API and updated */
    IssCustGetFipsCurrOperMode (&i4FipsOperMode);

    /* In FIPS mode, Self test will be run on bootup,
     * SNMP read/write communities will be blocked,
     * CLI console will be disabled */
    if ((FIPS_MODE == i4FipsOperMode) || (CNSA_MODE == i4FipsOperMode))
    {
#ifdef SNMP_3_WANTED
        SnmpApiSetCommunityCtrl (OSIX_TRUE);
#endif
        IssSetCliSerialConsoleToNvRam (ISS_CLI_SERIAL_CONSOLE_DISABLE);
    }

    MEMSET (&gau1UtilHwSupportForAlgo, OSIX_FALSE, UTIL_MAX_ALGOS);

#ifdef FULCRUM_WANTED
#ifdef RRC_WANTED
    UNUSED_PARAM (*pu1Insmod_str);
    UNUSED_PARAM (*pu1Focalpoint_str);
    UNUSED_PARAM (u4EnvLen);
    UNUSED_PARAM (u4MallocLen);
#else
    if (system ("lsmod | grep focalpoint >/dev/null") != 0)
    {
        pu1Focalpoint_str = getenv ("FOCALPOINT_PATH");

        if (pu1Focalpoint_str == NULL)
        {
            /* User has not provided the path, Set the default path for focalpoint.ko */
            pu1Focalpoint_str = "/fulcrum";
        }
        else
        {
            /* Remove string terminator */
            pu1Focalpoint_str[STRLEN (pu1Focalpoint_str)] = '\0';
        }

        u4EnvLen = STRLEN (pu1Focalpoint_str);

        u4MallocLen = STRLEN ("insmod ") + u4EnvLen + STRLEN ("/focalpoint.ko");
        pu1Insmod_str = MEM_MALLOC (u4MallocLen, char);
        if (pu1Insmod_str == NULL)
        {
            PRINTF ("Memory allocation failed for insmod string\n");
            return;
        }
        MEMSET (pu1Insmod_str, 0, u4MallocLen);
        SPRINTF (pu1Insmod_str, "%s%s%s", "insmod ", pu1Focalpoint_str,
                 "/focalpoint.ko");
        system (pu1Insmod_str);
        MEM_FREE (pu1Insmod_str);
    }
#endif

    InitializeFm (NULL);

#endif

    return;
}

/******************************************************************************/
/* Function Name     : IssCustGetConfigPrompt                                 */
/*                                                                            */
/* Description       : This function validates the given pi1ModeName          */
/*                     and returns the prompt in pi1DispStr if valid,         */
/*                                                                            */
/* Input Parameters  : pi1ModeName - Mode Name to validate                    */
/*                                                                            */
/* Output Parameters : pi1DispStr - Display string.                           */
/*                                                                            */
/* Return Value      : ISS_SUCCESS/ISS_FAILURE                                */
/******************************************************************************/
INT1
IssCustGetConfigPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UNUSED_PARAM (pi1ModeName);
    UNUSED_PARAM (pi1DispStr);
    /*This needs to be defined for Customization */
    return ISS_FAILURE;
}

/******************************************************************************/
/* Function Name     : IssCustMsrInit                                         */
/*                                                                            */
/*                                                                            */
/* Description       : This function posts an event to the MSR task to        */
/*                     initiate restoration of the saved configuration.       */
/*                                                                            */
/* Input Parameters  : None                                                   */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : ISS_SUCCESS/ISS_FAILURE                                */
/******************************************************************************/
INT4
IssCustMsrInit ()
{
    IssMsrInit ();
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetAggregatorMac                              */
/*                                                                          */
/*    Description        : This function is invoked to get the Aggregator   */
/*                         MAC address from the ISS Group Information.      */
/*                                                                          */
/*    Input(s)           : u2AggIndex - Aggregator Index.                   */
/*                                                                          */
/*    Output(s)          : AggrMac.                                         */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
INT4
IssGetAggregatorMac (UINT2 u2AggIndex, tMacAddr AggrMac)
{
    UINT4               u4MacIndex;

    u4MacIndex = (UINT4) (u2AggIndex - SYS_DEF_MAX_PHYSICAL_INTERFACES - 1);
    if (u4MacIndex < LA_DEV_MAX_TRUNK_GROUP)
    {
        ISS_MEMCPY (AggrMac, sNvRamAggMac[u4MacIndex].au1AggregatorMac,
                    CFA_ENET_ADDR_LEN);
    }
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssFreeAggregatorMac                             */
/*                                                                          */
/*    Description        : This function is invoked to free the Aggregator  */
/*                         MAC address.                                     */
/*                                                                          */
/*    Input(s)           : u2AggIndex - Aggregator Index.                   */
/*                                                                          */
/*    Output(s)          : AggrMac.                                         */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssFreeAggregatorMac (UINT4 u4AggIndex)
{
    UNUSED_PARAM (u4AggIndex);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssCustConfControlCli                            */
/*                                                                          */
/*    Description        : This function is used to restrict certain        */
/*                         configurations through CLI                       */
/*                                                                          */
/*    Input(s)           : pIssCustCliConfInfo -Pointer to the cli conf     */
/*                         structure.                                       */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ ISS_FAILURE                         */
/****************************************************************************/

INT4
IssCustConfControlCli (tIssCustCliConfInfo * pIssCustCliConfInfo)
{
    UNUSED_PARAM (pIssCustCliConfInfo);
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssCustConfControlWebnm                          */
/*                                                                          */
/*    Description        : This function is used to restrict certain        */
/*                         configurations through Webnm                     */
/*                                                                          */
/*    Input(s)           : pHttpValue -Pointer to the global HTTP data      */
/*                         structure.                                       */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ENM_SUCCESS/ ENM_FAILURE                         */
/****************************************************************************/
INT4
IssCustConfControlWebnm (VOID *pHttpValue)
{
    UNUSED_PARAM (pHttpValue);
    return ENM_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssCustConfControlSnmp                           */
/*                                                                          */
/*    Description        : This function is used to restrict certain        */
/*                         configurations through SNMP                      */
/*                                                                          */
/*    Input(s)           : pIssCustSnmpConfInfo -Pointer to the snmp conf   */
/*                         structure.                                       */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ ISS_FAILURE                         */
/****************************************************************************/

INT4
IssCustConfControlSnmp (tIssCustSnmpConfInfo * pIssCustSnmpConfInfo)
{
    UNUSED_PARAM (pIssCustSnmpConfInfo);
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssCustConfControlSnmpGet                        */
/*                                                                          */
/*    Description        : This function is used to implement specific      */
/*                         actions when invoked in the SNMPGet              */
/*                                                                          */
/*    Input(s)           : pIssCustSnmpConfInfo -Pointer to the snmp conf   */
/*                         structure.                                       */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ ISS_FAILURE                         */
/****************************************************************************/
INT4
IssCustConfControlSnmpGet (tIssCustSnmpConfInfo * pIssCustSnmpConfInfo)
{
    UNUSED_PARAM (pIssCustSnmpConfInfo);
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssCustDetermineSpeed                            */
/*                                                                          */
/*    Description        : This function returns the interface speed        */
/*                                                                          */
/*    Input(s)           : u4IfIndex - interface index of the interface     */
/*                               whose speed needs to be determine.   */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ ISS_FAILURE                         */
/****************************************************************************/
INT4
IssCustDetermineSpeed (UINT4 u4IfIndex, UINT4 *pu4IfSpeed,
                       UINT4 *pu4IfHighSpeed)
{
    UNUSED_PARAM (u4IfIndex);

    *pu4IfSpeed = CFA_ENET_SPEED;
    *pu4IfHighSpeed = (CFA_ENET_SPEED / CFA_1MB);
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssCustGetEthernetType                           */
/*                                                                          */
/*    Description        : This function returns the ethernet type          */
/*                                                                          */
/*    Input(s)           : u4IfIndex - interface index of the interface     */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : Ethernet Type                                    */
/****************************************************************************/
INT4
IssCustGetEthernetType (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return CFA_GI_ENET;
}

/************************************************************************/
/*  Function Name   : IssAppendFilePath                                 */
/*  Description     : To append input filename with FLASH               */
/*                                                                      */
/*  Input           : pu1FileName                                       */
/*                                                                      */
/*  Output          : pu1FileName                                       */
/*                                                                      */
/*  Returns         : ISS_SUCCESS/ISS_FAILURE                           */
/************************************************************************/
INT4
IssAppendFilePath (UINT1 *pu1FileName)
{
    UINT1               au1Temp[ISS_CONFIG_FILE_NAME_LEN];

    ISS_MEMSET (au1Temp, 0, sizeof (au1Temp));
    SPRINTF ((CHR1 *) au1Temp, "%s%s", FLASH, pu1FileName);

    pu1FileName = &au1Temp[0];
    return ISS_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IssCustReadFile                          */
/*  Description     : This function read the file and pass to  */
/*                    kernel                                   */
/*  Input(s)        : i4ImageType -> Normal / Fallback         */
/*  Returns         : ISS_SUCCESS/ISS_FAILURE                  */
/***************************************************************/

INT4
IssCustReadFile (UINT1 *u1FirmwareImageName)
{
#ifndef MRVLLS
    FILE               *fp = NULL;
    struct stat         fStatBuf;
    UINT1              *pu1BufferToKernel = NULL;
    UINT4               u4BufSize = 0;
    INT4                i4ImageType = 0;

    UNUSED_PARAM (u1FirmwareImageName);
    UNUSED_PARAM (i4ImageType);
    UNUSED_PARAM (u4BufSize);
    if (STRCMP (gu1FirmwareImageName, ISS_FIRMWARE_NORMAL))
    {
        fp = FOPEN ("/tmp/normal", "r");
        i4ImageType = ISS_FLASH_NORMAL;
    }
    else
    {
        fp = FOPEN ("/tmp/fallback", "r");
        i4ImageType = ISS_FLASH_FALLBACK;
    }

    if (fp == NULL)
    {
        PRINTF ("\r Error in opening the file \n");
        return ISS_FAILURE;
    }
    /* Get the total size of the file in bytes */

    if (STRCMP (gu1FirmwareImageName, ISS_FIRMWARE_NORMAL))
    {
        if (stat ("/tmp/normal", &fStatBuf) < 0)
        {
            fclose (fp);
            return ISS_FAILURE;
        }
    }
    else
    {
        if (stat ("/tmp/fallback", &fStatBuf) < 0)
        {
            fclose (fp);
            return ISS_FAILURE;
        }
    }
    if (ISS_FIRMWARE_ALLOC_MEM_BLOCK (pu1BufferToKernel) == NULL)
    {
        fclose (fp);
        return ISS_FAILURE;
    }
    ISS_MEMSET (pu1BufferToKernel, 0, fStatBuf.st_size);
    if (fread (pu1BufferToKernel, (size_t) fStatBuf.st_size, 1, fp) !=
        (UINT4) fStatBuf.st_size)
    {
        PRINTF ("\r Error in reading the file \n");
    }
    fclose (fp);

    u4BufSize = (UINT4) fStatBuf.st_size;
#ifdef NPAPI_WANTED
#ifdef MBSM_WANTED
    if (IsssysIssHwSendBufferToLinux (pu1BufferToKernel, u4BufSize,
                                      i4ImageType) != FNP_SUCCESS)
    {
        PRINTF ("\r Send Buffer to Linux Failed \n");
        ISS_FIRMWARE_FREE_MEM_BLOCK (pu1BufferToKernel);
        return ISS_FAILURE;
    }
#endif
#endif
    ISS_FIRMWARE_FREE_MEM_BLOCK (pu1BufferToKernel);
#else
    UNUSED_PARAM (u1FirmwareImageName);
#endif
    return ISS_SUCCESS;
}

INT4
IssCustAccessHwConsole (UINT1 *pu1HwCmd)
{
#ifdef NPAPI_WANTED
    INT4                i4RetVal = 0;
    UINT1              *pu1BufferToKernel = NULL;

    if (ISS_FIRMWARE_ALLOC_MEM_BLOCK (pu1BufferToKernel) == NULL)
    {
        return ISS_FAILURE;
    }

    ISS_MEMSET (pu1BufferToKernel, 0, MAX_HW_CMD_LEN);
    ISS_MEMCPY (pu1BufferToKernel, pu1HwCmd, MAX_HW_CMD_LEN);

    if ((i4RetVal =
         IsssysIssHwEnableHwConsole (pu1BufferToKernel)) != FNP_SUCCESS)
    {
        ISS_FIRMWARE_FREE_MEM_BLOCK (pu1BufferToKernel);
        return ISS_FAILURE;
    }
    ISS_FIRMWARE_FREE_MEM_BLOCK (pu1BufferToKernel);
#else
    UNUSED_PARAM (pu1HwCmd);
#endif

    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssCustGetCurrTemperature                        */
/*                                                                          */
/*    Description        : This function is invoked to get the current      */
/*                         temperature of the switch.                       */
/*                                                                          */
/*    Input(s)           : pi4CurrTemperature - Current temperature         */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ ISS_FAILURE                         */
/****************************************************************************/

INT4
IssCustGetCurrTemperature (INT4 *pi4CurrTemperature)
{
#ifdef NPAPI_WANTED
    INT4                i4IssSwitchNpRetVal = FNP_SUCCESS;

    /* Checking whether temperature status is set */
    if (gIssSysCapSupport.u1TempStatusSupport != OSIX_TRUE)
    {
        return ISS_SUCCESS;
    }
    i4IssSwitchNpRetVal = IssHwGetCurrTemperature (pi4CurrTemperature);
    if (i4IssSwitchNpRetVal == FNP_FAILURE)
    {
        return ISS_FAILURE;
    }
#else
    *pi4CurrTemperature = ISS_SWITCH_DEFAULT_TEMPERATURE;
#endif
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssCustGetCurrPowerSupply                        */
/*                                                                          */
/*    Description        : This function is invoked to get the current      */
/*                         power supply of the switch.                      */
/*                                                                          */
/*    Input(s)           : pu4CurrPowerSupply - Current power supply        */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ ISS_FAILURE                         */
/****************************************************************************/

INT4
IssCustGetCurrPowerSupply (UINT4 *pu4CurrPowerSupply)
{
#ifdef NPAPI_WANTED
    INT4                i4IssSwitchNpRetVal = FNP_SUCCESS;

    /* Checking whether power supply status is set */
    if (gIssSysCapSupport.u1PowerStatusSupport != OSIX_TRUE)
    {
        return ISS_SUCCESS;
    }

    i4IssSwitchNpRetVal = IssHwGetCurrPowerSupply (pu4CurrPowerSupply);
    if (i4IssSwitchNpRetVal == FNP_FAILURE)
    {
        return ISS_FAILURE;
    }
#else
    *pu4CurrPowerSupply = ISS_SWITCH_DEFAULT_POWER_SUPPLY;
#endif
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssCustGetFanStatus                              */
/*                                                                          */
/*    Description        : This function is invoked to get the current      */
/*                         Fan Status of the switch.                        */
/*                                                                          */
/*    Input(s)           : u4FanIndex - Fan Index                           */
/*                         pu4FanStatus - Fan Status                        */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ ISS_FAILURE                         */
/****************************************************************************/

INT4
IssCustGetFanStatus (UINT4 u4FanIndex, UINT4 *pu4FanStatus)
{
#ifdef NPAPI_WANTED
    INT4                i4IssSwitchNpRetVal = FNP_SUCCESS;

    /* Checking whether Fan Status is set */
    if (gIssSysCapSupport.u1FanStatusSupport != OSIX_TRUE)
    {
        return ISS_SUCCESS;
    }

    i4IssSwitchNpRetVal = IsssysIssHwGetFanStatus (u4FanIndex, pu4FanStatus);
    if (i4IssSwitchNpRetVal == FNP_FAILURE)
    {
        return ISS_FAILURE;
    }
#else
    UNUSED_PARAM (u4FanIndex);
    *pu4FanStatus = ISS_FAN_UP;
#endif

    return ISS_SUCCESS;
}

/******************************************************************************
 * Function           : IssCustGetSlotMACAddress
 * Input(s)           : u4SwitchId - Slot number
 *                      pu1SwitchHwAddr - switch mac address.
                        Memory is allocated for this variable by the caller.
 * Output(s)          : None
 * Returns            : ISS_SUCCESS/ISS_FAILURE
 * Action             : To return the Mac address of the switch for the given
 *                      Slot number.
 ******************************************************************************/
INT4
IssCustGetSlotMACAddress (UINT4 u4SwitchId, UINT1 *pu1SwitchHwAddr)
{
    UNUSED_PARAM (u4SwitchId);
    UNUSED_PARAM (pu1SwitchHwAddr);
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetVRMacAddr                                  */
/*                                                                          */
/*    Description        : This function is invoked to get the Virtual      */
/*                         Router MAC address.                              */
/*                                                                          */
/*    Input(s)           : u4ContextId - Virtual Router Id.                 */
/*                                                                          */
/*    Output(s)          : VRMacAddr    - Virtual Router Mac address.       */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
INT4
IssGetVRMacAddr (UINT4 u4ContextId, tMacAddr * pVrUnqMacAddr)
{
    UINT1               u1VrfMacFlag;

    u1VrfMacFlag = IssGetVrfUnqMacOptionFromNvRam ();
    if (u1VrfMacFlag == ISS_VRF_UNQ_MAC_DISABLE)
    {
        ISS_MEMCPY (pVrUnqMacAddr, gIssSysGroupInfo.BaseMacAddr, MAC_ADDR_LEN);
    }
    else
    {
        /* To provide unique mac for each Virtual routers */
        IssGetContextMacAddress (u4ContextId, *pVrUnqMacAddr);
    }
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssCustValidateAction                            */
/*                                                                          */
/*    Description        : This function is invoked to test if the config   */
/*                         is allowed.                                      */
/*                                                                          */
/*    Input(s)           : pOid - pointer to the oid string                 */
/*                                                                          */
/*    Output(s)          : pMultiIndex - which contains the index info      */
/*                         for the oid that is accessed                     */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ISS_FAILURE                          */
/****************************************************************************/
INT4
IssCustValidateAction (UINT1 *pOid, tSnmpIndex * pMultiIndex,
                       tSNMP_MULTI_DATA_TYPE * pMultiData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pMultiIndex);
    UNUSED_PARAM (pMultiData);
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssCustCheckLogOption                            */
/*                                                                          */
/*    Description        : This custom function is used to get the custom   */
/*                         audit log enable/disable option.                 */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : pu4StorageChkFlag   - Denotes file or dir storage*/
/*                         pu4OverWriteChkFlag - Check flag for overwrite   */
/*    Returns            : ISS_TRUE/ISS_FALSE.                              */
/****************************************************************************/

VOID
IssCustCheckLogOption (UINT4 *pu4StorageChkFlag, UINT4 *pu4OverWriteFlag)
{
    *pu4StorageChkFlag = LOG_FILE_STORAGE;
    *pu4OverWriteFlag = ISS_TRUE;
    return;
}

/***************************************************************/
/*  Function Name   : IssCustSetFipsCurrOperMode               */
/*                                                             */
/*  Description     : This function calls the appropriate      */
/*                    Hardware API to set the current operating*/
/*                    mode                                     */
/*                                                             */
/*  Input(s)        : i4OperMode                               */
/*                      Current Operating Mode. This value can */
/*                      be either FIPS_MODE or LEGACY_MODE     */
/*                      for which the Hardware API needs to be */
/*                      invoked                                */
/*                                                             */
/*  Output(s)       : None                                     */
/*                                                             */
/*  Returns         : ISS_SUCCESS or ISS_FAILURE               */
/***************************************************************/
INT4
IssCustSetFipsCurrOperMode (INT4 i4OperMode)
{
    IssSetFipsOperModeToNvRam (i4OperMode);
    return ISS_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IssCustGetFipsCurrOperMode               */
/*                                                             */
/*  Description     : This function calls the appropriate      */
/*                    Hardware API to get the current operating*/
/*                    mode                                     */
/*                                                             */
/*  Input(s)        : None                                     */
/*                                                             */
/*  Output(s)       : pi4OperMode                              */
/*                      Current Operating Mode. This value can */
/*                      be either FIPS_MODE or LEGACY_MODE   */
/*                                                             */
/*  Returns         : ISS_SUCCESS                              */
/***************************************************************/
INT4
IssCustGetFipsCurrOperMode (INT4 *pi4OperMode)
{
    *pi4OperMode = IssGetFipsOperModeFromNvRam ();
    return ISS_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IssCustCheckSwitchToFipsMode             */
/*                                                             */
/*  Description     : This function calls the appropriate      */
/*                    Hardware API to get the status of the    */
/*                    NVMRO signal assertion and returns the   */
/*                    value, whether the switching fo FIPS     */
/*                    module is possible or not                */
/*                                                             */
/*  Input(s)        : None                                     */
/*                                                             */
/*  Output(s)       : None                                     */
/*                                                             */
/*  Returns         : ISS_TRUE or ISS_FALSE                    */
/***************************************************************/
INT4
IssCustCheckSwitchToFipsMode (VOID)
{
    return ISS_TRUE;
}

/***************************************************************/
/*  Function Name   : IssCustFipsErrHandler                    */
/*                                                             */
/*  Description     : This is the main error handler function. */
/*                      This function calls the appropriate    */
/*                      Hardware API to set the LED status.    */
/*                      For the incoming events, corresponding */
/*                      LEDs will be set and the necessary     */
/*                      actions will be taken                  */
/*                                                             */
/*  Input(s)        : u4Event                                  */
/*                      This is a unique value to identify     */
/*                      which event triggered this function.   */
/*                                                             */
/*                    u4Value                                  */
/*                      This value is to identify what value   */
/*                      needs to be set to the selected LED.   */
/*                      Based on this value, necessary actions */
/*                      will be taken                          */
/*                                                             */
/*  Output(s)       : None                                     */
/*                                                             */
/*  Returns         : ISS_SUCCESS or ISS_FAILURE               */
/***************************************************************/
INT4
IssCustFipsErrHandler (UINT4 u4Event, UINT4 u4Value)
{
    UNUSED_PARAM (u4Event);
    UNUSED_PARAM (u4Value);
    return ISS_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name     : IssCustSystemRestart                              */
/*                                                                          */
/*    Description       : This custom function is to restart the switch     */
/*                                                                          */
/*    Input(s)          : None                                              */
/*                                                                          */
/*    Output(s)         : None                                              */
/*                                                                          */
/*    Returns           : None                                              */
/****************************************************************************/
VOID
IssCustSystemRestart (VOID)
{
    return;
}

/*****************************************************************************/
/* Function Name      : IssCustConfigRestore                                 */
/*                                                                           */
/* Description        : This function is invoked to perform any customer     */
/*                    : specific initialization and configuration            */
/*                    : after MSR completion                                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IssCustConfigRestore (VOID)
{
#ifdef RADIUS_WANTED
    RadApiRestoreSharedSecretFromNvRam ();
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : IssCustLedSet                                        */
/*                                                                           */
/* Description        : This function is invoked to set led to green / red   */
/*                      state based on the result of PBIT, CBIT, IBIT tests. */
/*                                                                           */
/* Input(s)           : pc1Result - Result of Test (PBIT, CBIT, IBIT)        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ ISS_FAILURE                             */
/*****************************************************************************/

INT4
IssCustLedSet (CHR1 * pc1Result)
{
    UNUSED_PARAM (pc1Result);
    return ISS_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssCustLocalLoggingCheck                         */
/*                                                                          */
/*    Description        : This function is invoked to check whether the    */
/*                         logging locally should be done or not.           */
/*                                                                          */
/*    Input(s)           : i4Module - Module that is logging                */
/*                         u4SeverityLevel - Severity level of the log      */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ISS_FAILURE if the syslog server is configured   */
/*                         for that severity level, otherwise ISS_SUCCESS   */
/*                                                                          */
/****************************************************************************/
INT4
IssCustLocalLoggingCheck (UINT4 u4SeverityLevel)
{
    UNUSED_PARAM (u4SeverityLevel);
    return ISS_SUCCESS;
}

/******************************************************************************/
/* Function Name     : IssCustRegisterNvramCallBk                             */
/*                                                                            */
/* Description       : This function registers callback functions for NVRAM   */
/*                     read and write which is required to read and write add */
/*                     itional details specific to  customer                  */
/* Input Parameters  : None                                                   */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : None                                                   */
/******************************************************************************/
VOID
IssCustRegisterNvramCallBk (VOID)
{
    ISS_NVRAM_CALLBACK[ISS_NVRAM_CUST_READ_EVENT].pIssCustReadNvram
        = IssCustNvramRead;
    ISS_NVRAM_CALLBACK[ISS_NVRAM_CUST_WRITE_EVENT].pIssCustWriteNvram
        = IssCustNvramWrite;
}

/******************************************************************************/
/* Function Name     : IssCustNvramWrite                                      */
/*                                                                            */
/* Description       : This function is a call back routine invoked at the end*/
/*                     NvRamWrite to write customer specific switch critical  */
/*                     configuration parameters to issnvram.txt               */
/*                                                                            */
/* Input Parameters  : None                                                   */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : ISS_SUCCESS/ISS_FAILURE                                */
/******************************************************************************/
INT4
IssCustNvramWrite (VOID)
{
    return ISS_SUCCESS;
}

/******************************************************************************/
/* Function Name     : IssCustNvramRead                                       */
/*                                                                            */
/* Description       : This function is a call back routine invoked at the end*/
/*                     GetNvRamValue to read customer specific switch critical*/
/*                     configuration parameters from issnvram.txt             */
/*                                                                            */
/* Input Parameters  : None                                                   */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      :  ISS_SUCCESS/ISS_FAILURE                               */
/******************************************************************************/
INT4
IssCustNvramRead (VOID)
{
    return ISS_SUCCESS;
}

/******************************************************************************/
/* Function Name     : IssCustClearConfig                                     */
/*                                                                            */
/* Description       : This function is a call back routine invoked to decide */
/*                     whether OID should be cleared. Customizations can be   */
/*                     done in this routine.                                  */
/*                                                                            */
/* Input Parameters  : u1IsClrConfigCompleted -Indicates whether clear-config */
/*                                             is completed or not .          */
/*                                             certain customizations can be  */
/*                                             done once clear config is      */
/*                                             completed                      */
/*                     u2OidType              - Indicates OID Type            */
/*                                              Scalar / Tabular              */
/*                                                                            */
/*                     pu1Oid                 - OID to be cleared             */
/*                                                                            */
/*                     pu1Instance            - Instance of the OID           */
/*                                if it is tabular object                     */
/*                                                                            */
/*                     pData                  - Value of the Current Instance */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      :  ISS_SUCCESS - If the OID can be cleared               */
/*                                                                            */
/*                      ISS_FAILURE - If the OID should not  be cleared       */
/******************************************************************************/

INT4
IssCustClearConfig (UINT1 u1IsClrConfigCompleted, UINT2 u2OidType,
                    UINT1 *pu1Oid, UINT1 *pu1Instance,
                    tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (u1IsClrConfigCompleted);
    UNUSED_PARAM (u2OidType);
    UNUSED_PARAM (pu1Oid);
    UNUSED_PARAM (pu1Instance);
    UNUSED_PARAM (pData);

    return ISS_SUCCESS;
}

/******************************************************************************/
/* Function Name     : IssCustGetMacAddrLogic                                  */
/*                                                                            */
/* Description       : This function is invoked to find which MAC address to  */
/*                     be used whether the Control Plane MAC or the MAC       */
/*                     returned from HW                                       */
/*                                                                            */
/* Input Parameters  : None                                                   */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      :  ISS_CUST_CTRL_PLANE_MAC/ISS_CUST_DATA_PLANE_MAC       */
/******************************************************************************/
UINT4
IssCustGetMacAddrLogic (VOID)
{
    return ISS_CUST_CTRL_PLANE_MAC;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssCustRemoveCliCmd                              */
/*                                                                          */
/*    Description        : This function removes the unwanted CLI commands  */
/*                         from the Command tree                            */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ ISS_FAILURE                         */
/****************************************************************************/
INT4
IssCustRemoveCliCmd (VOID)
{
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_DISABLE)
    {
        CliRemoveCommand ((INT1 *) "USEREXEC", (INT1 *) "stack");
        CliRemoveCommand ((INT1 *) "USEREXEC", (INT1 *) "no stack");
        CliRemoveCommand ((INT1 *) "USEREXEC", (INT1 *) "show stack");
    }
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssCustGetSysMonReqStatus                        */
/*                                                                          */
/*    Description        : This function returns system features monitoring */
/*                         status is required or not. If it is required,    */
/*                         then respective NP call gets hit, otherwise not. */
/*                                                                          */
/*    Input(s)           : u1SysMonParam - system monitor parameter         */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : OSIX_TRUE if to be monitored, else OSIX_FALSE    */
/****************************************************************************/
VOID
IssCustGetSysMonReqStatus (VOID)
{
    gIssSysCapSupport.u1TempStatusSupport = OSIX_TRUE;
    gIssSysCapSupport.u1FanStatusSupport = OSIX_TRUE;
    gIssSysCapSupport.u1RAMStatusSupport = OSIX_TRUE;
    gIssSysCapSupport.u1CPUStatusSupport = OSIX_TRUE;
    gIssSysCapSupport.u1FlashStatusSupport = OSIX_TRUE;
    gIssSysCapSupport.u1PowerStatusSupport = OSIX_TRUE;

    return;
}

#endif
