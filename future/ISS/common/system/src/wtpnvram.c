/****************************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: wtpnvram.c,v 1.6 2015/09/15 11:53:06 siva Exp $
*
* Description: This file has the routines for the WTP NvRam related Calls.
*
*****************************************************************************/

#ifndef __WTPNVRAM_C__
#define __WTPNVRAM_C__

#include "issinc.h"
#include "iss.h"
#include "cli.h"
#include "issnvram.h"
#include "wtpnvram.h"

tWTPNVRAM_DATA      sWtpNvRamData;

#ifndef NPAPI_WANTED
#define FNP_SUCCESS      1
#define FNP_FAILURE      0
#endif

#ifdef OPENSSL_WANTED
extern void         DtlsSslSetAuthType (unsigned int authtype);
#endif

/****************************************************************************
*                   WTP NVRAM RELATED PORTABLE FUNCIONS                     *
*****************************************************************************/

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WtpNvRamRead                                     */
/*                                                                          */
/*    Description        : This function is is a portable routine, to       */
/*                         read the critical configuration for the switch   */
/*                         from WtpNvRam.                                   */
/*                         While reading the information if it is failed    */
/*                         for any/all filed(s) it will set it to default   */
/*                         value.                                           */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : tWTPNVRAM_DATA  *pWtpNvRamData                   */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/*                                                                          */
/*    Note               : The return type should be as mentioned above for */
/*                         ported routines also.                            */
/****************************************************************************/

int
WtpNvRamRead (tWTPNVRAM_DATA * pWtpNvRamData)
{
    UINT1               au1Buffer[MAX_COLUMN_LENGTH];
    UINT1               u1ErrorCount = 0;

    MEMSET (au1Buffer, 0, MAX_COLUMN_LENGTH);

    /* First check whether the file exist or not */
    if (GetNvRamValue ((UINT1 *) WTP_NVRAM_FILE, (UINT1 *) "DISCOVERY_TYPE",
                       au1Buffer) == NVRAM_FILE_NOT_FOUND)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]:Writing new WTP_NVRAM_FILE with default values\n");
        WssInitialiseWtpNvRamDefVal ();

        return FNP_SUCCESS;
    }

    /*Read the AP Discovery Type */
    if (GetNvRamValue ((UINT1 *) WTP_NVRAM_FILE, (UINT1 *) "DISCOVERY_TYPE",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]:Failed to read DiscoveryType\n");
        u1ErrorCount++;
        pWtpNvRamData->u1DiscoveryType = WSS_DEFAULT_DISCOVERY_TYPE;
    }
    else
    {
        if (STRCMP (au1Buffer, "Unknown") == 0)
        {
            pWtpNvRamData->u1DiscoveryType = WSS_UNKNOWN_DISC_TYPE;
        }
        else if (STRCMP (au1Buffer, "Static") == 0)
        {
            pWtpNvRamData->u1DiscoveryType = WSS_STATIC_CONF_DISC_TYPE;
        }
        else if (STRCMP (au1Buffer, "DHCP") == 0)
        {
            pWtpNvRamData->u1DiscoveryType = WSS_DHCP_DISC_TYPE;
        }
        else if (STRCMP (au1Buffer, "DNS") == 0)
        {
            pWtpNvRamData->u1DiscoveryType = WSS_DNS_DISC_TYPE;
        }
        else if (STRCMP (au1Buffer, "AcReferral") == 0)
        {
            pWtpNvRamData->u1DiscoveryType = WSS_AC_REFERRAL_DISC_TYPE;
        }
        else
        {
            ISS_TRC (ALL_FAILURE_TRC, "[ERROR]:Failed to read DiscoveryType\n");
            u1ErrorCount++;
            pWtpNvRamData->u1DiscoveryType = WSS_DEFAULT_DISCOVERY_TYPE;
        }
    }

    /* Read the AC_REFERRAL_TYPE */
    if (GetNvRamValue ((UINT1 *) WTP_NVRAM_FILE, (UINT1 *) "AC_REFERRAL_TYPE",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]:Failed to read AC Referral Type\n");
        u1ErrorCount++;
        pWtpNvRamData->u2AcReferralType = WSS_DEFAULT_AC_REFERRAL_TYPE;
    }
    else
    {
        if (STRCMP (au1Buffer, "Broadcast") == 0)
        {
            /* Broadcast AC Referral Type */
            pWtpNvRamData->u2AcReferralType = WSS_AC_REFERRAL_TYPE_BROADCAST;
        }
        else if (STRCMP (au1Buffer, "Multicast") == 0)
        {
            /* Multicast AC Referral Type */
            pWtpNvRamData->u2AcReferralType = WSS_AC_REFERRAL_TYPE_MULTICAST;
        }
        else
        {
            /* AC Referral Type is neither Broadcast nor Multicast */
            ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: AC Referral Type incorrect\n");
            u1ErrorCount++;
            pWtpNvRamData->u2AcReferralType = WSS_DEFAULT_AC_REFERRAL_TYPE;
        }
    }

    /* Read the WTP CAPWAP MAC Type */
    if (GetNvRamValue ((UINT1 *) WTP_NVRAM_FILE,
                       (UINT1 *) "WTP_CAPWAP_MAC_TYPE",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]:Failed to read WTP CAPWAP MAC Type\n");
        u1ErrorCount++;
        pWtpNvRamData->u2WtpCapwapMacType = WSS_DEFAULT_CAPWAP_MAC_TYPE;
    }
    else
    {
        if (STRCMP (au1Buffer, "SplitMAC") == 0)
        {
            /* SplitMAC - WTP CAPWAP MAC Type */
            pWtpNvRamData->u2WtpCapwapMacType = WSS_CAPWAP_MAC_TYPE_SPLIT;
        }
        else if (STRCMP (au1Buffer, "LocalMAC") == 0)
        {
            /* LocalMAC - WTP CAPWAP MAC Type */
            pWtpNvRamData->u2WtpCapwapMacType = WSS_CAPWAP_MAC_TYPE_LOCAL;
        }
        else if (STRCMP (au1Buffer, "Both") == 0)
        {
            /* Both - WTP CAPWAP MAC Type */
            pWtpNvRamData->u2WtpCapwapMacType = WSS_CAPWAP_MAC_TYPE_BOTH;
        }
        else
        {
            /* WTP CAPWAP MAC Type is neither SplitMAC/LocalMAC/Both */
            ISS_TRC (ALL_FAILURE_TRC,
                     "[ERROR]:WTP CAPWAP MAC Type incorrect\n");
            u1ErrorCount++;
            pWtpNvRamData->u2WtpCapwapMacType = WSS_DEFAULT_CAPWAP_MAC_TYPE;
        }
    }

    /* Read the WTP CAPWAP Tunnel Mode */
    if (GetNvRamValue ((UINT1 *) WTP_NVRAM_FILE,
                       (UINT1 *) "WTP_CAPWAP_TUNNEL_MODE",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]:Failed to read WTP CAPWAP Tunnel Mode\n");
        u1ErrorCount++;
        pWtpNvRamData->u2WtpCapwapTunnelMode = WSS_DEFAULT_CAPWAP_TUNL_MODE;
    }
    else
    {
        if (STRCMP (au1Buffer, "NativeTunnel") == 0)
        {
            /* NativeTunnel - WTP CAPWAP Tunnel Mode */
            pWtpNvRamData->u2WtpCapwapTunnelMode = WSS_CAPWAP_TUNL_MODE_NATIVE;
        }
        else if (STRCMP (au1Buffer, "Dot3Tunnel") == 0)
        {
            /* Dot3Tunnel - WTP CAPWAP Tunnel Mode */
            pWtpNvRamData->u2WtpCapwapTunnelMode = WSS_CAPWAP_TUNL_MODE_DOT3;
        }
        else if (STRCMP (au1Buffer, "LocalBridging") == 0)
        {
            /* LocalBridging - WTP CAPWAP Tunnel Mode */
            pWtpNvRamData->u2WtpCapwapTunnelMode =
                WSS_CAPWAP_TUNL_MODE_LOCALBRIDGE;
        }
        else
        {
            /* WTP CAPWAP Tunnel Mode is neither 
               NativeTunnel/Dot3Tunnel/LocalBridging */
            ISS_TRC (ALL_FAILURE_TRC,
                     "[ERROR]:WTP CAPWAP Tun Mode incorrect\n");
            u1ErrorCount++;
            pWtpNvRamData->u2WtpCapwapTunnelMode = WSS_DEFAULT_CAPWAP_TUNL_MODE;
        }
    }

    /* Read the WTP_MODEL_NUMBER */
    if (GetNvRamValue ((UINT1 *) WTP_NVRAM_FILE, (UINT1 *) "WTP_MODEL_NUMBER",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]:Failed to read WTP Model Number\n");
        u1ErrorCount++;
        ISS_MEMCPY ((UINT1 *) pWtpNvRamData->ai1WtpModelNumber,
                    WSS_DEFAULT_WTP_MODEL_NUMBER,
                    STRLEN (WSS_DEFAULT_WTP_MODEL_NUMBER));
        pWtpNvRamData->ai1WtpModelNumber[STRLEN (WSS_DEFAULT_WTP_MODEL_NUMBER)]
            = '\0';
    }
    else
    {
        if ((STRLEN (au1Buffer) >= WTP_NVRAM_MAX_LEN))
        {
            ISS_TRC (ALL_FAILURE_TRC,
                     "[ERROR]: WTP_MODEL_NUMBER size is too big\r\n");
            u1ErrorCount++;
            ISS_MEMCPY ((UINT1 *) pWtpNvRamData->ai1WtpModelNumber,
                        WSS_DEFAULT_WTP_MODEL_NUMBER,
                        STRLEN (WSS_DEFAULT_WTP_MODEL_NUMBER));
            pWtpNvRamData->
                ai1WtpModelNumber[STRLEN (WSS_DEFAULT_WTP_MODEL_NUMBER)] = '\0';
        }
        else
        {
            ISS_MEMCPY ((UINT1 *) pWtpNvRamData->ai1WtpModelNumber,
                        au1Buffer, STRLEN ((const char *) au1Buffer));
            pWtpNvRamData->ai1WtpModelNumber[STRLEN (au1Buffer)] = '\0';
        }
    }

    /* Read the WTP_SERIAL_NUMBER */
    if (GetNvRamValue ((UINT1 *) WTP_NVRAM_FILE, (UINT1 *) "WTP_SERIAL_NUMBER",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]:Failed to read WTP Serial Number\n");
        u1ErrorCount++;
        ISS_MEMCPY ((UINT1 *) pWtpNvRamData->ai1WtpSerialNumber,
                    WSS_DEFAULT_WTP_SERIAL_NUMBER,
                    STRLEN (WSS_DEFAULT_WTP_SERIAL_NUMBER));
        pWtpNvRamData->
            ai1WtpSerialNumber[STRLEN (WSS_DEFAULT_WTP_SERIAL_NUMBER)] = '\0';
    }
    else
    {
        if ((STRLEN (au1Buffer) >= WTP_NVRAM_MAX_LEN))
        {
            ISS_TRC (ALL_FAILURE_TRC,
                     "[ERROR]: WTP_SERIAL_NUMBER size is too big\r\n");
            u1ErrorCount++;
            ISS_MEMCPY ((UINT1 *) pWtpNvRamData->ai1WtpSerialNumber,
                        WSS_DEFAULT_WTP_SERIAL_NUMBER,
                        STRLEN (WSS_DEFAULT_WTP_SERIAL_NUMBER));
            pWtpNvRamData->
                ai1WtpSerialNumber[STRLEN (WSS_DEFAULT_WTP_SERIAL_NUMBER)] =
                '\0';
        }
        else
        {
            ISS_MEMCPY ((UINT1 *) pWtpNvRamData->ai1WtpSerialNumber,
                        au1Buffer, STRLEN ((const char *) au1Buffer));
            pWtpNvRamData->ai1WtpSerialNumber[STRLEN (au1Buffer)] = '\0';
        }
    }

    /* Read the WTP_BOARD_ID */
    if (GetNvRamValue ((UINT1 *) WTP_NVRAM_FILE, (UINT1 *) "WTP_BOARD_ID",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]:Failed to read WTP Board ID\n");
        u1ErrorCount++;
        ISS_MEMCPY ((UINT1 *) pWtpNvRamData->ai1WtpBoardId,
                    WSS_DEFAULT_WTP_BOARD_ID,
                    STRLEN (WSS_DEFAULT_WTP_BOARD_ID));
        pWtpNvRamData->ai1WtpBoardId[STRLEN (WSS_DEFAULT_WTP_BOARD_ID)] = '\0';
    }
    else
    {
        if ((STRLEN (au1Buffer) >= WTP_NVRAM_MAX_LEN))
        {
            ISS_TRC (ALL_FAILURE_TRC,
                     "[ERROR]: WTP_BOARD_ID size is too big\r\n");
            u1ErrorCount++;
            ISS_MEMCPY ((UINT1 *) pWtpNvRamData->ai1WtpBoardId,
                        WSS_DEFAULT_WTP_BOARD_ID,
                        STRLEN (WSS_DEFAULT_WTP_BOARD_ID));
            pWtpNvRamData->ai1WtpBoardId[STRLEN (WSS_DEFAULT_WTP_BOARD_ID)] =
                '\0';
        }
        else
        {
            ISS_MEMCPY ((UINT1 *) pWtpNvRamData->ai1WtpBoardId,
                        au1Buffer, STRLEN ((const char *) au1Buffer));
            pWtpNvRamData->ai1WtpBoardId[STRLEN (au1Buffer)] = '\0';
        }
    }

    /* Read the WTP_BOARD_REVISION */
    if (GetNvRamValue ((UINT1 *) WTP_NVRAM_FILE, (UINT1 *) "WTP_BOARD_REVISION",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]:Failed to read WTP Board Revision\n");
        u1ErrorCount++;
        ISS_MEMCPY ((UINT1 *) pWtpNvRamData->ai1WtpBoardRevision,
                    WSS_DEFAULT_WTP_BOARD_REVISION,
                    STRLEN (WSS_DEFAULT_WTP_BOARD_REVISION));
        pWtpNvRamData->
            ai1WtpBoardRevision[STRLEN (WSS_DEFAULT_WTP_BOARD_REVISION)] = '\0';
    }
    else
    {
        if ((STRLEN (au1Buffer) >= WTP_NVRAM_MAX_LEN))
        {
            ISS_TRC (ALL_FAILURE_TRC,
                     "[ERROR]: WTP_BOARD_REVISION size is too big\r\n");
            u1ErrorCount++;
            ISS_MEMCPY ((UINT1 *) pWtpNvRamData->ai1WtpBoardRevision,
                        WSS_DEFAULT_WTP_BOARD_REVISION,
                        STRLEN (WSS_DEFAULT_WTP_BOARD_REVISION));
            pWtpNvRamData->
                ai1WtpBoardRevision[STRLEN (WSS_DEFAULT_WTP_BOARD_REVISION)] =
                '\0';
        }
        else
        {
            ISS_MEMCPY ((UINT1 *) pWtpNvRamData->ai1WtpBoardRevision,
                        au1Buffer, STRLEN ((const char *) au1Buffer));
            pWtpNvRamData->ai1WtpBoardRevision[STRLEN (au1Buffer)] = '\0';
        }
    }

    /* Read the CAPWAP_UDP_SERVER_PORT */
    if (GetNvRamValue
        ((UINT1 *) WTP_NVRAM_FILE, (UINT1 *) "CAPWAP_UDP_SERVER_PORT",
         au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]:Failed to read CAPWAP UDP Server Port\n");
        u1ErrorCount++;
        pWtpNvRamData->u4CapwapUdpServerPort =
            WSS_DEFAULT_CAPWAP_UDP_SERVER_PORT;
    }
    else
    {
        pWtpNvRamData->u4CapwapUdpServerPort = (UINT4) ISS_ATOI (au1Buffer);
    }

    /* Read the NATIVE_VLAN */
    if (GetNvRamValue ((UINT1 *) WTP_NVRAM_FILE, (UINT1 *) "NATIVE_VLAN",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]:Failed to read Native Vlan\n");
        u1ErrorCount++;
        pWtpNvRamData->u4NativeVlan = WSS_DEFAULT_NATIVE_VLAN;
    }
    else
    {
        pWtpNvRamData->u4NativeVlan = (UINT4) ISS_ATOI (au1Buffer);
    }

    /* Read WLC IP Address */
    if (GetNvRamValue ((UINT1 *) WTP_NVRAM_FILE, (UINT1 *) "WLC_IP_ADDRESS",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Failed to read WLC IP Address\n");
        u1ErrorCount++;
        pWtpNvRamData->u4WlcIpAddr = WSS_DEFAULT_WLC_IP_ADDR;
    }
    else
    {
        pWtpNvRamData->u4WlcIpAddr = OSIX_NTOHL (INET_ADDR (au1Buffer));

	if (pWtpNvRamData->u4WlcIpAddr != 0)
	{
	    if (pWtpNvRamData->u1DiscoveryType != WSS_STATIC_CONF_DISC_TYPE)
	    {
		/* Allow IP address which are Class A, B or C */
		if (!((ISS_IS_ADDR_CLASS_A (pWtpNvRamData->u4WlcIpAddr)) ||
			    (ISS_IS_ADDR_CLASS_B (pWtpNvRamData->u4WlcIpAddr)) ||
			    (ISS_IS_ADDR_CLASS_C (pWtpNvRamData->u4WlcIpAddr))))
		{
		    ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Invalid WLC IP Address\n");
		    u1ErrorCount++;
		    pWtpNvRamData->u4WlcIpAddr = WSS_DEFAULT_WLC_IP_ADDR;
		}
	    }
	}
    }

    /* Read the DTLS_KEY_TYPE */
    if (GetNvRamValue ((UINT1 *) WTP_NVRAM_FILE, (UINT1 *) "DTLS_KEY_TYPE",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]:Failed to read DTLS_KEY_TYPE\n");
        u1ErrorCount++;
        pWtpNvRamData->u4DtlsKeyType = WSS_DEFAULT_DTLS_KEY_TYPE;
    }
    else
    {
        pWtpNvRamData->u4DtlsKeyType = (UINT4) ISS_ATOI (au1Buffer);

        if (pWtpNvRamData->u4DtlsKeyType == WSS_DTLS_KEY_TYPE_CERT)
        {
#ifdef OPENSSL_WANTED
            DtlsSslSetAuthType (1);
#endif
        }
        else
        {
#ifdef OPENSSL_WANTED
            DtlsSslSetAuthType (0);
#endif
            pWtpNvRamData->u4DtlsKeyType = WSS_DEFAULT_DTLS_KEY_TYPE;
        }
    }

    if (u1ErrorCount > 0)
    {
        return FNP_FAILURE;
    }
    else
    {
        return FNP_SUCCESS;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WtpNvRamWrite                                    */
/*                                                                          */
/*    Description        : This function is a portable routine, to          */
/*                         write the critical configuration for the switch  */
/*                         to the Wtp NvRam.                                */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/*                                                                          */
/*    Note               : The return type should be as mentioned above for */
/*                         ported routines also.                            */
/****************************************************************************/
int
WtpNvRamWrite (tWTPNVRAM_DATA * pWtpNvRamData)
{
    INT4                i4Fd = 0;
    CHR1                au1Buf[WTP_NVRAM_MAX_LEN_1];
    tUtlInAddr          WlcIpAddr;

    MEMSET (au1Buf, 0, sizeof (au1Buf));
    MEMSET (&WlcIpAddr, 0, sizeof (tUtlInAddr));

    i4Fd = FileOpen ((CONST UINT1 *) WTP_NVRAM_FILE,
                     OSIX_FILE_WO | OSIX_FILE_TR | OSIX_FILE_CR);
    if (i4Fd < 0)
    {
        return FNP_FAILURE;
    }

    /*AP DISOCVERY TYPE */
    switch (pWtpNvRamData->u1DiscoveryType)
    {
        case WSS_UNKNOWN_DISC_TYPE:
            SPRINTF (au1Buf, "DISCOVERY_TYPE =Unknown\n");
            FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
            break;
        case WSS_STATIC_CONF_DISC_TYPE:
            SPRINTF (au1Buf, "DISCOVERY_TYPE =Static\n");
            FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
            break;
        case WSS_DHCP_DISC_TYPE:
            SPRINTF (au1Buf, "DISCOVERY_TYPE =DHCP\n");
            FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
            break;
        case WSS_DNS_DISC_TYPE:
            SPRINTF (au1Buf, "DISCOVERY_TYPE =DNS\n");
            FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
            break;
        case WSS_AC_REFERRAL_DISC_TYPE:
            SPRINTF (au1Buf, "DISCOVERY_TYPE =AcReferral\n");
            FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
            break;
        default:
            break;
    }
    /* AC Referral Type */
    if (pWtpNvRamData->u2AcReferralType == WSS_AC_REFERRAL_TYPE_BROADCAST)
    {
        SPRINTF (au1Buf, "AC_REFERRAL_TYPE =Broadcast\n");
        FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    }
    else if (pWtpNvRamData->u2AcReferralType == WSS_AC_REFERRAL_TYPE_MULTICAST)
    {
        SPRINTF (au1Buf, "AC_REFERRAL_TYPE =Multicast\n");
        FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    }

    /* CAPWAP MAC Type */
    if (pWtpNvRamData->u2WtpCapwapMacType == WSS_CAPWAP_MAC_TYPE_SPLIT)
    {
        SPRINTF (au1Buf, "WTP_CAPWAP_MAC_TYPE =SplitMAC\n");
        FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    }
    else if (pWtpNvRamData->u2WtpCapwapMacType == WSS_CAPWAP_MAC_TYPE_LOCAL)
    {
        SPRINTF (au1Buf, "WTP_CAPWAP_MAC_TYPE =LocalMAC\n");
        FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    }
    else if (pWtpNvRamData->u2WtpCapwapMacType == WSS_CAPWAP_MAC_TYPE_BOTH)
    {
        SPRINTF (au1Buf, "WTP_CAPWAP_MAC_TYPE =Both\n");
        FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    }

    /* CAPWAP Tunnel Mode */
    if (pWtpNvRamData->u2WtpCapwapTunnelMode == WSS_CAPWAP_TUNL_MODE_NATIVE)
    {
        SPRINTF (au1Buf, "WTP_CAPWAP_TUNNEL_MODE =NativeTunnel\n");
        FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    }
    else if (pWtpNvRamData->u2WtpCapwapTunnelMode == WSS_CAPWAP_TUNL_MODE_DOT3)
    {
        SPRINTF (au1Buf, "WTP_CAPWAP_TUNNEL_MODE =Dot3Tunnel\n");
        FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    }
    else if (pWtpNvRamData->u2WtpCapwapTunnelMode ==
             WSS_CAPWAP_TUNL_MODE_LOCALBRIDGE)
    {
        SPRINTF (au1Buf, "WTP_CAPWAP_TUNNEL_MODE =LocalBridging\n");
        FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    }

    /* WTP Model Number */
    SPRINTF (au1Buf, "WTP_MODEL_NUMBER =%s\n",
             pWtpNvRamData->ai1WtpModelNumber);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));

    /* WTP Serial Number */
    SPRINTF (au1Buf, "WTP_SERIAL_NUMBER =%s\n",
             pWtpNvRamData->ai1WtpSerialNumber);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));

    /* WTP Board Id */
    SPRINTF (au1Buf, "WTP_BOARD_ID =%s\n", pWtpNvRamData->ai1WtpBoardId);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));

    /* WTP Board Revision */
    SPRINTF (au1Buf, "WTP_BOARD_REVISION =%s\n",
             pWtpNvRamData->ai1WtpBoardRevision);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));

    /* CAPWAP UDP Server Port */
    SPRINTF (au1Buf, "CAPWAP_UDP_SERVER_PORT =%d\n",
             pWtpNvRamData->u4CapwapUdpServerPort);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));

    /* Native VLAN */
    SPRINTF (au1Buf, "NATIVE_VLAN =%d\n", pWtpNvRamData->u4NativeVlan);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));

    /* WLC IP Addr */
    WlcIpAddr.u4Addr = OSIX_NTOHL (pWtpNvRamData->u4WlcIpAddr);
    SPRINTF (au1Buf, "WLC_IP_ADDRESS =%s\n", UtlInetNtoa (WlcIpAddr));
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));

    /* DTLS Key Type */
    SPRINTF (au1Buf, "DTLS_KEY_TYPE =%d\n", pWtpNvRamData->u4DtlsKeyType);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));

    FileClose (i4Fd);

#if defined(SWC)
    system ("etc flush");
#endif
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssGetDiscoveryTypeFromWtpNvRam                  */
/*                                                                          */
/*    Description        : This function is used to get the DiscoveryType   */
/*                         from the WTP NVRAM.                              */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : Discovery Type                                   */
/*                        (AcReferral/Static/DHCP/DNS/Unknown)              */
/****************************************************************************/
UINT1
WssGetDiscoveryTypeFromWtpNvRam (VOID)
{
    return (sWtpNvRamData.u1DiscoveryType);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssSetDiscoveryTypeToWtpNvRam                    */
/*                                                                          */
/*    Description        : This function is invoked to set the value of     */
/*                         Discovery Type to the WTP NvRam.                 */
/*                                                                          */
/*    Input(s)           : Discovery Type                                   */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
WssSetDiscoveryTypeToWtpNvRam (UINT1 u1DiscoveryType)
{
    sWtpNvRamData.u1DiscoveryType = u1DiscoveryType;

    if (WtpNvRamWrite (&sWtpNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to WTP NvRam\n");
        return;
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssGetAcRefTypeFromWtpNvRam                      */
/*                                                                          */
/*    Description        : This function is used to get the AC Referral Type*/
/*                         from the WTP NVRAM.                              */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : AC Referral Type (Broadcast / Multicast)         */
/****************************************************************************/
UINT2
WssGetAcRefTypeFromWtpNvRam (VOID)
{
    return (sWtpNvRamData.u2AcReferralType);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssSetAcRefTypeToWtpNvRam                        */
/*                                                                          */
/*    Description        : This function is invoked to set the value of     */
/*                         AC Referral Type to the WTP NvRam.               */
/*                                                                          */
/*    Input(s)           : AC Referral Type                                 */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
WssSetAcRefTypeToWtpNvRam (UINT2 u2AcReferralType)
{
    sWtpNvRamData.u2AcReferralType = u2AcReferralType;

    if (WtpNvRamWrite (&sWtpNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to WTP NvRam\n");
        return;
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssGetWtpMacTypeFromWtpNvRam                     */
/*                                                                          */
/*    Description        : This function is used to get the WTP MAC Type    */
/*                         from the WTP NVRAM.                              */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : WTP CAPWAP MAC Type (SplitMAC / LocalMAC / Both) */
/****************************************************************************/
UINT2
WssGetWtpMacTypeFromWtpNvRam (VOID)
{
    return (sWtpNvRamData.u2WtpCapwapMacType);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssSetWtpMacTypeToWtpNvRam                       */
/*                                                                          */
/*    Description        : This function is invoked to set the value of     */
/*                         WTP CAPWAP MAC Type to the WTP NvRam.            */
/*                                                                          */
/*    Input(s)           : WTP CAPWAP MAC Type                              */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
WssSetWtpMacTypeToWtpNvRam (UINT2 u2WtpCapwapMacType)
{
    sWtpNvRamData.u2WtpCapwapMacType = u2WtpCapwapMacType;

    if (WtpNvRamWrite (&sWtpNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to WTP NvRam\n");
        return;
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssGetWtpTunlModeFromWtpNvRam                    */
/*                                                                          */
/*    Description        : This function is used to get the WTP Tunnel Mode */
/*                         from the WTP NVRAM.                              */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : WTP CAPWAP Tunnel Mode(Native/Dot3/LocalBridging)*/
/****************************************************************************/
UINT2
WssGetWtpTunlModeFromWtpNvRam (VOID)
{
    return (sWtpNvRamData.u2WtpCapwapTunnelMode);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssSetWtpTunlModeToWtpNvRam                      */
/*                                                                          */
/*    Description        : This function is invoked to set the value of     */
/*                         WTP CAPWAP Tunnel Mode to the WTP NvRam.         */
/*                                                                          */
/*    Input(s)           : WTP CAPWAP Tunnel Mode                           */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
WssSetWtpTunlModeToWtpNvRam (UINT2 u2WtpCapwapTunnelMode)
{
    sWtpNvRamData.u2WtpCapwapTunnelMode = u2WtpCapwapTunnelMode;

    if (WtpNvRamWrite (&sWtpNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to WTP NvRam\n");
        return;
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssGetWtpModelNumFromWtpNvRam                    */
/*                                                                          */
/*    Description        : This function is used to get the WTP Model Number*/
/*                         from the WTP NVRAM.                              */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : WTP Model Number                                 */
/****************************************************************************/
INT1               *
WssGetWtpModelNumFromWtpNvRam (VOID)
{
    return ((INT1 *) sWtpNvRamData.ai1WtpModelNumber);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssSetWtpModelNumToWtpNvRam                      */
/*                                                                          */
/*    Description        : This function is invoked to set the value of     */
/*                         WTP Model Number to the WTP NvRam.               */
/*                                                                          */
/*    Input(s)           : WTP Model Number                                 */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
WssSetWtpModelNumToWtpNvRam (INT1 *ai1WtpModelNumber)
{
    ISS_MEMCPY (sWtpNvRamData.ai1WtpModelNumber, ai1WtpModelNumber,
                STRLEN (ai1WtpModelNumber));
    sWtpNvRamData.ai1WtpModelNumber[STRLEN (ai1WtpModelNumber)] = '\0';

    if (WtpNvRamWrite (&sWtpNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to WTP NvRam\n");
        return;
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssGetWtpSerialNumFromWtpNvRam                   */
/*                                                                          */
/*    Description        : This function is used to get the WTP Serial Num  */
/*                         from the WTP NVRAM.                              */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : WTP Serial Number                                */
/****************************************************************************/
INT1               *
WssGetWtpSerialNumFromWtpNvRam (VOID)
{
    return ((INT1 *) sWtpNvRamData.ai1WtpSerialNumber);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssSetWtpSerialNumToWtpNvRam                     */
/*                                                                          */
/*    Description        : This function is invoked to set the value of     */
/*                         WTP Serial Number to the WTP NvRam.              */
/*                                                                          */
/*    Input(s)           : WTP Serial Number                                */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
WssSetWtpSerialNumToWtpNvRam (INT1 *ai1WtpSerialNumber)
{
    ISS_MEMCPY (sWtpNvRamData.ai1WtpSerialNumber, ai1WtpSerialNumber,
                STRLEN (ai1WtpSerialNumber));
    sWtpNvRamData.ai1WtpSerialNumber[STRLEN (ai1WtpSerialNumber)] = '\0';

    if (WtpNvRamWrite (&sWtpNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to WTP NvRam\n");
        return;
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssGetWtpBoardIdFromWtpNvRam                     */
/*                                                                          */
/*    Description        : This function is used to get the WTP Board Id    */
/*                         from the WTP NVRAM.                              */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : WTP Board ID                                     */
/****************************************************************************/
INT1               *
WssGetWtpBoardIdFromWtpNvRam (VOID)
{
    return ((INT1 *) sWtpNvRamData.ai1WtpBoardId);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssSetWtpBoardIdToWtpNvRam                       */
/*                                                                          */
/*    Description        : This function is invoked to set the value of     */
/*                         WTP Board ID to the WTP NvRam.                   */
/*                                                                          */
/*    Input(s)           : WTP Board ID                                     */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
WssSetWtpBoardIdToWtpNvRam (INT1 *ai1WtpBoardId)
{
    ISS_MEMCPY (sWtpNvRamData.ai1WtpBoardId, ai1WtpBoardId,
                STRLEN (ai1WtpBoardId));
    sWtpNvRamData.ai1WtpBoardId[STRLEN (ai1WtpBoardId)] = '\0';

    if (WtpNvRamWrite (&sWtpNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to WTP NvRam\n");
        return;
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssGetWtpBoardRevFromWtpNvRam                    */
/*                                                                          */
/*    Description        : This function is used to get the WTP Board Revsn */
/*                         from the WTP NVRAM.                              */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : WTP Board Revision                               */
/****************************************************************************/
INT1               *
WssGetWtpBoardRevFromWtpNvRam (VOID)
{
    return ((INT1 *) sWtpNvRamData.ai1WtpBoardRevision);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssSetWtpBoardRevToWtpNvRam                      */
/*                                                                          */
/*    Description        : This function is invoked to set the value of     */
/*                         WTP Board Revision to the WTP NvRam.             */
/*                                                                          */
/*    Input(s)           : WTP Board Revision                               */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
WssSetWtpBoardRevToWtpNvRam (INT1 *ai1WtpBoardRevision)
{
    ISS_MEMCPY (sWtpNvRamData.ai1WtpBoardRevision, ai1WtpBoardRevision,
                STRLEN (ai1WtpBoardRevision));
    sWtpNvRamData.ai1WtpBoardRevision[STRLEN (ai1WtpBoardRevision)] = '\0';

    if (WtpNvRamWrite (&sWtpNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to WTP NvRam\n");
        return;
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssGetCapwapUdpSPortFromWtpNvRam                 */
/*                                                                          */
/*    Description        : This function is used to get the CAPWAP UDP      */
/*                         Server Port from the WTP NVRAM.                  */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : CAPWAP UDP Server Port                           */
/****************************************************************************/
UINT4
WssGetCapwapUdpSPortFromWtpNvRam (VOID)
{
    return (sWtpNvRamData.u4CapwapUdpServerPort);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssSetCapwapUdpSPortToWtpNvRam                   */
/*                                                                          */
/*    Description        : This function is invoked to set the value of     */
/*                         CAPWAP UDP Server Port to the WTP NvRam.         */
/*                                                                          */
/*    Input(s)           : CAPWAP UDP Server Port                           */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
WssSetCapwapUdpSPortToWtpNvRam (UINT4 u4CapwapUdpServerPort)
{
    sWtpNvRamData.u4CapwapUdpServerPort = u4CapwapUdpServerPort;

    if (WtpNvRamWrite (&sWtpNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to WTP NvRam\n");
        return;
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssGetNativeVlanFromWtpNvRam                     */
/*                                                                          */
/*    Description        : This function is used to get the Native VLAN     */
/*                         from the WTP NVRAM.                              */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : Native VLAN                                      */
/****************************************************************************/
UINT4
WssGetNativeVlanFromWtpNvRam (VOID)
{
    return (sWtpNvRamData.u4NativeVlan);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssSetNativeVlanToWtpNvRam                       */
/*                                                                          */
/*    Description        : This function is invoked to set the value of     */
/*                         Native Vlan to the WTP NvRam.                    */
/*                                                                          */
/*    Input(s)           : Native VLAN                                      */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
WssSetNativeVlanToWtpNvRam (UINT4 u4NativeVlan)
{
    sWtpNvRamData.u4NativeVlan = u4NativeVlan;

    if (WtpNvRamWrite (&sWtpNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to WTP NvRam\n");
        return;
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssGetWlcIpAddrFromWtpNvRam                      */
/*                                                                          */
/*    Description        : This function is used to get the WLC IP          */
/*                         from the WTP NVRAM.                              */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : WLC IP Addr                                      */
/****************************************************************************/
UINT4
WssGetWlcIpAddrFromWtpNvRam (VOID)
{
    return (sWtpNvRamData.u4WlcIpAddr);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssSetWlcIpAddrToWtpNvRam                        */
/*                                                                          */
/*    Description        : This function is invoked to set the value of     */
/*                         WLC IP Addr to the WTP NvRam.                    */
/*                                                                          */
/*    Input(s)           : WLC IP Addr                                      */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
WssSetWlcIpAddrToWtpNvRam (UINT4 u4WlcIpAddr)
{
    sWtpNvRamData.u4WlcIpAddr = u4WlcIpAddr;

    if (WtpNvRamWrite (&sWtpNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write WLC IPAddr to WTP NvRam\n");
        return;
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssGetDtlsKeyTypeFromWtpNvRam                    */
/*                                                                          */
/*    Description        : This function is invoked to get the DTLS Key Type*/
/*                         from the WTP NVRAM.                              */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : DTLS Key Type                                    */
/****************************************************************************/
UINT4
WssGetDtlsKeyTypeFromWtpNvRam (VOID)
{
    return (sWtpNvRamData.u4DtlsKeyType);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssSetDtlsKeyTypeToWtpNvRam                      */
/*                                                                          */
/*    Description        : This function is invoked to set the value of     */
/*                         DTLS Key Type to the WTP NvRam.                  */
/*                                                                          */
/*    Input(s)           : DTLS Key Type                                    */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
WssSetDtlsKeyTypeToWtpNvRam (UINT4 u4DtlsKeyType)
{
    sWtpNvRamData.u4DtlsKeyType = u4DtlsKeyType;

    if (WtpNvRamWrite (&sWtpNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to WTP NvRam\n");
        return;
    }
    return;
}

#endif /*_WTPNVRAM_C */
