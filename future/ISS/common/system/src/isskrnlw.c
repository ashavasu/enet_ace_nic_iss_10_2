#include "isswinc.h"
#define IOCTL_SUCCESS 0

/* -------------------------------------------------------------
 *
 * Function: NmhIoctl
 *
 * -------------------------------------------------------------
 */
int
NmhIoctl (unsigned long p)
{
    int                 rc = 0;
    int                 nmhiocnr;
    union unNmh         lv;

    /* Copy the ioctl command number */
    copy_from_user (&nmhiocnr, (int *) p, 4);

    MEMSET (&lv, 0, sizeof (union unNmh));

    switch (nmhiocnr)
    {

        case NMH_GETFIRST_IPAUTH_TABLE:
        {
            if (copy_from_user (&lv.GetFirstIndexIssIpAuthMgrTable,
                                (tNmhGetFirstIndexIssIpAuthMgrTable *) p,
                                sizeof (tNmhGetFirstIndexIssIpAuthMgrTable)))
            {
                return (-EFAULT);
            }

            /* Invoke Nmh Routine */
            lv.GetFirstIndexIssIpAuthMgrTable.rval =
                nmhGetFirstIndexIssIpAuthMgrTable (lv.
                                                   GetFirstIndexIssIpAuthMgrTable.
                                                   pu4IssIpAuthMgrIpAddr,
                                                   lv.
                                                   GetFirstIndexIssIpAuthMgrTable.
                                                   pu4IssIpAuthMgrIpMask);
            /* Copy Nmh Routine return value(s) back to user */
            if (copy_to_user ((tNmhGetFirstIndexIssIpAuthMgrTable *) p,
                              &lv.GetFirstIndexIssIpAuthMgrTable,
                              sizeof (tNmhGetFirstIndexIssIpAuthMgrTable)))
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_GETNEXT_IPAUTH_TABLE:
        {
            if (copy_from_user (&lv.GetNextIndexIssIpAuthMgrTable,
                                (tNmhGetNextIndexIssIpAuthMgrTable *) p,
                                sizeof (tNmhGetNextIndexIssIpAuthMgrTable)))
            {
                return (-EFAULT);
            }

            /* Invoke Nmh Routine */
            lv.GetNextIndexIssIpAuthMgrTable.rval =
                nmhGetNextIndexIssIpAuthMgrTable (lv.
                                                  GetNextIndexIssIpAuthMgrTable.
                                                  u4IssIpAuthMgrIpAddr,
                                                  lv.
                                                  GetNextIndexIssIpAuthMgrTable.
                                                  pu4NextIssIpAuthMgrIpAddr,
                                                  lv.
                                                  GetNextIndexIssIpAuthMgrTable.
                                                  u4IssIpAuthMgrIpMask,
                                                  lv.
                                                  GetNextIndexIssIpAuthMgrTable.
                                                  pu4NextIssIpAuthMgrIpMask);
            /* Copy Nmh Routine return value(s) back to user */
            if (copy_to_user ((tNmhGetNextIndexIssIpAuthMgrTable *) p,
                              &lv.GetNextIndexIssIpAuthMgrTable,
                              sizeof (tNmhGetNextIndexIssIpAuthMgrTable)))
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_VALIDATE_IDXINST_IPAUTH_TABLE:
        {
            if (copy_from_user (&lv.ValidateIdxInstIssIpAuthMgrTable,
                                (tNmhValidateIndexInstanceIssIpAuthMgrTable *)
                                p,
                                sizeof
                                (tNmhValidateIndexInstanceIssIpAuthMgrTable)))
            {
                return (-EFAULT);
            }

            /* Invoke Nmh Routine */
            lv.ValidateIdxInstIssIpAuthMgrTable.rval =
                nmhValidateIndexInstanceIssIpAuthMgrTable (lv.
                                                           ValidateIdxInstIssIpAuthMgrTable.
                                                           u4IssIpAuthMgrIpAddr,
                                                           lv.
                                                           ValidateIdxInstIssIpAuthMgrTable.
                                                           u4IssIpAuthMgrIpMask);
            /* Copy Nmh Routine return value(s) back to user */
            if (copy_to_user ((tNmhValidateIndexInstanceIssIpAuthMgrTable *) p,
                              &lv.ValidateIdxInstIssIpAuthMgrTable,
                              sizeof
                              (tNmhValidateIndexInstanceIssIpAuthMgrTable)))
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_GET_IPAUTH_PORTLIST:
        {
            if (copy_from_user (&lv.GetIssIpAuthMgrPortList,
                                (tNmhGetIssIpAuthMgrPortList *) p, sizeof
                                (tNmhGetIssIpAuthMgrPortList)))
            {
                return (-EFAULT);
            }

            /* Invoke Nmh Routine */
            lv.GetIssIpAuthMgrPortList.rval =
                nmhGetIssIpAuthMgrPortList (lv.GetIssIpAuthMgrPortList.
                                            u4IssIpAuthMgrIpAddr,
                                            lv.GetIssIpAuthMgrPortList.
                                            u4IssIpAuthMgrIpMask,
                                            lv.GetIssIpAuthMgrPortList.
                                            pRetValIssIpAuthMgrPortList);
            /* Copy Nmh Routine return value(s) back to user */
            if (copy_to_user ((tNmhGetIssIpAuthMgrPortList *) p,
                              &lv.GetIssIpAuthMgrPortList,
                              sizeof (tNmhGetIssIpAuthMgrPortList)))
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_GET_IPAUTH_VLANLIST:
        {
            if (copy_from_user (&lv.GetIssIpAuthMgrVlanList,
                                (tNmhGetIssIpAuthMgrVlanList *) p, sizeof
                                (tNmhGetIssIpAuthMgrVlanList)))
            {
                return (-EFAULT);
            }

            /* Invoke Nmh Routine */
            lv.GetIssIpAuthMgrVlanList.rval =
                nmhGetIssIpAuthMgrVlanList (lv.GetIssIpAuthMgrVlanList.
                                            u4IssIpAuthMgrIpAddr,
                                            lv.GetIssIpAuthMgrVlanList.
                                            u4IssIpAuthMgrIpMask,
                                            lv.GetIssIpAuthMgrVlanList.
                                            pRetValIssIpAuthMgrVlanList);
            /* Copy Nmh Routine return value(s) back to user */
            if (copy_to_user ((tNmhGetIssIpAuthMgrVlanList *) p,
                              &lv.GetIssIpAuthMgrVlanList,
                              sizeof (tNmhGetIssIpAuthMgrVlanList)))
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_GET_IPAUTH_OOBPORT:
        {
            if (copy_from_user (&lv.GetIssIpAuthMgrOOBPort,
                                (tNmhGetIssIpAuthMgrOOBPort *) p, sizeof
                                (tNmhGetIssIpAuthMgrOOBPort)))
            {
                return (-EFAULT);
            }

            /* Invoke Nmh Routine */
            lv.GetIssIpAuthMgrOOBPort.rval =
                nmhGetIssIpAuthMgrOOBPort (lv.GetIssIpAuthMgrOOBPort.
                                           u4IssIpAuthMgrIpAddr,
                                           lv.GetIssIpAuthMgrOOBPort.
                                           u4IssIpAuthMgrIpMask,
                                           lv.GetIssIpAuthMgrOOBPort.
                                           pi4RetValIssIpAuthMgrOOBPort);
            /* Copy Nmh Routine return value(s) back to user */
            if (copy_to_user ((tNmhGetIssIpAuthMgrOOBPort *) p,
                              &lv.GetIssIpAuthMgrOOBPort,
                              sizeof (tNmhGetIssIpAuthMgrOOBPort)))
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_GET_IPAUTH_ALLOWEDSERVICES:
        {
            if (copy_from_user (&lv.GetIssIpAuthMgrAllowedServices,
                                (tNmhGetIssIpAuthMgrAllowedServices *) p, sizeof
                                (tNmhGetIssIpAuthMgrAllowedServices)))
            {
                return (-EFAULT);
            }

            /* Invoke Nmh Routine */
            lv.GetIssIpAuthMgrAllowedServices.rval =
                nmhGetIssIpAuthMgrAllowedServices (lv.
                                                   GetIssIpAuthMgrAllowedServices.
                                                   u4IssIpAuthMgrIpAddr,
                                                   lv.
                                                   GetIssIpAuthMgrAllowedServices.
                                                   u4IssIpAuthMgrIpMask,
                                                   lv.
                                                   GetIssIpAuthMgrAllowedServices.
                                                   pi4RetValIssIpAuthMgrAllowedServices);
            /* Copy Nmh Routine return value(s) back to user */
            if (copy_to_user ((tNmhGetIssIpAuthMgrAllowedServices *) p,
                              &lv.GetIssIpAuthMgrAllowedServices,
                              sizeof (tNmhGetIssIpAuthMgrAllowedServices)))
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_GET_IPAUTH_ROWSTATUS:
        {
            if (copy_from_user (&lv.GetIssIpAuthMgrRowStatus,
                                (tNmhGetIssIpAuthMgrRowStatus *) p, sizeof
                                (tNmhGetIssIpAuthMgrRowStatus)))
            {
                return (-EFAULT);
            }

            /* Invoke Nmh Routine */
            lv.GetIssIpAuthMgrRowStatus.rval =
                nmhGetIssIpAuthMgrRowStatus (lv.GetIssIpAuthMgrRowStatus.
                                             u4IssIpAuthMgrIpAddr,
                                             lv.GetIssIpAuthMgrRowStatus.
                                             u4IssIpAuthMgrIpMask,
                                             lv.GetIssIpAuthMgrRowStatus.
                                             pi4RetValIssIpAuthMgrRowStatus);
            /* Copy Nmh Routine return value(s) back to user */
            if (copy_to_user ((tNmhGetIssIpAuthMgrRowStatus *) p,
                              &lv.GetIssIpAuthMgrRowStatus,
                              sizeof (tNmhGetIssIpAuthMgrRowStatus)))
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_SET_IPAUTH_PORTLIST:
        {
            if (copy_from_user (&lv.SetIssIpAuthMgrPortList,
                                (tNmhSetIssIpAuthMgrPortList *) p, sizeof
                                (tNmhSetIssIpAuthMgrPortList)))
            {
                return (-EFAULT);
            }

            /* Invoke Nmh Routine */
            lv.SetIssIpAuthMgrPortList.rval =
                nmhSetIssIpAuthMgrPortList (lv.SetIssIpAuthMgrPortList.
                                            u4IssIpAuthMgrIpAddr,
                                            lv.SetIssIpAuthMgrPortList.
                                            u4IssIpAuthMgrIpMask,
                                            lv.SetIssIpAuthMgrPortList.
                                            pSetValIssIpAuthMgrPortList);
            /* Copy Nmh Routine return value(s) back to user */
            if (copy_to_user ((tNmhSetIssIpAuthMgrPortList *) p,
                              &lv.SetIssIpAuthMgrPortList,
                              sizeof (tNmhSetIssIpAuthMgrPortList)))
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_SET_IPAUTH_VLANLIST:
        {
            if (copy_from_user (&lv.SetIssIpAuthMgrVlanList,
                                (tNmhSetIssIpAuthMgrVlanList *) p, sizeof
                                (tNmhSetIssIpAuthMgrVlanList)))
            {
                return (-EFAULT);
            }

            /* Invoke Nmh Routine */
            lv.SetIssIpAuthMgrVlanList.rval =
                nmhSetIssIpAuthMgrVlanList (lv.SetIssIpAuthMgrVlanList.
                                            u4IssIpAuthMgrIpAddr,
                                            lv.SetIssIpAuthMgrVlanList.
                                            u4IssIpAuthMgrIpMask,
                                            lv.SetIssIpAuthMgrVlanList.
                                            pSetValIssIpAuthMgrVlanList);
            /* Copy Nmh Routine return value(s) back to user */
            if (copy_to_user ((tNmhSetIssIpAuthMgrVlanList *) p,
                              &lv.SetIssIpAuthMgrVlanList,
                              sizeof (tNmhSetIssIpAuthMgrVlanList)))
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_SET_IPAUTH_OOBPORT:
        {
            if (copy_from_user (&lv.SetIssIpAuthMgrOOBPort,
                                (tNmhSetIssIpAuthMgrOOBPort *) p, sizeof
                                (tNmhSetIssIpAuthMgrOOBPort)))
            {
                return (-EFAULT);
            }

            /* Invoke Nmh Routine */
            lv.SetIssIpAuthMgrOOBPort.rval =
                nmhSetIssIpAuthMgrOOBPort (lv.SetIssIpAuthMgrOOBPort.
                                           u4IssIpAuthMgrIpAddr,
                                           lv.SetIssIpAuthMgrOOBPort.
                                           u4IssIpAuthMgrIpMask,
                                           lv.SetIssIpAuthMgrOOBPort.
                                           i4SetValIssIpAuthMgrOOBPort);
            /* Copy Nmh Routine return value(s) back to user */
            if (copy_to_user ((tNmhSetIssIpAuthMgrOOBPort *) p,
                              &lv.SetIssIpAuthMgrOOBPort,
                              sizeof (tNmhSetIssIpAuthMgrOOBPort)))
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_SET_IPAUTH_ALLOWEDSERVICES:
        {
            if (copy_from_user (&lv.SetIssIpAuthMgrAllowedServices,
                                (tNmhSetIssIpAuthMgrAllowedServices *) p, sizeof
                                (tNmhSetIssIpAuthMgrAllowedServices)))
            {
                return (-EFAULT);
            }

            /* Invoke Nmh Routine */
            lv.SetIssIpAuthMgrAllowedServices.rval =
                nmhSetIssIpAuthMgrAllowedServices (lv.
                                                   SetIssIpAuthMgrAllowedServices.
                                                   u4IssIpAuthMgrIpAddr,
                                                   lv.
                                                   SetIssIpAuthMgrAllowedServices.
                                                   u4IssIpAuthMgrIpMask,
                                                   lv.
                                                   SetIssIpAuthMgrAllowedServices.
                                                   i4SetValIssIpAuthMgrAllowedServices);
            /* Copy Nmh Routine return value(s) back to user */
            if (copy_to_user ((tNmhSetIssIpAuthMgrAllowedServices *) p,
                              &lv.SetIssIpAuthMgrAllowedServices,
                              sizeof (tNmhSetIssIpAuthMgrAllowedServices)))
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_SET_IPAUTH_ROWSTATUS:
        {
            if (copy_from_user (&lv.SetIssIpAuthMgrRowStatus,
                                (tNmhSetIssIpAuthMgrRowStatus *) p, sizeof
                                (tNmhSetIssIpAuthMgrRowStatus)))
            {
                return (-EFAULT);
            }

            /* Invoke Nmh Routine */
            lv.SetIssIpAuthMgrRowStatus.rval =
                nmhSetIssIpAuthMgrRowStatus (lv.SetIssIpAuthMgrRowStatus.
                                             u4IssIpAuthMgrIpAddr,
                                             lv.SetIssIpAuthMgrRowStatus.
                                             u4IssIpAuthMgrIpMask,
                                             lv.SetIssIpAuthMgrRowStatus.
                                             i4SetValIssIpAuthMgrRowStatus);
            /* Copy Nmh Routine return value(s) back to user */
            if (copy_to_user ((tNmhSetIssIpAuthMgrRowStatus *) p,
                              &lv.SetIssIpAuthMgrRowStatus,
                              sizeof (tNmhSetIssIpAuthMgrRowStatus)))
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_TEST_IPAUTH_PORTLIST:
        {
            if (copy_from_user (&lv.Testv2IssIpAuthMgrPortList,
                                (tNmhTestv2IssIpAuthMgrPortList *) p, sizeof
                                (tNmhTestv2IssIpAuthMgrPortList)))
            {
                return (-EFAULT);
            }

            /* Invoke Nmh Routine */
            lv.Testv2IssIpAuthMgrPortList.rval =
                nmhTestv2IssIpAuthMgrPortList (lv.Testv2IssIpAuthMgrPortList.
                                               pu4ErrorCode,
                                               lv.Testv2IssIpAuthMgrPortList.
                                               u4IssIpAuthMgrIpAddr,
                                               lv.Testv2IssIpAuthMgrPortList.
                                               u4IssIpAuthMgrIpMask,
                                               lv.Testv2IssIpAuthMgrPortList.
                                               pTestValIssIpAuthMgrPortList);
            /* Copy Nmh Routine return value(s) back to user */
            if (copy_to_user ((tNmhTestv2IssIpAuthMgrPortList *) p,
                              &lv.Testv2IssIpAuthMgrPortList,
                              sizeof (tNmhTestv2IssIpAuthMgrPortList)))
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_TEST_IPAUTH_VLANLIST:
        {
            if (copy_from_user (&lv.Testv2IssIpAuthMgrVlanList,
                                (tNmhTestv2IssIpAuthMgrVlanList *) p, sizeof
                                (tNmhTestv2IssIpAuthMgrVlanList)))
            {
                return (-EFAULT);
            }

            /* Invoke Nmh Routine */
            lv.Testv2IssIpAuthMgrVlanList.rval =
                nmhTestv2IssIpAuthMgrVlanList (lv.Testv2IssIpAuthMgrVlanList.
                                               pu4ErrorCode,
                                               lv.Testv2IssIpAuthMgrVlanList.
                                               u4IssIpAuthMgrIpAddr,
                                               lv.Testv2IssIpAuthMgrVlanList.
                                               u4IssIpAuthMgrIpMask,
                                               lv.Testv2IssIpAuthMgrVlanList.
                                               pTestValIssIpAuthMgrVlanList);
            /* Copy Nmh Routine return value(s) back to user */
            if (copy_to_user ((tNmhTestv2IssIpAuthMgrVlanList *) p,
                              &lv.Testv2IssIpAuthMgrVlanList,
                              sizeof (tNmhTestv2IssIpAuthMgrVlanList)))
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_TEST_IPAUTH_OOBPORT:
        {
            if (copy_from_user (&lv.Testv2IssIpAuthMgrOOBPort,
                                (tNmhTestv2IssIpAuthMgrOOBPort *) p, sizeof
                                (tNmhTestv2IssIpAuthMgrOOBPort)))
            {
                return (-EFAULT);
            }

            /* Invoke Nmh Routine */
            lv.Testv2IssIpAuthMgrOOBPort.rval =
                nmhTestv2IssIpAuthMgrOOBPort (lv.Testv2IssIpAuthMgrOOBPort.
                                              pu4ErrorCode,
                                              lv.Testv2IssIpAuthMgrOOBPort.
                                              u4IssIpAuthMgrIpAddr,
                                              lv.Testv2IssIpAuthMgrOOBPort.
                                              u4IssIpAuthMgrIpMask,
                                              lv.Testv2IssIpAuthMgrOOBPort.
                                              i4TestValIssIpAuthMgrOOBPort);
            /* Copy Nmh Routine return value(s) back to user */
            if (copy_to_user ((tNmhTestv2IssIpAuthMgrOOBPort *) p,
                              &lv.Testv2IssIpAuthMgrOOBPort,
                              sizeof (tNmhTestv2IssIpAuthMgrOOBPort)))
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_TEST_IPAUTH_ALLOWEDSERVICES:
        {
            if (copy_from_user (&lv.Testv2IssIpAuthMgrAllowedServices,
                                (tNmhTestv2IssIpAuthMgrAllowedServices *) p,
                                sizeof (tNmhTestv2IssIpAuthMgrAllowedServices)))
            {
                return (-EFAULT);
            }

            /* Invoke Nmh Routine */
            lv.Testv2IssIpAuthMgrAllowedServices.rval =
                nmhTestv2IssIpAuthMgrAllowedServices (lv.
                                                      Testv2IssIpAuthMgrAllowedServices.
                                                      pu4ErrorCode,
                                                      lv.
                                                      Testv2IssIpAuthMgrAllowedServices.
                                                      u4IssIpAuthMgrIpAddr,
                                                      lv.
                                                      Testv2IssIpAuthMgrAllowedServices.
                                                      u4IssIpAuthMgrIpMask,
                                                      lv.
                                                      Testv2IssIpAuthMgrAllowedServices.
                                                      i4TestValIssIpAuthMgrAllowedServices);
            /* Copy Nmh Routine return value(s) back to user */
            if (copy_to_user ((tNmhTestv2IssIpAuthMgrAllowedServices *) p,
                              &lv.Testv2IssIpAuthMgrAllowedServices,
                              sizeof (tNmhTestv2IssIpAuthMgrAllowedServices)))
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        case NMH_TEST_IPAUTH_ROWSTATUS:
        {
            if (copy_from_user (&lv.Testv2IssIpAuthMgrRowStatus,
                                (tNmhTestv2IssIpAuthMgrRowStatus *) p,
                                sizeof (tNmhTestv2IssIpAuthMgrRowStatus)))
            {
                return (-EFAULT);
            }

            /* Invoke Nmh Routine */
            lv.Testv2IssIpAuthMgrAllowedServices.rval =
                nmhTestv2IssIpAuthMgrRowStatus (lv.Testv2IssIpAuthMgrRowStatus.
                                                pu4ErrorCode,
                                                lv.Testv2IssIpAuthMgrRowStatus.
                                                u4IssIpAuthMgrIpAddr,
                                                lv.Testv2IssIpAuthMgrRowStatus.
                                                u4IssIpAuthMgrIpMask,
                                                lv.Testv2IssIpAuthMgrRowStatus.
                                                i4TestValIssIpAuthMgrRowStatus);
            /* Copy Nmh Routine return value(s) back to user */
            if (copy_to_user ((tNmhTestv2IssIpAuthMgrRowStatus *) p,
                              &lv.Testv2IssIpAuthMgrRowStatus,
                              sizeof (tNmhTestv2IssIpAuthMgrRowStatus)))
            {
                return (-EFAULT);
            }
            else
            {
                rc = IOCTL_SUCCESS;
            }

            break;
        }
        default:
            rc = -EFAULT;
            break;

    }                            /* switch */

    return (rc);
}

/*****************************************************************************/
/* Function Name      : IssLock                                              */
/*                                                                           */
/* Description        : This function is used to take the ISS mutual         */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS or ISS_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
IssLock (VOID)
{
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssUnLock                                            */
/*                                                                           */
/* Description        : This function is used to give the ISS mutual         */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS or ISS_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
IssUnLock (VOID)
{
    return ISS_SUCCESS;
}
