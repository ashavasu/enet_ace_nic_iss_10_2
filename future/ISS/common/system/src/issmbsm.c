/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: issmbsm.c,v 1.7
 *
 * Description : This file has the routines for the Iss
 *               Startup and supporting routines for the
 *               Iss Mib code.
 *
 ******************************************************************************/
#include "issinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : IssMbsmCardInsertConfigCtrlTable                           */
/*                                                                           */
/* Description  : This function updates the port related HW configuration    */
/*                for Config Control Table when                              */
/*                a card is inserted into the MBSM System.                   */
/*                                                                           */
/* Input        : pPortInfo  - Pointer to Port Related Info.                 */
/*              : pSlotInfo  - Pointer to Slot Related Info.                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IssMbsmCardInsertConfigCtrlTable (tMbsmPortInfo * pPortInfo,
                                  tMbsmSlotInfo * pSlotInfo)
{

    tIssConfigCtrlEntry *pIssConfigCtrlEntry = NULL;
    INT4                i4ConfigCtrlIndex = 0;
    UINT4               u4StartPortIndex = 0;
    UINT4               u4EndPortIndex = 0;
    INT4                i4RetVal;
    UINT1               u1IsSetInPortList = OSIX_FALSE;
    UINT1               u1IsSetInPortListStatus = OSIX_FALSE;

    if (pPortInfo->u4PortCount == 0)
    {
        return MBSM_SUCCESS;
    }

    pIssConfigCtrlEntry = gIssGlobalInfo.
        apIssConfigCtrlEntry[i4ConfigCtrlIndex];

    if ((pIssConfigCtrlEntry != NULL) &&
        (pIssConfigCtrlEntry->IssConfigCtrlStatus == ISS_VALID))
    {

        if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
        {
            /* Global Control is Enabled */
            i4RetVal = IsssysIssMbsmHwSetPortEgressStatus (i4ConfigCtrlIndex,
                                                           pIssConfigCtrlEntry->
                                                           IssConfigCtrlEgressStatus,
                                                           pSlotInfo);

            if (i4RetVal != FNP_SUCCESS)
            {
                 ISS_TRC (ALL_FAILURE_TRC,
                     "\nIssMbsmCardInsertConfigCtrlTable  - IsssysIssMbsmHwSetPortEgressStatus returns failure");
                return MBSM_FAILURE;
            }

            i4RetVal = IsssysIssMbsmHwSetPortStatsCollection (i4ConfigCtrlIndex,
                                                              pIssConfigCtrlEntry->
                                                              IssConfigCtrlStatsCollection,
                                                              pSlotInfo);
            if (i4RetVal != FNP_SUCCESS)
            {
                ISS_TRC (ALL_FAILURE_TRC,
                     "\nIssMbsmCardInsertConfigCtrlTable  - IsssysIssMbsmHwSetPortStatsCollection returns failure");
                return MBSM_FAILURE;
            }
        }

    }
    else
    {
        /* Interface wise control is enabled  */
        if (pSlotInfo->i1IsPreConfigured == MBSM_FALSE)
        {
            return MBSM_SUCCESS;
        }

        u4StartPortIndex = MBSM_PORT_INFO_STARTINDEX (pPortInfo);
        u4EndPortIndex = MBSM_PORT_INFO_STARTINDEX (pPortInfo) +
            MBSM_PORT_INFO_PORTCOUNT (pPortInfo);
        i4ConfigCtrlIndex = u4StartPortIndex;

        while (u4StartPortIndex != u4EndPortIndex)
        {
            OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST (pPortInfo),
                                     u4StartPortIndex, sizeof (tPortList),
                                     u1IsSetInPortList);
            OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST_STATUS (pPortInfo),
                                     u4StartPortIndex, sizeof (tPortList),
                                     u1IsSetInPortListStatus);
            /* Handling port insert event. incase of slot insert, all
             * ports will be set by default
             * we need not handle port delete event
             */
            if ((u1IsSetInPortList == OSIX_TRUE)
                && (OSIX_FALSE == u1IsSetInPortListStatus))
            {

                pIssConfigCtrlEntry = gIssGlobalInfo.
                    apIssConfigCtrlEntry[i4ConfigCtrlIndex];

                if ((pIssConfigCtrlEntry != NULL) &&
                    (pIssConfigCtrlEntry->IssConfigCtrlStatus == ISS_VALID))
                {
                    if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                    {
                        i4RetVal =
                            IsssysIssMbsmHwSetPortEgressStatus
                            (i4ConfigCtrlIndex,
                             pIssConfigCtrlEntry->IssConfigCtrlEgressStatus,
                             pSlotInfo);

                        if (i4RetVal != FNP_SUCCESS)
                        {
                            ISS_TRC (ALL_FAILURE_TRC,
                     "\nIssMbsmCardInsertConfigCtrlTable  - IsssysIssMbsmHwSetPortEgressStatus returns failure");
                            return MBSM_FAILURE;
                        }

                        i4RetVal =
                            IsssysIssMbsmHwSetPortStatsCollection
                            (i4ConfigCtrlIndex,
                             pIssConfigCtrlEntry->IssConfigCtrlStatsCollection,
                             pSlotInfo);

                        if (i4RetVal != FNP_SUCCESS)
                        {
                            ISS_TRC (ALL_FAILURE_TRC,
                     "\nIssMbsmCardInsertConfigCtrlTable  - IsssysIssMbsmHwSetPortStatsCollection returns failure");
                            return MBSM_FAILURE;
                        }
                    }

                }

            }
            u4StartPortIndex++;
            i4ConfigCtrlIndex++;
        }

    }
 
    ISS_TRC (CONTROL_PLANE_TRC,
                     "\nIssMbsmCardInsertConfigCtrlTable updated the port related HW configuration for Config Control Table"
                      " and returns success");

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssMbsmCardInsertPortCtrlTable                             */
/*                                                                           */
/* Description  : This function updates the port related HW configuration    */
/*                for Port Control Table when                                */
/*                a card is inserted into the MBSM System.                   */
/*                                                                           */
/* Input        : pPortInfo  - Pointer to Port Related Info.                 */
/*              : pSlotInfo  - Pointer to Slot Related Info.                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IssMbsmCardInsertPortCtrlTable (tMbsmPortInfo * pPortInfo,
                                tMbsmSlotInfo * pSlotInfo)
{

    tIssPortCtrlEntry  *pIssPortCtrlEntry = NULL;
    UINT1               au1PortName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4PortCtrlIndex = 0;
    UINT4               u4StartPortIndex = 0;
    UINT4               u4EndPortIndex = 0;
    INT4                i4RetVal;
    UINT1               u1IsSetInPortList = OSIX_FALSE;
    UINT1               u1IsSetInPortListStatus = OSIX_FALSE;
    tCfaIfInfo          IfInfo;
#ifdef L2RED_WANTED
    INT4                i4IssPortSpeed = 0;
#endif

    MEMSET (&au1PortName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
    if ((pSlotInfo->i1IsPreConfigured == MBSM_FALSE) ||
        (pPortInfo->u4PortCount == 0))
    {
        return MBSM_SUCCESS;
    }

    u4StartPortIndex = MBSM_PORT_INFO_STARTINDEX (pPortInfo);
    u4EndPortIndex = MBSM_PORT_INFO_STARTINDEX (pPortInfo) +
        MBSM_PORT_INFO_PORTCOUNT (pPortInfo);
    i4PortCtrlIndex = u4StartPortIndex;

    while (u4StartPortIndex != u4EndPortIndex)
    {
        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST (pPortInfo),
                                 i4PortCtrlIndex, sizeof (tPortList),
                                 u1IsSetInPortList);
        OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST_STATUS (pPortInfo),
                                 i4PortCtrlIndex, sizeof (tPortList),
                                 u1IsSetInPortListStatus);
        /*Handling Of Port Insert event */
        if ((u1IsSetInPortList == OSIX_TRUE)
            && (OSIX_FALSE == u1IsSetInPortListStatus))
        {

            pIssPortCtrlEntry = gIssGlobalInfo.
                apIssPortCtrlEntry[i4PortCtrlIndex - 1];

            if (pIssPortCtrlEntry != NULL)
            {
                /* Get the default values of interface parameters from CFA */
                if ((CfaGetIfInfo ((UINT4) i4PortCtrlIndex, &IfInfo)) ==
                    CFA_FAILURE)
                {
                     ISS_TRC (ALL_FAILURE_TRC,
                     "\nIssMbsmCardInsertPortCtrlTable - CfaGetIfInfo returns failure");
                    return MBSM_FAILURE;
                }

                if (IfInfo.u1AutoNegStatus == ISS_AUTO)
                {
                    /* Port specific NP calls will call only BCMX call with IfIndex 
                     * as an argument. The BCMX call will call bcm call only for that
                     * specific LPort. Hence no need to call NP call in npx.c file
                     */

                    if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                    {
                        i4RetVal =
                            IsssysIssHwSetPortMode ((UINT4) i4PortCtrlIndex,
                                                    ISS_AUTO);

                        if (i4RetVal != FNP_SUCCESS)
                        {
                            ISS_TRC (ALL_FAILURE_TRC,
                     "\nIssMbsmCardInsertPortCtrlTable - IsssysIssHwSetPortMode returns failure");
                            return MBSM_FAILURE;
                        }
                    }

                }
                else
                {
                    /* Port specific NP calls will call only BCMX call with IfIndex 
                     * as an argument. The BCMX call will call bcm call only for that
                     * specific LPort. Hence no need to call NP call in npx.c file
                     */
                    if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                    {
                        i4RetVal =
                            IsssysIssHwSetPortMode ((UINT4) i4PortCtrlIndex,
                                                    ISS_NONEGOTIATION);

                        if (i4RetVal != FNP_SUCCESS)
                        {
                            ISS_TRC (ALL_FAILURE_TRC,
                     "\nIssMbsmCardInsertPortCtrlTable - IsssysIssHwSetPortMode returns failure");
                            return MBSM_FAILURE;
                        }

                        i4RetVal =
                            IsssysIssHwSetPortDuplex ((UINT4) i4PortCtrlIndex,
                                                      (INT4) IfInfo.
                                                      u1DuplexStatus);

                        if (i4RetVal != FNP_SUCCESS)
                        {
                            ISS_TRC (ALL_FAILURE_TRC,
                     "\nIssMbsmCardInsertPortCtrlTable - IsssysIssHwSetPortDuplex returns failure");
                            return MBSM_FAILURE;
                        }
                    }

                    /* Port specific NP calls will call only BCMX call with IfIndex 
                     * as an argument. The BCMX call will call bcm call only for that
                     * specific LPort. Hence no need to call NP call in npx.c file
                     */
#ifdef L2RED_WANTED
                    if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
                    {
                        /* Port speed will be updated after getting attach indication */
                        CfaGetPortName (i4PortCtrlIndex, au1PortName);
                        if ((STRCMP (au1PortName, "Hg") != 0)
                            && (STRCMP (au1PortName, "Xl") != 0)
                            && (STRCMP (au1PortName, "Lvi") != 0)
                            && (STRCMP (au1PortName, "Fe") != 0))
                        {
                            /* To set the updated port speed information of the 
                             * attached line card to CFA and other modules. 
                             */
                            IssSysConvertCfaSpeedToIssSpeed (IfInfo.u4IfSpeed,
                                                             IfInfo.
                                                             u4IfHighSpeed,
                                                             &i4IssPortSpeed);
                            IssSetPortCtrlSpeed (i4PortCtrlIndex,
                                                 i4IssPortSpeed);
                            i4RetVal =
                                IsssysIssHwSetPortSpeed ((UINT4)
                                                         i4PortCtrlIndex,
                                                         i4IssPortSpeed);

                        }
                        else if (STRCMP (au1PortName, "Xl") == 0)
                        {
                            i4RetVal =
                                IsssysIssHwSetPortSpeed ((UINT4)
                                                         i4PortCtrlIndex,
                                                         ISS_40GB);
                        }
                        else if (STRCMP (au1PortName, "Lvi") == 0)
                        {
                            i4RetVal =
                                IsssysIssHwSetPortSpeed ((UINT4)
                                                         i4PortCtrlIndex,
                                                         ISS_56GB);
                        }
                        else if (STRCMP (au1PortName, "Fe") == 0)
                        {
                            i4RetVal =
                                IsssysIssHwSetPortSpeed ((UINT4)
                                                         i4PortCtrlIndex,
                                                         ISS_100MBPS);
                        }
                        else
                        {
                            i4RetVal =
                                IsssysIssHwSetPortSpeed ((UINT4)
                                                         i4PortCtrlIndex,
                                                         ISS_10GB);

                        }
                        if (i4RetVal != FNP_SUCCESS)
                        {
                            ISS_TRC (ALL_FAILURE_TRC,
                     "\nIssMbsmCardInsertPortCtrlTable - IsssysIssHwSetPortSpeed returns failure");
                            return MBSM_FAILURE;
                        }
                    }
#endif
                }

            }

        }
        u4StartPortIndex++;
        i4PortCtrlIndex++;
    }
 
    ISS_TRC (CONTROL_PLANE_TRC,
                     "\nIssMbsmCardInsertPortCtrlTable updated the port related HW configuration for Port Control table"                               " and returns success");

    return MBSM_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssMbsmCardInsertMirrorCtrlTable                            */
/*                                                                           */
/* Description  : This function updates the port related HW configuration    */
/*                for Mirror Control Table when                             */
/*                a card is inserted into the MBSM System.               */
/*                                                                           */
/* Input        : pPortInfo  - Pointer to Port Related Info.                 */
/*              : pSlotInfo  - Pointer to Slot Related Info.                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IssMbsmCardInsertMirrorCtrlTable (tMbsmPortInfo * pPortInfo,
                                  tMbsmSlotInfo * pSlotInfo)
{
    tIssHwMirrorInfo    HwMirrorInfo;
    tIssMirrSessionInfo *pSessionInfo;
    tHwPortInfo         HwPortInfo;
    UINT4               u4StartPortIndex = 0;
    UINT4               u4TempStartIndex = 0;
    UINT4               u4EndPortIndex = 0;
    UINT4               u4LocalStartIndex = 0;
    UINT4               u4LocalEndIndex = 0;
    INT4                i4RetVal;
    UINT2               u2SessionNo;
    UINT1               u1IsSetInPortList = OSIX_FALSE;
    UINT1               u1Mode;
    UINT1               u1ControlStatus;
    UINT1               u1NumOfDest = 0;
    UINT1               u1NumOfSrc = 0;
    UINT1               u1IsSetInPortListStatus = OSIX_FALSE;
    UINT1               u1Index = 0;
  
    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));

    if ((gIssGlobalInfo.u1MirrorStatus == ISS_MIRRORDISABLE) ||
        (pPortInfo->u4PortCount == 0))
    {
        ISS_TRC (CONTROL_PLANE_TRC,
                "\nIssMbsmCardInsertMirrorCtrlTable: Mirroring is disabled or"
                "Ports inserted is zero; so returns success ");

        return MBSM_SUCCESS;
    }

    if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
    {
        i4RetVal =
            IsssysIssMbsmHwSetPortMirroringStatus (ISS_MIRRORENABLE, pSlotInfo);

        if (i4RetVal != FNP_SUCCESS)
        {
            ISS_TRC (ALL_FAILURE_TRC,
                    "\nIssMbsmCardInsertMirrorCtrlTable : "
                    "IsssysIssMbsmHwSetPortMirroringStatus returns failure ");
            return MBSM_FAILURE;
        }
    }
    /* Interface wise control is enabled  */
    if (pSlotInfo->i1IsPreConfigured == MBSM_FALSE)
    {
        ISS_TRC (CONTROL_PLANE_TRC,
                "\nIssMbsmCardInsertMirrorCtrlTable: Interface wise control is enabled"
                "so returning success ");
        return MBSM_SUCCESS;
    }

    u4StartPortIndex = MBSM_PORT_INFO_STARTINDEX (pPortInfo);
    u4EndPortIndex = MBSM_PORT_INFO_STARTINDEX (pPortInfo) +
        MBSM_PORT_INFO_PORTCOUNT (pPortInfo);

    CfaGetLocalUnitPortInformation (&HwPortInfo);
    u4LocalStartIndex = HwPortInfo.u4StartIfIndex;
    u4LocalEndIndex = HwPortInfo.u4EndIfIndex;

    /*For each session check if the port exists in destination or source list,
       If the port exists for the sesson then update the port in hardware. */
    for (u2SessionNo = 1; u2SessionNo <= ISS_MIRR_MAX_SESSIONS; u2SessionNo++)
    {
        pSessionInfo = GET_MIRR_SESSION_INFO (u2SessionNo);
        u4TempStartIndex = u4StartPortIndex;

        /*Traverse all the ports for the slot inserted */
        while (u4TempStartIndex != u4EndPortIndex)
        {

            OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST (pPortInfo),
                                     u4TempStartIndex, sizeof (tPortList),
                                     u1IsSetInPortList);
            OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST_STATUS (pPortInfo),
                                     u4TempStartIndex, sizeof (tPortList),
                                     u1IsSetInPortListStatus);
            if ((u1IsSetInPortList == OSIX_TRUE)
                && (OSIX_FALSE == u1IsSetInPortListStatus))
            {
                /*Only port based Mirroring session will be affected by card insert event */
                if ((pSessionInfo->u1MirroringType != ISS_MIRR_PORT_BASED) 
                    && (pSessionInfo->u1MirroringType != ISS_MIRR_INVALID))
                {
                    /*Only destination ports needs to be updated for this session
                       mark sources configured as completed */
                    u1NumOfSrc = ISS_MIRR_HW_SRC_RECD;
                }
                /*Check if Port is a Source for mirroing session */
                else if (IssMirrGetSrcRecrdInfo
                         (u2SessionNo, ISS_DEFAULT_CONTEXT, u4TempStartIndex,
                          &u1Mode, &u1ControlStatus) == ISS_SUCCESS)
                {
                    /*Update Hardware structure */
                    if (u1ControlStatus == ISS_VALID)
                    {
                        HwMirrorInfo.SourceList[u1NumOfSrc].u4SourceNo =
                            u4TempStartIndex;
                        HwMirrorInfo.SourceList[u1NumOfSrc].u4ContextId =
                            ISS_DEFAULT_CONTEXT;
                        HwMirrorInfo.SourceList[u1NumOfSrc].u1Mode = u1Mode;

                        u1NumOfSrc++;
                        /* Destination port has to be set in Secondary card as well */
                        if(IssPortGetMirrDestPortForSrcPort(u4TempStartIndex, 
                                    &HwMirrorInfo.u4DestList[0]) == ISS_SUCCESS)
                        {
                            /* Max of only one destination port can be configured */
                            u1NumOfDest = 1;
                        }

                    }
                }
            }

            u4TempStartIndex++;
        }

        /*If all the ports are traversed then configure hardware as 
          configuration completed
          This scenerio will come if HW info structure has still space to fill information but ports
          on the card are fininshed */
        if ((u1NumOfSrc != 0) || (u1NumOfDest != 0))
        {
                HwMirrorInfo.u4RSpanContextId = pSessionInfo->u4RSpanContextId;
                HwMirrorInfo.u2RSpanVlanId = pSessionInfo->RSpanVlanId;
                HwMirrorInfo.u2SessionId = u2SessionNo;
                HwMirrorInfo.u1MirrType = pSessionInfo->u1MirroringType;
            HwMirrorInfo.u1RSpanSessionType = pSessionInfo->u1RSpanSessionType;
            HwMirrorInfo.u1Action = ISS_MIRR_ADD_DONE;
            ISS_TRC_ARG3 (ISS_TRC_ALL |CONTROL_PLANE_TRC,
                    "\r\nCalling MBSM for SessionNo %d  having mirror type %d,"
                    "desination index %d , with source index/indices ",
                    u2SessionNo, HwMirrorInfo.u1MirrType,
                    HwMirrorInfo.u4DestList[0]);
            for(u1Index = 0;u1Index < u1NumOfSrc;u1Index++)
            {
                ISS_TRC_ARG1 (ISS_TRC_ALL |CONTROL_PLANE_TRC,
                        "%d ,",HwMirrorInfo.SourceList[u1Index].u4SourceNo);
            }


            /* For MBSM support */
            if(IsssysIssMbsmHwSetMirroring(&HwMirrorInfo, pSlotInfo)
                            == FNP_FAILURE)
                {

                ISS_TRC_ARG1 (ISS_TRC_ALL |ISS_TRC_ALL_FAIL,
                        "%d ,",HwMirrorInfo.SourceList[u1Index].u4SourceNo);

                
            }

        }
        /* Reset the values for next iteration */
                    u1NumOfSrc = 0;
                    u1NumOfDest = 0;
        MEMSET(&HwMirrorInfo, 0, sizeof(tIssHwMirrorInfo));

        /*Only port based Mirroring session will be affected by card insert event */
        if (pSessionInfo->u1MirroringType == ISS_MIRR_PORT_BASED) 
        {
            /* For sessions having source ports in local node and 
             * monitor ports in remote node, the mirror has to be installed in 
             * local node when the remote slot is attached */
            if(IssMirrGetNextDestRecrd(u2SessionNo, 0, 
                        &HwMirrorInfo.u4DestList[0]) == ISS_SUCCESS)
            {
                /* Check if the Destination port is one of the Remote card's port */
                if ((HwMirrorInfo.u4DestList[0] >= u4StartPortIndex) &&
                        (HwMirrorInfo.u4DestList[0] <= u4EndPortIndex))
                {
                    /* Max of only one destination port can be configured */
                    u1NumOfDest = 1;
                }
                else
                {
                    continue;
                }
                }

            u4TempStartIndex = u4LocalStartIndex;
            /*Traverse all the ports for the local slot */
            while (u4TempStartIndex != u4LocalEndIndex)
                   {
                if (IssMirrGetSrcRecrdInfo
                        (u2SessionNo, ISS_DEFAULT_CONTEXT, u4TempStartIndex,
                         &u1Mode, &u1ControlStatus) == ISS_SUCCESS)
                {
                    /*Update Hardware structure */
                    if (u1ControlStatus == ISS_VALID)
                    {
                        HwMirrorInfo.SourceList[u1NumOfSrc].u4SourceNo =
                            u4TempStartIndex;
                        HwMirrorInfo.SourceList[u1NumOfSrc].u4ContextId =
                            ISS_DEFAULT_CONTEXT;
                        HwMirrorInfo.SourceList[u1NumOfSrc].u1Mode = u1Mode;

                        u1NumOfSrc++;
                   }
            }
            u4TempStartIndex++;
        }

        /*If all the ports are traversed then configure hardware as 
              configuration completed. This scenerio will come if HW info structure has still space to fill information but ports
           on the card are fininshed */
            if ((u1NumOfSrc != 0) || (u1NumOfDest != 0))
        {
            HwMirrorInfo.u4RSpanContextId = pSessionInfo->u4RSpanContextId;
            HwMirrorInfo.u2RSpanVlanId = pSessionInfo->RSpanVlanId;
            HwMirrorInfo.u2SessionId = u2SessionNo;
            HwMirrorInfo.u1MirrType = pSessionInfo->u1MirroringType;
            HwMirrorInfo.u1RSpanSessionType = pSessionInfo->u1RSpanSessionType;
            HwMirrorInfo.u1Action = ISS_MIRR_ADD_DONE;
                ISS_TRC_ARG3 (ISS_TRC_ALL |CONTROL_PLANE_TRC,
                        "\r\nInstaling mirror locally for SessionNo %d "
                        " having mirror type %d,"
                        "desination index %d , with source index/indices ",
                        u2SessionNo, HwMirrorInfo.u1MirrType,
                        HwMirrorInfo.u4DestList[0]);
                for(u1Index = 0;u1Index < u1NumOfSrc;u1Index++)
                {
                    ISS_TRC_ARG1 (ISS_TRC_ALL |CONTROL_PLANE_TRC,
                            "%d ,",HwMirrorInfo.SourceList[u1Index].u4SourceNo);
                }

                if(IsssysIssHwSetMirroring(&HwMirrorInfo)
                        == FNP_FAILURE)
               {

                    ISS_TRC_ARG1 (ISS_TRC_ALL |ISS_TRC_ALL_FAIL,
                            "\r\nInstallation of Mirror locally for SessionNo %d failed",
                            u2SessionNo);

        }

            }
            /* Reset the values for next iteration */
            u1NumOfSrc = 0;
            u1NumOfDest = 0;
            MEMSET(&HwMirrorInfo, 0, sizeof(tIssHwMirrorInfo));
        }
    }
    ISS_TRC (CONTROL_PLANE_TRC,
            "\nIssMbsmCardInsertMirrorCtrlTable updated the port related HW configuration "
            "for Port Control table and returns success ");

    return MBSM_SUCCESS;

    }

/*****************************************************************************/
/*                                                                           */
/* Function     : IssMbsmCardRemovePortCtrlTable                             */
/*                                                                           */
/* Description  : This function updates the port related HW configuration    */
/*                for Port Control Table when                                */
/*                a card is removed from the MBSM System.                    */
/*                                                                           */
/* Input        : pPortInfo  - Pointer to Port Related Info.                 */
/*              : pSlotInfo  - Pointer to Slot Related Info.                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IssMbsmCardRemoveMirrorCtrlTable (tMbsmPortInfo * pPortInfo,
                                tMbsmSlotInfo * pSlotInfo)
{
    tIssHwMirrorInfo    HwMirrorInfo;
    tIssMirrSessionInfo *pSessionInfo;
    tHwPortInfo         HwPortInfo;
    UINT4               u4StartPortIndex = 0;
    UINT4               u4TempStartIndex = 0;
    UINT4               u4LocalStartIndex = 0;
    UINT4               u4LocalEndIndex = 0;
    UINT4               u4EndPortIndex = 0;
    UINT2               u2SessionNo;
    UINT1               u1Mode;
    UINT1               u1ControlStatus;
    UINT1               u1NumOfDest = 0;
    UINT1               u1NumOfSrc = 0;
    UINT1               u1Index = 0;
  
    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));

    UNUSED_PARAM(pSlotInfo);

    if ((gIssGlobalInfo.u1MirrorStatus == ISS_MIRRORDISABLE) ||
        (pPortInfo->u4PortCount == 0))
    {
    ISS_TRC (CONTROL_PLANE_TRC,
                "\nIssMbsmCardRemoveMirrorCtrlTable: Mirroring is disabled or"
                "Ports inserted is zero; so returns success ");
        return MBSM_SUCCESS;
    }
    u4StartPortIndex = MBSM_PORT_INFO_STARTINDEX (pPortInfo);
    u4EndPortIndex = MBSM_PORT_INFO_STARTINDEX (pPortInfo) +
        MBSM_PORT_INFO_PORTCOUNT (pPortInfo);


    CfaGetLocalUnitPortInformation (&HwPortInfo);
    u4LocalStartIndex = HwPortInfo.u4StartIfIndex;
    u4LocalEndIndex = HwPortInfo.u4EndIfIndex;

    /*For each session check if the port exists in destination or source list,
       If the port exists for the sesson then update the port in hardware. */
    for (u2SessionNo = 1; u2SessionNo <= ISS_MIRR_MAX_SESSIONS; u2SessionNo++)
    {
        pSessionInfo = GET_MIRR_SESSION_INFO (u2SessionNo);
        /*Only port based Mirroring session will be affected by card insert event */
        if (pSessionInfo->u1MirroringType == ISS_MIRR_PORT_BASED) 
        {
            /* For sessions having source ports in local node and 
             * monitor ports in remote node, the mirror has to be removed in 
             * local node when the remote slot is detached */

            /* Destination port has to be set in Secondary card as well */
            if(IssMirrGetNextDestRecrd(u2SessionNo, 0, 
                        &HwMirrorInfo.u4DestList[0]) == ISS_SUCCESS)
            {
                /* Check if the Destination port is one of the Remote card's port */
                if ((HwMirrorInfo.u4DestList[0] >= u4StartPortIndex) &&
                        (HwMirrorInfo.u4DestList[0] <= u4EndPortIndex))
                {
                    /* Max of only one destination port can be configured */
                    u1NumOfDest = 1;
                }
                else
                {
                    continue;
                }
            }

            u4TempStartIndex = u4LocalStartIndex;
            /*Traverse all the ports for the local slot */
            while (u4TempStartIndex != u4LocalEndIndex)
            {
                if (IssMirrGetSrcRecrdInfo
                        (u2SessionNo, ISS_DEFAULT_CONTEXT, u4TempStartIndex,
                         &u1Mode, &u1ControlStatus) == ISS_SUCCESS)
                {
                    /*Update Hardware structure */
                    if (u1ControlStatus == ISS_VALID)
                    {
                        HwMirrorInfo.SourceList[u1NumOfSrc].u4SourceNo =
                            u4TempStartIndex;
                        HwMirrorInfo.SourceList[u1NumOfSrc].u4ContextId =
                            ISS_DEFAULT_CONTEXT;
                        HwMirrorInfo.SourceList[u1NumOfSrc].u1Mode = u1Mode;

                        u1NumOfSrc++;
                    }
                }
                u4TempStartIndex++;
            }

            /*If all the ports are traversed then configure hardware as 
              configuration completed. This scenerio will come if HW info structure
              has still space to fill information but ports
              on the card are fininshed */
            if ((u1NumOfSrc != 0) || (u1NumOfDest != 0))
            {
                HwMirrorInfo.u4RSpanContextId = pSessionInfo->u4RSpanContextId;
                HwMirrorInfo.u2RSpanVlanId = pSessionInfo->RSpanVlanId;
                HwMirrorInfo.u2SessionId = u2SessionNo;
                HwMirrorInfo.u1MirrType = pSessionInfo->u1MirroringType;
                HwMirrorInfo.u1RSpanSessionType = pSessionInfo->u1RSpanSessionType;
                HwMirrorInfo.u1Action = ISS_MIRR_REMOVE_DONE;
                ISS_TRC_ARG3 (ISS_TRC_ALL |CONTROL_PLANE_TRC,
                        "\r\nRemoving mirror locally failed for SessionNo %d  "
                        "having mirror type %d,"
                        "desination index %d , with source index/indices ",
                        u2SessionNo, HwMirrorInfo.u1MirrType,
                        HwMirrorInfo.u4DestList[0]);
                for(u1Index = 0;u1Index < u1NumOfSrc;u1Index++)
                {
                    ISS_TRC_ARG1 (ISS_TRC_ALL |CONTROL_PLANE_TRC,
                            "%d ,",HwMirrorInfo.SourceList[u1Index].u4SourceNo);
                }

                if(IsssysIssHwSetMirroring(&HwMirrorInfo)
                        == FNP_FAILURE)
                {
                    ISS_TRC_ARG1 (ISS_TRC_ALL |CONTROL_PLANE_TRC,
                            "\r\nRemoving of Mirror locally for SessionNo %d failed",
                            u2SessionNo);
                }

            }
            /* Reset the values for next iteration */
            u1NumOfSrc = 0;
            u1NumOfDest = 0;
            MEMSET(&HwMirrorInfo, 0, sizeof(tIssHwMirrorInfo));
        }
    }


    ISS_TRC (CONTROL_PLANE_TRC,
            "\nIssMbsmCardRemoveMirrorCtrlTable: updated the port related HW configuration "
            "for Port Control table and returns success ");
    return MBSM_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssMbsmCardInsertCpuMirroring                              */
/*                                                                           */
/* Description  : This function updates the port related HW configuration    */
/*                for Cpu Mirroring when                                     */
/*                a card is inserted into the MBSM System.                   */
/*                                                                           */
/* Input        : pPortInfo  - Pointer to Port Related Info.                 */
/*              : pSlotInfo  - Pointer to Slot Related Info.                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IssMbsmCardInsertCpuMirroring (tMbsmSlotInfo * pSlotInfo)
{
    INT4                i4RetVal = FNP_FAILURE;
    INT4                i4IssCpuMirrorType = 0;
    INT4                i4IssCpuMirrorToPort = 0;

  /* Updation of Cpu Mirroring parameters*/
    nmhGetIssCpuMirrorType(&i4IssCpuMirrorType);
    nmhGetIssCpuMirrorToPort(&i4IssCpuMirrorToPort);
    
    i4RetVal = IsssysIssMbsmHwSetCpuMirroring (i4IssCpuMirrorType, 
            (UINT4)i4IssCpuMirrorToPort, pSlotInfo);
    if (i4RetVal != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                "\nIssMbsmCardInsertMirrorCtrlTable : "
                "IsssysIssMbsmHwSetCpuMirroring returns failure ");
        return MBSM_FAILURE;
    }
    return MBSM_SUCCESS;
}
