/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: isssys.c,v 1.279 2017/12/28 10:40:14 siva Exp $
 *
 * Description : This file has the routines for the Iss  
 *               Startup and supporting routines for the  
 *               Iss Mib code.  
 *
 ******************************************************************************/

/* SOURCE FILE  :
 *  ---------------------------------------------------------------------------
 * |  Copyright (C) 2006 Aricent Inc . All Rights Reserved                                 |
 * |  Copyright (C) 2006 Aricent Inc . All Rights Reserved                       |
 * |                                                                           |
 * |  FILE NAME                   :  isssys.c                                  |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR            :  Aricent Inc.                       |
 * |                                                                           |
 * |  SUBSYSTEM NAME              :  ISS                                       |
 * |                                                                           |
 * |  MODULE NAME                 :  ISS                                       |
 * |                                                                           |
 * |  LANGUAGE                    :  C                                         |
 * |                                                                           |
 * |  TARGET ENVIRONMENT          :  LINUX                                     |
 * |                                                                           |
 * |  AUTHOR                      :  ISS Team                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |   
 * |  $Id: isssys.c,v 1.279 2017/12/28 10:40:14 siva Exp $ 
 * |
 * |  DESCRIPTION                 :  This file has the routines for the Iss    |
 * |                                 Startup and supporting routines for the   |
 * |                                 Iss Mib code.                             |
 *  ---------------------------------------------------------------------------
 */

#ifndef _ISSSYS_C
#define _ISSSYS_C
#include "issinc.h"
#include "msr.h"
#include "tftpc.h"

#ifdef SSH_WANTED
#include "sftpc.h"
#include "sftpapi.h"
#endif

#include "issglob.h"
#include "isscli.h"

#ifdef SNMP_2_WANTED
#include "fsisswr.h"
#endif
#include "bridge.h"
#include "fsvlan.h"
#include "garp.h"
#include "utilipvx.h"
#include "snp.h"
#include "pnac.h"

#include "rstp.h"
#include "elm.h"
#include "mstp.h"
#include "diffsrv.h"
#include "rmcli.h"
#include "isspi.h"
#include "isspiinc.h"
#include "cust.h"
#include "ip6util.h"

#ifdef NPAPI_WANTED
#include "nputil.h"
#endif
#ifndef NPAPI_WANTED
#define FNP_SUCCESS      1
#define FNP_FAILURE      0
#endif

#ifdef NPAPI_WANTED
extern tIssL2FilterEntry *IssExtGetL2FilterEntry (INT4);
extern tIssL3FilterEntry *IssExtGetL3FilterEntry (INT4);
#endif
extern tNVRAM_DATA  sNvRamData;
extern UINT4        IssMirrorCtrlExtnSrcCfg[12];
extern UINT4        IssMirrorCtrlExtnSrcMode[12];
extern UINT4        IssMirrorCtrlExtnDestCfg[12];
extern UINT1        gu1MsrRestoreCount;
extern tIssPIGlobalInfo gIssPIGlobalInfo;
extern tMsrRegParams gMsrRegTbl[ISS_MAX_PROTOCOLS];
extern INT4         gi4TftpSysLogId;
#if defined (VRF_WANTED) && defined (LNXIP4_WANTED) && defined (LINUX_310_WANTED)
extern VOID         IssNetnsDeleteAll (VOID);
#endif
#if defined (LNXIP4_WANTED)
extern VOID         LnxIpDeleteIpAddrOnRestart (VOID);
#endif
tISSGlbCfg          gIssGlbCfg;
tISSHttpCfg         gIssHttpCfg;
tOsixSemId          gIssSemId;
tOsixSemId          gMgmtSemId;
tShowRunningConfigRegParams gShowRunningConfigRegTbl[ISS_MAX_PROTOCOLS];

tIssMemPoolId       gIssFirmwarePoolId;

#define ISS_FIRMWARE_POOL_ID           gIssFirmwarePoolId
/****************************************************************************/
/*                                                                          */
/*    Function Name      : CustomStartup                                    */
/*                                                                          */
/*    Description        : This function is invoked to initialize the       */
/*                         ip address and device name for the default       */
/*                         interface from the nvram.                        */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
CustomStartup (int argc, char *argv[])
{

    IssCustRegisterNvramCallBk ();

    /* Initialising the various data-structures */
    ISS_MEMSET (&gIssConfigSaveInfo, 0, sizeof (tConfigSaveInfo));
    ISS_MEMSET (&gIssConfigRestoreInfo, 0, sizeof (tConfigRestoreInfo));
    ISS_MEMSET (&gIssGlobalInfo, 0, sizeof (tIssGlobalInfo));
    ISS_MEMSET (&gIssuStartupInfo, 0, sizeof (tIssuStartupInfo));
    ISS_MEMSET (&gIssSysGroupInfo, 0, sizeof (tIssSysGroupInfo));
    ISS_MEMSET (&gIssDlImageInfo, 0, sizeof (tIssDlImageInfo));
    ISS_MEMSET (&gIssUlLogFileInfo, 0, sizeof (tIsslogFileUlInfo));
    ISS_MEMSET (&gIssRestart, 0, sizeof (tIssRestart));
    ISS_MEMSET (&gIssStandbyRestart, 0, sizeof (tIssRestart));
    ISS_MEMSET (&gIssHwCapabilities, 0, sizeof (tIssHwCapabilities));

    gIssHwCapabilities.u1HwKnetIntf = ISS_HW_NOT_SUPPORTED;
    gIssHwCapabilities.u1HwLaWithDiffPortSpeed = ISS_HW_NOT_SUPPORTED;
    gIssHwCapabilities.u1HwUntaggedPortsForVlans = ISS_HW_SUPPORTED;
    gIssHwCapabilities.u1HwUnicastMacLearningLimit = ISS_HW_SUPPORTED;
    gIssHwCapabilities.u1HwQueueConfigOnLaPort = ISS_HW_SUPPORTED;
    gIssHwCapabilities.u1HwLagOnAllPorts = ISS_HW_SUPPORTED;
    gIssHwCapabilities.u1HwLagOnCep = ISS_HW_SUPPORTED;
    gIssHwCapabilities.u1HwSelfMacInMacTable = ISS_HW_NOT_SUPPORTED;
    gIssHwCapabilities.u1HwMoreScheduler = ISS_HW_SUPPORTED;
    gIssHwCapabilities.u1HwDwrrSupport = ISS_HW_NOT_SUPPORTED;
    gIssHwCapabilities.u1HwHighCapacityCntr = ISS_HW_NOT_SUPPORTED;
    gIssHwCapabilities.u1HwCustomerTpidAllow = ISS_HW_SUPPORTED;
    gIssHwCapabilities.u1ShapeParamEIR = ISS_HW_NOT_SUPPORTED;
    gIssHwCapabilities.u1ShapeParamEBS = ISS_HW_NOT_SUPPORTED;
    gIssHwCapabilities.u1HwMeterColorBlind = ISS_HW_SUPPORTED;
    gIssHwCapabilities.u1HwUntagCepPep = ISS_HW_SUPPORTED;

    gIssUlLogFileInfo.IssInitiateUlLogFile = ISS_FALSE;
    gIssDlImageInfo.IssInitiateDlImage = ISS_FALSE;

    gIssDlImageInfo.u4DownloadStatus = MIB_DOWNLOAD_NOT_INITIATED;
    gIssDlImageInfo.u4IssDlTransferMode = ISS_TFTP_TRANSFER_MODE;
    gIssConfigSaveInfo.u4IssConfigSaveTransferMode = ISS_TFTP_TRANSFER_MODE;
    gIssUlLogFileInfo.u4IssUlTransferMode = ISS_TFTP_TRANSFER_MODE;
    gIssConfigSaveInfo.IssConfigSaveIpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    MEMSET (gIssConfigSaveInfo.IssConfigSaveIpAddr.au1Addr, 0,
            IPVX_MAX_INET_ADDR_LEN);
    gIssConfigRestoreInfo.IssConfigRestoreIpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    MEMSET (gIssConfigRestoreInfo.IssConfigRestoreIpAddr.au1Addr, 0,
            IPVX_MAX_INET_ADDR_LEN);
    gIssDlImageInfo.IssDlImageFromIp.u1Afi = IPVX_ADDR_FMLY_IPV4;
    MEMSET (gIssDlImageInfo.IssDlImageFromIp.au1Addr, 0,
            IPVX_MAX_INET_ADDR_LEN);
    gIssUlLogFileInfo.IssUlLogFileFromIp.u1Afi = IPVX_ADDR_FMLY_IPV4;
    MEMSET (gIssUlLogFileInfo.IssUlLogFileFromIp.au1Addr, 0,
            IPVX_MAX_INET_ADDR_LEN);

    gu1IssDlImageFlag = 0;
    gu1IssUlLogFlag = 0;
    gu1IssLoginAuthMode = LOCAL_LOGIN;

    /* Read nvram file */
    if (IssReadSystemInfoFromNvRam () != ISS_SUCCESS)
    {
        /* Condition - No NvRam configuration is present */
        /* or part of/all information present is wrong */
        /* so write with existing (and correct) fields */
        /* and default values with non-existant fields */
        if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
        {
            ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        }

#ifdef WSS_WANTED
        WssReadSystemInfoFromWssNvRam ();
#endif
    }
    IssCustInit (argc, argv);

    /* Set the IssSysGroupInfo and gSystemSize Data-Structures. */
    IssInitSystemParams ();
#if defined RM_WANTED || defined MBSM_WANTED
    /* Read nodeid file and set in gIssGlobalInfo */
    IssReadNodeid ();
#endif
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssInitSystemParams                              */
/*                                                                          */
/*    Description        : This function initialize the ip address and      */
/*                         device name for the default interface            */
/*                         from the nvram.                                  */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssInitSystemParams ()
{
    tIPvXAddr          *pRestoreIP = NULL;
    UINT4               u4Count;
    UINT4               u4Len = 0;

    gIssSysGroupInfo.u4IssDefaultIpSubnetMask = IssGetSubnetMaskFromNvRam ();

    /* Login Prompt Block Parameters */
    gIssSysGroupInfo.IssBlockPrompt.u1LoginAttempts = ISS_LOGIN_ATTEMPT_DEFVAL;
    gIssSysGroupInfo.IssBlockPrompt.u4LoginLockOutTime =
        ISS_MIN_CLI_LOCK_OUT_TIME;

    if (IssValidateCidrSubnetMask (gIssSysGroupInfo.u4IssDefaultIpSubnetMask)
        != ISS_SUCCESS)
    {
        /* Condition - NvRam data is incorrect. */
        IssInitialiseNvRamDefVal ();

        /* Assumption IssValidateCidrSubnetMask will not fail after 
         * IssInitialiseNvRamDefVal */

        gIssSysGroupInfo.u4IssDefaultIpSubnetMask =
            IssGetSubnetMaskFromNvRam ();

        IssValidateCidrSubnetMask (gIssSysGroupInfo.u4IssDefaultIpSubnetMask);
    }

    /* Read default device name from the nvram */
    u4Len = ((STRLEN (IssGetInterfaceFromNvRam ()) <
              (sizeof (gIssSysGroupInfo.au1IssDefaultInterface) - 1))
             ? STRLEN (IssGetInterfaceFromNvRam ()) :
             (sizeof (gIssSysGroupInfo.au1IssDefaultInterface) - 1));
    ISS_STRNCPY ((INT1 *) &(gIssSysGroupInfo.au1IssDefaultInterface),
                 IssGetInterfaceFromNvRam (), u4Len);
    gIssSysGroupInfo.au1IssDefaultInterface[u4Len] = '\0';

    u4Len = (STRLEN (IssGetRmIfNameFromNvRam ()) <
             (sizeof (gIssSysGroupInfo.au1IssDefaultRmIfName) - 1)
             ? STRLEN (IssGetRmIfNameFromNvRam ()) :
             (sizeof (gIssSysGroupInfo.au1IssDefaultRmIfName) - 1));
    ISS_STRNCPY ((INT1 *) &(gIssSysGroupInfo.au1IssDefaultRmIfName),
                 IssGetRmIfNameFromNvRam (), (u4Len));
    gIssSysGroupInfo.au1IssDefaultRmIfName[u4Len] = '\0';
    gIssSysGroupInfo.u4IssDefaultIpAddress = IssGetIpAddrFromNvRam ();

    gu4MgmtPort = IssGetMgmtPortFromNvRam ();

#ifdef LNXIP4_WANTED
    if (gu4MgmtPort == TRUE)
    {
        /* Assmpution : when the system has mgmt port and Linux IP stack,
           routing over oob/linux vlan will be enabled */
        gu4MgmtIntfRouting = TRUE;
    }
#endif

    /* Set the Hardware version name */
    if (ISS_STRLEN (IssGetHardwareVersion ()) > ISS_STR_LEN)
    {
        ISS_MEMCPY (gIssSysGroupInfo.au1IssHardwareVersion,
                    IssGetHardwareVersion (), ISS_STR_LEN);
    }
    else
    {
        ISS_MEMCPY (gIssSysGroupInfo.au1IssHardwareVersion,
                    IssGetHardwareVersion (),
                    ISS_STRLEN (IssGetHardwareVersion ()));
    }
    /* Set System capabilities supported and enabled */
    ISS_MEMSET (gIssSysGroupInfo.au1SysCapabilitiesSupported,
                0, ISS_SYS_CAPABILITIES_LEN);
    ISS_MEMSET (gIssSysGroupInfo.au1SysCapabilitiesEnabled,
                0, ISS_SYS_CAPABILITIES_LEN);
    IssGetSysCapabilitiesSupported
        (gIssSysGroupInfo.au1SysCapabilitiesSupported);
    IssGetSysCapabilitiesEnabled (gIssSysGroupInfo.au1SysCapabilitiesEnabled);

    /* Set the Firmware version name */
    if (ISS_STRLEN (IssGetFirmwareVersion ()) > ISS_STR_LEN)
    {
        ISS_MEMCPY (gIssSysGroupInfo.au1IssFirmwareVersion,
                    IssGetFirmwareVersion (), ISS_STR_LEN);
    }
    else
    {
        ISS_MEMCPY (gIssSysGroupInfo.au1IssFirmwareVersion,
                    IssGetFirmwareVersion (),
                    ISS_STRLEN (IssGetFirmwareVersion ()));
    }

    /* Set the Configuration Mode From NvRam */
    gIssSysGroupInfo.IssCfgMode = IssGetConfigModeFromNvRam ();

    /* Set the RM details From NvRam */
    gIssSysGroupInfo.IssRmHBMode = IssGetHeartBeatModeFromNvRam ();
    gIssSysGroupInfo.IssRmRType = IssGetRedundancyTypeFromNvRam ();
    gIssSysGroupInfo.IssRmDType = IssGetRmDataPlaneFromNvRam ();

    gIssSysGroupInfo.u1IssDefaultIpAddrAllocProto = (UINT1)
        IssGetIpAddrAllocProtoFromNvRam ();

    ISS_STRCPY (gIssSysGroupInfo.au1IssRestoreFileVersion,
                ISS_RESTORE_FILE_VERSION);

    ISS_STRCPY (gIssSysGroupInfo.au1IssRestoreFileFormat,
                ISS_RESTORE_FILE_FORMAT);

    /* Set the Config restore parameters */
    gIssConfigRestoreInfo.IssInitiateConfigRestore = ISS_FALSE;

    pRestoreIP = IssGetRestoreIpAddrFromNvRam ();

    MEMCPY (&gIssConfigRestoreInfo.IssConfigRestoreIpAddr, pRestoreIP,
            sizeof (tIPvXAddr));

    gIssConfigRestoreInfo.IssConfigRestoreOption =
        IssGetRestoreOptionFromNvRam ();

    if (ISS_STRLEN (IssGetRestoreFileNameFromNvRam ()) >
        ISS_CONFIG_FILE_NAME_LEN)
    {
        ISS_MEMCPY (gIssConfigRestoreInfo.au1IssConfigRestoreFileName,
                    IssGetRestoreFileNameFromNvRam (),
                    ISS_CONFIG_FILE_NAME_LEN);
    }
    else
    {
        ISS_MEMCPY (gIssConfigRestoreInfo.au1IssConfigRestoreFileName,
                    IssGetRestoreFileNameFromNvRam (),
                    ISS_STRLEN (IssGetRestoreFileNameFromNvRam ()));
    }
    if (ISS_STRLEN (IssGetIncrSaveFileNameFromNvRam ()) >
        ISS_CONFIG_FILE_NAME_LEN)
    {
        ISS_MEMCPY (gIssConfigSaveInfo.au1IssIncrSaveFileName,
                    IssGetIncrSaveFileNameFromNvRam (),
                    ISS_CONFIG_FILE_NAME_LEN);
    }
    else
    {
        ISS_MEMCPY (gIssConfigSaveInfo.au1IssIncrSaveFileName,
                    IssGetIncrSaveFileNameFromNvRam (),
                    ISS_STRLEN (IssGetIncrSaveFileNameFromNvRam ()));
    }

    /* Set the PIM config Mode */
    gIssSysGroupInfo.IssPimMode = IssGetPimModeFromNvRam ();

    /* Set the Bridge Mode */
    gIssSysGroupInfo.IssBridgeMode = IssGetBridgeModeFromNvRam ();
    gIssSysGroupInfo.IssSnoopFwdMode = IssGetSnoopFwdModeFromNvRam ();

    ISS_MEMCPY (gIssSysGroupInfo.BaseMacAddr, sNvRamData.au1SwitchMac, 6);

    u4Len = (STRLEN (IssGetSwitchName ()) <
             (sizeof (gIssSysGroupInfo.au1IssSwitchName) - 1)
             ? STRLEN (IssGetSwitchName ()) :
             (sizeof (gIssSysGroupInfo.au1IssSwitchName) - 1));
    ISS_STRNCPY (gIssSysGroupInfo.au1IssSwitchName, IssGetSwitchName (), u4Len);
    gIssSysGroupInfo.au1IssSwitchName[u4Len] = '\0';

    /* Save file name and restore file name are same */
    u4Len = (STRLEN (IssGetRestoreFileNameFromNvRam ()) <
             (sizeof (gIssConfigSaveInfo.au1IssConfigSaveFileName) - 1)
             ? STRLEN (IssGetRestoreFileNameFromNvRam ()) :
             (sizeof (gIssConfigSaveInfo.au1IssConfigSaveFileName) - 1));
    ISS_STRNCPY (gIssConfigSaveInfo.au1IssConfigSaveFileName,
                 IssGetRestoreFileNameFromNvRam (), u4Len);
    gIssConfigSaveInfo.au1IssConfigSaveFileName[u4Len] = '\0';

    u4Len = (STRLEN (IssGetDlImageName ()) <
             (sizeof (gIssDlImageInfo.au1IssDlImageName) - 1)
             ? STRLEN (IssGetDlImageName ()) :
             (sizeof (gIssDlImageInfo.au1IssDlImageName) - 1));
    ISS_STRNCPY (gIssDlImageInfo.au1IssDlImageName, IssGetDlImageName (),
                 u4Len);
    gIssDlImageInfo.au1IssDlImageName[u4Len] = '\0';

    u4Len = (STRLEN (IssGetUlLogFileName ()) <
             (sizeof (gIssUlLogFileInfo.au1IssUlLogFileName) - 1)
             ? STRLEN (IssGetUlLogFileName ()) :
             (sizeof (gIssUlLogFileInfo.au1IssUlLogFileName) - 1));

    ISS_STRNCPY (gIssUlLogFileInfo.au1IssUlLogFileName, IssGetUlLogFileName (),
                 u4Len);
    gIssUlLogFileInfo.au1IssUlLogFileName[u4Len] = '\0';

    u4Len = (STRLEN (IssGetUlLogFileName ()) <
             (sizeof (gIssUlLogFileInfo.au1IssUlLogRemoteFileName) - 1)
             ? STRLEN (IssGetUlLogFileName ()) :
             (sizeof (gIssUlLogFileInfo.au1IssUlLogRemoteFileName) - 1));
    ISS_STRNCPY (gIssUlLogFileInfo.au1IssUlLogRemoteFileName,
                 IssGetUlLogFileName (), u4Len);
    gIssUlLogFileInfo.au1IssUlLogRemoteFileName[u4Len] = '\0';

    u4Len = (STRLEN (IssGetSysLocation ()) <
             (sizeof (gIssSysGroupInfo.au1IssLocation) - 1)
             ? STRLEN (IssGetSysLocation ()) :
             (sizeof (gIssSysGroupInfo.au1IssLocation) - 1));
    ISS_STRNCPY (gIssSysGroupInfo.au1IssLocation, IssGetSysLocation (), u4Len);
    gIssSysGroupInfo.au1IssLocation[u4Len] = '\0';

    u4Len = (STRLEN (IssGetSysContact ()) <
             (sizeof (gIssSysGroupInfo.au1IssContact) - 1)
             ? STRLEN (IssGetSysContact ()) :
             (sizeof (gIssSysGroupInfo.au1IssContact) - 1));
    ISS_STRNCPY (gIssSysGroupInfo.au1IssContact, IssGetSysContact (), u4Len);
    gIssSysGroupInfo.au1IssContact[u4Len] = '\0';

    /*Read NVRAM and get the MAC Addresses for aggregators */
    NvRamReadAggregatorMac ();

    gIssGlobalInfo.u4IssCliSerialConsole = IssGetCliSerialConsoleFromNvRam ();

    if (gIssGlobalInfo.u4IssCliSerialConsole == ISS_SERIAL_CONSOLE_FALSE)
    {
        /* Serial Console is disabled, call exported CLI function
         * to disable CLI console prompt for the serial console session
         */
        CliSetSessionBitMap (CLI_MASK_CONSOLE_SESSION);
    }

    gIssSysGroupInfo.u2IssDefaultVlanId = IssGetDefaultVlanIdFromNvRam ();
    /*code added for auto save flag under incremental save release */
    gIssConfigSaveInfo.IssAutoConfigSave = IssGetAutoFlagnFromNvRam ();
    gIssSysGroupInfo.IssIncrSaveFlag = IssGetIncrSaveFlagnFromNvRam ();
    gIssConfigSaveInfo.IssIncrSaveFlag = IssGetIncrSaveFlagnFromNvRam ();
    /*code change end */
    gIssConfigSaveInfo.IssRollbackFlag = IssGetRollbackFlagnFromNvRam ();
    gIssConfigSaveInfo.i4IssConfigSyncUp = ISS_TRUE;
    u4Len = (STRLEN (IssGetNpapiModeFromNvRam ()) <
             (sizeof (gIssSysGroupInfo.au1IssSysNpapiMode) - 1)
             ? STRLEN (IssGetNpapiModeFromNvRam ()) :
             (sizeof (gIssSysGroupInfo.au1IssSysNpapiMode) - 1));
    ISS_STRNCPY (gIssSysGroupInfo.au1IssSysNpapiMode,
                 (UINT1 *) IssGetNpapiModeFromNvRam (), u4Len);
    gIssSysGroupInfo.au1IssSysNpapiMode[u4Len] = '\0';
    gIssSysGroupInfo.u2StackPortCount = IssGetStackPortCountFromNvRam ();
    gIssSysGroupInfo.u2IssGetColdStandby =
        (UINT2) IssGetColdStandbyFromNvRam ();
    gIssSysGroupInfo.u4FrontPanelPortCount =
        IssGetFrontPanelPortCountFromNvRam ();
    gIssSysGroupInfo.b1VrfUnqMacFlag =
        (BOOL1) IssGetVrfUnqMacOptionFromNvRam ();
    gIssSysGroupInfo.IssAutomaticPortCreate =
        IssGetAutoPortCreateFlagFromNvRam ();

    /*Set the switch threshold range */
    gIssSysGroupInfo.IssSwitchThreshold.i4MinThresholdTemp =
        ISS_DEFAULT_MIN_THRESHOLD;
    gIssSysGroupInfo.IssSwitchThreshold.i4MaxThresholdTemp =
        ISS_DEFAULT_MAX_THRESHOLD;
    gIssSysGroupInfo.IssSwitchThreshold.u4MinPowerSupply =
        ISS_SWITCH_MIN_POWER_SUPPLY;
    gIssSysGroupInfo.IssSwitchThreshold.u4MaxPowerSupply =
        ISS_SWITCH_MAX_POWER_SUPPLY;
    gIssSysGroupInfo.IssSwitchThreshold.u4MaxCPUThreshold =
        ISS_SWITCH_MAX_CPU_THRESHOLD;
    gIssSysGroupInfo.IssSwitchThreshold.u4MaxRAMThreshold =
        ISS_SWITCH_MAX_RAM_USAGE;
    gIssSysGroupInfo.IssSwitchThreshold.u4MaxFlashThreshold =
        ISS_SWITCH_MAX_FLASH_USAGE;

    gIssSysGroupInfo.IssSwitchInfo.i4CurrTemperature =
        IssCustGetCurrTemperature
        (&(gIssSysGroupInfo.IssSwitchInfo.i4CurrTemperature));
    gIssSysGroupInfo.IssSwitchInfo.u4CurrCPUUsage = 0;
    gIssSysGroupInfo.IssSwitchInfo.u4CurrRAMUsage = 0;
    gIssSysGroupInfo.IssSwitchInfo.u4CurrFlashUsage = 0;
    gIssSysGroupInfo.IssSwitchInfo.u4CurrPowerSupply =
        (UINT4) IssCustGetCurrPowerSupply
        (&(gIssSysGroupInfo.IssSwitchInfo.u4CurrPowerSupply));
    gIssSysGroupInfo.b1IssDefValSaveFlag = IssGetDefValSaveFlagFromNvRam ();
    gIssConfigSaveInfo.IssDefValSaveFlag = IssGetDefValSaveFlagFromNvRam ();
    gIssGlobalInfo.u1IssHealthChkErrorReason = 0;
    gIssGlobalInfo.u4IssHealthChkMemAllocErrPoolId = 0;
    gIssGlobalInfo.u1IssHealthChkConfigRestoreStatus =
        ISS_CONFIG_RESTORE_DEFAULT;
    gIssGlobalInfo.u1IssHealthChkClearCtr = 0;
    gIssGlobalInfo.i4IssCpuMirrorType = ISS_MIRR_DISABLED;
    gIssGlobalInfo.u4IssCpuMirrorToPort = 0;
    /* Initializing the structure used for MSR Indication */

    for (u4Count = 0; u4Count < ISS_MAX_PROTOCOLS; u4Count++)
    {
        gMsrRegTbl[u4Count].bFlag = MSR_FALSE;
        gMsrRegTbl[u4Count].pMsrCompleteStatusCallBack = NULL;
    }

    /* Initializing the structure used for show running config Indication */

    for (u4Count = 0; u4Count < ISS_MAX_PROTOCOLS; u4Count++)
    {
        gShowRunningConfigRegTbl[u4Count].bFlag = ISS_FALSE;
        gShowRunningConfigRegTbl[u4Count].pShowRunningConfig = NULL;
    }

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssInitPortSpeedParams                           */
/*                                                                          */
/*    Description        : This function initialize the port speed to its   */
/*                         default values                                   */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssInitPortSpeedParams ()
{
    UINT1               u1IfType = 0;
    UINT1               u1EtherType = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4Speed = 0;

    for (u4IfIndex = 1; u4IfIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES;
         u4IfIndex++)
    {
        CfaGetIfType (u4IfIndex, &u1IfType);
        if (u1IfType == CFA_ENET)
        {
            CfaGetEthernetType (u4IfIndex, &u1EtherType);
            switch (u1EtherType)
            {
                case CFA_GI_ENET:
                    u4Speed = ISS_10GB;
                    break;
                case CFA_FA_ENET:
                    u4Speed = ISS_40GB;
                    break;
                case CFA_XE_ENET:
                    u4Speed = ISS_10GB;
                    break;
                case CFA_XL_ENET:
                    u4Speed = ISS_40GB;
                    break;
                case CFA_LVI_ENET:
                    u4Speed = ISS_1GB;
                    break;
                default:
                    break;
            }
            /* Set the port Speed According to the Ether Type */
            nmhSetIssPortCtrlSpeed ((INT4) u4IfIndex, (INT4) u4Speed);
        }
    }
    return;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSwitchMacFromNvRam                         */
/*                                                                          */
/*    Description        : This function is invoked to read the Switch Mac  */
/*                         Address from the NVRAM.                          */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : Switch Mac Address.                              */
/****************************************************************************/

UINT1              *
IssGetSwitchMacFromNvRam ()
{
    return (sNvRamData.au1SwitchMac);
}

INT1
IssGetFlashLoggingLocation (UINT1 *pu1FlashLogFileName)
{
    STRNCPY (pu1FlashLogFileName,
             sNvRamData.au1FlashLoggingFilePath,
             STRLEN (sNvRamData.au1FlashLoggingFilePath));
    return SNMP_SUCCESS;
}

INT1
IssSetFlashLoggingLocation (UINT1 *pSetValIssFlashLoggingFileName)
{
    UINT1               au1FileName[ISS_PATH_LEN];
    tOsixTaskId         TskId = 0;

    if (STRLEN (pSetValIssFlashLoggingFileName) == 0)
    {
        return SNMP_FAILURE;
    }

    /* Create the task for debug logging thread to log on mounted flash drives */
    if (UtlGetLoggerTaskId () == 0)
    {
        if (OsixTskCrt
            ((UINT1 *) "LOGF", (200 | OSIX_SCHED_RR),
             OSIX_DEFAULT_STACK_SIZE, (OsixTskEntry) LoggerTaskMain,
             0, &TskId) != OSIX_SUCCESS)
        {
            PRINTF ("Logger Task creation failed.\n");
            return SNMP_FAILURE;
        }
    }

    SNPRINTF ((CHR1 *) au1FileName, ISS_PATH_LEN, "%s",
              pSetValIssFlashLoggingFileName);

    IssSetFlashLoggingFileNameToNvRam ((UINT1 *) au1FileName);

    UtlSetLogPathForFlash (au1FileName);

    return SNMP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetFlashLoggingFileNameToNvRam                */
/*                                                                          */
/*    Description        : This function is invoked to set the Logging file */
/*                          name to the NVRAM.                              */
/*                                                                          */
/*    Input(s)           : pi1FileName - The Logging file name.             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetFlashLoggingFileNameToNvRam (UINT1 *pu1FileName)
{
    MEMSET (sNvRamData.au1FlashLoggingFilePath, 0, ISS_CONFIG_FILE_NAME_LEN);
    ISS_STRNCPY (sNvRamData.au1FlashLoggingFilePath, pu1FileName,
                 (sizeof (sNvRamData.au1FlashLoggingFilePath) - 1));
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssReadSystemInfoFromNvRam                       */
/*                                                                          */
/*    Description        : This function is invoked to read the System info */
/*                         from the NVRAM.                                  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
INT4
IssReadSystemInfoFromNvRam ()
{
    if (NvRamRead (&sNvRamData) != FNP_SUCCESS)
    {
        return ISS_FAILURE;
    }
    else
    {
#ifdef WSS_WANTED
        WssReadSystemInfoFromWssNvRam ();
#endif

        return ISS_SUCCESS;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetRestoreOptionFromNvRam                     */
/*                                                                          */
/*    Description        : This function is used to get the Restore Option  */
/*                         from the NVRAM.                                  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : RestoreOpion i.e. LOCAL/REMOTE                   */
/****************************************************************************/
UINT4
IssGetRestoreOptionFromNvRam ()
{
    return (sNvRamData.u4RestoreOption);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetImageDumpLocation                          */
/*                                                                          */
/*    Description        : This function is used to set the location        */
/*                          for dump file.                                  */
/*                                                                          */
/*    Input(s)           : pSetValIssImageDumpLocation                      */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : SNMP_SUCCESS                                     */
/****************************************************************************/

INT1
IssSetImageDumpLocation (UINT1 *pSetValIssImageDumpLocation)
{
    INT4                i4length = 0;
    MEMSET (sNvRamData.au1ImageDumpFilePath, 0, ISS_CONFIG_FILE_NAME_LEN);

    if (STRLEN (pSetValIssImageDumpLocation) == 0)
    {
        return SNMP_FAILURE;
    }

    ISS_STRNCPY (sNvRamData.au1ImageDumpFilePath, pSetValIssImageDumpLocation,
                 (sizeof (sNvRamData.au1ImageDumpFilePath) - 1));
    i4length = (INT4) STRLEN (sNvRamData.au1ImageDumpFilePath);
    if ((i4length > 0) && (i4length < (ISS_CONFIG_FILE_NAME_LEN - 2)))
    {
        sNvRamData.au1ImageDumpFilePath[i4length] = '/';
        sNvRamData.au1ImageDumpFilePath[i4length + 1] = '\0';
    }
    else
    {
        return SNMP_FAILURE;
    }

    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return SNMP_FAILURE;
    }
    OsixCoreDumpPathSetting ((UINT1 *) sNvRamData.au1ImageDumpFilePath);
    return SNMP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetPimModeFromNvRam                           */
/*                                                                          */
/*    Description        : This function is used to get the Pim Mode        */
/*                         from the NVRAM.                                  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PIM Mode i.e. ISS_SM/ISS_DM                      */
/****************************************************************************/
UINT4
IssGetPimModeFromNvRam (VOID)
{
    return (sNvRamData.u4PimMode);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetBridgeModeFromNvRam                        */
/*                                                                          */
/*    Description        : This function is used to get the Bridge Mode     */
/*                         from the NVRAM.                                  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Bridge Mode i.e. ISS_CUSTOMER_BRIDGE_MODE /      */
/*                         ISS_PROVIDER_BRIDGE_MODE /                       */
/*                         ISS_PROVIDER_EDGE_BRIDGE_MODE /                  */
/*                         ISS_PROVIDER_CORE_BRIDGE_MODE                    */
/****************************************************************************/
UINT4
IssGetBridgeModeFromNvRam (VOID)
{
    return (sNvRamData.u4BridgeMode);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSnoopFwdModeFromNvRam                        */
/*                                                                          */
/*    Description        : This function is used to get the IGS Forward Mode*/
/*                         from the NVRAM.                                  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : IGS Forward Mode i.e. ISS_IGS_IP/ISS_IGS_MAC     */
/****************************************************************************/
UINT4
IssGetSnoopFwdModeFromNvRam (VOID)
{
    return (sNvRamData.u4SnoopFwdMode);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetConfigModeFromNvRam                        */
/*                                                                          */
/*    Description        : This function is used to get the Configuration   */
/*                         mode from the NVRAM.                             */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Config Mode  i.e. ISS_CFG_MANUAL/DYNAMIC         */
/****************************************************************************/
UINT4
IssGetConfigModeFromNvRam ()
{
    return (sNvRamData.u4CfgMode);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetIpAddrAllocProtoFromNvRam                  */
/*                                                                          */
/*    Description        : This function is used to get the boot protocol   */
/*                         to used to get the ip address from the NVRAM.    */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Boot Proto  i.e. ISS_PROTO_DHCP / ISS_PROTO_BOOTP*/
/*                         ISS_PROTO_RARP                                   */
/****************************************************************************/
UINT4
IssGetIpAddrAllocProtoFromNvRam ()
{
    return (sNvRamData.u4IpAddrAllocProto);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetRestoreFileNameFromNvRam                   */
/*                                                                          */
/*    Description        : This function is invoked to get the File Name    */
/*                         from the NvRam.                                  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Restore File Name                                */
/****************************************************************************/
INT1               *
IssGetRestoreFileNameFromNvRam ()
{
    return ((INT1 *) sNvRamData.ai1RestoreFileName);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetBgpConfRestoreFileNameFromNvRam            */
/*                                                                          */
/*    Description        : This function is invoked to get the File Name    */
/*                         from the NvRam for BGP configuration             */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Restore File Name                                */
/****************************************************************************/
INT1               *
IssGetBgpConfRestoreFileNameFromNvRam ()
{
    return ((INT1 *) sNvRamData.ai1BgpConfRestoreFileName);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetCsrConfRestoreFileNameFromNvRam            */
/*                                                                          */
/*    Description        : This function is invoked to get the File Name    */
/*                         from the NvRam for BGP configuration             */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Restore File Name                                */
/****************************************************************************/
INT1               *
IssGetCsrConfRestoreFileNameFromNvRam ()
{
    return ((INT1 *) sNvRamData.ai1CsrConfRestoreFileName);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetOspfConfRestoreFileNameFromNvRam            */
/*                                                                          */
/*    Description        : This function is invoked to get the File Name    */
/*                         from the NvRam for BGP configuration             */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Restore File Name                                */
/****************************************************************************/
INT1               *
IssGetOspfConfRestoreFileNameFromNvRam ()
{
    return ((INT1 *) sNvRamData.ai1OspfConfRestoreFileName);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetIncrSaveFileNameFromNvRam                  */
/*                                                                          */
/*    Description        : This function is invoked to get the File Name    */
/*                         from the NvRam.                                  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Restore File Name                                */
/****************************************************************************/
INT1               *
IssGetIncrSaveFileNameFromNvRam ()
{
    return ((INT1 *) sNvRamData.ai1IncrSaveFileName);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetAutoFlagnFromNvRam                         */
/*                                                                          */
/*    Description        : This function is invoked to get the value of auto*/
/*                         save flag from the NvRam.                        */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : value of auto save flag                          */
/****************************************************************************/
INT4
IssGetAutoFlagnFromNvRam ()
{

    return (sNvRamData.i1AutoSaveFlag);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetIncrSaveFlagnFromNvRam                     */
/*                                                                          */
/*    Description        : This function is invoked to get the value of Incr*/
/*                         save flag from the NvRam.                        */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : value of auto save flag                          */
/****************************************************************************/
INT4
IssGetIncrSaveFlagnFromNvRam ()
{

    return (sNvRamData.i1IncrSaveFlag);
}

VOID
IssSetIncrSaveToNvRam (INT4 i4IncrSaveFlag)
{
    sNvRamData.i1IncrSaveFlag = (INT1) i4IncrSaveFlag;
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
    return;
}

/****************************************************************************/
 /*                                                                          */
 /*    Function Name      : IssGetDefValSaveFlagFromNvRam                    */
 /*                                                                          */
 /*    Description        : This function is invoked to get the value of     */
 /*                         DefVal save flag from the NvRam.                 */
 /*                                                                          */
 /*    Input(s)           : None.                                            */
 /*                                                                          */
 /*    Output(s)          : None.                                            */
 /*                                                                          */
 /*    Returns            : value of auto save flag                          */
 /****************************************************************************/
INT1
IssGetDefValSaveFlagFromNvRam ()
{

    return (sNvRamData.i1DefValSaveFlag);
}

 /****************************************************************************/
 /*                                                                          */
 /*    Function Name      : IssSetDefValSaveFlagToNvRam                      */
 /*                                                                          */
 /*    Description        : This function is invoked to set the value of     */
 /*                         DefVal save flag in NvRam.                       */
 /*                                                                          */
 /*    Input(s)           : None.                                            */
 /*                                                                          */
 /*    Output(s)          : None.                                            */
 /*                                                                          */
 /*    Returns            : value of auto save flag                          */
 /****************************************************************************/

VOID
IssSetDefValSaveFlagToNvRam (INT1 i1DefValSaveFlag)
{
    sNvRamData.i1DefValSaveFlag = i1DefValSaveFlag;

    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetAutoSaveToNvRam                     */
/*                                                                          */
/*    Description        : This function is invoked to set the value of auto*/
/*                         save flag from the NvRam.                        */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : value of auto save flag                          */
/****************************************************************************/
VOID
IssSetAutoSaveToNvRam (INT4 i4AutoSaveFlag)
{
    sNvRamData.i1AutoSaveFlag = (INT1) i4AutoSaveFlag;
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
    return;
}

/*********************************************************************************/
/*                                                                               */
/*    Function Name      : IssSetRollbackFlagToNvRam                             */
/*                                                                               */
/*    Description        : This function is invoked to set the value of Rollback */
/*                         flag to the NvRam.                                    */
/*                                                                               */
/*    Input(s)           : None.                                                 */
/*                                                                               */
/*    Output(s)          : None.                                                 */
/*                                                                               */
/*    Returns            : void                                                  */
/*********************************************************************************/
VOID
IssSetRollbackFlagToNvRam (INT4 i4RollbackFlag)
{
    sNvRamData.i1RollbackFlag = (INT1) i4RollbackFlag;
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetRollbackFlagnFromNvRam                     */
/*                                                                          */
/*    Description        : This function is invoked to get the value of Roll*/
/*                         back flag from the NvRam.                        */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : value of auto save flag                          */
/****************************************************************************/
INT4
IssGetRollbackFlagnFromNvRam ()
{

    return (sNvRamData.i1RollbackFlag);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetIpAddrFromNvRam                            */
/*                                                                          */
/*    Description        : This function is invoked to get the IP address   */
/*                         of the default interface from the NVRAM.         */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : pIpAddr - IP Address of the default interface    */
/****************************************************************************/
UINT4
IssGetIpAddrFromNvRam ()
{
    return sNvRamData.u4LocalIpAddr;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetIp6AddrFromNvRam                           */
/*                                                                          */
/*    Description        : This function is invoked to get the IPv6 address */
/*                         of the default interface from the NVRAM.         */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : pLocalIp6Addr - pointer to default IPv6 address. */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/

void
IssGetIp6AddrFromNvRam (tIp6Addr * pLocalIp6Addr)
{
    *pLocalIp6Addr = sNvRamData.localIp6Addr;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetIp6PrefixLenFromNvRam                      */
/*                                                                          */
/*    Description        : This function is invoked to get the prefix length*/
/*                         of the IPv6 address ofdefault interface from the */
/*                         NVRAM.                                           */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : value of the prefix length                       */
/****************************************************************************/

UINT1
IssGetIp6PrefixLenFromNvRam ()
{
    return sNvRamData.u1PrefixLen;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetInterfaceFromNvRam                         */
/*                                                                          */
/*    Description        : This function is invoked to get the default      */
/*                         interface name from the NVRAM.                   */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Interface Name                                   */
/****************************************************************************/
INT1               *
IssGetInterfaceFromNvRam ()
{
    return ((INT1 *) sNvRamData.ai1InterfaceName);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSoftwareVersionFromNvRam                   */
/*                                                                          */
/*    Description        : This function is invoked to get the current      */
/*                         software version from the NVRAM.                 */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Software version                                 */
/****************************************************************************/
INT1               *
IssGetSoftwareVersionFromNvRam ()
{
    return ((INT1 *) sNvRamData.ai1SoftwareVersion);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetRmIfNameFromNvRam                          */
/*                                                                          */
/*    Description        : This function is invoked to get the backplane    */
/*                         interface name from the NVRAM.                   */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Interface Name                                   */
/****************************************************************************/
INT1               *
IssGetRmIfNameFromNvRam ()
{
    return ((INT1 *) sNvRamData.ai1RmIfName);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSubnetMaskFromNvRam                        */
/*                                                                          */
/*    Description        : This function is invoked to get the Subnet Mask  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Local Subnet Mask                                */
/****************************************************************************/
UINT4
IssGetSubnetMaskFromNvRam ()
{
    return sNvRamData.u4LocalSubnetMask;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetMgmtPortFromNvRam                          */
/*                                                                          */
/*    Description        : This function is invoked to get MgmtPort flag    */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : TRUE/FALSE                                       */
/****************************************************************************/
UINT4
IssGetMgmtPortFromNvRam ()
{
    return sNvRamData.u4MgmtPort;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetRestoreIpAddrFromNvRam                     */
/*                                                                          */
/*    Description        : This function is invoked to get Restore IP Addr. */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Restoration Ip Address                           */
/****************************************************************************/
tIPvXAddr          *
IssGetRestoreIpAddrFromNvRam ()
{
    return (&(sNvRamData.RestoreIpAddr));
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetRestoreFlagFromNvRam                       */
/*                                                                          */
/*    Description        : This function is invoked to get Restore Flag.    */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Restore Flag                                     */
/****************************************************************************/
INT1
IssGetRestoreFlagFromNvRam ()
{
    return (sNvRamData.i1ResFlag);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetAsyncMode                                  */
/*                                                                          */
/*    Description        : This function is invoked to get Restore Flag.    */
/*                                                                          */
/*    Input(s)           : i4ProtoId - Protocol Identifier                  */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Restore Flag                                     */
/****************************************************************************/
INT4
IssGetAsyncMode (INT4 i4ProtoId)
{
    UNUSED_PARAM (i4ProtoId);

    if (STRCMP (gIssSysGroupInfo.au1IssSysNpapiMode, "Asynchronous") == 0)
    {
        return ((INT4) ISS_NPAPI_MODE_ASYNCHRONOUS);
    }
    else if (STRCMP (gIssSysGroupInfo.au1IssSysNpapiMode, "Synchronous") == 0)
    {
        return ((INT4) ISS_NPAPI_MODE_SYNCHRONOUS);
    }

    return ((INT4) ISS_NPAPI_MODE_SYNCHRONOUS);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetDissRolePlayed                             */
/*                                                                          */
/*    Description        : This function is invoked to get Role played by   */
/*                         the node frm nvram when Distributed ISS is used. */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Master or Slave or None                          */
/****************************************************************************/
UINT1
IssGetDissRolePlayed ()
{
    return (sNvRamData.u1DissRolePlayed);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSaveFlagFromNvRam                          */
/*                                                                          */
/*    Description        : This function is invoked to get Restore IP Addr. */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Save Flag                                        */
/****************************************************************************/
INT1
IssGetSaveFlagFromNvRam ()
{
    return (sNvRamData.i1SaveFlag);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetLoginPromptFromNvRam                       */
/*                                                                          */
/*    Description        : This function is invoked to get the Login prompt */
/*                         status from the NVRAM.                           */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : u4LoginFlag  -Login Prompt Status                */
/*                                                                          */
/****************************************************************************/

UINT4
IssGetLoginPromptFromNvRam ()
{
    return ISS_LOGIN_PROMPT_ENABLE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetCliSerialConsoleFromNvRam                  */
/*                                                                          */
/*    Description        : This function is used to get value of a variable */
/*                         that determines whether CLI console prompt       */
/*                         would be present to user or NOT                  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Flag to determine presence of CLI console        */
/****************************************************************************/
UINT4
IssGetCliSerialConsoleFromNvRam ()
{
    return (sNvRamData.u4CliSerialConsole);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSnmpEngineID                               */
/*                                                                          */
/*    Description        : This function is invoked to get Snmp EngineID.  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : pointer to Snmp EngineID                         */
/****************************************************************************/
INT1               *
IssGetSnmpEngineID ()
{
    return ((INT1 *) sNvRamData.ai1SnmpEngineID);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSnmpEngineBoots                            */
/*                                                                          */
/*    Description        : This function is invoked to get Snmp EngineID.   */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : pointer to Snmp EngineBoots                      */
/****************************************************************************/
UINT4
IssGetSnmpEngineBoots ()
{
    return (sNvRamData.u4SnmpEngineBoots);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetDefaultVlanIdFromNvRam                     */
/*                                                                          */
/*    Description        : This function is used to get the Default Vlan Id */
/*                         from the NVRAM.                                  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Default Vlan Id                                  */
/****************************************************************************/
UINT2
IssGetDefaultVlanIdFromNvRam ()
{
    return (sNvRamData.u2DefaultVlan);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetNpapiModeFromNvRam                          */
/*                                                                          */
/*    Description        : This function is used to get the NPAPI Mode of   */
/*                         Processing from the NVRAM.                       */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Default Vlan Id                                  */
/****************************************************************************/
UINT1              *
IssGetNpapiModeFromNvRam ()
{
    return ((UINT1 *) sNvRamData.au1NpapiMode);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetStackPortCountFromNvRam                    */
/*                                                                          */
/*    Description        : This function is used to get the configured      */
/*                         port count from the NVRAM.                       */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Number of valid Front Pannel Ports               */
/****************************************************************************/
UINT2
IssGetStackPortCountFromNvRam ()
{
    return (sNvRamData.u2StackPortCount);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetColdStdbyState                             */
/*                                                                          */
/*    Description        : This function is invoked to get Switch Id        */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ColdStandby                                      */
/****************************************************************************/
UINT4
IssGetColdStdbyState ()
{
    return (sNvRamData.u4ColdStandby);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetTimeStampMethodFromNvRam                   */
/*                                                                          */
/*    Description        : This function is used to get the configured      */
/*                         Time Stamp Method from NVRAM.                    */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Time Stamping Method.                            */
/****************************************************************************/
UINT2
IssGetTimeStampMethodFromNvRam (VOID)
{
    return (sNvRamData.u2TimeStampMethod);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetColdStandbyFromNvRam                       */
/*                                                                          */
/*    Description        : This function is invoked to get Switch Id        */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ColdStandby                                      */
/****************************************************************************/
UINT4
IssGetColdStandbyFromNvRam ()
{
    return (sNvRamData.u4ColdStandby);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetColdStandbyToNvRam                         */
/*                                                                          */
/*    Description        : This function is used to set the number of stack */
/*                         ports in target                                  */
/*                                                                          */
/*    Input(s)           : u2StackPortCount - Number of stack ports         */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetColdStandbyToNvRam (UINT4 u4ColdStandby)
{
    sNvRamData.u4ColdStandby = u4ColdStandby;
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetFrontPanelPortCountFromNvRam                  */
/*                                                                          */
/*    Description        : This function is used to get the configured      */
/*                         port count from the NVRAM.                       */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Number of valid Front Panel Ports               */
/****************************************************************************/
UINT4
IssGetFrontPanelPortCountFromNvRam ()
{
    return (sNvRamData.u4FrontPanelPortCount);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetVrfUnqMacOptionFromNvRam                   */
/*                                                                          */
/*    Description        : This function is used to get the VRF             */
/*                         unique mac option from the NVRAM.                */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ENABLE/DISABLE                                   */
/****************************************************************************/
UINT1
IssGetVrfUnqMacOptionFromNvRam ()
{
    return ((UINT1) sNvRamData.bVrfUnqMacOption);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetFipsOperModeFromNvRam                      */
/*                                                                          */
/*    Description        : This function is used to get the FIPS operating  */
/*                         mode from the NVRAM.                             */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ENABLE/DISABLE                                   */
/****************************************************************************/
INT4
IssGetFipsOperModeFromNvRam (VOID)
{
    return (sNvRamData.i4FipsOperMode);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetVrfUnqMacOptionToNvRam                     */
/*                                                                          */
/*    Description        : This function is used to set the VRF unique      */
/*                         mac option                                       */
/*                                                                          */
/*    Input(s)           : u1UnqMacOption - Unique mac option               */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetVrfUnqMacOptionToNvRam (UINT1 u1UnqMacOption)
{
    sNvRamData.bVrfUnqMacOption = (BOOL1) u1UnqMacOption;
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetPimModeToNvRam                             */
/*                                                                          */
/*    Description        : This function is used to set the Pim Mode        */
/*                         from the NVRAM.                                  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetPimModeToNvRam (UINT4 u4PimMode)
{
    sNvRamData.u4PimMode = u4PimMode;
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetBridgeModeToNvRam                          */
/*                                                                          */
/*    Description        : This function is used to set the Bridge Mode     */
/*                         to the NVRAM.                                    */
/*                                                                          */
/*    Input(s)           : u4BridgeMode - CB/PB/PEB/PCB.                    */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetBridgeModeToNvRam (UINT4 u4BridgeMode)
{
    sNvRamData.u4BridgeMode = u4BridgeMode;
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetSnoopFwdModeToNvRam                          */
/*                                                                          */
/*    Description        : This function is used to set the Snoop Forward Mode*/
/*                         to the NVRAM.                                    */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetSnoopFwdModeToNvRam (UINT4 u4SnoopFwdMode)
{
    sNvRamData.u4SnoopFwdMode = u4SnoopFwdMode;
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetIpAddrToNvRam                              */
/*                                                                          */
/*    Description        : This function is invoked to set the IP address   */
/*                         for the default interface to the NVRAM.          */
/*                                                                          */
/*    Input(s)           : u4IpAddr - The IP Address of default interface   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetIpAddrToNvRam (UINT4 u4IpAddr)
{
    sNvRamData.u4LocalIpAddr = u4IpAddr;
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetInterfaceToNvRam                           */
/*                                                                          */
/*    Description        : This function is invoked to set the default      */
/*                         interface name to the NVRAM.                     */
/*                                                                          */
/*    Input(s)           : pi1InterfaceName - The default interface name.   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetInterfaceToNvRam (INT1 *pi1InterfaceName)
{
    UINT4               u4Len = 0;

    u4Len = ((STRLEN (pi1InterfaceName) <
              (sizeof (sNvRamData.ai1InterfaceName) - 1))
             ? STRLEN (pi1InterfaceName) :
             (sizeof (sNvRamData.ai1InterfaceName) - 1));
    ISS_STRNCPY (sNvRamData.ai1InterfaceName, pi1InterfaceName, u4Len);
    sNvRamData.ai1InterfaceName[u4Len] = '\0';
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetDefaultVlanIdToNvRam                       */
/*                                                                          */
/*    Description        : This function is used to set the default VLAN    */
/*                         Identifier  to  NVRAM.                           */
/*                                                                          */
/*    Input(s)           : u4DefaultVlanId - VLAN Identifier                */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/

VOID
IssSetDefaultVlanIdToNvRam (UINT2 u2DefaultVland)
{
    sNvRamData.u2DefaultVlan = u2DefaultVland;
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetNpapiModeToNvRam                           */
/*                                                                          */
/*    Description        : This function is used to set the default VLAN    */
/*                         Identifier  to  NVRAM.                           */
/*                                                                          */
/*    Input(s)           : pu1NpapiMode - Pointer to NPAPI Mode             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetNpapiModeToNvRam (UINT1 *pu1NpapiMode)
{
    UINT4               u4Len = 0;

    u4Len = (STRLEN (pu1NpapiMode) <
             (sizeof (sNvRamData.au1NpapiMode) - 1)
             ? STRLEN (pu1NpapiMode) : (sizeof (sNvRamData.au1NpapiMode) - 1));
    ISS_STRNCPY (sNvRamData.au1NpapiMode, pu1NpapiMode, u4Len);
    sNvRamData.au1NpapiMode[u4Len] = '\0';
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetFrontPanelPortCountToNvRam                 */
/*                                                                          */
/*    Description        : This function is used to set the number of       */
/*                         Front panel ports in issnvarm file and target    */
/*                                                                          */
/*    Input(s)           : u4FrontPanelPortCount - Number of valid ports    */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/

VOID
IssSetFrontPanelPortCountToNvRam (UINT4 u4FrontPanelPortCount)
{
    sNvRamData.u4FrontPanelPortCount = u4FrontPanelPortCount;
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetRmIfNameToNvRam                            */
/*                                                                          */
/*    Description        : This function is invoked to set the backplane    */
/*                         interface name to the NVRAM.                     */
/*                                                                          */
/*    Input(s)           : pi1InterfaceName - The default interface name.   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetRmIfNameToNvRam (INT1 *pi1InterfaceName)
{
    MEMSET (sNvRamData.ai1RmIfName, 0, INTERFACE_STR_SIZE);
    ISS_STRNCPY (sNvRamData.ai1RmIfName, pi1InterfaceName,
                 (sizeof (sNvRamData.ai1RmIfName) - 1));
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetSoftwareVersionToNvRam                     */
/*                                                                          */
/*    Description        : This function is invoked to set the software     */
/*                         version number to the NVRAM.                     */
/*                                                                          */
/*    Input(s)           : pi1SoftwareVersion - The software version.       */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetSoftwareVersionToNvRam (INT1 *pi1SoftwareVersion)
{
    MEMSET (sNvRamData.ai1SoftwareVersion, 0, ISS_STR_LEN);
    ISS_STRNCPY (sNvRamData.ai1SoftwareVersion, pi1SoftwareVersion,
                 (sizeof (sNvRamData.ai1SoftwareVersion) - 1));
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetSubnetMaskToNvRam                          */
/*                                                                          */
/*    Description        : This function to set the IpAddress Mask          */
/*                         in the NvRam.                                    */
/*                                                                          */
/*    Input(s)           : pIpAddr - The destination IP Address             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetSubnetMaskToNvRam (UINT4 u4IpAddrMask)
{
    sNvRamData.u4LocalSubnetMask = u4IpAddrMask;
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetRestoreIpAddrToNvRam                       */
/*                                                                          */
/*    Description        : This function is invoked to set Restore IP Addr. */
/*                                                                          */
/*    Input(s)           : UINT4 u4RestoreIPAddr.                           */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : VOID.                                            */
/****************************************************************************/
VOID
IssSetRestoreIpAddrToNvRam (tIPvXAddr * pRestoreIpAddr)
{
    MEMCPY (&(sNvRamData.RestoreIpAddr), pRestoreIpAddr, sizeof (tIPvXAddr));
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetRestoreOptionToNvRam                       */
/*                                                                          */
/*    Description        : This function is used to get the Restore Option  */
/*                         from the NVRAM.                                  */
/*                                                                          */
/*    Input(s)           : RestoreOpion i.e. LOCAL/REMOTE                   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetRestoreOptionToNvRam (UINT4 u4RestoreOption)
{
    sNvRamData.u4RestoreOption = u4RestoreOption;
    if ((sNvRamData.u4RestoreOption == ISS_CONFIG_RESTORE) ||
        ((sNvRamData.u4RestoreOption == ISS_CONFIG_REMOTE_RESTORE)))
    {
        sNvRamData.i1ResFlag = SET_FLAG;
    }
    else
    {
        sNvRamData.i1ResFlag = UNSET_FLAG;
    }

    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
}

/****************************************************************************/
/*    Function Name      : IssSetConfigRestoreOption                        */
/*                                                                          */
/*    Description        : This function is used to set the configuration   */
/*                         Restore Option                                   */
/*                                                                          */
/*    Input(s)           : RestoreOption i.e. RESTORE OR NO-RESTORE         */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetConfigRestoreOption (UINT4 u4RestoreOption)
{

    gIssConfigRestoreInfo.IssConfigRestoreOption = u4RestoreOption;

    IssSetRestoreOptionToNvRam (gIssConfigRestoreInfo.IssConfigRestoreOption);
    return;
}

/****************************************************************************/
/*    Function Name      : IssSetConfigSaveOption                           */
/*                                                                          */
/*    Description        : This function is used to set the configuration   */
/*                          Save Option                                     */
/*                                                                          */
/*    Input(s)           : SAVE_FLAG = NO_SAVE or SAVE                      */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetConfigSaveOption (UINT4 u4SaveOption)
{
    gIssConfigSaveInfo.IssConfigSaveOption = u4SaveOption;

    IssSetSaveFlagToNvRam (gIssConfigSaveInfo.IssConfigSaveOption);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetConfigModeToNvRam                          */
/*                                                                          */
/*    Description        : This function is used to get the Configuration   */
/*                         mode from the NVRAM.                             */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Config Mode  i.e. ISS_CFG_MANUAL/DYNAMIC         */
/****************************************************************************/
VOID
IssSetConfigModeToNvRam (UINT4 u4CfgMode)
{
    sNvRamData.u4CfgMode = u4CfgMode;
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetIpAddrAllocProtoToNvRam                    */
/*                                                                          */
/*    Description        : This function is used to set the boot protocol   */
/*                         to used to get the ip address to the NVRAM.      */
/*                                                                          */
/*    Input(s)           : u4IpAddrAllocProto - ISS_PROTO_DHCP              */
/*                                              ISS_PROTO_BOOTP             */
/*                                              ISS_PROTO_RARP              */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetIpAddrAllocProtoToNvRam (UINT4 u4IpAddrAllocProto)
{
    sNvRamData.u4IpAddrAllocProto = u4IpAddrAllocProto;
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetRestoreFileNameToNvRam                     */
/*                                                                          */
/*    Description        : This function is invoked to set the Restore      */
/*                         File Name to the NvRam.                          */
/*                                                                          */
/*    Input(s)           : pIpAddr - The destination IP Address.            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetRestoreFileNameToNvRam (INT1 *pi1RestoreFileName)
{
    MEMSET (sNvRamData.ai1RestoreFileName, 0,
            sizeof (sNvRamData.ai1RestoreFileName));
    ISS_STRNCPY (sNvRamData.ai1RestoreFileName, pi1RestoreFileName,
                 (sizeof (sNvRamData.ai1RestoreFileName) - 1));
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetSaveFlagToNvRam                            */
/*                                                                          */
/*    Description        : This function is invoked to set Save Flag.       */
/*                                                                          */
/*    Input(s)           : INT1 i1SaveFlag.                                 */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetSaveFlagToNvRam (INT1 i1SaveFlag)
{
    sNvRamData.i1SaveFlag = i1SaveFlag;
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetRestoreFlagToNvRam                         */
/*                                                                          */
/*    Description        : This function is invoked to set Restore Flag.    */
/*                                                                          */
/*    Input(s)           : INT1 i1RestoreFlag.                              */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetRestoreFlagToNvRam (INT1 i1RestoreFlag)
{
    sNvRamData.i1ResFlag = i1RestoreFlag;
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetSwitchBaseMacAddressToNvRam                */
/*                                                                          */
/*    Description        : This function is invoked to set the Switch       */
/*                         Base MAC address to the NVRAM.                   */
/*                                                                          */
/*    Input(s)           : IssSwitchBaseMacAddress - Base MAC Address       */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetSwitchBaseMacAddressToNvRam (tMacAddr IssSwitchBaseMacAddress)
{
    ISS_MEMCPY (sNvRamData.au1SwitchMac, IssSwitchBaseMacAddress,
                sizeof (tMacAddr));

    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetStackPortCountToNvRam                      */
/*                                                                          */
/*    Description        : This function is used to set the number of stack */
/*                         ports in target                                  */
/*                                                                          */
/*    Input(s)           : u2StackPortCount - Number of stack ports         */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetStackPortCountToNvRam (UINT2 u2StackPortCount)
{
    sNvRamData.u2StackPortCount = u2StackPortCount;
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetContextMacAddress                          */
/*                                                                          */
/*    Description        : This function is invoked to get the Context      */
/*                         Base MAC address                                 */
/*                                                                          */
/*    Input(s)           : u4ContextId -Context Identifier                  */
/*                                                                          */
/*    Output(s)          : IssContextBaseMacAddress - Base MAC Address      */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssGetContextMacAddress (UINT4 u4ContextId,
                         tMacAddr pRetValIssContextMacAddress)
{
    tMacAddr            au1ContextMacAddr;
    INT4                i4Index;

    MEMCPY (au1ContextMacAddr, sNvRamData.au1SwitchMac, ISS_ETHERNET_ADDR_SIZE);

    /* Add the context ID to the NVRAM Mac address to get the
       switch mac address for the context */
    for (i4Index = ISS_ETHERNET_ADDR_SIZE - 1; i4Index >= 0; i4Index--)
    {
        if ((((UINT4) 255) - au1ContextMacAddr[i4Index]) > u4ContextId)
        {
            au1ContextMacAddr[i4Index] =
                (UINT1) (au1ContextMacAddr[i4Index] + u4ContextId);
            break;
        }
        else
        {
            u4ContextId -= (((UINT4) 255) - au1ContextMacAddr[i4Index]);
            au1ContextMacAddr[i4Index] = (UINT1) 255;
        }
    }

    MEMCPY (pRetValIssContextMacAddress, au1ContextMacAddr,
            ISS_ETHERNET_ADDR_SIZE);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetVrfUnqMacAddress                           */
/*                                                                          */
/*    Description        : This function is invoked to get the unique       */
/*                         VRF MAC address  for a specific VR               */
/*                                                                          */
/*    Input(s)           : u4VrId - Virtual router Identifier               */
/*                                                                          */
/*    Output(s)          :  Unique VRF MAC Address                          */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssGetVrfUnqMacAddress (UINT4 u4VrId, tMacAddr * pRetValIssVrfMacAddress)
{
    tMacAddr            au1VrfUnqMacAddr;
    INT4                i4Index = (ISS_ETHERNET_ADDR_SIZE - 1);

    MEMSET (au1VrfUnqMacAddr, 0, sizeof (tMacAddr));

    MEMCPY (au1VrfUnqMacAddr, sNvRamData.au1SwitchMac, ISS_ETHERNET_ADDR_SIZE);

#if ((defined (ALTA_WANTED) || defined (RRC_WANTED)) && defined (VRF_WANTED))
    /* ALTA requires one to one mapping with the last byte 
     * of virtual mac id with the u4VrId and the virtual mac id must 
     * be different from the physical mac id.hence uniques virtual mac 
     * is programmed for different u4VrId.*/
    if (u4VrId != 0)
    {
        au1VrfUnqMacAddr[i4Index] = (UINT1) (u4VrId);
        au1VrfUnqMacAddr[ISS_ETHERNET_ADDR_SIZE - 3] =
            au1VrfUnqMacAddr[ISS_ETHERNET_ADDR_SIZE - 3] + 1;
    }
#else
    /* Currently xcat alone supporting unique mac per virtual router.
     * In that case, last byte of mac address will be added with VrId. */
    au1VrfUnqMacAddr[i4Index] = (UINT1) (au1VrfUnqMacAddr[i4Index] + u4VrId);
#endif

    MEMCPY (pRetValIssVrfMacAddress, au1VrfUnqMacAddr, ISS_ETHERNET_ADDR_SIZE);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetCliSerialConsoleToNvRam                    */
/*                                                                          */
/*    Description        : This function is used to set value of a variable */
/*                         that determines whether CLI console prompt       */
/*                         would be present to user or NOT                  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Flag to determine presence of CLI console        */
/****************************************************************************/
VOID
IssSetCliSerialConsoleToNvRam (UINT4 u4CliSerialConsoleFlag)
{

    sNvRamData.u4CliSerialConsole = u4CliSerialConsoleFlag;
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetSnmpEngineIDToNvRam                        */
/*                                                                          */
/*    Description        : This function is invoked to set the SNMP         */
/*                         EngineID to the NVRAM.                           */
/*                                                                          */
/*    Input(s)           : pi1SnmpEngineID - pointer to Snmp EngineID       */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetSnmpEngineIDToNvRam (INT1 *pi1SnmpEngineID)
{
    if (pi1SnmpEngineID == NULL)
    {
        pi1SnmpEngineID = gi1SnmpEngineID;
    }
    ISS_MEMSET (sNvRamData.ai1SnmpEngineID, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);

    ISS_MEMCPY (sNvRamData.ai1SnmpEngineID, pi1SnmpEngineID,
                ISS_STRLEN (pi1SnmpEngineID));

    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetSnmpEngineBootsToNvRam                     */
/*                                                                          */
/*    Description        : This function is invoked to write the SNMP       */
/*                         EngineBoot count to the NVRAM.                   */
/*                                                                          */
/*    Input(s)           : u4SnmpEngineBoots - Snmp EngineBoots count       */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetSnmpEngineBootsToNvRam (UINT4 u4SnmpEngineBoots)
{
    sNvRamData.u4SnmpEngineBoots = u4SnmpEngineBoots;

    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetFipsOperModeToNvRam                        */
/*                                                                          */
/*    Description        : This function is invoked to write the FIPS       */
/*                         operating mode to the NVRAM.                     */
/*                                                                          */
/*    Input(s)           : i4FipsOperMode - FIPS operating mode             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetFipsOperModeToNvRam (INT4 i4FipsOperMode)
{
    sNvRamData.i4FipsOperMode = i4FipsOperMode;

    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
    }
    return;
}

    /* HITLESS RESTART */
/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetHRFlagFromNvRam                            */
/*                                                                          */
/*    Description        : This function is used to get the hitless restart */
/*                         flag from the NVRAM.                             */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Hiltess restart flag                             */
/****************************************************************************/
INT1
IssGetHRFlagFromNvRam (VOID)
{
    return (sNvRamData.i1RmHRFlag);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetHRFlagToNvRam                              */
/*                                                                          */
/*    Description        : This function is invoked to write the hiltess    */
/*                         restart flag to the NVRAM.                       */
/*                                                                          */
/*    Input(s)           : bHRFlag - Hitless restart flag                  */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetHRFlagToNvRam (INT1 i1HRFlag)
{
    sNvRamData.i1RmHRFlag = i1HRFlag;

    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam on HR flag\n");
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetRMTypeFromNvRam                            */
/*                                                                          */
/*    Description        : This function is used to get the RM Interface    */
/*                         Type flag from the NVRAM.                        */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : RM Type                                          */
/****************************************************************************/
INT4
IssGetRMTypeFromNvRam (VOID)
{
    return (INT4) (sNvRamData.u4RmStackingInterfaceType);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetRMTypeToNvRam                              */
/*                                                                          */
/*    Description        : This function is invoked to write the RM         */
/*                         type flag to the NVRAM.                          */
/*                                                                          */
/*    Input(s)           : u4RmStackingInterfaceType - RM Stacking          */
/*                                                     Interface Flag       */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetRMTypeToNvRam (UINT4 u4RmStackingInterfaceType)
{
    sNvRamData.u4RmStackingInterfaceType = u4RmStackingInterfaceType;

    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam on RM Type\n");
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetRestoreTypeFromNvRam                       */
/*                                                                          */
/*    Description        : This function is used to get the Restore         */
/*                         Type from the NVRAM                              */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Restore Type                                     */
/****************************************************************************/
INT4
IssGetRestoreTypeFromNvRam (VOID)
{
    return (INT4) (sNvRamData.u1RestoreType);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetRetoreTypeToNvRam                          */
/*                                                                          */
/*    Description        : This function is invoked to write the Restore    */
/*                         type to the NVRAM.                               */
/*                                                                          */
/*    Input(s)           : u1RestoreType - Restore Type                     */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetRestoreTypeToNvRam (UINT1 u1RestoreType)
{
    sNvRamData.u1RestoreType = u1RestoreType;

    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam on Restore Type\n");
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssInit                                          */
/*                                                                          */
/*    Description        : This function initialises the memory pools used  */
/*                         by the ISS Module.                               */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ISS_FAILURE                          */
/****************************************************************************/
INT4
IssInit (INT1 *pi1Dummy)
{
    INT4                i4RetStatus = 0;

#ifdef NPAPI_WANTED
    tNpIssHwGetCapabilities NpIssHwGetCapabilities;
    MEMSET (&NpIssHwGetCapabilities, 0, sizeof (tNpIssHwGetCapabilities));
#endif

    /* Dummy pointers for system sizing during run time */
    tIssFirmwareBlock  *pIssFirmwareBlock = NULL;
    tIssPIEgressPortBlock *pIssPIEgressPortBlock = NULL;

    UNUSED_PARAM (pIssFirmwareBlock);
    UNUSED_PARAM (pIssPIEgressPortBlock);

    UNUSED_PARAM (pi1Dummy);

    /* Assign Trace Option */
    gIssGlobalInfo.u4IssTraceOption = 0;
    gi4AuditStatus = ISS_DISABLE;
    SPRINTF ((CHR1 *) gau1IssAuditLogFileName, "%s", AUDIT_DEFAULT_FILE);

    UtlSetLogMode (CUST_LOG_MODE);
    /* MGMT_LOCK */
    if (OsixCreateSem (MGMT_PROTOCOL_SEM, 1, 0, &gMgmtSemId) != OSIX_SUCCESS)
    {
        ISS_TRC (INIT_SHUT_TRC, "Failed to create Management semaphore\n");
        ISS_INIT_COMPLETE (OSIX_FAILURE);
        return ISS_FAILURE;
    }

    /* If MSR task is used, then the UI to the switch should not be
       available to the user till the restoration process is completed.
       Hence, the MGMT_LOCK is taken to avoid access to ISS */
#ifdef MSR_WANTED
    MGMT_LOCK ();
#endif

    if (OsixCreateSem (ISS_PROTOCOL_SEM, 1, 0, &gIssSemId) != OSIX_SUCCESS)
    {
        ISS_TRC (INIT_SHUT_TRC, "Failed to create ISS semaphore\n");
        ISS_INIT_COMPLETE (OSIX_FAILURE);
        OsixSemDel (gMgmtSemId);
        return ISS_FAILURE;
    }

    if (SystemSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        ISS_TRC (INIT_SHUT_TRC, "Memory Pool Creation FAILED\n");
        ISS_INIT_COMPLETE (OSIX_FAILURE);
        OsixSemDel (gMgmtSemId);
        OsixSemDel (gIssSemId);
        SystemSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }

    ISS_CONFIGENTRY_POOL_ID =
        SYSTEMMemPoolIds[MAX_ISS_CONFIG_ENTRIES_SIZING_ID];
    ISS_PORTENTRY_POOL_ID = SYSTEMMemPoolIds[MAX_ISS_PORT_ENTRIES_SIZING_ID];
    ISS_MIRR_DEST_RECORD_POOL_ID =
        SYSTEMMemPoolIds[MAX_ISS_MIRR_DEST_RECORD_BLOCKS_SIZING_ID];
    ISS_MIRR_SRC_RECORD_POOL_ID =
        SYSTEMMemPoolIds[MAX_ISS_MIRR_SRC_RECORD_BLOCKS_SIZING_ID];
    ISS_L4SFILTERENTRY_POOL_ID =
        SYSTEMMemPoolIds[MAX_ISS_L4SFILTERENTRY_BLOCKS_SIZING_ID];
    gIssFirmwarePoolId = SYSTEMMemPoolIds[MAX_ISS_FIRMWARE_BLOCKS_SIZING_ID];
    ISS_ENT_PHY_POOL_ID = SYSTEMMemPoolIds[MAX_ISS_ENT_PHY_ENTRIES_SIZING_ID];
    /* Initialise the SLL lists for L4S Filter Table */
    ISS_SLL_INIT (&(ISS_L4SFILTER_LIST));

    /* Initialize the ISS extension module */
    if (IssExInit () != ISS_SUCCESS)
    {
        printf ("[ERROR]: ISS extension initialization failed!\r\n");
        ISS_INIT_COMPLETE (OSIX_FAILURE);
        OsixSemDel (gMgmtSemId);
        OsixSemDel (gIssSemId);
        SystemSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }

    /* Create the default entries in the ConfigCtrl Table*
     * Initialise them with the default values as  some of variable are not
     * assigned the default values in the ISS Mib. */
    if (IssCreateDefaultPort (ISS_ZERO_ENTRY) != ISS_SUCCESS)
    {
        ISS_TRC (INIT_SHUT_TRC, "Iss Default table entry creation failed\n");
        /* Indicate the status of initialization to the main routine */
        ISS_INIT_COMPLETE (OSIX_FAILURE);
        OsixSemDel (gMgmtSemId);
        OsixSemDel (gIssSemId);
        SystemSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }

    /* Initialise all the default Scarlar Objects in the MIB */
    gIssConfigSaveInfo.IssConfigSaveOption = ISS_CONFIG_NO_SAVE;
    gIssConfigSaveInfo.IssInitiateConfigSave = ISS_FALSE;
    gIssRestart = ISS_FALSE;
    gIssStandbyRestart = ISS_FALSE;

    /* Initialize by default to Not Initiated */
    gi4MibSaveStatus = MIB_SAVE_NOT_INITIATED;

    /* Initialise ISS system related tables for all ports */
    IssCreateAllPorts (ISS_ALL_TABLES);

    /*Initialize Mirroring related Data */
    IssInitMirrDataBase ();

    /* Initialize Port Isolation related Information */
    if (IssPIInitPortIsolationTable () != ISS_SUCCESS)
    {
        ISS_INIT_COMPLETE (OSIX_FAILURE);
        OsixSemDel (gMgmtSemId);
        OsixSemDel (gIssSemId);
        SystemSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }

#ifndef KERNEL_WANTED
    if (IssIpAuthMgrInit () != ISS_SUCCESS)
    {
        printf ("[ERROR]: IP Authorization initialization failed!\r\n");
        ISS_INIT_COMPLETE (OSIX_FAILURE);
        OsixSemDel (gMgmtSemId);
        OsixSemDel (gIssSemId);
        IssPIDeInitPortIsolationTable ();
        SystemSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }
#endif
#ifdef SNMP_2_WANTED
    RegisterFSISS ();
    RegisterSTDENT ();
#endif
    EntityInit ();
#ifdef DIFFSRV_WANTED
    /* Initialise the system control and system status */
    if (DsInit () != DS_SUCCESS)
    {
        printf ("[ERROR]: DS Initialization Failed! \r\n");
        ISS_INIT_COMPLETE (OSIX_FAILURE);
        OsixSemDel (gMgmtSemId);
        OsixSemDel (gIssSemId);
        IssPIDeInitPortIsolationTable ();
        SystemSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }
#endif

#ifdef FLFR_WANTED
    /* Initialise the flfr feature */
    if (FlfrInit () != FLFR_SUCCESS)
    {
        printf ("[ERROR]: FLFR Initialization Failed! \r\n");
        ISS_INIT_COMPLETE (OSIX_FAILURE);
        OsixSemDel (gMgmtSemId);
        OsixSemDel (gIssSemId);
        IssPIDeInitPortIsolationTable ();
        SystemSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }
#endif
    /* MEMSET the graceful restart structure */
    MEMSET (gIssGlobalInfo.au1IssModuleSystemControl, 0, MAX_PROTOCOLS + 3);

    /* Initialise Mac Learn Rate limit Pramaeters */
    IssInitMacLearnLimitParams ();

#ifdef NPAPI_WANTED
    /* Setting the Hardware Supported information */
    IsssysIssNpHwGetCapabilities (&NpIssHwGetCapabilities);

    ISS_MEMCPY (&gIssHwCapabilities, &NpIssHwGetCapabilities,
                sizeof (tIssHwCapabilities));
#endif

    /*Traffic Seperation Initilization */
    if (nmhGetIssAclTrafficSeperationCtrl (&i4RetStatus) == SNMP_SUCCESS)
    {
        if (i4RetStatus == ACL_TRAFFICSEPRTN_CTRL_SYSTEM_DEFAULT)
        {
            if (IssAclQosProcessL2L3Proto () == ISS_FAILURE)
            {
                ISS_TRC (INIT_SHUT_TRC, "Failed:IssAclQosProcessL2L3Proto\n");
                return (ISS_FAILURE);
            }
        }
    }

#ifdef L2RED_WANTED
    if (ISS_FAILURE == IssRedInit ())
    {
        ISS_TRC (OS_RESOURCE_TRC,
                 "\nIss -Redundancy initialization failed\r\n");
        ISS_INIT_COMPLETE (OSIX_FAILURE);
        OsixSemDel (gMgmtSemId);
        OsixSemDel (gIssSemId);
        IssPIDeInitPortIsolationTable ();
        SystemSizingMemDeleteMemPools ();
        return ISS_FAILURE;
    }
#endif /* L2RED_WANTED */
    /* Indicate the status of initialization to the main routine */
    ISS_INIT_COMPLETE (OSIX_SUCCESS);

    gu1IsIssInitCompleted = ISS_TRUE;
    gIssSysGroupInfo.i4ProvisionMode = ISS_IMMEDIATE;

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssCreateDefaultPort                                 */
/*                                                                           */
/* Description        : This function is called by IssInit for the default   */
/*                      physical port creation.  It allocates memory blocks  */
/*                      for the port.                                        */
/*                                                                           */
/* Input(s)           : u2PortIndex                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS - On success                             */
/*                      ISS_FAILURE - On failure                             */
/*****************************************************************************/
INT4
IssCreateDefaultPort (UINT2 u2PortIndex)
{
    tIssConfigCtrlEntry *pIssConfigCtrlEntry = NULL;

    /* Allocate a block for the Config Ctrl entry */
    if (ISS_CONFIGENTRY_ALLOC_MEM_BLOCK (pIssConfigCtrlEntry) == NULL)
    {

        ISS_TRC (INIT_SHUT_TRC, "ISS_CONFIGENTRY_ALLOC_MEM_BLOCK FAILED\n");
        return ISS_FAILURE;
    }

    /* Memset the ConfigCtrl entry to zero */
    ISS_MEMSET (pIssConfigCtrlEntry, 0, sizeof (tIssConfigCtrlEntry));
    gIssGlobalInfo.apIssConfigCtrlEntry[u2PortIndex] = pIssConfigCtrlEntry;
    IssSetDefaultConfigCtrlValues (u2PortIndex, pIssConfigCtrlEntry);

    /* Set the status to the default ZERO ENTRY value for the Status fields */
    pIssConfigCtrlEntry->IssConfigCtrlStatus = ISS_INVALID;

    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssHandlePortCreateFailure.                          */
/*                                                                           */
/* Description        : This function is called from IssCreatePort() when    */
/*                      it fails at any point during deletion of the port.   */
/*                                                                           */
/* Input(s)           : pIssConfig/Mirror/PortEntry                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : IssPortlist, IssAggConfigList, IssAggIfList          */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : IssPortlist, IssAggConfigList, IssAggIfList          */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS - On success                             */
/*****************************************************************************/
INT4
IssHandlePortCreateFailure (tIssConfigCtrlEntry * pIssConfigCtrlEntry,
                            tIssPortCtrlEntry * pIssPortCtrlEntry)
{
    /* Release the ConfigCtrl Mem Block to the Pool */
    if (pIssConfigCtrlEntry != NULL)
    {
        if (ISS_CONFIGENTRY_FREE_MEM_BLOCK (pIssConfigCtrlEntry) != MEM_SUCCESS)
        {
            ISS_TRC (INIT_SHUT_TRC,
                     "ISS_CONFIGENTRY_FREE_MEM_BLOCK() FAILED\n");
        }
    }
    /* Release the PortCtrl Mem Block to the Pool */
    if (pIssPortCtrlEntry != NULL)
    {
        if (ISS_PORTENTRY_FREE_MEM_BLOCK (pIssPortCtrlEntry) != MEM_SUCCESS)
        {
            ISS_TRC (INIT_SHUT_TRC, "ISS_PORTENTRY_FREE_MEM_BLOCK() FAILED\n");
        }
    }

    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssSetDefaultConfigCtrlValues                        */
/*                                                                           */
/* Description        : This function is called from IssCreatePort() to      */
/*                      initialise the Config table entry with default values*/
/*                                                                           */
/* Input(s)           : tIssConfigCtrlEntry *                                */
/*                      u2PortIndex - port index                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    :                                                      */
/*****************************************************************************/
VOID
IssSetDefaultConfigCtrlValues (UINT2 u2PortIndex,
                               tIssConfigCtrlEntry * pIssConfigCtrlEntry)
{
    /* Setting the default values for the Config Entry */
    pIssConfigCtrlEntry->IssConfigCtrlEgressStatus = ISS_ENABLE;
    pIssConfigCtrlEntry->IssConfigCtrlStatsCollection = ISS_ENABLE;
    pIssConfigCtrlEntry->IssConfigCtrlStatus = ISS_VALID;
#ifdef NPAPI_WANTED
    IsssysIssHwSetPortEgressStatus (u2PortIndex,
                                    pIssConfigCtrlEntry->
                                    IssConfigCtrlEgressStatus);
    IsssysIssHwSetPortStatsCollection (u2PortIndex,
                                       pIssConfigCtrlEntry->
                                       IssConfigCtrlStatsCollection);
#else
    UNUSED_PARAM (u2PortIndex);
#endif
}

/*****************************************************************************/
/* Function Name      : IssSetDefaultPortCtrlValues                          */
/*                                                                           */
/* Description        : This function is called from IssCreatePort() to      */
/*                      initialise the Port table entry with default values  */
/*                                                                           */
/* Input(s)           : tIssPortCtrlEntry *                                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IssSetDefaultPortCtrlValues (UINT2 u2PortIndex,
                             tIssPortCtrlEntry * pIssPortCtrlEntry)
{
    INT4                i4RetValIssPortCtrlSpeed = ISS_1GB;
    INT4                i4RetValIssPortCtrlMode = ISS_AUTO;
    /* Setting the default values for the Port Entry */
    pIssPortCtrlEntry->IssPortCtrlRenegotiate = ISS_FALSE;
    pIssPortCtrlEntry->i4IssPortCtrlMaxMacAddr = ISS_MAXMAC_PER_PORT;
    pIssPortCtrlEntry->IssPortCtrlMaxMacAction = ISS_DROPPKT;

    /* Set the Flow Control Status to Disabled by default,
     * Set the duplex to Full duples by default
     */
    pIssPortCtrlEntry->IssPortCtrlFlowControl = ISS_DISABLE;
    pIssPortCtrlEntry->IssPortCtrlDuplex = ISS_FULLDUP;
    pIssPortCtrlEntry->IssPortHOLBlockPrevention = ISS_ENABLE_HOL;
    pIssPortCtrlEntry->IssPortCtrlMdiOrMdixCap = ISS_AUTO_MDIX;
    /* Set the Speed to the default 100Mbps,if NPAPI_WANTED is no.
     * or get the port speed value from Hardware */
#ifdef NPAPI_WANTED
    if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
    {
        IsssysIssHwGetPortSpeed ((UINT4) u2PortIndex,
                                 &i4RetValIssPortCtrlSpeed);
        if (CfaGetIfAutoNegStatus
            ((UINT4) u2PortIndex, 0, &i4RetValIssPortCtrlMode) != CFA_SUCCESS)
        {
            pIssPortCtrlEntry->IssPortCtrlMode = ISS_AUTO;
        }
        pIssPortCtrlEntry->IssPortCtrlSpeed = i4RetValIssPortCtrlSpeed;
        pIssPortCtrlEntry->IssPortCtrlMode = i4RetValIssPortCtrlMode;
    }
    else
    {
        pIssPortCtrlEntry->IssPortCtrlSpeed = ISS_100MBPS;
        pIssPortCtrlEntry->IssPortCtrlMode = ISS_AUTO;
    }
#else
    UNUSED_PARAM (u2PortIndex);
    UNUSED_PARAM (i4RetValIssPortCtrlSpeed);
    UNUSED_PARAM (i4RetValIssPortCtrlMode);
    pIssPortCtrlEntry->IssPortCtrlSpeed = ISS_100MBPS;
    pIssPortCtrlEntry->IssPortCtrlMode = ISS_AUTO;
#endif
}

/*****************************************************************************/
/* Function Name     : IssCreatePort                                         */
/*                                                                           */
/* Description       : This function is called from L2-IWF module after      */
/*                     each call to BridgePortCreateIndication.              */
/*                                                                           */
/* Input(s)          : u2PortIndex                                           */
/*                                                                           */
/* Output(s)         : None                                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred          : None                                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Modified          :                                                       */
/*                                                                           */
/* Return Value(s)   : ISS_SUCCESS - On success                              */
/*                     ISS_FAILURE - On failure                              */
/*****************************************************************************/
INT4
IssCreatePort (UINT2 u2PortIndex, tIssTableName IssTableFlag)
{
    if (gu1IsIssInitCompleted == ISS_TRUE)
    {
        ISS_LOCK ();
        IssUtilCreatePort (u2PortIndex, IssTableFlag);
        ISS_UNLOCK ();
#ifdef DIFFSRV_WANTED
#ifndef MRVLLS
        DsCreatePort (u2PortIndex);
#endif
#endif
    }

    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssUtilCreatePort                                    */
/*                                                                           */
/* Description        : This function allocates memory blocks for the port   */
/*                      and sets the default values for the entries.         */
/*                                                                           */
/* Input(s)           : u2PortIndex                                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS - On success                             */
/*                      ISS_FAILURE - On failure                             */
/*****************************************************************************/
INT4
IssUtilCreatePort (UINT2 u2PortIndex, tIssTableName IssTableFlag)
{
    UINT1               u1FlagDefaultCreat = 0;
    tIssConfigCtrlEntry *pIssConfigCtrlEntry = NULL;
    tIssPortCtrlEntry  *pIssPortCtrlEntry = NULL;

    if ((u2PortIndex < ISS_MIN_PORTS)
        || (u2PortIndex > BRG_MAX_PHY_PLUS_LOG_PORTS))
    {
        return ISS_SUCCESS;
    }

    if (u2PortIndex <= ISS_MAX_PORTS)
    {
        if ((ISS_GET_CONFIGCTRL_DENTRY_STATUS == ISS_INVALID) &&
            ((IssTableFlag == ISS_ALL_TABLES) ||
             (IssTableFlag == ISS_CONFIGCTRL_TABLE)))
        {
            /* Check if an entry for config ctrl index exists */
            if (gIssGlobalInfo.apIssConfigCtrlEntry[u2PortIndex] != NULL)
            {
                ISS_TRC (INIT_SHUT_TRC,
                         "Port with same Index EXISTS Another ConfigCtrl Entry with"
                         " same Index cannot be created\n");
                return ISS_SUCCESS;
            }

            /* Allocate a block for the config ctrl entry */
            if (ISS_CONFIGENTRY_ALLOC_MEM_BLOCK (pIssConfigCtrlEntry) == NULL)
            {
                ISS_TRC (INIT_SHUT_TRC,
                         "ISS_CONFIGENTRY_ALLOC_MEM_BLOCK FAILED\n");
                return ISS_FAILURE;
            }
            u1FlagDefaultCreat =
                (UINT1) (u1FlagDefaultCreat + ISS_CFGCTRL_MASK);
            /* Memset the ConfigCtrl entry to zero */
            ISS_MEMSET (pIssConfigCtrlEntry, 0, sizeof (tIssConfigCtrlEntry));
            gIssGlobalInfo.apIssConfigCtrlEntry[u2PortIndex] =
                pIssConfigCtrlEntry;
            IssSetDefaultConfigCtrlValues (u2PortIndex, pIssConfigCtrlEntry);
        }
    }

    if ((IssTableFlag == ISS_ALL_TABLES)
        || (IssTableFlag == ISS_PORTCTRL_TABLE))
    {
        if (u2PortIndex <= ISS_MAX_PORTS)
        {
            /* Check if an entry for Port ctrl index exists */
            if (gIssGlobalInfo.apIssPortCtrlEntry[u2PortIndex - 1] != NULL)
            {
                ISS_TRC (INIT_SHUT_TRC,
                         "Port with same Index EXISTS Another Port Ctrl Entry with"
                         " same Index cannot be created\n");
                return ISS_SUCCESS;
            }

            /* Allocate a block for Port Ctrl entry */
            if (ISS_PORTENTRY_ALLOC_MEM_BLOCK (pIssPortCtrlEntry) == NULL)
            {
                if (u1FlagDefaultCreat & ISS_CFGCTRL_MASK)
                {
                    IssHandlePortCreateFailure (pIssConfigCtrlEntry, NULL);
                    gIssGlobalInfo.apIssConfigCtrlEntry[u2PortIndex] = NULL;
                }

                ISS_TRC (INIT_SHUT_TRC,
                         "ISS_RATEENTRY_ALLOC_MEM_BLOCK FAILED\n");
                return ISS_FAILURE;
            }
            u1FlagDefaultCreat =
                (UINT1) (u1FlagDefaultCreat + ISS_PORTCTRL_MASK);
            /* Memset the Port Ctrl entry to zero */
            ISS_MEMSET (pIssPortCtrlEntry, 0, sizeof (tIssPortCtrlEntry));
            IssSetDefaultPortCtrlValues (u2PortIndex, pIssPortCtrlEntry);
            gIssGlobalInfo.apIssPortCtrlEntry[u2PortIndex - 1] =
                pIssPortCtrlEntry;
        }

        if (IssExCreatePortRateCtrl (u2PortIndex, IssTableFlag) == ISS_FAILURE)
        {
            if (u1FlagDefaultCreat & ISS_CFGCTRL_MASK)
            {
                IssHandlePortCreateFailure (pIssConfigCtrlEntry, NULL);
                gIssGlobalInfo.apIssConfigCtrlEntry[u2PortIndex] = NULL;
            }

            if (u1FlagDefaultCreat & ISS_PORTCTRL_MASK)
            {
                IssHandlePortCreateFailure (NULL, pIssPortCtrlEntry);
                gIssGlobalInfo.apIssPortCtrlEntry[u2PortIndex] = NULL;
            }

            ISS_TRC (INIT_SHUT_TRC, "IssExCreatePort FAILED\n");
            return ISS_FAILURE;
        }
    }

    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssDeletePort                                        */
/*                                                                           */
/* Description        : This function is called from cfa2 module after each  */
/*                      call to BridgePortDeleteIndication. It frees         */
/*                      memory blocks for the port.                          */
/*                                                                           */
/* Input(s)           : u2PortIndex, IssTableFlag                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS - On success                             */
/*                      ISS_FAILURE - On failure                             */
/*****************************************************************************/
INT4
IssDeletePort (UINT2 u2PortIndex, tIssTableName IssTableFlag)
{
    if (gu1IsIssInitCompleted == ISS_FALSE)
    {
        return ISS_SUCCESS;
    }

    ISS_LOCK ();
    if (u2PortIndex < SYS_DEF_MAX_PHYSICAL_INTERFACES)
    {
        IssPIUtilUpdatePIEntry (u2PortIndex, 0, ISS_PI_DELETE);
    }
    else
    {
        IssPIUtilUpdatePIEntry (0, u2PortIndex, ISS_PI_DELETE);
    }
    /* Going in a loop release all the entries */
    if (IssTableFlag == ISS_ALL_TABLES)
    {
        if ((u2PortIndex < MAX_ISS_PORT_ENTRIES_LIMIT) && (u2PortIndex != 0))
        {
            IssHandlePortCreateFailure
                (gIssGlobalInfo.apIssConfigCtrlEntry[u2PortIndex],
                 gIssGlobalInfo.apIssPortCtrlEntry[u2PortIndex - 1]);

            gIssGlobalInfo.apIssConfigCtrlEntry[u2PortIndex] = NULL;
            gIssGlobalInfo.apIssPortCtrlEntry[u2PortIndex - 1] = NULL;
            IssExDeletePortRateCtrl (u2PortIndex);
        }
    }

    else if (IssTableFlag == ISS_CONFIGCTRL_TABLE)
    {
        IssHandlePortCreateFailure
            (gIssGlobalInfo.apIssConfigCtrlEntry[u2PortIndex], NULL);
        gIssGlobalInfo.apIssConfigCtrlEntry[u2PortIndex] = NULL;
    }

    else if (IssTableFlag == ISS_RATECTRL_TABLE)
    {
        IssExDeletePortRateCtrl (u2PortIndex);
    }

    else if (IssTableFlag == ISS_PORTCTRL_TABLE)
    {
        if ((u2PortIndex <= MAX_ISS_PORT_ENTRIES_LIMIT) && (u2PortIndex != 0))
        {
            IssHandlePortCreateFailure
                (NULL, gIssGlobalInfo.apIssPortCtrlEntry[u2PortIndex - 1]);
            gIssGlobalInfo.apIssPortCtrlEntry[u2PortIndex - 1] = NULL;
        }
    }
    else
    {
        ISS_UNLOCK ();
        return ISS_FAILURE;
    }

    ISS_UNLOCK ();
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssCreateAllPorts                                    */
/*                                                                           */
/* Description        : This function is called by CtrlStatus set routines.  */
/*                      It allocates memory blocks for the port and sets the */
/*                      default values for the entries.                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS - On success                             */
/*                      ISS_FAILURE - On failure                             */
/*****************************************************************************/
INT4
IssCreateAllPorts (tIssTableName IssTableFlag)
{
    tCfaIfInfo          IfInfo;
    UINT4               u4Port = 0;
    UINT4               u4PrevPort = 0;

    while (CfaGetNextActivePort (u4PrevPort, &u4Port) == CFA_SUCCESS)
    {
        if (CfaGetIfInfo (u4Port, &IfInfo) == CFA_SUCCESS)
        {
            if (IfInfo.u1IfType == CFA_ENET)
            {
                if (IssUtilCreatePort ((UINT2) u4Port, IssTableFlag) !=
                    ISS_SUCCESS)
                {
                    ISS_TRC_ARG1 (INIT_SHUT_TRC,
                                  "Failed to create port %d in ISS", u4Port);
                }
            }
        }
        u4PrevPort = u4Port;
    }

    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssInitMirrDataBase                                  */
/*                                                                           */
/* Description        : This function is called at ISS module intialization. */
/*                      It initializes default Mirroring session parameters. */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS - On success                             */
/*                      ISS_FAILURE - On failure                             */
/*****************************************************************************/
INT4
IssInitMirrDataBase ()
{
    UINT2               u2SessionNo = 0;

    for (u2SessionNo = 1; u2SessionNo <= ISS_MIRR_MAX_SESSIONS; u2SessionNo++)
    {
        /*Set RowStatus as INACTIVE for the session */
        GET_MIRR_SESSION_INFO (u2SessionNo)->u1SessionStatus =
            ISS_ROW_STATUS_DESTROY;
        TMO_SLL_Init (&GET_MIRR_SESSION_DEST_HDR (u2SessionNo));
        TMO_SLL_Init (&GET_MIRR_SESSION_SRC_HDR (u2SessionNo));

        GET_MIRR_SESSION_INFO (u2SessionNo)->u1RSpanSessionType =
            ISS_MIRR_RSPAN_DISABLED;

        GET_MIRR_SESSION_INFO (u2SessionNo)->u1MirroringType = ISS_MIRR_INVALID;
    }

    /*By default Access Mode is set as Extended MIB */
    gIssGlobalInfo.u1MirrAccessMode = ISS_MIRR_EXTENDED_MODE;
    gIssGlobalInfo.u1MirrorStatus = ISS_MIRRORENABLE;
#ifdef NPAPI_WANTED
    IsssysIssHwInitMirrDataBase ();
#endif
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssInitMirrSession                                   */
/*                                                                           */
/* Description        : This function is called at ISS module intialization. */
/*                      It initializes default Mirroring session parameters. */
/*                                                                           */
/* Input(s)           : SessionId                                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS - On success                             */
/*                      ISS_FAILURE - On failure                             */
/*****************************************************************************/
INT4
IssInitMirrSession (INT4 i4SessionNo)
{

    UINT4               u4ContextId = 0;
    UINT4               u4SrcNum = 0;
    UINT4               u4NextContextNum = 0;
    UINT4               u4NextSrcNum = 0;
    UINT4               u4DestNum = 0;
    UINT4               u4NextDestNum = 0;
    UINT1               u1Mode = 0;
    UINT1               u1ControlStatus = 0;
    UINT4               u4SeqNum = 0;
    tSnmpNotifyInfo     SnmpNotifyInfo;

    while (IssMirrGetNextSrcRecrd ((UINT4) i4SessionNo, u4ContextId, u4SrcNum,
                                   &u4NextContextNum, &u4NextSrcNum,
                                   &u1Mode, &u1ControlStatus) != ISS_FAILURE)
    {
        if (IssMirrRemoveSrcRecrd
            ((UINT4) i4SessionNo, u4NextContextNum,
             u4NextSrcNum) == ISS_FAILURE)
        {
            ISS_TRC (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                     "\nRemove record failure\n");
            ISS_TRC_FN_EXIT ();
        }
        else
        {
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, IssMirrorCtrlExtnSrcCfg,
                                  u4SeqNum, FALSE, IssLock, IssUnLock, 2,
                                  SNMP_SUCCESS);

            SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                                  i4SessionNo, u4NextSrcNum, ISS_MIRR_DELETE));

            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, IssMirrorCtrlExtnSrcMode,
                                  u4SeqNum, FALSE, IssLock, IssUnLock, 2,
                                  SNMP_SUCCESS);

            SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                                  i4SessionNo, u4NextSrcNum,
                                  ISS_MIRR_DISABLED));
        }

        u4SrcNum = u4NextSrcNum;
        u4ContextId = u4NextContextNum;

    }

    while (IssMirrGetNextDestRecrd ((UINT4) i4SessionNo, u4DestNum,
                                    &u4NextDestNum) != ISS_FAILURE)
    {
        if (IssMirrRemoveDestRecrd ((UINT4) i4SessionNo, u4NextDestNum) ==
            ISS_FAILURE)
        {
            ISS_TRC (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                     "\nRemove record failure\n");
            ISS_TRC_FN_EXIT ();
        }
        else
        {
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, IssMirrorCtrlExtnDestCfg,
                                  u4SeqNum, FALSE, IssLock, IssUnLock, 2,
                                  SNMP_SUCCESS);

            SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i",
                                  i4SessionNo, u4NextDestNum, ISS_MIRR_DELETE));
        }

        u4DestNum = u4NextDestNum;

    }

    /*Set RowStatus as INACTIVE for the session */
    GET_MIRR_SESSION_INFO (i4SessionNo)->u1SessionStatus =
        ISS_ROW_STATUS_DESTROY;

    GET_MIRR_SESSION_INFO (i4SessionNo)->u1RSpanSessionType =
        ISS_MIRR_RSPAN_DISABLED;

    GET_MIRR_SESSION_INFO (i4SessionNo)->u1MirroringType = ISS_MIRR_INVALID;
#ifdef NPAPI_WANTED
    IsssysIssHwInitMirrDataBase ();
#endif
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssReleaseAllPorts                                   */
/*                                                                           */
/* Description        : This function is called by CtrlStatus set routines.  */
/*                      It de-allocates memory blocks for the ports.         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS - On success                             */
/*                      ISS_FAILURE - On failure                             */
/*****************************************************************************/
INT4
IssReleaseAllPorts (tIssTableName IssTableFlag)
{
    UINT2               u2PortNum;

    /* Going in a loop release all the entries */
    for (u2PortNum = ISS_MIN_PORTS; u2PortNum <= ISS_MAX_PORTS; u2PortNum++)
    {
        if (IssTableFlag == ISS_ALL_TABLES)
        {
            IssHandlePortCreateFailure
                (gIssGlobalInfo.apIssConfigCtrlEntry[u2PortNum],
                 gIssGlobalInfo.apIssPortCtrlEntry[u2PortNum - 1]);

            gIssGlobalInfo.apIssConfigCtrlEntry[u2PortNum] = NULL;
            gIssGlobalInfo.apIssPortCtrlEntry[u2PortNum - 1] = NULL;
        }
        else if (IssTableFlag == ISS_CONFIGCTRL_TABLE)
        {
            IssHandlePortCreateFailure
                (gIssGlobalInfo.apIssConfigCtrlEntry[u2PortNum], NULL);
            gIssGlobalInfo.apIssConfigCtrlEntry[u2PortNum] = NULL;
        }
        else if (IssTableFlag == ISS_RATECTRL_TABLE)
        {
            IssExDeletePortRateCtrl (u2PortNum);
        }
        else if (IssTableFlag == ISS_PORTCTRL_TABLE)
        {
            IssHandlePortCreateFailure
                (NULL, gIssGlobalInfo.apIssPortCtrlEntry[u2PortNum - 1]);
            gIssGlobalInfo.apIssPortCtrlEntry[u2PortNum - 1] = NULL;
        }
        else
        {
            return ISS_FAILURE;
        }
    }
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSystemRestart                                 */
/*                                                                          */
/*    Description        : This function restarts the switch.               */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : VOID                                             */
/****************************************************************************/
VOID
IssSystemRestart ()
{
    gIssRestart = ISS_FALSE;
    gIssStandbyRestart = ISS_FALSE;

    /* During ISSU maintenance mode reboot the system immediately */
#if !defined(LINUXSIM_WANTED) && !defined(NPSIM_WANTED)
    if (IssuGetMaintModeOperation () == OSIX_TRUE)
    {
        system ("reboot");
    }
#endif

#ifdef CLI_WANTED
    /*Close all TELNET/SSH sessions */
    CliCloseAllConnections ();
#endif
#if defined LNXIP6_WANTED
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
    Lip6StopAllRadvd ();
#else
    /* Kill the currently running radvd process */
    Lip6StopRadvd (VCM_DEFAULT_CONTEXT);
#endif /* VRF_WANTED &&  LINUX_310_WANTED */

#endif

    if (ISS_GET_STACKING_MODEL () == ISS_CTRL_PLANE_STACKING_MODEL)
    {
        /* Disable IPC Sync up for NPAPI Invocation in reload case */
        /* When Active Node is restarted , no more IPC call should be
           sent to Stand-by */
#ifdef NPAPI_WANTED
        NpUtilSetIPCSyncStatus (OSIX_FALSE);
#endif
    }
    /* Shutdown all the layer2 protocols so that we get enuf
     * memory to do a system call. Protocols will check whether
     * they are started first before actually shutting itself. */
#ifdef EOAM_WANTED
    EoamModuleShutDown ();
#endif /* EOAM_WANTED */

#ifdef RFC2544_WANTED
    R2544ApiModuleShutDown ();
#endif

#ifdef Y1564_WANTED
    Y1564ApiModuleShutDown ();
#endif /* Y1564_WANTED */

#if defined (VRF_WANTED) && defined (LNXIP4_WANTED) && defined (LINUX_310_WANTED)
    IssNetnsDeleteAll ();
#endif /* VRF_WANTED */
#if defined (LNXIP4_WANTED)
    LnxIpDeleteIpAddrOnRestart ();
#endif

#if defined(LINUXSIM_WANTED) || defined(NPSIM_WANTED)
    PRINTF ("\nAlert: Switch needs to be restarted "
            "to complete the system restart process!!!!!!\n");
    /* 
     * Shutdown OSIX in case the restart is attempted in Desktop linux version, 
     * since restart cannot be triggered 
     */
    OsixShutDown ();
    return;
#endif

#ifdef RM_WANTED
#ifdef L2RED_WANTED
    /* Disable dynamic sync-up before module disable/shutdown in reload 
     * case. Reason: Module disable/shutdown sends the dynamic sync-up to
     * standby node. Dynamic sync-up should not happen in this case.  
     * Disabling dynamic sync-up should be done before disabling/shutdown 
     * of other modules. So RmDisableDynamicSyncup() should be called 
     * before module disable/shutdowm. */
    RmDisableDynamicSyncup ();
#endif
#endif

#ifdef ELPS_WANTED
    ElpsApiModuleShutDown ();
#endif /* ELPS_WANTED */

#ifdef BFD_WANTED
    BfdMainModuleShutdown ();
#endif

#ifdef MPLS_WANTED
    MplsShutdownModule ();
#endif /* MPLS_WANTED */

#ifdef GARP_WANTED
    GarpAppReleaseMemory ();
#endif /* GARP_WANTED */

#ifdef VLAN_WANTED
    VlanShutdownModule ();
#endif /* VLAN_WANTED */

#if defined IGS_WANTED || defined MLDS_WANTED
    SnoopModuleShutdown ();
#endif

#ifdef LA_WANTED
    LaShutDown ();
#endif /* LA_WANTED */
#ifdef PNAC_WANTED
    PnacModuleShutDown ();
#endif /* PNAC_WANTED */
#ifdef RSTP_WANTED
    AstReleaseMemory ();
#endif

#if defined(BGP_WANTED) && defined(MSR_WANTED)
    /*Remove the GR conf file */
    FlashRemoveFile (BGP4_GR_CONF);
    FlashRemoveFile (BGP4_CSR_CONFIG_FILE);
#endif
#if defined(NPAPI_WANTED) && !defined(NPSIM_WANTED)
    IsssysIssHwRestartSystem ();
#if !defined(MBSM_WANTED) && defined(NP_KERNEL_WANTED)
    /* Take care of sending proper signals to remote units 
     * to restart the remote systems in Chassis.
     */
    system ("reboot");
#endif
#endif /* NPAPI_WANTED */

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssMirrorStatusCheck                             */
/*                                                                          */
/*    Description        : This function checks if mirroring can be enabled */
/*                         on a given port.                                 */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : SNMP_SUCCESS if mirroring can be enabled         */
/*                         SNMP_FAILURE if mirroring cannot be enabled      */
/****************************************************************************/
INT4
IssMirrorStatusCheck (UINT2 u2Port)
{
    ISS_LOCK ();

    if ((u2Port == gu2IssMirrorToPort) &&
        (gIssMirrorStatus == ISS_MIRRORENABLE))
    {
        ISS_UNLOCK ();
        return ISS_FAILURE;
    }
    else
    {
        ISS_UNLOCK ();
        return ISS_SUCCESS;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssImageDownload                             */
/*                                                                          */
/*    Description        : Starts  image download                       */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS                                      */
/*                         ISS_FAILURE                                      */
/****************************************************************************/

INT4
IssImageDownload (UINT1 *pu1ErrBuf)
{
    INT4                i4Status = ISS_FAILURE;
    UNUSED_PARAM (pu1ErrBuf);

    if (gIssDlImageInfo.u4IssDlTransferMode == ISS_TFTP_TRANSFER_MODE)
    {
        i4Status = IssTftpImageDownload (NULL);
    }
    else if (gIssDlImageInfo.u4IssDlTransferMode == ISS_SFTP_TRANSFER_MODE)
    {
#ifdef SSH_WANTED
        i4Status = IssSftpImageDownload (NULL);
#endif
    }
    if (ISS_SUCCESS == i4Status)
    {                            /*Callback function for Customized firmware upgrade */
        if (ISS_CALLBACK_FN (ISS_EVT_CUST_FIRM_UPG).uIssAppCbFn.
            pIssCustFirmUpgradeFn != NULL)

        {

            if (ISS_FAILURE ==
                ISS_CALLBACK_FN (ISS_EVT_CUST_FIRM_UPG).uIssAppCbFn.
                pIssCustFirmUpgradeFn ((CONST UINT1 *) ISS_IMAGE_FILE,
                                       (UINT1 *) ISS_IMAGE_PATH))
            {
                return ISS_FAILURE;
            }

        }
        SYS_LOG_MSG ((SYSLOG_NOTICE_LEVEL, (UINT4) gi4TftpSysLogId,
                      "Firmware upgrade successful ..!!!"));
    }

    return (i4Status);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssTftpImageDownload                             */
/*                                                                          */
/*    Description        : Starts tftp image download                       */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS                                      */
/*                         ISS_FAILURE                                      */
/****************************************************************************/

INT4
IssTftpImageDownload (UINT1 *pu1ErrBuf)
{
    INT4                i4RetVal;
    INT4                i4Status = -1;
    UINT1               au1FileName[ISS_CONFIG_FILE_NAME_LEN];
    UINT1               au1TmpFileName[ISS_CONFIG_FILE_NAME_LEN];
    UINT4               u4Len = 0;

    gIssDlImageInfo.u4DownloadStatus = MIB_DOWNLOAD_IN_PROGRESS;
    if (STRLEN (gIssDlImageInfo.au1IssDlImageName) == 0)
    {
        if (pu1ErrBuf != NULL)
        {
            SPRINTF ((CHR1 *) pu1ErrBuf, "NULL File Name");
        }
        gIssDlImageInfo.u4DownloadStatus = LAST_MIB_DOWNLOAD_FAILED;
        return ISS_FAILURE;
    }
    if ((i4RetVal = tftpcRecvFile (gIssDlImageInfo.IssDlImageFromIp,
                                   (CONST UINT1 *) gIssDlImageInfo.
                                   au1IssDlImageName,
                                   (CONST UINT1 *) IssGetDlImageName ())) ==
        TFTPC_OK)
    {
        ISS_COPY_TO_FLASH (i4Status);
#ifdef NPAPI_WANTED
        if (i4Status == ISS_FAILURE)
        {
            gIssDlImageInfo.u4DownloadStatus = LAST_MIB_DOWNLOAD_FAILED;
            return ISS_FAILURE;
        }
#endif
        u4Len = ((STRLEN (IssGetImageName ()) < (sizeof (au1FileName) - 1))
                 ? STRLEN (IssGetImageName ()) : (sizeof (au1FileName) - 1));
        ISS_STRNCPY (au1FileName, IssGetImageName (), u4Len);
        au1FileName[u4Len] = '\0';

        u4Len = ((STRLEN (IssGetDlImageName ()) < (sizeof (au1TmpFileName) - 1))
                 ? STRLEN (IssGetDlImageName ()) : (sizeof (au1TmpFileName) -
                                                    1));
        ISS_STRNCPY (au1TmpFileName, IssGetDlImageName (), u4Len);
        au1TmpFileName[u4Len] = '\0';
        if (issMoveLocalFile (au1TmpFileName, au1FileName) != ISS_SUCCESS)
        {
            gIssDlImageInfo.u4DownloadStatus = LAST_MIB_DOWNLOAD_FAILED;
            return ISS_FAILURE;
        }

        gIssDlImageInfo.u4DownloadStatus = LAST_MIB_DOWNLOAD_SUCCESSFUL;
    }
    else
    {
        if (pu1ErrBuf != NULL)
        {
            SPRINTF ((CHR1 *) pu1ErrBuf, "[%s]",
                     (CONST CHR1 *) TFTPC_ERR_MSG (i4RetVal));
        }
        gIssDlImageInfo.u4DownloadStatus = LAST_MIB_DOWNLOAD_FAILED;
        return ISS_FAILURE;
    }
    UNUSED_PARAM (i4Status);
    return ISS_SUCCESS;
}

#ifdef SSH_WANTED
/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSftpImageDownload                             */
/*                                                                          */
/*    Description        : Starts sftp image download                       */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS                                      */
/*                         ISS_FAILURE                                      */
/****************************************************************************/

INT4
IssSftpImageDownload (UINT1 *pu1ErrBuf)
{
    UINT1               au1Buff[MAX_FILEPATH_LEN + MAX_TFTP_FILENAME_LEN] = "";
    INT4                i4RetVal;

    gIssDlImageInfo.u4DownloadStatus = MIB_DOWNLOAD_IN_PROGRESS;
    gIssDlImageInfo.u4IssDlTransferMode = ISS_SFTP_TRANSFER_MODE;
    if (STRLEN (gIssDlImageInfo.au1IssDlImageName) == 0)
    {
        if (pu1ErrBuf != NULL)
        {
            SPRINTF ((CHR1 *) pu1ErrBuf, "NULL File Name");
        }
        gIssDlImageInfo.u4DownloadStatus = LAST_MIB_DOWNLOAD_FAILED;
        return ISS_FAILURE;
    }

    SPRINTF ((CHR1 *) au1Buff, "%s%s", ISS_IMAGE_PATH, ISS_IMAGE_FILE);

    if ((i4RetVal =
         sftpcArRecvFile (gIssDlImageInfo.IssSftpDlImageInfo.au1UserName,
                          gIssDlImageInfo.IssSftpDlImageInfo.au1Passwd,
                          gIssDlImageInfo.IssDlImageFromIp,
                          (UINT1 *) gIssDlImageInfo.au1IssDlImageName,
                          (UINT1 *) au1Buff)) == SFTP_SUCCESS)
    {
        gIssDlImageInfo.u4DownloadStatus = LAST_MIB_DOWNLOAD_SUCCESSFUL;
    }
    else
    {
        if (pu1ErrBuf != NULL)
        {
            SPRINTF ((CHR1 *) pu1ErrBuf, "File Transfer Failed");
        }
        gIssDlImageInfo.u4DownloadStatus = LAST_MIB_DOWNLOAD_FAILED;

        /*SFTP file tranfer failed.So copied file will be removed here */
        FileDelete (au1Buff);

        return ISS_FAILURE;
    }

    return ISS_SUCCESS;
}
#endif
/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssUploadLogs                                    */
/*                                                                          */
/*    Description        : Starts upload logs                               */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS                                      */
/*                         ISS_FAILURE                                      */
/****************************************************************************/
INT4
IssUploadLogs (UINT1 *pu1ErrBuf)
{
    INT4                i4Status = ISS_FAILURE;
    UNUSED_PARAM (pu1ErrBuf);

    if (gIssUlLogFileInfo.u4IssUlTransferMode == ISS_TFTP_TRANSFER_MODE)
    {
        i4Status = IssTftpUploadLogs (ISS_ACTIVE_LOG, NULL);
    }
    else if (gIssUlLogFileInfo.u4IssUlTransferMode == ISS_SFTP_TRANSFER_MODE)
    {
#ifdef SSH_WANTED
        i4Status = IssSftpUploadLogs (ISS_ACTIVE_LOG, NULL);
#endif
    }
    return (i4Status);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssTftpUploadLogs                                */
/*                                                                          */
/*    Description        : Starts tftp to upload logs                       */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS                                      */
/*                         ISS_FAILURE                                      */
/****************************************************************************/
INT4
IssTftpUploadLogs (UINT1 u1Status, UINT1 *pu1ErrBuf)
{
    INT4                i4RetVal;
    UINT4               u4Count = 0;
    FILE               *pPeerLogFile = NULL;

    gi4RemoteSaveStatus = MIB_DOWNLOAD_IN_PROGRESS;
    /* Check is added to fix klocwork warning.
     * If gpu1StrFlash is NULL, then the below
     * memset would crash.
     * */

    if (gpu1StrFlash != NULL)
    {
        /* Get the trace to buffer */
        ISS_MEMSET (gpu1StrFlash, 0, ISS_LOG_BUFFER_SIZE);
        /* The size of gpu1StrFlash should be > (UTL_MAX_LOGS*UTL_MAX_LOG_LEN) */
        if (u1Status == ISS_ACTIVE_LOG)
        {
            gIssUlLogFileInfo.u4LogSize =
                (UINT4) UtlGetLogs ((CHR1 *) gpu1StrFlash, UTL_MAX_LOGS);
            gIssUlLogFileInfo.pu1LogBuf = gpu1StrFlash;
        }
        if (u1Status == ISS_STANDBY_LOG)
        {
            pPeerLogFile = FOPEN (ISS_STANDBY_LOG_FILE, "r");
            if (pPeerLogFile == NULL)
            {
                printf ("\r Error in opening the file \n");
                return ISS_FAILURE;
            }
            gIssUlLogFileInfo.pu1LogBuf = gpu1StrFlash;
            gIssUlLogFileInfo.u4LogSize = 0;
            while (!feof (pPeerLogFile))
            {
                if ((gIssUlLogFileInfo.u4LogSize + UTL_MAX_LOGS) >
                    ISS_LOG_BUFFER_SIZE)
                {
                    break;
                }

                if ((u4Count =
                     fread ((gIssUlLogFileInfo.pu1LogBuf +
                             gIssUlLogFileInfo.u4LogSize), 1, UTL_MAX_LOGS,
                            pPeerLogFile)) > 0)
                {
                    gIssUlLogFileInfo.u4LogSize += u4Count;
                    continue;
                }
                else
                {
                    printf ("\n Error in Reading file,File does not exist\n");
                    break;
                }
            }
            fflush (pPeerLogFile);
            fclose (pPeerLogFile);
        }
    }
    else
    {
        if (pu1ErrBuf != NULL)
        {
            SPRINTF ((CHR1 *) pu1ErrBuf, "Flash Pointer is NULL");
        }
        gi4RemoteSaveStatus = LAST_MIB_DOWNLOAD_FAILED;
        return ISS_FAILURE;
    }

    if (STRLEN (gIssUlLogFileInfo.au1IssUlLogFileName) == 0)
    {
        if (pu1ErrBuf != NULL)
        {
            SPRINTF ((CHR1 *) pu1ErrBuf, "NULL File Name");
        }
        gi4RemoteSaveStatus = LAST_MIB_DOWNLOAD_FAILED;
        return ISS_FAILURE;
    }
    if (gIssUlLogFileInfo.pu1LogBuf == NULL || gIssUlLogFileInfo.u4LogSize <= 0)
    {
        if (pu1ErrBuf != NULL)
        {
            SPRINTF ((CHR1 *) pu1ErrBuf, "[%s]", "Log Buffer is Empty");
        }
        gi4RemoteSaveStatus = LAST_MIB_DOWNLOAD_FAILED;
        return ISS_FAILURE;
    }
    if ((i4RetVal = tftpcSendBuf (gIssUlLogFileInfo.IssUlLogFileFromIp,
                                  gIssUlLogFileInfo.au1IssUlLogFileName,
                                  gIssUlLogFileInfo.pu1LogBuf,
                                  gIssUlLogFileInfo.u4LogSize)) == TFTPC_OK)
    {
        gi4RemoteSaveStatus = LAST_MIB_DOWNLOAD_SUCCESSFUL;
    }
    else
    {
        if (pu1ErrBuf != NULL)
        {
            SPRINTF ((CHR1 *) pu1ErrBuf, "[%s]",
                     (CONST CHR1 *) TFTPC_ERR_MSG (i4RetVal));
        }
        gi4RemoteSaveStatus = LAST_MIB_DOWNLOAD_FAILED;
        return ISS_FAILURE;
    }
    return ISS_SUCCESS;
}

#ifdef SSH_WANTED
/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSftpUploadLogs                                */
/*                                                                          */
/*    Description        : Starts Sftp to upload logs                       */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS                                      */
/*                         ISS_FAILURE                                      */
/****************************************************************************/
INT4
IssSftpUploadLogs (UINT1 u1Status, UINT1 *pu1ErrBuf)
{
    INT4                i4RetVal;
    UINT4               u4Count = 0;
    CHR1                i1SftpError[MSG_BUF_SIZE];
    FILE               *pPeerLogFile = NULL;
    MEMSET (i1SftpError, '\0', MSG_BUF_SIZE);

    gi4RemoteSaveStatus = MIB_DOWNLOAD_IN_PROGRESS;

    /* Check is added to fix klocwork warning.
     * If gpu1StrFlash is NULL, then the below
     * memset would crash.
     * */
    if (gpu1StrFlash != NULL)
    {
        /* Get the trace to buffer */
        ISS_MEMSET (gpu1StrFlash, 0, ISS_LOG_BUFFER_SIZE);
        /* The size of gpu1StrFlash should be > (UTL_MAX_LOGS*UTL_MAX_LOG_LEN) */
        if (u1Status == ISS_ACTIVE_LOG)
        {
            gIssUlLogFileInfo.u4LogSize =
                (UINT4) UtlGetLogs ((CHR1 *) gpu1StrFlash, UTL_MAX_LOGS);
            gIssUlLogFileInfo.pu1LogBuf = gpu1StrFlash;
        }
        if (u1Status == ISS_STANDBY_LOG)
        {
            pPeerLogFile = FOPEN (ISS_STANDBY_LOG_FILE, "r");
            if (pPeerLogFile == NULL)
            {
                printf ("\r Error in opening the file \n");
                return ISS_FAILURE;
            }
            gIssUlLogFileInfo.pu1LogBuf = gpu1StrFlash;
            gIssUlLogFileInfo.u4LogSize = 0;
            while (!feof (pPeerLogFile))
            {
                if ((gIssUlLogFileInfo.u4LogSize + UTL_MAX_LOGS) >
                    ISS_LOG_BUFFER_SIZE)
                {
                    break;
                }

                if ((u4Count =
                     fread ((gIssUlLogFileInfo.pu1LogBuf +
                             gIssUlLogFileInfo.u4LogSize), 1, UTL_MAX_LOGS,
                            pPeerLogFile)) > 0)
                {
                    gIssUlLogFileInfo.u4LogSize += u4Count;
                    continue;
                }
                else
                {
                    printf ("\n Error in Reading file,File does not exist\n");
                    break;
                }
            }
            fflush (pPeerLogFile);
            fclose (pPeerLogFile);
        }
    }
    else
    {
        if (pu1ErrBuf != NULL)
        {
            SPRINTF ((CHR1 *) pu1ErrBuf, "Flash Pointer is NULL");
        }
        gi4RemoteSaveStatus = LAST_MIB_DOWNLOAD_FAILED;
        return ISS_FAILURE;
    }

    if (STRLEN (gIssUlLogFileInfo.au1IssUlLogFileName) == 0)
    {
        if (pu1ErrBuf != NULL)
        {
            SPRINTF ((CHR1 *) pu1ErrBuf, "NULL File Name");
        }
        gi4RemoteSaveStatus = LAST_MIB_DOWNLOAD_FAILED;
        return ISS_FAILURE;
    }
    if (gIssUlLogFileInfo.pu1LogBuf == NULL || gIssUlLogFileInfo.u4LogSize <= 0)
    {
        if (pu1ErrBuf != NULL)
        {
            SPRINTF ((CHR1 *) pu1ErrBuf, "[%s]", "Log Buffer is Empty");
        }
        gi4RemoteSaveStatus = LAST_MIB_DOWNLOAD_FAILED;
        return ISS_FAILURE;
    }

    if ((i4RetVal =
         sftpcArSendLogBuffer (gIssUlLogFileInfo.IssSftpUlLogInfo.au1UserName,
                               gIssUlLogFileInfo.IssSftpUlLogInfo.au1Passwd,
                               gIssUlLogFileInfo.IssUlLogFileFromIp,
                               gIssUlLogFileInfo.au1IssUlLogFileName,
                               gIssUlLogFileInfo.pu1LogBuf,
                               gIssUlLogFileInfo.u4LogSize)) == SFTP_SUCCESS)
    {
        gi4RemoteSaveStatus = LAST_MIB_DOWNLOAD_SUCCESSFUL;
    }
    else
    {
        if (pu1ErrBuf != NULL)
        {
            SftpcArErrString (i1SftpError);
            SPRINTF ((CHR1 *) pu1ErrBuf, "[%s]", i1SftpError);
        }
        gi4RemoteSaveStatus = LAST_MIB_DOWNLOAD_FAILED;
        return ISS_FAILURE;
    }
    return ISS_SUCCESS;

}
#endif
/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssIsPortListValid ()                            */
/*                                                                          */
/*    Description        : This function checks if the Portlist contains    */
/*                         Valid Ports or not.                              */
/*                                                                          */
/*    Input(s)           : pu1Ports - Portlist bitmap.                      */
/*                         i4Len    - Size of pu1Ports in bytes             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_TRUE, if all the ports are valid.            */
/*                         ISS_FALSE, otherwise.                            */
/****************************************************************************/
INT4
IssIsPortListValid (UINT1 *pu1Ports, INT4 i4Len)
{
    UINT1               u1Mod;
    UINT1               u1Byte;

    if (i4Len > ISS_PORT_LIST_SIZE)
    {
        return ISS_FALSE;
    }

    if (i4Len == ISS_PORT_LIST_SIZE)
    {
        u1Byte = 0xFF;
#ifdef NPAPI_WANTED
        u1Mod = (UINT1) (ISS_MAX_PORTS % 8);

        if (u1Mod != 0)
        {
            u1Byte = (UINT1) (u1Byte >> u1Mod);

            if ((u1Byte & pu1Ports[ISS_PORT_LIST_SIZE - 1]) != 0)
            {
                return ISS_FALSE;
            }
        }
#else
        UNUSED_PARAM (u1Mod);
        UNUSED_PARAM (pu1Ports);
        UNUSED_PARAM (u1Byte);
#endif
    }

    return ISS_TRUE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssValidateFileName ()                           */
/*                                                                          */
/*    Description        : This function checks if the passed string        */
/*                         contains unwanted special characters             */
/*                                                                          */
/*    Input(s)           : pu1String - Null terminated string               */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_TRUE, if the string doesn't contain special  */
/*                                   characters                             */
/*                         ISS_FALSE, otherwise.                            */
/****************************************************************************/

UINT1
IssValidateFileName (UINT1 *pu1String)
{
    while (*pu1String != '\0')
    {
        if (!(((*pu1String) >= 'a' && (*pu1String) <= 'z') ||
              ((*pu1String) >= 'A' && (*pu1String) <= 'Z') ||
              ((*pu1String) >= '0' && (*pu1String) <= '9') ||
              ((*pu1String) == '-') || ((*pu1String) == '_') ||
              ((*pu1String) == '/') || ((*pu1String) == '.')))

        {
            return ISS_FALSE;
        }

        pu1String++;
    }

    return ISS_TRUE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSysGetSwitchName                              */
/*                                                                          */
/*    Description        : This function is invoked to get the Switch name  */
/*                         from the ISS Group Information                   */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : Pointer to the switch name string                */
/****************************************************************************/
UINT1              *
IssSysGetSwitchName (VOID)
{
    return (gIssSysGroupInfo.au1IssSwitchName);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetSwitchNameToNvRam                          */
/*                                                                          */
/*    Description        : This function is invoked to set the Switch       */
/*                         Name to the NvRam.                               */
/*                                                                          */
/*    Input(s)           : pu1SwitchName - Switch Name                      */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetSwitchNameToNvRam (UINT1 *pu1SwitchName)
{
    MEMSET (sNvRamData.ai1SwitchName, 0, sizeof (ISS_STR_LEN));
    ISS_STRNCPY (sNvRamData.ai1SwitchName, pu1SwitchName, (ISS_STR_LEN - 1));
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write Switch Name to NvRam\n");
        return;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSysGetDefSwitchIp                                */
/*                                                                          */
/*    Description        : This function is invoked to get the Default      */
/*                            Switch IP from the ISS Group Information        */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : Pointer to the switch Ip                            */
/****************************************************************************/
UINT4
IssSysGetDefSwitchIp (VOID)
{
    return (gIssSysGroupInfo.u4IssDefaultIpAddress);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssIsFrontPanelPortValid                      */
/*                                                                          */
/*    Description        : This function checks if given interface index    */
/*                         is a valid front pannel port                     */
/*                                                                          */
/*    Input(s)           : u2IfIndex - Interface Index                      */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_TRUE, if the given index is configured       */
/*                         ISS_FALSE, otherwise.                            */
/****************************************************************************/
INT4
IssIsFrontPanelPortValid (UINT2 u2IfIndex)
{

    if ((u2IfIndex > gIssSysGroupInfo.u4FrontPanelPortCount) &&
        (u2IfIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES))
    {
        /* the given index is greater than the user configured number of    */
        /* front pannel ports, hence physically */
        /* the port may not be present - return false (1)                    */
        return ISS_FALSE;
    }

    return ISS_TRUE;
}

/*****************************************************************************/
/* Function Name      : IssCallBackRegister                                  */
/*                                                                           */
/* Description        : This function registers the call backs from          */
/*                      application                                          */
/*                                                                           */
/* Input(s)           : u4Event - Event for which callback is registered     */
/*                      pFsCbInfo - Call Back Function                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/
INT4
IssCallBackRegister (UINT4 u4Event, tFsCbInfo * pFsCbInfo)
{
    INT4                i4RetVal = ISS_SUCCESS;

    switch (u4Event)
    {
        case ISS_EVT_SYS_INIT:
            ISS_CALLBACK_FN (u4Event).uIssAppCbFn.pIssAppSysInitFn
                = pFsCbInfo->pIssInitCallBack;
            break;
        case ISS_EVT_CUST_FIRM_UPG:
            ISS_CALLBACK_FN (u4Event).uIssAppCbFn.pIssCustFirmUpgradeFn
                = pFsCbInfo->pIssCustFirmUpgrade;
            break;
        default:
            i4RetVal = ISS_FAILURE;

            PRINTF ("ISS_SYS: Unidentified Event %u\n\r", u4Event);
            break;
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : IssRegisterProtocol                                  */
/*                                                                           */
/* Description        : This function registers the call backs from          */
/*                      ISIS module                                          */
/*                                                                           */
/* Input(s)           : u4Event - Event for which callback is registered     */
/*                      pIssHostNmeUpdtFn - Call Back Function               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/

PUBLIC VOID
IssRegisterProtocol (tIssRegInfo * pIssRegInfo)
{
    IssLock ();
    if (pIssRegInfo->u1InfoMask == ISS_HOSTNME_CHG_REQ)
    {
        ISS_CALLBACK_FN (ISS_EVT_HOSTNME_UPDT).uIssAppCbFn.pIssHostNmeUpdtFn
            = pIssRegInfo->pHostNmeChng;
    }
    IssUnLock ();
}

/*****************************************************************************/
/* Function Name      : IssDeRegisterProtocol                                  */
/*                                                                           */
/* Description        : This function deregisters the call backs from          */
/*                      ISIS module                                          */
/*                                                                           */
/* Input(s)           : None     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/

PUBLIC VOID
IssDeRegisterProtocol ()
{
    IssLock ();
    ISS_CALLBACK_FN (ISS_EVT_HOSTNME_UPDT).uIssAppCbFn.pIssHostNmeUpdtFn = NULL;
    IssUnLock ();
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssValidateCidrSubnetMask                        */
/*                                                                          */
/*    Description        : This function is validates the given subnet mask */
/*                         with the cidr table.                             */
/*                                                                          */
/*    Input(s)           : u4SubnetMask - SubNetMask.                       */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ISS_SUCCESS / ISS_FAILURE                        */
/****************************************************************************/

INT4
IssValidateCidrSubnetMask (UINT4 u4SubnetMask)
{
    UINT1               u1Counter;

    /* The valid subnet address is from 0 to 32 */
    for (u1Counter = 0; u1Counter <= ISS_MAX_CIDR; ++u1Counter)
    {
        if (gu4IssCidrSubnetMask[u1Counter] == u4SubnetMask)
        {
            break;
        }
    }

    if (u1Counter > ISS_MAX_CIDR)
    {
        return ISS_FAILURE;
    }
    else
    {
        return ISS_SUCCESS;
    }

}

/*****************************************************************************/
/* Function Name      : IssLock                                              */
/*                                                                           */
/* Description        : This function is used to take the ISS mutual         */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS or ISS_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
IssLock (VOID)
{
    if (OsixSemTake (gIssSemId) != OSIX_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "Failed to take ISS Sem\n");
        return ISS_FAILURE;
    }
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssUnLock                                            */
/*                                                                           */
/* Description        : This function is used to give the ISS mutual         */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS or ISS_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT4
IssUnLock (VOID)
{
    if (OsixSemGive (gIssSemId) != OSIX_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "Failed to release ISS Sem\n");
        return ISS_FAILURE;
    }
    return ISS_SUCCESS;
}

#ifdef WEBNM_WANTED
/************************************************************************/
/*  Function Name   :IssSystemInit                                            */
/*  Description     :This is the initialization function                */
/*  Input(s)        :None                                               */
/*  Output(s)       :None                                               */
/*  Returns         :ISS_SUCCESS/ISS_FAILURE                            */
/************************************************************************/

INT1
IssSystemInit (VOID)
{
    /* Initialize the ISS control socket */
    if (ENM_FAILURE == HttpIssCtrlSockInit ())
    {
        ISS_TRC (ALL_FAILURE_TRC, "Unable to create Control socket for ISS\n");
        return ISS_FAILURE;
    }

    return ISS_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IssSendCtrlMsg                                     */
/*  Description     :This function is used to send Ctrl messages        */
/*  Input(s)        :pIssCtrlMsg - ptr to Control Message               */
/*  Output(s)       :None                                               */
/*  Returns         :ISS_SUCCESS/ISS_FAILURE                            */
/************************************************************************/

INT1
IssSendCtrlMsg (tISSCtrlMsg * pIssCtrlMsg)
{
    if (ENM_FAILURE == HttpIssSendCtrlMsg (pIssCtrlMsg))
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "Unable to Send ISS Control message for CLI\n");
        return ISS_FAILURE;
    }
    return ISS_SUCCESS;
}

/************************************************************************/
/*  Function Name   :IssProcessCtrlMsgs                                 */
/*  Description     :This function is used to Process Ctrl messages     */
/*  Input(s)        :None                                               */
/*  Output(s)       :None                                               */
/*  Returns         :None                                               */
/************************************************************************/
VOID
IssProcessCtrlMsgs (VOID)
{
    HttpIssProcessCtrlMsgs ();
    return;
}
#endif
/*****************************************************************************/
/* Function Name      : IssGetPortCtrlMode                                   */
/*                                                                           */
/* Description        : Get the port control mode for the given port.        */
/*                                                                           */
/* Input(s)           : i4IfIndex - IfIndex of the port.                     */
/*                                                                           */
/* Output(s)          : pi4PortCtrlMode - Control Mode of the port           */
/*                                        (enableRx, enableTx, enableRxTx,   */
/*                                         disabled).                        */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
IssGetPortCtrlMode (INT4 i4IfIndex, INT4 *pi4PortCtrlMode)
{
    ISS_LOCK ();
    nmhGetIssPortCtrlMode (i4IfIndex, pi4PortCtrlMode);
    ISS_UNLOCK ();
}

/*****************************************************************************/
/* Function Name      : IssGetPortSpeedDuplex                                */
/*                                                                           */
/* Description        : This function returns the speed and duplexity values */
/*                      for the given port.                                  */
/*                                                                           */
/* Input(s)           : i4IssPortCtrlIndex - IfIndex of the port.            */
/*                                                                           */
/* Output(s)          : pi4RetValIssPortCtrlSpeed - Port speed               */
/*                      pi4RetValIssPortCtrlDuplex - Duplexity value of      */
/*                      the port                                             */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

UINT4
IssGetPortSpeedDuplex (INT4 i4IssPortCtrlIndex,
                       INT4 *pi4RetValIssPortCtrlSpeed,
                       INT4 *pi4RetValIssPortCtrlDuplex)
{
    tIssPortCtrlEntry  *pIssPortCtrlEntry = NULL;

    if (IssValidatePortCtrlEntry (i4IssPortCtrlIndex) != ISS_SUCCESS)
    {
        return ISS_FAILURE;
    }

    pIssPortCtrlEntry = gIssGlobalInfo.
        apIssPortCtrlEntry[i4IssPortCtrlIndex - 1];

    *pi4RetValIssPortCtrlSpeed = pIssPortCtrlEntry->IssPortCtrlSpeed;
    *pi4RetValIssPortCtrlDuplex = pIssPortCtrlEntry->IssPortCtrlDuplex;

    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssGetPortCtrlDuplex                                 */
/*                                                                           */
/* Description        : This function returns the duplexity values           */
/*                      for the given port.                                  */
/*                                                                           */
/* Input(s)           : i4IssPortCtrlIndex - IfIndex of the port.            */
/*                                                                           */
/* Output(s)          : pi4PortCtrlDuplex - Duplexity value of               */
/*                      the port                                             */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/
UINT4
IssGetPortCtrlDuplex (INT4 i4IssPortCtrlIndex, INT4 *pi4PortCtrlDuplex)
{
    UINT4               u4RetVal = ISS_SUCCESS;

    ISS_LOCK ();

    if (IssSnmpLowGetPortCtrlDuplex (i4IssPortCtrlIndex,
                                     pi4PortCtrlDuplex) == ISS_FAILURE)
    {
        u4RetVal = ISS_FAILURE;
    }

    ISS_UNLOCK ();

    return (u4RetVal);
}

/*****************************************************************************/
/* Function Name      : MgmtLock                                             */
/*                                                                           */
/* Description        : This function takes the management lock semaphore    */
/*                                                                           */
/* Input(s)           : NONE                                                 */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/
INT4
MgmtLock (VOID)
{
    if (OsixSemTake (gMgmtSemId) != OSIX_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "Failed to take Mgmt Sem\n");
        return ISS_FAILURE;
    }
    CliUtilMgmtLockStatus (OSIX_TRUE);
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MgmtUnLock                                           */
/*                                                                           */
/* Description        : This function releases the management lock semaphore */
/*                                                                           */
/* Input(s)           : NONE                                                 */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/
INT4
MgmtUnLock (VOID)
{
    if (OsixSemGive (gMgmtSemId) != OSIX_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "Failed to release Mgmt Sem\n");
        return ISS_FAILURE;
    }
    CliUtilMgmtLockStatus (OSIX_FALSE);
    return ISS_SUCCESS;
}

/************************************************************************/
/*  Function Name   : IssNotifyConfiguration                            */
/*  Description     : This function is called from the low level        */
/*                     set routines, to send configuration update event */
/*                     triggers to MSR.                                 */
/*                                                                      */
/*  Input           : SnmpNotifyInfo - Snmp Notification Strucutre      */
/*                    pu1Fmt      - The data type of indices and data.  */
/*                    variable set of inputs depending the on the       */
/*                    indices and the data.                             */
/*  Output          : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IssNotifyConfiguration (tSnmpNotifyInfo SnmpNotifyInfo, CHR1 * pu1Fmt, ...)
{
    va_list             ap;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;
    UINT1               au1Oid[SNMP_MAX_OID_LENGTH];
    UINT1              *pOid = NULL, *pu1Oid = NULL;
    INT1                i1RetStatus;
    UINT4               u4Cntr = 0;
#ifdef L2RED_WANTED
    tRmNotifConfChg     RmNotifConfChg;
    ISS_MEMSET (&RmNotifConfChg, 0, sizeof (tRmNotifConfChg));
#endif

    if (MsrGetSaveStatus () == ISS_TRUE)
    {
        /* when MSR is ongoing, no need to notify to MSR */
        return;
    }

    i1RetStatus = SnmpNotifyInfo.i1ConfStatus;
    if ((MsrGetRestorationStatus () == ISS_TRUE) ||
        (MsrGetIncrementalSaveStatus () == ISS_FALSE))
    {
        /* When Incremental save is disabled, no need to send notification 
         * to MSR. But RM has to be notified. */
        SnmpNotifyInfo.i1ConfStatus = SNMP_FAILURE;
    }

    ISS_MEMSET (au1Oid, 0, SNMP_MAX_OID_LENGTH);

    pu1Oid = au1Oid;

    for (u4Cntr = 0; u4Cntr < (SnmpNotifyInfo.u4OidLen - 1); u4Cntr++)
    {
        SPRINTF ((CHR1 *) pu1Oid, "%u.", SnmpNotifyInfo.pu4ObjectId[u4Cntr]);
        pu1Oid += STRLEN (pu1Oid);
    }
    SPRINTF ((CHR1 *) pu1Oid, "%u", SnmpNotifyInfo.pu4ObjectId[u4Cntr]);

    va_start (ap, pu1Fmt);

    /* Only if the set configuration succeeds, the trigger has to be sent to
     * MSR */
#ifdef ISS_WANTED
    if (SnmpNotifyInfo.i1ConfStatus == SNMP_SUCCESS)
    {
        if (SnmpAllocMultiIndexAndData (&pMultiIndex, SnmpNotifyInfo.u4Indices,
                                        &pMultiData,
                                        &pOid, STRLEN (au1Oid)) == FAILURE)
        {
            va_end (ap);
            ISS_TRC (ALL_FAILURE_TRC,
                     "FAILED to allocate memory for MultiIndex and MultiData\n");
            return;
        }
        STRCPY (pOid, au1Oid);
    }
#endif
#ifdef L2RED_WANTED
    if (SnmpNotifyInfo.u4SeqNum != 0)
    {
        if (SnmpAllocMultiIndexAndData
            (&(RmNotifConfChg.pMultiIndex), SnmpNotifyInfo.u4Indices,
             &(RmNotifConfChg.pMultiData), &(RmNotifConfChg.pu1ObjectId),
             STRLEN (au1Oid)) == FAILURE)
        {
#ifdef ISS_WANTED
            if (SnmpNotifyInfo.i1ConfStatus == SNMP_SUCCESS)
            {
                /* Freeing MultiIndex, MultiData, pOid */
                free_MultiData (pMultiData);
                free_MultiIndex (pMultiIndex, pMultiIndex->u4No);

                if (pOid != NULL)
                {
                    free_MultiOid (pOid);
                }
            }
#endif
            va_end (ap);
            ISS_TRC (ALL_FAILURE_TRC,
                     "FAILED to allocate memory for RM Config Info\n");
            return;
        }

#ifdef ISS_WANTED
        if (SnmpNotifyInfo.i1ConfStatus == SNMP_SUCCESS)
        {
            SNMPFillTwoInstOfMultiIndexandData (SnmpNotifyInfo.u4Indices,
                                                pu1Fmt, pMultiIndex, pMultiData,
                                                RmNotifConfChg.pMultiIndex,
                                                RmNotifConfChg.pMultiData, ap);
        }
        else
#endif
        {
            SNMPFillMultiIndexandData (SnmpNotifyInfo.u4Indices, pu1Fmt,
                                       RmNotifConfChg.pMultiIndex,
                                       RmNotifConfChg.pMultiData, ap);
        }

        STRCPY (RmNotifConfChg.pu1ObjectId, au1Oid);
    }
    else
#endif
    {
        /* Only if the set configuration succeeds, the trigger has to be sent to
         * MSR */
#ifdef ISS_WANTED
        if (SnmpNotifyInfo.i1ConfStatus == SNMP_SUCCESS)
        {
            SNMPFillMultiIndexandData (SnmpNotifyInfo.u4Indices, pu1Fmt,
                                       pMultiIndex, pMultiData, ap);
        }
#endif
    }
    va_end (ap);

    if (SnmpNotifyInfo.pUnLockPointer != NULL)
    {

        /* Release the protocol lock to avoid deadlock becasue
         * the MSR can be waiting for the lock in the validating routines
         * and the protocol waits for the MSR lock to notify MSR */
        SnmpNotifyInfo.pUnLockPointer ();
    }

#ifdef L2RED_WANTED
    if (SnmpNotifyInfo.u4SeqNum != 0)
    {
        RmNotifConfChg.u4SeqNo = SnmpNotifyInfo.u4SeqNum;
        RmNotifConfChg.i4OidLen = (INT4) STRLEN (au1Oid);
        RmNotifConfChg.i1ConfigSetStatus = i1RetStatus;
        RmApiSyncStaticConfig (&RmNotifConfChg);
    }
#else
    UNUSED_PARAM (i1RetStatus);
#endif

#ifdef ISS_WANTED
    if (SnmpNotifyInfo.i1ConfStatus == SNMP_SUCCESS)
    {
        MSRNotifyConfChg (pMultiIndex, (BOOL1) SnmpNotifyInfo.u1RowStatus,
                          pMultiData, pOid);
    }
#endif
    if (SnmpNotifyInfo.pLockPointer != NULL)
    {
        SnmpNotifyInfo.pLockPointer ();
    }

    /* The pMultiData, pMultiIndex and pOid are released
     * by the MSR and RM after notification */

    return;
}

/************************************************************************/
/*  Function Name   : IssMsrNotifyConfiguration                         */
/*  Description     : This function is called from the low level        */
/*                     set routines, to send configuration update event */
/*                     triggers to MSR.                                 */
/*                                                                      */
/*  Input           : SnmpNotifyInfo - Snmp Notification Strucutre      */
/*                    pu1Fmt      - The data type of indices and data.  */
/*                    variable set of inputs depending the on the       */
/*                    indices and the data.                             */
/*  Output          : None                                              */
/*  Returns         : None                                              */
/************************************************************************/

VOID
IssMsrNotifyConfiguration (tSnmpNotifyInfo SnmpNotifyInfo, CHR1 * pu1Fmt, ...)
{
    va_list             ap;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;
    tIssBool            MsrFlag;
    UINT1               au1Oid[SNMP_MAX_OID_LENGTH];
    UINT1              *pOid = NULL, *pu1Oid = NULL;
    UINT4               u4Cntr = 0;

    MsrFlag = MsrGetIncrementalSaveStatus ();
    if (MsrFlag == ISS_FALSE)
    {
        /* When Incremental save is disabled, no need to send notification 
         * to MSR.*/
        return;
    }

    MsrFlag = MsrGetRestorationStatus ();
    if (MsrFlag == ISS_TRUE)
    {
        /* When restoration is in progress no need to send notification 
         * to MSR */
        return;
    }

    if (MsrGetSaveStatus () == ISS_TRUE)
    {
        /* when MSR is ongoing, no need to notify to MSR */
        return;
    }

    ISS_MEMSET (au1Oid, 0, SNMP_MAX_OID_LENGTH);

    pu1Oid = au1Oid;

    for (u4Cntr = 0; u4Cntr < (SnmpNotifyInfo.u4OidLen - 1); u4Cntr++)
    {
        SPRINTF ((CHR1 *) pu1Oid, "%u.", SnmpNotifyInfo.pu4ObjectId[u4Cntr]);
        pu1Oid += STRLEN (pu1Oid);
    }
    SPRINTF ((CHR1 *) pu1Oid, "%u", SnmpNotifyInfo.pu4ObjectId[u4Cntr]);

    va_start (ap, pu1Fmt);

    /* Only if the set configuration succeeds, the trigger has to be sent to
     * MSR */
    if (SnmpNotifyInfo.i1ConfStatus == SNMP_SUCCESS)
    {
        if (SnmpAllocMultiIndexAndData (&pMultiIndex, SnmpNotifyInfo.u4Indices,
                                        &pMultiData,
                                        &pOid, STRLEN (au1Oid)) == FAILURE)
        {
            va_end (ap);
            return;
        }

        STRCPY (pOid, au1Oid);

        SNMPFillMultiIndexandData (SnmpNotifyInfo.u4Indices, pu1Fmt,
                                   pMultiIndex, pMultiData, ap);
    }
    va_end (ap);

    if (SnmpNotifyInfo.pUnLockPointer != NULL)
    {

        /* Release the protocol lock to avoid deadlock becasue
         * the MSR can be waiting for the lock in the validating routines
         * and the protocol waits for the MSR lock to notify MSR */
        SnmpNotifyInfo.pUnLockPointer ();
    }

    if (SnmpNotifyInfo.i1ConfStatus == SNMP_SUCCESS)
    {
        MSRNotifyConfChg (pMultiIndex, (BOOL1) SnmpNotifyInfo.u1RowStatus,
                          pMultiData, pOid);
    }

    if (SnmpNotifyInfo.pLockPointer != NULL)
    {
        SnmpNotifyInfo.pLockPointer ();
    }

    /* The pMultiData, pMultiIndex and pOid are released
     * by the MSR and RM after notification */

    return;
}

/*****************************************************************************/
 /* Function Name      : IssUpdateDownLoadStatus                              */
 /*                                                                           */
 /* Description        : This function updates download status                */
 /*                                                                           */
 /* Input(s)           : u4Downloadstatus                                     */
 /*                                                                           */
 /* Output(s)          : None                                                 */
 /*                                                                           */
 /* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
 /*****************************************************************************/
INT4
IssUpdateDownLoadStatus (UINT4 u4DownLoadStatus)
{
    gIssDlImageInfo.u4DownloadStatus = u4DownLoadStatus;
    return ISS_SUCCESS;
}

/*****************************************************************************/
 /* Function Name      : IssGetDownLoadStatus                                 */
 /*                                                                           */
 /* Description        : This function Gets download status                   */
 /*                                                                           */
 /* Input(s)           : None                                                 */
 /*                                                                           */
 /* Output(s)          : None                                                 */
 /*                                                                           */
 /* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
 /*****************************************************************************/
INT4
IssGetDownLoadStatus (VOID)
{
    return (INT4) gIssDlImageInfo.u4DownloadStatus;
}

/*****************************************************************************/
 /* Function Name      : IssGetTransfermode                                   */
 /*                                                                           */
 /* Description        : This function Gets Transfermode of Image download    */
 /*                                                                           */
 /* Input(s)           : None                                                 */
 /*                                                                           */
 /* Output(s)          : None                                                 */
 /*                                                                           */
 /* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
 /*****************************************************************************/
INT4
IssGetTransferMode (VOID)
{
    return (INT4) gIssDlImageInfo.u4IssDlTransferMode;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetIncrementalSaveStatus                      */
/*                                                                          */
/*    Description        : This function is invoked to get the incremental  */
/*                         save status used for the current session         */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Incremental save status.                         */
/****************************************************************************/

INT4
IssGetIncrementalSaveStatus (VOID)
{
    return gIssSysGroupInfo.IssIncrSaveFlag;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetAutoSaveStatus                             */
/*                                                                          */
/*    Description        : This function is invoked to get the auto save    */
/*                         flag status configured for the switch            */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Auto save status.                                */
/****************************************************************************/

INT4
IssGetAutoSaveStatus (VOID)
{
    return gIssConfigSaveInfo.IssAutoConfigSave;
}

/*****************************************************************************/
/* Function Name      : IssGetMemInfo                                        */
/*                                                                           */
/* Description        : This function is used to get the percentage of       */
/*                      memory used.                                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/
UINT4
IssGetMemInfo (VOID)
{
    UINT4               u4MemFree = 0;
    UINT4               u4MemTot = 0;
    UINT4               u4MemUsed = 0;

    if (OsixGetMemInfo (&u4MemTot, &u4MemFree) != OSIX_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\rFailed to get MemTot and"
                 " MemFree in OsixGetMemInfo\n");
        return ISS_FAILURE;
    }

    if (u4MemTot > u4MemFree)
    {
        u4MemUsed = (((u4MemTot - u4MemFree) * 100) / u4MemTot);
    }

    gIssSysGroupInfo.IssSwitchInfo.u4CurrRAMUsage = u4MemUsed;

    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssGetCPUInfo                                        */
/*                                                                           */
/* Description        : This function is used to get the percentage of       */
/*                      CPU utilized.                                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/
UINT4
IssGetCPUInfo (VOID)
{
    UINT4               u4TotalUsage = 0;
    UINT4               u4CPUUsage = 0;
    UINT4               u4CPUUsed = 0;

    ISS_UNLOCK ();
    if (OsixGetCPUInfo (&u4TotalUsage, &u4CPUUsage) != OSIX_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\rFailed to get CPU Idle time"
                 "in IssGetCPUInfo\n");

        ISS_LOCK ();
        return ISS_FAILURE;
    }
    ISS_LOCK ();

    if (u4TotalUsage > u4CPUUsage)
    {
        u4CPUUsed = ((u4CPUUsage * 100) / u4TotalUsage);
    }

    gIssSysGroupInfo.IssSwitchInfo.u4CurrCPUUsage = u4CPUUsed;

    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssGetFlashInfo                                      */
/*                                                                           */
/* Description        : This function is used to get the percentage of       */
/*                      flash utilized.                                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/
UINT4
IssGetFlashInfo (VOID)
{
    UINT4               u4FlashUsage = 0;
    UINT4               u4FlashFree = 0;
    UINT4               u4FlashTot = 0;
    UINT4               u4FreePer = 0;

    if (OsixGetFlashInfo (&u4FlashTot, &u4FlashFree) != OSIX_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\rFailed to get Flash usage"
                 " details in IssGetFlashInfo\n");
        return ISS_FAILURE;
    }

    if (u4FlashTot > u4FlashFree)
    {
        u4FreePer = ((u4FlashFree * 100) / u4FlashTot);
    }

    u4FlashUsage = 100 - u4FreePer;
    gIssSysGroupInfo.IssSwitchInfo.u4CurrFlashUsage = u4FlashUsage;

    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssReadNodeid                                         */
/*                                                                           */
/* Description        : This function will read the nodeid file and          */
/*                      it will update RM global Info                        */
/*                                                                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
IssReadNodeid (VOID)
{
    UINT1               au1Buffer[MAX_COLUMN_LENGTH];
    UINT1               u1ErrorCount = 0;
    UINT1               u1Index = 0;

    if (IssReadNodeidFile ((UINT1 *) ISS_NODEID_FILE, (UINT1 *) "SWITCHID",
                           au1Buffer) == 0)
    {
        ISS_TRC (ALL_FAILURE_TRC, "!!!failed to read switchid!!!\n");
        return ISS_FAILURE;
    }
    else
    {
        gIssGlobalInfo.u4IssSwitchId = (UINT4) atoi ((CONST CHR1 *) au1Buffer);
    }
    if (IssReadNodeidFile ((UINT1 *) ISS_NODEID_FILE,
                           (UINT1 *) "USER_PREFERENCE", au1Buffer) == 0)
    {
        ISS_TRC (ALL_FAILURE_TRC, "!!!failed to read Usr Preference!!!\n");
        return ISS_FAILURE;
    }
    else
    {
        if (STRCMP (au1Buffer, "PM") == 0)
        {
            gIssGlobalInfo.u4IssConfigState = ISS_PREFERRED_MASTER;
        }
        else if (STRCMP (au1Buffer, "BM") == 0)
        {
            gIssGlobalInfo.u4IssConfigState = ISS_BACKUP_MASTER;
        }
        else
        {
            gIssGlobalInfo.u4IssConfigState = ISS_PREFERRED_SLAVE;
        }

    }
    if (IssGetRMTypeFromNvRam () == ISS_RM_STACK_INTERFACE_TYPE_INBAND)
    {
        if (IssReadNodeidFile
            ((UINT1 *) ISS_NODEID_FILE, (UINT1 *) "RM_IP_ADDRESS",
             au1Buffer) == 0)
        {
            ISS_TRC (ALL_FAILURE_TRC, "!!!failed to read RM IP Address!!!\n");
            gIssGlobalInfo.u4IssRmIpAddress = ISS_DEFAULT_RM_IP_ADDRESS +
                (UINT4) IssGetSwitchid () + 1;
        }
        else
        {
            gIssGlobalInfo.u4IssRmIpAddress =
                OSIX_NTOHL (INET_ADDR (au1Buffer));
        }

        if (IssReadNodeidFile
            ((UINT1 *) ISS_NODEID_FILE, (UINT1 *) "RM_SUBNET_MASK",
             au1Buffer) == 0)
        {
            ISS_TRC (ALL_FAILURE_TRC, "!!!failed to read RM IP Address!!!\n");
            gIssGlobalInfo.u4IssRmSubnetMask = ISS_DEFAULT_RM_SUBNET_MASK;
        }
        else
        {
            gIssGlobalInfo.u4IssRmSubnetMask =
                OSIX_NTOHL (INET_ADDR (au1Buffer));
        }
        if (IssReadNodeidFile
            ((UINT1 *) ISS_NODEID_FILE, (UINT1 *) "RM_STK_INTERFACE",
             au1Buffer) == 0)
        {
            ISS_TRC (ALL_FAILURE_TRC,
                     "!!!failed to read RM Stack Interface!!!\n");
            MEMCPY (gIssGlobalInfo.au1IssRmStackInterface,
                    ISS_DEFAULT_RM_STK_INTERFACE,
                    STRLEN (ISS_DEFAULT_RM_STK_INTERFACE));
            gIssGlobalInfo.
                au1IssRmStackInterface[STRLEN (ISS_DEFAULT_RM_STK_INTERFACE)] =
                '\0';
        }
        else
        {
            /* e.g RM_STK_INTERFACE = stk0 */
            /* au1Buffer[3] = 0 -> First stacking port will be used */
            if (ATOI (&au1Buffer[3]) > SYS_DEF_MAX_INFRA_SYS_PORT_COUNT)
            {
                MEMCPY (gIssGlobalInfo.au1IssRmStackInterface,
                        ISS_DEFAULT_RM_STK_INTERFACE,
                        STRLEN (ISS_DEFAULT_RM_STK_INTERFACE));
                gIssGlobalInfo.
                    au1IssRmStackInterface[STRLEN
                                           (ISS_DEFAULT_RM_STK_INTERFACE)] =
                    '\0';
            }
            else
            {
                if (STRLEN (au1Buffer) <= ISS_RM_INTERFACE_LEN)
                {
                    MEMCPY (gIssGlobalInfo.au1IssRmStackInterface, au1Buffer,
                            (sizeof (gIssGlobalInfo.au1IssRmStackInterface) -
                             1));
                    gIssGlobalInfo.
                        au1IssRmStackInterface[(sizeof
                                                (gIssGlobalInfo.
                                                 au1IssRmStackInterface) - 1)] =
                        '\0';
                }
            }
        }
    }

    /* Read the ISSU STARTUP_MODE */
    if (IssReadNodeidFile ((UINT1 *) ISS_NODEID_FILE,
                           (UINT1 *) "STARTUP_MODE", au1Buffer) == 0)
    {
        ISS_TRC (ALL_FAILURE_TRC, "!!!failed to read ISSU startup mode!!!\n");
        u1ErrorCount++;
        gIssuStartupInfo.u1StartUpMode = ISSU_DEFAULT_STARTUP_MODE;
    }
    else
    {
        if (STRCMP (au1Buffer, "NON-ISSU") == 0)
        {
            gIssuStartupInfo.u1StartUpMode = ISSU_MAINTENANCE_MODE_DISABLE;
        }
        else if (STRCMP (au1Buffer, "ISSU") == 0)
        {
            gIssuStartupInfo.u1StartUpMode = ISSU_MAINTENANCE_MODE_ENABLE;
        }
        else
        {
            ISS_TRC (ALL_FAILURE_TRC, "[ERROR]:Failed to read Startup mode\n");
            u1ErrorCount++;
            gIssuStartupInfo.u1StartUpMode = ISSU_DEFAULT_STARTUP_MODE;
        }
    }

    /* Read the ISSU_MODE */
    if (IssReadNodeidFile ((UINT1 *) ISS_NODEID_FILE,
                           (UINT1 *) "ISSU_MODE", au1Buffer) == 0)
    {
        ISS_TRC (ALL_FAILURE_TRC, "!!!failed to read ISSU mode!!!\n");
        u1ErrorCount++;
        gIssuStartupInfo.u4IssuMode = ISSU_DEFAULT_MODE;
    }
    else
    {
        if (STRCMP (au1Buffer, "Auto") == 0)
        {
            gIssuStartupInfo.u4IssuMode = ISSU_AUTO_MODE;
        }
        else if (STRCMP (au1Buffer, "Full-Compatible") == 0)
        {
            gIssuStartupInfo.u4IssuMode = ISSU_FULL_COMPATIBLE_MODE;
        }
        else if (STRCMP (au1Buffer, "Base-Compatible") == 0)
        {
            gIssuStartupInfo.u4IssuMode = ISSU_BASE_COMPATIBLE_MODE;
        }
        else if (STRCMP (au1Buffer, "In-Compatible") == 0)
        {
            gIssuStartupInfo.u4IssuMode = ISSU_INCOMPATIBLE_MODE;
        }
        else
        {
            ISS_TRC (ALL_FAILURE_TRC, "[ERROR]:ISSU mode is incorrect\n");
            u1ErrorCount++;
            gIssuStartupInfo.u4IssuMode = ISSU_DEFAULT_MODE;
        }

    }

    /* Read the ISSU NODE_ROLE */
    if (IssReadNodeidFile ((UINT1 *) ISS_NODEID_FILE,
                           (UINT1 *) "NODE_ROLE", au1Buffer) == 0)
    {
        ISS_TRC (ALL_FAILURE_TRC, "!!!failed to read ISSU startup mode!!!\n");
        u1ErrorCount++;
        gIssuStartupInfo.u1NodeRole = ISSU_DEFAULT_NODE_ROLE;
    }
    else
    {
        /* If ISSU maintenance mode is in disabled state,
         * keep the node role as default */
        if ((STRCMP (au1Buffer, "Auto") == 0) ||
            (gIssuStartupInfo.u1StartUpMode == ISSU_MAINTENANCE_MODE_DISABLE))
        {
            gIssuStartupInfo.u1NodeRole = ISSU_NODE_ROLE_AUTO;
        }
        else if (STRCMP (au1Buffer, "Active") == 0)
        {
            gIssuStartupInfo.u1NodeRole = ISSU_NODE_ROLE_ACTIVE;
        }
        else if (STRCMP (au1Buffer, "Standby") == 0)
        {
            gIssuStartupInfo.u1NodeRole = ISSU_NODE_ROLE_STANDBY;
        }
        else
        {
            ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: ISSU Node Role is incorrect\n");
            u1ErrorCount++;
            gIssuStartupInfo.u1NodeRole = ISSU_DEFAULT_NODE_ROLE;
        }
    }

    /* Read the ROLL_BACK_ISS_VERSION */
    if (IssReadNodeidFile ((UINT1 *) ISS_NODEID_FILE,
                           (UINT1 *) "ROLL_BACK_ISS_VERSION", au1Buffer) == 0)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "!!!failed to read Rollback ISS version!!!\n");
        u1ErrorCount++;
        MEMSET (gIssuStartupInfo.au1RollBackIssVersion, 0,
                sizeof (gIssuStartupInfo.au1RollBackIssVersion));
    }
    else
    {
        STRNCPY (gIssuStartupInfo.au1RollBackIssVersion, au1Buffer,
                 MEM_MAX_BYTES ((sizeof (gIssuStartupInfo.au1RollBackIssVersion)
                                 - 1), STRLEN (au1Buffer)));
    }

    /*Read the Last Upgraded Time */
    if (IssReadNodeidFile ((UINT1 *) ISS_NODEID_FILE,
                           (UINT1 *) "ISSU_LAST_UPGRADE_TIME", au1Buffer) == 0)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "!!!failed to read ISSU last upgraded time!!!\n");
        u1ErrorCount++;
        gIssuStartupInfo.u4LastUpgradedTime = 0;
    }
    else
    {
        gIssuStartupInfo.u4LastUpgradedTime =
            (UINT4) atoi ((CONST CHR1 *) au1Buffer);
    }

    /* Read the RollBack Software Path */
    if (IssReadNodeidFile ((UINT1 *) ISS_NODEID_FILE,
                           (UINT1 *) "ROLL_BACK_SW_PATH", au1Buffer) == 0)
    {
        ISS_TRC (ALL_FAILURE_TRC, "!!!failed to read Rollback Software Path\n");
        u1ErrorCount++;
        MEMSET (gIssuStartupInfo.au1RollBackSwPath, 0,
                sizeof (gIssuStartupInfo.au1RollBackSwPath));
    }
    else
    {
        STRNCPY (gIssuStartupInfo.au1RollBackSwPath, au1Buffer,
                 MEM_MAX_BYTES ((sizeof (gIssuStartupInfo.au1RollBackSwPath) -
                                 1), STRLEN (au1Buffer)));
    }

    /* Read the Current Software Path */
    if (IssReadNodeidFile ((UINT1 *) ISS_NODEID_FILE,
                           (UINT1 *) "CURRENT_SW_PATH", au1Buffer) == 0)
    {
        ISS_TRC (ALL_FAILURE_TRC, "!!!failed to read Current Software Path\n");
        u1ErrorCount++;
        MEMSET (gIssuStartupInfo.au1CurrentSwPath, 0,
                sizeof (gIssuStartupInfo.au1CurrentSwPath));
    }
    else
    {
        STRNCPY (gIssuStartupInfo.au1CurrentSwPath, au1Buffer,
                 MEM_MAX_BYTES ((sizeof (gIssuStartupInfo.au1CurrentSwPath) -
                                 1), STRLEN (au1Buffer)));
    }

    /* Read default Secondary IP Address */
    if (IssReadNodeidFile
        ((UINT1 *) ISS_NODEID_FILE, (UINT1 *) "SEC_IP_ADDRESS", au1Buffer) == 0)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Failed to read ip address\n");
        u1ErrorCount++;
        gIssGlobalInfo.u4LocalSecIpAddr = ISS_DEFAULT_SEC_IP_ADDRESS;
        gIssGlobalInfo.u4LocalSecSubnetMask = ISS_DEFAULT_SEC_IP_MASK;
    }
    else
    {
        gIssGlobalInfo.u4LocalSecIpAddr = OSIX_NTOHL (INET_ADDR (au1Buffer));

        /* Allow IP address which are Class A, B or C */
        if (!((ISS_IS_ADDR_CLASS_A (gIssGlobalInfo.u4LocalSecIpAddr)) ||
              (ISS_IS_ADDR_CLASS_B (gIssGlobalInfo.u4LocalSecIpAddr)) ||
              (ISS_IS_ADDR_CLASS_C (gIssGlobalInfo.u4LocalSecIpAddr))))
        {
            ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Invalid ip address\n");
            u1ErrorCount++;
            gIssGlobalInfo.u4LocalSecIpAddr = ISS_DEFAULT_SEC_IP_ADDRESS;
            gIssGlobalInfo.u4LocalSecSubnetMask = ISS_DEFAULT_SEC_IP_MASK;
        }
        else
        {
            /* Read the Secondary IP Mask */
            if (IssReadNodeidFile ((UINT1 *) ISS_NODEID_FILE,
                                   (UINT1 *) "SEC_IP_MASK", au1Buffer) == 0)
            {
                ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Failed to read Ip mask\n");
                u1ErrorCount++;
                gIssGlobalInfo.u4LocalSecSubnetMask = ISS_DEFAULT_SEC_IP_MASK;
            }
            else
            {
                gIssGlobalInfo.u4LocalSecSubnetMask =
                    OSIX_NTOHL (INET_ADDR (au1Buffer));

                /* inet_addr routine returns 0xffffffff for all invalid values of
                 * au1Buffer. However if au1Buffer = "255.255.255.255" , which is a
                 * valid, value it again returns 0xffffffff. This is taken care of by
                 * the following check */
                if (gIssGlobalInfo.u4LocalSecSubnetMask == 0xffffffff)
                {
                    if (STRNCMP ("255.255.255.255", au1Buffer,
                                 STRLEN ("255.255.255.255")))
                    {
                        ISS_TRC (ALL_FAILURE_TRC,
                                 "[ERROR]: Incorrect Ip mask\n");
                        u1ErrorCount++;
                        gIssGlobalInfo.u4LocalSecSubnetMask =
                            ISS_DEFAULT_SEC_IP_MASK;
                    }
                }
                /* The valid subnet address is from 0 to 32 */
                for (u1Index = 0; u1Index <= ISS_MAX_CIDR; ++u1Index)
                {
                    if (gu4IssCidrSubnetMask[u1Index] ==
                        gIssGlobalInfo.u4LocalSecSubnetMask)
                    {
                        break;
                    }
                }
                if (u1Index > ISS_MAX_CIDR)
                {
                    ISS_TRC (ALL_FAILURE_TRC, "[ERROR]:Invalid Subnet Mask\n");
                    u1ErrorCount++;
                    gIssGlobalInfo.u4LocalSecSubnetMask =
                        ISS_DEFAULT_SEC_IP_MASK;
                }
            }
        }
    }

    /* Read default Secondary IPv6 Address */
    if (IssReadNodeidFile
        ((UINT1 *) ISS_NODEID_FILE, (UINT1 *) "SEC_IPV6_ADDRESS",
         au1Buffer) == 0)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Failed to read ipv6 address\n");
        u1ErrorCount++;
        INET_ATON6 (ISS_DEFAULT_SEC_IPV6_ADDRESS,
                    &gIssGlobalInfo.localSecIp6Addr);
        gIssGlobalInfo.u1SecPrefixLen = ISS_DEFAULT_SEC_IPV6_PREFIX_LEN;
    }
    else
    {
        if (INET_ATON6 (au1Buffer, &gIssGlobalInfo.localSecIp6Addr) == 0)
        {
            ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Invalid ipv6 address\n");
            u1ErrorCount++;
            INET_ATON6 (ISS_DEFAULT_SEC_IPV6_ADDRESS,
                        &gIssGlobalInfo.localSecIp6Addr);
        }
        if (IssReadNodeidFile ((UINT1 *) ISS_NODEID_FILE,
                               (UINT1 *) "SEC_IPV6_PREFIX_LEN", au1Buffer) == 0)
        {
            ISS_TRC (ALL_FAILURE_TRC,
                     "[ERROR]: Failed to read ipv6 prefix length\n");

            ISS_TRC (ALL_FAILURE_TRC,
                     "[ERROR]: Failed to read ipv6 prefix length\n");
            u1ErrorCount++;
            gIssGlobalInfo.u1SecPrefixLen = ISS_DEFAULT_SEC_IPV6_PREFIX_LEN;
        }
        else
        {
            gIssGlobalInfo.u1SecPrefixLen = (UINT1) ISS_ATOI (au1Buffer);
            if (gIssGlobalInfo.u1SecPrefixLen > 128)
            {
                ISS_TRC (ALL_FAILURE_TRC,
                         "[ERROR]: Invalid ipv6 prefix length\n");
                u1ErrorCount++;
                gIssGlobalInfo.u1SecPrefixLen = ISS_DEFAULT_SEC_IPV6_PREFIX_LEN;
            }
        }
    }

    if (u1ErrorCount > 0)
    {
        return ISS_FAILURE;
    }
    else
    {
        return ISS_SUCCESS;
    }
}

/*****************************************************************************/
/* Function Name      : IssReadNodeidFile                                     */
/*                                                                           */
/* Description        : This function will read the nodeid file              */
/*                      parameters                                           */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pu1FileName  - FileName                              */
/*                      pu1SearchStr - Striing to be searched                */
/*                                                                           */
/* Output(s)          : pu1StrValue  - Value of the input string             */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
IssReadNodeidFile (UINT1 *pu1FileName, UINT1 *pu1SearchStr, UINT1 *pu1StrValue)
{
    FILE               *pFile;
    UINT1               au1Buf[MAX_COLUMN_LENGTH];
    UINT1               au1ReadStr[MAX_COLUMN_LENGTH];
    UINT1               au1StrValue[MAX_COLUMN_LENGTH];
    UINT1               au1TmpValue[MAX_COLUMN_LENGTH];
    UINT4               u4Retval = ISS_FAILURE;
    UINT1              *pu1Temp;

    pFile = FOPEN ((CONST CHR1 *) pu1FileName, "r");
    if (pFile == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC, "!!!failed to open nodeid file!!!\n");
        return ISS_FAILURE;
    }

    MEMSET (pu1StrValue, 0, MAX_COLUMN_LENGTH);

    while (!feof (pFile))
    {

        fgets ((CHR1 *) au1Buf, MAX_COLUMN_LENGTH, pFile);
        if ((STRSTR (au1Buf, pu1SearchStr)) != NULL)
        {
            MEMSET (au1ReadStr, 0, MAX_COLUMN_LENGTH);
            MEMSET (au1StrValue, 0, MAX_COLUMN_LENGTH);
            MEMSET (au1TmpValue, 0, MAX_COLUMN_LENGTH);

            SSCANF ((CONST CHR1 *) au1Buf, "%119s%119s%119s", au1ReadStr,
                    au1StrValue, au1TmpValue);
            if (STRCMP (au1ReadStr, pu1SearchStr) != 0)
            {
                continue;
            }
            if (STRLEN (au1TmpValue) != 0)
            {
                STRCPY (pu1StrValue, au1TmpValue);
            }
            else
            {
                pu1Temp = au1StrValue;
                pu1Temp++;
                STRCPY (pu1StrValue, pu1Temp);
            }

            u4Retval = ISS_SUCCESS;
            break;
        }
    }

    fclose (pFile);
    return ((INT4) u4Retval);
}

/******************************************************************************
 * Function           : IssWriteNodeidFile
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None
 * Action             : Routine to write node id file
******************************************************************************/

VOID
IssWriteNodeidFile (VOID)
{
    FILE               *fp = NULL;
    tUtlInAddr          IpAddr;
    UINT1               au1NullIssVersion[ISS_ISSU_IMAGE_NAME_MAX_SIZE + 1];
    UINT1               au1NullIssPath[ISS_ISSU_MAX_PATH_LEN + 1];

    MEMSET (&IpAddr, 0, sizeof (tUtlInAddr));
    MEMSET (&au1NullIssVersion, 0, sizeof (au1NullIssVersion));
    MEMSET (&au1NullIssPath, 0, sizeof (au1NullIssPath));

    fp = FOPEN (ISS_NODEID_FILE, "w");
    if (fp == NULL)
    {
        printf ("\r\nError in writing to the nodeid file");
        return;
    }
    fprintf (fp, "SWITCHID = %u\n", gIssGlobalInfo.u4IssSwitchId);
    if (gIssGlobalInfo.u4IssConfigState == ISS_PREFERRED_SLAVE)
    {
        fprintf (fp, "USER_PREFERENCE = PS\n");
    }
    else if (gIssGlobalInfo.u4IssConfigState == ISS_BACKUP_MASTER)
    {
        fprintf (fp, "USER_PREFERENCE = BM\n");
    }
    else
    {
        fprintf (fp, "USER_PREFERENCE = PM\n");
    }
    if (IssGetRMTypeFromNvRam () == ISS_RM_STACK_INTERFACE_TYPE_INBAND)
    {
        IpAddr.u4Addr = OSIX_NTOHL (gIssGlobalInfo.u4IssRmIpAddress);
        fprintf (fp, "RM_IP_ADDRESS   = %s\n", UtlInetNtoa (IpAddr));

        MEMSET (&IpAddr, 0, sizeof (tUtlInAddr));

        IpAddr.u4Addr = OSIX_NTOHL (gIssGlobalInfo.u4IssRmSubnetMask);
        fprintf (fp, "RM_SUBNET_MASK   = %s\n", UtlInetNtoa (IpAddr));

        fprintf (fp, "RM_STK_INTERFACE = %s\n",
                 gIssGlobalInfo.au1IssRmStackInterface);
    }

    /* Mode in which ISSU starts up */
    if (gIssuStartupInfo.u1StartUpMode == ISSU_MAINTENANCE_MODE_ENABLE)
    {
        fprintf (fp, "STARTUP_MODE = ISSU\n");
    }
    else
    {
        fprintf (fp, "STARTUP_MODE = NON-ISSU\n");
    }

    /* ISSU mode */
    if (gIssuStartupInfo.u4IssuMode == ISSU_FULL_COMPATIBLE_MODE)
    {
        fprintf (fp, "ISSU_MODE = Full-Compatible\n");
    }
    else if (gIssuStartupInfo.u4IssuMode == ISSU_BASE_COMPATIBLE_MODE)
    {
        fprintf (fp, "ISSU_MODE = Base-Compatible\n");
    }
    else if (gIssuStartupInfo.u4IssuMode == ISSU_INCOMPATIBLE_MODE)
    {
        fprintf (fp, "ISSU_MODE = In-Compatible\n");
    }
    else
    {
        fprintf (fp, "ISSU_MODE = Auto\n");
    }

    /* Node Role while booting up */
    if (gIssuStartupInfo.u1NodeRole == ISSU_NODE_ROLE_ACTIVE)
    {
        fprintf (fp, "NODE_ROLE = Active\n");
    }
    else if (gIssuStartupInfo.u1NodeRole == ISSU_NODE_ROLE_STANDBY)
    {
        fprintf (fp, "NODE_ROLE = Standby\n");
    }
    else
    {
        fprintf (fp, "NODE_ROLE = Auto\n");
    }

    /* Set the ISS Roll back version */
    if (MEMCMP (gIssuStartupInfo.au1RollBackIssVersion, au1NullIssVersion,
                sizeof (gIssuStartupInfo.au1RollBackIssVersion)) != 0)
    {
        fprintf (fp, "ROLL_BACK_ISS_VERSION = %s\n",
                 gIssuStartupInfo.au1RollBackIssVersion);
    }

    /* Set the ISSU last upgraded time */
    fprintf (fp, "ISSU_LAST_UPGRADE_TIME = %u\n",
             gIssuStartupInfo.u4LastUpgradedTime);

    /* Set the ISSU Roll back Software Path */
    if (MEMCMP (gIssuStartupInfo.au1RollBackSwPath, au1NullIssPath,
                sizeof (gIssuStartupInfo.au1RollBackSwPath)) != 0)
    {
        fprintf (fp, "ROLL_BACK_SW_PATH = %s\n",
                 gIssuStartupInfo.au1RollBackSwPath);
    }

    /* Set the ISSU Current Software Path */
    if (MEMCMP (gIssuStartupInfo.au1CurrentSwPath, au1NullIssPath,
                sizeof (gIssuStartupInfo.au1CurrentSwPath)) != 0)
    {
        fprintf (fp, "CURRENT_SW_PATH = %s\n",
                 gIssuStartupInfo.au1CurrentSwPath);
    }

    IpAddr.u4Addr = OSIX_NTOHL (gIssGlobalInfo.u4LocalSecIpAddr);
    fprintf (fp, "SEC_IP_ADDRESS =%s\n", UtlInetNtoa (IpAddr));

    IpAddr.u4Addr = OSIX_NTOHL (gIssGlobalInfo.u4LocalSecSubnetMask);
    fprintf (fp, "SEC_IP_MASK =%s\n", UtlInetNtoa (IpAddr));

    fprintf (fp, "SEC_IPV6_ADDRESS =%s\n",
             Ip6PrintNtop (&gIssGlobalInfo.localSecIp6Addr));

    fprintf (fp, "SEC_IPV6_PREFIX_LEN =%d\n", gIssGlobalInfo.u1SecPrefixLen);

    fflush (fp);
    fclose (fp);

    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetRMIpAddress                                */
/*                                                                          */
/*    Description        : This function is invoked to get the IP-Address   */
/*                         used for RM TCP/IP communication which is        */
/*                         configured in nodeid file                        */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Switchid                                         */
/****************************************************************************/

INT4
IssGetRMIpAddress (VOID)
{
    return (INT4) gIssGlobalInfo.u4IssRmIpAddress;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetRMIpAddress                                */
/*                                                                          */
/*    Description        : This function is invoked to set the IP-Address   */
/*                         to be used for RM TCP/IP communication           */
/*                         which is configured in nodeid file               */
/*                                                                          */
/*    Input(s)           : u4RmIpAddress -  IpAddress to be used for RM     */
/*                                          communication                   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
IssSetRMIpAddress (UINT4 u4RmIpAddress)
{
    gIssGlobalInfo.u4IssRmIpAddress = u4RmIpAddress;
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetRMSubnetMask                               */
/*                                                                          */
/*    Description        : This function is invoked to get the Subnet Mask  */
/*                         associated with IP Address used for RM TCP/IP    */
/*                         communication which is configured in nodeid file */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Switchid                                         */
/****************************************************************************/

INT4
IssGetRMSubnetMask (VOID)
{
    return (INT4) gIssGlobalInfo.u4IssRmSubnetMask;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetRMSubnetMask                               */
/*                                                                          */
/*    Description        : This function is invoked to set the Subnet Mask  */
/*                         associated with IP Address used for RM TCP/IP    */
/*                         communication which is configured in nodeid file */
/*                                                                          */
/*    Input(s)           : u4RmSubnetMask - Subnet Mask                     */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Switchid                                         */
/****************************************************************************/

VOID
IssSetRMSubnetMask (UINT4 u4RmSubnetMask)
{
    gIssGlobalInfo.u4IssRmSubnetMask = u4RmSubnetMask;
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetRMStackInterface                           */
/*                                                                          */
/*    Description        : This function is invoked to get the Interface    */
/*                         Name used for RM TCP/IP communication which is   */
/*                         configured in nodeid file                        */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : RM Stack Interface                               */
/****************************************************************************/

UINT1              *
IssGetRMStackInterface (VOID)
{
    return gIssGlobalInfo.au1IssRmStackInterface;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetRMStackInterface                           */
/*                                                                          */
/*    Description        : This function is invoked to set the Interface    */
/*                         Name which is used for RM TCP/IP communication   */
/*                         configured in nodeid file                        */
/*                                                                          */
/*    Input(s)           : pu1IssRmStackInterface - Stack Interface         */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
IssSetRMStackInterface (UINT1 *pu1IssRmStackInterface)
{
    MEMCPY (gIssGlobalInfo.au1IssRmStackInterface, pu1IssRmStackInterface,
            STRLEN (pu1IssRmStackInterface));
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSwitchid                                   */
/*                                                                          */
/*    Description        : This function is invoked to get the switchid     */
/*                         which is configured in nodeid file               */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Switchid                                         */
/****************************************************************************/

INT4
IssGetSwitchid (VOID)
{
    return (INT4) gIssGlobalInfo.u4IssSwitchId;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetSwitchid                                   */
/*                                                                          */
/*    Description        : This function is invoked to set the switchid     */
/*                         which is configured in nodeid file               */
/*                                                                          */
/*    Input(s)           : u4Switchid  - Switch Number                      */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/

VOID
IssSetSwitchid (UINT4 u4Switchid)
{
    gIssGlobalInfo.u4IssSwitchId = u4Switchid;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetUserPreference                             */
/*                                                                          */
/*    Description        : This function is invoked to get the priority     */
/*                         which is configured in nodeid file               */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : u4IssConfigState  - user preference              */
/****************************************************************************/

INT4
IssGetUserPreference (VOID)
{
    return (INT4) gIssGlobalInfo.u4IssConfigState;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetUserPreference                             */
/*                                                                          */
/*    Description        : This function is invoked to set the priority     */
/*                         which is configured in nodeid file               */
/*                                                                          */
/*    Input(s)           : u4Priority - User preference                     */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/

VOID
IssSetUserPreference (UINT4 u4Priority)
{
    gIssGlobalInfo.u4IssConfigState = u4Priority;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetIssuStartupMode                            */
/*                                                                          */
/*    Description        : This function is invoked to get the ISSU         */
/*                         startup mode which is configured in nodeid file  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : u1StartUpMode - Startup Mode                     */
/****************************************************************************/
INT4
IssGetIssuStartupMode (VOID)
{
    return (INT4) gIssuStartupInfo.u1StartUpMode;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetIssuStartupMode                            */
/*                                                                          */
/*    Description        : This function is invoked to set the ISSU         */
/*                         Startup Mode which is configured in nodeid file  */
/*                                                                          */
/*    Input(s)           : u1StartUpMode - Startup Mode                     */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetIssuStartupMode (UINT1 u1StartUpMode)
{
    gIssuStartupInfo.u1StartUpMode = u1StartUpMode;

    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetIssuMode                                   */
/*                                                                          */
/*    Description        : This function is invoked to get the ISSU         */
/*                         mode which is configured in nodeid file          */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : u4IssuMode - ISSU Mode                           */
/****************************************************************************/
INT4
IssGetIssuMode (VOID)
{
    return (INT4) gIssuStartupInfo.u4IssuMode;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetIssuMode                                   */
/*                                                                          */
/*    Description        : This function is invoked to set the ISSU         */
/*                         Mode which is configured in nodeid file          */
/*                                                                          */
/*    Input(s)           : u4IssuMode - ISSU Mode                           */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetIssuMode (UINT4 u4IssuMode)
{
    gIssuStartupInfo.u4IssuMode = u4IssuMode;

    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetRollBackIssVersion                         */
/*                                                                          */
/*    Description        : This function is invoked to get the Old ISS      */
/*                         Version from the nodeid file                      */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : au1RollBackIssVersion - Old Iss Version          */
/****************************************************************************/
UINT1              *
IssGetRollBackIssVersion (VOID)
{
    return (gIssuStartupInfo.au1RollBackIssVersion);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetRollBackIssVersion                         */
/*                                                                          */
/*    Description        : This function will set the ISS version to be     */
/*                         Roll back in case of upgrade failure              */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetRollBackIssVersion (UINT1 au1RollBackIssVersion[])
{
    STRNCPY (gIssuStartupInfo.au1RollBackIssVersion, au1RollBackIssVersion,
             (sizeof (gIssuStartupInfo.au1RollBackIssVersion) - 1));
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetLastUpgradeTime                            */
/*                                                                          */
/*    Description        : This function is invoked to get the              */
/*                         Last Upgraded Time                                */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : u4LastUpgradedTime                               */
/****************************************************************************/
UINT4
IssGetIssuLastUpgradeTime (VOID)
{
    return (gIssuStartupInfo.u4LastUpgradedTime);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetLastUpgradeTime                            */
/*                                                                          */
/*    Description        : This function will set the ISSU                    */
/*                         Last upgraded time                                  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetIssuLastUpgradeTime (UINT4 u4LastUpgradeTime)
{
    gIssuStartupInfo.u4LastUpgradedTime = u4LastUpgradeTime;

    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetRollBackSwPath                             */
/*                                                                          */
/*    Description        : This function is invoked to get the Old ISS      */
/*                         Software Path from the nodeid file               */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : au1RollBackSwPath - Rollback Spftware Path       */
/****************************************************************************/
UINT1              *
IssGetRollBackSwPath (VOID)
{
    return (gIssuStartupInfo.au1RollBackSwPath);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetRollBackSwPath                             */
/*                                                                          */
/*    Description        : This function will set the Path to be            */
/*                         Roll back in case of upgrade failure             */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetRollBackSwPath (UINT1 au1RollBackSwPath[])
{
    STRNCPY (gIssuStartupInfo.au1RollBackSwPath, au1RollBackSwPath,
             (sizeof (gIssuStartupInfo.au1RollBackSwPath) - 1));
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetCurrentSwPath                              */
/*                                                                          */
/*    Description        : This function is invoked to get the Current      */
/*                         Software Path from the nodeid file               */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : au1RollBackSwPath - Rollback Spftware Path       */
/****************************************************************************/
UINT1              *
IssGetCurrentSwPath (VOID)
{
    return (gIssuStartupInfo.au1CurrentSwPath);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetCurrentSwPath                              */
/*                                                                          */
/*    Description        : This function will set the current software Path */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetCurrentSwPath (UINT1 au1CurrentSwPath[])
{
    STRNCPY (gIssuStartupInfo.au1CurrentSwPath, au1CurrentSwPath,
             (sizeof (gIssuStartupInfo.au1CurrentSwPath) - 1));
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetIssuNodeRole                               */
/*                                                                          */
/*    Description        : This function is invoked to get the ISSU         */
/*                         node role which is configured in nodeid file     */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : u1NodeRole - ISSU Node Role                      */
/****************************************************************************/
INT4
IssGetIssuNodeRole (VOID)
{
    return (INT4) gIssuStartupInfo.u1NodeRole;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetIssuNodeRole                               */
/*                                                                          */
/*    Description        : This function is invoked to set the ISSU         */
/*                         node role which is configured in nodeid file     */
/*                                                                          */
/*    Input(s)           : u1NodeRole - ISSU Node role                      */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetIssuNodeRole (UINT1 u1NodeRole)
{
    gIssuStartupInfo.u1NodeRole = u1NodeRole;

    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetAuditStatus                          */
/*                                                                          */
/*    Description        : This function is invoked to check               
 *                          whether Audit is enabled or not                 */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ISS_TRUE/ISS_FALSE                                */
/****************************************************************************/

INT4
IssGetAuditStatus (VOID)
{
    if ((gi4AuditStatus == ISS_ENABLE) && (gu1MsrRestoreCount == 1))
    {
        return ISS_TRUE;
    }

    return ISS_FALSE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssUploadFile                                    */
/*                                                                          */
/*    Description        : Starts uploading  generic files                  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/*                         None                                             */
/****************************************************************************/

VOID
IssUploadFile (UINT1 *pu1ErrBuf)
{
    INT4                i4RetVal = ISS_FAILURE;
#ifdef SSH_WANTED
    CHR1                i1SftpError[MSG_BUF_SIZE];
    MEMSET (i1SftpError, '\0', MSG_BUF_SIZE);
#endif

    gi4RemoteSaveStatus = MIB_REMOTESAVE_IN_PROGRESS;
    if ((STRLEN (gIssUlLogFileInfo.au1IssUlLogFileName) == 0)
        || (STRLEN (gIssUlLogFileInfo.au1IssUlLogRemoteFileName) == 0))
    {
        if (pu1ErrBuf != NULL)
        {
            SPRINTF ((CHR1 *) pu1ErrBuf, "NULL File Name");
        }
        gi4RemoteSaveStatus = LAST_MIB_REMOTESAVE_FAILED;
    }

    if (gIssUlLogFileInfo.u4IssUlTransferMode == ISS_TFTP_TRANSFER_MODE)
    {
        if ((i4RetVal = tftpcSendFile (gIssUlLogFileInfo.IssUlLogFileFromIp,
                                       (const UINT1 *) gIssUlLogFileInfo.
                                       au1IssUlLogRemoteFileName,
                                       (const UINT1 *) gIssUlLogFileInfo.
                                       au1IssUlLogFileName)) == TFTPC_OK)
        {
            gi4RemoteSaveStatus = LAST_MIB_REMOTESAVE_SUCCESSFUL;
        }
        else
        {
            if (pu1ErrBuf != NULL)
            {
                SPRINTF ((CHR1 *) pu1ErrBuf, "[%s]",
                         (CONST CHR1 *) TFTPC_ERR_MSG (i4RetVal));
            }
            gi4RemoteSaveStatus = LAST_MIB_REMOTESAVE_FAILED;
        }

    }
#ifdef SSH_WANTED
    else
    {
        if ((i4RetVal =
             sftpcArSendFile (gIssUlLogFileInfo.IssSftpUlLogInfo.au1UserName,
                              gIssUlLogFileInfo.IssSftpUlLogInfo.au1Passwd,
                              gIssUlLogFileInfo.IssUlLogFileFromIp,
                              (UINT1 *) gIssUlLogFileInfo.
                              au1IssUlLogRemoteFileName,
                              (UINT1 *) gIssUlLogFileInfo.
                              au1IssUlLogFileName)) == SFTP_SUCCESS)
        {
            gi4RemoteSaveStatus = LAST_MIB_REMOTESAVE_SUCCESSFUL;

        }
        else
        {
            if (pu1ErrBuf != NULL)
            {
                SftpcArErrString (i1SftpError);
                SPRINTF ((CHR1 *) pu1ErrBuf, "[%s]", i1SftpError);

            }
            gi4RemoteSaveStatus = LAST_MIB_REMOTESAVE_FAILED;
        }
    }
#endif
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssDownloadFile                                  */
/*                                                                          */
/*    Description        : Starts downloading generic files                 */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/*                         None                                             */
/****************************************************************************/

VOID
IssDownloadFile (UINT1 *pu1ErrBuf)
{
    INT4                i4RetVal = ISS_FAILURE;
#ifdef SSH_WANTED
    CHR1                i1SftpError[MSG_BUF_SIZE];
    MEMSET (i1SftpError, '\0', MSG_BUF_SIZE);
#endif

    gIssDlImageInfo.u4DownloadStatus = MIB_DOWNLOAD_IN_PROGRESS;

    if (STRLEN (gIssDlImageInfo.au1IssDlImageName) == 0)
    {
        if (pu1ErrBuf != NULL)
        {
            SPRINTF ((CHR1 *) pu1ErrBuf, "NULL File Name");
        }
        gIssDlImageInfo.u4DownloadStatus = LAST_MIB_DOWNLOAD_FAILED;
    }

    if (gIssDlImageInfo.u4IssDlTransferMode == ISS_TFTP_TRANSFER_MODE)
    {
        if ((i4RetVal = tftpcRecvFile (gIssDlImageInfo.IssDlImageFromIp,
                                       (const UINT1 *) gIssDlImageInfo.
                                       au1IssDlImageName,
                                       (const UINT1 *) gIssDlImageInfo.
                                       au1IssDlImageName)) == TFTPC_OK)
        {
            gIssDlImageInfo.u4DownloadStatus = LAST_MIB_DOWNLOAD_SUCCESSFUL;
        }
        else
        {
            if (pu1ErrBuf != NULL)
            {
                SPRINTF ((CHR1 *) pu1ErrBuf, "[%s]",
                         (CONST CHR1 *) TFTPC_ERR_MSG (i4RetVal));
            }
            gIssDlImageInfo.u4DownloadStatus = LAST_MIB_DOWNLOAD_FAILED;
        }

    }
#ifdef SSH_WANTED
    else
    {
        if ((i4RetVal =
             sftpcArRecvFile (gIssDlImageInfo.IssSftpDlImageInfo.au1UserName,
                              gIssDlImageInfo.IssSftpDlImageInfo.au1Passwd,
                              gIssDlImageInfo.IssDlImageFromIp,
                              (UINT1 *) gIssDlImageInfo.au1IssDlImageName,
                              (UINT1 *) gIssDlImageInfo.au1IssDlImageName)) ==
            SFTP_SUCCESS)
        {
            gIssDlImageInfo.u4DownloadStatus = LAST_MIB_DOWNLOAD_SUCCESSFUL;
        }
        else
        {
            if (pu1ErrBuf != NULL)
            {
                SftpcArErrString (i1SftpError);
                SPRINTF ((CHR1 *) pu1ErrBuf, "[%s]", i1SftpError);
            }
            gIssDlImageInfo.u4DownloadStatus = LAST_MIB_DOWNLOAD_FAILED;
        }
    }
#endif
}

/*****************************************************************************/
/* Function Name      : IssSetModuleSystemControl                            */
/*                                                                           */
/* Description        : This function is called from all the modules         */
/*                      after task creation. The routine sets the system     */
/*                      control as MODULE_START                              */
/*                                                                           */
/* Input(s)           : i4ModuleId         - Module ID                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : au1IssModuleSystemControl                            */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : au1IssModuleSystemControl                            */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/
INT4
IssSetModuleSystemControl (INT4 i4ModuleId, INT4 i4State)
{
    gIssGlobalInfo.au1IssModuleSystemControl[i4ModuleId] = (UINT1) i4State;
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssGetModuleSystemControl                            */
/*                                                                           */
/* Description        : The routine Get the system Module Status             */
/*                                                                           */
/* Input(s)           : i4ModuleId         - Module ID                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : au1IssModuleSystemControl                            */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : au1IssModuleSystemControl                            */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/
INT4
IssGetModuleSystemControl (INT4 i4ModuleId)
{
    return (gIssGlobalInfo.au1IssModuleSystemControl[i4ModuleId]);
}

/*****************************************************************************/
/* Function Name      : IssGetCsrRestoreFlag                                 */
/*                                                                           */
/* Description        : This function is called from all the modules         */
/*                      after task creation. The routine give the flag value */
/*                      of csr                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : au1IssModuleCsrRestoreFlag                           */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/
INT4
IssGetCsrRestoreFlag (VOID)
{
    return (INT4) gIssGlobalInfo.u4IssModuleCsrRestoreFlag;
}

/*****************************************************************************/
/* Function Name      : IssGetCsrRestoresFlag                                 */
/*                                                                           */
/* Description        : This function is called from all the modules         */
/*                      after task creation. The routine give the flag value */
/*                      of csr                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : au1IssModuleCsrRestoreFlag                           */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/
INT4
IssGetCsrRestoresFlag (INT4 i4ModuleId)
{
    return gIssGlobalInfo.au1IssModuleCsrRestoreFlag[i4ModuleId];
}

/*****************************************************************************/
/* Function Name      : IssSaveConfigurations                                */
/*                                                                           */
/* Description        : This function is called by RM when Active Crashes    */
/*                      while dynamic bulk update is in process.This can be  */
/*                      considered as double failure (Standby is coming up,  */
/*                      and before is completely up active crashes). But in  */
/*                      in this case, the static bulk update has completely  */
/*                      finished, hence the existing static configurations   */
/*                      need to be restored when the ISS is rebooted         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS - On success                             */
/*                      ISS_FAILURE - On failure                             */
/*****************************************************************************/
INT4
IssSaveConfigurations ()
{
    INT4                i4MibSaveResult;
    INT4                i4RemoteSaveResult = 1;
    INT4                i4DlStatus;
    INT4                i4RestoreStatus;

    /* Save the Configurations here.. The Save Option would have been 
     * synchronized with standby during static bulk update. It is possible 
     * that the Save option is NO_SAVE, in this case, we cannot restore the
     * static bulk configurations when the standby ISS reboots.
     */
    if (gIssConfigSaveInfo.IssConfigSaveOption == ISS_CONFIG_NO_SAVE)
    {
        /* Do nothing */
        return ISS_SUCCESS;
    }

    gIssConfigRestoreInfo.IssConfigRestoreOption = ISS_CONFIG_RESTORE;

    /* Call the NvRam Write routine here */
    IssSetRestoreOptionToNvRam (gIssConfigRestoreInfo.IssConfigRestoreOption);

    /* Get the status of mib save operation..
     * If it is already in progress flag an error
     */
    nmhGetIssConfigSaveStatus (&i4MibSaveResult);
    if (i4MibSaveResult == MIB_SAVE_IN_PROGRESS)
    {
        return ISS_FAILURE;
    }

    nmhGetIssRemoteSaveStatus (&i4RemoteSaveResult);
    if (i4RemoteSaveResult == MIB_SAVE_IN_PROGRESS)
    {
        return ISS_FAILURE;
    }

    nmhGetIssDownloadStatus (&i4DlStatus);
    if (i4DlStatus == MIB_DOWNLOAD_IN_PROGRESS)
    {
        return ISS_FAILURE;
    }

    nmhGetIssConfigRestoreStatus (&i4RestoreStatus);
    if (i4RestoreStatus == MIB_RESTORE_IN_PROGRESS)
    {
        return ISS_FAILURE;
    }

    /* Triggering MSR task */
    gu1IssUlLogFlag = UNSET_FLAG;

    if (nmhSetIssInitiateConfigSave (ISS_TRUE) != SNMP_SUCCESS)
    {
        if (gIssConfigSaveInfo.IssConfigSaveOption == ISS_CONFIG_REMOTE_SAVE)
        {
            /* Cannot trigger TFTP. Verify Remote IP and File Name */
        }
        else
        {
            /* Cannot save now. Please try after some time */
        }
        return ISS_FAILURE;
    }

    /* Building configuration ... */

    while (1)
    {
        OsixTskDelay (ISS_LOOP_DURATION * SYS_TIME_TICKS_IN_A_SEC);

        /* Get the status of Remote save operation */

        nmhGetIssRemoteSaveStatus (&i4RemoteSaveResult);
        nmhGetIssConfigSaveStatus (&i4MibSaveResult);

        /* Check the progress of backup operation. 
         * Break from the while loop if the operation 
         * is complete or on time out */
        if (gIssConfigSaveInfo.IssConfigSaveOption == ISS_CONFIG_REMOTE_SAVE)
        {
            if ((i4RemoteSaveResult == LAST_MIB_SAVE_SUCCESSFUL) ||
                (i4RemoteSaveResult == LAST_MIB_SAVE_FAILED))
            {
                break;
            }
        }
        else
        {
            if ((i4MibSaveResult == LAST_MIB_SAVE_SUCCESSFUL) ||
                (i4MibSaveResult == LAST_MIB_SAVE_FAILED))
            {
                break;
            }
        }
    }

    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssEraseConfigurations                               */
/*                                                                           */
/* Description        : This function is called by RM when static bulk update*/
/*                      is happening and a crash has occured. This can be    */
/*                      considered as double failure (Standby is coming up,  */
/*                      and before is completely up active crashes).         */
/*                      update. In this case the ISS is to be rebooted, hence*/
/*                      old configurations are to be erased.                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS - On success                             */
/*                      ISS_FAILURE - On failure                             */
/*****************************************************************************/
VOID
IssEraseConfigurations ()
{
    IssLock ();

    /* To erase the Configurations, just set the Restore option as 
     * ISS_CONFIG_NO_RESTORE in issnvram.txt as the system is going to reboot 
     * now. 
     */
    gIssConfigRestoreInfo.IssConfigRestoreOption = ISS_CONFIG_NO_RESTORE;

    /* Call the NvRam Write routine here */
    IssSetRestoreOptionToNvRam (gIssConfigRestoreInfo.IssConfigRestoreOption);

    IssUnLock ();
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetCpuCountInChassis                            */
/*                                                                          */
/*    Description        : This function is used to get the Cpu count       */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : u4ChassisNum - Specifies the chassis number      */
/*                                                                          */
/*    Output(s)          : pu2CpuCount - Number of CPU present in chassis   */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
IssGetCpuCountInChassis (UINT4 u4ChassisNum, UINT2 *pu2CpuCount)
{
    UNUSED_PARAM (u4ChassisNum);
#if !defined (STACKING_WANTED)
    *pu2CpuCount = 1;
#else
    *pu2CpuCount = 0;
#endif
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetPsCountInChassis                           */
/*                                                                          */
/*    Description        : This function is used to get the Power supply 
 *                         count of the system                              */
/*                                                                          */
/*    Input(s)           : u4ChassisNum - Specifies the chassis number      */
/*                                                                          */
/*    Output(s)          : pu2PSCount - Number of Power Supplies present in 
 *                         chassis   */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
IssGetPsCountInChassis (UINT4 u4ChassisNum, UINT2 *pu2PSCount)
{
    UNUSED_PARAM (u4ChassisNum);
#if !defined (STACKING_WANTED)
    *pu2PSCount = 1;
#else
    *pu2PSCount = 0;
#endif
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetFanCountInChassis                                   */
/*                                                                          */
/*    Description        : This function is used to get the fan count       */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : u4ChassisNum - Specifies the chassis number      */
/*                                                                          */
/*    Output(s)          : pu2FanCount - Number of Fans present in chassis  */

/*    Returns            : None                                             */
/****************************************************************************/

VOID
IssGetFanCountInChassis (UINT4 u4ChassisNum, UINT2 *pu2FanCount)
{
    UNUSED_PARAM (u4ChassisNum);
#if !defined (STACKING_WANTED)
    *pu2FanCount = ISS_SWITCH_MAX_FAN;
#else
    *pu2FanCount = 0;
#endif
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetChassisCount                              */
/*                                                                          */
/*    Description        : This function is used to set the serial number   */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : Chassis Count                                        */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
IssGetChassisCount (UINT2 *pu2ChassisCount)
{
#ifdef MBSM_WANTED
    *pu2ChassisCount = MBSM_MAX_SLOTS;
#else
    *pu2ChassisCount = 0;
#endif
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetCpuCountInStack                            */
/*                                                                          */
/*    Description        : This function is used to get the Cpu count       */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : pu2CpuCount - Cpu Count                          */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
IssGetCpuCountInStack (UINT2 *pu2CpuCount)
{
    *pu2CpuCount = 1;
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetFanCountInStack                            */
/*                                                                          */
/*    Description        : This function is used to get the fan count       */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : None.                                            */

/*    Output(s)          : pu2FanCount - Fan Count                          */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssGetFanCountInStack (UINT2 *pu2FanCount)
{
    *pu2FanCount = ISS_SWITCH_MAX_FAN;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetPsCountInStack                         */
/*                                                                          */
/*    Description        : This function is used to get the Power Supply    */
/*                         count of the system                              */
/*                                                                          */
/*    Input(s)           : None.                                            */

/*    Output(s)          : pu2PSCount - Power Supply Count in Stack         */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
IssGetPsCountInStack (UINT2 *pu2PSCount)
{
    *pu2PSCount = 1;
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetPsPhyInfoInStack                           */
/*                                                                          */
/*    Description        : This function is used to set power supply        */
/*                         in stack parameters                              */
/*                                                                          */
/*    Input(s)           : u4PSNum                                          */
/*                         u1ParamType                                      */
/*                         pu1Param                                         */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/FAILURE                              */
/****************************************************************************/

INT1
IssSetPsPhyInfoInStack (UINT4 u4PSNum, UINT1 u1ParamType, UINT1 *pu1Param)
{
    UNUSED_PARAM (u4PSNum);
    if (pu1Param == NULL)
    {
        return ISS_FAILURE;
    }
    switch (u1ParamType)
    {
        case ISS_ENT_PHY_SERIAL_NUM:
            MEMSET (gIssSysGroupInfo.IssSwitchInfo.au1PSSerialNum, 0,
                    ISS_ENT_PHY_SER_NUM_LEN);

            MEMCPY (gIssSysGroupInfo.IssSwitchInfo.au1PSSerialNum, pu1Param,
                    STRLEN (pu1Param));
            break;
        case ISS_ENT_PHY_ASSET_ID:
            MEMSET (gIssSysGroupInfo.IssSwitchInfo.au1PSAssetId, 0,
                    ISS_ENT_PHY_ASSET_ID_LEN);

            MEMCPY (gIssSysGroupInfo.IssSwitchInfo.au1PSAssetId, pu1Param,
                    STRLEN (pu1Param));
            break;
        case ISS_ENT_PHY_URIS:
            MEMSET (gIssSysGroupInfo.IssSwitchInfo.au1PSUris, 0,
                    ISS_ENT_PHY_URIS_LEN + 1);

            MEMCPY (gIssSysGroupInfo.IssSwitchInfo.au1PSUris, pu1Param,
                    STRLEN (pu1Param));
            break;
        case ISS_ENT_PHY_ALIAS_NAME:
            MEMSET (gIssSysGroupInfo.IssSwitchInfo.au1PSAlias, 0,
                    ISS_ENT_PHY_ALIAS_LEN);

            MEMCPY (gIssSysGroupInfo.IssSwitchInfo.au1PSAlias, pu1Param,
                    STRLEN (pu1Param));
            break;
        default:
            return ISS_FAILURE;
            break;
    }
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetCpuPhyInfoInStack                          */
/*                                                                          */
/*    Description        : This function is used to set power supply        */
/*                         in stack parameters                              */
/*                                                                          */
/*    Input(s)           : u4PSNum                                          */
/*                         u1ParamType                                      */
/*                         pu1Param                                         */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/FAILURE                              */
/****************************************************************************/

INT1
IssSetCpuPhyInfoInStack (UINT4 u4CpuNum, UINT1 u1ParamType, UINT1 *pu1Param)
{
    UNUSED_PARAM (u4CpuNum);
    if (pu1Param == NULL)
    {
        return ISS_FAILURE;
    }
    switch (u1ParamType)
    {
        case ISS_ENT_PHY_SERIAL_NUM:
            MEMSET (gIssSysGroupInfo.IssSwitchInfo.au1CPUSerialNum, 0,
                    ISS_ENT_PHY_SER_NUM_LEN);
            MEMCPY (gIssSysGroupInfo.IssSwitchInfo.au1CPUSerialNum, pu1Param,
                    STRLEN (pu1Param));
            break;
        case ISS_ENT_PHY_ASSET_ID:
            MEMSET (gIssSysGroupInfo.IssSwitchInfo.au1CPUAssetId, 0,
                    ISS_ENT_PHY_ASSET_ID_LEN);
            MEMCPY (gIssSysGroupInfo.IssSwitchInfo.au1CPUAssetId, pu1Param,
                    STRLEN (pu1Param));
            break;
        case ISS_ENT_PHY_URIS:
            MEMSET (gIssSysGroupInfo.IssSwitchInfo.au1CPUUris, 0,
                    ISS_ENT_PHY_URIS_LEN + 1);
            MEMCPY (gIssSysGroupInfo.IssSwitchInfo.au1CPUUris, pu1Param,
                    STRLEN (pu1Param));
            break;
        case ISS_ENT_PHY_ALIAS_NAME:
            MEMSET (gIssSysGroupInfo.IssSwitchInfo.au1CPUAlias, 0,
                    ISS_ENT_PHY_ALIAS_LEN);
            MEMCPY (gIssSysGroupInfo.IssSwitchInfo.au1CPUAlias, pu1Param,
                    STRLEN (pu1Param));
            break;
        default:
            return ISS_FAILURE;
    }
    return ISS_SUCCESS;

}/****************************************************************************/

/*                                                                          */
/*    Function Name      : IssSetFanPhyInfoInStack                          */
/*                                                                          */
/*    Description        : This function is used to set power supply        */
/*                         in stack parameters                              */
/*                                                                          */
/*    Input(s)           : u4PSNum                                          */
/*                         u1ParamType                                      */
/*                         pu1Param                                         */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/FAILURE                              */
/****************************************************************************/

INT1
IssSetFanPhyInfoInStack (UINT4 u4CpuNum, UINT1 u1ParamType, UINT1 *pu1Param)
{
    UNUSED_PARAM (u4CpuNum);
    UNUSED_PARAM (u1ParamType);
    UNUSED_PARAM (pu1Param);
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetChassisPhyInfo                             */
/*                                                                          */
/*    Description        : This function is used to set power supply        */
/*                         in stack parameters                              */
/*                                                                          */
/*    Input(s)           : u4PSNum                                          */
/*                         u1ParamType                                      */
/*                         pu1Param                                         */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/FAILURE                              */
/****************************************************************************/
INT1
IssSetChassisPhyInfo (UINT4 u4ChassisNum, UINT1 u1ParamType, UINT1 *pu1Param)
{
    UNUSED_PARAM (u4ChassisNum);
#if defined (STACKING_WANTED)
    UNUSED_PARAM (u1ParamType);
    UNUSED_PARAM (pu1Param);
    return ISS_SUCCESS;
#endif
    if (pu1Param == NULL)
    {
        return ISS_FAILURE;
    }
    switch (u1ParamType)
    {
        case ISS_ENT_PHY_SERIAL_NUM:
            MEMSET (gIssSysGroupInfo.au1SerialNum, 0, ISS_ENT_PHY_SER_NUM_LEN);
            MEMCPY (gIssSysGroupInfo.au1SerialNum, pu1Param, STRLEN (pu1Param));
            break;
        case ISS_ENT_PHY_ASSET_ID:
            MEMSET (gIssSysGroupInfo.au1AssetId, 0, ISS_ENT_PHY_ASSET_ID_LEN);
            MEMCPY (gIssSysGroupInfo.au1AssetId, pu1Param, STRLEN (pu1Param));
            break;
        case ISS_ENT_PHY_URIS:
            MEMSET (gIssSysGroupInfo.au1Uris, 0, ISS_ENT_PHY_URIS_LEN);
            MEMCPY (gIssSysGroupInfo.au1Uris, pu1Param, STRLEN (pu1Param));
            break;
        case ISS_ENT_PHY_ALIAS_NAME:
            MEMSET (gIssSysGroupInfo.au1Alias, 0, ISS_ENT_PHY_ALIAS_LEN);
            MEMCPY (gIssSysGroupInfo.au1Alias, pu1Param, STRLEN (pu1Param));
            break;
        default:
            return ISS_FAILURE;
    }
    return ISS_SUCCESS;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetCpuPhyInfoInChassis                        */
/*                                                                          */
/*    Description        : This function is used to set power supply        */
/*                         in stack parameters                              */
/*                                                                          */
/*    Input(s)           : u4PSNum                                          */
/*                         u1ParamType                                      */
/*                         pu1Param                                         */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/FAILURE                              */
/****************************************************************************/

INT1
IssSetCpuPhyInfoInChassis (UINT4 u4ChassisNum, UINT2 u2CpuNum,
                           UINT1 u1ParamType, UINT1 *pu1Param)
{
    UNUSED_PARAM (u4ChassisNum);
    UNUSED_PARAM (u2CpuNum);
    if (pu1Param == NULL)
    {
        return ISS_FAILURE;
    }
    switch (u1ParamType)
    {
        case ISS_ENT_PHY_SERIAL_NUM:
            MEMSET (gIssSysGroupInfo.IssSwitchInfo.au1CPUSerialNum, 0,
                    ISS_ENT_PHY_SER_NUM_LEN);
            MEMCPY (gIssSysGroupInfo.IssSwitchInfo.au1CPUSerialNum, pu1Param,
                    STRLEN (pu1Param));
            break;
        case ISS_ENT_PHY_ASSET_ID:
            MEMSET (gIssSysGroupInfo.IssSwitchInfo.au1CPUAssetId, 0,
                    ISS_ENT_PHY_ASSET_ID_LEN);
            MEMCPY (gIssSysGroupInfo.IssSwitchInfo.au1CPUAssetId, pu1Param,
                    STRLEN (pu1Param));
            break;
        case ISS_ENT_PHY_URIS:
            MEMSET (gIssSysGroupInfo.IssSwitchInfo.au1CPUUris, 0,
                    ISS_ENT_PHY_URIS_LEN + 1);
            MEMCPY (gIssSysGroupInfo.IssSwitchInfo.au1CPUUris, pu1Param,
                    STRLEN (pu1Param));
            break;
        case ISS_ENT_PHY_ALIAS_NAME:
            MEMSET (gIssSysGroupInfo.IssSwitchInfo.au1CPUAlias, 0,
                    ISS_ENT_PHY_ALIAS_LEN);
            MEMCPY (gIssSysGroupInfo.IssSwitchInfo.au1CPUAlias, pu1Param,
                    STRLEN (pu1Param));
            break;
        default:
            return ISS_FAILURE;
    }
    return ISS_SUCCESS;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetPsPhyInfoInChassis                         */
/*                                                                          */
/*    Description        : This function is used to set power supply        */
/*                         in stack parameters                              */
/*                                                                          */
/*    Input(s)           : u4PSNum                                          */
/*                         u1ParamType                                      */
/*                         pu1Param                                         */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/FAILURE                              */
/****************************************************************************/

INT1
IssSetPsPhyInfoInChassis (UINT4 u4ChassisNum, UINT2 u2PSNum,
                          UINT1 u1ParamType, UINT1 *pu1Param)
{
    UNUSED_PARAM (u4ChassisNum);
    UNUSED_PARAM (u2PSNum);
    if (pu1Param == NULL)
    {
        return ISS_FAILURE;
    }
    switch (u1ParamType)
    {
        case ISS_ENT_PHY_SERIAL_NUM:
            MEMSET (gIssSysGroupInfo.IssSwitchInfo.au1PSSerialNum, 0,
                    ISS_ENT_PHY_SER_NUM_LEN);

            MEMCPY (gIssSysGroupInfo.IssSwitchInfo.au1PSSerialNum, pu1Param,
                    STRLEN (pu1Param));
            break;
        case ISS_ENT_PHY_ASSET_ID:
            MEMSET (gIssSysGroupInfo.IssSwitchInfo.au1PSAssetId, 0,
                    ISS_ENT_PHY_ASSET_ID_LEN);

            MEMCPY (gIssSysGroupInfo.IssSwitchInfo.au1PSAssetId, pu1Param,
                    STRLEN (pu1Param));
            break;
        case ISS_ENT_PHY_URIS:
            MEMSET (gIssSysGroupInfo.IssSwitchInfo.au1PSUris, 0,
                    ISS_ENT_PHY_URIS_LEN + 1);

            MEMCPY (gIssSysGroupInfo.IssSwitchInfo.au1PSUris, pu1Param,
                    STRLEN (pu1Param));
            break;
        case ISS_ENT_PHY_ALIAS_NAME:
            MEMSET (gIssSysGroupInfo.IssSwitchInfo.au1PSAlias, 0,
                    ISS_ENT_PHY_ALIAS_LEN);
            MEMCPY (gIssSysGroupInfo.IssSwitchInfo.au1PSAlias, pu1Param,
                    STRLEN (pu1Param));
            break;
        default:
            return ISS_FAILURE;
    }
    return ISS_SUCCESS;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetFanPhyInfoInChassis                        */
/*                                                                          */
/*    Description        : This function is used to set power supply        */
/*                         in stack parameters                              */
/*                                                                          */
/*    Input(s)           : u4PSNum                                          */
/*                         u1ParamType                                      */
/*                         pu1Param                                         */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/FAILURE                              */
/****************************************************************************/

INT1
IssSetFanPhyInfoInChassis (UINT4 u4ChassisNum, UINT2 u2FanNum,
                           UINT1 u1ParamType, UINT1 *pu1Param)
{
    UNUSED_PARAM (u4ChassisNum);
    UNUSED_PARAM (u2FanNum);
    UNUSED_PARAM (u1ParamType);
    UNUSED_PARAM (pu1Param);
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetIfPhyInfo                                  */
/*                                                                          */
/*    Description        : This function is used to set power supply        */
/*                         in stack parameters                              */
/*                                                                          */
/*    Input(s)           : u4PSNum                                          */
/*                         u1ParamType                                      */
/*                         pu1Param                                         */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/FAILURE                              */
/****************************************************************************/

INT1
IssSetIfPhyInfo (UINT4 u4ChassisNum, UINT4 u4IfIndex,
                 UINT1 u1ParamType, UINT1 *pu1Param)
{
    UINT2               u2IfIndex = (UINT2) u4IfIndex;

    UNUSED_PARAM (u4ChassisNum);

    if (pu1Param == NULL)
    {
        return ISS_FAILURE;
    }
    switch (u1ParamType)
    {
        case ISS_ENT_PHY_URIS:

            if ((u2IfIndex > 0) && (u2IfIndex <= ISS_MAX_PORTS + 1))
            {
                if (gIssGlobalInfo.apIssPortCtrlEntry[u2IfIndex - 1] != NULL)
                {
                    MEMSET (gIssGlobalInfo.apIssPortCtrlEntry[u2IfIndex - 1]->
                            au1Uris, 0, ISS_ENT_PHY_URIS_LEN + 1);
                    MEMCPY (gIssGlobalInfo.apIssPortCtrlEntry[u2IfIndex - 1]->
                            au1Uris, pu1Param, STRLEN (pu1Param));
                }
            }
            break;
        default:
            return ISS_FAILURE;
    }
    return ISS_SUCCESS;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetPsPhyInfoInStack                           */
/*                                                                          */
/*    Description        : This function is used to set power supply        */
/*                         in stack parameters                              */
/*                                                                          */
/*    Input(s)           : u4PSNum                                          */
/*                         u1ParamType                                      */
/*                         pu1Param                                         */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/FAILURE                              */
/****************************************************************************/

INT1
IssSetStackPhyInfo (UINT1 u1ParamType, UINT1 *pu1Param)
{
    if (pu1Param == NULL)
    {
        return ISS_FAILURE;
    }
    switch (u1ParamType)
    {
        case ISS_ENT_PHY_SERIAL_NUM:
            MEMSET (gIssSysGroupInfo.au1SerialNum, 0, ISS_ENT_PHY_SER_NUM_LEN);
            MEMCPY (gIssSysGroupInfo.au1SerialNum, pu1Param, STRLEN (pu1Param));
            break;
        case ISS_ENT_PHY_ASSET_ID:
            MEMSET (gIssSysGroupInfo.au1AssetId, 0, ISS_ENT_PHY_ASSET_ID_LEN);
            MEMCPY (gIssSysGroupInfo.au1AssetId, pu1Param, STRLEN (pu1Param));
            break;
        case ISS_ENT_PHY_URIS:
            MEMSET (gIssSysGroupInfo.au1Uris, 0, ISS_ENT_PHY_URIS_LEN);
            MEMCPY (gIssSysGroupInfo.au1Uris, pu1Param, STRLEN (pu1Param));
            break;
        case ISS_ENT_PHY_ALIAS_NAME:
            MEMSET (gIssSysGroupInfo.au1Alias, 0, ISS_ENT_PHY_ALIAS_LEN);
            MEMCPY (gIssSysGroupInfo.au1Alias, pu1Param, STRLEN (pu1Param));
            break;
        default:
            return ISS_FAILURE;
    }
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetPsPhyInfoInStack                          */
/*                                                                          */
/*    Description        : This function is used to get the Power Supply 
 *                         Info of the system                               */
/*                                                                          */
/*    Input(s)           : u4PSNum -                                        */
/*                                                                          */
/*    Output(s)          : pPhyInfo - Physical component information        */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

INT1
IssGetPsPhyInfoInStack (UINT4 u4PSNum, tIssEntPhyInfo * pPhyInfo)
{

    UNUSED_PARAM (u4PSNum);
    MEMCPY (pPhyInfo->au1SerialNum,
            gIssSysGroupInfo.IssSwitchInfo.au1PSSerialNum,
            STRLEN (gIssSysGroupInfo.IssSwitchInfo.au1PSSerialNum));
    MEMCPY (pPhyInfo->au1Alias, gIssSysGroupInfo.IssSwitchInfo.au1PSAlias,
            STRLEN (gIssSysGroupInfo.IssSwitchInfo.au1PSAlias));
    MEMCPY (pPhyInfo->au1AssetId,
            gIssSysGroupInfo.IssSwitchInfo.au1PSAssetId,
            STRLEN (gIssSysGroupInfo.IssSwitchInfo.au1PSAssetId));
    MEMCPY (pPhyInfo->au1Uris, gIssSysGroupInfo.IssSwitchInfo.au1PSUris,
            STRLEN (gIssSysGroupInfo.IssSwitchInfo.au1PSUris));

    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetCpuPhyInfoInStack                             */
/*                                                                          */
/*    Description        : This function is used to get the CPU Info        */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : u4CpuNum -  Count for which info to be           */
/*                         fetched                                          */
/*    Output(s)          : pPhyInfo - Physical component information        */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

INT1
IssGetCpuPhyInfoInStack (UINT4 u4CpuNum, tIssEntPhyInfo * pPhyInfo)
{

    UNUSED_PARAM (u4CpuNum);
    MEMCPY (pPhyInfo->au1SerialNum,
            gIssSysGroupInfo.IssSwitchInfo.au1CPUSerialNum,
            STRLEN (gIssSysGroupInfo.IssSwitchInfo.au1CPUSerialNum));
    MEMCPY (pPhyInfo->au1Alias, gIssSysGroupInfo.IssSwitchInfo.au1CPUAlias,
            STRLEN (gIssSysGroupInfo.IssSwitchInfo.au1CPUAlias));
    MEMCPY (pPhyInfo->au1AssetId, gIssSysGroupInfo.IssSwitchInfo.au1CPUAssetId,
            STRLEN (gIssSysGroupInfo.IssSwitchInfo.au1CPUAssetId));
    MEMCPY (pPhyInfo->au1Uris, gIssSysGroupInfo.IssSwitchInfo.au1CPUUris,
            STRLEN (gIssSysGroupInfo.IssSwitchInfo.au1CPUUris));

    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetFanPhyInfoInStack                             */
/*                                                                          */
/*    Description        : This function is used to get the Fan Info        */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : u4FanNum - Fan Count                             */
/*                                                                          */
/*    Output(s)          : pPhyInfo - Physical component information        */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

INT1
IssGetFanPhyInfoInStack (UINT4 u4CpuNum, tIssEntPhyInfo * pPhyInfo)
{

    UNUSED_PARAM (u4CpuNum);
    UNUSED_PARAM (pPhyInfo);
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetChassisPhyInfo                             */
/*                                                                          */
/*    Description        : This function is used to get the Chassis Info    */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : u4ChassisNum - Specifies the chassis number      */
/*                                                                          */
/*    Output(s)          : pPhyInfo - Physical component information        */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

INT1
IssGetChassisPhyInfo (UINT4 u4ChassisNum, tIssEntPhyInfo * pPhyInfo)
{
#if !defined (STACKING_WANTED)
    UNUSED_PARAM (u4ChassisNum);

    ISS_MEMCPY (pPhyInfo->au1Descr, ISS_DEFAULT_DESCR,
                ISS_STRLEN (ISS_DEFAULT_DESCR));

    ISS_MEMCPY (pPhyInfo->au1VendorType, ISS_DEFAULT_VENDOR_TYPE,
                ISS_STRLEN (ISS_DEFAULT_VENDOR_TYPE));

    ISS_MEMCPY (pPhyInfo->au1SoftwareRev,
                ISS_DEFAULT_SW_REV_NUM, ISS_STRLEN (ISS_DEFAULT_SW_REV_NUM));

    ISS_MEMCPY (pPhyInfo->au1Name,
                IssGetSwitchName (), ISS_STRLEN (IssGetSwitchName ()));

    ISS_MEMCPY (pPhyInfo->au1MfgName,
                ISS_DEFAULT_MFG_NAME, ISS_STRLEN (ISS_DEFAULT_MFG_NAME));

    ISS_MEMCPY (pPhyInfo->au1MfgDate,
                ISS_DEFAULT_MFG_DATE, ISS_STRLEN (ISS_DEFAULT_MFG_DATE));

    ISS_MEMCPY (pPhyInfo->au1HardwareRev,
                IssGetHardwareVersion (),
                ISS_STRLEN (IssGetHardwareVersion ()));

    ISS_MEMCPY (pPhyInfo->au1FirmwareRev,
                IssGetFirmwareVersion (),
                ISS_STRLEN (IssGetFirmwareVersion ()));

    ISS_MEMCPY (pPhyInfo->au1SerialNum, gIssSysGroupInfo.au1SerialNum,
                ISS_STRLEN (gIssSysGroupInfo.au1SerialNum));

    if (ISS_STRLEN (gIssSysGroupInfo.au1Alias) < ISS_ENT_PHY_ALIAS_LEN)
    {
        if (ISS_STRLEN (gIssSysGroupInfo.au1Alias) == 0)
        {
            ISS_MEMCPY (pPhyInfo->au1Alias, ISS_DEFAULT_ALIAS_NAME,
                        ISS_STRLEN (ISS_DEFAULT_ALIAS_NAME));
        }
        else
        {
            ISS_MEMCPY (pPhyInfo->au1Alias, gIssSysGroupInfo.au1Alias,
                        ISS_STRLEN (gIssSysGroupInfo.au1Alias));
        }
    }

    if (ISS_STRLEN (gIssSysGroupInfo.au1AssetId) < ISS_ENT_PHY_ASSET_ID_LEN)
    {
        if (ISS_STRLEN (gIssSysGroupInfo.au1AssetId) == 0)
        {
            ISS_MEMCPY (pPhyInfo->au1AssetId, ISS_DEFAULT_ASSET_ID,
                        ISS_STRLEN (ISS_DEFAULT_ASSET_ID));
        }
        else
        {
            ISS_MEMCPY (pPhyInfo->au1AssetId, gIssSysGroupInfo.au1AssetId,
                        ISS_STRLEN (gIssSysGroupInfo.au1AssetId));
        }
    }

    ISS_MEMCPY (pPhyInfo->au1Uris, gIssSysGroupInfo.au1Uris,
                ISS_STRLEN (gIssSysGroupInfo.au1Uris));

#else
    UNUSED_PARAM (u4ChassisNum);
    UNUSED_PARAM (pPhyInfo);
#endif
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetCpuPhyInfoInChassis                                    */
/*                                                                          */
/*    Description        : This function is used to get the CPU Info        */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : u4ChassisNum - Specifies the chassis number      */
/*                                                                          */
/*    Output(s)          : pPhyInfo - Physical component information        */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

INT1
IssGetCpuPhyInfoInChassis (UINT4 u4ChassisNum, UINT2 u2CpuNum,
                           tIssEntPhyInfo * pPhyInfo)
{
#if !defined (STACKING_WANTED)
    UNUSED_PARAM (u4ChassisNum);
    UNUSED_PARAM (u2CpuNum);
    MEMCPY (pPhyInfo->au1SerialNum,
            gIssSysGroupInfo.IssSwitchInfo.au1CPUSerialNum,
            STRLEN (gIssSysGroupInfo.IssSwitchInfo.au1CPUSerialNum));
    MEMCPY (pPhyInfo->au1Alias, gIssSysGroupInfo.IssSwitchInfo.au1CPUAlias,
            STRLEN (gIssSysGroupInfo.IssSwitchInfo.au1CPUAlias));
    MEMCPY (pPhyInfo->au1AssetId, gIssSysGroupInfo.IssSwitchInfo.au1CPUAssetId,
            STRLEN (gIssSysGroupInfo.IssSwitchInfo.au1CPUAssetId));
    MEMCPY (pPhyInfo->au1Uris, gIssSysGroupInfo.IssSwitchInfo.au1CPUUris,
            STRLEN (gIssSysGroupInfo.IssSwitchInfo.au1CPUUris));
#else
    UNUSED_PARAM (u4ChassisNum);
    UNUSED_PARAM (u2CpuNum);
    UNUSED_PARAM (pPhyInfo);
#endif
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetPsPhyInfoInChassis                                     */
/*                                                                          */
/*    Description        : This function is used to get the Power Supply    */
/*                         Info of the system                               */
/*                                                                          */
/*    Input(s)           : u4ChassisNum - Specifies the chassis number      */
/*                                                                          */
/*    Output(s)          : pPhyInfo - Physical component information        */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

INT1
IssGetPsPhyInfoInChassis (UINT4 u4ChassisNum, UINT2 u2PSNum,
                          tIssEntPhyInfo * pPhyInfo)
{
#if !defined (STACKING_WANTED)
    UNUSED_PARAM (u4ChassisNum);
    UNUSED_PARAM (u2PSNum);
    MEMCPY (pPhyInfo->au1SerialNum,
            gIssSysGroupInfo.IssSwitchInfo.au1PSSerialNum,
            STRLEN (gIssSysGroupInfo.IssSwitchInfo.au1PSSerialNum));
    MEMCPY (pPhyInfo->au1Alias, gIssSysGroupInfo.IssSwitchInfo.au1PSAlias,
            STRLEN (gIssSysGroupInfo.IssSwitchInfo.au1PSAlias));
    MEMCPY (pPhyInfo->au1AssetId,
            gIssSysGroupInfo.IssSwitchInfo.au1PSAssetId,
            STRLEN (gIssSysGroupInfo.IssSwitchInfo.au1PSAssetId));
    MEMCPY (pPhyInfo->au1Uris, gIssSysGroupInfo.IssSwitchInfo.au1PSUris,
            STRLEN (gIssSysGroupInfo.IssSwitchInfo.au1PSUris));
#else
    UNUSED_PARAM (u4ChassisNum);
    UNUSED_PARAM (u2PSNum);
    UNUSED_PARAM (pPhyInfo);
#endif
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetFanPhyInfoInChassis                                    */
/*                                                                          */
/*    Description        : This function is used to get the Fan             */
/*                         Info of the system                               */
/*                                                                          */
/*    Input(s)           : u4ChassisNum - Specifies the chassis number      */
/*                                                                          */
/*    Output(s)          : pPhyInfo - Physical component information        */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

INT1
IssGetFanPhyInfoInChassis (UINT4 u4ChassisNum, UINT2 u2FanNum,
                           tIssEntPhyInfo * pPhyInfo)
{
    UNUSED_PARAM (u4ChassisNum);
    UNUSED_PARAM (u2FanNum);
    UNUSED_PARAM (pPhyInfo);
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetIfPhyInfo                                  */
/*                                                                          */
/*    Description        : This function is used to get the Physical Info   */
/*                         of interface                                     */
/*                                                                          */
/* Input(s)           : u4ChassisNum - Specifies the chassis number         */
/*                                                                          */
/* Output(s)          : pPhyInfo - Physical component information           */
/*                                                                          */
/* Return Value(s)    : ISS_FAILURE/ ISS_SUCCESS                            */
/****************************************************************************/

INT1
IssGetIfPhyInfo (UINT4 u4ChassisNum, UINT4 u4IfIndex, tIssEntPhyInfo * pPhyInfo)
{
    tCfaIfInfo          IfInfo;
    UINT2               u2IfIndex = (UINT2) u4IfIndex;
    INT1                ai1Name[255];
    INT1               *pi1IfName = NULL;

    UNUSED_PARAM (u4ChassisNum);

    MEMSET (ai1Name, 0, 255);
    pi1IfName = ai1Name;
    if (CfaGetIfInfo (u4IfIndex, &IfInfo) == CFA_SUCCESS)
    {
        if ((STRLEN (IfInfo.au1IfAliasName) < ISS_ENT_PHY_ALIAS_LEN) &&
            (STRLEN (IfInfo.au1IfAliasName) != 0))
        {
            MEMCPY (pPhyInfo->au1Alias, IfInfo.au1IfAliasName,
                    (sizeof (pPhyInfo->au1Alias) - 1));
            pPhyInfo->au1Alias[(sizeof (pPhyInfo->au1Alias) - 1)] = '\0';
        }
    }
    else
    {
        return ISS_FAILURE;
    }

    if (CfaCliGetIfName (u4IfIndex, pi1IfName) == CLI_SUCCESS)
    {
        MEMCPY (pPhyInfo->au1Name, pi1IfName, STRLEN (pi1IfName));
    }
    else
    {
        return ISS_FAILURE;
    }
    MEMCPY (pPhyInfo->au1Descr, IfInfo.au1Descr, STRLEN (IfInfo.au1Descr));
    if ((u2IfIndex > 0) && (u2IfIndex <= ISS_MAX_PORTS + 1))
    {
        if (gIssGlobalInfo.apIssPortCtrlEntry[u2IfIndex - 1] != NULL)
        {
            MEMCPY (pPhyInfo->au1Uris,
                    gIssGlobalInfo.apIssPortCtrlEntry[u2IfIndex - 1]->au1Uris,
                    STRLEN (gIssGlobalInfo.apIssPortCtrlEntry[u2IfIndex - 1]->
                            au1Uris));
        }
    }
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetStackPhyInfo                                  */
/*                                                                          */
/*    Description        : This function is used to set the Uris            */
/*                         of the system                                    */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : Physical Information                             */
/*                                                                          */
/*    Returns            : None                                                 */
/****************************************************************************/

INT1
IssGetStackPhyInfo (tIssEntPhyInfo * pPhyInfo)
{

    ISS_MEMCPY (pPhyInfo->au1Descr, ISS_DEFAULT_DESCR,
                ISS_STRLEN (ISS_DEFAULT_DESCR));

    ISS_MEMCPY (pPhyInfo->au1VendorType, ISS_DEFAULT_VENDOR_TYPE,
                ISS_STRLEN (ISS_DEFAULT_VENDOR_TYPE));

    ISS_MEMCPY (pPhyInfo->au1SoftwareRev,
                ISS_DEFAULT_SW_REV_NUM, ISS_STRLEN (ISS_DEFAULT_SW_REV_NUM));

    ISS_MEMCPY (pPhyInfo->au1Name,
                IssGetSwitchName (), ISS_STRLEN (IssGetSwitchName ()));

    ISS_MEMCPY (pPhyInfo->au1MfgName,
                ISS_DEFAULT_MFG_NAME, ISS_STRLEN (ISS_DEFAULT_MFG_NAME));

    ISS_MEMCPY (pPhyInfo->au1HardwareRev,
                IssGetHardwareVersion (),
                ISS_STRLEN (IssGetHardwareVersion ()));

    ISS_MEMCPY (pPhyInfo->au1FirmwareRev,
                IssGetFirmwareVersion (),
                ISS_STRLEN (IssGetFirmwareVersion ()));

    ISS_MEMCPY (pPhyInfo->au1SerialNum, gIssSysGroupInfo.au1SerialNum,
                ISS_STRLEN (gIssSysGroupInfo.au1SerialNum));

    if (ISS_STRLEN (gIssSysGroupInfo.au1Alias) < ISS_ENT_PHY_ALIAS_LEN)
    {
        if (ISS_STRLEN (gIssSysGroupInfo.au1Alias) == 0)
        {
            ISS_MEMCPY (pPhyInfo->au1Alias, ISS_DEFAULT_ALIAS_NAME,
                        ISS_STRLEN (ISS_DEFAULT_ALIAS_NAME));
        }
        else
        {
            ISS_MEMCPY (pPhyInfo->au1Alias, gIssSysGroupInfo.au1Alias,
                        ISS_STRLEN (gIssSysGroupInfo.au1Alias));
        }
    }

    if (ISS_STRLEN (gIssSysGroupInfo.au1AssetId) < ISS_ENT_PHY_ASSET_ID_LEN)
    {
        if (ISS_STRLEN (gIssSysGroupInfo.au1AssetId) == 0)
        {
            ISS_MEMCPY (pPhyInfo->au1AssetId, ISS_DEFAULT_ASSET_ID,
                        ISS_STRLEN (ISS_DEFAULT_ASSET_ID));
        }
        else
        {
            ISS_MEMCPY (pPhyInfo->au1AssetId, gIssSysGroupInfo.au1AssetId,
                        ISS_STRLEN (gIssSysGroupInfo.au1AssetId));
        }
    }

    ISS_MEMCPY (pPhyInfo->au1Uris, gIssSysGroupInfo.au1Uris,
                ISS_STRLEN (gIssSysGroupInfo.au1Uris));
    return ISS_SUCCESS;
}

/*Utility routines to access Mirroring Database*/
/*****************************************************************************/
/* Function Name      : IssMirrIsDestConfigured                              */
/*                                                                           */
/* Description        : This function is called to check if any destination  */
/*                      exists with this Id for the given session number     */
/*                                                                           */
/* Input(s)           : u2Session - Mirroring Session Number to be traversed */
/*                      u4DestNum - Destination entity Id to be searched in  */
/*                                  session database                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/
INT4
IssMirrIsDestConfigured (UINT4 u4SessionIndex, UINT4 u4DestNum)
{

    UINT4               u4SessionNo = 1;
    tIssMirrDestRecordInfo *pDestRecordInfo = NULL;

    ISS_TRC_FN_ENTRY ();

    /*Validate Session Id */
    if (u4SessionIndex == 0 || u4SessionIndex > ISS_MIRR_MAX_SESSIONS
        || u4DestNum == 0)
    {
        ISS_TRC (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                 "\n Invalid Session Id/Destination Id\n");
        ISS_TRC_FN_EXIT ();
        return ISS_FAILURE;
    }

    /*  Iterations to check whether any sessions exists with this Dest ID  */
    for (u4SessionNo = 1; u4SessionNo <= ISS_MIRR_MAX_SESSIONS; u4SessionNo++)
    {
        /*  Fetching the Destination record header for current session  */
        pDestRecordInfo =
            TMO_SLL_First (&(GET_MIRR_SESSION_DEST_HDR (u4SessionNo)));

        while (pDestRecordInfo != NULL)
        {
            if ((u4DestNum >= pDestRecordInfo->u4DestMinId &&
                 u4DestNum <= pDestRecordInfo->u4DestMaxId))
            {
                ISS_TRC (ISS_TRC_ALL, "\n Destination exists in the session\n");
                ISS_TRC_FN_EXIT ();
                return ISS_SUCCESS;
            }
            pDestRecordInfo =
                TMO_SLL_Next (&(GET_MIRR_SESSION_DEST_HDR (u4SessionNo)),
                              &pDestRecordInfo->NextRecordInfo);
        }
    }
    ISS_TRC (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
             "\n Destination does not exist in the session\n");
    ISS_TRC_FN_EXIT ();
    return ISS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : IssMirrGetNextDestRecrd                              */
/*                                                                           */
/* Description        : This function is called to get next destination      */
/*                      Id for the given session number                      */
/*                                                                           */
/* Input(s)           : u2Session - Mirroring Session Number to be traversed */
/*                      u4DestNum - Destination entity Id to be searched in  */
/*                                  session database                         */
/*                                                                           */
/* Output(s)          : pu4NextDestNum - Next destination Id in the session  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS - If next Destination Id exists          */
/*                      ISS_FAILURE - If no destination exists beyond the    */
/*                                    given Destination Id for this session  */
/*****************************************************************************/
INT4
IssMirrGetNextDestRecrd (UINT4 u4SessionNo, UINT4 u4DestNum,
                         UINT4 *pu4NextDestNum)
{
    tIssMirrDestRecordInfo *pDestRecordInfo = NULL;

    ISS_TRC_FN_ENTRY ();
    /*Validate Session Id */
    if (u4SessionNo == 0 || u4SessionNo > ISS_MIRR_MAX_SESSIONS)
    {
        ISS_TRC (ISS_TRC_ALL | ISS_TRC_ALL_FAIL, "\n Invalid Session Id\n");
        ISS_TRC_FN_EXIT ();
        return ISS_FAILURE;
    }

    pDestRecordInfo =
        TMO_SLL_First (&(GET_MIRR_SESSION_DEST_HDR (u4SessionNo)));

    if (pDestRecordInfo == NULL)
    {
        ISS_TRC (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                 "\n No Destination configred in the session\n");
        ISS_TRC_FN_EXIT ();
        return ISS_FAILURE;
    }

    /*The call is made to get first destination entity Id */
    if (u4DestNum == 0)
    {
        *pu4NextDestNum = pDestRecordInfo->u4DestMinId;
        ISS_TRC_ARG1 (ISS_TRC_ALL,
                      "\n First destination id in the session = %d\n",
                      *pu4NextDestNum);
        ISS_TRC_FN_EXIT ();
        return ISS_SUCCESS;
    }

    while (pDestRecordInfo != NULL)
    {
        if (u4DestNum < pDestRecordInfo->u4DestMinId)
        {
            *pu4NextDestNum = pDestRecordInfo->u4DestMinId;
            ISS_TRC_ARG1 (ISS_TRC_ALL, "\n destination returned = %d\n",
                          *pu4NextDestNum);
            ISS_TRC_FN_EXIT ();
            return ISS_SUCCESS;
        }

        if (u4DestNum >= pDestRecordInfo->u4DestMinId &&
            u4DestNum <= pDestRecordInfo->u4DestMaxId)
        {
            if (u4DestNum == pDestRecordInfo->u4DestMaxId)
            {
                pDestRecordInfo =
                    TMO_SLL_Next (&(GET_MIRR_SESSION_DEST_HDR (u4SessionNo)),
                                  &pDestRecordInfo->NextRecordInfo);
                continue;
            }

            *pu4NextDestNum = u4DestNum + 1;
            ISS_TRC_ARG1 (ISS_TRC_ALL, "\n destination returned = %d\n",
                          *pu4NextDestNum);
            ISS_TRC_FN_EXIT ();
            return ISS_SUCCESS;
        }
        pDestRecordInfo =
            TMO_SLL_Next (&(GET_MIRR_SESSION_DEST_HDR (u4SessionNo)),
                          &pDestRecordInfo->NextRecordInfo);
    }

    *pu4NextDestNum = 0;
    ISS_TRC_ARG2 (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                  "\n next destination not found for session = %d destination = %d\n",
                  u4SessionNo, u4DestNum);
    ISS_TRC_FN_EXIT ();
    return ISS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : IssMirrAddDestRecrd                                  */
/*                                                                           */
/* Description        : This function is called to add a destination         */
/*                      Id in the mirroring database                         */
/*                                                                           */
/* Input(s)           : u2Session - Mirroring Session Number for which       */
/*                                  destination entity is to be added        */
/*                      u4DestNum - Destination entity Id to be added in     */
/*                                  session database                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS - If Destination is added successfully   */
/*                      ISS_FAILURE - If destination cannot be added in the  */
/*                                    database due to unavailibility of      */
/*                                    record entries in the system           */
/*****************************************************************************/
INT4
IssMirrAddDestRecrd (UINT4 u4SessionNo, UINT4 u4DestNum)
{
    tIssMirrDestRecordInfo *pDestRecordInfo = NULL;
    tIssMirrDestRecordInfo *pPrevRecordInfo = NULL;
    tIssMirrDestRecordInfo *pNextRecordInfo = NULL;

    ISS_TRC_FN_ENTRY ();
    /*Validate Session Id */
    if (u4SessionNo == 0 ||
        u4SessionNo > ISS_MIRR_MAX_SESSIONS || u4DestNum == 0)
    {
        ISS_TRC (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                 "\n Invalid Session Id/Destination Id\n");
        ISS_TRC_FN_EXIT ();
        return ISS_FAILURE;
    }

    /* Update L2Iwf */
    L2IwfBlockPortChannelConfig (u4DestNum, OSIX_TRUE);

    pDestRecordInfo =
        TMO_SLL_First (&(GET_MIRR_SESSION_DEST_HDR (u4SessionNo)));

    /*if no records exist for the sesion then allocate a record and 
       update for this destination entitiy Id and  ISS_TRC_FN_EXIT();
       update for this destination entitiy Id and  return */
    if (pDestRecordInfo == NULL)
    {
        ISS_MIRR_DEST_RECORD_ALLOC_MEM_BLOCK (pDestRecordInfo);

        /*Records entries exhausted in database */
        if (pDestRecordInfo == NULL)
        {
            ISS_TRC (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                     "\n Destination record allocation failure\n");
            L2IwfBlockPortChannelConfig (u4DestNum, OSIX_FALSE);
            ISS_TRC_FN_EXIT ();
            return ISS_FAILURE;
        }
        pDestRecordInfo->u4DestMinId = u4DestNum;
        pDestRecordInfo->u4DestMaxId = u4DestNum;
        TMO_SLL_Add (&(GET_MIRR_SESSION_DEST_HDR (u4SessionNo)),
                     &pDestRecordInfo->NextRecordInfo);

        ISS_TRC (ISS_TRC_ALL,
                 "\n First record entry in destination database\n");
        ISS_TRC_FN_EXIT ();
        return ISS_SUCCESS;
    }

    while (pDestRecordInfo != NULL)
    {
        if (u4DestNum < pDestRecordInfo->u4DestMinId)
        {
            if (u4DestNum == (pDestRecordInfo->u4DestMinId - 1))
            {
                pDestRecordInfo->u4DestMinId = pDestRecordInfo->u4DestMinId - 1;
            }
            else
            {
                ISS_MIRR_DEST_RECORD_ALLOC_MEM_BLOCK (pDestRecordInfo);

                if (pDestRecordInfo == NULL)
                {
                    ISS_TRC (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                             "\n Destination record allocation failure\n");
                    L2IwfBlockPortChannelConfig (u4DestNum, OSIX_FALSE);
                    ISS_TRC_FN_EXIT ();
                    return ISS_FAILURE;
                }
                pDestRecordInfo->u4DestMinId = u4DestNum;
                pDestRecordInfo->u4DestMaxId = u4DestNum;

                /*This should be the first record in case it is less than
                 * first destination in first record*/
                if (pPrevRecordInfo == NULL)
                {
                    TMO_SLL_Insert (&(GET_MIRR_SESSION_DEST_HDR (u4SessionNo)),
                                    &(GET_MIRR_SESSION_DEST_HDR (u4SessionNo).
                                      Head), &pDestRecordInfo->NextRecordInfo);
                }
                else
                {
                    TMO_SLL_Insert (&(GET_MIRR_SESSION_DEST_HDR (u4SessionNo)),
                                    &pPrevRecordInfo->NextRecordInfo,
                                    &pDestRecordInfo->NextRecordInfo);
                }
            }
            ISS_TRC (ISS_TRC_ALL, "\n Add Destination success\n");
            ISS_TRC_FN_EXIT ();
            return ISS_SUCCESS;
        }

        if (u4DestNum >= pDestRecordInfo->u4DestMinId &&
            u4DestNum <= pDestRecordInfo->u4DestMaxId)
        {
            ISS_TRC (ISS_TRC_ALL, "\n Add Destination Success\n");
            ISS_TRC_FN_EXIT ();
            return ISS_SUCCESS;
        }

        if (u4DestNum > pDestRecordInfo->u4DestMaxId)
        {
            if (u4DestNum == (pDestRecordInfo->u4DestMaxId + 1))
            {
                pDestRecordInfo->u4DestMaxId = pDestRecordInfo->u4DestMaxId + 1;

                pNextRecordInfo =
                    TMO_SLL_Next (&(GET_MIRR_SESSION_DEST_HDR
                                    (u4SessionNo)),
                                  &pDestRecordInfo->NextRecordInfo);
                if (pNextRecordInfo != NULL)
                {
                    /*Merge records and delete the next record */
                    if (u4DestNum == (pNextRecordInfo->u4DestMinId - 1))
                    {
                        pDestRecordInfo->u4DestMaxId =
                            pNextRecordInfo->u4DestMaxId;
                        TMO_SLL_Delete (&
                                        (GET_MIRR_SESSION_DEST_HDR
                                         (u4SessionNo)),
                                        &pNextRecordInfo->NextRecordInfo);
                        ISS_MIRR_RECORD_FREE_DEST_MEM_BLOCK (pNextRecordInfo);
                        ISS_TRC (ISS_TRC_ALL, "\nAdd Destination success\n");
                        ISS_TRC_FN_EXIT ();
                        return ISS_SUCCESS;
                    }
                }
                ISS_TRC (ISS_TRC_ALL, "\nAdd Destination success\n");
                ISS_TRC_FN_EXIT ();
                return ISS_SUCCESS;
            }
            else
            {
                pPrevRecordInfo = pDestRecordInfo;
                pDestRecordInfo =
                    TMO_SLL_Next (&(GET_MIRR_SESSION_DEST_HDR
                                    (u4SessionNo)),
                                  &pDestRecordInfo->NextRecordInfo);
            }
        }
    }

    /* If the destination id is greated than all the records in the 
       session then allocate and update destination in new record for this destination and exit */
    ISS_MIRR_DEST_RECORD_ALLOC_MEM_BLOCK (pDestRecordInfo);

    if (pDestRecordInfo == NULL)
    {
        ISS_TRC (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                 "\nDestination record allocation failure\n");
        L2IwfBlockPortChannelConfig (u4DestNum, OSIX_FALSE);
        ISS_TRC_FN_EXIT ();
        return ISS_FAILURE;
    }
    pDestRecordInfo->u4DestMinId = u4DestNum;
    pDestRecordInfo->u4DestMaxId = u4DestNum;
    TMO_SLL_Add (&(GET_MIRR_SESSION_DEST_HDR (u4SessionNo)),
                 &pDestRecordInfo->NextRecordInfo);

    ISS_TRC (ISS_TRC_ALL, "\nAdd Destination success\n");
    ISS_TRC_FN_EXIT ();
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssMirrRemoveDestRecrd                               */
/*                                                                           */
/* Description        : This function is called to delete a destination      */
/*                      Id in the mirroring database                         */
/*                                                                           */
/* Input(s)           : u2Session - Mirroring Session Number for which       */
/*                                  destination entity is to be added        */
/*                      u4DestNum - Destination entity Id to be removed in   */
/*                                  session database                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS - If Destination is removed successfully */
/*                      ISS_FAILURE - If destination cannot be removed in the*/
/*                                    database                               */
/*****************************************************************************/
INT4
IssMirrRemoveDestRecrd (UINT4 u4SessionNo, UINT4 u4DestNum)
{
    tIssMirrDestRecordInfo *pDestRecordInfo = NULL;
    tIssMirrDestRecordInfo *pNextRecordInfo = NULL;

    ISS_TRC_FN_ENTRY ();
    /*Validate Session Id */
    if (u4SessionNo == 0 ||
        u4SessionNo > ISS_MIRR_MAX_SESSIONS || u4DestNum == 0)
    {
        ISS_TRC (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                 "\nInvalid Session Id/Destination Id\n");
        ISS_TRC_FN_EXIT ();
        return ISS_FAILURE;
    }

    pDestRecordInfo =
        TMO_SLL_First (&(GET_MIRR_SESSION_DEST_HDR (u4SessionNo)));

    if (pDestRecordInfo == NULL)
    {
        ISS_TRC (ISS_TRC_ALL, "\nNo destination exist in session\n");
        ISS_TRC_FN_EXIT ();
        return ISS_SUCCESS;
    }

    /* Update L2Iwf */
    L2IwfBlockPortChannelConfig (u4DestNum, OSIX_FALSE);

    while (pDestRecordInfo != NULL)
    {
        if (u4DestNum < pDestRecordInfo->u4DestMinId)
        {
            ISS_TRC (ISS_TRC_ALL, "\nRemove destination record success\n");
            ISS_TRC_FN_EXIT ();
            return ISS_SUCCESS;
        }

        if (u4DestNum >= pDestRecordInfo->u4DestMinId &&
            u4DestNum <= pDestRecordInfo->u4DestMaxId)
        {
            if (pDestRecordInfo->u4DestMaxId == pDestRecordInfo->u4DestMinId)
            {
                TMO_SLL_Delete (&(GET_MIRR_SESSION_DEST_HDR (u4SessionNo)),
                                &pDestRecordInfo->NextRecordInfo);
                ISS_MIRR_RECORD_FREE_DEST_MEM_BLOCK (pDestRecordInfo);
                ISS_TRC (ISS_TRC_ALL, "\nRemove destination record success\n");
                ISS_TRC_FN_EXIT ();
                return ISS_SUCCESS;
            }

            if (u4DestNum == pDestRecordInfo->u4DestMinId)
            {
                pDestRecordInfo->u4DestMinId = pDestRecordInfo->u4DestMinId + 1;
                ISS_TRC (ISS_TRC_ALL, "\nRemove destination record success\n");
                ISS_TRC_FN_EXIT ();
                return ISS_SUCCESS;
            }

            if (u4DestNum == pDestRecordInfo->u4DestMaxId)
            {
                pDestRecordInfo->u4DestMaxId = pDestRecordInfo->u4DestMaxId - 1;
                ISS_TRC (ISS_TRC_ALL, "\nRemove destination record success\n");
                ISS_TRC_FN_EXIT ();
                return ISS_SUCCESS;
            }

            ISS_MIRR_DEST_RECORD_ALLOC_MEM_BLOCK (pNextRecordInfo);

            if (pNextRecordInfo == NULL)
            {
                ISS_TRC (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                         "\nAllocation for Destination record failure\n");
                L2IwfBlockPortChannelConfig (u4DestNum, OSIX_TRUE);
                ISS_TRC_FN_EXIT ();
                return ISS_FAILURE;
            }

            pDestRecordInfo->u4DestMaxId = pDestRecordInfo->u4DestMaxId - 1;
            pNextRecordInfo->u4DestMinId = u4DestNum;
            pNextRecordInfo->u4DestMaxId = pDestRecordInfo->u4DestMaxId;
            TMO_SLL_Insert (&(GET_MIRR_SESSION_DEST_HDR (u4SessionNo)),
                            &pDestRecordInfo->NextRecordInfo,
                            &pNextRecordInfo->NextRecordInfo);
            ISS_TRC (ISS_TRC_ALL,
                     "\nRecords split Remove destination record success\n");
            ISS_TRC_FN_EXIT ();
            return ISS_SUCCESS;
        }

        if (u4DestNum > pDestRecordInfo->u4DestMaxId)
        {
            pDestRecordInfo =
                TMO_SLL_Next (&(GET_MIRR_SESSION_DEST_HDR
                                (u4SessionNo)),
                              &pDestRecordInfo->NextRecordInfo);
        }
    }

    ISS_TRC (ISS_TRC_ALL, "\nRemove destination record success\n");
    ISS_TRC_FN_EXIT ();
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssMirrIsSrcConfigured                               */
/*                                                                           */
/* Description        : This function is called to check if any source       */
/*                      exists with this Id for the given session number     */
/*                                                                           */
/* Input(s)           : u2Session - Mirroring Session Number to be traversed */
/*                      u4SrcNum - Source entity Id to be searched in       */
/*                                  session database                         */
/*                      u4ContextId - Context Id                             */
/*                      u1MirrorType -Mirroring type                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/
INT4
IssMirrIsSrcConfigured (UINT4 u4SessionIndex, UINT4 u4ContextId,
                        UINT4 u4SrcNum, UINT1 u1MirrorType)
{
    INT4                i4CurrentMirrorType = 0;
    UINT4               u4SessionNo = 0;

    tIssMirrSrcRecordInfo *pSrcRecordInfo = NULL;
    ISS_TRC_FN_ENTRY ();
    if (u4SessionIndex > ISS_MIRR_MAX_SESSIONS ||
        u4ContextId > ISS_MAX_CONTEXTS || u4SrcNum == 0)
    {
        ISS_TRC (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                 "\nInvalid session Id/Context Id/Source Id\n");
        ISS_TRC_FN_EXIT ();
        return ISS_FAILURE;
    }

    /* Iteration to check whether any session is configured with the same
     * Dest ID  */
    for (u4SessionNo = 1; u4SessionNo <= ISS_MIRR_MAX_SESSIONS; u4SessionNo++)
    {
        /* Fetching Mirror Type of current session in loop  */
        i4CurrentMirrorType =
            GET_MIRR_SESSION_INFO ((UINT2) u4SessionNo)->u1MirroringType;

        /* Fetching source record header for the current session in loop */
        pSrcRecordInfo =
            TMO_SLL_First (&(GET_MIRR_SESSION_SRC_HDR (u4SessionNo)));

        while (pSrcRecordInfo != NULL)
        {
            if (u4ContextId == pSrcRecordInfo->u4ContextId)
            {
                if ((u4SrcNum >= pSrcRecordInfo->u4SrcMinId) &&
                    (u4SrcNum <= pSrcRecordInfo->u4SrcMaxId) &&
                    (u1MirrorType == i4CurrentMirrorType))
                {
                    ISS_TRC_ARG1 (ISS_TRC_ALL,
                                  "\nSource found in session = %d\n",
                                  u4SessionNo);
                    ISS_TRC_FN_EXIT ();
                    return ISS_SUCCESS;
                }
            }
            pSrcRecordInfo =
                TMO_SLL_Next (&(GET_MIRR_SESSION_SRC_HDR (u4SessionNo)),
                              &pSrcRecordInfo->NextRecordInfo);
        }
    }

    ISS_TRC_ARG1 (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                  "\nSource not found in session = %d\n", u4SessionIndex);
    ISS_TRC_FN_EXIT ();
    return ISS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : IssMirrGetSrcRecrdInfo                               */
/*                                                                           */
/* Description        : This function is called to get source information    */
/*                      from mirroring database                              */
/*                                                                           */
/* Input(s)           : u2Session - Mirroring Session Number to be traversed */
/*                      u4ContextId - Context Id to which the source belongs */
/*                      u4SrcNum - Source entity Id to get in session        */
/*                                 database                                  */
/*                                                                           */
/* Output(s)          : pu1Mode - Mode configured for the source             */
/*                      pu1ControlStatus - Control status of the source      */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/
INT4
IssMirrGetSrcRecrdInfo (UINT4 u4SessionNo, UINT4 u4ContextId, UINT4 u4SrcNum,
                        UINT1 *pu1Mode, UINT1 *pu1ControlStatus)
{
    tIssMirrSrcRecordInfo *pSrcRecordInfo = NULL;

    ISS_TRC_FN_ENTRY ();
    if (u4SessionNo == 0 ||
        u4SessionNo > ISS_MIRR_MAX_SESSIONS ||
        u4ContextId > ISS_MAX_CONTEXTS || u4SrcNum == 0)
    {
        ISS_TRC (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                 "\nInvalid session Id/Context Id/Source Id\n");
        ISS_TRC_FN_EXIT ();
        return ISS_FAILURE;
    }

    pSrcRecordInfo = TMO_SLL_First (&(GET_MIRR_SESSION_SRC_HDR (u4SessionNo)));

    if (pSrcRecordInfo == NULL)
    {
        ISS_TRC_ARG1 (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                      "\nNo source present in session = %d\n", u4SessionNo);
        ISS_TRC_FN_EXIT ();
        return ISS_FAILURE;
    }

    while (pSrcRecordInfo != NULL)
    {
        if (u4ContextId == pSrcRecordInfo->u4ContextId)
        {
            if (u4SrcNum >= pSrcRecordInfo->u4SrcMinId &&
                u4SrcNum <= pSrcRecordInfo->u4SrcMaxId)
            {
                *pu1Mode = pSrcRecordInfo->u1SessionMode;
                *pu1ControlStatus = pSrcRecordInfo->u1ControlStatus;
                ISS_TRC_ARG1 (ISS_TRC_ALL, "\nsource present in session = %d\n",
                              u4SessionNo);
                ISS_TRC_FN_EXIT ();

                return ISS_SUCCESS;
            }
        }
        pSrcRecordInfo =
            TMO_SLL_Next (&(GET_MIRR_SESSION_SRC_HDR (u4SessionNo)),
                          &pSrcRecordInfo->NextRecordInfo);
    }

    ISS_TRC_ARG1 (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                  "\nsource not present in session = %d\n", u4SessionNo);
    ISS_TRC_FN_EXIT ();
    return ISS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : IssMirrGetNextSrcRecrd                            */
/*                                                                           */
/* Description        : This function is called to get next source           */
/*                      Id for the given session number                      */
/*                                                                           */
/* Input(s)           : u2Session - Mirroring Session Number to be traversed */
/*                      u4ContextId - Context Id to which the source belongs */
/*                      u4SrcNum - Source entity Id to be searched in        */
/*                                  session database                         */
/*                                                                           */
/* Output(s)          : pu4NextContextNum - Context Id next source found     */
/*                      pu4NextSrcNum - Next source Id in the session        */
/*                      pu1Mode - Mode configured for Next Source found      */
/*                      pu1ControlStatus - Control status configured for     */
/*                                         source found                      */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS - If next source Id exists               */
/*                      ISS_FAILURE - If no source exists beyond the         */
/*                                    given source Id for this session       */
/*****************************************************************************/
INT4
IssMirrGetNextSrcRecrd (UINT4 u4SessionNo, UINT4 u4ContextId, UINT4 u4SrcNum,
                        UINT4 *pu4NextContextNum, UINT4 *pu4NextSrcNum,
                        UINT1 *pu1Mode, UINT1 *pu1ControlStatus)
{
    tIssMirrSrcRecordInfo *pSrcRecordInfo = NULL;

    ISS_TRC_FN_ENTRY ();
    if (u4SessionNo == 0 || u4SessionNo > ISS_MIRR_MAX_SESSIONS)
    {
        ISS_TRC (ISS_TRC_ALL | ISS_TRC_ALL_FAIL, "\nInvalid Session Id\n");
        ISS_TRC_FN_EXIT ();
        return ISS_FAILURE;
    }

    pSrcRecordInfo = TMO_SLL_First (&(GET_MIRR_SESSION_SRC_HDR (u4SessionNo)));

    /*No source entity present in this session */
    if (pSrcRecordInfo == NULL)
    {
        ISS_TRC_ARG1 (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                      "\nNo source present in session = %d\n", u4SessionNo);
        ISS_TRC_FN_EXIT ();
        return ISS_FAILURE;
    }

    /*Call made to get first source entity in the source database */
    if (u4ContextId == 0 && u4SrcNum == 0)
    {
        *pu4NextContextNum = pSrcRecordInfo->u4ContextId;
        *pu4NextSrcNum = pSrcRecordInfo->u4SrcMinId;
        *pu1Mode = pSrcRecordInfo->u1SessionMode;
        *pu1ControlStatus = pSrcRecordInfo->u1ControlStatus;

        ISS_TRC_ARG2 (ISS_TRC_ALL,
                      "\nFirst source returned = %d for session = %d\n",
                      *pu4NextSrcNum, u4SessionNo);
        ISS_TRC_FN_EXIT ();
        return ISS_SUCCESS;
    }

    /*Traverse all the source records in the session */
    while (pSrcRecordInfo != NULL)
    {
        if (u4ContextId < pSrcRecordInfo->u4ContextId)
        {
            *pu4NextContextNum = pSrcRecordInfo->u4ContextId;
            *pu4NextSrcNum = pSrcRecordInfo->u4SrcMinId;
            *pu1Mode = pSrcRecordInfo->u1SessionMode;
            *pu1ControlStatus = pSrcRecordInfo->u1ControlStatus;
            ISS_TRC_ARG2 (ISS_TRC_ALL,
                          "\n source returned = %d for session = %d\n",
                          *pu4NextSrcNum, u4SessionNo);
            ISS_TRC_FN_EXIT ();
            return ISS_SUCCESS;

        }
        else if (u4ContextId == pSrcRecordInfo->u4ContextId)
        {
            if (u4SrcNum < pSrcRecordInfo->u4SrcMinId)
            {
                *pu4NextContextNum = u4ContextId;
                *pu4NextSrcNum = pSrcRecordInfo->u4SrcMinId;
                *pu1Mode = pSrcRecordInfo->u1SessionMode;
                *pu1ControlStatus = pSrcRecordInfo->u1ControlStatus;
                ISS_TRC_ARG2 (ISS_TRC_ALL,
                              "\n source returned = %d for session = %d\n",
                              *pu4NextSrcNum, u4SessionNo);
                ISS_TRC_FN_EXIT ();
                return ISS_SUCCESS;
            }

            if (u4SrcNum >= pSrcRecordInfo->u4SrcMinId &&
                u4SrcNum <= pSrcRecordInfo->u4SrcMaxId)
            {
                if (u4SrcNum == pSrcRecordInfo->u4SrcMaxId)
                {
                    pSrcRecordInfo =
                        TMO_SLL_Next (&(GET_MIRR_SESSION_SRC_HDR (u4SessionNo)),
                                      &pSrcRecordInfo->NextRecordInfo);
                    continue;
                }
                *pu4NextContextNum = u4ContextId;
                *pu4NextSrcNum = u4SrcNum + 1;
                *pu1Mode = pSrcRecordInfo->u1SessionMode;
                *pu1ControlStatus = pSrcRecordInfo->u1ControlStatus;
                ISS_TRC_ARG2 (ISS_TRC_ALL,
                              "\nsource returned = %d for session = %d\n",
                              *pu4NextSrcNum, u4SessionNo);
                ISS_TRC_FN_EXIT ();
                return ISS_SUCCESS;
            }
        }
        pSrcRecordInfo =
            TMO_SLL_Next (&(GET_MIRR_SESSION_SRC_HDR (u4SessionNo)),
                          &pSrcRecordInfo->NextRecordInfo);
    }
    ISS_TRC_ARG2 (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                  "\nNext source not found for session = %d source= %d \n",
                  u4SessionNo, u4SrcNum);
    ISS_TRC_FN_EXIT ();
    return ISS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : IssMirrAddSrcRecrd                                   */
/*                                                                           */
/* Description        : This function is called to add a source              */
/*                      Id in the mirroring database                         */
/*                                                                           */
/* Input(s)           : u2Session - Mirroring Session Number for which       */
/*                                  destination entity is to be added        */
/*                      u4ContextId - Context Id to which the source belongs */
/*                      u4SrcNum - Destination entity Id to be added in      */
/*                                  session database                         */
/*                      u1Mode - Mirroring Mode for source                   */
/*                      u1ControlStatus - Control status for source          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS - If Source is added successfully        */
/*                      ISS_FAILURE - If Source cannot be added in the       */
/*                                    database due to unavailibility of      */
/*                                    record entries in the system           */
/*****************************************************************************/
INT4
IssMirrAddSrcRecrd (UINT4 u4SessionNo, UINT4 u4ContextId, UINT4 u4SrcNum,
                    UINT1 u1Mode, UINT1 u1ControlStatus)
{
    tIssMirrSrcRecordInfo *pSrcRecordInfo = NULL;
    tIssMirrSrcRecordInfo *pPrevRecordInfo = NULL;
    tIssMirrSrcRecordInfo *pNextRecordInfo = NULL;

    ISS_TRC_FN_ENTRY ();
    if ((u4SessionNo == 0) ||
        (u4SessionNo > ISS_MIRR_MAX_SESSIONS) ||
        (u4ContextId > ISS_MAX_CONTEXTS) || (u4SrcNum == 0) ||
        ((u1Mode < ISS_MIRR_INGRESS) || (u1Mode > ISS_MIRR_BOTH)) ||
        ((u1ControlStatus < ISS_VALID) || (u1ControlStatus > ISS_INVALID)))
    {
        ISS_TRC (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                 "\nInvalid Session Id/Context Id/Source Id/Mode/Control Status\n");
        ISS_TRC_FN_EXIT ();
        return ISS_FAILURE;
    }

    /*First Remove source entity from the list of sources */
    if (IssMirrRemoveSrcRecrd (u4SessionNo, u4ContextId, u4SrcNum) ==
        ISS_FAILURE)
    {
        ISS_TRC (ISS_TRC_ALL | ISS_TRC_ALL_FAIL, "\nRemove record failure\n");
        ISS_TRC_FN_EXIT ();
        return ISS_FAILURE;
    }

    /* Update L2Iwf */
    if (GET_MIRR_SESSION_INFO (u4SessionNo)->u1MirroringType ==
        ISS_MIRR_PORT_BASED)
    {
        L2IwfBlockPortChannelConfig (u4SrcNum, (UINT1) OSIX_TRUE);
    }

    pSrcRecordInfo = TMO_SLL_First (&(GET_MIRR_SESSION_SRC_HDR (u4SessionNo)));

    if (pSrcRecordInfo == NULL)
    {
        ISS_MIRR_SRC_RECORD_ALLOC_MEM_BLOCK (pSrcRecordInfo);

        if (pSrcRecordInfo == NULL)
        {
            ISS_TRC (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                     "\nMemory Allocation for Source record failure\n");

            if (GET_MIRR_SESSION_INFO (u4SessionNo)->u1MirroringType ==
                ISS_MIRR_PORT_BASED)
            {
                L2IwfBlockPortChannelConfig (u4SrcNum, (UINT1) OSIX_FALSE);
            }
            ISS_TRC_FN_EXIT ();
            return ISS_FAILURE;
        }

        pSrcRecordInfo->u4ContextId = u4ContextId;
        pSrcRecordInfo->u4SrcMinId = u4SrcNum;
        pSrcRecordInfo->u4SrcMaxId = u4SrcNum;
        pSrcRecordInfo->u1SessionMode = u1Mode;
        pSrcRecordInfo->u1ControlStatus = u1ControlStatus;
        TMO_SLL_Add (&(GET_MIRR_SESSION_SRC_HDR (u4SessionNo)),
                     &pSrcRecordInfo->NextRecordInfo);

        ISS_TRC (ISS_TRC_ALL, "\nSource addition success\n");
        ISS_TRC_FN_EXIT ();
        return ISS_SUCCESS;
    }

    while (pSrcRecordInfo != NULL)
    {
        if (u4ContextId < pSrcRecordInfo->u4ContextId)
        {
            ISS_MIRR_SRC_RECORD_ALLOC_MEM_BLOCK (pSrcRecordInfo);

            if (pSrcRecordInfo == NULL)
            {
                ISS_TRC (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                         "\nMemory Allocation for Source record failure\n");

                if (GET_MIRR_SESSION_INFO (u4SessionNo)->u1MirroringType ==
                    ISS_MIRR_PORT_BASED)
                {
                    L2IwfBlockPortChannelConfig (u4SrcNum, (UINT1) OSIX_FALSE);
                }
                ISS_TRC_FN_EXIT ();
                return ISS_FAILURE;
            }
            pSrcRecordInfo->u4ContextId = u4ContextId;
            pSrcRecordInfo->u4SrcMinId = u4SrcNum;
            pSrcRecordInfo->u4SrcMaxId = u4SrcNum;
            pSrcRecordInfo->u1SessionMode = u1Mode;
            pSrcRecordInfo->u1ControlStatus = u1ControlStatus;

            /*This should be the first record in case it is less than
             * first source in first record*/
            if (pPrevRecordInfo == NULL)
            {
                TMO_SLL_Insert (&(GET_MIRR_SESSION_SRC_HDR (u4SessionNo)),
                                &(GET_MIRR_SESSION_SRC_HDR (u4SessionNo).Head),
                                &pSrcRecordInfo->NextRecordInfo);
            }
            else
            {
                TMO_SLL_Insert (&(GET_MIRR_SESSION_SRC_HDR (u4SessionNo)),
                                &pPrevRecordInfo->NextRecordInfo,
                                &pSrcRecordInfo->NextRecordInfo);
            }

            ISS_TRC (ISS_TRC_ALL, "\nSource addition success\n");
            ISS_TRC_FN_EXIT ();
            return ISS_SUCCESS;
        }
        else if (u4ContextId == pSrcRecordInfo->u4ContextId)
        {
            if (u4SrcNum < pSrcRecordInfo->u4SrcMinId)
            {
                if ((u4SrcNum == (pSrcRecordInfo->u4SrcMinId - 1)) &&
                    (u1Mode == pSrcRecordInfo->u1SessionMode) &&
                    (u1ControlStatus == pSrcRecordInfo->u1ControlStatus))
                {
                    pSrcRecordInfo->u4SrcMinId = pSrcRecordInfo->u4SrcMinId - 1;
                }
                else
                {
                    ISS_MIRR_SRC_RECORD_ALLOC_MEM_BLOCK (pSrcRecordInfo);

                    if (pSrcRecordInfo == NULL)
                    {
                        ISS_TRC (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                                 "\nMemory Allocation for Source record failure\n");

                        if (GET_MIRR_SESSION_INFO (u4SessionNo)->
                            u1MirroringType == ISS_MIRR_PORT_BASED)
                        {
                            L2IwfBlockPortChannelConfig (u4SrcNum,
                                                         (UINT1) OSIX_FALSE);
                        }
                        ISS_TRC_FN_EXIT ();
                        return ISS_FAILURE;
                    }
                    pSrcRecordInfo->u4ContextId = u4ContextId;
                    pSrcRecordInfo->u4SrcMinId = u4SrcNum;
                    pSrcRecordInfo->u4SrcMaxId = u4SrcNum;
                    pSrcRecordInfo->u1SessionMode = u1Mode;
                    pSrcRecordInfo->u1ControlStatus = u1ControlStatus;

                    /*This should be the first record in case it is less than
                     * first source in first record*/
                    if (pPrevRecordInfo == NULL)
                    {
                        TMO_SLL_Insert (&
                                        (GET_MIRR_SESSION_SRC_HDR
                                         (u4SessionNo)),
                                        &(GET_MIRR_SESSION_SRC_HDR
                                          (u4SessionNo).Head),
                                        &pSrcRecordInfo->NextRecordInfo);
                    }
                    else
                    {
                        TMO_SLL_Insert (&
                                        (GET_MIRR_SESSION_SRC_HDR
                                         (u4SessionNo)),
                                        &pPrevRecordInfo->NextRecordInfo,
                                        &pSrcRecordInfo->NextRecordInfo);
                    }

                }
                ISS_TRC (ISS_TRC_ALL, "\nSource addition success\n");
                ISS_TRC_FN_EXIT ();
                return ISS_SUCCESS;
            }

            if (u4SrcNum > pSrcRecordInfo->u4SrcMaxId)
            {
                if (u4SrcNum == (pSrcRecordInfo->u4SrcMaxId + 1) &&
                    (u1Mode == pSrcRecordInfo->u1SessionMode) &&
                    (u1ControlStatus == pSrcRecordInfo->u1ControlStatus))
                {
                    pSrcRecordInfo->u4SrcMaxId = pSrcRecordInfo->u4SrcMaxId + 1;

                    pNextRecordInfo =
                        TMO_SLL_Next (&(GET_MIRR_SESSION_SRC_HDR (u4SessionNo)),
                                      &pSrcRecordInfo->NextRecordInfo);
                    if (pNextRecordInfo != NULL)
                    {
                        if (u4ContextId == pNextRecordInfo->u4ContextId)
                        {
                            /*Merge records */
                            if (u4SrcNum == (pNextRecordInfo->u4SrcMinId - 1) &&
                                (u1Mode == pNextRecordInfo->u1SessionMode) &&
                                (u1ControlStatus ==
                                 pNextRecordInfo->u1ControlStatus))
                            {
                                pSrcRecordInfo->u4SrcMaxId =
                                    pNextRecordInfo->u4SrcMaxId;
                                TMO_SLL_Delete (&
                                                (GET_MIRR_SESSION_SRC_HDR
                                                 (u4SessionNo)),
                                                &pNextRecordInfo->
                                                NextRecordInfo);
                                ISS_MIRR_RECORD_FREE_SRC_MEM_BLOCK
                                    (pNextRecordInfo);
                                ISS_TRC (ISS_TRC_ALL,
                                         "\nRecords Merged Source addition success\n");
                                ISS_TRC_FN_EXIT ();
                                return ISS_SUCCESS;
                            }
                        }
                    }
                    ISS_TRC (ISS_TRC_ALL, "\nSource addition success\n");
                    ISS_TRC_FN_EXIT ();
                    return ISS_SUCCESS;
                }
                else
                {
                    pPrevRecordInfo = pSrcRecordInfo;
                    pSrcRecordInfo =
                        TMO_SLL_Next (&(GET_MIRR_SESSION_SRC_HDR (u4SessionNo)),
                                      &pSrcRecordInfo->NextRecordInfo);
                    continue;
                }
            }
        }
        pPrevRecordInfo = pSrcRecordInfo;
        pSrcRecordInfo =
            TMO_SLL_Next (&(GET_MIRR_SESSION_SRC_HDR (u4SessionNo)),
                          &pSrcRecordInfo->NextRecordInfo);
    }

    /* If the source is not accomodate in any recored then allocate new 
       record and append to the list */
    ISS_MIRR_SRC_RECORD_ALLOC_MEM_BLOCK (pSrcRecordInfo);

    if (pSrcRecordInfo == NULL)
    {
        ISS_TRC (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                 "\nMemory Allocation for Source record failure\n");

        if (GET_MIRR_SESSION_INFO (u4SessionNo)->u1MirroringType ==
            ISS_MIRR_PORT_BASED)
        {
            L2IwfBlockPortChannelConfig (u4SrcNum, (UINT1) OSIX_FALSE);
        }
        ISS_TRC_FN_EXIT ();
        return ISS_FAILURE;
    }
    pSrcRecordInfo->u4ContextId = u4ContextId;
    pSrcRecordInfo->u4SrcMinId = u4SrcNum;
    pSrcRecordInfo->u4SrcMaxId = u4SrcNum;
    pSrcRecordInfo->u1SessionMode = u1Mode;
    pSrcRecordInfo->u1ControlStatus = u1ControlStatus;
    TMO_SLL_Add (&(GET_MIRR_SESSION_SRC_HDR (u4SessionNo)),
                 &pSrcRecordInfo->NextRecordInfo);
    ISS_TRC (ISS_TRC_ALL, "\nSource addition success\n");
    ISS_TRC_FN_EXIT ();
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssMirrRemoveSrcRecrd                                */
/*                                                                           */
/* Description        : This function is called to delete a source           */
/*                      Id in the mirroring database                         */
/*                                                                           */
/* Input(s)           : u2Session - Mirroring Session Number for which       */
/*                                  destination entity is to be added        */
/*                      u4ContextId - Context Id to which the source belongs */
/*                      u4DestNum - source entity Id to be removed in        */
/*                                  session database                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS - If Source is removed successfully      */
/*                      ISS_FAILURE - If Source cannot be removed in the     */
/*                                    database                               */
/*****************************************************************************/
INT4
IssMirrRemoveSrcRecrd (UINT4 u4SessionNo, UINT4 u4ContextId, UINT4 u4SrcNum)
{
    tIssMirrSrcRecordInfo *pSrcRecordInfo = NULL;
    tIssMirrSrcRecordInfo *pNextRecordInfo = NULL;

    ISS_TRC_FN_ENTRY ();
    if ((u4SessionNo == 0) ||
        (u4SessionNo > ISS_MIRR_MAX_SESSIONS) ||
        (u4ContextId > ISS_MAX_CONTEXTS) || (u4SrcNum == 0))
    {
        ISS_TRC (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                 "\nInvalid Session Id/Context Id/Source Id\n");
        ISS_TRC_FN_EXIT ();
        return ISS_FAILURE;
    }

    pSrcRecordInfo = TMO_SLL_First (&(GET_MIRR_SESSION_SRC_HDR (u4SessionNo)));

    /*No source entities exist for this Session */
    if (pSrcRecordInfo == NULL)
    {
        ISS_TRC (ISS_TRC_ALL, "\nNo source record present in session\n");
        ISS_TRC_FN_EXIT ();
        return ISS_SUCCESS;
    }

    /* Update L2Iwf */
    if (GET_MIRR_SESSION_INFO (u4SessionNo)->u1MirroringType ==
        ISS_MIRR_PORT_BASED)
    {
        L2IwfBlockPortChannelConfig (u4SrcNum, (UINT1) OSIX_FALSE);
    }

    while (pSrcRecordInfo != NULL)
    {
        if (u4ContextId == pSrcRecordInfo->u4ContextId)
        {
            if (u4SrcNum < pSrcRecordInfo->u4SrcMinId)
            {
                ISS_TRC (ISS_TRC_ALL, "\nremove source success\n");
                ISS_TRC_FN_EXIT ();
                return ISS_SUCCESS;
            }

            if (u4SrcNum >= pSrcRecordInfo->u4SrcMinId &&
                u4SrcNum <= pSrcRecordInfo->u4SrcMaxId)
            {
                if (pSrcRecordInfo->u4SrcMaxId == pSrcRecordInfo->u4SrcMinId)
                {
                    TMO_SLL_Delete (&(GET_MIRR_SESSION_SRC_HDR (u4SessionNo)),
                                    &pSrcRecordInfo->NextRecordInfo);
                    ISS_MIRR_RECORD_FREE_SRC_MEM_BLOCK (pSrcRecordInfo);
                    ISS_TRC (ISS_TRC_ALL, "\nremove source success\n");
                    ISS_TRC_FN_EXIT ();
                    return ISS_SUCCESS;
                }

                if (u4SrcNum == pSrcRecordInfo->u4SrcMinId)
                {
                    pSrcRecordInfo->u4SrcMinId = pSrcRecordInfo->u4SrcMinId + 1;
                    ISS_TRC (ISS_TRC_ALL, "\nremove source success\n");
                    ISS_TRC_FN_EXIT ();
                    return ISS_SUCCESS;
                }

                if (u4SrcNum == pSrcRecordInfo->u4SrcMaxId)
                {
                    pSrcRecordInfo->u4SrcMaxId = pSrcRecordInfo->u4SrcMaxId - 1;
                    ISS_TRC (ISS_TRC_ALL, "\nremove source success\n");
                    ISS_TRC_FN_EXIT ();
                    return ISS_SUCCESS;
                }

                ISS_MIRR_SRC_RECORD_ALLOC_MEM_BLOCK (pNextRecordInfo);

                if (pNextRecordInfo == NULL)
                {
                    ISS_TRC (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                             "\nMemory allocation for source record failure\n");
                    if (GET_MIRR_SESSION_INFO (u4SessionNo)->u1MirroringType ==
                        ISS_MIRR_PORT_BASED)
                    {
                        L2IwfBlockPortChannelConfig (u4SrcNum,
                                                     (UINT1) OSIX_TRUE);
                    }
                    ISS_TRC_FN_EXIT ();
                    return ISS_FAILURE;
                }

                pNextRecordInfo->u4SrcMaxId = pSrcRecordInfo->u4SrcMaxId;
                pNextRecordInfo->u4SrcMinId = u4SrcNum + 1;
                pSrcRecordInfo->u4SrcMaxId = u4SrcNum - 1;
                pNextRecordInfo->u1SessionMode = pSrcRecordInfo->u1SessionMode;
                pNextRecordInfo->u4ContextId = pSrcRecordInfo->u4ContextId;
                pNextRecordInfo->u1ControlStatus =
                    pSrcRecordInfo->u1ControlStatus;

                TMO_SLL_Insert (&(GET_MIRR_SESSION_SRC_HDR (u4SessionNo)),
                                &pSrcRecordInfo->NextRecordInfo,
                                &pNextRecordInfo->NextRecordInfo);

                ISS_TRC (ISS_TRC_ALL, "\nremove source success\n");
                ISS_TRC_FN_EXIT ();
                return ISS_SUCCESS;
            }
        }
        pSrcRecordInfo =
            TMO_SLL_Next (&(GET_MIRR_SESSION_SRC_HDR (u4SessionNo)),
                          &pSrcRecordInfo->NextRecordInfo);
    }

    ISS_TRC (ISS_TRC_ALL, "\nremove source success\n");
    ISS_TRC_FN_EXIT ();
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssMirrConfigHw                                      */
/*                                                                           */
/* Description        : This function is called to Configure/delete session  */
/*                      information from the hardware. It takes care of      */
/*                      configuring entities(source/destination) which are   */
/*                      active in the system Mirroring NPAPI will be called  */
/*                      multiple times in case complete session information  */
/*                      is not accomodated in one NPAPI call.                */
/*                                                                           */
/* Input(s)           : u2Session - Mirroring Session Number for which       */
/*                                  Hardware should be programmed            */
/*                      u1ConfigMode - Takes ISS_MIRR_ADD/ISS_MIRR_REMOVE    */
/*                                     ISS_MIRR_ADD - Configures Session     */
/*                                      configurations in hardware           */
/*                                     ISS_MIRR_DELETE - Removes Session     */
/*                                      configurations in hardware           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/
INT4
IssMirrConfigHw (UINT2 u2SessionNo, UINT1 u1ConfigMode)
{
#ifdef NPAPI_WANTED
    tIssMirrSessionInfo *pSessionInfo = NULL;
    tIssHwMirrorInfo    HwMirrorInfo;
    tIssL2FilterEntry  *pIssL2Filter = NULL;
    tIssL3FilterEntry  *pIssL3Filter = NULL;
    tCfaIfInfo          IfInfo;
    UINT4               u4LastDestConf = 0;
    UINT4               u4NextDestNum = 0;
    UINT4               u4LastSrcConf = 0;
    UINT4               u4NextSrcNum = 0;
    UINT4               u4LastContextId = 0;
    UINT4               u4NextContextNum = 0;
    INT4                i4RetVal = 0;
    UINT1               u1DestCurrentStatus = ISS_MIRR_ADD_IN_PROGRESS;
    UINT1               u1SrcCurrentStatus = ISS_MIRR_ADD_IN_PROGRESS;
    UINT1               u1Mode = 0;
    UINT1               u1ControlStatus = 0;
    UINT1               u1DestFilled = 0;
    UINT1               u1SrcFilled = 0;

    ISS_TRC_FN_ENTRY ();
    ISS_MEMSET (&HwMirrorInfo, 0, sizeof (tIssHwMirrorInfo));
    /*Traverse untill all the source and destination entities in the session
       are configure in hardware */
    while (u1DestCurrentStatus != ISS_MIRR_ADD_DONE ||
           u1SrcCurrentStatus != ISS_MIRR_ADD_DONE)
    {
        pSessionInfo = GET_MIRR_SESSION_INFO (u2SessionNo);
        u1DestFilled = 0;
        u1SrcFilled = 0;

        /* Still some Destination entities exist in system to be 
           configured in hardware */
        if (u1DestCurrentStatus != ISS_MIRR_ADD_DONE)
        {
            /*Fill hardware info with maximum destination ids that can 
               be accomodated */
            while (u1DestFilled < ISS_MIRR_HW_DEST_RECD)
            {

                if (IssMirrGetNextDestRecrd (u2SessionNo,
                                             u4LastDestConf, &u4NextDestNum)
                    == ISS_FAILURE)
                {
                    u1DestCurrentStatus = ISS_MIRR_ADD_DONE;
                    break;
                }

                if (CfaGetIfInfo (u4NextDestNum, &IfInfo) == CFA_SUCCESS)
                {
                    HwMirrorInfo.u4DestList[u1DestFilled] = u4NextDestNum;
                    u1DestFilled = u1DestFilled + 1;
                }

                u4LastDestConf = u4NextDestNum;
            }
        }

        /* Still some source entities exist in system to be 
           configured in hardware */
        if (u1SrcCurrentStatus != ISS_MIRR_ADD_DONE)
        {

            /*Fill hardware info with maximum source ids that can 
               be accomodated */
            while (u1SrcFilled < ISS_MIRR_HW_SRC_RECD)
            {

                if (IssMirrGetNextSrcRecrd (u2SessionNo,
                                            u4LastContextId,
                                            u4LastSrcConf,
                                            &u4NextContextNum,
                                            &u4NextSrcNum,
                                            &u1Mode,
                                            &u1ControlStatus) == ISS_FAILURE)
                {
                    u1SrcCurrentStatus = ISS_MIRR_ADD_DONE;
                    break;
                }

                if (u1ControlStatus == ISS_VALID)
                {
                    if (pSessionInfo->u1MirroringType == ISS_MIRR_PORT_BASED)
                    {
                        if (CfaGetIfInfo (u4NextSrcNum, &IfInfo) == CFA_SUCCESS)
                        {
                            HwMirrorInfo.SourceList[u1SrcFilled].u4SourceNo =
                                u4NextSrcNum;
                            HwMirrorInfo.SourceList[u1SrcFilled].u4ContextId =
                                u4NextContextNum;
                            HwMirrorInfo.SourceList[u1SrcFilled].u1Mode =
                                u1Mode;

                            u1SrcFilled = u1SrcFilled + 1;
                        }
                    }
                    else if (pSessionInfo->u1MirroringType ==
                             ISS_MIRR_VLAN_BASED)
                    {
                        if (IssL2IwfMiIsVlanActive
                            (u4NextContextNum, u4NextSrcNum) == OSIX_TRUE)
                        {
                            HwMirrorInfo.SourceList[u1SrcFilled].u4SourceNo =
                                u4NextSrcNum;
                            HwMirrorInfo.SourceList[u1SrcFilled].u4ContextId =
                                u4NextContextNum;
                            HwMirrorInfo.SourceList[u1SrcFilled].u1Mode =
                                u1Mode;

                            u1SrcFilled = u1SrcFilled + 1;
                        }
                    }
                    else if (pSessionInfo->u1MirroringType ==
                             ISS_MIRR_IP_FLOW_BASED)
                    {
                        /* Call routine to validate filter is present in the system */
                        if ((pIssL3Filter =
                             IssExtGetL3FilterEntry (u4NextSrcNum)) != NULL)
                        {
                            if (pIssL3Filter->u1IssL3FilterStatus ==
                                ISS_ROW_STATUS_ACTIVE)
                            {
                                HwMirrorInfo.SourceList[u1SrcFilled].
                                    u4SourceNo = u4NextSrcNum;
                                HwMirrorInfo.SourceList[u1SrcFilled].
                                    u4ContextId = u4NextContextNum;
                                HwMirrorInfo.SourceList[u1SrcFilled].
                                    u1Mode = u1Mode;
                                HwMirrorInfo.SourceList[u1SrcFilled].
                                    u1FilterDirection =
                                    (UINT1) (pIssL3Filter->
                                             IssL3FilterDirection);

                                u1SrcFilled = u1SrcFilled + 1;
                            }
                        }
                    }
                    else if (pSessionInfo->u1MirroringType ==
                             ISS_MIRR_MAC_FLOW_BASED)
                    {
                        /* Call routine to validate filter is present in the system */
                        if ((pIssL2Filter =
                             IssExtGetL2FilterEntry (u4NextSrcNum)) != NULL)
                        {
                            if (pIssL2Filter->u1IssL2FilterStatus ==
                                ISS_ROW_STATUS_ACTIVE)
                            {
                                HwMirrorInfo.SourceList[u1SrcFilled].
                                    u4SourceNo = u4NextSrcNum;
                                HwMirrorInfo.SourceList[u1SrcFilled].
                                    u4ContextId = u4NextContextNum;
                                HwMirrorInfo.SourceList[u1SrcFilled].
                                    u1Mode = u1Mode;
                                HwMirrorInfo.SourceList[u1SrcFilled].
                                    u1FilterDirection =
                                    (UINT1) (pIssL2Filter->u1FilterDirection);

                                u1SrcFilled = u1SrcFilled + 1;
                            }
                        }
                    }
                }

                u4LastSrcConf = u4NextSrcNum;
                u4LastContextId = u4NextContextNum;
            }

        }

        HwMirrorInfo.u4RSpanContextId = pSessionInfo->u4RSpanContextId;
        HwMirrorInfo.u2RSpanVlanId = pSessionInfo->RSpanVlanId;
        HwMirrorInfo.u2SessionId = u2SessionNo;
        HwMirrorInfo.u1MirrType = pSessionInfo->u1MirroringType;
        HwMirrorInfo.u1RSpanSessionType = pSessionInfo->u1RSpanSessionType;

        /*All the source and destination entries are configured in hardware 
           stored in Hardware info structure */
        if (u1DestCurrentStatus == ISS_MIRR_ADD_DONE &&
            u1SrcCurrentStatus == ISS_MIRR_ADD_DONE)
        {
            /* Set action as Add in case configuration call is to Add */
            if (u1ConfigMode == ISS_MIRR_ADD)
            {
                HwMirrorInfo.u1Action = ISS_MIRR_ADD_DONE;
            }
            /* Set action as remove in case configuration call is to remove 
               session information from the hardware */
            else
            {
                HwMirrorInfo.u1Action = ISS_MIRR_REMOVE_DONE;
            }
        }
        /*Still some source/destination entities remain in session to 
           be configured in hardware */
        else
        {
            if (u1ConfigMode == ISS_MIRR_ADD)
            {
                HwMirrorInfo.u1Action = ISS_MIRR_ADD_IN_PROGRESS;
            }
            else
            {
                HwMirrorInfo.u1Action = ISS_MIRR_REMOVE_IN_PROGRESS;
            }
        }
        if (GET_MIRR_SESSION_INFO (u2SessionNo)->u1SessionStatus
            == ISS_ROW_STATUS_DESTROY)
        {
            return ISS_FAILURE;
        }
        if ((u1SrcFilled != 0) && (u1DestFilled != 0))
        {
            if ((i4RetVal =
                 IsssysIssHwSetMirroring (&HwMirrorInfo)) != FNP_SUCCESS)
            {
                ISS_TRC (ISS_TRC_ALL | ISS_TRC_ALL_FAIL,
                         "\nIssHwSetMirroring returned failure\n");
                ISS_TRC_FN_EXIT ();
                return i4RetVal;
            }
        }
    }
    ISS_TRC_FN_EXIT ();
#else
    UNUSED_PARAM (u2SessionNo);
    UNUSED_PARAM (u1ConfigMode);
#endif

    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssMirrDelDefaultSessionDB                           */
/*                                                                           */
/* Description        : This function is called to delete default mirroring  */
/*                      session information. This is mainly required when    */
/*                      IssMirrCtrltable is used for Mirroring configuration */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IssMirrDelDefaultSessionDB ()
{
    tIssMirrDestRecordInfo *pDestRecordInfo = NULL;
    tIssMirrDestRecordInfo *pTempDestRecordInfo = NULL;
    tIssMirrSrcRecordInfo *pSrcRecordInfo = NULL;
    tIssMirrSrcRecordInfo *pTempSrcRecordInfo = NULL;

    ISS_TRC_FN_ENTRY ();
    IssMirrConfigHw (ISS_MIRR_DEFAULT_SESSION, ISS_MIRR_DELETE);

    pDestRecordInfo =
        TMO_SLL_First (&(GET_MIRR_SESSION_DEST_HDR (ISS_MIRR_DEFAULT_SESSION)));

    /*Remove all the previously configured destination ports, only one
     * destination port will exist when configuration of destination will 
     * be done thorugh older mib objects*/
    while (pDestRecordInfo != NULL)
    {

        pTempDestRecordInfo = pDestRecordInfo;

        pDestRecordInfo =
            TMO_SLL_Next (&
                          (GET_MIRR_SESSION_DEST_HDR
                           (ISS_MIRR_DEFAULT_SESSION)),
                          &pDestRecordInfo->NextRecordInfo);

        TMO_SLL_Delete (&(GET_MIRR_SESSION_DEST_HDR (ISS_MIRR_DEFAULT_SESSION)),
                        &pTempDestRecordInfo->NextRecordInfo);
        ISS_MIRR_RECORD_FREE_DEST_MEM_BLOCK (pTempDestRecordInfo);
    }

    pSrcRecordInfo =
        TMO_SLL_First (&(GET_MIRR_SESSION_SRC_HDR (ISS_MIRR_DEFAULT_SESSION)));

    /*Remove all the previously configured source entities */
    while (pSrcRecordInfo != NULL)
    {

        pTempSrcRecordInfo = pSrcRecordInfo;

        pSrcRecordInfo =
            TMO_SLL_Next (&
                          (GET_MIRR_SESSION_SRC_HDR (ISS_MIRR_DEFAULT_SESSION)),
                          &pSrcRecordInfo->NextRecordInfo);

        TMO_SLL_Delete (&(GET_MIRR_SESSION_SRC_HDR (ISS_MIRR_DEFAULT_SESSION)),
                        &pTempSrcRecordInfo->NextRecordInfo);
        ISS_MIRR_RECORD_FREE_SRC_MEM_BLOCK (pTempSrcRecordInfo);
    }
    ISS_TRC_FN_EXIT ();
    return;
}

/*****************************************************************************/
/* Function Name      : IssMirrHandleCreatePort                              */
/*                                                                           */
/* Description        : This function is called whenever a interface create  */
/*                      indication is received to ISS module. Hardware should*/
/*                      be reconfigured in case this port belongs to any     */
/*                      mirroring session                                    */
/*                                                                           */
/* Input(s)           : u2PortIndex - Port Number which is created in system */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IssMirrHandleCreatePort (UINT2 u2PortIndex)
{
    UINT2               u2SessionNo = 0;

    ISS_TRC_FN_ENTRY ();
    for (u2SessionNo = 1; u2SessionNo <= ISS_MIRR_MAX_SESSIONS; u2SessionNo++)
    {
        if ((GET_MIRR_SESSION_INFO (u2SessionNo)->u1SessionStatus !=
             ISS_ROW_STATUS_DESTROY)
            && (GET_MIRR_SESSION_INFO (u2SessionNo)->u1MirroringType ==
                ISS_MIRR_PORT_BASED))
        {
            if ((IssMirrIsDestConfigured (u2SessionNo, u2PortIndex)
                 == ISS_SUCCESS) ||
                (IssMirrIsSrcConfigured (u2SessionNo, ISS_DEFAULT_CONTEXT,
                                         u2PortIndex,
                                         ISS_MIRR_PORT_BASED) == ISS_SUCCESS))
            {
                IssMirrConfigHw (u2SessionNo, ISS_MIRR_DELETE);
                IssMirrConfigHw (u2SessionNo, ISS_MIRR_ADD);
            }
        }
    }
    ISS_TRC_FN_EXIT ();
    return;
}

/*****************************************************************************/
/* Function Name      : IssCreateVlan                                        */
/*                                                                           */
/* Description        : This function is called whenever a vlan create       */
/*                      indication is received to ISS module. Hardware should*/
/*                      be reconfigured in case this vlan belongs to any     */
/*                      mirroring session                                    */
/*                                                                           */
/* Input(s)           : u2ContextId - Context Id in which VLAN is created    */
/*                      u2VlanId    - Vlan Id Created in system              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IssCreateVlan (UINT2 u2ContextId, UINT2 u2VlanId)
{
    UINT2               u2SessionNo = 0;

    ISS_TRC_FN_ENTRY ();
    for (u2SessionNo = 1; u2SessionNo <= ISS_MIRR_MAX_SESSIONS; u2SessionNo++)
    {
        /* Vlans can only be source entities for a session so only source 
           entities are checked */
        if ((GET_MIRR_SESSION_INFO (u2SessionNo)->u1SessionStatus !=
             ISS_ROW_STATUS_DESTROY)
            && (GET_MIRR_SESSION_INFO (u2SessionNo)->u1MirroringType ==
                ISS_MIRR_VLAN_BASED))
        {
            if (IssMirrIsSrcConfigured
                (u2SessionNo, u2ContextId, u2VlanId,
                 ISS_MIRR_VLAN_BASED) == ISS_SUCCESS)
            {
                IssMirrConfigHw (u2SessionNo, ISS_MIRR_DELETE);
                IssMirrConfigHw (u2SessionNo, ISS_MIRR_ADD);
            }
        }
    }
    ISS_TRC_FN_EXIT ();
    return;
}

/*****************************************************************************/
/* Function Name      : IssCreateAcl                                         */
/*                                                                           */
/* Description        : This function is called whenever a Access control    */
/*                      list filter is create.                               */
/*                      Indication is received to Mirroring interface.       */
/*                      Hardware should be reconfigured in case this vlan    */
/*                      belongs to any mirroring session                     */
/*                                                                           */
/* Input(s)           : u4AclNumber - ACL Id                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IssMirrCreateAcl (UINT4 u4AclNumber, INT4 i4AclType)
{
    UINT2               u2SessionNo = 0;
    UINT1               u1Mode = 0;
    UINT1               u1CtrlStatus = 0;

    ISS_TRC_FN_ENTRY ();

    if (gIssGlobalInfo.u1MirrorStatus == ISS_MIRRORDISABLE)
    {
        ISS_TRC_FN_EXIT ()return;
    }

    for (u2SessionNo = 1; u2SessionNo <= ISS_MIRR_MAX_SESSIONS; u2SessionNo++)
    {
        /* ACL can only be source entities for a session so only source 
           entities are checked */

        if ((GET_MIRR_SESSION_INFO (u2SessionNo)->u1SessionStatus !=
             ISS_ROW_STATUS_DESTROY)
            && ((GET_MIRR_SESSION_INFO (u2SessionNo)->u1MirroringType ==
                 ISS_MIRR_IP_FLOW_BASED) ||
                (GET_MIRR_SESSION_INFO (u2SessionNo)->u1MirroringType ==
                 ISS_MIRR_MAC_FLOW_BASED)))
        {
            if (IssMirrGetSrcRecrdInfo
                ((UINT4) u2SessionNo, ISS_DEFAULT_CONTEXT, u4AclNumber, &u1Mode,
                 &u1CtrlStatus) == ISS_SUCCESS)
            {
                if (i4AclType == (INT4) u1Mode)
                {
                    IssMirrConfigHw (u2SessionNo, ISS_MIRR_ADD);
                }
            }
        }
    }
    ISS_TRC_FN_EXIT ();
    return;
}

/*****************************************************************************/
/* Function Name      : IssShowRunningConfigRegHL                            */
/*                                                                           */
/* Description        : This function is called to register the call back    */
/*                        function which indicates the protocol about show     */
/*                        running config                                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS                                          */
/*****************************************************************************/
INT4
IssShowRunningConfigRegHL (tShowRunningConfigRegParams *
                           pShowRunningConfigRegParams)
{
    UINT4               u4Index;

    for (u4Index = 0; u4Index < ISS_MAX_PROTOCOLS; u4Index++)
    {
        if (gShowRunningConfigRegTbl[u4Index].bFlag == ISS_FALSE)
        {
            gShowRunningConfigRegTbl[u4Index].bFlag = ISS_TRUE;
            gShowRunningConfigRegTbl[u4Index].pShowRunningConfig =
                pShowRunningConfigRegParams->pShowRunningConfig;
            return ISS_SUCCESS;
        }
    }
    return ISS_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : IssShowRunningConfigInd                              */
/*                                                                           */
/* Description        : This function is called to indicate the protocol     */
/*                        about configured commands which is displayed         */
/*                        for Show running config command                         */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS                                          */
/*****************************************************************************/

INT4
IssShowRunningConfigInd (tCliHandle CliHandle, UINT4 u4Module)
{
    UINT4               u4Index;

    for (u4Index = 0; u4Index < ISS_MAX_PROTOCOLS; u4Index++)
    {
        if ((gShowRunningConfigRegTbl[u4Index].bFlag == ISS_TRUE)
            && gShowRunningConfigRegTbl[u4Index].pShowRunningConfig != NULL)
        {
            gShowRunningConfigRegTbl[u4Index].pShowRunningConfig (CliHandle,
                                                                  (INT4)
                                                                  u4Module);
        }
    }
    return ISS_SUCCESS;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetWebMaxSession                              */
/*                                                                          */
/*    Description        : This function is used to get the maximum web     */
/*                         sessions configured.                             */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : gi4WebSessions                                   */
/****************************************************************************/
INT4
IssGetWebMaxSession (VOID)
{
    return gi4WebSessions;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetWebMaxSessionTimeOut                       */
/*                                                                          */
/*    Description        : This function is used to get the maximum web     */
/*                         session timeout configured.                      */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : gi4WebSessionTimeOut                             */
/****************************************************************************/
INT4
IssGetWebMaxSessionTimeOut (VOID)
{
    return gi4WebSessionTimeOut;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetHeartBeatModeFromNvRam                     */
/*                                                                          */
/*    Description        : This function is used to get the RM Heart Beat   */
/*                         Mode from the NVRAM.                             */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Heart Beat Mode (INTERNAL / EXTERNAL)            */
/****************************************************************************/
UINT4
IssGetHeartBeatModeFromNvRam (VOID)
{
    return (sNvRamData.u4RmHbMode);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetRedundancyTypeFromNvRam                    */
/*                                                                          */
/*    Description        : This function is used to get the Type of         */
/*                         Redundancy model from the NVRAM.                 */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Redundancy Type (HOT / COLD)                     */
/****************************************************************************/
UINT4
IssGetRedundancyTypeFromNvRam (VOID)
{
    return (sNvRamData.u4RmRType);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetRmDataPlaneFromNvRam                       */
/*                                                                          */
/*    Description        : This function is used to get the Type of hardware*/
/*                         model in a redundant system from the NVRAM.      */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Data Plane Type (SHARED / SEPARATE               */
/****************************************************************************/
UINT4
IssGetRmDataPlaneFromNvRam (VOID)
{
    return (sNvRamData.u4RmDType);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetAutoPortCreateFlagFromNvRam                */
/*                                                                          */
/*    Description        : This function is invoked to get the value of     */
/*                         automatic port create flag from the NvRam.       */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : value of automatic port create  flag             */
/****************************************************************************/
INT4
IssGetAutoPortCreateFlagFromNvRam ()
{

    return (sNvRamData.i1AutomaticPortCreate);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetAutoPortCreateFlag                         */
/*                                                                          */
/*    Description        : This function is invoked to get the value of     */
/*                         automatic port create flag from global config    */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : value of automatic port create  flag             */
/****************************************************************************/

INT4
IssGetAutoPortCreateFlag ()
{

    return (gIssSysGroupInfo.IssAutomaticPortCreate);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetHeartBeatModeToNvRam                       */
/*                                                                          */
/*    Description        : This function is invoked to set the Heart Beat   */
/*                         Mode in NvRam.                                   */
/*                                                                          */
/*    Input(s)           : HeartBeatMode i.e. INTERNAL / EXTERNAL           */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetHeartBeatModeToNvRam (UINT4 u4RmHbMode)
{
    sNvRamData.u4RmHbMode = u4RmHbMode;

    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetRedundancyTypeToNvRam                      */
/*                                                                          */
/*    Description        : This function is invoked to set the Redundancy   */
/*                         Type in NvRam.                                   */
/*                                                                          */
/*    Input(s)           : RedundancyType i.e. HOT / COLD                   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetRedundancyTypeToNvRam (UINT4 u4RmRType)
{
    sNvRamData.u4RmRType = u4RmRType;

    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetRmDataPlaneToNvRam                         */
/*                                                                          */
/*    Description        : This function is invoked to set the RM hardware  */
/*                         model in NvRam.                                  */
/*                                                                          */
/*    Input(s)           : RmDataPlane i.e. SHARED / SEPARATE               */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetRmDataPlaneToNvRam (UINT4 u4RmDType)
{
    sNvRamData.u4RmDType = u4RmDType;

    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssCPUControlledLearning                         */
/*                                                                          */
/*    Description        : This function is invoked to get the status for   */
/*                         CPU controlled learning for a particular port.   */
/*                                                                          */
/*    Input(s)           : port number                                      */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
INT4
IssCPUControlledLearning (UINT2 u2Port)
{
    tIssPortCtrlEntry  *pIssPortCtrlEntry = NULL;
    if (u2Port <= ISS_MAX_PORTS)
    {
        pIssPortCtrlEntry = gIssGlobalInfo.apIssPortCtrlEntry[u2Port - 1];
    }

    if (pIssPortCtrlEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    if (ISS_CPU_CNTRL_LEARN_ENABLE ==
        pIssPortCtrlEntry->IssPortCpuCntrlLearning)
    {
        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetAutoPortCreateFlagToNvRam                  */
/*                                                                          */
/*    Description        : This function is invoked to set the value of     */
/*                         automatic port create flag to the NvRam.         */
/*                                                                          */
/*    Input(s)           : Automatic Port create flag                       */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
IssSetAutoPortCreateFlagToNvRam (INT4 i4AutomaticPortCreate)
{
    sNvRamData.i1AutomaticPortCreate = (INT1) i4AutomaticPortCreate;
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
    return;
}

/*****************************************************************************/
 /* Function Name      : IssHealthChkStatus                                   */
 /*                                                                           */
 /* Description        : This function updates the iss status                 */
 /*                                                                           */
 /* Input(s)           : u1IssHealthStatus                                    */
 /*                                                                           */
 /* Output(s)          : None                                                 */
 /*                                                                           */
 /* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
 /*****************************************************************************/
PUBLIC INT4
ISSSetHealthChkStatus (UINT1 u1IssHealthStatus)
{
    CONST UINT1        *pu1Tmp = NULL;
    tOsixMemStatus      MemStatus;

    MemStatus.u1ErrorStatus = u1IssHealthStatus;
    MemStatus.u1MemAllocErrReason = gIssGlobalInfo.u1IssHealthChkErrorReason;

    if (gu1IsIssInitCompleted == ISS_TRUE)
    {
        ISS_LOCK ();
        gIssGlobalInfo.u1IssHealthChkStatus = u1IssHealthStatus;
        OsixSetHealthStatus (&MemStatus);
        ISS_UNLOCK ();
    }
    else
    {
        pu1Tmp = OsixExGetTaskName (OsixGetCurTaskId ());
        if (u1IssHealthStatus == ISS_DOWN_AND_NONRECOVERABLE_ERR)
        {
            UtlTrcLog (2, 2, (CONST char *) pu1Tmp,
                       " Task faced non-recoverable error\n");
        }
        else if (u1IssHealthStatus == ISS_UP_RECOVERABLE_RUNTIME_ERR)
        {
            UtlTrcLog (2, 2, (CONST char *) pu1Tmp,
                       " Task faced recoverable error\n");
        }
    }
    return ISS_SUCCESS;
}

/*****************************************************************************/
 /* Function Name      : IssHealthChkErrorReason                              */
 /*                                                                           */
 /* Description        : This function updates the iss failure status         */
 /*                                                                           */
 /* Input(s)           : u1IssHealthErrReason                                 */
 /*                                                                           */
 /* Output(s)          : None                                                 */
 /*                                                                           */
 /* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
 /*****************************************************************************/
PUBLIC INT4
ISSSetHealthChkErrReason (UINT1 u1IssHealthErrReason)
{
    CONST UINT1        *pu1Tmp = NULL;
    tOsixMemStatus      MemStatus;

    MemStatus.u1ErrorStatus = gIssGlobalInfo.u1IssHealthChkStatus;
    MemStatus.u1MemAllocErrReason = u1IssHealthErrReason;

    if (gu1IsIssInitCompleted == ISS_TRUE)
    {
        ISS_LOCK ();
        gIssGlobalInfo.u1IssHealthChkErrorReason = u1IssHealthErrReason;
        OsixSetHealthStatus (&MemStatus);
        ISS_UNLOCK ();
    }
    else
    {
        pu1Tmp = OsixExGetTaskName (OsixGetCurTaskId ());
        if (u1IssHealthErrReason == NON_RECOV_TASK_INIT_FAILURE)
        {
            UtlTrcLog (2, 2, (CONST char *) pu1Tmp,
                       " Task failed due to init error\n");
        }
        else if (u1IssHealthErrReason == NON_RECOV_INSUFFICIENT_STARTUP_MEMORY)
        {
            UtlTrcLog (2, 2, (CONST char *) pu1Tmp,
                       " Task failed due to insufficent memory\n");
        }
        else if (u1IssHealthErrReason == RECOV_CRU_BUFF_EXHAUSTED)
        {
            UtlTrcLog (2, 2, (CONST char *) pu1Tmp,
                       "Task failed due to CRU-BUF alloc\n");
        }
        else if (u1IssHealthErrReason == RECOV_CONFIG_RESTORE_FAILED)
        {
            UtlTrcLog (2, 2, (CONST char *) pu1Tmp,
                       "Task failed due to config restoration\n");
        }
        else
        {
            UtlTrcLog (2, 2, (CONST char *) pu1Tmp,
                       "Task failed due to mempool alloc\n");
        }
    }
    return ISS_SUCCESS;
}

/*****************************************************************************/
 /* Function Name      : ISSHealthChkMemAllocErrPoolId                        */
 /*                                                                           */
 /* Description        : This function updates the pool id of Memory exhaust  */
 /*                                                                           */
 /* Input(s)           : u4PoolId                                             */
 /*                                                                           */
 /* Output(s)          : None                                                 */
 /*                                                                           */
 /* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
 /*****************************************************************************/
PUBLIC INT4
ISSSetHealthChkMemAllocErrPoolId (UINT4 u4PoolId)
{
    CONST UINT1        *pu1Tmp = NULL;

    if (gu1IsIssInitCompleted == ISS_TRUE)
    {
        ISS_LOCK ();
        gIssGlobalInfo.u4IssHealthChkMemAllocErrPoolId = u4PoolId;
        ISS_UNLOCK ();
    }
    else
    {
        pu1Tmp = OsixExGetTaskName (OsixGetCurTaskId ());
        UtlTrcLog (2, 2, (CONST char *) pu1Tmp,
                   "Task faced recoverable error\n");

    }
    return ISS_SUCCESS;
}

/*****************************************************************************/
 /* Function Name      : ISSHealthConfigRestoreStatus                         */
 /*                                                                           */
 /* Description        : This function updates configuration restore status   */
 /*                                                                           */
 /* Input(s)           : u1ConfigRestoreStatus                                */
 /*                                                                           */
 /* Output(s)          : None                                                 */
 /*                                                                           */
 /* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
 /*****************************************************************************/
PUBLIC INT4
ISSSetHealthConfigRestoreStatus (UINT1 u1ConfigRestoreStatus)
{

    if (LAST_MIB_RESTORE_SUCCESSFUL == (UINT4) u1ConfigRestoreStatus)
    {
        u1ConfigRestoreStatus = ISS_CONFIG_RESTORE_SUCCESS;
    }
    else if (LAST_MIB_RESTORE_FAILED == (UINT4) gi4MibResStatus)
    {
        u1ConfigRestoreStatus = ISS_CONFIG_RESTORE_FAILED;
    }
    else if (MIB_RESTORE_IN_PROGRESS == (UINT4) gi4MibResStatus)
    {
        u1ConfigRestoreStatus = ISS_CONFIG_RESTORE_IN_PROGRESS;
    }
    else if (MIB_RESTORE_NOT_INITIATED == (UINT4) gi4MibResStatus)
    {
        u1ConfigRestoreStatus = ISS_CONFIG_RESTORE_DEFAULT;
    }
    ISS_LOCK ();
    gIssGlobalInfo.u1IssHealthChkConfigRestoreStatus = u1ConfigRestoreStatus;
    ISS_UNLOCK ();
    return ISS_SUCCESS;
}

/*********************************************************************
*  Function Name : IssPrintStrIpAddress
*  Description   : This function prints the IP6 address in Octet String format.
*
*  Input(s)      : pIpAddr - IpAddr to be modified.
*  Output(s)     : pu1Temp - IP address in dotted format.
*  Return Values : None.
*********************************************************************/

VOID
IssPrintStrIpAddress (UINT1 *pIpAddr, UINT1 *pu1Temp)
{
    UINT1               au1IpAddr[16];
    UINT1               u1IpLen;

    MEMCPY (au1IpAddr, pIpAddr, 16);

    for (u1IpLen = 0; u1IpLen < 4; u1IpLen++)
    {
        sprintf ((CHR1 *) pu1Temp, "%d", au1IpAddr[u1IpLen]);
        pu1Temp += STRLEN (pu1Temp);

        if (u1IpLen != (3))
        {

            STRCPY (pu1Temp, ".");
            pu1Temp += STRLEN (pu1Temp);

        }
    }
    pu1Temp += STRLEN (pu1Temp);

}

#ifdef NPAPI_WANTED
/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : IssSetPortCtrlSpeed                                     */
/*                                                                            */
/* DESCRIPTION      : Sets the port control speed in the data structure.      */
/* Input            : The Indices                                             */
/*                    IssPortCtrlIndex                          */
/*                    The Object                          */
/*                    setValIssPortCtrlSpeed                                  */
/*                                                                            */
/* OUTPUT           : NONE                                                    */
/*                                                                            */
/* RETURNS          : TRUE/FALSE                                              */
/*                                                                            */
/******************************************************************************/
INT1
IssSetPortCtrlSpeed (INT4 i4IssPortCtrlIndex, INT4 i4SetValIssPortCtrlSpeed)
{
    tIssPortCtrlEntry  *pIssPortCtrlEntry = NULL;
    UINT1               u1Status = CFA_DISABLED;
    tCfaIfInfo          CfaIfInfo;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    if (CfaGetIfBridgedIfaceStatus (i4IssPortCtrlIndex, &u1Status) ==
        CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (u1Status == CFA_ENABLED)
    {

        pIssPortCtrlEntry = gIssGlobalInfo.
            apIssPortCtrlEntry[i4IssPortCtrlIndex - 1];

        if (pIssPortCtrlEntry == NULL)
        {
            return FALSE;
        }

        if (pIssPortCtrlEntry->IssPortCtrlSpeed ==
            (UINT4) i4SetValIssPortCtrlSpeed)
        {
            return TRUE;
        }
    }

    switch (i4SetValIssPortCtrlSpeed)
    {
        case ISS_10MBPS:
            CfaIfInfo.u4IfSpeed = CFA_ENET_SPEED_10M;
            CfaIfInfo.u4IfHighSpeed = (CfaIfInfo.u4IfSpeed / CFA_1MB);
            break;
        case ISS_100MBPS:
            CfaIfInfo.u4IfSpeed = CFA_ENET_SPEED_100M;
            CfaIfInfo.u4IfHighSpeed = (CfaIfInfo.u4IfSpeed / CFA_1MB);
            break;
        case ISS_1GB:
            CfaIfInfo.u4IfSpeed = CFA_ENET_SPEED_1G;
            CfaIfInfo.u4IfHighSpeed = (CfaIfInfo.u4IfSpeed / CFA_1MB);
            break;
        case ISS_2500MBPS:
            CfaIfInfo.u4IfSpeed = CFA_ENET_SPEED_2500M;
            CfaIfInfo.u4IfHighSpeed = (CfaIfInfo.u4IfSpeed / CFA_1MB);
            break;
        case ISS_10GB:
            CfaIfInfo.u4IfSpeed = CFA_ENET_MAX_SPEED_VALUE;
            CfaIfInfo.u4IfHighSpeed = CFA_ENET_HISPEED_10G;
            break;
        case ISS_40GB:
            CfaIfInfo.u4IfSpeed = CFA_ENET_MAX_SPEED_VALUE;
            CfaIfInfo.u4IfHighSpeed = CFA_ENET_HISPEED_40G;
            break;
        case ISS_56GB:
            CfaIfInfo.u4IfSpeed = CFA_ENET_MAX_SPEED_VALUE;
            CfaIfInfo.u4IfHighSpeed = CFA_ENET_HISPEED_56G;
            break;
        case ISS_25GB:
            CfaIfInfo.u4IfSpeed = CFA_ENET_MAX_SPEED_VALUE;
            CfaIfInfo.u4IfHighSpeed = CFA_ENET_HISPEED_25G;
            break;
        case ISS_100GB:
            CfaIfInfo.u4IfSpeed = CFA_ENET_MAX_SPEED_VALUE;
            CfaIfInfo.u4IfHighSpeed = CFA_ENET_HISPEED_100G;
            break;
        default:
            CfaIfInfo.u4IfSpeed = CFA_ENET_MAX_SPEED_VALUE;
            CfaIfInfo.u4IfHighSpeed = 0;
            break;
    }
    /* update the speed info to CFA */
    CfaSetIfInfo (IF_SPEED, i4IssPortCtrlIndex, &CfaIfInfo);
    CfaNotifySpeedToHigherLayer (i4IssPortCtrlIndex);

    if (u1Status == CFA_ENABLED)
    {

        CfaNotifySpeedToHigherLayer (i4IssPortCtrlIndex);

        pIssPortCtrlEntry->IssPortCtrlSpeed = i4SetValIssPortCtrlSpeed;
    }
    return TRUE;
}

/******************************************************************************/
/*                                                                            */
/* FUNCTION NAME    : IssSetPortCtrlDuplex                                    */
/*                                                                            */
/* DESCRIPTION      : Sets the port control Duplexity in the data structure.  */
/* Input            : The Indices                                             */
/*                    IssPortCtrlIndex                          */
/*                    The Object                          */
/*                    setValIssPortCtrlDuplex                                 */
/*                                                                            */
/* OUTPUT           : NONE                                                    */
/*                                                                            */
/* RETURNS          : TRUE/FALSE                                              */
/*                                                                            */
/******************************************************************************/
INT1
IssSetPortCtrlDuplex (INT4 i4IssPortCtrlIndex, INT4 i4SetValIssPortCtrlDuplex)
{
    tIssPortCtrlEntry  *pIssPortCtrlEntry = NULL;

    pIssPortCtrlEntry = gIssGlobalInfo.
        apIssPortCtrlEntry[i4IssPortCtrlIndex - 1];

    if (pIssPortCtrlEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pIssPortCtrlEntry->IssPortCtrlDuplex ==
        (UINT4) i4SetValIssPortCtrlDuplex)
    {
        return SNMP_SUCCESS;
    }

    pIssPortCtrlEntry->IssPortCtrlDuplex = i4SetValIssPortCtrlDuplex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  IssSysGetIssPrtCtrlSpeed
 Input       :  i4IssPortCtrlIndex
                IssPrtCtrlSpeed

 Output      :  get the IssPrtCtrlSpeed using CfaDetermineSpeed function for 
                the given i4IssPortCtrlIndex
 Returns     :  VOID
****************************************************************************/
VOID
IssSysGetIssPrtCtrlSpeed (INT4 i4IssPortCtrlIndex,
                          tIssPortCtrlSpeed * pIssPortCtrlSpeed)
{
    UINT4               IssPrtCtrlHighSpeed;
    UINT4               IssPrtCtrlSpeed;

    CfaDetermineSpeed ((UINT4) i4IssPortCtrlIndex,
                       &IssPrtCtrlSpeed, &IssPrtCtrlHighSpeed);

    if (IssPrtCtrlSpeed == CFA_ENET_SPEED_10M)
    {
        *pIssPortCtrlSpeed = ISS_10MBPS;
    }
    else if (IssPrtCtrlSpeed == CFA_ENET_SPEED_1G)
    {
        *pIssPortCtrlSpeed = ISS_1GB;
    }
    else if (IssPrtCtrlSpeed == CFA_ENET_SPEED_2500M)
    {
        *pIssPortCtrlSpeed = ISS_2500MBPS;
    }
    else if (IssPrtCtrlSpeed == CFA_ENET_SPEED_100M)
    {
        *pIssPortCtrlSpeed = ISS_100MBPS;
    }
    else if (IssPrtCtrlSpeed == CFA_ENET_SPEED)
    {
        *pIssPortCtrlSpeed = ISS_100MBPS;
    }
    else if ((IssPrtCtrlSpeed == CFA_ENET_MAX_SPEED_VALUE)
             && (IssPrtCtrlHighSpeed == CFA_ENET_HISPEED_10G))
    {
        *pIssPortCtrlSpeed = ISS_10GB;
    }
    else if ((IssPrtCtrlSpeed == CFA_ENET_MAX_SPEED_VALUE)
             && (IssPrtCtrlHighSpeed == CFA_ENET_HISPEED_40G))
    {
        *pIssPortCtrlSpeed = ISS_40GB;
    }
    else if ((IssPrtCtrlSpeed == CFA_ENET_MAX_SPEED_VALUE)
             && (IssPrtCtrlHighSpeed == CFA_ENET_HISPEED_56G))
    {
        *pIssPortCtrlSpeed = ISS_56GB;
    }
    else if ((IssPrtCtrlSpeed == CFA_ENET_MAX_SPEED_VALUE)
             && (IssPrtCtrlHighSpeed == CFA_ENET_HISPEED_25G))
    {
        *pIssPortCtrlSpeed = ISS_25GB;
    }
    else if ((IssPrtCtrlSpeed == CFA_ENET_MAX_SPEED_VALUE)
             && (IssPrtCtrlHighSpeed == CFA_ENET_HISPEED_100G))
    {
        *pIssPortCtrlSpeed = ISS_100GB;
    }
    else
    {
        *pIssPortCtrlSpeed = ISS_100MBPS;
    }
}
#endif

/****************************************************************************
 Function    :  IssSysConvertCfaSpeedToIssSpeed
 Input       :  u4IfIndex
                u4Speed
                u4HiSpeed

 Output      :  Update the ISS database using the infor from CFA
 Returns     :  VOID
****************************************************************************/
VOID
IssSysConvertCfaSpeedToIssSpeed (UINT4 u4Speed, UINT4 u4HiSpeed,
                                 INT4 *pu4PortCtrlSpeed)
{

    *pu4PortCtrlSpeed = 0;

    switch (u4Speed)
    {
        case CFA_ENET_SPEED_10M:
            *pu4PortCtrlSpeed = ISS_10MBPS;
            break;
        case CFA_ENET_SPEED_100M:
            *pu4PortCtrlSpeed = ISS_100MBPS;
            break;
        case CFA_ENET_SPEED_1G:
            *pu4PortCtrlSpeed = ISS_1GB;
            break;
        case CFA_ENET_SPEED_2500M:
            *pu4PortCtrlSpeed = ISS_2500MBPS;
            break;
        case CFA_ENET_MAX_SPEED_VALUE:
            if (u4HiSpeed == CFA_ENET_HISPEED_10G)
            {
                *pu4PortCtrlSpeed = ISS_10GB;
            }
            else if (u4HiSpeed == CFA_ENET_HISPEED_40G)
            {
                *pu4PortCtrlSpeed = ISS_40GB;
            }
            else if (u4HiSpeed == CFA_ENET_HISPEED_56G)
            {
                *pu4PortCtrlSpeed = ISS_56GB;
            }
            else if (u4HiSpeed == CFA_ENET_HISPEED_25G)
            {
                *pu4PortCtrlSpeed = ISS_25GB;
            }
            else if (u4HiSpeed == CFA_ENET_HISPEED_100G)
            {
                *pu4PortCtrlSpeed = ISS_100GB;
            }
        default:
            break;
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetHwCapabilities                             */
/*                                                                          */
/*    Description        : This function is invoked to get the Hardware     */
/*                         supported informations                           */
/*                                                                          */
/*    Input(s)           : u4HwSupport - Opcode for which the information   */
/*                         on hardware support is required                  */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Status of Hardware Support                       */
/****************************************************************************/

UINT1
IssGetHwCapabilities (UINT1 u1HwSupport)
{
    switch (u1HwSupport)
    {
        case ISS_HW_KNET_IFACE_SUPPORT:
            return gIssHwCapabilities.u1HwKnetIntf;
        case ISS_HW_LA_WITH_DIFF_PORT_SPEED:
            return gIssHwCapabilities.u1HwLaWithDiffPortSpeed;
        case ISS_HW_UNTAGGED_PORTS_FOR_VLANS:
            return gIssHwCapabilities.u1HwUntaggedPortsForVlans;
        case ISS_HW_UNICAST_MAC_LEARNING_LIMIT:
            return gIssHwCapabilities.u1HwUnicastMacLearningLimit;
        case ISS_HW_QUEUE_CONFIG_ON_LA_PORT:
            return gIssHwCapabilities.u1HwQueueConfigOnLaPort;
        case ISS_HW_LA_CREATE_ON_ALL_PORT:
            return gIssHwCapabilities.u1HwLagOnAllPorts;
        case ISS_HW_LAG_ON_CEP_PORT:
            return gIssHwCapabilities.u1HwLagOnCep;
        case ISS_HW_SELF_MAC_IN_MAC_ADDR_TAB:
            return gIssHwCapabilities.u1HwSelfMacInMacTable;
        case ISS_HW_MORE_SCHEDULER_SUPPORT:
            return gIssHwCapabilities.u1HwMoreScheduler;
        case ISS_HW_DWRR_SUPPORT:
            return gIssHwCapabilities.u1HwDwrrSupport;
        case ISS_HW_HIGH_CAP_CNTR:
            return gIssHwCapabilities.u1HwHighCapacityCntr;
        case ISS_HW_CP_TPID_ALLOW:
            return gIssHwCapabilities.u1HwCustomerTpidAllow;
        case ISS_HW_SHAPE_PARAM_EIR_SUPPORT:
            return gIssHwCapabilities.u1ShapeParamEIR;
        case ISS_HW_SHAPE_PARAM_EBS_SUPPORT:
            return gIssHwCapabilities.u1ShapeParamEBS;
        case ISS_HW_METER_COLOR_BLIND:
            return gIssHwCapabilities.u1HwMeterColorBlind;
        case ISS_HW_UNTAG_CEP_PEP:
            return gIssHwCapabilities.u1HwUntagCepPep;
        case ISS_HW_QOS_CLASS_TO_POLICER_AND_Q:
            return gIssHwCapabilities.u1HwQoSClassToPolicerAndQ;

        default:
            return ISS_FAILURE;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSecIpAddrFromNodeId                        */
/*                                                                          */
/*    Description        : This function is invoked to get the IP address   */
/*                         of the OOB secondary interface from the NVRAM    */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : pIpAddr - IP Address of OOB secondary interface  */
/****************************************************************************/
UINT4
IssGetSecIpAddrFromNodeId ()
{
    return gIssGlobalInfo.u4LocalSecIpAddr;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSecIp6AddrFromNodeId                       */
/*                                                                          */
/*    Description        : This function is invoked to get the IPv6 address */
/*                         of the OOB secondary interface                   */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : pLocalIp6Addr - pointer to IPv6 address.         */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssGetSecIp6AddrFromNodeId (tIp6Addr * pLocalIp6Addr)
{
    *pLocalIp6Addr = gIssGlobalInfo.localSecIp6Addr;
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSecSubnetMaskFromNodeId                    */
/*                                                                          */
/*    Description        : This function is invoked to get the Subnet Mask  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Local Subnet Mask                                */
/****************************************************************************/
UINT4
IssGetSecSubnetMaskFromNodeId ()
{
    return gIssGlobalInfo.u4LocalSecSubnetMask;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetSecIp6PrefixLenFromNodeId                  */
/*                                                                          */
/*    Description        : This function is invoked to get the prefix length*/
/*                         of the OOB secondary interface                   */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : value of the prefix length                       */
/****************************************************************************/
UINT1
IssGetSecIp6PrefixLenFromNodeId ()
{
    return gIssGlobalInfo.u1SecPrefixLen;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetSecIpAddrForNodeId                         */
/*                                                                          */
/*    Description        : This function is invoked to set the IP address   */
/*                         of the OOB secondary interface from the NVRAM    */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : pIpAddr - IP Address of OOB secondary interface  */
/****************************************************************************/
VOID
IssSetSecIpAddrForNodeId (UINT4 u4LocalSecIpAddr)
{
    gIssGlobalInfo.u4LocalSecIpAddr = u4LocalSecIpAddr;
    IssWriteNodeidFile ();

    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetSecIp6AddrForNodeId                        */
/*                                                                          */
/*    Description        : This function is invoked to set the IPv6 address */
/*                         of the OOB secondary interface                   */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : pLocalIp6Addr - pointer to IPv6 address.         */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssSetSecIp6AddrForNodeId (tIp6Addr * pLocalIp6Addr)
{
    MEMCPY (&gIssGlobalInfo.localSecIp6Addr, pLocalIp6Addr, sizeof (tIp6Addr));
    IssWriteNodeidFile ();

    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetSecSubnetMaskForNodeId                     */
/*                                                                          */
/*    Description        : This function is invoked to set the Subnet Mask  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Local Subnet Mask                                */
/****************************************************************************/
VOID
IssSetSecSubnetMaskForNodeId (UINT4 u4LocalSecSubnetMask)
{
    gIssGlobalInfo.u4LocalSecSubnetMask = u4LocalSecSubnetMask;
    IssWriteNodeidFile ();

    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssSetSecIp6PrefixLenForNodeId                   */
/*                                                                          */
/*    Description        : This function is invoked to set the prefix length*/
/*                         of the OOB secondary interface                   */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : value of the prefix length                       */
/****************************************************************************/
VOID
IssSetSecIp6PrefixLenForNodeId (UINT1 u1SecPrefixLen)
{
    gIssGlobalInfo.u1SecPrefixLen = u1SecPrefixLen;
    IssWriteNodeidFile ();

    return;
}

/*************************************************************************************/
/*                                                                                   */
/* Function Name    :IssSetSyslogFileSize                                            */
/*                                                                                   */
/* Description      : This function is invoked to set logging file size              */
/*                                                                                   */
/*                                                                                   */
/* Input Parameters : i4filesize - logging file size                                 */
/*                                                                                   */
/* Output Parameters: None                                                           */
/*                                                                                   */
/* Return Value     : ISS_SUCCESS/ISS_FAILURE                                        */
/*************************************************************************************/
INT4
IssSetSyslogFileSize (INT4 i4FileSize)
{
    if (i4FileSize == 0)
    {
        return ISS_FAILURE;
    }
    else
    {
        gu4AuditLogSize = (UINT4) i4FileSize;
        return ISS_SUCCESS;
    }
}

/*************************************************************************************/
/*                                                                                   */
/* Function Name    :IssGetSyslogFileSize                                            */
/*                                                                                   */
/* Description      : This function is invoked to get logging file size              */
/*                                                                                   */
/*                                                                                   */
/* Input Parameters : i4filesize - logging file size                                 */
/*                                                                                   */
/* Output Parameters: i4filesize                                                     */
/*                                                                                   */
/* Return Value     : FileSize                                                       */
/*************************************************************************************/
INT4
IssGetSyslogFileSize (VOID)
{
    return (INT4) gu4AuditLogSize;
}

#endif /* _ISSSYS_C */
