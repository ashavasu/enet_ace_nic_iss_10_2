/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved                      */
/* $Id: issapi.c,v 1.13 2015/07/18 10:05:34 siva Exp $                    */
/* Licensee Aricent Inc., 2001-2002                                          */
/*****************************************************************************/
/*    FILE  NAME            : issapi.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    MODULE NAME           : ISS                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Linux                                          */
/*    DATE OF FIRST RELEASE : 26 August 2010                                 */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains access API's                */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    26 August 2010           Initial Creation                      */
/*---------------------------------------------------------------------------*/
#ifndef ISSAPI_C
#define ISSAPI_C
#include "issinc.h"
#include "isspiinc.h"
#include "isspi.h"

/*****************************************************************************/
/*  Function Name   : IssApiUpdtPortIsolationEntry                           */
/*  Description     : This function is used to do the following              */
/*                    1. Add a port or list of ports for a port isolation    */
/*                       entry                                               */
/*                    2. Remove a port or list of ports from the egress ports*/
/*                       for a port isolation entry.                         */
/*                    3. Remove a port isolation entry                       */
/*  Input(s)        :  pPortIsolation - port Isolation Entry                 */
/*  Output(s)       :  None                                                  */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ISS_SUCCESS on success                    */
/*                                 ISS_FAILURE on failure                    */
/*****************************************************************************/
INT4
IssApiUpdtPortIsolationEntry (tIssUpdtPortIsolation * pPortIsolation)
{
    INT4                i4RetVal = ISS_FAILURE;

    ISS_LOCK ();
    i4RetVal = IssPIUpdtPortIsolationEntry (pPortIsolation);
    ISS_UNLOCK ();

    return i4RetVal;
}

/*****************************************************************************/
/*  Function Name   : IssApiGetIsolationFwdStatus                            */
/*  Description     : This function does the following                       */
/*                      If the u1Action is set as ISS_PI_GET_FWD_STATUS then */
/*                      it checks whether the egress port is an allowed      */
/*                      egress port for the given ingress port, invlanid     */
/*                      pair.                                                */
/*                        (i) Fetches RB Node corresponding to this ingress  */
/*                            index.                                         */
/*                        (ii) If entry is not found, return ISS_SUCCESS.    */
/*                        (iii)If an entry is found and egress port also     */
/*                             matches then return ISS_SUCCESS.              */
/*                        (iv) If an entry is found and egress port match is */
/*                             not found, then return ISS_FAILURE            */
/*                                                                           */
/*  Input(s)        : u4IngressIfIndex - Ingress Port                        */
/*                    u4EgressIfIndex  - Egress Port                         */
/*                    InVlanId       - Vlan Id                               */
/*                    u1Action       - Action supported by this API.         */
/*                      (i) ISS_PI_GET_FWD_STATUS- Get the Port Isolation    */
/*                        forwarding status.                                 */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ISS_SUCCESS,if the given egress port is an*/
/*                                 allowed forward port for the given        */
/*                                 (ingress port, vlanid) pair.              */
/*                                 ISS_FAILURE, otherwise                    */
/*****************************************************************************/
INT1
IssApiGetIsolationFwdStatus (tIssPortIsoInfo * pIssPortIsoInfo)
{
    tIssPortIsolationEntry *pPortIsolationEntry = NULL;
    INT4                i4Scan = 0;

    ISS_LOCK ();
    switch (pIssPortIsoInfo->u1Action)
    {
        case ISS_PI_GET_FWD_STATUS:
            pPortIsolationEntry =
                IssPIGetPortIsolationNode (pIssPortIsoInfo->u4IngressPort,
                                           pIssPortIsoInfo->InVlanId);
            if (NULL == pPortIsolationEntry)
            {
                /* There is no entry in the Port Isolation table for this
                 * ingressport, VlanId pair. Hence forwarding can happen 
                 * via this egress port.
                 */
                ISS_UNLOCK ();
                return ISS_SUCCESS;
            }

            /* Ingress Port entry is matched in the RBTree */
            for (i4Scan = 0; i4Scan < ISS_MAX_UPLINK_PORTS; i4Scan++)
            {
                /* But a matching entry is not found for the egress port.
                 * Hence return Failure.
                 */
                if (0 == pPortIsolationEntry->pu4EgressPorts[i4Scan])
                {
                    ISS_UNLOCK ();
                    return ISS_FAILURE;
                }

                if (pIssPortIsoInfo->u4EgressPort ==
                    pPortIsolationEntry->pu4EgressPorts[i4Scan])
                {
                    /* There is a matching entry for the egress port in the
                     * egress list. Forwarding is allowed on this egress port.
                     * Return Success. */
                    ISS_UNLOCK ();
                    return ISS_SUCCESS;
                }
            }                    /* End of for */
            break;

        default:
            ISS_UNLOCK ();
            return ISS_FAILURE;
    }

    ISS_UNLOCK ();
    return ISS_FAILURE;
}                                /* End of IssApiGetIsolationFwdStatus */

/*****************************************************************************/
/*  Function Name   : IssPIApiIsPIStorageTypeVolatile                        */
/*  Description     : This Function checks if  the storage type of the Port  */
/*                    isolation node is volatile.                            */
/*  Input(s)        : u4IngressPort - Ingress Port                           */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ISS_SUCCESS on success/                   */
/*                                 ISS_FAILURE if PI node is not found.      */
/*****************************************************************************/
INT1
IssPIApiIsPIStorageTypeVolatile (UINT4 u4IngressPort, tVlanId InVlanId)
{
    tIssPortIsolationEntry *pIssPortIsolation = NULL;

    pIssPortIsolation = IssPIGetPortIsolationNode (u4IngressPort, InVlanId);
    if (NULL == pIssPortIsolation)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nIssPIGetPortIsolationStorageType - Match not found in the \
                PI table for the ingress port, VlanId: (%d, %d) \r\n",
                      u4IngressPort, InVlanId);
        return ISS_FAILURE;
    }
    else
    {
        if (ISS_PI_NON_VOLATILE == pIssPortIsolation->u1StorageClass)
        {
            return ISS_FAILURE;
        }
    }
    return ISS_SUCCESS;
}

INT1
IssGetPortANCapBits (INT4 i4Port, INT4 *pi4PortANCapBits)
{
    tIssPortCtrlEntry  *pIssPortCtrlEntry = NULL;
    pIssPortCtrlEntry = gIssGlobalInfo.apIssPortCtrlEntry[i4Port - 1];
    if (pIssPortCtrlEntry == NULL)
    {
        return ISS_FAILURE;
    }
    *pi4PortANCapBits = pIssPortCtrlEntry->i4IssPortCtrlANCapBits;
    return ISS_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : IssGetStackingModel
 *
 *    Description          : This function is called to get the Stacking Model
 *
 *
 *    Input(s)             : None
 *
 *
 *    Output(s)            : None.
 *
 *    Returns              : ISS_CTRL_PLANE_STACKING_MODEL /
 *                           ISS_DATA_PLANE_STACKING_MODEL / 
 *                           ISS_DISS_STACKING_MODEL 
 *
 *****************************************************************************/
PUBLIC INT4
IssGetStackingModel (VOID)
{
    return (INT4) gu4StackingModel;

}

/*****************************************************************************
 *
 *    Function Name        : IssGetLoginAttempts
 *
 *    Description          : This function is called to get the Login Attemts
 *
 *
 *    Input(s)             : None
 *
 *
 *    Output(s)            : None
 *
 *    Returns              : LoginAttempts 
 *
 *****************************************************************************/
PUBLIC INT4
IssGetLoginAttempts (VOID)
{
    return ((INT4) gIssSysGroupInfo.IssBlockPrompt.u1LoginAttempts);
}

/*****************************************************************************
 * 
 *     Function Name        : IssIsClearConfigInProgress
 * 
 *     Description          : This function is called to check whether the
 *                            clear config operation is in progress. Returns 
 *                            ISS_TRUE if clear config is in progress,
 *                            else ISS_FALSE
 * 
 *     Input(s)             : None
 * 
 * 
 *     Output(s)            : None
 * 
 *     Returns              : ISS_TRUE / ISS_FALSE
 * 
 * ****************************************************************************/
PUBLIC INT4
IssIsClearConfigInProgress (VOID)
{
    return gi4ClrConfigInProgress;
}

/*****************************************************************************/
/* Function Name      : IssGetShowCmdOutputAndCalcChkSum                     */
/*                                                                           */
/* Description        : This funcion handles the execution of show commands  */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu2SwAudChkSum                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MBSM_SUCESS/MBSM_FAILURE                             */
/*****************************************************************************/
INT4
IssGetShowCmdOutputAndCalcChkSum (UINT2 *pu2SwAudChkSum)
{
#if (defined CLI_WANTED && defined RM_WANTED)

    if (RmGetNodeState () == RM_ACTIVE)
    {
        if (IssCliGetShowCmdOutputToFile ((UINT1 *) ISS_AUDIT_FILE_ACTIVE) !=
            ISS_SUCCESS)
        {
            ISS_TRC (ALL_FAILURE_TRC, "GetShRunFile Failed\n");
            return ISS_FAILURE;
        }
        if (IssCliCalcSwAudCheckSum
            ((UINT1 *) ISS_AUDIT_FILE_ACTIVE, pu2SwAudChkSum) != ISS_SUCCESS)
        {
            ISS_TRC (ALL_FAILURE_TRC, "CalcSwAudChkSum Failed\n");
            return ISS_FAILURE;
        }
    }
    else if (RmGetNodeState () == RM_STANDBY)
    {
        if (IssCliGetShowCmdOutputToFile ((UINT1 *) ISS_AUDIT_FILE_STDBY) !=
            ISS_SUCCESS)
        {
            ISS_TRC (ALL_FAILURE_TRC, "GetShRunFile Failed\n");
            return ISS_FAILURE;
        }
        if (IssCliCalcSwAudCheckSum
            ((UINT1 *) ISS_AUDIT_FILE_STDBY, pu2SwAudChkSum) != ISS_SUCCESS)
        {
            ISS_TRC (ALL_FAILURE_TRC, "CalcSwAudChkSum Failed\n");
            return ISS_FAILURE;
        }
    }
    else
    {
        ISS_TRC (ALL_FAILURE_TRC, "Node is neither active nor standby\n");
        return ISS_FAILURE;
    }
#else
    UNUSED_PARAM (pu2SwAudChkSum);
#endif
    return ISS_SUCCESS;
}
/*****************************************************************************/
/*  Function Name   : IssApiGetIsolationEgressPorts                          */
/*  Description     : This function is used to do the following              */
/*                    get egress ports present in port isolation table       */
/*  Input(s)        :  InVlanId       - Vlan Id                              */
/*                     u4IngressPort  - Ingress Port                         */
/*  Output(s)       :  Egress ports for port isolation entry                 */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ISS_SUCCESS on success                    */
/*                                 ISS_FAILURE on failure                    */
/*****************************************************************************/
INT4
IssApiGetIsolationEgressPorts (tIssPortIsoInfo *pIssPortIsoInfo, UINT4 *pu4EgressPorts)
{

    tIssPortIsolationEntry *pPortIsolationEntry = NULL;

    ISS_LOCK ();

    pPortIsolationEntry =
        IssPIGetPortIsolationNode (pIssPortIsoInfo->u4IngressPort,
                                   pIssPortIsoInfo->InVlanId);
    if (pPortIsolationEntry != NULL)
    {
        /* Updating Egress Ports */
        MEMCPY (pu4EgressPorts,
                        pPortIsolationEntry->pu4EgressPorts,
                        ((sizeof (UINT4))*ISS_MAX_UPLINK_PORTS));
	ISS_UNLOCK ();
    	return ISS_SUCCESS;
    }

    ISS_UNLOCK ();
    return ISS_FAILURE;
}

/*****************************************************************************/
/* Function Name      : IssIsMirrorVaildActiveStatus	                     */
/*                                                                           */
/* Description        : This function will validate configurations for a     */
/*                      monitor session and checks wheather the session can  */
/*                  	be made active.					     */
/*                                                                           */
/* Input(s)           : i4SessionIndex                      		     */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCESS/SNMP_FAILURE                             */
/*****************************************************************************/

INT1
IssIsMirrorVaildActiveStatus (INT4 i4SessionIndex)
{
	INT4		i4ExtnStatus;
	UINT4		u4NextContextNum;
	UINT4           u4NextSrcNum;
	UINT4		u4NextDestNum;
	UINT1           u1Mode;
	UINT1           u1ControlStatus;

	i4ExtnStatus = GET_MIRR_SESSION_INFO
		((UINT2) i4SessionIndex)->u1SessionStatus;

	if (i4ExtnStatus == ISS_DESTROY || i4ExtnStatus == ISS_CREATE_AND_WAIT || i4ExtnStatus == ISS_CREATE_AND_GO)
	{
		return SNMP_FAILURE;
	}

	else
	{
		/*Check destination is configured */
		if (IssMirrGetNextDestRecrd
				((UINT4) i4SessionIndex, 0,
				 &u4NextDestNum) == ISS_FAILURE)
		{
			return SNMP_FAILURE;
		}
		if (GET_MIRR_SESSION_INFO ((UINT2) i4SessionIndex)->u1MirroringType != ISS_MIRR_VLAN_BASED)
		{
			/*Check source are configured */
			if (IssMirrGetNextSrcRecrd ((UINT4) i4SessionIndex,
						ISS_DEFAULT_CONTEXT, 0,
						&u4NextContextNum, &u4NextSrcNum,
						&u1Mode,
						&u1ControlStatus) == ISS_FAILURE)
			{
				return SNMP_FAILURE;
			}
		}

	}
	return SNMP_SUCCESS;
}
#endif /* ISSAPI_C */
