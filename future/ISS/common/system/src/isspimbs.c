/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved                      */
/* $Id: isspimbs.c,v 1.2 2012/10/31 09:46:12 siva Exp $                 */
/* Licensee Aricent Inc., 2001-2002                                          */
/*****************************************************************************/
/*    FILE  NAME            : pimbsm.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Port Isolation MBSM                            */
/*    MODULE NAME           : ISS                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Linux                                          */
/*    DATE OF FIRST RELEASE : 22 July 2010                                   */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains MBSM handlers for the       */
/*                            port isolation table manipulation.             */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    22 July 2010           Initial Creation                        */
/*---------------------------------------------------------------------------*/

#ifndef PIMBSM_C
#define PIMBSM_C

#include "issinc.h"
#include "issnp.h"
#include "isspinp.h"
#include "isspi.h"
#include "isspiinc.h"

extern tIssPIGlobalInfo gIssPIGlobalInfo;
/*****************************************************************************/
/*  Function Name   : IssPIMbsmCardInsertPITable                             */
/*  Description     : This Function updates the Port Isolation table  related*/
/*                    hardware configuration information when                */
/*                    a card is inserted into the MBSM System.               */
/*  Input(s)        : pPortInfo  - Pointer to Port Related Info.             */
/*                  : pSlotInfo  - Pointer to Slot Related Info.             */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/* Returns                      : MBSM_SUCCESS or MBSM_FAILURE               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IssPIMbsmCardInsertPITable (tMbsmPortInfo * pPortInfo,
                            tMbsmSlotInfo * pSlotInfo)
{
    tIssPortIsolationEntry IssPortIsolationEntry;
    tIssPortIsolationEntry *pIssPortIsolationEntry = NULL;
    UINT4               u4StartPortIndex = 0;
    UINT4               u4EndPortIndex = 0;
    UINT4               u4ScanIndex = 0;

    MEMSET (&IssPortIsolationEntry, 0, sizeof (tIssPortIsolationEntry));

    if ((pSlotInfo->i1IsPreConfigured == MBSM_FALSE) ||
        (pPortInfo->u4PortCount == 0))
    {
        return MBSM_SUCCESS;
    }

    /* Scan through the entire Port Isolation table. */
    pIssPortIsolationEntry = (tIssPortIsolationEntry *)
        RBTreeGetFirst (gIssPIGlobalInfo.PortIsolationTable);
    if (NULL == pIssPortIsolationEntry)
    {
        return MBSM_SUCCESS;
    }

    /* Get the port range of the card newly inserted. */
    u4StartPortIndex = MBSM_PORT_INFO_STARTINDEX (pPortInfo);
    u4EndPortIndex = MBSM_PORT_INFO_STARTINDEX (pPortInfo) +
        MBSM_PORT_INFO_PORTCOUNT (pPortInfo);

    do
    {
        /* Step 1: This condition checks if the ingress interface index lies
         * in the index range of the newly inserted card.
         */
        if (IssPIMbsmCheckPortRange (pIssPortIsolationEntry->
                                     u4IngressIfIndex,
                                     u4StartPortIndex,
                                     u4EndPortIndex) == ISS_TRUE)
        {
            /* If the ingress index lies in the range of the card newly
             * inserted, program in hardware. 
             */
            IssPIMbsmAddPortIsolationNode (pIssPortIsolationEntry, pSlotInfo);
            IssPortIsolationEntry.u4IngressIfIndex = pIssPortIsolationEntry->
                u4IngressIfIndex;
            IssPortIsolationEntry.InVlanId = pIssPortIsolationEntry->InVlanId;
            continue;
        }

        /* Step 2: Scan the entire egress list. */
        for (u4ScanIndex = 0; u4ScanIndex < ISS_MAX_UPLINK_PORTS; u4ScanIndex++)
        {
            /* If a 0 is encountered, then it means, no more ports
             * are present in the list. 
             * If the interface index exceeds the range of the newly
             * inserted card, no more ports need to be programmed
             * in the hardware, since the ports are maintained in the
             * ascending order in the egress list. 
             */
            if ((0 ==
                 pIssPortIsolationEntry->pu4EgressPorts[u4ScanIndex]) ||
                (pIssPortIsolationEntry->pu4EgressPorts[u4ScanIndex] >
                 u4EndPortIndex))
            {
                break;
            }

            /* If any of the egress ports fall in the range of the 
             * newly inserted card, program that port in the
             * hardware. 
             */
            if ((pIssPortIsolationEntry->pu4EgressPorts[u4ScanIndex] >=
                 u4StartPortIndex))
            {
                IssPIMbsmAddPortIsolationEntry (pIssPortIsolationEntry,
                                                *(pIssPortIsolationEntry->
                                                  pu4EgressPorts + u4ScanIndex),
                                                pSlotInfo);
            }
        }                        /* End of for scanning the egress ports list */

        IssPortIsolationEntry.u4IngressIfIndex = pIssPortIsolationEntry->
            u4IngressIfIndex;
        IssPortIsolationEntry.InVlanId = pIssPortIsolationEntry->InVlanId;
    }
    while ((pIssPortIsolationEntry =
            RBTreeGetNext (gIssPIGlobalInfo.PortIsolationTable,
                           &IssPortIsolationEntry, NULL)) != NULL);
    return MBSM_SUCCESS;
}                                /* End of IssPIMbsmCardInsertPITable */

/*****************************************************************************/
/*  Function Name   : IssPIMbsmCardRemovePITable                             */
/*  Description     : This Function updates the Port Isolation table  related*/
/*                    hardware configuration information when                */
/*                    a card is removed from the MBSM System.                */
/*  Input(s)        : pPortInfo  - Pointer to Port Related Info.             */
/*                  : pSlotInfo  - Pointer to Slot Related Info.             */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/* Returns                      : MBSM_SUCCESS or MBSM_FAILURE               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
IssPIMbsmCardRemovePITable (tMbsmPortInfo * pPortInfo,
                            tMbsmSlotInfo * pSlotInfo)
{
    tIssPortIsolationEntry IssPortIsolation;
    tIssPortIsolationEntry *pIssPortIsolationEntry = NULL;
    UINT4               u4StartPortIndex = 0;
    UINT4               u4EndPortIndex = 0;
    UINT4               u4ScanIndex = 0;
    MEMSET (&IssPortIsolation, 0, sizeof (tIssPortIsolationEntry));

    if ((pSlotInfo->i1IsPreConfigured == MBSM_FALSE) ||
        (pPortInfo->u4PortCount == 0))
    {
        return MBSM_SUCCESS;
    }

    /* Get the port range of the card newly inserted. */
    u4StartPortIndex = MBSM_PORT_INFO_STARTINDEX (pPortInfo);
    u4EndPortIndex = MBSM_PORT_INFO_STARTINDEX (pPortInfo) +
        MBSM_PORT_INFO_PORTCOUNT (pPortInfo);

    /* Scan through the entire Port Isolation table. */
    pIssPortIsolationEntry =
        RBTreeGetFirst (gIssPIGlobalInfo.PortIsolationTable);
    if (NULL == pIssPortIsolationEntry)
    {
        return MBSM_SUCCESS;
    }

    do
    {
        /* (i) Check for the ingress port.
         * If the ingress port of this Port Isolation node, belongs
         * to the card removed, do nothing, instead continue with 
         * the looping, since it does not exist in the hardware. 
         */
        if (IssPIMbsmCheckPortRange (pIssPortIsolationEntry->u4IngressIfIndex,
                                     u4StartPortIndex,
                                     u4EndPortIndex) == ISS_TRUE)
        {
            IssPortIsolation.u4IngressIfIndex = pIssPortIsolationEntry->
                u4IngressIfIndex;
            IssPortIsolation.InVlanId = pIssPortIsolationEntry->InVlanId;
            continue;
        }

        /* (ii) Check the egress port list. 
         *  If the egress ports of this Port Isolation node, belong to 
         *  the card removed, safely remove the configuration from the
         *  hardware as well. 
         */
        for (u4ScanIndex = 0; u4ScanIndex < ISS_MAX_UPLINK_PORTS; u4ScanIndex++)
        {
            /* If a 0 is encountered, then it means, no more ports
             * are present in the list. 
             * If the interface index exceeds the range of the 
             * removed card, no more ports need to be programmed
             * in the hardware, since the ports are maintained in the
             * ascending order in the egress list. 
             */

            if ((0 == pIssPortIsolationEntry->pu4EgressPorts[u4ScanIndex]) ||
                (pIssPortIsolationEntry->pu4EgressPorts[u4ScanIndex] >
                 u4EndPortIndex))
            {
                break;
            }

            /* If any of the egress ports fall in the range of the 
             * removed card, removed that port from this Port Isolation node
             * in the hardware also.
             */
            if ((pIssPortIsolationEntry->pu4EgressPorts[u4ScanIndex] >=
                 u4StartPortIndex))
            {
                IssPIMbsmDelPortIsolationEntry (pIssPortIsolationEntry,
                                                *(pIssPortIsolationEntry->
                                                  pu4EgressPorts + u4ScanIndex),
                                                pSlotInfo);
            }
        }                        /* End of for scanning the egress ports list */

        IssPortIsolation.u4IngressIfIndex = pIssPortIsolationEntry->
            u4IngressIfIndex;
        IssPortIsolation.InVlanId = pIssPortIsolationEntry->InVlanId;
    }
    while ((pIssPortIsolationEntry =
            RBTreeGetNext (gIssPIGlobalInfo.PortIsolationTable,
                           &IssPortIsolation, NULL)) != NULL);
    /* End of while scanning the entire port isolation node. */
    return MBSM_SUCCESS;
}                                /* End of IssPIMbsmCardRemovePITable */

/*****************************************************************************/
/*  Function Name   : IssPIMbsmAddPortIsolationEntry                         */
/*  Description     : This Function updates the Port Isolation table  related*/
/*                    hardware configuration information when                */
/*                    a card is added in the MBSM System.                    */
/*  Input(s)        : pSlotInfo  - Pointer to Slot Related Info.             */
/*                  : pIssPortIsolationEntry  - Pointer to PI Entry          */
/*                    u4EgressPort            - EgressPort                   */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*                                                                           */
/*****************************************************************************/
VOID
IssPIMbsmAddPortIsolationEntry (tIssPortIsolationEntry * pIssPortIsolationEntry,
                                UINT4 u4EgressPort, tMbsmSlotInfo * pSlotInfo)
{
    tIssHwUpdtPortIsolation IssHwUpdPortIsolation;
    tIssMbsmInfo        IssMbsmInfo;

    MEMSET (&IssHwUpdPortIsolation, 0, sizeof (tIssHwUpdtPortIsolation));
    MEMSET (&IssMbsmInfo, 0, sizeof (tIssMbsmInfo));

    /* Populate the H/W port Isolation information. */
    IssHwUpdPortIsolation.u4IngressPort = pIssPortIsolationEntry->
        u4IngressIfIndex;
    IssHwUpdPortIsolation.InVlanId = pIssPortIsolationEntry->InVlanId;
    IssHwUpdPortIsolation.au4EgressPorts[0] = u4EgressPort;
    IssHwUpdPortIsolation.u2NumEgressPorts = 1;
    IssHwUpdPortIsolation.u1Action = ISS_PI_ADD;

    /* Populate the MBSM related information. */
    IssMbsmInfo.pSlotInfo = pSlotInfo;
    IssMbsmInfo.u1CardAction = MBSM_MSG_CARD_INSERT;

    IsssysIssPIMbsmHwConfigPortIsolationEntry (&IssHwUpdPortIsolation,
                                               &IssMbsmInfo);
    return;
}                                /* End of IssPIMbsmAddPortIsolationEntry */

/*****************************************************************************/
/*  Function Name   : IssPIMbsmDelPortIsolationEntry                         */
/*  Description     : This Function updates the Port Isolation table  related*/
/*                    hardware configuration information when                */
/*                    a card is removed from the MBSM System.                */
/*  Input(s)        : pSlotInfo  - Pointer to Slot Related Info.             */
/*                  : pIssPortIsolationEntry  - Pointer to PI Entry          */
/*                    u4EgressPort            - EgressPort                   */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*                                                                           */
/*****************************************************************************/
VOID
IssPIMbsmDelPortIsolationEntry (tIssPortIsolationEntry * pIssPortIsolationEntry,
                                UINT4 u4EgressPort, tMbsmSlotInfo * pSlotInfo)
{
    tIssHwUpdtPortIsolation IssHwUpdPortIsolation;
    tIssMbsmInfo        IssMbsmInfo;

    MEMSET (&IssHwUpdPortIsolation, 0, sizeof (tIssHwUpdtPortIsolation));
    MEMSET (&IssMbsmInfo, 0, sizeof (tIssMbsmInfo));

    /* Populate the H/W port Isolation information. */
    IssHwUpdPortIsolation.u4IngressPort = pIssPortIsolationEntry->
        u4IngressIfIndex;
    IssHwUpdPortIsolation.InVlanId = pIssPortIsolationEntry->InVlanId;
    IssHwUpdPortIsolation.au4EgressPorts[0] = u4EgressPort;
    IssHwUpdPortIsolation.u2NumEgressPorts = 1;
    IssHwUpdPortIsolation.u1Action = ISS_PI_DELETE;

    /* Populate the MBSM related information. */
    IssMbsmInfo.pSlotInfo = pSlotInfo;
    IssMbsmInfo.u1CardAction = MBSM_MSG_CARD_REMOVE;

    IsssysIssPIMbsmHwConfigPortIsolationEntry (&IssHwUpdPortIsolation,
                                               &IssMbsmInfo);
    return;
}                                /* End of IssPIMbsmDelPortIsolationEntry */

/*****************************************************************************/
/*  Function Name   : IssPIMbsmAddPortIsolationNode                          */
/*  Description     : This Function updates the Port Isolation table  related*/
/*                    hardware configuration information when                */
/*                    a card is added in the MBSM System.                    */
/*  Input(s)        : pSlotInfo  - Pointer to Slot Related Info.             */
/*                  : pIssPortIsolationEntry  - Pointer to PI Entry          */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*                                                                           */
/*****************************************************************************/
VOID
IssPIMbsmAddPortIsolationNode (tIssPortIsolationEntry * pIssPortIsolationEntry,
                               tMbsmSlotInfo * pSlotInfo)
{
    tIssHwUpdtPortIsolation IssHwUpdPortIsolation;
    tIssMbsmInfo        IssMbsmInfo;
    UINT2               u2Scan = 0;

    MEMSET (&IssHwUpdPortIsolation, 0, sizeof (tIssHwUpdtPortIsolation));
    MEMSET (&IssMbsmInfo, 0, sizeof (tIssMbsmInfo));

    /* Populate the H/W port Isolation information. */
    IssHwUpdPortIsolation.u4IngressPort = pIssPortIsolationEntry->
        u4IngressIfIndex;
    IssHwUpdPortIsolation.InVlanId = pIssPortIsolationEntry->InVlanId;
    /* This scanning is required to identify the number of egress ports. */
    while (u2Scan < ISS_MAX_UPLINK_PORTS)
    {
        if (0 == *(pIssPortIsolationEntry->pu4EgressPorts + u2Scan))
        {
            break;
        }

        IssHwUpdPortIsolation.au4EgressPorts[u2Scan] =
            *(pIssPortIsolationEntry->pu4EgressPorts + u2Scan);
        u2Scan++;
    }                            /* End of while */

    IssHwUpdPortIsolation.u2NumEgressPorts = u2Scan;
    IssHwUpdPortIsolation.u1Action = ISS_PI_ADD;

    /* Populate the MBSM related information. */
    IssMbsmInfo.pSlotInfo = pSlotInfo;
    IssMbsmInfo.u1CardAction = MBSM_MSG_CARD_INSERT;

    IsssysIssPIMbsmHwConfigPortIsolationEntry (&IssHwUpdPortIsolation,
                                               &IssMbsmInfo);
    return;
}                                /* End of IssPIMbsmAddPortIsolationNode */

/*****************************************************************************/
/*  Function Name   : IssPIMbsmAddPortIsolationNode                          */
/*  Description     : This Function checks if the interface index falls in   */
/*                    the range of the start index and end index passed as   */
/*                    arguments.                                             */
/*  Input(s)        : u4IfIndex  - Interface index.                          */
/*                    u4StartPortIndex - Start Interface Index.              */
/*                    u4EndPortIndex   - End Interface Index.                */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ISS_TRUE/ISS_FALSE                        */
/*                                                                           */
/*****************************************************************************/
INT1
IssPIMbsmCheckPortRange (UINT4 u4IfIndex,
                         UINT4 u4StartPortIndex, UINT4 u4EndPortIndex)
{
    if ((u4IfIndex >= u4StartPortIndex) && (u4IfIndex <= u4EndPortIndex))
    {
        return ISS_TRUE;
    }

    return ISS_FALSE;
}                                /* End of IssPIMbsmCheckPortRange */
#endif /* PIMBSM_C */
