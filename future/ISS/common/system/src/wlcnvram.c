/****************************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: wlcnvram.c,v 1.3 2014/07/16 12:45:09 siva Exp $
*
* Description: This file has the routines for the WLC NvRam related Calls.
*
*****************************************************************************/

#ifndef __WLCNVRAM_C__
#define __WLCNVRAM_C__

#include "issinc.h"
#include "iss.h"
#include "cli.h"
#include "issnvram.h"
#include "wlcnvram.h"

tWLCNVRAM_DATA      sWlcNvRamData;

#ifndef NPAPI_WANTED
#define FNP_SUCCESS      1
#define FNP_FAILURE      0
#endif

#ifdef OPENSSL_WANTED
extern void         DtlsSslSetAuthType (unsigned int authtype);
#endif

/****************************************************************************
*                   WLC NVRAM RELATED PORTABLE FUNCIONS                     *
*****************************************************************************/

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WlcNvRamRead                                     */
/*                                                                          */
/*    Description        : This function is is a portable routine, to       */
/*                         read the critical configuration for the switch   */
/*                         from WlcNvRam.                                   */
/*                         While reading the information if it is failed    */
/*                         for any/all filed(s) it will set it to default   */
/*                         value.                                           */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : tWLCNVRAM_DATA  *pWlcNvRamData                   */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/*                                                                          */
/*    Note               : The return type should be as mentioned above for */
/*                         ported routines also.                            */
/****************************************************************************/

int
WlcNvRamRead (tWLCNVRAM_DATA * pWlcNvRamData)
{
    UINT1               au1Buffer[MAX_COLUMN_LENGTH];
    UINT1               u1ErrorCount = 0;

    MEMSET (au1Buffer, 0, MAX_COLUMN_LENGTH);
    /* First check whether the file exist or not */
    if (GetNvRamValue
        ((UINT1 *) WLC_NVRAM_FILE, (UINT1 *) "CAPWAP_UDP_SERVER_PORT",
         au1Buffer) == NVRAM_FILE_NOT_FOUND)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]:Writing new WLC_NVRAM_FILE with default values\n");
        WssInitialiseWlcNvRamDefVal ();

        return FNP_SUCCESS;
    }
    /* Read the CAPWAP_UDP_SERVER_PORT */
    if (GetNvRamValue
        ((UINT1 *) WLC_NVRAM_FILE, (UINT1 *) "CAPWAP_UDP_SERVER_PORT",
         au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]:Failed to read CAPWAP UDP Server Port\n");
        u1ErrorCount++;
        pWlcNvRamData->u4CapwapUdpServerPort =
            WSS_DEFAULT_CAPWAP_UDP_SERVER_PORT;
    }
    else
    {
        pWlcNvRamData->u4CapwapUdpServerPort = (UINT4) ISS_ATOI (au1Buffer);
    }

    /* Read the DTLS_KEY_TYPE */
    if (GetNvRamValue ((UINT1 *) WLC_NVRAM_FILE, (UINT1 *) "DTLS_KEY_TYPE",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]:Failed to read DTLS_KEY_TYPE\n");
        u1ErrorCount++;
        pWlcNvRamData->u4DtlsKeyType = WSS_DEFAULT_DTLS_KEY_TYPE;
    }
    else
    {
        pWlcNvRamData->u4DtlsKeyType = (UINT4) ISS_ATOI (au1Buffer);

        if (pWlcNvRamData->u4DtlsKeyType == WSS_DTLS_KEY_TYPE_CERT)
        {
#ifdef DTLS_WANTED
            DtlsSslSetAuthType (1);
#endif
        }
        else
        {
#ifdef DTLS_WANTED
            DtlsSslSetAuthType (0);
#endif
            pWlcNvRamData->u4DtlsKeyType = WSS_DEFAULT_DTLS_KEY_TYPE;
        }
    }

    if (u1ErrorCount > 0)
    {
        return FNP_FAILURE;
    }
    else
    {
        return FNP_SUCCESS;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WlcNvRamWrite                                    */
/*                                                                          */
/*    Description        : This function is a portable routine, to          */
/*                         write the critical configuration for the switch  */
/*                         to the Wlc NvRam.                                */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/*                                                                          */
/*    Note               : The return type should be as mentioned above for */
/*                         ported routines also.                            */
/****************************************************************************/
int
WlcNvRamWrite (tWLCNVRAM_DATA * pWlcNvRamData)
{
    INT4                i4Fd = 0;
    CHR1                au1Buf[WLC_NVRAM_MAX_LEN_1];

    MEMSET (au1Buf, 0, sizeof (au1Buf));
    i4Fd = FileOpen ((CONST UINT1 *) WLC_NVRAM_FILE,
                     OSIX_FILE_WO | OSIX_FILE_TR | OSIX_FILE_CR);
    if (i4Fd < 0)
    {
        return FNP_FAILURE;
    }

    /* CAPWAP UDP Server Port */
    SPRINTF (au1Buf, "CAPWAP_UDP_SERVER_PORT =%d\n",
             pWlcNvRamData->u4CapwapUdpServerPort);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));

    /* DTLS Key Type */
    SPRINTF (au1Buf, "DTLS_KEY_TYPE =%d\n", pWlcNvRamData->u4DtlsKeyType);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));

    FileClose (i4Fd);

#if defined(SWC)
    system ("etc flush");
#endif
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssGetCapwapUdpSPortFromWlcNvRam                 */
/*                                                                          */
/*    Description        : This function is used to get the CAPWAP UDP      */
/*                         Server Port from the WLC NVRAM.                  */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : CAPWAP UDP Server Port                           */
/****************************************************************************/
UINT4
WssGetCapwapUdpSPortFromWlcNvRam (VOID)
{
    return (sWlcNvRamData.u4CapwapUdpServerPort);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssSetCapwapUdpSPortToWlcNvRam                   */
/*                                                                          */
/*    Description        : This function is invoked to set the value of     */
/*                         CAPWAP UDP Server Port to the WLC NvRam.         */
/*                                                                          */
/*    Input(s)           : CAPWAP UDP Server Port                           */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
WssSetCapwapUdpSPortToWlcNvRam (UINT4 u4CapwapUdpServerPort)
{
    if (WlcNvRamRead (&sWlcNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to Read from WLC NvRam\n");
        return;
    }
    sWlcNvRamData.u4CapwapUdpServerPort = u4CapwapUdpServerPort;

    if (WlcNvRamWrite (&sWlcNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to WLC NvRam\n");
        return;
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssGetDtlsKeyTypeFromWlcNvRam                    */
/*                                                                          */
/*    Description        : This function is invoked to get the DTLS Key Type*/
/*                         from the WLC NVRAM.                              */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : DTLS Key Type                                    */
/****************************************************************************/
UINT4
WssGetDtlsKeyTypeFromWlcNvRam (VOID)
{
    return (sWlcNvRamData.u4DtlsKeyType);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssSetDtlsKeyTypeToWlcNvRam                      */
/*                                                                          */
/*    Description        : This function is invoked to set the value of     */
/*                         DTLS Key Type to the WLC NvRam.                  */
/*                                                                          */
/*    Input(s)           : DTLS Key Type                                    */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
WssSetDtlsKeyTypeToWlcNvRam (UINT4 u4DtlsKeyType)
{
    sWlcNvRamData.u4DtlsKeyType = u4DtlsKeyType;

    if (WlcNvRamWrite (&sWlcNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to WLC NvRam\n");
        return;
    }
    return;
}

#endif
