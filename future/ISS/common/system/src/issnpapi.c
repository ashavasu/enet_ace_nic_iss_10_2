/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: issnpapi.c,v 1.1
 ******************************************************************************/

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSetPortEgressStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetPortEgressStatus
 *                                                                          
 *    Input(s)            : Arguments of IssHwSetPortEgressStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#ifndef _ISS_SYS_NP_API_C
#define ISS_SYS_NP_API_C

#include "issinc.h"
#include "nputil.h"

UINT1
IsssysIssHwSetPortEgressStatus (UINT4 u4IfIndex, UINT1 u1EgressEnable)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetPortEgressStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_PORT_EGRESS_STATUS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortEgressStatus;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1EgressEnable = u1EgressEnable;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSetPortStatsCollection                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetPortStatsCollection
 *                                                                          
 *    Input(s)            : Arguments of IssHwSetPortStatsCollection
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwSetPortStatsCollection (UINT4 u4IfIndex, UINT1 u1StatsEnable)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetPortStatsCollection *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_PORT_STATS_COLLECTION,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortStatsCollection;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1StatsEnable = u1StatsEnable;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSetPortMode                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetPortMode
 *                                                                          
 *    Input(s)            : Arguments of IssHwSetPortMode
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwSetPortMode (UINT4 u4IfIndex, INT4 i4Value)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetPortMode *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_PORT_MODE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortMode;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4Value = i4Value;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSetPortDuplex                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetPortDuplex
 *                                                                          
 *    Input(s)            : Arguments of IssHwSetPortDuplex
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwSetPortDuplex (UINT4 u4IfIndex, INT4 i4Value)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetPortDuplex *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_PORT_DUPLEX,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortDuplex;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4Value = i4Value;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSetPortSpeed                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetPortSpeed
 *                                                                          
 *    Input(s)            : Arguments of IssHwSetPortSpeed
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwSetPortSpeed (UINT4 u4IfIndex, INT4 i4Value)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetPortSpeed *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_PORT_SPEED,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortSpeed;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4Value = i4Value;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSetPortFlowControl                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetPortFlowControl
 *                                                                          
 *    Input(s)            : Arguments of IssHwSetPortFlowControl
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwSetPortFlowControl (UINT4 u4IfIndex, UINT1 u1FlowCtrlEnable)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetPortFlowControl *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_PORT_FLOW_CONTROL,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortFlowControl;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1FlowCtrlEnable = u1FlowCtrlEnable;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSetPortRenegotiate                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetPortRenegotiate
 *                                                                          
 *    Input(s)            : Arguments of IssHwSetPortRenegotiate
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwSetPortRenegotiate (UINT4 u4IfIndex, INT4 i4Value)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetPortRenegotiate *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_PORT_RENEGOTIATE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortRenegotiate;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4Value = i4Value;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSetPortMaxMacAddr                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetPortMaxMacAddr
 *                                                                          
 *    Input(s)            : Arguments of IssHwSetPortMaxMacAddr
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwSetPortMaxMacAddr (UINT4 u4IfIndex, INT4 i4Value)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetPortMaxMacAddr *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_PORT_MAX_MAC_ADDR,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortMaxMacAddr;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4Value = i4Value;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSetPortMaxMacAction                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetPortMaxMacAction
 *                                                                          
 *    Input(s)            : Arguments of IssHwSetPortMaxMacAction
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwSetPortMaxMacAction (UINT4 u4IfIndex, INT4 i4Value)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetPortMaxMacAction *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_PORT_MAX_MAC_ACTION,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortMaxMacAction;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4Value = i4Value;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSetPortMirroringStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetPortMirroringStatus
 *                                                                          
 *    Input(s)            : Arguments of IssHwSetPortMirroringStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwSetPortMirroringStatus (INT4 i4MirrorStatus)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetPortMirroringStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_PORT_MIRRORING_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortMirroringStatus;

    pEntry->i4MirrorStatus = i4MirrorStatus;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSetMirrorToPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetMirrorToPort
 *                                                                          
 *    Input(s)            : Arguments of IssHwSetMirrorToPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwSetMirrorToPort (UINT4 u4IfIndex)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetMirrorToPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_MIRROR_TO_PORT,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetMirrorToPort;

    pEntry->u4IfIndex = u4IfIndex;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSetIngressMirroring                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetIngressMirroring
 *                                                                          
 *    Input(s)            : Arguments of IssHwSetIngressMirroring
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwSetIngressMirroring (UINT4 u4IfIndex, INT4 i4IngMirroring)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetIngressMirroring *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_INGRESS_MIRRORING,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetIngressMirroring;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4IngMirroring = i4IngMirroring;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSetEgressMirroring                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetEgressMirroring
 *                                                                          
 *    Input(s)            : Arguments of IssHwSetEgressMirroring
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwSetEgressMirroring (UINT4 u4IfIndex, INT4 i4EgrMirroring)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetEgressMirroring *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_EGRESS_MIRRORING,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetEgressMirroring;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4EgrMirroring = i4EgrMirroring;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSetRateLimitingValue                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetRateLimitingValue
 *                                                                          
 *    Input(s)            : Arguments of IssHwSetRateLimitingValue
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#undef IsssysIssHwSetRateLimitingValue
UINT1
IsssysIssHwSetRateLimitingValue (UINT4 u4IfIndex, UINT1 u1PacketType,
                                 INT4 i4RateLimitVal)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetRateLimitingValue *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_RATE_LIMITING_VALUE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetRateLimitingValue;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1PacketType = u1PacketType;
    pEntry->i4RateLimitVal = i4RateLimitVal;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSetPortEgressPktRate                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetPortEgressPktRate
 *                                                                          
 *    Input(s)            : Arguments of IssHwSetPortEgressPktRate
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#undef IsssysIssHwSetPortEgressPktRate
UINT1
IsssysIssHwSetPortEgressPktRate (UINT4 u4IfIndex, INT4 i4PktRate,
                                 INT4 i4BurstRate)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetPortEgressPktRate *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_PORT_EGRESS_PKT_RATE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortEgressPktRate;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4PktRate = i4PktRate;
    pEntry->i4BurstRate = i4BurstRate;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwGetPortEgressPktRate                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwGetPortEgressPktRate
 *                                                                          
 *    Input(s)            : Arguments of IssHwGetPortEgressPktRate
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwGetPortEgressPktRate (UINT4 u4Port, INT4 *pi4PortPktRate,
                                 INT4 *pi4PortBurstRate)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwGetPortEgressPktRate *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_GET_PORT_EGRESS_PKT_RATE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwGetPortEgressPktRate;

    pEntry->u4Port = u4Port;
    pEntry->pi4PortPktRate = pi4PortPktRate;
    pEntry->pi4PortBurstRate = pi4PortBurstRate;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwRestartSystem                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwRestartSystem
 *                                                                          
 *    Input(s)            : Arguments of IssHwRestartSystem
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwRestartSystem ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_RESTART_SYSTEM,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwInitFilter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwInitFilter
 *                                                                          
 *    Input(s)            : Arguments of IssHwInitFilter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwInitFilter ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_INIT_FILTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwGetPortDuplex                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwGetPortDuplex
 *                                                                          
 *    Input(s)            : Arguments of IssHwGetPortDuplex
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwGetPortDuplex (UINT4 u4IfIndex, INT4 *pi4PortDuplexStatus)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwGetPortDuplex *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_GET_PORT_DUPLEX,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwGetPortDuplex;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pi4PortDuplexStatus = pi4PortDuplexStatus;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwGetPortSpeed                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwGetPortSpeed
 *                                                                          
 *    Input(s)            : Arguments of IssHwGetPortSpeed
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwGetPortSpeed (UINT4 u4IfIndex, INT4 *pi4PortSpeed)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwGetPortSpeed *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_GET_PORT_SPEED,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwGetPortSpeed;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pi4PortSpeed = pi4PortSpeed;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwGetPortFlowControl                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwGetPortFlowControl
 *                                                                          
 *    Input(s)            : Arguments of IssHwGetPortFlowControl
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwGetPortFlowControl (UINT4 u4IfIndex, INT4 *pi4PortFlowControl)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwGetPortFlowControl *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_GET_PORT_FLOW_CONTROL,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwGetPortFlowControl;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pi4PortFlowControl = pi4PortFlowControl;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSetPortHOLBlockPrevention                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetPortHOLBlockPrevention
 *                                                                          
 *    Input(s)            : Arguments of IssHwSetPortHOLBlockPrevention
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwSetPortHOLBlockPrevention (UINT4 u4IfIndex, INT4 i4Value)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetPortHOLBlockPrevention *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_PORT_H_O_L_BLOCK_PREVENTION,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortHOLBlockPrevention;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4Value = i4Value;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwGetPortHOLBlockPrevention                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwGetPortHOLBlockPrevention
 *                                                                          
 *    Input(s)            : Arguments of IssHwGetPortHOLBlockPrevention
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwGetPortHOLBlockPrevention (UINT4 u4IfIndex, INT4 *pi4Value)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwGetPortHOLBlockPrevention *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_GET_PORT_H_O_L_BLOCK_PREVENTION,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwGetPortHOLBlockPrevention;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pi4Value = pi4Value;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwUpdateL2Filter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwUpdateL2Filter
 *                                                                          
 *    Input(s)            : Arguments of IssHwUpdateL2Filter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#undef IsssysIssHwUpdateL2Filter
UINT1
IsssysIssHwUpdateL2Filter (tIssL2FilterEntry * pIssL2FilterEntry, INT4 i4Value)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwUpdateL2Filter *pEntry = NULL;
    tSNMP_OCTET_STRING_TYPE  IssAclL2FilterBkpInPortList ; 
    UINT1               au1InPortList[BRG_PHY_PORT_LIST_SIZE]; 
    UINT1               u1Return; 
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_UPDATE_L2_FILTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwUpdateL2Filter;

    pEntry->pIssL2FilterEntry = pIssL2FilterEntry;
    pEntry->i4Value = i4Value;
    MEMSET (au1InPortList, 0, BRG_PHY_PORT_LIST_SIZE); 
    IssAclL2FilterBkpInPortList.pu1_OctetList = &au1InPortList[0]; 
    IssAclL2FilterBkpInPortList.i4_Length = BRG_PHY_PORT_LIST_SIZE; 

    /*Copying the InPortList  into  local Octet List.*/ 
    MEMCPY (IssAclL2FilterBkpInPortList.pu1_OctetList, 
	    pIssL2FilterEntry->IssL2FilterInPortList, 
	    BRG_PHY_PORT_LIST_SIZE); 

    u1Return = NpUtilHwProgram (&FsHwNp); 

    ISS_MEMSET (pIssL2FilterEntry->IssL2FilterInPortList, 0, 
	    BRG_PHY_PORT_LIST_SIZE); 

    /* Copy the local portlist back to InPortList */ 
    MEMCPY (pIssL2FilterEntry->IssL2FilterInPortList, 
	    IssAclL2FilterBkpInPortList.pu1_OctetList, 
	    BRG_PHY_PORT_LIST_SIZE); 

    return (u1Return); 


}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwUpdateL3Filter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwUpdateL3Filter
 *                                                                          
 *    Input(s)            : Arguments of IssHwUpdateL3Filter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#undef IsssysIssHwUpdateL3Filter
UINT1
IsssysIssHwUpdateL3Filter (tIssL3FilterEntry * pIssL3FilterEntry, INT4 i4Value)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwUpdateL3Filter *pEntry = NULL;
    tSNMP_OCTET_STRING_TYPE  IssAclL3FilterBkpInPortList ; 
    UINT1               au1InPortList[BRG_PHY_PORT_LIST_SIZE]; 
    UINT1               u1Return; 
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_UPDATE_L3_FILTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwUpdateL3Filter;

    pEntry->pIssL3FilterEntry = pIssL3FilterEntry;
    pEntry->i4Value = i4Value;
    MEMSET (au1InPortList, 0, BRG_PHY_PORT_LIST_SIZE); 
    IssAclL3FilterBkpInPortList.pu1_OctetList = &au1InPortList[0]; 
    IssAclL3FilterBkpInPortList.i4_Length = BRG_PHY_PORT_LIST_SIZE; 

    /*Copying the InPortList into  local Octet List.*/ 
    MEMCPY (IssAclL3FilterBkpInPortList.pu1_OctetList, 
	    pIssL3FilterEntry->IssL3FilterInPortList, BRG_PHY_PORT_LIST_SIZE); 

    u1Return = NpUtilHwProgram (&FsHwNp); 

    ISS_MEMSET (pIssL3FilterEntry->IssL3FilterInPortList, 0, BRG_PHY_PORT_LIST_SIZE); 

    /* Copy the local portlist back to InPortList */ 
    MEMCPY (pIssL3FilterEntry->IssL3FilterInPortList, 
	    IssAclL3FilterBkpInPortList.pu1_OctetList, BRG_PHY_PORT_LIST_SIZE); 

    return (u1Return); 


}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwUpdateL4SFilter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwUpdateL4SFilter
 *                                                                          
 *    Input(s)            : Arguments of IssHwUpdateL4SFilter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwUpdateL4SFilter (tIssL4SFilterEntry * pIssL4SFilterEntry,
                            INT4 i4Value)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwUpdateL4SFilter *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_UPDATE_L4_S_FILTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwUpdateL4SFilter;

    pEntry->pIssL4SFilterEntry = pIssL4SFilterEntry;
    pEntry->i4Value = i4Value;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *
 *    Function Name       : IsssysIssHwResrvFrameFilter
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IsssysIssHwResrvFrameFilter
 *
 *    Input(s)            : Arguments of IsssysIssHwResrvFrameFilter
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
IsssysIssHwSetResrvFrameFilter (tIssReservFrmCtrlTable *pIssReservFrmCtrlInfo ,
                            INT4 i4Value)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetResrvFrameFilter *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_NP_HW_UPDATE_RESERV_FRAME_ACTION,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetResrvFrameFilter;

    pEntry->pIssReservFrmCtrlInfo = pIssReservFrmCtrlInfo;
    pEntry->i4Value = i4Value;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSendBufferToLinux                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSendBufferToLinux
 *                                                                          
 *    Input(s)            : Arguments of IssHwSendBufferToLinux
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwSendBufferToLinux (UINT1 *pBuffer, UINT4 u4Len, INT4 i4ImageType)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSendBufferToLinux *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SEND_BUFFER_TO_LINUX,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSendBufferToLinux;

    pEntry->pBuffer = pBuffer;
    pEntry->u4Len = u4Len;
    pEntry->i4ImageType = i4ImageType;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwGetFanStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwGetFanStatus
 *                                                                          
 *    Input(s)            : Arguments of IssHwGetFanStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwGetFanStatus (UINT4 u4FanIndex, UINT4 *pu4FanStatus)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwGetFanStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_GET_FAN_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwGetFanStatus;

    pEntry->u4FanIndex = u4FanIndex;
    pEntry->pu4FanStatus = pu4FanStatus;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwInitMirrDataBase                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwInitMirrDataBase
 *                                                                          
 *    Input(s)            : Arguments of IssHwInitMirrDataBase
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwInitMirrDataBase ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_INIT_MIRR_DATA_BASE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSetMirroring                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetMirroring
 *                                                                          
 *    Input(s)            : Arguments of IssHwSetMirroring
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwSetMirroring (tIssHwMirrorInfo * pMirrorInfo)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetMirroring *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_MIRRORING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetMirroring;

    pEntry->pMirrorInfo = pMirrorInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwMirrorAddRemovePort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwMirrorAddRemovePort
 *                                                                          
 *    Input(s)            : Arguments of IssHwMirrorAddRemovePort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwMirrorAddRemovePort (UINT4 u4SrcIndex, UINT4 u4DestIndex,
                                UINT1 u1Mode, UINT1 u1MirrCfg)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwMirrorAddRemovePort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_MIRROR_ADD_REMOVE_PORT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwMirrorAddRemovePort;

    pEntry->u4SrcIndex = u4SrcIndex;
    pEntry->u4DestIndex = u4DestIndex;
    pEntry->u1Mode = u1Mode;
    pEntry->u1MirrCfg = u1MirrCfg;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSetMacLearningRateLimit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetMacLearningRateLimit
 *                                                                          
 *    Input(s)            : Arguments of IssHwSetMacLearningRateLimit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwSetMacLearningRateLimit (INT4 i4IssMacLearnLimitVal)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetMacLearningRateLimit *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_MAC_LEARNING_RATE_LIMIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetMacLearningRateLimit;

    pEntry->i4IssMacLearnLimitVal = i4IssMacLearnLimitVal;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwGetLearnedMacAddrCount                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwGetLearnedMacAddrCount
 *                                                                          
 *    Input(s)            : Arguments of IssHwGetLearnedMacAddrCount
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwGetLearnedMacAddrCount (INT4 *pi4LearnedMacAddrCount)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwGetLearnedMacAddrCount *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_GET_LEARNED_MAC_ADDR_COUNT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwGetLearnedMacAddrCount;

    pEntry->pi4LearnedMacAddrCount = pi4LearnedMacAddrCount;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwUpdateUserDefinedFilter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwUpdateUserDefinedFilter
 *                                                                          
 *    Input(s)            : Arguments of IssHwUpdateUserDefinedFilter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwUpdateUserDefinedFilter (tIssUDBFilterEntry * pAccessFilterEntry,
                                    tIssL2FilterEntry * pL2AccessFilterEntry,
                                    tIssL3FilterEntry * pL3AccessFilterEntry,
                                    INT4 i4FilterAction)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwUpdateUserDefinedFilter *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_UPDATE_USER_DEFINED_FILTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwUpdateUserDefinedFilter;

    pEntry->pAccessFilterEntry = pAccessFilterEntry;
    pEntry->pL2AccessFilterEntry = pL2AccessFilterEntry;
    pEntry->pL3AccessFilterEntry = pL3AccessFilterEntry;
    pEntry->i4FilterAction = i4FilterAction;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSetPortAutoNegAdvtCapBits                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetPortAutoNegAdvtCapBits
 *                                                                          
 *    Input(s)            : Arguments of IssHwSetPortAutoNegAdvtCapBits
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwSetPortAutoNegAdvtCapBits (UINT4 u4IfIndex, INT4 *pi4MauCapBits,
                                      INT4 *pi4IssCapBits)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetPortAutoNegAdvtCapBits *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_PORT_AUTO_NEG_ADVT_CAP_BITS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortAutoNegAdvtCapBits;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pi4MauCapBits = pi4MauCapBits;
    pEntry->pi4IssCapBits = pi4IssCapBits;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwGetPortAutoNegAdvtCapBits                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwGetPortAutoNegAdvtCapBits
 *                                                                          
 *    Input(s)            : Arguments of IssHwGetPortAutoNegAdvtCapBits
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwGetPortAutoNegAdvtCapBits (UINT4 u4IfIndex, INT4 *pElement)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwGetPortAutoNegAdvtCapBits *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_GET_PORT_AUTO_NEG_ADVT_CAP_BITS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwGetPortAutoNegAdvtCapBits;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pElement = pElement;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSetLearningMode                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetLearningMode
 *                                                                          
 *    Input(s)            : Arguments of IssHwSetLearningMode
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwSetLearningMode (UINT4 u4IfIndex, INT4 i4Status)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetLearningMode *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_LEARNING_MODE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetLearningMode;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4Status = i4Status;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSetPortMdiOrMdixCap                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetPortMdiOrMdixCap
 *                                                                          
 *    Input(s)            : Arguments of IssHwSetPortMdiOrMdixCap
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwSetPortMdiOrMdixCap (UINT4 u4IfIndex, INT4 i4Value)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetPortMdiOrMdixCap *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_PORT_MDI_OR_MDIX_CAP,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortMdiOrMdixCap;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4Value = i4Value;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwGetPortMdiOrMdixCap                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwGetPortMdiOrMdixCap
 *                                                                          
 *    Input(s)            : Arguments of IssHwGetPortMdiOrMdixCap
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwGetPortMdiOrMdixCap (UINT4 u4IfIndex, INT4 *pi4PortStatus)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwGetPortMdiOrMdixCap *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_GET_PORT_MDI_OR_MDIX_CAP,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwGetPortMdiOrMdixCap;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pi4PortStatus = pi4PortStatus;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSetPortFlowControlRate                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetPortFlowControlRate
 *                                                                          
 *    Input(s)            : Arguments of IssHwSetPortFlowControlRate
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssHwSetPortFlowControlRate (UINT4 u4IfIndex, INT4 i4PortMaxRate,
                                   INT4 i4PortMinRate)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetPortFlowControlRate *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_PORT_FLOW_CONTROL_RATE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortFlowControlRate;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->i4PortMaxRate = i4PortMaxRate;
    pEntry->i4PortMinRate = i4PortMinRate;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

UINT1
IsssysIssHwEnableHwConsole (UINT1 *pu1HwCmd)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwEnableHwConsole *pEntry = NULL;
    UINT1               u1RetVal;

    NP_UTIL_FILL_PARAMS (FsHwNp,
                         NP_ISSSYS_MOD, ISS_HW_ENABLE_HW_CONSOLE, 0, 0, 0);

    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpWrIssHwEnableHwConsole;
    pEntry->pu1Buffer = pu1HwCmd;

    if ((u1RetVal = NpUtilHwProgramLocalUnit (&FsHwNp)) == FNP_FAILURE)
    {
        return (u1RetVal);
    }

    return (u1RetVal);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwConfigPortIsolationEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwConfigPortIsolationEntry
 *                                                                          
 *    Input(s)            : Arguments of IssHwConfigPortIsolationEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#undef IsssysIssHwConfigPortIsolationEntry
UINT1
IsssysIssHwConfigPortIsolationEntry (tIssHwUpdtPortIsolation * pPortIsolation)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwConfigPortIsolationEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_CONFIG_PORT_ISOLATION_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwConfigPortIsolationEntry;

    pEntry->pPortIsolation = pPortIsolation;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *
 *    Function Name       : IssIssHwSetSetTrafficSeperationCtrl
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetSetTrafficSeperationCtrl
 *
 *    Input(s)            : Arguments of IssHwSetSetTrafficSeperationCtrl
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
IsssysIssHwSetSetTrafficSeperationCtrl (INT4 i4CtrlStatus)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetSetTrafficSeperationCtrl *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_SET_TRAFFIC_SEPERATION_CTRL,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetSetTrafficSeperationCtrl;

    pEntry->i4CtrlStatus = i4CtrlStatus;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#ifdef NPAPI_WANTED
/***************************************************************************
 *  
 *       Function Name       : IsssysNpSetTraceLevel
 *    
 *       Description         : This function using its arguments populates the
 *                                generic NP structure and invokes NpUtilHwProgram
 *                                 The Generic NP wrapper after validating the H/W
 *                                  parameters invokes NpSetTraceLevel
 *         
 *       Input(s)            : Arguments of NpSetTraceLevel
 *           
 *       Output(s)           : None
 *             
 *       Global Var Referred : None
 *               
 *       Global Var Modified : None.
 *                 
 *       Use of Recursion    : None.
 *                   
 *       Exceptions or Operating
 *       System Error Handling    : None.
 *                      
 *       Returns            : FNP_SUCCESS OR FNP_FAILURE
 *                        
 ******************************************************************************/

UINT1
IsssysNpSetTraceLevel (UINT2 u2TraceLevel)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrNpSetTraceLevel *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         NP_SET_TRACE_LEVEL,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpNpSetTraceLevel;

    pEntry->u2TraceLevel = u2TraceLevel;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
/***************************************************************************
 *  
 *       Function Name       : IsssysNpGetTraceLevel
 *    
 *       Description         : This function using its arguments populates the
 *                              generic NP structure and invokes NpUtilHwProgram
 *                                 The Generic NP wrapper after validating the H/W
 *                                  parameters invokes NpGetTraceLevel
 *         
 *       Input(s)            : Arguments of NpGetTraceLevel
 *           
 *       Output(s)           : None
 *             
 *       Global Var Referred : None
 *               
 *       Global Var Modified : None.
 *                 
 *       Use of Recursion    : None.
 *                   
 *       Exceptions or Operating
 *       System Error Handling    : None.
 *                      
 *       Returns            : FNP_SUCCESS OR FNP_FAILURE
 *                        
 ******************************************************************************/

UINT1
IsssysNpGetTraceLevel (UINT2 *pu2TraceLevel)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrNpGetTraceLevel *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         NP_GET_TRACE_LEVEL,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpNpGetTraceLevel;

    pEntry->pu2TraceLevel = pu2TraceLevel;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif
/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSetSwitchModeType
 *
 *    Description         : This function will configure the switch in either
 *                          cut-through or store-forward
 *
 *    Input(s)            : Type of SwitchMode
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
IsssysIssHwSetSwitchModeType (INT4 i4IssSwitchModeType)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetSwitchModeType *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_HW_SET_SWITCH_MODE_TYPE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetSwitchModeType;

    pEntry->i4IssSwitchModeType = i4IssSwitchModeType;


    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysNpSetTrace                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes NpSetTraceLevel
 *                                                                          
 *    Input(s)            : Arguments of NpSetTraceLevel
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysNpSetTrace (UINT1 u1TraceModule, UINT1 u1TraceLevel)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrNpSetTrace *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         NP_SET_TRACE,    /* Function OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpNpSetTrace;

    pEntry->u1TraceModule = u1TraceModule;
    pEntry->u1TraceLevel = u1TraceLevel;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysNpGetTrace                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes NpGetTraceLevel
 *                                                                          
 *    Input(s)            : Arguments of NpGetTraceLevel
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysNpGetTrace (UINT1 u1TraceModule, UINT1 *pu1TraceLevel)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrNpGetTrace *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         NP_GET_TRACE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpNpGetTrace;

    pEntry->u1TraceModule = u1TraceModule;
    pEntry->pu1TraceLevel = pu1TraceLevel;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

#ifdef MBSM_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssMbsmHwSetPortEgressStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssMbsmHwSetPortEgressStatus
 *                                                                          
 *    Input(s)            : Arguments of IssMbsmHwSetPortEgressStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssMbsmHwSetPortEgressStatus (UINT4 u4IfIndex, UINT1 u1Status,
                                    tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssMbsmHwSetPortEgressStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_MBSM_HW_SET_PORT_EGRESS_STATUS,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwSetPortEgressStatus;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1Status = u1Status;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssMbsmHwSetPortStatsCollection                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssMbsmHwSetPortStatsCollection
 *                                                                          
 *    Input(s)            : Arguments of IssMbsmHwSetPortStatsCollection
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssMbsmHwSetPortStatsCollection (UINT4 u4IfIndex, UINT1 u1Status,
                                       tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssMbsmHwSetPortStatsCollection *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_MBSM_HW_SET_PORT_STATS_COLLECTION,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwSetPortStatsCollection;

    pEntry->u4IfIndex = u4IfIndex;
    pEntry->u1Status = u1Status;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssMbsmHwSetPortMirroringStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssMbsmHwSetPortMirroringStatus
 *                                                                          
 *    Input(s)            : Arguments of IssMbsmHwSetPortMirroringStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssMbsmHwSetPortMirroringStatus (INT4 i4MirrorStatus,
                                       tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssMbsmHwSetPortMirroringStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_MBSM_HW_SET_PORT_MIRRORING_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwSetPortMirroringStatus;

    pEntry->i4MirrorStatus = i4MirrorStatus;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssMbsmHwSetMirrorToPort                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssMbsmHwSetMirrorToPort
 *                                                                          
 *    Input(s)            : Arguments of IssMbsmHwSetMirrorToPort
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssMbsmHwSetMirrorToPort (UINT4 u4MirrorPort, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssMbsmHwSetMirrorToPort *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_MBSM_HW_SET_MIRROR_TO_PORT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwSetMirrorToPort;

    pEntry->u4MirrorPort = u4MirrorPort;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssMbsmHwUpdateL2Filter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssMbsmHwUpdateL2Filter
 *                                                                          
 *    Input(s)            : Arguments of IssMbsmHwUpdateL2Filter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssMbsmHwUpdateL2Filter (tIssL2FilterEntry * pIssL2FilterEntry,
                               INT4 i4Value, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssMbsmHwUpdateL2Filter *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_MBSM_HW_UPDATE_L2_FILTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwUpdateL2Filter;

    pEntry->pIssL2FilterEntry = pIssL2FilterEntry;
    pEntry->i4Value = i4Value;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssMbsmHwUpdateL3Filter                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssMbsmHwUpdateL3Filter
 *                                                                          
 *    Input(s)            : Arguments of IssMbsmHwUpdateL3Filter
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssMbsmHwUpdateL3Filter (tIssL3FilterEntry * pIssL3FilterEntry,
                               INT4 i4Value, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssMbsmHwUpdateL3Filter *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_MBSM_HW_UPDATE_L3_FILTER,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwUpdateL3Filter;

    pEntry->pIssL3FilterEntry = pIssL3FilterEntry;
    pEntry->i4Value = i4Value;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssPIMbsmHwConfigPortIsolationEntry                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssPIMbsmHwConfigPortIsolationEntry
 *                                                                          
 *    Input(s)            : Arguments of IssPIMbsmHwConfigPortIsolationEntry
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
IsssysIssPIMbsmHwConfigPortIsolationEntry (tIssHwUpdtPortIsolation *
                                           pHwUpdPortIsolation,
                                           tIssMbsmInfo * pIssMbsmInfo)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssPIMbsmHwConfigPortIsolationEntry *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_P_I_MBSM_HW_CONFIG_PORT_ISOLATION_ENTRY,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssPIMbsmHwConfigPortIsolationEntry;

    pEntry->pHwUpdPortIsolation = pHwUpdPortIsolation;
    pEntry->pIssMbsmInfo = pIssMbsmInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssMbsmHwSetCpuMirroring 
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssMbsmHwSetCpuMirroring
 *                                                                          
 *    Input(s)            : Arguments of IssMbsmHwCpuMirroring
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
UINT1
IsssysIssMbsmHwSetCpuMirroring (INT4 i4IssCpuMirrorType,  UINT4 u4IssCpuMirrorToPort,
                                            tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssMbsmHwSetCpuMirroring *pEntry = NULL;

    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_MBSM_HW_CPU_MIRRORING, /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         1,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwSetCpuMirroring;

    pEntry->i4IssCpuMirrorType = i4IssCpuMirrorType;
    pEntry->u4IssCpuMirrorToPort = u4IssCpuMirrorToPort;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
/*****************************************************************************/
/*    Function Name       : IsssysIssMbsmHwSetMirroring                      */
/*    Description         : This function using its arguments populates the  */
/*                          generic NP structure and invokes NpUtilHwProgram */
/*                          The Generic NP wrapper after validating the H/W  */
/*                          parameters invokes IssMbsmHwSetMirroring         */
/*    Input(s)            : Arguments of IssMbsmHwSetMirroring               */
/*    Output(s)           : None                                             */
/*    Global Var Referred : None                                             */
/*    Global Var Modified : None.                                            */
/*    Use of Recursion    : None.                                            */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*    Returns            : FNP_SUCCESS OR FNP_FAILURE                        */
/*                                                                           */
/*****************************************************************************/

UINT1
IsssysIssMbsmHwSetMirroring (tIssHwMirrorInfo * pMirrorInfo,
                                tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssMbsmHwSetMirroring *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ISSSYS_MOD,    /* Module ID */
                         ISS_MBSM_HW_SET_MIRRORING,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwSetMirroring;

    pEntry->pMirrorInfo = pMirrorInfo;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}



#endif
/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssNpHwGetCapabilities                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssNpHwGetCapabilities
 *                                                                          
 *    Input(s)            : Arguments of IssNpHwGetCapabilities
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1 IsssysIssNpHwGetCapabilities (tNpIssHwGetCapabilities * pHwGetCapabilities)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo     *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssNpHwGetCapabilities *pEntry = NULL;
    NP_UTIL_FILL_PARAMS(FsHwNp,      /*Generic NP structure */ 
                        NP_ISSSYS_MOD,   /* Module ID */
                        ISS_NP_HW_GET_CAPABILITIES,     /* Function/OpCode */
                        0,           /* IfIndex value if applicable */  
                        0,     /* No. of Port Params */
                        0); /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pEntry = &pIsssysNpModInfo->IsssysNpIssNpHwGetCapabilities;

    pEntry->pHwGetCapabilities = pHwGetCapabilities;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysIssHwSetCpuMirroring                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes IssHwSetCpuMirroring
 *                                                                          
 *    Input(s)            : Arguments of IssHwSetCpuMirroring
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
UINT1
IsssysIssHwSetCpuMirroring (INT4 i4IssCpuMirrorType,
                             UINT4 u4IssCpuMirrorToPort)
{
    tFsHwNp             FsHwNp;
    tIsssysNpModInfo     *pIsssysNpModInfo = NULL;
    tIsssysNpWrIssHwSetCpuMirroring *pIssHwSetCpuMirroring = NULL;

    NP_UTIL_FILL_PARAMS(FsHwNp,      /*Generic NP structure */ 
                        NP_ISSSYS_MOD,   /* Module ID */
                        ISS_HW_CPU_MIRRORING,     /* Function/OpCode */
                        0,           /* IfIndex value if applicable */  
                        0,     /* No. of Port Params */
                        0); /* No. of PortList Parms */
    pIsssysNpModInfo = &(FsHwNp.IsssysNpModInfo);
    pIssHwSetCpuMirroring = &pIsssysNpModInfo->IsssysNpWrIssHwSetCpuMirroring;
    pIssHwSetCpuMirroring->u4IssCpuMirrorToPort = u4IssCpuMirrorToPort;
    pIssHwSetCpuMirroring->i4IssCpuMirrorType = i4IssCpuMirrorType;
    
    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif /* _ISS_SYS_NP_API_C */
