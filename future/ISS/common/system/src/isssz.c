#define _SYSTEMSZ_C
#include "issinc.h"
INT4
SystemSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < SYSTEM_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsSYSTEMSizingParams[i4SizingId].u4StructSize,
                              FsSYSTEMSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(SYSTEMMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            SystemSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
SystemSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsSYSTEMSizingParams);
    return OSIX_SUCCESS;
}

VOID
SystemSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < SYSTEM_MAX_SIZING_ID; i4SizingId++)
    {
        if (SYSTEMMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (SYSTEMMemPoolIds[i4SizingId]);
            SYSTEMMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
