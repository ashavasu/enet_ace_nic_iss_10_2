/*****************************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: l4scli.c,v 1.12 2013/07/03 12:19:56 siva Exp $
 * 
 *******************************************************************************/

/************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                 */
/* Licensee Aricent Inc., 2004-2005       */
/*                                                          */
/*  FILE NAME             : aclcli.c                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                 */
/*  SUBSYSTEM NAME        : ISS                             */
/*  MODULE NAME           : CLI                             */
/*  LANGUAGE              : C                               */
/*  TARGET ENVIRONMENT    :                                 */
/*  DATE OF FIRST RELEASE :                                 */
/*  AUTHOR                : Aricent Inc.                 */
/*  DESCRIPTION           : This file contains CLI routines */
/*                          related to system commands      */
/*                                                          */
/************************************************************/

#ifndef __L4SCLI_C__
#define __L4SCLI_C__

#include "lr.h"
#include "issexinc.h"
#include "fsisswr.h"
#include "isslow.h"
#include "l4scli.h"
#include "issmacro.h"
#include "isscli.h"

/***************************************************************/
/*  Function Name   : cli_process_l4s_command                      */
/*  Description     : This function servers as the handler for */
/*                    all system related CLI commands          */
/*  Input(s)        :                                          */
/*                    CliHandle - CLI Handle                   */
/*                    u4Command - Command given by user        */
/*                    Variable set of inputs depending on      */
/*                    the user command                         */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : None                                     */
/***************************************************************/
INT4
cli_process_l4s_command (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[CLI_L4S_MAX_ARGS];
    INT1                argno = 0;
    UINT1              *pu1Inst = NULL;
    INT4                i4RetStatus = CLI_SUCCESS;

    va_start (ap, u4Command);

    /* Third arguement is always interface name/index */
    pu1Inst = va_arg (ap, UINT1 *);
    UNUSED_PARAM (*pu1Inst);

    /* Walk through the rest of the arguements and store in args array.  */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == CLI_L4S_MAX_ARGS)
            break;
    }

    CLI_SET_ERR (0);

    va_end (ap);

    CliRegisterLock (CliHandle, IssLock, IssUnLock);

    ISS_LOCK ();

    switch (u4Command)
    {
        case CLI_L4S_ADD:
            i4RetStatus =
                L4SCreateFilter (CliHandle, CLI_PTR_TO_I4 (args[0]),
                                 CLI_PTR_TO_U4 (args[1]),
                                 CLI_PTR_TO_U4 (args[2]),
                                 CLI_PTR_TO_U4 (args[3]));
            break;
        case CLI_L4S_DELETE:
            i4RetStatus = L4SDeleteFilter (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;
        case CLI_L4S_SHOW:
            i4RetStatus = L4SShowFilter (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;
        default:
            break;
    }

    if (i4RetStatus == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Error in Layer4 Switch Filters\r\n");
    }

    ISS_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : L4SCreateFilter                                   */
/*                                                                          */
/*     DESCRIPTION      : This function will create Rule for L4 Switching   */
/*                                                                          */
/*     INPUT            : Protcol Number                                    */
/*              Port Number                                       */
/*              Interface Index                                   */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
L4SCreateFilter (tCliHandle CliHandle, INT4 i4FilterNo, UINT4 u4ProtoNo,
                 UINT4 u4PortNo, UINT4 u4IfIndex)
{
    INT4                i4Status = 0;
    UINT4               u4ErrCode;

    if (nmhGetIssL4SwitchingFilterStatus (i4FilterNo, &i4Status) ==
        SNMP_SUCCESS)
    {
        /* Already one Filter exist */
        CliPrintf (CliHandle, "\r%% Already one filter exist \r\n");
        return (CLI_FAILURE);
    }
    /* Addition of New entry */
    if (nmhTestv2IssL4SwitchingFilterStatus (&u4ErrCode,
                                             i4FilterNo,
                                             ISS_CREATE_AND_WAIT) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssL4SwitchingFilterStatus (i4FilterNo,
                                          ISS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    /* Setting Protocol Number */
    if (nmhTestv2IssL4SwitchingProtocol (&u4ErrCode,
                                         i4FilterNo,
                                         (INT4) u4ProtoNo) == SNMP_SUCCESS)
    {
        if (nmhSetIssL4SwitchingProtocol (i4FilterNo, (INT4) u4ProtoNo) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    /* Setting Port Number */
    if (nmhTestv2IssL4SwitchingPortNo (&u4ErrCode,
                                       i4FilterNo, u4PortNo) == SNMP_SUCCESS)
    {
        if (nmhSetIssL4SwitchingPortNo (i4FilterNo, u4PortNo) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    /* Setting Copy to port Field */
    if (nmhTestv2IssL4SwitchingCopyToPort (&u4ErrCode,
                                           i4FilterNo,
                                           (INT4) u4IfIndex) == SNMP_SUCCESS)
    {
        if (nmhSetIssL4SwitchingCopyToPort (i4FilterNo, (INT4) u4IfIndex)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    /* Now all the fields are set.. Go on for writing in Hardware */
    if (nmhSetIssL4SwitchingFilterStatus (i4FilterNo, ISS_ACTIVE) ==
        SNMP_FAILURE)
    {
        nmhSetIssL4SwitchingFilterStatus (i4FilterNo, ISS_DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : L4SDeleteFilter                                   */
/*                                                                          */
/*     DESCRIPTION      : This function will Delete the entry for L4Switchng*/
/*                                                                          */
/*     INPUT            : Filter  Number                                    */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
L4SDeleteFilter (tCliHandle CliHandle, INT4 i4FilterNo)
{
    INT4                i4Status = 0;
    UINT4               u4ErrCode;

    if (nmhGetIssL4SwitchingFilterStatus (i4FilterNo, &i4Status) ==
        SNMP_FAILURE)
    {
        /* Filter does not exist */
        CliPrintf (CliHandle, "\r%%Filter doesn't exist \r\n");
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssL4SwitchingFilterStatus (&u4ErrCode,
                                             i4FilterNo,
                                             ISS_DESTROY) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssL4SwitchingFilterStatus (i4FilterNo, ISS_DESTROY) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : L4SShowFilter                                      */
/*                                                                           */
/*     DESCRIPTION      : This function displays all the filters conifgured  */
/*                        for layer 4 switching                              */
/*                                                                           */
/*     INPUT            : CliHandle - CLI HAndler                            */
/*              i4FilterNo - FilterNO                              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : Success/Faliure                                    */
/*****************************************************************************/
INT4
L4SShowFilter (tCliHandle CliHandle, INT4 i4FilterNo)
{
    INT4                i4PrevFilter;
    UINT4               u4PortNo;
    INT4                i4CopyToPort;
    INT4                i4Protocol;
    INT4                i4TempFilterNo;
    UINT4               u4ProtoString;
    UINT4               u4PortString;

    if (i4FilterNo == 0)        /* Show All */
    {
        if (nmhGetFirstIndexIssL4SwitchingFilterTable (&i4FilterNo) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\rNo Layer 4 Filters are configured\r\n");
            return CLI_FAILURE;
        }

        CliPrintf (CliHandle, "\r\nLayer 4 Switching Filter \r\n");
        CliPrintf (CliHandle, "\r----------------------------\r\n");
        CliPrintf (CliHandle, "\r\n Protocol \t PortNo \t CopyToPort\r");
        CliPrintf (CliHandle, "\r\n -------- \t ------ \t ----------\r");
        do
        {
            nmhGetIssL4SwitchingProtocol (i4FilterNo, &i4Protocol);
            nmhGetIssL4SwitchingPortNo (i4FilterNo, &u4PortNo);
            nmhGetIssL4SwitchingCopyToPort (i4FilterNo, &i4CopyToPort);

            /* For Converting Protocol Numbers to String */
            switch (i4Protocol)
            {
                case ISS_TCP:
                    SPRINTF ((CHR1 *) & u4ProtoString, "%s", "TCP");
                    break;
                case ISS_UDP:
                    SPRINTF ((CHR1 *) & u4ProtoString, "%s", "UDP");
                    break;
                case ISS_PROTO_ANY:
                    SPRINTF ((CHR1 *) & u4ProtoString, "%s", "ANY");
                    break;
                default:
                    SNPRINTF ((CHR1 *) & u4ProtoString, sizeof (UINT4),
                              "%d", i4Protocol);
                    break;
            }

            /* For Converting Port Numbers to String */
            switch (u4PortNo)
            {
                case ISS_PROTO_ANY:
                    SPRINTF ((CHR1 *) & u4PortString, "%s", "ANY");
                    break;
                default:
                    SNPRINTF ((CHR1 *) & u4PortString, sizeof (UINT4),
                              "%u", u4PortNo);
                    break;
            }

            CliPrintf (CliHandle, "\r\n  %-8s  \t  %-8s  \t      %-8d  \r",
                       &u4ProtoString, &u4PortString, i4CopyToPort);

            i4PrevFilter = i4FilterNo;
        }
        while ((INT4) (nmhGetNextIndexIssL4SwitchingFilterTable
                       (i4PrevFilter, &i4FilterNo) != SNMP_FAILURE));
        CliPrintf (CliHandle, "\r\n");
        return CLI_SUCCESS;
    }

    else                        /* Show for Particular Entry */
    {
        if (nmhValidateIndexInstanceIssL4SwitchingFilterTable (i4FilterNo) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r Invalid Interface No \r\n");
            return CLI_FAILURE;
        }

        if (nmhGetFirstIndexIssL4SwitchingFilterTable (&i4TempFilterNo) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r Invalid Interface No \r\n");
            return CLI_FAILURE;
        }

        do
        {
            if (i4TempFilterNo == i4FilterNo)
            {
                nmhGetIssL4SwitchingProtocol (i4FilterNo, &i4Protocol);
                nmhGetIssL4SwitchingPortNo (i4FilterNo, &u4PortNo);
                nmhGetIssL4SwitchingCopyToPort (i4FilterNo, &i4CopyToPort);

                /* For Converting Protocol Numbers to String */
                switch (i4Protocol)
                {
                    case ISS_TCP:
                        SPRINTF ((CHR1 *) & u4ProtoString, "%s", "TCP");
                        break;
                    case ISS_UDP:
                        SPRINTF ((CHR1 *) & u4ProtoString, "%s", "UDP");
                        break;
                    case ISS_PROTO_ANY:
                        SPRINTF ((CHR1 *) & u4ProtoString, "%s", "ANY");
                        break;
                    default:
                        SNPRINTF ((CHR1 *) & u4ProtoString, sizeof (UINT4),
                                  "%d", i4Protocol);
                        break;
                }

                /* For Converting Port Numbers to String */
                switch (u4PortNo)
                {
                    case ISS_PROTO_ANY:
                        SPRINTF ((CHR1 *) & u4PortString, "%s", "ANY");
                        break;
                    default:
                        SNPRINTF ((CHR1 *) & u4PortString, sizeof (UINT4),
                                  "%u", u4PortNo);
                        break;
                }
                CliPrintf (CliHandle, "\r\nLayer 4 Switching Filter \r\n");
                CliPrintf (CliHandle, "\r----------------------------\r\n");
                CliPrintf (CliHandle, "\r\n Filter Number :%d\r", i4FilterNo);
                CliPrintf (CliHandle, "\r\n -----------------\r");
                CliPrintf (CliHandle,
                           "\r\n Protocol \t PortNo \t CopyToPort\r");
                CliPrintf (CliHandle,
                           "\r\n -------- \t ------ \t ----------\r");
                CliPrintf (CliHandle, "\r\n  %-8s  \t  %-8s  \t      %-8d  \r",
                           &u4ProtoString, &u4PortString, i4CopyToPort);
                CliPrintf (CliHandle, "\r\n");
                return CLI_SUCCESS;
            }

            i4PrevFilter = i4TempFilterNo;
        }
        while ((INT4) (nmhGetNextIndexIssL4SwitchingFilterTable
                       (i4PrevFilter, &i4TempFilterNo) != SNMP_FAILURE));
    }
    return CLI_FAILURE;
}
#endif
