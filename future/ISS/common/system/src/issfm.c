/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: issfm.c,v 1.26 2016/06/29 11:13:25 siva Exp $
 *
 * Description: This file contains the iss fault management routines 
 *              to check and raise a fault whenever flash, cpu, memory, 
 *              temperature, RAM and power are crossing the threshold.
 *              Similarly Fan status change is intimated to user
 *
 *******************************************************************/

#ifndef _ISSFM_C
#define _ISSFM_C

#include "issinc.h"
#include "fm.h"

#ifdef NPAPI_WANTED
#include "issnp.h"

PRIVATE VOID IssCheckCurrentFanStatus PROTO ((UINT4));
PRIVATE VOID IssCheckCurrentPowerStatus PROTO ((VOID));
PRIVATE VOID IssCheckCurrentTemperatureStatus PROTO ((VOID));
#endif

PRIVATE VOID IssCheckCurrentRAMUsage PROTO ((VOID));
PRIVATE VOID IssCheckCurrentCPUUsage PROTO ((VOID));
PRIVATE VOID IssCheckCurrentFlashUsage PROTO ((VOID));

static UINT1        gu1MemStatus = OSIX_FALSE;
static UINT1        gu1CPUStatus = OSIX_FALSE;
static UINT1        gu1FlashStatus = OSIX_FALSE;

#ifdef NPAPI_WANTED
static UINT1        gu1PowerStatus = OSIX_FALSE;
static UINT1        gu1TemperatureStatus = OSIX_FALSE;
static UINT1        gu1FanStatus = OSIX_FALSE;
#endif

tTmrAppTimer        gIssFmSysPollTmr;
extern VOID         AclProcessQMsgEvent (VOID);

/* ISS Functions for TRAP Support */
tSNMP_OID_TYPE     *IssMakeObjIdFromDotNew (INT1 *pi1textStr);

/* SysLog message for various traps */
CHR1               *gau1LogMsg[] = {
    NULL,
    "RAMUsage",
    "FanStatus",
    "CPUUsage",
    "Temperature",
    "PowerSupply",
    "FlashUsage"
};

/****************************************************************************
 *
 *    FUNCTION NAME    : IssSystMainTask

 *    DESCRIPTION      : Entry point function of System task.
 *
 *    INPUT            : pArg - Pointer to the arguments
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/

PUBLIC VOID
IssSysMainTask (INT1 *pi1Args)
{
    UINT4               u4EventsRecvd = 0;

    UNUSED_PARAM (pi1Args);

    ISS_TRC (DUMP_TRC, "Entering the function IssSystMainTask \r\n");
    IssCustGetSysMonReqStatus () ;


    /* Get The task Id for FS system  */
    ISS_SYS_GET_TASK_ID (&(ISS_SYS_TASK_ID));

    lrInitComplete (OSIX_SUCCESS);

    /* Timer Initialization */
    if (IssSystTmrInit () == OSIX_FAILURE)
    {
        ISS_TRC (DUMP_TRC,
                 "Timer Initialization Failed in IssSystMainTask \r\n");
        IssDeInitMacLearnLimitParams ();
        ISS_INIT_COMPLETE (OSIX_FAILURE);
        TmrDeleteTimerList (ISS_SYS_TMR_LIST_ID);
        TmrDeleteTimerList (ISS_MAC_LEARN_LIMIT_TMRID);
        return;
    }

    /* Timer is started only if the System Capabilities are Suppported */
    if ((gIssSysCapSupport.u1TempStatusSupport != OSIX_FALSE) ||
            (gIssSysCapSupport.u1FanStatusSupport != OSIX_FALSE) || 
            (gIssSysCapSupport.u1RAMStatusSupport != OSIX_FALSE) || 
            (gIssSysCapSupport.u1CPUStatusSupport != OSIX_FALSE) || 
            (gIssSysCapSupport.u1FlashStatusSupport != OSIX_FALSE) ||
            (gIssSysCapSupport.u1PowerStatusSupport != OSIX_FALSE))
    {
    TmrStartTimer (ISS_SYS_TMR_LIST_ID, &gIssFmSysPollTmr,
                   ISS_SYSTEM_POLL_DURATION * SYS_TIME_TICKS_IN_A_SEC);
    }

    IssMacLearnLimitStartTmr ();

    while (OSIX_TRUE)
    {
        if (ISS_SYS_RECEIVE_EVENT (ISS_SYS_TASK_ID,
                                   ISS_SYS_TIMER_EXP_EVENT | ISS_ACL_MSG_EVENT
                                   | ISS_RED_MSG_EVENT |
                                   ISS_MAC_LEARN_RATE_LIMIT_EXP_EVENT,
                                   OSIX_WAIT, &u4EventsRecvd) == OSIX_SUCCESS)
        {
            ISS_LOCK ();
            if (u4EventsRecvd & ISS_ACL_MSG_EVENT)
            {
                /* Process the message posted from ACL module to the 
                   ISS task on receiving this event. */
                AclProcessQMsgEvent ();
            }

            if (u4EventsRecvd & ISS_MAC_LEARN_RATE_LIMIT_EXP_EVENT)
            {
                /* Restart the Mac Learn rate limit timer when ISSU maintenance
                 * mode is enabled */
                if (IssuGetMaintModeOperation () == OSIX_TRUE)
                {
                    IssMacLearnLimitStartTmr ();
                }
                else
                {
                    IssMacLearnLimitProcessTimeout ();
                }
            }

            if (u4EventsRecvd & ISS_SYS_TIMER_EXP_EVENT)
            {
                /* Restart the Fan Status timer when ISSU maintenance
                 * mode is enabled */
                if (IssuGetMaintModeOperation () == OSIX_TRUE)
                {
                    TmrStartTimer (ISS_SYS_TMR_LIST_ID, &gIssFmSysPollTmr,
                                   ISS_SYSTEM_POLL_DURATION *
                                   SYS_TIME_TICKS_IN_A_SEC);
                }
                else
                {
                    IssSysTmrExpHandler ();
                }
            }
#ifdef L2RED_WANTED
            if (u4EventsRecvd & ISS_RED_MSG_EVENT)
            {
                /* Process the ISS redundancy message posted 
                 * on receiving this event. 
                 */
                IssRedProcessQMsgEvent ();
            }
#endif /* L2RED_WANTED */
            ISS_UNLOCK ();
        }
    }

}

/****************************************************************************
 *
 *    FUNCTION NAME    : IssSystTmrInit
 *
 *    DESCRIPTION      : This function creates a timer list.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 ****************************************************************************/

PUBLIC INT4
IssSystTmrInit (VOID)
{

    ISS_TRC (DUMP_TRC, "Entering the function IssSystTmrInit \r\n");

    if (TmrCreateTimerList (ISS_SYS_TASK_NAME, ISS_SYS_TIMER_EXP_EVENT,
                            NULL, &(ISS_SYS_TMR_LIST_ID)) == TMR_FAILURE)
    {
        ISS_TRC_ARG1 (DUMP_TRC, "Timer list creation FAILED ID = %d!!!\n",
                      ISS_SYS_TMR_LIST_ID);
        lrInitComplete (OSIX_FAILURE);
        return (OSIX_FAILURE);
    }

    if (TmrCreateTimerList (ISS_SYS_TASK_NAME,
                            ISS_MAC_LEARN_RATE_LIMIT_EXP_EVENT,
                            NULL, &(ISS_MAC_LEARN_LIMIT_TMRID)) == TMR_FAILURE)
    {
        ISS_TRC_ARG1 (DUMP_TRC, "Timer list creation FAILED ID = %d!!!\n",
                      ISS_MAC_LEARN_LIMIT_TMRID);
        return (OSIX_FAILURE);
    }

    ISS_TRC (DUMP_TRC, "Exiting the function IssSystTmrInit \r\n");

    return (OSIX_SUCCESS);
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IssSysTmrExpHandler
 *
 *    DESCRIPTION      : This function is called whenever a timer expiry
 *                       message is received by system task.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC VOID
IssSysTmrExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTmr = NULL;
    UINT1               u1FanIndex = 0;

    ISS_TRC (DUMP_TRC, "Entering the function IssSystTmrExpHandler \r\n");
    if ((pExpiredTmr = TmrGetNextExpiredTimer (ISS_SYS_TMR_LIST_ID)) != NULL)
    {
        if (gIssSysCapSupport.u1RAMStatusSupport == OSIX_TRUE)
        {
        /* Check for RAM usage of the system */
        IssCheckCurrentRAMUsage ();
        }

        if (gIssSysCapSupport.u1CPUStatusSupport == OSIX_TRUE)
        {
        /* Check for CPU utilization of the system */
        IssCheckCurrentCPUUsage ();
        }

        if (gIssSysCapSupport.u1FlashStatusSupport == OSIX_TRUE)
        {
        /* Check for Flash utilization of the system */
        IssCheckCurrentFlashUsage ();
        }

#ifdef NPAPI_WANTED
        if (gIssSysCapSupport.u1FanStatusSupport == OSIX_TRUE)
        {
        for (u1FanIndex = 1; u1FanIndex <= ISS_SWITCH_MAX_FAN; u1FanIndex++)
        {
            /* Check for fan status of the system */
            IssCheckCurrentFanStatus (u1FanIndex);
        }
        }
        if (gIssSysCapSupport.u1PowerStatusSupport == OSIX_TRUE)
        {
        /* Check for Power status of the system */
        IssCheckCurrentPowerStatus ();
        }
        if (gIssSysCapSupport.u1TempStatusSupport == OSIX_TRUE)
        {
        /* Check for temperature status of the system */
        IssCheckCurrentTemperatureStatus ();
        }
#else
        UNUSED_PARAM (u1FanIndex);
#endif

        TmrStartTimer (ISS_SYS_TMR_LIST_ID, pExpiredTmr,
                       ISS_SYSTEM_POLL_DURATION * SYS_TIME_TICKS_IN_A_SEC);
    }

    ISS_TRC (DUMP_TRC, "Exiting the function IssSystTmrExpHandler \r\n");
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IssCheckCurrentRAMUsage
 *
 *    DESCRIPTION      : Function that checks the free memory, if it is not 
 *                       with in the allowed limit,
 *                       SNMP trap will be generated.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
IssCheckCurrentRAMUsage (VOID)
{
    UINT1               u1TrapType = ISS_SYS_TRAP_RAM_THRESHOLD;
    tIssSystTrapInfo    IssSystTrapInfo;

    MEMSET (&IssSystTrapInfo.ThresholdTrap, 0,
            sizeof (IssSystTrapInfo.ThresholdTrap));
    MEMSET (&IssSystTrapInfo.InfoTrap, 0, sizeof (IssSystTrapInfo.InfoTrap));

    ISS_TRC (DUMP_TRC, "Entering the function IssSysCheckCurrentRAMUsage \r\n");

    if (IssGetMemInfo () != ISS_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "Failed to get Mem info\r\n");
        return;
    }

    /* Check whether current free memory is out of range and
     * whether trap was generated previously */
    if (gIssSysGroupInfo.IssSwitchInfo.u4CurrRAMUsage >
        gIssSysGroupInfo.IssSwitchThreshold.u4MaxRAMThreshold)
    {
        if (gu1MemStatus == OSIX_FALSE)
        {
            /* Send SNMP Trap Message */
            IssSystTrapInfo.InfoTrap.u4CurrRAMUsage =
                gIssSysGroupInfo.IssSwitchInfo.u4CurrRAMUsage;
            IssSystTrapInfo.ThresholdTrap.u4MaxRAMThreshold =
                gIssSysGroupInfo.IssSwitchThreshold.u4MaxRAMThreshold;

            IssSendTrapAndLog (&IssSystTrapInfo, u1TrapType);
            gu1MemStatus = OSIX_TRUE;
        }
    }
    else if (gIssSysGroupInfo.IssSwitchInfo.u4CurrRAMUsage <=
             gIssSysGroupInfo.IssSwitchThreshold.u4MaxRAMThreshold)
    {
        gu1MemStatus = OSIX_FALSE;
    }

    ISS_TRC (DUMP_TRC, "Exiting the function IssSystCheckCurrentRAM \r\n");
}

#ifdef NPAPI_WANTED
/****************************************************************************
 *
 *    FUNCTION NAME    : IssCheckCurrentFanStatus
 *
 *    DESCRIPTION      : Function that checks the fan status, if it is down,
 *                       SNMP trap will be generated.
 *
 *    INPUT            : u4FanIndex - Fan Index
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
IssCheckCurrentFanStatus (UINT4 u4FanIndex)
{
    UINT1               u1TrapType = ISS_SYS_TRAP_FAN_STATUS;
    tIssSystTrapInfo    IssSystTrapInfo;

    MEMSET (&IssSystTrapInfo.FanStatusTrap, 0,
            sizeof (IssSystTrapInfo.FanStatusTrap));

    ISS_TRC (DUMP_TRC, "Entering the function IssCheckCurrentFanStatus \r\n");

#ifdef NPAPI_WANTED
    if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
    {
        if (IsssysIssHwGetFanStatus (u4FanIndex,
                                     &gIssSysGroupInfo.IssSwitchFanState.
                                     u4CurrFanStatus) != FNP_SUCCESS)
        {
            ISS_TRC (ALL_FAILURE_TRC, "Failed to get fan status\n");
            return;
        }
    }
    else
    {
        return;
    }
#endif

    /* Check whether current fan status is down and
     * whether trap was generated previously */
    if (gIssSysGroupInfo.IssSwitchFanState.u4CurrFanStatus == 0)
    {
        if (gu1FanStatus == OSIX_FALSE)
        {
            /* Send SNMP Trap Message */
            IssSystTrapInfo.FanStatusTrap.u4FanIndex = u4FanIndex;
            gIssSysGroupInfo.IssSwitchFanState.u4FanIndex = u4FanIndex;
            IssSystTrapInfo.FanStatusTrap.u4CurrFanStatus =
                gIssSysGroupInfo.IssSwitchFanState.u4CurrFanStatus;

            IssSendTrapAndLog (&IssSystTrapInfo, u1TrapType);
            gu1FanStatus = OSIX_TRUE;
        }
    }
    else if (gIssSysGroupInfo.IssSwitchFanState.u4CurrFanStatus != 0)
    {
        gu1FanStatus = OSIX_FALSE;
    }

    ISS_TRC (DUMP_TRC, "Exiting the function IssCheckCurrentFanStatus\r\n");
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IssCheckCurrentTemperatureStatus
 *
 *    DESCRIPTION      : Function that checks the current temperature in celsius, 
 *                       if it is not in the allowed threshold limit, 
 *                       SNMP trap will be generated.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
IssCheckCurrentTemperatureStatus (VOID)
{
    UINT1               u1TrapType = ISS_SYS_TRAP_TEMPERATURE_THRES;
    tIssSystTrapInfo    IssSystTrapInfo;

    MEMSET (&IssSystTrapInfo.ThresholdTrap, 0,
            sizeof (IssSystTrapInfo.ThresholdTrap));
    MEMSET (&IssSystTrapInfo.InfoTrap, 0, sizeof (IssSystTrapInfo.InfoTrap));

    ISS_TRC (DUMP_TRC, "\rEntering the function"
             " IssCheckCurrentTemperatureStatus\r\n");

#ifdef NPAPI_WANTED
    if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
    {
        if (IssHwGetCurrTemperature
            (&gIssSysGroupInfo.IssSwitchInfo.i4CurrTemperature) != FNP_SUCCESS)
        {
            ISS_TRC (ALL_FAILURE_TRC, "\rFailed to get current Temperature\n");
            return;
        }
    }
#endif

    /* Check whether current temperature is out of range and
     * whether trap was generated previously; which has to be
     * verified with previous max and min temperatures */
    if ((gIssSysGroupInfo.IssSwitchInfo.i4CurrTemperature >
         gIssSysGroupInfo.IssSwitchThreshold.i4MaxThresholdTemp) ||
        (gIssSysGroupInfo.IssSwitchInfo.i4CurrTemperature <
         gIssSysGroupInfo.IssSwitchThreshold.i4MinThresholdTemp))
    {
        if (gu1TemperatureStatus == OSIX_FALSE)
        {
            /* Send SNMP Trap Message */
            IssSystTrapInfo.InfoTrap.i4CurrTemperature =
                gIssSysGroupInfo.IssSwitchInfo.i4CurrTemperature;
            IssSystTrapInfo.ThresholdTrap.i4MinThresholdTemp =
                gIssSysGroupInfo.IssSwitchThreshold.i4MinThresholdTemp;
            IssSystTrapInfo.ThresholdTrap.i4MaxThresholdTemp =
                gIssSysGroupInfo.IssSwitchThreshold.i4MaxThresholdTemp;

            IssSendTrapAndLog (&IssSystTrapInfo, u1TrapType);
            gu1TemperatureStatus = OSIX_TRUE;
        }
    }
    else if ((gIssSysGroupInfo.IssSwitchInfo.i4CurrTemperature <=
              gIssSysGroupInfo.IssSwitchThreshold.i4MaxThresholdTemp) &&
             (gIssSysGroupInfo.IssSwitchInfo.i4CurrTemperature >=
              gIssSysGroupInfo.IssSwitchThreshold.i4MinThresholdTemp))
    {
        gu1TemperatureStatus = OSIX_FALSE;
    }

    ISS_TRC (DUMP_TRC, "Exiting the function"
             " IssCheckCurrentTemperatureStatus\r\n");
}

/****************************************************************************
 *
 *    FUNCTION NAME    : IssCheckCurrentPowerStatus
 *
 *    DESCRIPTION      : Function that checks the current power supply in volts, 
 *                       if it is not in the allowed limit, 
 *                       SNMP trap will be generated.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PRIVATE VOID
IssCheckCurrentPowerStatus (VOID)
{
    UINT1               u1TrapType = ISS_SYS_TRAP_POWER_STATUS;
    tIssSystTrapInfo    IssSystTrapInfo;

    MEMSET (&IssSystTrapInfo.ThresholdTrap, 0,
            sizeof (IssSystTrapInfo.ThresholdTrap));
    MEMSET (&IssSystTrapInfo.InfoTrap, 0, sizeof (IssSystTrapInfo.InfoTrap));

    ISS_TRC (DUMP_TRC,
             "\rEntering the function IssCheckCurrentPowerStatus\r\n");

#ifdef NPAPI_WANTED
    if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
    {
        if (IssHwGetCurrPowerSupply
            (&gIssSysGroupInfo.IssSwitchInfo.u4CurrPowerSupply) != FNP_SUCCESS)
        {
            ISS_TRC (ALL_FAILURE_TRC, "\rFailed to get current Power supply\n");
            return;
        }
    }
#endif

    /* Check whether current power supply is out of range and
     * whether trap was generated previously; which has to be
     * verified with previous max and min power supply */
    if ((gIssSysGroupInfo.IssSwitchInfo.u4CurrPowerSupply >
         gIssSysGroupInfo.IssSwitchThreshold.u4MaxPowerSupply) ||
        (gIssSysGroupInfo.IssSwitchInfo.u4CurrPowerSupply <
         gIssSysGroupInfo.IssSwitchThreshold.u4MinPowerSupply))
    {
        if (gu1PowerStatus == OSIX_FALSE)
        {
            /* Send SNMP Trap Message */
            IssSystTrapInfo.InfoTrap.u4CurrPowerSupply =
                gIssSysGroupInfo.IssSwitchInfo.u4CurrPowerSupply;
            IssSystTrapInfo.ThresholdTrap.u4MinPowerSupply =
                gIssSysGroupInfo.IssSwitchThreshold.u4MinPowerSupply;
            IssSystTrapInfo.ThresholdTrap.u4MaxPowerSupply =
                gIssSysGroupInfo.IssSwitchThreshold.u4MaxPowerSupply;

            IssSendTrapAndLog (&IssSystTrapInfo, u1TrapType);
            gu1PowerStatus = OSIX_TRUE;
        }
    }
    else if ((gIssSysGroupInfo.IssSwitchInfo.u4CurrPowerSupply <=
              gIssSysGroupInfo.IssSwitchThreshold.u4MaxPowerSupply) &&
             (gIssSysGroupInfo.IssSwitchInfo.u4CurrPowerSupply >=
              gIssSysGroupInfo.IssSwitchThreshold.u4MinPowerSupply))
    {
        gu1PowerStatus = OSIX_FALSE;
    }

    ISS_TRC (DUMP_TRC, "Exiting the function IssCheckCurrentPowerStatus\r\n");
}
#endif

/******************************************************************************
 *
 *    FUNCTION NAME    : IssCheckCurrentCPUUsage
 *
 *    DESCRIPTION      : This function checks the CPU utilized, if it is not
 *                       with in the allowed limit, SNMP trap will be generated.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ******************************************************************************/
PRIVATE VOID
IssCheckCurrentCPUUsage (VOID)
{
    UINT1               u1TrapType = ISS_SYS_TRAP_CPU_THRESHOLD;
    tIssSystTrapInfo    IssSystTrapInfo;

    MEMSET (&IssSystTrapInfo.ThresholdTrap, 0,
            sizeof (IssSystTrapInfo.ThresholdTrap));
    MEMSET (&IssSystTrapInfo.InfoTrap, 0, sizeof (IssSystTrapInfo.InfoTrap));

    ISS_TRC (DUMP_TRC, "Entering the function IssCheckCurrentCPUUsage \r\n");

    if (IssGetCPUInfo () != ISS_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\rFailed to get CPU info\r\n");
        return;
    }

    /* Check whether current CPU utilized is out of range and
     * whether trap was generated previously */
    if (gIssSysGroupInfo.IssSwitchInfo.u4CurrCPUUsage >
        gIssSysGroupInfo.IssSwitchThreshold.u4MaxCPUThreshold)
    {
        if (gu1CPUStatus == OSIX_FALSE)
        {
            /* Send SNMP Trap Message */
            IssSystTrapInfo.InfoTrap.u4CurrCPUUsage =
                gIssSysGroupInfo.IssSwitchInfo.u4CurrCPUUsage;
            IssSystTrapInfo.ThresholdTrap.u4MaxCPUThreshold =
                gIssSysGroupInfo.IssSwitchThreshold.u4MaxCPUThreshold;

            IssSendTrapAndLog (&IssSystTrapInfo, u1TrapType);
            gu1CPUStatus = OSIX_TRUE;
        }
    }
    else if (gIssSysGroupInfo.IssSwitchInfo.u4CurrCPUUsage <=
             gIssSysGroupInfo.IssSwitchThreshold.u4MaxCPUThreshold)
    {
        gu1CPUStatus = OSIX_FALSE;
    }

    ISS_TRC (DUMP_TRC, "Exiting the function IssCheckCurrentCPUUsage \r\n");
}

/******************************************************************************
 *
 *    FUNCTION NAME    : IssCheckCurrentFlashUsage
 *
 *    DESCRIPTION      : This function checks the Flash utilized, if it is not
 *                       with in the allowed limit, SNMP trap will be generated.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ******************************************************************************/
PRIVATE VOID
IssCheckCurrentFlashUsage (VOID)
{
    UINT1               u1TrapType = ISS_SYS_TRAP_FLASH_THRESHOLD;
    tIssSystTrapInfo    IssSystTrapInfo;

    MEMSET (&IssSystTrapInfo.ThresholdTrap, 0,
            sizeof (IssSystTrapInfo.ThresholdTrap));
    MEMSET (&IssSystTrapInfo.InfoTrap, 0, sizeof (IssSystTrapInfo.InfoTrap));

    ISS_TRC (DUMP_TRC, "Entering the function IssCheckCurrentFlashUsage \r\n");

    if (IssGetFlashInfo () != ISS_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "\rFailed to get flash info\r\n");
        return;
    }

    /* Check whether current CPU utilized is out of range and
     * whether trap was generated previously */
    if (gIssSysGroupInfo.IssSwitchInfo.u4CurrFlashUsage >
        gIssSysGroupInfo.IssSwitchThreshold.u4MaxFlashThreshold)
    {
        if (gu1FlashStatus == OSIX_FALSE)
        {
            /* Send SNMP Trap Message */
            IssSystTrapInfo.InfoTrap.u4CurrFlashUsage =
                gIssSysGroupInfo.IssSwitchInfo.u4CurrFlashUsage;
            IssSystTrapInfo.ThresholdTrap.u4MaxFlashThreshold =
                gIssSysGroupInfo.IssSwitchThreshold.u4MaxFlashThreshold;

            IssSendTrapAndLog (&IssSystTrapInfo, u1TrapType);
            gu1FlashStatus = OSIX_TRUE;
        }
    }
    else if (gIssSysGroupInfo.IssSwitchInfo.u4CurrFlashUsage <=
             gIssSysGroupInfo.IssSwitchThreshold.u4MaxFlashThreshold)
    {
        gu1FlashStatus = OSIX_FALSE;
    }

    ISS_TRC (DUMP_TRC, "Exiting the function IssCheckCurrentFlashUsage\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssSendTrapAndLog                                          */
/*                                                                           */
/* Description  : Routine to form the trap message, syslog message and file  */
/*                log message. Send the messages to FM module to send trap,  */
/*                                                                           */
/* Input(s)     : u1TrapType - Trap type                                     */
/*                pTrapInfo - Trap information                               */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
IssSendTrapAndLog (tIssSystTrapInfo * pTrapInfo, UINT1 u1TrapType)
{
#ifdef FM_WANTED
    tFmFaultMsg         FmMsg;
    UINT1               au1LogMsg[ISS_MAX_LOG_STR_LEN];

    MEMSET (&FmMsg, 0, sizeof (tFmFaultMsg));
    MEMSET (au1LogMsg, 0, ISS_MAX_LOG_STR_LEN);

    /* Check the trap status of the trap type. */
    /* Sys log message should be stored when the syslog status
     * is enabled for the trap type */
    switch (u1TrapType)
    {
        case ISS_SYS_TRAP_RAM_THRESHOLD:

            FmMsg.pTrapMsg =
                IssFormRAMThresholdTrapMsg
                (pTrapInfo->InfoTrap.u4CurrRAMUsage,
                 pTrapInfo->ThresholdTrap.u4MaxRAMThreshold);

            SPRINTF ((CHR1 *) au1LogMsg, "%s: %u percentage crosses"
                     " the threshold limit. Maximum RAM threshold:"
                     " %u percentage\n", gau1LogMsg[u1TrapType],
                     pTrapInfo->InfoTrap.u4CurrRAMUsage,
                     pTrapInfo->ThresholdTrap.u4MaxRAMThreshold);

            FmMsg.pSyslogMsg = (UINT1 *) au1LogMsg;
            break;

        case ISS_SYS_TRAP_FAN_STATUS:

            FmMsg.pTrapMsg =
                IssFormFanStatusTrapMsg
                (pTrapInfo->FanStatusTrap.u4FanIndex,
                 pTrapInfo->FanStatusTrap.u4CurrFanStatus);

            SPRINTF ((CHR1 *) au1LogMsg, " %s : Index:"
                     " %u Operational Status: %u \n", gau1LogMsg[u1TrapType],
                     pTrapInfo->FanStatusTrap.u4FanIndex,
                     pTrapInfo->FanStatusTrap.u4CurrFanStatus);

            FmMsg.pSyslogMsg = (UINT1 *) au1LogMsg;
            break;

        case ISS_SYS_TRAP_CPU_THRESHOLD:

            FmMsg.pTrapMsg =
                IssFormCPUThresholdTrapMsg
                (pTrapInfo->InfoTrap.u4CurrCPUUsage,
                 pTrapInfo->ThresholdTrap.u4MaxCPUThreshold);

            SPRINTF ((CHR1 *) au1LogMsg, "%s: %u percentage crosses"
                     " the threshold limit. Maximum CPU threshold:"
                     " %u percentage\n", gau1LogMsg[u1TrapType],
                     pTrapInfo->InfoTrap.u4CurrCPUUsage,
                     pTrapInfo->ThresholdTrap.u4MaxCPUThreshold);

            FmMsg.pSyslogMsg = (UINT1 *) au1LogMsg;
            break;

        case ISS_SYS_TRAP_TEMPERATURE_THRES:

            FmMsg.pTrapMsg =
                IssFormTemperatureThresTrapMsg
                (pTrapInfo->InfoTrap.i4CurrTemperature,
                 pTrapInfo->ThresholdTrap.i4MinThresholdTemp,
                 pTrapInfo->ThresholdTrap.i4MaxThresholdTemp);

            SPRINTF ((CHR1 *) au1LogMsg, "%s: %d celsius crosses the"
                     " threshold limit. Min Temperature threshold is"
                     " %d celsius and Max Temperature threshold"
                     " is %d celsius\n", gau1LogMsg[u1TrapType],
                     pTrapInfo->InfoTrap.i4CurrTemperature,
                     pTrapInfo->ThresholdTrap.i4MinThresholdTemp,
                     pTrapInfo->ThresholdTrap.i4MaxThresholdTemp);

            FmMsg.pSyslogMsg = (UINT1 *) au1LogMsg;
            break;

        case ISS_SYS_TRAP_POWER_STATUS:

            FmMsg.pTrapMsg =
                IssFormPowerSupplyTrapMsg
                (pTrapInfo->InfoTrap.u4CurrPowerSupply,
                 pTrapInfo->ThresholdTrap.u4MaxPowerSupply,
                 pTrapInfo->ThresholdTrap.u4MinPowerSupply);

            SPRINTF ((CHR1 *) au1LogMsg, "%s: %u volts exceeds the"
                     " the limit. Max power supply is %u volts"
                     " and Min power supply is %u volts\n",
                     gau1LogMsg[u1TrapType],
                     pTrapInfo->InfoTrap.u4CurrPowerSupply,
                     pTrapInfo->ThresholdTrap.u4MaxPowerSupply,
                     pTrapInfo->ThresholdTrap.u4MinPowerSupply);

            FmMsg.pSyslogMsg = (UINT1 *) au1LogMsg;
            break;

        case ISS_SYS_TRAP_FLASH_THRESHOLD:

            FmMsg.pTrapMsg =
                IssFormFlashThresholdTrapMsg
                (pTrapInfo->InfoTrap.u4CurrFlashUsage,
                 pTrapInfo->ThresholdTrap.u4MaxFlashThreshold);

            SPRINTF ((CHR1 *) au1LogMsg, "%s: %u percentage crosses the"
                     " threshold limit. Maximum Flash threshold:"
                     " %u percentage\n", gau1LogMsg[u1TrapType],
                     pTrapInfo->InfoTrap.u4CurrFlashUsage,
                     pTrapInfo->ThresholdTrap.u4MaxFlashThreshold);

            FmMsg.pSyslogMsg = (UINT1 *) au1LogMsg;
            break;

        default:
            break;
    }
    if (FmMsg.pTrapMsg == NULL)
    {
        return;
    }

    /* Store the module ID in FM data structre */
    FmMsg.u4ModuleId = FM_NOTIFY_MOD_ID_SYS;

    if (FmApiNotifyFaults (&FmMsg) != OSIX_FAILURE)
    {
        ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                      " Failed in Notification of faults in FM module for"
                      " type: %d \r\n", u1TrapType);
    }
#else
    UNUSED_PARAM (pTrapInfo);
    UNUSED_PARAM (u1TrapType);
#endif
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssFormRAMThresholdTrapMsg                                 */
/*                                                                           */
/* Description  : Routine to form the SNMP trap message.                     */
/*                                                                           */
/* Input        : u4CurRAMUsage - current RAM usage                          */
/*                u4MaxRAMUsage - maximum RAM usage                          */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC tSNMP_VAR_BIND *
IssFormRAMThresholdTrapMsg (UINT4 u4CurRAMUsage, UINT4 u4MaxRAMUsage)
{
#ifdef SNMP_3_WANTED

    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_COUNTER64_TYPE pSnmpCnt64Type;
    tSNMP_COUNTER64_TYPE u8CounterVal = { 0, 0 };
    UINT4               u4SpecTrapType = 0;
    UINT1               au1Buf[ISS_SYS_OBJ_NAME_LEN];
    UINT4               au4TrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    INT1                ISS_SYS_TRAPS_OID[] = "1.3.6.1.4.1.2076.81.0.7";

    pSnmpCnt64Type.msn = 0;
    pSnmpCnt64Type.lsn = 0;
    u4SpecTrapType = ENTERPRISE_SPECIFIC;
    UNUSED_PARAM (u4SpecTrapType);

    pEnterpriseOid = SNMP_AGT_GetOidFromString (ISS_SYS_TRAPS_OID);

    if (pEnterpriseOid == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "Failed in allocating memory for enterprise OID. \r\n");
        return NULL;
    }

    pSnmpTrapOid = alloc_oid (SNMP_TRAP_OID_LEN);

    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        ISS_TRC (ALL_FAILURE_TRC,
                 "Failed in allocating memory for enterprise OID. \r\n");
        return NULL;
    }

    MEMCPY (pSnmpTrapOid->pu4_OidList, au4TrapOid,
            SNMP_TRAP_OID_LEN * sizeof (UINT4));

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0, 0,
                                                       NULL, pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        ISS_TRC (ALL_FAILURE_TRC,
                 "IssFormRAMThresholdTrapMsg: Variable Binding Failed\r\n");
        return NULL;
    }

    /* Starting of Variable Binding */
    pStartVb = pVbList;

    /*Status have to be notified through the TRAP. */
    SPRINTF ((char *) au1Buf, "issSwitchMaxRAMUsage");

    /* Pass the Object Name ifIndex to get the SNMP Oid */
    pOid = IssMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        /* Release the memory Assigned for Entrerprise Oid */
        SNMP_free_snmp_vb_list (pStartVb);
        return NULL;
    }
    /* Concatenate the OID with the value - */
    pOid->pu4_OidList[SNMP_TRAP_OID_LEN - 1] = 0;
    pOid->u4_Length = SNMP_TRAP_OID_LEN;

    /* For the SNMP Variable Binding of OID and associated Value
     * for u4MaxRAMUsage*/
    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER,
                                                 0, (INT4) u4MaxRAMUsage,
                                                 NULL, NULL, pSnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        ISS_TRC (ALL_FAILURE_TRC,
                 "IssFormRAMThresholdTrapMsg: Variable Binding Failed\r\n");
        return NULL;
    }

    /* Start of Next Variable Binding */
    pVbList = pVbList->pNextVarBind;

    SPRINTF ((char *) au1Buf, "issSwitchCurrentRAMUsage");
    pOid = IssMakeObjIdFromDotNew ((INT1 *) au1Buf);

    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        return NULL;
    }

    /* Concatenate the OID with the value */
    pOid->pu4_OidList[SNMP_TRAP_OID_LEN - 1] = 0;
    pOid->u4_Length = SNMP_TRAP_OID_LEN;

    /* For the SNMP Variable Binding of OID and associated Value
     * for u4CurRAMUsage*/
    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER,
                                                 0, (INT4) u4CurRAMUsage, NULL,
                                                 NULL, pSnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return NULL;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    return pStartVb;
#else
    UNUSED_PARAM (u4CurRAMUsage);
    UNUSED_PARAM (u4MaxRAMUsage);
    return NULL;
#endif /* SNMP_3_WANTED */

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssFormFanStatusTrapMsg                                    */
/*                                                                           */
/* Description  : Routine to form the SNMP trap message.                     */
/*                                                                           */
/* Input        : u4FanIndex - Fan index                                     */
/*                u4FanStatus - Fan status                                   */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC tSNMP_VAR_BIND *
IssFormFanStatusTrapMsg (UINT4 u4FanIndex, UINT4 u4FanStatus)
{
#ifdef SNMP_3_WANTED

    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_COUNTER64_TYPE pSnmpCnt64Type;
    tSNMP_COUNTER64_TYPE u8CounterVal = { 0, 0 };
    UINT4               u4SpecTrapType = 0;
    UINT1               au1Buf[ISS_SYS_OBJ_NAME_LEN];
    UINT4               au4TrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    INT1                ISS_SYS_TRAPS_OID[] = "1.3.6.1.4.1.2076.81.0.9";

    pSnmpCnt64Type.msn = 0;
    pSnmpCnt64Type.lsn = 0;

    u4SpecTrapType = ENTERPRISE_SPECIFIC;
    UNUSED_PARAM (u4SpecTrapType);
    pEnterpriseOid = SNMP_AGT_GetOidFromString (ISS_SYS_TRAPS_OID);

    if (pEnterpriseOid == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "Failed in allocating memory for enterprise OID. \r\n");
        return NULL;
    }
    pSnmpTrapOid = alloc_oid (SNMP_TRAP_OID_LEN + 1);

    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        ISS_TRC (ALL_FAILURE_TRC,
                 "Failed in allocating memory for enterprise OID. \r\n");
        return NULL;
    }

    MEMCPY (pSnmpTrapOid->pu4_OidList, au4TrapOid,
            (SNMP_TRAP_OID_LEN) * sizeof (UINT4));

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0, 0,
                                                       NULL, pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        ISS_TRC (ALL_FAILURE_TRC,
                 "IssFormFanStatusTrapMsg: Variable Binding Failed\r\n");
        return NULL;
    }

    /* Starting of Variable Binding */
    pStartVb = pVbList;

    /* Status have to be notified through the TRAP. */
    SPRINTF ((char *) au1Buf, "issSwitchFanIndex");

    /* Pass the Object Name ifIndex to get the SNMP Oid */
    pOid = IssMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        /* Release the memory Assigned for Entrerprise Oid */
        SNMP_free_snmp_vb_list (pStartVb);
        return NULL;
    }
    /* Concatenate the OID with the value - */
    pOid->pu4_OidList[SNMP_TRAP_OID_LEN] = u4FanIndex;
    pOid->u4_Length = SNMP_TRAP_OID_LEN + 1;

    /* For the SNMP Variable Binding of OID and associated Value
     * for u4FanIndex*/
    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER,
                                                 0, (INT4) u4FanIndex,
                                                 NULL, NULL, pSnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        ISS_TRC (ALL_FAILURE_TRC,
                 "IssFormFanStatusTrapMsg: Variable Binding Failed\r\n");
        return NULL;
    }
    /* Start of Next Variable Binding */
    pVbList = pVbList->pNextVarBind;

    SPRINTF ((char *) au1Buf, "issSwitchFanStatus");
    /* Pass the Object Name ifSwitchFanStatus to get the SNMP Oid */
    pOid = IssMakeObjIdFromDotNew ((INT1 *) au1Buf);

    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        return NULL;
    }

    /* Concatenate the OID with the value */
    pOid->pu4_OidList[SNMP_TRAP_OID_LEN] = u4FanIndex;
    pOid->u4_Length = SNMP_TRAP_OID_LEN + 1;

    /* For the SNMP Variable Binding of OID and associated Value
     * for u4FanStatus*/
    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER,
                                                 0, (INT4) u4FanStatus, NULL,
                                                 NULL, pSnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return NULL;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    return pStartVb;
#else
    UNUSED_PARAM (u4FanIndex);
    UNUSED_PARAM (u4FanStatus);
    return NULL;
#endif /* SNMP_3_WANTED */

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssFormTemperatureThresTrapMsg                             */
/*                                                                           */
/* Description  : Routine to form the SNMP trap message.                     */
/*                                                                           */
/* Input(s)     : i4CurTempThres - current temperature threshold             */
/*                i4MaxTempThres - maximum temperature threshold             */
/*                i4MinTempThres - maximum temperature threshold             */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC tSNMP_VAR_BIND *
IssFormTemperatureThresTrapMsg (INT4 i4CurTempThres, INT4 i4MinTempThres,
                                INT4 i4MaxTempThres)
{
#ifdef SNMP_3_WANTED

    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_COUNTER64_TYPE pSnmpCnt64Type;
    tSNMP_COUNTER64_TYPE u8CounterVal = { 0, 0 };
    UINT4               u4SpecTrapType = 0;
    UINT1               au1Buf[ISS_SYS_OBJ_NAME_LEN];
    UINT4               au4TrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    INT1                ISS_SYS_TRAPS_OID[] = "1.3.6.1.4.1.2076.81.0.4";

    pSnmpCnt64Type.msn = 0;
    pSnmpCnt64Type.lsn = 0;

    u4SpecTrapType = ENTERPRISE_SPECIFIC;
    UNUSED_PARAM (u4SpecTrapType);

    pEnterpriseOid = SNMP_AGT_GetOidFromString (ISS_SYS_TRAPS_OID);

    if (pEnterpriseOid == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "Failed in allocating memory for enterprise OID. \r\n");
        return NULL;
    }

    pSnmpTrapOid = alloc_oid (SNMP_TRAP_OID_LEN);

    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        ISS_TRC (ALL_FAILURE_TRC,
                 "Failed in allocating memory for enterprise OID. \r\n");
        return NULL;
    }

    MEMCPY (pSnmpTrapOid->pu4_OidList, au4TrapOid,
            SNMP_TRAP_OID_LEN * sizeof (UINT4));

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0, 0,
                                                       NULL, pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        ISS_TRC (ALL_FAILURE_TRC,
                 "IssFormTemperatureThresTrapMsg: Variable Binding Failed\r\n");
        return NULL;
    }

    /* Starting of Variable Binding */
    pStartVb = pVbList;

    /*Status have to be notified through the TRAP. */
    SPRINTF ((char *) au1Buf, "issSwitchMinThresholdTemperature");

    /* Pass the Object Name ifIndex to get the SNMP Oid */
    pOid = IssMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        /* Release the memory Assigned for Entrerprise Oid */
        SNMP_free_snmp_vb_list (pStartVb);
        return NULL;
    }
    /* Concatenate the OID with the value - */
    pOid->pu4_OidList[SNMP_TRAP_OID_LEN - 1] = 0;
    pOid->u4_Length = SNMP_TRAP_OID_LEN;

    /* For the SNMP Variable Binding of OID and associated Value
     * for i4MinTempThres*/
    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER,
                                                 0, i4MinTempThres,
                                                 NULL, NULL, pSnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        ISS_TRC (ALL_FAILURE_TRC,
                 "IssFormTemperatureThresTrapMsg: Variable Binding Failed\r\n");
        return NULL;
    }

    /* Start of Next Variable Binding */
    pVbList = pVbList->pNextVarBind;

    /*Status have to be notified through the TRAP. */
    SPRINTF ((char *) au1Buf, "issSwitchMaxThresholdTemperature");

    /* Pass the Object Name ifIndex to get the SNMP Oid */
    pOid = IssMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        /* Release the memory Assigned for Entrerprise Oid */
        SNMP_free_snmp_vb_list (pStartVb);
        return NULL;
    }
    /* Concatenate the OID with the value - */
    pOid->pu4_OidList[SNMP_TRAP_OID_LEN - 1] = 0;
    pOid->u4_Length = SNMP_TRAP_OID_LEN;

    /* For the SNMP Variable Binding of OID and associated Value
     * for i4MaxTempThres*/
    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER,
                                                 0, i4MaxTempThres,
                                                 NULL, NULL, pSnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        ISS_TRC (ALL_FAILURE_TRC,
                 "IssFormTemperatureThresTrapMsg: Variable Binding Failed\r\n");
        return NULL;
    }

    /* Start of Next Variable Binding */
    pVbList = pVbList->pNextVarBind;

    SPRINTF ((char *) au1Buf, "issSwitchCurrentTemperature");
    pOid = IssMakeObjIdFromDotNew ((INT1 *) au1Buf);

    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        return NULL;
    }

    /* Concatenate the OID with the value */
    pOid->pu4_OidList[SNMP_TRAP_OID_LEN - 1] = 0;
    pOid->u4_Length = SNMP_TRAP_OID_LEN;

    /* For the SNMP Variable Binding of OID and associated Value
     * for i4CurTempThres*/
    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER,
                                                 0, i4CurTempThres, NULL,
                                                 NULL, pSnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return NULL;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    return pStartVb;
#else
    UNUSED_PARAM (i4CurTempThres);
    UNUSED_PARAM (i4MaxTempThres);
    UNUSED_PARAM (i4MinTempThres);
    return NULL;
#endif /* SNMP_3_WANTED */

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssFormPowerSupplyTrapMsg                                  */
/*                                                                           */
/* Description  : Routine to form the SNMP trap message.                     */
/*                                                                           */
/* Input(s)     : u4CurPowerSupply - current power supply                    */
/*                u4MaxPowerSupply - maximum power supply                    */
/*                u4MinPowerSupply - maximum power supply                    */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC tSNMP_VAR_BIND *
IssFormPowerSupplyTrapMsg (UINT4 u4CurPowerSupply, UINT4 u4MaxPowerSupply,
                           UINT4 u4MinPowerSupply)
{
#ifdef SNMP_3_WANTED

    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_COUNTER64_TYPE pSnmpCnt64Type;
    tSNMP_COUNTER64_TYPE u8CounterVal = { 0, 0 };
    UINT4               u4SpecTrapType = 0;
    UINT1               au1Buf[ISS_SYS_OBJ_NAME_LEN];
    UINT4               au4TrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    INT1                ISS_SYS_TRAPS_OID[] = "1.3.6.1.4.1.2076.81.0.6";

    pSnmpCnt64Type.msn = 0;
    pSnmpCnt64Type.lsn = 0;

    u4SpecTrapType = ENTERPRISE_SPECIFIC;
    UNUSED_PARAM (u4SpecTrapType);

    pEnterpriseOid = SNMP_AGT_GetOidFromString (ISS_SYS_TRAPS_OID);

    if (pEnterpriseOid == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "Failed in allocating memory for enterprise OID. \r\n");
        return NULL;
    }

    pSnmpTrapOid = alloc_oid (SNMP_TRAP_OID_LEN);

    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        ISS_TRC (ALL_FAILURE_TRC,
                 "Failed in allocating memory for enterprise OID. \r\n");
        return NULL;
    }

    MEMCPY (pSnmpTrapOid->pu4_OidList, au4TrapOid,
            SNMP_TRAP_OID_LEN * sizeof (UINT4));

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0, 0,
                                                       NULL, pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        ISS_TRC (ALL_FAILURE_TRC,
                 "IssFormPowerSupplyTrapMsg: Variable Binding Failed\r\n");
        return NULL;
    }

    /* Starting of Variable Binding */
    pStartVb = pVbList;

    /*Status have to be notified through the TRAP. */
    SPRINTF ((char *) au1Buf, "issSwitchPowerSurge");

    /* Pass the Object Name ifIndex to get the SNMP Oid */
    pOid = IssMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        /* Release the memory Assigned for Entrerprise Oid */
        SNMP_free_snmp_vb_list (pStartVb);
        return NULL;
    }
    /* Concatenate the OID with the value - */
    pOid->pu4_OidList[SNMP_TRAP_OID_LEN - 1] = 0;
    pOid->u4_Length = SNMP_TRAP_OID_LEN;

    /* For the SNMP Variable Binding of OID and associated Value
     * for u4MinPowerSupply */
    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER,
                                                 0, (INT4) u4MinPowerSupply,
                                                 NULL, NULL, pSnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        ISS_TRC (ALL_FAILURE_TRC,
                 "IssFormPowerSupplyTrapMsg: Variable Binding Failed\r\n");
        return NULL;
    }

    /* Start of Next Variable Binding */
    pVbList = pVbList->pNextVarBind;

    /*Status have to be notified through the TRAP. */
    SPRINTF ((char *) au1Buf, "issSwitchPowerFailure");

    /* Pass the Object Name ifIndex to get the SNMP Oid */
    pOid = IssMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        /* Release the memory Assigned for Entrerprise Oid */
        SNMP_free_snmp_vb_list (pStartVb);
        return NULL;
    }
    /* Concatenate the OID with the value - */
    pOid->pu4_OidList[SNMP_TRAP_OID_LEN - 1] = 0;
    pOid->u4_Length = SNMP_TRAP_OID_LEN;

    /* For the SNMP Variable Binding of OID and associated Value
     * for u4MaxPowerSupply */
    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER,
                                                 0, (INT4) u4MaxPowerSupply,
                                                 NULL, NULL, pSnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        ISS_TRC (ALL_FAILURE_TRC,
                 "IssFormPowerSupplyTrapMsg: Variable Binding Failed\r\n");
        return NULL;
    }

    /* Start of Next Variable Binding */
    pVbList = pVbList->pNextVarBind;

    SPRINTF ((char *) au1Buf, "issSwitchCurrentPowerSupply");
    pOid = IssMakeObjIdFromDotNew ((INT1 *) au1Buf);

    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        return NULL;
    }

    /* Concatenate the OID with the value */
    pOid->pu4_OidList[SNMP_TRAP_OID_LEN - 1] = 0;
    pOid->u4_Length = SNMP_TRAP_OID_LEN;

    /* For the SNMP Variable Binding of OID and associated Value
     * for u4CurPowerSupply */
    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER,
                                                 0, (INT4) u4CurPowerSupply,
                                                 NULL, NULL, pSnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return NULL;
    }

    pVbList = pVbList->pNextVarBind;

    return pStartVb;
#else
    UNUSED_PARAM (u4CurPowerSupply);
    UNUSED_PARAM (u4MaxPowerSupply);
    UNUSED_PARAM (u4MinPowerSupply);
    return NULL;
#endif /* SNMP_3_WANTED */

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssFormCPUThresholdTrapMsg                                 */
/*                                                                           */
/* Description  : Routine to form the SNMP trap message.                     */
/*                                                                           */
/* Input        : u4CurCPUUsage - current CPU threshold                      */
/*                u4MaxCPUUsage - maximum CPU threshold                      */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC tSNMP_VAR_BIND *
IssFormCPUThresholdTrapMsg (UINT4 u4CurCPUUsage, UINT4 u4MaxCPUUsage)
{
#ifdef SNMP_3_WANTED

    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_COUNTER64_TYPE pSnmpCnt64Type;
    tSNMP_COUNTER64_TYPE u8CounterVal = { 0, 0 };
    UINT4               u4SpecTrapType = 0;
    UINT1               au1Buf[ISS_SYS_OBJ_NAME_LEN];
    UINT4               au4TrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    INT1                ISS_SYS_TRAPS_OID[] = "1.3.6.1.4.1.2076.81.0.5";

    pSnmpCnt64Type.msn = 0;
    pSnmpCnt64Type.lsn = 0;

    u4SpecTrapType = ENTERPRISE_SPECIFIC;
    UNUSED_PARAM (u4SpecTrapType);

    pEnterpriseOid = SNMP_AGT_GetOidFromString (ISS_SYS_TRAPS_OID);

    if (pEnterpriseOid == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "Failed in allocating memory for enterprise OID. \r\n");
        return NULL;
    }

    pSnmpTrapOid = alloc_oid (SNMP_TRAP_OID_LEN);

    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        ISS_TRC (ALL_FAILURE_TRC,
                 "Failed in allocating memory for enterprise OID. \r\n");
        return NULL;
    }

    MEMCPY (pSnmpTrapOid->pu4_OidList, au4TrapOid,
            SNMP_TRAP_OID_LEN * sizeof (UINT4));

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0, 0,
                                                       NULL, pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        ISS_TRC (ALL_FAILURE_TRC,
                 "IssFormCPUThresholdTrapMsg: Variable Binding Failed\r\n");
        return NULL;
    }

    /* Starting of Variable Binding */
    pStartVb = pVbList;

    /*Status have to be notified through the TRAP. */
    SPRINTF ((char *) au1Buf, "issSwitchMaxCPUThreshold");

    /* Pass the Object Name ifIndex to get the SNMP Oid */
    pOid = IssMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        /* Release the memory Assigned for Entrerprise Oid */
        SNMP_free_snmp_vb_list (pStartVb);
        return NULL;
    }
    /* Concatenate the OID with the value */
    pOid->pu4_OidList[SNMP_TRAP_OID_LEN - 1] = 0;
    pOid->u4_Length = SNMP_TRAP_OID_LEN;

    /* For the SNMP Variable Binding of OID and associated Value
     * for u4MaxCPUUsage*/
    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER,
                                                 0, (INT4) u4MaxCPUUsage,
                                                 NULL, NULL, pSnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        ISS_TRC (ALL_FAILURE_TRC,
                 "IssFormCPUThresholdTrapMsg: Variable Binding Failed\r\n");
        return NULL;
    }

    /* Start of Next Variable Binding */
    pVbList = pVbList->pNextVarBind;

    SPRINTF ((char *) au1Buf, "issSwitchCurrentCPUThreshold");
    pOid = IssMakeObjIdFromDotNew ((INT1 *) au1Buf);

    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        return NULL;
    }

    /* Concatenate the OID with the value */
    pOid->pu4_OidList[SNMP_TRAP_OID_LEN - 1] = 0;
    pOid->u4_Length = SNMP_TRAP_OID_LEN;

    /* For the SNMP Variable Binding of OID and associated Value
     * for u4CurCPUUsage*/
    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER,
                                                 0, (INT4) u4CurCPUUsage, NULL,
                                                 NULL, pSnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return NULL;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    return pStartVb;
#else
    UNUSED_PARAM (u4CurCPUUsage);
    UNUSED_PARAM (u4MaxCPUUsage);
    return NULL;
#endif /* SNMP_3_WANTED */

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssFormFlashThresholdTrapMsg                               */
/*                                                                           */
/* Description  : Routine to form the SNMP trap message.                     */
/*                                                                           */
/* Input        : u4CurFlashUsage - current RAM usage                        */
/*                u4MaxFlashUsage - maximum RAM usage                        */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC tSNMP_VAR_BIND *
IssFormFlashThresholdTrapMsg (UINT4 u4CurFlashUsage, UINT4 u4MaxFlashUsage)
{
#ifdef SNMP_3_WANTED

    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_COUNTER64_TYPE pSnmpCnt64Type;
    tSNMP_COUNTER64_TYPE u8CounterVal = { 0, 0 };
    UINT4               u4SpecTrapType = 0;
    UINT1               au1Buf[ISS_SYS_OBJ_NAME_LEN];
    UINT4               au4TrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    INT1                ISS_SYS_TRAPS_OID[] = "1.3.6.1.4.1.2076.81.0.8";

    pSnmpCnt64Type.msn = 0;
    pSnmpCnt64Type.lsn = 0;

    u4SpecTrapType = ENTERPRISE_SPECIFIC;
    UNUSED_PARAM (u4SpecTrapType);

    pEnterpriseOid = SNMP_AGT_GetOidFromString (ISS_SYS_TRAPS_OID);

    if (pEnterpriseOid == NULL)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "Failed in allocating memory for enterprise OID. \r\n");
        return NULL;
    }

    pSnmpTrapOid = alloc_oid (SNMP_TRAP_OID_LEN);

    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        ISS_TRC (ALL_FAILURE_TRC,
                 "Failed in allocating memory for enterprise OID. \r\n");
        return NULL;
    }

    MEMCPY (pSnmpTrapOid->pu4_OidList, au4TrapOid,
            SNMP_TRAP_OID_LEN * sizeof (UINT4));

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0, 0,
                                                       NULL, pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        ISS_TRC (ALL_FAILURE_TRC,
                 "IssFormFlashThresholdTrapMsg: Variable Binding Failed\r\n");
        return NULL;
    }

    /* Starting of Variable Binding */
    pStartVb = pVbList;

    /*Status have to be notified through the TRAP. */
    SPRINTF ((char *) au1Buf, "issSwitchMaxFlashUsage");

    /* Pass the Object Name ifIndex to get the SNMP Oid */
    pOid = IssMakeObjIdFromDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        /* Release the memory Assigned for Entrerprise Oid */
        SNMP_free_snmp_vb_list (pStartVb);
        return NULL;
    }
    /* Concatenate the OID with the value - */
    pOid->pu4_OidList[SNMP_TRAP_OID_LEN - 1] = 0;
    pOid->u4_Length = SNMP_TRAP_OID_LEN;

    /* For the SNMP Variable Binding of OID and associated Value
     * for u4MaxFlashUsage*/
    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER,
                                                 0, (INT4) u4MaxFlashUsage,
                                                 NULL, NULL, pSnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        ISS_TRC (ALL_FAILURE_TRC,
                 "IssFormFlashThresholdTrapMsg: Variable Binding Failed\r\n");
        return NULL;
    }

    /* Start of Next Variable Binding */
    pVbList = pVbList->pNextVarBind;

    SPRINTF ((char *) au1Buf, "issSwitchCurrentFlashUsage");
    pOid = IssMakeObjIdFromDotNew ((INT1 *) au1Buf);

    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        return NULL;
    }

    /* Concatenate the OID with the value */
    pOid->pu4_OidList[SNMP_TRAP_OID_LEN - 1] = 0;
    pOid->u4_Length = SNMP_TRAP_OID_LEN;

    /* For the SNMP Variable Binding of OID and associated Value
     * for u4CurFlashUsage*/
    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                 SNMP_DATA_TYPE_INTEGER,
                                                 0, (INT4) u4CurFlashUsage,
                                                 NULL, NULL, pSnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return NULL;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    return pStartVb;
#else
    UNUSED_PARAM (u4CurFlashUsage);
    UNUSED_PARAM (u4MaxFlashUsage);
    return NULL;
#endif /* SNMP_3_WANTED */

}

/**********************************************************************
* Function             : IssMakeObjIdFromDotNew                       *
*                                                                     *
* Role of the function : This function returns the pointer to the     *
*                         object id of the passed object to it.       *
* Input(s)             : pi1textStr : pointer to the object whose     *
                                      object id is to be found        *
* OutPut(s)            : None                                         *
* Return Value         : pOid : pointer to object id.                 *
**********************************************************************/
tSNMP_OID_TYPE     *
IssMakeObjIdFromDotNew (INT1 *pi1TextStr)
{
    tSNMP_OID_TYPE     *pOid = NULL;
    INT1               *pTemp = NULL, *pDot = NULL;
    UINT1               au1tempBuffer[ISS_SYS_OBJ_NAME_LEN];
    UINT4               au4MaxRAMUsage[] =
        { 1, 3, 6, 1, 4, 1, 2076, 81, 1, 72 };
    UINT4               au4CurRAMUsage[] =
        { 1, 3, 6, 1, 4, 1, 2076, 81, 1, 73 };
    UINT4               au4FanIndex[] =
        { 1, 3, 6, 1, 4, 1, 2076, 81, 13, 1, 1, 1 };
    UINT4               au4FanStatus[] =
        { 1, 3, 6, 1, 4, 1, 2076, 81, 13, 1, 1, 2 };
    UINT4               au4MaxCPUUsage[] =
        { 1, 3, 6, 1, 4, 1, 2076, 81, 1, 67 };
    UINT4               au4CurCPUUsage[] =
        { 1, 3, 6, 1, 4, 1, 2076, 81, 1, 68 };
    UINT4               au4MaxFlashUsage[] =
        { 1, 3, 6, 1, 4, 1, 2076, 81, 1, 74 };
    UINT4               au4CurFlashUsage[] =
        { 1, 3, 6, 1, 4, 1, 2076, 81, 1, 75 };
    UINT4               au4MaxTemperature[] =
        { 1, 3, 6, 1, 4, 1, 2076, 81, 1, 65 };
    UINT4               au4MinTemperature[] =
        { 1, 3, 6, 1, 4, 1, 2076, 81, 1, 64 };
    UINT4               au4CurTemperature[] =
        { 1, 3, 6, 1, 4, 1, 2076, 81, 1, 66 };
    UINT4               au4MaxPowerSupply[] =
        { 1, 3, 6, 1, 4, 1, 2076, 81, 1, 69 };
    UINT4               au4MinPowerSupply[] =
        { 1, 3, 6, 1, 4, 1, 2076, 81, 1, 70 };
    UINT4               au4CurPowerSupply[] =
        { 1, 3, 6, 1, 4, 1, 2076, 81, 1, 71 };

    /* Is there an alpha descriptor at begining ?? */
    if (isalpha (*pi1TextStr) != 0)
    {
        pDot = ((INT1 *) STRCHR ((char *) pi1TextStr, '.'));

        /* if no dot, point to end of string */
        if (pDot == NULL)
        {
            pDot = ((INT1 *) (pi1TextStr + STRLEN ((char *) pi1TextStr)));
        }

        pTemp = ((INT1 *) pi1TextStr);
        MEMSET (au1tempBuffer, 0, ISS_SYS_OBJ_NAME_LEN);

        STRNCPY (au1tempBuffer, pTemp, (sizeof (au1tempBuffer) - 1));
        au1tempBuffer[STRLEN (au1tempBuffer)] = '\0';

        if ((STRCMP ("issSwitchMaxRAMUsage", (INT1 *) au1tempBuffer) == 0)
            && (STRLEN ((INT1 *) au1tempBuffer)
                == STRLEN ("issSwitchMaxRAMUsage")))
        {
            if ((pOid = alloc_oid (ISS_SYS_TRAP_OID_LEN - 1)) == NULL)
            {
                return (NULL);
            }

            MEMCPY (pOid->pu4_OidList, au4MaxRAMUsage, MEM_MAX_BYTES
                    (sizeof (au4MaxRAMUsage),
                     (ISS_SYS_TRAP_OID_LEN - 1) * sizeof (UINT4)));
            return (pOid);
        }
        else if ((STRCMP ("issSwitchCurrentRAMUsage", (INT1 *)
                          au1tempBuffer) == 0)
                 && (STRLEN ((INT1 *) au1tempBuffer)
                     == STRLEN ("issSwitchCurrentRAMUsage")))
        {
            if ((pOid = alloc_oid (ISS_SYS_TRAP_OID_LEN)) == NULL)
            {
                return (NULL);
            }

            MEMCPY (pOid->pu4_OidList, au4CurRAMUsage, MEM_MAX_BYTES
                    (sizeof (au4CurRAMUsage),
                     (ISS_SYS_TRAP_OID_LEN - 1) * sizeof (UINT4)));
            return (pOid);
        }
        else if ((STRCMP ("issSwitchFanIndex", (INT1 *)
                          au1tempBuffer) == 0)
                 && (STRLEN ((INT1 *) au1tempBuffer)
                     == STRLEN ("issSwitchFanIndex")))
        {
            if ((pOid = alloc_oid (ISS_SYS_TRAP_OID_LEN)) == NULL)
            {
                return (NULL);
            }

            MEMCPY (pOid->pu4_OidList, au4FanIndex, MEM_MAX_BYTES
                    (sizeof (au4FanIndex),
                     (ISS_SYS_TRAP_OID_LEN - 1) * sizeof (UINT4)));
            return (pOid);
        }
        else if ((STRCMP ("issSwitchFanStatus", (INT1 *)
                          au1tempBuffer) == 0)
                 && (STRLEN ((INT1 *) au1tempBuffer)
                     == STRLEN ("issSwitchFanStatus")))
        {
            if ((pOid = alloc_oid (ISS_SYS_TRAP_OID_LEN)) == NULL)
            {
                return (NULL);
            }

            MEMCPY (pOid->pu4_OidList, au4FanStatus, MEM_MAX_BYTES
                    (sizeof (au4FanStatus),
                     (ISS_SYS_TRAP_OID_LEN - 1) * sizeof (UINT4)));
            return (pOid);
        }
        else if ((STRCMP ("issSwitchMaxCPUThreshold", (INT1 *) au1tempBuffer) ==
                  0)
                 && (STRLEN ((INT1 *) au1tempBuffer) ==
                     STRLEN ("issSwitchMaxCPUThreshold")))
        {
            if ((pOid = alloc_oid (ISS_SYS_TRAP_OID_LEN - 1)) == NULL)
            {
                return (NULL);
            }

            MEMCPY (pOid->pu4_OidList, au4MaxCPUUsage, MEM_MAX_BYTES
                    (sizeof (au4MaxCPUUsage),
                     (ISS_SYS_TRAP_OID_LEN - 1) * sizeof (UINT4)));
            return (pOid);
        }
        else if ((STRCMP ("issSwitchCurrentCPUThreshold", (INT1 *)
                          au1tempBuffer) == 0)
                 && (STRLEN ((INT1 *) au1tempBuffer)
                     == STRLEN ("issSwitchCurrentCPUThreshold")))
        {
            if ((pOid = alloc_oid (ISS_SYS_TRAP_OID_LEN)) == NULL)
            {
                return (NULL);
            }

            MEMCPY (pOid->pu4_OidList, au4CurCPUUsage, MEM_MAX_BYTES
                    (sizeof (au4CurCPUUsage),
                     (ISS_SYS_TRAP_OID_LEN - 1) * sizeof (UINT4)));
            return (pOid);
        }
        else if ((STRCMP
                  ("issSwitchMinThresholdTemperature",
                   (INT1 *) au1tempBuffer) == 0)
                 && (STRLEN ((INT1 *) au1tempBuffer) ==
                     STRLEN ("issSwitchMinThresholdTemperature")))
        {
            if ((pOid = alloc_oid (ISS_SYS_TRAP_OID_LEN - 1)) == NULL)
            {
                return (NULL);
            }

            MEMCPY (pOid->pu4_OidList, au4MinTemperature, MEM_MAX_BYTES
                    (sizeof (au4MinTemperature),
                     (ISS_SYS_TRAP_OID_LEN - 1) * sizeof (UINT4)));
            return (pOid);
        }
        else if ((STRCMP
                  ("issSwitchMaxThresholdTemperature",
                   (INT1 *) au1tempBuffer) == 0)
                 && (STRLEN ((INT1 *) au1tempBuffer) ==
                     STRLEN ("issSwitchMaxThresholdTemperature")))
        {
            if ((pOid = alloc_oid (ISS_SYS_TRAP_OID_LEN - 1)) == NULL)
            {
                return (NULL);
            }

            MEMCPY (pOid->pu4_OidList, au4MaxTemperature, MEM_MAX_BYTES
                    (sizeof (au4MaxTemperature),
                     (ISS_SYS_TRAP_OID_LEN - 1) * sizeof (UINT4)));
            return (pOid);
        }
        else if ((STRCMP ("issSwitchCurrentTemperature", (INT1 *)
                          au1tempBuffer) == 0)
                 && (STRLEN ((INT1 *) au1tempBuffer)
                     == STRLEN ("issSwitchCurrentTemperature")))
        {
            if ((pOid = alloc_oid (ISS_SYS_TRAP_OID_LEN)) == NULL)
            {
                return (NULL);
            }

            MEMCPY (pOid->pu4_OidList, au4CurTemperature, MEM_MAX_BYTES
                    (sizeof (au4CurTemperature),
                     (ISS_SYS_TRAP_OID_LEN - 1) * sizeof (UINT4)));
            return (pOid);
        }
        else if ((STRCMP ("issSwitchPowerSurge", (INT1 *) au1tempBuffer) == 0)
                 && (STRLEN ((INT1 *) au1tempBuffer)
                     == STRLEN ("issSwitchPowerSurge")))
        {
            if ((pOid = alloc_oid (ISS_SYS_TRAP_OID_LEN - 1)) == NULL)
            {
                return (NULL);
            }

            MEMCPY (pOid->pu4_OidList, au4MaxPowerSupply, MEM_MAX_BYTES
                    (sizeof (au4MaxPowerSupply),
                     (ISS_SYS_TRAP_OID_LEN - 1) * sizeof (UINT4)));
            return (pOid);
        }
        else if ((STRCMP ("issSwitchPowerFailure", (INT1 *) au1tempBuffer) == 0)
                 && (STRLEN ((INT1 *) au1tempBuffer)
                     == STRLEN ("issSwitchPowerFailure")))
        {
            if ((pOid = alloc_oid (ISS_SYS_TRAP_OID_LEN - 1)) == NULL)
            {
                return (NULL);
            }

            MEMCPY (pOid->pu4_OidList, au4MinPowerSupply, MEM_MAX_BYTES
                    (sizeof (au4MinPowerSupply),
                     (ISS_SYS_TRAP_OID_LEN - 1) * sizeof (UINT4)));
            return (pOid);
        }
        else if ((STRCMP ("issSwitchCurrentPowerSupply", (INT1 *)
                          au1tempBuffer) == 0)
                 && (STRLEN ((INT1 *) au1tempBuffer)
                     == STRLEN ("issSwitchCurrentPowerSupply")))
        {
            if ((pOid = alloc_oid (ISS_SYS_TRAP_OID_LEN)) == NULL)
            {
                return (NULL);
            }

            MEMCPY (pOid->pu4_OidList, au4CurPowerSupply, MEM_MAX_BYTES
                    (sizeof (au4CurPowerSupply),
                     (ISS_SYS_TRAP_OID_LEN - 1) * sizeof (UINT4)));
            return (pOid);
        }
        else if ((STRCMP ("issSwitchMaxFlashUsage", (INT1 *)
                          au1tempBuffer) == 0)
                 && (STRLEN ((INT1 *) au1tempBuffer)
                     == STRLEN ("issSwitchMaxFlashUsage")))
        {
            if ((pOid = alloc_oid (ISS_SYS_TRAP_OID_LEN - 1)) == NULL)
            {
                return (NULL);
            }

            MEMCPY (pOid->pu4_OidList, au4MaxFlashUsage, MEM_MAX_BYTES
                    (sizeof (au4MaxFlashUsage),
                     (ISS_SYS_TRAP_OID_LEN - 1) * sizeof (UINT4)));
            return (pOid);
        }
        else if ((STRCMP ("issSwitchCurrentFlashUsage", (INT1 *)
                          au1tempBuffer) == 0)
                 && (STRLEN ((INT1 *) au1tempBuffer)
                     == STRLEN ("issSwitchCurrentFlashUsage")))
        {
            if ((pOid = alloc_oid (ISS_SYS_TRAP_OID_LEN)) == NULL)
            {
                return (NULL);
            }

            MEMCPY (pOid->pu4_OidList, au4CurFlashUsage, MEM_MAX_BYTES
                    (sizeof (au4CurFlashUsage),
                     (ISS_SYS_TRAP_OID_LEN - 1) * sizeof (UINT4)));
            return (pOid);
        }
    }
    return NULL;
}

/*****************************************************************************
 *  Function Name   : IssInitMacLearnLimitParams
 *  Description     : This Function Initializes the global datastructures used 
 *                    for MAC Learn Limit Rate feature. 
 *
 *  Input(s)        : 
 *  Output(s)       : None.
 *  Returns         : ISS_SUCCESS
 ***************************************************************/
VOID
IssInitMacLearnLimitParams (VOID)
{
    ISS_TRC (ISS_TRC_FUNC_ENTRY_EXIT, "Entering the function"
             "IssInitMacLearnLimitParams \r\n");
    ISS_MEMSET (&(gIssSwitchMacLearnLimitRate), 0,
                sizeof (tIssSwitchMacLearnLimitRate));
    gIssSwitchMacLearnLimitRate.i4IssSwitchMacLearnLimitRate =
        ISS_DEF_SWITCH_MAC_LEARN_LIMIT_RATE;
    gIssSwitchMacLearnLimitRate.u4IssSwitchMacLearnRateLimitInterval =
        ISS_DEF_SWITCH_MAC_LEARN_LIMIT_INTERVAL;
    gIssSwitchMacLearnLimitRate.i4IssSwitchMacPresentCount = 0;
    gIssSwitchMacLearnLimitRate.i4IssSwitchMacMaxLearnLimit =
        ISS_SWITCH_MAC_LEARN_LIMIT_RATE;
#ifdef NPAPI_WANTED
    if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
    {
        IsssysIssHwSetMacLearningRateLimit (ISS_SWITCH_MAC_MAX_LEARN_LIMIT);
    }
#endif
    ISS_TRC (ISS_TRC_FUNC_ENTRY_EXIT, "Exiting the function"
             "IssInitMacLearnLimitParams \r\n");

    return;
}

/*****************************************************************************
 *  Function Name   : IssDeInitMacLearnLimitParams
 *  Description     : This Function De-Initializes the global datastructures  
 *                    used for MAC Learn Limit Rate feature. 
 *
 *  Input(s)        : 
 *  Output(s)       : None.
 *  Returns         : None.
 ***************************************************************/
VOID
IssDeInitMacLearnLimitParams (VOID)
{
    ISS_TRC (ISS_TRC_FUNC_ENTRY_EXIT, "Entering the function"
             "IssDeInitMacLearnLimitParams \r\n");

    gIssSwitchMacLearnLimitRate.i4IssSwitchMacLearnLimitRate = 0;
    gIssSwitchMacLearnLimitRate.u4IssSwitchMacLearnRateLimitInterval = 0;
    gIssSwitchMacLearnLimitRate.i4IssSwitchMacPresentCount = 0;
    gIssSwitchMacLearnLimitRate.i4IssSwitchMacMaxLearnLimit = 0;

    ISS_TRC (ISS_TRC_FUNC_ENTRY_EXIT, "Exiting the function"
             "IssDeInitMacLearnLimitParams \r\n");
}

/*****************************************************************************
 *  Function Name   : IssMacLearnLimitStartTmr
 *  Description     : This function starts MAC learn Rate limit timer
 *                  : with the duration as LearnRateLimitInterval
 *
 *  Input(s)        : 
 *  Output(s)       : None.
 *  Returns         : None
 ***************************************************************/
VOID
IssMacLearnLimitStartTmr (VOID)
{
    ISS_TRC_ARG1 (DUMP_TRC, "Starting MAC Learn Limit Rate timer"
                  " Timer Id = %d\r\n", ISS_MAC_LEARN_LIMIT_TMRID);
    TmrStartTimer (ISS_MAC_LEARN_LIMIT_TMRID,
                   &(gIssSwitchMacLearnLimitRate.IssMacLearnRateLimitTimer),
                   ISS_MAC_LEARN_LIMIT_DUR_SEC * SYS_TIME_TICKS_IN_A_SEC);
}

/*****************************************************************************
 *  Function Name   : IssMacLearnLimitProcessTimeout
 *  Description     : This function is called on timer expiry event of 
 *                    MAC Learn Limit timer. It sets the MAC Learn limit rate  
 *                    based on the configured Learn limit rate value. 
 *
 *  Input(s)        : 
 *  Output(s)       : None.
 *  Returns         : None.
 ***************************************************************/
VOID
IssMacLearnLimitProcessTimeout (VOID)
{
    tTmrAppTimer       *pExpiredTmr = NULL;
    INT4                i4SwitchMacAddrCount = 0;
    INT4                i4TempMaxLearnLimit = 0;

    ISS_TRC (ISS_TRC_FUNC_ENTRY_EXIT, "Entering the function"
             "IssMacLearnLimitProcessTimeout \r\n");

    if ((pExpiredTmr = TmrGetNextExpiredTimer
         (ISS_MAC_LEARN_LIMIT_TMRID)) != NULL)
    {
        /* If Learn limit is set to 0, then MAC LearnRateLimit is disabled,
         * and we need not restart MAC LearnRateLimit timer 
         */
        if (ISS_SWITCH_MAC_LEARN_LIMIT_RATE > ISS_ZERO_ENTRY)
        {
            /* Get the Current Dynamic Unicast count.                        
             * Increment the Switch learn limit by a rate when timer expires.
             * Till Maximum limit of Switch is reached       
             */
#ifdef NPAPI_WANTED
            if (ISS_IS_NP_PROGRAMMING_ALLOWED () == ISS_TRUE)
            {
                if (IsssysIssHwGetLearnedMacAddrCount (&i4SwitchMacAddrCount)
                    == FNP_SUCCESS)
                {
                    gIssSwitchMacLearnLimitRate.i4IssSwitchMacPresentCount =
                        i4SwitchMacAddrCount;

                    i4TempMaxLearnLimit = ISS_SWITCH_MAC_MAX_LEARN_LIMIT;

                    ISS_SWITCH_MAC_MAX_LEARN_LIMIT =
                        gIssSwitchMacLearnLimitRate.i4IssSwitchMacPresentCount +
                        ISS_SWITCH_MAC_LEARN_LIMIT_RATE;

                    if (ISS_SWITCH_MAC_MAX_LEARN_LIMIT >
                        ISS_DEF_UNICAST_LEARNING_LIMIT)
                    {
                        ISS_SWITCH_MAC_MAX_LEARN_LIMIT
                            = ISS_DEF_UNICAST_LEARNING_LIMIT;
                    }
                    /* Check if there is any change in prev. Learn Limit val. 
                     * If so Call NPAPI to set max learnlimit.                
                     */
                    if (ISS_SWITCH_MAC_MAX_LEARN_LIMIT != i4TempMaxLearnLimit)
                    {
                        IsssysIssHwSetMacLearningRateLimit
                            (ISS_SWITCH_MAC_MAX_LEARN_LIMIT);
                    }
                }
            }
#else
            UNUSED_PARAM (i4SwitchMacAddrCount);
            UNUSED_PARAM (i4TempMaxLearnLimit);
#endif
            TmrStartTimer (ISS_MAC_LEARN_LIMIT_TMRID, pExpiredTmr,
                           ISS_MAC_LEARN_LIMIT_DUR_SEC *
                           SYS_TIME_TICKS_IN_A_SEC);
        }
    }

    ISS_TRC (ISS_TRC_FUNC_ENTRY_EXIT, "Exiting the function"
             "IssMacLearnLimitProcessTimeout \r\n");
}

/*****************************************************************************
 *  Function Name   : IssProcessPktFromCfa
 *  Description     : This function is called to post event to ISS
 *                    on recieving peer info msg from cfa
 *
 *  Input(s)        : pBuf - Pointer to the input packet buffer
 *  Output(s)       : None.
 *  Returns         : None.
 ***************************************************************/
VOID
IssProcessPktFromCfa (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    ISS_TRC (ISS_TRC_FUNC_ENTRY_EXIT, "Entering the function"
             "IssProcessPktFromCfa \r\n");

    UNUSED_PARAM (pBuf);

    ISS_TRC (ISS_TRC_FUNC_ENTRY_EXIT, "Exiting the function"
             "IssProcessPktFromCfa \r\n");
    return;
}
#endif /* _ISSFM_C */
