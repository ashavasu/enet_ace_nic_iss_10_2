/*****************************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved 
* $Id: issipmgr.c,v 1.16 2013/11/29 10:58:33 siva Exp $ 
* Licensee Aricent Inc., 2001-2002                         
*****************************************************************************/
/*    FILE  NAME            : issipmgr.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*    DESCRIPTION           : This file has CLI related routines  required   */
/*                            for the general features of IPAuthMgr          */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*  1.0       July, 6, 2003           Initial Creation.                      */
/*---------------------------------------------------------------------------*/
/*****************************************************************************/

#ifndef _ISSIPMGR_C
#define _ISSIPMGR_C

#ifndef KERNEL_WANTED
#include "issinc.h"
#else
#include "isswinc.h"

extern UINT4        NpGetRportIfIndex (UINT4 u4VlanId);
#endif

/**************************************************************************
 * Function Name     : IssIpAuthMgrInit ()
 *
 * Description       : This function initialises the global Filter table.
 * 
 * Generic manager -> IP address = 0,IP Mask = 0.
 * This facility is provided to create a rule over the interfaces,
 * Vlans and services.
 * If generic entry is found in the table,all IP address satisfying
 * the rule will be allowed to access the system. 
 *
 * Specifc manager having individual IP address and mask.
 * Rule will be created based upon the particular manager or a
 * particular network.
 * Based upon the IP address from which the packet arrived,
 * respective rule from the table will be verified.
 * 
 * In table 0th entry is used for generic managers (0/0)
 * For specific managers 1-10 entries will be used.
 * Including generic and specific managers, upto the maximum of 10
 * managers can be created.
 * 
 * If generic manager is not in the table, then only 10th entry
 * will be used for specific manager.
 *
 * Input(s)          : NONE.                                    
 *
 * Output(s)         : None.
 *
 * Global Variables 
 * Referred          : gIssIpMgrTable. 
 *
 * Global Variables 
 * Modified          : gIssIpMgrTable. 
 *
 * Exceptions or OS
 * Error Handling    : None.
 *
 * Use of Recursion  : None.
 *
 * Returns           : ISS_SUCCESS/ISS_FAILURE
 *
**************************************************************************/
INT4
IssIpAuthMgrInit ()
{

    UINT4               u4Index;

    gIssIpMgrTable.pIpMgrEntry =
        (tIssIpMgrEntry *) (VOID *) &gau1IssIpMgrArr[0];

    /* By default authorized manager list is empty */
    gIssIpMgrTable.u4Count = 0;

    /* By default active managers list is empty */
    gu4IssIpAuthActiveManagers = 0;

    /* To include the generic manager in the list, u4Index starts from 0 */
    for (u4Index = 0; u4Index <= ISS_MAX_MGR_IP_ENTRIES; u4Index++)
    {
        gIssIpMgrTable.pIpMgrEntry[u4Index].i4Status = ISS_IP_AUTH_INVALID;
    }

    for (u4Index = 0; u4Index < CLI_NUM_SSH_CLIENT_WATCH; u4Index++)
    {
        MEMSET (&gIssIpMgrBlockTable[u4Index], 0, sizeof (tIssIpMgrBlockTable));
    }

    return ISS_SUCCESS;
}

/*******************************************************************************
 * Function Name    : IssApplyFilter () 
 *
 * Description      : This function applies the ISS filter based on the 
 *                    Protocol and Source IP address.
 *
 * Input(s)         : u2InPort - The port on which the frame is received.
 *                    InVlan - The Vlan on which the frame is received.
 *                    u4SrcIp -The source ip address of the frame received.
 *                    u1L3Proto -The protocol associated with frame received .
 *                    u2L4DstPort -The application to which the frame is 
 *                    destined .
 *                    bOOBFlag -The flag indicating whether the received port
 *                    is a management port or not.
 *
 * Output(s)        : None.
 *
 * Global Variables 
 * Referred         : gIssIpMgrTable.
 *
 * Global Variables 
 * Modified         : None.                  
 *
 * Exceptions or OS
 * Error Handling   : None.
 *
 * Use of Recursion : None.
 *
 * Returns          : ISS_SUCCESS, if the frame is to be processed further.
 *                    ISS_FAILURE, if the frame it to be dropped.
 ******************************************************************************/
INT4
IssApplyFilter (UINT2 u2InPort, UINT2 InVlan, UINT4 u4SrcIp, UINT1 u1L3Proto,
                UINT2 u2L4DstPort, BOOL1 bOOBFlag)
{
    UINT4               u4Index = 0;
    UINT4               u4Count = 0;
    UINT4               u4IsMgmtPkt = ISS_FALSE;
    UINT4               u4Found = ISS_FALSE;
    tIssIpMgrEntry     *pIpMgrEntry = NULL;
    UINT1              *pu1VlanList = NULL;
    tCfaIfInfo          IfInfo;
    UINT4               u4LPMIpAddr = 0;    /*Longest Prefix Match */
    UINT4               u4LPMIpMask = 0;
    UINT4               u4TempIndex = 0;
#ifdef KERNEL_WANTED
    UINT4               u4Rport = 0;
#endif
    UINT2               u2AggId = 0;
    INT4                i4Count = 0;

    for (i4Count = 0; i4Count < CLI_NUM_SSH_CLIENT_WATCH; i4Count++)
    {
        if ((gIssIpMgrBlockTable[i4Count].u4IpAddr == u4SrcIp) &&
            (gIssIpMgrBlockTable[i4Count].u4Port == (UINT4) u2L4DstPort))
        {
            return ISS_FAILURE;
        }
    }

    /* If active managers list is empty, no need to apply filter.
     * So all the management packets will be allowed into the system. */

    MEMSET (&IfInfo, 0, sizeof (IfInfo));

    pu1VlanList = UtilVlanAllocVlanListSize (sizeof (tIssVlanList));

    if (pu1VlanList == NULL)
    {
        return ISS_FAILURE;
    }

    MEMSET (pu1VlanList, 0xff, ISS_VLAN_LIST_SIZE - 1);

    /* To set values for the last byte in au1VlanList.
     * Since max vlan id may vary, to be general this
     * function will be used. */
    ISS_LAST_BYTE_IN_PORTLIST (pu1VlanList, ISS_MAX_VLAN_ID,
                               ISS_VLAN_LIST_SIZE);

    if (gu4IssIpAuthActiveManagers == 0)
    {
        UtilVlanReleaseVlanListSize (pu1VlanList);
        return ISS_SUCCESS;
    }

    /* If the Packet received on a port other than Management Port,Check it
     * is in that available range of Physical Ports and VLAN */
    if (bOOBFlag == OSIX_FALSE)
    {
        /* If port no cross the range, packet will be discarded. */
        if (u2InPort < ISS_MIN_PORTS || u2InPort > ISS_MAX_PORTS)
        {
            UtilVlanReleaseVlanListSize (pu1VlanList);
            return ISS_FAILURE;
        }
        /* In LinuxIp case, get the router port info from NPAPI module
         * Else get the router port info frm CFA Module */
#ifdef KERNEL_WANTED
        u4Rport = NpGetRportIfIndex (InVlan);

        if (u4Rport == 0)
        {
            IfInfo.u1BridgedIface = CFA_ENABLED;
        }
        else
        {
            IfInfo.u1BridgedIface = CFA_DISABLED;
        }
#else
        CfaGetIfInfo (u2InPort, &IfInfo);
#endif
        /* If Vlan id cross the range, packet will be discarded. */
        if (InVlan < ISS_MIN_VLAN_ID || InVlan > ISS_MAX_VLAN_ID)
        {
            /* Check whether the port is a router port. For Router port, incoming
             * vlan - InVlan - will be 0. So do not return failure */
            if (IfInfo.u1BridgedIface != CFA_DISABLED)
            {
                UtilVlanReleaseVlanListSize (pu1VlanList);
                return ISS_FAILURE;
            }
        }
    }

    /* Filer will be applied only for management packet.
     * If the recieved packet is not management packet, no need
     * to apply filter over it. */
    IssCheckForMgmtPkt (u1L3Proto, u2L4DstPort, &u4IsMgmtPkt);

    if (u4IsMgmtPkt == ISS_FALSE)
    {
        UtilVlanReleaseVlanListSize (pu1VlanList);
        return ISS_SUCCESS;
    }

    ISS_LOCK ();

    /* First check for specific managers then go to generic manager */
    for (u4Index = 1; u4Index <= ISS_MAX_MGR_IP_ENTRIES; u4Index++)
    {
        pIpMgrEntry = ISS_GET_IP_MGR_ENTRY (u4Index);

        /* Increment the counter only when the entry is not invalid */
        if (pIpMgrEntry->i4Status != ISS_IP_AUTH_INVALID)
        {
            if ((u4SrcIp & pIpMgrEntry->u4IpMask) == pIpMgrEntry->u4IpAddr)
            {
                if (pIpMgrEntry->i4Status == ISS_IP_AUTH_ACTIVE)
                {
                    if ((u4LPMIpAddr < pIpMgrEntry->u4IpAddr) ||
                        ((u4LPMIpAddr == pIpMgrEntry->u4IpAddr)
                         && (u4LPMIpMask < pIpMgrEntry->u4IpMask)))
                    {
                        u4LPMIpAddr = pIpMgrEntry->u4IpAddr;
                        u4LPMIpMask = pIpMgrEntry->u4IpMask;
                        u4TempIndex = u4Index;
                        u4Found = ISS_TRUE;
                    }

                }
            }

            u4Count++;

            /* If the counter reaches the gIssIpMgrTable.u4Count, no need to 
             * check further */
            if (u4Count == gIssIpMgrTable.u4Count && u4Found == ISS_FALSE)
            {
                ISS_UNLOCK ();
                UtilVlanReleaseVlanListSize (pu1VlanList);
                return ISS_FAILURE;
            }
        }
    }
    u4Index = u4TempIndex;
    /* If there is no match in the specific entries, check for generic entry */
    if (u4Found == ISS_FALSE)
    {
        pIpMgrEntry = ISS_GET_IP_MGR_ENTRY (0);
        if (pIpMgrEntry->i4Status == ISS_IP_AUTH_ACTIVE)
        {
            u4Index = 0;
            u4Found = ISS_TRUE;
        }
    }

    /* Filters will be applied only when there is a match in table,
     * else packet will be discarded. */
    if (u4Found == ISS_TRUE)
    {
        /* Based on the u2L4DstPort, particular service will be 
         * identified and apply filter over it. */
        if (IssApplyProtocolFilter (u4Index, u2L4DstPort) == ISS_FAILURE)
        {
            ISS_UNLOCK ();
            UtilVlanReleaseVlanListSize (pu1VlanList);
            return ISS_FAILURE;
        }

        /* If the packet is received on OOB Interface,Apply filter
         * Based on IpAuthManager Over OOB Port.Vlan/Port Filters
         * are not applicaple to OOB Interface */
        if (bOOBFlag == OSIX_TRUE)
        {
            if (IssApplyOobFilter (u4Index) == ISS_SUCCESS)
            {
                ISS_UNLOCK ();
                UtilVlanReleaseVlanListSize (pu1VlanList);
                return ISS_SUCCESS;
            }
            else
            {
                ISS_UNLOCK ();
                UtilVlanReleaseVlanListSize (pu1VlanList);
                return ISS_FAILURE;
            }
        }

        /* Based on the port through which packet arrived, filter
         * will be applied. */
        if (IssL2IwfIsPortInPortChannel (u2InPort) == L2IWF_SUCCESS)
        {
            if (IssL2IwfGetPortChannelForPort ((UINT4) u2InPort,
                                               &u2AggId) == L2IWF_FAILURE)
            {
                ISS_UNLOCK ();
                UtilVlanReleaseVlanListSize (pu1VlanList);
                return ISS_FAILURE;
            }
        }

        /* Check whether the port is there in the portlist */
        if (IssApplyPortFilter (u4Index, u2InPort) == ISS_FAILURE)
        {
            /* Check whether the portchannel is there in the list */
            if (u2AggId != 0)
            {
                if (IssApplyPortFilter (u4Index, u2AggId) == ISS_FAILURE)
                {
                    ISS_UNLOCK ();
                    UtilVlanReleaseVlanListSize (pu1VlanList);
                    return ISS_FAILURE;
                }
            }
            else
            {
                ISS_UNLOCK ();
                UtilVlanReleaseVlanListSize (pu1VlanList);
                return ISS_FAILURE;
            }
        }

        /* Check for router port configuration */
        if (IfInfo.u1BridgedIface == CFA_DISABLED)
        {
            /* If the incoming port is a router port, do not check the 
             * incoming Vlan. Instead, check whether the IpMgrEntry contains
             * all Vlans allowed. */
            if (MEMCMP (pu1VlanList, pIpMgrEntry->IpAuthVlanList,
                        ISS_VLAN_LIST_SIZE) != 0)
            {
                ISS_UNLOCK ();
                UtilVlanReleaseVlanListSize (pu1VlanList);
                return ISS_FAILURE;
            }
        }
        else
        {
            /* Based on the Vlan id from which packet arrives, filter
             * will be applied. */
            if (IssApplyVlanFilter (u4Index, InVlan) == ISS_FAILURE)
            {
                ISS_UNLOCK ();
                UtilVlanReleaseVlanListSize (pu1VlanList);
                return ISS_FAILURE;
            }
        }

        /* If all the rules get satisfied, packet will be allowed */
        ISS_UNLOCK ();
        UtilVlanReleaseVlanListSize (pu1VlanList);
        return ISS_SUCCESS;
    }

    ISS_UNLOCK ();
    UtilVlanReleaseVlanListSize (pu1VlanList);
    return ISS_FAILURE;
}

/*******************************************************************************
 * Function Name    : IssCheckForMgmtPkt () 
 *
 * Description      : This function checke whether recieved packet is 
 *                    Protocol or not.
 *
 * Input(s)         : u1L3Proto   - Protocol field present in the IP header.
 *                    u2L4DstPort - Destination port present in L4 header.
 *                pIsMgmtPkt  - Return value. 
 *     
 * Output(s)        : None.
 *
 * Global Variables 
 * Referred         : gIssIpMgrTable
 *
 * Global Variables 
 * Modified         : None.                  
 *
 * Exceptions or OS
 * Error Handling   : None.
 *
 * Use of Recursion : None.
 *
 * Returns          : None.
 ******************************************************************************/
VOID
IssCheckForMgmtPkt (UINT1 u1L3Proto, UINT2 u2L4DstPort, UINT4 *pu4IsMgmtPkt)
{
    INT4                i4Result = ISS_FALSE;

    *pu4IsMgmtPkt = ISS_FALSE;

    /* To identify management packet u2L4DstPort will be checked with 
     * the port no's of the management services. */

    ISS_IS_PKT_SNMP (i4Result, u1L3Proto, u2L4DstPort);
    if (i4Result == ISS_TRUE)
    {
        *pu4IsMgmtPkt = ISS_TRUE;
        return;
    }

#ifdef CLI_WANTED
    ISS_IS_PKT_TELNET (i4Result, u1L3Proto, u2L4DstPort);
    if (i4Result == ISS_TRUE)
    {
        *pu4IsMgmtPkt = ISS_TRUE;
        return;
    }
#endif

#ifdef WEBNM_WANTED
    ISS_IS_PKT_HTTP (i4Result, u1L3Proto, u2L4DstPort);
    if (i4Result == ISS_TRUE)
    {
        *pu4IsMgmtPkt = ISS_TRUE;
        return;
    }
#endif
    ISS_IS_PKT_HTTPS (i4Result, u1L3Proto, u2L4DstPort);
    if (i4Result == ISS_TRUE)
    {
        *pu4IsMgmtPkt = ISS_TRUE;
        return;
    }

    ISS_IS_PKT_SSH (i4Result, u1L3Proto, u2L4DstPort);
    if (i4Result == ISS_TRUE)
    {
        *pu4IsMgmtPkt = ISS_TRUE;
        return;
    }

    return;
}

/*******************************************************************************
* Function Name    : IssApplyPortFilter () 
*
* Description      : This function applies the ISS filter based on the 
*                    Port.
*
* Input(s)         : u2InPort  - The port on which the frame is received.
*                    u4Index   - Index of the authorized manager in the 
*                                gIssIpMgrTable.
* Output(s)        : None.
*
* Global Variables 
* Referred         : gIssIpMgrTable
*
* Global Variables 
* Modified         : None.                  
*
* Exceptions or OS
* Error Handling   : None.
*
* Use of Recursion : None.
*
* Returns          : ISS_SUCCESS, if the frame is to be processed further.
*                    ISS_FAILURE, if the frame it to be dropped.
******************************************************************************/
INT4
IssApplyPortFilter (UINT4 u4Index, UINT2 u2InPort)
{
    UINT1               u1Result = ISS_FALSE;
    tIssIpMgrEntry     *pIssIpMgrEntry = NULL;

    pIssIpMgrEntry = ISS_GET_IP_MGR_ENTRY (u4Index);

    /* Check u2InPort is a member in the PortList of manager. */
    ISS_IS_MEMBER_OF_PORTLIST (pIssIpMgrEntry->IpAuthPortList,
                               u2InPort, u1Result);
    if (u1Result == ISS_TRUE)
    {
        return ISS_SUCCESS;
    }
    else
    {
        return ISS_FAILURE;
    }
}

/*******************************************************************************
* Function Name    : IssApplyOobFilter () 
*
* Description      : This function applies the ISS filter for the OOB Port 
*                    
*
* Input(s)         :  u4Index   - Index of the authorized manager in the 
*                                 gIssIpMgrTable.
* Output(s)        : None.
*
* Global Variables 
* Referred         : gIssIpMgrTable
*
* Global Variables 
* Modified         : None.                  
*
* Exceptions or OS
* Error Handling   : None.
*
* Use of Recursion : None.
*
* Returns          : ISS_SUCCESS, if the frame is to be processed further.
*                    ISS_FAILURE, if the frame it to be dropped.
******************************************************************************/
INT4
IssApplyOobFilter (UINT4 u4Index)
{
    tIssIpMgrEntry     *pIssIpMgrEntry = NULL;

    pIssIpMgrEntry = ISS_GET_IP_MGR_ENTRY (u4Index);

    if (pIssIpMgrEntry->u1IpAuthOobPort == ISS_TRUE)
    {
        return ISS_SUCCESS;
    }
    else
    {
        return ISS_FAILURE;
    }
}

/*******************************************************************************
* Function Name    : IssApplyVlanFilter () 
*
* Description      : This function applies the ISS filter based on the 
*                    Vlan.
*
* Input(s)         : u2InVlan  - The Vlan on which the frame is received.
*                    u4Index   - Index of the authorized manager in the 
*                                gIssIpMgrTable.
* Output(s)        : None.
*
* Global Variables 
* Referred         : gIssIpMgrTable
*
* Global Variables 
* Modified         : None.                  
*
* Exceptions or OS
* Error Handling   : None.
*
* Use of Recursion : None.
*
* Returns          : ISS_SUCCESS, if the frame is to be processed further.
*                    ISS_FAILURE, if the frame it to be dropped.
*******************************************************************************/
INT4
IssApplyVlanFilter (UINT4 u4Index, UINT2 u2InVlan)
{
    UINT1               u1Result = ISS_FALSE;
    tIssIpMgrEntry     *pIssIpMgrEntry = NULL;

    pIssIpMgrEntry = ISS_GET_IP_MGR_ENTRY (u4Index);

    /* Check u2InVlan is a member in the VlanList of manager */
    ISS_IS_MEMBER_OF_PORTLIST (pIssIpMgrEntry->IpAuthVlanList,
                               u2InVlan, u1Result);
    if (u1Result == ISS_TRUE)
    {
        return ISS_SUCCESS;
    }
    else
    {
        return ISS_FAILURE;
    }
}

/*******************************************************************************
 * Function Name    : IssApplyProtocolFilter () 
 *
 * Description      : This function applies the ISS filter based on the 
 *                    Protocol.
 *
 * Input(s)         : u4Index  - The index of the authorized manager entry.
 *                               in the gIssIpMgrTable.       
 *                    u4DstPort - Port in which management packet arrives.
 *                               
 * Output(s)        : None.
 *
 * Global Variables 
 * Referred         : gIssIpMgrTable.
 *
 * Global Variables 
 * Modified         : None.                  
 *
 * Exceptions or OS
 * Error Handling   : None.
 *
 * Use of Recursion : None.
 *
 * Returns          : ISS_SUCCESS, if the frame is to be processed further.
 *                    ISS_FAILURE, if the frame it to be dropped.
 *******************************************************************************/
INT4
IssApplyProtocolFilter (UINT4 u4Index, UINT4 u4DstPort)
{

    /* u4DstPort will be checked against the allowed services of manager. */
#ifdef WEBNM_WANTED
    if (u4DstPort == (UINT4) HttpGetSourcePort ())
    {

        if (ISS_IS_HTTP_FILTERED (u4Index) == ISS_FALSE)
        {
            return ISS_SUCCESS;
        }
    }
#endif

#ifdef CLI_WANTED
    if (u4DstPort == (UINT4) CliGetTelnetPort ())
    {
        if (ISS_IS_TELNET_FILTERED (u4Index) == ISS_FALSE)
        {
            return ISS_SUCCESS;
        }
    }
#endif

    switch (u4DstPort)
    {
        case ISS_SNMP_PORT:

            if (ISS_IS_SNMP_FILTERED (u4Index) == ISS_FALSE)
            {
                return ISS_SUCCESS;
            }
            break;

        case ISS_HTTPS_PORT:

            if (ISS_IS_HTTPS_FILTERED (u4Index) == ISS_FALSE)
            {
                return ISS_SUCCESS;
            }
            break;

        case ISS_SSH_PORT:

            if (ISS_IS_SSH_FILTERED (u4Index) == ISS_FALSE)
            {
                return ISS_SUCCESS;
            }
            break;

        default:

            break;
    }
    return ISS_FAILURE;
}

/*******************************************************************************
 * Function Name    : IssFindIpAuthManager () 
 *
 * Description      : This function finds the valid, matching Ip auth manager 
 *                    from the gIssIpMgrTable 
 *
 * Input(s)         : u4IpAddr - Ip address of manager to find.
 *                    u4IpMask - Ip address mask.
 *                    pIpMgrEntry - Return pointer for location of 
 *                                  Ip Auth Manager.
 *                               
 * Output(s)        : None.
 *
 * Global Variables 
 * Referred         : gIssIpMgrTable.
 *
 * Global Variables 
 * Modified         : None.                  
 *
 * Exceptions or OS
 * Error Handling   : None.
 *
 * Use of Recursion : None.
 *
 * Returns          : ISS_SUCCESS, if the Manager is found.
 *                    ISS_FAILURE, if not found.
 ******************************************************************************/
INT4
IssFindIpAuthManager (UINT4 u4IpAddr, UINT4 u4IpMask,
                      tIssIpMgrEntry ** pIpAuthMgr)
{
    tIssIpMgrEntry     *pIpMgrEntry;
    UINT4               u4Index;

    /* To include the generic manager in the list, u4Index starts from 0 */
    for (u4Index = 0; u4Index <= ISS_MAX_MGR_IP_ENTRIES; u4Index++)
    {
        pIpMgrEntry = ISS_GET_IP_MGR_ENTRY (u4Index);

        if ((pIpMgrEntry->u4IpAddr == u4IpAddr) &&
            (pIpMgrEntry->u4IpMask == u4IpMask) &&
            (pIpMgrEntry->i4Status != ISS_IP_AUTH_INVALID))
        {
            *pIpAuthMgr = pIpMgrEntry;
            return ISS_SUCCESS;
        }
    }
    return ISS_FAILURE;
}

/*******************************************************************************
 * Function Name    :  IssFindFreeEntryInTable() 
 *
 * Description      : This function finds free entry for Ip auth manager 
 *                    in gIssIpMgrTable 
 *
 * Input(s)         : pIpAuthMgr - Return pointer of free entry .
 *                               
 * Output(s)        : None.
 *
 * Global Variables 
 * Referred         : gIssIpMgrTable.
 *
 * Global Variables 
 * Modified         : None.                  
 *
 * Exceptions or OS
 * Error Handling   : None.
 *
 * Use of Recursion : None.
 *
 * Returns          : ISS_SUCCESS, if the free entry is found.
 *                    ISS_FAILURE, if not found.
 ******************************************************************************/
INT4
IssFindFreeEntryInTable (tIssIpMgrEntry ** pIpAuthMgr)
{
    UINT4               u4Index;
    tIssIpMgrEntry     *pIpMgrEntry;

    /* This search for free entry is for specific managers.
     * So first search in 1-9 indexes */
    for (u4Index = 1; u4Index < ISS_MAX_MGR_IP_ENTRIES; u4Index++)
    {
        pIpMgrEntry = ISS_GET_IP_MGR_ENTRY (u4Index);
        if (pIpMgrEntry->i4Status == ISS_IP_AUTH_INVALID)
        {
            *pIpAuthMgr = pIpMgrEntry;
            return ISS_SUCCESS;
        }
    }

    /* If free entry is not found in 1-9 indexes. 
     * ------------------------------------------
     * Since only 10 managers can be allowed in table, check for 
     * generic manager's presence.
     * If generic manager is not in table, specific manager can use
     * 10th index. */

    pIpMgrEntry = ISS_GET_IP_MGR_ENTRY (0);
    if (pIpMgrEntry->i4Status == ISS_IP_AUTH_INVALID)
    {
        pIpMgrEntry = ISS_GET_IP_MGR_ENTRY (ISS_MAX_MGR_IP_ENTRIES);
        if (pIpMgrEntry->i4Status == ISS_IP_AUTH_INVALID)
        {
            *pIpAuthMgr = pIpMgrEntry;
            return ISS_SUCCESS;
        }
    }
    return ISS_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssValidateSubnetMask                            */
/*                                                                          */
/*    Description        : This function is validates the given subnet mask */
/*                         with the cidr table. This function is a          */
/*                         duplication of IssValidateCidrSubnetMask defined */
/*                         in isssys.c. This is to solve the compilation    */
/*                         problem in LINUX IP.                             */
/*                                                                          */
/*    Input(s)           : u4SubnetMask - SubNetMask.                       */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ISS_SUCCESS / ISS_FAILURE                        */
/****************************************************************************/

INT4
IssValidateSubnetMask (UINT4 u4SubnetMask)
{
    UINT1               u1Counter;

    /* The valid subnet address is from 0 to 32 */
    for (u1Counter = 0; u1Counter <= ISS_MAX_CIDR; ++u1Counter)
    {
        if (gu4IssIpCidrSubnetMask[u1Counter] == u4SubnetMask)
        {
            break;
        }
    }

    if (u1Counter > ISS_MAX_CIDR)
    {
        return ISS_FAILURE;
    }
    else
    {
        return ISS_SUCCESS;
    }

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IpMgrBlockTableInsert                            */
/*                                                                          */
/*    Description        : This function is to insert ip address and port   */
/*                         number details into gIssIpMgrBlockTable.         */
/*                         Adding a node here, ensures if incoming packet   */
/*                         is having this IP and port, that packet is       */
/*                         dropped.                                         */
/*                                                                          */
/*    Input(s)           : u4IpAddr - IP address for inserting into IP      */
/*                         Manager table.                                   */
/*                         u4Port - Port details for inserting into IP      */
/*                         Manager table.                                   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
IpMgrBlockTableInsert (UINT4 u4IpAddr, UINT4 u4Port)
{
    INT4                i4Count = 0;
    for (i4Count = 0; i4Count < CLI_NUM_SSH_CLIENT_WATCH; i4Count++)
    {
        if ((gIssIpMgrBlockTable[i4Count].u4IpAddr == u4IpAddr) &&
            (gIssIpMgrBlockTable[i4Count].u4Port == u4Port))
        {
            return;
        }
    }

    for (i4Count = 0; i4Count < CLI_NUM_SSH_CLIENT_WATCH; i4Count++)
    {
        if (gIssIpMgrBlockTable[i4Count].u4IpAddr == 0)
        {
            gIssIpMgrBlockTable[i4Count].u4IpAddr = u4IpAddr;
            gIssIpMgrBlockTable[i4Count].u4Port = u4Port;
            return;
        }
    }

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IpMgrBlockTableDelete                            */
/*                                                                          */
/*    Description        : This function is to remove entry from blocking   */
/*                         table. so if packet comes with this IP and port  */
/*                         it will not be blocked.                          */
/*                                                                          */
/*    Input(s)           : u4IpAddr - IP address for deleting entry from    */
/*                         Manager table.                                   */
/*                         u4Port - Port details for inserting into IP      */
/*                         Manager table.                                   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
IpMgrBlockTableDelete (UINT4 u4IpAddr, UINT4 u4Port)
{
    INT4                i4Count = 0;
    for (i4Count = 0; i4Count < CLI_NUM_SSH_CLIENT_WATCH; i4Count++)
    {
        if ((gIssIpMgrBlockTable[i4Count].u4IpAddr == u4IpAddr) &&
            (gIssIpMgrBlockTable[i4Count].u4Port == u4Port))
        {
            gIssIpMgrBlockTable[i4Count].u4IpAddr = 0;
            gIssIpMgrBlockTable[i4Count].u4Port = 0;
            return;
        }
    }
}

#endif /* _ISSIPMGR_C */
