/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/* $Id: isspicli.c,v 1.11 2015/07/23 11:11:07 siva Exp $ */
/*****************************************************************************/
/*    FILE  NAME            : isspicli.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Port Isolation                                 */
/*    MODULE NAME           : ISS                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Linux                                          */
/*    DATE OF FIRST RELEASE : 21 July 2010                                   */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains CLI handlers for the        */
/*                            port isolation table manipulation.             */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    21 July 2010           Initial Creation                        */
/*---------------------------------------------------------------------------*/

#ifndef ISSPICLI_C
#define ISSPICLI_C

#include "issinc.h"
#include "isspiinc.h"
#include "isscli.h"
#include "fsisscli.h"
#include "fsisswr.h"
#include "utilcli.h"

/*****************************************************************************/
/*  Function Name   : cli_process_iss_pi_cmd                                 */
/*  Description     : This function servers as the handler for Port Isolation*/
/*                    related CLI commands                                   */
/*  Input(s)        :                                                        */
/*                    CliHandle - CLI Handle                                 */
/*                    u4Command - Command given by user                      */
/*                    Variable set of inputs depending on                    */
/*                    the user command                                       */
/* Global variables referred : None                                          */
/* Global variables modified : None                                          */
/* Exceptions or Error Handling: CLI_SET_ERR_CODE macro is used for error    */
/*                               handling.                                   */
/* Output(s)       : Error message - on failure                              */
/* Returns         : None                                                    */
/*****************************************************************************/

VOID
cli_process_iss_pi_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[CLI_ISS_MAX_ARGS];
    UINT4               au4PortArray[ISS_MAX_UPLINK_PORTS];
    UINT4               u4ErrCode = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4IfType = 0;
    UINT4               u4Count = 0;
    UINT4               u4MaxCount = 0;
    INT4                i4Inst = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT4               u4VlanId = 0;
    INT1                i1argno = 0;

    MEMSET (au4PortArray, 0, (sizeof (UINT4) * ISS_MAX_UPLINK_PORTS));
    va_start (ap, u4Command);
    /* Third arguement is always interface name/index */
    i4Inst = va_arg (ap, INT4);
    UNUSED_PARAM (i4Inst);
    /* Walk through the rest of the arguements and store in args array.  */

    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == CLI_ISS_MAX_ARGS)
        {
            break;
        }
    }                            /* End of while */

    va_end (ap);

    CLI_SET_ERR (0);

    CliRegisterLock (CliHandle, IssLock, IssUnLock);

    ISS_LOCK ();

    switch (u4Command)
    {
        case CLI_ISS_CREATE_PORT_ISOLATION:
        case CLI_ISS_ADD_PORT_ISOLATION:
        case CLI_ISS_REM_PORT_ISOLATION:
            /* args[0] -  Vlan Id (optional) */
            /* args[1] -  Interface type1 */
            /* args[2] -  Interface list */
            /* args[3] -  Interface type2 */
            /* args[4] -  Interface list */
            /* args[5] -  Interface type2 */
            /* args[6] -  Interface list */

            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();

            /*This NULL check is added since Vlan Id is an optional parameter */
            if (args[0] != NULL)
            {
                MEMCPY (&u4VlanId, args[0], sizeof (UINT4));
            }

            /* Check if fast ethernet ports are given */
            if (args[1] != NULL)
            {
                if (CfaCliValidateXInterfaceName ((INT1 *) args[1], NULL,
                                                  &u4IfType) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "%% Invalid Interface type\r\n");
                    CliUnRegisterLock (CliHandle);
                    ISS_UNLOCK ();
                    return;
                }

            }

            if (args[2] != NULL)
            {
                if (IssPIConvertStrToPortArray (CliHandle,
                                                (UINT1 *) args[2],
                                                (au4PortArray + u4MaxCount),
                                                &u4Count, ISS_MAX_UPLINK_PORTS,
                                                u4IfType) == CLI_FAILURE)
                {
                    CliUnRegisterLock (CliHandle);
                    ISS_UNLOCK ();
                    return;
                }
                u4MaxCount += u4Count;
                u4Count = 0;
            }

            /* Check if gigabit ethernet ports are given */
            if (args[3] != NULL)
            {
                if (CfaCliValidateXInterfaceName ((INT1 *) args[3], NULL,
                                                  &u4IfType) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "%% Invalid Interface type\r\n");
                    CliUnRegisterLock (CliHandle);
                    ISS_UNLOCK ();
                    return;
                }

            }
            if (args[4] != NULL)
            {
                if (IssPIConvertStrToPortArray (CliHandle,
                                                (UINT1 *) args[4],
                                                (au4PortArray + u4MaxCount),
                                                &u4Count, ISS_MAX_UPLINK_PORTS,
                                                u4IfType) == CLI_FAILURE)
                {
                    CliUnRegisterLock (CliHandle);
                    ISS_UNLOCK ();
                    return;
                }
                u4MaxCount += u4Count;
                if (u4MaxCount > ISS_MAX_UPLINK_PORTS)
                {
                    CliPrintf (CliHandle,
                               "%% Only %d ports can be configured as Uplink ports\r\n",
                               ISS_MAX_UPLINK_PORTS);
                    break;
                }

                u4Count = 0;
            }

            /* Check if port channel ports are given */
            if (args[5] != NULL)
            {
                if (CfaCliValidateXInterfaceName ((INT1 *) args[5], NULL,
                                                  &u4IfType) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "%% Invalid Interface type\r\n");
                    CliUnRegisterLock (CliHandle);
                    ISS_UNLOCK ();
                    return;
                }

            }

            /* IssPIConvertStrToPortArray- 
             * Converts port list format to array of interface indices. 
             * e.g. 0/1, 0/3, 0/10 is converted to interface indices and 
             * stored in au4TempPortArray sequentially. 
             */
            if (args[6] != NULL)
            {
                if (IssPIConvertStrToPortArray (CliHandle,
                                                (UINT1 *) args[6],
                                                (au4PortArray + u4MaxCount),
                                                &u4Count, ISS_MAX_UPLINK_PORTS,
                                                u4IfType) == CLI_FAILURE)
                {
                    CliUnRegisterLock (CliHandle);
                    ISS_UNLOCK ();
                    return;
                }

                u4MaxCount += u4Count;
                if (u4MaxCount > ISS_MAX_UPLINK_PORTS)
                {
                    CliPrintf (CliHandle,
                               "%% Only %d ports can be configured as Uplink ports\r\n",
                               ISS_MAX_UPLINK_PORTS);
                    break;
                }

                u4Count = 0;
            }

            if (CLI_ISS_CREATE_PORT_ISOLATION == u4Command)
            {
                i4RetStatus = IssPICliCreatePortIsolation (CliHandle,
                                                           u4IfIndex,
                                                           (tVlanId) u4VlanId,
                                                           au4PortArray);
            }
            else if (CLI_ISS_ADD_PORT_ISOLATION == u4Command)
            {
                i4RetStatus = IssPICliAddPortIsolation (CliHandle,
                                                        u4IfIndex,
                                                        (tVlanId) u4VlanId,
                                                        au4PortArray);
            }
            else
            {
                i4RetStatus = IssPICliDelPortIsolation (CliHandle,
                                                        u4IfIndex,
                                                        (tVlanId) u4VlanId,
                                                        au4PortArray);
            }
            break;

        case CLI_ISS_DEL_PORT_ISOLATION:

            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = IssPICliDelPortIsolation (CliHandle,
                                                    u4IfIndex,
                                                    (tVlanId) u4VlanId, NULL);
            break;

        case CLI_ISS_SHOW_PORT_ISOLATION:
            /* args[0] - Ingress interface type */
            /* args[1] - Ingress interface index */

            /* Check if a valid port is given as input */
            if (args[0] != NULL)
            {
                if (CfaCliValidateXInterfaceName ((INT1 *) args[0], NULL,
                                                  &u4IfType) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "%% Invalid Interface type\r\n");
                    CliUnRegisterLock (CliHandle);
                    ISS_UNLOCK ();
                    return;
                }

            }
            else
            {
                i4RetStatus = IssPICliShowPortIsolation (CliHandle, 0);
                break;
            }

            if (args[1] != NULL)
            {
                if (IssPIConvertStrToPortArray (CliHandle,
                                                (UINT1 *) args[1], &u4IfIndex,
                                                &u4Count, 1, u4IfType)
                    == CLI_FAILURE)
                {
                    CliUnRegisterLock (CliHandle);
                    ISS_UNLOCK ();
                    return;
                }

            }

            i4RetStatus =
                IssPICliShowPortIsolation (CliHandle, (INT4) u4IfIndex);
            break;

        default:
            CliPrintf (CliHandle, "\r%% Invalid Command\r\n");
            i4RetStatus = CLI_FAILURE;
            break;

    }                            /* End of switch */

    if ((CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode >= CLI_ERR_START_ID_ISS) &&
            (u4ErrCode < CLI_ISS_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s",
                       IssCliErrString[CLI_ERR_OFFSET_ISS (u4ErrCode)]);
        }

        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    CliUnRegisterLock (CliHandle);

    ISS_UNLOCK ();
    return;
}                                /* End of cli_process_iss_pi_cmd */

/*****************************************************************************/
/*  Function Name   : IssPICliAddPortIsolation                               */
/*  Description     : This function does the following                       */
/*                    (i) Add the egress ports to the Port Isolation node.   */
/*  CLI command     : port-isolation add [<integer(1-4094)>] ([<ifXtype>     */
/*                    <iface_list>]                                          */
/*                    [<ifXtype> <iface_list>])                              */
/*  Input(s)        : CliHandle - CLI Identifier                             */
/*                    u4IfIndex - Ingress Port                               */
/*                    pu4EgressList - Egress port list                       */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  CLI_FAILURE on failure                    */
/*                                 CLI_SUCCESS on success                    */
/*****************************************************************************/
INT1
IssPICliAddPortIsolation (tCliHandle CliHandle,
                          UINT4 u4IfIndex, tVlanId VlanId, UINT4 *pu4EgressList)
{
    UINT4               u4Scan = 0;
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);
    for (u4Scan = 0; u4Scan < ISS_MAX_UPLINK_PORTS; u4Scan++)
    {

        if (0 == pu4EgressList[u4Scan])
        {
            break;
        }

        if (nmhTestv2IssPortIsolationRowStatus (&u4ErrorCode,
                                                (INT4) u4IfIndex,
                                                VlanId,
                                                (INT4) pu4EgressList[u4Scan],
                                                CREATE_AND_GO) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_ISS_PI_ADD_FAILED);
            return CLI_FAILURE;
        }

        if (nmhSetIssPortIsolationRowStatus (u4IfIndex, VlanId,
                                             pu4EgressList[u4Scan],
                                             CREATE_AND_GO) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }                            /* End of for */

    return CLI_SUCCESS;

}                                /* End of IssPICliAddPortIsolation */

/*****************************************************************************/
/*  Function Name   : IssPICliCreatePortIsolation                            */
/*  Description     : This function does the following                       */
/*                    (i) Creates a  Port Isolation node.                    */
/*  CLI command     : port-isolation ([<ifXtype> <iface_list>]               */
/*                    [<ifXtype> <iface_list>])                              */
/*  Input(s)        : CliHandle - CLI Identifier                             */
/*                    u4IfIndex - Ingress Port                               */
/*                    pu4EgressList - Egress port list                       */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  CLI_FAILURE on failure                    */
/*                                 CLI_SUCCESS on success                    */
/*****************************************************************************/
INT1
IssPICliCreatePortIsolation (tCliHandle CliHandle,
                             UINT4 u4IfIndex,
                             tVlanId InVlanId, UINT4 *pu4EgressList)
{
    tIssPortIsolationEntry *pIssPortIsolationEntry = NULL;
    UINT4                   u4Scan = 0;


    pIssPortIsolationEntry = IssPIGetPortIsolationNode (u4IfIndex, InVlanId);
    if (NULL != pIssPortIsolationEntry)
    {
        /* Override the exisisting egress port list for this ingress ifindex. 
         * To acheive the above functionality, first delete the existing 
         * node, then create a new node with the provided egress list.
         */
        for (u4Scan = 0; u4Scan < ISS_MAX_UPLINK_PORTS; u4Scan++)
        {

            if (0 == pu4EgressList[u4Scan])
            {
                break;
            }
            /* Validate the indices to this table (ingressPort, egress port) */
            if (nmhValidateIndexInstanceIssPortIsolationTable
                    ((INT4) u4IfIndex,InVlanId,
                     (INT4) pu4EgressList[u4Scan]) == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_ISS_PI_ADD_FAILED);
                return SNMP_FAILURE;
            }


        }
        IssPICliUtlDelPortIsolationEntry (CliHandle,
                                          u4IfIndex, InVlanId, ISS_FALSE);
    }

    return (IssPICliAddPortIsolation (CliHandle, u4IfIndex, InVlanId,
                                      pu4EgressList));
}                                /* End of IssPICliCreatePortIsolation */

/*****************************************************************************/
/*  Function Name   : IssPICliDelPortIsolation                               */
/*  Description     : This function does the following                       */
/*                    (i) If the egress list is null, then delete all the    */
/*                        egress ports of the PI node.                       */
/*                    (i) else                                               */
/*                        if the index matches then delete the egress ports  */
/*                        that matches the pu4EgressList.                    */
/*  CLI command(s)  : (i) port-isolation del ([<ifXtype> <iface_list>]       */
/*                    [<ifXtype> <iface_list>])                              */
/*                    (ii)no port-isolation                                  */
/*  Input(s)        : CliHandle - CLI Identifier                             */
/*                    u4IfIndex - Ingress Port                               */
/*                    pu4EgressList - Egress port list                       */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  CLI_FAILURE on failure                    */
/*                                 CLI_SUCCESS on success                    */
/*****************************************************************************/
INT1
IssPICliDelPortIsolation (tCliHandle CliHandle,
                          UINT4 u4IfIndex,
                          tVlanId InVlanId, UINT4 *pu4EgressList)
{
    UINT4               u4Scan = 0;
    UINT4               u4ErrorCode = 0;

    if (NULL == pu4EgressList)
    {
        /* If the Egress List is not mentioned, then delete all 
         * the entries for this ingress index. 
         */
        return (IssPICliUtlDelPortIsolationEntry (CliHandle,
                                                  u4IfIndex,
                                                  InVlanId, ISS_TRUE));
    }
    else
    {
        /* An egress list can have a maximum of ISS_MAX_UPLINK_PORTS
         * Scan through the entire list and delete the node that 
         * matches the input. 
         */
        for (u4Scan = 0; u4Scan < ISS_MAX_UPLINK_PORTS; u4Scan++)
        {
            if (0 == pu4EgressList[u4Scan])
            {
                /* Entries are not found in this index u4Scan. */
                break;
            }
            else
            {
                if (SNMP_FAILURE ==
                    (nmhTestv2IssPortIsolationRowStatus
                     (&u4ErrorCode, (INT4) u4IfIndex, InVlanId,
                      (INT4) pu4EgressList[u4Scan], DESTROY)))
                {
                    return CLI_FAILURE;
                }

                if (SNMP_FAILURE ==
                    nmhSetIssPortIsolationRowStatus (u4IfIndex,
                                                     InVlanId,
                                                     (INT4)
                                                     pu4EgressList[u4Scan],
                                                     DESTROY))
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
            }
        }                        /* End of for */
    }
    return CLI_SUCCESS;
}                                /* End of IssPICliDelPortIsolation */

/*****************************************************************************/
/*  Function Name   : IssPICliUtlDelPortIsolationEntry                      */
/*  Description     : This function does the following                       */
/*                    (i) Scan the Port Isolation table.                     */
/*                        a. if the index matches then delete all the egress */
/*                           ports for this vlan id, if specified.           */
/*                        b. Delete all the entries for this ingress index,if*/
/*                           vlan id is CFA_INVALID_INDEX                    */
/*  CLI command(s)  : (i) port-isolation del ([<ifXtype> <iface_list>]       */
/*                    [<ifXtype> <iface_list>])                              */
/*                    (ii)no port-isolation                                  */
/*  Input(s)        : CliHandle - CLI Identifier                             */
/*                    u4IfIndex - Ingress Port                               */
/*                    pu4EgressList - Egress port list                       */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  CLI_FAILURE on failure                    */
/*                                 CLI_SUCCESS on success                    */
/*****************************************************************************/
INT1
IssPICliUtlDelPortIsolationEntry (tCliHandle CliHandle,
                                  UINT4 u4IfIndex,
                                  tVlanId InVlanId, INT4 i4IsAllVlanId)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4IngressIndex = (INT4) u4IfIndex;
    INT4                i4EgressIndex = 0;
    INT4                i4VlanId = (INT4) InVlanId;
    INT4                i4NextIngressIndex = 0;
    INT4                i4NextEgressIndex = 0;
    INT4                i4NextVlanId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    UNUSED_PARAM (CliHandle);

    /* Two types of deletions are handled here. 
     * i). If the i4IsAllVlanid is set to ISS_TRUE, then delete all the entries 
     *     pertaining to this ingress index. 
     * ii). If the i4IsAllVlanId is set to ISS_FALSE, then delete the entries
     *      for this ingress and vlan id match. 
     */
    /* Since the egress list is not known for deletion, 
     * start scanning the table with the search parameters as
     * i4IngressIndex, i4VlanId and 0. 
     * This GetNextIndex call will fetch the next 
     * valid entry in the Port Isolation table. 
     */
    if (SNMP_FAILURE == nmhGetNextIndexIssPortIsolationTable (i4IngressIndex,
                                                              &i4IngressIndex,
                                                              i4VlanId,
                                                              &i4VlanId,
                                                              i4EgressIndex,
                                                              &i4EgressIndex))
    {
        CLI_SET_ERR (CLI_ISS_PI_DEL_FAILED);
        return CLI_FAILURE;
    }

    while (1)
    {
        i1RetVal = nmhGetNextIndexIssPortIsolationTable (i4IngressIndex,
                                                         &i4NextIngressIndex,
                                                         i4VlanId,
                                                         &i4NextVlanId,
                                                         i4EgressIndex,
                                                         &i4NextEgressIndex);
        if ((i4VlanId != InVlanId) && (i4IsAllVlanId == ISS_FALSE))
        {
            /* i4IsAllVlanId is set to ISS_FALSE means only the entries
             * matching this InVlanId has to be deleted. If the GetNext
             * call fetches a VlanId that doesnot match the Input VlanId, 
             * then break. It means scan for this invlan id
             * is complete. 
             */
            break;
        }

        /* Ingress Index matches */
        if (((UINT4) i4IngressIndex == u4IfIndex))
        {

            if (nmhTestv2IssPortIsolationRowStatus (&u4ErrorCode,
                                                    i4IngressIndex,
                                                    i4VlanId,
                                                    i4EgressIndex,
                                                    DESTROY) == SNMP_FAILURE)
            {
                break;
            }

            if (SNMP_FAILURE == nmhSetIssPortIsolationRowStatus (i4IngressIndex,
                                                                 i4VlanId,
                                                                 i4EgressIndex,
                                                                 DESTROY))
            {
                CLI_FATAL_ERROR (CliHandle);
                break;
            }
        }
        else
        {
            /* If the ingress index does not match the input ingress index. 
             * then break the loop. It indicates the scanning is complete for
             * this ingress index.
             */
            break;
        }

        if (SNMP_FAILURE == i1RetVal)
        {
            break;
        }

        i4IngressIndex = i4NextIngressIndex;
        i4VlanId = i4NextVlanId;
        i4EgressIndex = i4NextEgressIndex;
    }                            /* End of while */

    return CLI_SUCCESS;
}                                /* End of IssPICliUtlDelPortIsolationEntry */

/*****************************************************************************/
/*  Function Name   : IssPICliShowPortIsolation                              */
/*  Description     : This function does the following                       */
/*                    (i) Scan the Port Isolation table.                     */
/*                        a. if the index matches the ingress port specified */
/*                           displays the entries for that ingress port alone*/
/*                        b. If the ingress port is not specified (ie.0),    */
/*                           display all the entries in the PI table.        */
/*  CLI command(s)  : show port-isolation [ingress-port <ifXtype> <ifnum>]   */
/*  Input(s)        : CliHandle     - CLI Identifier                         */
/*                    i4IngressPort - Ingress Port                           */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  CLI_FAILURE on failure                    */
/*                                 CLI_SUCCESS on success                    */
/*****************************************************************************/
INT4
IssPICliShowPortIsolation (tCliHandle CliHandle, INT4 i4IngressPort)
{
    UINT4               au4EgressPorts[ISS_MAX_UPLINK_PORTS];
    INT1                ai1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4Scan = 0;
    UINT4               u4Temp = 0;
    INT4                i4IngressIndex = 0;
    INT4                i4NextIngressIndex = 0;
    INT4                i4EgressIndex = 0;
    INT4                i4NextEgressIndex = 0;
    INT4                i4VlanId = 0;
    INT4                i4NextVlanId = 0;
    INT4                i4StorageType = 0;
    INT1                i1MatchFound = ISS_FALSE;
    INT1                i1RetVal = SNMP_FAILURE;
    INT1                i1Print = ISS_TRUE;

    MEMSET (ai1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au4EgressPorts, 0, (sizeof (UINT4) * ISS_MAX_UPLINK_PORTS));

    /* Scan the entire Port Isolation table. 
     * If i4IngressPort is 0 => Display entire table. 
     * if i4IngressPort != 0 => Display entries for this i4IngressPort.
     */

    if (0 == i4IngressPort)
    {
        /* As mentioned earlier, if IngressPort is 0, set the Match flag 
         * to 0.
         */
        i1MatchFound = ISS_TRUE;
    }

    if (nmhGetFirstIndexIssPortIsolationTable (&i4IngressIndex,
                                               &i4VlanId,
                                               &i4EgressIndex) == SNMP_FAILURE)
    {
        /* No entries are available in Port Isolation table. */
        CLI_SET_ERR (CLI_ISS_PI_TABLE_EMPTY);
        return CLI_FAILURE;
    }

    while (1)
    {
        i1RetVal = nmhGetNextIndexIssPortIsolationTable (i4IngressIndex,
                                                         &i4NextIngressIndex,
                                                         i4VlanId,
                                                         &i4NextVlanId,
                                                         i4EgressIndex,
                                                         &i4NextEgressIndex);
        if (i4IngressIndex == i4IngressPort)
        {
            /* Set the match flag to ISS_TRUE if a match is found in the
             * port isolation table. 
             * For display all, match flag is already set in the initial 
             * block itself.
             */
            i1MatchFound = ISS_TRUE;
        }

        if (i1MatchFound == ISS_TRUE)
        {
            /* Set the bitlist if match is found . */
            au4EgressPorts[u4Scan] = (UINT4) i4EgressIndex;
            u4Scan++;
            if (i1Print == ISS_TRUE)
            {
                CliPrintf (CliHandle,
                           "\r\nIngress Port     VlanId     StorageType    Egress List\r\n");
                CliPrintf (CliHandle,
                           "============     ======     ===========    ===========\r\n");
                i1Print = ISS_FALSE;
            }
        }

        /* For a matched entry, if the next ingress index varies,or the vlan id
         * varies then the entire egress list for this (ingress, vlanid) pair
         * has to be displayed.
         */
        if (((i4IngressIndex != i4NextIngressIndex) &&
             (ISS_TRUE == i1MatchFound)) ||
            ((i4VlanId != i4NextVlanId) && (ISS_TRUE == i1MatchFound)))
        {
            CfaCliGetIfName ((UINT4) i4IngressIndex, ai1IfName);
            CliPrintf (CliHandle, "%-18s", ai1IfName);

            if (0 == i4VlanId)
            {
                CliPrintf (CliHandle, "%-10s", "-");
            }
            else
            {
                CliPrintf (CliHandle, "%-10d", i4VlanId);
            }
            nmhGetIssPortIsolationStorageType (i4IngressIndex,
                                               i4VlanId,
                                               i4EgressIndex, &i4StorageType);
            if (ISS_PI_VOLATILE == i4StorageType)
            {
                CliPrintf (CliHandle, "%-4s   ", "Volatile ");
            }
            else
            {
                CliPrintf (CliHandle, "%-4s", "Non-Volatile");
            }

            CliPrintf (CliHandle, "%4s", " ");
            for (u4Temp = 0; u4Temp < (u4Scan - 1); u4Temp++)
            {
                CfaCliGetIfName (au4EgressPorts[u4Temp], ai1IfName);
                CliPrintf (CliHandle, "%s,", ai1IfName);
                if ((u4Temp != 0) && ((u4Temp % 5) == 0))
                {
                    CliPrintf (CliHandle, "\r\n%44s", " ");
                }
            }                    /* End of for */

            CfaCliGetIfName (au4EgressPorts[u4Temp], ai1IfName);
            CliPrintf (CliHandle, "%s", ai1IfName);
            CliPrintf (CliHandle, "\r\n");

            if ((i4IngressPort != 0) && (i4IngressIndex != i4NextIngressIndex))
            {
                /* The PI entries for this ingress is completely scanned */
                break;
            }
            u4Scan = 0;
            MEMSET (au4EgressPorts, 0, ISS_MAX_UPLINK_PORTS);
        }

        if (i1RetVal == SNMP_FAILURE)
        {
            break;
        }
        else
        {

            i4IngressIndex = i4NextIngressIndex;
            i4VlanId = i4NextVlanId;
            i4EgressIndex = i4NextEgressIndex;
            i4NextIngressIndex = i4NextVlanId = 0;
        }
    }                            /* End of while */

    if (i1MatchFound == ISS_FALSE)
    {
        CliPrintf (CliHandle,
                   "%% Entry does not exist in the Port Isolation table\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}                                /* End of IssPICliShowPortIsolation */

/********************************************************************/
/*  Function Name   : IssPICliPIShowRunningConfig                   */
/*  Description     : This function is used to implement            */
/*                    show running config for Port Isolation        */
/*  Input(s)        :                                               */
/*                    CliHandle   - CLI Handle                      */
/*                    u4IfIndex   - Interface Index.                */
/*  Output(s)       : None                                          */
/*  Global variable  referred : None                                */
/*  Global variable  modified : None                                */
/*  Exceptions                : None                                */
/*  Recursions                : None                                */
/*  Returns         : None                                          */
/********************************************************************/
VOID
IssPICliShowRunningConfig (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    tPortList          *pPortList = NULL;
    tSNMP_OCTET_STRING_TYPE IfEgressPorts;
    UINT4               u4PagingStatus = 0;
    INT4                i4IngressIfIndex = (INT4) u4IfIndex;
    INT4                i4NextIngressIfIndex = 0;
    INT4                i4VlanId = 0;
    INT4                i4NextVlanId = 0;
    INT4                i4EgressIfIndex = 0;
    INT4                i4NextEgressIfIndex = 0;
    INT4                i4StorageType = ISS_PI_NON_VOLATILE;
    INT1                i1RetVal = SNMP_FAILURE;
    INT1               *piIfName;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    piIfName = (INT1 *) &au1IfName[0];

    pPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));
    UNUSED_PARAM (i1RetVal);
    if (pPortList == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return;
    }

    MEMSET (*pPortList, 0, sizeof (tPortList));
    IfEgressPorts.pu1_OctetList = *pPortList;
    IfEgressPorts.i4_Length = sizeof (tPortList);

    if (SNMP_FAILURE == nmhGetNextIndexIssPortIsolationTable (i4IngressIfIndex,
                                                              &i4IngressIfIndex,
                                                              i4VlanId,
                                                              &i4VlanId,
                                                              i4EgressIfIndex,
                                                              &i4EgressIfIndex))
    {
        FsUtilReleaseBitList ((UINT1 *) pPortList);
        return;
    }

    while (1)
    {

        i1RetVal = nmhGetNextIndexIssPortIsolationTable (i4IngressIfIndex,
                                                         &i4NextIngressIfIndex,
                                                         i4VlanId,
                                                         &i4NextVlanId,
                                                         i4EgressIfIndex,
                                                         &i4NextEgressIfIndex);

        nmhGetIssPortIsolationStorageType (i4IngressIfIndex,
                                           i4VlanId,
                                           i4EgressIfIndex, &i4StorageType);
        if (ISS_PI_VOLATILE != i4StorageType)
        {

            if (((i4VlanId != i4NextVlanId) &&
                 ((UINT4) i4IngressIfIndex == u4IfIndex)) ||
                (((UINT4) i4NextIngressIfIndex != u4IfIndex)))
            {
                /* next vlan id varies with the current one.
                 * Then its time to print all the egress ports for this
                 * (ingress,vlan) pair. 
                 */
                ISS_SET_MEMBER_PORT ((*pPortList), i4EgressIfIndex);
                CfaCliConfGetIfName ((UINT4) u4IfIndex, piIfName);
                CliPrintf (CliHandle, "interface %s\r\n", piIfName);

                if (0 == i4VlanId)
                {
                    CliPrintf (CliHandle, "port-isolation add");
                    CliConfOctetToIfName (CliHandle,
                                          " ", NULL, &IfEgressPorts,
                                          &u4PagingStatus);
                }
                else
                {
                    CliPrintf (CliHandle, "port-isolation  %d add", i4VlanId);
                    CliConfOctetToIfName (CliHandle,
                                          " ", NULL, &IfEgressPorts,
                                          &u4PagingStatus);
                }
                CliPrintf (CliHandle, "\r\n!\n");
                MEMSET (*pPortList, 0, sizeof (tPortList));
            }
            else
            {
                ISS_SET_MEMBER_PORT ((*pPortList), i4EgressIfIndex);
            }
        }
        i4VlanId = i4NextVlanId;
        i4IngressIfIndex = i4NextIngressIfIndex;
        i4EgressIfIndex = i4NextEgressIfIndex;
        i4NextVlanId = 0;

        if (((INT4) u4IfIndex != i4NextIngressIfIndex))
        {
            /* Entries for u4IfIndex does not exist or 
             * complete scanning of u4IfIndex is already done.
             */
            break;
        }
        i4NextIngressIfIndex = 0;

    }                            /* End of while */

    FsUtilReleaseBitList ((UINT1 *) pPortList);
    return;
}                                /* End of IssPICliShowRunningConfig */

/***************************************************************************/
/*  Function Name   : IssPICliPIShowRunningConfig                          */
/*  Description     : This function is used to convert the pu1str          */
/*                    to interface index formats.                          */
/*                    (i) For Interface list types.                        */
/*                        (a) "0/1-5" is converted to their respective     */
/*                            interface indices and get stored in          */
/*                            pu4PortArray.                                */
/*                    (ii) For port channel types                          */
/*                        (a) "po 1" is converted to the corresponding     */
/*                             interface index.                            */
/*  Input(s)        :                                                      */
/*                    CliHandle   - CLI Handle                             */
/*                    pu1Str      - string containing port list formats    */
/*                    u4PortArrayLen - Port Array length                   */
/*                    u4IfType     - Interface type                        */
/*  Output(s)       : *pu4PortArray - Array of pointers.                   */
/*                    *pu4Count     - Number of interface indices in array */
/*  Global variable  referred : None                                       */
/*  Global variable  modified : None                                       */
/*  Exceptions                : None                                       */
/*  Recursions                : None                                       */
/*  Returns         : None                                                 */
/***************************************************************************/
INT4
IssPIConvertStrToPortArray (tCliHandle CliHandle,
                            UINT1 *pu1Str,
                            UINT4 *pu4PortArray,
                            UINT4 *pu4Count,
                            UINT4 u4PortArrayLen, UINT4 u4IfType)
{
    tCliIfaceList       CliIfaceList;
    tCliPortList        CliPortList;
    INT1               *pi1Temp = (INT1 *) pu1Str;
    INT1               *pi1Pos = NULL;
    UINT4               u4Val1 = 0;
    UINT4               u4Val2 = 0;
    UINT4               u4RetPortNum = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    INT4                i4RetType = 0;
    INT1                ai1TempArr[ISS_MAX_UPLINK_PORTS + 1];
    INT1                i1EndFlag = OSIX_FALSE;
    INT1                i1TempChar = 0;

    MEMSET (&CliIfaceList, 0, sizeof (tCliIfaceList));
    MEMSET (&CliPortList, 0, sizeof (tCliPortList));
    MEMSET (ai1TempArr, 0, ISS_MAX_UPLINK_PORTS + 1);

    if (!(pi1Temp))
    {
        CliPrintf (CliHandle, "\r\n %% Invalid interface port list \r\n");
        i4RetStatus = CLI_FAILURE;
    }

    while ((i4RetStatus != CLI_FAILURE) && (i1EndFlag != OSIX_TRUE))
    {
        pi1Pos = pi1Temp;

        /* Check for port list seperater delimiters */
        pi1Pos = CliGetToken (pi1Pos, CLI_LIST_DELIMIT, CLI_VALIDIFACE_LIST);

        if (!pi1Pos)
        {
            i4RetStatus = CLI_FAILURE;
            CliPrintf (CliHandle, "\r\n %% Invalid interface port list \r\n");
            break;
        }

        if (!(*pi1Pos))
        {
            i1EndFlag = OSIX_TRUE;
        }

        if (CFA_LAGG != u4IfType)
        {
            i1TempChar = *pi1Pos;
        }

        *pi1Pos = '\0';

        if (CFA_LAGG != u4IfType)
        {
            /* Get values between the list seperaters based on the type */
            i4RetType = CliGetValFromIfaceType (&CliIfaceList, pi1Temp,
                                                u4IfType);
            *pi1Pos = i1TempChar;
        }
        else
        {
            /* Get values between the list seperaters based on the type */
            i4RetType = CliGetVal (&CliPortList, pi1Temp);
        }

        switch (i4RetType)
        {
            case CLI_LIST_TYPE_VAL:
                if (CFA_LAGG != u4IfType)
                {
                    u4Val1 = CliIfaceList.uPortList.u4PortNum;
                    u4Val2 = CliIfaceList.uPortList.u4PortNum;
                }
                else
                {
                    u4Val1 = CliPortList.uPortList.u4PortNum;
                    u4Val2 = CliPortList.uPortList.u4PortNum;
                }
                break;

            case CLI_LIST_TYPE_RANGE:
                if (CFA_LAGG != u4IfType)
                {
                    u4Val1 = CliIfaceList.uPortList.PortListRange.u4PortFrom;
                    u4Val2 = CliIfaceList.uPortList.PortListRange.u4PortTo;
                }
                else
                {
                    u4Val1 = CliPortList.uPortList.PortListRange.u4PortFrom;
                    u4Val2 = CliPortList.uPortList.PortListRange.u4PortTo;
                }
                break;

            default:
                i4RetStatus = CLI_FAILURE;
                CliPrintf (CliHandle, "\r\n%% Invalid Port List");
                continue;
        }                        /* End of switch */

        if (pu4PortArray != NULL)
        {
            for (; u4Val1 <= u4Val2; u4Val1++)
            {
                if (u4PortArrayLen > *pu4Count)
                {
                    if (CFA_LAGG == u4IfType)
                    {
                        SPRINTF ((CHR1 *) ai1TempArr, "%u", u4Val1);
                        if (CliGetIfIndexFromTypeAndPortNum (u4IfType,
                                                             ai1TempArr,
                                                             &u4RetPortNum) ==
                            CLI_SUCCESS)
                        {
                            pu4PortArray[*pu4Count] = u4RetPortNum;
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "%% Interface does not exist\r\n");
                            return CLI_FAILURE;
                        }
                    }
                    else
                    {
                        pu4PortArray[*pu4Count] = u4Val1;
                    }
                    (*pu4Count)++;
                }
                else
                {
                    CliPrintf (CliHandle,
                               "%% Only %d ports can be configured as Uplink ports\r\n",
                               ISS_MAX_UPLINK_PORTS);
                    return CLI_FAILURE;
                }
            }                    /* End of for */
        }
        pi1Temp = pi1Pos + 1;
    }

    return i4RetStatus;
}

#endif /* ISSPICLI_C */
