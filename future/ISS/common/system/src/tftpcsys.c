/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* $Id: tftpcsys.c,v 1.39 2015/10/28 06:13:54 siva Exp $                   */
/* Licensee Aricent Inc., 2001-2002                                          */
/*****************************************************************************/
/*    FILE  NAME            : tftpcsys.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : TFTP client system file                        */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*    DESCRIPTION           : This file contains functions for tftp client   */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
#ifndef __TFTPCSYS_C__
#define __TFTPCSYS_C__
#include "lr.h"
#include "iss.h"
#include "fssocket.h"
#include "tftpcsys.h"
#ifdef IP6_WANTED
#include "ip6util.h"
#endif

const INT1         *gaTftpcErrMsg[] = TFTPC_ERR_MSGS;
UINT1               gMsrPendBuf[TFTPC_BLOCKSIZE_MAX];
UINT1               gau1OutPktBuf[TFTPC_MAX_BUFFER];
UINT1               gau1InPktBuf[TFTPC_MAX_BUFFER];
UINT4               gMsrPendBufLength = 0;
extern UINT1        gu1FileCopyingStatus;
/****************************************************************************/
/*                                                                          */
/*    Function Name      : tftpcExtractOpt                                  */
/*                                                                          */
/*    Description        : Extracts options from TFTP packet                */
/*                                                                          */
/*    Input(s)           : Tftp session pointer                             */
/*                                                                          */
/*    Output(s)          : Updates option in the session                    */
/*                                                                          */
/*    Returns            : NONE                                             */
/****************************************************************************/
VOID
tftpcExtractOpt (tTftpcSession * tftpS)
{
    UINT1              *pu1Option;
    UINT1              *pu1Value;
    UINT2               i4Idx = TFTPC_BLK_NO_OFFSET;
    pu1Option = &tftpS->pu1InPktBuf[i4Idx];
    /* Reset the Options */
    tftpS->u2BlkSize = TFTPC_BLOCKSIZE_STD;
    tftpS->u4FileSize = TFTPC_ZERO;
    /* Reset the Blk# */
    tftpS->u2BlkNo = TFTPC_ZERO;
    while (tftpS->i4InBufLen > i4Idx)
    {
        while (tftpS->pu1InPktBuf[i4Idx++] != TFTPC_EOS);
        pu1Value = &tftpS->pu1InPktBuf[i4Idx];
        if (TFTPC_STRCMP (pu1Option, TFTPC_BLOCKSIZE_OPT) == TFTPC_ZERO)
        {
            tftpS->u2BlkSize = (UINT2) TFTPC_ATOI (pu1Value);
            TFTPC_TRC_MED_ARG1 ("Negotiated Block Size = %d\n",
                                tftpS->u2BlkSize);
        }
        else if (TFTPC_STRCMP (pu1Option, TFTPC_TIMEOUT_OPT) == TFTPC_ZERO)
        {
            tftpS->u2TimeOut = (UINT2) TFTPC_ATOI (pu1Value);
            TFTPC_TRC_MED_ARG1 ("Negotiated TimeOut = %d\n", tftpS->u2TimeOut);
        }
        else if (TFTPC_STRCMP (pu1Option, TFTPC_TSIZE_OPT) == TFTPC_ZERO)
        {
            tftpS->u4FileSize = (UINT4) TFTPC_ATOI (pu1Value);
            TFTPC_TRC_MED_ARG1 ("File Size = %d\n", tftpS->u4FileSize);
        }
        else
        {
            TFTPC_TRC_CRI ("UnKnown Option\n");
        }
        while (tftpS->pu1InPktBuf[i4Idx++] != TFTPC_EOS);
        pu1Option = &tftpS->pu1InPktBuf[i4Idx];
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : tftpcProcessResponse                             */
/*                                                                          */
/*    Description        : Process TFTP packet recd.                        */
/*                                                                          */
/*    Input(s)           : Tftp session pointer                             */
/*                                                                          */
/*    Output(s)          : Updates  the session                             */
/*                                                                          */
/*    Returns            : TFTPC_OK / TFTPC_E_XX on error                   */
/****************************************************************************/
INT4
tftpcProcessResponse (tTftpcSession * tftpS)
{
    UINT2               u2BlkNo;
    UINT2               u2Error;
    tTftpcPkt          *pTftpPkt;

    pTftpPkt = (tTftpcPkt *) (VOID *) tftpS->pu1InPktBuf;
    switch (TFTPC_NTOHS (pTftpPkt->u2OpCode))
    {
        case TFTPC_DATA_PKT:
            TFTPC_TRC_LOW (" RX:TFTP Data Packet \n");
            if (tftpS->u2ReqType == TFTPC_RRQ_PKT)
            {
                TFTPC_TRC_MED (("The Server Does not support options !!!\n"));
                /* Reset Options */
                tftpS->u2BlkSize = TFTPC_BLOCKSIZE_STD;
                tftpS->u2ReqType = TFTPC_ZERO;
            }
            u2BlkNo = TFTPC_NTOHS (pTftpPkt->u2BlkAckErr);
            if (tftpS->u2BlkNo == TFTPC_MAX_BLOCKS)
            {
                if (u2BlkNo != 0)
                {
                    TFTPC_TRC_CRI_ARG2 ("1Duplicate Pkt Received - Blk No: %d tftpS->u2BlkNo - %d\n", 
                                         u2BlkNo, tftpS->u2BlkNo);
                    tftpS->pu1Data = NULL;
                    tftpS->i4DataLen = (-1);
                }
                else
                {
                    TFTPC_TRC_LOW_ARG2 ("1Block Number from pkt received is Blk No: %d tftpS Blk no - %d\n",
                                    u2BlkNo, tftpS->u2BlkNo);
                    tftpS->pu1Data = pTftpPkt->au1Data;
                    tftpS->i4DataLen = tftpS->i4InBufLen - TFTPC_DATA_OFFSET;
                    tftpS->u2BlkNo = u2BlkNo;
                }
            }
            else if ((tftpS->u2BlkNo + 1) != u2BlkNo)
            { 
                TFTPC_TRC_CRI_ARG2 ("2Duplicate Pkt Received - Blk No: %d tftpS->u2BlkNo - %d\n", 
                                     u2BlkNo, tftpS->u2BlkNo);
                tftpS->pu1Data = NULL;
                tftpS->i4DataLen = (-1);
            }
            else
            {
                TFTPC_TRC_LOW_ARG2 ("2Block Number from pkt received is Blk No: %d tftpS Blk no - %d\n",
                                    u2BlkNo, tftpS->u2BlkNo);
                tftpS->pu1Data = pTftpPkt->au1Data;
                tftpS->i4DataLen = tftpS->i4InBufLen - TFTPC_DATA_OFFSET;
                tftpS->u2BlkNo = u2BlkNo;
            }
            break;
        case TFTPC_ACK_PKT:
            TFTPC_TRC_LOW_ARG1 ("TFTP ACK Packet BlockId: %d\n", TFTPC_NTOHS (pTftpPkt->u2BlkAckErr));
            if (tftpS->u2ReqType == TFTPC_WRQ_PKT)
            {
                TFTPC_TRC_MED ("The Server Does not support options !!!\n");
                /* Reset Options */
                tftpS->u2BlkSize = TFTPC_BLOCKSIZE_STD;
                tftpS->u2ReqType = TFTPC_ZERO;
            }
            u2BlkNo = TFTPC_NTOHS (pTftpPkt->u2BlkAckErr);

            if (u2BlkNo == TFTPC_MAX_BLOCKS)
            {
               tftpS->u2BlkNo++;
            }
            else
            {
            tftpS->u2BlkNo = (UINT2) (u2BlkNo + 1); 
            }
            /* Send Next Pkt */
            break;
        case TFTPC_ERROR_PKT:
            u2Error = TFTPC_NTOHS (pTftpPkt->u2BlkAckErr);
            TFTPC_TRC_CRI ("TFTP ERROR Packet \n");
            TFTPC_TRC_CRI_ARG1 ("ERROR:<%s>:\n", TFTPC_ERROR_INFO (u2Error));
            TFTPC_TRC_CRI_ARG1 ("INFO:<%s>\n\n", pTftpPkt->au1Data);
            return (TFTPC_ERROR_CODE (u2Error));
        case TFTPC_OAK_PKT:
            TFTPC_TRC_LOW ("TFTP OACK Packet \n");
            /* We Send All options, check the respose */
            tftpcExtractOpt (tftpS);
            if (tftpS->u2ReqType == TFTPC_WRQ_PKT)
            {
                tftpS->u2BlkNo = 1;
            }
            tftpS->u2ReqType = TFTPC_ZERO;
            break;
        default:
            break;
    }
    return TFTPC_OK;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : tftpcSendPkt                                     */
/*                                                                          */
/*    Description        : Send the TFTP packet on the session socket       */
/*                                                                          */
/*    Input(s)           : Tftp session pointer                             */
/*                                                                          */
/*    Output(s)          : Transmits the packet                             */
/*                                                                          */
/*    Returns            : TFTPC_OK / TFTPC_E_HOST                          */
/****************************************************************************/
INT4
tftpcSendPkt (tTftpcSession * tftpS)
{
    INT4                i4Ret;

    TFTPC_TRC_LOW_ARG1 ("Sending Pkt ...Block: %d\n", tftpS->u2BlkNo);

    if (tftpS->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        if ((i4Ret =
             sendto (tftpS->i4SockFd, tftpS->pu1OutPktBuf, tftpS->i4OutBufLen,
                     TFTPC_ZERO,
                     (struct sockaddr *) &tftpS->srv_ip_addr_u.srvAddr,
                     sizeof (struct sockaddr))) < TFTPC_ZERO)
        {
            TFTPC_ASSERT_ER (i4Ret);
            TFTPC_TRC_CRI ("Sendto ERROR\n");
            return (TFTPC_E_HOST);
        }
    }
#ifdef IP6_WANTED
    else if (tftpS->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        if ((i4Ret =
             sendto (tftpS->i4SockFd, tftpS->pu1OutPktBuf, tftpS->i4OutBufLen,
                     TFTPC_ZERO,
                     (struct sockaddr *) &tftpS->srv_ip_addr_u.srvAddr6,
                     sizeof (struct sockaddr_in6))) < TFTPC_ZERO)
        {
            TFTPC_ASSERT_ER (i4Ret);
            TFTPC_TRC_CRI ("Sendto ERROR\n");
            return (TFTPC_E_HOST);
        }
    }
#endif
    return TFTPC_OK;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : tftpcTerminate                                   */
/*                                                                          */
/*    Description        : Terminate TFTP session                           */
/*                                                                          */
/*    Input(s)           : Tftp session pointer                             */
/*                                                                          */
/*    Output(s)          : Terminate session                                */
/*                                                                          */
/*    Returns            : NONE                                             */
/****************************************************************************/
VOID
tftpcTerminate (tTftpcSession * tftpS)
{
    TFTPC_TRC_LOW (" Terminate Session ...\n\n");
    close (tftpS->i4SockFd);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : tftpcGetPkt                                      */
/*                                                                          */
/*    Description        : Recv pkt from tftp session connection            */
/*                                                                          */
/*    Input(s)           : Tftp session pointer                             */
/*                                                                          */
/*    Output(s)          : Copy rx packet to session                        */
/*                                                                          */
/*    Returns            : TFTPC_OK/TFTPC_E_HOST                            */
/****************************************************************************/
INT4
tftpcGetPkt (tTftpcSession * tftpS)
{
    struct sockaddr_in  ServAddr;
    struct timeval      tv;
    fd_set              ReadSockList;
    tTftpcPkt          *pTftpPkt;
    UINT4               u4AddrLen = sizeof (ServAddr);
#ifdef IP6_WANTED
    struct sockaddr_in6 ServAddr6;
    UINT4               u4Addr6Len = sizeof (ServAddr6);
#endif
    INT4                i4RetVal = 0;
    UINT2               u2Try;
    UINT2               u2BlkNo;
    UINT1               u1Recvd = FALSE;

    MEMSET (&ReadSockList, 0, sizeof (fd_set));

    /* MAX Retries */
    u2Try = tftpS->u2RetryCnt;
    u4AddrLen = sizeof (ServAddr);
    do
    {
        tv.tv_sec = tftpS->u2TimeOut;
        tv.tv_usec = TFTPC_ZERO;
        TFTPC_FD_ZERO (&ReadSockList);
        TFTPC_FD_SET (tftpS->i4SockFd, &ReadSockList);
        TFTPC_TRC_LOW ("Waiting for Pkt ...");
        i4RetVal = select (tftpS->i4SockFd + 1, &ReadSockList, NULL, NULL, &tv);
        if (i4RetVal > 0)
        {
            TFTPC_TRC_LOW (" Rcvd...\n");

            if (tftpS->u1AddrType == IPVX_ADDR_FMLY_IPV4)
            {
                tftpS->i4InBufLen = recvfrom ((INT4) tftpS->i4SockFd,
                                              (VOID *) tftpS->pu1InPktBuf,
                                              (INT4) TFTPC_MAX_BUFFER,
                                              (UINT4) TFTPC_ZERO,
                                              (struct sockaddr *)
                                              &ServAddr, (INT4 *) &u4AddrLen);
                if (tftpS->i4InBufLen > TFTPC_ZERO)
                {
                    if ((tftpS->srv_ip_addr_u.srvAddr.sin_port !=
                         TFTPC_HTONS (TFTPC_SRV_PORT)) &&
                        (tftpS->srv_ip_addr_u.srvAddr.sin_port !=
                         ServAddr.sin_port))
                    {
                        tftpcSendErrPkt (tftpS, TFTPC_ERR_UNKNOWN_TID);
                        continue;
                    }
                    tftpS->srv_ip_addr_u.srvAddr.sin_port = ServAddr.sin_port;
                    u1Recvd = TRUE;
                }
                else if (tftpS->i4InBufLen == TFTPC_ZERO)
                {
                    return (TFTPC_E_HOST);
                }
            }
#ifdef IP6_WANTED
            else if (tftpS->u1AddrType == IPVX_ADDR_FMLY_IPV6)
            {
                if ((tftpS->i4InBufLen = recvfrom ((INT4) tftpS->i4SockFd,
                                                   (VOID *) tftpS->pu1InPktBuf,
                                                   (INT4) TFTPC_MAX_BUFFER,
                                                   (UINT4) TFTPC_ZERO,
                                                   (struct sockaddr *)
                                                   &ServAddr6,
                                                   (INT4 *) &u4Addr6Len)) >
                    TFTPC_ZERO)
                {
                    tftpS->srv_ip_addr_u.srvAddr6.sin6_port =
                        ServAddr6.sin6_port;
                    u1Recvd = TRUE;
                }
            }
#endif
            if (TRUE == u1Recvd)
            {
                pTftpPkt = (tTftpcPkt *) (VOID *) tftpS->pu1InPktBuf;
                u2BlkNo = TFTPC_NTOHS (pTftpPkt->u2BlkAckErr);
                if (tftpS->u2TransferType != TFTPC_WRITE)
                {
                    return TFTPC_OK;
                }
                else
                {
                    /* If optional ACK is received,
                       then proceed without further checks */
                    if (TFTPC_NTOHS (pTftpPkt->u2OpCode) == TFTPC_OAK_PKT)
                    {
                        return TFTPC_OK;
                    }
                    else if (u2BlkNo == TFTPC_MAX_BLOCKS)
                    {
                        /* wrap around case after max block is reached 
                         */
                        TFTPC_TRC_LOW ("Hitting wrap around case \n");
                        return TFTPC_OK;
                    }
                    /* Check if the the block number in the received ACK is
                       same as the block number last transmitted. If this is not
                       the case, then the received ACk must be a duplicate ACK */
                    else if (tftpS->u2BlkNo == u2BlkNo)
                    {
                        return TFTPC_OK;
                    }
                    else
                    {
                        TFTPC_TRC_LOW_ARG2 ("Duplicate ACK received. Drop it.. Client Blk %d Server Blk %d\n",
                                       tftpS->u2BlkNo, u2BlkNo);
                        continue;
                    }
                }
            }
        }
        else if (i4RetVal < 0)
        {
            return (TFTPC_E_HOST);
        }
        TFTPC_TRC_LOW_ARG1 ("TimeOut ReTry (%d)...\n",
                            TFTPC_MAX_RETRY - u2Try + 1);
        /* Retransmit Last packet */
        tftpcSendPkt (tftpS);
    }
    while (--u2Try > TFTPC_ZERO);
    return (TFTPC_E_HOST);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : tftpcConfig                                      */
/*                                                                          */
/*    Description        : Configures the tftp session                      */
/*                                                                          */
/*    Input(s)           : Tftp session pointer                             */
/*                                                                          */
/*    Output(s)          : Configures the tftp session                      */
/*                                                                          */
/*    Returns            : TFTPC_OK/TFTPC_E_XX                              */
/****************************************************************************/
INT4
tftpcConfig (tTftpcSession * tftpS, UINT1 u1Action, UINT2 u2TimeOut,
             UINT2 u2RetryCnt)
{
#if (defined IP6_WANTED && !defined LNXIP6_WANTED)
    INT1                i1Optval = 1;
#endif

#if defined IP_WANTED
    UINT1               u1Optval = 1;
#endif
    /* Open socket */
    if (tftpS->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        tftpS->i4SockFd = socket (AF_INET, SOCK_DGRAM, TFTPC_ZERO);
    }
#ifdef IP6_WANTED
    else if (tftpS->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        tftpS->i4SockFd = socket (AF_INET6, SOCK_DGRAM, TFTPC_ZERO);
    }
#endif
    else
    {
        TFTPC_TRC_CRI ("Wrong Address Type\n");
        return (TFTPC_E_GEN);
    }

    if (tftpS->i4SockFd < TFTPC_ZERO)
    {
        TFTPC_TRC_CRI ("Unable to create socket\n");
        return (TFTPC_E_GEN);
    }

    if (tftpS->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        /* Bind to socket */
        tftpS->client_ip_addr_u.cliAddr.sin_family = AF_INET;
        tftpS->client_ip_addr_u.cliAddr.sin_addr.s_addr = INADDR_ANY;
        tftpS->client_ip_addr_u.cliAddr.sin_port = TFTPC_ZERO;
        if (bind (tftpS->i4SockFd,
                  (struct sockaddr *) &tftpS->client_ip_addr_u.cliAddr,
                  sizeof (struct sockaddr_in)) < TFTPC_ZERO)
        {
            TFTPC_TRC_CRI ("Bind Failed\n");
            close (tftpS->i4SockFd);
            return (TFTPC_E_GEN);
        }

#ifdef IP_WANTED
        if (setsockopt
            (tftpS->i4SockFd, IPPROTO_UDP, IP_PKTINFO,
             &u1Optval, sizeof (UINT1)) != OSIX_SUCCESS)
        {
            TFTPC_TRC_CRI ("Setsocket option Failed\n");
            close (tftpS->i4SockFd);
            return (TFTPC_E_GEN);
        }
#endif
    }
#ifdef IP6_WANTED
    else if (tftpS->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        tftpS->client_ip_addr_u.cliAddr6.sin6_family = AF_INET6;
        tftpS->client_ip_addr_u.cliAddr6.sin6_port = TFTPC_ZERO;
        inet_pton (AF_INET6, (const CHR1 *) "TFTPC_ZERO::TFTPC_ZERO",
                   &tftpS->client_ip_addr_u.cliAddr6.sin6_addr.s6_addr);
        if (bind
            (tftpS->i4SockFd,
             (struct sockaddr *) &tftpS->client_ip_addr_u.cliAddr6,
             sizeof (struct sockaddr_in6)) < TFTPC_ZERO)
        {
            TFTPC_TRC_CRI ("Bind Failed\n");
            close (tftpS->i4SockFd);
            return (TFTPC_E_GEN);
        }

#ifndef LNXIP6_WANTED
        if (setsockopt
            (tftpS->i4SockFd, IPPROTO_UDP, IPV6_PKTINFO,
             (INT2 *) (VOID *) &i1Optval, sizeof (UINT1)) != OSIX_SUCCESS)
        {
            TFTPC_TRC_CRI ("Setsocket option Failed\n");
            close (tftpS->i4SockFd);
            return (TFTPC_E_GEN);
        }
#endif
    }
#endif

    /* Get the file info from server */
    /* Create TFTPC Read Request Pkt */
    tftpS->pu1OutPktBuf = &gau1OutPktBuf[0];
    /* Create Response Buf */
    tftpS->pu1InPktBuf = &gau1InPktBuf[0];
    if (u2TimeOut > TFTPC_ZERO)
    {
        tftpS->u2TimeOut = u2TimeOut;
    }
    else
    {
        tftpS->u2TimeOut = (UINT2) ATOI (TFTPC_TIMEOUT);
    }
    if (u2RetryCnt > TFTPC_ZERO)
    {
        tftpS->u2RetryCnt = u2RetryCnt;
    }
    else
    {
        tftpS->u2RetryCnt = (UINT2) TFTPC_MAX_RETRY;
    }
    /* Set Read/Write Request op code */
    if (u1Action == TFTPC_READ)
    {
        tftpS->u2ReqType = TFTPC_RRQ_PKT;
        tftpS->u2TransferType = TFTPC_READ;
    }
    else
    {
        tftpS->u2ReqType = TFTPC_WRQ_PKT;
        tftpS->u2TransferType = TFTPC_WRITE;
    }
    return TFTPC_OK;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : tftpcConnect                                     */
/*                                                                          */
/*    Description        : Sends TFTP get/put request pkt                   */
/*                                                                          */
/*    Input(s)           : Tftp session pointer                             */
/*                                                                          */
/*    Output(s)          : Sends TFTP get/put request pkt                   */
/*                                                                          */
/*    Returns            : TFTPC_OK/TFTPC_E_XX                              */
/****************************************************************************/
INT4
tftpcConnect (tTftpcSession * tftpS, const UINT1 *pu1File,
              UINT4 u4FileSize, tIPvXAddr SrvIp)
{
    INT4                i4RetVal;
    UINT4               u4SrvIp = 0;
    tTftpcPkt          *pTftpPkt;
#ifdef IP6_WANTED
    UINT1              *pu1Ip6Addr = NULL;
    UINT1               au1TempArr[IPVX_MAX_INET_ADDR_LEN];
    MEMSET (au1TempArr, 0, IPVX_MAX_INET_ADDR_LEN);
    pu1Ip6Addr = au1TempArr;
#endif

    pTftpPkt = (tTftpcPkt *) (VOID *) tftpS->pu1OutPktBuf;

    /* Set Read/Write Request op code */
    if (tftpS->u2ReqType == TFTPC_READ)
    {
        pTftpPkt->u2OpCode = TFTPC_HTONS (TFTPC_RRQ_PKT);
        tftpS->u2ReqType = TFTPC_RRQ_PKT;
    }
    else
    {
        pTftpPkt->u2OpCode = TFTPC_HTONS (TFTPC_WRQ_PKT);
        tftpS->u2ReqType = TFTPC_WRQ_PKT;
    }
    tftpS->i4OutBufLen = TFTPC_OP_SIZE;

    /* Set File Name */
    TFTPC_STRCPY (&tftpS->pu1OutPktBuf[tftpS->i4OutBufLen], pu1File);
    tftpS->i4OutBufLen += (INT4) (TFTPC_STRLEN (pu1File) + 1);
    /* Set Xfer Mode */
    TFTPC_STRCPY (&tftpS->pu1OutPktBuf[tftpS->i4OutBufLen], TFTPC_OCTET_MODE);
    tftpS->i4OutBufLen += (INT4) TFTPC_STRLEN (TFTPC_OCTET_MODE) + 1;
    /* Set BlockSize Option */
    TFTPC_STRCPY (&tftpS->pu1OutPktBuf[tftpS->i4OutBufLen],
                  TFTPC_BLOCKSIZE_OPT);
    tftpS->i4OutBufLen += (INT4) TFTPC_STRLEN (TFTPC_BLOCKSIZE_OPT) + 1;
    TFTPC_STRCPY (&tftpS->pu1OutPktBuf[tftpS->i4OutBufLen], TFTPC_BLOCKSIZE);
    tftpS->i4OutBufLen += (INT4) TFTPC_STRLEN (TFTPC_BLOCKSIZE) + 1;
    /* Set TimeOut Option */
    TFTPC_STRCPY (&tftpS->pu1OutPktBuf[tftpS->i4OutBufLen], TFTPC_TIMEOUT_OPT);
    tftpS->i4OutBufLen += (INT4) TFTPC_STRLEN (TFTPC_TIMEOUT_OPT) + 1;
    TFTPC_STRCPY (&tftpS->pu1OutPktBuf[tftpS->i4OutBufLen], TFTPC_TIMEOUT);
    tftpS->i4OutBufLen += (INT4) TFTPC_STRLEN (TFTPC_TIMEOUT) + 1;
    /* Set File Size Option */
    if (tftpS->u2ReqType == TFTPC_READ || u4FileSize > TFTPC_ZERO)
    {
        TFTPC_STRCPY (&tftpS->pu1OutPktBuf[tftpS->i4OutBufLen],
                      TFTPC_TSIZE_OPT);
        tftpS->i4OutBufLen += (INT4) TFTPC_STRLEN (TFTPC_TSIZE_OPT) + 1;
        if (tftpS->u2ReqType == TFTPC_READ)
        {
            tftpS->pu1OutPktBuf[tftpS->i4OutBufLen++] = '0';
            tftpS->pu1OutPktBuf[tftpS->i4OutBufLen++] = TFTPC_EOS;
        }
        else
        {
            SPRINTF ((CHR1 *) & tftpS->pu1OutPktBuf[tftpS->i4OutBufLen],
                     "%u", u4FileSize);
            tftpS->i4OutBufLen +=
                (INT4) (TFTPC_STRLEN (&tftpS->pu1OutPktBuf[tftpS->i4OutBufLen])
                        + 1);
        }
    }

    if (tftpS->u1AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        /* Connect to server */
        PTR_FETCH4 (u4SrvIp, SrvIp.au1Addr);
        tftpS->srv_ip_addr_u.srvAddr.sin_addr.s_addr = TFTPC_HTONL (u4SrvIp);
        tftpS->srv_ip_addr_u.srvAddr.sin_family = AF_INET;
        tftpS->srv_ip_addr_u.srvAddr.sin_port = TFTPC_HTONS (TFTPC_SRV_PORT);
    }
#ifdef IP6_WANTED
    else if (tftpS->u1AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        /* Connect to server */
        tftpS->srv_ip_addr_u.srvAddr6.sin6_family = AF_INET6;
        tftpS->srv_ip_addr_u.srvAddr6.sin6_port = TFTPC_HTONS (TFTPC_SRV_PORT);
        pu1Ip6Addr = Ip6PrintNtop ((tIp6Addr *) (VOID *) SrvIp.au1Addr);
        inet_pton (AF_INET6, (const CHR1 *) pu1Ip6Addr,
                   &tftpS->srv_ip_addr_u.srvAddr6.sin6_addr.s6_addr);
    }
#endif

    if ((i4RetVal = tftpcSendPkt (tftpS)) != TFTPC_OK)
    {
        close (tftpS->i4SockFd);
        return (i4RetVal);
    }
    if ((i4RetVal = tftpcGetPkt (tftpS)) != TFTPC_OK)
    {
        TFTPC_TRC_CRI ("TFTP: Failed to Recv Response \n");
        close (tftpS->i4SockFd);
        return (i4RetVal);
    }
    if ((i4RetVal = tftpcProcessResponse (tftpS)) != TFTPC_OK)
    {
        close (tftpS->i4SockFd);
        return (i4RetVal);
    }
    return TFTPC_OK;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : tftpcSendAck                                     */
/*                                                                          */
/*    Description        : Sends TFTP ACK packet                            */
/*                                                                          */
/*    Input(s)           : Tftp session pointer                             */
/*                                                                          */
/*    Output(s)          : Sends TFTP ACK packet                            */
/*                                                                          */
/*    Returns            : TFTPC_OK/TFTPC_E_XX                              */
/****************************************************************************/
INT4
tftpcSendAck (tTftpcSession * tftpS)
{
    INT4                i4RetVal;
    tTftpcPkt          *pTftpPkt = (tTftpcPkt *) (VOID *) tftpS->pu1OutPktBuf;

    pTftpPkt->u2OpCode = TFTPC_HTONS (TFTPC_ACK_PKT);
    tftpS->i4OutBufLen = TFTPC_OP_SIZE;
    pTftpPkt->u2BlkAckErr = TFTPC_HTONS (tftpS->u2BlkNo);
    tftpS->i4OutBufLen += TFTPC_OP_SIZE;
    TFTPC_TRC_MED_ARG1 ("Sending ACK <%u>\n", tftpS->u2BlkNo);
    if ((i4RetVal = tftpcSendPkt (tftpS)) != TFTPC_OK)
    {
        return (i4RetVal);
    }
    return TFTPC_OK;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : tftpcSendErrPkt                                  */
/*                                                                          */
/*    Description        : This function is used to send error packet to    */
/*                        indicate disruption in the client side. Based on  */
/*                        this indication server can take its recovery      */
/*                        mechanism                                         */
/*                                                                          */
/*    Input(s)           : Tftp session pointer                             */
/*                         u1ErrorCode - Error code                         */
/*                                                                          */
/*    Output(s)          : Sends TFTP ERROR packet                          */
/*                                                                          */
/*    Returns            : TFTPC_OK/TFTPC_ERR_XX                            */
/****************************************************************************/

INT4
tftpcSendErrPkt (tTftpcSession * tftpS, UINT1 u1ErrorCode)
{
    INT4                i4RetVal;
    tTftpcPkt          *pTftpPkt = (tTftpcPkt *) (VOID *) tftpS->pu1OutPktBuf;
    UINT2               u2ErrorCode;

    u2ErrorCode = (UINT2) u1ErrorCode;
    pTftpPkt->u2OpCode = TFTPC_HTONS (TFTPC_ERROR_PKT);
    tftpS->i4OutBufLen = TFTPC_OP_SIZE;
    pTftpPkt->u2BlkAckErr = TFTPC_HTONS (u2ErrorCode);
    tftpS->i4OutBufLen += TFTPC_OP_SIZE;
    TFTPC_STRCPY (pTftpPkt->au1Data, "Session terminated");
    tftpS->i4OutBufLen += (INT4) (TFTPC_STRLEN (pTftpPkt->au1Data) + 1);

    TFTPC_TRC_MED_ARG1 ("Sending Error Pkt <%u>\n", pTftpPkt->u2BlkAckErr);
    if ((i4RetVal = tftpcSendPkt (tftpS)) != TFTPC_OK)
    {
        return (i4RetVal);
    }
    return TFTPC_OK;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : tftpcRecvFileToBuf                               */
/*                                                                          */
/*    Description        : Recv file to the supplied buffer                 */
/*                                                                          */
/*    Input(s)           : server ip,file,buf,bufsize                       */
/*                                                                          */
/*    Output(s)          : Size of the data recd                            */
/*                                                                          */
/*    Returns            : TFTPC_OK/TFTPC_E_XX                              */
/****************************************************************************/

INT4
tftpcRecvFileToBuf (tIPvXAddr SrvIp, const UINT1 *pu1File,
                    UINT1 *pu1Buf, INT4 i4BufSize, UINT4 *pu4Data)
{
    tTftpcSession       tftpS;
    INT4                i4Data = TFTPC_ZERO, i4RetVal;

    TFTPC_MEMSET (&tftpS, TFTPC_EOS, sizeof (tTftpcSession));
    tftpS.u1AddrType = SrvIp.u1Afi;

    if ((i4RetVal =
         tftpcConfig (&tftpS, TFTPC_READ, (UINT2) ATOI (TFTPC_TIMEOUT),
                      (UINT2) TFTPC_MAX_RETRY)) != TFTPC_OK)
    {
        return (i4RetVal);
    }
    if ((i4RetVal =
         tftpcConnect (&tftpS, pu1File, TFTPC_ZERO, SrvIp)) != TFTPC_OK)
    {
        return i4RetVal;
    }
    if (tftpS.u4FileSize > TFTPC_ZERO && tftpS.u4FileSize > (UINT4) i4BufSize)
    {
        TFTPC_TRC_CRI ("ERROR: Insufficent buffer\n");
        tftpcTerminate (&tftpS);
        return TFTPC_E_BUF;
    }
    if (tftpS.u2BlkNo == 1)
    {
        /* Server does not support options, first data packet
         * might be recd already, copy the buffer and send ack
         */
        if (i4BufSize < tftpS.i4DataLen)
        {
            tftpcTerminate (&tftpS);
            return TFTPC_E_BUF;
        }

        TFTPC_MEMCPY (pu1Buf, tftpS.pu1Data, tftpS.i4DataLen);
        i4Data = tftpS.i4DataLen;
        if (tftpS.i4DataLen != tftpS.u2BlkSize)
        {
            TFTPC_TRC_MED_ARG1 ("Received Last Packet Total <%d> Blocks\n",
                                tftpS.u2BlkNo);
            tftpcSendAck (&tftpS);
            tftpcTerminate (&tftpS);
            *pu4Data = (UINT4) i4Data;
            return TFTPC_OK;
        }
    }
    while (1)
    {
        if ((i4RetVal = tftpcSendAck (&tftpS)) != TFTPC_OK)
            break;
        if ((i4RetVal = tftpcGetPkt (&tftpS)) != TFTPC_OK)
            break;
        if ((i4RetVal = tftpcProcessResponse (&tftpS)) != TFTPC_OK)
            break;
        if (tftpS.i4DataLen < TFTPC_ZERO)
        {
            /* Duplicate packet */
            continue;
        }
        if ((i4Data + tftpS.i4DataLen) > i4BufSize)
        {
            TFTPC_TRC_CRI ("InSufficent Buffer to get file\n");
            i4RetVal = TFTPC_E_BUF;
            break;
        }
        if (tftpS.i4DataLen > TFTPC_ZERO)
        {
            TFTPC_MEMCPY (&pu1Buf[i4Data], tftpS.pu1Data, tftpS.i4DataLen);
            i4Data += tftpS.i4DataLen;
        }
        if (tftpS.i4DataLen != tftpS.u2BlkSize)
        {
            TFTPC_TRC_MED_ARG1 ("Received Last Packet Total <%d> Blocks\n",
                                tftpS.u2BlkNo);
            tftpcSendAck (&tftpS);
            tftpcTerminate (&tftpS);
            *pu4Data = (UINT4) i4Data;
            return TFTPC_OK;
        }
    }
    return (i4RetVal);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : tftpcRecvFile                                    */
/*                                                                          */
/*    Description        : Recv file using tftp client                      */
/*                                                                          */
/*    Input(s)           : server ip/file local file                        */
/*                                                                          */
/*    Output(s)          : gets the file from server                        */
/*                                                                          */
/*    Returns            : TFTPC_OK/TFTPC_E_XX                              */
/****************************************************************************/
INT4
tftpcRecvFile (tIPvXAddr SrvIp, const UINT1 *pu1RemoteFile,
               const UINT1 *pu1LocalFile)
{
    tTftpcSession       tftpS;
    INT4                i4RetVal, i4Option = TFTPC_ZERO;
    UINT4               u4Data = 0;
    INT4                i4Flag = 1;
    FILE               *pFile;
    FS_UINT8            u8TempData, u8TempData1, u8Cent, u8FileSize, u8Option,
        u8Rem;

    FSAP_U8_CLR (&u8TempData);
    FSAP_U8_CLR (&u8Cent);
    FSAP_U8_CLR (&u8FileSize);
    FSAP_U8_CLR (&u8Option);
    FSAP_U8_CLR (&u8Rem);
    gu1FileCopyingStatus = TFTP_IN_PROGRESS;
    if (SrvIp.u1AddrLen <= TFTPC_ZERO || pu1RemoteFile == NULL)
    {
        gu1FileCopyingStatus = TFTP_PROCESS_COMPLETE;
        return TFTPC_E_ARG;
    }
    if (pu1LocalFile == NULL)
    {
        pu1LocalFile = pu1RemoteFile;
    }
    TFTPC_MEMSET (&tftpS, TFTPC_EOS, sizeof (tTftpcSession));
    tftpS.u1AddrType = SrvIp.u1Afi;

    pFile = TFTPC_FOPEN ((const CHR1 *) pu1LocalFile, (const CHR1 *) "w");
    if (pFile == NULL)
    {
        TFTPC_TRC_CRI_ARG1 ("Unable to open File <%s> for writing \n",
                            pu1LocalFile);
        gu1FileCopyingStatus = TFTP_PROCESS_COMPLETE;
        return TFTPC_E_CREATE;
    }

    if (tftpcConfig
        (&tftpS, TFTPC_READ, (UINT2) ATOI (TFTPC_TIMEOUT),
         (UINT2) TFTPC_MAX_RETRY) != TFTPC_OK)
    {
        tftpcTerminate (&tftpS);
        TFTPC_FCLOSE (pFile);
        FileDelete (pu1LocalFile);
        gu1FileCopyingStatus = TFTP_PROCESS_COMPLETE;
        return (TFTPC_E_ARG);
    }
    if ((i4RetVal =
         tftpcConnect (&tftpS, pu1RemoteFile, TFTPC_ZERO, SrvIp)) != TFTPC_OK)
    {
        tftpcTerminate (&tftpS);
        TFTPC_FCLOSE (pFile);
        FileDelete (pu1LocalFile);
        gu1FileCopyingStatus = TFTP_PROCESS_COMPLETE;
        return i4RetVal;
    }

    if (tftpS.u2BlkNo == 1)
    {
        /* Server does not support options, first data packet
         * might be recd already, copy the buffer and send ack
         */
        if (TFTPC_FWRITE
            (tftpS.pu1Data, sizeof (INT1), (size_t) tftpS.i4DataLen,
             pFile) != (UINT4) tftpS.i4DataLen)
        {
            TFTPC_TRC_CRI ("Error in writing to file\n");
            TFTPC_FCLOSE (pFile);
            tftpcSendErrPkt (&tftpS, TFTPC_ERR_UNKNOWN_ERR);
            tftpcTerminate (&tftpS);
            FileDelete (pu1LocalFile);
            gu1FileCopyingStatus = TFTP_PROCESS_COMPLETE;
            return (TFTPC_E_WRITE);
        }
        u4Data = (UINT4) tftpS.i4DataLen;
        if (tftpS.i4DataLen != tftpS.u2BlkSize)
        {
            TFTPC_TRC_MED_ARG1 ("Received Last Packet Total <%d> Blocks\n",
                                tftpS.u2BlkNo);
            tftpcSendAck (&tftpS);
            tftpcTerminate (&tftpS);
            TFTPC_FCLOSE (pFile);
            gu1FileCopyingStatus = TFTP_PROCESS_COMPLETE;
            return TFTPC_OK;
        }
    }
    while (1)
    {
        if ((i4RetVal = tftpcSendAck (&tftpS)) != TFTPC_OK)
            break;
        if ((i4RetVal = tftpcGetPkt (&tftpS)) != TFTPC_OK)
            break;
        if ((i4RetVal = tftpcProcessResponse (&tftpS)) != TFTPC_OK)
            break;
        if (tftpS.i4DataLen < TFTPC_ZERO)
        {
            /* Duplicate Data packet */
            continue;
        }
        if (tftpS.i4DataLen > TFTPC_ZERO)
        {
            if (TFTPC_FWRITE
                (tftpS.pu1Data, sizeof (INT1), (size_t) tftpS.i4DataLen,
                 pFile) != (UINT4) tftpS.i4DataLen)
            {
                TFTPC_TRC_CRI ("Error in writing to file\n");
                TFTPC_FCLOSE (pFile);
                tftpcSendErrPkt (&tftpS, TFTPC_ERR_UNKNOWN_ERR);
                tftpcTerminate (&tftpS);
                FileDelete (pu1LocalFile);
                gu1FileCopyingStatus = TFTP_PROCESS_COMPLETE;
                return (TFTPC_E_WRITE);
            }

            u4Data += (UINT4) tftpS.i4DataLen;
            if ((u4Data != tftpS.u4FileSize) &&
                (tftpS.u4FileSize != TFTPC_ZERO))
            {
                FSAP_U8_ASSIGN_LO (&u8TempData, u4Data);
                FSAP_U8_ASSIGN_LO (&u8Cent, TFTPC_HUNDRED);
                FSAP_U8_ASSIGN_LO (&u8FileSize, tftpS.u4FileSize);

                FSAP_U8_MUL (&u8TempData1, &u8TempData, &u8Cent);
                FSAP_U8_DIV (&u8Option, &u8Rem, &u8TempData1, &u8FileSize);
                i4Option = (INT4) UINT8_LO (&u8Option);

                switch (i4Option)
                {
                    case ISS_10_PERCENT:
                    case ISS_20_PERCENT:
                    case ISS_30_PERCENT:
                    case ISS_40_PERCENT:
                    case ISS_50_PERCENT:
                    case ISS_60_PERCENT:
                    case ISS_70_PERCENT:
                    case ISS_80_PERCENT:
                    case ISS_90_PERCENT:
                        if (i4Flag)
                        {
                            CliTftpDownloadStatus (i4Option, tftpS.u4FileSize);
                        }
                        i4Flag = 0;
                        break;
                    default:
                        i4Flag = 1;
                }
            }
        }
        if (tftpS.i4DataLen != tftpS.u2BlkSize)
        {
            TFTPC_TRC_MED_ARG1 ("Received Last Packet Total <%d> Blocks\n",
                                tftpS.u2BlkNo);
            tftpcSendAck (&tftpS);
            tftpcTerminate (&tftpS);
            TFTPC_FCLOSE (pFile);
            gu1FileCopyingStatus = TFTP_PROCESS_COMPLETE;
            return TFTPC_OK;
        }
    }
    if (i4RetVal != TFTPC_OK)
    {
        tftpcSendErrPkt (&tftpS, TFTPC_ERR_UNKNOWN_ERR);
    }
    tftpcTerminate (&tftpS);
    TFTPC_FCLOSE (pFile);
    if (i4RetVal != TFTPC_OK)
    {
        FileDelete (pu1LocalFile);
    }
    gu1FileCopyingStatus = TFTP_PROCESS_COMPLETE;
    return (i4RetVal);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : tftpcSendFile                                    */
/*                                                                          */
/*    Description        : Send file using tftp client                      */
/*                                                                          */
/*    Input(s)           : server ip/file local file                        */
/*                                                                          */
/*    Output(s)          : Sends the file to   server                       */
/*                                                                          */
/*    Returns            : TFTPC_OK/TFTPC_E_XX                              */
/****************************************************************************/
INT4
tftpcSendFile (tIPvXAddr SrvIp, const UINT1 *pu1RemoteFile,
               const UINT1 *pu1LocalFile)
{
    tTftpcSession       tftpS;
    FILE               *pFile;
    UINT4               u4FileSize = TFTPC_ZERO;
    UINT4               u4LocalFileSize = TFTPC_ZERO;
    UINT4               u4LocalPktSize = TFTPC_ZERO;
    INT4                i4RetVal;
    tTftpcPkt          *pTftpPkt;

    if (SrvIp.u1AddrLen <= TFTPC_ZERO || pu1RemoteFile == NULL)
    {
        return TFTPC_E_ARG;
    }
    if (pu1LocalFile == NULL)
    {
        pu1LocalFile = pu1RemoteFile;
    }
    TFTPC_MEMSET (&tftpS, TFTPC_EOS, sizeof (tTftpcSession));
    tftpS.u1AddrType = SrvIp.u1Afi;

    if (issGetLocalFileSize ((const UINT1 *) pu1LocalFile,
                             &u4FileSize) != (INT4) ISS_SUCCESS)
    {
        TFTPC_TRC_CRI ("Unable to stat the file \n");
        return (TFTPC_E_NOFILE);
    }

    pFile = TFTPC_FOPEN ((const CHR1 *) pu1LocalFile, (const CHR1 *) "r");
    if (pFile == NULL)
    {
        TFTPC_TRC_CRI_ARG1 ("Unable to open File <%s> for Reading \n",
                            pu1LocalFile);
        return TFTPC_E_ARG;
    }
    if (tftpcConfig (&tftpS, TFTPC_WRITE, (UINT2) ATOI (TFTPC_TIMEOUT),
                     TFTPC_MAX_RETRY) != TFTPC_OK)
    {
        TFTPC_FCLOSE (pFile);
        return (TFTPC_E_ARG);
    }
    if ((i4RetVal = tftpcConnect (&tftpS, pu1RemoteFile, u4FileSize,
                                  SrvIp)) != TFTPC_OK)
    {
        TFTPC_FCLOSE (pFile);
        return i4RetVal;
    }
    pTftpPkt = (tTftpcPkt *) (VOID *) tftpS.pu1OutPktBuf;
    /* Set DataPkt OP */
    pTftpPkt->u2OpCode = TFTPC_HTONS (TFTPC_DATA_PKT);
    while (!TFTPC_FEOF (pFile))
    {
        pTftpPkt->u2BlkAckErr = TFTPC_HTONS (tftpS.u2BlkNo);
        if (tftpS.u2BlkSize <= TFTPC_BLOCKSIZE_MAX)
        {
            tftpS.i4OutBufLen =
                (INT4) TFTPC_FREAD (pTftpPkt->au1Data, sizeof (INT1),
                                    tftpS.u2BlkSize, pFile);
        }
        if (tftpS.i4OutBufLen < TFTPC_ZERO)
        {
            TFTPC_TRC_CRI ("TFTPC_FREAD error \n");
            break;
        }
        u4LocalPktSize = (UINT4) tftpS.i4OutBufLen;
        tftpS.i4OutBufLen += 4;    /* For DataPkt header */
        if (tftpcSendPkt (&tftpS) != TFTPC_OK)
            break;
        if (tftpcGetPkt (&tftpS) != TFTPC_OK)
            break;
        if (tftpcProcessResponse (&tftpS) != TFTPC_OK)
            break;
        u4LocalFileSize = u4LocalFileSize + u4LocalPktSize;
    }
    if (TFTPC_FEOF (pFile))
    {
        if (tftpS.i4OutBufLen == tftpS.u2BlkSize)
        {
            /* Send a zero len pkt to indicate end of Xfer */
            pTftpPkt->u2BlkAckErr = TFTPC_HTONS (tftpS.u2BlkNo);
            tftpS.i4OutBufLen = 4;
            tftpcSendPkt (&tftpS);
            tftpcGetPkt (&tftpS);
            tftpcProcessResponse (&tftpS);
        }
        tftpcTerminate (&tftpS);
        TFTPC_FCLOSE (pFile);
        if (u4LocalFileSize != u4FileSize)
        {
            TFTPC_TRC_CRI ("\n File Copy Failed \n");
            return (TFTPC_E_WRITE);
        }
        return (TFTPC_OK);
    }
    tftpcTerminate (&tftpS);
    TFTPC_FCLOSE (pFile);

    if (u4LocalFileSize != u4FileSize)
    {
        TFTPC_TRC_CRI ("\n File Copy Failed \n");
        return (TFTPC_E_WRITE);
    }
    return (i4RetVal);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : tftpcSendBuf                                     */
/*                                                                          */
/*    Description        : Send the data in the buf to remote host          */
/*                                                                          */
/*    Input(s)           : server ip,file,buf,bufsize                       */
/*                                                                          */
/*    Output(s)          :                                                  */
/*                                                                          */
/*    Returns            : TFTPC_OK/TFTPC_E_XX                              */
/****************************************************************************/
INT4
tftpcSendBuf (tIPvXAddr SrvIp, const UINT1 *pu1File, UINT1 *pu1Buf,
              UINT4 u4BufSize)
{
    tTftpcSession       tftpS;
    INT4                i4RetVal;
    UINT4               u4SendCnt = 0;
    tTftpcPkt          *pTftpPkt;

    if (SrvIp.u1AddrLen <= TFTPC_ZERO || pu1File == NULL ||
        pu1Buf == NULL || u4BufSize <= 0)
    {
        return TFTPC_E_ARG;
    }

    TFTPC_MEMSET (&tftpS, TFTPC_EOS, sizeof (tTftpcSession));

    tftpS.u1AddrType = SrvIp.u1Afi;
    if ((i4RetVal =
         tftpcConfig (&tftpS, TFTPC_WRITE, (UINT2) ATOI (TFTPC_TIMEOUT),
                      TFTPC_MAX_RETRY)) != TFTPC_OK)
    {
        return (i4RetVal);
    }
    if ((i4RetVal =
         tftpcConnect (&tftpS, pu1File, u4BufSize, SrvIp)) != TFTPC_OK)
    {
        return i4RetVal;
    }
    /* Set DataPkt OP */
    pTftpPkt = (tTftpcPkt *) (VOID *) tftpS.pu1OutPktBuf;
    pTftpPkt->u2OpCode = TFTPC_HTONS (TFTPC_DATA_PKT);
    while (u4SendCnt < u4BufSize)
    {
        pTftpPkt->u2BlkAckErr = TFTPC_HTONS (tftpS.u2BlkNo);

        if ((u4BufSize - u4SendCnt) > tftpS.u2BlkSize)
        {
            MEMCPY (pTftpPkt->au1Data, &pu1Buf[u4SendCnt],
                    MEM_MAX_BYTES (tftpS.u2BlkSize,
                                   sizeof (pTftpPkt->au1Data)));
            tftpS.i4OutBufLen = tftpS.u2BlkSize;
            u4SendCnt += tftpS.u2BlkSize;
        }
        else
        {
            MEMCPY (pTftpPkt->au1Data, &pu1Buf[u4SendCnt],
                    MEM_MAX_BYTES ((u4BufSize - u4SendCnt),
                                   sizeof (pTftpPkt->au1Data)));
            tftpS.i4OutBufLen = (INT4) (u4BufSize - u4SendCnt);
            u4SendCnt += (u4BufSize - u4SendCnt);
            /* Now the u4SendCnt == u4BufSize, the loop should break */
        }

        tftpS.i4OutBufLen += 4;    /* For DataPkt header */
        if ((i4RetVal = tftpcSendPkt (&tftpS)) != TFTPC_OK)
            break;
        if ((i4RetVal = tftpcGetPkt (&tftpS)) != TFTPC_OK)
            break;
        if ((i4RetVal = tftpcProcessResponse (&tftpS)) != TFTPC_OK)
            break;
    }
    if (i4RetVal == TFTPC_OK)
    {
        if ((tftpS.i4OutBufLen - 4) == tftpS.u2BlkSize)
        {
            /* Send a zero len pkt to indicate end of Xfer */
            pTftpPkt->u2BlkAckErr = TFTPC_HTONS (tftpS.u2BlkNo);
            tftpS.i4OutBufLen = 4;
            tftpcSendPkt (&tftpS);
            tftpcGetPkt (&tftpS);
            tftpcProcessResponse (&tftpS);
        }
        tftpcTerminate (&tftpS);
        return (TFTPC_OK);
    }
    tftpcTerminate (&tftpS);
    return (i4RetVal);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : tftpcSendMultipleBuffers                         */
/*                                                                          */
/*    Description        : Send the data in the given buffers to            */
/*                          the remote host                                 */
/*                                                                          */
/*    Input(s)           : server ip,file,buf,bufsize,connection status     */
/*                                                                          */
/*    Output(s)          :                                                  */
/*                                                                          */
/*    Returns            : TFTPC_OK/TFTPC_E_XX                              */
/****************************************************************************/
INT4
tftpcSendMultipleBuffers (tIPvXAddr SrvIp, const UINT1 *pu1File, UINT1 *pu1Buf,
                          UINT4 u4BufSize, tTftpcConnStatus ConnStatus)
{
    static tTftpcSession tftpS;
    tTftpcPkt          *pTftpPkt;
    INT4                i4RetVal = 0;
    UINT4               u4SendCnt = 0;
    UINT4               u4RemLen = 0;
    UINT1              *pData = NULL;

    if (SrvIp.u1AddrLen <= TFTPC_ZERO || pu1File == NULL ||
        pu1Buf == NULL || u4BufSize <= 0)
    {
        return TFTPC_E_ARG;
    }

    if ((ConnStatus == TFTP_INITIATE) || (tftpS.i4SockFd == 0))
    {
        TFTPC_MEMSET (&tftpS, TFTPC_EOS, sizeof (tTftpcSession));
        tftpS.u1AddrType = SrvIp.u1Afi;

        if ((i4RetVal = tftpcConfig (&tftpS, TFTPC_WRITE,
                                     (UINT2) ATOI (TFTPC_TIMEOUT),
                                     TFTPC_MAX_RETRY)) != TFTPC_OK)
        {
            TFTPC_MEMSET (&tftpS, TFTPC_EOS, sizeof (tTftpcSession));
            return (i4RetVal);
        }
        if ((i4RetVal = tftpcConnect (&tftpS, pu1File, 0, SrvIp)) != TFTPC_OK)
        {
            TFTPC_MEMSET (&tftpS, TFTPC_EOS, sizeof (tTftpcSession));
            return i4RetVal;
        }
        MEMSET (gMsrPendBuf, 0, TFTPC_BLOCKSIZE_MAX);
        gMsrPendBufLength = 0;
    }

    /* Set DataPkt OP */
    pTftpPkt = (tTftpcPkt *) (VOID *) tftpS.pu1OutPktBuf;
    pTftpPkt->u2OpCode = TFTPC_HTONS (TFTPC_DATA_PKT);
    while (u4SendCnt < u4BufSize)
    {
        pTftpPkt->u2BlkAckErr = TFTPC_HTONS (tftpS.u2BlkNo);

        if (gMsrPendBufLength != 0)
        {
            pData = pTftpPkt->au1Data;
            MEMCPY (pData, gMsrPendBuf,
                    MEM_MAX_BYTES (gMsrPendBufLength, sizeof (gMsrPendBuf)));
            u4RemLen = tftpS.u2BlkSize - gMsrPendBufLength;
            pData += gMsrPendBufLength;

            /* Even when the u4BufSize is less than u4RemLen,
             * the pu1Buf (FlashArray) will never be accessed
             * outside its boundary */
            MEMCPY (pData, &pu1Buf[u4SendCnt], u4RemLen);
            tftpS.i4OutBufLen = tftpS.u2BlkSize;
            u4SendCnt = u4RemLen;
            MEMSET (gMsrPendBuf, 0, TFTPC_BLOCKSIZE_MAX);
            gMsrPendBufLength = 0;
        }
        else if ((u4BufSize - u4SendCnt) > tftpS.u2BlkSize)
        {
            MEMCPY (pTftpPkt->au1Data, &pu1Buf[u4SendCnt],
                    MEM_MAX_BYTES (tftpS.u2BlkSize,
                                   sizeof (pTftpPkt->au1Data)));
            tftpS.i4OutBufLen = tftpS.u2BlkSize;
            u4SendCnt += tftpS.u2BlkSize;
        }
        else
        {
            if (ConnStatus != TFTP_TERMINATE)
            {
                gMsrPendBufLength = u4BufSize - u4SendCnt;
                MEMCPY (gMsrPendBuf, &pu1Buf[u4SendCnt],
                        MEM_MAX_BYTES (gMsrPendBufLength,
                                       sizeof (gMsrPendBuf)));
                return i4RetVal;
            }
            else
            {
                MEMCPY (pTftpPkt->au1Data, &pu1Buf[u4SendCnt],
                        MEM_MAX_BYTES ((u4BufSize - u4SendCnt),
                                       sizeof (pTftpPkt->au1Data)));
                tftpS.i4OutBufLen = (INT4) (u4BufSize - u4SendCnt);
                u4SendCnt += (u4BufSize - u4SendCnt);
            }
            /* Now the u4SendCnt == u4BufSize, the loop should break */
        }

        tftpS.i4OutBufLen += 4;    /* For DataPkt header */
        if ((i4RetVal = tftpcSendPkt (&tftpS)) != TFTPC_OK)
            break;
        if ((i4RetVal = tftpcGetPkt (&tftpS)) != TFTPC_OK)
            break;
        if ((i4RetVal = tftpcProcessResponse (&tftpS)) != TFTPC_OK)
            break;
    }

    if (ConnStatus == TFTP_TERMINATE)
    {
        if ((tftpS.i4OutBufLen - 4) == tftpS.u2BlkSize)
        {
            /* Send a zero len pkt to indicate end of Xfer */
            pTftpPkt->u2BlkAckErr = TFTPC_HTONS (tftpS.u2BlkNo);
            tftpS.i4OutBufLen = 4;
            tftpcSendPkt (&tftpS);
            tftpcGetPkt (&tftpS);
            tftpcProcessResponse (&tftpS);
        }
    }
    tftpcTerminate (&tftpS);
    TFTPC_MEMSET (&tftpS, TFTPC_EOS, sizeof (tTftpcSession));
    return (i4RetVal);
}

#endif /* __TFTPCSYS_C__ */
