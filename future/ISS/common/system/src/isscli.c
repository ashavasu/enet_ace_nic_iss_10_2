/* SOURCE FILE  :
 *  ---------------------------------------------------------------------------
 * |  Copyright (C) 2006 Aricent Inc . All Rights Reserved                    |
 * |                                                                          |
 * |  FILE NAME                   :  isscli.c                                 |
 * |                                                                          |
 * |  PRINCIPAL AUTHOR            :  Aricent Inc.                             |
 * |                                                                          |
 * |  SUBSYSTEM NAME              :  ISS                                      |
 * |                                                                          |
 * |  MODULE NAME                 :  ISS                                      |
 * |                                                                          |
 * |  LANGUAGE                    :  C                                        |
 * |                                                                          |
 * |  TARGET ENVIRONMENT          :  LINUX                                    |
 * |                                                                          |
 * |  AUTHOR                      :  ISS Team                                 |
 * |                                                                          |
 * |  DATE OF FIRST RELEASE :                                                 |
 * |
 * |  $Id: isscli.c,v 1.477.2.1 2018/03/07 12:52:02 siva Exp $
 * |
 * |  DESCRIPTION                 :  This file contains CLI routines related  |
 * |                                 to system commands                       |
 *  ---------------------------------------------------------------------------
 */
#ifndef  __ISSCLI_C__
#define  __ISSCLI_C__
#include "lr.h"
#include "aclcli.h"
#include "fssnmp.h"
#include "iss.h"
#include "msr.h"
#include "issmacro.h"
#include "issinc.h"
#include "fsisswr.h"
#include "isslow.h"
#include "fsisselw.h"
#include "tftpc.h"
#include "sftpc.h"
#include "isscli.h"
#include "bridge.h"
#include "vcm.h"
#include "fsisscli.h"
#include "bpsrvcli.h"
#include "sntpcli.h"
#ifdef NTPS_WANTED
#include "ntpscli.h"
#endif
#include "rmcli.h"
#include "hbcli.h"
#ifdef RRD_WANTED
#include "rtm6cli.h"
#endif
#ifdef ICCH_WANTED
#include "icchcli.h"
#endif /* ICCH_WANTED */
#include "taccli.h"
#include "ospf3cli.h"
#include "snp.h"
#ifdef WEBNM_WANTED
#include "httpcli.h"
#endif
#ifdef DCBX_WANTED
#include "dcbxcli.h"
#endif /*DCBX_WANTED */
#ifdef CN_WANTED
#include "cncli.h"
#endif /*CN_WANTED */
#ifdef SSH_WANTED
#include "sftpapi.h"
#endif
#ifdef PTP_WANTED
#include "ptpcli.h"
#endif
#ifdef CLKIWF_WANTED
#include "clkiwcli.h"
#endif
#ifdef HOTSPOT2_WANTED
#include "hscli.h"
#endif
#ifdef CFA_WANTED
#include "ipdbcli.h"
#endif /* CFA_WANTED */
#ifdef DNS_WANTED
#include "dnscli.h"
#endif
#include "dvmrpcli.h"
#include "issnpwr.h"

#ifdef QOSX_WANTED
#include "qosxcli.h"
#endif
#ifdef RBRG_WANTED
#include "rbrgcli.h"
#endif
#ifdef RFC2544_WANTED
#include "r2544cli.h"
#endif
#ifdef Y1564_WANTED
#include "y1564cli.h"
#endif
#include "fsapcli.h"
extern INT4         gi4TftpSysLogId;
extern INT4         gi4ClearConfigSupport;
#ifdef ISS_TEST_WANTED
extern INT4         gi4CentralizedIPSupport;
#endif
extern tNVRAM_DATA  sNvRamData;
#ifdef WTP_WANTED
extern tWTPNVRAM_DATA sWtpNvRamData;
#endif
#include <dirent.h>
extern tLnkUpSysControl gLnkUpSystemControl;
static UINT1        au1TmpVlanList[ISS_VLAN_LIST_SIZE];
extern INT4         CliOpenFile (CONST CHR1 * pi1Filename, INT4 i4Flag);
extern INT1         CliGetLine (INT4, INT1 *);
extern INT2         CliCloseFile (INT4 i4FileFd);
static INT1         ai1Cmd[MAX_LINE_LEN + 4];
static UINT1        au1CustFilePath[MAX_FILEPATH_LEN] = ISS_IMAGE_PATH;
static UINT1        au1TempFileName[ISS_CONFIG_FILE_NAME_LEN + 1];
static UINT1        au1SrcUserName[ISS_CONFIG_USER_NAME_LEN];
static UINT1        au1SrcPassWd[ISS_CONFIG_PASSWD_LEN];
static UINT1        au1DstUserName[ISS_CONFIG_USER_NAME_LEN];
static UINT1        au1DstPassWd[ISS_CONFIG_PASSWD_LEN];
extern VOID         IssIpArpShowDebugging (tCliHandle);

#define LOGGER_MSG_ENQ_EVENT          0x0002
#define LOGGER_SEND_EVENT    OsixEvtSend
/***************************************************************/
/*  Function Name   : cli_process_iss_cmd                      */
/*  Description     : This function servers as the handler for */
/*                    all system related CLI commands          */
/*  Input(s)        :                                          */
/*                    CliHandle - CLI Handle                   */
/*                    u4Command - Command given by user        */
/*                    Variable set of inputs depending on      */
/*                    the user command                         */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : None                                     */
/***************************************************************/

VOID
cli_process_iss_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *apu1ClientSessionInfo[CLI_ISS_MAX_ARGS];
    UINT4              *args[CLI_ISS_MAX_ARGS];
    INT1                argno = 0;
    INT1                i1Flag = OSIX_FALSE;
    UINT4               u4ErrCode = 0;
    INT4                i4Inst;
    INT4                i4RetStatus = CLI_SUCCESS;
    INT4                i4SessionIndex = 0;
    INT4                i4SessionStatus = 0;
    INT4                i4RspanStatus = 0;
    INT4                i4IssMacLearnLimitRate = 0;
    UINT4               u4IssMacLearnRateLimitInterval = 0;
    UINT4               u4Mask, u4Mode, u4IfIndex, u4Mon;
    UINT4               u4Type = 0;
    UINT4               u4IpAddress = 0;
    tIPvXAddr           IpAddress;
    tIPvXAddr           DstIpAddress;
    UINT4               u4SrcIpAddress = 0;
    UINT4               u4DstIpAddress = 0;
    UINT4               u4Services;

    UINT1               u1AuthOOBFlag = ISS_FALSE;
    tMacAddr            BaseMacAddr;
    tIp6Addr            Ip6Addr;
    UINT1              *pu1ContextName = NULL;
    UINT4               u4Module = 0;
    UINT4               u4Val = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4Status = 0;
    UINT4               u1Status = 0;
    UINT4               u4RspanVlan = 0;
    UINT4               u4MirrorMode = 0;
    UINT4               u4SourceId = 0;
    UINT4               u4MirrorCfg = 0;
    UINT4               u4StartIndex = 0;
    UINT4               u4EndIndex = 0;
    UINT4               u4MirroringMode = ISS_MIRR_TEMP_MODE;
    UINT1               au1FileName[ISS_CONFIG_FILE_NAME_LEN + 1];
    UINT1               au1FilePath[ISS_CONFIG_FILE_NAME_LEN + 1];
    static UINT1        au1PortList[ISS_AUTH_PORT_LIST_SIZE];
    static UINT1        au1VlanList[ISS_VLAN_LIST_SIZE];
    UINT1               au1DstFileName[ISS_CONFIG_FILE_NAME_LEN + 1];
    UINT1               u1TempMirrAccessMode = ISS_MIRR_TEMP_MODE;
    UINT1               au1MirrorSesList[ISS_MIRR_LIST_SIZE];
    UINT1               au1Session[MAX_NAME_SIZE];
    UINT1               u1ArgCount = 0;
    INT4                i4IssPortType = 0;
#ifdef NPAPI_WANTED
    UINT1               u1TraceLevel = 0;
    UINT1               u1TraceModule = 0;
    BOOLEAN             bIsSvrtNum = FALSE;
    UINT2               u2TraceLevel = 0;
    UINT2               u2TraceVal = 0;
#endif

    UINT1               u1Index = 0;
    UINT1              *pu1MemLoc = NULL;
    UINT1              *u4exeEnd = NULL;
    UINT1               au1HostName[DNS_MAX_QUERY_LEN];
    UINT1               au1DstHostName[DNS_MAX_QUERY_LEN];
    UINT4               u4Len = 0;
    UINT4               length = 0;
    tHwPortInfo         pHwPortInfo;

    MEMSET (&au1MirrorSesList, 0, ISS_MIRR_LIST_SIZE);
    MEMSET (&DstIpAddress, 0, sizeof (tIPvXAddr));
    MEMSET (&IpAddress, 0, sizeof (tIPvXAddr));
    MEMSET (au1FileName, 0, sizeof (au1FileName));
    MEMSET (au1FilePath, 0, sizeof (au1FilePath));
    MEMSET (au1DstFileName, 0, sizeof (au1DstFileName));
    MEMSET (au1TempFileName, 0, sizeof (au1TempFileName));
    MEMSET (&au1Session, 0, sizeof (au1Session));
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&au1CustFilePath, 0, sizeof (au1CustFilePath));
    MEMSET (&au1SrcUserName, 0, sizeof (au1SrcUserName));
    MEMSET (&au1SrcPassWd, 0, sizeof (au1SrcPassWd));
    MEMSET (&au1DstUserName, 0, sizeof (au1DstUserName));
    MEMSET (&au1DstPassWd, 0, sizeof (au1DstPassWd));
    MEMSET (&au1HostName, 0, DNS_MAX_QUERY_LEN);
    MEMSET (&au1DstHostName, 0, DNS_MAX_QUERY_LEN);

    va_start (ap, u4Command);

    /* Third arguement is always interface name/index */
    i4Inst = va_arg (ap, INT4);

    /* Walk through the rest of the arguements and store in args array.  */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == CLI_ISS_MAX_ARGS)
            break;
    }
    va_end (ap);

    CLI_SET_ERR (0);

    CliRegisterLock (CliHandle, IssLock, IssUnLock);

    ISS_LOCK ();

    switch (u4Command)
    {
        case CLI_ISS_DEF_IPADDR_MODE:
            i4RetStatus = CliSetIssDefaultIpAddrCfgMode (CliHandle,
                                                         CLI_PTR_TO_U4 (args
                                                                        [0]));
            break;

        case CLI_ISS_SWITCH_NAME:
            i4RetStatus = CliSetIssSwitchName (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_ISS_DEF_RESTORE_FILE:
            i4RetStatus = CliSetIssDefaultRestoreFile (CliHandle,
                                                       (UINT1 *) args[0]);
            break;

        case CLI_ISS_DEF_IPADDR_ALLOC_PROTO:

            i4RetStatus = CliSetIssDefaultIpAddrAllocProto (CliHandle,
                                                            CLI_PTR_TO_U4
                                                            (args[0]));
            break;

        case CLI_ISS_DEF_IPADDR:
            if (args[1] == NULL)
            {
                u4Mask = 0;
            }
            else
            {
                u4Mask = *args[1];
            }
            i4RetStatus = CliSetIssDefaultIpAddress (CliHandle,
                                                     *args[0],
                                                     u4Mask, (UINT4) i4Inst);
            break;
        case CLI_ISS_DEF_RM_INT:

            i4RetStatus = CliSetIssDefaultRmInit (CliHandle, (UINT1 *) args[0]);
            break;

        case CLI_ISS_DEF_RM_INT_TYPE:

            i4RetStatus = CliSetIssDefaultRmInterfaceType (CliHandle,
                                                           CLI_PTR_TO_I4
                                                           (args[0]));
            break;

        case CLI_ISS_BASE_MAC:

            StrToMac ((UINT1 *) args[0], BaseMacAddr);

            i4RetStatus = CliSetIssBaseMacAddr (CliHandle, BaseMacAddr);
            break;

        case CLI_ISS_DEBUG_LOGGING:
            i4RetStatus = CliSetIssDebugLogging (CliHandle,
                                                 CLI_PTR_TO_U4 (args[0]),
                                                 (UINT1)
                                                 CLI_PTR_TO_I4 (args[1]));
            break;

        case CLI_ISS_DEBUG_FLASH_LOGGING:

            if (CLI_PTR_TO_U4 (args[0]) == FLASH_LOG_LOCATION)
            {
                if (CliGetFlashFileName ((INT1 *) args[1],
                                         (INT1 *) au1FileName) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Error in accessing Flash Path \r\n");
                    CliUnRegisterLock (CliHandle);
                    ISS_UNLOCK ();
                    return;
                }
                /* 25 characters for "DEBUG_LOG_FILE_LOCATION =". We need to consider
                 * this also when the length is checked
                 * */
                if (STRLEN (au1FilePath) >=
                    (MAX_FLASH_FILENAME_LEN -
                     STRLEN ("DEBUG_LOG_FILE_LOCATION =")))
                {
                    CliPrintf (CliHandle,
                               "\r%% Error - File Length is too big \r\n");
                    CliUnRegisterLock (CliHandle);
                    ISS_UNLOCK ();
                    return;
                }

                i4RetStatus = CliSetIssDebugFlashLogging (CliHandle,
                                                          CLI_PTR_TO_U4 (args
                                                                         [0]),
                                                          (UINT1 *)
                                                          au1FileName);
            }
            break;

        case CLI_ISS_IMAGE_DUMP:
            if (CliGetFlashFileName ((INT1 *) args[0],
                                     (INT1 *) au1FilePath) == CLI_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Error in accessing Flash Path \r\n");
                CliUnRegisterLock (CliHandle);
                ISS_UNLOCK ();
                return;
            }
            /* 15 characters for "IMG_DUMP_PATH =". We need to consider
             * this also when the length is checked
             * */
            if (STRLEN (au1FilePath) >=
                (MAX_FLASH_FILENAME_LEN - STRLEN ("IMG_DUMP_PATH =")))
            {
                CliPrintf (CliHandle,
                           "\r%% Error - Path Length is too big \r\n");
                CliUnRegisterLock (CliHandle);
                ISS_UNLOCK ();
                return;
            }

            i4RetStatus = CliSetImageDump (CliHandle, (UINT1 *) au1FilePath);
            break;
        case CLI_ISS_NO_DEBUG_LOGGING:
            i4RetStatus =
                CliSetIssDebugLogging (CliHandle, CONSOLE_LOG,
                                       (UINT1) CLI_PTR_TO_U4 (args[0]));
            break;
#ifdef NPAPI_WANTED

        case CLI_ISS_NPAPI_DEBUG_ON:
            u2TraceLevel = (UINT2) CLI_PTR_TO_U4 (args[0]);
            if (u2TraceLevel == ISS_CLI_NP_TIME_STAMP_FLG)
            {
                UtlShowTime (1);
                break;
            }
            IsssysNpGetTraceLevel (&u2TraceVal);
            u2TraceLevel |= u2TraceVal;
            IsssysNpSetTraceLevel (u2TraceLevel);
            break;

        case CLI_ISS_NPAPI_DEBUG_OFF:
            u2TraceLevel = (UINT2) CLI_PTR_TO_U4 (args[0]);
            if (u2TraceLevel == ISS_CLI_NP_TIME_STAMP_FLG)
            {
                UtlShowTime (0);
                break;
            }
            IsssysNpGetTraceLevel (&u2TraceVal);
            u2TraceVal &= (~(u2TraceLevel));
            IsssysNpSetTraceLevel (u2TraceVal);
            break;

        case CLI_ISS_SWITCH_MODE:
            i4RetStatus = CliSetIssCutThrough (CliHandle);
            break;
        case CLI_ISS_NO_SWITCH_MODE:
            i4RetStatus = CliSetIssNoCutThrough (CliHandle);
            break;
        case CLI_ISS_SHOW_SWITCHING_MODE:
            i4RetStatus = CliIssShowSwitchModeType (CliHandle);
            break;

#endif

        case CLI_ISS_HW_CONSOLE:

            i4RetStatus = CliSetIssHwConsole (CliHandle, (UINT1 *) (args[0]));
            break;

        case CLI_ISS_DUMP_OSRESOURCE:
            i4RetStatus = CliSetIssDumpOsResource (CliHandle,
                                                   CLI_PTR_TO_U4 (args[0]),
                                                   (CHR1 *) args[1]);
            break;
#ifdef NPAPI_WANTED
        case CLI_ISS_DEBUG_NP:
            /*
             * args[0] -  Module name
             * args[1] -  Flag to identify whether severity level is numeric or not
             * args[2] -  Severity level value
             * args[3] -  Severity type
             */
        {
            u1TraceModule = (UINT1) CLI_PTR_TO_U4 (args[0]);
            bIsSvrtNum = (BOOLEAN) CLI_PTR_TO_U4 (args[1]);
            if (bIsSvrtNum == TRUE)
            {
                u1TraceLevel = (UINT1) CLI_PTR_TO_U4 (*(args[2]));
            }
            else
            {
                u1TraceLevel = (UINT1) CLI_PTR_TO_U4 (args[3]);
            }
            IsssysNpSetTrace (u1TraceModule, u1TraceLevel);
        }
            break;
        case CLI_ISS_NO_DEBUG_NP:
            /*
             * args[0] - Module Name
             */
        {
            u1TraceModule = (UINT1) CLI_PTR_TO_U4 (args[0]);
            IsssysNpSetTrace (u1TraceModule, CLI_ISS_NP_LOG_OFF_LEVEL);
        }
            break;

#endif

        case CLI_ISS_DEFAULT_VLAN_ID:
            i4RetStatus = CliSetIssDefaultVlanId (CliHandle, *args[0]);
            break;

        case CLI_ISS_SET_TIMER_SPEED:
            i4RetStatus = CliSetIssSystemTmrSpeed (CliHandle, *args[0]);
            break;
#ifndef MBSM_WANTED
        case CLI_ISS_FRONT_PANEL_PORT_COUNT:
            i4RetStatus = CliSetIssFrontPanelPortCount (CliHandle, *args[0]);
            break;

        case CLI_ISS_DEF_FRONT_PANEL_PORT_COUNT:
            i4RetStatus =
                CliSetIssFrontPanelPortCount (CliHandle,
                                              ISS_DEFAULT_FRONT_PANEL_PORT_COUNT);
            break;
#endif
        case CLI_ISS_AUDIT_STATUS:
            i4RetStatus =
                CliSetIssAuditLogStatus (CliHandle,
                                         (UINT4) CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_ISS_AUDIT_LOG_FILE:
            i4RetStatus = CliSetIssAuditLogFile (CliHandle, (UINT1 *) args[0]);
            break;
        case CLI_ISS_AUDIT_LOG_FILE_SIZE:
            i4RetStatus = CliSetIssAuditLogFileSize (CliHandle, *args[0]);
            break;

        case CLI_ISS_AUDIT_LOG_SIZE_THRESHOLD:
            i4RetStatus = CliSetIssAuditLogSizeThreshold (CliHandle, *args[0]);
            break;

        case CLI_ISS_AUDIT_LOG_RESET:
            i4RetStatus = CliSetIssAuditLogReset (CliHandle);
            break;

        case CLI_ISS_SHOW_AUDIT:
            i4RetStatus = CliShowIssAuditLog (CliHandle);
            break;

        case CLI_ISS_SHOW_AUDIT_LOG_FILE:
            i4RetStatus = CliShowIssAuditLogFile (CliHandle);
            break;

        case CLI_ISS_RESTART:
            i4RetStatus = CliSetIssRestart ();
            break;

        case CLI_ISS_STANDBY_RESTART:
            i4RetStatus = CliSetIssStandbyRestart ();
            break;

        case CLI_ISS_MIRROR_STATUS:
            /* args[0]- Enables/Disables the mirror status */
            i4RetStatus =
                CliSetIssMirrorStatus (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_ISS_CPU_MIRRORING:
            /* args[0] - Mirror type 
             * args[1] - Mirror-to port IfIndex*/
            i4RetStatus =
                CliSetIssCpuMirroring (CliHandle, CLI_PTR_TO_I4 (args[0]),
                                       CLI_PTR_TO_U4 (args[1]));
            break;

        case CLI_ISS_MIRR_SPAN_SOURCE:

            i4SessionIndex = (INT4) CLI_PTR_TO_U4 (args[0]);
            u4IfIndex = CLI_PTR_TO_U4 (args[1]);
            u4MirrorMode = CLI_PTR_TO_U4 (args[2]);
            u4MirrorCfg = CLI_PTR_TO_U4 (args[3]);
            u4MirroringMode = CLI_PTR_TO_U4 (args[4]);

            if (u4MirroringMode == ISS_MIRR_COMPATIBILITY_MODE)

            {
                if (i4SessionIndex != ISS_MIRR_DEFAULT_SESSION)
                {
                    CliPrintf (CliHandle,
                               "%% Session %d not valid for Compatibilty mode\r\n",
                               args[0]);
                    break;
                }
                u1TempMirrAccessMode = gIssGlobalInfo.u1MirrAccessMode;
                gIssGlobalInfo.u1MirrAccessMode = ISS_MIRR_TEMP_MODE;
                i4RetStatus = CliSetIssMirrorSpanSource (CliHandle,
                                                         i4SessionIndex,
                                                         ISS_MIRR_PORT_BASED,
                                                         u4IfIndex,
                                                         u4MirrorMode,
                                                         u4MirrorCfg);
                gIssGlobalInfo.u1MirrAccessMode = u1TempMirrAccessMode;
            }
            else
            {
                i4RetStatus = CliSetIssMirrorSpanSource (CliHandle,
                                                         i4SessionIndex,
                                                         ISS_MIRR_PORT_BASED,
                                                         u4IfIndex,
                                                         u4MirrorMode,
                                                         u4MirrorCfg);
            }
            break;

        case CLI_ISS_MIRR_SPAN_SOURCE_VLAN:

            i4SessionIndex = (INT4) CLI_PTR_TO_U4 (args[0]);
            u4SourceId = CLI_PTR_TO_U4 (args[1]);
            u4MirrorMode = CLI_PTR_TO_U4 (args[3]);
            u4MirrorCfg = CLI_PTR_TO_U4 (args[4]);

            if (args[2] != NULL)
            {

                if (VcmIsSwitchExist ((UINT1 *) args[2], &u4ContextId) !=
                    VCM_TRUE)
                {
                    CliPrintf (CliHandle,
                               "%% Switch %s Does not exist.\r\n", args[2]);
                    break;
                }

            }
            else
            {
                u4ContextId = L2IWF_DEFAULT_CONTEXT;
            }

            i4RetStatus = CliSetIssMirrorSpanSourceVlan (CliHandle,
                                                         i4SessionIndex,
                                                         ISS_MIRR_VLAN_BASED,
                                                         u4SourceId,
                                                         u4ContextId,
                                                         u4MirrorMode,
                                                         u4MirrorCfg);
            break;
        case CLI_ISS_MIRR_SPAN_MAC_SOURCE_ACL:

            i4SessionIndex = (INT4) CLI_PTR_TO_U4 (args[0]);
            u4SourceId = CLI_PTR_TO_U4 (args[1]);
            u4MirrorMode = CLI_PTR_TO_U4 (args[2]);
            u4MirrorCfg = CLI_PTR_TO_U4 (args[3]);

            i4RetStatus = CliSetIssMirrorSpanSource (CliHandle,
                                                     i4SessionIndex,
                                                     ISS_MIRR_MAC_FLOW_BASED,
                                                     u4SourceId,
                                                     u4MirrorMode, u4MirrorCfg);
            break;
        case CLI_ISS_MIRR_SPAN_IP_SOURCE_ACL:

            i4SessionIndex = (INT4) CLI_PTR_TO_U4 (args[0]);
            u4SourceId = CLI_PTR_TO_U4 (args[1]);
            u4MirrorMode = CLI_PTR_TO_U4 (args[2]);
            u4MirrorCfg = CLI_PTR_TO_U4 (args[3]);

            i4RetStatus = CliSetIssMirrorSpanSource (CliHandle,
                                                     i4SessionIndex,
                                                     ISS_MIRR_IP_FLOW_BASED,
                                                     u4SourceId,
                                                     u4MirrorMode, u4MirrorCfg);
            break;
        case CLI_ISS_MIRR_SPAN_DEST:

            i4SessionIndex = (INT4) CLI_PTR_TO_U4 (args[0]);
            u4IfIndex = CLI_PTR_TO_U4 (args[1]);
            u4MirrorCfg = CLI_PTR_TO_U4 (args[2]);
            u4MirroringMode = CLI_PTR_TO_U4 (args[3]);

            if (u4MirroringMode == ISS_MIRR_COMPATIBILITY_MODE)
            {
                if (i4SessionIndex != ISS_MIRR_DEFAULT_SESSION)
                {
                    CliPrintf (CliHandle,
                               "%% Session %d not valid for Compatibilty mode\r\n",
                               args[0]);
                    break;
                }
                u1TempMirrAccessMode = gIssGlobalInfo.u1MirrAccessMode;
                gIssGlobalInfo.u1MirrAccessMode = ISS_MIRR_TEMP_MODE;

                i4RetStatus = CliSetIssMirrorSpanDestination (CliHandle,
                                                              i4SessionIndex,
                                                              u4IfIndex,
                                                              u4MirrorCfg);
                gIssGlobalInfo.u1MirrAccessMode = u1TempMirrAccessMode;
            }
            else
            {
                i4RetStatus = CliSetIssMirrorSpanDestination (CliHandle,
                                                              i4SessionIndex,
                                                              u4IfIndex,
                                                              u4MirrorCfg);
            }
            break;

        case CLI_ISS_MIRR_RSPAN:

            i4SessionIndex = (INT4) CLI_PTR_TO_U4 (args[0]);
            u4Status = CLI_PTR_TO_U4 (args[1]);
            u4RspanVlan = CLI_PTR_TO_U4 (args[2]);
            u4MirrorCfg = CLI_PTR_TO_U4 (args[4]);
            if (args[3] != NULL)
            {

                if (VcmIsSwitchExist ((UINT1 *) args[3], &u4ContextId) !=
                    VCM_TRUE)
                {
                    CliPrintf (CliHandle,
                               "%% Switch %s Does not exist.\r\n", args[3]);
                    break;
                }

            }
            else
            {
                u4ContextId = L2IWF_DEFAULT_CONTEXT;
            }

            i4RetStatus = CliSetIssMirrorRspan (CliHandle,
                                                i4SessionIndex, u4Status,
                                                u4RspanVlan, u4ContextId,
                                                u4MirrorCfg);

            break;
        case CLI_ISS_NO_MIRR:

            u4MirrorCfg = CLI_PTR_TO_U4 (args[0]);

            if (u4MirrorCfg == ISS_MIRR_LOCAL_DEL)
            {
                for (i4SessionIndex = 1;
                     i4SessionIndex <= ISS_MIRR_MAX_SESSIONS; i4SessionIndex++)
                {
                    i4RetStatus = CliSetIssNoMirror (CliHandle, i4SessionIndex);
                }
            }
            else if (u4MirrorCfg == ISS_MIRR_RANGE_DEL)
            {

                if (CliGetRange ((UINT1 *) args[1], &u4StartIndex, &u4EndIndex)
                    == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "%% Incorrect range specified\r\n");
                    break;
                }
                if (u4EndIndex > ISS_MIRR_MAX_SESSIONS)
                {
                    CliPrintf (CliHandle,
                               "%% Monitor session range must be in between (1-20)\r\n");
                    break;
                }
                CLI_MEMSET (au1MirrorSesList, 0, ISS_MIRR_LIST_SIZE);
                if (CliStrToPortList ((UINT1 *) args[1], au1MirrorSesList,
                                      ISS_MIRR_LIST_SIZE,
                                      ISS_MIRR_INIT_VAL) == CLI_FAILURE)
                {

                    CliPrintf (CliHandle, "%% Invalid Session List\r\n");
                    break;
                }

                for (i4SessionIndex = 1;
                     i4SessionIndex <= ISS_MIRR_MAX_SESSIONS; i4SessionIndex++)
                {
                    ISS_IS_MEMBER_OF_PORTLIST (au1MirrorSesList, i4SessionIndex,
                                               u1Status);
                    if (u1Status == ISS_TRUE)
                    {

                        i4RetStatus = CliSetIssNoMirror (CliHandle,
                                                         i4SessionIndex);
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            else if (u4MirrorCfg == ISS_MIRR_SES_DEL)
            {
                i4SessionIndex = (INT4) CLI_PTR_TO_U4 (args[1]);
                i4RetStatus = CliSetIssNoMirror (CliHandle, i4SessionIndex);
            }
            break;

        case CLI_ISS_SHOW_MIRR:

            u4MirrorCfg = CLI_PTR_TO_U4 (args[0]);
            CliIssShowMirrorStatus (CliHandle, i4SessionStatus,
                                    ISS_MIRR_SHOW_DETAIL);
            nmhGetIssMirrorStatus (&i4SessionStatus);
            if (i4SessionStatus != ISS_MIRRORDISABLE)
            {
                if (u4MirrorCfg == ISS_MIRR_SHOW_ALL)
                {

                    for (i4SessionIndex = 1;
                         i4SessionIndex <= ISS_MIRR_MAX_SESSIONS;
                         i4SessionIndex++)
                    {

                        CliIssShowMirrorSessionDetail (CliHandle,
                                                       i4SessionIndex);
                    }

                }
                else if (u4MirrorCfg == ISS_MIRR_SHOW_SESSION)
                {
                    i4SessionIndex = (INT4) CLI_PTR_TO_U4 (args[1]);
                    i4RetStatus = CliIssShowMirrorSessionDetail (CliHandle,
                                                                 i4SessionIndex);
                    if (i4RetStatus == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "%% Session %d does "
                                   "not exist \r\n", i4SessionIndex);
                    }

                }

                else if (u4MirrorCfg == ISS_MIRR_SHOW_RANGE)
                {
                    CLI_MEMSET (au1MirrorSesList, 0, ISS_MIRR_LIST_SIZE);

                    if (CliGetRange
                        ((UINT1 *) args[1], &u4StartIndex,
                         &u4EndIndex) == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "%% Incorrect range specified\r\n");
                        break;
                    }
                    if (u4EndIndex > ISS_MIRR_MAX_SESSIONS)
                    {
                        CliPrintf (CliHandle,
                                   "%% Monitor session range must be in between (1-20)\r\n");
                        break;
                    }

                    if (CliStrToPortList ((UINT1 *) args[1], au1MirrorSesList,
                                          ISS_MIRR_LIST_SIZE,
                                          ISS_MIRR_INIT_VAL) == CLI_FAILURE)
                    {

                        CliPrintf (CliHandle, "%% Invalid Session List\r\n");
                        break;
                    }

                    for (i4SessionIndex = 1;
                         i4SessionIndex <= ISS_MIRR_MAX_SESSIONS;
                         i4SessionIndex++)
                    {
                        ISS_IS_MEMBER_OF_PORTLIST (au1MirrorSesList,
                                                   i4SessionIndex, u1Status);
                        if (u1Status == ISS_TRUE)
                        {

                            i4RetStatus =
                                CliIssShowMirrorSessionDetail (CliHandle,
                                                               i4SessionIndex);
                            if (i4RetStatus == CLI_FAILURE)
                            {
                                CliPrintf (CliHandle, "%% Session %d does "
                                           "not exist \r\n", i4SessionIndex);
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }

                }
                else if (u4MirrorCfg == ISS_MIRR_SHOW_DETAIL)
                {
                    i4SessionIndex = (INT4) CLI_PTR_TO_U4 (args[1]);

                    i4RetStatus = CliIssShowMirrorSessionDetail (CliHandle,
                                                                 i4SessionIndex);
                    if (i4RetStatus == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "%% Session %d does "
                                   "not exist \r\n", i4SessionIndex);
                    }
                }
                else if (u4MirrorCfg == ISS_MIRR_SHOW_LOCAL)
                {

                    for (i4SessionIndex = 1;
                         i4SessionIndex <= ISS_MIRR_MAX_SESSIONS;
                         i4SessionIndex++)
                    {
                        nmhGetIssMirrorCtrlExtnRSpanStatus (i4SessionIndex,
                                                            &i4RspanStatus);
                        if (i4RspanStatus == ISS_MIRR_RSPAN_DISABLED)
                        {
                            CliIssShowMirrorSessionDetail (CliHandle,
                                                           i4SessionIndex);
                        }
                    }
                }
            }
            break;

        case CLI_ISS_SHOW_CPU_MIRR_DETAILS:
            CliShowIssCpuMirroring (CliHandle, ISS_MIRR_SHOW_DETAIL);
            break;

        case CLI_ISS_SHOW_MIRR_RECORD:

            i4RetStatus = CliIssShowMirrorRecords (CliHandle);
            break;

        case CLI_ISS_LOGIN_AUTH:
            i4RetStatus =
                CliSetIssLoginAuth (CliHandle, CLI_PTR_TO_U4 (args[0]),
                                    CLI_PTR_TO_U4 (args[1]));
            break;

        case CLI_ISS_NO_LOGIN_AUTH:
            i4RetStatus =
                CliSetIssLoginAuth (CliHandle, ISS_LOCAL,
                                    CLI_PTR_TO_U4 (args[1]));
            break;

        case CLI_ISS_IP_AUTH:
            u4IpAddress = *args[0];

            u4Mask = CLI_PTR_TO_U4 (args[1]);

            CLI_MEMSET (au1PortList, 0, ISS_AUTH_PORT_LIST_SIZE);
            CLI_MEMSET (au1VlanList, 0, ISS_VLAN_LIST_SIZE);
            /* build member port list */
            /* Check if fastethernet ports are given */
            if (args[3] != NULL)
            {
                if (CfaCliGetIfList ((INT1 *) args[2], (INT1 *) args[3],
                                     au1PortList, ISS_AUTH_PORT_LIST_SIZE)
                    == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "%% Invalid Port List\r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }

            }

            /* Check if gigabitethernet ports are given */
            if (args[5] != NULL)
            {
                /* Currently all interfaces are represented as ethernet */
                if (CfaCliGetIfList ((INT1 *) args[4], (INT1 *) args[5],
                                     au1PortList, ISS_AUTH_PORT_LIST_SIZE)
                    == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "%% Invalid Port List\r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }
            }

            /* Check if portchannel are given */
            if (args[7] != NULL)
            {
                /*Currently all interfaces are represented as ethernet */
                if (CfaCliGetIfList ((INT1 *) args[6], (INT1 *) args[7],
                                     au1PortList, ISS_AUTH_PORT_LIST_SIZE)
                    == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "%% Invalid Port List\r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }
            }

            if ((args[3] == NULL) && (args[5] == NULL) && (args[7] == NULL))
            {
                IssEnableAllPortsInPortList (au1PortList,
                                             ISS_AUTH_PORT_LIST_SIZE);
            }

            if (args[8] != NULL)
            {
                CLI_MEMSET (au1VlanList, 0, ISS_VLAN_LIST_SIZE);
                if (CliStrToPortList ((UINT1 *) args[8], au1VlanList,
                                      ISS_VLAN_LIST_SIZE,
                                      CFA_L2VLAN) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "%% Invalid Vlan List\r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }
            }
            else
            {
                CLI_MEMSET (au1VlanList, 0xFF, ISS_VLAN_LIST_SIZE - 1);
                /* To set values for the last byte in au1VlanList.
                 * Since max vlan id may vary, to be general this 
                 * function will be used. */
                ISS_LAST_BYTE_IN_PORTLIST (au1VlanList, ISS_MAX_VLAN_ID,
                                           ISS_VLAN_LIST_SIZE);
            }

            /* If `cpu0' is issued in the command, IpAuthManager Over
             * OOB Port will be allowed to access,Else disable IpAuthMgr 
             * over OOB Interface */
            if (args[9] != NULL)
            {
                u1AuthOOBFlag = ISS_TRUE;
                /*If only 'cpu0' is supplied,it means than only OOB
                 * Managment is applicaple for IpAuth Manager.In-Band
                 * Mangement service is disabled */
                if ((args[3] == NULL) && (args[8] == NULL))
                {
                    CLI_MEMSET (au1PortList, 0, ISS_AUTH_PORT_LIST_SIZE);
                    CLI_MEMSET (au1VlanList, 0, ISS_VLAN_LIST_SIZE);
                }
            }

            u4Services = CLI_PTR_TO_U4 (args[10]);

            i4RetStatus = CliIssIpAuth (CliHandle, u4IpAddress,
                                        u4Mask, au1PortList,
                                        au1VlanList, u1AuthOOBFlag, u4Services);
            break;

        case CLI_ISS_NO_IP_AUTH:
            u4IpAddress = *args[0];
            u4Mask = CLI_PTR_TO_U4 (args[1]);

            i4RetStatus = CliIssNoIpAuth (CliHandle, u4IpAddress, u4Mask);
            break;

        case CLI_ISS_HTTP_PORT:
            i4RetStatus = CliIssHttpPort (CliHandle, (INT4) *args[0]);
            break;

        case CLI_ISS_NO_HTTP_PORT:
            i4RetStatus = CliIssHttpPort (CliHandle, ISS_DEF_HTTP_PORT);
            break;

        case CLI_ISS_HTTP_STATUS:

            /* args[0] -  Status Enable/Status Disable */
            i4RetStatus = CliIssHttpStatus (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_ISS_TELNET_STATUS:
            /* args[0] - Status Enable/Status Disable */
            i4RetStatus =
                CliIssTelnetStatus (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_ISS_INCR_SAVE:
            /* args[0] - Incremental Save Flag Enable/Disable */
            i4RetStatus = CliIssConfigIncrSaveFlag (CliHandle,
                                                    CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_ISS_DEFVAL_SAVE:
            /* args[0] - DefVal Save Flag Enable/Disable */
            i4RetStatus = CliIssConfigDefValSaveFlag (CliHandle,
                                                      CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_ISS_DEF_RESTORE_TYPE:
            /* args[0] - Restore Type MSR/CSR */
            i4RetStatus = CliIssSetRestoreType (CliHandle,
                                                CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_ISS_AUTO_SAVE:
            /* args[0] - Auto Save Flag Enable/Disable */
            i4RetStatus = CliIssConfigAutoSaveFlag (CliHandle,
                                                    CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_ISS_AUTO_PORT_CREATE:
            /* args[0] - Auto Port Create Flag Enable/Disable */
            i4RetStatus = CliIssConfigAutoPortCreate (CliHandle,
                                                      CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_ISS_ERASE_CONF:

            if (CLI_PTR_TO_U4 (args[0]) == ISS_FLASH)
            {
                if (CliGetFlashFileName ((INT1 *) args[1],
                                         (INT1 *) au1TempFileName)
                    == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid File Name \r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }
            }
            i4RetStatus =
                CliSetIssErase (CliHandle, CLI_PTR_TO_U4 (args[0]),
                                au1TempFileName);
            break;

        case CLI_ISS_ROLLBACK:
            i4RetStatus =
                CliSetIssRollbackFlag (CliHandle,
                                       (INT4) CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_ISS_SHOW_IP_AUTH:
            if (args[0] == NULL)
            {
                u4IpAddress = 0;
                i4RetStatus = CliIssShowIpAuth (CliHandle, u4IpAddress,
                                                ISS_ALL_ENTRIES);
            }
            else
            {
                u4IpAddress = *args[0];
                i4RetStatus = CliIssShowIpAuth (CliHandle, u4IpAddress,
                                                ISS_SPECIFIC_ENTRY);
            }
            break;

        case CLI_ISS_SHOW_HTTP_STATUS:

            i4RetStatus = CliIssShowHttpStatus (CliHandle);
            break;

        case CLI_ISS_SHOW_TELNET_STATUS:
            i4RetStatus = CliIssShowTelnetStatus (CliHandle);
            break;

        case CLI_ISS_SET_DATE:

            u4Mon = CLI_PTR_TO_U4 (args[2]);
            i4RetStatus = CliIssSetDate (CliHandle, (INT1 *) args[0],
                                         args[1], u4Mon, args[3]);

            break;

        case CLI_ISS_CONFIG_SAVE:

            u4Mode = CLI_PTR_TO_U4 (args[0]);
            if (u4Mode == ISS_FLASH_SAVE)
            {
                if (CliGetFlashFileName ((INT1 *) args[1],
                                         (INT1 *) au1TempFileName)
                    == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid File Name \r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }
                SPRINTF ((CHR1 *) au1FileName, "%s", au1TempFileName);
            }
            else if (u4Mode == ISS_REMOTE_TFTP_SAVE)
            {
                if (CliGetTftpParams ((INT1 *) args[2], (INT1 *) au1FileName,
                                      &IpAddress, (UINT1 *) au1HostName)
                    == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid TFTP Parameters \r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }
            }
            else if (u4Mode == ISS_REMOTE_SFTP_SAVE)
            {
                if (CliGetSftpParams ((INT1 *) args[3], (INT1 *) au1FileName,
                                      (INT1 *) au1DstUserName,
                                      (INT1 *) au1DstPassWd, &IpAddress,
                                      (UINT1 *) au1HostName))
                {
                    CliPrintf (CliHandle, "\r%% Invalid SFTP Parameters \r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }

            }
            i4RetStatus =
                CliIssConfigSave (CliHandle, u4Mode, au1FileName, &IpAddress,
                                  au1HostName, au1DstUserName, au1DstPassWd);
            break;

        case CLI_ISS_COPY_RESTORE:

            u4Type = CLI_PTR_TO_U4 (args[0]);

            if (u4Type == ISS_REMOTE_TFTP_RESTORE)
            {

                /* Copying from TFTP file to iss.conf */

                if (CliGetTftpParams ((INT1 *) args[1], (INT1 *) au1FileName,
                                      &IpAddress, (UINT1 *) au1HostName)
                    == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid TFTP Parameters \r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }
                if (IpAddress.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    PTR_FETCH4 (u4IpAddress, IpAddress.au1Addr);
                    /* Check for self IP address */
                    if (CfaIpIfIsOurAddress (u4IpAddress) == CFA_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% This TFTP operation is not permitted for self IP \r\n");

                        CliUnRegisterLock (CliHandle);

                        ISS_UNLOCK ();
                        return;
                    }
                }

                i4RetStatus = CliIssCopyToStartupFile (CliHandle, u4Type,
                                                       au1FileName,
                                                       IpAddress, au1HostName,
                                                       NULL, NULL);

            }
            else if (u4Type == ISS_REMOTE_SFTP_RESTORE)
            {
                if (CliGetSftpParams ((INT1 *) args[1], (INT1 *) au1FileName,
                                      (INT1 *) au1SrcUserName,
                                      (INT1 *) au1SrcPassWd, &IpAddress,
                                      (UINT1 *) au1HostName) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid SFTP Parameters \r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }
                if (IpAddress.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    PTR_FETCH4 (u4IpAddress, IpAddress.au1Addr);
                    /* Check for self IP address */
                    if (CfaIpIfIsOurAddress (u4IpAddress) == CFA_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% This SFTP operation is not permitted for self IP \r\n");

                        CliUnRegisterLock (CliHandle);

                        ISS_UNLOCK ();
                        return;
                    }
                }

                i4RetStatus = CliIssCopyToStartupFile (CliHandle, u4Type,
                                                       au1FileName,
                                                       IpAddress,
                                                       au1HostName,
                                                       au1SrcUserName,
                                                       au1SrcPassWd);

            }
            else
            {
                /* Copying from FLASH file to iss.conf
                 */

                if (CliGetFlashFileName ((INT1 *) args[1],
                                         (INT1 *) au1FileName) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid File Name \r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }

                i4RetStatus = CliIssCopyToStartupFile (CliHandle, u4Type,
                                                       au1FileName,
                                                       IpAddress, au1HostName,
                                                       NULL, NULL);
            }
            if (i4RetStatus == CLI_SUCCESS)
            {
                mmi_printf ("File Copied Successfully\r\n");
            }
            break;

        case CLI_ISS_COPY_SAVE:
            u4Mode = CLI_PTR_TO_U4 (args[0]);

            if (u4Mode == ISS_REMOTE_TFTP_SAVE)
            {
                /*Copy iss.conf to remote location */

                if (CliGetTftpParams ((INT1 *) args[1], (INT1 *) au1FileName,
                                      &IpAddress, (UINT1 *) au1HostName)
                    == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid TFTP Parameters \r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }
                i4RetStatus = CliIssCopyFromStartupFile (CliHandle, u4Mode,
                                                         au1FileName,
                                                         IpAddress, au1HostName,
                                                         NULL, NULL);
            }
            else if (u4Mode == ISS_REMOTE_SFTP_SAVE)
            {
                if (CliGetSftpParams ((INT1 *) args[1], (INT1 *) au1FileName,
                                      (INT1 *) au1DstUserName,
                                      (INT1 *) au1DstPassWd, &IpAddress,
                                      (UINT1 *) au1HostName) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid SFTP Parameters \r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }

                i4RetStatus = CliIssCopyFromStartupFile (CliHandle, u4Mode,
                                                         au1FileName,
                                                         IpAddress,
                                                         au1HostName,
                                                         au1DstUserName,
                                                         au1DstPassWd);

            }
            else
            {
                /*Copy iss.conf to FLASH file */

                if (CliGetFlashFileName ((INT1 *) args[1],
                                         (INT1 *) au1FileName) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid File Name \r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }

                i4RetStatus = CliIssCopyFromStartupFile (CliHandle, u4Mode,
                                                         au1FileName, IpAddress,
                                                         au1HostName,
                                                         NULL, NULL);
            }
            break;

        case CLI_ISS_COPY_LOGS:
            u4Mode = CLI_PTR_TO_U4 (args[0]);
            if (u4Mode == ISS_TFTP)
            {

                if (CliGetTftpParams ((INT1 *) args[1], (INT1 *) au1FileName,
                                      &IpAddress, (UINT1 *) au1HostName)
                    == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid TFTP Parameters \r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }
            }
            else if (u4Mode == ISS_SFTP)
            {
                if (CliGetSftpParams ((INT1 *) args[1], (INT1 *) au1FileName,
                                      (INT1 *) au1DstUserName,
                                      (INT1 *) au1DstPassWd, &IpAddress,
                                      (UINT1 *) au1HostName) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid SFTP Parameters \r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }
            }

            i4RetStatus = CliIssCopyLogs (CliHandle, au1FileName, IpAddress,
                                          au1HostName, au1DstUserName,
                                          au1DstPassWd, u4Mode,
                                          (UINT1) CLI_PTR_TO_I4 (args[2]));
            break;

        case CLI_ISS_ARCHIVE:
            u4Type = CLI_PTR_TO_U4 (args[0]);
            if (u4Type == ISS_TFTP)
            {

                if (CliGetTftpParams ((INT1 *) args[1], (INT1 *) au1FileName,
                                      &IpAddress, (UINT1 *) au1HostName)
                    == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid TFTP Parameters \r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }
            }
            else if (u4Type == ISS_SFTP)
            {
                if (CliGetSftpParams ((INT1 *) args[1], (INT1 *) au1FileName,
                                      (INT1 *) au1SrcUserName,
                                      (INT1 *) au1SrcPassWd, &IpAddress,
                                      (UINT1 *) au1HostName) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid SFTP Parameters \r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }
            }

            else
            {
                if (CliGetFlashFileName ((INT1 *) args[1],
                                         (INT1 *) au1FileName) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Flash File \r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }
            }
            mmi_printf ("\r Download is in Progress...\r\n");
            i4RetStatus =
                CliIssImageDownload (CliHandle, au1FileName, IpAddress,
                                     au1HostName, au1SrcUserName,
                                     au1SrcPassWd, u4Type);
            break;

        case CLI_ISS_INT_SPEED:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            CliSetIssSpeed (CliHandle, u4IfIndex, CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_ISS_NO_INT_SPEED:
            MEMSET (&pHwPortInfo, 0, sizeof (tHwPortInfo));
            pHwPortInfo.u4StartIfIndex = (UINT4) CLI_GET_IFINDEX ();
            pHwPortInfo.u1MsgType = ISS_NP_GET_DEFAULT_SPEED;
#ifdef NPAPI_WANTED
            CfaNpGetHwPortInfo (&pHwPortInfo);
#endif
            i4RetStatus =
                CliSetIssSpeed (CliHandle, pHwPortInfo.u4StartIfIndex,
                                pHwPortInfo.i4MaxPortSpeed);
            break;

        case CLI_ISS_INT_DUPLEX:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = CliSetIssDuplexity (CliHandle, u4IfIndex,
                                              CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_ISS_NO_INT_DUPLEX:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus =
                CliSetIssDuplexity (CliHandle, u4IfIndex, ISS_FULLDUP);

            break;

        case CLI_ISS_INT_NEGO:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = CliSetIssNegotiation (CliHandle, u4IfIndex, ISS_AUTO);
            break;

        case CLI_ISS_NO_INT_NEGO:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = CliSetIssNegotiation (CliHandle, u4IfIndex,
                                                ISS_NONEGOTIATION);

            break;

        case CLI_ISS_INT_HOL:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = CliSetIssHOlBlockPrevention (CliHandle, u4IfIndex,
                                                       ISS_ENABLE_HOL);
            break;
        case CLI_ISS_NO_INT_HOL:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = CliSetIssHOlBlockPrevention (CliHandle, u4IfIndex,
                                                       ISS_DISABLE_HOL);
            break;

        case CLI_ISS_INT_CPU_LEARN:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = CliSetIssCpuCntrlLearning (CliHandle, u4IfIndex,
                                                     ISS_CPU_CNTRL_LEARN_ENABLE);
            break;

        case CLI_ISS_NO_INT_CPU_LEARN:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = CliSetIssCpuCntrlLearning (CliHandle, u4IfIndex,
                                                     ISS_CPU_CNTRL_LEARN_DISABLE);
            break;

        case CLI_ISS_INT_AUTO_MDI_OR_MDIX:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            CliSetIssMdiOrMdixCap (CliHandle, u4IfIndex, ISS_AUTO_MDIX);
            break;

        case CLI_ISS_NO_INT_AUTO_MDI_OR_MDIX:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            CliSetIssMdiOrMdixCap (CliHandle, u4IfIndex, ISS_MDIX);
            break;

        case CLI_ISS_SET_INT_MDI_OR_MDIX:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            if (CliGetIssMdiOrMdixCap (CliHandle, u4IfIndex, &i4IssPortType) ==
                CLI_SUCCESS && i4IssPortType == ISS_AUTO_MDIX)
            {
                CliPrintf (CliHandle,
                           "\r%% AutoCross is enable. Disable the autocross first. \r\n");
            }
            else
            {
                CliSetIssMdiOrMdixCap (CliHandle, u4IfIndex,
                                       CLI_PTR_TO_I4 (args[0]));
            }
            break;

        case CLI_ISS_PORT_RATE_PAUSE_LIMIT:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = CliSetIssPauseRateLimit (CliHandle, u4IfIndex,
                                                   (INT4) *(args[0]),
                                                   (INT4) *(args[1]));
            break;

        case CLI_ISS_NO_PORT_RATE_PAUSE_LIMIT:
            u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
            i4RetStatus = CliSetIssPauseRateLimit (CliHandle, u4IfIndex, 0, 0);
            break;

        case CLI_ISS_SHOW_SYS_INFO:
            i4RetStatus = CliIssShowSysInfo (CliHandle);
            break;

        case CLI_ISS_SET_SWITCH_THRESHOLD:
            if (args[0] != NULL)
            {
                u4Type = CLI_PTR_TO_U4 (args[0]);
            }
            switch (u4Type)
            {
                case ISS_SET_MAX_RAM:

                    i4RetStatus = CliSetIssSwitchRAMInfo (CliHandle, *args[1]);
                    break;

                case ISS_SET_MAX_CPU:

                    i4RetStatus = CliSetIssSwitchCPUInfo (CliHandle, *args[1]);
                    break;

                case ISS_SET_MAX_FLASH:

                    i4RetStatus = CliSetIssSwitchFlashInfo (CliHandle,
                                                            *args[1]);
                    break;

                default:
                    break;
            }
            break;

        case CLI_ISS_SET_SWITCH_TEMPERATURE:
            u1Status = ISS_TRUE;
            if ((isdigit (*(UINT1 *) args[1]) != 0)
                || (*(UINT1 *) args[1] == '-'))
            {
                u1Status = ISS_FALSE;
                for (u1Index = 1; u1Index < STRLEN ((UINT1 *) args[1]);
                     u1Index++)
                {
                    if (isdigit (*((UINT1 *) args[1] + u1Index)) == 0)
                    {
                        u1Status = ISS_TRUE;
                    }
                }
            }
            if (u1Status == ISS_TRUE)
            {
                CliPrintf (CliHandle,
                           "\r%% Not an valid Temperature value  \r\n");
                CliUnRegisterLock (CliHandle);
                ISS_UNLOCK ();
                return;
            }

            i4RetStatus =
                CliSetIssSwitchTemperatureInfo (CliHandle,
                                                CLI_PTR_TO_U4 (args[0]),
                                                (INT4) CLI_PTR_TO_U4 (CLI_ATOI
                                                                      (args
                                                                       [1])));
            break;

        case CLI_ISS_SET_SWITCH_POWER:

            i4RetStatus =
                CliSetIssSwitchPowerSupplyInfo (CliHandle,
                                                CLI_PTR_TO_U4 (args[0]),
                                                *args[1]);
            break;

        case CLI_ISS_SHOW_SWITCH_INFO:
            if (args[0] != NULL)
            {
                u4Type = CLI_PTR_TO_U4 (args[0]);
            }
            switch (u4Type)
            {
                case ISS_SHOW_ALL:

                    i4RetStatus = CliIssShowAllSwitchInfo (CliHandle);
                    break;

                case ISS_SHOW_TEMPERATURE:

                    i4RetStatus = CliIssShowSwitchTemperature (CliHandle);
                    break;

                case ISS_SHOW_FAN:

                    i4RetStatus = CliIssShowSwitchFanStatus (CliHandle);
                    break;

                case ISS_SHOW_RAM:

                    i4RetStatus = CliIssShowSwitchRAMThreshold (CliHandle);
                    break;

                case ISS_SHOW_CPU:

                    i4RetStatus = CliIssShowSwitchCPUThreshold (CliHandle);
                    break;

                case ISS_SHOW_FLASH:

                    i4RetStatus = CliIssShowSwitchFlashThreshold (CliHandle);
                    break;

                case ISS_SHOW_POWER:

                    i4RetStatus = CliIssShowSwitchPowerStatus (CliHandle);
                    break;

                default:
                    break;
            }
            break;

        case CLI_ISS_SHOW_SYS_ACK:
            i4RetStatus = CliIssShowSysAck (CliHandle);
            break;

        case CLI_ISS_SHOW_NVRAM:
            i4RetStatus = CliIssShowNvRam (CliHandle);
            break;

        case CLI_ISS_SLEEP:
            i4RetStatus = CliIssSleep (*(INT4 *) args[0]);
            break;

        case CLI_ISS_SHOW_DATE:

            i4RetStatus = CliIssShowDate (CliHandle);

            break;
        case CLI_ISS_COPY_GEN:
            if (CLI_PTR_TO_U4 (args[0]) == ISS_FLASH)
            {
                if (CliGetFlashFileName ((INT1 *) args[1],
                                         (INT1 *) au1FileName) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Flash File \r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }
            }
            else if (CLI_PTR_TO_U4 (args[0]) == ISS_TFTP)
            {

                if (CliGetTftpParams ((INT1 *) args[1], (INT1 *) au1FileName,
                                      &IpAddress, (UINT1 *) au1HostName)
                    == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid TFTP Parameters \r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }
                /* Check for self IP address */
                if (IpAddress.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    PTR_FETCH4 (u4SrcIpAddress, IpAddress.au1Addr);
                    if (CfaIpIfIsOurAddress (u4SrcIpAddress) == CFA_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% This TFTP operation is not permitted for self IP  \r\n");

                        CliUnRegisterLock (CliHandle);

                        ISS_UNLOCK ();
                        return;
                    }
                }
#ifdef IP6_WANTED
                else
                {
                    MEMCPY (&Ip6Addr, IpAddress.au1Addr, sizeof (tIp6Addr));
                    if (NetIpv6IsOurAddress (&Ip6Addr, &u4Val) ==
                        NETIPV6_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% This TFTP operation is not permitted for self IP  \r\n");

                        CliUnRegisterLock (CliHandle);

                        ISS_UNLOCK ();
                        return;
                    }
                }
#endif

            }
            else if (CLI_PTR_TO_U4 (args[0]) == ISS_SFTP)
            {
                if (CliGetSftpParams ((INT1 *) args[1], (INT1 *) au1FileName,
                                      (INT1 *) au1SrcUserName,
                                      (INT1 *) au1SrcPassWd, &IpAddress,
                                      (UINT1 *) au1HostName) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid SFTP Parameters \r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }
                /* Check for self IP address */
                if (IpAddress.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    PTR_FETCH4 (u4SrcIpAddress, IpAddress.au1Addr);
                    if (CfaIpIfIsOurAddress (u4SrcIpAddress) == CFA_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% This SFTP operation is not permitted for self IP  \r\n");

                        CliUnRegisterLock (CliHandle);

                        ISS_UNLOCK ();
                        return;
                    }
                }
#ifdef IP6_WANTED
                else
                {
                    MEMCPY (&Ip6Addr, IpAddress.au1Addr, sizeof (tIp6Addr));
                    if (NetIpv6IsOurAddress (&Ip6Addr, &u4Val) ==
                        NETIPV6_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% This SFTP operation is not permitted for self IP  \r\n");

                        CliUnRegisterLock (CliHandle);

                        ISS_UNLOCK ();
                        return;
                    }
                }
#endif
            }

            if (CLI_PTR_TO_U4 (args[2]) == ISS_FLASH)
            {
                if (CliGetFlashFileName ((INT1 *) args[3],
                                         (INT1 *) au1TempFileName)
                    == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Flash File \r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }
            }
            else if (CLI_PTR_TO_U4 (args[2]) == ISS_ABSOLUTE_PATH)
            {
                MEMSET (au1TempFileName, 0, sizeof (au1TempFileName));
                STRNCPY ((INT1 *) au1TempFileName, (INT1 *) args[3],
                         sizeof (au1TempFileName) - 1);
            }
            else if (CLI_PTR_TO_U4 (args[2]) == ISS_TFTP)
            {

                if (CliGetTftpParams
                    ((INT1 *) args[3], (INT1 *) au1TempFileName,
                     &DstIpAddress, (UINT1 *) au1DstHostName) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid TFTP Parameters \r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }
                if (DstIpAddress.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    PTR_FETCH4 (u4DstIpAddress, DstIpAddress.au1Addr);
                    /* Check for self IP address */
                    if (CfaIpIfIsOurAddress (u4DstIpAddress) == CFA_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% This TFTP operation is not permitted for self IP  \r\n");

                        CliUnRegisterLock (CliHandle);

                        ISS_UNLOCK ();
                        return;
                    }
                }
#ifdef IP6_WANTED
                else
                {
                    MEMCPY (&Ip6Addr, DstIpAddress.au1Addr, sizeof (tIp6Addr));
                    if (NetIpv6IsOurAddress (&Ip6Addr, &u4Val) ==
                        NETIPV6_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% This SFTP operation is not permitted for self IP  \r\n");

                        CliUnRegisterLock (CliHandle);

                        ISS_UNLOCK ();
                        return;
                    }
                }
#endif
            }
            else if (CLI_PTR_TO_U4 (args[2]) == ISS_SFTP)
            {
                if (CliGetSftpParams
                    ((INT1 *) args[3], (INT1 *) au1TempFileName,
                     (INT1 *) au1DstUserName, (INT1 *) au1DstPassWd,
                     &DstIpAddress, (UINT1 *) au1DstHostName) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid SFTP Parameters \r\n");

                    CliUnRegisterLock (CliHandle);
                    ISS_UNLOCK ();
                    return;
                }
                if (DstIpAddress.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    PTR_FETCH4 (u4DstIpAddress, DstIpAddress.au1Addr);
                    /* Check for self IP address */
                    if (CfaIpIfIsOurAddress (u4DstIpAddress) == CFA_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% This SFTP operation is not permitted for self IP  \r\n");

                        CliUnRegisterLock (CliHandle);

                        ISS_UNLOCK ();
                        return;
                    }
                }
#ifdef IP6_WANTED
                else
                {
                    MEMCPY (&Ip6Addr, DstIpAddress.au1Addr, sizeof (tIp6Addr));
                    if (NetIpv6IsOurAddress (&Ip6Addr, &u4Val) ==
                        NETIPV6_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "\r%% This SFTP operation is not permitted for self IP  \r\n");

                        CliUnRegisterLock (CliHandle);

                        ISS_UNLOCK ();
                        return;
                    }
                }
#endif
            }

            CliModifyMoreFlag (CliHandle, &i1Flag);

            i4RetStatus = IssCopyFileGeneric (CliHandle, au1FileName,
                                              CLI_PTR_TO_U4 (args[0]),
                                              au1SrcUserName, au1SrcPassWd,
                                              IpAddress,
                                              au1HostName,
                                              au1TempFileName,
                                              CLI_PTR_TO_U4 (args[2]),
                                              au1DstUserName, au1DstPassWd,
                                              DstIpAddress, au1DstHostName);

            CliModifyMoreFlag (CliHandle, &i1Flag);

            break;

        case CLI_ISS_FIRMWARE_UPGRADE:

            if (STRSTR (args[0], "tftp") != NULL)
            {
                if (CliGetTftpParams ((INT1 *) args[0], (INT1 *) au1FileName,
                                      &IpAddress, (UINT1 *) au1HostName)
                    == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid TFTP Parameters \r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }
            }
            else if (STRSTR (args[0], "sftp") != NULL)
            {
                if (CliGetSftpParams ((INT1 *) args[0], (INT1 *) au1FileName,
                                      (INT1 *) au1DstUserName,
                                      (INT1 *) au1DstPassWd, &IpAddress,
                                      (UINT1 *) au1HostName) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid SFTP Parameters \r\n");

                    CliUnRegisterLock (CliHandle);

                    ISS_UNLOCK ();
                    return;
                }
            }
            if (STRSTR (args[1], "cust:") != NULL)
            {
                if (CliGetCustFileName
                    ((INT1 *) args[1], (INT1 *) au1DstFileName,
                     (INT1 *) au1CustFilePath) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid File \r\n");
                    CliUnRegisterLock (CliHandle);
                    ISS_UNLOCK ();
                    return;
                }
            }
            else
            {
                if (CliGetFlashFileName
                    ((INT1 *) args[1], (INT1 *) au1DstFileName) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Flash File \r\n");
                    CliUnRegisterLock (CliHandle);
                    ISS_UNLOCK ();
                    return;
                }
            }

            if (STRSTR (args[0], "sftp") != NULL)
            {
                i4RetStatus =
                    CliIssImageDownload (CliHandle, au1FileName, IpAddress,
                                         au1HostName, au1DstUserName,
                                         au1DstPassWd, ISS_SFTP);
            }
            else
            {
                i4RetStatus = IssFirmwareUpgrade (CliHandle, au1FileName,
                                                  IpAddress, au1HostName,
                                                  au1DstFileName,
                                                  au1CustFilePath);
            }

            break;

        case CLI_ISS_SHOW_DBG_LOGGING:
            i4RetStatus = CliIssShowDebugLogging (CliHandle);
            break;

        case CLI_ISS_SHOW_STDBYDBG_LOGGING:
            i4RetStatus = CliIssShowStandbyDebugLogging (CliHandle);
            break;

        case CLI_ISS_SHOW_DEBUGGING:
            i4RetStatus = CliIssShowDebugging (CliHandle);
            break;

        case CLI_ISS_SHOW_RUNNING_CONFIG:

            if (args[0] != NULL)
            {
                u4Module = CLI_PTR_TO_U4 (args[0]);
            }
            if (args[1] != NULL)
            {
                u4Val = CLI_PTR_TO_U4 (args[1]);
            }
            if (args[2] != NULL)
            {
                pu1ContextName = (UINT1 *) args[2];
            }
            if (pu1ContextName != NULL)
            {
                if ((VcmIsSwitchExist (pu1ContextName, &u4ContextId) ==
                     VCM_FALSE)
                    && (VcmIsVrfExist (pu1ContextName, &u4ContextId) ==
                        VCM_FALSE))
                {
                    CliPrintf (CliHandle, "%% Switch %s Does not exist.\r\n",
                               args[2]);

                    break;
                }
            }
            i4RetStatus = CliIssShowRunningConfig (CliHandle, u4Module, u4Val,
                                                   pu1ContextName);
            break;

        case CLI_ISS_SERIAL_CONSOLE_PROMPT:
            i4RetStatus = CliIssSetCliSerialConsolePrompt (CliHandle,
                                                           CLI_PTR_TO_U4
                                                           (args[0]));
            break;
        case CLI_MODULE_SHUTDOWN:
            i4RetStatus = CliIssSetModuleSystemControl (CliHandle,
                                                        CLI_PTR_TO_I4
                                                        (args[0]),
                                                        MODULE_SHUTDOWN);
            break;
        case CLI_MODULE_START:
            i4RetStatus = CliIssSetModuleSystemControl (CliHandle,
                                                        CLI_PTR_TO_I4
                                                        (args[0]),
                                                        MODULE_START);
            break;

        case CLI_ISS_MGMT_PORT_ROUTING:
            i4RetStatus =
                CliIssSetMgmtPortRouting (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_ISS_LOGIN_BLOCK_PARAMS:
            i4RetStatus =
                CliSetIssMaxLoginAttempt (CliHandle, CLI_PTR_TO_U4 (args[0]),
                                          (UINT1) CLI_PTR_TO_U4 (args[1]));
            break;

        case CLI_ISS_MAC_LEARNING_RATE_LIMIT:

            if (args[0] != NULL)
            {
                i4IssMacLearnLimitRate = (INT4) *args[0];
            }
            else
            {
                i4IssMacLearnLimitRate = 1000;    /* Deafult Value */
            }
            if (args[1] != NULL)
            {
                u4IssMacLearnRateLimitInterval = *args[1];
            }
            else
            {
                u4IssMacLearnRateLimitInterval = 1000;
            }

            i4RetStatus =
                CliIssSetMacLearnLimitRate (CliHandle,
                                            i4IssMacLearnLimitRate,
                                            u4IssMacLearnRateLimitInterval);
            break;

        case CLI_ISS_NO_MAC_LEARNING_RATE_LIMIT:

            i4IssMacLearnLimitRate = 0;
            u4IssMacLearnRateLimitInterval = 1000;

            i4RetStatus =
                CliIssSetMacLearnLimitRate (CliHandle,
                                            i4IssMacLearnLimitRate,
                                            u4IssMacLearnRateLimitInterval);
            break;

        case CLI_ISS_SHOW_MAC_LEARN_RATE:

            i4RetStatus = CliIssShowMacLearnLimitRate (CliHandle);

            break;
        case CLI_ISS_ACL_PROVISION_MODE:
            i4RetStatus = CliIssSetAclHwTrigger (CliHandle,
                                                 CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_ISS_ACL_HW_ACTION:
            i4RetStatus = CliIssSetAclHwAction (CliHandle,
                                                ISS_COMMIT_ACTION_TRUE);
            break;

        case CLI_ISS_RM_HB_MODE:
            /* args[0] -  Mode Internal / External */
            i4RetStatus = CliIssSetHeartBeatMode (CliHandle,
                                                  CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_ISS_RM_RTYPE:
            /* args[0] -  RType HOT / Cold */
            i4RetStatus = CliIssSetRmRType (CliHandle, CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_ISS_RM_DTYPE:
            /* args[0] -  DType Shared / Separate */
            i4RetStatus = CliIssSetRmDType (CliHandle, CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_ISS_TRAFFIC_SEPARATION_CONTROL:
            /* args[0] = Traffic control */
            i4RetStatus = CliIssTrafficSeprtnCliSetControl (CliHandle,
                                                            CLI_PTR_TO_I4 (args
                                                                           [0]));
            break;

        case CLI_ISS_VRF_UNQ_MAC_OPTION:
            i4RetStatus =
                CliIssSetVrfUnqMacOption (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_ISS_CONFIG_RESTORE:

            if (CLI_PTR_TO_U4 (args[0]) == ISS_RESTORE_LOCAL)
            {
                CliIssConfigRestore (CliHandle, ISS_RESTORE_LOCAL, 0, NULL);
            }
            else if (CLI_PTR_TO_U4 (args[0]) == ISS_RESTORE_REMOTE)
            {
                MEMCPY (&u4IpAddress, args[1], sizeof (u4IpAddress));

                CliIssConfigRestore (CliHandle, ISS_RESTORE_REMOTE,
                                     u4IpAddress, ((UINT1 *) args[2]));
            }
            else
            {
                CliIssConfigRestore (CliHandle, ISS_CONFIG_NO_RESTORE, 0, NULL);
            }
            break;

        case CLI_ISS_CLEAR_CONFIG:
            i4RetStatus = CliIssClearConfig (CliHandle, ((UINT1 *) args[0]));
            break;
#ifdef ISS_TEST_WANTED
        case CLI_ISS_TEST_CLEAR_CONFIG:
            gi4ClearConfigSupport = ISS_TRUE;
            break;

        case CLI_ISS_DISS_CENTRALIZED_IP:
            if (CLI_PTR_TO_U4 (args[0]) == (UINT4) ISS_TRUE)
            {
                gi4CentralizedIPSupport = ISS_TRUE;
            }
            else
            {
                gi4CentralizedIPSupport = ISS_FALSE;
            }
            break;
#endif

        case CLI_ISS_SSH_CLIENT_SESSION:

            MEMCPY (&au1Session, ISS_SSH_STRING, STRLEN (ISS_SSH_STRING));
            apu1ClientSessionInfo[++u1ArgCount] = (UINT1 *) &au1Session;

            for (u1Index = 0; u1Index < 17; u1Index++)
            {
                if (args[u1Index] != NULL)
                {
                    apu1ClientSessionInfo[++u1ArgCount] =
                        (UINT1 *) args[u1Index];
                }
            }

            ++u1ArgCount;
            /* This function will return only when the client session is terminated
             * So the MGMT_LOCK and ISS_LOCK should be released, so that
             * the other CLI commands from other CLI consoles can be processed*/
            MGMT_UNLOCK ();
            ISS_UNLOCK ();
            i4RetStatus =
                CliIssEstablishClientSession ((UINT1 **) &apu1ClientSessionInfo,
                                              u1ArgCount, ISS_SSH_CLIENT);
            ISS_LOCK ();
            MGMT_LOCK ();
            break;

        case CLI_ISS_TELNET_CLIENT_SESSION:

            MEMCPY (&au1Session, ISS_TELNET_STRING, STRLEN (ISS_TELNET_STRING));
            apu1ClientSessionInfo[++u1ArgCount] = (UINT1 *) &au1Session;
            apu1ClientSessionInfo[++u1ArgCount] = (UINT1 *) args[0];

            if (args[1] != NULL)
            {
                apu1ClientSessionInfo[++u1ArgCount] = (UINT1 *) args[1];
                apu1ClientSessionInfo[++u1ArgCount] = (UINT1 *) args[2];
            }
            ++u1ArgCount;
            /* This function will return only when the client session is terminated
             * So the MGMT_LOCK and ISS_LOCK should be released, so that
             * the other CLI commands from other CLI consoles can be processed*/
            MGMT_UNLOCK ();
            ISS_UNLOCK ();
            i4RetStatus =
                CliIssEstablishClientSession ((UINT1 **) &apu1ClientSessionInfo,
                                              u1ArgCount, ISS_TELNET_CLIENT);
            ISS_LOCK ();
            MGMT_LOCK ();
            break;

        case CLI_ISS_TELNET_CLIENT_STATUS:

            i4RetStatus =
                CliIssSetSshTelnetClientStatus (CLI_PTR_TO_I4 (args[0]),
                                                ISS_TELNET_CLIENT);
            break;

        case CLI_ISS_SSH_CLIENT_STATUS:

            i4RetStatus =
                CliIssSetSshTelnetClientStatus (CLI_PTR_TO_I4 (args[0]),
                                                ISS_SSH_CLIENT);
            break;

        case CLI_ISS_SHOW_SSH_TEL_CLIENT:
            CliIssShowSshTelnetClient (CliHandle,
                                       (UINT1) CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_ISS_WEB_SESSION_TIMEOUT:
            CliIssWebSessionTimeOut (CliHandle, (INT4 *) (args[0]));
            break;

        case CLI_ISS_HEALTH_CHK_CLEAR_COUNTER:
            i4RetStatus = CliIssHealthChkClr (CliHandle, (UINT1 *) (&args[0]));
            break;

        case CLI_ISS_SHOW_HEALTH_CHK_STATUS:
            CliIssShowHealthChkStatus (CliHandle);
            break;

        case CLI_ISS_SHOW_CONFIG_RESTORE_STATUS:
            CliIssShowConfigRestoreStatus (CliHandle);
            break;

        case CLI_ISS_DEF_EXEC_TIMEOUT:
            CliIssDefaultExecTimeOut (CliHandle, (INT4 *) (args[0]));
            break;

        case CLI_ISS_NO_DEF_EXEC_TIMEOUT:
            CliIssNoDefaultExecTimeOut (CliHandle);
            break;

        case CLI_ISS_DEBUG_TRACE:
            CliIssDebugEnable (CliHandle, (UINT1 *) (VOID *) (args[0]));
            break;

        case CLI_ISS_DUMP_MEM_LOC:

            pu1MemLoc = ((UINT1 *) (args[0]));
            u4Len = CLI_PTR_TO_U4 (args[1]);
            length = u4Len;
            u4exeEnd = ((UINT1 *) (args[2]));
            CliPrintf (CliHandle, "\r\n");
            while (u4Len != 0)
            {
                if ((UINT1 *) (pu1MemLoc + u1Index) > u4exeEnd - 1)
                {
                    CliPrintf (CliHandle,
                               "\n You can access location upto %d bytes",
                               length - u4Len);
                    break;

                }
                CliPrintf (CliHandle, "0x%02x\t",
                           *((UINT1 *) (pu1MemLoc + u1Index)));

                if (u1Index == (ISS_MAX_NUM_BYTES - 1))
                {
                    pu1MemLoc = pu1MemLoc + ISS_MAX_NUM_BYTES;
                    u1Index = 0;
                    CliPrintf (CliHandle, "\r\n");
                }
                else
                {
                    u1Index++;
                }
                u4Len--;
            }
            CliPrintf (CliHandle, "\r\n");
            break;

        case CLI_ISS_DEBUG_TIMESTAMP:
            /* args[0] - DefVal Save Flag Enable/Disable */
            CliIssConfigDebugTimeStamp (CliHandle, CLI_PTR_TO_I4 (args[0]));
            break;

        default:
            break;
    }

    if ((CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode >= CLI_ERR_START_ID_ISS) &&
            (u4ErrCode < CLI_ISS_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s",
                       IssCliErrString[CLI_ERR_OFFSET_ISS (u4ErrCode)]);
        }

        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    CliUnRegisterLock (CliHandle);

    ISS_UNLOCK ();
}

/***************************************************************/
/*  Function Name   : CliSetSwitchName                         */
/*  Description     : This function is used to rename a switch */
/*                                                          */
/*  Input(s)        : pu1SwitchName - Pointer to Switch name   */
/*                    CliHandle - CLI Handle                   */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliSetIssSwitchName (tCliHandle CliHandle, UINT1 *pu1SwitchName)
{
    tSNMP_OCTET_STRING_TYPE *pSwitchName = NULL;
    UINT4               u4ErrCode = 0;
    pSwitchName = allocmem_octetstring ((INT4) STRLEN (pu1SwitchName));
    if (pSwitchName == NULL)
    {
        CliPrintf (CliHandle, "%% Unable to allocate memory\r\n");
        return CLI_FAILURE;
    }
    pSwitchName->i4_Length = (INT4) STRLEN (pu1SwitchName);
    STRNCPY (pSwitchName->pu1_OctetList, pu1SwitchName,
             (size_t) pSwitchName->i4_Length);

    if (nmhTestv2IssSwitchName (&u4ErrCode, pSwitchName) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Unable to set switch Name\r\n");
        free_octetstring (pSwitchName);
        return CLI_FAILURE;
    }
    if (nmhSetIssSwitchName (pSwitchName) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        free_octetstring (pSwitchName);
        return (CLI_FAILURE);
    }
    free_octetstring (pSwitchName);
    return CLI_SUCCESS;
}

INT4
CliSetIssHwConsole (tCliHandle CliHandle, UINT1 *pu1HwCmd)
{
    UNUSED_PARAM (CliHandle);

#ifdef NPAPI_WANTED
    if (IssCustAccessHwConsole (pu1HwCmd) == ISS_FAILURE)
    {
        return (CLI_FAILURE);
    }
#else
    UNUSED_PARAM (pu1HwCmd);
#endif
    return (CLI_SUCCESS);
}

INT4
CliConfigHwMode (tCliHandle CliHandle)
{
    if (CliChangePath (CLI_HW_MODE) == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to enter into Hardware configuration mode\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

INT1
CliGetHwCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (CLI_HW_MODE);

    /* check the mode passed is relavent to us and move
     * the name pointer to point to the interface index in it
     */
    if (STRNCMP (pi1ModeName, CLI_HW_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    STRNCPY (pi1DispStr, "(config-hw)#", (MAX_PROMPT_LEN - 1));
    pi1DispStr[(MAX_PROMPT_LEN - 1)] = '\0';
    return TRUE;
}

/***************************************************************/
/*  Function Name   :  CliSetIssDebugFlashLogging              */
/*  Description     : This function is used to enable/disable  */
/*                    debug logging and also specify path      */
/*                    logs will be sent to a file.             */
/*  Input(s)        :                                          */
/*                    CliHandle - CLI Handle                   */
/*                    u4Mode    - Logging mode Console/file    */
/*                    pi1FileName - Logging filename           */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliSetIssDebugFlashLogging (tCliHandle CliHandle, UINT4 u4Mode,
                            UINT1 *pu1FilePath)
{
    UINT4               u4ErrorCode;
    DIR                *pLogDir = NULL;

    pLogDir = opendir ((CHR1 *) pu1FilePath);
    if (pLogDir == NULL)
    {
        if (OsixCreateDir ((const CHR1 *) pu1FilePath) == OSIX_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Error in creation of LogDir \r\n");
            return (CLI_FAILURE);
        }
    }
    else
    {
        closedir (pLogDir);
    }
    if (nmhTestv2IssLoggingOption (&u4ErrorCode, (INT4) u4Mode) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Test flash logging mode failed \r\n");
        return (CLI_FAILURE);
    }
    if (nmhSetIssLoggingOption (u4Mode) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Set flash logging mode failed \r\n");
        return (CLI_FAILURE);
    }
    if (IssSetFlashLoggingLocation (pu1FilePath) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Set flash logging path %s failed \r\n",
                   pu1FilePath);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliSetIssDefaultIpAddrCfgMode            */
/*  Description     : This function sets the mode in which the */
/*                    default interface obtains the IP address */
/*  Input(s)        :                                          */
/*                    CliHandle - CLI Handle                   */
/*                    u4Mode    - IP address mode              */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliSetIssDefaultIpAddrCfgMode (tCliHandle CliHandle, UINT4 u4Mode)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2IssDefaultIpAddrCfgMode (&u4ErrorCode, (INT4) u4Mode) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIssDefaultIpAddrCfgMode (u4Mode) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);

    }

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliSetIssDefaultRestoreFile              */
/*  Description     : This function sets the default           */
/*                    restoration file name with full path     */
/*  Input(s)        :                                          */
/*                    CliHandle - CLI Handle                   */
/*                    pu1FileName - Restore file name with path*/
/*  Output(s)       : Error message - on failure               */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliSetIssDefaultRestoreFile (tCliHandle CliHandle, UINT1 *pu1FileName)
{
    UINT4               u4ErrorCode;
    UINT1               au1RestoreFileName[ISS_CONFIG_FILE_NAME_LEN];
    tSNMP_OCTET_STRING_TYPE RestoreFileName;

    RestoreFileName.i4_Length = (INT4) STRLEN (pu1FileName);
    RestoreFileName.pu1_OctetList = au1RestoreFileName;

    MEMSET (au1RestoreFileName, 0, ISS_CONFIG_FILE_NAME_LEN);
    MEMCPY (RestoreFileName.pu1_OctetList, pu1FileName,
            RestoreFileName.i4_Length);

    if (nmhTestv2IssConfigRestoreFileName (&u4ErrorCode, &RestoreFileName) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Error in restore file name \r\n");
        return (CLI_FAILURE);
    }
    if (nmhSetIssConfigRestoreFileName (&RestoreFileName) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliSetIssDefaultIpAddrAllocProto         */
/*  Description     : This function sets the protocol in which */
/*                    default interface obtains the IP address */
/*  Input(s)        :                                          */
/*                    CliHandle - CLI Handle                   */
/*                    u4Protocol- BOOTP/RARP/DHCP              */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliSetIssDefaultIpAddrAllocProto (tCliHandle CliHandle, UINT4 u4Protocol)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2IssDefaultIpAddrAllocProtocol (&u4ErrorCode,
                                                (INT4) u4Protocol) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIssDefaultIpAddrAllocProtocol (u4Protocol) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);

    }

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliSetIssDefaultIpAddress                */
/*  Description     : This function sets the default IP address*/
/*                    in case the start-up mode is manual      */
/*  Input(s)        :                                          */
/*                    CliHandle      - CLI Handle              */
/*                    u4IpAddress    - IP address              */
/*                    u4Mask         - Subnet Mask             */
/*                    u4IfIndex      - Interface Index         */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliSetIssDefaultIpAddress (tCliHandle CliHandle, UINT4 u4IpAddress,
                           UINT4 u4Mask, UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE Alias;
    tCfaIfInfo          IfInfo;
    UINT1               au1Temp[CFA_MAX_PORT_NAME_LENGTH];

    MEMSET (au1Temp, 0, CFA_MAX_PORT_NAME_LENGTH);

    Alias.pu1_OctetList = &au1Temp[0];

    /* Choose the appropriate mask based on the IP address as
     * the user has not given a mask
     */

    if (u4Mask == 0)
    {

        if (ISS_IS_ADDR_CLASS_A (u4IpAddress))
        {
            u4Mask = 0xff000000;

        }
        else if (ISS_IS_ADDR_CLASS_B (u4IpAddress))
        {
            u4Mask = 0xffff0000;
        }
        else if (ISS_IS_ADDR_CLASS_C (u4IpAddress))
        {
            u4Mask = 0xffffff00;
        }
    }

    /* Check whether the mask is valid against the given IP address . The host
     * part of the IP address should be not zero and should not be broadcast 
     * address
     */

    if (u4Mask != 0xffffffff)
    {
        if ((u4IpAddress & ~u4Mask) == 0)

        {
            CliPrintf (CliHandle, "\r%% Invalid IP Address \r\n");
            return CLI_FAILURE;
        }

        if ((u4IpAddress | ~u4Mask) == u4IpAddress)

        {
            CliPrintf (CliHandle, "\r%% Invalid Mask for this IP Address \r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2IssDefaultIpAddr (&u4ErrorCode, u4IpAddress) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2IssDefaultIpSubnetMask (&u4ErrorCode, u4Mask) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (u4IfIndex != 0)
    {
        if (CfaGetIfInfo ((UINT2) u4IfIndex, &IfInfo) == CFA_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to obtain the Interface Name\r\n");

            return CLI_FAILURE;
        }

        Alias.i4_Length = (INT4) STRLEN (IfInfo.au1IfName);
        MEMCPY (Alias.pu1_OctetList, IfInfo.au1IfName,
                MEM_MAX_BYTES (Alias.i4_Length, CFA_MAX_PORT_NAME_LENGTH));

        if (nmhTestv2IssDefaultInterface (&u4ErrorCode, &Alias) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

    }

    if (nmhSetIssDefaultIpAddr (u4IpAddress) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (nmhSetIssDefaultIpSubnetMask (u4Mask) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (u4IfIndex != 0)
    {
        if (nmhSetIssDefaultInterface (&Alias) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }

    UNUSED_PARAM (u4IfIndex);

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliSetIssDefaultRmInit                   */
/*  Description     : This function sets the default RM        */
/*                    Interface.                               */
/*  Input(s)        :                                          */
/*                    CliHandle      - CLI Handle              */
/*                    pu1RmIfName    - RM Interface Name       */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliSetIssDefaultRmInit (tCliHandle CliHandle, UINT1 *pu1RmIfName)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE InterfaceName;

    InterfaceName.pu1_OctetList = pu1RmIfName;
    InterfaceName.i4_Length = CFA_MAX_PORT_NAME_LENGTH;

    if (nmhTestv2IssDefaultRmIfName (&u4ErrorCode, &InterfaceName) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssDefaultRmIfName (&InterfaceName) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/***************************************************************/
/*  Function Name   : CliSetIssDefaultRmInterfaceType          */
/*                                                             */
/*  Description     : This function sets the default RM        */
/*                    Interface Type.                          */
/*                                                             */
/*  Input(s)        : CliHandle          - CLI Handle          */
/*                    i4RmInterfaceType  - RM Interface Type   */
/*                                                             */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliSetIssDefaultRmInterfaceType (tCliHandle CliHandle, INT4 i4RmInterfaceType)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2IssRmStackingInterfaceType (&u4ErrorCode, i4RmInterfaceType) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_ISS_INVALID_RM_INTERFACE_TYPE);
        return (CLI_FAILURE);
    }

    if (nmhSetIssRmStackingInterfaceType (i4RmInterfaceType) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/***************************************************************/
/*  Description     : This function sets the base MAC address  */
/*                    of the switch                            */
/*  Input(s)        :                                          */
/*                    CliHandle     - CLI Handle               */
/*                    pu1BaseMac    - Base MAC address         */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliSetIssBaseMacAddr (tCliHandle CliHandle, tMacAddr BaseMac)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2IssSwitchBaseMacAddress (&u4ErrorCode, BaseMac) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhSetIssSwitchBaseMacAddress (BaseMac) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/***************************************************************/
/*  Function Name   : CliSetIssSwitchRAMInfo                   */
/*  Description     : This function sets the max RAM threshold */
/*                    of the switch.                           */
/*  Input(s)        : CliHandle     - CLI Handle               */
/*                    u4RAMUsage    - Maximum RAM threshold    */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliSetIssSwitchRAMInfo (tCliHandle CliHandle, UINT4 u4RAMUsage)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2IssSwitchMaxRAMUsage (&u4ErrorCode, (INT4) u4RAMUsage)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    nmhSetIssSwitchMaxRAMUsage ((INT4) u4RAMUsage);

    return (CLI_SUCCESS);
}

INT4
CliSetIssMirrorStatus (tCliHandle CliHandle, INT4 i4MirrorStatus)
{
    UINT4               u4ErrorCode = 0;

    if (SNMP_SUCCESS != nmhTestv2IssMirrorStatus (&u4ErrorCode, i4MirrorStatus))
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    if (SNMP_SUCCESS != nmhSetIssMirrorStatus (i4MirrorStatus))
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/***************************************************************/
/*  Function Name   : CliSetIssMirrorSpanSource                */
/*  Description     : This function is used to configure source*/
/*                    entities other than vlan(ports,tunnels,  */
/*                    port channel,acl) for a session.         */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4SessionIndex - Session Index           */
/*                    u4SourceId   - Source entity Id          */
/*                    i4MirrorType - Mirroring Type            */
/*                    u4MirrorMode - Mode(Ingress/Egress/Both) */
/*                    u4SourceCfg  - Configuration(Add/Delete) */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliSetIssMirrorSpanSource (tCliHandle CliHandle, INT4 i4SessionIndex,
                           INT4 i4MirrorType, UINT4 u4SourceId,
                           UINT4 u4MirrorMode, UINT4 u4SrcCfg)
{

    INT4                i4Status = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4TempMode = 0;
    INT4                i4TempMirrorType = ISS_MIRR_INVALID;
    INT4                i4Direction = 0;

    if (nmhGetIssMirrorCtrlExtnStatus (i4SessionIndex,
                                       &i4Status) == SNMP_FAILURE)
    {
        if (u4SrcCfg == ISS_MIRR_ADD)
        {
            nmhTestv2IssMirrorCtrlExtnStatus (&u4ErrorCode,
                                              i4SessionIndex,
                                              ISS_CREATE_AND_WAIT);

            if (nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex,
                                               ISS_CREATE_AND_WAIT) ==
                SNMP_FAILURE)
            {
                ISSCliCheckAndThrowFatalError (CliHandle);
                return (CLI_FAILURE);
            }
        }
        else
        {
            CliPrintf (CliHandle, "%% Session %d does not exist\r\n",
                       i4SessionIndex);
            return CLI_FAILURE;
        }
    }
    else
    {
        /* When, the Initially configured "type" of mirroring(Say port based mirroring) is 
         * over-written with other type (Say Vlan or ACL based mirroring),
         * It should remove the previous mirroring entry and add the new type to the
         * data-structure. In-order-to do this, fetch the Mirroring type of the existing
         * Mirroring session*/

        nmhGetIssMirrorCtrlExtnMirrType (i4SessionIndex, &i4TempMirrorType);
    }
    /* Egress ACL Mirroring is not supported in H/W.Blocking H/W set in Control plane
     * itself */
    if (i4MirrorType == ISS_MIRR_MAC_FLOW_BASED)
    {
        nmhGetIssExtL2FilterDirection ((INT4) u4SourceId, &i4Direction);
        if (i4Direction == ACL_ACCESS_OUT)
        {
            CLI_SET_ERR (CLI_MIRR_EGRESS_ACL_UNSUPPORTED);
            return (CLI_FAILURE);
        }
    }
    else if (i4MirrorType == ISS_MIRR_IP_FLOW_BASED)
    {
        nmhGetIssExtL3FilterDirection ((INT4) u4SourceId, &i4Direction);
        if (i4Direction == ACL_ACCESS_OUT)
        {
            CLI_SET_ERR (CLI_MIRR_EGRESS_ACL_UNSUPPORTED);
            return (CLI_FAILURE);
        }
    }

    nmhGetIssMirrorCtrlExtnSrcMode (i4SessionIndex, (INT4) u4SourceId,
                                    &i4TempMode);
    if (u4SrcCfg == ISS_MIRR_DELETE)
    {
        /* if user tries to remove a source port of un configured direction 
         * error should be thrown */
        if ((u4MirrorMode != (UINT4) i4TempMode)
            && (u4MirrorMode != ISS_MIRR_BOTH)
            && ((UINT4) i4TempMode) != ISS_MIRR_BOTH)
        {
            if (i4MirrorType == ISS_MIRR_PORT_BASED)
            {
                if (u4MirrorMode == ISS_MIRR_INGRESS)
                {
                    CliPrintf (CliHandle, "%% Rx Mode does not exist for the"
                               " source port\r\n");
                }
                else
                {
                    CliPrintf (CliHandle, "%% Tx Mode does not exist for the"
                               " source port\r\n");
                }
            }
            else
            {
                CliPrintf (CliHandle,
                           "%% ACL does not exist as mirror source. \r\n");
                return CLI_FAILURE;
            }
        }
    }
    nmhTestv2IssMirrorCtrlExtnStatus (&u4ErrorCode,
                                      i4SessionIndex, ISS_NOT_IN_SERVICE);

    if (nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex,
                                       ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        ISSCliCheckAndThrowFatalError (CliHandle);
        return (CLI_FAILURE);
    }
    /* Check if the type(Port/VLan/FLow) of the Mirroring-session's existing source
     * is same as the type(Port/VLan/FLow) of new source. If it's not same.
     * remove the old source and add the new source. */

    if ((i4TempMirrorType != i4MirrorType) && (u4SrcCfg == ISS_MIRR_ADD) &&
        (i4TempMirrorType != ISS_MIRR_INVALID))
    {
        nmhSetIssMirrorCtrlExtnSrcCfg (i4SessionIndex, u4SourceId,
                                       ISS_MIRR_DELETE);
        IssRemoveMirrorCtrlEntry (&u4ErrorCode, i4SessionIndex, u4SourceId,
                                  u4MirrorMode);
        nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ISS_DESTROY);
        if (nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ISS_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            ISSCliCheckAndThrowFatalError (CliHandle);
            return (CLI_FAILURE);
        }
    }

    nmhTestv2IssMirrorCtrlExtnMirrType (&u4ErrorCode,
                                        i4SessionIndex, i4MirrorType);

    if (nmhSetIssMirrorCtrlExtnMirrType (i4SessionIndex,
                                         i4MirrorType) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssMirrorCtrlExtnSrcCfg (&u4ErrorCode,
                                          i4SessionIndex, (INT4) u4SourceId,
                                          (INT4) u4SrcCfg) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIssMirrorCtrlExtnSrcCfg (i4SessionIndex, u4SourceId,
                                       u4SrcCfg) == SNMP_FAILURE)
    {
        ISSCliCheckAndThrowFatalError (CliHandle);
        return (CLI_FAILURE);
    }

    if (u4SrcCfg == ISS_MIRR_ADD)
    {
        if (nmhTestv2IssMirrorCtrlExtnSrcMode (&u4ErrorCode,
                                               i4SessionIndex,
                                               (INT4) u4SourceId,
                                               (INT4) u4MirrorMode) ==
            SNMP_FAILURE)
        {
            nmhSetIssMirrorCtrlExtnSrcCfg (i4SessionIndex, u4SourceId,
                                           ISS_MIRR_DELETE);
            return (CLI_FAILURE);
        }

        if (nmhSetIssMirrorCtrlExtnSrcMode (i4SessionIndex, u4SourceId,
                                            u4MirrorMode) == SNMP_FAILURE)
        {
            nmhSetIssMirrorCtrlExtnSrcCfg (i4SessionIndex, u4SourceId,
                                           ISS_MIRR_DELETE);
            ISSCliCheckAndThrowFatalError (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhTestv2IssMirrorCtrlExtnStatus (&u4ErrorCode,
                                              i4SessionIndex,
                                              ISS_ACTIVE) == SNMP_FAILURE)
        {
            return (CLI_SUCCESS);
        }
        if (nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex,
                                           ISS_ACTIVE) == SNMP_FAILURE)
        {
            nmhSetIssMirrorCtrlExtnSrcCfg (i4SessionIndex, u4SourceId,
                                           ISS_MIRR_DELETE);

            if (nmhTestv2IssMirrorCtrlExtnStatus (&u4ErrorCode,
                                                  i4SessionIndex,
                                                  ISS_ACTIVE) == SNMP_SUCCESS)
            {
                nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ISS_ACTIVE);
            }
            ISSCliCheckAndThrowFatalError (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else
    {
        if (IssRemoveMirrorCtrlEntry
            (&u4ErrorCode, i4SessionIndex, u4SourceId,
             u4MirrorMode) == CLI_FAILURE)
        {
            ISSCliCheckAndThrowFatalError (CliHandle);
            return (CLI_FAILURE);
        }
    }
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IssRemoveMirrorCtrlEntry                 */
/*  Description     : This function is used to remove the      */
/*                    mirror from Port/VLan/Flow(ACL)          */
/*  Input(s)        :                                          */
/*                    u4SessionIndex - Session Index           */
/*                    u4SourceId   - Source entity Id          */
/*                    u4MirrorMode - Mode(Ingress/Egress/Both) */
/*  Output(s)       : pu4ErrorCode                             */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
IssRemoveMirrorCtrlEntry (UINT4 *pu4ErrorCode, INT4 i4SessionIndex,
                          UINT4 u4SourceId, UINT4 u4MirrorMode)
{
    INT4                i4TempMode = 0;
    nmhGetIssMirrorCtrlExtnSrcMode (i4SessionIndex, (INT4) u4SourceId,
                                    &i4TempMode);

    if (nmhTestv2IssMirrorCtrlExtnSrcMode (pu4ErrorCode,
                                           i4SessionIndex,
                                           (INT4) u4SourceId,
                                           ISS_MIRR_DISABLED) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if ((u4MirrorMode == (UINT4) i4TempMode) || (u4MirrorMode == ISS_MIRR_BOTH))
    {
        if (nmhSetIssMirrorCtrlExtnSrcMode (i4SessionIndex, u4SourceId,
                                            ISS_MIRR_DISABLED) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }
    else if ((u4MirrorMode == ISS_MIRR_INGRESS)
             && ((UINT4) i4TempMode == ISS_MIRR_BOTH))
    {
        if (nmhSetIssMirrorCtrlExtnSrcMode (i4SessionIndex, u4SourceId,
                                            ISS_MIRR_EGRESS) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }
    else if ((u4MirrorMode == ISS_MIRR_EGRESS)
             && ((UINT4) i4TempMode == ISS_MIRR_BOTH))
    {
        if (nmhSetIssMirrorCtrlExtnSrcMode (i4SessionIndex, u4SourceId,
                                            ISS_MIRR_INGRESS) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    if (nmhTestv2IssMirrorCtrlExtnStatus (pu4ErrorCode,
                                          i4SessionIndex,
                                          ISS_ACTIVE) == SNMP_FAILURE)
    {
        return (CLI_SUCCESS);
    }
    if (nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex,
                                       ISS_ACTIVE) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliSetIssMirrorSpanSourceVlan            */
/*  Description     : This function is used to configure both  */
/*                    mirror port and monitor port in the      */
/*                    the switch. It also enables mirroring    */
/*                    automatically. This function is also     */
/*                    used to disable mirroring                */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4MirrorCfg - Mirror Port/Monitor Port   */
/*                    u4IfIndex   - Interface index to be used */
/*                    u4Dir       - Ingress/Egress/Both        */
/*                    u4Flag      - Mirror Enable/Disable      */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliSetIssMirrorSpanSourceVlan (tCliHandle CliHandle, INT4 i4SessionIndex,
                               INT4 i4MirrorType, UINT4 u4SourceVlanId,
                               UINT4 u4ContextId, UINT4 u4MirrorMode,
                               UINT4 u4SrcCfg)
{

    INT4                i4Status = 0;
    UINT4               u4ErrorCode = 0;

    if (nmhGetIssMirrorCtrlExtnStatus (i4SessionIndex,
                                       &i4Status) == SNMP_FAILURE)
    {
        if (u4SrcCfg == ISS_MIRR_ADD)
        {
            nmhTestv2IssMirrorCtrlExtnStatus (&u4ErrorCode,
                                              i4SessionIndex,
                                              ISS_CREATE_AND_WAIT);

            if (nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex,
                                               ISS_CREATE_AND_WAIT) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }
        else
        {
            CliPrintf (CliHandle, "%% Session %d does not exist\r\n",
                       i4SessionIndex);
            return CLI_FAILURE;
        }
    }

    nmhTestv2IssMirrorCtrlExtnStatus (&u4ErrorCode,
                                      i4SessionIndex, ISS_NOT_IN_SERVICE);

    if (nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex,
                                       ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    nmhTestv2IssMirrorCtrlExtnMirrType
        (&u4ErrorCode, i4SessionIndex, i4MirrorType);

    if (nmhSetIssMirrorCtrlExtnMirrType (i4SessionIndex,
                                         i4MirrorType) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssMirrorCtrlExtnSrcVlanCfg (&u4ErrorCode,
                                              i4SessionIndex,
                                              (INT4) u4ContextId,
                                              (INT4) u4SourceVlanId,
                                              (INT4) u4SrcCfg) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (nmhSetIssMirrorCtrlExtnSrcVlanCfg (i4SessionIndex,
                                           u4ContextId, u4SourceVlanId,
                                           u4SrcCfg) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (u4SrcCfg == ISS_MIRR_ADD)
    {

        if (nmhTestv2IssMirrorCtrlExtnSrcVlanMode (&u4ErrorCode,
                                                   i4SessionIndex,
                                                   (INT4) u4ContextId,
                                                   (INT4) u4SourceVlanId,
                                                   (INT4) u4MirrorMode) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhSetIssMirrorCtrlExtnSrcVlanMode (i4SessionIndex,
                                                u4ContextId, u4SourceVlanId,
                                                u4MirrorMode) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        if (nmhTestv2IssMirrorCtrlExtnStatus (&u4ErrorCode,
                                              i4SessionIndex,
                                              ISS_ACTIVE) == SNMP_FAILURE)
        {
            return (CLI_SUCCESS);
        }
        if (nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex,
                                           ISS_ACTIVE) == SNMP_FAILURE)
        {
            if (nmhSetIssMirrorCtrlExtnSrcVlanCfg (i4SessionIndex,
                                                   u4ContextId, u4SourceVlanId,
                                                   u4SrcCfg) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
            if (nmhTestv2IssMirrorCtrlExtnStatus (&u4ErrorCode,
                                                  i4SessionIndex,
                                                  ISS_ACTIVE) == SNMP_SUCCESS)
            {
                nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ISS_ACTIVE);
            }

            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliSetIssMirrorSpanDestination           */
/*  Description     : This function is used to configure both  */
/*                    mirror port and monitor port in the      */
/*                    the switch. It also enables mirroring    */
/*                    automatically. This function is also     */
/*                    used to disable mirroring                */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4MirrorCfg - Mirror Port/Monitor Port   */
/*                    u4IfIndex   - Interface index to be used */
/*                    u4Dir       - Ingress/Egress/Both        */
/*                    u4Flag      - Mirror Enable/Disable      */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliSetIssMirrorSpanDestination (tCliHandle CliHandle, INT4 i4SessionIndex,
                                UINT4 u4IfIndex, UINT4 u4DestCfg)
{

    INT4                i4Status = 0;
    UINT4               u4NextMirrorDestId = 0;
    UINT4               u4DestId = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4NewRow = OSIX_FALSE;

    if (nmhGetIssMirrorCtrlExtnStatus (i4SessionIndex,
                                       &i4Status) == SNMP_FAILURE)
    {
        if (u4DestCfg == ISS_MIRR_ADD)
        {
            nmhTestv2IssMirrorCtrlExtnStatus (&u4ErrorCode,
                                              i4SessionIndex,
                                              ISS_CREATE_AND_WAIT);

            if (nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex,
                                               ISS_CREATE_AND_WAIT) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_MIRR_INVALID_CONF_SESSION);
                return (CLI_FAILURE);
            }
            u4NewRow = OSIX_TRUE;
        }
        else
        {
            CliPrintf (CliHandle, "%% Session %d does not exist\r\n",
                       i4SessionIndex);
            return CLI_FAILURE;
        }
    }

    if (u4DestCfg == ISS_MIRR_ADD)
    {
        if (IssMirrGetNextDestRecrd
            ((UINT4) i4SessionIndex, u4DestId,
             &u4NextMirrorDestId) != SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%% Configuration not allowed. Destination port is already confiigured\r\n");
            return CLI_FAILURE;
        }
    }

    nmhTestv2IssMirrorCtrlExtnStatus (&u4ErrorCode,
                                      i4SessionIndex, ISS_NOT_IN_SERVICE);

    if (nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex,
                                       ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_MIRR_INVALID_CONF_SESSION);
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssMirrorCtrlExtnDestCfg (&u4ErrorCode,
                                           i4SessionIndex, (INT4) u4IfIndex,
                                           (INT4) u4DestCfg) == SNMP_FAILURE)
    {
        if (u4NewRow == OSIX_TRUE)
        {
            nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ISS_DESTROY);
        }
        return (CLI_FAILURE);
    }
    if (nmhSetIssMirrorCtrlExtnDestCfg (i4SessionIndex,
                                        u4IfIndex, u4DestCfg) == SNMP_FAILURE)
    {
        if (u4NewRow != OSIX_TRUE)
        {

            if (nmhTestv2IssMirrorCtrlExtnStatus (&u4ErrorCode,
                                                  i4SessionIndex,
                                                  ISS_ACTIVE) == SNMP_SUCCESS)
            {

                nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ISS_ACTIVE);
            }
        }
        else
        {
            nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ISS_DESTROY);
        }

        CLI_SET_ERR (CLI_MIRR_INVALID_CONF_SESSION);
        return (CLI_FAILURE);
    }

    if (nmhTestv2IssMirrorCtrlExtnStatus (&u4ErrorCode,
                                          i4SessionIndex,
                                          ISS_ACTIVE) == SNMP_FAILURE)
    {
        return (CLI_SUCCESS);
    }
    if (nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ISS_ACTIVE)
        == SNMP_FAILURE)
    {
        if (u4NewRow != OSIX_TRUE)
        {
            if (u4DestCfg == ISS_MIRR_ADD)
            {
                /* reverting failed destination configuration */
                nmhSetIssMirrorCtrlExtnDestCfg (i4SessionIndex,
                                                u4IfIndex, ISS_MIRR_DELETE);
                nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ISS_DESTROY);
            }
        }
        else
        {
            nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ISS_DESTROY);
        }

        CLI_GET_ERR (&u4ErrorCode);
        if (u4ErrorCode != CLI_MIRR_MORE_THAN_ONE_DEST_PER_SESSION)
        {
            CLI_SET_ERR (CLI_MIRR_INVALID_CONF_SESSION);
        }
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliSetIssMirrorRspan                     */
/*  Description     : This function is used to configure both  */
/*                    mirror port and monitor port in the      */
/*                    the switch. It also enables mirroring    */
/*                    automatically. This function is also     */
/*                    used to disable mirroring                */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4MirrorCfg - Mirror Port/Monitor Port   */
/*                    u4IfIndex   - Interface index to be used */
/*                    u4Dir       - Ingress/Egress/Both        */
/*                    u4Flag      - Mirror Enable/Disable      */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliSetIssMirrorRspan (tCliHandle CliHandle, INT4 i4SessionIndex,
                      UINT4 u4RspanStatus, UINT4 u4RspanVlanId,
                      UINT4 u4ContextId, UINT4 u4MirrorCfg)
{

    INT4                i4Status = 0;
    INT4                i4CurrRspanStatus = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4CurrRspanVlanId = 0;

    if (u4MirrorCfg == ISS_MIRR_ADD)
    {
        if (nmhGetIssMirrorCtrlExtnStatus (i4SessionIndex,
                                           &i4Status) == SNMP_FAILURE)
        {
            nmhTestv2IssMirrorCtrlExtnStatus (&u4ErrorCode,
                                              i4SessionIndex,
                                              ISS_CREATE_AND_WAIT);

            if (nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex,
                                               ISS_CREATE_AND_WAIT) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }
        nmhTestv2IssMirrorCtrlExtnStatus (&u4ErrorCode,
                                          i4SessionIndex, ISS_NOT_IN_SERVICE);

        if (nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex,
                                           ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        if (nmhTestv2IssMirrorCtrlExtnRSpanStatus (&u4ErrorCode,
                                                   i4SessionIndex,
                                                   (INT4) u4RspanStatus) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhSetIssMirrorCtrlExtnRSpanStatus (i4SessionIndex,
                                                u4RspanStatus) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhTestv2IssMirrorCtrlExtnRSpanVlanId (&u4ErrorCode,
                                                   i4SessionIndex,
                                                   (INT4) u4RspanVlanId) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhSetIssMirrorCtrlExtnRSpanVlanId (i4SessionIndex,
                                                u4RspanVlanId) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhTestv2IssMirrorCtrlExtnRSpanContext (&u4ErrorCode,
                                                    i4SessionIndex,
                                                    (INT4) u4ContextId) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhSetIssMirrorCtrlExtnRSpanContext (i4SessionIndex,
                                                 u4ContextId) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhTestv2IssMirrorCtrlExtnStatus (&u4ErrorCode,
                                              i4SessionIndex,
                                              ISS_ACTIVE) == SNMP_FAILURE)
        {
            return (CLI_SUCCESS);
        }
        if (nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ISS_ACTIVE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else                        /* ISS_MIRR_DELETE */
    {
        if (nmhGetIssMirrorCtrlExtnStatus (i4SessionIndex,
                                           &i4Status) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Session %d does not exist\r\n",
                       i4SessionIndex);
            return CLI_FAILURE;
        }
        nmhGetIssMirrorCtrlExtnRSpanVlanId (i4SessionIndex,
                                            (INT4 *) &u4CurrRspanVlanId);
        nmhGetIssMirrorCtrlExtnRSpanStatus (i4SessionIndex, &i4CurrRspanStatus);

        if ((u4RspanVlanId != u4CurrRspanVlanId) ||
            (u4RspanStatus != (UINT4) i4CurrRspanStatus))
        {
            CliPrintf (CliHandle, "\r%% RSPAN Vlan specified is not configured "
                       "for this session\r\n");
            return CLI_FAILURE;
        }

        if (nmhTestv2IssMirrorCtrlExtnStatus (&u4ErrorCode,
                                              i4SessionIndex,
                                              ISS_NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {
            return (CLI_SUCCESS);
        }

        if (nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex,
                                           ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhTestv2IssMirrorCtrlExtnRSpanStatus (&u4ErrorCode,
                                                   i4SessionIndex,
                                                   ISS_MIRR_RSPAN_DISABLED) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex,
                                           ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhTestv2IssMirrorCtrlExtnRSpanStatus (&u4ErrorCode,
                                                   i4SessionIndex,
                                                   ISS_MIRR_RSPAN_DISABLED) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhSetIssMirrorCtrlExtnRSpanStatus (i4SessionIndex,
                                                ISS_MIRR_RSPAN_DISABLED) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        nmhGetIssMirrorCtrlExtnRSpanStatus (i4SessionIndex, &i4CurrRspanStatus);

        if (nmhTestv2IssMirrorCtrlExtnStatus (&u4ErrorCode,
                                              i4SessionIndex,
                                              ISS_ACTIVE) == SNMP_FAILURE)
        {
            return (CLI_SUCCESS);
        }

        if (nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex, ISS_ACTIVE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliSetIssNoMirror                        */
/*  Description     : This function is used to set the method  */
/*                    with which the switch should authenticate*/
/*                    users                                    */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4Mode      - Authentication mode        */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliSetIssNoMirror (tCliHandle CliHandle, INT4 i4SessionIndex)
{
    INT4                i4Status = 0;
    UINT4               u4ErrorCode = 0;

    if (nmhGetIssMirrorCtrlExtnStatus (i4SessionIndex,
                                       &i4Status) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Session %d does not exist\r\n",
                   i4SessionIndex);
        return CLI_FAILURE;
    }

    nmhTestv2IssMirrorCtrlExtnStatus (&u4ErrorCode, i4SessionIndex,
                                      ISS_DESTROY);

    if (nmhSetIssMirrorCtrlExtnStatus (i4SessionIndex,
                                       ISS_DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliSetIssCpuMirroring                    */
/*  Description     : This function is used to set the type    */
/*                    of cpu traffic to be mirrored and the    */
/*                    port to in which the traffic is to be    */
/*                    monitored.                               */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    i4IssCpuMirrorType - Rx/Tx/Both/None     */
/*                    u4IssCpuMirrorToPort -  Mirror-To port   */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliSetIssCpuMirroring (tCliHandle CliHandle, INT4 i4IssCpuMirrorType,
                       UINT4 u4IssCpuMirrorToPort)
{
    UINT4               u4ErrorCode = 0;

    if (SNMP_SUCCESS != nmhTestv2IssCpuMirrorType (&u4ErrorCode,
                                                   i4IssCpuMirrorType))
    {
        CliPrintf (CliHandle, "\r%% CpuMirrorType validation failed\r\n");
        return (CLI_FAILURE);
    }

    /* No need to validate in case of disabling mirroring */
    if (i4IssCpuMirrorType != ISS_MIRR_DISABLED)
    {
        if (SNMP_SUCCESS != nmhTestv2IssCpuMirrorToPort (&u4ErrorCode,
                                                         (INT4)
                                                         u4IssCpuMirrorToPort))
        {
            CliPrintf (CliHandle,
                       "\r%% Cpu monitor port validation failed\r\n");
            return (CLI_FAILURE);
        }
    }

    if (SNMP_SUCCESS != nmhSetIssCpuMirrorType (i4IssCpuMirrorType))
    {
        CliPrintf (CliHandle, "\r%% Failed to set CpuMirror type \r\n");
        return (CLI_FAILURE);
    }

    if (i4IssCpuMirrorType != ISS_MIRR_DISABLED)
    {
        if (SNMP_SUCCESS != nmhSetIssCpuMirrorToPort ((INT4)
                                                      u4IssCpuMirrorToPort))
        {
            CliPrintf (CliHandle, "\r%% Failed to set Cpu monitor port \r\n");
            return (CLI_FAILURE);
        }
    }

    return (CLI_SUCCESS);
}

/***************************************************************/
/*  Function Name   : CliShowIssCpuMirroring                   */
/*  Description     : This function is used to display         */
/*                    CPU mirroring details                    */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
CliShowIssCpuMirroring (tCliHandle CliHandle, UINT1 u1Status)
{

    INT1               *piIfName = NULL;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    INT4                i4IssCpuMirrorType = 0;
    INT4                i4IssCpuMirrorToPort = 0;

    nmhGetIssCpuMirrorType (&i4IssCpuMirrorType);
    nmhGetIssCpuMirrorToPort (&i4IssCpuMirrorToPort);

    MEMSET (au1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);
    piIfName = (INT1 *) &au1IfName[0];

    if (u1Status == ISS_MIRR_SHOW_DETAIL)
    {
        CfaCliGetIfName ((UINT4) i4IssCpuMirrorToPort, piIfName);

        /* In case of show command */
        CliPrintf (CliHandle, " CPU Traffic Mirror-To port : %s\r\n", piIfName);

        if (i4IssCpuMirrorType == ISS_MIRR_INGRESS)
        {
            CliPrintf (CliHandle, " CPU Traffic Mirroring Type : Ingress\r\n");
        }
        else if (i4IssCpuMirrorType == ISS_MIRR_EGRESS)
        {
            CliPrintf (CliHandle, " CPU Traffic Mirroring Type : Egress\r\n");
        }
        else if (i4IssCpuMirrorType == ISS_MIRR_BOTH)
        {

            CliPrintf (CliHandle,
                       " CPU Trsffic Mirroring Type : Ingress & Egress\r\n");
        }
        else
        {
            CliPrintf (CliHandle, " CPU Traffic Mirroring Type : Disabled\r\n");
        }
    }
    else
    {
        CfaCliConfGetIfName ((UINT4) i4IssCpuMirrorToPort, piIfName);

        /* For show running config */
        if (i4IssCpuMirrorType == ISS_MIRR_INGRESS)
        {
            CliPrintf (CliHandle, "mirror cpu-port rx %s\r\n", piIfName);
        }
        else if (i4IssCpuMirrorType == ISS_MIRR_EGRESS)
        {
            CliPrintf (CliHandle, "mirror cpu-port tx %s\r\n", piIfName);
        }
        else if (i4IssCpuMirrorType == ISS_MIRR_BOTH)
        {
            CliPrintf (CliHandle, "mirror cpu-port both %s\r\n", piIfName);
        }
    }
    return;
}

/***************************************************************/
/*  Function Name   : CliIssShowMirrorSessionDetail            */
/*  Description     : This function is used to set the method  */
/*                    with which the switch should authenticate*/
/*                    users                                    */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4Mode      - Authentication mode        */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliIssShowMirrorSessionDetail (tCliHandle CliHandle, INT4 i4SessionIndex)
{
    INT4                i4RSpanStatus = 0;
    INT4                i4RspanVlanId = 0;

    if (CliIssShowMirrorSession (CliHandle, i4SessionIndex) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhGetIssMirrorCtrlExtnRSpanStatus (i4SessionIndex, &i4RSpanStatus);

    if (i4RSpanStatus != ISS_MIRR_RSPAN_DISABLED)
    {
        if (i4RSpanStatus == ISS_MIRR_RSPAN_DEST)
        {
            CliPrintf (CliHandle, " Rspan Type        : Destination\r\n");
        }
        else
        {
            CliPrintf (CliHandle, " Rspan Type        : Source\r\n");
        }

        nmhGetIssMirrorCtrlExtnRSpanVlanId (i4SessionIndex, &i4RspanVlanId);
        CliPrintf (CliHandle, " Rspan Vlan Id     : %d\r\n", i4RspanVlanId);
    }
    else
    {
        CliPrintf (CliHandle, " Rspan Disabled \r\n");
    }

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;

}

/***************************************************************/
/*  Function Name   : CliIssShowMirrorStatus                   */
/*  Description     : This function prints the global mirroring*/
/*                   status                                    */
/*  Input(s)        : CliHandle                                */
/*                    i4SessionStatus-  mirror status          */
/*  Output(s)       : None                                     */
/*  Returns         : nothing                                  */
/***************************************************************/

VOID
CliIssShowMirrorStatus (tCliHandle CliHandle, INT4 i4SessionStatus,
                        UINT1 u1Status)
{
    nmhGetIssMirrorStatus (&i4SessionStatus);

    if (u1Status == ISS_MIRR_SHOW_RUNNING)
    {

        if (i4SessionStatus == ISS_MIRRORDISABLE)
        {
            CliPrintf (CliHandle, "\r\nset mirroring disable\r\n");
            return;
        }
    }
    else
    {
        if (i4SessionStatus == ISS_MIRRORENABLE)
        {
            CliPrintf (CliHandle, "Mirroring is globally Enabled.\n ");
            return;
        }

        else if (i4SessionStatus == ISS_MIRRORDISABLE)
        {
            CliPrintf (CliHandle, "Mirroring is globally Disabled.\n");
            return;
        }
    }
    return;
}

/***************************************************************/
/*  Function Name   : CliSetIssShowMirrorSession               */
/*  Description     : This function is used to set the method  */
/*                    with which the switch should authenticate*/
/*                    users                                    */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4Mode      - Authentication mode        */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliIssShowMirrorSession (tCliHandle CliHandle, INT4 i4SessionIndex)
{
    tCfaIfInfo          IfInfo;
    INT4                i4MirrorType = 0;
    INT4                i4SourceId = 0;
    INT4                i4DestId = 0;
    INT4                i4NextSessId = 0;
    INT4                i4NextMirrorSrcId = 0;
    INT4                i4NextMirrorDestId = 0;
    INT4                i4SourceMode = 0;
    INT4                i4ContextId = 0;
    INT4                i4NextContextId = 0;
    INT4                i4SessionStatus = 0;
    INT4                i4TempMode = 0;
    INT4                i4Count = 0;
    INT4                i4MaxPortsPerLine = 0;
    BOOL1               b1SourceExist = ISS_FALSE;
    BOOL1               b1DestExist = ISS_FALSE;

    if (nmhGetIssMirrorCtrlExtnStatus (i4SessionIndex, &i4SessionStatus)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (i4SessionStatus != ISS_ROW_STATUS_DESTROY)
    {

        if (nmhGetIssMirrorCtrlExtnMirrType (i4SessionIndex, &i4MirrorType)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        CliPrintf (CliHandle, " Session     : %d\r\n", i4SessionIndex);
        CliPrintf (CliHandle, " -------\r\n");

        if (i4MirrorType == ISS_MIRR_PORT_BASED)
        {
            i4NextSessId = i4SessionIndex;
            i4NextMirrorSrcId = i4SourceId;
            i4SourceMode = ISS_MIRR_INGRESS;

            CliPrintf (CliHandle, " Source Ports \r\n");
            while (i4SourceMode != ISS_MIRR_DISABLED)
            {
                i4NextSessId = i4SessionIndex;
                i4SourceId = 0;
                i4NextMirrorSrcId = 0;

                if (i4SourceMode == ISS_MIRR_INGRESS)
                {
                    CliPrintf (CliHandle, "   Rx              : ");
                }
                else if (i4SourceMode == ISS_MIRR_EGRESS)
                {
                    CliPrintf (CliHandle, "   Tx              : ");
                }
                else
                {
                    CliPrintf (CliHandle, "   Both            : ");
                }

                while (nmhGetNextIndexIssMirrorCtrlExtnSrcTable (i4SessionIndex,
                                                                 &i4NextSessId,
                                                                 i4SourceId,
                                                                 &i4NextMirrorSrcId)
                       != SNMP_FAILURE)
                {
                    if (i4SessionIndex != i4NextSessId)
                    {
                        break;
                    }
                    i4SourceId = i4NextMirrorSrcId;

                    nmhGetIssMirrorCtrlExtnSrcMode (i4SessionIndex,
                                                    i4SourceId, &i4TempMode);

                    if (i4SourceMode == i4TempMode)
                    {
                        if (b1SourceExist == ISS_TRUE)
                        {
                            CliPrintf (CliHandle, ",");
                        }

                        if ((i4SourceId > SYS_DEF_MAX_PHYSICAL_INTERFACES) &&
                            (i4SourceId < BRG_MAX_PHY_PLUS_LOG_PORTS))
                        {
                            CfaGetIfInfo ((UINT4) i4SourceId, &IfInfo);
                        }
                        else
                        {
                            CfaCliGetIfName ((UINT4) i4SourceId,
                                             (INT1 *) IfInfo.au1IfName);
                            i4Count++;
                        }

/*Show command will display six ports per line*/

                        i4MaxPortsPerLine = 6;

                        if (((i4Count - 1) % i4MaxPortsPerLine == 0)
                            && (i4Count != 1))
                        {
                            if (i4SourceId <= 9)
                            {
                                CliPrintf (CliHandle, "\r\n%26s",
                                           IfInfo.au1IfName);
                            }
                            else
                            {
                                CliPrintf (CliHandle, "\r\n%27s",
                                           IfInfo.au1IfName);
                            }
                        }

                        else
                        {
                            CliPrintf (CliHandle, "%s", IfInfo.au1IfName);
                        }
                        b1SourceExist = ISS_TRUE;
                    }
                }

                if (b1SourceExist == ISS_FALSE)
                {
                    CliPrintf (CliHandle, " None \r\n");
                }
                else
                {
                    CliPrintf (CliHandle, "\r\n");
                    b1SourceExist = ISS_FALSE;
                }

                i4SourceMode++;
            }
        }
        else if (i4MirrorType == ISS_MIRR_MAC_FLOW_BASED)
        {
            i4NextSessId = i4SessionIndex;
            i4NextMirrorSrcId = i4SourceId;

            CliPrintf (CliHandle, " Mac ACL           : ");

            while (nmhGetNextIndexIssMirrorCtrlExtnSrcTable (i4SessionIndex,
                                                             &i4NextSessId,
                                                             i4SourceId,
                                                             &i4NextMirrorSrcId)
                   != SNMP_FAILURE)
            {
                if (i4SessionIndex != i4NextSessId)
                {
                    break;
                }

                if (b1SourceExist == ISS_TRUE)
                {
                    CliPrintf (CliHandle, ",");
                }

                i4SourceId = i4NextMirrorSrcId;

                CliPrintf (CliHandle, "%d", i4SourceId);
                b1SourceExist = ISS_TRUE;
            }

            if (b1SourceExist == ISS_FALSE)
            {
                CliPrintf (CliHandle, " None \r\n");
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
            }
        }
        else if (i4MirrorType == ISS_MIRR_IP_FLOW_BASED)
        {
            i4NextSessId = i4SessionIndex;
            i4NextMirrorSrcId = i4SourceId;

            CliPrintf (CliHandle, " IP ACL            : ");

            while (nmhGetNextIndexIssMirrorCtrlExtnSrcTable (i4SessionIndex,
                                                             &i4NextSessId,
                                                             i4SourceId,
                                                             &i4NextMirrorSrcId)
                   != SNMP_FAILURE)
            {
                if (i4SessionIndex != i4NextSessId)
                {
                    break;
                }

                if (b1SourceExist == ISS_TRUE)
                {
                    CliPrintf (CliHandle, ",");
                }

                i4SourceId = i4NextMirrorSrcId;

                CliPrintf (CliHandle, "%d", i4SourceId);
                b1SourceExist = ISS_TRUE;
            }

            if (b1SourceExist == ISS_FALSE)
            {
                CliPrintf (CliHandle, " None \r\n");
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
            }
        }
        else if (i4MirrorType == ISS_MIRR_VLAN_BASED)
        {
            i4NextSessId = i4SessionIndex;
            i4NextMirrorSrcId = i4SourceId;
            i4SourceMode = ISS_MIRR_INGRESS;

            CliPrintf (CliHandle, " Source Vlans \r\n");
            while (i4SourceMode != ISS_MIRR_DISABLED)
            {
                i4NextSessId = i4SessionIndex;
                i4SourceId = 0;
                i4NextMirrorSrcId = 0;

                if (i4SourceMode == ISS_MIRR_INGRESS)
                {
                    CliPrintf (CliHandle, "   Rx              : ");
                }
                else if (i4SourceMode == ISS_MIRR_EGRESS)
                {
                    CliPrintf (CliHandle, "   Tx              : ");
                }
                else
                {
                    CliPrintf (CliHandle, "   Both            : ");
                }
                while (nmhGetNextIndexIssMirrorCtrlExtnSrcVlanTable
                       (i4SessionIndex, &i4NextSessId, i4ContextId,
                        &i4NextContextId, i4SourceId,
                        &i4NextMirrorSrcId) != SNMP_FAILURE)
                {
                    if (i4SessionIndex != i4NextSessId)
                    {
                        break;
                    }
                    i4SourceId = i4NextMirrorSrcId;

                    nmhGetIssMirrorCtrlExtnSrcVlanMode (i4SessionIndex,
                                                        i4ContextId, i4SourceId,
                                                        &i4TempMode);

                    if (i4SourceMode == i4TempMode)
                    {
                        if (b1SourceExist == ISS_TRUE)
                        {
                            CliPrintf (CliHandle, ",");
                        }
                        CliPrintf (CliHandle, "%d", i4SourceId);
                        b1SourceExist = ISS_TRUE;
                    }
                }

                if (b1SourceExist == ISS_FALSE)
                {
                    CliPrintf (CliHandle, " None \r\n");
                }
                else
                {
                    CliPrintf (CliHandle, "\r\n");
                    b1SourceExist = ISS_FALSE;
                }

                i4SourceMode++;
            }
        }
        if (i4MirrorType != (ISS_MIRR_VLAN_BASED ||
                             ISS_MIRR_PORT_BASED ||
                             ISS_MIRR_MAC_FLOW_BASED || ISS_MIRR_IP_FLOW_BASED))

        {
            CliPrintf (CliHandle, " Source Ports\r\n");
            CliPrintf (CliHandle, "   Rx              : None\r\n");
            CliPrintf (CliHandle, "   Tx              : None\r\n");
            CliPrintf (CliHandle, "   Both            : None\r\n");
        }

        /* Print Destination Index */
        i4NextSessId = i4SessionIndex;

        CliPrintf (CliHandle, " Destination Ports : ");
        while (nmhGetNextIndexIssMirrorCtrlExtnDestinationTable (i4SessionIndex,
                                                                 &i4NextSessId,
                                                                 i4DestId,
                                                                 &i4NextMirrorDestId)
               != SNMP_FAILURE)
        {
            i4DestId = i4NextMirrorDestId;
            if (i4SessionIndex != i4NextSessId)
            {
                break;
            }

            if (b1DestExist == ISS_TRUE)
            {
                CliPrintf (CliHandle, ",");
            }

            if ((i4DestId > SYS_DEF_MAX_PHYSICAL_INTERFACES) &&
                (i4DestId < BRG_MAX_PHY_PLUS_LOG_PORTS))
            {
                CfaGetIfInfo ((UINT4) i4DestId, &IfInfo);
            }
            else
            {
                CfaCliGetIfName ((UINT4) i4DestId, (INT1 *) IfInfo.au1IfName);
            }

            CliPrintf (CliHandle, "%s", IfInfo.au1IfName);
            b1DestExist = ISS_TRUE;
        }
        if (b1DestExist == ISS_FALSE)
        {
            CliPrintf (CliHandle, " None \r\n");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n");
        }
        CliPrintf (CliHandle, " Session Status    :");
        if (i4SessionStatus == ISS_ROW_STATUS_ACTIVE)
        {
            CliPrintf (CliHandle, " Active \r\n");
        }
        else
        {
            CliPrintf (CliHandle, " Inactive \r\n");
        }
        return CLI_SUCCESS;
    }
    else
    {
        return CLI_FAILURE;
    }

}

/***************************************************************/
/*  Function Name   : CliSetIssShowMirrorRecords               */
/*  Description     : This function is used to set the method  */
/*                    with which the switch should authenticate*/
/*                    users                                    */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4Mode      - Authentication mode        */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliIssShowMirrorRecords (tCliHandle CliHandle)
{
    UINT4               u4MirrAvlSrcRec = 0;
    UINT4               u4MirrAvlDestRec = 0;
    CliPrintf (CliHandle, " Mirroring Record Information \r\n");
    CliPrintf (CliHandle, " ---------------------------- \r\n");
    u4MirrAvlSrcRec = ISS_GET_MIRR_FREE_SRC_RECORDS;
    u4MirrAvlDestRec = ISS_GET_MIRR_FREE_DEST_RECORDS;
    CliPrintf (CliHandle, " Maximum   Source Records       : %d\r\n",
               MAX_ISS_MIRR_SRC_RECORD_BLOCKS);

    CliPrintf (CliHandle, " Available Source Records       : %d\r\n",
               u4MirrAvlSrcRec);
    CliPrintf (CliHandle, " Maximum   Destination  Records : %d\r\n",
               MAX_ISS_MIRR_DEST_RECORD_BLOCKS);
    CliPrintf (CliHandle, " Available Destination  Records : %d\r\n",
               u4MirrAvlDestRec);

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliSetIssSwitchCPUInfo                   */
/*  Description     : This function sets the max CPU threshold */
/*                    of the switch.                           */
/*  Input(s)        : CliHandle     - CLI Handle               */
/*                    u4CPUUsage    - CPU threshold            */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliSetIssSwitchCPUInfo (tCliHandle CliHandle, UINT4 u4CPUUsage)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2IssSwitchMaxCPUThreshold (&u4ErrorCode, (INT4) u4CPUUsage)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    nmhSetIssSwitchMaxCPUThreshold ((INT4) u4CPUUsage);

    return (CLI_SUCCESS);
}

/***************************************************************/
/*  Function Name   : CliSetIssSwitchFlashInfo                 */
/*  Description     : This function sets the max Flash         */
/*                    threshold of the switch.                 */
/*  Input(s)        : CliHandle     - CLI Handle               */
/*                    u4FlashUsage    - CPU threshold          */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliSetIssSwitchFlashInfo (tCliHandle CliHandle, UINT4 u4FlashUsage)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2IssSwitchMaxFlashUsage (&u4ErrorCode, (INT4) u4FlashUsage)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    nmhSetIssSwitchMaxFlashUsage ((INT4) u4FlashUsage);

    return (CLI_SUCCESS);
}

/***************************************************************/
/*  Function Name   : CliSetIssSwitchTemperatureInfo           */
/*  Description     : This function sets the max or min        */
/*                    temperature threshold of the switch.     */
/*  Input(s)        : CliHandle      - CLI Handle              */
/*                    u4Type         - max or min tempeartue   */
/*                    i4Tempearature - tempearture threshold   */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliSetIssSwitchTemperatureInfo (tCliHandle CliHandle, UINT4 u4Type,
                                INT4 i4Tempearature)
{
    UINT4               u4ErrorCode;

    if (u4Type == ISS_SET_MIN_TEMPERATURE)
    {

        if (nmhTestv2IssSwitchMinThresholdTemperature
            (&u4ErrorCode, i4Tempearature) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        nmhSetIssSwitchMinThresholdTemperature (i4Tempearature);
    }
    else if (u4Type == ISS_SET_MAX_TEMPERATURE)
    {
        if (nmhTestv2IssSwitchMaxThresholdTemperature
            (&u4ErrorCode, i4Tempearature) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        nmhSetIssSwitchMaxThresholdTemperature (i4Tempearature);
    }
    return (CLI_SUCCESS);
}

/*******************************************************************/
/*  Function Name   : CliSetIssSwitchPowerSupplyInfo               */
/*  Description     : This function sets the max and min power     */
/*                    supply threshold of the switch.              */
/*  Input(s)        : CliHandle     - CLI Handle                   */
/*                  : u4Type        - max or min                   */
/*                    u4PowerSupply - Power supply  threshold      */
/*  Output(s)       : Error message - on failure                   */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                      */
/*******************************************************************/
INT4
CliSetIssSwitchPowerSupplyInfo (tCliHandle CliHandle, UINT4 u4Type,
                                UINT4 u4PowerSupply)
{
    UINT4               u4ErrorCode;

    if (u4Type == ISS_SET_MIN_POWER)
    {
        if (nmhTestv2IssSwitchPowerFailure (&u4ErrorCode, (INT4) u4PowerSupply)
            != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        nmhSetIssSwitchPowerFailure ((INT4) u4PowerSupply);
    }
    else if (u4Type == ISS_SET_MAX_POWER)
    {
        if (nmhTestv2IssSwitchPowerSurge (&u4ErrorCode, (INT4) u4PowerSupply)
            != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        nmhSetIssSwitchPowerSurge ((INT4) u4PowerSupply);
    }
    return (CLI_SUCCESS);
}

/***************************************************************/
/*  Function Name   : CliSetIssDebugLogging                    */
/*  Description     : This function is used to enable/disable  */
/*                    debug logging and also specify whether   */
/*                    logs should be printed on console or sent*/
/*                    to a file                                */
/*  Input(s)        :                                          */
/*                    CliHandle - CLI Handle                   */
/*                    u4Mode    - Logging mode Console/file    */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliSetIssDebugLogging (tCliHandle CliHandle, UINT4 u4Mode, UINT1 u1Status)
{
    UINT4               u4ErrorCode;
    CHR1                ac1TmpBuf[ISS_PATH_LEN];
    UINT1               au1FileName[ISS_PATH_LEN];
    if (u1Status == ISS_ACTIVE_LOG)
    {

        if (nmhTestv2IssLoggingOption (&u4ErrorCode, (INT4) u4Mode) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        if (nmhSetIssLoggingOption (u4Mode) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else if (u1Status == ISS_STANDBY_LOG)
    {
#ifdef RM_WANTED
        if (RmGetPeerNodeCount () == 0)
        {
            CliPrintf (CliHandle, "\r%% Peer Node is not available\r\n");
            CliUnRegisterLock (CliHandle);
            ISS_UNLOCK ();
            return (CLI_FAILURE);
        }
        else
        {
            if (nmhTestv2IssPeerLoggingOption (&u4ErrorCode, (INT4) u4Mode) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
            if (nmhSetIssPeerLoggingOption (u4Mode) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return (CLI_FAILURE);
            }
        }
#else
        CliUnRegisterLock (CliHandle);
        ISS_UNLOCK ();
        return (CLI_FAILURE);
#endif
    }

    if (u4Mode == CONSOLE_LOG)
    {
        UtlGetLogPathForFlash (ac1TmpBuf);

        if ((u4Mode == CONSOLE_LOG) && (STRCMP (ac1TmpBuf, "") != 0))
        {
            LOGGER_SEND_EVENT (UtlGetLoggerTaskId (), LOGGER_MSG_ENQ_EVENT);
        }

        SNPRINTF ((CHR1 *) au1FileName, ISS_PATH_LEN, "%s",
                  ISS_DEFAULT_DEBUG_LOG_FILE);

        IssSetFlashLoggingFileNameToNvRam ((UINT1 *) au1FileName);

        UtlSetLogPathForFlash (au1FileName);
    }

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliSetIssDumpOsRsc                       */
/*  Description     : This function is used to display or      */
/*                    list out the task details, queue details */
/*                    and semaphore details using dump         */
/*                    os resouce command.                       */
/*  Input(s)        : CliHandle - CLI Handle                   */
/*                    u4value   - Task/Que/Sem                 */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliSetIssDumpOsResource (tCliHandle CliHandle, UINT4 u4value, CHR1 * taskname)
{
    static UINT1        pu1Result[STORE_MAX_SIZE_TABLE];
    UINT4              *pu4NextIdx = NULL;

    if (u4value == TASK)
    {
        if (FsapShowTask
            ((UINT1 *) taskname, (UINT1 *) &pu1Result,
             STORE_MAX_SIZE_TABLE) == OSIX_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        CliPrintf (CliHandle, "%s\r\n", (CHR1 *) pu1Result);
    }
    if (u4value == QUE)
    {
        if (FsapShowQue
            ((UINT1 *) taskname, (UINT1 *) &pu1Result,
             STORE_MAX_SIZE_TABLE) == OSIX_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        CliPrintf (CliHandle, "%s\r\n", (CHR1 *) pu1Result);
    }
    if (u4value == SEM)
    {
        if (FsapShowSem
            ((UINT1 *) taskname, (UINT1 *) &pu1Result,
             (UINT4 *) pu4NextIdx, STORE_MAX_SIZE_TABLE) == OSIX_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        CliPrintf (CliHandle, "%s\r\n", (CHR1 *) pu1Result);
    }
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   :  CliSetImageDump                         */
/*  Description     : This function is used to store the dump  */
/*                    the file in user given location          */
/*  Input(s)        :                                          */
/*                    CliHandle - CLI Handle                   */
/*                    pi1FileName - Filepath                   */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliSetImageDump (tCliHandle CliHandle, UINT1 *pi1FilePath)
{
    INT1                i1Count = 0;
    while (pi1FilePath[i1Count] != '\0')
    {
        if (!
            (ISALPHA (pi1FilePath[i1Count]) || ISDIGIT (pi1FilePath[i1Count])
             || ISFWDSLASH (pi1FilePath[i1Count])))
        {
            CliPrintf (CliHandle,
                       "\r\n %% Configure valid name for directory \r\n");
            return CLI_FAILURE;
        }
        i1Count++;
    }
    OsixCreateDir ((const CHR1 *) pi1FilePath);
    if (IssSetImageDumpLocation (pi1FilePath) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/***************************************************************/
/*  Function Name   : CliSetIssRestart                         */
/*  Description     : This function is used to restart         */
/*                    the switch                               */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/

INT4
CliSetIssRestart ()
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2IssRestart (&u4ErrorCode, ISS_TRUE) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_ISS_INVALID_RESTART);
        return CLI_FAILURE;
    }
    if (nmhSetIssRestart (ISS_TRUE) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_ISS_INVALID_RESTART);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliSetIssStandbyRestart                  */
/*  Description     : This function is used to restart         */
/*                    the switch                               */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
INT4
CliSetIssStandbyRestart ()
{
    UINT4               u4ErrorCode = 0;
#ifndef RM_WANTED
    CLI_SET_ERR (CLI_ISS_HA_NOT_AVAIL);
    return CLI_FAILURE;
#endif

    if (nmhTestv2IssStandbyRestart (&u4ErrorCode, ISS_TRUE) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_ISS_INVALID_RESTART);
        return CLI_FAILURE;
    }
    nmhSetIssStandbyRestart (ISS_TRUE);
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliSetIssLoginAuth                       */
/*  Description     : This function is used to set the method  */
/*                    with which the switch should authenticate*/
/*                    users                                    */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4Mode      - Authentication mode        */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliSetIssLoginAuth (tCliHandle CliHandle, UINT4 u4Mode, UINT4 u4DefPrivil)
{
    UINT4               u4LogMode = 0;
    UINT4               u4ErrorCode;

    switch (u4Mode)
    {
        case ISS_LOCAL:
            u4LogMode = LOCAL_LOGIN;
            break;

        case ISS_RADIUS:
            u4LogMode = REMOTE_LOGIN_RADIUS;
            break;

        case ISS_TACACS:
            u4LogMode = REMOTE_LOGIN_TACACS;
            break;

        case ISS_RADIUS_LOCAL:
            u4LogMode = REMOTE_LOGIN_RADIUS_FALLBACK_TO_LOCAL;
            break;

        case ISS_TACACS_LOCAL:
            u4LogMode = REMOTE_LOGIN_TACACS_FALLBACK_TO_LOCAL;
            break;
#ifdef PAM_AUTH_WANTED
        case ISS_PAM_AUTH:
            u4LogMode = PAM_LOGIN;
            break;
#endif
        default:
            break;
    }

    if (nmhTestv2IssLoginAuthentication (&u4ErrorCode, (INT4) u4LogMode) ==
        SNMP_FAILURE)
    {

        CliPrintf (CliHandle,
                   "\r%% RADIUS server must first be configured \r\n");
        return (CLI_FAILURE);
    }
    if (nmhSetIssLoginAuthentication (u4LogMode) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

#ifdef PAM_AUTH_WANTED
    if (u4LogMode == PAM_LOGIN)
    {
        if (nmhTestv2IssPamLoginPrivilege (&u4ErrorCode, u4DefPrivil) ==
            SNMP_FAILURE)
        {

            CliPrintf (CliHandle, "\r%% Invalid Privilege value \r\n");
            return (CLI_FAILURE);
        }
        if (nmhSetIssPamLoginPrivilege (u4DefPrivil) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
#else
    UNUSED_PARAM (u4DefPrivil);
#endif
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : CliSetIssRollbackFlag                               */
/*  Description     : This function configures the Rollback               */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    i4RollbackFlag - ISS_ROLLBACK_ENABLED  or           */
/*                                     ISS_ROLLBACK_DISABLED              */
/*                                                                        */
/*  Output(s)       : NONE                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
CliSetIssRollbackFlag (tCliHandle CliHandle, INT4 i4RollbackFlag)
{
    UINT4               u4ErrCode;

    UNUSED_PARAM (CliHandle);

    /* Validate the Value Passed and check whether any pre-requisite 
     * is required or not */
    if (nmhTestv2IssConfigRollbackFlag (&u4ErrCode, i4RollbackFlag)
        == SNMP_FAILURE)
    {
        if (i4RollbackFlag == ISS_ROLLBACK_ENABLED)
        {
            CLI_SET_ERR (CLI_ISS_ROLLBACK_ENABLE_FAILED);
        }
        else
        {
            CLI_SET_ERR (CLI_ISS_ROLLBACK_DISABLE_FAILED);
        }
        return CLI_FAILURE;
    }

    if (nmhSetIssConfigRollbackFlag (i4RollbackFlag) == SNMP_FAILURE)
    {
        if (i4RollbackFlag == ISS_ROLLBACK_ENABLED)
        {
            CLI_SET_ERR (CLI_ISS_ROLLBACK_ENABLE_FAILED);
        }
        else
        {
            CLI_SET_ERR (CLI_ISS_ROLLBACK_DISABLE_FAILED);
        }
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliSetIssErase                           */
/*  Description     : This function is used to erase the       */
/*                    startup configuration file-iss.conf or   */
/*                    the NVRAM                                */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4Flag      - Flag - NVRAM/Startup File  */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliSetIssErase (tCliHandle CliHandle, UINT4 u4Flag, UINT1 *pFileName)
{
    UINT1               au1FileName[ISS_CONFIG_FILE_NAME_LEN];
    INT4                i4ResType = 0;
    /* The Delete functionality is supported only for VxWorks and Linux. The
     * command depends on the operating system and should be ported for all other
     * operating system.
     */

    /* Get Restoration Type - whether CSR or MSR */
    i4ResType = IssGetRestoreTypeFromNvRam ();

    if (u4Flag == ISS_NVRAM)
    {
        SPRINTF ((CHR1 *) au1FileName, "%s%s", FLASH, "issnvram.txt");
        issDeleteLocalFile (au1FileName);

#ifdef WTP_WANTED
        SPRINTF ((CHR1 *) au1FileName, "%s%s", FLASH, "wtpnvram.txt");
        issDeleteLocalFile (au1FileName);
#endif
    }
    else if (u4Flag == ISS_FLASH)
    {
        if ((gIssSysGroupInfo.IssIncrSaveFlag == ISS_TRUE) &&
            (gIssConfigSaveInfo.IssAutoConfigSave == ISS_TRUE) &&
            (i4ResType != SET_FLAG))
        {
            CliPrintf (CliHandle,
                       "\r%% Auto save of configurations is enabled, Erase event not processed ...... \r\n");
            return (CLI_FAILURE);
        }
        SPRINTF ((CHR1 *) au1FileName, "%s%s", FLASH, pFileName);
        issDeleteLocalFile (au1FileName);

        if (nmhSetIssConfigSaveOption (ISS_CONFIG_NO_SAVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        if (nmhSetIssConfigRestoreOption (ISS_CONFIG_NO_RESTORE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }

    else if (u4Flag == ISS_STARTUP)
    {
        if ((gIssSysGroupInfo.IssIncrSaveFlag == ISS_TRUE) &&
            (gIssConfigSaveInfo.IssAutoConfigSave == ISS_TRUE) &&
            (i4ResType != SET_FLAG))
        {
            CliPrintf (CliHandle,
                       "\r%% Auto save of configurations is enabled, Erase event not processed ...... \r\n");
            return (CLI_FAILURE);
        }

        SPRINTF ((CHR1 *) au1FileName, "%s%s", FLASH,
                 (CONST CHR1 *) IssGetRestoreFileNameFromNvRam ());
        issDeleteLocalFile (au1FileName);

        SPRINTF ((CHR1 *) au1FileName, "%s%s", FLASH,
                 (CONST CHR1 *) IssGetBgpConfRestoreFileNameFromNvRam ());
        issDeleteLocalFile (au1FileName);

        SPRINTF ((CHR1 *) au1FileName, "%s%s", FLASH,
                 (CONST CHR1 *) "bgpgr.txt");
        issDeleteLocalFile (au1FileName);

        SPRINTF ((CHR1 *) au1FileName, "%s%s", FLASH,
                 (CONST CHR1 *) IssGetCsrConfRestoreFileNameFromNvRam ());
        issDeleteLocalFile (au1FileName);

        SPRINTF ((CHR1 *) au1FileName, "%s%s", FLASH,
                 (CONST CHR1 *) IssGetOspfConfRestoreFileNameFromNvRam ());
        issDeleteLocalFile (au1FileName);

        if (nmhSetIssConfigSaveOption (ISS_CONFIG_NO_SAVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhSetIssConfigRestoreOption (ISS_CONFIG_NO_RESTORE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }
    return (CLI_SUCCESS);
}

/***************************************************************/
/*  Function Name   : CliIssSetDate                            */
/*  Description     : This function is used to set athe date   */
/*                                                             */
/*  Input(s)        :                                          */
/*                    CliHandle - CLI Handle                   */
/*                    *pi1Time - Time                          */
/*                    *pu4Day  - day                           */
/*                    u4Month  - Month                         */
/*                    *pu4Year - year                          */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliIssSetDate (tCliHandle CliHandle, INT1 *pi1Time, UINT4 *pu4Day,
               UINT4 u4Month, UINT4 *pu4Year)
{
    tSNMP_OCTET_STRING_TYPE Date;
    UINT4               u4ErrCode;
    UINT1               au1Temp[ISS_MAX_DATE_LEN];
    UINT1               u1Hrs;
    UINT1               u1Mins;
    UINT1               u1Secs;

    Date.pu1_OctetList = &au1Temp[0];
    Date.i4_Length = ISS_MAX_DATE_LEN;

    /* Time is split into hours,minutes & seconds */

    if (CliSplitTimeToken (pi1Time, &u1Hrs, &u1Mins, &u1Secs) != CLI_SUCCESS)
        return CLI_FAILURE;

    SNPRINTF ((CHR1 *) au1Temp, ISS_MAX_DATE_LEN,
              "%.2d:%.2d:%.2d %.2u %.2u %.4u",
              u1Hrs, u1Mins, u1Secs, *pu4Day, u4Month, *pu4Year);

    Date.i4_Length = (INT4) STRLEN (Date.pu1_OctetList);
    if (nmhTestv2IssSwitchDate (&u4ErrCode, &Date) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Date and Time \r\n");
        return (CLI_FAILURE);
    }

    if (nmhSetIssSwitchDate (&Date) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;

}

/***************************************************************/
/*  Function Name   : CliSetIssConfigSave                      */
/*  Description     : This function is used to erase the       */
/*                    startup configuration file-iss.conf or   */
/*                    the NVRAM                                */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4Mode      - Mode ISS_LOCAL/ISS_REMOTE  */
/*                    pu1FileName - File Name                  */
/*                    u4IpAddress - IP address                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliIssConfigSave (tCliHandle CliHandle, UINT4 u4Mode, UINT1 *pu1FileName,
                  tIPvXAddr * pIpAddress, UINT1 *pu1HostName,
                  UINT1 *pu1UserName, UINT1 *pu1Passwd)
{
    INT4                i4RetStatus;
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE sFileOctet;
    tSNMP_OCTET_STRING_TYPE sOldFileOctet;

    tSNMP_OCTET_STRING_TYPE sUserNameOctet;
    tSNMP_OCTET_STRING_TYPE sPasswdOctet;
    tSNMP_OCTET_STRING_TYPE sSrvAddr;
    UINT4               u4TransMode;
    UINT1               u1TempLen = 0;
    UINT1               au1Addr[DNS_MAX_QUERY_LEN];

    UINT1               au1FileName[ISS_CONFIG_FILE_NAME_LEN];
    tDNSResolvedIpInfo  ResolvedIpInfo;

    MEMSET (&sFileOctet, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&sOldFileOctet, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1FileName, 0, ISS_CONFIG_FILE_NAME_LEN);

    sOldFileOctet.pu1_OctetList = au1FileName;

    MEMSET (au1Addr, 0, DNS_MAX_QUERY_LEN);
    MEMSET (&ResolvedIpInfo, 0, sizeof (tDNSResolvedIpInfo));
    if (u4Mode == ISS_REMOTE_TFTP_SAVE || u4Mode == ISS_REMOTE_SFTP_SAVE)
    {
        /* We are trying to save configuration to a remote site . Get the 
         * TFTP server address,filename. Set the restore option as 
         * remote restore.
         */
        sFileOctet.pu1_OctetList = pu1FileName;
        sFileOctet.i4_Length = (INT4) STRLEN (pu1FileName);

        /* If family is DNS host name resolve and copy IpAddress to au1Addr. */
        if (pIpAddress->u1Afi != IPVX_DNS_FAMILY)
        {
            MEMCPY (au1Addr, pIpAddress->au1Addr, IPVX_MAX_INET_ADDR_LEN);
            sSrvAddr.i4_Length = (INT4) pIpAddress->u1AddrLen;
        }
        else
        {
            u1TempLen = STRLEN (pu1HostName);
            STRNCPY (au1Addr, pu1HostName, u1TempLen);
            au1Addr[u1TempLen] = '\0';
            i4RetStatus =
                FsUtlIPvXResolveHostName (au1Addr, DNS_BLOCK, &ResolvedIpInfo);

            if (i4RetStatus == DNS_NOT_RESOLVED)
            {
                CliPrintf (CliHandle, "\r%%Cannot resolve the host name");
                return (CLI_FAILURE);
                /*not resolved */
            }
            else if (i4RetStatus == DNS_IN_PROGRESS)
            {
                CliPrintf (CliHandle, "\r%% Host name resolution in Progress");
                return (CLI_FAILURE);
                /*in progress */
            }
            else if (i4RetStatus == DNS_CACHE_FULL)
            {
                CliPrintf (CliHandle,
                           "\r%%DNS cache full, cannot resolve at the moment");
                return (CLI_FAILURE);
                /*cache full */
            }

            MEMSET (au1Addr, 0, DNS_MAX_QUERY_LEN);
            /* If the hostname resolves to both IPv4 and IPv6 
             * address, IPv6 address is considered */
            if (ResolvedIpInfo.Resolv6Addr.u1AddrLen != 0)
            {
                MEMCPY (au1Addr, ResolvedIpInfo.Resolv6Addr.au1Addr,
                        IPVX_IPV6_ADDR_LEN);
                sSrvAddr.i4_Length = (INT4) IPVX_IPV6_ADDR_LEN;
                pIpAddress->u1Afi = (UINT1) IPVX_ADDR_FMLY_IPV6;
            }
            else
            {
                MEMCPY (au1Addr, ResolvedIpInfo.Resolv4Addr.au1Addr,
                        IPVX_IPV4_ADDR_LEN);
                sSrvAddr.i4_Length = (INT4) IPVX_IPV4_ADDR_LEN;
                pIpAddress->u1Afi = (UINT1) IPVX_ADDR_FMLY_IPV4;
            }
        }
        sSrvAddr.pu1_OctetList = au1Addr;
        sSrvAddr.i4_Length = (INT4) pIpAddress->u1AddrLen;

        if (nmhTestv2IssConfigSaveIpvxAddr (&u4ErrorCode, &sSrvAddr)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid IP-Address \r\n");
            return (CLI_FAILURE);
        }

        if (nmhTestv2IssConfigSaveFileName (&u4ErrorCode, &sFileOctet)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid Filename \r\n");
            return (CLI_FAILURE);
        }

        if (nmhTestv2IssConfigSaveIpAddrType (&u4ErrorCode,
                                              pIpAddress->u1Afi) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetIssConfigSaveIpAddrType (pIpAddress->u1Afi) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        if (nmhSetIssConfigSaveIpvxAddr (&sSrvAddr) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        /* Store the old file name which will be used when transfer fails */
        nmhGetIssConfigSaveFileName (&sOldFileOctet);

        if (nmhSetIssConfigSaveFileName (&sFileOctet) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhSetIssConfigSaveOption (ISS_CONFIG_REMOTE_SAVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
        u4TransMode =
            (u4Mode ==
             ISS_REMOTE_SFTP_SAVE) ? ISS_SFTP_TRANSFER_MODE :
            ISS_TFTP_TRANSFER_MODE;

        nmhSetIssConfigSaveTransferMode ((INT4) u4TransMode);

        if (u4Mode == ISS_REMOTE_SFTP_SAVE)
        {
            sUserNameOctet.pu1_OctetList = pu1UserName;
            sUserNameOctet.i4_Length = (INT4) STRLEN (pu1UserName);
            if (nmhTestv2IssConfigSaveUserName (&u4ErrorCode,
                                                &sUserNameOctet) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid Username \r\n");
                return (CLI_FAILURE);
            }
            nmhSetIssConfigSaveUserName (&sUserNameOctet);

            sPasswdOctet.pu1_OctetList = pu1Passwd;
            sPasswdOctet.i4_Length = (INT4) STRLEN (pu1Passwd);
            if (nmhTestv2IssConfigSavePassword (&u4ErrorCode, &sPasswdOctet) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid Password \r\n");
                return (CLI_FAILURE);
            }
            nmhSetIssConfigSavePassword (&sPasswdOctet);
        }
    }
    else if (u4Mode == ISS_STARTUP)
    {
        if (nmhSetIssConfigSaveOption (ISS_CONFIG_STARTUP_SAVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhSetIssConfigRestoreOption (ISS_CONFIG_RESTORE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }
    else
    {
        sFileOctet.pu1_OctetList = pu1FileName;
        sFileOctet.i4_Length = (INT4) STRLEN (pu1FileName);

        if (nmhTestv2IssConfigSaveFileName (&u4ErrorCode, &sFileOctet)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid Filename \r\n");
            return (CLI_FAILURE);
        }

        /* Store the old file name which will be used when transfer fails */
        nmhGetIssConfigSaveFileName (&sOldFileOctet);

        if (nmhSetIssConfigSaveFileName (&sFileOctet) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhSetIssConfigSaveOption (ISS_CONFIG_FLASH_SAVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }

    /* Going to trigger MSR & MSR requires ISS_LOCK(through SNMP)
     * so Release it here.
     */
    CliUnRegisterLock (CliHandle);
    ISS_UNLOCK ();

    i4RetStatus = CliMibSave (CliHandle, u4Mode);

    CliRegisterLock (CliHandle, IssLock, IssUnLock);
    ISS_LOCK ();

    /* If MIB save fails and if Config file name had changed
     * then revert the config file name */
    if ((i4RetStatus != CLI_SUCCESS) || (sFileOctet.pu1_OctetList == NULL))
    {
        if (nmhSetIssConfigSaveFileName (&sOldFileOctet) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    return (i4RetStatus);
}

/***************************************************************/
/*  Function Name   : CliIssCopyToStartupFile                  */
/*  Description     : This function is used to copy a file from*/
/*                    local flash or from the remote site to   */
/*                    iss.conf file (startup file)             */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4Mode      - Mode (Remote/Local)        */
/*                    pu1FileName - File Name                  */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliIssCopyToStartupFile (tCliHandle CliHandle, UINT4 u4Mode,
                         UINT1 *pSrcFile, tIPvXAddr IpAddress,
                         UINT1 *pu1HostName, UINT1 *pu1UserName,
                         UINT1 *pu1Passwd)
{
    UINT1               au1FileName[ISS_CONFIG_FILE_NAME_LEN];
    UINT1               au1TmpFileName[ISS_CONFIG_FILE_NAME_LEN];
    INT4                i4ConfFd = 0;
    tDNSResolvedIpInfo  ResolvedIpInfo;
    INT4                i4RetVal = 0;
    INT4                i4ResType = 0;

    tSNMP_OCTET_STRING_TYPE RestoreCfgFile;

    MEMSET (au1FileName, 0, ISS_CONFIG_FILE_NAME_LEN);
    MEMSET (au1TmpFileName, 0, ISS_CONFIG_FILE_NAME_LEN);
    MEMSET (&ResolvedIpInfo, 0, sizeof (tDNSResolvedIpInfo));

    /* Get Restoration Type - whether CSR or MSR */
    i4ResType = IssGetRestoreTypeFromNvRam ();

    if (STRCMP
        (pSrcFile + STRLEN (pSrcFile) - STRLEN (ISS_CONF_FILE),
         ISS_CONF_FILE) != 0)
    {
        CliPrintf (CliHandle,
                   "\r%%only conf file can be copied as startupconfig");
        return CLI_FAILURE;
    }

    if (u4Mode == ISS_REMOTE_TFTP_RESTORE || u4Mode == ISS_REMOTE_SFTP_RESTORE)
    {
        if (CliIssCheckMsrStatus (CliHandle) == CLI_FAILURE)
        {
            return (CLI_FAILURE);
        }

    }

    /* If host name is sent, resolve it and copy to IpAddress.nu1Addr. */
    if (IpAddress.u1Afi == IPVX_DNS_FAMILY)
    {
        i4RetVal =
            FsUtlIPvXResolveHostName (pu1HostName, DNS_BLOCK, &ResolvedIpInfo);
        if (i4RetVal == DNS_NOT_RESOLVED)
        {
            CliPrintf (CliHandle, "\r%%Cannot resolve the host name");
            return (CLI_FAILURE);
            /*not resolved */
        }
        else if (i4RetVal == DNS_IN_PROGRESS)
        {
            CliPrintf (CliHandle, "\r%% Host name resolution in Progress");
            return (CLI_FAILURE);
            /*in progress */
        }
        else if (i4RetVal == DNS_CACHE_FULL)
        {
            CliPrintf (CliHandle,
                       "\r%%DNS cache full, cannot resolve at the moment");
            return (CLI_FAILURE);
            /*cache full */
        }

        /* If the hostname resolves to both IPv4 and IPv6 
         * address, IPv6 address is considered */
        if (ResolvedIpInfo.Resolv6Addr.u1AddrLen != 0)
        {
            MEMCPY (IpAddress.au1Addr,
                    ResolvedIpInfo.Resolv6Addr.au1Addr, IPVX_IPV6_ADDR_LEN);
            IpAddress.u1Afi = (UINT1) IPVX_ADDR_FMLY_IPV6;
        }
        else
        {
            MEMCPY (IpAddress.au1Addr,
                    ResolvedIpInfo.Resolv4Addr.au1Addr, IPVX_IPV4_ADDR_LEN);
            IpAddress.u1Afi = (UINT1) IPVX_ADDR_FMLY_IPV4;
        }

    }

    if (u4Mode == ISS_REMOTE_TFTP_RESTORE)
    {
        if (tftpcRecvFile (IpAddress, pSrcFile, (UINT1 *) ISS_TMP_CONF_FILE) !=
            TFTPC_OK)
        {
            CliPrintf (CliHandle, "\r%% TFTP Failed \r\n");
            gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
            return CLI_FAILURE;
        }
    }
#ifdef SSH_WANTED
    else if (u4Mode == ISS_REMOTE_SFTP_RESTORE)
    {
        if (sftpcArRecvFile (pu1UserName, pu1Passwd,
                             IpAddress, pSrcFile,
                             (UINT1 *) ISS_TMP_CONF_FILE) != SFTP_SUCCESS)
        {

            CliPrintf (CliHandle, "\r%% SFTP Failed \r\n");
            gi4MibResStatus = LAST_MIB_RESTORE_FAILED;

            /*SFTP file tranfer failed.So copied file will be removed here */
            FileDelete ((UINT1 *) ISS_TMP_CONF_FILE);

            return CLI_FAILURE;
        }
    }
#endif
    else
    {
        SPRINTF ((CHR1 *) au1TmpFileName, "%s%s", FLASH, pSrcFile);
        if (issCopyLocalFile (au1TmpFileName, (UINT1 *) ISS_TMP_CONF_FILE) ==
            ISS_FAILURE)
        {
            gi4MibResFailureReason = MIB_RES_FILE_COPY_FAILURE;
            gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
            return CLI_FAILURE;
        }
    }

    /* File is copied into the backup file. Check whether the Checksum is correct. */
    if ((i4ConfFd = FileOpen ((UINT1 *) ISS_TMP_CONF_FILE, OSIX_FILE_RO)) < 0)
    {
        CliPrintf (CliHandle, "\r%% Openning Conf File failed\n");
        gi4MibResFailureReason = MIB_RES_FILE_OPEN_FAILURE;
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        return CLI_FAILURE;
    }

    if (i4ResType != SET_FLAG)
    {
        /* This function will take care of Closing the File */
        if (MsrVerifyRestFileChecksum (i4ConfFd) == ISS_FAILURE)
        {
            issDeleteLocalFile ((UINT1 *) ISS_TMP_CONF_FILE);
            CliPrintf (CliHandle,
                       "\r%% Invalid Config file. Config file not copied.\n");
            gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
            return CLI_FAILURE;
        }
        i4ConfFd = 0;
    }

    /* The Config file is valid and is present in the ISS_TMP_CONF_FILE.
     * Replace the existing config file with this */

    RestoreCfgFile.pu1_OctetList = &au1TmpFileName[0];

    nmhGetIssConfigRestoreFileName (&RestoreCfgFile);
    SPRINTF ((CHR1 *) au1FileName, "%s%s", FLASH, RestoreCfgFile.pu1_OctetList);

    issDeleteLocalFile ((UINT1 *) au1FileName);

    if (issCopyLocalFile ((UINT1 *) ISS_TMP_CONF_FILE, au1FileName)
        == ISS_FAILURE)
    {
        gi4MibResFailureReason = MIB_RES_FILE_COPY_FAILURE;
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        issDeleteLocalFile ((UINT1 *) ISS_TMP_CONF_FILE);
        CliPrintf (CliHandle, "\r%% Copying config file failed.\n");
        return CLI_FAILURE;
    }

    issDeleteLocalFile ((UINT1 *) ISS_TMP_CONF_FILE);

    if (nmhSetIssConfigRestoreOption (ISS_CONFIG_RESTORE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

#ifndef SSH_WANTED
    UNUSED_PARAM (pu1UserName);
    UNUSED_PARAM (pu1Passwd);
    UNUSED_PARAM (pu1HostName);
#endif
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliIssCopyFromStartupFile                */
/*  Description     : This function is used to copy the        */
/*                    iss.conf to a remote site or to a file   */
/*                    in the local FLASH                       */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4Mode      - Mode (Remote/Local)        */
/*                    pu1DestFile - File Name                  */
/*                    IpAddress - IP address                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliIssCopyFromStartupFile (tCliHandle CliHandle, UINT4 u4Mode,
                           UINT1 *pu1DestFile, tIPvXAddr IpAddress,
                           UINT1 *pu1HostName, UINT1 *pu1UserName,
                           UINT1 *pu1Passwd)
{
    UINT1               au1FileName[ISS_CONFIG_FILE_NAME_LEN];
    UINT1               au1TmpFileName[ISS_CONFIG_FILE_NAME_LEN];
    tSNMP_OCTET_STRING_TYPE SaveCfgFile;
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE sSrvAddr;
    UINT1               au1Addr[DNS_MAX_QUERY_LEN];
    tSNMP_OCTET_STRING_TYPE RestoreCfgFile;

    tDNSResolvedIpInfo  ResolvedIpInfo;
    UINT1               u1TempLen = 0;
    INT4                i4RetVal = 0;

    MEMSET (au1FileName, 0, ISS_CONFIG_FILE_NAME_LEN);
    MEMSET (au1TmpFileName, 0, ISS_CONFIG_FILE_NAME_LEN);
    MEMSET (au1Addr, 0, DNS_MAX_QUERY_LEN);

    RestoreCfgFile.pu1_OctetList = &au1TmpFileName[0];

    nmhGetIssConfigRestoreFileName (&RestoreCfgFile);
    SPRINTF ((CHR1 *) au1FileName, "%s%s", FLASH, RestoreCfgFile.pu1_OctetList);

    MEMSET (au1Addr, 0, DNS_MAX_QUERY_LEN);
    MEMSET (&ResolvedIpInfo, 0, sizeof (tDNSResolvedIpInfo));
    MEMSET (au1FileName, 0, ISS_CONFIG_FILE_NAME_LEN);
    MEMSET (au1TmpFileName, 0, ISS_CONFIG_FILE_NAME_LEN);

    SaveCfgFile.pu1_OctetList = &au1TmpFileName[0];
    if (u4Mode != ISS_FLASH_SAVE)
    {
        if (IpAddress.u1Afi != IPVX_DNS_FAMILY)
        {
            MEMCPY (au1Addr, IpAddress.au1Addr, IPVX_MAX_INET_ADDR_LEN);
            sSrvAddr.i4_Length = IpAddress.u1AddrLen;
        }
        else
        {
            u1TempLen = STRLEN (pu1HostName);
            STRNCPY (au1Addr, pu1HostName, u1TempLen);
            au1Addr[u1TempLen] = '\0';
            i4RetVal =
                FsUtlIPvXResolveHostName (au1Addr, DNS_BLOCK, &ResolvedIpInfo);
            if (i4RetVal == DNS_NOT_RESOLVED)
            {
                CliPrintf (CliHandle, "\n%%Cannot resolve the host name \n");
                return (CLI_FAILURE);
                /*not resolved */
            }
            else if (i4RetVal == DNS_IN_PROGRESS)
            {
                CliPrintf (CliHandle,
                           "\n%% Host name resolution in Progress \n");
                return (CLI_FAILURE);
                /*in progress */
            }
            else if (i4RetVal == DNS_CACHE_FULL)
            {
                CliPrintf (CliHandle,
                           "\n%%DNS cache full, cannot resolve at the moment");
                return (CLI_FAILURE);
                /*cache full */
            }

            /* If the hostname resolves to both IPv4 and IPv6 
             * address, IPv6 address is considered */
            MEMSET (au1Addr, 0, DNS_MAX_QUERY_LEN);
            if (ResolvedIpInfo.Resolv6Addr.u1AddrLen != 0)
            {
                MEMCPY (au1Addr, ResolvedIpInfo.Resolv6Addr.au1Addr,
                        IPVX_IPV6_ADDR_LEN);
                MEMCPY (IpAddress.au1Addr, ResolvedIpInfo.Resolv6Addr.au1Addr,
                        IPVX_IPV6_ADDR_LEN);
                sSrvAddr.i4_Length = (INT4) IPVX_IPV6_ADDR_LEN;
                IpAddress.u1Afi = (UINT1) IPVX_ADDR_FMLY_IPV6;
                IpAddress.u1AddrLen = (UINT1) IPVX_IPV6_ADDR_LEN;
            }
            else
            {
                MEMCPY (au1Addr, ResolvedIpInfo.Resolv4Addr.au1Addr,
                        IPVX_IPV4_ADDR_LEN);

                MEMCPY (IpAddress.au1Addr, ResolvedIpInfo.Resolv4Addr.au1Addr,
                        IPVX_IPV4_ADDR_LEN);
                sSrvAddr.i4_Length = (INT4) IPVX_IPV4_ADDR_LEN;
                IpAddress.u1Afi = (UINT1) IPVX_ADDR_FMLY_IPV4;
                IpAddress.u1AddrLen = (UINT1) IPVX_IPV4_ADDR_LEN;
            }
        }
        sSrvAddr.pu1_OctetList = au1Addr;
        sSrvAddr.i4_Length = IpAddress.u1AddrLen;

        if (nmhTestv2IssConfigSaveIpvxAddr (&u4ErrorCode, &sSrvAddr)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid IP-Address \r\n");
            return (CLI_FAILURE);
        }
    }

    nmhGetIssConfigSaveFileName (&SaveCfgFile);
    SPRINTF ((CHR1 *) au1FileName, "%s%s", FLASH, SaveCfgFile.pu1_OctetList);

    if (u4Mode == ISS_FLASH_SAVE)
    {
        SPRINTF ((CHR1 *) au1TmpFileName, "%s%s", FLASH, pu1DestFile);

        if (issStatStartupFile (au1FileName) == ISS_FAILURE)

        {
            CliPrintf (CliHandle, "\r%% Startup File does not exist \r\n");
            return CLI_FAILURE;
        }

        issCopyLocalFile (au1FileName, au1TmpFileName);

    }
    else if (u4Mode == ISS_REMOTE_TFTP_SAVE)
    {
        if (tftpcSendFile (IpAddress, pu1DestFile, au1FileName) != TFTPC_OK)
        {
            CliPrintf (CliHandle, "\r%% TFTP Failed \r\n");
            return CLI_FAILURE;
        }
    }
#ifdef SSH_WANTED
    else if (u4Mode == ISS_REMOTE_SFTP_SAVE)
    {
        if (sftpcArSendFile (pu1UserName, pu1Passwd, IpAddress, pu1DestFile,
                             au1FileName) != SFTP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% SFTP Failed \r\n");
            return CLI_FAILURE;
        }
    }
#else
    UNUSED_PARAM (pu1UserName);
    UNUSED_PARAM (pu1Passwd);
    UNUSED_PARAM (pu1HostName);
#endif
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliIssLogs                               */
/*  Description     : This function is used to copy the        */
/*                    Log file to a remote site                */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    pu1FileName - File Name                  */
/*                    IpAddress - IP address                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliIssCopyLogs (tCliHandle CliHandle, UINT1 *pu1FileName, tIPvXAddr IpAddress,
                UINT1 *pu1HostName, UINT1 *pu1UserName,
                UINT1 *pu1Passwd, UINT4 u4TransferMode, UINT1 u1Status)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE sIpAddress;
    tSNMP_OCTET_STRING_TYPE sName;
    tSNMP_OCTET_STRING_TYPE SnmpIssUserName;
    tSNMP_OCTET_STRING_TYPE SnmpIssPasswd;
    UINT1               au1ErrorMsg[ISS_ERR_BUF_LEN] = "";
    UINT1               au1IpAddr[DNS_MAX_QUERY_LEN];
    UINT1               u1TempLen = 0;
    tDNSResolvedIpInfo  ResolvedIpInfo;
    INT4                i4RetVal = 0;

    MEMSET (&ResolvedIpInfo, 0, sizeof (tDNSResolvedIpInfo));
    MEMSET (au1IpAddr, 0, DNS_MAX_QUERY_LEN);

    if (CliIssCheckMsrStatus (CliHandle) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (IpAddress.u1AddrLen > IPVX_MAX_INET_ADDR_LEN)
    {
        return (CLI_FAILURE);
    }

    sIpAddress.i4_Length = IpAddress.u1AddrLen;
    sIpAddress.pu1_OctetList = au1IpAddr;
    if (IpAddress.u1Afi != IPVX_DNS_FAMILY)
    {
        MEMCPY (sIpAddress.pu1_OctetList, IpAddress.au1Addr,
                IPVX_MAX_INET_ADDR_LEN);
    }
    else
    {
        u1TempLen = (UINT1) STRLEN (pu1HostName);
        STRNCPY (sIpAddress.pu1_OctetList, pu1HostName, u1TempLen);
        sIpAddress.pu1_OctetList[u1TempLen] = '\0';
        i4RetVal =
            FsUtlIPvXResolveHostName (sIpAddress.pu1_OctetList, DNS_BLOCK,
                                      &ResolvedIpInfo);
        if (i4RetVal == DNS_NOT_RESOLVED)
        {
            CliPrintf (CliHandle, "\n%%Cannot resolve the host name \n");
            return (CLI_FAILURE);
            /*not resolved */
        }
        else if (i4RetVal == DNS_IN_PROGRESS)
        {
            CliPrintf (CliHandle, "\n%% Host name resolution in Progress \n");
            return (CLI_FAILURE);
            /*in progress */
        }
        else if (i4RetVal == DNS_CACHE_FULL)
        {
            CliPrintf (CliHandle,
                       "\n%%DNS cache full, cannot resolve at the moment");
            return (CLI_FAILURE);
            /*cache full */
        }

        /* If the hostname resolves to both IPv4 and IPv6 
         * address, IPv6 address is considered */
        MEMSET (sIpAddress.pu1_OctetList, 0, DNS_MAX_QUERY_LEN);

        if (ResolvedIpInfo.Resolv6Addr.u1AddrLen != 0)
        {
            MEMCPY (sIpAddress.pu1_OctetList,
                    ResolvedIpInfo.Resolv6Addr.au1Addr, IPVX_IPV6_ADDR_LEN);
            IpAddress.u1Afi = (UINT1) IPVX_ADDR_FMLY_IPV6;
            sIpAddress.i4_Length = (INT4) IPVX_IPV6_ADDR_LEN;
        }
        else
        {
            MEMCPY (sIpAddress.pu1_OctetList,
                    ResolvedIpInfo.Resolv4Addr.au1Addr, IPVX_IPV4_ADDR_LEN);
            IpAddress.u1Afi = (UINT1) IPVX_ADDR_FMLY_IPV4;
            sIpAddress.i4_Length = (INT4) IPVX_IPV4_ADDR_LEN;
        }
    }

    if (nmhTestv2IssUploadLogFileToIpAddrType (&u4ErrorCode,
                                               IpAddress.u1Afi) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2IssUploadLogFileToIpvx (&u4ErrorCode, &sIpAddress) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid IP-Address \r\n");
        return CLI_FAILURE;
    }

    sName.i4_Length = (INT4) STRLEN (pu1FileName);
    sName.pu1_OctetList = pu1FileName;

    if (nmhTestv2IssLogFileName (&u4ErrorCode, &sName) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Filename \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetIssUploadLogFileToIpAddrType (IpAddress.u1Afi) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetIssUploadLogFileToIpvx (&sIpAddress) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetIssLogFileName (&sName) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhTestv2IssUploadLogTransferMode (&u4ErrorCode,
                                           (INT4) u4TransferMode) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid TransferMode \r\n");
        return CLI_FAILURE;

    }
    nmhSetIssUploadLogTransferMode ((INT4) u4TransferMode);
    if (u4TransferMode != ISS_TFTP_TRANSFER_MODE)
    {
        /* Checking for username */
        SnmpIssUserName.i4_Length = (INT4) STRLEN (pu1UserName);
        SnmpIssUserName.pu1_OctetList = pu1UserName;
        if (nmhTestv2IssUploadLogUserName (&u4ErrorCode, &SnmpIssUserName) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid Username \r\n");
            return CLI_FAILURE;
        }
        nmhSetIssUploadLogUserName (&SnmpIssUserName);

        /* Checking for password */
        SnmpIssPasswd.i4_Length = (INT4) STRLEN (pu1Passwd);
        SnmpIssPasswd.pu1_OctetList = pu1Passwd;
        if (nmhTestv2IssUploadLogPasswd (&u4ErrorCode,
                                         &SnmpIssPasswd) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid Password \r\n");
            return CLI_FAILURE;
        }
        nmhSetIssUploadLogPasswd (&SnmpIssPasswd);
    }

    if (u4TransferMode == ISS_TFTP_TRANSFER_MODE)
    {
        if (IssTftpUploadLogs (u1Status, au1ErrorMsg) != ISS_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Log Upload Failed [%s]\r\n",
                       au1ErrorMsg);
            return CLI_FAILURE;
        }
        else
        {
            CliPrintf (CliHandle, "\r Log Upload Successful\r\n");
            return CLI_SUCCESS;
        }
    }
#ifdef SSH_WANTED
    else
    {
        if (IssSftpUploadLogs (u1Status, au1ErrorMsg) != ISS_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Log Upload Failed [%s]\r\n",
                       au1ErrorMsg);
            return CLI_FAILURE;
        }
        else
        {

            CliPrintf (CliHandle, "\r Log Upload Successful\r\n");
            return CLI_SUCCESS;
        }
    }
#endif
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliIssImageDownload                      */
/*  Description     : This function is used to copy the        */
/*                    image file from  a remote site           */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    pu1FileName - File Name                  */
/*                    IpAddress - IP address                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliIssImageDownload (tCliHandle CliHandle,
                     UINT1 *pu1FileName, tIPvXAddr IpAddress,
                     UINT1 *pu1HostName, UINT1 *pu1UserName,
                     UINT1 *pu1Passwd, UINT4 u4Type)
{
    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE sIpAddress;
    tSNMP_OCTET_STRING_TYPE sName;
    tSNMP_OCTET_STRING_TYPE sUserName;
    tSNMP_OCTET_STRING_TYPE sPasswd;
    UINT1               au1FileName[ISS_CONFIG_FILE_NAME_LEN];
    UINT1               au1TmpFileName[ISS_CONFIG_FILE_NAME_LEN];
    UINT1               au1ErrorMsg[ISS_ERR_BUF_LEN] = "";
    UINT1               au1IpAddr[DNS_MAX_QUERY_LEN];
    UINT4               u4Len = 0;
    tDNSResolvedIpInfo  ResolvedIpInfo;
    INT4                i4RetVal = 0;

    MEMSET (&ResolvedIpInfo, 0, sizeof (tDNSResolvedIpInfo));
    MEMSET (au1IpAddr, 0, DNS_MAX_QUERY_LEN);

    sName.i4_Length = (INT4) STRLEN (pu1FileName);
    sName.pu1_OctetList = pu1FileName;

    if (u4Type != ISS_FLASH)
    {
        if (CliIssCheckMsrStatus (CliHandle) == CLI_FAILURE)
        {
            return (CLI_FAILURE);
        }

        u4Len = (UINT4) STRLEN (pu1HostName);
        if (IpAddress.u1AddrLen > IPVX_MAX_INET_ADDR_LEN ||
            u4Len > DNS_MAX_QUERY_LEN)
        {
            return (CLI_FAILURE);
        }

        sIpAddress.i4_Length = (INT4) IpAddress.u1AddrLen;
        sIpAddress.pu1_OctetList = au1IpAddr;

        if (IpAddress.u1Afi != IPVX_DNS_FAMILY)
        {
            sIpAddress.i4_Length = (INT4) IpAddress.u1AddrLen;
            MEMCPY (sIpAddress.pu1_OctetList, IpAddress.au1Addr,
                    IPVX_MAX_INET_ADDR_LEN);
        }
        else
        {
            sIpAddress.i4_Length = (INT4) STRLEN (pu1HostName);
            STRNCPY (sIpAddress.pu1_OctetList, pu1HostName,
                     sIpAddress.i4_Length);
            sIpAddress.pu1_OctetList[sIpAddress.i4_Length] = '\0';
            i4RetVal =
                FsUtlIPvXResolveHostName (sIpAddress.pu1_OctetList,
                                          DNS_BLOCK, &ResolvedIpInfo);
            if (i4RetVal == DNS_NOT_RESOLVED)
            {
                CliPrintf (CliHandle, "\n%%Cannot resolve the host name \n");
                return (CLI_FAILURE);
                /*not resolved */
            }
            else if (i4RetVal == DNS_IN_PROGRESS)
            {
                CliPrintf (CliHandle,
                           "\n%% Host name resolution in Progress \n");
                return (CLI_FAILURE);
                /*in progress */
            }
            else if (i4RetVal == DNS_CACHE_FULL)
            {
                CliPrintf (CliHandle,
                           "\n%%DNS cache full, cannot resolve at the moment");
                return (CLI_FAILURE);
                /*cache full */
            }

            /* If the hostname resolves to both IPv4 and IPv6 
             * address, IPv6 address is considered */
            MEMSET (sIpAddress.pu1_OctetList, 0, DNS_MAX_QUERY_LEN);
            if (ResolvedIpInfo.Resolv6Addr.u1AddrLen != 0)
            {
                sIpAddress.i4_Length = (INT4) IPVX_IPV6_ADDR_LEN;
                MEMCPY (sIpAddress.pu1_OctetList,
                        ResolvedIpInfo.Resolv6Addr.au1Addr, IPVX_IPV6_ADDR_LEN);
                IpAddress.u1Afi = (UINT1) IPVX_ADDR_FMLY_IPV6;
            }
            else
            {
                sIpAddress.i4_Length = (INT4) IPVX_IPV4_ADDR_LEN;
                MEMCPY (sIpAddress.pu1_OctetList,
                        ResolvedIpInfo.Resolv4Addr.au1Addr, IPVX_IPV4_ADDR_LEN);
                IpAddress.u1Afi = (UINT1) IPVX_ADDR_FMLY_IPV4;
            }
        }

        if (nmhTestv2IssDlImageFromIpAddrType (&u4ErrCode,
                                               IpAddress.u1Afi) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhTestv2IssDlImageFromIpvx (&u4ErrCode, &sIpAddress) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid IP-Address\r\n");
            return CLI_FAILURE;
        }

        if (nmhTestv2IssDlImageName (&u4ErrCode, &sName) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Invalid Imagename\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetIssDlImageFromIpAddrType (IpAddress.u1Afi) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetIssDlImageFromIpvx (&sIpAddress) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetIssDlImageName (&sName) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (u4Type == ISS_SFTP)
        {
            /* Checking for Usermame */
            sUserName.i4_Length = (INT4) STRLEN (pu1UserName);
            sUserName.pu1_OctetList = pu1UserName;
            if (nmhTestv2IssDownLoadUserName (&u4ErrCode,
                                              &sUserName) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid Download Username\r\n");
                return CLI_FAILURE;

            }
            nmhSetIssDownLoadUserName (&sUserName);
            /* Checking for password */
            sPasswd.i4_Length = (INT4) STRLEN (pu1Passwd);
            sPasswd.pu1_OctetList = pu1Passwd;
            if (nmhTestv2IssDownLoadPassword (&u4ErrCode,
                                              &sPasswd) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid Download Password\r\n");
                return CLI_FAILURE;
            }
            nmhSetIssDownLoadPassword (&sPasswd);
#ifdef SSH_WANTED
            /* Starts Downloading using SFTP */
            if (IssSftpImageDownload (au1ErrorMsg) != ISS_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\n%% Image Download Failed [%s]\r\n",
                           au1ErrorMsg);
                return CLI_FAILURE;
            }
            else
            {
                u4Len =
                    ((STRLEN (IssGetImageName ()) <
                      (sizeof (au1FileName) -
                       1)) ? STRLEN (IssGetImageName ()) : (sizeof (au1FileName)
                                                            - 1));
                ISS_STRNCPY (au1FileName, IssGetImageName (), u4Len);
                au1FileName[u4Len] = '\0';
                u4Len =
                    ((STRLEN (IssGetDlImageName ()) <
                      (sizeof (au1TmpFileName) -
                       1)) ? STRLEN (IssGetDlImageName ())
                     : (sizeof (au1TmpFileName) - 1));
                ISS_STRNCPY (au1TmpFileName, IssGetDlImageName (), u4Len);
                au1TmpFileName[u4Len] = '\0';
                if (issMoveLocalFile (au1TmpFileName, au1FileName) !=
                    ISS_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r\n%% Unable to overwrite image \r\n");
                    return CLI_FAILURE;
                }
                else
                {

                    CliPrintf (CliHandle, "\r\nImage Download Successful\r\n");
                    IssCustSystemRestart ();
                }
            }
#endif

        }
        else if (u4Type == ISS_TFTP)
        {
            /* Starts Downloading using tftp */
            if (IssTftpImageDownload (au1ErrorMsg) != ISS_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\n%% Image Download Failed [%s]\r\n",
                           au1ErrorMsg);
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle, "\r\nImage Download Successful\r\n");
            IssCustSystemRestart ();
        }
    }
    else
    {
        SPRINTF ((CHR1 *) au1FileName, "%s%s", FLASH, pu1FileName);
        u4Len = ((STRLEN (IssGetDlImageName ()) < (sizeof (au1TmpFileName) - 1))
                 ? STRLEN (IssGetDlImageName ()) : (sizeof (au1TmpFileName) -
                                                    1));
        ISS_STRNCPY (au1TmpFileName, IssGetDlImageName (), u4Len);
        au1TmpFileName[u4Len] = '\0';
        issCopyLocalFile (au1FileName, au1TmpFileName);
    }
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliSetIssSpeed                           */
/*  Description     : This function is used to set the speed   */
/*                    speed of an interace                     */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4IfIndex   - Interface Index            */
/*                    u4Speed     - Interface Speed            */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliSetIssSpeed (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4Speed)
{

    UINT4               u4ErrCode;
    INT4                i4CurrentSpeed = 0;
    INT1                i1RetValue = 0;

    UNUSED_PARAM (i1RetValue);

    i1RetValue = nmhGetIssPortCtrlSpeed ((INT4) u4IfIndex, &i4CurrentSpeed);

    if (nmhTestv2IssPortCtrlSpeed (&u4ErrCode,
                                   (INT4) u4IfIndex,
                                   (INT4) u4Speed) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
#ifdef LA_WANTED

    if ((LaIsPortInPortChannel (u4IfIndex)
         == L2IWF_SUCCESS) && (i4CurrentSpeed != (INT4) u4Speed))
    {
        CliPrintf (CliHandle,
                   "\rChanging the speed for ports belonging to port channel"
                   " causes the link state to Down\r\n");
    }
#endif
    if (nmhSetIssPortCtrlSpeed (u4IfIndex, u4Speed) == SNMP_FAILURE)
    {
        /* Speed cannot be set if auto-negotiation is enabled */
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;

}

/***************************************************************/
/*  Function Name   : CliSetIssDuplexity                       */
/*  Description     : This function is used to set the         */
/*                    duplexity for an interace                */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4IfIndex   - Interface Index            */
/*                    u4Duplexity - Duplexity                  */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliSetIssDuplexity (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4Duplexity)
{
    UINT4               u4ErrCode;

    if (nmhTestv2IssPortCtrlDuplex
        (&u4ErrCode, (INT4) u4IfIndex, (INT4) u4Duplexity) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIssPortCtrlDuplex (u4IfIndex, u4Duplexity) == SNMP_FAILURE)
    {
        /*  Duplexity cannot be set if auto-negotiation is enabled */
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;

}

/***************************************************************/
/*  Function Name   : CliSetIssNegotiation                     */
/*  Description     : This function is used to set the         */
/*                    auto-negotiation status  for an interace */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4IfIndex   - Interface Index            */
/*                    u4NegStatus - Auto-negotiation status    */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliSetIssNegotiation (tCliHandle CliHandle, UINT4 u4IfIndex, UINT4 u4NegStatus)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2IssPortCtrlMode
        (&u4ErrorCode, (INT4) u4IfIndex, (INT4) u4NegStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetIssPortCtrlMode (u4IfIndex, u4NegStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliMibSave                               */
/*  Description     : This function is used to set the         */
/*                    config save                              */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliMibSave (tCliHandle CliHandle, UINT4 u4Mode)
{
    INT4                i4MibSaveResult;
    INT4                i4RemoteSaveResult = 1;
    UINT1               au1ErrorMsg[] =
        "\r                                                              \r";

    /* Get the status of mib save operation..
     * If it is already in progress flag an error
     */
    if (CliIssCheckMsrStatus (CliHandle) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }
    /* Triggering MSR task */
    gu1IssUlLogFlag = UNSET_FLAG;

    if (nmhSetIssInitiateConfigSave (ISS_TRUE) != SNMP_SUCCESS)
    {
        if (u4Mode == ISS_REMOTE_TFTP_SAVE)
        {
            CliPrintf (CliHandle,
                       "\r%% Cannot trigger TFTP. Verify Remote IP and "
                       "File Name\r\n");
        }
        else if (u4Mode == ISS_REMOTE_SFTP_SAVE)
        {
            CliPrintf (CliHandle,
                       "\r%% Cannot trigger SFTP. Verify Remote IP, User Name, "
                       "Password and File Name\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "\r%% Cannot save now.\r\n");
        }
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\rBuilding configuration ...\r\n");

    while (1)
    {
        OsixTskDelay (ISS_LOOP_DURATION * SYS_TIME_TICKS_IN_A_SEC);

        /* Get the status of Remote save operation */

        nmhGetIssRemoteSaveStatus (&i4RemoteSaveResult);
        nmhGetIssConfigSaveStatus (&i4MibSaveResult);

        /* Check the progress of backup operation. 
         * Break from the while loop if the operation 
         * is complete or on time out */
        if (u4Mode == ISS_REMOTE_TFTP_SAVE || u4Mode == ISS_REMOTE_SFTP_SAVE)
        {
            if ((i4RemoteSaveResult == LAST_MIB_SAVE_SUCCESSFUL) ||
                (i4RemoteSaveResult == LAST_MIB_SAVE_FAILED))
            {
                break;
            }
        }
        else
        {
            if ((i4MibSaveResult == LAST_MIB_SAVE_SUCCESSFUL) ||
                (i4MibSaveResult == LAST_MIB_SAVE_FAILED))
            {
                break;
            }
        }
    }
    CliPrintf (CliHandle, (CONST CHR1 *) au1ErrorMsg);

    /* RemoteSave operation may be success or failure */

    if (u4Mode == ISS_REMOTE_TFTP_SAVE || u4Mode == ISS_REMOTE_SFTP_SAVE)
    {
        if (i4RemoteSaveResult == LAST_MIB_SAVE_SUCCESSFUL)
        {
            CliPrintf (CliHandle, "[OK]\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "\r%% Error writing. [Failed]\r\n");
            return CLI_FAILURE;
        }

    }
    else
    {
        if (i4MibSaveResult == LAST_MIB_SAVE_SUCCESSFUL)
        {
            CliPrintf (CliHandle, "[OK]\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "\r%% Error writing. [Failed]\r\n");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliIssSetCliSerialConsolePrompt          */
/*  Description     : This function is used to configure if    */
/*                    CLI console prompt would be available at */
/*                    the serial console                       */
/*  Input(s)        : u4CliSerialConsole - Value indicating    */
/*                    requirement of CLI console session       */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliIssSetCliSerialConsolePrompt (tCliHandle CliHandle, UINT4 u4CliSerialConsole)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2IssNoCliConsole (&u4ErrorCode, (INT4) u4CliSerialConsole)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_ISS_INVALID_CONSOLE_OPTION);
        return CLI_FAILURE;
    }

    if (nmhSetIssNoCliConsole ((INT4) u4CliSerialConsole) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

#ifdef NPAPI_WANTED
/***************************************************************/
/*  Function Name   : cliSetIssCutThrough                      */
/*  Description     : This function is used to configure the   */
/*                    switchMode to cut-through                */
/*  Input(s)        : cliHandle - CLI HANDLE                   */
/*                                                           */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliSetIssCutThrough (tCliHandle CliHandle)
{
    UINT4               u4ErrorCode = 0;

    if (ISS_CUT_THROUGH == gi4IssSwitchModeType)
    {
        CliPrintf (CliHandle, "\r\n..Switchmode is already CUT-THROUGH..\r\n");
        return CLI_FAILURE;

    }
    if (nmhTestv2IssSwitchModeType (&u4ErrorCode, ISS_CUT_THROUGH)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIssSwitchModeType (ISS_CUT_THROUGH) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle,
               "\r\n..Changing switchmode to CUT-THROUGH was successful..\r\n");
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : cliSetIssNoCutThrough                    */
/*  Description     : This function is used to configure the   */
/*                    switchMode to store-forward              */
/*  Input(s)        : CliHandle - CLI HANDLE                   */
/*                    mode type selected                       */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliSetIssNoCutThrough (tCliHandle CliHandle)
{
    UINT4               u4ErrorCode = 0;

    if (ISS_STORE_FORWARD == gi4IssSwitchModeType)
    {
        CliPrintf (CliHandle,
                   "\r\n..Switchmode is already STORE-FORWARD..\r\n");
        return CLI_FAILURE;

    }
    if (nmhTestv2IssSwitchModeType (&u4ErrorCode, ISS_STORE_FORWARD)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIssSwitchModeType (ISS_STORE_FORWARD) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle,
               "\r\n..Changing switchmode to STORE-FORWARD was successful..\r\n");
    return CLI_SUCCESS;
}

#endif

/****************************************************************************/
/*  Function Name   : CliIssShowSwitchingMode                               */
/*  Description     : This function is used to show the configured          */
/*                    switching mode in the hardware                        */
/*  Input(s)        : CliHandle  - CLI Handle                               */
/*                                                                          */
/*  Output(s)       : None                                                  */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                               */
/****************************************************************************/

INT4
CliIssShowSwitchModeType (tCliHandle CliHandle)
{
    INT4                i4IssSwitchModeType = ISS_STORE_FORWARD;

    nmhGetIssSwitchModeType (&i4IssSwitchModeType);
    CliPrintf (CliHandle, "\r\n Switching Mode Type:");
    switch (i4IssSwitchModeType)
    {
        case ISS_STORE_FORWARD:

            CliPrintf (CliHandle, "STORE-FORWARD\r\n");
            break;

        case ISS_CUT_THROUGH:

            CliPrintf (CliHandle, "CUT-THROUGH\r\n");
            break;

        default:
            break;

    }
    return (CLI_SUCCESS);
}

/****************************************************************************/
/*  Function Name   : SwitchModeTypeShowRunningConfig                       */
/*  Description     : This function is used to show the running-config      */
/*                    for switching mode in the hardware                    */
/*  Input(s)        : CliHandle  - CLI Handle                               */
/*                                                                          */
/*  Output(s)       : None                                                  */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                               */
/****************************************************************************/

INT4
SwitchModeTypeShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4IssSwitchModeType = ISS_STORE_FORWARD;

    nmhGetIssSwitchModeType (&i4IssSwitchModeType);
    if (ISS_CUT_THROUGH == i4IssSwitchModeType)
    {
        CliPrintf (CliHandle, "\rswitching-mode cut-through\n");
        return (CLI_SUCCESS);
    }
    return (CLI_FAILURE);
}

/***************************************************************/
/*  Function Name   : CliSetIssDefaultVlanId                   */
/*  Description     : This function is used to configure the   */
/*                    CLI Default VLAN Identifier              */
/*  Input(s)        : u4CliDefaultVlanId - Value indicating       */
/*                    default VLAN Identifier                  */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliSetIssDefaultVlanId (tCliHandle CliHandle, UINT4 u4CliDefaultVlanId)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2IssDefaultVlanId (&u4ErrorCode, (INT4) u4CliDefaultVlanId)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIssDefaultVlanId ((INT4) u4CliDefaultVlanId) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle,
               "<Information> Changing default VLAN Id is successful.\
                                Comes into effect after reboot\r\n");
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliSetIssSystemTmrSpeed                  */
/*  Description     : This function is used to configure the   */
/*                    system timer speed                       */
/*  Input(s)        : u4SystemTimerSpeed - Timer Speed         */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliSetIssSystemTmrSpeed (tCliHandle CliHandle, UINT4 u4SystemTimerSpeed)
{
    UINT4               u4ErrorCode;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2IssSystemTimerSpeed (&u4ErrorCode, u4SystemTimerSpeed)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIssSystemTimerSpeed (u4SystemTimerSpeed) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliSetIssFrontPanelPortCount             */
/*  Description     : This function is used to configure the   */
/*                    number of physical ports present for     */
/*                    normal switching/routing                 */
/*  Input(s)        : u4CliFrontPanelPortCount -               */
/*                     Value indicating                        */
/*                    configured Front panel ports             */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliSetIssFrontPanelPortCount (tCliHandle CliHandle,
                              UINT4 u4CliFrontPanelPortCount)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2IssFrontPanelPortCount (&u4ErrorCode,
                                         (INT4) u4CliFrontPanelPortCount) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhSetIssFrontPanelPortCount ((INT4) u4CliFrontPanelPortCount);

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliIssShowSysInfo                        */
/*  Description     : This function is used to display System  */
/*                    Information                              */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliIssShowSysInfo (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE HwVersion;
    tSNMP_OCTET_STRING_TYPE FirmVersion;
    tSNMP_OCTET_STRING_TYPE SwitchName;
#ifdef SNMP_3_WANTED
    tSNMP_OCTET_STRING_TYPE *pSysContact = NULL;
    tSNMP_OCTET_STRING_TYPE *pSysName = NULL;
    tSNMP_OCTET_STRING_TYPE *pSysLocation = NULL;
#endif
    INT4                i4LoggingOpt;
    INT4                i4LoginAuth;
    INT4                i4ConfigSaveStat;
    INT4                i4RemSaveStat;
    INT4                i4ConfResStat;
    INT4                i4RoutingFlag;
    UINT1               au1HwVersion[ISS_STR_LEN + 1];
    UINT1               au1FirmVersion[ISS_STR_LEN + 1];
    UINT1               au1SwitchName[ISS_STR_LEN + 1];
    UINT1               au1HwPartNum[ISS_STR_LEN + 1];
    UINT1               au1SwVersion[ISS_STR_LEN + 1];
    UINT1               au1SwSerialNum[ISS_STR_LEN + 1];
    INT4                i4TrafficControl = ISS_ZERO_ENTRY;
    UINT4               u4Min = 0, u4Sec = 0, u4Hrs = 0;
    UINT4               u4Days = 0;
    UINT4               u4Seconds = 0;

    MEMSET (au1HwVersion, 0, ISS_STR_LEN + 1);
    MEMSET (au1FirmVersion, 0, ISS_STR_LEN + 1);
    MEMSET (au1SwitchName, 0, ISS_STR_LEN + 1);
    MEMSET (au1HwPartNum, 0, ISS_STR_LEN + 1);
    MEMSET (au1SwVersion, 0, ISS_STR_LEN + 1);
    MEMSET (au1SwSerialNum, 0, ISS_STR_LEN + 1);

    HwVersion.pu1_OctetList = &au1HwVersion[0];
    FirmVersion.pu1_OctetList = &au1FirmVersion[0];
    SwitchName.pu1_OctetList = &au1SwitchName[0];
#ifdef SNMP_3_WANTED
    pSysLocation = allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
    if (pSysLocation == NULL)
    {
        CliPrintf (CliHandle, "%% Unable to allocate memory\r\n");
        return CLI_FAILURE;
    }
    MEMSET (pSysLocation->pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

#endif

    nmhGetIssHardwareVersion (&HwVersion);
    nmhGetIssFirmwareVersion (&FirmVersion);
    MEMCPY (au1HwPartNum, IssGetHardwarePartNumber (),
            STRLEN (IssGetHardwarePartNumber ()));
    MEMCPY (au1SwSerialNum, IssGetSoftwareSerialNumber (),
            STRLEN (IssGetSoftwareSerialNumber ()));

    MEMCPY (au1SwVersion, IssGetSoftwareVersion (),
            STRLEN (IssGetSoftwareVersion ()));

    u4Seconds = OsixGetSysUpTime ();
    u4Sec = (UINT4) (u4Seconds % SECS_IN_MINUTE);
    u4Min = (UINT4) ((u4Seconds / SECS_IN_MINUTE) % MINUTES_IN_HOUR);
    u4Hrs = (UINT4) ((u4Seconds / SECS_IN_HOUR) % HOURS_IN_DAY);
    u4Days = (UINT4) (u4Seconds / SECS_IN_DAY);

    nmhGetIssSwitchName (&SwitchName);
#ifdef SNMP_3_WANTED
    pSysContact = allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
    if (pSysContact == NULL)
    {
        CliPrintf (CliHandle, "%% Unable to allocate memory\r\n");
        free_octetstring (pSysLocation);
        return CLI_FAILURE;
    }
    MEMSET (pSysContact->pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

    pSysName = allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
    if (pSysName == NULL)
    {
        CliPrintf (CliHandle, "%% Unable to allocate memory\r\n");
        free_octetstring (pSysLocation);
        return CLI_FAILURE;
    }
    MEMSET (pSysName->pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

    nmhGetSysContact (pSysContact);
    nmhGetSysName (pSysName);
    nmhGetSysLocation (pSysLocation);
#endif
    nmhGetIssLoggingOption (&i4LoggingOpt);
    nmhGetIssLoginAuthentication (&i4LoginAuth);
    nmhGetIssConfigSaveStatus (&i4ConfigSaveStat);
    nmhGetIssRemoteSaveStatus (&i4RemSaveStat);
    nmhGetIssConfigRestoreStatus (&i4ConfResStat);
    nmhGetIssMgmtInterfaceRouting (&i4RoutingFlag);
    nmhGetIssAclTrafficSeperationCtrl (&i4TrafficControl);
/* Since semaphore has been taken before the switch construct in 
 * cli_process_iss_cmd() */

    CliPrintf (CliHandle, "\r\n%-33s : %s\r\n", "Hardware Version",
               HwVersion.pu1_OctetList);

    CliPrintf (CliHandle, "%-33s : %s\r\n", "Software Version", au1SwVersion);

    CliPrintf (CliHandle, "%-33s : %s\r\n", "Firmware Version",
               FirmVersion.pu1_OctetList);

    CliPrintf (CliHandle, "\r\n%-33s : %s\r\n", "Hardware Serial Number",
               au1HwPartNum);

    CliPrintf (CliHandle, "%-33s : %s\r\n", "Software Serial Number",
               au1SwSerialNum);

    CliPrintf (CliHandle, "%-33s : %s\r\n", "Switch Name",
               SwitchName.pu1_OctetList);

#ifdef SNMP_3_WANTED
    CliPrintf (CliHandle, "%-33s : %s\r\n", "System Contact",
               pSysContact->pu1_OctetList);
    CliPrintf (CliHandle, "%-33s : %s\r\n", "System Name",
               pSysName->pu1_OctetList);

    CliPrintf (CliHandle, "%-33s : %s\r\n", "System Location",
               pSysLocation->pu1_OctetList);
    free_octetstring (pSysContact);
    free_octetstring (pSysName);
    free_octetstring (pSysLocation);
#endif

    if ((i4LoggingOpt) == CONSOLE_LOG)
    {
        CliPrintf (CliHandle, "%-33s : Console Logging\r\n", "Logging Option");
    }
    else if ((i4LoggingOpt) == INCORE_LOG)
    {
        CliPrintf (CliHandle, "%-33s : File Logging\r\n", "Logging Option");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Flash Logging\r\n", "Logging Option");
    }

    CliPrintf (CliHandle, "%-33s : %d Days, %d Hrs, %d Mins, %d Secs\r\n",
               "Device Uptime", u4Days, u4Hrs, u4Min, u4Sec);

    if (i4LoginAuth == LOCAL_LOGIN)
    {
        CliPrintf (CliHandle, "%-33s : Local\r\n", "Login Authentication Mode");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Remote\r\n",
                   "Login Authentication Mode");
    }

    if (i4ConfigSaveStat == MIB_SAVE_IN_PROGRESS)
    {
        CliPrintf (CliHandle, "%-33s : In Progress\r\n", "Config Save Status");
    }
    else if (i4ConfigSaveStat == LAST_MIB_SAVE_SUCCESSFUL)
    {
        CliPrintf (CliHandle, "%-33s : Successful\r\n", "Config Save Status");
    }
    else if (i4ConfigSaveStat == LAST_MIB_SAVE_FAILED)
    {
        CliPrintf (CliHandle, "%-33s : Failed\r\n", "Config Save Status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Not Initiated\r\n",
                   "Config Save Status");
    }

    if (i4RemSaveStat == MIB_SAVE_IN_PROGRESS)
    {
        CliPrintf (CliHandle, "%-33s : In Progress\r\n", "Remote Save Status");
    }
    else if (i4RemSaveStat == LAST_MIB_SAVE_SUCCESSFUL)
    {
        CliPrintf (CliHandle, "%-33s : Successful\r\n", "Remote Save Status");
    }
    else if (i4RemSaveStat == LAST_MIB_SAVE_FAILED)
    {
        CliPrintf (CliHandle, "%-33s : Failed\r\n", "Remote Save Status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Not Initiated\r\n",
                   "Remote Save Status");
    }

    if (i4ConfResStat == MIB_RESTORE_IN_PROGRESS)
    {
        CliPrintf (CliHandle, "%-33s : In Progress\r\n",
                   "Config Restore Status");
    }
    else if (i4ConfResStat == LAST_MIB_RESTORE_SUCCESSFUL)
    {
        CliPrintf (CliHandle, "%-33s : Successful\r\n",
                   "Config Restore Status");
    }
    else if (i4ConfResStat == LAST_MIB_RESTORE_FAILED)
    {
        CliPrintf (CliHandle, "%-33s : Failed\r\n", "Config Restore Status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Not Initiated\r\n",
                   "Config Restore Status");
    }

    if (i4TrafficControl == ACL_TRAFFICSEPRTN_CTRL_SYSTEM_DEFAULT)
    {
        CliPrintf (CliHandle, "%-33s : system-default\r\n",
                   "Traffic Separation Control");
    }
    else if (i4TrafficControl == ACL_TRAFFICSEPRTN_CTRL_USER_DEFINED)
    {
        CliPrintf (CliHandle, "%-33s : Userdefined\r\n",
                   "Traffic Separation Control");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : none\r\n", "Traffic Separation Control");
    }

    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/***************************************************************/
/*  Function Name   : CliIssShowAllSwitchInfo                  */
/*  Description     : This function is used to display all the */
/*                    switch informations.                     */
/*  Input(s)        : CliHandle   - CLI Handle                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliIssShowAllSwitchInfo (tCliHandle CliHandle)
{
    INT4                i4RetVal = CLI_SUCCESS;

    INT4                i4RoutingFlag = ISS_DISABLE;
    CliPrintf (CliHandle, "\r\n");

    i4RetVal = CliIssShowSwitchRAMThreshold (CliHandle);
    if (i4RetVal != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    i4RetVal = CliIssShowSwitchCPUThreshold (CliHandle);
    if (i4RetVal != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    i4RetVal = CliIssShowSwitchFanStatus (CliHandle);
    if (i4RetVal != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    i4RetVal = CliIssShowSwitchPowerStatus (CliHandle);
    if (i4RetVal != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    i4RetVal = CliIssShowSwitchTemperature (CliHandle);
    if (i4RetVal != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    i4RetVal = CliIssShowSwitchFlashThreshold (CliHandle);
    if (i4RetVal != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
    nmhGetIssMgmtInterfaceRouting (&i4RoutingFlag);
    if (i4RoutingFlag == ISS_ENABLE)
    {
        CliPrintf (CliHandle, "%-33s : Enabled\r\n", "Mgmt Port Routing");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : Disabled\r\n", "Mgmt Port Routing");
    }
    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "\r\n");

    return (CLI_SUCCESS);
}

/***************************************************************/
/*  Function Name   : CliIssShowSwitchTemperature              */
/*  Description     : This function is used to display Switch  */
/*                    temperature threshold information        */
/*  Input(s)        : CliHandle   - CLI Handle                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliIssShowSwitchTemperature (tCliHandle CliHandle)
{
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                i4MaxTemperature = 0;
    INT4                i4MinTemperature = 0;
    INT4                i4CurTemperature = 0;

    i4RetVal = nmhGetIssSwitchMaxThresholdTemperature (&i4MaxTemperature);
    if (i4RetVal != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    i4RetVal = nmhGetIssSwitchMinThresholdTemperature (&i4MinTemperature);
    if (i4RetVal != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    i4RetVal = nmhGetIssSwitchCurrentTemperature (&i4CurTemperature);
    if (i4RetVal != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Display Information */

    CliPrintf (CliHandle, "\r%-33s : %d%s\r\n", "Max Temperature",
               i4MaxTemperature, "C");

    CliPrintf (CliHandle, "\r%-33s : %d%s\r\n", "Min Temperature",
               i4MinTemperature, "C");

    CliPrintf (CliHandle, "\r%-33s : %d%s\r\n", "Current Temperature",
               i4CurTemperature, "C");

    return (CLI_SUCCESS);
}

/***************************************************************/
/*  Function Name   : CliIssShowSwitchFanStatus                */
/*  Description     : This function is used to display Switch  */
/*                    fan status(up/down).                     */
/*  Input(s)        : CliHandle   - CLI Handle                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliIssShowSwitchFanStatus (tCliHandle CliHandle)
{
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                i4FanIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4FanStatus;

    /* Display Information */

    i4RetVal = nmhGetFirstIndexIssSwitchFanTable (&i4FanIndex);
    if (i4RetVal != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    do
    {
        i4RetVal = nmhGetIssSwitchFanStatus (i4FanIndex, &i4FanStatus);
        if (i4RetVal != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (i4FanStatus == 0)
        {
            CliPrintf (CliHandle, "\r%-12s %-21d: %s\r\n", "Fan Status",
                       i4FanIndex, "Not-Operational");
        }
        else
        {
            CliPrintf (CliHandle, "\r%-12s %-21d: %s\r\n", "Fan Status",
                       i4FanIndex, "Operational");
        }

        i4RetVal = nmhGetNextIndexIssSwitchFanTable (i4FanIndex,
                                                     &i4NextIfIndex);

        i4FanIndex = i4NextIfIndex;
    }
    while (i4RetVal == SNMP_SUCCESS);

    return (CLI_SUCCESS);
}

/***************************************************************/
/*  Function Name   : CliIssShowSwitchRAMThreshold             */
/*  Description     : This function is used to display Switch  */
/*                    RAM threshold Information                */
/*  Input(s)        : CliHandle   - CLI Handle                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliIssShowSwitchRAMThreshold (tCliHandle CliHandle)
{
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                i4MaxRAMUsage = 0;
    INT4                i4CurRAMUsage = 0;

    i4RetVal = nmhGetIssSwitchMaxRAMUsage (&i4MaxRAMUsage);
    if (i4RetVal != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    i4RetVal = nmhGetIssSwitchCurrentRAMUsage (&i4CurRAMUsage);
    if (i4RetVal != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Display Information */

    CliPrintf (CliHandle, "\r%-33s : %d%%\r\n", "RAM Threshold", i4MaxRAMUsage);

    CliPrintf (CliHandle, "\r%-33s : %d%%\r\n", "Current RAM Usage",
               i4CurRAMUsage);

    return (CLI_SUCCESS);
}

/***************************************************************/
/*  Function Name   : CliIssShowSwitchCPUThreshold             */
/*  Description     : This function is used to display Switch  */
/*                    CPU threshold information                */
/*  Input(s)        : CliHandle   - CLI Handle                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliIssShowSwitchCPUThreshold (tCliHandle CliHandle)
{
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                i4MaxCPUUsage = 0;
    INT4                i4CurCPUUsage = 0;

    i4RetVal = nmhGetIssSwitchMaxCPUThreshold (&i4MaxCPUUsage);
    if (i4RetVal != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    i4RetVal = nmhGetIssSwitchCurrentCPUThreshold (&i4CurCPUUsage);
    if (i4RetVal != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Display Information */

    CliPrintf (CliHandle, "\r%-33s : %d%%\r\n", "CPU Threshold", i4MaxCPUUsage);

    CliPrintf (CliHandle, "\r%-33s : %d%%\r\n", "Current CPU Usage",
               i4CurCPUUsage);

    return (CLI_SUCCESS);
}

/***************************************************************/
/*  Function Name   : CliIssShowSwitchFlashThreshold           */
/*  Description     : This function is used to display Switch  */
/*                    flash threshold Information              */
/*  Input(s)        : CliHandle   - CLI Handle                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliIssShowSwitchFlashThreshold (tCliHandle CliHandle)
{
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                i4MaxFlashUsage = 0;
    INT4                i4CurFlashUsage = 0;

    i4RetVal = nmhGetIssSwitchMaxFlashUsage (&i4MaxFlashUsage);
    if (i4RetVal != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    i4RetVal = nmhGetIssSwitchCurrentFlashUsage (&i4CurFlashUsage);
    if (i4RetVal != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Display Information */

    CliPrintf (CliHandle, "\r%-33s : %d%%\r\n", "Flash Threshold",
               i4MaxFlashUsage);

    CliPrintf (CliHandle, "\r%-33s : %d%%\r\n", "Current Flash Usage",
               i4CurFlashUsage);

    return (CLI_SUCCESS);
}

/***************************************************************/
/*  Function Name   : CliIssShowSwitchPowerStatus              */
/*  Description     : This function is used to display Switch  */
/*                    power status                             */
/*  Input(s)        : CliHandle   - CLI Handle                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliIssShowSwitchPowerStatus (tCliHandle CliHandle)
{
    INT4                i4RetVal = SNMP_SUCCESS;
    INT4                i4PowerSurge = 0;
    INT4                i4PowerFailure = 0;
    INT4                i4CurPowerSupply = 0;

    i4RetVal = nmhGetIssSwitchPowerSurge (&i4PowerSurge);
    if (i4RetVal != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    i4RetVal = nmhGetIssSwitchPowerFailure (&i4PowerFailure);
    if (i4RetVal != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    i4RetVal = nmhGetIssSwitchCurrentPowerSupply (&i4CurPowerSupply);
    if (i4RetVal != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    /* Display Information */

    CliPrintf (CliHandle, "\r%-33s : %d%s\r\n", "Min power supply",
               i4PowerFailure, "v");

    CliPrintf (CliHandle, "\r%-33s : %d%s\r\n", "Max power supply",
               i4PowerSurge, "v");

    CliPrintf (CliHandle, "\r%-33s : %d%s\r\n", "Current power supply",
               i4CurPowerSupply, "v");

    return (CLI_SUCCESS);
}

/***************************************************************/
/*  Function Name   : CliIssShowSysAck                         */
/*  Description     : This function is used to display all     */
/*                    Open Souces used in this system          */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliIssShowSysAck (tCliHandle CliHandle)
{
    CliPrintf (CliHandle,
               "\r\n 1. SSH (Secure Shell)\n "
               "The SSH functionality in ISS is implemented using the open source\r\n "
               "software from http://www.openssh.org, developed by Theo de Raadt,\r\n "
               "Niels Provos, Markus Friedl, Bob Beck, Aaron Campbell and Dug Song.\r\n "
               "All copyrights listed at http://www.openssh.org/ apply. With respect to\r\n "
               "licensing terms, the same website explains the following:\r\n "
               "\"OpenSSH is developed by the OpenBSD Project. The software is\r\n "
               "developed in countries that permit cryptography export and is freely\r\n "
               "useable and re-useable by everyone under a BSD license.\"\r\n "
               "A copy of the license file is available at:\r\n "
               "http://www.mips.com/LicenseMapper/OpenBSD.html. The BSD license is\r\n "
               "also described at - http://www.openbsd.org/faq/faq1.html#WhatIs\r\n "
               "OpenSSH version used - 5.1\r\n\r\n ");
    CliPrintf (CliHandle,
               "\r\n 2. SSL (Secure Socket Layer)\r\n\r\n "
               "This product includes software developed by the OpenSSL Project\r\n "
               "for use in the OpenSSL Toolkit. (http://www.openssl.org/)\r\n\r\n "
               "This product includes cryptographic software written by Eric Young\r\n "
               "(eay@cryptsoft.com).  This product includes software written by Tim\r\n "
               "Hudson (tjh@cryptsoft.com).\r\n\r\n ");
    CliPrintf (CliHandle,
               "\r\n The SSL functionality in ISS is implemented using the open source\r\n "
               "software from http://www.openssl.org, which include software written by\r\n "
               "Eric A. Young and Tim J. Hudson. All copyrights listed at\r\n "
               "http://www.openssl.org/ apply. With respect to licensing terms, the same\r\n "
               "website explains the following:\r\n "
               "\"The OpenSSL toolkit is licensed under an Apache-style license, which\r\n "
               "basically means that you are free to get and use it for commercial and\r\n "
               "non-commercial purposes subject to some simple license conditions.\"\r\n "
               "A copy of the license file is available at:\r\n "
               "http://www.openssl.org/source/license.html.\r\n "
               "OpenSSL version used - 0.9.8i\r\n\r\n ");
    CliPrintf (CliHandle,
               "\r\n 3. For secure transfer of the software image or configuration file, ISS uses\r\n "
               "the SFTP (SSH File Transfer Protocol) from http://www.openssh.org,\r\n "
               "developed by Theo de Raadt, Niels Provos, Markus Friedl, Bob Beck,\r\n "
               "Aaron Campbell and Dug Song. All copyrights listed at\r\n "
               "http://www.openssh.org/ apply. With respect to licensing terms, the same\r\n "
               "website explains the following:\r\n "
               "-\"OpenSSH is developed by the OpenBSD Project. The software is\r\n "
               "developed in countries that permit cryptography export and is freely\r\n "
               "useable and re-useable by everyone under a BSD license.\"\r\n "
               "A copy of the license file is available at:\r\n "
               "http://www.mips.com/LicenseMapper/OpenBSD.html. The BSD license is\r\n "
               "also described at - http://www.openbsd.org/faq/faq1.html#WhatIs.\r\n "
               "OpenSSH version used - 5.1\r\n\r\n ");
    CliPrintf (CliHandle,
               "\r\n 4. Telnet Client\r\n "
               "The Telnet client functionality in ISS is implemented using the open\r\n "
               "source software PuTTY available at:\r\n "
               "http://www.chiark.greenend.org.uk/~sgtatham/putty/\r\n "
               "The PuTTY source code is distributed under the MIT license.\r\n "
               "A copy of the license file is available at:\r\n "
               "http://www.chiark.greenend.org.uk/~sgtatham/putty/licence.html\r\n "
               "PuTTY version used - 0.60\r\n\r\n ");
    CliPrintf (CliHandle,
               "\r\n 5. SSH Client\r\n "
               "The SSH client functionality in ISS is implemented using the open source\r\n "
               "software PuTTY available at:\r\n "
               "http://www.chiark.greenend.org.uk/~sgtatham/putty/ .\r\n "
               "The PuTTY source code is distributed under the MIT license.\r\n "
               "A copy of the license file is available at:\r\n "
               "http://www.chiark.greenend.org.uk/~sgtatham/putty/licence.html\r\n "
               "PuTTY version used - 0.60\r\n ");

#ifdef NETCONF_WANTED
    CliPrintf (CliHandle,
               "\r\n 6. OpenYuma Netconf Solution\r\n "
               "The Netconf support in ISS is implemented using the open source\r\n "
               "software Openyuma available at: https://github.com/OpenClovis/OpenYuma,\r\n "
               "authored by Andy Bierman <andy@yumaworks.com>. The Openyuma source code\r\n "
               "is distributed under the BSD Licence. A Copy of the license file is available at\r\n "
               "https://github.com/OpenClovis/OpenYuma/blob/master/netconf/doc/extra/COPYRIGHT\r\n "
               "Copyright:\r\n "
               "Copyright (C) 2008 - 2012 by Andy Bierman <andy@yumaworks.com>\r\n "
               "Copyright (C) 2012 by YumaWorks, Inc. <support@yumaworks.com>)\r\n "
               "Packaging:\r\n "
               "Copyright (C) 2012, YumaWorks, Inc. <support@yumaworks.com>\r\n "
               "released under the BSD license.\r\n ");
#endif

    return (CLI_SUCCESS);
}

/***************************************************************/
/*  Function Name   : CliIssShowNvRam                          */
/*  Description     : This function is used to display System  */
/*                    NVRAM Information                        */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliIssShowNvRam (tCliHandle CliHandle)
{
    CHR1               *pu1String;
    tMacAddr            BaseMac;
    tSNMP_OCTET_STRING_TYPE Name;
    tSNMP_OCTET_STRING_TYPE CfgSaveFile;
    tSNMP_OCTET_STRING_TYPE CfgResFile;
    tSNMP_OCTET_STRING_TYPE RmIfName;
    tSNMP_OCTET_STRING_TYPE sCfgSaveIpAddr;
    tSNMP_OCTET_STRING_TYPE HwVersion;
    tSNMP_OCTET_STRING_TYPE FirmVersion;
    tSNMP_OCTET_STRING_TYPE SwitchName;
    INT4                i4DefIpAddr;
    INT4                i4DefIpMask;
    INT4                i4DefIpCfgMode;
    INT4                i4CfgResOption;
    INT4                i4CfgSaveOption;
    INT4                i4PimMode;
    INT4                i4SnoopFwdMode;
    INT4                i4OobPresent;
    UINT4               u4BootCount;
    UINT4               u4IfIndex = 0;
    UINT4               u4CfgSaveIpAddr;
    UINT4               u4RmHbMode;
    UINT4               u4RmRType;
    UINT4               u4RmDType;
    UINT4               u4RmStackingInterfaceType = 0;
    tIPvXAddr           CfgSaveIpAddr;
    UINT4               u4FrontPanelPortCount = 0;
    UINT2               u2DefaultVlanId;
    UINT2               u2ColdStandby;
    UINT2               u2StackPortCount;
    INT4                i4CliSerialConsole;
    INT4                i4IpAllocProto;
    INT1               *piIfName;
    INT1               *pi1EngineID;
    UINT1               au1IfName[MAX_NAME_SIZE];
    UINT1               au1CfgSaveFile[MAX_FILENAME_SIZE];
    UINT1               au1CfgResFile[MAX_FILENAME_SIZE];
    UINT1               au1HwVersion[ISS_STR_LEN + 1];
    UINT1               au1FirmVersion[ISS_STR_LEN + 1];
    UINT1               au1SwitchName[ISS_STR_LEN + 1];
    UINT1               au1HwPartNum[ISS_STR_LEN + 1];
    UINT1               au1SwVersion[ISS_STR_LEN + 1];
    UINT1               au1SwSerialNum[ISS_STR_LEN + 1];
    UINT1               au1IfIP[MAX_NAME_SIZE];
    UINT1               au1Temp[MAX_NAME_SIZE];
    UINT1               au1RmIfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1              *pu1NpapiMode;
    UINT1               u1VrfUnqMacOption = ISS_VRF_UNQ_MAC_DISABLE;
    INT4                i4AutoSaveOption = 0;
    INT4                i4IncrSaveOption = 0;
    INT4                i4RollBackOption = 0;
    INT1                i1DefValSaveFlag = 0;
    INT4                i4CfgSaveAddrType = 0;
    INT4                i4AutomaticPortCreate = 0;
    INT1                i1HRFlag = 0;
    UINT1              *pu1FlashLogFileName;
    UINT1               au1LogFileName[ISS_CONFIG_FILE_NAME_LEN + 1];

    UNUSED_PARAM (u4FrontPanelPortCount);
    UNUSED_PARAM (pu1NpapiMode);
    MEMSET (au1IfName, 0, MAX_NAME_SIZE);
    MEMSET (au1CfgSaveFile, 0, MAX_FILENAME_SIZE);
    MEMSET (au1CfgResFile, 0, MAX_FILENAME_SIZE);
    MEMSET (au1IfIP, 0, MAX_NAME_SIZE);
    MEMSET (au1Temp, 0, MAX_NAME_SIZE);
    MEMSET (au1RmIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (CfgSaveIpAddr.au1Addr, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1HwVersion, 0, ISS_STR_LEN + 1);
    MEMSET (au1FirmVersion, 0, ISS_STR_LEN + 1);
    MEMSET (au1SwitchName, 0, ISS_STR_LEN + 1);
    MEMSET (au1HwPartNum, 0, ISS_STR_LEN + 1);
    MEMSET (au1SwVersion, 0, ISS_STR_LEN + 1);
    MEMSET (au1SwSerialNum, 0, ISS_STR_LEN + 1);
    MEMSET (au1LogFileName, 0, ISS_CONFIG_FILE_NAME_LEN + 1);

    Name.pu1_OctetList = &au1IfName[0];
    CfgSaveFile.pu1_OctetList = &au1CfgSaveFile[0];
    CfgResFile.pu1_OctetList = &au1CfgResFile[0];
    RmIfName.pu1_OctetList = &au1RmIfName[0];
    pu1String = (CHR1 *) & au1IfIP[0];
    piIfName = (INT1 *) &au1Temp[0];
    sCfgSaveIpAddr.pu1_OctetList = &CfgSaveIpAddr.au1Addr[0];
    HwVersion.pu1_OctetList = &au1HwVersion[0];
    FirmVersion.pu1_OctetList = &au1FirmVersion[0];
    SwitchName.pu1_OctetList = &au1SwitchName[0];
    pu1FlashLogFileName = (UINT1 *) &au1LogFileName[0];

    nmhGetIssDefaultIpAddr ((UINT4 *) &i4DefIpAddr);
    nmhGetIssDefaultIpAddrAllocProtocol (&i4IpAllocProto);
    nmhGetIssDefaultIpSubnetMask ((UINT4 *) &i4DefIpMask);
    nmhGetIssDefaultIpAddrCfgMode (&i4DefIpCfgMode);
    nmhGetIssSwitchBaseMacAddress (&BaseMac);
    nmhGetIssDefaultInterface (&Name);
    nmhGetIssConfigRestoreOption (&i4CfgResOption);
    nmhGetIssConfigSaveOption (&i4CfgSaveOption);
    nmhGetIssConfigSaveIpAddrType (&i4CfgSaveAddrType);
    nmhGetIssConfigSaveIpvxAddr (&sCfgSaveIpAddr);
    nmhGetIssConfigSaveFileName (&CfgSaveFile);
    nmhGetIssConfigRestoreFileName (&CfgResFile);
    nmhGetIssOOBInterface (&i4OobPresent);
    nmhGetIssNoCliConsole (&i4CliSerialConsole);
    nmhGetIssDefaultRmIfName (&RmIfName);
    nmhGetIssConfigAutoSaveTrigger (&i4AutoSaveOption);
    nmhGetIssConfigIncrSaveFlag (&i4IncrSaveOption);
    nmhGetIssConfigRollbackFlag (&i4RollBackOption);
    nmhGetIssHardwareVersion (&HwVersion);
    nmhGetIssFirmwareVersion (&FirmVersion);
    IssGetFlashLoggingLocation (pu1FlashLogFileName);

    MEMCPY (au1HwPartNum, IssGetHardwarePartNumber (),
            STRLEN (IssGetHardwarePartNumber ()));
    MEMCPY (au1SwSerialNum, IssGetSoftwareSerialNumber (),
            STRLEN (IssGetSoftwareSerialNumber ()));

    MEMCPY (au1SwVersion, IssGetSoftwareVersion (),
            STRLEN (IssGetSoftwareVersion ()));

    nmhGetIssSwitchName (&SwitchName);
    i4PimMode = (INT4) IssGetPimModeFromNvRam ();
    pi1EngineID = IssGetSnmpEngineID ();
    u4BootCount = IssGetSnmpEngineBoots ();
    u2DefaultVlanId = IssGetDefaultVlanIdFromNvRam ();
    u2StackPortCount = IssGetStackPortCountFromNvRam ();
    u2ColdStandby = (UINT2) IssGetColdStdbyState ();
    i1DefValSaveFlag = IssGetDefValSaveFlagFromNvRam ();
    u1VrfUnqMacOption = IssGetVrfUnqMacOptionFromNvRam ();
    i1HRFlag = IssGetHRFlagFromNvRam ();
    u4RmHbMode = IssGetHeartBeatModeFromNvRam ();
    u4RmRType = IssGetRedundancyTypeFromNvRam ();
    u4RmDType = IssGetRmDataPlaneFromNvRam ();
    u4RmStackingInterfaceType = (UINT4) IssGetRMTypeFromNvRam ();
    i4AutomaticPortCreate = IssGetAutoPortCreateFlagFromNvRam ();

    CLI_CONVERT_IPADDR_TO_STR (pu1String, (UINT4) i4DefIpAddr);
    CliPrintf (CliHandle, "\r\n%-38s : %s\r\n", "Default IP Address",
               pu1String);
    MEMSET (au1IfIP, 0, MAX_NAME_SIZE);

    CLI_CONVERT_IPADDR_TO_STR (pu1String, (UINT4) i4DefIpMask);
    CliPrintf (CliHandle, "%-38s : %s\r\n", "Default Subnet Mask", pu1String);
    MEMSET (au1IfIP, 0, MAX_NAME_SIZE);

    if (i4DefIpCfgMode == ISS_CFG_MANUAL)
    {
        CliPrintf (CliHandle, "%-38s : Manual\r\n", "Default IP Address"
                   " Config Mode");
    }
    else
    {
        CliPrintf (CliHandle, "%-38s : Dynamic\r\n", "Default IP Address"
                   " Config Mode");
    }
    if (i4IpAllocProto == CFA_PROTO_DHCP)
    {
        CliPrintf (CliHandle, "%-38s : DHCP\r\n", "Default IP Address"
                   " Allocation Protocol");
    }
    else if (i4IpAllocProto == CFA_PROTO_BOOTP)
    {
        CliPrintf (CliHandle, "%-38s : BOOTP\r\n", "Default IP Address"
                   " Allocation Protocol");
    }
    else if (i4IpAllocProto == CFA_PROTO_RARP)
    {
        CliPrintf (CliHandle, "%-38s : RARP\r\n", "Default IP Address"
                   " Allocation Protocol");
    }

    pu1String = (CHR1 *) & au1IfIP[0];
    PrintMacAddress (BaseMac, (UINT1 *) pu1String);
    CliPrintf (CliHandle, "%-38s : %s\r\n", "Switch Base MAC Address",
               pu1String);
    MEMSET (au1IfIP, 0, MAX_NAME_SIZE);

    if (i4OobPresent == ISS_FALSE)
    {
        if (CfaGetInterfaceIndexFromName (Name.pu1_OctetList, &u4IfIndex)
            == OSIX_SUCCESS)
        {
            CfaCliGetIfName (u4IfIndex, piIfName);
        }
    }
    else
    {
        CfaCliGetIfName (CFA_OOB_MGMT_IFINDEX, piIfName);
    }

    CliPrintf (CliHandle, "%-38s : %s\r\n", "Default Interface Name", piIfName);
    if (RmIfName.i4_Length == 0)
    {
        CliPrintf (CliHandle, "%-38s : NONE\r\n", "Default RM Interface Name");
    }
    else
    {
        CliPrintf (CliHandle, "%-38s : %s\r\n", "Default RM Interface Name",
                   RmIfName.pu1_OctetList);
    }
    MEMSET (au1IfName, 0, MAX_NAME_SIZE);

    if (i4CfgResOption == ISS_CONFIG_NO_RESTORE)
    {
        CliPrintf (CliHandle, "%-38s : No restore\r\n",
                   "Config Restore Option");
    }
    else if (i4CfgResOption == ISS_CONFIG_RESTORE)
    {
        CliPrintf (CliHandle, "%-38s : Restore\r\n", "Config Restore Option");
    }

    if (i4CfgSaveOption == ISS_CONFIG_NO_SAVE)
    {
        CliPrintf (CliHandle, "%-38s : No save\r\n", "Config Save Option");
    }
    else if (i4CfgSaveOption == ISS_CONFIG_FLASH_SAVE)
    {
        CliPrintf (CliHandle, "%-38s : Flash save\r\n", "Config Save Option");
    }
    else if (i4CfgSaveOption == ISS_CONFIG_REMOTE_SAVE)
    {
        CliPrintf (CliHandle, "%-38s : Remote save\r\n", "Config Save Option");
    }
    else if (i4CfgSaveOption == ISS_CONFIG_STARTUP_SAVE)
    {
        CliPrintf (CliHandle, "%-38s : Startup save\r\n", "Config Save Option");
    }
    if (i4AutoSaveOption == ISS_TRUE)
    {
        CliPrintf (CliHandle, "%-38s : Enable\r\n", "Auto Save");
    }
    else
    {
        CliPrintf (CliHandle, "%-38s : Disable\r\n", "Auto Save");
    }
    if (i4IncrSaveOption == ISS_TRUE)
    {
        CliPrintf (CliHandle, "%-38s : Enable\r\n", "Incremental Save");
    }
    else
    {
        CliPrintf (CliHandle, "%-38s : Disable\r\n", "Incremental Save");
    }
    if (i4RollBackOption == SNMP_ROLLBACK_ENABLED)
    {
        CliPrintf (CliHandle, "%-38s : Enable\r\n", "Roll Back");
    }
    else
    {
        CliPrintf (CliHandle, "%-38s : Disable\r\n", "Roll Back");
    }

    if (i4CfgSaveAddrType == IPVX_ADDR_FMLY_IPV4)
    {
        PTR_FETCH4 (u4CfgSaveIpAddr, sCfgSaveIpAddr.pu1_OctetList);
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4CfgSaveIpAddr);
        CliPrintf (CliHandle, "%-38s : %s\r\n", "Config Save IP Address",
                   pu1String);
    }
    else if (i4CfgSaveAddrType == IPVX_ADDR_FMLY_IPV6)
    {
        CliPrintf (CliHandle, "%-38s : %s\r\n", "Config Save IP Address",
                   Ip6PrintNtop ((tIp6Addr *) (VOID *) sCfgSaveIpAddr.
                                 pu1_OctetList));
    }

    MEMSET (au1IfIP, 0, MAX_NAME_SIZE);

    CliPrintf (CliHandle, "%-38s : %s\r\n", "Config Save Filename",
               CfgSaveFile.pu1_OctetList);

    CliPrintf (CliHandle, "%-38s : %s\r\n", "Config Restore Filename",
               CfgResFile.pu1_OctetList);

    if (i4PimMode == ISS_DM)
    {
        CliPrintf (CliHandle, "%-38s : Dense Mode\r\n", "PIM Mode");
    }
    else if (i4PimMode == ISS_SM)
    {
        CliPrintf (CliHandle, "%-38s : Sparse Mode\r\n", "PIM Mode");
    }

    i4SnoopFwdMode = (INT4) IssGetSnoopFwdModeFromNvRam ();

    if (i4SnoopFwdMode == ISS_SNOOP_IP)
    {
        CliPrintf (CliHandle, "%-38s : IP based\r\n", "IGS Forwarding Mode");
    }
    else if (i4SnoopFwdMode == ISS_SNOOP_MAC)
    {
        CliPrintf (CliHandle, "%-38s : MAC based\r\n", "IGS Forwarding Mode");
    }

    if (i4CliSerialConsole == ISS_CLI_SERIAL_CONSOLE)
    {
        CliPrintf (CliHandle, "%-38s : Yes \r\n", "Cli Serial Console");
    }
    else if (i4CliSerialConsole == ISS_CLI_NO_SERIAL_CONSOLE)
    {
        CliPrintf (CliHandle, "%-38s : No \r\n", "Cli Serial Console");
    }

    CliPrintf (CliHandle, "%-38s : %s\r\n", "SNMP EngineID", pi1EngineID);
    CliPrintf (CliHandle, "%-38s : %d\r\n", "SNMP Engine Boots", u4BootCount);
    CliPrintf (CliHandle, "%-38s : %d\r\n", "Default VLAN Identifier",
               u2DefaultVlanId);
    CliPrintf (CliHandle, "%-38s : %d\r\n", "Stack PortCount",
               u2StackPortCount);
    if (u2ColdStandby == ISS_COLDSTDBY_ENABLE)
    {
        CliPrintf (CliHandle, "%-38s : Enable\r\n", "ColdStandby");
    }
    else
    {
        CliPrintf (CliHandle, "%-38s : Disable\r\n", "ColdStandby");
    }

    if (i1DefValSaveFlag == ISS_TRUE)
    {
        CliPrintf (CliHandle, "%-38s : Enable\r\n", "Store Default Value");
    }
    else
    {
        CliPrintf (CliHandle, "%-38s : Disable\r\n", "Store Default Value");
    }

    if (u1VrfUnqMacOption == ISS_VRF_UNQ_MAC_ENABLE)
    {
        CliPrintf (CliHandle, "%-38s : Enable\r\n", "Vrf Unique Mac");
    }
    else
    {
        CliPrintf (CliHandle, "%-38s : Disable\r\n", "Vrf Unique Mac");
    }
/* HITLESS RESTART */
    if (i1HRFlag == ISS_DEFAULT_HR_FLAG)
    {
        CliPrintf (CliHandle, "%-38s : Disable\r\n", "Hitless Restart"
                   "  Flag");
    }
    else
    {
        CliPrintf (CliHandle, "%-38s : Enable\r\n", "Hitless Restart" " Flag");
    }
    CliPrintf (CliHandle, "%-38s : %s\r\n", "Hardware Version",
               HwVersion.pu1_OctetList);

    CliPrintf (CliHandle, "%-38s : %s\r\n", "Software Version", au1SwVersion);

    CliPrintf (CliHandle, "%-38s : %s\r\n", "Firmware Version",
               FirmVersion.pu1_OctetList);

    CliPrintf (CliHandle, "%-38s : %s\r\n", "Hardware Serial Number",
               au1HwPartNum);

    CliPrintf (CliHandle, "%-38s : %s\r\n", "Software Serial Number",
               au1SwSerialNum);

    CliPrintf (CliHandle, "%-38s : %s\r\n", "Switch Name",
               SwitchName.pu1_OctetList);

    if (u4RmHbMode == ISS_RM_HB_MODE_INTERNAL)
    {
        CliPrintf (CliHandle, "%-38s : Internal\r\n", "RM Heart Beat" " Mode");
    }
    else
    {
        CliPrintf (CliHandle, "%-38s : External\r\n", "RM Heart Beat" " Mode");
    }

    if (u4RmRType == ISS_RM_RTYPE_HOT)
    {
        CliPrintf (CliHandle, "%-38s : Hot\r\n", "RM Redundancy" " Type");
    }
    else
    {
        CliPrintf (CliHandle, "%-38s : Cold\r\n", "RM Redundancy" " Type");
    }

    if (u4RmDType == ISS_RM_DTYPE_SHARED)
    {
        CliPrintf (CliHandle, "%-38s : Shared\r\n", "RM Data Plane" " Type");
    }
    else
    {
        CliPrintf (CliHandle, "%-38s : Separate\r\n", "RM Data Plane" " Type");
    }

    if (u4RmStackingInterfaceType == ISS_RM_STACK_INTERFACE_TYPE_OOB)
    {
        CliPrintf (CliHandle, "%-38s : OOB\r\n", "RM" " Type");
    }
    else
    {
        CliPrintf (CliHandle, "%-38s : Ethernet\r\n", "RM" " Type");
    }
    if (ISS_NVRAM_CALLBACK[ISS_NVRAM_CLI_SHOW_EVENT].pIssCustShowNvram != NULL)
    {
        IssCustNvramCallBack (ISS_NVRAM_CLI_SHOW_EVENT, CliHandle);
    }

    CliPrintf (CliHandle, "%-38s : %s\r\n", "NPAPI mode",
               sNvRamData.au1NpapiMode);

    if (sNvRamData.u2TimeStampMethod == ISS_SOFTWARE_TIME_STAMP)
    {
        CliPrintf (CliHandle, "%-38s : Software\r\n", "TimeStamp Method");
    }
    else if (sNvRamData.u2TimeStampMethod == ISS_HARDWARE_TRANS_TIME_STAMP)
    {
        CliPrintf (CliHandle, "%-38s : TransHardware\r\n", "TimeStamp Method");
    }
    else if (sNvRamData.u2TimeStampMethod == ISS_HARDWARE_TIME_STAMP)
    {
        CliPrintf (CliHandle, "%-38s : Hardware\r\n", "TimeStamp Method");
    }
    else
    {
        CliPrintf (CliHandle, "%-38s : None\r\n", "TimeStamp Method");
    }

    if (sNvRamData.i1ResFlag == ISS_DEFAULT_RESTORE_FLAG)
    {
        CliPrintf (CliHandle, "%-38s : Enabled\r\n", "Restore Flag");
    }
    else
    {
        CliPrintf (CliHandle, "%-38s : Disabled\r\n", "Restore Flag");
    }

    CliPrintf (CliHandle, "%-38s : %d\r\n", "Dynamic Port Count",
               sNvRamData.u4FrontPanelPortCount);

    if (sNvRamData.i4FipsOperMode == ISS_DEFAULT_FIPS_OPER_MODE)
    {
        CliPrintf (CliHandle, "%-38s : Disabled\r\n", "FIPS operation mode");
    }
    else
    {
        CliPrintf (CliHandle, "%-38s : Enabled\r\n", "FIPS operation mode");
    }

    if (sNvRamData.u4RestoreOption == ISS_CONFIG_NO_RESTORE)
    {
        CliPrintf (CliHandle, "%-38s : Disabled\r\n", "Restore Option");
    }
    else
    {
        CliPrintf (CliHandle, "%-38s : Enabled\r\n", "Restore Option");
    }

    if (sNvRamData.u4BridgeMode == VLAN_CUSTOMER_BRIDGE_MODE)
    {
        CliPrintf (CliHandle, "%-38s : Customer Bridge \r\n", "Bridge Mode");
    }
    else if (sNvRamData.u4BridgeMode == VLAN_PROVIDER_BRIDGE_MODE)
    {
        CliPrintf (CliHandle, "%-38s : Provider Bridge \r\n", "Bridge Mode");
    }
    else if (sNvRamData.u4BridgeMode == VLAN_PROVIDER_EDGE_BRIDGE_MODE)
    {
        CliPrintf (CliHandle, "%-38s : Provider Edge Bridge \r\n",
                   "Bridge Mode");
    }
    else if (sNvRamData.u4BridgeMode == VLAN_PROVIDER_CORE_BRIDGE_MODE)
    {
        CliPrintf (CliHandle, "%-38s : Provider Core Bridge \r\n",
                   "Bridge Mode");
    }

    CliPrintf (CliHandle, "%-38s : %s\r\n", "Debugging Log File Location",
               pu1FlashLogFileName);

    if (sNvRamData.u4MgmtPort == TRUE)
    {
        CliPrintf (CliHandle, "%-38s : Enabled \r\n", "Management Port");
    }
    else
    {
        CliPrintf (CliHandle, "%-38s : Disabled \r\n", "Management Port");
    }

    if (i4AutomaticPortCreate == ISS_ENABLE)
    {
        CliPrintf (CliHandle, "%-38s : Enabled\r\n", "Automatic Port Create"
                   " Flag");
    }
    else
    {
        CliPrintf (CliHandle, "%-38s : Disabled\r\n",
                   "Automatic Port Create" " Flag");
    }
    if (sNvRamData.u1RestoreType == ISS_CSR_RESTORE)
    {
        CliPrintf (CliHandle, "%-38s : CSR \r\n", "Restore Type");
    }
    else
    {
        CliPrintf (CliHandle, "%-38s : MSR \r\n", "Restore Type");
    }

    CliPrintf (CliHandle, "%-38s : %s\r\n\r\n",
               "IMG_DUMP_PATH", sNvRamData.au1ImageDumpFilePath);

    return (CLI_SUCCESS);
}

#ifdef WTP_WANTED
/***************************************************************/
/*  Function Name   : CliIssShowWtpNvRam                       */
/*  Description     : This function is used to display         */
/*                    WTP NVRAM Information.                   */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliIssShowWtpNvRam (tCliHandle CliHandle)
{
    CHR1               *pu1String;
    UINT1               au1WlcIP[MAX_NAME_SIZE];

    MEMSET (au1WlcIP, 0, sizeof (au1WlcIP));
    pu1String = (CHR1 *) & au1WlcIP[0];

    if (sWtpNvRamData.u2AcReferralType == WSS_AC_REFERRAL_TYPE_BROADCAST)
    {
        CliPrintf (CliHandle, "\r\n%-38s : Broadcast \r\n", "AC Referral Type");
    }
    else if (sWtpNvRamData.u2AcReferralType == WSS_AC_REFERRAL_TYPE_MULTICAST)
    {
        CliPrintf (CliHandle, "\r\n%-38s : Multicast \r\n", "AC Referral Type");
    }
    else
    {
        CliPrintf (CliHandle, "\r\n%-38s : Broadcast \r\n",
                   "Default AC Referral Type");
    }

    if (sWtpNvRamData.u2WtpCapwapMacType == WSS_CAPWAP_MAC_TYPE_SPLIT)
    {
        CliPrintf (CliHandle, "%-38s : SplitMAC \r\n", "CAPWAP MAC Type");
    }
    else if (sWtpNvRamData.u2WtpCapwapMacType == WSS_CAPWAP_MAC_TYPE_LOCAL)
    {
        CliPrintf (CliHandle, "%-38s : LocalMAC \r\n", "CAPWAP MAC Type");
    }
    else if (sWtpNvRamData.u2WtpCapwapMacType == WSS_CAPWAP_MAC_TYPE_BOTH)
    {
        CliPrintf (CliHandle, "%-38s : Both \r\n", "CAPWAP MAC Type");
    }
    else
    {
        CliPrintf (CliHandle, "%-38s : SplitMAC \r\n",
                   "Default CAPWAP MAC Type");
    }

    if (sWtpNvRamData.u2WtpCapwapTunnelMode == WSS_CAPWAP_TUNL_MODE_NATIVE)
    {
        CliPrintf (CliHandle, "%-38s : NativeTunnel \r\n",
                   "CAPWAP Tunnel Mode");
    }
    else if (sWtpNvRamData.u2WtpCapwapTunnelMode == WSS_CAPWAP_TUNL_MODE_DOT3)
    {
        CliPrintf (CliHandle, "%-38s : Dot3Tunnel \r\n", "CAPWAP Tunnel Mode");
    }
    else if (sWtpNvRamData.u2WtpCapwapTunnelMode ==
             WSS_CAPWAP_TUNL_MODE_LOCALBRIDGE)
    {
        CliPrintf (CliHandle, "%-38s : LocalBridging \r\n",
                   "CAPWAP Tunnel Mode");
    }
    else
    {
        CliPrintf (CliHandle, "%-38s : NativeTunnel \r\n",
                   "Default CAPWAP Tunnel Mode");
    }

    CliPrintf (CliHandle, "%-38s : %s \r\n", "WTP Model Number",
               sWtpNvRamData.ai1WtpModelNumber);

    CliPrintf (CliHandle, "%-38s : %s \r\n", "WTP Serial Number",
               sWtpNvRamData.ai1WtpSerialNumber);

    CliPrintf (CliHandle, "%-38s : %s \r\n", "WTP Board Id",
               sWtpNvRamData.ai1WtpBoardId);

    CliPrintf (CliHandle, "%-38s : %s \r\n", "WTP Board Revision",
               sWtpNvRamData.ai1WtpBoardRevision);

    CliPrintf (CliHandle, "%-38s : %d \r\n", "CAPWAP UDP Server Port",
               sWtpNvRamData.u4CapwapUdpServerPort);

    CliPrintf (CliHandle, "%-38s : %d \r\n", "Native VLAN",
               sWtpNvRamData.u4NativeVlan);

    return (CLI_SUCCESS);

}
#endif
/****************************************************************/
/*  Function Name   : CliIssSleep                               */
/*  Description     : This function triggers delay in execution */
/*                    of CLI thread for configured seconds      */
/*                                                              */
/*  Input(s)        : i4SleepSeconds - time for which ISS has   */
/*                    to sleep/wait.                            */
/*  Output(s)       : None                                      */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                   */
/****************************************************************/
INT4
CliIssSleep (INT4 i4SleepSeconds)
{
    OsixTskDelay ((UINT4) i4SleepSeconds * SYS_TIME_TICKS_IN_A_SEC);
    return CLI_SUCCESS;
}

/****************************************************************/
/*  Function Name   : CliIssShowDate                            */
/*  Description     : This function is used to display the date */
/*                                                              */
/*  Input(s)        :                                           */
/*                    CliHandle   - CLI Handle                  */
/*  Output(s)       : None                                      */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                   */
/****************************************************************/

INT4
CliIssShowDate (tCliHandle CliHandle)
{

    tSNMP_OCTET_STRING_TYPE Date;
    UINT1               au1Date[ISS_MAX_DATE_LEN];

    UINT1               au1Hrs[3], au1Min[3], au1Sec[3], au1Day[3], au1Mon[3],
        au1Year[5], au1Month[4], au1Wday[3], au1WeekDay[4], au1UtcOffset[13];
    UINT4               u4Val = 0;
    UINT4               u4WeekDay = 0;

    Date.pu1_OctetList = &au1Date[0];
    Date.i4_Length = ISS_MAX_DATE_LEN;

    MEMSET (au1Date, 0, ISS_MAX_DATE_LEN);
    MEMSET (au1Hrs, 0, 3);
    MEMSET (au1Min, 0, 3);
    MEMSET (au1Sec, 0, 3);
    MEMSET (au1Day, 0, 3);
    MEMSET (au1Mon, 0, 3);
    MEMSET (au1Year, 0, 5);
    MEMSET (au1Month, 0, 4);
    MEMSET (au1Wday, 0, 3);
    MEMSET (au1WeekDay, 0, 4);
    MEMSET (au1UtcOffset, 0, 13);

    nmhGetIssSwitchDate (&Date);

    MEMCPY (&au1Wday, Date.pu1_OctetList, 2);
    MEMCPY (&au1Mon, Date.pu1_OctetList + 3, 2);
    MEMCPY (&au1Day, Date.pu1_OctetList + 6, 2);
    MEMCPY (&au1Hrs, Date.pu1_OctetList + 9, 2);
    MEMCPY (&au1Min, Date.pu1_OctetList + 12, 2);
    MEMCPY (&au1Sec, Date.pu1_OctetList + 15, 2);
    MEMCPY (&au1Year, Date.pu1_OctetList + 18, 4);
    MEMCPY (&au1UtcOffset, Date.pu1_OctetList + 23, 12);

    u4Val = (UINT4) ATOI (&au1Mon);

    switch (u4Val)
    {
        case 1:
            STRCPY (au1Month, "Jan");
            break;
        case 2:
            STRCPY (au1Month, "Feb");
            break;
        case 3:
            STRCPY (au1Month, "Mar");
            break;
        case 4:
            STRCPY (au1Month, "Apr");
            break;
        case 5:
            STRCPY (au1Month, "May");
            break;
        case 6:
            STRCPY (au1Month, "Jun");
            break;
        case 7:
            STRCPY (au1Month, "Jul");
            break;
        case 8:
            STRCPY (au1Month, "Aug");
            break;
        case 9:
            STRCPY (au1Month, "Sep");
            break;
        case 10:
            STRCPY (au1Month, "Oct");
            break;
        case 11:
            STRCPY (au1Month, "Nov");
            break;
        case 12:
            STRCPY (au1Month, "Dec");
            break;

        default:
            break;
    }

    u4WeekDay = (UINT4) ATOI (&au1Wday);

    switch (u4WeekDay)
    {
        case 1:
            STRCPY (au1WeekDay, "Sun");
            break;
        case 2:
            STRCPY (au1WeekDay, "Mon");
            break;
        case 3:
            STRCPY (au1WeekDay, "Tue");
            break;
        case 4:
            STRCPY (au1WeekDay, "Wed");
            break;
        case 5:
            STRCPY (au1WeekDay, "Thu");
            break;
        case 6:
            STRCPY (au1WeekDay, "Fri");
            break;
        case 7:
            STRCPY (au1WeekDay, "Sat");
            break;
        default:
            break;
    }

    CliPrintf (CliHandle, "\r\n %s %s %s %s:%s:%s %s %s\r\n",
               au1WeekDay, au1Month, au1Day, au1Hrs, au1Min, au1Sec,
               au1Year, au1UtcOffset);

    return (CLI_SUCCESS);

}

/***************************************************************/
/*  Function Name   : IssCopyFileGeneric                       */
/*  Description     : This function is used to copy files from */
/*                    remote to local, local to local , local  */
/*                    to remote                                */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    au1SrcFileName - Source File Name        */
/*                    SrcIpAddress - Source IP address       */
/*                    au1DstFileName - Dest File Name          */
/*                    u4DstIpAddress - IP address              */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
IssCopyFileGeneric (tCliHandle CliHandle, UINT1 *pu1SrcFileName,
                    UINT4 u1SrcRemoteType, UINT1 *pu1SrcUserName,
                    UINT1 *pu1SrcPassWd, tIPvXAddr SrcIpAddress,
                    UINT1 *pu1SrcHostName, UINT1 *pu1DstFileName,
                    UINT4 u1DstRemoteType, UINT1 *pu1DstUserName,
                    UINT1 *pu1DstPassWd, tIPvXAddr DstIpAddress,
                    UINT1 *pu1DstHostName)
{
    UINT4               u4DstIpAddress = 0;
    UINT4               u4SrcIpAddress = 0;
    INT4                i4RetVal;
    CHR1               *pu1String;
    UINT1               au1IfIP[MAX_NAME_SIZE];
    UINT1              *pu1TempString;
    UINT1               au1IfIPAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1DstFileName[ISS_CONFIG_FILE_NAME_LEN + 10];
    UINT4               u4SymbolIndex = 0;
    UINT4               u4Index = 0;
    /* the local file name is prepended with the base dir which is 
     * defied as FLASH, allocate extra 10 bytes for this
     */
    UINT1               au1LocalSrcFile[ISS_CONFIG_FILE_NAME_LEN + 10] = "";
    UINT1               au1LocalDstFile[ISS_CONFIG_FILE_NAME_LEN + 10] = "";
    UINT1               u1sftpCopyStatus = 0;
    tDNSResolvedIpInfo  ResolvedIpInfo;
    UINT1               au1TempName[DNS_MAX_QUERY_LEN];
    UINT1               u1TempLen = 0;

    MEMSET (&ResolvedIpInfo, 0, sizeof (tDNSResolvedIpInfo));
    MEMSET (au1TempName, 0, DNS_MAX_QUERY_LEN);

    MEMSET (au1DstFileName, 0, sizeof (au1DstFileName));
    SNPRINTF ((CHR1 *) au1DstFileName, sizeof (au1DstFileName), "%s",
              pu1DstFileName);

    pu1String = (CHR1 *) & au1IfIP[0];
    pu1TempString = au1IfIPAddr;

    if (au1DstFileName[u4Index] == '/')
    {
        u4Index++;
    }
    while (au1DstFileName[u4Index] != '\0')
    {
        for (u4SymbolIndex = 0; ISS_SPL_CHAR_LIST[u4SymbolIndex] != '\0';
             u4SymbolIndex++)
        {
            if (au1DstFileName[u4Index] == ISS_SPL_CHAR_LIST[u4SymbolIndex])
            {

                CliPrintf (CliHandle,
                           "\r%% Enter Valid characters in file name \r\n");
                return (CLI_FAILURE);

            }
        }
        u4Index++;
    }

    if (CliIssCheckMsrStatus (CliHandle) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if ((SrcIpAddress.u1AddrLen != 0) && (DstIpAddress.u1AddrLen != 0))
    {
        CliPrintf (CliHandle,
                   "\r%% Copying files from remote location to a remote"
                   " location is not supported\r\n");
        return (CLI_FAILURE);
    }

    MEMSET (au1LocalDstFile, 0, sizeof (au1LocalDstFile));
    if (SrcIpAddress.u1Afi != 0)
    {
        /* Remote to local */
        if (u1DstRemoteType == ISS_ABSOLUTE_PATH)
        {
            SNPRINTF ((CHR1 *) au1LocalDstFile, sizeof (au1LocalDstFile), "%s",
                      pu1DstFileName);
        }
        else
        {
            SNPRINTF ((CHR1 *) au1LocalDstFile, sizeof (au1LocalDstFile),
                      "%s%s", FLASH, pu1DstFileName);
        }
        /* Resolving src host name here */
        /* If family is DNS host name resolve and copy IpAddress to au1Addr. */
        if (SrcIpAddress.u1Afi == IPVX_DNS_FAMILY)
        {
            u1TempLen = (UINT1) STRLEN (pu1SrcHostName);
            STRNCPY (au1TempName, pu1SrcHostName, u1TempLen);
            au1TempName[u1TempLen] = '\0';
            i4RetVal =
                FsUtlIPvXResolveHostName (au1TempName, DNS_BLOCK,
                                          &ResolvedIpInfo);

            if (i4RetVal == DNS_NOT_RESOLVED)
            {
                CliPrintf (CliHandle, "\n%%Cannot resolve the host name \n");
                return (CLI_FAILURE);
                /*not resolved */
            }
            else if (i4RetVal == DNS_IN_PROGRESS)
            {
                CliPrintf (CliHandle,
                           "\n%% Host name resolution in Progress \n");
                return (CLI_FAILURE);
                /*in progress */
            }
            else if (i4RetVal == DNS_CACHE_FULL)
            {
                CliPrintf (CliHandle,
                           "\n%%DNS cache full, cannot resolve at the moment");
                return (CLI_FAILURE);
                /*cache full */
            }

            MEMSET (au1TempName, 0, DNS_MAX_QUERY_LEN);
            MEMSET (SrcIpAddress.au1Addr, 0, IPVX_MAX_INET_ADDR_LEN);
            /* If the hostname resolves to both IPv4 and IPv6 
             * address, IPv6 address is considered */
            if (ResolvedIpInfo.Resolv6Addr.u1AddrLen != 0)
            {
                MEMCPY (au1TempName, ResolvedIpInfo.Resolv6Addr.au1Addr,
                        IPVX_IPV6_ADDR_LEN);
                SrcIpAddress.u1AddrLen = (UINT1) IPVX_IPV6_ADDR_LEN;
                SrcIpAddress.u1Afi = (UINT1) IPVX_ADDR_FMLY_IPV6;
                MEMCPY (SrcIpAddress.au1Addr, au1TempName, IPVX_IPV6_ADDR_LEN);
            }
            else
            {
                MEMCPY (au1TempName, ResolvedIpInfo.Resolv4Addr.au1Addr,
                        IPVX_IPV4_ADDR_LEN);
                SrcIpAddress.u1AddrLen = (UINT1) IPVX_IPV4_ADDR_LEN;
                SrcIpAddress.u1Afi = (UINT1) IPVX_ADDR_FMLY_IPV4;
                MEMCPY (SrcIpAddress.au1Addr, au1TempName, IPVX_IPV4_ADDR_LEN);
            }
        }
        if (u1SrcRemoteType == ISS_TFTP)
        {
            if ((i4RetVal =
                 tftpcRecvFile (SrcIpAddress, pu1SrcFileName,
                                au1LocalDstFile)) != TFTPC_OK)
            {
                if (u1DstRemoteType == ISS_ABSOLUTE_PATH)
                {
                    CliPrintf (CliHandle,
                               "\r%% Unable to copy remote file\r\n");
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\r%% Unable to copy remote file to flash\r\n");
                }
                return (CLI_FAILURE);
            }
            else
            {
                MEMSET (au1IfIP, 0, MAX_NAME_SIZE);
                PTR_FETCH4 (u4SrcIpAddress, SrcIpAddress.au1Addr);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcIpAddress);
                if (u1DstRemoteType == ISS_ABSOLUTE_PATH)
                {
                    CliPrintf (CliHandle, "\rCopied tftp://%s/%s ==> %s \r\n",
                               pu1String, pu1SrcFileName, au1LocalDstFile);
                }
                else
                {
                    CliPrintf (CliHandle, "\r ...Completed: 100 %%... \r\n");
                    CliPrintf (CliHandle,
                               "\rCopied tftp://%s/%s ==> flash:%s \r\n",
                               pu1String, pu1SrcFileName, au1LocalDstFile);
                }
                return (CLI_SUCCESS);
            }
        }
#ifdef SSH_WANTED
        if (u1SrcRemoteType == ISS_SFTP)
        {
            if ((i4RetVal = sftpcArRecvFile (pu1SrcUserName, pu1SrcPassWd,
                                             SrcIpAddress, pu1SrcFileName,
                                             au1LocalDstFile)) != SFTP_SUCCESS)
            {
                if (u1DstRemoteType == ISS_ABSOLUTE_PATH)
                {
                    SftpcArGetCopyStatus (&u1sftpCopyStatus);
                    if (u1sftpCopyStatus == OSIX_FAILURE)
                    {
                        UtlTrcLog (1, 1, "",
                                   "\n\n\r%% SFTP copy failed -- retrying !!!\r\n");
                        /* Delay added to delete the running task *
                         * as OsixRscFind would return FAILURE if it tries to create
                         * task with same name in short time*/
                        OsixTskDelay (2 * SYS_TIME_TICKS_IN_A_SEC);
                        i4RetVal =
                            sftpcArRecvFile (pu1SrcUserName, pu1SrcPassWd,
                                             SrcIpAddress, pu1SrcFileName,
                                             au1LocalDstFile);
                    }
                }
                else
                {
                    SftpcArGetCopyStatus (&u1sftpCopyStatus);
                    if (u1sftpCopyStatus == OSIX_FAILURE)
                    {
                        UtlTrcLog (1, 1, "",
                                   "\n\n\r%% SFTP copy failed -- retrying !!!\r\n");
                        /* Delay added to delete the running task *
                         * as OsixRscFind would return FAILURE if it tries to create
                         * task with same name in short time*/

                        OsixTskDelay (2 * SYS_TIME_TICKS_IN_A_SEC);
                        i4RetVal =
                            sftpcArRecvFile (pu1SrcUserName, pu1SrcPassWd,
                                             SrcIpAddress, pu1SrcFileName,
                                             au1LocalDstFile);
                    }
                }
            }
            if (i4RetVal == SFTP_SUCCESS)
            {
                MEMSET (au1IfIP, 0, MAX_NAME_SIZE);
                PTR_FETCH4 (u4SrcIpAddress, SrcIpAddress.au1Addr);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4SrcIpAddress);
                if (u1DstRemoteType == ISS_ABSOLUTE_PATH)
                {
                    CliPrintf (CliHandle, "\rCopied sftp://%s/%s ==> %s \r\n",
                               pu1String, pu1SrcFileName, au1LocalDstFile);
                }
                else
                {
                    CliPrintf (CliHandle,
                               "\rCopied sftp://%s/%s ==> flash:%s \r\n",
                               pu1String, pu1SrcFileName, au1LocalDstFile);
                }
                return (CLI_SUCCESS);
            }
            if (i4RetVal == SFTP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\n\n\r%% Unable to copy remote file\r\n");

                /*SFTP file tranfer failed.So copied file will be removed here */
                FileDelete (au1LocalDstFile);

                return (CLI_FAILURE);
            }

        }
#endif
    }
    else if (DstIpAddress.u1Afi != 0)
    {
        /* Resolving Destination hostname here */
        /* If family is DNS host name resolve and copy IpAddress to au1Addr. */
        if (DstIpAddress.u1Afi == IPVX_DNS_FAMILY)
        {
            u1TempLen = STRLEN (pu1DstHostName);
            MEMSET (au1TempName, 0, DNS_MAX_QUERY_LEN);
            STRNCPY (au1TempName, pu1DstHostName, u1TempLen);
            au1TempName[u1TempLen] = '\0';
            i4RetVal =
                FsUtlIPvXResolveHostName (au1TempName, DNS_BLOCK,
                                          &ResolvedIpInfo);

            if (i4RetVal == DNS_NOT_RESOLVED)
            {
                CliPrintf (CliHandle, "\n%%Cannot resolve the host name \n");
                return (CLI_FAILURE);
                /*not resolved */
            }
            else if (i4RetVal == DNS_IN_PROGRESS)
            {
                CliPrintf (CliHandle,
                           "\n%% Host name resolution in Progress \n");
                return (CLI_FAILURE);
                /*in progress */
            }
            else if (i4RetVal == DNS_CACHE_FULL)
            {
                CliPrintf (CliHandle,
                           "\n%%DNS cache full, cannot resolve at the moment");
                return (CLI_FAILURE);
                /*cache full */
            }

            MEMSET (au1TempName, 0, DNS_MAX_QUERY_LEN);
            MEMSET (DstIpAddress.au1Addr, 0, IPVX_MAX_INET_ADDR_LEN);
            /* If the hostname resolves to both IPv4 and IPv6 
             * address, IPv6 address is considered */
            if (ResolvedIpInfo.Resolv6Addr.u1AddrLen != 0)
            {
                MEMCPY (au1TempName, ResolvedIpInfo.Resolv6Addr.au1Addr,
                        IPVX_IPV6_ADDR_LEN);
                DstIpAddress.u1AddrLen = (UINT1) IPVX_IPV6_ADDR_LEN;
                DstIpAddress.u1Afi = (UINT1) IPVX_ADDR_FMLY_IPV6;
                MEMCPY (DstIpAddress.au1Addr, au1TempName, IPVX_IPV6_ADDR_LEN);
            }
            else
            {
                MEMCPY (au1TempName, ResolvedIpInfo.Resolv4Addr.au1Addr,
                        IPVX_IPV4_ADDR_LEN);
                DstIpAddress.u1AddrLen = (UINT1) IPVX_IPV4_ADDR_LEN;
                DstIpAddress.u1Afi = (UINT1) IPVX_ADDR_FMLY_IPV4;
                MEMCPY (DstIpAddress.au1Addr, au1TempName, IPVX_IPV4_ADDR_LEN);
            }
        }
        /* Local to Remote */
        SPRINTF ((CHR1 *) au1LocalSrcFile, "%s%s", FLASH, pu1SrcFileName);
        if (u1DstRemoteType == ISS_TFTP)
        {
            if ((i4RetVal =
                 tftpcSendFile (DstIpAddress, pu1DstFileName,
                                au1LocalSrcFile)) != TFTPC_OK)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to copy file from flash to remote "
                           "location\r\n");
                return (CLI_FAILURE);
            }
            else
            {
                MEMSET (pu1TempString, 0, IPVX_MAX_INET_ADDR_LEN);

                IssPrintStrIpAddress (DstIpAddress.au1Addr, pu1TempString);
                CliPrintf (CliHandle, "\rCopied flash:%s ==> tftp://%s/%s \r\n",
                           au1LocalSrcFile, pu1TempString, pu1DstFileName);
                return (CLI_SUCCESS);
            }
        }
#ifdef SSH_WANTED
        if (u1DstRemoteType == ISS_SFTP)
        {
            if ((i4RetVal =
                 sftpcArSendFile (pu1DstUserName, pu1DstPassWd,
                                  DstIpAddress, pu1DstFileName,
                                  au1LocalSrcFile)) != SFTP_SUCCESS)
            {
                SftpcArGetCopyStatus (&u1sftpCopyStatus);
                if (u1sftpCopyStatus == OSIX_FAILURE)
                {
                    UtlTrcLog (1, 1, "",
                               "\n\n\r%% SFTP copy failed -- retrying !!!\r\n");
                    /* Delay added to delete the running task *
                     * as OsixRscFind would return FAILURE if it tries to create
                     * task with same name in short time*/
                    OsixTskDelay (2 * SYS_TIME_TICKS_IN_A_SEC);
                    i4RetVal = sftpcArSendFile (pu1DstUserName, pu1DstPassWd,
                                                DstIpAddress, pu1DstFileName,
                                                au1LocalSrcFile);
                }
            }

            if (i4RetVal == SFTP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to copy file from flash to remote "
                           "location\r\n");
                return (CLI_FAILURE);
            }
            else
            {
                MEMSET (au1IfIP, 0, MAX_NAME_SIZE);
                MEMCPY ((UINT1 *) (&(u4DstIpAddress)), DstIpAddress.au1Addr,
                        sizeof (UINT4));
                u4DstIpAddress = OSIX_NTOHL (u4DstIpAddress);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4DstIpAddress);
                CliPrintf (CliHandle, "\rCopied flash:%s ==> sftp://%s/%s \r\n",
                           au1LocalSrcFile, pu1String, pu1DstFileName);
                return (CLI_SUCCESS);
            }
        }
#endif
    }
    else
    {
        /* Local Copy */

        SPRINTF ((CHR1 *) au1LocalDstFile, "%s%s", FLASH, pu1DstFileName);
        SPRINTF ((CHR1 *) au1LocalSrcFile, "%s%s", FLASH, pu1SrcFileName);
        if ((i4RetVal =
             issCopyLocalFile (au1LocalSrcFile,
                               au1LocalDstFile)) == ISS_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to copy file \r\n");
            return (CLI_FAILURE);
        }
        else
        {
            CliPrintf (CliHandle, "\rCopied flash:%s ==> flash:%s \r\n",
                       au1LocalSrcFile, au1LocalDstFile);
            return (CLI_SUCCESS);
        }
    }

#ifndef SSH_WANTED
    UNUSED_PARAM (pu1SrcUserName);
    UNUSED_PARAM (pu1SrcPassWd);
    UNUSED_PARAM (pu1DstUserName);
    UNUSED_PARAM (pu1DstPassWd);
    UNUSED_PARAM (u4DstIpAddress);
    UNUSED_PARAM (u4SrcIpAddress);
    UNUSED_PARAM (pu1SrcHostName);
    UNUSED_PARAM (pu1DstHostName);
    UNUSED_PARAM (u1sftpCopyStatus);
#endif
    return (CLI_SUCCESS);
}

/***************************************************************/
/*  Function Name   : CliIssShowDebugLoggin                    */
/*  Description     : This function is used to display debug   */
/*                    logs from file                           */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliIssShowDebugLogging (tCliHandle CliHandle)
{
    UINT1              *pu1String = NULL;
    /*Making it static to address stack size issues */
    static UINT1        au1Temp[ISS_LOG_BUFFER_SIZE];

    MEMSET (au1Temp, 0, ISS_LOG_BUFFER_SIZE);
    pu1String = &au1Temp[0];

    UtlGetLogs ((CHR1 *) pu1String, UTL_MAX_LOGS);
    CliPrintf (CliHandle, "\r\n%s\r\n", pu1String);

    return (CLI_SUCCESS);
}

/***************************************************************/
/*  Function Name   : CliIssShowStandbyDebugLogging            */
/*  Description     : This function is used to display debug   */
/*                    logs from file                           */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliIssShowStandbyDebugLogging (tCliHandle CliHandle)
{
    FILE               *pPeerLogFile;
    CHR1                ac1ReadBuffer[MAX_COLUMN_SIZE];

    MEMSET (ac1ReadBuffer, 0, MAX_COLUMN_SIZE);

    if ((pPeerLogFile =
         FOPEN ((CONST CHR1 *) ISS_STANDBY_LOG_FILE, "r")) == NULL)
    {
        CliPrintf (CliHandle, "\r%% File does not exist \r\n");
        return CLI_FAILURE;
    }

    while (!feof (pPeerLogFile))
    {
        fgets (ac1ReadBuffer, MAX_COLUMN_SIZE, pPeerLogFile);
        CliPrintf (CliHandle, "%s", ac1ReadBuffer);
    }
    fclose (pPeerLogFile);

    return (CLI_SUCCESS);

}

/***************************************************************/
/*  Function Name   : CliIssIpAuth                             */
/*  Description     : This function is used to add new         */
/*                    IP authorized manager.                   */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4IpAddr    - IP Address of manager.     */
/*                    u4IpMask    - IP Mask.                   */
/*                    au1PortList - Ports allowed for manager. */
/*                    au1VlanList - Vlans allowed for manager. */
/*                    u4Services  - Services allowed.          */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliIssIpAuth (tCliHandle CliHandle, UINT4 u4IpAddr,
              UINT4 u4IpMask, UINT1 *pu1PortList,
              UINT1 *pu1VlanList, UINT1 u1AuthOOBFlag, UINT4 u4Services)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4RowStatus;
    UINT4               u4NewEntry = 0;
    static UINT1        au1PortList[ISS_AUTH_PORT_LIST_SIZE];
    static UINT1        au1VlanList[ISS_VLAN_LIST_SIZE];
    tSNMP_OCTET_STRING_TYPE PortList;
    tSNMP_OCTET_STRING_TYPE VlanList;

    PortList.i4_Length = ISS_AUTH_PORT_LIST_SIZE;
    PortList.pu1_OctetList = au1PortList;
    VlanList.i4_Length = ISS_VLAN_LIST_SIZE;
    VlanList.pu1_OctetList = au1VlanList;

    if (nmhValidateIndexInstanceIssIpAuthMgrTable (u4IpAddr, u4IpMask)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_ISS_INVALID_IP_ADDR_IP_MASK);
        return CLI_FAILURE;
    }

    if (nmhGetIssIpAuthMgrRowStatus (u4IpAddr, u4IpMask, (INT4 *) &u4RowStatus)
        == SNMP_FAILURE)
    {
        if (nmhTestv2IssIpAuthMgrRowStatus (&u4ErrorCode, u4IpAddr,
                                            u4IpMask, ISS_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Manager not configured, "
                       "since maximum number entries created\r\n");
            return CLI_FAILURE;
        }
        else
        {
            if (nmhSetIssIpAuthMgrRowStatus (u4IpAddr, u4IpMask,
                                             ISS_CREATE_AND_WAIT)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            u4NewEntry = ISS_NEW_ENTRY;
        }
    }

    if (nmhTestv2IssIpAuthMgrRowStatus (&u4ErrorCode, u4IpAddr,
                                        u4IpMask, ISS_NOT_IN_SERVICE)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% No such manager found \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetIssIpAuthMgrRowStatus (u4IpAddr, u4IpMask,
                                     ISS_NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    CLI_MEMSET (PortList.pu1_OctetList, 0, PortList.i4_Length);
    CLI_MEMCPY (PortList.pu1_OctetList, pu1PortList, PortList.i4_Length);
    CLI_MEMSET (VlanList.pu1_OctetList, 0, VlanList.i4_Length);
    CLI_MEMCPY (VlanList.pu1_OctetList, pu1VlanList, VlanList.i4_Length);

    if (nmhTestv2IssIpAuthMgrPortList (&u4ErrorCode,
                                       u4IpAddr, u4IpMask,
                                       &PortList) == SNMP_FAILURE)
    {
        if (u4NewEntry == ISS_NEW_ENTRY)
        {
            nmhSetIssIpAuthMgrRowStatus (u4IpAddr, u4IpMask, ISS_DESTROY);
        }
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2IssIpAuthMgrVlanList (&u4ErrorCode,
                                       u4IpAddr, u4IpMask,
                                       &VlanList) == SNMP_FAILURE)
    {
        if (u4NewEntry == ISS_NEW_ENTRY)
        {
            nmhSetIssIpAuthMgrRowStatus (u4IpAddr, u4IpMask, ISS_DESTROY);
        }
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2IssIpAuthMgrAllowedServices (&u4ErrorCode,
                                              u4IpAddr, u4IpMask,
                                              (INT4) u4Services) ==
        SNMP_FAILURE)
    {
        if (u4NewEntry == ISS_NEW_ENTRY)
        {
            nmhSetIssIpAuthMgrRowStatus (u4IpAddr, u4IpMask, ISS_DESTROY);
        }
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetIssIpAuthMgrPortList (u4IpAddr, u4IpMask,
                                    &PortList) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetIssIpAuthMgrVlanList (u4IpAddr, u4IpMask,
                                    &VlanList) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2IssIpAuthMgrOOBPort (&u4ErrorCode, u4IpAddr,
                                      u4IpMask,
                                      (INT4) u1AuthOOBFlag) == SNMP_FAILURE)
    {
        if (u4NewEntry == ISS_NEW_ENTRY)
        {
            nmhSetIssIpAuthMgrRowStatus (u4IpAddr, u4IpMask, ISS_DESTROY);
        }
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetIssIpAuthMgrOOBPort (u4IpAddr, u4IpMask,
                                   (INT4) u1AuthOOBFlag) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetIssIpAuthMgrAllowedServices (u4IpAddr, u4IpMask,
                                           u4Services) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetIssIpAuthMgrRowStatus (u4IpAddr, u4IpMask, ISS_ACTIVE)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliIssNoIpAuth                           */
/*  Description     : This function is used to remove          */
/*                    IP authorized manager.                   */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4IpAddr    - IP Address of manager.     */
/*                    u4IpMask    - IP Mask.                   */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliIssNoIpAuth (tCliHandle CliHandle, UINT4 u4IpAddr, UINT4 u4IpMask)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2IssIpAuthMgrRowStatus (&u4ErrorCode, u4IpAddr,
                                        u4IpMask, ISS_DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% No such manager found \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetIssIpAuthMgrRowStatus (u4IpAddr, u4IpMask,
                                     ISS_DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% No such manager found \r\n");
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliIssHttpPort                           */
/*  Description     : This function is used to configure the   */
/*                    HTTP port for the system.                */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    i4HttpPort  - HTTP port                  */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliIssHttpPort (tCliHandle CliHandle, INT4 i4HttpPort)
{
#ifdef WEBNM_WANTED
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2IssHttpPort (&u4ErrorCode, i4HttpPort) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetIssHttpPort (i4HttpPort) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle,
               "\r\nHTTP port number will take effect only when HTTP is "
               "disabled and enabled again\r\n");
#else
    CliPrintf (CliHandle, "\r\nCannot configure port number\r\n");
    UNUSED_PARAM (i4HttpPort);
#endif
    return CLI_SUCCESS;

}

/**************************************************************************/
/*  Function Name   : CliIssHttpStatus                                    */
/*  Description     : This function configures the HTTP Status            */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    i4HttpStatus - CLI_ENABLE/CLI_DISABLE               */
/*                                                                        */
/*  Output(s)       : NONE                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
CliIssHttpStatus (tCliHandle CliHandle, INT4 i4HttpStatus)
{
#ifdef WEBNM_WANTED
    UINT4               u4ErrCode;

    i4HttpStatus =
        (i4HttpStatus == CLI_ENABLE) ? (TRUE) : (ISS_HTTP_STATUS_DISABLED);

    if (nmhTestv2IssHttpStatus (&u4ErrCode, i4HttpStatus) == SNMP_SUCCESS)
    {
        if (nmhSetIssHttpStatus (i4HttpStatus) == SNMP_SUCCESS)
        {
            return (CLI_SUCCESS);
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
    }
    return (CLI_FAILURE);
#else
    CliPrintf (CliHandle, "\r\nCannot configure HTTP status\r\n");
    UNUSED_PARAM (i4HttpStatus);
    return (CLI_SUCCESS);
#endif
}

/**************************************************************************/
/*  Function Name   : CliIssTelnetStatus                                  */
/*  Description     : This function configures the TELNET Status          */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    i4TelnetStatus - CLI_ENABLE/CLI_DISABLE             */
/*                                                                        */
/*  Output(s)       : NONE                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
CliIssTelnetStatus (tCliHandle CliHandle, INT4 i4TelnetStatus)
{
    UINT4               u4ErrorCode = 0;

    if (SNMP_SUCCESS == nmhTestv2IssTelnetStatus (&u4ErrorCode, i4TelnetStatus))
    {
        if (SNMP_SUCCESS == nmhSetIssTelnetStatus (i4TelnetStatus))
        {
            return (CLI_SUCCESS);
        }
    }
    if (ISS_ENABLE == i4TelnetStatus)
    {
        CliPrintf (CliHandle, "\r%% Failed to start TELNET Service\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r%% Failed to stop TELNET Service\r\n");
    }
    return (CLI_FAILURE);
}

 /**************************************************************************/
/*  Function Name   : CliIssConfigDefValSaveFlag                          */
/*  Description     : This function configures the DefaVal Save Flag      */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    i4IncrSaveFlag - ISS_DEFVAL_SAVE_ENABLED (1) or     */
/*                                     ISS_DEFVAL_SAVE_DISABLED (2)       */
/*                                                                        */
/*  Output(s)       : NONE                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
CliIssConfigDefValSaveFlag (tCliHandle CliHandle, INT4 i4IncrSaveFlag)
{
    UINT4               u4ErrCode = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2IssConfigDefaultValueSaveOption (&u4ErrCode, i4IncrSaveFlag)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIssConfigDefaultValueSaveOption (i4IncrSaveFlag) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_ISS_SET_DEFVAL_SAVE_FAILED);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

 /**************************************************************************/
/*  Function Name   : CliIssConfigDefValSaveFlag                          */
/*  Description     : This function configures the DefaVal Save Flag      */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    i4IncrSaveFlag - ISS_DEBUG_TIMESTAMP_ENABLED (1) or  */
/*                                     ISS_DEBUG_TIMESTAMP_DISABLED (2)   */
/*                                                                        */
/*  Output(s)       : NONE                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
CliIssConfigDebugTimeStamp (tCliHandle CliHandle, INT4 i4IncrSaveFlag)
{
    UINT4               u4ErrCode = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2IssDebugTimeStampOption (&u4ErrCode, i4IncrSaveFlag)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIssDebugTimeStampOption (i4IncrSaveFlag) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_ISS_SET_DEBUG_TIMESTAMP_FAILED);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

 /*************************************************************************/
/*  Function Name   : CliIssSetRestoreType                                */
/*  Description     : This function configures the Restore Type           */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    i4RestoreType - ISS_MSR_RESTORE (1) or              */
/*                                    ISS_CSR_RESTORE (2)                 */
/*                                                                        */
/*  Output(s)       : NONE                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/
INT4
CliIssSetRestoreType (tCliHandle CliHandle, INT4 i4RestoreType)
{
    UINT4               u4ErrCode = 0;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2IssRestoreType (&u4ErrCode, i4RestoreType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIssRestoreType (i4RestoreType) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_ISS_SET_RESTORE_TYPE_FAILED);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : CliIssConfigIncrSaveFlag                            */
/*  Description     : This function configures the Incremental Save Flag  */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    i4IncrSaveFlag - ISS_INCR_SAVE_ENABLED (1) or       */
/*                                     ISS_INCR_SAVE_DISABLED (2)         */
/*                                                                        */
/*  Output(s)       : NONE                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
CliIssConfigIncrSaveFlag (tCliHandle CliHandle, INT4 i4IncrSaveFlag)
{
    UINT4               u4ErrCode;
    INT4                i4ResType = 0;

    UNUSED_PARAM (CliHandle);

    /* Get Restoration Type - whether CSR or MSR */
    i4ResType = IssGetRestoreTypeFromNvRam ();

    /* Validate the Value Passed and check whether any pre-requisite 
     * is required or not */
    if (nmhTestv2IssConfigIncrSaveFlag (&u4ErrCode, i4IncrSaveFlag)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_ISS_INCR_SAVE_DISABLE_FAILED);
        return CLI_FAILURE;
    }

    /* Set the Object */
    if (nmhSetIssConfigIncrSaveFlag (i4IncrSaveFlag) == SNMP_FAILURE)
    {
        if (i4IncrSaveFlag == ISS_INCR_SAVE_ENABLED)
        {
            CLI_SET_ERR (CLI_ISS_INCR_SAVE_ENABLE_FAILED);
        }
        else
        {
            CLI_SET_ERR (CLI_ISS_INCR_SAVE_DISABLE_FAILED);
        }
        return CLI_FAILURE;
    }
    if (i4ResType == SET_FLAG)
    {
        CliPrintf (CliHandle,
                   "\r%% Configuration will be reflected in next reboot "
                   "when mode is set as MSR\r\n");
    }
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : CliIssConfigAutoSaveFlag                            */
/*  Description     : This function configures the Auto Save Trigger      */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    i4AutoSaveFlag - ISS_AUTO_SAVE_ENABLED (1) or       */
/*                                     ISS_AUTO_SAVE_DISABLED (2)         */
/*                                                                        */
/*  Output(s)       : NONE                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/

INT4
CliIssConfigAutoSaveFlag (tCliHandle CliHandle, INT4 i4AutoSaveFlag)
{
    UINT4               u4ErrCode;
    INT4                i4ResType = 0;

    UNUSED_PARAM (CliHandle);

    /* Get Restoration Type - whether CSR or MSR */
    i4ResType = IssGetRestoreTypeFromNvRam ();

    /* Validate the Value Passed and check whether any pre-requisite 
     * is required or not */
    if (nmhTestv2IssConfigAutoSaveTrigger (&u4ErrCode, i4AutoSaveFlag)
        == SNMP_FAILURE)
    {
        if (i4AutoSaveFlag == ISS_AUTO_SAVE_ENABLED)
        {
            CLI_SET_ERR (CLI_ISS_AUTO_SAVE_ENABLE_FAILED);
        }
        else
        {
            CLI_SET_ERR (CLI_ISS_AUTO_SAVE_DISABLE_FAILED);
        }
        return CLI_FAILURE;
    }

    /* Set the Object */
    if (nmhSetIssConfigAutoSaveTrigger (i4AutoSaveFlag) == SNMP_FAILURE)
    {
        if (i4AutoSaveFlag == ISS_AUTO_SAVE_ENABLED)
        {
            CLI_SET_ERR (CLI_ISS_AUTO_SAVE_ENABLE_FAILED);
        }
        else
        {
            CLI_SET_ERR (CLI_ISS_AUTO_SAVE_DISABLE_FAILED);
        }
        return CLI_FAILURE;
    }

    if (i4ResType == SET_FLAG)
    {
        CliPrintf (CliHandle, "Configuration will be reflected in next reboot "
                   "when mode is set as MSR\r\n");
    }
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : CliIssConfigAutoPortCreate                          */
/*  Description     : This function configures the Auto Port create       */
/*                             Feature                                    */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    i4AutoPortCreate - ISS_AUTO_PORT_CREATE_ENABLED (1)*/
/*                    (or)            ISS_AUTO_PORT_CREATE_ENABLED     (2)*/
/*                                                                        */
/*  Output(s)       : NONE                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/
INT4
CliIssConfigAutoPortCreate (tCliHandle CliHandle, INT4 i4AutoPortCreate)
{
    UINT4               u4ErrCode;

    UNUSED_PARAM (CliHandle);

    /* Validate the Value Passed */
    if (nmhTestv2IssAutomaticPortCreate (&u4ErrCode, i4AutoPortCreate)
        == SNMP_FAILURE)
    {

        return CLI_FAILURE;
    }

    /* Set the Object */
    if (nmhSetIssAutomaticPortCreate (i4AutoPortCreate) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliIssShowIpAuth                         */
/*  Description     : This function is used to display         */
/*                    IP authorized managers.                  */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4IpAddr    - IP Address of manager.     */
/*                    u4Flag      - Denotes specific or all    */
/*                                  entries to be displayed.   */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliIssShowIpAuth (tCliHandle CliHandle, UINT4 u4IpAddr, UINT4 u4Flag)
{
    UINT4               u4CurrIpAddr;
    UINT4               u4CurrIpMask;
    UINT4               u4NextIpAddr = 0;
    UINT4               u4NextIpMask;

    CliPrintf (CliHandle, "\r\nIp Authorized Manager Table\r\n");
    CliPrintf (CliHandle, "\r---------------------------\r\n");

    if (nmhGetFirstIndexIssIpAuthMgrTable (&u4NextIpAddr,
                                           &u4NextIpMask) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    do
    {
        u4CurrIpAddr = u4NextIpAddr;
        u4CurrIpMask = u4NextIpMask;

        /* Display for particular manager */
        if (((u4Flag == ISS_SPECIFIC_ENTRY) && (u4IpAddr == u4CurrIpAddr)) ||
            /* Display for all managers */
            (u4Flag == ISS_ALL_ENTRIES))
        {
            IssShowIpManagerTable (CliHandle, u4CurrIpAddr, u4CurrIpMask);
        }
    }
    while (nmhGetNextIndexIssIpAuthMgrTable (u4CurrIpAddr,
                                             &u4NextIpAddr, u4CurrIpMask,
                                             &u4NextIpMask) == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliIssShowHttpStatus                     */
/*  Description     : This function is used to display         */
/*                    the http server status                   */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliIssShowHttpStatus (tCliHandle CliHandle)
{
#ifdef WEBNM_WANTED
    INT4                i4HttpStatus = ISS_HTTP_STATUS_ENABLED;
    INT4                i4HttpPort;
    INT4                i4HttpRequests;
    INT4                i4HttpInvalids;

    nmhGetIssHttpStatus (&i4HttpStatus);
    CliPrintf (CliHandle, "\r\nHTTP server status              : ");
    if (i4HttpStatus == ISS_HTTP_STATUS_DISABLED)
    {
        CliPrintf (CliHandle, "Disabled\r\n");
    }
    else if (i4HttpStatus == ISS_HTTP_STATUS_ENABLED)
    {
        CliPrintf (CliHandle, "Enabled\r\n");
    }

    i4HttpPort = HttpGetSourcePort ();
    CliPrintf (CliHandle, "HTTP port is                    : %d \r\n",
               i4HttpPort);
    nmhGetFsHttpRequestCount (&i4HttpRequests);
    CliPrintf (CliHandle, "HTTP Requests In                : %d \r\n",
               i4HttpRequests);
    nmhGetFsHttpRequestDiscards (&i4HttpInvalids);
    CliPrintf (CliHandle, "HTTP Invalids                   : %d \r\n",
               i4HttpInvalids);

#else
    CliPrintf (CliHandle, "\r\nHTTP sever status cannot be displayed\r\n");
#endif
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliIssShowTelnetStatus                   */
/*  Description     : This function is used to display         */
/*                    the telnet server status                 */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliIssShowTelnetStatus (tCliHandle CliHandle)
{
    INT4                i4TelnetStatus = 0;

    nmhGetIssTelnetStatus (&i4TelnetStatus);
    CliPrintf (CliHandle, "telnet service ");
    switch (i4TelnetStatus)
    {
        case ISS_ENABLE:
        {
            CliPrintf (CliHandle, "enabled\r\n");
            break;
        }
        case ISS_DISABLE:
        {
            CliPrintf (CliHandle, "disabled\r\n");
            break;
        }
        default:
        {
            CliPrintf (CliHandle, "cannot be displayed\r\n");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IssShowIpManagerTable                    */
/*  Description     : This function is used to display         */
/*                    particular manager.                      */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4IpMgrAddr - IP Address of manager.     */
/*                    u4IpMgrMask - IP Mask.                   */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
IssShowIpManagerTable (tCliHandle CliHandle, UINT4 u4IpAddr, UINT4 u4IpMask)
{
    CHR1               *pu1Netaddress;
    UINT4               u4Services;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4IpAuthMgrOOBPort;
    UINT1               au1PortList[ISS_AUTH_PORT_LIST_SIZE];
    UINT1              *pu1VlanList = NULL;
    tSNMP_OCTET_STRING_TYPE PortList;
    tSNMP_OCTET_STRING_TYPE VlanList;

    PortList.i4_Length = ISS_AUTH_PORT_LIST_SIZE;
    PortList.pu1_OctetList = au1PortList;

    pu1VlanList = UtlShMemAllocVlanList ();
    if (pu1VlanList == NULL)
    {
        CliPrintf (CliHandle, "\r%% Memory allocation for Vlan List"
                   " failed.\r\n");
        return CLI_FAILURE;
    }

    VlanList.i4_Length = ISS_VLAN_LIST_SIZE;
    VlanList.pu1_OctetList = pu1VlanList;

    CLI_MEMSET (PortList.pu1_OctetList, 0, ISS_AUTH_PORT_LIST_SIZE);
    CLI_MEMSET (VlanList.pu1_OctetList, 0, ISS_VLAN_LIST_SIZE);

    nmhGetIssIpAuthMgrAllowedServices (u4IpAddr, u4IpMask,
                                       (INT4 *) &u4Services);

    nmhGetIssIpAuthMgrPortList (u4IpAddr, u4IpMask, &PortList);

    nmhGetIssIpAuthMgrVlanList (u4IpAddr, u4IpMask, &VlanList);

    nmhGetIssIpAuthMgrOOBPort (u4IpAddr, u4IpMask, &i4IpAuthMgrOOBPort);

    CLI_CONVERT_IPADDR_TO_STR (pu1Netaddress, u4IpAddr);
    CliPrintf (CliHandle, "\r\nIp Address       : %s\r\n", pu1Netaddress);

    CLI_CONVERT_IPADDR_TO_STR (pu1Netaddress, u4IpMask);
    CliPrintf (CliHandle, "Ip Mask          : %s\r\n", pu1Netaddress);

    CliPrintf (CliHandle, "Services allowed : ");
    IssDisplayServicesAllowed (CliHandle, u4Services);
    CliPrintf (CliHandle, "\r\n");

    CliOctetToIfName (CliHandle, "Ports allowed    :",
                      &PortList, ISS_MAX_PORTS + LA_DEV_MAX_TRUNK_GROUP,
                      ISS_AUTH_PORT_LIST_SIZE, 0, &u4PagingStatus, 4);

    if (i4IpAuthMgrOOBPort == ISS_TRUE)
    {
        CliPrintf (CliHandle, "On cpu0         : Allow\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "On cpu0         : Deny \r\n");
    }

    CliPrintf (CliHandle, "Vlans allowed    : ");
    IssIpAuthOctetToVlanList (CliHandle, VlanList.pu1_OctetList,
                              (UINT4) VlanList.i4_Length);
    CliPrintf (CliHandle, "\r\n\r\n");
    UtlShMemFreeVlanList (pu1VlanList);

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IssIpAuthOctetToVlanList                 */
/*  Description     : This function is used to convert         */
/*                    octet to vlan list                       */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    pOctet      - Pointer to octet           */
/*                    u4OctetLen  - Length of octet            */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
IssIpAuthOctetToVlanList (tCliHandle CliHandle, UINT1 *pOctet, UINT4 u4OctetLen)
{
    UINT2               u2VlanIdStart = ISS_MIN_VLAN_ID;
    UINT2               u2VlanIdEnd = ISS_MAX_VLAN_ID;
    UINT2               u2VlanId = 0;
    UINT2               u2Check = 0;
    UINT4               u4Status = 0;
    UINT1               bOutCome;
    UINT1              *pu1VlanList = NULL;

    CLI_MEMSET (au1TmpVlanList, 0, ISS_VLAN_LIST_SIZE);

    CLI_MEMSET (au1TmpVlanList, 0xff, ISS_VLAN_LIST_SIZE - 1);
    ISS_LAST_BYTE_IN_PORTLIST (au1TmpVlanList, VLAN_DEV_MAX_VLAN_ID,
                               ISS_VLAN_LIST_SIZE);

    pu1VlanList = UtlShMemAllocVlanList ();
    if (pu1VlanList == NULL)
    {
        CliPrintf (CliHandle, "\r%% Memory allocation for Vlan List"
                   " failed.\r\n");
        return;
    }

    CLI_MEMSET (pu1VlanList, 0, ISS_VLAN_LIST_SIZE);
    CLI_MEMCPY (pu1VlanList, pOctet, u4OctetLen);

    u4Status =
        (UINT4) CLI_MEMCMP (pu1VlanList, au1TmpVlanList, ISS_VLAN_LIST_SIZE);
    if (u4Status == 0)
    {
        UtlShMemFreeVlanList (pu1VlanList);
        CliPrintf (CliHandle, "All Available Vlans");
        return;
    }

    u4Status = CLI_SUCCESS;
    for (u2VlanId = u2VlanIdStart; u2VlanId <= u2VlanIdEnd; u2VlanId++)
    {
        ISS_IS_MEMBER_OF_PORTLIST (pu1VlanList, u2VlanId, bOutCome);
        if (bOutCome == ISS_TRUE)
        {
            if (u2Check == 0)
            {
                u4Status = (UINT4) CliPrintf (CliHandle, "%d", u2VlanId);
            }

            else if (!(u2Check % 10))
            {
                u4Status = (UINT4) CliPrintf (CliHandle, "\n, %25d", u2VlanId);
            }
            else
            {
                u4Status = (UINT4) CliPrintf (CliHandle, ", %d", u2VlanId);
            }
            u2Check++;
        }

        if (u4Status == CLI_FAILURE)
        {
            break;
        }
    }
    UtlShMemFreeVlanList (pu1VlanList);

    return;
}

/***************************************************************/
/*  Function Name   : IssDisplayServicesAllowed                */
/*  Description     : This function is used to display         */
/*                    services allowed                         */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4Services  - Allowed services           */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
IssDisplayServicesAllowed (tCliHandle CliHandle, UINT4 u4Services)
{
    UINT4               u4Index;
    UINT4               u4Flag = ISS_FALSE;
    UINT4               u4CheckService;

    if (u4Services == ISS_ALL_MASK)
    {
        CliPrintf (CliHandle, "ALL");
        return;
    }

    for (u4Index = 0; u4Index < ISS_MAX_AUTH_MGR_PROTOCOLS; u4Index++)
    {
        u4CheckService = u4Services & (UINT4) (0x01 << u4Index);

        switch (u4CheckService)
        {
            case ISS_SNMP_MASK:

                if (u4Flag == ISS_TRUE)
                {
                    CliPrintf (CliHandle, ", SNMP");
                }
                else
                {
                    CliPrintf (CliHandle, "SNMP");
                }

                u4Flag = ISS_TRUE;
                break;

            case ISS_TELNET_MASK:

                if (u4Flag == ISS_TRUE)
                {
                    CliPrintf (CliHandle, ", TELNET");
                }
                else
                {
                    CliPrintf (CliHandle, "TELNET");
                }

                u4Flag = ISS_TRUE;
                break;

            case ISS_HTTP_MASK:

                if (u4Flag == ISS_TRUE)
                {
                    CliPrintf (CliHandle, ", HTTP");
                }
                else
                {
                    CliPrintf (CliHandle, "HTTP");
                }

                u4Flag = ISS_TRUE;
                break;

            case ISS_HTTPS_MASK:

                if (u4Flag == ISS_TRUE)
                {
                    CliPrintf (CliHandle, ", HTTPS");
                }
                else
                {
                    CliPrintf (CliHandle, "HTTPS");
                }

                u4Flag = ISS_TRUE;
                break;

            case ISS_SSH_MASK:

                if (u4Flag == ISS_TRUE)
                {
                    CliPrintf (CliHandle, ", SSH");
                }
                else
                {
                    CliPrintf (CliHandle, "SSH");
                }

                u4Flag = ISS_TRUE;
                break;

            default:
                break;
        }
    }

    return;
}

/***************************************************************/
/*  Function Name   : CliIssShowDebugging                      */
/*  Description     : This function is used to display         */
/*                    debug levels of all modules              */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4Services  - Allowed services           */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
INT4
CliIssShowDebugging (tCliHandle CliHandle)
{
#ifdef DHCP_RLY_WANTED
    UINT4               u4ContextId = 0;
#endif
    CliPrintf (CliHandle, "\r\n");

#ifdef ELPS_WANTED
    ElpsCliShowDebugging (CliHandle, L2IWF_DEFAULT_CONTEXT);
#endif
#ifdef ERPS_WANTED
    ErpsCliShowDebugging (CliHandle, L2IWF_DEFAULT_CONTEXT);
#endif
#ifdef ECFM_WANTED
    EcfmCliShowDebug (CliHandle);
#endif
#ifdef Y1564_WANTED
    Y1564CliShowDebug (CliHandle, L2IWF_DEFAULT_CONTEXT);
#endif

#ifdef RFC2544_WANTED
    R2544CliShowDebug (CliHandle);
#endif
#ifdef FM_WANTED

#ifdef MPLS_WANTED
    IssMplsFmShowDebugging (CliHandle);
#endif
#endif

#ifdef NPAPI_WANTED
    IssNpapiShowDebugging (CliHandle);
#endif

#ifdef EOAM_WANTED
    EoamCliShowDebugging (CliHandle);
#ifdef EOAM_FM_WANTED
    FmCliShowDebugging (CliHandle);
#endif /* EOAM_FM_WANTED */
#endif /* EOAM_WANTED */

#ifdef PNAC_WANTED
    IssPnacShowDebugging (CliHandle);
#endif

#if defined (RSTP_WANTED) || defined (MSTP_WANTED) || defined (PVRST_WANTED)
    IssSpanningTreeShowDebugging (CliHandle);
#endif

#if defined IGS_WANTED
    IssSnoopShowDebugging (CliHandle, IPVX_ADDR_FMLY_IPV4);
#endif

#if defined MLDS_WANTED
    IssSnoopShowDebugging (CliHandle, IPVX_ADDR_FMLY_IPV6);
#endif

#ifdef VLAN_WANTED
    IssVlanShowDebugging (CliHandle);
#ifdef GARP_WANTED
    IssGarpShowDebugging (CliHandle);
#endif
#endif

#ifdef RADIUS_WANTED
    IssRadiusShowDebugging (CliHandle);
#endif

#ifdef SSL_WANTED
    IssSslShowDebugging (CliHandle);
#endif

#ifdef SSH_WANTED
    IssSshShowDebugging (CliHandle);
#endif

#ifdef DHCP_RLY_WANTED
    /* Starting from the default(0) context */
    u4ContextId = 0;
    do
    {
        IssDhcpRelayShowDebugging (CliHandle, (INT4) u4ContextId);
    }
    while (VcmGetNextActiveL3Context (u4ContextId, &u4ContextId)
           != VCM_FAILURE);
#endif

#ifdef DHCP_SRV_WANTED
    IssDhcpSrvShowDebugging (CliHandle);
#endif

#ifdef DHCPC_WANTED
    IssDhcpClientShowDebugging (CliHandle);
#endif

#ifdef DHCP6_CLNT_WANTED
    IssDhcp6ClientShowDebugging (CliHandle);
#endif

#ifdef DHCP6_RLY_WANTED
    IssDhcp6RelayShowDebugging (CliHandle);
#endif

#ifdef DHCP6_SRV_WANTED
    IssDhcp6ServerShowDebugging (CliHandle);
#endif

#ifdef OSPF_WANTED
    IssOspfShowDebugging (CliHandle, OSPF_DEFAULT_CXT_ID);
#endif

#ifdef OSPFTE_WANTED
    IssOspfteCliShowDebugging (CliHandle);
#endif

#ifdef TLM_WANTED
    IssTlmCliShowDebugging (CliHandle);
#endif

#ifdef BFD_WANTED
    IssBfdShowDebugging (CliHandle, VCM_DEFAULT_CONTEXT);
#endif

#if defined (PIM_WANTED) || defined (PIMV6_WANTED)
    IssPimShowDebugging (CliHandle);
#endif

#ifdef RIP_WANTED
    IssRipShowDebugging (CliHandle);
#endif

#ifdef DVMRP_WANTED
    IssDvmrpShowDebugging (CliHandle);
#endif

#ifdef MFWD_WANTED
    IssMfwdShowDebugging (CliHandle);
#endif
#ifdef IGMP_WANTED
    IssIgmpShowDebugging (CliHandle);
#endif

#ifdef TACACS_WANTED
    IssTacacShowDebugging (CliHandle);
#endif

#ifdef BGP_WANTED
    IssBgpShowDebugging (CliHandle);
#endif

#ifdef OSPF3_WANTED
    IssOspf3ShowDebugging (CliHandle);
#endif

#ifdef LLDP_WANTED
    LldpCliShowDebugging (CliHandle);
#endif

#ifdef RMON2_WANTED
    Rmon2CliShowTrace (CliHandle);
#endif

#ifdef DSMON_WANTED
    DsmonCliShowTrace (CliHandle);
#endif

#ifdef TAC_WANTED
    TacCliShowDebugging (CliHandle);
#endif
#ifdef DCBX_WANTED
    IssDCBXShowDebugging (CliHandle);
#endif /* DCBX_WANTED */
#ifdef CN_WANTED
    IssCnShowDebugging (CliHandle);
#endif /* CN_WANTED */
#ifdef CFA_WANTED
    IssIpdbShowDebugging (CliHandle);
    IssInterfaceShowDebugging (CliHandle);
    IssDhcpSnoopingShowDebugging (CliHandle);
#endif /* CFA_WANTED */
#ifdef RIP6_WANTED
    IssRip6ShowDebugging (CliHandle);
#endif /*RIP6_WANTED */
#ifdef IP6_WANTED
    IssIp6ShowDebugging (CliHandle);
#endif
#ifdef VRRP_WANTED
    IssVrrpShowDebugging (CliHandle);
#endif /*VRRP_WANTED */
#ifdef SYNCE_WANTED
    IssSynceShowDebugging (CliHandle);
#endif /*SYNCE__WANTED */
#ifdef DNS_WANTED
    DnsCliShowTraceOption (CliHandle);
#endif
#ifdef LA_WANTED
    IssLaShowDebugging (CliHandle);
#endif
#ifdef SNTP_WANTED
    IssSntpShowDebugging (CliHandle);
#endif /*SNTP_WANTED */
#ifdef HB_WANTED
    HbCliHbShowDebugInfo (CliHandle);
#endif
#ifdef RM_WANTED
    IssRmgrShowDebugging (CliHandle);
    IssRmgrModuleShowDebugging (CliHandle);
#endif
#ifdef ICCH_WANTED
    IssIcchShowDebugging (CliHandle);
#endif
#ifdef QOSX_WANTED
    IssQosShowDebugging (CliHandle);
#endif /*QOSX_WANTED */
#ifdef MSDP_WANTED
    IssMsdpShowDebugging (CliHandle);
#endif
#ifdef MRP_WANTED
    IssMrpShowDebugging (CliHandle);
#endif

#ifdef PBB_WANTED
    PbbCliShowDebugging (CliHandle);
#endif
#ifdef ISIS_WANTED
    IsisCliShowDbgInfo (CliHandle);
#endif
#ifdef PBBTE_WANTED
    IssPbbTeShowDebugging (CliHandle);
#endif
#ifdef MLD_WANTED
    IssMldShowDebugging (CliHandle);
#endif
#ifdef MPLS_WANTED
    IssMplsCommonShowDebugging (CliHandle);
    IssMplsTeShowDebugging (CliHandle);
    IssMplsL2vpnShowDebugging (CliHandle);
    IssMplsOamShowDebugging (CliHandle);
#ifdef RFC6374_WANTED
    IssMplsPmRFC6374ShowDebugging (CliHandle);
#endif
#ifdef MPLS_SIG_WANTED
    IssMplsLdpShowDebugging (CliHandle);
    IssRsvpShowDebugging (CliHandle);
#endif
#ifdef LSPP_WANTED
    IssMplsOamEchoShowDebugging (CliHandle);
#endif
#endif

#if defined ARP_WANTED && defined IP_WANTED
    IssIpArpShowDebugging (CliHandle);
#endif

#if defined ISSU_WANTED
    IssuCliShowDebug (CliHandle);
#endif

    return (CLI_SUCCESS);
}

/***************************************************************/
/*  Function Name   : CliIssCheckMsrStatus                     */
/*  Description     : This function is used to check           */
/*                    if other MSR operations are in progress  */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4Services  - Allowed services           */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
INT4
CliIssCheckMsrStatus (tCliHandle CliHandle)
{
    INT4                i4MibSaveResult;
    INT4                i4RemoteSaveResult;
    INT4                i4DlStatus;
    INT4                i4RestoreStatus;

    nmhGetIssConfigSaveStatus (&i4MibSaveResult);
    if (i4MibSaveResult == MIB_SAVE_IN_PROGRESS)
    {
        CliPrintf (CliHandle, "\r%% Local save already in progress\r\n");
        return CLI_FAILURE;
    }
    nmhGetIssRemoteSaveStatus (&i4RemoteSaveResult);
    if (i4RemoteSaveResult == MIB_SAVE_IN_PROGRESS)
    {
        CliPrintf (CliHandle,
                   "\r%% Remote save or log upload already in progress\r\n");
        return CLI_FAILURE;
    }
    nmhGetIssDownloadStatus (&i4DlStatus);
    if (i4DlStatus == MIB_DOWNLOAD_IN_PROGRESS)
    {
        CliPrintf (CliHandle, "\r%% Image download already in progress\r\n");
        return CLI_FAILURE;
    }
    nmhGetIssConfigRestoreStatus (&i4RestoreStatus);
    if (i4RestoreStatus == MIB_RESTORE_IN_PROGRESS)
    {
        CliPrintf (CliHandle, "\r%% Config restore already in progress\r\n");
        return CLI_FAILURE;
    }
    return (CLI_SUCCESS);
}

/***************************************************************/
/*  Function Name   : CliIssShowRunningConfig                  */
/*  Description     : This function is used to display         */
/*                    the currently operating Mutiple Instance */
/*                    based configuration of                   */
/*                    all the modules in the system            */
/*  Input(s)        :                                          */
/*                    CliHandle  - CLI Handle                  */
/*                    u4Module   - Module for which the        */
/*                                 configuration to be         */
/*                                 displayed                   */
/*                    u4Args     - Variable argument. It could */
/*                                 be interface index,         */
/*                                 VLAN ID..                   */
/*                    u4ContextType  - Context spcific Configrn*/
/*                    pu1ContextName - ContextName             */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS or CLI_FAILURE               */
/***************************************************************/

INT4
CliIssShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module, UINT4 u4Args,
                         UINT1 *pu1ContextName)
{
    INT4                i4CxtType = 0;
    UINT4               u4BridgeMode = VLAN_INVALID_BRIDGE_MODE;
    UINT4               u4TempContextId = 0;
    UINT4               u4CurrContextId = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4L2Mode = 0;
    UINT4               u4L3Mode = 0;
    UINT2               u2LocalPortId;
    UINT1               au1TempContextName[L2IWF_CONTEXT_ALIAS_LEN + 1];
    UINT1              *pu1TempContextName;
    UINT1               u1BangStatus = FALSE;
    UINT1               u1VrfStatus = ISS_ZERO_ENTRY;

#ifdef RMON_WANTED
    UINT1               u1Flag = ISS_SUCCESS;
    INT4                i4Val = ISS_FAILURE;
#endif
    CliUnRegisterLock (CliHandle);

    ISS_UNLOCK ();

    CliSetSkipDoubleBang (CLI_HANDLE_BANG);
    CliPrintf (CliHandle, "\r\n#Building configuration...\n");
    CliPrintf (CliHandle, "! \r\n");

#ifdef MRP_WANTED
    if (u4Module == ISS_SHOW_MRP_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {

        if (VcmGetFirstActiveContext (&u4ContextId) == VCM_SUCCESS)
        {
            MrpSrcShowRunningConfig (CliHandle, u4ContextId);
            u4TempContextId = u4ContextId;
        }

        while (VcmGetNextActiveContext (u4TempContextId, &u4ContextId) ==
               VCM_SUCCESS)
        {
            MrpSrcShowRunningConfig (CliHandle, u4ContextId);
            u4TempContextId = u4ContextId;

        }
    }
#endif

    if (u4Module == ISS_ENT_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        EntShowRunningConfig (CliHandle);
    }

#ifdef SYNCE_WANTED
    if (u4Module == ISS_SYNCE_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        SynceShowRunningConfig (CliHandle, u4Module);
    }
#endif

#ifdef OPENFLOW_WANTED
    if (u4Module == ISS_OFCL_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        OfcShowRunningConfig (CliHandle, u4Module);
    }
#endif
#ifdef ECFM_WANTED
    if (u4Module == ISS_ECFM_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        EcfmGlobalShowRunningConfig (CliHandle);
    }
#endif

    pu1TempContextName = pu1ContextName;

    u4L2Mode = (UINT4) VcmGetL2ModeExt ();
    u4L3Mode = (UINT4) VcmGetL3ModeExt ();

#ifdef SYSLOG_WANTED
    if (u4Module == ISS_SYSLOG_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        SyslgShowRunningConfig (CliHandle);
    }
#endif
#ifdef PBB_WANTED
    if (u4Module == ISS_PBB_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        PbbGlobalShowRunningConfig (CliHandle);
    }
#endif

#ifdef BEEP_SERVER_WANTED
    if (u4Module == ISS_BEEP_SERVER_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        BeepSrvShowRunningConfig (CliHandle);
    }
#endif

#ifdef DHCP_SRV_WANTED
    if (u4Module == ISS_DHCP_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        DhcpSrvShowRunningConfig (CliHandle);
    }
#endif

#ifdef DHCPC_WANTED
    if (u4Module == ISS_DHCP_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        DhcpClientShowRunningConfig (CliHandle, u4Module);
    }
#endif

#ifdef DVMRP_WANTED
    if (u4Module == ISS_DVMRP_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        DvmrpShowRunningConfig (CliHandle, u4Module);
    }
#endif

#ifdef PPP_WANTED
    if (u4Module == ISS_PPP_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        PppShowRunningConfig (CliHandle, u4Module);
    }
#endif

#ifdef DCBX_WANTED
    if (u4Module == ISS_DCB_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        DCBXCliShowRunningConfig (CliHandle, OSIX_FALSE, OSIX_FALSE);
    }
#endif /* DCBX_WANTED */
#ifdef CN_WANTED
    if (u4Module == ISS_CN_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        CnCliShowGlobalRunningConfig (CliHandle);
    }
#endif /* CN_WANTED */

#ifdef DIFFSRV_WANTED
    if (u4Module == ISS_DIFFSERV_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
#ifdef NPAPI_WANTED
#ifdef SWC
        DiffservShowRunningConfig (CliHandle, u4Module);
#elif MRVLLS
        QoSShowRunningConfig (CliHandle);
#else
        DiffservShowRunningConfig (CliHandle);
#endif
#endif

    }
#endif

#ifdef DSMON_WANTED
    if ((u4Module == ISS_DSMON_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
    {

        DsmonShowRunningConfig (CliHandle);
    }
#endif
#ifdef RMON2_WANTED
    if ((u4Module == ISS_RMON2_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
    {

        Rmon2ShowRunningConfig (CliHandle);
    }
#endif
    if (u4Module == ISS_INTERFACE_SHOW_RUNNING_CONFIG)
    {
        VcmGetContextInfoFromIfIndex (u4Args, &u4ContextId, &u2LocalPortId);
    }

    if (pu1TempContextName != NULL)
    {
        if ((VcmIsSwitchExist (pu1ContextName, &u4ContextId) == VCM_FALSE) &&
            (VcmIsVrfExist (pu1ContextName, &u4ContextId) == VCM_FALSE))
        {
            /* If there is no context with the specified name, return */
            CliSetSkipDoubleBang (CLI_HANDLE_AND_RESET_BANG);
            return CLI_FAILURE;

        }
    }
    else
    {
        if (u4Module != ISS_INTERFACE_SHOW_RUNNING_CONFIG)
        {
            if (VcmGetFirstActiveContext (&u4ContextId) != VCM_SUCCESS)
            {
                CliSetSkipDoubleBang (CLI_HANDLE_AND_RESET_BANG);
                return CLI_FAILURE;
            }
            else
            {
                VcmGetVcCxtType (u4ContextId, &i4CxtType);
            }
        }
    }

    u4CurrContextId = u4ContextId;
    do
    {
        if (u4Module == ISS_INTERFACE_SHOW_RUNNING_CONFIG)
        {
            break;
        }
        else
        {
            VcmGetVcCxtType (u4CurrContextId, &i4CxtType);
            if (pu1TempContextName == NULL)
            {
                VcmGetAliasName (u4CurrContextId, au1TempContextName);
                pu1ContextName = au1TempContextName;
            }

            if (u4Module != ISS_INTERFACE_SHOW_RUNNING_CONFIG)
            {
                L2IwfGetBridgeMode (u4CurrContextId, &u4BridgeMode);

                /* Print the Dot1ad Bridge mode for,other than Default 
                 * context */
                if (((u4CurrContextId == L2IWF_DEFAULT_CONTEXT) &&
                     (u4BridgeMode != VLAN_CUSTOMER_BRIDGE_MODE)) ||
                    (u4CurrContextId != L2IWF_DEFAULT_CONTEXT))

                {
                    if (u4BridgeMode != VLAN_INVALID_BRIDGE_MODE)
                    {
                        CliPrintf (CliHandle, "\rswitch  %s \r\n",
                                   pu1ContextName);
                    }
                    switch (u4BridgeMode)
                    {
                        case VLAN_CUSTOMER_BRIDGE_MODE:
                            CliPrintf (CliHandle, "bridge-mode customer\r\n");
                            break;
                        case VLAN_PROVIDER_BRIDGE_MODE:
                            CliPrintf (CliHandle, "bridge-mode provider\r\n");
                            break;
                        case VLAN_PROVIDER_EDGE_BRIDGE_MODE:
                            CliPrintf (CliHandle,
                                       "bridge-mode provider-edge\r\n");
                            break;
                        case VLAN_PROVIDER_CORE_BRIDGE_MODE:
                            CliPrintf (CliHandle,
                                       "bridge-mode provider-core\r\n");
                            break;
                        case VLAN_PBB_ICOMPONENT_BRIDGE_MODE:
                            CliPrintf (CliHandle,
                                       "bridge-mode provider-backbone-icomp\r\n");
                            break;
                        case VLAN_PBB_BCOMPONENT_BRIDGE_MODE:
                            CliPrintf (CliHandle,
                                       "bridge-mode provider-backbone-bcomp\r\n");
                            break;
                        default:
                            break;
                    }

                    if (u4BridgeMode != VLAN_INVALID_BRIDGE_MODE)
                    {
                        CliPrintf (CliHandle, "! \r\n");
                    }
                }

                if (CFA_LNKUP_SYSTEM_CONTROL () == CFA_LINKUP_DELAY_START)
                {
                    CliPrintf (CliHandle, "set linkup-delay enable\r\n");

                }

                VcmGetVrfCounterStatus (u4CurrContextId, &u1VrfStatus);

                if ((u4L3Mode == VCM_MI_MODE) &&
                    (u4CurrContextId != VCM_DEFAULT_CONTEXT) &&
                    ((i4CxtType == VCM_L2_L3_CONTEXT) ||
                     (i4CxtType == VCM_L3_CONTEXT)))
                {

                    CliPrintf (CliHandle, "\rip vrf %s \r\n", pu1ContextName);
                }

                if ((VRF_STAT_ENABLE == u1VrfStatus)
                    && ((i4CxtType == VCM_L2_L3_CONTEXT)
                        || (i4CxtType == VCM_L3_CONTEXT)))
                {
                    CliPrintf (CliHandle, "\rset vrf %s counters enable \r\n",
                               pu1ContextName);
                }
            }

            u4TempContextId = u4CurrContextId;

            if (pu1TempContextName != NULL)
            {
                break;
            }
        }
    }
    while (VcmGetNextActiveContext (u4TempContextId, &u4CurrContextId) ==
           VCM_SUCCESS);

#ifdef IGMP_WANTED
    if (u4Module == ISS_IGMP_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        IgmpShowRunningConfig (CliHandle, u4Module);
    }
#endif

#ifdef MLD_WANTED
    if (u4Module == ISS_MLD_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        MldShowRunningConfig (CliHandle, u4Module);
    }
#endif

#ifdef IGMPPRXY_WANTED
    if (u4Module == ISS_IGP_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        IgmpProxyShowRunningConfig (CliHandle, u4Module);
    }
#endif
#ifdef ISS_WANTED
#ifdef NPAPI_WANTED
    if (u4Module == ISS_ACL_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        AclShowRunningConfig (CliHandle, u4Module);
    }
#endif
#endif

#ifdef PIM_WANTED
    if (u4Module == ISS_PIM_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        PimShowRunningConfig (CliHandle, u4Module, IPVX_ADDR_FMLY_IPV4);
    }
#endif
#ifdef PIMV6_WANTED
    if (u4Module == ISS_PIMV6_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        PimShowRunningConfig (CliHandle, u4Module, IPVX_ADDR_FMLY_IPV6);
    }
#endif

#ifdef OSPF3_WANTED
    if (u4Module == ISS_OSPF3_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        if (gIssGlobalInfo.au1IssModuleSystemControl[OSPF3_MODULE_ID] ==
            MODULE_SHUTDOWN)
        {
            CliIssShutModuleShowRunningConfig (CliHandle, (UINT1 *) "OSPF3");
        }
        else
        {
            if (pu1TempContextName != NULL)
            {
                if (VcmIsSwitchExist (pu1ContextName, &u4ContextId) == VCM_TRUE)
                {
                    Ospfv3ShowRunningConfigInCxt (CliHandle, u4Module,
                                                  u4ContextId);
                }
            }
            else
            {
                if (VcmGetFirstActiveContext (&u4ContextId) != VCM_SUCCESS)
                {
                    CliSetSkipDoubleBang (CLI_HANDLE_AND_RESET_BANG);
                    return CLI_FAILURE;
                }
                do
                {
                    Ospfv3ShowRunningConfigInCxt (CliHandle, u4Module,
                                                  u4ContextId);
                    u4TempContextId = u4ContextId;

                }
                while (VcmGetNextActiveContext (u4TempContextId, &u4ContextId)
                       == VCM_SUCCESS);
            }
        }
    }
#endif

#ifdef CFA_WANTED
    if ((u4L2Mode == VCM_MI_MODE) || (u4L2Mode == VCM_SI_MODE)
        || (u4L3Mode == VCM_MI_MODE))
    {
        if (((INT4) u4Module == ISS_INTERFACE_SHOW_RUNNING_CONFIG) ||
            ((INT4) u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
        {
            InterfaceShowRunningConfigMappingDetails (CliHandle, (INT4) u4Args);
        }
    }
    if ((u4Module == ISS_INTERFACE_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_MPLS_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
    {
        InterfaceShowRunningConfig (CliHandle, (INT4) u4Args, u4Module);
    }
    if ((u4Module == ISS_L2DHCSNP_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
    {
        L2dsShowRunningConfig (CliHandle);
    }
    if (u4Module == ISS_UFD_SHOW_RUNNING_CONFIG
        || u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        CfaUfdShowRunningConfig (CliHandle, u4Module);
    }

    if (u4Module == ISS_SH_SHOW_RUNNING_CONFIG
        || u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        CfaShShowRunningConfig (CliHandle, u4Module);
    }
#endif
#ifdef ISIS_WANTED
    if (u4Module == ISS_ISIS_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {

        if (gIssGlobalInfo.au1IssModuleSystemControl[ISIS_MODULE_ID] ==
            MODULE_SHUTDOWN)
        {
            CliIssShutModuleShowRunningConfig (CliHandle, (UINT1 *) "ISIS");
        }
        else
        {
            if (pu1TempContextName != NULL)
            {
                if (VcmIsVrfExist (pu1ContextName, &u4ContextId) == VCM_TRUE)
                {
                    IsIsShowRunningConfigInCxt (CliHandle, u4Module,
                                                (INT4) (u4ContextId));
                }
            }
            else
            {
                if (VcmGetFirstActiveL3Context (&u4ContextId) != VCM_SUCCESS)
                {
                    CliSetSkipDoubleBang (CLI_HANDLE_AND_RESET_BANG);
                    return CLI_FAILURE;
                }
                do
                {
                    IsIsShowRunningConfigInCxt (CliHandle, u4Module,
                                                (INT4) (u4ContextId));
                    u4TempContextId = u4ContextId;

                }
                while (VcmGetNextActiveL3Context (u4TempContextId, &u4ContextId)
                       == VCM_SUCCESS);
            }
        }

    }

#endif

#ifdef DHCP6_SRV_WANTED
    if (u4Module == ISS_DHCP6_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        D6SrCliShowRunningConfig (CliHandle, u4Module);
    }
#endif

#ifdef DHCP6_CLNT_WANTED
    if (u4Module == ISS_DHCP6_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        D6ClCliShowRunningConfig (CliHandle, u4Module);
    }
#endif

#ifdef HOTSPOT2_WANTED
    if (u4Module == ISS_HS_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        HsShowRunningConfig (CliHandle);
    }
#endif

#ifdef DHCP6_RLY_WANTED
    if (u4Module == ISS_DHCP6_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        D6RlCliShowRunningConfig (CliHandle, u4Module);
    }
#endif

#ifdef WLC_WANTED
    if (u4Module == ISS_WSS_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        WssShowRunningConfig (CliHandle);
    }
#ifdef WSSUSER_WANTED
    if (u4Module == ISS_WSSUSER_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG ||
        u4Module == ISS_WSS_SHOW_RUNNING_CONFIG)
    {
        WssUserShowRunningConfig (CliHandle);
    }
#endif
#endif

    if ((u4Module == ISS_INTERFACE_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
    {
        VcmSispShowRunningConfig (CliHandle, u4Args);
    }

    /*u4Args indicates the vlanid.
     * MI support is not provided for RMON.
     * Hence, RMON vlan configurations are displayed before displaying context specific configurations*/

#ifdef RMON_WANTED
    if ((u4Args == 0) && (u4Module == ISS_VLAN_SHOW_RUNNING_CONFIG))
    {
        for (i4Val = 1; i4Val <= VLAN_DEV_MAX_NUM_VLAN; i4Val++)
        {
            RmonShowRunningConfigVlan (CliHandle, u1Flag, (UINT4) i4Val);
        }

    }
    else if ((u4Args != 0) && (u4Module == ISS_VLAN_SHOW_RUNNING_CONFIG))
    {
        RmonShowRunningConfigVlan (CliHandle, u1Flag, u4Args);
    }
#endif

    /* Displays for Module specific configrn along with the context,
     * also to  display the configuration of all contexts in the system,
     * and to display a particular  context specific configration.*/

    if (u4Module == ISS_EVB_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
#ifdef EVB_WANTED
        VlanEvbShowRunningConfig (CliHandle);
#else
        CliPrintf (CliHandle, "\r\n");
#endif
    }

    VcmGetFirstActiveL3Context (&u4ContextId);

    do
    {

        if (u4Module == ISS_INTERFACE_SHOW_RUNNING_CONFIG)
        {
            IssStpVlanShowRunningConfig (CliHandle, u4ContextId,
                                         u4Module, u4Args);
            break;
        }
        else
        {

            if (pu1TempContextName == NULL)
            {
                VcmGetAliasName (u4ContextId, au1TempContextName);
                pu1ContextName = au1TempContextName;
            }

#ifdef MPLS_WANTED
            /* show vfi */
            if (u4Module == ISS_MPLS_SHOW_RUNNING_CONFIG ||
                u4Module == ISS_CONTEXT_SHOW_RUNNING_CONFIG ||
                u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
            {
                MplsVfiShowRunningConfig (CliHandle);
                MplsXCShowRunningConfigInterface (CliHandle);
            }
#endif
            IssStpVlanShowRunningConfig (CliHandle, u4ContextId, u4Module,
                                         u4Args);
#if defined IGS_WANTED || defined MLDS_WANTED
            if (u4Module == ISS_IGS_SHOW_RUNNING_CONFIG ||
                u4Module == ISS_MLDS_SHOW_RUNNING_CONFIG ||
                u4Module == ISS_CONTEXT_SHOW_RUNNING_CONFIG ||
                u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
            {
                SnoopBasicShowRunningConfig (CliHandle, u4ContextId);
                SnoopShowRunningConfig (CliHandle, u4ContextId, u4Module);
            }
#endif
            u4TempContextId = u4ContextId;

            if (pu1TempContextName != NULL)
            {
                break;
            }
        }

    }
    while (VcmGetNextActiveL2Context (u4TempContextId, &u4ContextId) ==
           VCM_SUCCESS);

#if MEF_WANTED

    if (u4Module == ISS_MEF_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        MefShowRunningConfig (CliHandle);
    }
#endif

#ifdef MPLS_WANTED
    /* show running config for Mi case */
    if (u4Module == ISS_MPLS_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        MplsShowRunningConfig (CliHandle);
    }
#endif

#ifdef QOSX_WANTED
    if (u4Module == ISS_QOSXTD_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        QoSShowRunningConfig (CliHandle);
    }
#endif
#ifdef ELMI_WANTED

    if (u4Module == ISS_ELMI_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        ElmShowRunningConfig (CliHandle, u4Module);
    }

#endif

#ifdef EOAM_WANTED

    if (u4Module == ISS_EOAM_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        EoamCliShowRunningConfig (CliHandle, u4Module);
    }

#ifdef EOAM_FM_WANTED
    if (u4Module == ISS_FM_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        FmCliShowRunningConfig (CliHandle, u4Module);
    }
#endif /* EOAM_FM_WANTED */
#endif /* EOAM_WANTED */

#ifdef LA_WANTED

    if (u4Module == ISS_LA_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        LaShowRunningConfig (CliHandle, u4Module);
    }
#endif

#ifdef FSB_WANTED
    if (u4Module == ISS_FSB_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        FsbCliShowRunningConfig (CliHandle);
    }
#endif

#ifdef PNAC_WANTED
    if (u4Module == ISS_DOT1X_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        PnacShowRunningConfig (CliHandle, u4Module);
    }
#endif

#ifdef ISS_WANTED
#ifdef ROUTEMAP_WANTED
    if (u4Module == ISS_RMAP_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        RMapShowRunningConfig (CliHandle);
    }
#endif

#ifdef OSPF_WANTED
    if (u4Module == ISS_OSPF_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG ||
        u4Module == ISS_VRF_SHOW_RUNNING_CONFIG)

    {
        if (gIssGlobalInfo.au1IssModuleSystemControl[OSPF_MODULE_ID] ==
            MODULE_SHUTDOWN)
        {
            CliIssShutModuleShowRunningConfig (CliHandle, (UINT1 *) "OSPF");
        }
        else
        {

            if (pu1TempContextName != NULL)
            {
                if (VcmIsVrfExist (pu1ContextName, &u4ContextId) == VCM_TRUE)
                {
                    OspfShowRunningConfigInCxt (CliHandle, u4Module,
                                                u4ContextId);
                }
            }
            else
            {
                if (VcmGetFirstActiveL3Context (&u4ContextId) != VCM_SUCCESS)
                {
                    CliSetSkipDoubleBang (CLI_HANDLE_AND_RESET_BANG);
                    return CLI_FAILURE;
                }
                do
                {
                    OspfShowRunningConfigInCxt (CliHandle, u4Module,
                                                u4ContextId);
                    u4TempContextId = u4ContextId;

                }
                while (VcmGetNextActiveL3Context (u4TempContextId, &u4ContextId)
                       == VCM_SUCCESS);
            }
        }
    }
#endif
#ifdef OSPFTE_WANTED
    if ((u4Module == ISS_SHOW_ALL_RUNNING_CONFIG) ||
        (u4Module == ISS_OSPFTE_SHOW_RUNNING_CONFIG))
    {
        OspfTeShowRunningConfig (CliHandle);
    }
#endif

#ifdef MPLS_WANTED
    if ((u4Module == ISS_SHOW_ALL_RUNNING_CONFIG) ||
        (u4Module == ISS_MPLS_SHOW_RUNNING_CONFIG))
    {
        MplsDsTeShowRunningConfig (CliHandle);
    }
#endif

#ifdef BFD_WANTED

    /*Show running config for BFD protocol */
    if ((u4Module == ISS_BFD_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
    {
        BfdShowRunningConfig (CliHandle, u4ContextId);
    }

#endif

#ifdef TLM_WANTED
    if ((u4Module == ISS_TLM_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG) ||
        (u4Module == ISS_MPLS_SHOW_RUNNING_CONFIG))
    {
        TlmCliShowRunningConfig (CliHandle);
    }
#endif

#ifdef RIP_WANTED
    if ((u4Module == ISS_RIP_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG) ||
        (u4Module == ISS_VRF_SHOW_RUNNING_CONFIG))

    {
        if (pu1TempContextName != NULL)
        {
            if (VcmIsVrfExist (pu1ContextName, &u4ContextId) == VCM_TRUE)
            {
                RipShowRunningConfigCxt (CliHandle, u4Module, u4ContextId);
            }
        }
        else
        {
            if (VcmGetFirstActiveL3Context (&u4ContextId) != VCM_SUCCESS)
            {
                CliSetSkipDoubleBang (CLI_HANDLE_AND_RESET_BANG);
                return CLI_FAILURE;
            }
            do
            {
                RipShowRunningConfigCxt (CliHandle, u4Module, u4ContextId);
                u4TempContextId = u4ContextId;

            }
            while (VcmGetNextActiveL3Context (u4TempContextId, &u4ContextId) ==
                   VCM_SUCCESS);
        }
    }
#endif

#ifdef RRD_WANTED
    if ((u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
        || (u4Module == ISS_VRF_SHOW_RUNNING_CONFIG)
        || (u4Module == ISS_IP_SHOW_RUNNING_CONFIG))
    {
        if (pu1TempContextName != NULL)
        {
            if (VcmIsVrfExist (pu1ContextName, &u4ContextId) == VCM_TRUE)
            {
                RrdShowRunningConfig (CliHandle, (INT4) u4ContextId, u4Module);
            }
        }
        else
        {
            if (VcmGetFirstActiveL3Context (&u4ContextId) != VCM_SUCCESS)
            {
                CliSetSkipDoubleBang (CLI_HANDLE_AND_RESET_BANG);
                return CLI_FAILURE;
            }
            do
            {
                RrdShowRunningConfig (CliHandle, (INT4) u4ContextId, u4Module);
                u4TempContextId = u4ContextId;
            }
            while (VcmGetNextActiveL3Context (u4TempContextId, &u4ContextId) ==
                   VCM_SUCCESS);
        }
    }
#endif
#ifdef BGP_WANTED
    if (u4Module == ISS_BGP_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG
        || u4Module == ISS_VRF_SHOW_RUNNING_CONFIG)

    {
        if (gIssGlobalInfo.au1IssModuleSystemControl[BGP_MODULE_ID] ==
            MODULE_SHUTDOWN)
        {
            CliIssShutModuleShowRunningConfig (CliHandle, (UINT1 *) "BGP4");
        }
        else
        {
            BgpShowRunningConfig (CliHandle, pu1TempContextName);
        }
    }
#endif

#ifdef DHCP_RLY_WANTED
    if (u4Module == ISS_DHCP_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        DhcpRelayShowRunningConfig (CliHandle, u4Module);
    }
#endif

#ifdef IP6_WANTED
    if (u4Module == ISS_IPV6_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG
        || u4Module == ISS_VRF_SHOW_RUNNING_CONFIG)
    {

        if (pu1TempContextName != NULL)
        {
            if (VcmIsVrfExist (pu1ContextName, &u4ContextId) == VCM_TRUE)
            {
                Ipv6ShowRunningConfigInCxt (CliHandle, u4Module, u4ContextId);
#ifdef RRD_WANTED
                Rtm6ShowScalarsInCxt (CliHandle);
#endif
                /*setting u1BangStatus to TRUE */
                CliGetBangStatus (CliHandle, &u1BangStatus);
                if (u1BangStatus == TRUE)
                {
                    CliPrintf (CliHandle, "!\r\n");
                }
                CliSetBangStatus (CliHandle, FALSE);

            }
        }
        else
        {
            if (VcmGetFirstActiveContext (&u4ContextId) != VCM_SUCCESS)
            {
                CliSetSkipDoubleBang (CLI_HANDLE_AND_RESET_BANG);
                return CLI_FAILURE;
            }
#ifdef RRD_WANTED
            Rtm6ShowScalarsInCxt (CliHandle);
#endif
            do
            {
                Ipv6ShowRunningConfigInCxt (CliHandle, u4Module, u4ContextId);
                u4TempContextId = u4ContextId;

            }
            while (VcmGetNextActiveContext (u4TempContextId, &u4ContextId) ==
                   VCM_SUCCESS);
            /*setting u1BangStatus to TRUE */
            CliGetBangStatus (CliHandle, &u1BangStatus);
            if (u1BangStatus == TRUE)
            {
                CliPrintf (CliHandle, "!\r\n");
            }
            CliSetBangStatus (CliHandle, FALSE);

        }
    }
#endif
#if defined (IPSECv6_WANTED) && !defined (VPN_WANTED)
    if (u4Module == ISS_SECV6_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
        IPSecv6ShowRunningconfig (CliHandle);
#endif

#ifdef RIP6_WANTED
    if (u4Module == ISS_RIP6_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        Rip6ShowRunningConfig (CliHandle, u4Module);
    }
#endif

#ifdef SSH_WANTED
    if (u4Module == ISS_SSH_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        SshShowRunningConfig (CliHandle);
    }
#endif

#ifdef SSL_WANTED
    if (u4Module == ISS_SSL_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        SslShowRunningConfig (CliHandle);
    }
#endif

#endif
#ifdef SNTP_WANTED
    if (u4Module == ISS_SNTP_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        SntpShowRunningConfig (CliHandle);
    }
#endif
#ifdef MSDP_WANTED
    if (u4Module == ISS_MSDP_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        MsdpShowRunningConfig (CliHandle, u4Module, IPVX_ADDR_FMLY_IPV4);
    }
    if (u4Module == ISS_MSDPV6_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        MsdpShowRunningConfig (CliHandle, u4Module, IPVX_ADDR_FMLY_IPV6);
    }

#endif
#ifdef VRRP_WANTED
    if (u4Module == ISS_VRRP_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        VrrpShowRunningConfig (CliHandle);
    }
#endif

#ifdef SNMP_3_WANTED
    if (u4Module == ISS_SNMP_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        Snmpv3ShowRunningConfig (CliHandle);
    }
#endif

#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
    if (u4Module == ISS_IP_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG
        || u4Module == ISS_VRF_SHOW_RUNNING_CONFIG)
    {

        if (pu1TempContextName != NULL)
        {
            if (VcmIsVrfExist (pu1ContextName, &u4ContextId) == VCM_TRUE)
            {
                IpShowRunningConfigInCxt (CliHandle, u4ContextId);
#if defined (LNXIP4_WANTED) && defined (RRD_WANTED)
                RrdShowRunningConfig (CliHandle, u4ContextId, u4Module);
#else
                FsIpShowRunningConfigInCxt (CliHandle, u4Module,
                                            (INT4) u4ContextId);
#endif
                /*setting u1BangStatus to TRUE */
                CliGetBangStatus (CliHandle, &u1BangStatus);
                if (u1BangStatus == TRUE)
                {
                    CliPrintf (CliHandle, "!\r\n");
                }
                CliSetBangStatus (CliHandle, FALSE);
            }
        }
        else
        {
            if (VcmGetFirstActiveL3Context (&u4ContextId) != VCM_SUCCESS)
            {
                CliSetSkipDoubleBang (CLI_HANDLE_AND_RESET_BANG);
                return CLI_FAILURE;
            }
            do
            {
                IpShowRunningConfigInCxt (CliHandle, u4ContextId);
#if defined (LNXIP4_WANTED) && defined (RRD_WANTED)
                RrdShowRunningConfig (CliHandle, u4ContextId, u4Module);
#else
                FsIpShowRunningConfigInCxt (CliHandle, u4Module,
                                            (INT4) u4ContextId);
#endif
                u4TempContextId = u4ContextId;

            }
            while (VcmGetNextActiveL3Context (u4TempContextId, &u4ContextId) ==
                   VCM_SUCCESS);

            /*setting u1BangStatus to TRUE */
            CliGetBangStatus (CliHandle, &u1BangStatus);
            if (u1BangStatus == TRUE)
            {
                CliPrintf (CliHandle, "!\r\n");
            }
            CliSetBangStatus (CliHandle, FALSE);

        }

#ifdef MRI_WANTED
        MriShowRunningConfig (CliHandle);
#endif
    }
#endif

#ifdef RADIUS_WANTED
    if (u4Module == ISS_RAD_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        RadiusShowRunningConfig (CliHandle);
    }
#endif

#ifdef RMON_WANTED
    if (u4Module == ISS_RMON_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        RmonShowRunningConfig (CliHandle, u4Module);
    }
#endif

#ifdef ISS_WANTED
    if (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        IssSysShowRunningConfig (CliHandle);
    }
#endif

#ifdef RM_WANTED
    if (u4Module == ISS_RM_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        RmShowRunningConfig (CliHandle);
    }
#endif

#ifdef HB_WANTED
    if (u4Module == ISS_HB_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        HbShowRunningConfig (CliHandle);
    }
#endif

#ifdef ICCH_WANTED
    if (u4Module == ISS_ICCH_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)

    {
        IcchShowRunningConfig (CliHandle);
    }
#endif

#ifdef MBSM_WANTED
    if (u4Module == ISS_MBSM_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        MbsmShowRunningConfig (CliHandle);
    }
#endif

#ifdef TACACS_WANTED
    /* show running config for Tacacs case */
    if (u4Module == ISS_TACACS_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        TacacsShowRunningConfig (CliHandle);
    }
#endif

#ifdef TAC_WANTED
    /* show running config for Mi case */
    if (u4Module == ISS_TACM_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        TacShowRunningConfig (CliHandle);
    }
#endif
#ifdef NAT_WANTED
    if (u4Module == ISS_NAT_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        NatShowRunningConfig (CliHandle, u4Module);
    }
#endif
#ifdef ERPS_WANTED
    /* show running config for Mi case */
    if (u4Module == ISS_ERPS_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        ErpsShowRunningConfig (CliHandle);
    }
#endif
#ifdef WEBNM_WANTED
    /* show running config for HTTP case */
    if (u4Module == ISS_HTTP_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        HttpShowRunningConfig (CliHandle);
    }
#endif
#ifdef POE_WANTED
    if (u4Module == ISS_POE_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        PoeShowRunningConfig (CliHandle, u4Module);
    }
#endif
#ifdef PTP_WANTED
    if ((u4Module == ISS_PTP_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
    {
        PtpShowRunningConfig (CliHandle, u4Module);
    }
#endif
#ifdef CLKIWF_WANTED
    if ((u4Module == ISS_CLKIWF_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
    {
        ClkIwShowRunningConfig (CliHandle, u4Module);
    }
#endif
#ifdef DNS_WANTED
    if ((u4Module == ISS_DNS_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
    {
        DnsShowRunningConfig (CliHandle, u4Module);
    }
#endif

#ifdef LLDP_WANTED
    if (u4Module == ISS_LLDP_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        LldpShowRunningConfig (CliHandle, u4Module);
    }
#endif

#ifdef FIREWALL_WANTED
    if ((u4Module == ISS_FIREWALL_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
    {
        FwlShowRunningConfig (CliHandle);
    }
#endif
    if ((u4Module == ISS_IPSOURCEGUARD_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
    {
        IpdbShowRunningConfig (CliHandle, 0, ISS_IPDB_SHOW_RUNNING_CONFIG);
    }

    if ((u4Module == ISS_SYS_INFO_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
    {
        SysInfoShowRunningConfig (CliHandle);
        FpamUtlShowRunningConfig (CliHandle);
    }
#ifdef VPN_WANTED
    if ((u4Module == ISS_VPN_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
    {
        VpnShowRunningConfig (CliHandle);
    }
#endif
#ifdef WLC_WANTED
#ifdef RSNA_WANTED
    if (u4Module == ISS_RSNA_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG ||
        u4Module == ISS_WSS_SHOW_RUNNING_CONFIG)
    {
        RsnaShowRunningConfig (CliHandle, u4Args);
    }
#endif
#ifdef WPS_WANTED
    if (u4Module == ISS_WPS_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG ||
        u4Module == ISS_WSS_SHOW_RUNNING_CONFIG)
    {
        WpsShowRunningConfig (CliHandle, u4Args);
    }
#endif
#ifdef RFMGMT_WANTED
    if (u4Module == ISS_RFMGMT_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG ||
        u4Module == ISS_WSS_SHOW_RUNNING_CONFIG)
    {
        RfmgmtShowRunningConfig (CliHandle);
    }
#endif
#endif
#ifdef VXLAN_WANTED
    if (u4Module == ISS_VXLAN_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        VxlanShowRunningConfig (CliHandle, u4Module);
#ifdef EVPN_VXLAN_WANTED
        EvpnShowRunningConfig (CliHandle, u4Module);
#endif /* EVPN_VXLAN_WANTED */
    }
#endif

#ifdef Y1564_WANTED
    if (u4Module == ISS_Y1564_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        Y1564ShowRunningConfig (CliHandle);
    }
#endif /* Y1564_WANTED */

#ifdef RFC2544_WANTED
    if (u4Module == ISS_RFC2544_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        R2544ShowRunningConfig (CliHandle);
    }
#endif
#ifdef  WLC_WANTED
    if (u4Module == ISS_WSSLR_SHOW_RUNNING_CONFIG)
    {
        WssBatchConfigShowRunningConfig (CliHandle);
    }

#endif

#ifdef NPAPI_WANTED
/*NPAPI_WANTED since setting Switch-mode Applicable only for Targets*/

    if (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        SwitchModeTypeShowRunningConfig (CliHandle);
    }
#endif

    IssShowRunningConfigInd (CliHandle, u4Module);
    if (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        CliExecTimeoutShowRunningConfig (CliHandle);
    }

    CliPrintf (CliHandle, "\r\nend\r\n");

    CliSetSkipDoubleBang (CLI_HANDLE_AND_RESET_BANG);

    CliRegisterLock (CliHandle, IssLock, IssUnLock);

    ISS_LOCK ();

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : SysInfoShowRunningConfig                             */
/*                                                                           */
/* Description        : Displays configurations done for system information. */
/*                                                                           */
/* Input(s)           : CliHandle     -  CLI context                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS or CLI_FAILURE                           */
/*****************************************************************************/

INT4
SysInfoShowRunningConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE SwitchName;
    UINT1               au1SwitchName[ISS_STR_LEN + 1];
    INT4                i4LoggingOpt = 0;
    INT4                i4PeerLoggingOpt = 0;
    INT4                i4LoginAuth = 0;
    INT4                i4SshTelnetClientStatus = 0;
    INT4                i4DefExecTimeOut = 0;
    UINT1              *pu1FlashLogFilePath;
    UINT1               au1LogFilePath[ISS_CONFIG_FILE_NAME_LEN + 1];

#ifdef SNMP_3_WANTED
    tSNMP_OCTET_STRING_TYPE SysTemp;
    UINT1               au1SysTemp[SNMP_MAX_OCTETSTRING_SIZE + 1];
    MEMSET (au1SysTemp, 0, SNMP_MAX_OCTETSTRING_SIZE + 1);
    SysTemp.pu1_OctetList = au1SysTemp;
#endif

    MEMSET (au1LogFilePath, 0, ISS_CONFIG_FILE_NAME_LEN + 1);
    pu1FlashLogFilePath = (UINT1 *) &au1LogFilePath[0];

    MEMSET (au1SwitchName, 0, ISS_STR_LEN + 1);
    SwitchName.pu1_OctetList = au1SwitchName;

    nmhGetIssSwitchName (&SwitchName);
    if (MEMCMP (SwitchName.pu1_OctetList, "ISS", STRLEN ("ISS")) != 0)
    {
        CliPrintf (CliHandle, "set switch-name \"%s\"\r\n",
                   SwitchName.pu1_OctetList);
    }
#ifdef SNMP_3_WANTED
    nmhGetSysContact (&SysTemp);
    /* default value is #Aricent Ltd, India */
    if ((SysTemp.pu1_OctetList != NULL) &&
        (STRCMP (SysTemp.pu1_OctetList, ISS_SYS_DEF_CONTACT) != 0))
    {
        CliPrintf (CliHandle, "system contact \"%s\"\r\n",
                   SysTemp.pu1_OctetList);
    }

    MEMSET (au1SysTemp, 0, SNMP_MAX_OCTETSTRING_SIZE + 1);
    nmhGetSysName (&SysTemp);
    /* default value is #Aricent Ltd, India */
    if ((SysTemp.pu1_OctetList != NULL) &&
        (STRCMP (SysTemp.pu1_OctetList, ISS_SYS_DEF_NAME) != 0))
    {
        CliPrintf (CliHandle, "system name \"%s\"\r\n", SysTemp.pu1_OctetList);
    }

    MEMSET (au1SysTemp, 0, SNMP_MAX_OCTETSTRING_SIZE + 1);
    nmhGetSysLocation (&SysTemp);
    /* default value is #Aricent Ltd, India */
    if ((SysTemp.pu1_OctetList != NULL) &&
        (STRCMP (SysTemp.pu1_OctetList, ISS_SYS_DEF_LOCATION) != 0))
    {
        CliPrintf (CliHandle, "system location \"%s\"\r\n",
                   SysTemp.pu1_OctetList);
    }
#endif
    nmhGetIssLoggingOption (&i4LoggingOpt);
    nmhGetIssPeerLoggingOption (&i4PeerLoggingOpt);

    if ((i4PeerLoggingOpt) == INCORE_LOG)
    {
        CliPrintf (CliHandle, "debug-logging file standby\r\n");
    }
    else if (i4PeerLoggingOpt == FLASH_LOG)
    {
        CliPrintf (CliHandle, "debug-logging flash standby\r\n");
    }
    if ((i4LoggingOpt) == INCORE_LOG)
    {
        CliPrintf (CliHandle, "debug-logging file\r\n");
    }
    else if (i4LoggingOpt == FLASH_LOG)
    {
        CliPrintf (CliHandle, "debug-logging flash\r\n");
    }
    else if (i4LoggingOpt == FLASH_LOG_LOCATION)
    {
        IssGetFlashLoggingLocation (pu1FlashLogFilePath);
        CliPrintf (CliHandle, "debug-logging flash:%s\r\n",
                   pu1FlashLogFilePath);
    }

    if (STRLEN (sNvRamData.au1ImageDumpFilePath) != 0)
    {
        CliPrintf (CliHandle, "dump core-file flash:%s\r\n",
                   sNvRamData.au1ImageDumpFilePath);
    }

    nmhGetIssLoginAuthentication (&i4LoginAuth);
    if (i4LoginAuth == REMOTE_LOGIN_RADIUS)
    {
        CliPrintf (CliHandle, "login authentication radius\r\n");
    }

    if (i4LoginAuth == REMOTE_LOGIN_TACACS)
    {
        CliPrintf (CliHandle, "login authentication tacacs\r\n");
    }

    if (i4LoginAuth == REMOTE_LOGIN_RADIUS_FALLBACK_TO_LOCAL)
    {
        CliPrintf (CliHandle, "login authentication radius local\r\n");
    }

    if (i4LoginAuth == REMOTE_LOGIN_TACACS_FALLBACK_TO_LOCAL)
    {
        CliPrintf (CliHandle, "login authentication tacacs local\r\n");
    }

    nmhGetIssTelnetClientStatus (&i4SshTelnetClientStatus);
    if (i4SshTelnetClientStatus != ISS_TELNET_CLIENT_ENABLE)
    {
        CliPrintf (CliHandle, "set telnet-client disable\r\n");
    }
    nmhGetIssSshClientStatus (&i4SshTelnetClientStatus);
    if (i4SshTelnetClientStatus != ISS_SSH_CLIENT_ENABLE)
    {
        CliPrintf (CliHandle, "set ssh-client disable\r\n");
    }
    nmhGetIssDefaultExecTimeOut (&i4DefExecTimeOut);
    if (i4DefExecTimeOut != CLI_SESSION_TIMEOUT)
    {
        CliPrintf (CliHandle, "default exec-timeout %d\r\n", i4DefExecTimeOut);
    }
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : IssStpVlanShowRunningConfig              */
/*  Description     : This function is used to display         */
/*                    the currently operating configuration of */
/*                    all the modules in the system            */
/*  Input(s)        :                                          */
/*                    CliHandle  - CLI Handle                  */
/*                    u4Module   - Module for which the        */
/*                                 configuration to be         */
/*                                 displayed                   */
/*                    u4Args     - Variable argument. It could */
/*                                 be interface index,         */
/*                                 VLAN ID..                   */
/*                    u4ContextType - ContextSpecific Configrn */
/*                    u4ContextId   -Context Identifier      */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS or CLI_FAILURE               */
/***************************************************************/

INT4
IssStpVlanShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId,
                             UINT4 u4Module, UINT4 u4Args)
{
#if defined IGS_WANTED || defined MLDS_WANTED
    INT4                i4AddrType = 0;
#endif

#ifdef PBB_WANTED
    if ((u4Module == ISS_PBB_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_CONTEXT_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
    {
        PbbShowRunningConfig (CliHandle, u4ContextId, u4Module);
    }
#endif

#ifdef VLAN_WANTED
    if ((u4Module == ISS_VLAN_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_CONTEXT_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
    {
        VlanShowRunningConfig (CliHandle, u4ContextId, u4Args, u4Module);
#ifdef GARP_WANTED
        if (u4Args == 0)
        {
            GarpShowRunningConfig (CliHandle, u4ContextId);
        }
#endif
#if defined IGS_WANTED || defined MLDS_WANTED
        if (u4Module != ISS_SHOW_ALL_RUNNING_CONFIG)
        {
            for (i4AddrType = SNOOP_ADDR_TYPE_IPV4;
                 i4AddrType <= SNOOP_ADDR_TYPE_IPV6; i4AddrType++)
            {
                if ((i4AddrType == SNOOP_ADDR_TYPE_IPV4)
                    || (i4AddrType == SNOOP_ADDR_TYPE_IPV6))
                {
                    SnoopShowRunningConfigVlanTable (CliHandle, i4AddrType,
                                                     u4ContextId,
                                                     (UINT2) u4Args);
                    SnoopShowRunningConfigVlanStaticMacstTable (CliHandle,
                                                                (INT4)
                                                                u4ContextId,
                                                                i4AddrType);

                }
            }
        }
#endif
#ifdef MRP_WANTED
        MrpSrcShowRunningConfig (CliHandle, u4ContextId);
#endif
    }
#endif
#ifdef RSTP_WANTED
    if (u4Module == ISS_STP_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_CONTEXT_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        RstpShowRunningConfig (CliHandle, u4ContextId, u4Module);
    }
#endif

#ifdef MSTP_WANTED

    if (u4Module == ISS_STP_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_CONTEXT_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        MstpShowRunningConfig (CliHandle, u4ContextId, u4Module);
    }

#endif
#ifdef PVRST_WANTED

    if (u4Module == ISS_STP_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_CONTEXT_SHOW_RUNNING_CONFIG ||
        u4Module == ISS_SHOW_ALL_RUNNING_CONFIG)
    {
        PvrstShowRunningConfig (CliHandle, u4ContextId, u4Module);
    }

#endif

#ifdef VLAN_WANTED
#ifdef MPLS_WANTED
    if (u4Module == ISS_MPLS_SHOW_RUNNING_CONFIG)
    {
        VlanMplsShowRunningConfig (CliHandle, u4ContextId, u4Args, u4Module);
    }
#endif
#endif

#ifdef CN_WANTED
    if ((u4Module == ISS_CN_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_CONTEXT_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
    {
        CnCliShowRunningConfig (CliHandle, u4ContextId, u4Module);
    }
#endif /* CN_WANTED */
#ifdef RBRG_WANTED
    if ((u4Module == ISS_RBRG_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_CONTEXT_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
    {
        RbrgShowRunningConfig (CliHandle, u4Module);
    }
#endif /* RBRG_WANTED */
#ifdef ECFM_WANTED
    if ((u4Module == ISS_ECFM_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_CONTEXT_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
    {
        EcfmShowRunningConfig (CliHandle, u4ContextId, u4Module);
    }
#endif

#ifdef ELPS_WANTED
    /* show running config for Mi case */
    if ((u4Module == ISS_ELPS_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_CONTEXT_SHOW_RUNNING_CONFIG) ||
        (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG))
    {
        ElpsCliShowRunningConfig (CliHandle, u4ContextId);
    }
#endif

#if ! ((defined (RSTP_WANTED)) || (defined (MSTP_WANTED)) || (defined (PVRST_WANTED)) || (defined (VLAN_WANTED)) || (defined (MPLS_WANTED)) || (defined (GARP_WANTED)) || (defined (ECFM_WANTED)))
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Module);
#endif

#ifndef VLAN_WANTED
    UNUSED_PARAM (u4Args);
#endif
    return CLI_SUCCESS;

}

/***************************************************************/
/*  Function Name   : IssSysShowRunningConfig                  */
/*  Description     : This function is used to display         */
/*                    the current system configuration         */
/*  Input(s)        :                                          */
/*                    CliHandle  - CLI Handle                  */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
IssSysShowRunningConfig (tCliHandle CliHandle)
{
    CHR1               *pu1Netaddress;
    UINT1               au1PortList[ISS_AUTH_PORT_LIST_SIZE];
    UINT1              *pu1VlanList = NULL;
    UINT1               au1AuditFileName[ISS_AUDIT_FILE_NAME_LENGTH];
    CHR1                au1DefaultConf[ISS_STATIC_MAX_NAME_LEN];
    INT4                i4IpAuthMgrOOBPort;
    INT4                i4AuditLogStatus;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    INT4                i4NextSessId = 0;
    INT4                i4MirrSessionId = 0;
    INT4                i4SessionStatus = 0;
    INT4                i4MirrType = 0;
    INT4                i4SourceId = 0;
    INT4                i4NextMirrorSrcId = 0;
    INT4                i4SourceMode = 0;
    INT4                i4DestId = 0;
    INT4                i4NextMirrorDestId = 0;
    INT4                i4RSpanStatus = 0;
    INT4                i4RspanVlanId = 0;
    INT4                i4HttpStatus = 0;
    INT4                i4HttpPort = 0;
    INT4                i4ContextId = 0;
    INT4                i4NextContextId = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4Services;
    UINT4               u4Flag = 0;
    UINT4               u4IpAddr;
    UINT4               u4IpMask;
    UINT4               u4NextIpAddr = 0;
    UINT4               u4NextIpMask;
    UINT4               u4Val = 0;
    UINT4               u4CheckService;
    UINT4               u4AuditFileSize;
    UINT4               u4AuditLogSizeThreshold;
    INT4                i4RetStatus = CLI_SUCCESS;
    INT4                i4MaxTemperature;
    INT4                i4MinTemperature;
    INT4                i4MaxRAMUsage;
    INT4                i4MaxCPUUsage;
    INT4                i4MaxFlashUsage;
    INT4                i4PowerSurge;
    INT4                i4PowerFailure;
    INT4                i4IssAclTrafficSeperationCtrl = 0;
    INT4                i4ProvisionMode = 0;
    INT4                i4TelnetStatus = 0;
    tSNMP_OCTET_STRING_TYPE PortList;
    tSNMP_OCTET_STRING_TYPE VlanList;
    tSNMP_OCTET_STRING_TYPE AuditFileName;
    INT4                i4RoutingFlag;

    CliRegisterLock (CliHandle, IssLock, IssUnLock);

    ISS_LOCK ();

    pu1VlanList = UtlShMemAllocVlanList ();

    if (pu1VlanList == NULL)
    {
        CliUnRegisterLock (CliHandle);
        ISS_UNLOCK ();
        return CLI_FAILURE;
    }

    MEMSET (au1DefaultConf, 0, ISS_STATIC_MAX_NAME_LEN);

    SNPRINTF (au1DefaultConf, ISS_STATIC_MAX_NAME_LEN, "gigabitethernet 0/1-%d",
              ISS_MAX_PORTS);

    nmhGetIssAclTrafficSeperationCtrl (&i4IssAclTrafficSeperationCtrl);
    if (i4IssAclTrafficSeperationCtrl != ACL_TRAFFICSEPRTN_CTRL_NONE)
    {
        CliPrintf (CliHandle, "traffic-separation control %d\r\n",
                   i4IssAclTrafficSeperationCtrl);
        u4Flag = 1;
    }

    nmhGetIssAclProvisionMode (&i4ProvisionMode);
    if (i4ProvisionMode == ISS_CONSOLIDATED)
    {
        CliPrintf (CliHandle, "access-list provision mode consolidated\r\n");
        u4Flag = 1;
    }
    nmhGetIssSwitchMinThresholdTemperature (&i4MinTemperature);
    if (i4MinTemperature != ISS_DEFAULT_MIN_THRESHOLD)
    {
        CliPrintf (CliHandle, "\r%-36s %d\r\n",
                   "set switch temperature min threshold", i4MinTemperature);
    }
    nmhGetIssSwitchMaxThresholdTemperature (&i4MaxTemperature);
    if (i4MaxTemperature != ISS_DEFAULT_MAX_THRESHOLD)
    {
        CliPrintf (CliHandle, "\r%-36s %d\r\n",
                   "set switch temperature max threshold", i4MaxTemperature);
    }
    nmhGetIssSwitchMaxRAMUsage (&i4MaxRAMUsage);
    if (i4MaxRAMUsage != ISS_SWITCH_MAX_RAM_USAGE)
    {
        CliPrintf (CliHandle, "\r%-36s %d\r\n",
                   "set switch maximum RAM threshold", i4MaxRAMUsage);
    }
    nmhGetIssSwitchMaxCPUThreshold (&i4MaxCPUUsage);
    if (i4MaxCPUUsage != ISS_SWITCH_MAX_CPU_THRESHOLD)
    {
        CliPrintf (CliHandle, "\r%-36s %d\r\n",
                   "set switch maximum CPU threshold", i4MaxCPUUsage);
    }
    nmhGetIssSwitchMaxFlashUsage (&i4MaxFlashUsage);
    if (i4MaxFlashUsage != ISS_SWITCH_MAX_FLASH_USAGE)
    {
        CliPrintf (CliHandle, "\r%-36s %d\r\n",
                   "set switch maximum Flash threshold", i4MaxFlashUsage);
    }
    nmhGetIssSwitchPowerFailure (&i4PowerFailure);
    if (i4PowerFailure != ISS_SWITCH_MIN_POWER_SUPPLY)
    {
        CliPrintf (CliHandle, "\r%-36s %d\r\n",
                   "set switch power min threshold", i4PowerFailure);
    }
    nmhGetIssSwitchPowerSurge (&i4PowerSurge);
    if (i4PowerSurge != ISS_SWITCH_MAX_POWER_SUPPLY)
    {
        CliPrintf (CliHandle, "\r%-36s %d\r\n",
                   "set switch power max threshold", i4PowerSurge);
    }

    if (nmhGetFirstIndexIssIpAuthMgrTable (&u4NextIpAddr,
                                           &u4NextIpMask) != SNMP_FAILURE)
    {
        do
        {
            u4IpAddr = u4NextIpAddr;
            u4IpMask = u4NextIpMask;

            CliPrintf (CliHandle, "authorized-manager");

            PortList.i4_Length = ISS_AUTH_PORT_LIST_SIZE;
            PortList.pu1_OctetList = au1PortList;

            VlanList.i4_Length = ISS_VLAN_LIST_SIZE;
            VlanList.pu1_OctetList = pu1VlanList;

            CLI_MEMSET (PortList.pu1_OctetList, 0, ISS_AUTH_PORT_LIST_SIZE);
            CLI_MEMSET (VlanList.pu1_OctetList, 0, ISS_VLAN_LIST_SIZE);

            CLI_CONVERT_IPADDR_TO_STR (pu1Netaddress, u4IpAddr);
            CliPrintf (CliHandle, " ip-source %s", pu1Netaddress);

            if (u4IpMask != ISS_IP_DEF_NET_MASK)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1Netaddress, u4IpMask);
                CliPrintf (CliHandle, " %s", pu1Netaddress);
            }

            nmhGetIssIpAuthMgrPortList (u4IpAddr, u4IpMask, &PortList);
            if ((PortList.i4_Length) != 0)
            {
                CliConfOctetToIfName (CliHandle, " interface ", au1DefaultConf,
                                      &PortList, &u4PagingStatus);
            }

            nmhGetIssIpAuthMgrVlanList (u4IpAddr, u4IpMask, &VlanList);
            if ((VlanList.i4_Length) != 0)
            {
                IssConfIpAuthOctetToVlanList (CliHandle, VlanList.pu1_OctetList,
                                              (UINT4) VlanList.i4_Length);

            }
            nmhGetIssIpAuthMgrOOBPort (u4IpAddr, u4IpMask, &i4IpAuthMgrOOBPort);
            if (i4IpAuthMgrOOBPort == ISS_TRUE)
            {
                CliPrintf (CliHandle, " cpu0");
            }
            nmhGetIssIpAuthMgrAllowedServices (u4IpAddr, u4IpMask,
                                               (INT4 *) &u4Services);

            if (u4Services != ISS_ALL_MASK)
            {
                CliPrintf (CliHandle, " service");

                for (u4Val = 0; u4Val < ISS_MAX_AUTH_MGR_PROTOCOLS; u4Val++)
                {
                    u4CheckService = u4Services & (UINT4) (0x01 << u4Val);

                    switch (u4CheckService)
                    {
                        case ISS_SNMP_MASK:
                            CliPrintf (CliHandle, " snmp");
                            break;

                        case ISS_TELNET_MASK:
                            CliPrintf (CliHandle, " telnet");
                            break;

                        case ISS_HTTP_MASK:
                            CliPrintf (CliHandle, " http");
                            break;

                        case ISS_HTTPS_MASK:
                            CliPrintf (CliHandle, " https");
                            break;

                        case ISS_SSH_MASK:
                            CliPrintf (CliHandle, " ssh");
                            break;

                        default:
                            break;
                    }
                }
            }
            u4Flag = 1;
            CliPrintf (CliHandle, "\r\n");

        }
        while (nmhGetNextIndexIssIpAuthMgrTable
               (u4IpAddr, &u4NextIpAddr, u4IpMask,
                &u4NextIpMask) == SNMP_SUCCESS);
    }
    /** SHOW RUNNING CONFIG FOR AUDIT **/
    MEMSET (au1AuditFileName, 0, ISS_AUDIT_FILE_NAME_LENGTH);
    AuditFileName.pu1_OctetList = &au1AuditFileName[0];

    nmhGetIssAuditLogStatus (&i4AuditLogStatus);
    nmhGetIssAuditLogFileName (&AuditFileName);
    nmhGetIssAuditLogFileSize (&u4AuditFileSize);
    nmhGetIssAuditLogSizeThreshold (&u4AuditLogSizeThreshold);

    if (i4AuditLogStatus == ISS_TRUE)
    {
        CliPrintf (CliHandle, "audit-logging enable\r\n");
        u4Flag = 1;

    }
    if (ISS_STRCMP (AuditFileName.pu1_OctetList, AUDIT_DEFAULT_FILE) != 0)
    {
        CliPrintf (CliHandle, "audit-logging filename %s\r\n",
                   AuditFileName.pu1_OctetList);
        u4Flag = 1;
    }
    /* default Audit Log File Size is 1048576 */
    if (u4AuditFileSize != AUDIT_MAX_FILE_SIZE)
    {
        CliPrintf (CliHandle, "audit-logging filesize %d\r\n", u4AuditFileSize);
        u4Flag = 1;
    }
    if (u4AuditLogSizeThreshold != AUDIT_DEFAULT_THRESHOLD_VALUE)
    {
        CliPrintf (CliHandle, "audit-logging logsize-threshold %u\r\n",
                   u4AuditLogSizeThreshold);
        u4Flag = 1;
    }

    /* Running config for CPU mirroring */
    CliShowIssCpuMirroring (CliHandle, ISS_MIRR_SHOW_RUNNING);
    CliIssShowMirrorStatus (CliHandle, i4SessionStatus, ISS_MIRR_SHOW_RUNNING);
    i4RetStatus = nmhGetIssMirrorStatus (&i4SessionStatus);
    UNUSED_PARAM (i4RetStatus);
    if ((i4SessionStatus != ISS_MIRRORDISABLE)
        && (nmhGetFirstIndexIssMirrorCtrlExtnTable (&i4NextSessId) !=
            SNMP_FAILURE))
    {
        do
        {
            i4MirrSessionId = i4NextSessId;
            nmhGetIssMirrorCtrlExtnMirrType (i4MirrSessionId, &i4MirrType);

            /* Source info */
            i4SourceId = 0;
            while (nmhGetNextIndexIssMirrorCtrlExtnSrcTable (i4MirrSessionId,
                                                             &i4NextSessId,
                                                             i4SourceId,
                                                             &i4NextMirrorSrcId)
                   != SNMP_FAILURE)
            {
                if (i4MirrSessionId != i4NextSessId)
                {
                    break;
                }
                i4SourceId = i4NextMirrorSrcId;

                nmhGetIssMirrorCtrlExtnSrcMode (i4MirrSessionId,
                                                i4SourceId, &i4SourceMode);

                if (i4MirrType == ISS_MIRR_PORT_BASED)
                {
                    CfaCliConfGetIfName ((UINT4) i4SourceId,
                                         (INT1 *) au1IfName);
                    switch (i4SourceMode)
                    {
                        case ISS_MIRR_INGRESS:
                            CliPrintf (CliHandle,
                                       "monitor session %d source interface "
                                       "%s rx\r\n", i4MirrSessionId, au1IfName);
                            break;
                        case ISS_MIRR_EGRESS:
                            CliPrintf (CliHandle,
                                       "monitor session %d source interface "
                                       "%s tx\r\n", i4MirrSessionId, au1IfName);
                            break;
                        case ISS_MIRR_BOTH:
                        default:
                            CliPrintf (CliHandle,
                                       "monitor session %d source interface "
                                       "%s both\r\n", i4MirrSessionId,
                                       au1IfName);
                            break;
                    }

                }
                else if (i4MirrType == ISS_MIRR_MAC_FLOW_BASED)
                {
                    CliPrintf (CliHandle,
                               "monitor session %d source mac-acl %d\r\n",
                               i4MirrSessionId, i4SourceId);
                }
                else if (i4MirrType == ISS_MIRR_IP_FLOW_BASED)
                {
                    CliPrintf (CliHandle,
                               "monitor session %d source ip-acl %d\r\n",
                               i4MirrSessionId, i4SourceId);
                }
            }

            if (i4MirrType == ISS_MIRR_VLAN_BASED)
            {
                while (nmhGetNextIndexIssMirrorCtrlExtnSrcVlanTable
                       (i4MirrSessionId, &i4NextSessId, i4ContextId,
                        &i4NextContextId, i4SourceId,
                        &i4NextMirrorSrcId) != SNMP_FAILURE)
                {
                    if (i4MirrSessionId != i4NextSessId)
                    {
                        break;
                    }
                    i4SourceId = i4NextMirrorSrcId;
                    nmhGetIssMirrorCtrlExtnSrcMode (i4MirrSessionId,
                                                    i4SourceId, &i4SourceMode);

                    CfaCliConfGetIfName ((UINT4) i4SourceId,
                                         (INT1 *) au1IfName);
                    switch (i4SourceMode)
                    {
                        case ISS_MIRR_INGRESS:
                            CliPrintf (CliHandle,
                                       "monitor session %d source vlan "
                                       "%d rx\r\n", i4MirrSessionId,
                                       i4SourceId);
                            break;
                        case ISS_MIRR_EGRESS:
                            CliPrintf (CliHandle,
                                       "monitor session %d source vlan "
                                       "%d tx\r\n", i4MirrSessionId,
                                       i4SourceId);
                            break;
                        case ISS_MIRR_BOTH:
                        default:
                            CliPrintf (CliHandle,
                                       "monitor session %d source vlan "
                                       "%d both\r\n", i4MirrSessionId,
                                       i4SourceId);
                            break;
                    }

                }
            }

            /* Destination Info */
            i4DestId = 0;
            while (nmhGetNextIndexIssMirrorCtrlExtnDestinationTable
                   (i4MirrSessionId, &i4NextSessId, i4DestId,
                    &i4NextMirrorDestId) != SNMP_FAILURE)
            {
                if (i4MirrSessionId != i4NextSessId)
                {
                    break;
                }
                i4DestId = i4NextMirrorDestId;
                CfaCliConfGetIfName ((UINT4) i4DestId, (INT1 *) au1IfName);
                CliPrintf (CliHandle,
                           "monitor session %d destination interface %s\r\n",
                           i4MirrSessionId, au1IfName);
            }

            /* RSPAN info */
            nmhGetIssMirrorCtrlExtnRSpanStatus (i4MirrSessionId,
                                                &i4RSpanStatus);

            if (i4RSpanStatus != ISS_MIRR_RSPAN_DISABLED)
            {
                nmhGetIssMirrorCtrlExtnRSpanVlanId (i4MirrSessionId,
                                                    &i4RspanVlanId);

                if (i4RSpanStatus == ISS_MIRR_RSPAN_DEST)
                {
                    CliPrintf (CliHandle,
                               "monitor session %d destination remote "
                               "vlan %d\r\n", i4MirrSessionId, i4RspanVlanId);
                }
                else
                {
                    CliPrintf (CliHandle, "monitor session %d source remote "
                               "vlan %d\r\n", i4MirrSessionId, i4RspanVlanId);
                }

            }
        }
        while (nmhGetNextIndexIssMirrorCtrlExtnTable
               (i4MirrSessionId, &i4NextSessId) == SNMP_SUCCESS);
    }
    /* Running config for http */
    nmhGetIssHttpStatus (&i4HttpStatus);
    nmhGetIssHttpPort (&i4HttpPort);
    if (i4HttpPort != ISS_DEF_HTTP_PORT)
    {
        CliPrintf (CliHandle, "ip http port %d\r\n", i4HttpPort);
        u4Flag = 1;
    }
    if (i4HttpStatus != ISS_HTTP_STATUS_ENABLED)
    {
        CliPrintf (CliHandle, "set ip http disable\r\n");
        u4Flag = 1;
    }
    /* Running config for telnet */
    nmhGetIssTelnetStatus (&i4TelnetStatus);
    if (i4TelnetStatus != ISS_ENABLE)
    {
        CliPrintf (CliHandle, "no feature telnet\r\n");
        u4Flag = 1;
    }

    nmhGetIssMgmtInterfaceRouting (&i4RoutingFlag);
    if (i4RoutingFlag != ISS_DISABLE)
    {
        CliPrintf (CliHandle, "set mgmt-port routing enable\r\n");
    }
    if (u4Flag == 1)
    {
        CliPrintf (CliHandle, "!\r\n");
    }

    UtlShMemFreeVlanList (pu1VlanList);
    CliUnRegisterLock (CliHandle);

    ISS_UNLOCK ();
    return (CLI_SUCCESS);
}

/***************************************************************/
/*  Function Name   : IssConfIpAuthOctetToVlanList             */
/*  Description     : This function is used to convert         */
/*                    octet to vlan list                       */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    pOctet      - Pointer to octet           */
/*                    u4OctetLen  - Length of octet            */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
IssConfIpAuthOctetToVlanList (tCliHandle CliHandle,
                              UINT1 *pOctet, UINT4 u4OctetLen)
{
    UINT2               u2VlanId = 0;
    UINT2               u2Check = 0;
    UINT2               u2PrevVlanId = 0;
    UINT4               u4IsAll = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               bOutCome;
    UINT1              *pu1VlanList = NULL;

    CLI_MEMSET (au1TmpVlanList, 0, ISS_VLAN_LIST_SIZE);

    CLI_MEMSET (au1TmpVlanList, 0xff, ISS_VLAN_LIST_SIZE - 1);

    ISS_LAST_BYTE_IN_PORTLIST (au1TmpVlanList, VLAN_DEV_MAX_VLAN_ID,
                               ISS_VLAN_LIST_SIZE);

    pu1VlanList = UtlShMemAllocVlanList ();
    if (pu1VlanList == NULL)
    {
        CliPrintf (CliHandle, "\r%% Memory allocation for Vlan List"
                   " failed.\r\n");
        return;
    }

    CLI_MEMSET (pu1VlanList, 0, ISS_VLAN_LIST_SIZE);
    CLI_MEMCPY (pu1VlanList, pOctet, u4OctetLen);

    u4IsAll =
        (UINT4) CLI_MEMCMP (pu1VlanList, au1TmpVlanList, ISS_VLAN_LIST_SIZE);
    if (u4IsAll != 0)
    {
        for (u2VlanId = ISS_MIN_VLAN_ID; u2VlanId <= ISS_MAX_VLAN_ID;
             u2VlanId++)
        {
            ISS_IS_MEMBER_OF_PORTLIST (pu1VlanList, u2VlanId, bOutCome);
            if (bOutCome == ISS_TRUE)
            {
                if (u2Check == 0)
                {
                    u4PagingStatus =
                        (UINT4) CliPrintf (CliHandle, " vlan %d", u2VlanId);
                }
                else if (u2Check == 1)
                {
                    if (++u2PrevVlanId == u2VlanId)
                    {
                        u2Check++;
                        u2PrevVlanId = u2VlanId;
                        continue;
                    }
                    u4PagingStatus =
                        (UINT4) CliPrintf (CliHandle, ",%d", u2VlanId);
                    u2Check = 0;
                }
                else
                {
                    if (++u2PrevVlanId == u2VlanId)
                    {
                        u2Check++;
                        u2PrevVlanId = u2VlanId;
                        continue;
                    }
                    u4PagingStatus = (UINT4) CliPrintf (CliHandle, "-%d,%d",
                                                        u2PrevVlanId, u2VlanId);
                    u2Check = 0;
                }
                u2Check++;
                u2PrevVlanId = u2VlanId;

            }

            if (u2Check > 1)
            {
                u4PagingStatus =
                    (UINT4) CliPrintf (CliHandle, "-%d", u2PrevVlanId);
                u2Check = 1;
            }

            if (u4PagingStatus == CLI_FAILURE)
            {
                break;
            }
        }
    }
    UtlShMemFreeVlanList (pu1VlanList);

    return;

}

/***************************************************************/
/*  Function Name   : IssEnableAllPortsInPortList              */
/*  Description     : This function is used to enable all ports*/
/*                    in port list                             */
/*  Input(s)        :                                          */
/*                    au1PortArray      - array of ports       */
/*                    u4Length  -        Length of octet       */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
IssEnableAllPortsInPortList (UINT1 *pu1PortArray, UINT4 u4Length)
{
    MEMSET (pu1PortArray, 0, u4Length);

    VcmGetContextPortList (VCM_DEFAULT_CONTEXT, pu1PortArray);

}

INT4
CliSetIssHOlBlockPrevention (tCliHandle CliHandle, UINT4 u4IfIndex,
                             INT4 i4HolStatus)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2IssPortHOLBlockPrevention (&u4ErrorCode, (INT4) u4IfIndex,
                                            i4HolStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIssPortHOLBlockPrevention ((INT4) u4IfIndex, i4HolStatus) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliSetIssCpuCntrlLearning                */
/*  Description     : This function is used to enable/disable  */
/*                    the CPU controlled learning.             */
/*  Input(s)        : CliHandle   - CLI Handle                 */
/*                    u4IfIndex   - Interface Index            */
/*                    i4CpuLearnStatus   - CPU Controlled      */
/*                                       enable/disable status */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
INT4
CliSetIssCpuCntrlLearning (tCliHandle CliHandle, UINT4 u4IfIndex,
                           INT4 i4CpuLearnStatus)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2IssPortCpuControlledLearning (&u4ErrorCode, (INT4) u4IfIndex,
                                               i4CpuLearnStatus) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIssPortCpuControlledLearning ((INT4) u4IfIndex, i4CpuLearnStatus)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliSetIssMdiOrMdixCap                    */
/*  Description     : This function is used to set the Auto    */
/*                    MDI/MDIX Crossover of an interace        */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4IfIndex   - Interface Index            */
/*                    i4Val       - AUTO/MDI/MDIX              */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliSetIssMdiOrMdixCap (tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4Val)
{

    UINT4               u4ErrCode;

    if (nmhTestv2IssPortMdiOrMdixCap (&u4ErrCode,
                                      (INT4) u4IfIndex, i4Val) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIssPortMdiOrMdixCap ((INT4) u4IfIndex, i4Val) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliGetIssMdiOrMdixCap                    */
/*  Description     : This function is used to get the Auto    */
/*                    MDI/MDIX Crossover of an interace        */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    u4IfIndex   - Interface Index            */
/*                    i4Val       - Pointer to AUTO/MDI/MDIX   */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliGetIssMdiOrMdixCap (tCliHandle CliHandle, UINT4 u4IfIndex, INT4 *i4Val)
{

    if (nmhGetIssPortMdiOrMdixCap ((INT4) u4IfIndex, i4Val) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliSetIssPauseRateLimit                  */
/*  Description     : This function is used to configure the   */
/*                    packet rate to transmit MAC control pause*/
/*                    frames on a interface                    */
/*  Input(s)        : CliHandle   - CLI Handle                 */
/*                    u4IfIndex   - Interface Index            */
/*                    i4MaxPortRate - threshold packet rate    */
/*                    i4MinPortRate - resume packet rate       */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
INT4
CliSetIssPauseRateLimit (tCliHandle CliHandle, UINT4 u4IfIndex,
                         INT4 i4MaxPortRate, INT4 i4MinPortRate)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2IssPortCtrlFlowControlMaxRate (&u4ErrorCode, (INT4) u4IfIndex,
                                                i4MaxPortRate) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Invalid higher water mark\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetIssPortCtrlFlowControlMaxRate ((INT4) u4IfIndex, i4MaxPortRate) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2IssPortCtrlFlowControlMinRate (&u4ErrorCode, (INT4) u4IfIndex,
                                                i4MinPortRate) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Invalid lower water mark\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetIssPortCtrlFlowControlMinRate ((INT4) u4IfIndex, i4MinPortRate) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

#ifdef NPAPI_WANTED
/***************************************************************/
/*  Function Name   : IssIntfShowRunningConfig                 */
/*  Description     : This function is used to display the     */
/*                    current system interface configuration   */
/*  Input(s)        :                                          */
/*                    CliHandle  - CLI Handle                  */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
IssIntfShowRunningConfig (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 *pu1Flag)
{
    INT4                i4IssPortCtrlIndex;
    INT4                i4IssNextPortCtrlIndex;
    INT4                i4RetPortCtrlMode;
    INT4                i4RetVal = 0;
    INT4                i4BurstSize = 0;
    INT4                i4RateLimitSize = 0;
    INT4                i4MinPortRate = 0;
    INT4                i4MaxPortRate = 0;
    INT4                i4TempIndex = 0;
    INT4                i4StormControlStatus = 0;
#ifdef MRVLLS
    INT4                i4RateCtrl;
#endif
    INT1                ai1IfName[CFA_CLI_MAX_IF_NAME_LEN];

    MEMSET (ai1IfName, 0, CFA_CLI_MAX_IF_NAME_LEN);

    CliRegisterLock (CliHandle, IssLock, IssUnLock);
    ISS_LOCK ();

    CfaCliConfGetIfName ((UINT4) i4IfIndex, ai1IfName);
    if (nmhGetFirstIndexIssExtRateCtrlTable (&i4IssPortCtrlIndex) !=
        SNMP_FAILURE)
    {
        do
        {
            i4IssNextPortCtrlIndex = i4IssPortCtrlIndex;

            if (i4IssNextPortCtrlIndex == i4IfIndex)
            {
                nmhGetIssPortCtrlMode (i4IssNextPortCtrlIndex,
                                       &i4RetPortCtrlMode);
                if (i4RetPortCtrlMode != ISS_AUTO)
                {
                    if (*pu1Flag == OSIX_FALSE)
                    {
                        CliPrintf (CliHandle, "interface %s\r\n", ai1IfName);
                        *pu1Flag = OSIX_TRUE;
                    }
                    if ((i4IssPortCtrlIndex > ISS_ZERO_ENTRY) &&
                        (i4IssPortCtrlIndex <= ISS_MAX_PORTS))
                    {
                        CliPrintf (CliHandle, " no negotiation\r\n");
                    }

                    if (nmhGetIssPortCtrlDuplex (i4IssNextPortCtrlIndex,
                                                 &i4RetVal) != SNMP_FAILURE)
                    {
                        if (i4RetVal == ISS_HALFDUP)
                        {
                            CliPrintf (CliHandle, " duplex half\r\n");
                        }
                    }

                    i4TempIndex =
                        ((i4IssNextPortCtrlIndex -
                          1) / ISS_INTF_BREAKOUT_BASE_IDX) *
                        ISS_INTF_BREAKOUT_BASE_IDX + 1;

                    if (nmhGetIssPortCtrlSpeed (i4TempIndex,
                                                &i4RetVal) != SNMP_FAILURE)
                    {
                        if (((i4TempIndex != i4IssNextPortCtrlIndex) &&
                             (((i4RetVal != ISS_40GB)
                               && (i4RetVal != ISS_100GB))
                              &&
                              (((i4RetVal == ISS_56GB)
                                && (((i4IssNextPortCtrlIndex - i4TempIndex) % 2)
                                    == 0)) || (i4RetVal != ISS_56GB))))
                            || (i4TempIndex == i4IssNextPortCtrlIndex))
                        {
                            if (nmhGetIssPortCtrlSpeed (i4IssNextPortCtrlIndex,
                                                        &i4RetVal) !=
                                SNMP_FAILURE)
                            {
                                if (i4RetVal == ISS_10MBPS)
                                {
                                    CliPrintf (CliHandle, " speed 10\r\n");
                                }
                                else if (i4RetVal == ISS_1GB)
                                {
                                    CliPrintf (CliHandle, " speed 1000\r\n");
                                }
                                else if (i4RetVal == ISS_2500MBPS)
                                {
                                    CliPrintf (CliHandle, " speed 2500\r\n");
                                }
                                else if (i4RetVal == ISS_10GB)
                                {
                                    CliPrintf (CliHandle, " speed 10000\r\n");
                                }
                                else if (i4RetVal == ISS_40GB)
                                {
                                    CliPrintf (CliHandle, " speed 40000\r\n");
                                }
                                else if (i4RetVal == ISS_25GB)
                                {
                                    CliPrintf (CliHandle, " speed 25000\r\n");
                                }
                                else if (i4RetVal == ISS_56GB)
                                {
                                    CliPrintf (CliHandle, " speed 50000\r\n");
                                }
                                else if (i4RetVal == ISS_100GB)
                                {
                                    CliPrintf (CliHandle, " speed 100000\r\n");
                                }
                                else
                                {
                                    if (i4RetVal != ISS_100MBPS)
                                    {
                                        CliPrintf (CliHandle,
                                                   " speed auto\r\n");
                                    }
                                }
                            }
                        }

                    }

                }
                if (nmhGetIssPortMdiOrMdixCap (i4IssNextPortCtrlIndex,
                                               &i4RetVal) != SNMP_FAILURE)
                {
                    if (i4RetVal != ISS_DEFAULT_AUTO_MDI_OR_MDIX_CAP)
                    {
                        /* flag check to prevent multiple print */
                        if (*pu1Flag == OSIX_FALSE)
                        {
                            CliPrintf (CliHandle, "interface %s\r\n",
                                       ai1IfName);
                            *pu1Flag = OSIX_TRUE;
                        }
                        if (i4RetVal == ISS_AUTO_MDIX)
                        {
                            CliPrintf (CliHandle, " mdix auto\r\n");
                        }
                        else if (i4RetVal == ISS_MDI)
                        {
                            CliPrintf (CliHandle, " no mdix auto\r\n");
                            CliPrintf (CliHandle, " set port mdi\r\n");
                        }
                        else if (i4RetVal == ISS_MDIX)
                        {
                            CliPrintf (CliHandle, " no mdix auto\r\n");
                            CliPrintf (CliHandle, " set port mdix\r\n");
                        }
                    }
                }
                if (nmhGetIssPortCpuControlledLearning
                    (i4IssNextPortCtrlIndex, &i4RetVal) != SNMP_FAILURE)
                {
                    if (*pu1Flag == OSIX_FALSE)
                    {
                        CliPrintf (CliHandle, "interface %s\r\n", ai1IfName);
                        *pu1Flag = OSIX_TRUE;
                    }
                    if (i4RetVal == ISS_CPU_CNTRL_LEARN_ENABLE)
                    {
                        CliPrintf (CliHandle, " cpu controlled learning\r\n");
                    }
                }
                if (nmhGetIssPortHOLBlockPrevention
                    (i4IssNextPortCtrlIndex, &i4RetVal) != SNMP_FAILURE)
                {
                    /* flag check to prevent multiple print */
                    if ((*pu1Flag == OSIX_FALSE)
                        && (i4RetVal == ISS_ENABLE_HOL))
                    {
                        CliPrintf (CliHandle, "interface %s\r\n", ai1IfName);
                        *pu1Flag = OSIX_TRUE;
                    }
                }

#ifdef MRVLLS
                if (nmhGetIssExtRateCtrlPackets (i4IssNextPortCtrlIndex,
                                                 &i4RateCtrl) != SNMP_FAILURE)
                {
                    if (nmhGetIssExtRateCtrlLimitValue (i4IssNextPortCtrlIndex,
                                                        &i4RetVal) !=
                        SNMP_FAILURE)
                    {
                        if ((i4RetVal != 0) && (i4RateCtrl != 0))
                        {
                            CliPrintf (CliHandle, " storm-control");
                            switch (i4RateCtrl)
                            {
                                case ISS_RATE_BCAST:
                                    CliPrintf (CliHandle, " bcast");
                                    break;
                                case ISS_RATE_MCAST_BCAST:
                                    CliPrintf (CliHandle, " mcast_bcast");
                                    break;
                                case ISS_RATE_DLF_MCAST_BCAST:
                                    CliPrintf (CliHandle, " dlf_mcast_bcast");
                                    break;
                                default:
                                    break;
                            }
                            switch (i4RetVal)
                            {
                                case ISS_STORM_CONTROL_128K:
                                    CliPrintf (CliHandle, " level 128K\r\n");
                                    break;
                                case ISS_STORM_CONTROL_256K:
                                    CliPrintf (CliHandle, " level 256K\r\n");
                                    break;
                                case ISS_STORM_CONTROL_512K:
                                    CliPrintf (CliHandle, " level 512K\r\n");
                                    break;
                                case ISS_STORM_CONTROL_1M:
                                    CliPrintf (CliHandle, " level 1M\r\n");
                                    break;
                                case ISS_STORM_CONTROL_2M:
                                    CliPrintf (CliHandle, " level 2M\r\n");
                                    break;
                                case ISS_STORM_CONTROL_4M:
                                    CliPrintf (CliHandle, " level 4M\r\n");
                                    break;
                                case ISS_STORM_CONTROL_8M:
                                    CliPrintf (CliHandle, " level 8M\r\n");
                                    break;
                                case ISS_STORM_CONTROL_16M:
                                    CliPrintf (CliHandle, " level 16M\r\n");
                                    break;
                                case ISS_STORM_CONTROL_32M:
                                    CliPrintf (CliHandle, " level 32M\r\n");
                                    break;
                                case ISS_STORM_CONTROL_64M:
                                    CliPrintf (CliHandle, " level 64M\r\n");
                                    break;
                                case ISS_STORM_CONTROL_128M:
                                    CliPrintf (CliHandle, " level 128M\r\n");
                                    break;
                                case ISS_STORM_CONTROL_256M:
                                    CliPrintf (CliHandle, " level 256M\r\n");
                                    break;
                            }
                        }
                    }
                }
                if (nmhGetIssExtRateCtrlPortRateLimit (i4IssNextPortCtrlIndex,
                                                       &i4RetVal) !=
                    SNMP_FAILURE)
                {
                    if (i4RetVal == 0)
                    {
                        continue;
                    }
                    CliPrintf (CliHandle, " rate-limit output");

                    switch (i4RetVal)
                    {
                        case ISS_RATE_LIMIT_128K:
                            CliPrintf (CliHandle, " 128K\r\n");
                            break;
                        case ISS_RATE_LIMIT_256K:
                            CliPrintf (CliHandle, " 256K\r\n");
                            break;
                        case ISS_RATE_LIMIT_512K:
                            CliPrintf (CliHandle, " 512K\r\n");
                            break;
                        case ISS_RATE_LIMIT_1M:
                            CliPrintf (CliHandle, " 1M\r\n");
                            break;
                        case ISS_RATE_LIMIT_2M:
                            CliPrintf (CliHandle, " 2M\r\n");
                            break;
                        case ISS_RATE_LIMIT_4M:
                            CliPrintf (CliHandle, " 4M\r\n");
                            break;
                        case ISS_RATE_LIMIT_8M:
                            CliPrintf (CliHandle, " 8M\r\n");
                            break;
                        case ISS_RATE_LIMIT_16M:
                            CliPrintf (CliHandle, " 16M\r\n");
                            break;
                        case ISS_RATE_LIMIT_32M:
                            CliPrintf (CliHandle, " 32M\r\n");
                            break;
                        case ISS_RATE_LIMIT_64M:
                            CliPrintf (CliHandle, " 64M\r\n");
                            break;
                        case ISS_RATE_LIMIT_128M:
                            CliPrintf (CliHandle, " 128M\r\n");
                            break;
                        case ISS_RATE_LIMIT_256M:
                            CliPrintf (CliHandle, " 256M\r\n");
                            break;
                        default:
                            CliPrintf (CliHandle, " None\r\n");
                            break;
                    }
                }

#else
                if (nmhGetIssExtDefaultRateCtrlStatus
                    (i4IfIndex, &i4StormControlStatus) != SNMP_FAILURE)
                {
                    if (i4StormControlStatus != ISS_STORM_CONTROL_ENABLE)
                    {
                        CliPrintf (CliHandle, " storm-control disable\r\n");
                    }
                    else
                    {

                        if (nmhGetIssRateCtrlDLFLimitValue
                            (i4IssNextPortCtrlIndex, &i4RetVal) != SNMP_FAILURE)
                        {
                            /* flag check to prevent multiple print */
                            if ((*pu1Flag == OSIX_FALSE) && (i4RetVal != 0))
                            {
                                CliPrintf (CliHandle, "interface %s\r\n",
                                           ai1IfName);
                                *pu1Flag = OSIX_TRUE;
                            }
                            if (i4RetVal != 0)
                            {
                                CliPrintf (CliHandle, " storm-control");
                                CliPrintf (CliHandle, " dlf level %d\r\n",
                                           i4RetVal);
                            }
                        }
                        if (nmhGetIssRateCtrlBCASTLimitValue
                            (i4IssNextPortCtrlIndex, &i4RetVal) != SNMP_FAILURE)
                        {
                            /* flag check to prevent multiple print */
                            if ((*pu1Flag == OSIX_FALSE) && (i4RetVal != 0))
                            {
                                CliPrintf (CliHandle, "interface %s\r\n",
                                           ai1IfName);
                                *pu1Flag = OSIX_TRUE;
                            }
                            if (i4RetVal != 0)
                            {
                                CliPrintf (CliHandle, " storm-control");
                                CliPrintf (CliHandle, " broadcast level %d\r\n",
                                           i4RetVal);
                            }

                        }
                        if (nmhGetIssRateCtrlMCASTLimitValue
                            (i4IssNextPortCtrlIndex, &i4RetVal) != SNMP_FAILURE)
                        {
                            /* flag check to prevent multiple print */
                            if ((*pu1Flag == OSIX_FALSE) && (i4RetVal != 0))
                            {
                                CliPrintf (CliHandle, "interface %s\r\n",
                                           ai1IfName);
                                *pu1Flag = OSIX_TRUE;
                            }
                            if (i4RetVal != 0)
                            {
                                CliPrintf (CliHandle, " storm-control");
                                CliPrintf (CliHandle, " multicast level %d\r\n",
                                           i4RetVal);
                            }
                        }
                    }
                }
                if (nmhGetIssPortCtrlFlowControlMaxRate
                    (i4IssNextPortCtrlIndex, &i4MaxPortRate) != SNMP_FAILURE)
                {
                    if (nmhGetIssPortCtrlFlowControlMinRate
                        (i4IssNextPortCtrlIndex,
                         &i4MinPortRate) != SNMP_FAILURE)
                    {
                        if (i4MaxPortRate != 0)
                        {
                            if (*pu1Flag == OSIX_FALSE)
                            {
                                CliPrintf (CliHandle, "interface %s\r\n",
                                           ai1IfName);
                                *pu1Flag = OSIX_TRUE;
                            }
                            CliPrintf (CliHandle, " rate-limit pause");
                            CliPrintf (CliHandle, " %d %d\r\n", i4MaxPortRate,
                                       i4MinPortRate);
                        }

                    }

                }
                if (nmhGetIssExtRateCtrlPortRateLimit (i4IssNextPortCtrlIndex,
                                                       &i4RateLimitSize) !=
                    SNMP_FAILURE)
                {
                    if (nmhGetIssExtRateCtrlPortBurstSize
                        (i4IssNextPortCtrlIndex, &i4BurstSize) != SNMP_FAILURE)
                    {
                        if ((i4RateLimitSize == 0) && (i4BurstSize == 0))
                        {
                            continue;
                        }
                        /* flag check to prevent multiple print */
                        if (*pu1Flag == OSIX_FALSE)
                        {
                            CliPrintf (CliHandle, "interface %s\r\n",
                                       ai1IfName);
                            *pu1Flag = OSIX_TRUE;
                        }

#ifdef FULCRUM_WANTED
                        CliPrintf (CliHandle, "rate-limit-output");
                        if (i4RateLimitSize != ISS_CLI_RATE_LIMIT_INVALID)
                        {
                            CliPrintf (CliHandle, " packet-rate %d",
                                       i4RateLimitSize);
                        }
                        if (i4BurstSize != ISS_CLI_RATE_LIMIT_INVALID)
                        {
                            CliPrintf (CliHandle, " burst-size %d",
                                       i4BurstSize);
                        }
#else
                        CliPrintf (CliHandle, " rate-limit output");
                        if ((i4RateLimitSize != 0) && (i4BurstSize != 0))
                        {
                            CliPrintf (CliHandle,
                                       " rate-value %d burst-value %d\r\n",
                                       i4RateLimitSize, i4BurstSize);
                            continue;
                        }
                        if (i4RateLimitSize != 0)
                        {
                            CliPrintf (CliHandle, " rate-value %d\r\n",
                                       i4RateLimitSize);
                            continue;
                        }
                        if (i4BurstSize != 0)
                        {
                            CliPrintf (CliHandle, " burst-value %d\r\n",
                                       i4BurstSize);
                            continue;
                        }
#endif
                        CliPrintf (CliHandle, "\r\n");
                    }
                }
#endif
            }

        }
        while (nmhGetNextIndexIssExtRateCtrlTable
               (i4IssNextPortCtrlIndex, &i4IssPortCtrlIndex) == SNMP_SUCCESS);
    }

    ISS_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return (CLI_SUCCESS);
}
#endif /* NPAPI_WANTED */

/***************************************************************/
/*  Function Name   : IssFirmwareUpgrade                       */
/*  Description     : This function is used to copy upgrading  */
/*                    the image from remote to local           */
/*  Input(s)        :                                          */
/*                    CliHandle   - CLI Handle                 */
/*                    au1SrcFileName - Source File Name        */
/*                    SrcIpAddress - Source IP address       */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
IssFirmwareUpgrade (tCliHandle CliHandle, UINT1 *pu1SrcImageName,
                    tIPvXAddr SrcIpAddress, UINT1 *pu1HostName,
                    UINT1 *pu1DstImageName, UINT1 *pu1CustFilePath)
{
    INT4                i4RetVal;
    CHR1               *pu1String;
    UINT1               au1IfIP[MAX_NAME_SIZE];
    /* the local file name is prepended with the base dir which is
     * defied as FLASH, allocate extra 10 bytes for this
     */
    UINT1               au1LocalDstFile[ISS_CONFIG_FILE_NAME_LEN +
                                        MAX_FILEPATH_LEN] = "";
#ifdef RM_WANTED
    INT4                i4ImageType = 0;
#endif
    tDNSResolvedIpInfo  ResolvedIpInfo;
    UINT1               au1TempName[DNS_MAX_QUERY_LEN];
    UINT1               u1TempLen = 0;

    MEMSET (&ResolvedIpInfo, 0, sizeof (tDNSResolvedIpInfo));
    MEMSET (au1TempName, 0, DNS_MAX_QUERY_LEN);
    pu1String = (CHR1 *) & au1IfIP[0];
    UNUSED_PARAM (*pu1String);

    /* If family is DNS host name resolve and copy IpAddress to au1Addr. */
    if (SrcIpAddress.u1Afi == IPVX_DNS_FAMILY)
    {
        u1TempLen = (UINT1) STRLEN (pu1HostName);
        STRNCPY (au1TempName, pu1HostName, u1TempLen);
        au1TempName[u1TempLen] = '\0';
        i4RetVal =
            FsUtlIPvXResolveHostName (au1TempName, DNS_BLOCK, &ResolvedIpInfo);

        if (i4RetVal == DNS_NOT_RESOLVED)
        {
            CliPrintf (CliHandle, "\n%%Cannot resolve the host name \n");
            return (CLI_FAILURE);
            /*not resolved */
        }
        else if (i4RetVal == DNS_IN_PROGRESS)
        {
            CliPrintf (CliHandle, "\n%% Host name resolution in Progress \n");
            return (CLI_FAILURE);
            /*in progress */
        }
        else if (i4RetVal == DNS_CACHE_FULL)
        {
            CliPrintf (CliHandle,
                       "\n%%DNS cache full, cannot resolve at the moment");
            return (CLI_FAILURE);
            /*cache full */
        }

        MEMSET (au1TempName, 0, DNS_MAX_QUERY_LEN);
        MEMSET (SrcIpAddress.au1Addr, 0, IPVX_MAX_INET_ADDR_LEN);
        /* If the hostname resolves to both IPv4 and IPv6 
         * address, IPv6 address is considered */
        if (ResolvedIpInfo.Resolv6Addr.u1AddrLen != 0)
        {
            MEMCPY (au1TempName, ResolvedIpInfo.Resolv6Addr.au1Addr,
                    IPVX_IPV6_ADDR_LEN);
            SrcIpAddress.u1AddrLen = (UINT1) IPVX_IPV6_ADDR_LEN;
            SrcIpAddress.u1Afi = (UINT1) IPVX_ADDR_FMLY_IPV6;
            MEMCPY (SrcIpAddress.au1Addr, au1TempName, IPVX_IPV6_ADDR_LEN);
        }
        else
        {
            MEMCPY (au1TempName, ResolvedIpInfo.Resolv4Addr.au1Addr,
                    IPVX_IPV4_ADDR_LEN);
            SrcIpAddress.u1AddrLen = (UINT1) IPVX_IPV4_ADDR_LEN;
            SrcIpAddress.u1Afi = (UINT1) IPVX_ADDR_FMLY_IPV4;
            MEMCPY (SrcIpAddress.au1Addr, au1TempName, IPVX_IPV4_ADDR_LEN);
        }
    }

    if (CliIssCheckMsrStatus (CliHandle) == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }
    if (SrcIpAddress.u1Afi != 0)
    {
        /* Remote to local */
        SPRINTF ((CHR1 *) au1LocalDstFile, "%s%s", (UINT1 *) pu1CustFilePath,
                 (UINT1 *) pu1DstImageName);
        if ((i4RetVal =
             tftpcRecvFile (SrcIpAddress, pu1SrcImageName,
                            au1LocalDstFile)) != TFTPC_OK)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to copy remote file to flash\r\n");
            return (CLI_FAILURE);
        }
#ifdef RM_WANTED
        else
        {
            MEMSET (au1IfIP, 0, MAX_NAME_SIZE);

            if (ISS_STRCMP (pu1DstImageName, "normal") == 0)
            {
                STRCPY (gu1FirmwareImageName, ISS_FIRMWARE_NORMAL);
                i4ImageType = ISS_FLASH_NORMAL;
            }
            else if (ISS_STRCMP (pu1DstImageName, "fallback") == 0)
            {
                STRCPY (gu1FirmwareImageName, ISS_FIRMWARE_FALLBACK);
                i4ImageType = ISS_FLASH_FALLBACK;
            }
            if ((i4ImageType == ISS_FLASH_NORMAL) ||
                (i4ImageType == ISS_FLASH_FALLBACK))
            {
                if (IssCustReadFile (gu1FirmwareImageName) == ISS_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Unable to read and send buffer to kernel \r\n");
                    return (CLI_FAILURE);
                }
            }

            MEMSET (au1IfIP, 0, MAX_NAME_SIZE);
            RmSendImagetoAttachedPeers ();
            CliPrintf (CliHandle, "\r ...Completed: 100 %%... \r\n");
            CliPrintf (CliHandle,
                       "\rFirmware Upgrade Over://%s/%s ==> flash:%s" "\r\n",
                       SrcIpAddress.au1Addr, pu1SrcImageName, au1LocalDstFile);

            /*  To initiate firmware upgrade from master */
            SYS_LOG_MSG ((SYSLOG_NOTICE_LEVEL, (UINT4) gi4TftpSysLogId,
                          "Firmware upgrade successful ..!!!"));
            return (CLI_SUCCESS);
        }
#else
        if (i4RetVal == TFTPC_OK)
        {
            /*Callback function for Custom firmware upgrade */
            if (ISS_CALLBACK_FN (ISS_EVT_CUST_FIRM_UPG).uIssAppCbFn.
                pIssCustFirmUpgradeFn != NULL)

            {
                if (ISS_FAILURE ==
                    ISS_CALLBACK_FN (ISS_EVT_CUST_FIRM_UPG).uIssAppCbFn.
                    pIssCustFirmUpgradeFn ((CONST UINT1 *) pu1DstImageName,
                                           pu1CustFilePath))
                {
                    return CLI_FAILURE;
                }
            }
            SYS_LOG_MSG ((SYSLOG_NOTICE_LEVEL, (UINT4) gi4TftpSysLogId,
                          "Firmware upgrade successful ..!!!"));
            return (CLI_SUCCESS);
        }
#endif
    }
    return (CLI_FAILURE);
}

/***************************************************************/
/*  Function Name   : CliSetIssAuditLogStatus                    */
/*  Description     : This Function sets the Audit Logging Status*/
/*  Input(s)        : CliHandle  - CLI Handle                    */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliSetIssAuditLogStatus (tCliHandle CliHandle, UINT4 u4CliAuditStatus)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2IssAuditLogStatus (&u4ErrorCode, (INT4) u4CliAuditStatus) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIssAuditLogStatus (u4CliAuditStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (u4CliAuditStatus == ISS_DISABLE)
    {
        CliLogAuditDisableStatus ();
    }
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliSetIssAuditLogFile                    */
/*  Description     : Sets the filename for Audit Logging      */
/*  Input(s)        :            CliHandle  - CLI Handle       */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliSetIssAuditLogFile (tCliHandle CliHandle, UINT1 *pu1AuditFile)
{
    UINT4               u4ErrorCode;
    UINT1               au1AuditFileName[ISS_AUDIT_FILE_NAME_LENGTH];
    tSNMP_OCTET_STRING_TYPE AuditFileName;

    AuditFileName.i4_Length = (INT4) STRLEN (pu1AuditFile);
    AuditFileName.pu1_OctetList = au1AuditFileName;

    MEMSET (au1AuditFileName, 0, ISS_AUDIT_FILE_NAME_LENGTH);
    MEMCPY (AuditFileName.pu1_OctetList, pu1AuditFile, AuditFileName.i4_Length);

    if (nmhTestv2IssAuditLogFileName (&u4ErrorCode, &AuditFileName) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Error in restore file name \r\n");
        return (CLI_FAILURE);
    }
    if (nmhSetIssAuditLogFileName (&AuditFileName) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;

}

/***************************************************************/
/*  Function Name   : CliSetIssAuditLogFileSize               */
/*  Description     : Sets the file size for Audit Log File   */
/*  Input(s)        : CliHandle  - CLI Handle                 */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliSetIssAuditLogFileSize (tCliHandle CliHandle, UINT4 u4AuditFileSize)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2IssAuditLogFileSize (&u4ErrorCode, u4AuditFileSize) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Audit log File size\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetIssAuditLogFileSize (u4AuditFileSize) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************/
/*  Function Name   : CliSetIssAuditLogSizeThreshold               */
/*  Description     : Sets the threshold value for Audit Log File  */
/*  Input(s)        : CliHandle  - CLI Handle                      */
/*  Output(s)       : None                                         */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                      */
/*******************************************************************/
INT4
CliSetIssAuditLogSizeThreshold (tCliHandle CliHandle,
                                UINT4 u4AuditLogSizeThreshold)
{
    UINT4               u4ErrorCode;
    if (SNMP_FAILURE == nmhTestv2IssAuditLogSizeThreshold
        (&u4ErrorCode, u4AuditLogSizeThreshold))
    {
        CliPrintf (CliHandle, "\r%% Invalid Audit Log Threshold size \r\n");
        return CLI_FAILURE;
    }
    if (SNMP_FAILURE == nmhSetIssAuditLogSizeThreshold
        (u4AuditLogSizeThreshold))
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliSetIssAuditLogReset                   */
/*  Description     : Clear the default log file snd sets log status TRUE */
/*  Input(s)        : CliHandle  - CLI Handle                  */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliSetIssAuditLogReset (tCliHandle CliHandle)
{

    if (nmhSetIssAuditLogReset (ISS_TRUE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliShowIssAuditLog                       */
/*  Description     : Displays Audit information               */
/*  Input(s)        : CliHandle  - CLI Handle                  */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliShowIssAuditLog (tCliHandle CliHandle)
{
    INT4                i4AuditStatus;
    tSNMP_OCTET_STRING_TYPE AuditFile;
    UINT4               u4AuditFileSize;
    UINT4               u4AuditLogSizeThreshold;
    UINT1               au1AudFile[MAX_FILENAME_SIZE + 1];

    MEMSET (au1AudFile, 0, MAX_FILENAME_SIZE + 1);
    AuditFile.pu1_OctetList = &au1AudFile[0];
    nmhGetIssAuditLogStatus (&i4AuditStatus);

    if (i4AuditStatus == ISS_TRUE)
    {
        CliPrintf (CliHandle, "\r\n Audit Status :  Enabled\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\n Audit Status :  Disabled\n");
    }

    nmhGetIssAuditLogFileName (&AuditFile);
    CliPrintf (CliHandle, "\r\n Audit File Name :  %s\n",
               AuditFile.pu1_OctetList);
    nmhGetIssAuditLogFileSize (&u4AuditFileSize);
    CliPrintf (CliHandle, "\r\n Audit File Size :  %d\n", u4AuditFileSize);
    nmhGetIssAuditLogSizeThreshold (&u4AuditLogSizeThreshold);
    CliPrintf (CliHandle, "\r\n Audit Log Size Threshold : %u\n",
               u4AuditLogSizeThreshold);
    return (CLI_SUCCESS);
}

/***************************************************************/
/*  Function Name   : CliShowIssAuditLogFile                   */
/*  Description     : Displays the content of the Audit        */
/*                    Log File                                 */
/*  Input(s)        : CliHandle  - CLI Handle                  */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliShowIssAuditLogFile (tCliHandle CliHandle)
{
    FILE               *pAuditLogFilePtr1;
    FILE               *pAuditLogFilePtr2;
    INT4                i4LineCount = 0;
    tSNMP_OCTET_STRING_TYPE AuditFile;
    UINT1               au1AudFile[MAX_FILENAME_SIZE + 1];
    CHR1                ac1ReadBuffer[MAX_COLUMN_SIZE];

    MEMSET (au1AudFile, 0, MAX_FILENAME_SIZE + 1);
    MEMSET (ac1ReadBuffer, 0, MAX_COLUMN_SIZE);

    AuditFile.pu1_OctetList = &au1AudFile[0];
    nmhGetIssAuditLogFileName (&AuditFile);

    if ((pAuditLogFilePtr1 = FOPEN ((CONST CHR1 *) au1AudFile, "r")) == 0)
    {
        return CLI_FAILURE;
    }
    if ((pAuditLogFilePtr2 = FOPEN ((CONST CHR1 *) au1AudFile, "r")) == 0)
    {
        fclose (pAuditLogFilePtr1);
        return CLI_FAILURE;
    }

    while (fgets (ac1ReadBuffer, MAX_COLUMN_SIZE, pAuditLogFilePtr1) != NULL)
    {
        if (i4LineCount < LINES_TO_DISPLAY)
        {
            i4LineCount++;
        }
        else
        {
            fgets (ac1ReadBuffer, MAX_COLUMN_SIZE, pAuditLogFilePtr2);
        }
    }
    if (i4LineCount < LINES_TO_DISPLAY)
    {
        while (fgets (ac1ReadBuffer, MAX_COLUMN_SIZE, pAuditLogFilePtr1) !=
               NULL)
        {
            if (ac1ReadBuffer[STRLEN (ac1ReadBuffer) - 1] == '\n')
            {
                /*removing "\n" from that line to avoid double new line character */
                ac1ReadBuffer[STRLEN (ac1ReadBuffer) - 1] = '\0';
            }
            CliPrintf (CliHandle, "%s\r\n", ac1ReadBuffer);
        }
    }

    while (fgets (ac1ReadBuffer, MAX_COLUMN_SIZE, pAuditLogFilePtr2) != NULL)
    {
        if (ac1ReadBuffer[STRLEN (ac1ReadBuffer) - 1] == '\n')
        {
            /*removing "\n" from that line to avoid double new line character */
            ac1ReadBuffer[STRLEN (ac1ReadBuffer) - 1] = '\0';
        }
        CliPrintf (CliHandle, "%s\r\n", ac1ReadBuffer);
    }

    fclose (pAuditLogFilePtr1);
    fclose (pAuditLogFilePtr2);

    return (CLI_SUCCESS);

}

/* CLI routine to start and shutdown a process */
/****************************************************************************/
/*  Function Name   : CliIssSetModuleSystemControl                          */
/*  Description     : This function is used to start/shutdown               */
/*                    a module                                              */
/*  Input(s)        : CliHandle  - CLI Handle                               */
/*                    i4ModuleId - Module ID                                */
/*                    i4SysCtrl  - shutdown/start                           */
/*  Output(s)       : None                                                  */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                               */
/****************************************************************************/
INT4
CliIssSetModuleSystemControl (tCliHandle CliHandle, INT4 i4ModuleId,
                              INT4 i4SysCtrl)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2IssModuleSystemControl (&u4ErrorCode, i4ModuleId, i4SysCtrl)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIssModuleSystemControl (i4ModuleId, i4SysCtrl) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return SNMP_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : CliIssSetMgmtPortRouting                            */
/*  Description     : This function Enables/Disables the Routing          */
/*                    feature over the Management Port                    */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    i4RoutingFlag - ISS_ENABLE                          */
/*                                    ISS_DISABLE                         */
/*                                                                        */
/*  Output(s)       : NONE                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/
INT4
CliIssSetMgmtPortRouting (tCliHandle CliHandle, INT4 i4RoutingFlag)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2IssMgmtInterfaceRouting (&u4ErrorCode, i4RoutingFlag)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIssMgmtInterfaceRouting (i4RoutingFlag) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*  Function Name   : CliIssSetMacLearnLimitRate                            */
/*  Description     : This function is used to set the MAC learn limit Rate */
/*                    and Interval .                                        */
/*  Input(s)        : CliHandle  - CLI Handle                               */
/*                    i4IssMacLearnLimitRate  - Learn Limit value           */
/*                    u4IssMacLearnRateLimitInterval - Learn Limit Interval */
/*  Output(s)       : None                                                  */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                               */
/****************************************************************************/
INT4
CliIssSetMacLearnLimitRate (tCliHandle CliHandle, INT4 i4IssMacLearnLimitRate,
                            UINT4 u4IssMacLearnRateLimitInterval)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2IssMacLearnRateLimit (&u4ErrorCode, i4IssMacLearnLimitRate)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% LearnRateLimit Out of Range \r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2IssMacLearnRateLimitInterval
        (&u4ErrorCode, u4IssMacLearnRateLimitInterval) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% LearnRateLimitInterval Out of Range \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetIssMacLearnRateLimitInterval (u4IssMacLearnRateLimitInterval)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% LearnRateLimitInterval Out of Range \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetIssMacLearnRateLimit (i4IssMacLearnLimitRate) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% LearnRateLimit Out of Range \r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*  Function Name   : CliIssShowMacLearnLimitRate                           */
/*  Description     : This function is used to show the configured switch   */
/*                    MAC learn limit Rate and Interval                     */
/*  Input(s)        : CliHandle  - CLI Handle                               */
/*                                                                          */
/*  Output(s)       : None                                                  */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                               */
/****************************************************************************/
INT4
CliIssShowMacLearnLimitRate (tCliHandle CliHandle)
{
    INT4                i4IssMacLearnLimitRate;
    UINT4               u4IssMacLearnRateLimitInterval;

    nmhGetIssMacLearnRateLimit (&i4IssMacLearnLimitRate);
    nmhGetIssMacLearnRateLimitInterval (&u4IssMacLearnRateLimitInterval);
    CliPrintf (CliHandle, "\r\n Switch MAC Learn Limit Rate :  %d\n",
               i4IssMacLearnLimitRate);
    CliPrintf (CliHandle, "\r\n Switch MAC Learn Limit Rate Interval: %d\n",
               u4IssMacLearnRateLimitInterval);

    return (CLI_SUCCESS);
}

/***************************************************************/
/*  Function Name   : CliIssSetAclHwTrigger                    */
/*  Description     : This function sets the mode to immediate */
/*                     or consolidate based on this mode HW ACL*/
/*                     will be updated.                        */
/*  Input(s)        :                                          */
/*                    CliHandle - CLI Handle                   */
/*                    u4Mode    - Immediate/Consolidated       */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliIssSetAclHwTrigger (tCliHandle CliHandle, INT4 i4Mode)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2IssAclProvisionMode (&u4ErrorCode, i4Mode) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r Unsupported Mode\r\n");
        return (CLI_FAILURE);
    }
    if (nmhSetIssAclProvisionMode (i4Mode) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Provision Mode Set Failed \r\n");
        return (CLI_FAILURE);

    }
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliIssSetAclHwAction                     */
/*  Description     : This function sets the action to either  */
/*                    TRUE or FALSE                            */
/*  Input(s)        :                                          */
/*                    CliHandle - CLI Handle                   */
/*                    iAction   - Immediate/Consolidated       */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/

INT4
CliIssSetAclHwAction (tCliHandle CliHandle, INT4 iAction)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2IssAclTriggerCommit (&u4ErrorCode, iAction) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r Provision mode is incorrect\r\n");
        return (CLI_FAILURE);
    }
    if (nmhSetIssAclTriggerCommit (iAction) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Trigger Commit Failed \r\n");
        return (CLI_FAILURE);

    }
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliIssSetHeartBeatMode                   */
/*  Description     : This function sets the Heart Beat Mode   */
/*                    as either Internal or External.          */
/*                    This configuration will be effective on  */
/*                    next reboot.                             */
/*  Input(s)        :                                          */
/*                    CliHandle  - CLI Handle                  */
/*                    u4RmHbMode - Heart Beat Mode             */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliIssSetHeartBeatMode (tCliHandle CliHandle, UINT4 u4RmHbMode)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2IssHeartBeatMode (&u4ErrorCode, (INT4) u4RmHbMode) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Unable to set Heart Beat Mode\r\n");
        return (CLI_FAILURE);
    }
    if (nmhSetIssHeartBeatMode (u4RmHbMode) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliIssSetRmRType                         */
/*  Description     : This function sets the Redundancy Type   */
/*                    of the system either to Hot or Cold.     */
/*                    This configuration will be effective on  */
/*                    next reboot.                             */
/*  Input(s)        :                                          */
/*                    CliHandle  - CLI Handle                  */
/*                    u4RmRType  - Redundancy Type             */
/*  Output(s)       : Error message - on failure               */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliIssSetRmRType (tCliHandle CliHandle, UINT4 u4RmRType)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2IssRmRType (&u4ErrorCode, (INT4) u4RmRType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Unable to set Redundancy Type\r\n");
        return (CLI_FAILURE);
    }
    if (nmhSetIssRmRType (u4RmRType) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : CliIssSetRmDType                         */
/*  Description     : This function sets the Date Plane Type   */
/*                    in a Redundant systems either to Shared  */
/*                    or Separate.                             */
/*                    This configuration will be effective on  */
/*                    next reboot.                             */
/*  Input(s)        :                                          */
/*                    CliHandle  - CLI Handle                  */
/*                    u4RmDType  - HW model in Redundant system*/
/*  Output(s)       : Error message - on failure               */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
CliIssSetRmDType (tCliHandle CliHandle, UINT4 u4RmDType)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2IssRmDType (&u4ErrorCode, (INT4) u4RmDType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% Unable to set RM Hardware Type\r\n");
        return (CLI_FAILURE);
    }
    if (nmhSetIssRmDType (u4RmDType) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : CliIssTrafficSeprtnCliSetControl                   */
/*                                                                           */
/*     DESCRIPTION      : This function sets traffic separation control to   */
/*                        install ACL/QoS system default or user defined     */
/*                        rules.                                             */
/*     INPUT            :  CliHandle-CLI Handler                             */
/*                         i4TrafficControl -  system default/userdefined    */
/*                         none                                              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CliIssTrafficSeprtnCliSetControl (tCliHandle CliHandle, INT4 i4TrafficControl)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_ERROR;

    /* Test the given input if success then set */
    if (nmhTestv2IssAclTrafficSeperationCtrl (&u4ErrorCode, i4TrafficControl)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetIssAclTrafficSeperationCtrl (i4TrafficControl) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : CliIssTrafficSeprtnShowControl                     */
/*                                                                           */
/*     DESCRIPTION      : This shows traffic separation control status       */
/*     INPUT            :  CliHandle-CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CliIssTrafficSeprtnShowControl (tCliHandle CliHandle)
{
    INT4                i4TrafficControl = ISS_ZERO_ENTRY;

    CliPrintf (CliHandle, "\r\nTRAFFIC SEPARATION AND PROTECTION\r\n");
    CliPrintf (CliHandle, "---------------------------------\r\n");

    nmhGetIssAclTrafficSeperationCtrl (&i4TrafficControl);

    if (i4TrafficControl == ACL_TRAFFICSEPRTN_CTRL_SYSTEM_DEFAULT)
    {
        CliPrintf (CliHandle, "%-33s : system-default\r\n",
                   " Traffic Separation Control");
    }
    else if (i4TrafficControl == ACL_TRAFFICSEPRTN_CTRL_USER_DEFINED)
    {
        CliPrintf (CliHandle, "%-33s : Userdefined\r\n",
                   " Traffic Separation Control");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s : none\r\n",
                   " Traffic Separation Control");
    }

    return (CLI_SUCCESS);
}

/**************************************************************************/
/*  Function Name   : CliIssSetVrfUnqMacOption                            */
/*  Description     : This function Enables/Disables the assigning        */
/*                    unique mac address to each VR                       */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    i4VrfUnqMacFlag - CLI_ENABLE                        */
/*                                      CLI_DISABLE                       */
/*                                                                        */
/*  Output(s)       : NONE                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/
INT4
CliIssSetVrfUnqMacOption (tCliHandle CliHandle, INT4 i4VrfUnqMacFlag)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4Status;

    i4Status =
        (i4VrfUnqMacFlag ==
         CLI_ENABLE) ? (ISS_VRF_UNQ_MAC_ENABLE) : (ISS_VRF_UNQ_MAC_DISABLE);

    if (nmhTestv2IssVrfUnqMacFlag (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIssVrfUnqMacFlag (i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : CliSetIssMaxLoginAttempt                            */
/*  Description     : This function configures the maximum number         */
/*                    of login attempts and lock out time value           */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*              u1MaxLoginAttempt - Maximum login attempt          */
/*              u1LockOutTime     - Time to block a user            */
/*                                                                        */
/*  Output(s)       : NONE                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/
INT4
CliSetIssMaxLoginAttempt (tCliHandle CliHandle,
                          UINT4 u4LockOutTime, UINT1 u1MaxLoginAttempt)
{
    UINT4               u4ErrorCode = 0;
    if (nmhTestv2IssLoginAttempts (&u4ErrorCode, u1MaxLoginAttempt)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhTestv2IssLoginLockTime (&u4ErrorCode, (INT4) u4LockOutTime) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetIssLoginAttempts (u1MaxLoginAttempt) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (nmhSetIssLoginLockTime (u4LockOutTime) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CliIssConfigRestore                                */
/*                                                                           */
/*     DESCRIPTION      : This function sets the mode of restoration         */
/*                        to flash / remote / none and also the IP address   */
/*                        and file name when the mode is remote              */
/*                                                                           */
/*     INPUT            : u4Mode - Mode of restoration                       */
/*                        u4IpAddress  - applicable when mode is remote      */
/*                        *pu1FileName - remote file name                    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
CliIssConfigRestore (tCliHandle CliHandle, UINT4 u4Mode, UINT4 u4IpAddress,
                     UINT1 *pu1FileName)
{
    tSNMP_OCTET_STRING_TYPE sFileOctet;
    UINT4               u4ErrorCode;
    UINT1              *pu1RestoreFile = NULL;

    MEMSET (&sFileOctet, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (u4Mode == ISS_RESTORE_REMOTE)
    {
        /*  Set the TFTP server address,filename. Set the restore option as
         * remote restore.
         */
        pu1RestoreFile = (UINT1 *) pu1FileName;
        sFileOctet.pu1_OctetList = pu1RestoreFile;
        sFileOctet.i4_Length = (INT4) STRLEN (pu1RestoreFile);

        if (nmhTestv2IssConfigRestoreIpAddrType (&u4ErrorCode,
                                                 IPVX_ADDR_FMLY_IPV4)
            == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhTestv2IssConfigRestoreIpAddr (&u4ErrorCode, u4IpAddress)
            == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhTestv2IssConfigRestoreFileName (&u4ErrorCode, &sFileOctet) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhTestv2IssConfigRestoreOption
            (&u4ErrorCode, ISS_CONFIG_REMOTE_RESTORE) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetIssConfigRestoreIpAddrType (IPVX_ADDR_FMLY_IPV4) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhSetIssConfigRestoreIpAddr (u4IpAddress) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhSetIssConfigRestoreFileName (&sFileOctet) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

        if (nmhSetIssConfigRestoreOption (ISS_CONFIG_REMOTE_RESTORE) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }

    }
    else if (u4Mode == ISS_RESTORE_LOCAL)
    {
        if (nmhTestv2IssConfigRestoreOption
            (&u4ErrorCode, ISS_CONFIG_RESTORE) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhSetIssConfigRestoreOption (ISS_CONFIG_RESTORE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    else
    {
        if (nmhTestv2IssConfigRestoreOption
            (&u4ErrorCode, ISS_CONFIG_NO_RESTORE) == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
        if (nmhSetIssConfigRestoreOption (ISS_CONFIG_NO_RESTORE) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return (CLI_FAILURE);
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CliIssClearConfig                                  */
/*                                                                           */
/*     DESCRIPTION      : This function clears all the configuration that has*/
/*                        been made in ISS                                   */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/

INT4
CliIssClearConfig (tCliHandle CliHandle, UINT1 *pu1FileName)
{
    UINT4               u4ErrCode;
    tSNMP_OCTET_STRING_TYPE ConfigRestoreFileName;

    if (pu1FileName != NULL)
    {
        ConfigRestoreFileName.i4_Length = (INT4) STRLEN (pu1FileName);
        ConfigRestoreFileName.pu1_OctetList = pu1FileName;

        if (nmhTestv2IssClearConfigFileName (&u4ErrCode,
                                             &ConfigRestoreFileName) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetIssClearConfigFileName (&ConfigRestoreFileName)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2IssClearConfig (&u4ErrCode, ISS_TRUE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetIssClearConfig (ISS_TRUE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

#endif
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CliIssEstablishClientSession                       */
/*                                                                           */
/*     DESCRIPTION      : This function is to establish SSH/Telnet client    */
/*                        session from ISS                                   */
/*                                                                           */
/*     INPUT            : ppu1ClientSessionInfo - Double pointer containig   */
/*                                                the session info           */
/*                        u1ArgCount            - Number of arguments present*/
/*                                                in client session info     */
/*                        u1Session             - To identify whether it is  */
/*                                                SSH client session or      */
/*                                                Telnet client session      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CliIssEstablishClientSession (UINT1 **ppu1ClientSessionInfo, UINT1 u1ArgCount,
                              UINT1 u1Session)
{
#ifdef OPENPUTTY_WANTED
    INT4                i4Status = 0;

    if (u1Session == ISS_SSH_CLIENT)
    {
        nmhGetIssSshClientStatus (&i4Status);

        if (i4Status == ISS_SSH_CLIENT_DISABLE)
        {
            CLI_SET_ERR (CLI_ISS_SSH_CLIENT_DISABLED);
            return CLI_FAILURE;
        }
        else
        {
            /* Increment the active SSH client session count when the session is established */
            gi4ActiveSshClientSessions++;
            PuttyEstablishClientSession (u1ArgCount, ppu1ClientSessionInfo);
            /* Decrement the active SSH client session count when the session is terminated */
            gi4ActiveSshClientSessions--;
        }
    }
    else if (u1Session == ISS_TELNET_CLIENT)
    {
        nmhGetIssTelnetClientStatus (&i4Status);

        if (i4Status == ISS_TELNET_CLIENT_DISABLE)
        {
            CLI_SET_ERR (CLI_ISS_TELNET_CLIENT_DISABLED);
            return CLI_FAILURE;
        }
        else
        {
            /* Increment the active Telnet client session count when the session is established */
            gi4ActiveTelnetClientSessions++;
            PuttyEstablishClientSession (u1ArgCount, ppu1ClientSessionInfo);
            /* Decrement the active Telnet client session count when the session is terminated */
            gi4ActiveTelnetClientSessions--;
        }
    }
#else
    UNUSED_PARAM (u1Session);
    UNUSED_PARAM (ppu1ClientSessionInfo);
    UNUSED_PARAM (u1ArgCount);
#endif
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CliIssShowSshTelnetClient                          */
/*                                                                           */
/*     DESCRIPTION      : This function is to display the SSH/Telnet client  */
/*                        status and the number of active sessions           */
/*                        session from ISS                                   */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Identifier to display the output in*/
/*                                    the corresponding  ISS console         */
/*                        u1Session - Identifer to decide whether SSH session*/
/*                                    info or Telnet session info has to be  */
/*                                    displayed                              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
CliIssShowSshTelnetClient (tCliHandle CliHandle, UINT1 u1SessionIdentifier)
{
    INT4                i4Status = 0;
    INT4                i4ActiveSessions = 0;

    if (u1SessionIdentifier == ISS_SSH_CLIENT)
    {
        nmhGetIssSshClientStatus (&i4Status);
        CliPrintf (CliHandle, "\r\nSSH Client Status        : %s",
                   i4Status ? "ENABLED" : "DISABLED");
        nmhGetIssActiveSshClientSessions (&i4ActiveSessions);
        CliPrintf (CliHandle, "\r\nNo.of Active SSH Clients : %d\n",
                   i4ActiveSessions);
    }
    else
    {
        nmhGetIssTelnetClientStatus (&i4Status);
        CliPrintf (CliHandle, "\r\nTelnet Client Status        : %s",
                   i4Status ? "ENABLED" : "DISABLED");
        nmhGetIssActiveTelnetClientSessions (&i4ActiveSessions);
        CliPrintf (CliHandle, "\r\nNo.of Active Telnet Clients : %d\n",
                   i4ActiveSessions);
    }
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CliIssWebSessionTimeOut                            */
/*                                                                           */
/*     DESCRIPTION      : This function sets time out value for web session  */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Identifier to display the output in*/
/*                                    the corresponding  ISS console         */
/*                        i4WebSessionTimeOut - time out value               */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
CliIssWebSessionTimeOut (tCliHandle CliHandle, INT4 *pi4WebSessionTimeOut)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2IssWebSessionTimeOut (&u4ErrCode, *pi4WebSessionTimeOut) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nError in configuring Web Session Time Out",
                   "Value out of range");
        return CLI_FAILURE;
    }
    if (nmhSetIssWebSessionTimeOut (*pi4WebSessionTimeOut) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

INT4
CliIssDefaultExecTimeOut (tCliHandle CliHandle, INT4 *pi4DefExecTimeOut)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2IssDefaultExecTimeOut (&u4ErrCode, *pi4DefExecTimeOut) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nError in configuring Default",
                   "Exec-Time Out");
        return CLI_FAILURE;
    }
    if (nmhSetIssDefaultExecTimeOut (*pi4DefExecTimeOut) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
CliIssNoDefaultExecTimeOut (tCliHandle CliHandle)
{

    if (nmhSetIssDefaultExecTimeOut (CLI_SESSION_TIMEOUT) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CliIssSetSshTelnetClientStatus                     */
/*                                                                           */
/*     DESCRIPTION      : This function is to set the status of the telnet or*/
/*                        ssh client                                         */
/*                                                                           */
/*     INPUT            : u1Session - To identify whether the status has to  */
/*                                    be configured for SSH client session or*/
/*                                    Telnet client session                  */
/*                        i4Status  - Status value to be configured          */
/*                                    Enable/Disable                         */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CliIssSetSshTelnetClientStatus (INT4 i4Status, UINT1 u1Session)
{
    UINT4               u4ErrorCode;

    if (u1Session == ISS_TELNET_CLIENT)
    {
        if (nmhTestv2IssTelnetClientStatus (&u4ErrorCode, i4Status) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetIssTelnetClientStatus (i4Status) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2IssSshClientStatus (&u4ErrorCode, i4Status) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetIssSshClientStatus (i4Status) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CliTftpDownloadStatus                              */
/*                                                                           */
/*     DESCRIPTION      : This function is to display the status of the      */
/*                        download initiated by TFTP                         */
/*                                                                           */
/*     INPUT            : i4Data -  Size of Data being downloaded at that    */
/*                                  instant                                  */
/*                                                                           */
/*                                                                           */
/*                        u4FileSize - Entire file size being downloaded     */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : Prints the download status                         */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
CliTftpDownloadStatus (INT4 i4Data, UINT4 u4FileSize)
{
    mmi_printf ("\r ...Completed: %d %%... \r\n", i4Data);
    UNUSED_PARAM (u4FileSize);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CliIssShutModuleShowRunningConfig                  */
/*                                                                           */
/*     DESCRIPTION      : This function is to read the configurations from   */
/*                        the file and display it even if module is shutdown */
/*                                                                           */
/*     INPUT            : u4ModuleId- The module identifier whose configurati*/
/*                                    ons have to be read and displayed      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CliIssShutModuleShowRunningConfig (tCliHandle CliHandle, UINT1 *pu1TaskName)
{
    UINT1               au1StoreFile[ISS_CONFIG_FILE_NAME_LEN];
    INT4                i4TempReadFd = -1;

    MEMSET (au1StoreFile, 0, sizeof (au1StoreFile));
    MEMSET (ai1Cmd, 0, sizeof (ai1Cmd));
    if (STRCMP (pu1TaskName, "OSPF") == 0)
    {
        STRCPY (au1StoreFile, CSR_CONFIG_FILE);
    }
    if (STRCMP (pu1TaskName, "OSV3") == 0)
    {
        STRCPY (au1StoreFile, OSPF3_CSR_CONFIG_FILE);
    }
    if (STRCMP (pu1TaskName, "BGP4") == 0)
    {
        STRCPY (au1StoreFile, BGP4_CSR_CONFIG_FILE);
    }
    if (STRCMP (pu1TaskName, "ISIS") == 0)
    {
        STRCPY (au1StoreFile, ISIS_CSR_FILE);
    }
    if (STRCMP (pu1TaskName, "RSVPTE") == 0)
    {
        STRCPY (au1StoreFile, RSVPTE_CSR_FILE);
    }
    if (STRCMP (pu1TaskName, "LDP") == 0)
    {
        STRCPY (au1StoreFile, LDP_CSR_FILE);
    }

    /*Restoring the config file */
    if (FlashFileExists ((CONST CHR1 *) au1StoreFile) == ISS_SUCCESS)
    {
        if ((i4TempReadFd =
             CliOpenFile ((CONST CHR1 *) au1StoreFile, OSIX_FILE_RO)) < 0)
        {
            mmi_printf ("\rUnable to open File: %s\r\n",
                        (CONST CHR1 *) au1StoreFile);
            return CLI_FAILURE;
        }
        else
        {
            while ((CliGetLine (i4TempReadFd, ai1Cmd)) != ISS_EOF)
            {
                CliPrintf (CliHandle, "\r %s", ai1Cmd);
            }
            CliCloseFile (i4TempReadFd);
            return CLI_SUCCESS;
        }
    }
    else
    {
        return CLI_FAILURE;
    }
}
VOID
CliIssGetDefaultExecTimeOut (INT4 *pi4DefExecTimeOut)
{
    nmhGetIssDefaultExecTimeOut (pi4DefExecTimeOut);
    return;
}

/******************************************************************************
 * FUNCTION NAME    :  CliISSHealthChkClr
 *
 * DESCRIPTION      : This function clear the Stats of the
 *                    OSPF/BGP/RIP/NETIP
 *
 * INPUT            : u1ModuleId
 *                 
 *
 * OUTPUT           : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ******************************************************************************/
INT4
CliIssHealthChkClr (tCliHandle CliHandle, UINT1 *pu1ModuleId)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE ModuleId;

    ModuleId.i4_Length = (INT4) STRLEN (pu1ModuleId);

    ModuleId.pu1_OctetList = (UINT1 *) pu1ModuleId;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2IssHealthChkClearCtr (&u4ErrorCode, &ModuleId) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_ISS_PROTOCOL_NOT_SPECIFIED);
        return CLI_FAILURE;
    }

    if (nmhSetIssHealthChkClearCtr (&ModuleId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*     FUNCTION NAME    : CliIssShowHealthChkStatus                          */
/*                                                                           */
/*     DESCRIPTION      : This shows ISS status                              */
/*     INPUT            :  CliHandle-CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
CliIssShowHealthChkStatus (tCliHandle CliHandle)
{

    INT4                i4IssHealthChkStatus = 0;
    INT4                i4IssHealthChkErrorReason = 0;
    INT4                i4IssHealthChkMemAllocErrPoolId = 0;

    CliPrintf (CliHandle, "\r\nISS HEALTH STATUS\r\n");
    CliPrintf (CliHandle, "---------------------------------\r\n");

    nmhGetIssHealthChkStatus (&i4IssHealthChkStatus);
    nmhGetIssHealthChkErrorReason (&i4IssHealthChkErrorReason);

    if (i4IssHealthChkStatus == ISS_UP_AND_RUNNING)
    {
        CliPrintf (CliHandle, "%-33s : Up & Running\r\n", " ISS Status");
        CliPrintf (CliHandle, "%-33s : None\r\n", " Error Reason");
    }
    if (i4IssHealthChkStatus == ISS_DOWN_AND_NONRECOVERABLE_ERR)
    {

        CliPrintf (CliHandle, "%-33s : Down & Non-Recoverable Error\r\n",
                   " ISS Status");

        if (i4IssHealthChkErrorReason == NON_RECOV_TASK_INIT_FAILURE)
        {
            CliPrintf (CliHandle, "%-33s : Task Initialization Failed\r\n",
                       " Error Reason");
        }
        if (i4IssHealthChkErrorReason == NON_RECOV_INSUFFICIENT_STARTUP_MEMORY)
        {
            CliPrintf (CliHandle, "%-33s : Insufficient Memory\r\n",
                       " Error Reason");
        }

    }

    if (i4IssHealthChkStatus == ISS_UP_RECOVERABLE_RUNTIME_ERR)
    {
        CliPrintf (CliHandle, "%-33s : Up & Recoverable Runtime Error\r\n",
                   " ISS Status");

        if (i4IssHealthChkErrorReason == RECOV_CONFIG_RESTORE_FAILED)
        {
            CliPrintf (CliHandle, "%-33s : Configuration-restore failed\r\n",
                       " Error Reason");
        }
        if (i4IssHealthChkErrorReason == RECOV_PROTOCOL_MEMPOOL_EXHAUSTED)
        {
            nmhGetIssHealthChkMemAllocErrPoolId
                (&i4IssHealthChkMemAllocErrPoolId);
            CliPrintf (CliHandle, "%-33s : MemPool Exhausted\r\n",
                       " Error Reason");
            CliPrintf (CliHandle, "%-33s : %d\r\n", " MemAllocErrPoolId",
                       i4IssHealthChkMemAllocErrPoolId);

        }
        if (i4IssHealthChkErrorReason == RECOV_CRU_BUFF_EXHAUSTED)
        {
            CliPrintf (CliHandle, "%-33s : CRU Buffer Exhaustion\r\n",
                       " Error Reason");
        }

    }

    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : CliIssShowConfigRestoreStatus                      */
/*                                                                           */
/*     DESCRIPTION      : This shows configuration restore status            */
/*     INPUT            :  CliHandle-CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
CliIssShowConfigRestoreStatus (tCliHandle CliHandle)
{

    INT4                i4IssHealthChkConfigRestoreStatus = 0;

    CliPrintf (CliHandle, "\r\nISS CONFIGURATION RESTORE STATUS\r\n");
    CliPrintf (CliHandle, "---------------------------------\r\n");

    nmhGetIssHealthChkConfigRestoreStatus (&i4IssHealthChkConfigRestoreStatus);
    if (i4IssHealthChkConfigRestoreStatus == ISS_CONFIG_RESTORE_SUCCESS)
    {
        CliPrintf (CliHandle, "%-33s : configuration-restore success\r\n",
                   " Config Restore Staus");
    }
    if (i4IssHealthChkConfigRestoreStatus == ISS_CONFIG_RESTORE_FAILED)
    {
        CliPrintf (CliHandle, "%-33s : configuration-restore failed\r\n",
                   " Config Restore Status");
    }
    if (i4IssHealthChkConfigRestoreStatus == ISS_CONFIG_RESTORE_IN_PROGRESS)
    {
        CliPrintf (CliHandle, "%-33s : configuration-restore InProgress\r\n",
                   " Config Restore Status");
    }
    if (i4IssHealthChkConfigRestoreStatus == ISS_CONFIG_RESTORE_DEFAULT)
    {
        CliPrintf (CliHandle, "%-33s : default configuration-restore\r\n",
                   " Config Restore Status");
    }

    return;
}

/*****************************************************************************/
/* Function Name      : IssCliGetShowCmdOutputToFile                         */
/*                                                                           */
/* Description        : This function handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1FileName - The output of the show cmd is          */
/*                      redirected to this file                              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/
INT4
IssCliGetShowCmdOutputToFile (UINT1 *pu1FileName)
{
    if (FileStat ((const CHR1 *) pu1FileName) == OSIX_SUCCESS)
    {
        if (0 != FileDelete (pu1FileName))
        {
            return ISS_FAILURE;
        }
    }
    if (CliGetShowCmdOutputToFile
        ((UINT1 *) pu1FileName, (UINT1 *) ISS_AUDIT_SHOW_CMD) == CLI_FAILURE)
    {
        return ISS_FAILURE;
    }
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssCliCalcSwAudCheckSum                              */
/*                                                                           */
/* Description        : This function handles the Calculation of checksum    */
/*                      for the data in the given input file.                */
/*                                                                           */
/* Input(s)           : pu1FileName - The checksum is calculated for the data*/
/*                      in this file                                         */
/*                                                                           */
/* Output(s)          : pu2ChkSum - The calculated checksum is stored here   */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/
INT4
IssCliCalcSwAudCheckSum (UINT1 *pu1FileName, UINT2 *pu2ChkSum)
{
    INT1                ai1Buf[ISS_CLI_MAX_GROUPS_LINE_LEN + 1];
    INT4                i4Fd;
    INT2                i2ReadLen;
    UINT4               u4Sum = 0;
    UINT2               u2CkSum = 0;

    i4Fd = FileOpen ((CONST UINT1 *) (pu1FileName), ISS_CLI_RDONLY);
    if (i4Fd == -1)
    {
        return ISS_FAILURE;
    }
    MEMSET (ai1Buf, 0, ISS_CLI_MAX_GROUPS_LINE_LEN + 1);
    while (IssCliReadLineFromFile (i4Fd, ai1Buf, ISS_CLI_MAX_GROUPS_LINE_LEN,
                                   &i2ReadLen) != ISS_CLI_EOF)

    {
        if (i2ReadLen > 0)
        {
            UtilCalcCheckSum ((UINT1 *) ai1Buf, (UINT4) i2ReadLen, &u4Sum,
                              &u2CkSum, CKSUM_DATA);
            MEMSET (ai1Buf, '\0', ISS_CLI_MAX_GROUPS_LINE_LEN + 1);
        }
    }
    /*CheckSum for last line */
    if ((u4Sum != 0) || (i2ReadLen != 0))
    {
        UtilCalcCheckSum ((UINT1 *) ai1Buf, (UINT4) i2ReadLen, &u4Sum,
                          &u2CkSum, CKSUM_LASTDATA);
    }
    *pu2ChkSum = u2CkSum;
    if (FileClose (i4Fd) < 0)
    {
        return ISS_FAILURE;
    }
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssCliReadLineFromFile                               */
/*                                                                           */
/* Description        : It is a utility to read a line from the given file   */
/*                      descriptor.                                          */
/*                                                                           */
/* Input(s)           : i4Fd - File Descriptor for the file                  */
/*                      i2MaxLen - Maximum length that can be read and store */
/*                                                                           */
/* Output(s)          : pi1Buf - Buffer to store the line read from the file */
/*                      pi2ReadLen - the total length of the data read       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*****************************************************************************/
INT1
IssCliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen,
                        INT2 *pi2ReadLen)
{
    INT1                i1Char;

    *pi2ReadLen = 0;

    while (FileRead (i4Fd, (CHR1 *) & i1Char, 1) == 1)
    {
        pi1Buf[*pi2ReadLen] = i1Char;
        (*pi2ReadLen)++;
        if (i1Char == '\n')
        {
            return (ISS_CLI_NO_EOF);
        }
        if (*pi2ReadLen == i2MaxLen)
        {
            *pi2ReadLen = 0;
            break;
        }
    }
    pi1Buf[*pi2ReadLen] = '\0';
    return (ISS_CLI_EOF);
}

/*****************************************************************************/
/*     FUNCTION NAME    : CliIssDebugEnable                                  */
/*                                                                           */
/*     DESCRIPTION      : This function calls the low level routines of      */
/*                          debugtrace                                       */
/*     INPUT            :  CliHandle-CLI Handler                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
INT4
CliIssDebugEnable (tCliHandle CliHandle, UINT1 *pu1TraceString)
{
    tSNMP_OCTET_STRING_TYPE SetValIssDebugOption;
    UINT4               u4ErrorCode = ISS_ZERO_ENTRY;
    MEMSET (&SetValIssDebugOption, ISS_ZERO_ENTRY,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    SetValIssDebugOption.pu1_OctetList = pu1TraceString;
    SetValIssDebugOption.i4_Length = (INT4) STRLEN (pu1TraceString);

    if (nmhTestv2IssDebugOption (&u4ErrorCode, &SetValIssDebugOption) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%% Trace Update Failed\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetIssDebugOption (&SetValIssDebugOption) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "%% Trace Update Failed\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}
