/* SOURCE FILE  :
 *
* $Id: issnvram.c,v 1.88 2016/05/11 11:40:58 siva Exp $
 *  ---------------------------------------------------------------------------
 * |  Copyright (C) 2006 Aricent Inc . All Rights Reserved                                 |
 * |                                                                           |                                                                           |
 * |  FILE NAME                   :  issnvram.c                                |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR            :  Aricent Inc.                       |
 * |                                                                           |
 * |  SUBSYSTEM NAME              :  ISS                                       |
 * |                                                                           |
 * |  MODULE NAME                 :  ISS                                       |
 * |                                                                           |
 * |  LANGUAGE                    :  C                                         |
 * |                                                                           |
 * |  TARGET ENVIRONMENT          :  LINUX                                     |
 * |                                                                           |
 * |  AUTHOR                      :  ISS Team                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE       :                                            |
 * |                                                                           |
 * |  DESCRIPTION                 :  This file has the routines for the Iss    |
 * |                                 Nvram related calls.                      |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 * 
 */

#include "issinc.h"
#include "iss.h"
#include "cli.h"
#include "issglob.h"
#include "issnvram.h"
#include "ip6util.h"
#include "msr.h"
#ifdef BGP_WANTED
#include "bgp.h"
#endif /* BGP_WANTED */

tNVRAM_DATA         sNvRamData;
tNVRAM_AGG_MAC      sNvRamAggMac[LA_DEV_MAX_TRUNK_GROUP];
BOOL1               b1IsRollbackInNvRam;
#ifndef NPAPI_WANTED
#define FNP_SUCCESS      1
#define FNP_FAILURE      0
#endif

/****************************************************************************
*                   NVRAM RELATED PORTABLE FUNCIONS                         *
*****************************************************************************/

/****************************************************************************/
/*                                                                          */
/*    Function Name      : GetNvRamValue                                    */
/*                                                                          */
/*    Description        : This function searches pu1SearchString in the    */
/*                         given file and stores the result in pu1StrValue. */
/*                         Each and every line of the file should be in the */
/*                         following format:                                */
/*                         IP_ADDRESS        =     12.0.0.1                 */
/*                         i.e pu1SearchString = pu1StrValue                */
/*                         White space character(s) should follow           */
/*                         pu1SearchString (atleast one). No matter whether */
/*                         pu1StrValue is preceded by white space character */
/*                         or not. Size of pu1StrValue is expected to be    */
/*                         greater than or equal to MAX_COLUMN_LENGTH. It   */
/*                         will store first match value in case of duplicate*/
/*                         pu1SearchString.                                 */
/*                                                                          */
/*    Input(s)           : File name, Search string                         */
/*                                                                          */
/*    Output(s)          : pu1StrValue                                      */
/*                                                                          */
/*    Returns            : NVRAM_SUCCESS/NVRAM_FAILURE/NVRAM_FILE_NOT_FOUND */
/*                                                                          */
/****************************************************************************/

UINT4
GetNvRamValue (UINT1 *pu1FileName, UINT1 *pu1SearchStr, UINT1 *pu1StrValue)
{
    INT4                i4Fd = 0;
    CHR1                au1Buf[MAX_COLUMN_LENGTH],
        au1ReadStr[MAX_COLUMN_LENGTH];
    UINT1               au1StrValue[MAX_COLUMN_LENGTH],
        au1TmpValue[MAX_COLUMN_LENGTH];
    UINT4               u4Flag = 0;
    UINT1              *pu1Temp;

    i4Fd = FileOpen ((CONST UINT1 *) pu1FileName, OSIX_FILE_RO);
    if (i4Fd < 0)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to open NvRam\n");
        return NVRAM_FILE_NOT_FOUND;
    }

    MEMSET (pu1StrValue, 0, MAX_COLUMN_LENGTH);
    MEMSET (au1Buf, 0, MAX_COLUMN_LENGTH);

    while (FsUtlEnhReadLine (i4Fd, (INT1 *) au1Buf, MAX_COLUMN_LENGTH) == OSIX_SUCCESS)
    {
        if ((STRSTR (au1Buf, pu1SearchStr)) != NULL)
        {
            MEMSET (au1ReadStr, 0, MAX_COLUMN_LENGTH);
            MEMSET (au1StrValue, 0, MAX_COLUMN_LENGTH);
            MEMSET (au1TmpValue, 0, MAX_COLUMN_LENGTH);

            SSCANF (au1Buf, "%s%s%s", au1ReadStr, au1StrValue, au1TmpValue);
            if (STRCMP (au1ReadStr, pu1SearchStr) != 0)
            {
                continue;
            }
            if (STRLEN (au1TmpValue) != 0)
            {
                STRCPY (pu1StrValue, au1TmpValue);
            }
            else
            {
                pu1Temp = au1StrValue;
                pu1Temp++;
                STRCPY (pu1StrValue, pu1Temp);
            }

            u4Flag = 1;
            break;
        }
        MEMSET (au1Buf, 0, MAX_COLUMN_LENGTH);
    }

    FileClose (i4Fd);

    if (u4Flag == 1)
    {
        return NVRAM_SUCCESS;
    }
    else
    {
        return NVRAM_FAILURE;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : NvRamRead                                        */
/*                                                                          */
/*    Description        : This function is is a portable routine, to       */
/*                         read the critical configuration for the switch   */
/*                         from NvRam.                                      */
/*                         While reading the information if it is failed    */
/*                         for any/all filed(s) it will set it to default   */
/*                         value.                                           */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : tNVRAM_DATA  *pNvRamData                         */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/*                                                                          */
/*    Note               : The return type should be as mentioned above for */
/*                         ported routines also.                            */
/****************************************************************************/

int
NvRamRead (tNVRAM_DATA * pNvRamData)
{
    UINT4               u4RestoreIpAddr = 0;
    UINT1               u1Index;
    UINT1               au1Buffer[MAX_COLUMN_LENGTH];
    UINT1              *pTemp = NULL;
    INT4                i4Fd = 0;
    UINT1               au1TempMac[18];
    UINT1               u1ErrorCount = 0;
    UINT1               u1RetStatus = 0;
    tMacAddr            DefaultSwitchMac
        = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05 };
    CHR1                au1RestoreFileName[RESTORE_FILE_NAME_LEN +
                                           FLASH_STR_LEN + 1];

    MEMSET (au1Buffer, 0, MAX_COLUMN_LENGTH);

    /* First check whether the file exist or not */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "IP_CONFIG_MODE",
                       au1Buffer) == NVRAM_FILE_NOT_FOUND)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]:Writing new ISS_NVRAM_FILE with default values\n");
        IssInitialiseNvRamDefVal ();

        return FNP_SUCCESS;
    }

    /* Read the IP Config Mode */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "IP_CONFIG_MODE",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]:Failed to read ip config mode\n");
        u1ErrorCount++;
        pNvRamData->u4CfgMode = ISS_CFG_MANUAL;
    }
    else
    {
        pNvRamData->u4CfgMode = (UINT4) ISS_ATOI (au1Buffer);
    }

    /* Check for presence of IP_ADDR_ALLOC_PROTO */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE,
                       (UINT1 *) "IP_ADDR_ALLOC_PROTO", au1Buffer)
        == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Failed to read boot protocol\n");
        u1ErrorCount++;
        pNvRamData->u4IpAddrAllocProto = CFA_PROTO_DHCP;
    }
    else
    {
        pNvRamData->u4IpAddrAllocProto = (UINT4) ISS_ATOI (au1Buffer);
    }


    /* Read default IP Address */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "IP_ADDRESS",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Failed to read ip address\n");
        u1ErrorCount++;
        pNvRamData->u4LocalIpAddr = ISS_DEFAULT_IP_ADDRESS;
    }
    else
    {
        pNvRamData->u4LocalIpAddr = OSIX_NTOHL (INET_ADDR (au1Buffer));

        /* Allow IP address which are Class A, B or C */
        if (!((ISS_IS_ADDR_CLASS_A (pNvRamData->u4LocalIpAddr)) ||
              (ISS_IS_ADDR_CLASS_B (pNvRamData->u4LocalIpAddr)) ||
              (ISS_IS_ADDR_CLASS_C (pNvRamData->u4LocalIpAddr))))
        {
            ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Invalid ip address\n");
            u1ErrorCount++;
            pNvRamData->u4LocalIpAddr = ISS_DEFAULT_IP_ADDRESS;
        }
    }

    /* Read the IP Mask */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "IP_MASK",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Failed to read Ip mask\n");
        u1ErrorCount++;
        pNvRamData->u4LocalSubnetMask = ISS_DEFAULT_IP_MASK;
    }
    else
    {
        pNvRamData->u4LocalSubnetMask = OSIX_NTOHL (INET_ADDR (au1Buffer));

        /* inet_addr routine returns 0xffffffff for all invalid values of
         * au1Buffer. However if au1Buffer = "255.255.255.255" , which is a
         * valid, value it again returns 0xffffffff. This is taken care of by
         * the following check */
        if (pNvRamData->u4LocalSubnetMask == 0xffffffff)
        {
            if (STRNCMP ("255.255.255.255", au1Buffer,
                         STRLEN ("255.255.255.255")))
            {
                ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Incorrect Ip mask\n");
                u1ErrorCount++;
                pNvRamData->u4LocalSubnetMask = ISS_DEFAULT_IP_MASK;
            }
        }

        /* The valid subnet address is from 0 to 32 */
        for (u1Index = 0; u1Index <= ISS_MAX_CIDR; ++u1Index)
        {
            if (gu4IssCidrSubnetMask[u1Index] == pNvRamData->u4LocalSubnetMask)
            {
                break;
            }
        }
        if (u1Index > ISS_MAX_CIDR)
        {
            ISS_TRC (ALL_FAILURE_TRC, "[ERROR]:Invalid Subnet Mask\n");
            u1ErrorCount++;
            pNvRamData->u4LocalSubnetMask = ISS_DEFAULT_IP_MASK;
        }
    }

    /* Read default IPv6 Address */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "IPV6_ADDRESS",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Failed to read ipv6 address\n");
        u1ErrorCount++;
        INET_ATON6 (ISS_DEFAULT_IPV6_ADDRESS, &pNvRamData->localIp6Addr);
    }
    else
    {
        if (INET_ATON6 (au1Buffer, &pNvRamData->localIp6Addr) == 0)
        {
            ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Invalid ipv6 address\n");
            u1ErrorCount++;
            INET_ATON6 (ISS_DEFAULT_IPV6_ADDRESS, &pNvRamData->localIp6Addr);
        }
    }

    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "IPV6_PREFIX_LEN",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]: Failed to read ipv6 prefix length\n");
        u1ErrorCount++;
        pNvRamData->u1PrefixLen = ISS_DEFAULT_IPV6_PREFIX_LEN;
    }
    else
    {
        pNvRamData->u1PrefixLen = (UINT1) ISS_ATOI (au1Buffer);
        if (pNvRamData->u1PrefixLen > 128)
        {
            ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Invalid ipv6 prefix length\n");
            u1ErrorCount++;
            pNvRamData->u1PrefixLen = ISS_DEFAULT_IPV6_PREFIX_LEN;
        }
    }

    /* Read the Default Interface */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "INTERFACE",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]:Failed to read default interface\n");
        u1ErrorCount++;
        ISS_MEMCPY ((UINT1 *) pNvRamData->ai1InterfaceName,
                    ISS_DEFAULT_INTERFACE_NAME,
                    STRLEN (ISS_DEFAULT_INTERFACE_NAME));
        pNvRamData->ai1InterfaceName[STRLEN (ISS_DEFAULT_INTERFACE_NAME)]
            = '\0';
    }
    else
    {
        if (STRLEN (au1Buffer) >= INTERFACE_STR_SIZE)
        {
            ISS_TRC (ALL_FAILURE_TRC,
                     "[ERROR]: default interface exceeds size\n");
            u1ErrorCount++;
            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1InterfaceName,
                        ISS_DEFAULT_INTERFACE_NAME,
                        STRLEN (ISS_DEFAULT_INTERFACE_NAME));
            pNvRamData->ai1InterfaceName[STRLEN (ISS_DEFAULT_INTERFACE_NAME)]
                = '\0';
        }
        else
        {
            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1InterfaceName, au1Buffer,
                        STRLEN (au1Buffer));
            pNvRamData->ai1InterfaceName[STRLEN (au1Buffer)] = '\0';
        }
    }

    /* Check for presence of OOB port */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "MGMT_PORT",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Failed to read MgmtPort Flag\n");
        u1ErrorCount++;
#ifdef MBSM_WANTED
        pNvRamData->u4MgmtPort = TRUE;
#else
        pNvRamData->u4MgmtPort = FALSE;
#endif
    }
    else
    {
        if (STRNCMP (au1Buffer, "YES", STRLEN (au1Buffer)) == 0)
        {
            pNvRamData->u4MgmtPort = TRUE;    /* MgmtPort is present */

        }
        else
        {
            pNvRamData->u4MgmtPort = FALSE;    /* MgmtPort is not present */
        }
    }

    /* Validate the Default Interface Name if OOB is not Present */
    if (pNvRamData->u4MgmtPort == FALSE)
    {
        if (STRNCMP (pNvRamData->ai1InterfaceName, ISS_ALIAS_PREFIX,
                     STRLEN (ISS_ALIAS_PREFIX)) != 0)
        {
            ISS_TRC (ALL_FAILURE_TRC,
                     "[ERROR]: Mismatch between Alias prefix & Default interface\n");
            u1ErrorCount++;
        }
    }

#if defined (RM_WANTED)&& defined (NPAPI_WANTED) && \
    defined (MBSM_WANTED) && ! defined (L2RED_WANTED)

    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "STACK_PORT_COUNT",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Failed to get Stack Port count\n");

        u1ErrorCount++;
        pNvRamData->u2StackPortCount = ISS_DEFAULT_STACK_PORT_COUNT;
    }
    else
    {
        pNvRamData->u2StackPortCount = (UINT2) ISS_ATOI (au1Buffer);
        if (pNvRamData->u2StackPortCount > ISS_MAX_POSSIBLE_STK_PORTS)
        {
            ISS_TRC (ALL_FAILURE_TRC,
                     "[ERROR]: Failed to get Stack Port count\n");
            u1ErrorCount++;
            pNvRamData->u2StackPortCount = ISS_DEFAULT_STACK_PORT_COUNT;
        }

    }
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "STACK_COLD_STDBY",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Failed to get coldstandby state\n");
        u1ErrorCount++;
        pNvRamData->u4ColdStandby = ISS_DEFAULT_STACK_COLD_STANDBY;
    }
    else
    {
        pNvRamData->u4ColdStandby = (UINT4) ISS_ATOI (au1Buffer);
        if ((pNvRamData->u4ColdStandby != ISS_COLDSTDBY_ENABLE) &&
            (pNvRamData->u4ColdStandby != ISS_COLDSTDBY_DISABLE))
        {
            ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Invalid coldstandby state\n");
            u1ErrorCount++;
            pNvRamData->u4ColdStandby = ISS_DEFAULT_STACK_COLD_STANDBY;
        }

    }
#else
    pNvRamData->u2StackPortCount = ISS_DEFAULT_STACK_PORT_COUNT;
    pNvRamData->u4ColdStandby = ISS_DEFAULT_STACK_COLD_STANDBY;
#endif

    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "RM_INTERFACE",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Failed to read RM Interface\n");
        u1ErrorCount++;
        ISS_MEMCPY ((UINT1 *) pNvRamData->ai1RmIfName,
                    ISS_DEFAULT_RM_INTERFACE_NAME,
                    STRLEN (ISS_DEFAULT_RM_INTERFACE_NAME));
        pNvRamData->ai1RmIfName[STRLEN (ISS_DEFAULT_RM_INTERFACE_NAME)] = '\0';
    }
    else
    {
        if ((STRLEN (au1Buffer)) >= CFA_MAX_PORT_NAME_LENGTH)
        {
            ISS_TRC (ALL_FAILURE_TRC, "RM interface exceeds size\n");
            u1ErrorCount++;
            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1RmIfName,
                        ISS_DEFAULT_RM_INTERFACE_NAME,
                        STRLEN (ISS_DEFAULT_RM_INTERFACE_NAME));
            pNvRamData->ai1RmIfName[STRLEN (ISS_DEFAULT_RM_INTERFACE_NAME)]
                = '\0';
        }
        else
        {
            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1RmIfName, au1Buffer,
                        (sizeof (pNvRamData->ai1RmIfName) - 1));
            pNvRamData->ai1RmIfName[(sizeof (pNvRamData->ai1RmIfName) - 1)] =
                '\0';
        }
    }

    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "PIM_MODE",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Failed to read Pim mode\n");
        u1ErrorCount++;
        pNvRamData->u4PimMode = ISS_DEFAULT_PIM_MODE;
    }
    else
    {
        pNvRamData->u4PimMode = (UINT4) ISS_ATOI (au1Buffer);
        /* PIM Modes
         * PIM_DM_MODE = 1
         * PIM_SM_MODE = 2
         */
        if ((pNvRamData->u4PimMode != 1) && (pNvRamData->u4PimMode != 2))
        {
            ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Invalid Pim Mode\n");
            u1ErrorCount++;
            pNvRamData->u4PimMode = ISS_DEFAULT_PIM_MODE;
        }
    }

    /* Read the Bridge Mode */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "BRIDGE_MODE",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Failed to Bridge Mode\n");
        u1ErrorCount++;
        pNvRamData->u4BridgeMode = ISS_DEFAULT_BRIDGE_MODE;
    }
    else
    {
        pNvRamData->u4BridgeMode = (UINT4) ISS_ATOI (au1Buffer);

        /* Bridge Modes
         * VLAN_CUSTOMER_BRIDGE_MODE = 1
         * VLAN_PROVIDER_BRIDGE_MODE = 2
         * VLAN_PROVIDER_EDGE_BRIDGE_MODE = 3
         * VLAN_PROVIDER_CORE_BRIDGE_MODE = 4
         */
        if ((pNvRamData->u4BridgeMode < ISS_CUSTOMER_BRIDGE_MODE) ||
            (pNvRamData->u4BridgeMode > ISS_PBB_BCOMPONENT_BRIDGE_MODE))
        {
            ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Invalid Bridge Mode\n");
            u1ErrorCount++;
            pNvRamData->u4PimMode = ISS_DEFAULT_BRIDGE_MODE;
        }

    }

    /* Read the IGS Forward mode */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE,
                       (UINT1 *) "SNOOP_FORWARD_MODE", au1Buffer)
        == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Failed to read IGS Forward mode\n");
        u1ErrorCount++;
        pNvRamData->u4SnoopFwdMode = ISS_DEFAULT_SNOOP_FWD_MODE;
    }
    else
    {
        pNvRamData->u4SnoopFwdMode = (UINT4) ISS_ATOI (au1Buffer);
        /* IGS Forward Modes
         * ISS_IGS_IP = 1
         * ISS_IGS_MAC = 2
         */
        if ((pNvRamData->u4SnoopFwdMode != 1)
            && (pNvRamData->u4SnoopFwdMode != 2))
        {
            ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Invalid IGS Forward mode\n");
            u1ErrorCount++;
            pNvRamData->u4SnoopFwdMode = ISS_DEFAULT_SNOOP_FWD_MODE;
        }
    }

    /* Read the save flag */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "SAVE_FLAG",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Failed to read SAve Flag \n");
        u1ErrorCount++;
        pNvRamData->i1SaveFlag = ISS_DEFAULT_SAVE_FLAG;
    }
    else
    {
        pNvRamData->i1SaveFlag = (INT1) ISS_ATOI (au1Buffer);
    }
    /* Read the  auto save flag added as part of incremental save release */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "AUTO_SAVE_FLAG",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Failed to read Auto Save Flag \n");
        u1ErrorCount++;
        pNvRamData->i1AutoSaveFlag = ISS_DEFAULT_AUTO_SAVE_FLAG;
    }
    else
    {
        pNvRamData->i1AutoSaveFlag = (INT1) ISS_ATOI (au1Buffer);
    }

    /*reading of auto save flag end */

    /* Read the  Incrementa flag added as part of incremental save release */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "INCR_SAVE_ENABLED",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]: Failed to read Incremental Save Flag \n");
        u1ErrorCount++;
        pNvRamData->i1IncrSaveFlag = ISS_DEFAULT_INCR_SAVE_FLAG;
    }
    else
    {
        pNvRamData->i1IncrSaveFlag = (INT1) ISS_ATOI (au1Buffer);
    }

    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "DEFVAL_SAVE_FLAG",
                       au1Buffer) == NVRAM_FAILURE)
    {
        u1ErrorCount++;
        pNvRamData->i1DefValSaveFlag = ISS_FALSE;
    }
    else
    {
        pNvRamData->i1DefValSaveFlag = (INT1) ISS_ATOI (au1Buffer);

        if ((pNvRamData->i1DefValSaveFlag != ISS_FALSE) &&
            (pNvRamData->i1DefValSaveFlag != ISS_TRUE))
        {
            u1ErrorCount++;
            pNvRamData->i1DefValSaveFlag = ISS_FALSE;
        }

    }

    /*reading of Incrementa flag end */
#ifdef SNMP_3_WANTED
    /* Read the  Rollback   flag added as part of rollback release */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "ROLLBACK_ENABLED",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Failed to read  Rollback Flag\n");
        u1ErrorCount++;
        pNvRamData->i1RollbackFlag = ISS_DEFAULT_ROLLBACK_FLAG;
        issSnmpInitParams (pNvRamData->i1RollbackFlag);
        b1IsRollbackInNvRam = FALSE;
    }
    else
    {
        pNvRamData->i1RollbackFlag = (INT1) ISS_ATOI (au1Buffer);
        if (pNvRamData->i1RollbackFlag != SNMP_ROLLBACK_ENABLED &&
            pNvRamData->i1RollbackFlag != SNMP_ROLLBACK_DISABLED)
            pNvRamData->i1RollbackFlag = ISS_DEFAULT_ROLLBACK_FLAG;
        issSnmpInitParams (pNvRamData->i1RollbackFlag);
        b1IsRollbackInNvRam = TRUE;
    }
#endif
    /*reading of Rollback flag end */
    /* Read the restore flag */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "RES_FLAG",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Failed to read Restore Flag\n");
        u1ErrorCount++;
        pNvRamData->i1ResFlag = ISS_DEFAULT_RESTORE_FLAG;
    }
    else
    {
        pNvRamData->i1ResFlag = (INT1) ISS_ATOI (au1Buffer);
    }

    /* Read the restore option */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "RES_OPTION",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Failed to read Restore Option\n");
        u1ErrorCount++;
        pNvRamData->u4RestoreOption = ISS_CONFIG_NO_RESTORE;
    }
    else
    {
        pNvRamData->u4RestoreOption = (UINT4) ISS_ATOI (au1Buffer);
    }

    pNvRamData->RestoreIpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    pNvRamData->RestoreIpAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    /* Read the restore IP address */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "RES_IP_ADDRESS",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]: Failed to read Restore Ip Address\n");
        u1ErrorCount++;
        u4RestoreIpAddr = OSIX_NTOHL (ISS_DEFAULT_RES_IP_ADDRESS);
        MEMCPY (pNvRamData->RestoreIpAddr.au1Addr,
                &u4RestoreIpAddr, IPVX_IPV4_ADDR_LEN);
    }
    else
    {
        u4RestoreIpAddr = (INET_ADDR (au1Buffer));
        MEMCPY (pNvRamData->RestoreIpAddr.au1Addr,
                &u4RestoreIpAddr, IPVX_IPV4_ADDR_LEN);

        /* Allow IP address which are Class A, B or C */
        if (!(((ISS_IS_ADDR_CLASS_A (u4RestoreIpAddr)) ||
               (ISS_IS_ADDR_CLASS_B (u4RestoreIpAddr)) ||
               (ISS_IS_ADDR_CLASS_C (u4RestoreIpAddr))) &&
              (ISS_IS_ADDR_VALID (pNvRamData->u4LocalIpAddr))))
        {
            ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Invalid Restore Ip Address\n");
            u1ErrorCount++;
            u4RestoreIpAddr = OSIX_NTOHL (ISS_DEFAULT_RES_IP_ADDRESS);
            MEMCPY (pNvRamData->RestoreIpAddr.au1Addr,
                    &u4RestoreIpAddr, IPVX_IPV4_ADDR_LEN);
        }
    }

    /* Read the restore file name */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "RES_FILE_NAME",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]: Failed to read Restore File Name\n");
        u1ErrorCount++;
        if (ISS_STRLEN (IssGetSaveFileName ()) <= ISS_CONFIG_FILE_NAME_LEN)
        {
            pTemp = (UINT1 *) IssGetSaveFileName ();
            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1RestoreFileName, pTemp,
                        STRLEN (pTemp));
            pNvRamData->ai1RestoreFileName[STRLEN (pTemp)] = '\0';
        }
        else
        {
            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1RestoreFileName,
                        ISS_DEFAULT_RES_FILE_NAME,
                        STRLEN (ISS_DEFAULT_RES_FILE_NAME));
            pNvRamData->ai1RestoreFileName[STRLEN (ISS_DEFAULT_RES_FILE_NAME)]
                = '\0';
        }
    }
    else
    {
        if ((STRLEN (au1Buffer) >= RESTORE_FILE_NAME_LEN))
        {
            ISS_TRC (ALL_FAILURE_TRC,
                     "[ERROR]: Restore File name size is too big\n");
            u1ErrorCount++;
            pTemp = (UINT1 *) IssGetSaveFileName ();
            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1RestoreFileName,
                        ISS_DEFAULT_RES_FILE_NAME,
                        STRLEN (ISS_DEFAULT_RES_FILE_NAME));
            pNvRamData->ai1RestoreFileName[STRLEN (pTemp)] = '\0';
        }
        else
        {
            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1RestoreFileName, au1Buffer,
                        STRLEN ((const char *) au1Buffer));
            pNvRamData->ai1RestoreFileName[STRLEN (au1Buffer)] = '\0';
        }
    }
    /* Read the bgp restore file name */
    if (GetNvRamValue
        ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "RES_BGP_CONF_FILE_NAME",
         au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]: Failed to BGP read Restore File Name\n");
        u1ErrorCount++;
        if (ISS_STRLEN (IssGetBgpConfSaveFileName ()) <=
            ISS_CONFIG_FILE_NAME_LEN)
        {
            pTemp = (UINT1 *) IssGetBgpConfSaveFileName ();
            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1BgpConfRestoreFileName, pTemp,
                        STRLEN (pTemp));
            pNvRamData->ai1BgpConfRestoreFileName[STRLEN (pTemp)] = '\0';
        }
        else
        {
            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1BgpConfRestoreFileName,
                        BGP4_CSR_CONFIG_FILE, STRLEN (BGP4_CSR_CONFIG_FILE));
            pNvRamData->ai1BgpConfRestoreFileName[STRLEN (BGP4_CSR_CONFIG_FILE)]
                = '\0';
        }

    }
    else
    {
        if ((STRLEN (au1Buffer) >= RESTORE_FILE_NAME_LEN))
        {
            ISS_TRC (ALL_FAILURE_TRC,
                     "[ERROR]: Restore File name size is too big\n");
            u1ErrorCount++;
            pTemp = (UINT1 *) IssGetBgpConfSaveFileName ();
            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1BgpConfRestoreFileName,
                        BGP4_CSR_CONFIG_FILE, STRLEN (BGP4_CSR_CONFIG_FILE));
            pNvRamData->ai1BgpConfRestoreFileName[STRLEN (pTemp)] = '\0';
        }
        else
        {
            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1BgpConfRestoreFileName,
                        au1Buffer, STRLEN ((const char *) au1Buffer));
            pNvRamData->ai1BgpConfRestoreFileName[STRLEN (au1Buffer)] = '\0';
        }
    }

    /* Read the CSR restore file name */
    if (GetNvRamValue
        ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "CSR_CONFIG_FILE_NAME",
         au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]: Failed to CSR read Restore File Name\n");
        u1ErrorCount++;
        if (ISS_STRLEN (IssGetCsrConfSaveFileName ()) <=
            ISS_CONFIG_FILE_NAME_LEN)
        {
            pTemp = (UINT1 *) IssGetCsrConfSaveFileName ();
            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1CsrConfRestoreFileName, pTemp,
                        STRLEN (pTemp));
            pNvRamData->ai1CsrConfRestoreFileName[STRLEN (pTemp)] = '\0';
        }
        else
        {
            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1CsrConfRestoreFileName,
                        CSR_CONFIG_FILE, STRLEN (CSR_CONFIG_FILE));
            pNvRamData->ai1CsrConfRestoreFileName[STRLEN (CSR_CONFIG_FILE)]
                = '\0';
        }

    }
    else
    {
        if ((STRLEN (au1Buffer) >= RESTORE_FILE_NAME_LEN))
        {
            ISS_TRC (ALL_FAILURE_TRC,
                     "[ERROR]: Restore File name size is too big\n");
            u1ErrorCount++;
            pTemp = (UINT1 *) IssGetCsrConfSaveFileName ();
            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1CsrConfRestoreFileName,
                        CSR_CONFIG_FILE, STRLEN (CSR_CONFIG_FILE));
            pNvRamData->ai1CsrConfRestoreFileName[STRLEN (pTemp)] = '\0';
        }
        else
        {
            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1CsrConfRestoreFileName,
                        au1Buffer, STRLEN ((const char *) au1Buffer));
            pNvRamData->ai1CsrConfRestoreFileName[STRLEN (au1Buffer)] = '\0';
        }
    }

    /* Read the OSPF3 restore file name */
    if (GetNvRamValue
        ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "OSPF3_CSR_CONFIG_FILE_NAME",
         au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]: Failed to read OSPF Restore File Name\n");
        u1ErrorCount++;
        if (ISS_STRLEN (IssGetOspfConfSaveFileName ()) <=
            ISS_CONFIG_FILE_NAME_LEN)
        {
            pTemp = (UINT1 *) IssGetOspfConfSaveFileName ();
            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1OspfConfRestoreFileName, pTemp,
                        STRLEN (pTemp));
            pNvRamData->ai1OspfConfRestoreFileName[STRLEN (pTemp)] = '\0';
        }
        else
        {
            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1OspfConfRestoreFileName,
                        OSPF3_CSR_CONFIG_FILE, STRLEN (OSPF3_CSR_CONFIG_FILE));
            pNvRamData->
                ai1OspfConfRestoreFileName[STRLEN (OSPF3_CSR_CONFIG_FILE)] =
                '\0';
        }

    }
    else
    {
        if ((STRLEN (au1Buffer) >= RESTORE_FILE_NAME_LEN))
        {
            ISS_TRC (ALL_FAILURE_TRC,
                     "[ERROR]: Restore File name size is too big\n");
            u1ErrorCount++;
            pTemp = (UINT1 *) IssGetOspfConfSaveFileName ();
            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1OspfConfRestoreFileName,
                        OSPF3_CSR_CONFIG_FILE, STRLEN (OSPF3_CSR_CONFIG_FILE));
            pNvRamData->ai1OspfConfRestoreFileName[STRLEN (pTemp)] = '\0';
        }
        else
        {
            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1OspfConfRestoreFileName,
                        au1Buffer, STRLEN ((const char *) au1Buffer));
            pNvRamData->ai1OspfConfRestoreFileName[STRLEN (au1Buffer)] = '\0';
        }
    }

    if ((pNvRamData->u4RestoreOption == ISS_CONFIG_RESTORE) &&
        (pNvRamData->i1ResFlag == ISS_CONFIG_RESTORE))
    {

        /* Check for the config file in Extern storage */
        if (gMsrSavResPriority == EXTERN_STORAGE)
        {
            MEMSET (au1RestoreFileName, 0, sizeof (au1RestoreFileName));
            SPRINTF (au1RestoreFileName, "%s%s", MSR_SYS_EXTN_STRG_PATH,
                     pNvRamData->ai1RestoreFileName);
            i4Fd = FileOpen ((CONST UINT1 *) au1RestoreFileName, OSIX_FILE_RO);
            if (i4Fd < 0)
            {
                MEMSET (au1RestoreFileName, 0, sizeof (au1RestoreFileName));
                SPRINTF (au1RestoreFileName, "%s%s", MSR_SYS_EXTN_STRG_PATH,
                         ISS_CONFIG_FILE2);
                i4Fd = FileOpen ((CONST UINT1 *) au1RestoreFileName,
                                 OSIX_FILE_RO);
                if (i4Fd < 0)
                {
                    /*Check for the config file of the flash
                     * when the one in extern storage is not present*/
                    MEMSET (au1RestoreFileName, 0, sizeof (au1RestoreFileName));
                    SPRINTF (au1RestoreFileName, "%s%s", FLASH,
                             pNvRamData->ai1RestoreFileName);

                    i4Fd = FileOpen ((CONST UINT1 *) au1RestoreFileName,
                                     OSIX_FILE_RO);
                    if (i4Fd < 0)
                    {
                        MEMSET (au1RestoreFileName, 0,
                                sizeof (au1RestoreFileName));
                        SPRINTF (au1RestoreFileName, "%s%s", FLASH,
                                 ISS_CONFIG_FILE2);
                        i4Fd = FileOpen ((CONST UINT1 *) au1RestoreFileName,
                                         OSIX_FILE_RO);
                    }
                }
            }
        }

        else
        {
            MEMSET (au1RestoreFileName, 0, sizeof (au1RestoreFileName));
            SPRINTF (au1RestoreFileName, "%s%s", FLASH,
                     pNvRamData->ai1RestoreFileName);
            i4Fd = FileOpen ((CONST UINT1 *) au1RestoreFileName, OSIX_FILE_RO);

            if (i4Fd < 0)
            {
                MEMSET (au1RestoreFileName, 0, sizeof (au1RestoreFileName));
                SPRINTF (au1RestoreFileName, "%s%s", FLASH, ISS_CONFIG_FILE2);
                i4Fd = FileOpen ((CONST UINT1 *) au1RestoreFileName,
                                 OSIX_FILE_RO);
            }
        }

        if (i4Fd < 0)
        {
            pNvRamData->u4RestoreOption = ISS_CONFIG_NO_RESTORE;
            pNvRamData->i1ResFlag = ISS_DEFAULT_RESTORE_FLAG;
            pNvRamData->i1SaveFlag = ISS_DEFAULT_SAVE_FLAG;
            u1ErrorCount++;
        }
        else
        {
            FileClose (i4Fd);
        }

        ISS_MEMCPY ((UINT1 *) pNvRamData->au1FlashLoggingFilePath,
                    ISS_DEFAULT_DEBUG_LOG_FILE,
                    STRLEN (ISS_DEFAULT_DEBUG_LOG_FILE));
        pNvRamData->
            au1FlashLoggingFilePath[STRLEN (ISS_DEFAULT_DEBUG_LOG_FILE)] = '\0';

    }
    /* Read the incr file name */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "INC_FILE_NAME",
                       au1Buffer) == NVRAM_FAILURE)
    {
        /* incr file not present in the issnvram.txt
         * this file name taken as default in this release */
        ISS_MEMCPY ((UINT1 *) pNvRamData->ai1IncrSaveFileName,
                    ISS_DEFAULT_INCR_SAVE_FILE_NAME,
                    STRLEN (ISS_DEFAULT_INCR_SAVE_FILE_NAME));
        pNvRamData->
            ai1IncrSaveFileName[STRLEN (ISS_DEFAULT_INCR_SAVE_FILE_NAME)] =
            '\0';
    }
    else
    {
        if ((STRLEN (au1Buffer) >= ISS_CONFIG_FILE_NAME_LEN))
        {
            ISS_TRC (ALL_FAILURE_TRC,
                     "[ERROR]: Incremental File name size is too big\n");

            u1ErrorCount++;
            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1IncrSaveFileName,
                        ISS_DEFAULT_INCR_SAVE_FILE_NAME,
                        STRLEN (ISS_DEFAULT_INCR_SAVE_FILE_NAME));
            pNvRamData->
                ai1IncrSaveFileName[STRLEN (ISS_DEFAULT_INCR_SAVE_FILE_NAME)] =
                '\0';
        }
        else
        {
            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1IncrSaveFileName, au1Buffer,
                        STRLEN ((const char *) au1Buffer));
            pNvRamData->ai1IncrSaveFileName[STRLEN (au1Buffer)] = '\0';
        }
    }

    /* Read the CLI serial console presence option */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "CONSOLE_CLI",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]:  Failed to read CLI Serial Console Option\n");

        u1ErrorCount++;
        pNvRamData->u4CliSerialConsole = ISS_DEFAULT_CLI_SERIAL_CONSOLE;
    }
    else
    {
        /* CLI console prompt required in serial console */
        pNvRamData->u4CliSerialConsole = (UINT4) ISS_ATOI (au1Buffer);
    }

    /* Read the switch mac address */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "SWITCH_MAC",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]:  Failed to read Switch Mac Address\n");
        u1ErrorCount++;
        ISS_MEMCPY (pNvRamData->au1SwitchMac, DefaultSwitchMac, 6);
    }
    else
    {
        ISS_MEMSET (au1TempMac, 0, 18);
        ISS_MEMSET (pNvRamData->au1SwitchMac, 0, 6);
        ISS_MEMCPY (au1TempMac, au1Buffer, 17);
        au1TempMac[17] = '\0';

        if ((pTemp = (UINT1 *) STRSTR (au1Buffer, ".")) != NULL)
        {
            UNUSED_PARAM (pTemp);
            ISS_TRC (ALL_FAILURE_TRC,
                     "[ERROR]:  Failed to read Switch Mac Address\n");
            u1ErrorCount++;
            ISS_MEMCPY (pNvRamData->au1SwitchMac, DefaultSwitchMac, 6);
        }
        else
        {
            StrToMac (au1TempMac, pNvRamData->au1SwitchMac);
        }
    }

    /* Read the DEBUG_LOG_FILE_LOCATION */
    if (GetNvRamValue
        ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "DEBUG_LOG_FILE_LOCATION",
         au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]:  Failed to read Snmp Engine ID\n");
        u1ErrorCount++;
        ISS_MEMCPY ((UINT1 *) pNvRamData->au1FlashLoggingFilePath,
                    ISS_DEFAULT_DEBUG_LOG_FILE,
                    STRLEN (ISS_DEFAULT_DEBUG_LOG_FILE));
    }
    else
    {
        ISS_MEMCPY ((UINT1 *) pNvRamData->au1FlashLoggingFilePath, au1Buffer,
                    STRLEN ((const char *) au1Buffer));
    }

    /* Read the IMG_DUMP_PATH */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "IMG_DUMP_PATH",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]:  Failed to read Snmp Engine ID\n");
        u1ErrorCount++;
        ISS_MEMCPY ((UINT1 *) pNvRamData->au1ImageDumpFilePath,
                    ISS_DEFAULT_IMG_DUMP_PATH,
                    STRLEN (ISS_DEFAULT_IMG_DUMP_PATH));
    }
    else
    {
        ISS_MEMCPY ((UINT1 *) pNvRamData->au1ImageDumpFilePath, au1Buffer,
                    STRLEN ((const char *) au1Buffer));
    }

    /* Read the SNMP Engine ID */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "SNMP_ENGINE_ID",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]:  Failed to read Snmp Engine ID\n");
        u1ErrorCount++;
        ISS_MEMCPY (pNvRamData->ai1SnmpEngineID, gi1SnmpEngineID,
                    STRLEN (gi1SnmpEngineID));
    }
    else
    {
        if ((STRLEN (au1Buffer) >= SNMP_MAX_STR_ENGINEID_LEN))
        {
            ISS_TRC (ALL_FAILURE_TRC,
                     "[ERROR]:  Failed to read Snmp Engine ID\n");
            u1ErrorCount++;
            ISS_MEMCPY (pNvRamData->ai1SnmpEngineID, gi1SnmpEngineID,
                        STRLEN (gi1SnmpEngineID));
            pNvRamData->ai1SnmpEngineID[STRLEN (gi1SnmpEngineID)] = '\0';
        }
        else
        {
            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1SnmpEngineID, au1Buffer,
                        (sizeof (pNvRamData->ai1SnmpEngineID) - 1));
            pNvRamData->ai1SnmpEngineID[STRLEN (au1Buffer)] = '\0';
        }
    }

    /* Read the Default Vlan Id */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "DEFAULT_VLAN",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]:  Failed to get the default vlan\n");
        u1ErrorCount++;
        pNvRamData->u2DefaultVlan = ISS_DEFAULT_VLAN_ID;
    }
    else
    {
        pNvRamData->u2DefaultVlan = (UINT2) ISS_ATOI (au1Buffer);

        if ((pNvRamData->u2DefaultVlan < ISS_MIN_VLAN_ID) ||
            (pNvRamData->u2DefaultVlan > ISS_MAX_VLAN_ID))
        {
            ISS_TRC (ALL_FAILURE_TRC,
                     "[ERROR]:  Default vlan Id not in the allowed range\n");

            u1ErrorCount++;
            pNvRamData->u2DefaultVlan = ISS_DEFAULT_VLAN_ID;
        }
    }

    /* Read the NPAPI Mode of Processing */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "NPAPI_MODE",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Failed to get the NPAPI MODE\n");
        u1ErrorCount++;
        ISS_MEMCPY ((UINT1 *) pNvRamData->au1NpapiMode,
                    ISS_DEFAULT_NPAPI_MODE, STRLEN (ISS_DEFAULT_NPAPI_MODE));
        pNvRamData->au1NpapiMode[STRLEN (ISS_DEFAULT_NPAPI_MODE)] = '\0';
    }
    else
    {
        if ((STRCMP (au1Buffer, "Asynchronous") != 0) &&
            (STRCMP (au1Buffer, "Synchronous") != 0))
        {
            ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: NPAPI Mode Incorrect");
            u1ErrorCount++;
            ISS_MEMCPY ((UINT1 *) pNvRamData->au1NpapiMode,
                        ISS_DEFAULT_NPAPI_MODE,
                        STRLEN (ISS_DEFAULT_NPAPI_MODE));
            pNvRamData->au1NpapiMode[STRLEN (ISS_DEFAULT_NPAPI_MODE)] = '\0';
        }
        else
        {
            ISS_MEMCPY ((UINT1 *) pNvRamData->au1NpapiMode,
                        au1Buffer, (sizeof (pNvRamData->au1NpapiMode) - 1));
            pNvRamData->au1NpapiMode[(sizeof (pNvRamData->au1NpapiMode) - 1)] =
                '\0';
        }
    }
    /* Read the Number of Normal (Front pannel) ports */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "DYN_PORT_COUNT",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]: Failed to get the Front Panel Port count value\n");
        u1ErrorCount++;
        pNvRamData->u4FrontPanelPortCount = ISS_DEFAULT_FRONT_PANEL_PORT_COUNT;
    }
    else
    {
        pNvRamData->u4FrontPanelPortCount = (UINT4) ISS_ATOI (au1Buffer);

        if ((pNvRamData->u4FrontPanelPortCount <= 0) ||
            (pNvRamData->u4FrontPanelPortCount >
             SYS_DEF_MAX_PHYSICAL_INTERFACES))
        {

            ISS_TRC (ALL_FAILURE_TRC,
                     "[ERROR]: Port count exceeds System defined MAX interfaces\n");
            u1ErrorCount++;
            pNvRamData->u4FrontPanelPortCount =
                ISS_DEFAULT_FRONT_PANEL_PORT_COUNT;
        }
    }
#if defined (VRF_WANTED)
    /* Read the VRF_UNIQUE_MAC  */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "VRF_UNIQUE_MAC",
                       au1Buffer) == NVRAM_FAILURE)
    {

        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]:Failed to get vrf unique mac option \n");
        u1ErrorCount++;
        pNvRamData->bVrfUnqMacOption = ISS_VRF_UNQ_MAC_DISABLE;
    }
    else
    {
        pNvRamData->bVrfUnqMacOption = (UINT1) ISS_ATOI (au1Buffer);

        if ((pNvRamData->bVrfUnqMacOption != ISS_VRF_UNQ_MAC_DISABLE) &&
            (pNvRamData->bVrfUnqMacOption != ISS_VRF_UNQ_MAC_ENABLE))
        {

            ISS_TRC (ALL_FAILURE_TRC,
                     "[ERROR]:Invalid vrf unique mac option \n");
            u1ErrorCount++;
            pNvRamData->bVrfUnqMacOption = ISS_VRF_UNQ_MAC_DISABLE;
        }
    }
#else
    /* VRF_UNQ_MAC option can be enabled only in VRF enabled case */
    pNvRamData->bVrfUnqMacOption = ISS_VRF_UNQ_MAC_DISABLE;
#endif
    /* Read the Time Stamping Method */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE,
                       (UINT1 *) "TIMESTAMP_METHOD", au1Buffer)
        == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]:  Failed to get the TIMESTAMP_METHOD\n");
        u1ErrorCount++;
        pNvRamData->u2TimeStampMethod = ISS_DEFAULT_TIME_STAMP_METHOD;
    }
    else
    {
        if (STRCMP (au1Buffer, "Software") == 0)
        {
            /* Software Time Stamping method */
            pNvRamData->u2TimeStampMethod = ISS_SOFTWARE_TIME_STAMP;
        }
        else if (STRCMP (au1Buffer, "TransHardware") == 0)
        {
            /* Hardware Time Stamping method */
            pNvRamData->u2TimeStampMethod = ISS_HARDWARE_TRANS_TIME_STAMP;
        }
        else if (STRCMP (au1Buffer, "Hardware") == 0)
        {
            /* Hardware Time Stamping method */
            pNvRamData->u2TimeStampMethod = ISS_HARDWARE_TIME_STAMP;
        }
        else
        {
            /* Method is neither software not hardware */
            ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Time Stamp Method incorrect\n");
            u1ErrorCount++;
            pNvRamData->u2TimeStampMethod = ISS_DEFAULT_TIME_STAMP_METHOD;
        }
    }

    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE,
                       (UINT1 *) "FIPS_OPER_MODE", au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Failed to read FIPS_OPER_MODE\n");
        u1ErrorCount++;
        pNvRamData->i4FipsOperMode = ISS_DEFAULT_FIPS_OPER_MODE;
    }
    else
    {
        pNvRamData->i4FipsOperMode = ISS_ATOI (au1Buffer);
    }

    /* Read the SNMP Engine Boot Count */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE,
                       (UINT1 *) "SNMP_ENGINE_BOOTS", au1Buffer)
        == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]: Failed to read SNMP_ENGINE_BOOTS\n");
        u1ErrorCount++;
        pNvRamData->u4SnmpEngineBoots = gu4SnmpEngineBoots;
    }
    else
    {
        pNvRamData->u4SnmpEngineBoots = (UINT4) ISS_ATOI (au1Buffer);
        u1RetStatus = MsrRMGetNodeStatus ();
        if (u1RetStatus != RM_STANDBY)
        {
            pNvRamData->u4SnmpEngineBoots++;
        }
    }

/* HITLESS RESTART */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "HITLESS_RESTART",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]: Failed to read Hitless restart flag\n");
        u1ErrorCount++;
        pNvRamData->i1RmHRFlag = ISS_DEFAULT_HR_FLAG;
    }
    else
    {
        pNvRamData->i1RmHRFlag = (INT1) ISS_ATOI (au1Buffer);
        /* Hitless Restart Feature
         * Hiltess Restart Disabled = 0
         * Hiltess Restart Enabled = 1
         */
        if ((pNvRamData->i1RmHRFlag != 0) && (pNvRamData->i1RmHRFlag != 1))
        {
            ISS_TRC (ALL_FAILURE_TRC,
                     "[ERROR]: Invalid Hitless restart flag\r\n");
            u1ErrorCount++;
            pNvRamData->i1RmHRFlag = ISS_DEFAULT_HR_FLAG;
        }
    }
  
    /* Do not read the version numbers from the nvram.txt file as the
     * versions present in the software will be more appropriate than
     * the one present in the nvram.txt file
     */    
    ISS_MEMCPY ((UINT1 *) pNvRamData->ai1HardwareVersion,
            ISS_HARDWARE_VARSION, STRLEN (ISS_HARDWARE_VARSION));
    pNvRamData->ai1HardwareVersion[STRLEN (ISS_HARDWARE_VARSION)] = '\0';

    ISS_MEMCPY ((UINT1 *) pNvRamData->ai1FirmwareVersion,
            ISS_FIRMWARE_VERSION, STRLEN (ISS_FIRMWARE_VERSION));
    pNvRamData->ai1FirmwareVersion[STRLEN (ISS_FIRMWARE_VERSION)] = '\0';

    ISS_MEMCPY ((UINT1 *) pNvRamData->ai1HardwarePartNumber,
            ISS_HARDWARE_PART_NUMBER,
            STRLEN (ISS_HARDWARE_PART_NUMBER));
    pNvRamData->ai1HardwarePartNumber[STRLEN (ISS_HARDWARE_PART_NUMBER)] =
        '\0';

    ISS_MEMCPY ((UINT1 *) pNvRamData->ai1SoftwareVersion,
            ISS_SOFTWARE_VERSION, STRLEN (ISS_SOFTWARE_VERSION));
    pNvRamData->ai1SoftwareVersion[STRLEN (ISS_SOFTWARE_VERSION)] = '\0';
 
    ISS_MEMCPY ((UINT1 *) pNvRamData->ai1SoftwareSerialNumber,
            ISS_SOFTWARE_SERIAL_NUMBER,
            STRLEN (ISS_SOFTWARE_SERIAL_NUMBER));
    pNvRamData->
        ai1SoftwareSerialNumber[STRLEN (ISS_SOFTWARE_SERIAL_NUMBER)] = '\0';

   if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE,
                       (UINT1 *) "SWITCH_NAME", au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]: Failed to get the Switch Name \r\n");
        u1ErrorCount++;
        ISS_MEMCPY ((UINT1 *) pNvRamData->ai1SwitchName,
                    ISS_SWITCH_NAME, STRLEN (ISS_SWITCH_NAME));
        pNvRamData->ai1SwitchName[STRLEN (ISS_SWITCH_NAME)] = '\0';

    }
    else
    {
        if ((STRLEN (au1Buffer) >= ISS_STR_LEN))
        {
            ISS_TRC (ALL_FAILURE_TRC,
                     "[ERROR]: Switch Name size is too big\r\n");
            u1ErrorCount++;
            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1SwitchName,
                        ISS_SWITCH_NAME, STRLEN (ISS_SWITCH_NAME));
            pNvRamData->ai1SwitchName[STRLEN (ISS_SWITCH_NAME)] = '\0';
        }
        else
        {

            ISS_MEMCPY ((UINT1 *) pNvRamData->ai1SwitchName, au1Buffer,
                        (sizeof (pNvRamData->ai1SwitchName) - 1));
            pNvRamData->
                ai1SwitchName[(sizeof (pNvRamData->ai1SwitchName) - 1)] = '\0';
        }
    }

    /* Read the RM Heart Beat Mode */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "RM_HB_MODE",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]:Failed to read Heart Beat Mode\n");
        u1ErrorCount++;
        pNvRamData->u4RmHbMode = ISS_RM_HB_MODE_INTERNAL;
    }
    else
    {
        pNvRamData->u4RmHbMode = (UINT4) ISS_ATOI (au1Buffer);

        if ((pNvRamData->u4RmHbMode != ISS_RM_HB_MODE_INTERNAL) &&
            (pNvRamData->u4RmHbMode != ISS_RM_HB_MODE_EXTERNAL))
        {
            pNvRamData->u4RmHbMode = ISS_RM_HB_MODE_INTERNAL;
        }
    }

    /* Read the RM Redundancy Type */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "RM_RTYPE",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]:Failed to read Heart Beat Mode\n");
        u1ErrorCount++;
        pNvRamData->u4RmRType = ISS_RM_RTYPE_HOT;
    }
    else
    {
        pNvRamData->u4RmRType = (UINT4) ISS_ATOI (au1Buffer);

        if ((pNvRamData->u4RmRType != ISS_RM_RTYPE_HOT) &&
            (pNvRamData->u4RmRType != ISS_RM_RTYPE_COLD))
        {
            pNvRamData->u4RmRType = ISS_RM_RTYPE_HOT;
        }
    }

    /* Read the RM Data Plane Type */
    if (GetNvRamValue ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "RM_DTYPE",
                       au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]:Failed to read Heart Beat Mode\n");
        u1ErrorCount++;
        pNvRamData->u4RmDType = ISS_RM_DTYPE_SHARED;
    }
    else
    {
        pNvRamData->u4RmDType = (UINT4) ISS_ATOI (au1Buffer);

        if ((pNvRamData->u4RmDType != ISS_RM_DTYPE_SHARED) &&
            (pNvRamData->u4RmDType != ISS_RM_DTYPE_SEPARATE))
        {
            pNvRamData->u4RmDType = ISS_RM_DTYPE_SHARED;
        }
    }

    /* Read the  automatic port create flag */
    if (GetNvRamValue
        ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "AUTOMATIC_PORT_CREATE",
         au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]: Failed to read Automatic Port Create Flag \n");
        u1ErrorCount++;
        pNvRamData->i1AutomaticPortCreate = ISS_DEFAULT_AUTO_PORT_CREATE;
    }
    else
    {
        pNvRamData->i1AutomaticPortCreate = (INT1) ISS_ATOI (au1Buffer);
    }

    /* Read the RM Type Flag */
    if (GetNvRamValue
        ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "RM_TYPE",
         au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "[ERROR]: Failed to read RM Type Flag \n");
        u1ErrorCount++;
        pNvRamData->u4RmStackingInterfaceType = ISS_RM_STACK_INTERFACE_TYPE_OOB;
    }
    else
    {
        pNvRamData->u4RmStackingInterfaceType = (UINT4) ISS_ATOI (au1Buffer);
        if ((pNvRamData->u4RmStackingInterfaceType !=
             ISS_RM_STACK_INTERFACE_TYPE_OOB)
            && (pNvRamData->u4RmStackingInterfaceType !=
                ISS_RM_STACK_INTERFACE_TYPE_INBAND))
        {
            pNvRamData->u4RmStackingInterfaceType =
                ISS_RM_STACK_INTERFACE_TYPE_OOB;
        }
    }
    /*reading of auto save flag end */

    /* Read the Diss Role Played */
    if (GetNvRamValue
        ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "DISS_ROLE_PLAYED",
         au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]: Failed to read DISS role played Flag \n");
        u1ErrorCount++;
        pNvRamData->u1DissRolePlayed = ISS_DISS_ROLE_NONE;
    }
    else
    {
        pNvRamData->u1DissRolePlayed = (UINT1) ISS_ATOI (au1Buffer);
    }

    /* Read the restoration type */
    if (GetNvRamValue
        ((UINT1 *) ISS_NVRAM_FILE, (UINT1 *) "RESTORE_TYPE",
         au1Buffer) == NVRAM_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "[ERROR]: Failed to read Restore Type \n");
        u1ErrorCount++;
        pNvRamData->u1RestoreType = ISS_DEFAULT_RESTORE_TYPE;
    }
    else
    {
        pNvRamData->u1RestoreType = (UINT1) ISS_ATOI (au1Buffer);
    }


    if (ISS_NVRAM_CALLBACK[ISS_NVRAM_CUST_READ_EVENT].pIssCustReadNvram != NULL)
    {
        IssCustNvramCallBack (ISS_NVRAM_CUST_READ_EVENT, 0);
    }

    IssSetSnmpEngineBootsToNvRam (pNvRamData->u4SnmpEngineBoots);

    if (u1ErrorCount > 0)
    {
        return FNP_FAILURE;
    }
    else
    {
        return FNP_SUCCESS;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : NvRamWrite                                       */
/*                                                                          */
/*    Description        : This function is a portable routine, to       */
/*                         write the critical configuration for the switch  */
/*                         to the NvRam.                                    */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : FNP_SUCCESS/FNP_FAILURE                          */
/*                                                                          */
/*    Note               : The return type should be as mentioned above for */
/*                         ported routines also.                            */
/****************************************************************************/
int
NvRamWrite (tNVRAM_DATA * pNvRamData)
{
    INT4                i4Fd = 0;
    tUtlInAddr          IpAddr;
    tUtlInAddr          RestoreIpAddr;
    UINT1               u1MacLen;
    CHR1                au1Buf[ISS_NVRAM_MAX_LEN];

    MEMSET (&RestoreIpAddr, 0, sizeof (tUtlInAddr));

    MEMSET (au1Buf, 0, sizeof (au1Buf));
    i4Fd = FileOpen ((CONST UINT1 *) ISS_NVRAM_FILE,
                     OSIX_FILE_WO | OSIX_FILE_TR | OSIX_FILE_CR);
    if (i4Fd < 0)
    {
        return FNP_FAILURE;
    }
    SPRINTF (au1Buf, "IP_CONFIG_MODE    =%d\n", pNvRamData->u4CfgMode);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));

    SPRINTF (au1Buf, "IP_ADDR_ALLOC_PROTO =%d\n",
             pNvRamData->u4IpAddrAllocProto);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));

    IpAddr.u4Addr = OSIX_NTOHL (pNvRamData->u4LocalIpAddr);
    SPRINTF (au1Buf, "IP_ADDRESS        =%s\n", UtlInetNtoa (IpAddr));
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));

    IpAddr.u4Addr = OSIX_NTOHL (pNvRamData->u4LocalSubnetMask);
    SPRINTF (au1Buf, "IP_MASK           =%s\n", UtlInetNtoa (IpAddr));
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));

     SPRINTF (au1Buf, "IPV6_ADDRESS      =%s\n",
             Ip6PrintNtop (&pNvRamData->localIp6Addr));
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));

    SPRINTF (au1Buf, "IPV6_PREFIX_LEN   =%d\n", pNvRamData->u1PrefixLen);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));

    SPRINTF (au1Buf, "INTERFACE         =%s\n", pNvRamData->ai1InterfaceName);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));

    if (pNvRamData->u4MgmtPort == TRUE)
    {
        SPRINTF (au1Buf, "MGMT_PORT         =%s\n", "YES");
        FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    }
    else
    {
        SPRINTF (au1Buf, "MGMT_PORT         =%s\n", "NO");
        FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    }

    SPRINTF (au1Buf, "STACK_PORT_COUNT =%d\n", pNvRamData->u2StackPortCount);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "STACK_COLD_STDBY =%d\n", pNvRamData->u4ColdStandby);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "RM_INTERFACE      =%s\n", pNvRamData->ai1RmIfName);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "PIM_MODE          =%d\n", pNvRamData->u4PimMode);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "BRIDGE_MODE          =%d\n", pNvRamData->u4BridgeMode);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "SNOOP_FORWARD_MODE  =%d\n", pNvRamData->u4SnoopFwdMode);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "SAVE_FLAG         =%d\n", pNvRamData->i1SaveFlag);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    /*code added for auto save flag as part of incremental save */
    SPRINTF (au1Buf, "AUTO_SAVE_FLAG    =%d\n", pNvRamData->i1AutoSaveFlag);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    /*change end */
    /*code added for Incremental save flag as part of incremental save */
    SPRINTF (au1Buf, "INCR_SAVE_ENABLED    =%d\n", pNvRamData->i1IncrSaveFlag);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    /*change end */
    SPRINTF (au1Buf, "ROLLBACK_ENABLED  =%d\n", pNvRamData->i1RollbackFlag);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    /*change end */

    SPRINTF (au1Buf, "RES_FLAG          =%d\n", pNvRamData->i1ResFlag);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "RES_OPTION        =%d\n", pNvRamData->u4RestoreOption);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    PTR_FETCH4 (RestoreIpAddr.u4Addr, pNvRamData->RestoreIpAddr.au1Addr);
    RestoreIpAddr.u4Addr = OSIX_NTOHL (RestoreIpAddr.u4Addr);
    SPRINTF (au1Buf, "RES_IP_ADDRESS    =%s\n", UtlInetNtoa (RestoreIpAddr));
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "RES_FILE_NAME     =%s\n", pNvRamData->ai1RestoreFileName);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "RES_BGP_CONF_FILE_NAME     =%s\n",
             pNvRamData->ai1BgpConfRestoreFileName);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "CSR_CONFIG_FILE_NAME     =%s\n",
             pNvRamData->ai1CsrConfRestoreFileName);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "OSPF3_CSR_CONFIG_FILE_NAME     =%s\n",
             pNvRamData->ai1OspfConfRestoreFileName);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "CONSOLE_CLI       =%d\n", pNvRamData->u4CliSerialConsole);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "SWITCH_MAC        =");
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    for (u1MacLen = 0; u1MacLen < ISS_MAC_LEN; u1MacLen++)
    {
        SPRINTF (au1Buf, "%02x", pNvRamData->au1SwitchMac[u1MacLen]);
        FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
        if (u1MacLen < (ISS_MAC_LEN - 1))
        {
            SPRINTF (au1Buf, ":");
            FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
        }
    }
    SPRINTF (au1Buf, "\n");
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "SNMP_ENGINE_ID    =%s\n", pNvRamData->ai1SnmpEngineID);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    if (pNvRamData->u2TimeStampMethod == ISS_SOFTWARE_TIME_STAMP)
    {
        SPRINTF (au1Buf, "TIMESTAMP_METHOD =Software\n");
        FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    }
    else if (pNvRamData->u2TimeStampMethod == ISS_HARDWARE_TIME_STAMP)
    {
        SPRINTF (au1Buf, "TIMESTAMP_METHOD =Hardware\n");
        FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    }
    else
    {
        SPRINTF (au1Buf, "TIMESTAMP_METHOD =TransHardware\n");
        FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    }
    SPRINTF (au1Buf, "SNMP_ENGINE_BOOTS =%d\n", pNvRamData->u4SnmpEngineBoots);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "DEFAULT_VLAN =%d\n", pNvRamData->u2DefaultVlan);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "NPAPI_MODE =%s\n", pNvRamData->au1NpapiMode);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "DYN_PORT_COUNT =%d\n", pNvRamData->u4FrontPanelPortCount);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "VRF_UNIQUE_MAC  =%d\n", pNvRamData->bVrfUnqMacOption);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "DEFVAL_SAVE_FLAG  =%d\n", pNvRamData->i1DefValSaveFlag);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "HARDWARE_VERSION  =%s\n", pNvRamData->ai1HardwareVersion);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "HARDWARE_PART_NUMBER  =%s\n",
             pNvRamData->ai1HardwarePartNumber);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "FIRMWARE_VERSION  =%s\n", pNvRamData->ai1FirmwareVersion);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "SOFTWARE_VERSION  =%s\n", pNvRamData->ai1SoftwareVersion);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "SOFTWARE_SERIAL_NUMBER  =%s\n",
             pNvRamData->ai1SoftwareSerialNumber);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "SWITCH_NAME  =%s\n", pNvRamData->ai1SwitchName);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    SPRINTF (au1Buf, "FIPS_OPER_MODE =%d\n", pNvRamData->i4FipsOperMode);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    /* HITLESS RESTART */
    SPRINTF (au1Buf, "HITLESS_RESTART =%d\n", pNvRamData->i1RmHRFlag);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    /* RM Heart Beat Mode */
    SPRINTF (au1Buf, "RM_HB_MODE =%d\n", pNvRamData->u4RmHbMode);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    /* RM Redundancy Type */
    SPRINTF (au1Buf, "RM_RTYPE =%d\n", pNvRamData->u4RmRType);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    /* RM Data Plane Type */
    SPRINTF (au1Buf, "RM_DTYPE =%d\n", pNvRamData->u4RmDType);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    /*Automatic Port Create Feature */
    SPRINTF (au1Buf, "AUTOMATIC_PORT_CREATE =%d\n",
             pNvRamData->i1AutomaticPortCreate);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));
    /* RM Type */
    SPRINTF (au1Buf, "RM_TYPE  =%d\n", pNvRamData->u4RmStackingInterfaceType);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));

    SPRINTF (au1Buf, "DEBUG_LOG_FILE_LOCATION =%s\n",
             pNvRamData->au1FlashLoggingFilePath);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));

    SPRINTF (au1Buf, "IMG_DUMP_PATH =%s\n", pNvRamData->au1ImageDumpFilePath);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));

    /*DISS Role Played */ 
    SPRINTF (au1Buf, "DISS_ROLE_PLAYED =%d\n",
                     pNvRamData->u1DissRolePlayed);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));

    /* Restoration Type */
    SPRINTF (au1Buf, "RESTORE_TYPE =%d\n",
                     pNvRamData->u1RestoreType);
    FileWrite (i4Fd, au1Buf, STRLEN (au1Buf));

    FileClose (i4Fd);

    if (ISS_NVRAM_CALLBACK[ISS_NVRAM_CUST_WRITE_EVENT].pIssCustWriteNvram !=
        NULL)
    {
        IssCustNvramCallBack (ISS_NVRAM_CUST_WRITE_EVENT, 0);
    }

#if defined(SWC)
    system ("etc flush");
#endif
    return FNP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : NvRamReadAggregatorMac                           */
/*                                                                          */
/*    Description        : This is a portable routine                       */
/*                         --> To derive the Mac address for each aggregator*/
/*                             in the switch based on the 'base mac address'*/
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
NvRamReadAggregatorMac (VOID)
{
    UINT4               u4MacIndex;
    UINT4               u4AggIndex = SYS_DEF_MAX_PHYSICAL_INTERFACES;
    tMacAddr            au1BaseMac;
    UINT1               u1OldMac;

    ISS_MEMSET (au1BaseMac, 0, CFA_ENET_ADDR_LEN);

    CfaGetSysMacAddress (au1BaseMac);

    for (u4MacIndex = 0; u4MacIndex < LA_DEV_MAX_TRUNK_GROUP;
         u4MacIndex++, u4AggIndex++)
    {
        ISS_MEMCPY (sNvRamAggMac[u4MacIndex].au1AggregatorMac, au1BaseMac,
                    CFA_ENET_ADDR_LEN);

        u1OldMac = sNvRamAggMac[u4MacIndex].au1AggregatorMac[5];
        sNvRamAggMac[u4MacIndex].au1AggregatorMac[5] =
            (UINT1) (sNvRamAggMac[u4MacIndex].au1AggregatorMac[5] + u4AggIndex);

        /* If 6th octet New value is less than old value, the other octets
         * needs to be updated.
         */
        if (sNvRamAggMac[u4MacIndex].au1AggregatorMac[5] < u1OldMac)
        {
            sNvRamAggMac[u4MacIndex].au1AggregatorMac[4] =
                (UINT1) (sNvRamAggMac[u4MacIndex].au1AggregatorMac[4] + 1);
            if (sNvRamAggMac[u4MacIndex].au1AggregatorMac[4] == 0x00)
            {
                sNvRamAggMac[u4MacIndex].au1AggregatorMac[3] =
                    (UINT1) (sNvRamAggMac[u4MacIndex].au1AggregatorMac[3] + 1);
                if (sNvRamAggMac[u4MacIndex].au1AggregatorMac[3] == 0x00)
                {
                    sNvRamAggMac[u4MacIndex].au1AggregatorMac[2] =
                        (UINT1) (sNvRamAggMac[u4MacIndex].au1AggregatorMac[2] +
                                 1);
                    if (sNvRamAggMac[u4MacIndex].au1AggregatorMac[2] == 0x00)
                    {
                        sNvRamAggMac[u4MacIndex].au1AggregatorMac[1] =
                            (UINT1) (sNvRamAggMac[u4MacIndex].
                                     au1AggregatorMac[1] + 1);
                    }
                }
            }
        }

        /* If AggIndex exceeds 256, sNvRamAggMac[u4MacIndex].au1AggregatorMac[5]
         * value >= old-value.The other octets will not get updated.So we need
         * to check it.
         */
        if ((u4AggIndex / 256) != 0)
        {
            u1OldMac = sNvRamAggMac[u4MacIndex].au1AggregatorMac[4];

            /* If 5th octet New value is less than old value, the other octets
             * needs to be updated.
             */
            sNvRamAggMac[u4MacIndex].au1AggregatorMac[4] =
                (UINT1) (sNvRamAggMac[u4MacIndex].au1AggregatorMac[4] +
                         (u4AggIndex / 256));

            if (sNvRamAggMac[u4MacIndex].au1AggregatorMac[4] <= u1OldMac)
            {
                sNvRamAggMac[u4MacIndex].au1AggregatorMac[3] =
                    (UINT1) (sNvRamAggMac[u4MacIndex].au1AggregatorMac[3] + 1);
                if (sNvRamAggMac[u4MacIndex].au1AggregatorMac[3] == 0x00)
                {
                    sNvRamAggMac[u4MacIndex].au1AggregatorMac[2] =
                        (UINT1) (sNvRamAggMac[u4MacIndex].au1AggregatorMac[2] +
                                 1);
                    if (sNvRamAggMac[u4MacIndex].au1AggregatorMac[2] == 0x00)
                    {
                        sNvRamAggMac[u4MacIndex].au1AggregatorMac[1] =
                            (UINT1) (sNvRamAggMac[u4MacIndex].
                                     au1AggregatorMac[1] + 1);
                    }
                }

            }
        }

    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssStoreGraceContent                             */
/*                                                                          */
/*    Description        : Helps to store Graceful restart                  */
/*                         information in a file                            */
/*                                                                          */
/*    Input(s)           : buffer - Contains grace info to be saved         */
/*                         protocol - protocol to which info, to be stored   */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssStoreGraceContent (VOID *buffer, UINT4 u4protocol)
{

    switch (u4protocol)
    {
#ifdef BGP_WANTED
        case BGP_ID:
            IssBgpStoreGraceContent (buffer);
            break;
#endif /* BGP_WANTED */

#ifdef ISIS_WANTED
        case ISIS_MODULE:
            IsisStore (buffer);    /*Storing ISIS Grace information */
            break;
#endif
        default:
            UNUSED_PARAM (buffer);
            break;

    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssRestoreGraceContent                           */
/*                                                                          */
/*    Description        : Helps to populate the structure from the file    */
/*                         */
/*                         */
/*                                                                          */
/*    Input(s)           : buffer - to which info needs to be restored from */
/*                                   file                                   */
/*                         protocol - protocol to which info, to be restored*/
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IssRestoreGraceContent (VOID *buffer, UINT4 u4protocol)
{
    switch (u4protocol)
    {
#ifdef BGP_WANTED
        case BGP_ID:
            IssBgpRestoreGraceContent (buffer);
            break;
#endif /* BGP_WANTED */
#ifdef ISIS_WANTED
        case ISIS_MODULE:
            IsisRestore (buffer);
            break;
#endif
        default:
            UNUSED_PARAM (buffer);
            break;
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssRemoveGraceContent                            */
/*                                                                          */
/*    Description        : Removes protocol content from frace info file    */
/*                         */
/*                         */
/*                                                                          */
/*    Input(s)           : protocol info to be removed                      */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/

VOID
IssRemoveGraceContent (UINT4 u4protocol)
{
#ifdef ISIS_WANTED
    FILE               *pConfFilePtr = NULL;    /* File pointer */
#endif

    switch (u4protocol)
    {
#ifdef ISIS_WANTED
        case ISIS_MODULE:
            pConfFilePtr = FOPEN (ISIS_GR_CONF, "w");
            unlink (ISIS_GR_CONF);
            if (pConfFilePtr != NULL)
            {
                fclose (pConfFilePtr);
            }
            break;
#endif
        default:
            UNUSED_PARAM (u4protocol);
            break;
    }
}

#ifdef BGP_WANTED
/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssBgpReStoreGraceContent                        */
/*                                                                          */
/*    Description        : This routine restores the BGP GR related         */
/*                         configurations from file bgp4gr.txt              */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : buffer.(which contains the remaining time and sys*/
/*                         shut time)                                       */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/

VOID
IssBgpRestoreGraceContent (VOID *buffer)
{
    FILE               *pBgpConfFilePtr = NULL;    /* File pointer to bgpgr.conf file */
    UINT1               au1Buf[MAX_COLUMN_LENGTH];    /* Buffer to fetch the file contents */
    UINT1               au1NameStr[MAX_COLUMN_LENGTH];    /* Buffer to fetch the tokens saved in gr.conf */
    UINT1               au1EqualStr[MAX_COLUMN_LENGTH];    /* Buffer to fetch the tokens saved in file */
    UINT1               au1ValueStr[MAX_COLUMN_LENGTH];    /* Buffer to fetch the GR conf values stored */
    tBgpGrSaveConfig   *pGrSaveConfig = NULL;

    pGrSaveConfig = (tBgpGrSaveConfig *) buffer;

    /* Clear memory */
    MEMSET (au1Buf, ZERO, MAX_COLUMN_LENGTH);
    MEMSET (au1NameStr, ZERO, MAX_COLUMN_LENGTH);
    MEMSET (au1EqualStr, ZERO, MAX_COLUMN_LENGTH);
    MEMSET (au1ValueStr, ZERO, MAX_COLUMN_LENGTH);

    pBgpConfFilePtr = FOPEN (BGP4_GR_CONF, "r");
    if (pBgpConfFilePtr == NULL)
    {
        /* File Doesn't exists */
        return;
    }

    while (!feof (pBgpConfFilePtr))
    {
        /* Clear the buffer before processing the next token */
        MEMSET (au1Buf, ZERO, MAX_COLUMN_LENGTH);
        MEMSET (au1NameStr, ZERO, MAX_COLUMN_LENGTH);
        MEMSET (au1EqualStr, ZERO, MAX_COLUMN_LENGTH);
        MEMSET (au1ValueStr, ZERO, MAX_COLUMN_LENGTH);

        /* Get the next line */
        fgets ((char *) au1Buf, MAX_COLUMN_LENGTH, pBgpConfFilePtr);

        if ((STRCMP (au1Buf, "")) != ZERO)
        {
            MEMSET (au1NameStr, ZERO, MAX_COLUMN_LENGTH);
            MEMSET (au1EqualStr, ZERO, MAX_COLUMN_LENGTH);
            MEMSET (au1ValueStr, ZERO, MAX_COLUMN_LENGTH);
            SSCANF ((char *) au1Buf, "%119s%119s%119s", au1NameStr, au1EqualStr,
                    au1ValueStr);
            au1ValueStr[MAX_COLUMN_LENGTH - 1] = '\0';
            if (STRLEN (au1ValueStr) == 0)
            {
                continue;
            }

            /* Restore the restart remaining time */
            if (STRCMP (au1NameStr, "GR_TIME") == ZERO)
            {
                pGrSaveConfig->u4RestartTime = (UINT4) ATOI (au1ValueStr);
            }

            /* Fetch the shutdown time */
            if (STRCMP (au1NameStr, "GR_SHUT_TIME") == ZERO)
            {
                pGrSaveConfig->u4ShutTime = (UINT4) ATOI (au1ValueStr);
            }

        }                        /* If line exists */
    }                            /* End of while */

    fclose (pBgpConfFilePtr);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssBgpStoreGraceContent                          */
/*                                                                          */
/*    Description        : This routine stores the BGP GR related           */
/*                         configurations in file bgp4gr.txt                */
/*                                                                          */
/*    Input(s)           : buffer.(which contains the remaining time and sys*/
/*                         shut time)                                       */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/

VOID
IssBgpStoreGraceContent (VOID *buffer)
{
    FILE               *pBgpConfFilePtr = NULL;
    tBgpGrSaveConfig   *pGrSaveConfig = NULL;

    pGrSaveConfig = (tBgpGrSaveConfig *) buffer;

    pBgpConfFilePtr = FOPEN (BGP4_GR_CONF, "w");
    if (NULL == pBgpConfFilePtr)
    {
        /* File Creation failed */
        return;
    }

    fprintf (pBgpConfFilePtr, "\n");
    fflush (pBgpConfFilePtr);

    /* Store the status of the restart as planned */
    fprintf (pBgpConfFilePtr, "GR_STATUS          = ");
    fflush (pBgpConfFilePtr);
    fprintf (pBgpConfFilePtr, "planned\n");
    fflush (pBgpConfFilePtr);

    /* Save the Remaining grace period for the restart */
    fprintf (pBgpConfFilePtr, "GR_TIME   = %u\n", pGrSaveConfig->u4RestartTime);
    fflush (pBgpConfFilePtr);

    /* Save the system shut down time */
    fprintf (pBgpConfFilePtr, "GR_SHUT_TIME   = %u\n",
             pGrSaveConfig->u4ShutTime);
    fflush (pBgpConfFilePtr);

    /* Close the file bgp4gr.txt */
    fclose (pBgpConfFilePtr);
    return;

}
#endif /* BGP_WANTED */

#ifdef ISIS_WANTED
/****************************************************************************/
/*                                                                          */
/*    Function Name      : IsisStore                                        */
/*                                                                          */
/*    Description        : Helps to store Graceful restart                  */
/*                         information in a file                            */
/*                                                                          */
/*    Input(s)           : buffer - Contains grace info to be saved         */
/*                                                     */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IsisStore (VOID *buffer)
{
    tIsisGrInfo        *pu1GrInfo;
    FILE               *pConfFilePtr = NULL;
    UINT4               u4Index = 0;
    pConfFilePtr = FOPEN (ISIS_GR_CONF, "a+");

    if (pConfFilePtr == NULL)
    {
        /* File Creation failed */
        return;
    }

    if (fseek (pConfFilePtr, 0, SEEK_SET) != 0)
    {
        fclose (pConfFilePtr);
        return;
    }
    pu1GrInfo = (tIsisGrInfo *) buffer;

    fprintf (pConfFilePtr, "NO_OF_CXT   = %d\n", pu1GrInfo->u4CxtCount);

    while (u4Index < (pu1GrInfo->u4CxtCount))
    {
        fprintf (pConfFilePtr, "\n");
        fflush (pConfFilePtr);

        fprintf (pConfFilePtr, "CONTEXT         = %d\n",
                 pu1GrInfo->pCxtinfo[u4Index]->u4IsisCxtId);

        fprintf (pConfFilePtr, "STATUS          = ");
        fflush (pConfFilePtr);

        if (pu1GrInfo->pCxtinfo[u4Index]->u1RestartStatus == ISIS_RESTART_NONE)
        {
            fprintf (pConfFilePtr, "none\n");
        }
        else if (pu1GrInfo->pCxtinfo[u4Index]->u1RestartStatus ==
                 ISIS_RESTARTING_ROUTER)
        {
            fprintf (pConfFilePtr, "planned\n");
        }
        else if (pu1GrInfo->pCxtinfo[u4Index]->u1RestartStatus ==
                 ISIS_STARTING_ROUTER)
        {
            fprintf (pConfFilePtr, "unplanned\n");
        }
        fflush (pConfFilePtr);

        fprintf (pConfFilePtr, "GR_REASON       = ");
        fflush (pConfFilePtr);

        if (pu1GrInfo->pCxtinfo[u4Index]->u1RestartReason == ISIS_GR_UNKNOWN)
        {
            fprintf (pConfFilePtr, "unknown\n");
        }

        else if (pu1GrInfo->pCxtinfo[u4Index]->u1RestartReason ==
                 ISIS_GR_SW_RESTART)
        {
            fprintf (pConfFilePtr, "restart\n");
        }

        else if (pu1GrInfo->pCxtinfo[u4Index]->u1RestartReason ==
                 ISIS_GR_SW_UPGRADE)
        {
            fprintf (pConfFilePtr, "upgrade\n");
        }

        else if (pu1GrInfo->pCxtinfo[u4Index]->u1RestartReason ==
                 ISIS_GR_SW_RED)
        {
            fprintf (pConfFilePtr, "redundancy\n");
        }
        fflush (pConfFilePtr);

        fprintf (pConfFilePtr, "GR_EXIT_REASON  = ");
        fflush (pConfFilePtr);

        if (pu1GrInfo->pCxtinfo[u4Index]->u1ExitReason == ISIS_GR_RESTART_NONE)
        {
            fprintf (pConfFilePtr, "none\n");
        }

        else if (pu1GrInfo->pCxtinfo[u4Index]->u1ExitReason ==
                 ISIS_GR_RESTART_INPROGRESS)
        {
            fprintf (pConfFilePtr, "inprogress\n");
        }

        else if (pu1GrInfo->pCxtinfo[u4Index]->u1ExitReason ==
                 ISIS_GR_RESTART_COMPLETED)
        {
            fprintf (pConfFilePtr, "completed\n");
        }

        else if (pu1GrInfo->pCxtinfo[u4Index]->u1ExitReason ==
                 ISIS_GR_RESTART_TIMEDOUT)
        {
            fprintf (pConfFilePtr, "timedout\n");
        }

        else if (pu1GrInfo->pCxtinfo[u4Index]->u1ExitReason ==
                 ISIS_GR_RESTART_TOP_CHG)
        {
            fprintf (pConfFilePtr, "topchg\n");
        }
        fflush (pConfFilePtr);

        fprintf (pConfFilePtr, "GR_END_TIME     = %d\n",
                 pu1GrInfo->pCxtinfo[u4Index]->u4GrEndTime);

        u4Index++;
    }

    fclose (pConfFilePtr);

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IsisRestore                                */
/*                                                                          */
/*    Description        : Helps to populate the structure from the file    */
/*                         */
/*                         */
/*                                                                          */
/*    Input(s)           : buffer - to which info needs to be restored from */
/*                                   file                                   */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
IsisRestore (VOID *buffer)
{

    tIsisGrInfo        *pu1GrInfo = NULL;
    tIsisGrCxtInfo    **pu1GrCxtinfo = NULL;
    UINT4               u4Index = 0;
    FILE               *pConfFilePtr = NULL;    /* File pointer */
    UINT1               au1Buf[MAX_COLUMN_LENGTH];
    /* Stores each line in file */
    UINT1               au1NameStr[MAX_COLUMN_LENGTH];
    /* Stores line identification */
    UINT1               au1EqualStr[MAX_COLUMN_LENGTH];
    /* Stores the string "=" */
    UINT1               au1ValueStr[MAX_COLUMN_LENGTH];
    /* Stores the value for each identification */

    pConfFilePtr = FOPEN (ISIS_GR_CONF, "r");

    if (pConfFilePtr == NULL)
    {
        /* File Creation failed */
        return;
    }

    pu1GrInfo = (tIsisGrInfo *) buffer;

    /* Allocate memory to hold the GR Cxt information. */

    pu1GrCxtinfo = IsisUtlAllocMemGRInfoList ();

    if (pu1GrCxtinfo == NULL)
    {
        fclose (pConfFilePtr);
        return;
    }

    while (!feof (pConfFilePtr))
    {

        MEMSET (au1Buf, 0, MAX_COLUMN_LENGTH);
        MEMSET (au1NameStr, 0, MAX_COLUMN_LENGTH);
        MEMSET (au1EqualStr, 0, MAX_COLUMN_LENGTH);
        MEMSET (au1ValueStr, 0, MAX_COLUMN_LENGTH);

        /* Get the next line */
        fgets ((char *) au1Buf, MAX_COLUMN_LENGTH, pConfFilePtr);

        if ((STRCMP (au1Buf, "")) != 0)
        {
            SSCANF ((char *) au1Buf, "%119s%119s%119s", au1NameStr, au1EqualStr,
                    au1ValueStr);
            au1ValueStr[MAX_COLUMN_LENGTH - 1] = '\0';

            if (STRLEN (au1ValueStr) == 0)
            {
                continue;
            }
            if (STRCMP (au1NameStr, "NO_OF_CXT") == 0)
            {
                pu1GrInfo->u4CxtCount = (UINT4) ATOI (au1ValueStr);
            }
            if (STRCMP (au1NameStr, "CONTEXT") == 0)
            {
                /* Allocate memory to hold the GR Cxt information. */
                pu1GrCxtinfo[u4Index] = IsisUtlAllocMemGRInfo ();

                if (pu1GrCxtinfo[u4Index] == NULL)
                {
                    break;
                }

                pu1GrCxtinfo[u4Index]->u4IsisCxtId = (UINT4) ATOI (au1ValueStr);
            }
            if (STRCMP (au1NameStr, "STATUS") == 0)
            {
                if (STRCMP (au1ValueStr, "planned") == 0)
                {
                    pu1GrCxtinfo[u4Index]->u1RestartStatus =
                        ISIS_RESTARTING_ROUTER;
                }
                if (STRCMP (au1ValueStr, "unplanned") == 0)
                {
                    pu1GrCxtinfo[u4Index]->u1RestartStatus =
                        ISIS_STARTING_ROUTER;
                }
                if (STRCMP (au1ValueStr, "none") == 0)
                {
                    pu1GrCxtinfo[u4Index]->u1RestartStatus = ISIS_RESTART_NONE;
                }
            }
            if (STRCMP (au1NameStr, "GR_REASON") == 0)
            {
                if (STRCMP (au1ValueStr, "unknown") == 0)
                {
                    pu1GrCxtinfo[u4Index]->u1RestartReason = ISIS_GR_UNKNOWN;
                }
                if (STRCMP (au1ValueStr, "restart") == 0)
                {
                    pu1GrCxtinfo[u4Index]->u1RestartReason = ISIS_GR_SW_RESTART;
                }
                if (STRCMP (au1ValueStr, "upgrade") == 0)
                {
                    pu1GrCxtinfo[u4Index]->u1RestartReason = ISIS_GR_SW_UPGRADE;
                }
                if (STRCMP (au1ValueStr, "redundancy") == 0)
                {
                    pu1GrCxtinfo[u4Index]->u1RestartReason = ISIS_GR_SW_RED;
                }

            }
            if (STRCMP (au1NameStr, "GR_EXIT_REASON") == 0)
            {
                if (STRCMP (au1ValueStr, "none") == 0)
                {
                    pu1GrCxtinfo[u4Index]->u1ExitReason = ISIS_GR_RESTART_NONE;
                }
                if (STRCMP (au1ValueStr, "completed") == 0)
                {
                    pu1GrCxtinfo[u4Index]->u1ExitReason =
                        ISIS_GR_RESTART_COMPLETED;
                }
                if (STRCMP (au1ValueStr, "timedout") == 0)
                {
                    pu1GrCxtinfo[u4Index]->u1ExitReason =
                        ISIS_GR_RESTART_TIMEDOUT;
                }
                if (STRCMP (au1ValueStr, "topchg") == 0)
                {
                    pu1GrCxtinfo[u4Index]->u1ExitReason =
                        ISIS_GR_RESTART_TOP_CHG;
                }
                if (STRCMP (au1ValueStr, "inprogress") == 0)
                {
                    pu1GrCxtinfo[u4Index]->u1ExitReason =
                        ISIS_GR_RESTART_INPROGRESS;
                }

            }
            if (STRCMP (au1NameStr, "GR_END_TIME") == 0)
            {
                pu1GrCxtinfo[u4Index]->u4GrEndTime = (UINT4) ATOI (au1ValueStr);

                u4Index++;

                if (u4Index == pu1GrInfo->u4CxtCount)
                {
                    u4Index = 0;
                }
            }
        }

    }
    pu1GrInfo->pCxtinfo = pu1GrCxtinfo;
    fclose (pConfFilePtr);
}
#endif
/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssInitialiseNvRamDefVal                         */
/*                                                                          */
/*    Description        : This function initialises NvRam with the default */
/*                         values.                                          */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : VOID                                             */
/****************************************************************************/
VOID
IssInitialiseNvRamDefVal (VOID)
{
    tMacAddr            DefaultSwitchMac =
        { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05 };
    INT1               *pi1FileName = NULL;
    UINT4               u4RestIpAddr = 0;

    sNvRamData.u4LocalIpAddr = ISS_DEFAULT_IP_ADDRESS;
    sNvRamData.u4IpAddrAllocProto = CFA_PROTO_DHCP;
    sNvRamData.u4LocalSubnetMask = ISS_DEFAULT_IP_MASK;
    INET_ATON6 (ISS_DEFAULT_IPV6_ADDRESS, &sNvRamData.localIp6Addr);
    sNvRamData.u1PrefixLen = ISS_DEFAULT_IPV6_PREFIX_LEN;
    sNvRamData.u4CfgMode = ISS_CFG_MANUAL;
    sNvRamData.u4RestoreOption = ISS_CONFIG_NO_RESTORE;
    sNvRamData.i1ResFlag = ISS_DEFAULT_RESTORE_FLAG;
    sNvRamData.i1SaveFlag = ISS_DEFAULT_SAVE_FLAG;
    sNvRamData.RestoreIpAddr.u1Afi = IPVX_ADDR_FMLY_IPV4;
    sNvRamData.RestoreIpAddr.u1AddrLen = IPVX_IPV4_ADDR_LEN;
    u4RestIpAddr = OSIX_NTOHL (ISS_DEFAULT_RES_IP_ADDRESS);
    MEMCPY (sNvRamData.RestoreIpAddr.au1Addr,
            &u4RestIpAddr, IPVX_IPV4_ADDR_LEN);
    sNvRamData.u2DefaultVlan = ISS_DEFAULT_VLAN_ID;
    sNvRamData.u2StackPortCount = ISS_DEFAULT_STACK_PORT_COUNT;
    sNvRamData.u4FrontPanelPortCount = ISS_DEFAULT_FRONT_PANEL_PORT_COUNT;

    /* Initialise Restore file name */
    if (ISS_STRLEN (IssGetSaveFileName ()) <= ISS_CONFIG_FILE_NAME_LEN)
    {
        pi1FileName = (INT1 *) IssGetSaveFileName ();
        ISS_STRNCPY (sNvRamData.ai1RestoreFileName, pi1FileName,
                     (sizeof (sNvRamData.ai1RestoreFileName) - 1));
        sNvRamData.
            ai1RestoreFileName[(sizeof (sNvRamData.ai1RestoreFileName) - 1)] =
            '\0';
    }
    else
    {
        ISS_STRCPY (sNvRamData.ai1RestoreFileName, ISS_DEFAULT_RES_FILE_NAME);
    }

    /* Initialise BGP Restore file name */
    if (ISS_STRLEN (IssGetBgpConfSaveFileName ()) <= ISS_CONFIG_FILE_NAME_LEN)
    {
        pi1FileName = (INT1 *) IssGetBgpConfSaveFileName ();
        ISS_STRNCPY (sNvRamData.ai1BgpConfRestoreFileName, pi1FileName,
                     (sizeof (sNvRamData.ai1BgpConfRestoreFileName) - 1));
        sNvRamData.ai1BgpConfRestoreFileName[(sizeof
                                              (sNvRamData.
                                               ai1BgpConfRestoreFileName) -
                                              1)] = '\0';
    }
    else
    {
        ISS_STRCPY (sNvRamData.ai1BgpConfRestoreFileName, BGP4_CSR_CONFIG_FILE);
    }

    /* Initialise CSR Restore file name */
    if (ISS_STRLEN (IssGetCsrConfSaveFileName ()) <= ISS_CONFIG_FILE_NAME_LEN)
    {
        pi1FileName = (INT1 *) IssGetCsrConfSaveFileName ();
        ISS_STRNCPY (sNvRamData.ai1CsrConfRestoreFileName, pi1FileName,
                     (sizeof (sNvRamData.ai1CsrConfRestoreFileName) - 1));
        sNvRamData.ai1CsrConfRestoreFileName[(sizeof
                                              (sNvRamData.
                                               ai1CsrConfRestoreFileName) -
                                              1)] = '\0';
    }
    else
    {
        ISS_STRCPY (sNvRamData.ai1CsrConfRestoreFileName, CSR_CONFIG_FILE);
    }

    /* Initialise OSPF Restore file name */
    if (ISS_STRLEN (IssGetOspfConfSaveFileName ()) <= ISS_CONFIG_FILE_NAME_LEN)
    {
        pi1FileName = (INT1 *) IssGetOspfConfSaveFileName ();
        ISS_STRNCPY (sNvRamData.ai1OspfConfRestoreFileName, pi1FileName,
                     (sizeof (sNvRamData.ai1OspfConfRestoreFileName) - 1));
        sNvRamData.ai1OspfConfRestoreFileName[(sizeof
                                               (sNvRamData.
                                                ai1OspfConfRestoreFileName) -
                                               1)] = '\0';
    }
    else
    {
        ISS_STRCPY (sNvRamData.ai1OspfConfRestoreFileName,
                    OSPF3_CSR_CONFIG_FILE);
    }

    ISS_STRCPY (sNvRamData.ai1IncrSaveFileName,
                ISS_DEFAULT_INCR_SAVE_FILE_NAME);

    ISS_STRCPY (sNvRamData.ai1InterfaceName, ISS_DEFAULT_INTERFACE_NAME);
    ISS_STRCPY (sNvRamData.ai1RmIfName, ISS_DEFAULT_RM_INTERFACE_NAME);
    sNvRamData.u4PimMode = ISS_DEFAULT_PIM_MODE;
    sNvRamData.u4BridgeMode = ISS_DEFAULT_BRIDGE_MODE;
    sNvRamData.u4SnoopFwdMode = ISS_DEFAULT_SNOOP_FWD_MODE;
    ISS_MEMCPY (sNvRamData.au1SwitchMac, DefaultSwitchMac, 6);
    ISS_MEMCPY (sNvRamData.ai1SnmpEngineID, gi1SnmpEngineID,
                ISS_STRLEN (gi1SnmpEngineID));

#ifdef MBSM_WANTED
    sNvRamData.u4MgmtPort = TRUE;
#else
    sNvRamData.u4MgmtPort = FALSE;
#endif
    sNvRamData.u4CliSerialConsole = ISS_DEFAULT_CLI_SERIAL_CONSOLE;
    sNvRamData.i1AutoSaveFlag = ISS_DEFAULT_AUTO_SAVE_FLAG;
    sNvRamData.i1IncrSaveFlag = ISS_DEFAULT_INCR_SAVE_FLAG;
    sNvRamData.i1RollbackFlag = ISS_DEFAULT_ROLLBACK_FLAG;
    sNvRamData.u4SnmpEngineBoots = gu4SnmpEngineBoots;
    sNvRamData.i1DefValSaveFlag = ISS_FALSE;
    sNvRamData.i4FipsOperMode = ISS_DEFAULT_FIPS_OPER_MODE;
    ISS_STRCPY (sNvRamData.au1NpapiMode, ISS_DEFAULT_NPAPI_MODE);

    ISS_STRCPY (sNvRamData.ai1HardwareVersion, ISS_HARDWARE_VARSION);
    sNvRamData.ai1HardwareVersion[STRLEN (ISS_HARDWARE_VARSION)] = '\0';
    ISS_STRCPY (sNvRamData.ai1SoftwareVersion, ISS_SOFTWARE_VERSION);
    sNvRamData.ai1SoftwareVersion[STRLEN (ISS_SOFTWARE_VERSION)] = '\0';

    ISS_STRCPY ((UINT1 *) sNvRamData.ai1FirmwareVersion, ISS_FIRMWARE_VERSION);
    sNvRamData.ai1FirmwareVersion[STRLEN (ISS_FIRMWARE_VERSION)] = '\0';

    ISS_STRCPY ((UINT1 *) sNvRamData.ai1HardwarePartNumber,
                ISS_HARDWARE_PART_NUMBER);
    sNvRamData.ai1HardwarePartNumber[STRLEN (ISS_HARDWARE_PART_NUMBER)] = '\0';

    ISS_STRCPY ((UINT1 *) sNvRamData.ai1SoftwareSerialNumber,
                ISS_SOFTWARE_SERIAL_NUMBER);
    sNvRamData.ai1SoftwareSerialNumber[STRLEN (ISS_SOFTWARE_SERIAL_NUMBER)] =
        '\0';

    ISS_STRCPY ((UINT1 *) sNvRamData.ai1SwitchName, ISS_SWITCH_NAME);
    sNvRamData.ai1SwitchName[STRLEN (ISS_SWITCH_NAME)] = '\0';

#if ((defined (ALTA_WANTED) || defined (RC_WANTED) && defined (VRF_WANTED)))
    /*for VRF on ALTA VRF_UNIQUE_MAC should be always enabled*/
    sNvRamData.bVrfUnqMacOption = ISS_VRF_UNQ_MAC_ENABLE;
#endif

    /* HITLESS RESTART */
    sNvRamData.i1RmHRFlag = ISS_DEFAULT_HR_FLAG;

    /* RM Heart Beat Mode */
    sNvRamData.u4RmHbMode = ISS_RM_HB_MODE_INTERNAL;

    /* RM Redundancy Type */
    sNvRamData.u4RmRType = ISS_RM_RTYPE_HOT;

    /* RM Data Plane Type */
    sNvRamData.u4RmDType = ISS_RM_DTYPE_SHARED;

    /*Automatic Port Create Flag */
    sNvRamData.i1AutomaticPortCreate = ISS_DEFAULT_AUTO_PORT_CREATE;

    /* RM Type Flag */
    sNvRamData.u4RmStackingInterfaceType = ISS_RM_STACK_INTERFACE_TYPE_OOB;

    /* Restoration Type */
    sNvRamData.u1RestoreType = ISS_DEFAULT_RESTORE_TYPE;

    if (ISS_NVRAM_CALLBACK[ISS_NVRAM_CUST_INIT_EVENT].pIssCustInitNvRamDefVal !=
        NULL)
    {
        IssCustNvramCallBack (ISS_NVRAM_CUST_INIT_EVENT, 0);
    }

    /* Write to NvRam here */
    if (NvRamWrite (&sNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to NvRam\n");
        return;
    }
}

/*****************************************************************************/
/* Function Name      : IssCustNvramCallBack                                 */
/*                                                                           */
/* Description        : This function invokes the function pointer registered*/
/*                         for the given event                               */
/*                                                                           */
/* Input(s)           : NONE                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ISS_FAILURE if the event is not handled else returns */
/*            ISS_SUCCESS                         */
/*****************************************************************************/

INT4
IssCustNvramCallBack (UINT4 u4Event, INT4 CliHandle)
{
    INT4                i4RetVal = ISS_SUCCESS;

    switch (u4Event)
    {

        case ISS_NVRAM_CLI_SHOW_EVENT:
            if (ISS_NVRAM_CALLBACK[u4Event].pIssCustShowNvram != NULL)
            {
                i4RetVal =
                    ISS_NVRAM_CALLBACK[u4Event].pIssCustShowNvram (CliHandle);
            }
            break;
        case ISS_NVRAM_CUST_READ_EVENT:
            if (ISS_NVRAM_CALLBACK[u4Event].pIssCustReadNvram != NULL)
            {
                i4RetVal = ISS_NVRAM_CALLBACK[u4Event].pIssCustReadNvram ();
            }
            break;
        case ISS_NVRAM_CUST_WRITE_EVENT:
            if (ISS_NVRAM_CALLBACK[u4Event].pIssCustWriteNvram != NULL)
            {
                i4RetVal = ISS_NVRAM_CALLBACK[u4Event].pIssCustWriteNvram ();
            }
            break;
        case ISS_NVRAM_CUST_INIT_EVENT:
            if (ISS_NVRAM_CALLBACK[u4Event].pIssCustInitNvRamDefVal != NULL)
            {
                i4RetVal =
                    ISS_NVRAM_CALLBACK[u4Event].pIssCustInitNvRamDefVal ();
            }
            break;
        default:
            i4RetVal = ISS_FAILURE;
            break;
    }

    return i4RetVal;

}
