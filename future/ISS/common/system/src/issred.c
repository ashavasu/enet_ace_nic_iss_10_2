/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved                      
 *
 * $Id: issred.c,v 1.10 2015/08/11 11:50:07 siva Exp $
 *
 * Description   : This file contains access routines  for                   
 *                 high availability support in ISS.                
 *****************************************************************************/
/*****************************************************************************/
/*    FILE  NAME            : issred.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Port Isolation redundancy                      */
/*    MODULE NAME           : ISS                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Linux                                          */
/*    DATE OF FIRST RELEASE : 13 July 2010                                   */
/*    AUTHOR                : Aricent Inc.                                   */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    06 Oct  2010           Initial Creation                        */
/*---------------------------------------------------------------------------*/
#ifndef ISSRED_C
#define ISSRED_C

#include "issinc.h"
#include "isspi.h"
#include "isspinp.h"
#include "hwaud.h"
#include "hwaudmap.h"
#include "iss.h"

/*****************************************************************************/
/* Function Name      : IssRedInit                                           */
/* Description        : This function initializes the global structure       */
/*                      IssRedInfo and creates MemPools for Message Queues   */
/*                      and Np-Sync buffers.                                 */
/*                      update messages.                                     */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : if registration is success then ISS_SUCCESS          */
/*                      Otherwise ISS_FAILURE                                */
/*****************************************************************************/
INT4
IssRedInit (VOID)
{
    MEMSET (&gIssGlobalInfo.IssRedInfo, ISS_ZERO_ENTRY, sizeof (tIssRedInfo));

    /* Default value of Node Status is IDLE */
    gIssGlobalInfo.IssRedInfo.u1NodeStatus = RM_INIT;

    gIssGlobalInfo.IssRedInfo.u1NpSyncBlockCount = ISS_ZERO_ENTRY;

    /* NP-Syncups are not to be sent from Idle node. NPSync buffer Table
     * should not be accessed when the node is Idle. Set the Count value to
     * 1 so that the Np sync table is blocked.       */

    IssRedHwAuditIncBlkCounter ();

    /* Register ISS module with RM. */
    if (ISS_FAILURE == IssRedRegisterWithRM ())
    {
        return ISS_FAILURE;
    }

    if (OsixQueCrt (ISS_RED_QUEUE, OSIX_MAX_Q_MSG_LEN,
                    ISS_RED_QUEUE_SIZE,
                    &(gIssGlobalInfo.IssRedInfo.IssRedQueId)) != OSIX_SUCCESS)
    {
        ISS_TRC (INIT_SHUT_TRC, "ISS redundancy queue creation FAILED\n");
        return ISS_FAILURE;
    }

    if (MemCreateMemPool (sizeof (tIssRedQMsg),
                          ISS_RED_QUEUE_SIZE,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(gIssGlobalInfo.IssRedInfo.IssRedQueuePoolId))
        != MEM_SUCCESS)
    {
        return ISS_FAILURE;
    }

    /* Hardware Audit Np-Sync Buffer Mempool */

    if (MemCreateMemPool
        (sizeof (tIssRedNpSyncEntry), ISS_MAX_NPSYNC_BUF_ENTRIES,
         MEM_DEFAULT_MEMORY_TYPE,
         &(gIssGlobalInfo.IssRedInfo.HwAuditTablePoolId)) == MEM_FAILURE)
    {
        return ISS_FAILURE;
    }

    TMO_SLL_Init (&(gIssGlobalInfo.IssRedInfo.HwAuditTable));
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssRedDeInit                                         */
/* Description        : This function de-initializes ISS Red Global info and */
/*                      de-allocates the memory blocks.                      */
/*                      update messages.                                     */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : if registration is success then ISS_SUCCESS          */
/*                      Otherwise ISS_FAILURE                                */
/*****************************************************************************/
VOID
IssRedDeInit (VOID)
{
    if (gIssGlobalInfo.IssRedInfo.IssRedQueuePoolId != ISS_ZERO_ENTRY)
    {
        MemDeleteMemPool ((tMemPoolId)
                          gIssGlobalInfo.IssRedInfo.IssRedQueuePoolId);
        gIssGlobalInfo.IssRedInfo.IssRedQueuePoolId = ISS_ZERO_ENTRY;
    }

    if (gIssGlobalInfo.IssRedInfo.HwAuditTablePoolId != ISS_ZERO_ENTRY)
    {
        MemDeleteMemPool (gIssGlobalInfo.IssRedInfo.HwAuditTablePoolId);
        gIssGlobalInfo.IssRedInfo.HwAuditTablePoolId = ISS_ZERO_ENTRY;
    }

    if (gIssGlobalInfo.IssRedInfo.IssRedQueId != ISS_ZERO_ENTRY)
    {
        OsixQueDel (gIssGlobalInfo.IssRedInfo.IssRedQueId);
    }

    IssPortRmDeRegisterProtocols (RM_ISS_APP_ID);
    return;
}

/*****************************************************************************/
/* Function Name      : IssRedRegisterWithRM                                 */
/* Description        : Registers ISS module with RM to send and receive     */
/*                      update messages.                                     */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/* Return Value(s)    : if registration is success then ISS_SUCCESS          */
/*                      Otherwise ISS_FAILURE                                */
/*****************************************************************************/
INT4
IssRedRegisterWithRM (VOID)
{
    tRmRegParams        RmRegParams;

    MEMSET (&RmRegParams, 0, sizeof (tRmRegParams));

    RmRegParams.u4EntId = RM_ISS_APP_ID;
    RmRegParams.pFnRcvPkt = IssRedHandleUpdateEvents;

    if (IssPortRmRegisterProtocols (&RmRegParams) == RM_FAILURE)
    {
        ISS_TRC (BUFFER_TRC, "IssRedRegisterWithRM: IssPortRmRegisterProtocols"
                 "call failed\n");
        return ISS_FAILURE;
    }

    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssRedHandleUpdateEvents                             */
/*                                                                           */
/* Description        : This function will be invoked by the RM module to    */
/*                      pass events like GO_ACTIVE, GO_STANDBY etc. to ISS   */
/*                      module.                                              */
/*                                                                           */
/* Input(s)           : u1Event - Event given by RM module.                  */
/*                      pData   - Msg to be enqueued, valid if u1Event is    */
/*                                VALID_UPDATE_MESSAGE.                      */
/*                      u2DataLen - Size of the update message.              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS - If the message is processed           */
/*                      successfully                                         */
/*                      OSIX_FAILURE otherwise                               */
/*****************************************************************************/
INT4
IssRedHandleUpdateEvents (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tIssRedQMsg        *pMsg = NULL;

    /* If the received event is not any of the following, then just return
     * without processing the event */
    if ((u1Event != RM_MESSAGE) &&
        (u1Event != GO_ACTIVE) &&
        (u1Event != GO_STANDBY) &&
        (u1Event != RM_STANDBY_UP) &&
        (u1Event != RM_STANDBY_DOWN) &&
        (u1Event != L2_INITIATE_BULK_UPDATES) &&
        (u1Event != RM_DYNAMIC_SYNCH_AUDIT) &&
        (u1Event != RM_CONFIG_RESTORE_COMPLETE) &&
        (u1Event != RM_INIT_HW_AUDIT))
    {
        ISS_TRC (CONTROL_PLANE_TRC, "IssRedHandleUpdateEvents : Event is "
                 "not associated with RM\n");
        return OSIX_FAILURE;
    }

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Message absent and hence no need to process */
        ISS_TRC (BUFFER_TRC, "IssRedHandleUpdateEvents:Queue Message associated"
                 "with the event is not sent by RM\n");
        return OSIX_FAILURE;
    }

    pMsg = (tIssRedQMsg *)
        MemAllocMemBlk (gIssGlobalInfo.IssRedInfo.IssRedQueuePoolId);
    if (NULL == pMsg)
    {
        /* Memory allocation failure. Hence return failure. */
        ISS_TRC (BUFFER_TRC, "IssRedHandleUpdateEvents:Memory allocation"
                 "failure\n");
        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            IssPortRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        return OSIX_FAILURE;
    }

    MEMSET (pMsg, ISS_ZERO_ENTRY, sizeof (tIssRedQMsg));

    pMsg->u4MsgType = ISS_RED_MESSAGE;
    pMsg->IssRmMsg.pData = pData;
    pMsg->IssRmMsg.u1Event = u1Event;
    pMsg->IssRmMsg.u2DataLen = u2DataLen;

    if (IssRedQueEnqMsg (pMsg) == OSIX_FAILURE)
    {
        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            IssPortRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        ISS_TRC (CONTROL_PLANE_TRC, "IssRedHandleUpdateEvents:"
                 "IssRedQueEnqMsg failed \n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssRedProcessQMsgEvent                               */
/* Description        : This function process the RM  message posted in      */
/*                      ISS Redundancy  Queue.                               */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IssRedProcessQMsgEvent (VOID)
{
    tIssRedQMsg        *pIssRedQMsg = NULL;

    while (OsixQueRecv (gIssGlobalInfo.IssRedInfo.IssRedQueId,
                        (UINT1 *) &pIssRedQMsg,
                        OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
    {
        switch (pIssRedQMsg->u4MsgType)
        {
            case ISS_RED_MESSAGE:
                IssRedHandleRmEvents (pIssRedQMsg);
                break;
            default:
                /* Invalid message type received in the redundancy queue. 
                 */
                break;
        }                        /* End of switch */
        MemReleaseMemBlock (gIssGlobalInfo.IssRedInfo.IssRedQueuePoolId,
                            (UINT1 *) pIssRedQMsg);
    }                            /* End of while */
}

/*****************************************************************************/
/* Function Name      : IssRedQueEnqMsg                                      */
/* Description        : This function posts the messge to the ISS queue      */
/*                      and send the event to ISS System Task.               */
/* Input(s)           : pMsg- ISS Queue Message                              */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : OSIX_SUCCESS - On success                            */
/*                      OSIX_FAILURE - On failure                            */
/*****************************************************************************/
PUBLIC INT4
IssRedQueEnqMsg (tIssRedQMsg * pMsg)
{

    if (OsixQueSend (gIssGlobalInfo.IssRedInfo.IssRedQueId,
                     (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (gIssGlobalInfo.IssRedInfo.IssRedQueuePoolId,
                            (UINT1 *) pMsg);
        ISS_TRC (CONTROL_PLANE_TRC, "IssRedQueEnqMsg: Sending to Message"
                 "Queue failed\n");
        return OSIX_FAILURE;
    }

    if (OsixEvtSend (ISS_SYS_TASK_ID, ISS_RED_MSG_EVENT) != OSIX_SUCCESS)
    {
        ISS_TRC (CONTROL_PLANE_TRC, "IssRedQueEnqMsg: Sending Event to ISS"
                 "System Task failed\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssRedHandleRmEvents                                 */
/* Description        : This function handles the RM Message posted in the   */
/*                      Iss queue.                                           */
/* Input(s)           : pMsg- ISS Redundancy Queue Message                   */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
IssRedHandleRmEvents (tIssRedQMsg * pMsg)
{
    tRmProtoAck         ProtoAck;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4SeqNum = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    MEMSET (&ProtoAck, 0, sizeof (tRmProtoAck));

    ProtoEvt.u4AppId = RM_ISS_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    switch (pMsg->IssRmMsg.u1Event)
    {
            /* Standby up / down event is not received from RM module.  
             * since bulk updates are not sent from ISS.
             */
        case GO_ACTIVE:
            IssRedHandleGoActive ();
            break;

        case GO_STANDBY:
            IssRedHandleGoStandby ();
            break;

        case RM_CONFIG_RESTORE_COMPLETE:
            if (gIssGlobalInfo.IssRedInfo.u1NodeStatus == RM_INIT)
            {
                if (IssPortRmGetNodeState () == RM_STANDBY)
                {
                    IssRedHandleIdleToStandby ();
                }
            }
            break;

        case RM_INIT_HW_AUDIT:
            /* Hardware Audit for Port Isolation is performed here     */
            IssRedInitHardwareAudit ();
            break;

        case RM_MESSAGE:
            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM (pMsg->IssRmMsg.pData, &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR (pMsg->IssRmMsg.pData,
                                 pMsg->IssRmMsg.u2DataLen);

            ProtoAck.u4AppId = RM_ISS_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            if (gIssGlobalInfo.IssRedInfo.u1NodeStatus == RM_STANDBY)
            {
                IssRedProcessPeerMsgAtStandby (pMsg->IssRmMsg.pData,
                                               pMsg->IssRmMsg.u2DataLen);
            }
            RM_FREE (pMsg->IssRmMsg.pData);

            /* Sending ACK to RM */
            IssPortRmApiSendProtoAckToRM (&ProtoAck);
            break;

        case RM_STANDBY_UP:
        case RM_STANDBY_DOWN:
            /* Release the Memory to Memory Pool assigned by RM Module */
            IssPortRmReleaseMemoryForMsg ((UINT1 *) pMsg->IssRmMsg.pData);
            break;

        case RM_DYNAMIC_SYNCH_AUDIT:
            IssRedHandleDynSyncAudit ();
            break;

        default:
            break;
    }                            /* End of switch */
}

/*****************************************************************************/
/* Function Name      : IssRedInitHardwareAudit                              */
/* Description        : This function handles the hardware audit event sent  */
/*                      by redundancy manager.                               */
/* Input(s)           : pMsg- ISS redundancy Queue Message                   */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IssRedInitHardwareAudit (VOID)
{
    tIssRedNpSyncEntry *pBuf = NULL;
    tIssRedNpSyncEntry *pTempBuf = NULL;

    if (gIssGlobalInfo.IssRedInfo.u1NodeStatus != RM_ACTIVE)
    {
        ISS_TRC (CONTROL_PLANE_TRC | ALL_FAILURE_TRC, "IssRedInitHardwareAudit"
                 "INIT_HW_AUDIT event reached"
                 "for Standby node. Not processed.\r\n");
        return;
    }

    TMO_DYN_SLL_Scan (&gIssGlobalInfo.IssRedInfo.HwAuditTable, pBuf,
                      pTempBuf, tIssRedNpSyncEntry *)
    {
        switch (pBuf->u4NpApiId)
        {

            case NPSYNC_ISS_HW_CONFIG_PORT_ISOLATION_ENTRY:
                IssPIRedHandleHwAudit (&(pBuf->
                                         unNpData.IssHwConfigPortIsolationEntry.
                                         IssHwUpdtPortIsolation));
                break;
            default:
                break;
        }
        TMO_SLL_Delete (&gIssGlobalInfo.IssRedInfo.HwAuditTable, &(pBuf->Node));
        MemReleaseMemBlock (gIssGlobalInfo.IssRedInfo.
                            HwAuditTablePoolId, (UINT1 *) pBuf);
        pBuf = pTempBuf;

    }
    return;
}

/*****************************************************************************/
/* Function Name      : IssRedHandleGoActive                                 */
/* Description        : This function handles the GO_ACTIVE event received   */
/*                      from RM                                              */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : gIssGlobalInfo                                       */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
IssRedHandleGoActive (VOID)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, ISS_ZERO_ENTRY, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_ISS_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    if (gIssGlobalInfo.IssRedInfo.u1NodeStatus == RM_ACTIVE)
    {
        ISS_TRC (CONTROL_PLANE_TRC, "IssRedHandleGoActive:"
                 "GO_ACTIVE event reached"
                 " when node is already active!!!!\r\n");
        return;
    }

    if (gIssGlobalInfo.IssRedInfo.u1NodeStatus == RM_INIT)
    {
        ISS_TRC (CONTROL_PLANE_TRC, "IssRedHandleGoActive: Idle to Active"
                 " transition...\r\n");
        IssRedHandleIdleToActive ();
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }
    else if (gIssGlobalInfo.IssRedInfo.u1NodeStatus == RM_STANDBY)
    {
        ISS_TRC (CONTROL_PLANE_TRC, "IssRedHandleGoActive: Standby to Active"
                 " transition...\r\n");
        IssRedHandleStandbyToActive ();
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }

    if (IssPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "IssRedHandleGoActive:"
                 "Acknowledgement to RM for" "GO_ACTIVE event failed!!!!\r\n");
    }
    return;
}

/*****************************************************************************/
/* Function Name      : IssRedInitNpSyncBufferTable                          */
/* Description        : This function intialise the NP Sync Buffer           */
/*                      table which will be used for Pending entry during    */
/*                      Hardware Audit                                       */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : gIssGlobalInfo                                       */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
IssRedInitNpSyncBufferTable (VOID)
{
    tIssRedNpSyncEntry *pBuf = NULL;
    tIssRedNpSyncEntry *pTempBuf = NULL;

    /* If the SLL is not empty then delete all the buffer entries. */
    if (TMO_SLL_Count (&gIssGlobalInfo.IssRedInfo.HwAuditTable)
        != ISS_ZERO_ENTRY)
    {
        TMO_DYN_SLL_Scan (&gIssGlobalInfo.IssRedInfo.HwAuditTable,
                          pBuf, pTempBuf, tIssRedNpSyncEntry *)
        {
            TMO_SLL_Delete (&gIssGlobalInfo.IssRedInfo.HwAuditTable,
                            &(pBuf->Node));
            MemReleaseMemBlock (gIssGlobalInfo.IssRedInfo.
                                HwAuditTablePoolId, (UINT1 *) pBuf);
            pBuf = pTempBuf;
        }
    }

    TMO_SLL_Init (&(gIssGlobalInfo.IssRedInfo.HwAuditTable));
    return;

}

/*****************************************************************************/
/* Function Name      : IssRedHandleGoStandby                                */
/* Description        : This function handles the GO_STANDBY event received  */
/*                      from RM                                              */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : gIssGlobalInfo                                       */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
IssRedHandleGoStandby (VOID)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, ISS_ZERO_ENTRY, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_ISS_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    if (gIssGlobalInfo.IssRedInfo.u1NodeStatus == RM_STANDBY)
    {
        ISS_TRC (CONTROL_PLANE_TRC, "IssRedHandleGoStandby:"
                 "GO_STANDBY event reached"
                 " when node is already standby!!!!\r\n");
        return;
    }
    /* Create the Hardware Audit Np sync Buffer table */
    IssRedInitNpSyncBufferTable ();

    if (gIssGlobalInfo.IssRedInfo.u1NodeStatus == RM_INIT)
    {
        ISS_TRC (CONTROL_PLANE_TRC, "IssRedHandleGoStandby:"
                 "GO_STANDBY event received" " when state is Idle.\r\n");

        /* GO_STANDBY event is not processed here. It is done when
         * CONFIG_RESTORE_COMPLETE event is received. Since static bulk
         * update will be completed only by then, the acknowledgement can be
         * sent during CONFIG_RESTORE_COMPLETE handling, which will trigger RM
         * to send dynamic bulk update event to modules.  */
    }
    else if (gIssGlobalInfo.IssRedInfo.u1NodeStatus == RM_ACTIVE)
    {
        ISS_TRC (CONTROL_PLANE_TRC, "IssRedHandleGoStandby: Active to Standby"
                 " transition...\r\n");
        IssRedHandleActiveToStandby ();
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

        if (IssPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            ISS_TRC (ALL_FAILURE_TRC, "IssRedHandleGoStandby:"
                     "Acknowledgement to RM for"
                     "GO_STANDBY event failed!!!!\r\n");
        }
    }

    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncIssHwConfigPortIsolationEntry
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncIssHwConfigPortIsolationEntry (tIssHwUpdtPortIsolation *
                                     pIssHwUpdtPortIsolation)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));

    MEMCPY (&NpSync.IssHwConfigPortIsolationEntry.IssHwUpdtPortIsolation,
            pIssHwUpdtPortIsolation, sizeof (tIssHwUpdtPortIsolation));
    u4NodeState = (UINT4) IssPortRmGetNodeState ();

    if (u4NodeState == RM_ACTIVE)
    {
        if (IssPortRmGetStandbyNodeCount () != 0)
        {
            if (ISS_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncIssHwConfigPortIsolationEntrySync (NpSync.
                                                         IssHwConfigPortIsolationEntry,
                                                         RM_ISS_APP_ID,
                                                         NPSYNC_ISS_HW_CONFIG_PORT_ISOLATION_ENTRY);
            }
        }
#ifdef NPAPI_WANTED
#undef IsssysIssHwConfigPortIsolationEntry
        i4RetVal =
            IsssysIssHwConfigPortIsolationEntry (pIssHwUpdtPortIsolation);
        if (IssPortRmGetStandbyNodeCount () != 0)
        {
            if (ISS_NPSYNC_BLK () == OSIX_FALSE)
            {
                if (i4RetVal == FNP_FAILURE)
                {
                    NpSyncIssHwConfigPortIsolationEntrySync (NpSync.
                                                             IssHwConfigPortIsolationEntry,
                                                             RM_ISS_APP_ID,
                                                             NPSYNC_ISS_HW_CONFIG_PORT_ISOLATION_ENTRY);
                }
            }
        }
#endif /* NPAPI_WANTED */
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (ISS_NPSYNC_BLK () == OSIX_FALSE)
        {
            IssRedUpdateNpSyncBufferTable (&NpSync,
                                           NPSYNC_ISS_HW_CONFIG_PORT_ISOLATION_ENTRY,
                                           ISS_ZERO_ENTRY);
        }
    }
    return (i4RetVal);
}

/*****************************************************************************/
/* Function Name      : IssRedUpdateNpSyncBufferTable                        */
/* Description        : This function Update the NP Sync Buffer              */
/*                      table after processing the NP Sync Message from the  */
/*                      Peer node.                                           */
/* Input(s)           : pNpSync - Pointer to the Union of NP Sync Entry      */
/*                      u4NpApiId - NPAPI ID                                 */
/*                      u4EventId - Event Id                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : gIssGlobalInfo                                       */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
IssRedUpdateNpSyncBufferTable (unNpSync * pNpSync,
                               UINT4 u4NpApiId, UINT4 u4EventId)
{
    tIssRedNpSyncEntry *pBuf = NULL;
    tIssRedNpSyncEntry *pTempBuf = NULL;
    tIssRedNpSyncEntry *pPrevBuf = NULL;
    UINT1               u1Match = OSIX_FALSE;

    TMO_DYN_SLL_Scan (&gIssGlobalInfo.IssRedInfo.HwAuditTable, pBuf,
                      pTempBuf, tIssRedNpSyncEntry *)
    {
        /* If a match exists in the Hw Audit table, then delete the node */
        if ((u4NpApiId != ISS_ZERO_ENTRY) && (u4NpApiId == pBuf->u4NpApiId))
        {
            if (MEMCMP (&(pBuf->unNpData), pNpSync,
                        sizeof (unNpSync)) == ISS_ZERO_ENTRY)
            {
                TMO_SLL_Delete (&gIssGlobalInfo.IssRedInfo.
                                HwAuditTable, &(pBuf->Node));
                MemReleaseMemBlock (gIssGlobalInfo.IssRedInfo.
                                    HwAuditTablePoolId, (UINT1 *) pBuf);
                pBuf = NULL;
                u1Match = OSIX_TRUE;
                break;
            }
        }
        pPrevBuf = pBuf;
    }

    if (u1Match == OSIX_FALSE)
    {
        pTempBuf = NULL;

        pTempBuf = (tIssRedNpSyncEntry *) MemAllocMemBlk
            (gIssGlobalInfo.IssRedInfo.HwAuditTablePoolId);

        if (pTempBuf == NULL)
        {
            ISS_TRC (ALL_FAILURE_TRC, "IssRedUpdateNpSyncBufferTable :"
                     "Hardware Audit entry memory "
                     "allocation failed!!!!\r\n");
            return;
        }

        TMO_SLL_Init_Node (&pTempBuf->Node);

        MEMCPY (&pTempBuf->unNpData, pNpSync, sizeof (unNpSync));
        pTempBuf->u4NpApiId = u4NpApiId;

        pTempBuf->u4EventId = u4EventId;

        if (pPrevBuf == NULL)
        {
            /* Node to be inserted in first position */
            TMO_SLL_Insert (&gIssGlobalInfo.IssRedInfo.HwAuditTable,
                            NULL, (tTMO_SLL_NODE *) & (pTempBuf->Node));
        }
        else
        {
            /* Node to be inserted in last position */
            TMO_SLL_Insert (&gIssGlobalInfo.IssRedInfo.HwAuditTable,
                            (tTMO_SLL_NODE *) & (pPrevBuf->Node),
                            (tTMO_SLL_NODE *) & (pTempBuf->Node));
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : IssRedProcessPeerMsgAtStandby                        */
/* Description        : This function process the RM Message send by         */
/*             the Active Node.                                              */
/* Input(s)           : pMsg - RM Message Data                               */
/*                      u2DataLen = RM Message Data lenght                   */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
IssRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT2               u2Offset = ISS_ZERO_ENTRY;
    UINT4               u4Offset = 0;
    UINT2               u2MinLen = ISS_ZERO_ENTRY;
    UINT2               u2Length = ISS_ZERO_ENTRY;
    UINT2               u2ExtractMsgLen = ISS_ZERO_ENTRY;
    UINT1               u1MsgType = ISS_ZERO_ENTRY;

    MEMSET (&ProtoEvt, ISS_ZERO_ENTRY, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_ISS_APP_ID;
    u2MinLen = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH;

    while (u2Offset <= u2DataLen)
    {
        u2ExtractMsgLen = u2Offset;

        u4Offset = u2Offset;
        NPSYNC_RM_GET_1_BYTE (pMsg, &u4Offset, u1MsgType);
        NPSYNC_RM_GET_2_BYTE (pMsg, &u4Offset, u2Length);
        u2Offset = (UINT2) u4Offset;

        if (u2Length < u2MinLen)
        {
            /* The Length field in the RM packet is less than minimum
             * number of bytes, which is MessageType + Length.    */
            u2Offset = (UINT2) (u2Offset + u2Length);
            continue;
        }

        /* If extracting information upto the length present in the length
         * field goes beyond the total length, means length is not correct
         * discard the remaining information.           */
        u2ExtractMsgLen = (UINT2) (u2ExtractMsgLen + u2Length);

        if (u2ExtractMsgLen > u2DataLen)
        {
            /* The Length field in the RM packet is wrong, hence continuing
             * with the next packet */
            ProtoEvt.u4Error = RM_PROCESS_FAIL;
            if (IssPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
            {
                ISS_TRC (CONTROL_PLANE_TRC, "IssRedProcessPeerMsgAtStandby: "
                         "Acknowledgement to RM for length "
                         "mismatch failed!!\r\n");
            }
            ISS_TRC (CONTROL_PLANE_TRC, "IssRedProcessPeerMsgAtStandby: "
                     "Wrong length message received!!!\r\n");
            return;
        }

        switch (u1MsgType)
        {
            case NPSYNC_MESSAGE:
                IssProcessNpSyncMsg (pMsg, &u2Offset);
                break;
            default:
                break;
        }                        /* End of switch */

        if (u2Offset == u2DataLen)
        {
            /* All data extracted from the buffer */
            break;
        }
    }                            /* End of while */
    return;
}

/*****************************************************************************/
/* Function Name      : IssProcessNpSyncMsg                                  */
/* Description        : This function process the NP Sync message sent by the*/
/*                      Peer node                                            */
/* Input(s)           : Pointer to RM message and Offset                     */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IssProcessNpSyncMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    unNpSync            NpSync;
    UINT4               u4Scan = ISS_ZERO_ENTRY;
    INT4                i4NpApiId = ISS_ZERO_ENTRY;
    UINT4               u4NpApiId = 0;
    UINT4               u4TempOffSet = (UINT4) *pu2OffSet;

    MEMSET (&NpSync.IssHwConfigPortIsolationEntry.IssHwUpdtPortIsolation,
            ISS_ZERO_ENTRY, sizeof (tIssHwUpdtPortIsolation));

    u4NpApiId = (UINT4) i4NpApiId;
    NPSYNC_RM_GET_4_BYTE (pMsg, &u4TempOffSet, u4NpApiId);
    i4NpApiId = (INT4) u4NpApiId;

    switch (i4NpApiId)
    {
        case NPSYNC_ISS_HW_CONFIG_PORT_ISOLATION_ENTRY:
            NPSYNC_RM_GET_4_BYTE (pMsg, &u4TempOffSet,
                                  NpSync.IssHwConfigPortIsolationEntry.
                                  IssHwUpdtPortIsolation.u4IngressPort);

            for (u4Scan = 0; u4Scan < ISS_MAX_UPLINK_PORTS; u4Scan++)
            {
                NPSYNC_RM_GET_4_BYTE (pMsg, &u4TempOffSet,
                                      NpSync.IssHwConfigPortIsolationEntry.
                                      IssHwUpdtPortIsolation.
                                      au4EgressPorts[u4Scan]);
            }                    /* End of for */

            NPSYNC_RM_GET_2_BYTE (pMsg, &u4TempOffSet,
                                  NpSync.IssHwConfigPortIsolationEntry.
                                  IssHwUpdtPortIsolation.InVlanId);
            NPSYNC_RM_GET_2_BYTE (pMsg, &u4TempOffSet,
                                  NpSync.IssHwConfigPortIsolationEntry.
                                  IssHwUpdtPortIsolation.u2NumEgressPorts);
            NPSYNC_RM_GET_1_BYTE (pMsg, &u4TempOffSet,
                                  NpSync.IssHwConfigPortIsolationEntry.
                                  IssHwUpdtPortIsolation.u1Action);

            IssRedUpdateNpSyncBufferTable (&NpSync,
                                           NPSYNC_ISS_HW_CONFIG_PORT_ISOLATION_ENTRY,
                                           ISS_ZERO_ENTRY);
            break;
        default:
            break;
    }                            /* switch */
    *pu2OffSet = (UINT2) u4TempOffSet;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncIssHwConfigPortIsolationEntrySync
 *
 * -------------------------------------------------------------
 */
VOID
NpSyncIssHwConfigPortIsolationEntrySync (tNpSyncIssHwConfigPortIsolationEntry
                                         wConfigPortIsolationEntry,
                                         UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT4               u4Scan = 0;
    UINT2               u2OffSet = 0;
    UINT4               u4OffSet = 0;
    UINT2               u2MsgSize = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = u4AppId;

    u2MsgSize = NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH +
        NPSYNC_MSG_NPAPI_ID_SIZE + sizeof (tIssHwUpdtPortIsolation);

    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        IssPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = 0;
        u4OffSet = u2OffSet;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u4OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u4OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u4OffSet,
                              wConfigPortIsolationEntry.IssHwUpdtPortIsolation.
                              u4IngressPort);
        for (u4Scan = 0; u4Scan < ISS_MAX_UPLINK_PORTS; u4Scan++)
        {
            NPSYNC_RM_PUT_4_BYTE (pMsg, &u4OffSet,
                                  wConfigPortIsolationEntry.
                                  IssHwUpdtPortIsolation.
                                  au4EgressPorts[u4Scan]);
        }                        /* End of for */

        NPSYNC_RM_PUT_2_BYTE (pMsg, &u4OffSet,
                              wConfigPortIsolationEntry.IssHwUpdtPortIsolation.
                              InVlanId);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u4OffSet,
                              wConfigPortIsolationEntry.IssHwUpdtPortIsolation.
                              u2NumEgressPorts);
        NPSYNC_RM_PUT_1_BYTE (pMsg, &u4OffSet,
                              wConfigPortIsolationEntry.IssHwUpdtPortIsolation.
                              u1Action);
        u2OffSet = (UINT2) u4OffSet;
        u2OffSet =   
            (UINT2) (u2OffSet +     
             sizeof (wConfigPortIsolationEntry.IssHwUpdtPortIsolation.      
             au1Reserved));

        if (IssPortRmEnqMsgToRm (pMsg, u2OffSet, u4AppId,
                                 u4AppId) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            IssPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name      : IssPIRedHandleHwAudit                                */
/* Description        : This function Audit the pending entry in the         */
/*                      hardware buffer table for PortIsolation and take     */
/*                      actions accordingly.                                 */
/* Input(s)           : pIssHwAuditPortIsolation- PortIsolation config       */
/*                      information.                                         */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
IssPIRedHandleHwAudit (tIssHwUpdtPortIsolation * pIssHwAuditPortIsolation)
{
    tIssPortIsolationEntry *pIssPortIsolationEntry = NULL;
    tIssHwUpdtPortIsolation IssHwUpdtPortIsolation;
#ifdef NPAPI_WANTED
    INT4                i4RetVal = FNP_FAILURE;
    UINT4               u4Scan = 0;
#endif /* NPAPI_WANTED */

    MEMSET (&IssHwUpdtPortIsolation, 0, sizeof (tIssHwUpdtPortIsolation));

    pIssPortIsolationEntry =
        IssPIGetPortIsolationNode (pIssHwAuditPortIsolation->u4IngressPort,
                                   pIssHwAuditPortIsolation->InVlanId);
    /* Delete the existing entries for this Port Isolation node in hardware.
     * If the entry  exists in software, then re-program the entire node in 
     * hardware. 
     */
#ifdef NPAPI_WANTED
    IssHwUpdtPortIsolation.u4IngressPort =
        pIssHwAuditPortIsolation->u4IngressPort;
    IssHwUpdtPortIsolation.InVlanId = pIssHwAuditPortIsolation->InVlanId;
    IssHwUpdtPortIsolation.u2NumEgressPorts = 0;
    IssHwUpdtPortIsolation.u1Action = ISS_PI_DELETE;
#undef IssHwConfigPortIsolationEntry
    i4RetVal = IssHwConfigPortIsolationEntry (&IssHwUpdtPortIsolation);
    if (i4RetVal == FNP_FAILURE)
    {
        ISS_TRC (ALL_FAILURE_TRC, "IssPIRedHandleHwAudit: "
                 "Failure in programming the hardware \n");
    }
#endif /* NPAPI_WANTED */
    if (NULL != pIssPortIsolationEntry)
    {
#ifdef NPAPI_WANTED
        IssHwUpdtPortIsolation.u4IngressPort =
            pIssPortIsolationEntry->u4IngressIfIndex;
        IssHwUpdtPortIsolation.InVlanId = pIssPortIsolationEntry->InVlanId;
        IssHwUpdtPortIsolation.u1Action = ISS_PI_ADD;
        for (u4Scan = 0; u4Scan < ISS_MAX_UPLINK_PORTS; u4Scan++)
        {
            if (0 == pIssPortIsolationEntry->pu4EgressPorts[u4Scan])
            {
                break;
            }
            IssHwUpdtPortIsolation.au4EgressPorts[u4Scan] =
                pIssPortIsolationEntry->pu4EgressPorts[u4Scan];
        }                        /* End of for */

        IssHwUpdtPortIsolation.u2NumEgressPorts = u4Scan;
#undef IssHwConfigPortIsolationEntry
        i4RetVal = IssHwConfigPortIsolationEntry (&IssHwUpdtPortIsolation);
        if (i4RetVal == FNP_FAILURE)
        {
            ISS_TRC (ALL_FAILURE_TRC, "PIRedHandleHwAudit: "
                     "Failure in programming the hardware \n");
        }
#endif /* NPAPI_WANTED */
    }
    return;
}

/*****************************************************************************/
/* Function Name      : IssRedHwAuditDecBlkCounter                           */
/* Description        : This function decrement the NP Block counter so      */
/*                      that Sync Message can be sent to the peer node when  */
/*                      this counter is zero.                                */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : gIssGlobalInfo                                       */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
IssRedHwAuditDecBlkCounter (VOID)
{
    if (gIssGlobalInfo.IssRedInfo.u1NpSyncBlockCount != ISS_ZERO_ENTRY)
    {
        gIssGlobalInfo.IssRedInfo.u1NpSyncBlockCount--;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : IssRedHwAuditIncBlkCounter                           */
/* Description        : This function increment the NP Block counter so      */
/*                      that Sync Message will not be sent to the peer node. */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : gIssGlobalInfo                                       */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
IssRedHwAuditIncBlkCounter (VOID)
{
    gIssGlobalInfo.IssRedInfo.u1NpSyncBlockCount++;
    return;
}

/*****************************************************************************/
/* Function Name      : IssRedHandleIdleToActive                             */
/* Description        : This function handles the GO_ACTIVE event when the   */
/*                      node transition from Idle to Active State            */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : gIssGlobalInfo                                       */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
IssRedHandleIdleToActive (VOID)
{
    gIssGlobalInfo.IssRedInfo.u1NodeStatus = RM_ACTIVE;
    /* Np Sync-ups must be sent from Active node, hence the unblocking
     * by decrementing the counter. It was blocked on Boot-up.   */
    IssRedHwAuditDecBlkCounter ();
    return;
}

/*****************************************************************************/
/* Function Name      : IssRedHandleIdleToStandby                            */
/* Description        : This function handles the GO_Standby event when the  */
/*                      node transition from Idle to Standby State           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : gIssGlobalInfo                                       */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
IssRedHandleIdleToStandby (VOID)
{
    gIssGlobalInfo.IssRedInfo.u1NodeStatus = RM_STANDBY;
    /* Np Sync-ups must be processed by Standby node, hence the unblocking
     * by decrementing the counter. It was blocked on Boot-up.   */
    IssRedHwAuditDecBlkCounter ();
    return;
}

/*****************************************************************************/
/* Function Name      : IssRedHandleActiveToStandby                          */
/* Description        : This function handles the GO_Standby event when the  */
/*                      node transition from Active to Standby State         */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : gIssGlobalInfo                                       */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
IssRedHandleActiveToStandby (VOID)
{
    gIssGlobalInfo.IssRedInfo.u1NodeStatus = RM_STANDBY;
    /* During Force-switchover, the Np Sync-ups has to be blocked here, so
     * that bulk updates does not access the Np Sync Buffer table */
    IssRedHwAuditIncBlkCounter ();
    return;
}

/*****************************************************************************/
/* Function Name      : IssRedHandleStandbyToActive                          */
/* Description        : This function handles the GO_ACTIVE event when the   */
/*                      node transition from Standby to Active State         */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : gIssGlobalInfo                                       */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
PUBLIC VOID
IssRedHandleStandbyToActive (VOID)
{
    /* Update the Node Status */
    gIssGlobalInfo.IssRedInfo.u1NodeStatus = RM_ACTIVE;

    /* During Force-switchover, the Np Sync-ups has to be blocked here, so
     * that bulk updates does not access the Np Sync Buffer table */
    IssRedHwAuditIncBlkCounter ();
    return;
}

/*****************************************************************************/
/* Function Name      : IssRedHandleDynSyncAudit                             */
/*                                                                           */
/* Description        : This function Handles the Dynamic Sync-up Audit      */
/*                      event. This event is used to start the audit         */
/*                      between the active and standby units for the         */
/*                      dynamic sync-up happened                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
IssRedHandleDynSyncAudit ()
{
    IssExecuteCmdAndCalculateChkSum ();
    return;
}

/*****************************************************************************/
/* Function Name      : IssExecuteCmdAndCalculateChkSum                     */
/*                                                                           */
/* Description        : This function Handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
IssExecuteCmdAndCalculateChkSum (VOID)
{
    /*Execute CLI commands and calculate checksum */
    UINT2               u2AppId = RM_ISS_APP_ID;
    UINT2               u2ChkSum = 0;

    ISS_UNLOCK ();
    if (IssGetShowCmdOutputAndCalcChkSum (&u2ChkSum) == ISS_FAILURE)
    {
        ISS_LOCK ();
        ISS_TRC (ALL_FAILURE_TRC, "Checksum of calculation failed for VCM\n");
        return;
    }

    if (IssRmEnqChkSumMsgToRm (u2AppId, u2ChkSum) == ISS_FAILURE)
    {
        ISS_LOCK ();
        ISS_TRC (ALL_FAILURE_TRC, "Sending checkum to RM failed\n");
        return;
    }
    ISS_LOCK ();
    return;
}

#endif /* ISSRED_C */
