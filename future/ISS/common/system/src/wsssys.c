/****************************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: wsssys.c,v 1.4 2017/05/23 14:21:45 siva Exp $
* 
* Description: This file has the routines for the WSS Initialization related
*              calls. 
*
******************************************************************************/

#ifndef __WSSSYS_C__
#define __WSSSYS_C__

#include "issinc.h"

#ifdef WTP_WANTED
extern tWTPNVRAM_DATA sWtpNvRamData;
extern tNVRAM_DATA  sNvRamData;
#endif

#ifdef WLC_WANTED
extern tWLCNVRAM_DATA sWlcNvRamData;
#endif

#ifndef NPAPI_WANTED
#define FNP_SUCCESS      1
#define FNP_FAILURE      0
#endif

/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssReadSystemInfoFromWssNvRam                    */
/*                                                                          */
/*    Description        : This function is invoked to read the System info */
/*                         from the NVRAM.                                  */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
WssReadSystemInfoFromWssNvRam ()
{
    /* Read the WSS NVRAM Files */
#ifdef WTP_WANTED
    if (WtpNvRamRead (&sWtpNvRamData) != FNP_SUCCESS)
    {
        if (WtpNvRamWrite (&sWtpNvRamData) != FNP_SUCCESS)
        {
            ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to WTP NvRam\n");
        }
    }
#endif
#ifdef WLC_WANTED
    if (WlcNvRamRead (&sWlcNvRamData) != FNP_SUCCESS)
    {
        if (WlcNvRamWrite (&sWlcNvRamData) != FNP_SUCCESS)
        {
            ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to WLC NvRam\n");
        }
    }
#endif
}

#ifdef WTP_WANTED
/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssInitialiseWtpNvRamDefVal                      */
/*                                                                          */
/*    Description        : This function initialises WTP NvRam with the     */
/*                         default values                                   */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : VOID                                             */
/****************************************************************************/
VOID
WssInitialiseWtpNvRamDefVal (VOID)
{

    sWtpNvRamData.u2AcReferralType = WSS_DEFAULT_AC_REFERRAL_TYPE;

    sWtpNvRamData.u2WtpCapwapMacType = WSS_DEFAULT_CAPWAP_MAC_TYPE;

    sWtpNvRamData.u2WtpCapwapTunnelMode = WSS_DEFAULT_CAPWAP_TUNL_MODE;

    sWtpNvRamData.u1DiscoveryType = WSS_DEFAULT_DISCOVERY_TYPE;

    ISS_MEMCPY (sWtpNvRamData.ai1WtpModelNumber,
                WSS_DEFAULT_WTP_MODEL_NUMBER,
                STRLEN (WSS_DEFAULT_WTP_MODEL_NUMBER));
    sWtpNvRamData.ai1WtpModelNumber[STRLEN (WSS_DEFAULT_WTP_MODEL_NUMBER)]
        = '\0';

    ISS_MEMCPY (sWtpNvRamData.ai1WtpSerialNumber,
                WSS_DEFAULT_WTP_SERIAL_NUMBER,
                STRLEN (WSS_DEFAULT_WTP_SERIAL_NUMBER));
    sWtpNvRamData.ai1WtpSerialNumber[STRLEN (WSS_DEFAULT_WTP_SERIAL_NUMBER)]
        = '\0';

    ISS_MEMCPY (sWtpNvRamData.ai1WtpBoardId,
                WSS_DEFAULT_WTP_BOARD_ID, STRLEN (WSS_DEFAULT_WTP_BOARD_ID));
    sWtpNvRamData.ai1WtpBoardId[STRLEN (WSS_DEFAULT_WTP_BOARD_ID)] = '\0';

    ISS_MEMCPY (sWtpNvRamData.ai1WtpBoardRevision,
                WSS_DEFAULT_WTP_BOARD_REVISION,
                STRLEN (WSS_DEFAULT_WTP_BOARD_REVISION));
    sWtpNvRamData.ai1WtpBoardRevision[STRLEN (WSS_DEFAULT_WTP_BOARD_REVISION)]
        = '\0';

    sWtpNvRamData.u4CapwapUdpServerPort = WSS_DEFAULT_CAPWAP_UDP_SERVER_PORT;
    sWtpNvRamData.u4NativeVlan = WSS_DEFAULT_NATIVE_VLAN;

    sWtpNvRamData.u4WlcIpAddr = WSS_DEFAULT_WLC_IP_ADDR;

    sWtpNvRamData.u4DtlsKeyType = WSS_DEFAULT_DTLS_KEY_TYPE;

    /* Write to WTP NvRam here */
    if (WtpNvRamWrite (&sWtpNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to WTP NvRam\n");
        return;
    }
#ifdef NPAPI_WANTED
    WssReadSwitchMac ();
#endif
}
#endif

#ifdef WLC_WANTED
/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssInitialiseWlcNvRamDefVal                      */
/*                                                                          */
/*    Description        : This function initialises WLC NvRam with the     */
/*                         default values                                   */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : VOID                                             */
/****************************************************************************/
VOID
WssInitialiseWlcNvRamDefVal (VOID)
{
    sWlcNvRamData.u4CapwapUdpServerPort = WSS_DEFAULT_CAPWAP_UDP_SERVER_PORT;

    sWlcNvRamData.u4DtlsKeyType = WSS_DEFAULT_DTLS_KEY_TYPE;

    /* Write to WLC NvRam here */
    if (WlcNvRamWrite (&sWlcNvRamData) != FNP_SUCCESS)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to write to WLC NvRam\n");
        return;
    }
}
#endif

#ifdef WTP_WANTED
#ifdef NPAPI_WANTED
/****************************************************************************/
/*                                                                          */
/*    Function Name      : WssReadSwitchMac                                 */
/*                                                                          */
/*    Description        : This function reads the MAC Address from linux   */
/*                         and update the issnvram file                     */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : VOID                                             */
/****************************************************************************/
VOID
WssReadSwitchMac (VOID)
{
    INT4                i4fd;
    struct ifreq        ifr;
    UINT1              *u1Iface = BASE_MAC_INTERFACE;
    UINT1              *u1Mac;
    INT4                i4RetVal = 0;

    /* ISS_MEMCPY (iface, 6); */
    i4fd = socket (AF_INET, SOCK_DGRAM, 0);

    ifr.ifr_addr.sa_family = AF_INET;
    STRNCPY (ifr.ifr_name, u1Iface, IFNAMSIZ - 1);

    i4RetVal = ioctl (i4fd, SIOCGIFHWADDR, &ifr);

    if (i4RetVal != 0)
    {
        ISS_TRC (ALL_FAILURE_TRC, "FAILED to read Switch-Base-Mac\n");
        return;
    }
    close (i4fd);

    u1Mac = (UINT1 *) ifr.ifr_hwaddr.sa_data;

    ISS_MEMCPY (sNvRamData.au1SwitchMac, u1Mac, 6);
    IssSetSwitchBaseMacAddressToNvRam (sNvRamData.au1SwitchMac);
}
#endif
#endif
#endif    /*_WSSSYS_C */
