#include "issinc.h"

/* -------------------------------------------------------------
 *
 * Function: nmhGetFirstIndexIssIpAuthMgrTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetFirstIndexIssIpAuthMgrTable (UINT4 *pu4IssIpAuthMgrIpAddr,
                                   UINT4 *pu4IssIpAuthMgrIpMask)
{
    int                 rc;
    tNmhGetFirstIndexIssIpAuthMgrTable lv;

    lv.cmd = NMH_GETFIRST_IPAUTH_TABLE;
    lv.pu4IssIpAuthMgrIpAddr = pu4IssIpAuthMgrIpAddr;
    lv.pu4IssIpAuthMgrIpMask = pu4IssIpAuthMgrIpMask;

    rc = ioctl (gi4DevFd, IPAUTH_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetNextIndexIssIpAuthMgrTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetNextIndexIssIpAuthMgrTable (UINT4 u4IssIpAuthMgrIpAddr,
                                  UINT4 *pu4NextIssIpAuthMgrIpAddr,
                                  UINT4 u4IssIpAuthMgrIpMask,
                                  UINT4 *pu4NextIssIpAuthMgrIpMask)
{
    int                 rc;
    tNmhGetNextIndexIssIpAuthMgrTable lv;

    lv.cmd = NMH_GETNEXT_IPAUTH_TABLE;
    lv.u4IssIpAuthMgrIpAddr = u4IssIpAuthMgrIpAddr;
    lv.pu4NextIssIpAuthMgrIpAddr = pu4NextIssIpAuthMgrIpAddr;
    lv.u4IssIpAuthMgrIpMask = u4IssIpAuthMgrIpMask;
    lv.pu4NextIssIpAuthMgrIpMask = pu4NextIssIpAuthMgrIpMask;

    rc = ioctl (gi4DevFd, IPAUTH_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhValidateIndexInstanceIssIpAuthMgrTable
 *
 * -------------------------------------------------------------
 */
INT1
nmhValidateIndexInstanceIssIpAuthMgrTable (UINT4 u4IssIpAuthMgrIpAddr,
                                           UINT4 u4IssIpAuthMgrIpMask)
{
    int                 rc;
    tNmhValidateIndexInstanceIssIpAuthMgrTable lv;

    lv.cmd = NMH_VALIDATE_IDXINST_IPAUTH_TABLE;
    lv.u4IssIpAuthMgrIpAddr = u4IssIpAuthMgrIpAddr;
    lv.u4IssIpAuthMgrIpMask = u4IssIpAuthMgrIpMask;

    rc = ioctl (gi4DevFd, IPAUTH_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetIssIpAuthMgrPortList
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetIssIpAuthMgrPortList (UINT4 u4IssIpAuthMgrIpAddr,
                            UINT4 u4IssIpAuthMgrIpMask,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValIssIpAuthMgrPortList)
{
    int                 rc;
    tNmhGetIssIpAuthMgrPortList lv;

    lv.cmd = NMH_GET_IPAUTH_PORTLIST;
    lv.u4IssIpAuthMgrIpAddr = u4IssIpAuthMgrIpAddr;
    lv.u4IssIpAuthMgrIpMask = u4IssIpAuthMgrIpMask;
    lv.pRetValIssIpAuthMgrPortList = pRetValIssIpAuthMgrPortList;

    rc = ioctl (gi4DevFd, IPAUTH_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetIssIpAuthMgrVlanList
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetIssIpAuthMgrVlanList (UINT4 u4IssIpAuthMgrIpAddr,
                            UINT4 u4IssIpAuthMgrIpMask,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValIssIpAuthMgrVlanList)
{
    int                 rc;
    tNmhGetIssIpAuthMgrVlanList lv;

    lv.cmd = NMH_GET_IPAUTH_VLANLIST;
    lv.u4IssIpAuthMgrIpAddr = u4IssIpAuthMgrIpAddr;
    lv.u4IssIpAuthMgrIpMask = u4IssIpAuthMgrIpMask;
    lv.pRetValIssIpAuthMgrVlanList = pRetValIssIpAuthMgrVlanList;

    rc = ioctl (gi4DevFd, IPAUTH_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetIssIpAuthMgrOOBPort
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetIssIpAuthMgrOOBPort (UINT4 u4IssIpAuthMgrIpAddr,
                           UINT4 u4IssIpAuthMgrIpMask,
                           INT4 *pi4RetValIssIpAuthMgrOOBPort)
{
    int                 rc;
    tNmhGetIssIpAuthMgrOOBPort lv;

    lv.cmd = NMH_GET_IPAUTH_OOBPORT;
    lv.u4IssIpAuthMgrIpAddr = u4IssIpAuthMgrIpAddr;
    lv.u4IssIpAuthMgrIpMask = u4IssIpAuthMgrIpMask;
    lv.pi4RetValIssIpAuthMgrOOBPort = pi4RetValIssIpAuthMgrOOBPort;

    rc = ioctl (gi4DevFd, IPAUTH_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhGetIssIpAuthMgrAllowedServices
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetIssIpAuthMgrAllowedServices (UINT4 u4IssIpAuthMgrIpAddr,
                                   UINT4 u4IssIpAuthMgrIpMask,
                                   INT4 *pi4RetValIssIpAuthMgrAllowedServices)
{
    int                 rc;
    tNmhGetIssIpAuthMgrAllowedServices lv;

    lv.cmd = NMH_GET_IPAUTH_ALLOWEDSERVICES;
    lv.u4IssIpAuthMgrIpAddr = u4IssIpAuthMgrIpAddr;
    lv.u4IssIpAuthMgrIpMask = u4IssIpAuthMgrIpMask;
    lv.pi4RetValIssIpAuthMgrAllowedServices =
        pi4RetValIssIpAuthMgrAllowedServices;

    rc = ioctl (gi4DevFd, IPAUTH_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function:nmhGetIssIpAuthMgrRowStatus 
 *
 * -------------------------------------------------------------
 */
INT1
nmhGetIssIpAuthMgrRowStatus (UINT4 u4IssIpAuthMgrIpAddr,
                             UINT4 u4IssIpAuthMgrIpMask,
                             INT4 *pi4RetValIssIpAuthMgrRowStatus)
{
    int                 rc;
    tNmhGetIssIpAuthMgrRowStatus lv;

    lv.cmd = NMH_GET_IPAUTH_ROWSTATUS;
    lv.u4IssIpAuthMgrIpAddr = u4IssIpAuthMgrIpAddr;
    lv.u4IssIpAuthMgrIpMask = u4IssIpAuthMgrIpMask;
    lv.pi4RetValIssIpAuthMgrRowStatus = pi4RetValIssIpAuthMgrRowStatus;

    rc = ioctl (gi4DevFd, IPAUTH_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetIssIpAuthMgrPortList
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetIssIpAuthMgrPortList (UINT4 u4IssIpAuthMgrIpAddr,
                            UINT4 u4IssIpAuthMgrIpMask,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValIssIpAuthMgrPortList)
{
    int                 rc;
    tNmhSetIssIpAuthMgrPortList lv;

    lv.cmd = NMH_SET_IPAUTH_PORTLIST;
    lv.u4IssIpAuthMgrIpAddr = u4IssIpAuthMgrIpAddr;
    lv.u4IssIpAuthMgrIpMask = u4IssIpAuthMgrIpMask;
    lv.pSetValIssIpAuthMgrPortList = pSetValIssIpAuthMgrPortList;

    rc = ioctl (gi4DevFd, IPAUTH_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function:nmhSetIssIpAuthMgrVlanList 
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetIssIpAuthMgrVlanList (UINT4 u4IssIpAuthMgrIpAddr,
                            UINT4 u4IssIpAuthMgrIpMask,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValIssIpAuthMgrVlanList)
{
    int                 rc;
    tNmhSetIssIpAuthMgrVlanList lv;

    lv.cmd = NMH_SET_IPAUTH_VLANLIST;
    lv.u4IssIpAuthMgrIpAddr = u4IssIpAuthMgrIpAddr;
    lv.u4IssIpAuthMgrIpMask = u4IssIpAuthMgrIpMask;
    lv.pSetValIssIpAuthMgrVlanList = pSetValIssIpAuthMgrVlanList;

    rc = ioctl (gi4DevFd, IPAUTH_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function:nmhSetIssIpAuthMgrOOBPort 
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetIssIpAuthMgrOOBPort (UINT4 u4IssIpAuthMgrIpAddr,
                           UINT4 u4IssIpAuthMgrIpMask,
                           INT4 i4SetValIssIpAuthMgrOOBPort)
{
    int                 rc;
    tNmhSetIssIpAuthMgrOOBPort lv;

    lv.cmd = NMH_SET_IPAUTH_OOBPORT;
    lv.u4IssIpAuthMgrIpAddr = u4IssIpAuthMgrIpAddr;
    lv.u4IssIpAuthMgrIpMask = u4IssIpAuthMgrIpMask;
    lv.i4SetValIssIpAuthMgrOOBPort = i4SetValIssIpAuthMgrOOBPort;

    rc = ioctl (gi4DevFd, IPAUTH_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhSetIssIpAuthMgrAllowedServices
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetIssIpAuthMgrAllowedServices (UINT4 u4IssIpAuthMgrIpAddr,
                                   UINT4 u4IssIpAuthMgrIpMask,
                                   INT4 i4SetValIssIpAuthMgrAllowedServices)
{
    int                 rc;
    tNmhSetIssIpAuthMgrAllowedServices lv;

    lv.cmd = NMH_SET_IPAUTH_ALLOWEDSERVICES;
    lv.u4IssIpAuthMgrIpAddr = u4IssIpAuthMgrIpAddr;
    lv.u4IssIpAuthMgrIpMask = u4IssIpAuthMgrIpMask;
    lv.i4SetValIssIpAuthMgrAllowedServices =
        i4SetValIssIpAuthMgrAllowedServices;

    rc = ioctl (gi4DevFd, IPAUTH_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function:nmhSetIssIpAuthMgrRowStatus 
 *
 * -------------------------------------------------------------
 */
INT1
nmhSetIssIpAuthMgrRowStatus (UINT4 u4IssIpAuthMgrIpAddr,
                             UINT4 u4IssIpAuthMgrIpMask,
                             INT4 i4SetValIssIpAuthMgrRowStatus)
{
    int                 rc;
    tNmhSetIssIpAuthMgrRowStatus lv;

    lv.cmd = NMH_SET_IPAUTH_ROWSTATUS;
    lv.u4IssIpAuthMgrIpAddr = u4IssIpAuthMgrIpAddr;
    lv.u4IssIpAuthMgrIpMask = u4IssIpAuthMgrIpMask;
    lv.i4SetValIssIpAuthMgrRowStatus = i4SetValIssIpAuthMgrRowStatus;

    rc = ioctl (gi4DevFd, IPAUTH_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function:nmhTestv2IssIpAuthMgrPortList 
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2IssIpAuthMgrPortList (UINT4 *pu4ErrorCode, UINT4 u4IssIpAuthMgrIpAddr,
                               UINT4 u4IssIpAuthMgrIpMask,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValIssIpAuthMgrPortList)
{
    int                 rc;
    tNmhTestv2IssIpAuthMgrPortList lv;

    lv.cmd = NMH_TEST_IPAUTH_PORTLIST;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.u4IssIpAuthMgrIpAddr = u4IssIpAuthMgrIpAddr;
    lv.u4IssIpAuthMgrIpMask = u4IssIpAuthMgrIpMask;
    lv.pTestValIssIpAuthMgrPortList = pTestValIssIpAuthMgrPortList;

    rc = ioctl (gi4DevFd, IPAUTH_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function:nmhTestv2IssIpAuthMgrVlanList 
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2IssIpAuthMgrVlanList (UINT4 *pu4ErrorCode, UINT4 u4IssIpAuthMgrIpAddr,
                               UINT4 u4IssIpAuthMgrIpMask,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValIssIpAuthMgrVlanList)
{
    int                 rc;
    tNmhTestv2IssIpAuthMgrVlanList lv;

    lv.cmd = NMH_TEST_IPAUTH_VLANLIST;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.u4IssIpAuthMgrIpAddr = u4IssIpAuthMgrIpAddr;
    lv.u4IssIpAuthMgrIpMask = u4IssIpAuthMgrIpMask;
    lv.pTestValIssIpAuthMgrVlanList = pTestValIssIpAuthMgrVlanList;

    rc = ioctl (gi4DevFd, IPAUTH_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function:nmhTestv2IssIpAuthMgrOOBPort 
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2IssIpAuthMgrOOBPort (UINT4 *pu4ErrorCode,
                              UINT4 u4IssIpAuthMgrIpAddr,
                              UINT4 u4IssIpAuthMgrIpMask,
                              INT4 i4TestValIssIpAuthMgrOOBPort)
{
    int                 rc;
    tNmhTestv2IssIpAuthMgrOOBPort lv;

    lv.cmd = NMH_TEST_IPAUTH_OOBPORT;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.u4IssIpAuthMgrIpAddr = u4IssIpAuthMgrIpAddr;
    lv.u4IssIpAuthMgrIpMask = u4IssIpAuthMgrIpMask;
    lv.i4TestValIssIpAuthMgrOOBPort = i4TestValIssIpAuthMgrOOBPort;

    rc = ioctl (gi4DevFd, IPAUTH_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function:nmhTestv2IssIpAuthMgrAllowedServices 
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2IssIpAuthMgrAllowedServices (UINT4 *pu4ErrorCode,
                                      UINT4 u4IssIpAuthMgrIpAddr,
                                      UINT4 u4IssIpAuthMgrIpMask,
                                      INT4 i4TestValIssIpAuthMgrAllowedServices)
{
    int                 rc;
    tNmhTestv2IssIpAuthMgrAllowedServices lv;

    lv.cmd = NMH_TEST_IPAUTH_ALLOWEDSERVICES;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.u4IssIpAuthMgrIpAddr = u4IssIpAuthMgrIpAddr;
    lv.u4IssIpAuthMgrIpMask = u4IssIpAuthMgrIpMask;
    lv.i4TestValIssIpAuthMgrAllowedServices =
        i4TestValIssIpAuthMgrAllowedServices;

    rc = ioctl (gi4DevFd, IPAUTH_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}

/* -------------------------------------------------------------
 *
 * Function: nmhTestv2IssIpAuthMgrRowStatus
 *
 * -------------------------------------------------------------
 */
INT1
nmhTestv2IssIpAuthMgrRowStatus (UINT4 *pu4ErrorCode, UINT4 u4IssIpAuthMgrIpAddr,
                                UINT4 u4IssIpAuthMgrIpMask,
                                INT4 i4TestValIssIpAuthMgrRowStatus)
{
    int                 rc;
    tNmhTestv2IssIpAuthMgrRowStatus lv;

    lv.cmd = NMH_TEST_IPAUTH_ROWSTATUS;
    lv.pu4ErrorCode = pu4ErrorCode;
    lv.u4IssIpAuthMgrIpAddr = u4IssIpAuthMgrIpAddr;
    lv.u4IssIpAuthMgrIpMask = u4IssIpAuthMgrIpMask;
    lv.i4TestValIssIpAuthMgrRowStatus = i4TestValIssIpAuthMgrRowStatus;

    rc = ioctl (gi4DevFd, IPAUTH_NMH_IOCTL, &lv);
    return (rc < 0 ? (INT1) SNMP_FAILURE : lv.rval);
}
