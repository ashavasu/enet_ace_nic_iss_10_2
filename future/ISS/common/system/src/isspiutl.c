/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: isspiutl.c,v 1.15 2015/12/29 12:00:52 siva Exp $
 *
 * Description : This file contains access routines  for        
 *               performing well defined operations in the     
 *               port isolation table.                       
 *
 *****************************************************************************/

/*****************************************************************************/
/*    FILE  NAME            : isspiutl.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Port Isolation                                 */
/*    MODULE NAME           : ISS                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Linux                                          */
/*    DATE OF FIRST RELEASE : 13 July 2010                                   */
/*    AUTHOR                : Aricent Inc.                                   */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    13 July 2010           Initial Creation                        */
/*---------------------------------------------------------------------------*/
#ifndef ISSPIUTL_C
#define ISSPIUTL_C

#include "issinc.h"
#include "params.h"
#include "fssnmp.h"
#include "isspiinc.h"
#include "isspinp.h"
#ifdef L2RED_WANTED
#include "hwaudmap.h"
#endif /* L2RED_WANTED */

tIssPIGlobalInfo    gIssPIGlobalInfo;
/*****************************************************************************/
/*  Function Name   : IssPIInitPortIsolationTable                            */
/*  Description     : This function does the following                       */
/*                    1. Creates mempools for Port Isolation entry creation. */
/*                    2. Creates Red Black tree for Port Isolation table     */
/*  Input(s)        : None                                                   */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :(i) gIssPIGlobalInfo.IssPIEntryCtrlPoolId   */
/*                               (ii) gIssPIGlobalInfo.IssPIEgressPortPoolId */
/*                               (iii) gIssPIGlobalInfo.PortIsolationTable   */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ISS_FAILURE on failure                    */
/*                                 ISS_SUCCESS on success                    */
/*****************************************************************************/
INT4
IssPIInitPortIsolationTable (VOID)
{
    /* Initialize the global variables of Port Isolation. */
    gIssPIGlobalInfo.PortIsolationTable = NULL;

    gIssPIGlobalInfo.IssPIEntryCtrlPoolId =
        SYSTEMMemPoolIds[MAX_ISS_PORT_ISOLATION_ENTRIES_SIZING_ID];
    gIssPIGlobalInfo.IssPIEgressPortPoolId =
        SYSTEMMemPoolIds[MAX_ISS_PI_EGRESS_PORT_ENTRIES_SIZING_ID];

    /* Create Red Black Tree for Port Isolation Table. */
    gIssPIGlobalInfo.PortIsolationTable =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tIssPortIsolationEntry,
                                             PortIsolationNode),
                              (tRBCompareFn) IssPIUtilPITblCmpFn);
    if (NULL == gIssPIGlobalInfo.PortIsolationTable)
    {
        ISS_TRC (OS_RESOURCE_TRC,
                 "\nIssPIInitPortIsolationTable -RB Tree Creation \
                   for Port Isolation Table failed. ");
        IssPIDeInitPortIsolationTable ();
        return ISS_FAILURE;
    }

    /* No Semaphore creation is needed inside this RBTree. */
    RBTreeDisableMutualExclusion (gIssPIGlobalInfo.PortIsolationTable);

    return ISS_SUCCESS;
}                                /*End of IssPIInitPortIsolationTable  */

/*****************************************************************************/
/*  Function Name   : IssPIDeInitPortIsolationTable                          */
/*  Description     : This function de-initialize the following which are    */
/*                    allocated using IssPIInitPortIsolationTable call       */
/*                    (i) MemPools for Port Isolation Entry and Egress Ports */
/*                    (ii) RB Tree for Port Isolation Table maintanence.     */
/*  Input(s)        :   None                                                 */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :(i) gIssPIGlobalInfo.IssPIEntryCtrlPoolId   */
/*                               (ii) gIssPIGlobalInfo.IssPIEgressPortPoolId */
/*                               (iii) gIssPIGlobalInfo.PortIsolationTable   */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
VOID
IssPIDeInitPortIsolationTable (VOID)
{
    if (gIssPIGlobalInfo.PortIsolationTable != NULL)
    {
        /* Delete the RB Tree for Port Isolation. */
        RBTreeDestroy (gIssPIGlobalInfo.PortIsolationTable,
                       (tRBKeyFreeFn) IssPIUtilFreePIEntry, ISS_PI_INVALID_ID);
        gIssPIGlobalInfo.PortIsolationTable = NULL;
    }
    return;
}                                /* End of IssPIDeInitPortIsolationTable */

/*****************************************************************************/
/*  Function Name   :  IssPIInitPortIsolationEntry                           */
/*  Description     : (i) Allocates memory for port isolation entry from the */
/*                        Port Isolation Entry Mempool.                      */
/*                    (ii) Initializes the port isolation entry as follows   */
/*                         a.Sets u4IngressPortIfIndex field of the entry as */
/*                           the given input port                            */
/*                         b. Allocates memory for the *pu4EgressPorts from  */
/*                            the Port Isolation Egress Ports Mempool. Also  */
/*                            does a memset for this newly allocated memory. */
/*                         c. Set the vlan Id information.                   */
/*                         d. Insert the Node in RBTree using RBTreeAdd call */
/*  Input(s)        :  u4IngressIfIndex                                      */
/*  Output(s)       :  None                                                  */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  pIssPortIsolationEntry on successful      */
/*                                 addition to RB Tree.                      */
/*                                 NULL  on Failure processing.              */
/*****************************************************************************/
tIssPortIsolationEntry *
IssPIInitPortIsolationEntry (UINT4 u4IngressIfIndex, tVlanId InVlanId)
{
    tIssPortIsolationEntry *pIssPortIsolationEntry = NULL;

    pIssPortIsolationEntry = (tIssPortIsolationEntry *)
        MemAllocMemBlk (gIssPIGlobalInfo.IssPIEntryCtrlPoolId);
    if (NULL == pIssPortIsolationEntry)
    {
        ISS_TRC (OS_RESOURCE_TRC,
                 "\nIssInitPortIsolationEntry -  Memory Allocation for  \
                    Port Isolation Entry Failed. ");
        return NULL;
    }

    MEMSET (pIssPortIsolationEntry, 0, sizeof (tIssPortIsolationEntry));

    pIssPortIsolationEntry->u4IngressIfIndex = u4IngressIfIndex;
    pIssPortIsolationEntry->InVlanId = InVlanId;

    /* Allocate memory for Egress Port List */
    pIssPortIsolationEntry->pu4EgressPorts = (UINT4 *)
        MemAllocMemBlk (gIssPIGlobalInfo.IssPIEgressPortPoolId);
    if (NULL == pIssPortIsolationEntry->pu4EgressPorts)
    {
        ISS_TRC (OS_RESOURCE_TRC,
                 "\nIssInitPortIsolationEntry -  Memory Allocation for  \
                     Egress ports per ingress port Failed. ");
        MemReleaseMemBlock (gIssPIGlobalInfo.IssPIEntryCtrlPoolId,
                            (UINT1 *) pIssPortIsolationEntry);
        return NULL;
    }

    MEMSET (pIssPortIsolationEntry->pu4EgressPorts, 0,
            (sizeof (UINT4) * ISS_MAX_UPLINK_PORTS));

    /* Set the storage type of this entry to volatile  by  default. */
    pIssPortIsolationEntry->u1StorageClass = ISS_PI_VOLATILE;

    if (RB_SUCCESS != RBTreeAdd (gIssPIGlobalInfo.PortIsolationTable,
                                 (tRBElem *) pIssPortIsolationEntry))
    {
        MemReleaseMemBlock (gIssPIGlobalInfo.IssPIEgressPortPoolId,
                            (UINT1 *) (pIssPortIsolationEntry->pu4EgressPorts));
        MemReleaseMemBlock (gIssPIGlobalInfo.IssPIEntryCtrlPoolId,
                            (UINT1 *) pIssPortIsolationEntry);
        return NULL;
    }

    return pIssPortIsolationEntry;
}                                /* End of IssPIInitPortIsolationEntry */

/*****************************************************************************/
/*  Function Name   : IssPIUpdtPortIsolationEntry                            */
/*  Description     : This function is used to do the following              */
/*                    1. Add egress port(s) to port isolation                */
/*                       entry                                               */
/*                    2. If an entry does not exists for (ingress, vlan pair */
/*                    3. Create a port isolation entry and add the egress    */
/*                       port(s) to that list.                               */
/*                    4. Remove a port or list of ports from the egress ports*/
/*                       for a port isolation entry.                         */
/*                    5. Remove a port isolation entry                       */
/*  Input(s)        :  pIssUpdtPortIsolation - port Isolation Entry values   */
/*  Output(s)       :  None                                                  */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ISS_SUCCESS on success                    */
/*                                 ISS_FAILURE on failure                    */
/*****************************************************************************/
INT4
IssPIUpdtPortIsolationEntry (tIssUpdtPortIsolation * pIssUpdtPortIsolation)
{
    tIssPortIsolationEntry *pIssPortIsolationEntry = NULL;
    tIssHwUpdtPortIsolation IssHwPortIsolationEntry;
    UINT4               u4PIScan = 0;
    INT4                i4RetVal = ISS_FAILURE;
    INT1                i1Free = ISS_FALSE;

    MEMSET (&IssHwPortIsolationEntry, 0, sizeof (tIssHwUpdtPortIsolation));

    /* Fetch the Port Isolation Entry from the Hash Table corresponding to this
     * Ingress port 
     */
    pIssPortIsolationEntry =
        IssPIGetPortIsolationNode (pIssUpdtPortIsolation->u4IngressPort,
                                   pIssUpdtPortIsolation->InVlanId);

    switch (pIssUpdtPortIsolation->u1Action)
    {
            /* Addition of egress ports */
        case ISS_PI_ADD:

            if (NULL == pIssPortIsolationEntry)
            {
                /* If an entry does not exists already for this ingress port, 
                 * vlan Id then create a new PI Entry in the Port Isolation 
                 * table. 
                 */
                if (NULL == (pIssPortIsolationEntry =
                             IssPIInitPortIsolationEntry
                             (pIssUpdtPortIsolation->u4IngressPort,
                              pIssUpdtPortIsolation->InVlanId)))
                {
                    return ISS_FAILURE;
                }
            }

            /* Scan through the list of egress ports and add the egress ports
             * one by one. 
             */
            i4RetVal = ISS_SUCCESS;
            for (u4PIScan = 0;
                 u4PIScan < pIssUpdtPortIsolation->u2NumEgressPorts; u4PIScan++)
            {
                if (IssPIAddPIEntryEgressPorts (pIssPortIsolationEntry->
                                                pu4EgressPorts,
                                                pIssUpdtPortIsolation->
                                                pu4EgressPorts[u4PIScan]) ==
                    ISS_FAILURE)
                {
                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\n IssPIAddPIEntryEgressPorts- The port cannot be added  \
                         in the egress port list since  it exceeds the limit :\
                         %d\r\n", pIssUpdtPortIsolation->
                                  pu4EgressPorts[u4PIScan]);
                    i4RetVal = ISS_FAILURE;
                    break;
                }
            }                    /* End of For */

            /* To Update the same in Hardware, After adding the port */
            IssHwPortIsolationEntry.u2NumEgressPorts = 0;
            /* Count the number of Egress ports in the list */
            while ((pIssPortIsolationEntry->pu4EgressPorts
                    [IssHwPortIsolationEntry.u2NumEgressPorts] != 0)
                   && (IssHwPortIsolationEntry.u2NumEgressPorts <
                       ISS_MAX_UPLINK_PORTS))
            {
                IssHwPortIsolationEntry.u2NumEgressPorts++;
            }
            MEMCPY (IssHwPortIsolationEntry.au4EgressPorts,
                    pIssPortIsolationEntry->pu4EgressPorts,
                    ((sizeof (UINT4)) *
                     IssHwPortIsolationEntry.u2NumEgressPorts));
            break;

            /* Request for deleting egress ports */
        case ISS_PI_DELETE:

            /* Entry is not available for deletion */
            if (NULL == pIssPortIsolationEntry)
            {
                ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                              "IssPIUpdtPortIsolationEntry - Port Isolation \
                             Entry is not available for the port : %d\r\n", pIssUpdtPortIsolation->u4IngressPort);
                return ISS_FAILURE;
            }

            /* Scan through the number of egress ports requested for deletion. 
             * and delete the ports one by one using the function 
             * IssPIDelPIEntryEgressPorts.
             */
            for (u4PIScan = 0;
                 u4PIScan < pIssUpdtPortIsolation->u2NumEgressPorts; u4PIScan++)
            {
                if (IssPIDelPIEntryEgressPorts (pIssPortIsolationEntry->
                                                pu4EgressPorts,
                                                pIssUpdtPortIsolation->
                                                pu4EgressPorts[u4PIScan],
                                                &i1Free) == ISS_FAILURE)
                {

                    ISS_TRC_ARG1 (ALL_FAILURE_TRC,
                                  "\n IssPIDelPIEntryEgressPorts- The port requested for\
                     deletion is not found in the egress port list : %d\r\n",
                                  pIssUpdtPortIsolation->pu4EgressPorts[u4PIScan]);
                }
            }                    /* End of for */

            /* Hardware PortList should be updated only after
               port-list is updated in Control Plane */
            IssHwPortIsolationEntry.u2NumEgressPorts = 0;
            /* Count the number of Egress ports in the list */
            while ((pIssPortIsolationEntry->pu4EgressPorts
                    [IssHwPortIsolationEntry.u2NumEgressPorts] != 0)
                   && (IssHwPortIsolationEntry.u2NumEgressPorts <
                       ISS_MAX_UPLINK_PORTS))
            {
                IssHwPortIsolationEntry.u2NumEgressPorts++;
            }
            MEMCPY (IssHwPortIsolationEntry.au4EgressPorts,
                    pIssPortIsolationEntry->pu4EgressPorts,
                    ((sizeof (UINT4)) *
                     IssHwPortIsolationEntry.u2NumEgressPorts));

            /* (i)  If number of egress ports is 0 then it means all the egress
             * ports are to be deleted. 
             * (ii) If the i1Free variable returned by the 
             * delete routine is set to ISS_TRUE, it means that all the egress
             * ports are freed and ingress <==> egress  mapping is not found 
             * in the RB tree node. Hence this node can be safely deleted.  
             */
            if ((0 == pIssUpdtPortIsolation->u2NumEgressPorts) ||
                (ISS_TRUE == i1Free))
            {
                IssHwPortIsolationEntry.u2NumEgressPorts = 0;
                IssPIDeletePortIsolationNode (pIssPortIsolationEntry);
            }

            i4RetVal = ISS_SUCCESS;
            break;

        default:
            ISS_TRC (ALL_FAILURE_TRC,
                     "\nIssUpdtPortIsolationEntry - Invalid Action");
            return ISS_FAILURE;

    }                            /* End of switch */

    /* Update the same in hardware */
    IssHwPortIsolationEntry.u4IngressPort =
        pIssUpdtPortIsolation->u4IngressPort;
    IssHwPortIsolationEntry.u1Action = pIssUpdtPortIsolation->u1Action;
    IssHwPortIsolationEntry.InVlanId = pIssUpdtPortIsolation->InVlanId;
    MEMCPY(IssHwPortIsolationEntry.ExcludeList,
           pIssUpdtPortIsolation->ExcludeList,
	   BRG_PORT_LIST_SIZE);
#ifdef NPAPI_WANTED
    IsssysIssHwConfigPortIsolationEntry (&IssHwPortIsolationEntry);
#endif /* NPAPI_WANTED */

    return i4RetVal;
}                                /* End of IssUpdtPortIsolationEntry */

/*****************************************************************************/
/*  Function Name   :  IssPIGetPortIsolationNode                             */
/*  Description     :  This function returns the RB Tree Node corresponding  */
/*                     to the ingress index and vlan Id as stored in the     */
/*                     Port Isolation table.                                 */
/*  Input(s)        :  u4IngressPort   - Ingress Interface index             */
/*                     InVlanId        - VlanId                              */
/*  Output(s)       :   None                                                 */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling :                          */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  pPortIsolationNode if found in the table  */
/*                                 NULL if not found.                        */
/*****************************************************************************/
tIssPortIsolationEntry *
IssPIGetPortIsolationNode (UINT4 u4IngressPort, tVlanId InVlanId)
{
    tIssPortIsolationEntry IssPortIsolationEntry;

    MEMSET (&IssPortIsolationEntry, 0, sizeof (tIssPortIsolationEntry));

    IssPortIsolationEntry.u4IngressIfIndex = u4IngressPort;
    IssPortIsolationEntry.InVlanId = InVlanId;

    return ((tIssPortIsolationEntry *)
            RBTreeGet (gIssPIGlobalInfo.PortIsolationTable,
                       (tRBElem *) & IssPortIsolationEntry));
}                                /* End of IssPIGetPortIsolationNode */

/*****************************************************************************/
/*  Function Name   :  IssPIDeletePortIsolationNode                          */
/*  Description     :  This function clears memory associated with the       */
/*                     Port Isolation Node and Deletes the node from the RB  */
/*                     tree.                                                 */
/*  Input(s)        :  pIssPortIsolationEntry - pointer to the PI node.      */
/*  Output(s)       :  pIssPortIsolationEntry - pointer to the PI node.      */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Modified   :  None                                      */
/*  Global variables Referred   :  (i) gIssPIGlobalInfo.IssPIEntryCtrlPoolId */
/*                                 (ii)gIssPIGlobalInfo.IssPIEgressPortPoolId*/
/*                                 (iii) gIssPIGlobalInfo.PortIsolationTable */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
VOID
IssPIDeletePortIsolationNode (tIssPortIsolationEntry * pIssPortIsolationEntry)
{
    if (NULL != pIssPortIsolationEntry)
    {
        RBTreeRem (gIssPIGlobalInfo.PortIsolationTable,
                   (tRBElem *) pIssPortIsolationEntry);

        if (NULL != pIssPortIsolationEntry->pu4EgressPorts)
        {
            MemReleaseMemBlock (gIssPIGlobalInfo.IssPIEgressPortPoolId,
                                (UINT1 *) pIssPortIsolationEntry->
                                pu4EgressPorts);
        }

        MemReleaseMemBlock (gIssPIGlobalInfo.IssPIEntryCtrlPoolId,
                            (UINT1 *) pIssPortIsolationEntry);
    }
    return;
}                                /* End of IssPIDeletePortIsolationNode */

/*****************************************************************************/
/*  Function Name   :  IssPIGetFirstPortIsolationNode                        */
/*  Description     :  This API fetches the first node from the entire Port  */
/*                     isolation table                                       */
/*  Input(s)        : None                                                   */
/*  Output(s)       : pu4IngressPort - Ingress Port                          */
/*                    pu4InVlanId    - Ingress  Vlan Id                      */
/*                  : pu4EgressPort  - Egress Port                           */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  gIssPIGlobalInfo.PortIsolationTable       */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
INT1
IssPIGetFirstPortIsolationNode (UINT4 *pu4IngressPort,
                                UINT4 *pu4InVlanId, UINT4 *pu4EgressPort)
{
    tIssPortIsolationEntry *pIssPortIsolationEntry = NULL;

    pIssPortIsolationEntry =
        RBTreeGetFirst (gIssPIGlobalInfo.PortIsolationTable);
    if (NULL == pIssPortIsolationEntry)
    {
        ISS_TRC (ALL_FAILURE_TRC,
                 "\nIssPIGetFirstPortIsolationNode -  Entries are not found in the\
              Port Isolation table  ");
        return ISS_FAILURE;
    }

    *pu4IngressPort = pIssPortIsolationEntry->u4IngressIfIndex;
    *pu4InVlanId = pIssPortIsolationEntry->InVlanId;
    *pu4EgressPort = *(pIssPortIsolationEntry->pu4EgressPorts);
    return ISS_SUCCESS;
}                                /* End of IssPIGetFirstPortIsolationNode */

/*****************************************************************************/
/*  Function Name   : IssPIGetNextPortIsolationNode                          */
/*  Description     : This function fetches from the Port Isolation table    */
/*                    the next entries of the (Ingress port,vlan,Egress Port)*/
/*                    values passed as input.                                */
/*  Input(s)        : u4IngressPort - Ingress Port                           */
/*                    InVlanId      - Ingress Vlan Id                        */
/*                    u4EgressPort  - Egress Port                            */
/*  Output(s)       : pu4NextIngressPort - Next Ingress Port                 */
/*                  : NextInVlanId       - Next Vlan Id                      */
/*                  : pu4NextEgressPort  - Next Egress Port                  */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  gIssPIGlobalInfo.PortIsolationTable       */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/
INT1
IssPIGetNextPortIsolationNode (UINT4 u4IngressPort,
                               tVlanId InVlanId,
                               UINT4 u4EgressPort,
                               UINT4 *pu4NextIngressPort,
                               tVlanId * NextInVlanId, UINT4 *pu4NextEgressPort)
{
    tIssPortIsolationEntry IssPortIsolationEntry;
    tIssPortIsolationEntry *pIssPortIsolationEntry = NULL;
    INT4                i4Scan = 0;
    INT1                i1Found = ISS_FALSE;

    MEMSET (&IssPortIsolationEntry, 0, sizeof (tIssPortIsolationEntry));

    IssPortIsolationEntry.InVlanId = InVlanId;
    IssPortIsolationEntry.u4IngressIfIndex = u4IngressPort;

    pIssPortIsolationEntry = RBTreeGet (gIssPIGlobalInfo.PortIsolationTable,
                                        &IssPortIsolationEntry);
    if (NULL == pIssPortIsolationEntry)
    {
        /* Scenario 1. input (x, 0, 0).
         * If an entry is not found for this (ingress,vlan) pair,
         * then check if 0 is passed for vlan and egress port. If so then
         * fetch the next entry for this ingress port. */
        if ((0 == InVlanId) && (0 == u4EgressPort))
        {
            pIssPortIsolationEntry =
                RBTreeGetNext (gIssPIGlobalInfo.PortIsolationTable,
                               &IssPortIsolationEntry, NULL);
            if ((NULL == pIssPortIsolationEntry)
                || ((NULL != pIssPortIsolationEntry)
                    && (u4IngressPort !=
                        pIssPortIsolationEntry->u4IngressIfIndex)))
            {
                /* If next entry is not found. return failure */
                ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                              "\nIssPIGetNextPortIsolationNode -  Entry not found \
                  (%d, %d) ", u4IngressPort, InVlanId);
                return ISS_FAILURE;
            }
        }
        else
        {
            /* If vlan,egress port values are other than 0 and
             * entry is not found. return failure */
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nIssPIGetNextPortIsolationNode -  Entry not found \
                (%d, %d) ", u4IngressPort, InVlanId);
            return ISS_FAILURE;
        }
    }

    for (i4Scan = 0; i4Scan < ISS_MAX_UPLINK_PORTS; i4Scan++)
    {
        if (0 == pIssPortIsolationEntry->pu4EgressPorts[i4Scan])
        {
            return ISS_FAILURE;
        }

        if (0 == u4EgressPort)
        {
            /* Scenario 2. input (x, y, 0). 
             * If u4EgressPort is passed as 0 then break and return 
             * the first value of this egress list. */
            *pu4NextIngressPort = pIssPortIsolationEntry->u4IngressIfIndex;
            *pu4NextEgressPort = *(pIssPortIsolationEntry->pu4EgressPorts);
            *NextInVlanId = pIssPortIsolationEntry->InVlanId;
            return ISS_SUCCESS;
        }

        /* An egress port matches */
        if (u4EgressPort == pIssPortIsolationEntry->pu4EgressPorts[i4Scan])
        {
            /* Scenario 3. input (x, y, z). 
             * If u4EgressPort is passed as 0 then break and return 
             * the first value of this egress list. */
            i4Scan++;
            if (ISS_MAX_UPLINK_PORTS == i4Scan)
            {
                /* The matched entry is the last one in the egress list. 
                 * hence mark the status as found and break. */
                i1Found = ISS_TRUE;
                break;
            }
            /* Next entry is available in the egress list */
            if (0 != pIssPortIsolationEntry->pu4EgressPorts[i4Scan])
            {
                *pu4NextIngressPort = u4IngressPort;
                *pu4NextEgressPort = pIssPortIsolationEntry->
                    pu4EgressPorts[i4Scan];
                *NextInVlanId = InVlanId;
                return ISS_SUCCESS;
            }
            else
            {
                /* Next egress is not available in the egress list.
                 * hence mark the status as found and break;
                 */
                i1Found = ISS_TRUE;
                break;
            }
        }

    }                            /* End of for */

    if (ISS_TRUE == i1Found)
    {
        /* If a match is not found in the egress list, then fetch the next
         * entry and return the values. */
        pIssPortIsolationEntry = RBTreeGetNext (gIssPIGlobalInfo.
                                                PortIsolationTable,
                                                &IssPortIsolationEntry, NULL);
        if (NULL != pIssPortIsolationEntry)
        {
            *pu4NextIngressPort = pIssPortIsolationEntry->u4IngressIfIndex;
            *pu4NextEgressPort = *(pIssPortIsolationEntry->pu4EgressPorts);
            *NextInVlanId = pIssPortIsolationEntry->InVlanId;
            return ISS_SUCCESS;
        }
        else
        {
            ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                          "\nIssPIGetNextPortIsolationNode -  Entries not found next \
                to %d %d ", u4IngressPort, u4EgressPort);
            return ISS_FAILURE;
        }
    }

    return ISS_FAILURE;
}                                /* End of IssPIGetNextPortIsolationNode */

/*****************************************************************************/
/*  Function Name   : IssPIGetPortIsolationEntry                             */
/*  Description     : This function verifies if the (ingress, vlanId, egress)*/
/*                    passed as input is found in the Port Isolation         */
/*                    table.                                                 */
/*  Input(s)        : i4IngressPort - Ingress Port                           */
/*                    Vlan Id       - Ingress Vlan Id                        */
/*                    i4EgressPort  - Egress Port                            */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  gIssPIGlobalInfo.PortIsolationTable       */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ISS_SUCCESS if this entry is found in PI  */
/*                                 table, else ISS_FAILURE is returned       */
/*****************************************************************************/
INT1
IssPIGetPortIsolationEntry (UINT4 u4IngressPort,
                            tVlanId InVlanId, UINT4 u4EgressPort)
{
    tIssPortIsolationEntry IssPortIsolationEntry;
    tIssPortIsolationEntry *pIssPortIsolationEntry = NULL;
    INT4                i4Scan = 0;

    MEMSET (&IssPortIsolationEntry, 0, sizeof (tIssPortIsolationEntry));

    IssPortIsolationEntry.InVlanId = InVlanId;
    IssPortIsolationEntry.u4IngressIfIndex = u4IngressPort;

    pIssPortIsolationEntry = RBTreeGet (gIssPIGlobalInfo.PortIsolationTable,
                                        &IssPortIsolationEntry);
    if (NULL == pIssPortIsolationEntry)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nIssPIGetNextPortIsolationNode -  Entry not found \
                %d %d ", u4IngressPort, InVlanId);
        return ISS_FAILURE;
    }

    /* There exists a matching entry for the ingress, vlan pair. 
     * The following scan attempts in finding a match for the third
     * index(egress port) in the Port Isolation table. 
     */
    for (i4Scan = 0; i4Scan < ISS_MAX_UPLINK_PORTS; i4Scan++)
    {
        if (0 == pIssPortIsolationEntry->pu4EgressPorts[i4Scan])
        {
            break;
        }

        if (u4EgressPort == pIssPortIsolationEntry->pu4EgressPorts[i4Scan])
        {
            /* Egress port in the PI node matches the input egress port. */
            return ISS_SUCCESS;
        }

    }                            /* End of for */

    return ISS_FAILURE;

}                                /* End of IssPIGetPortIsolationEntry */

/*****************************************************************************/
/*  Function Name   : IssPISetPortIsolationStorageType                       */
/*  Description     : This Function sets the storage type of the Port        */
/*                    isolation node with the ingressport as index.          */
/*  Input(s)        : u4IngressPort - Ingress Port                           */
/*                    u4StorageType - Storage type (VOLATILE/NON-VOLATILE)   */
/*  Output(s)       : None                                                   */
/*                  :                                                        */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ISS_SUCCESS on success/                   */
/*                                 ISS_FAILURE if PI node is not found.      */
/*****************************************************************************/
INT1
IssPISetPortIsolationStorageType (UINT4 u4IngressPort,
                                  tVlanId InVlanId, UINT4 u4StorageType)
{
    tIssPortIsolationEntry *pIssPortIsolation = NULL;

    pIssPortIsolation = IssPIGetPortIsolationNode (u4IngressPort, InVlanId);
    if (NULL == pIssPortIsolation)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nIssPISetPortIsolationStorageType - Match not found in the \
                 RB Tree for the ingress port and vlan : (%d %d)\r\n",
                      u4IngressPort, InVlanId);
        return ISS_FAILURE;
    }
    else
    {
        pIssPortIsolation->u1StorageClass = (UINT1) u4StorageType;
    }

    return ISS_SUCCESS;
}                                /* End of IssPISetPortIsolationStorageType */

/*****************************************************************************/
/*  Function Name   : IssPIGetPortIsolationStorageType                       */
/*  Description     : This Function gets the storage type of the Port        */
/*                    isolation node with the ingressport as index.          */
/*  Input(s)        : u4IngressPort - Ingress Port                           */
/*  Output(s)       : pu4StorageType - Storage type (VOLATILE/NON-VOLATILE)  */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ISS_SUCCESS on success/                   */
/*                                 ISS_FAILURE if PI node is not found.      */
/*****************************************************************************/
INT1
IssPIGetPortIsolationStorageType (UINT4 u4IngressPort,
                                  tVlanId InVlanId, UINT1 *pu1StorageType)
{
    tIssPortIsolationEntry *pIssPortIsolation = NULL;

    pIssPortIsolation = IssPIGetPortIsolationNode (u4IngressPort, InVlanId);
    if (NULL == pIssPortIsolation)
    {
        ISS_TRC_ARG2 (ALL_FAILURE_TRC,
                      "\nIssPIGetPortIsolationStorageType - Match not found in the \
                 PI table for the ingress port, VlanId: (%d, %d) \r\n",
                      u4IngressPort, InVlanId);
        return ISS_FAILURE;
    }
    else
    {
        *pu1StorageType = pIssPortIsolation->u1StorageClass;
    }

    return ISS_SUCCESS;
}                                /* End of IssPIGetPortIsolationStorageType */

/*****************************************************************************/
/*  Function Name   : IssPIAddPIEntryEgressPorts                             */
/*  Description     : This Function updates the egress port list of the Port */
/*                    isolation node with the egress port passed as input    */
/*  Input(s)        : u4EgressPort - Egress Port                             */
/*  Output(s)       : *pu4EgressPorts - Updated egress port list.            */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ISS_SUCCESS on successful updation of     */
/*                                 pu4EgressPort array.                      */
/*                                 ISS_FAILURE if addition failed.           */
/*****************************************************************************/
INT1
IssPIAddPIEntryEgressPorts (UINT4 *pu4EgressPorts, UINT4 u4EgressPort)
{
    UINT4               u4TempPort = 0;
    INT4                i4Scan = 0;

    if (u4EgressPort == 0)
    {
        return ISS_SUCCESS;
    }

    if (pu4EgressPorts[ISS_MAX_UPLINK_PORTS - 1] != 0)
    {
        /* Means the egress list is full */
        return ISS_FAILURE;
    }

    for (i4Scan = 0; i4Scan < ISS_MAX_UPLINK_PORTS; i4Scan++)
    {
        if (0 == pu4EgressPorts[i4Scan])
        {
            /* A free slot exists */
            pu4EgressPorts[i4Scan] = u4EgressPort;
            break;
        }

        if (pu4EgressPorts[i4Scan] == u4EgressPort)
        {
            /* This egress port is already there in the list of egress 
             * ports. Return Success itself.  */
            break;
        }

        /* Swap the port with the next Egress Port to maintain
         * the ports in ascending order.
         */
        if (pu4EgressPorts[i4Scan] > u4EgressPort)
        {
            /* If the port in the current index is greater than port
             * to be inserted, swap both the variables so that the 
             * u4EgressPort is inserted in the list and the greater 
             * one will be inserted in the further iterations. 
             * (i.e) list => 1 3 4. u4EgressPort => 2 has to be inserted. 
             * 2nd iteration 3 > 2. list => 1 2 4, u4EgressPort = 3
             * 3rd iteration 4 > 3. list => 1 2 3, u4EgressPort = 4
             * since the 4th slot is empty, 4 will be inserted in the
             * beginning of the loop only. 
             */
            u4TempPort = u4EgressPort;
            u4EgressPort = pu4EgressPorts[i4Scan];
            pu4EgressPorts[i4Scan] = u4TempPort;
        }
    }                            /* End of for */

    return ISS_SUCCESS;
}                                /* End of IssPIAddPIEntryEgressPorts */

/*****************************************************************************/
/*  Function Name   : IssPIDelPIEntryEgressPorts                             */
/*  Description     : This Function updates the egress port list of the Port */
/*                    isolation node with the egress port passed as input    */
/*  Input(s)        : u4EgressPort - Egress Port                             */
/*  Output(s)       : *pu4EgressPorts - Updated egress port list.            */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  ISS_SUCCESS on successful updation of     */
/*                                 pu4EgressPort array.                      */
/*                                 ISS_FAILURE if addition failed.           */
/*****************************************************************************/
INT1
IssPIDelPIEntryEgressPorts (UINT4 *pu4EgressPorts,
                            UINT4 u4EgressPort, INT1 *i1Free)
{
    UINT4               au4TempArray[ISS_MAX_UPLINK_PORTS];
    INT4                i4Scan = 0;
    INT1                i1EntryFound = ISS_FALSE;

    MEMSET (au4TempArray, 0, (sizeof (UINT4) * ISS_MAX_UPLINK_PORTS));

    for (i4Scan = 0; i4Scan < ISS_MAX_UPLINK_PORTS; i4Scan++)
    {
        if (0 == pu4EgressPorts[i4Scan])
        {
            break;
        }

        if (pu4EgressPorts[i4Scan] == u4EgressPort)
        {
            i1EntryFound = ISS_TRUE;
            if ((ISS_MAX_UPLINK_PORTS - 1) == i4Scan)
            {
                /* Delete the entry from the egress list by marking it
                 * as 0.*/
                pu4EgressPorts[i4Scan] = 0;
                break;
            }
            else
            {                    /* Copy the remaining elements in the array to a temp 
                                 * memory. Then again copy the elements from the temp 
                                 * to the egress list from the index of the matching 
                                 * element, thereby replacing the matched egress port.
                                 */
                MEMCPY (au4TempArray,
                        pu4EgressPorts + i4Scan + 1,
                        (sizeof (UINT4) *
                         (UINT4) (ISS_MAX_UPLINK_PORTS - i4Scan - 1)));
                MEMCPY (pu4EgressPorts + i4Scan, au4TempArray,
                        (sizeof (UINT4) *
                         (UINT4) (ISS_MAX_UPLINK_PORTS - i4Scan - 1)));

                if (pu4EgressPorts[ISS_MAX_UPLINK_PORTS - 1] != 0)
                {
                    pu4EgressPorts[ISS_MAX_UPLINK_PORTS - 1] = 0;
                }
                break;
            }
        }

    }                            /* End of for */

    if (0 == pu4EgressPorts[0])
    {
        /* Set the flag to free since it indicates the egress port list is 
         * empty. This flag helps in indicating the deletion of the Port 
         * Isolation node when the egress list is emptied.  
         */
        *i1Free = ISS_TRUE;
        return ISS_SUCCESS;
    }

    if (ISS_FALSE == i1EntryFound)
    {
        return ISS_FAILURE;
    }

    return ISS_SUCCESS;

}                                /* End of IssPIDelPIEntryEgressPorts */

/*****************************************************************************/
/*  Function Name   : IssPIUtilPITblCmpFn                                    */
/*  Description     : This function is invoked by the RB Tree library during */
/*                    RB Tree traversal.                                     */
/*  Input(s)        : pRBElem1 - Pointer to the first PI Entry               */
/*                    pRBElem2 - Pointer to the second PI Entry              */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  1, if Ingress1 is greater than Ingress2   */
/*                                 -1, if Ingress2 is greater than Ingress1  */
/*                                 1, if vlan1 is greater than vlan2         */
/*                                 -1 if vlan2 is greater than vlan1         */
/*                                 0 if(Ingress1==Ingress2) && (vlan1==vlan2)*/
/*****************************************************************************/
INT4
IssPIUtilPITblCmpFn (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tIssPortIsolationEntry *pIssPortIsolationEntry1 = (tIssPortIsolationEntry *)
        pRBElem1;
    tIssPortIsolationEntry *pIssPortIsolationEntry2 = (tIssPortIsolationEntry *)
        pRBElem2;

    if (pIssPortIsolationEntry1->u4IngressIfIndex >
        pIssPortIsolationEntry2->u4IngressIfIndex)
    {
        return ISS_PI_RB_GREATER;
    }

    if (pIssPortIsolationEntry1->u4IngressIfIndex <
        pIssPortIsolationEntry2->u4IngressIfIndex)
    {
        return ISS_PI_RB_LESS;
    }

    if (pIssPortIsolationEntry1->InVlanId > pIssPortIsolationEntry2->InVlanId)
    {
        return ISS_PI_RB_GREATER;
    }

    if (pIssPortIsolationEntry1->InVlanId < pIssPortIsolationEntry2->InVlanId)
    {
        return ISS_PI_RB_LESS;
    }

    return ISS_PI_RB_EQUAL;
}                                /* End of IssPIUtilPITblCmpFn */

/*****************************************************************************/
/*  Function Name   : IssPIUtilFreePIEntry                                   */
/*  Description     : This function is invoked by the RB Tree library during */
/*                    RB Tree Deletion                                       */
/*  Input(s)        : pElem - Pointer to the first PI Entry                  */
/*  Output(s)       : None                                                   */
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Return (s)                  :  OSIX_SUCCESS                              */
/*****************************************************************************/
INT4
IssPIUtilFreePIEntry (tRBElem * pElem, UINT4 u4Arg)
{
    tIssPortIsolationEntry *pIssPortIsolationEntry = (tIssPortIsolationEntry *)
        pElem;
    UNUSED_PARAM (u4Arg);

    if (NULL != pIssPortIsolationEntry->pu4EgressPorts)
    {
        MemReleaseMemBlock (gIssPIGlobalInfo.IssPIEgressPortPoolId,
                            (UINT1 *) (pIssPortIsolationEntry->pu4EgressPorts));
    }

    MemReleaseMemBlock (gIssPIGlobalInfo.IssPIEntryCtrlPoolId,
                        (UINT1 *) (pIssPortIsolationEntry));
    return OSIX_SUCCESS;
}                                /* End of IssPIUtilFreePIEntry */

/*****************************************************************************/
/*  Function Name   : IssPIUtilUpdatePIEntry                                 */
/*  Description     : This Function updates the Port isolation               */
/*                    node for port channel and physical interfaces.         */
/*  Input(s)        : u2IfIndex - port to be added/removed                   */
/*                  : u2AggId - port chalnnel interface Id                   */
/*                  : u1Action (ADD/DELETE)                                  */
/*  Output(s)       : None                                                   */ 
/*  <OPTIONAL Fields>           :  None                                      */
/*  Global Variables Referred   :  None                                      */
/*  Global variables Modified   :  None                                      */
/*  Exceptions or Operating System Error Handling : None                     */
/*  Use of Recursion            :  None                                      */
/*  Returns                     :  None                                      */
/*****************************************************************************/


	VOID
IssPIUtilUpdatePIEntry(UINT2 u2IfIndex, UINT2 u2AggId, UINT1   u1Action )
{

	UINT4                u4IngressIndex = 0;
	UINT4                u4NextIngressIndex = 0;
	UINT4                u4EgressIndex = 0;
	UINT4                u4NextEgressIndex = 0;
	UINT4                u4VlanId = 0;
    tVlanId              NextVlanId = 0;
	INT1                 i1RetVal = 0;
	tIssHwUpdtPortIsolation IssHwPortIsolationEntry;
	tIssUpdtPortIsolation IssUpdtPortIsolation;

	if (IssPIGetFirstPortIsolationNode(&u4IngressIndex,
				&u4VlanId,
				&u4EgressIndex) == ISS_SUCCESS)
	{
		while (1)
		{

			MEMSET (&IssUpdtPortIsolation, 0, sizeof (tIssUpdtPortIsolation));
			MEMSET (&IssHwPortIsolationEntry, 0, sizeof (tIssHwUpdtPortIsolation));
			/*Get next entry is invoked here because the current
			 * entry may be deleted if the action is ISS_PI_DELETE
                         * If a PI entry has 3 egress ports (I1 --- E1,E2,E3)
                         * getNext will be invoked 3 times (I1,E1), (I1,E2) and (I1,E3)
                         */
			i1RetVal = IssPIGetNextPortIsolationNode(u4IngressIndex,
					(tVlanId) u4VlanId,
					u4EgressIndex,
					&u4NextIngressIndex,
					&NextVlanId,
					&u4NextEgressIndex);


			if(u4IngressIndex == (UINT4)u2AggId)
			{
				/* u2IfIndex will be non-zero if a port is removed/added from port-channel
				 * If a port is removed from  a port-channel which is ingress port of port-isolation entry,
				 * the port should be updated with the default egress ports in hardware.
				 */
				if ((u2IfIndex !=0) && (u1Action == ISS_PI_DELETE))
				{
                    IssHwPortIsolationEntry.u4IngressPort = (UINT4)u2IfIndex;
					IssHwPortIsolationEntry.u1Action = u1Action;
					IssHwPortIsolationEntry.InVlanId = u4VlanId;
					IssHwPortIsolationEntry.u2NumEgressPorts = 0;
#ifdef NPAPI_WANTED
					IsssysIssHwConfigPortIsolationEntry (&IssHwPortIsolationEntry);
#endif /* NPAPI_WANTED */
                    break;
                }
				else
				{
                    IssUpdtPortIsolation.pu4EgressPorts = &u4EgressIndex;
					IssUpdtPortIsolation.u4IngressPort = u4IngressIndex;
					IssUpdtPortIsolation.InVlanId = u4VlanId;
					IssUpdtPortIsolation.u2NumEgressPorts = 1;
					IssUpdtPortIsolation.u1Action = u1Action;
					IssPIUpdtPortIsolationEntry (&IssUpdtPortIsolation);
				    break;
                }
			}
			else if (u4EgressIndex == (UINT4)u2AggId)
			{
				/* u2IfIndex will be non-zero if a port is removed/added from port-channel
				 * If a port is removed from a port-channel which is egress port of PI entry,
				 * the port should be removed from the egress list. 
				 * Since the API is invoked after the port is removed from the port-channel,
				 * addition of the same egress port-channel interface to the PI entry
				 * will update the egress list with the current port list.                                 
				 */

				if((u2IfIndex !=0) && (u1Action == ISS_PI_DELETE))
				{
					u1Action = ISS_PI_ADD;
				}
                IssUpdtPortIsolation.pu4EgressPorts = &u4EgressIndex;
				IssUpdtPortIsolation.u4IngressPort = u4IngressIndex;
				IssUpdtPortIsolation.InVlanId = u4VlanId;
				IssUpdtPortIsolation.u2NumEgressPorts = 1;
				IssUpdtPortIsolation.u1Action = u1Action;
				IssPIUpdtPortIsolationEntry (&IssUpdtPortIsolation);
                break;
			}
			else if((u4IngressIndex == (UINT4)u2IfIndex) || (u4EgressIndex == (UINT4)u2IfIndex))
			{
                IssUpdtPortIsolation.pu4EgressPorts = &u4EgressIndex;
				IssUpdtPortIsolation.u4IngressPort = u4IngressIndex;
				IssUpdtPortIsolation.InVlanId = u4VlanId;
				IssUpdtPortIsolation.u2NumEgressPorts = 1;
				IssUpdtPortIsolation.u1Action = u1Action;
				IssPIUpdtPortIsolationEntry (&IssUpdtPortIsolation);
                break;
			}

			if(i1RetVal == ISS_FAILURE)
			{
                break;
			}
            u4IngressIndex = u4NextIngressIndex;
			u4EgressIndex = u4NextEgressIndex;
			u4VlanId = (UINT4)NextVlanId;
            u4NextIngressIndex = 0;
            NextVlanId = 0;
            u4NextEgressIndex = 0;
		}
}
return;
}

#endif /* ISSPIUTL_C */
