/* $Id: systemsz.c,v 1.4 2013/11/29 11:04:11 siva Exp $*/

#define _SYSTEMSZ_C
#include "issinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
SystemSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < SYSTEM_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            (INT4) MemCreateMemPool (FsSYSTEMSizingParams[i4SizingId].
                                     u4StructSize,
                                     FsSYSTEMSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(SYSTEMMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            SystemSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
SystemSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsSYSTEMSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, SYSTEMMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
SystemSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < SYSTEM_MAX_SIZING_ID; i4SizingId++)
    {
        if (SYSTEMMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (SYSTEMMemPoolIds[i4SizingId]);
            SYSTEMMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
