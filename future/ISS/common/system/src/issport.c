/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: issport.c,v 1.66 2017/12/06 09:35:31 siva Exp $
 *
 * Description : This file contains porting functions for ISS.
 *
 *****************************************************************************/

/*****************************************************************************/
/*    FILE  NAME            : issport.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Porting file for iss                           */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
#ifndef _ISSPORT_C
#define _ISSPORT_C
#include <fcntl.h>
#include <time.h>
#include "lr.h"
#include "msr.h"
#include "iss.h"
#include "issmacro.h"
#include "issnp.h"
#include "fsclk.h"

#ifdef OS_VXWORKS
#include <usrLib.h>
#include <stat.h>
#else
#include <sys/stat.h>
#endif

#ifdef OS_PTHREADS
#include "fsapsys.h"
#endif

#include "issinc.h"

INT4                issGetLocalFileSize (const UINT1 *pu1FileName,
                                         UINT4 *pu4Size);
INT4                issCopyLocalFile (UINT1 *pu1Src, UINT1 *pu1Dst);
INT4                issMoveLocalFile (UINT1 *pu1Src, UINT1 *pu1Dst);

static const UINT2  au2DaysInMonth[2][13] = {
    /* Normal Year */
    {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365},
    /* Leap Year   */
    {0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366}
};

static UINT1        gau1CopyBuffer[ISS_ONE_KILO_BYTE + 1];

/****************************************************************************/
/*                                                                          */
/*    Function Name      : issGetLocalFileSize                              */
/*                                                                          */
/*    Description        : Returns the size of the local file               */
/*                                                                          */
/*    Input(s)           : file name and pointer to size                    */
/*                                                                          */
/*    Output(s)          : File size                                        */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ISS_FAILURE                          */
/****************************************************************************/
INT4
issGetLocalFileSize (const UINT1 *pu1FileName, UINT4 *pu4Size)
{
    INT4                i4FileFd = -1;
    INT4                i4FileSize = 0;

    i4FileFd = FileOpen (pu1FileName, OSIX_FILE_RO);

    if (i4FileFd != -1)
    {
        i4FileSize = FileSize (i4FileFd);

        if (i4FileSize == -1)
        {
            FileClose (i4FileFd);
            return (ISS_FAILURE);
        }

        *pu4Size = (UINT4) i4FileSize;
    }
    else
    {
        return (ISS_FAILURE);
    }

    FileClose (i4FileFd);
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssGetLocalDirSize                               */
/*                                                                          */
/*    Description        : Returns the size of the whole directory          */
/*                                                                          */
/*    Input(s)           : Directory name and pointer to size               */
/*                                                                          */
/*    Output(s)          : Sum of sizes of all the contents in that         */
/*                         directory                                        */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ISS_FAILURE                          */
/****************************************************************************/
INT4
IssGetLocalDirSize (CONST CHR1 * pu1DirectoryName, UINT4 *pu4DirectorySize)
{
    FILE               *pTempFile = NULL;
    UINT1               au1Command[ISS_DIR_SIZE_CMD_LEN];
    UINT1               au1TempFileName[ISS_DIR_SIZE_CMD_LEN];
    UINT4               u4EmptyDirSize = 0;

    ISS_MEMSET (au1Command, 0, ISS_DIR_SIZE_CMD_LEN);
    ISS_MEMSET (au1TempFileName, 0, ISS_DIR_SIZE_CMD_LEN);
    if (NULL == pu1DirectoryName)
    {
        ISS_TRC (MGMT_TRC, "Error!!! Directory Does Not Exist \n");
        return ISS_FAILURE;
    }
    SPRINTF ((CHR1 *) au1TempFileName, "%s/Tempfile", pu1DirectoryName);
    SPRINTF ((CHR1 *) au1Command, "du -s %s > %s", pu1DirectoryName,
             au1TempFileName);

    if (ISS_ZERO_ENTRY != system ((const CHR1 *) au1Command))
    {
        ISS_TRC (MGMT_TRC,
                 "Error!!! Failed to execute the command "
                 "for directory size calculation\n");
        return ISS_FAILURE;
    }
    pTempFile = FOPEN ((CONST CHR1 *) au1TempFileName, "r");
    if (NULL == pTempFile)
    {
        ISS_TRC_ARG1 (MGMT_TRC,
                      "Error!!! Failed to open file %s\n", au1TempFileName);
        return ISS_FAILURE;
    }
    if (ISS_ZERO_ENTRY >= fscanf (pTempFile, "%d", pu4DirectorySize))
    {
        ISS_TRC (MGMT_TRC, "Error!!! Failed to get the directory size\n");
        fclose (pTempFile);
        return ISS_FAILURE;
    }
    fclose (pTempFile);

    /* To convert the value from kilobytes to bytes */
    *pu4DirectorySize = (*pu4DirectorySize) * ISS_ONE_KILO_BYTE;

    issGetLocalFileSize ((CONST UINT1 *) pu1DirectoryName, &u4EmptyDirSize);
    *pu4DirectorySize = (*pu4DirectorySize) - u4EmptyDirSize;
    FileDelete ((CONST UINT1 *) au1TempFileName);

    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : issCopyLocalFile                                 */
/*                                                                          */
/*    Description        : Copies local file src to dest                    */
/*                                                                          */
/*    Input(s)           : Src and destination file names                   */
/*                                                                          */
/*    Output(s)          :                                                  */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ISS_FAILURE                          */
/****************************************************************************/
INT4
issCopyLocalFile (UINT1 *pu1Src, UINT1 *pu1Dst)
{
    INT4                i4SrcFd = -1;
    INT4                i4DstFd = -1;
    INT4                i4DataRead = 0;
    INT4                i4DataWrite = 0;
    MEMSET (gau1CopyBuffer, 0, sizeof (gau1CopyBuffer));
    if (pu1Src == NULL || pu1Dst == NULL)
    {
        return (ISS_FAILURE);
    }
#ifdef OS_VXWORKS
    if (copy ((CHR1 *) pu1Src, (CHR1 *) pu1Dst) != 0)
    {
        return (ISS_FAILURE);
    }
#else

    i4SrcFd = FileOpen (pu1Src, OSIX_FILE_RO);

    if (i4SrcFd < 0)
    {
        return (ISS_FAILURE);
    }
    i4DstFd =
        FileOpen (pu1Dst,
                  OSIX_FILE_CR | OSIX_FILE_SY | OSIX_FILE_WO | OSIX_FILE_TR);
    if (i4DstFd < 0)
    {
        FileClose (i4SrcFd);
        return (ISS_FAILURE);
    }

    while ((i4DataRead = (INT4) FileRead (i4SrcFd, (CHR1 *) gau1CopyBuffer,
                                          ISS_ONE_KILO_BYTE)) > 0)
    {
        i4DataWrite =
            (INT4) FileWrite (i4DstFd, (CHR1 *) gau1CopyBuffer,
                              (UINT4) i4DataRead);
        if (i4DataWrite < 0)
        {
            FileClose (i4SrcFd);
            FileClose (i4DstFd);
            return ISS_FAILURE;
        }
    }
    FileClose (i4SrcFd);
    FileClose (i4DstFd);

#endif
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : issMoveLocalFile                                 */
/*                                                                          */
/*    Description        : Move local file Source to Destination            */
/*                                                                          */
/*    Input(s)           : Source and Destination file names                */
/*                                                                          */
/*    Output(s)          :                                                  */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ISS_FAILURE                          */
/****************************************************************************/
INT4
issMoveLocalFile (UINT1 *pu1Src, UINT1 *pu1Dst)
{
    FILE               *pTempFile = NULL;
    UINT1               au1MvStr[ISS_CONFIG_FILE_NAME_LEN +
                                 ISS_CONFIG_FILE_NAME_LEN];

    if (pu1Src == NULL || pu1Dst == NULL)
    {
        return (ISS_FAILURE);
    }

    /*to verify source file presence before making system call */
    pTempFile = FOPEN ((CONST CHR1 *) pu1Src, "r");
    if (NULL == pTempFile)
    {
        ISS_TRC_ARG1 (MGMT_TRC, "Error!!! Failed to open file %s\n", *pu1Src);
        return ISS_FAILURE;
    }
    fclose (pTempFile);

    SPRINTF ((CHR1 *) au1MvStr, "%s %s %s", "mv", pu1Src, pu1Dst);

    if (system ((const CHR1 *) au1MvStr) != 0)
    {
        return (ISS_FAILURE);
    }

    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : issDeleteLocalFile                               */
/*                                                                          */
/*    Description        : Deletes local file                               */
/*                                                                          */
/*    Input(s)           : file name to be deleted.                         */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ISS_FAILURE                          */
/****************************************************************************/
INT4
issDeleteLocalFile (UINT1 *pu1Src)
{
#ifndef OS_VXWORKS
    struct stat         status;
    UINT1               au1FileName[ISS_CONFIG_FILE_NAME_LEN];
    UINT4               u4Len = 0;

    MEMSET (&status, 0, sizeof (struct stat));
    MEMSET (au1FileName, 0, ISS_CONFIG_FILE_NAME_LEN);
    u4Len = ((STRLEN (pu1Src) < (sizeof (au1FileName) - 1))
             ? STRLEN (pu1Src) : (sizeof (au1FileName) - 1));
    STRNCPY (au1FileName, pu1Src, u4Len);
    au1FileName[u4Len] = '\0';
    if (stat ((CHR1 *) pu1Src, &status) < 0)
    {
        return ISS_FAILURE;
    }

    if (S_ISREG (status.st_mode) != 0)
    {

        if (remove ((CHR1 *) au1FileName) != 0)
        {
            return (ISS_FAILURE);
        }
    }
    else
    {
        return ISS_FAILURE;
    }

#else
#ifdef BCMX_WANTED
    extern INT4         flashFsSync (void);

    if (remove ((const CHR1 *) pu1Src) != 0)
    {
        return (ISS_FAILURE);
    }
    flashFsSync ();
#endif
#endif
    /* Pass an event to MSR  Task to erase the MIB */
    if (OsixEvtSend (MsrId, MIB_ERASE_EVENT) != OSIX_SUCCESS)
    {
        return ISS_FAILURE;
    }

    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssDeleteLocalDir                                */
/*                                                                          */
/*    Description        : Deletes all contents in a directory              */
/*                                                                          */
/*    Input(s)           : Directory name whose conetents are to be deleted */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ISS_FAILURE                          */
/****************************************************************************/
INT4
IssDeleteLocalDir (CONST CHR1 * pu1DirectoryName)
{
    UINT1               au1RmCommand[ISS_RM_DIR_CMD_LEN];

    ISS_MEMSET (au1RmCommand, 0, ISS_RM_DIR_CMD_LEN);
    SPRINTF ((CHR1 *) au1RmCommand, "rm -rf %s/*", pu1DirectoryName);
    if (system ((const CHR1 *) au1RmCommand) != 0)
    {
        ISS_TRC (MGMT_TRC,
                 "Error!!! Failed to delete the directory contents\n");
        return (ISS_FAILURE);
    }

    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssStackingSystemRestart                         */
/*                                                                          */
/*    Description        : This function restarts the stacking environment  */
/*                         board.                                           */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
IssStackingSystemRestart ()
{
#if defined (LINUXSIM_WANTED) || defined (NPSIM_WANTED)
#ifndef OS_QNX
    reboot (LINUX_REBOOT_CMD_RESTART);
#endif
#endif
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : issStatStartupFile                               */
/*                                                                          */
/*    Description        : Locates startup file                             */
/*                                                                          */
/*    Input(s)           : Src and destination file names                   */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ISS_FAILURE                          */
/****************************************************************************/
INT4
issStatStartupFile (UINT1 *pu1FileName)
{
    UINT4               u4Size;

    if (issGetLocalFileSize (pu1FileName, &u4Size) != ISS_SUCCESS)
    {
        return (ISS_FAILURE);
    }
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : issSetSwitchDate                                 */
/*                                                                          */
/*    Description        : sets the SwitchDate                              */
/*                                                                          */
/*    Input(s)           : pIssSetSwitchDate                                */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ISS_FAILURE                          */
/****************************************************************************/
UINT4
issSetSwitchDate (tSNMP_OCTET_STRING_TYPE * pIssSetSwitchDate)
{
#ifdef CLKIWF_WANTED
    tClkSysTimeInfo     ClkSysTimeInfo;
#endif

    UINT1               au1Temp[ISS_MAX_DATE_LEN];

    tUtlTm              curTime;
    UINT4               u4Months = 0;
    UINT4               u4Days = 0;
    UINT4               u4Leap = 0;

    MEMSET (&curTime, 0, sizeof (tUtlTm));
    MEMSET (&au1Temp, 0, sizeof (au1Temp));
#ifdef CLKIWF_WANTED
    MEMSET (&ClkSysTimeInfo, 0, sizeof (tClkSysTimeInfo));
#endif

    MEMCPY (au1Temp, pIssSetSwitchDate->pu1_OctetList,
            MEM_MAX_BYTES ((UINT4) pIssSetSwitchDate->i4_Length,
                           sizeof (au1Temp)));

    SSCANF ((CHR1 *) au1Temp, "%u", &(curTime.tm_hour));

    SSCANF ((CHR1 *) au1Temp + 3, "%u", &(curTime.tm_min));

    SSCANF ((CHR1 *) au1Temp + 6, "%u", &(curTime.tm_sec));

    SSCANF ((CHR1 *) au1Temp + 9, "%u", &(curTime.tm_mday));

    SSCANF ((CHR1 *) au1Temp + 12, "%u", &(curTime.tm_mon));
    curTime.tm_mon--;            /* range [0-11] */

    SSCANF ((CHR1 *) au1Temp + 15, "%u", &(curTime.tm_year));

    if (curTime.tm_year > 2037)
    {
        return ISS_FAILURE;
    }
    u4Months = curTime.tm_mon;
    u4Days = curTime.tm_mday - 1;

    u4Leap = (IS_LEAP (curTime.tm_year) ? 1 : 0);
    if (u4Months <= 11)
    {
        curTime.tm_yday = au2DaysInMonth[u4Leap][u4Months];
    }
    curTime.tm_yday += u4Days;
#ifdef CLKIWF_WANTED
    /*Call ClkIwf API to SetTime */
    ClkSysTimeInfo.FsClkTimeVal.UtlTmVal = curTime;
    if (ClkIwfSetClock (&ClkSysTimeInfo, CLK_HANDSET_CLK) == OSIX_FAILURE)
    {
        return ISS_FAILURE;
    }
#else
    UtlSetTime (&curTime);
#endif
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : issGetSwitchDate                                 */
/*                                                                          */
/*    Description        : Gets the System Date                              */
/*                                                                          */
/*    Input(s)           : pIssSetSwitchDate                                */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ISS_FAILURE                          */
/****************************************************************************/
UINT4
issGetSwitchDate (tSNMP_OCTET_STRING_TYPE * pIssGetSwitchDate)
{
#ifdef CLKIWF_WANTED
    tClkSysTimeInfo     ClkSysTimeInfo;
    tSNMP_OCTET_STRING_TYPE ClockUtcOffset;
    UINT1               au1ClockUtcOffset[CLK_U8_STR_LEN];
#endif
    UINT1               au1Temp[ISS_MAX_DATE_LEN];
    tUtlTm              tm;
#ifdef CLKIWF_WANTED
    /*Call ClkIwf API to GetTime */
    MEMSET (&ClkSysTimeInfo, 0, sizeof (tClkSysTimeInfo));
    if (ClkIwfGetClock (CLK_MOD_SYS, &ClkSysTimeInfo) == OSIX_FAILURE)
    {
        return ISS_FAILURE;
    }
    tm.tm_sec = ClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_sec;
    tm.tm_min = ClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_min;
    tm.tm_hour = ClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_hour;
    tm.tm_mday = ClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_mday;
    tm.tm_mon = ClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_mon;
    tm.tm_year = ClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_year;
    tm.tm_wday = ClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_wday;
    tm.tm_yday = ClkSysTimeInfo.FsClkTimeVal.UtlTmVal.tm_yday;

    MEMSET (&ClockUtcOffset, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1ClockUtcOffset, 0, CLK_U8_STR_LEN);
    ClockUtcOffset.pu1_OctetList = au1ClockUtcOffset;
    ClockUtcOffset.i4_Length = sizeof (FS_UINT8);
    ClkIwfGetUtcOffset (&ClockUtcOffset);
#else
    UtlGetTime (&tm);
#endif
    tm.tm_mon++;
    tm.tm_wday++;

    SPRINTF ((CHR1 *) au1Temp, "0%u ", tm.tm_wday);

    if (tm.tm_mon <= 9)
    {
        SPRINTF ((CHR1 *) au1Temp + 3, "0%u ", tm.tm_mon);
    }
    else
    {
        SPRINTF ((CHR1 *) au1Temp + 3, "%u ", tm.tm_mon);
    }
    if (tm.tm_mday <= 9)
    {
        SPRINTF ((CHR1 *) au1Temp + 6, "0%u ", tm.tm_mday);
    }
    else
    {
        SPRINTF ((CHR1 *) au1Temp + 6, "%u ", tm.tm_mday);
    }

    if (tm.tm_hour <= 9)
    {
        SPRINTF ((CHR1 *) au1Temp + 9, "0%u:", tm.tm_hour);
    }
    else
    {
        SPRINTF ((CHR1 *) au1Temp + 9, "%u:", tm.tm_hour);
    }

    if (tm.tm_min <= 9)
    {
        SPRINTF ((CHR1 *) au1Temp + 12, "0%u:", tm.tm_min);
    }
    else
    {
        SPRINTF ((CHR1 *) au1Temp + 12, "%u:", tm.tm_min);
    }

    if (tm.tm_sec <= 9)
    {
        SPRINTF ((CHR1 *) au1Temp + 15, "0%u ", tm.tm_sec);
    }
    else
    {
        SPRINTF ((CHR1 *) au1Temp + 15, "%u ", tm.tm_sec);
    }
    SPRINTF ((CHR1 *) au1Temp + 18, "%u ", tm.tm_year);
#ifdef CLKIWF_WANTED
    if (ClockUtcOffset.pu1_OctetList[0] == 0)
    {
        STRCPY (ClockUtcOffset.pu1_OctetList, "+00:00");
    }

    SPRINTF ((CHR1 *) au1Temp + 23, "(UTC %s)", ClockUtcOffset.pu1_OctetList);
#else
    SPRINTF ((CHR1 *) au1Temp + 23, "(UTC +00:00)");
#endif
    pIssGetSwitchDate->i4_Length = (INT4) ISS_STRLEN (au1Temp);

    MEMCPY (pIssGetSwitchDate->pu1_OctetList, au1Temp,
            pIssGetSwitchDate->i4_Length);

    return ISS_SUCCESS;
}

#if defined NPAPI_WANTED
/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssAsyncNpUpdateStatusCb                         */
/*                                                                          */
/*    Description        : This API is called by the NPAPI Task after       */
/*                         programming the hardware. This function calls    */
/*                         the respective module based on the Protocol Id   */
/*                         passed.                                          */
/*                                                                          */
/*    Input(s)           : u4NpCallId - NP Call Identifier.                 */
/*                         *lv        - Pointer to Union of Structures of   */
/*                                      NPAPI Call Arguments.               */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
IssAsyncNpUpdateStatusCb (UINT4 u4NpCallId, unAsyncNpapi * lv)
{
    switch (u4NpCallId)
    {
#ifdef PNAC_WANTED
        case AS_PNAC_HW_SET_AUTH_STATUS:
        case AS_PNAC_HW_ADD_OR_DEL_MAC_SESS:
            PnacNpCallBack (u4NpCallId, lv);
            break;
#endif
#ifdef LA_WANTED
        case AS_FS_LA_HW_CREATE_AGG_GROUP:
        case AS_FS_LA_HW_ADD_LINK_TO_AGG_GROUP:
        case AS_FS_LA_HW_SET_SELECTION_POLICY:
        case AS_FS_LA_HW_REMOVE_LINK_FROM_AGG_GROUP:
        case AS_FS_LA_HW_DELETE_AGGREGATOR:
        case AS_FS_LA_HW_ENABLE_COLLECTION:
        case AS_FS_LA_HW_ENABLE_DISTRIBUTION:
        case AS_FS_LA_HW_DISABLE_COLLECTION:
        case AS_FS_LA_HW_SET_PORT_CHANNEL_STATUS:
            LaAsyncNpUpdateStatus (u4NpCallId, lv);
            break;
#endif
#ifdef IGS_WANTED
        case AS_FS_MI_NP_UPDATE_IPMC_FWD_ENTRIES:
        case AS_FS_MI_IGS_HW_UPDATE_IPMC_ENTRY:
            IgsNpCallBack (u4NpCallId, lv);
            break;
#endif
#ifdef RSTP_WANTED
        case AS_FS_MI_RSTP_NP_SET_PORT_STATE:
        case AS_FS_MI_MSTP_NP_SET_INSTANCE_PORT_STATE:
        case AS_FS_MI_PVRST_NP_SET_VLAN_PORT_STATE:
            AstAsyncNpUpdateStatus (u4NpCallId, lv);
            break;
#endif
#ifdef ERPS_WANTED
        case AS_FS_ERPS_HW_RING_CONFIG:
            ErpsAsyncNpUpdateStatus (u4NpCallId, lv);
            break;
#endif
        default:
            UNUSED_PARAM (lv);
            break;
    }

    return;
}

#endif

/*****************************************************************************/
/* Function Name      : IssL2IwfMiIsVlanActive                               */
/*                                                                           */
/* Description        : This routine is called to check whether the given    */
/*                      Vlan is active.                                      */
/*                                                                           */
/* Input(s)           : ContextId, VlanId                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*****************************************************************************/
BOOL1
IssL2IwfMiIsVlanActive (UINT4 u4ContextId, UINT2 u2VlanId)
{
    return (L2IwfMiIsVlanActive (u4ContextId, u2VlanId));
}

/*****************************************************************************/
/* Function Name      : IssL2IwfIsPortInPortChannel                         */
/*                                                                           */
/* Description        : This function calls the L2IWF module to check        */
/*                      whether the port is in port-channel.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
IssL2IwfIsPortInPortChannel (UINT4 u4IfIndex)
{
    return (L2IwfIsPortInPortChannel (u4IfIndex));
}

/*****************************************************************************/
/* Function Name      : IssL2IwfGetPortChannelForPort                        */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the      */
/*                      Port channel ID to which the port belongs to.        */
/*                                                                           */
/* Input(s)           : u4IfIndex - Global IfIndex of the port whose         */
/*                                  PortChannel Id to be obtained.           */
/*                                                                           */
/* Output(s)          : u2AggId     - Port Channel Index                     */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
IssL2IwfGetPortChannelForPort (UINT4 u4IfIndex, UINT2 *pu2AggId)
{
    return (L2IwfGetPortChannelForPort (u4IfIndex, pu2AggId));
}

/*****************************************************************************/
/* Function Name      : IssMirrAddRemovePort                                 */
/*                                                                           */
/* Description        : This function indicates mirroring whenever the       */
/*                      port is added or removed in the portchannel.         */
/*                                                                           */
/* Input(s)           : u4IfIndex - IfIndex of the port whose mirroring      */
/*                                  config to be chacked and applied         */
/*                                  if required                              */
/*                      u2AggId   - PortChannel Id to which the port         */
/*                                  configurations to be done                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/* Called By          : L2Iwf                                                */
/*                                                                           */
/* Calling Function   : L2IwfAddActivePortToPortChannel                      */
/*                      L2IwfRemoveActivePortToPortChannel                   */
/*****************************************************************************/
INT4
IssMirrAddRemovePort (UINT4 u4SrcIndex, UINT4 u4LaggId, UINT1 u1MirrorCfg)
{
#ifdef NPAPI_WANTED
    INT4                i4RetVal = 0;
    UINT4               u4DestIndex = 0;
    UINT2               u2SessionNo = 0;
    UINT1               u1Mode = 0;
    UINT1               u1CtrlStatus = 0;

    ISS_LOCK ();
    for (u2SessionNo = 1; u2SessionNo <= ISS_MIRR_MAX_SESSIONS; u2SessionNo++)
    {
        if ((GET_MIRR_SESSION_INFO (u2SessionNo)->u1SessionStatus !=
             ISS_ROW_STATUS_DESTROY)
            && (GET_MIRR_SESSION_INFO (u2SessionNo)->u1MirroringType ==
                ISS_MIRR_PORT_BASED))
        {
            if (IssMirrIsSrcConfigured ((UINT4) u2SessionNo,
                                        ISS_DEFAULT_CONTEXT, u4LaggId,
                                        ISS_MIRR_PORT_BASED) == ISS_SUCCESS)
            {
                break;
            }
        }
    }

    if (u2SessionNo > ISS_MIRR_MAX_SESSIONS)
    {
        ISS_UNLOCK ();
        /* u4LaggId not configured for mirroring */
        return ISS_SUCCESS;
    }

    if (IssMirrGetSrcRecrdInfo
        ((UINT4) u2SessionNo, ISS_DEFAULT_CONTEXT, u4LaggId, &u1Mode,
         &u1CtrlStatus) == ISS_FAILURE)
    {
        ISS_UNLOCK ();
        return ISS_FAILURE;
    }

    if ((i4RetVal = IssMirrGetNextDestRecrd (u2SessionNo,
                                             0, &u4DestIndex)) == ISS_FAILURE)
    {
        ISS_UNLOCK ();
        return ISS_FAILURE;
    }

    while (i4RetVal != ISS_FAILURE)
    {
        IsssysIssHwMirrorAddRemovePort (u4SrcIndex, u4DestIndex, u1Mode,
                                        u1MirrorCfg);

        i4RetVal = IssMirrGetNextDestRecrd (u2SessionNo,
                                            u4DestIndex, &u4DestIndex);
    }

    ISS_UNLOCK ();

#else
    UNUSED_PARAM (u4LaggId);
    UNUSED_PARAM (u4SrcIndex);
    UNUSED_PARAM (u1MirrorCfg);
#endif

    return ISS_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : IssPortRmRegisterProtocols                           */
/*                                                                           */
/* Description        : This function calls the RM module to register.       */
/*                                                                           */
/* Input(s)           : tRmRegParams - Reg. params to be provided by         */
/*                      protocols.                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
UINT4
IssPortRmRegisterProtocols (tRmRegParams * pRmReg)
{
    UINT4               u4RetVal = RM_SUCCESS;
#ifdef L2RED_WANTED
    u4RetVal = RmRegisterProtocols (pRmReg);
#else
    UNUSED_PARAM (pRmReg);
#endif /* L2RED_WANTED */
    return u4RetVal;
}

/*****************************************************************************/
/* Function Name      : IssPortRmReleaseMemoryForMsg                         */
/* Description        : This function Release RM Message memory posted       */
/* Input(s)           : pData - Pointer to the RM Message                    */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                                */
/*****************************************************************************/
PUBLIC INT4
IssPortRmReleaseMemoryForMsg (UINT1 *pData)
{
    INT4                i4RetVal = RM_SUCCESS;
#ifdef L2RED_WANTED
    i4RetVal = (INT4) RmReleaseMemoryForMsg (pData);
#else
    UNUSED_PARAM (pData);
#endif
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : IssPortRmApiHandleProtocolEvent                      */
/* Description        : This function posts the event to the RM module       */
/*                      form ISS module                                      */
/* Input(s)           : pEvt - Pointer to the strucutre for RM protocol evt  */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                                */
/*****************************************************************************/
PUBLIC INT4
IssPortRmApiHandleProtocolEvent (tRmProtoEvt * pEvt)
{
    INT4                i4RetVal = RM_SUCCESS;
#ifdef L2RED_WANTED
    i4RetVal = RmApiHandleProtocolEvent (pEvt);
#else
    UNUSED_PARAM (pEvt);
#endif
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : IssPortRmApiSendProtoAckToRM                         */
/*                                                                           */
/* Description        : This function posts the acknowledgement to the       */
/*                      RM module                                            */
/*                                                                           */
/* Input(s)           : pProtoAck-Pointer to the struct for RM protocol Ack  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                                */
/*****************************************************************************/
PUBLIC INT4
IssPortRmApiSendProtoAckToRM (tRmProtoAck * pProtoAck)
{
    INT4                i4RetVal = RM_SUCCESS;
#ifdef L2RED_WANTED
    i4RetVal = RmApiSendProtoAckToRM (pProtoAck);
#else
    UNUSED_PARAM (pProtoAck);
#endif
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : IssPortRmEnqMsgToRm                                  */
/* Description        : This function posts the RM message to the RM Module  */
/* Input(s)           : pRmMsg - Pointer to the RM Message                   */
/*                      u2DataLen - Length of the data                       */
/*                      u4SrcEntId - Source Application ID.                  */
/*                      u4DestEntId - Destination Application ID.            */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                                */
/*****************************************************************************/
UINT4
IssPortRmEnqMsgToRm (tRmMsg * pRmMsg, UINT2 u2DataLen, UINT4 u4SrcEntId,
                     UINT4 u4DestEntId)
{
    UINT4               u4RetVal = RM_SUCCESS;
#ifdef L2RED_WANTED
    u4RetVal =
        RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen, u4SrcEntId, u4DestEntId);
#else
    UNUSED_PARAM (pRmMsg);
    UNUSED_PARAM (u2DataLen);
    UNUSED_PARAM (u4SrcEntId);
    UNUSED_PARAM (u4DestEntId);
#endif
    return u4RetVal;
}

/*****************************************************************************/
/* Function Name      : IssRmEnqChkSumMsgToRm                                */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS/ISS_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
IssRmEnqChkSumMsgToRm (UINT2 u2AppId, UINT2 u2ChkSum)
{
#ifdef L2RED_WANTED
    if ((RmEnqChkSumMsgToRmFromApp (u2AppId, u2ChkSum)) == RM_FAILURE)
    {
        return RM_FAILURE;
    }
    return RM_SUCCESS;
#else
    UNUSED_PARAM (u2AppId);
    UNUSED_PARAM (u2ChkSum);
    return RM_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : IssPortRmGetStandbyNodeCount                         */
/* Description        : This function gets the RM Standby Count.             */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                                */
/*****************************************************************************/
PUBLIC UINT1
IssPortRmGetStandbyNodeCount (VOID)
{
#ifdef L2RED_WANTED
    return (RmGetStandbyNodeCount ());
#else
    return ISS_ZERO_ENTRY;
#endif
}

/*****************************************************************************/
/* Function Name      : IssPortRmDeRegisterProtocols                         */
/* Description        : This function De-Register  module with RM Module     */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                                */
/*****************************************************************************/
PUBLIC INT4
IssPortRmDeRegisterProtocols (UINT4 u4ProtId)
{
    INT4                i4RetVal = RM_SUCCESS;
#ifdef L2RED_WANTED
    i4RetVal = (INT4) RmDeRegisterProtocols (u4ProtId);
#else
    UNUSED_PARAM (u4ProtId);
#endif
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : IssPortRmGetNodeState                                */
/* Description        : This function gets the RM node state.                */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                                */
/*****************************************************************************/
PUBLIC INT4
IssPortRmGetNodeState (VOID)
{
#ifdef L2RED_WANTED
    return ((INT4) RmGetNodeState ());
#else
    return RM_ACTIVE;
#endif
}

/*****************************************************************************/
/* Function Name      : IssPortGetMirrDestPortForSrcPort                     */
/*                                                                           */
/* Description        : This function retrieves mirror destination port      */
/*                      for the given source port.                           */
/*                                                                           */
/* Input(s)           : u4MirrIndex   - IfIndex of the source port           */
/*                                                                           */
/* Output(s)          : pu4DestPort - Destnination port or MTP               */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : ISS_SUCCESS / ISS_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
IssPortGetMirrDestPortForSrcPort (UINT4 u4MirrIndex, UINT4 *pu4DestPort)
{
    INT4                i4RetVal = 0;
    UINT2               u2SessionNo = 0;
    UINT1               u1Mode = 0;
    UINT1               u1CtrlStatus = 0;

    for (u2SessionNo = 1; u2SessionNo <= ISS_MIRR_MAX_SESSIONS; u2SessionNo++)
    {
        if ((GET_MIRR_SESSION_INFO (u2SessionNo)->u1SessionStatus ==
             ISS_ROW_STATUS_ACTIVE)
            && (GET_MIRR_SESSION_INFO (u2SessionNo)->u1MirroringType ==
                ISS_MIRR_PORT_BASED))
        {
            if (IssMirrGetSrcRecrdInfo
                ((UINT4) u2SessionNo, ISS_DEFAULT_CONTEXT, u4MirrIndex, &u1Mode,
                 &u1CtrlStatus) == ISS_SUCCESS)
            {
                break;
            }
        }
    }
    if (u2SessionNo > ISS_MIRR_MAX_SESSIONS)
    {
        /* u4MirrIndex not configured for egress mirroring */
        return ISS_FAILURE;
    }

    if ((i4RetVal = IssMirrGetNextDestRecrd (u2SessionNo,
                                             0, pu4DestPort)) == ISS_FAILURE)
    {
        return ISS_FAILURE;
    }

    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IssPortIcchIsIcclVlan                                */
/*                                                                           */
/* Description        : This function will check whether given VLAN is ICCL  */
/*                      VLAN or not.                                         */
/*                                                                           */
/* Input(s)           : u2VlanId - Vlan Id                                   */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE                            */
/*****************************************************************************/
INT1
IssPortIcchIsIcclVlan (UINT2 u2VlanId)
{
#ifdef ICCH_WANTED
    return ((INT1) IcchIsIcclVlan (u2VlanId));
#else
    UNUSED_PARAM (u2VlanId);
    return OSIX_FAILURE;
#endif
}
#endif /* _ISSPORT_C */
/* END OF FILE */
