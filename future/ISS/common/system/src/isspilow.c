/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/* $Id: isspilow.c,v 1.8 2015/11/11 08:39:57 siva Exp $                     */
/*****************************************************************************/
/*    FILE  NAME            : isspilow.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Port Isolation Feature                         */
/*    MODULE NAME           : ISS                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Linux                                          */
/*    DATE OF FIRST RELEASE : 06 July 2010                                   */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           :  This file contains low level routines for     */
/*                             for port isolation table creation/deletion/   */
/*                             manipulation                                  */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    12 July 2010           Initial Creation                        */
/*---------------------------------------------------------------------------*/

#ifndef ISSPILW_C
#define ISSPILW_C

#include "issinc.h"
#include "fssnmp.h"
#include "isspiinc.h"
#include "isscli.h"

/* LOW LEVEL Routines for Table : IssPortIsolationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIssPortIsolationTable
 Input       :  The Indices
                IssPortIsolationIngressPort
                IssPortIsolationInVlanId
                IssPortIsolationEgressPort
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIssPortIsolationTable (INT4
                                               i4IssPortIsolationIngressPort,
                                               INT4 i4IssPortIsolationInVlanId,
                                               INT4
                                               i4IssPortIsolationEgressPort)
{
    UINT1               u1IfType = 0;

    CfaGetIfaceType ((UINT4) i4IssPortIsolationIngressPort, &u1IfType);
    /* 1. Check if the ingress port is a valid physical/LA/PseudoWire port */
    if (((i4IssPortIsolationIngressPort >
          (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG)) ||
         (i4IssPortIsolationIngressPort < 1)) &&
        (ISS_PORT_ISOLATION_ALLOWED (u1IfType) != ISS_TRUE))
    {
        return SNMP_FAILURE;
    }

    CfaGetIfaceType ((UINT4) i4IssPortIsolationEgressPort, &u1IfType);
    /* 2. Check if the egress port is a valid physical/LA/PseudoWire port */
    if (((i4IssPortIsolationEgressPort >
          (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG)) ||
         (i4IssPortIsolationEgressPort < 1)) &&
        (ISS_PORT_ISOLATION_ALLOWED (u1IfType) != ISS_TRUE))
    {
        return SNMP_FAILURE;
    }

    /* 3. Check if this is a valid VLAN Id. 
     * between the range (0..4094) 
     */
    if ((i4IssPortIsolationInVlanId < 0) ||
        (i4IssPortIsolationInVlanId > VLAN_MAX_VLAN_ID))
    {
        return SNMP_FAILURE;
    }

    /* Check if the ingress port is not equal to egress port */
    if (i4IssPortIsolationIngressPort == i4IssPortIsolationEgressPort)
    {
		CLI_SET_ERR (CLI_ISS_PI_INVALID_ADDITION_DELETION);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}                                /* End of nmhValidateIndexInstanceIssPortIsolationTable */

/****************************************************************************
 Function    :  nmhGetFirstIndexIssPortIsolationTable
 Input       :  The Indices
                IssPortIsolationIngressPort
                IssPortIsolationInVlanId
                IssPortIsolationEgressPort
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexIssPortIsolationTable (INT4 *pi4IssPortIsolationIngressPort,
                                       INT4 *pi4IssPortIsolationInVlanId,
                                       INT4 *pi4IssPortIsolationEgressPort)
{

    if (ISS_SUCCESS ==
        IssPIGetFirstPortIsolationNode ((UINT4 *)
                                        pi4IssPortIsolationIngressPort,
                                        (UINT4 *) pi4IssPortIsolationInVlanId,
                                        (UINT4 *)
                                        pi4IssPortIsolationEgressPort))
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}                                /* End of nmhGetFirstIndexIssPortIsolationTable */

/****************************************************************************
 Function    :  nmhGetNextIndexIssPortIsolationTable
 Input       :  The Indices
                IssPortIsolationIngressPort
                nextIssPortIsolationIngressPort
                IssPortIsolationInVlanId
                nextIssPortIsolationInVlanId
                IssPortIsolationEgressPort
                nextIssPortIsolationEgressPort
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexIssPortIsolationTable (INT4 i4IssPortIsolationIngressPort,
                                      INT4 *pi4NextIssPortIsolationIngressPort,
                                      INT4 i4IssPortIsolationInVlanId,
                                      INT4 *pi4NextIssPortIsolationInVlanId,
                                      INT4 i4IssPortIsolationEgressPort,
                                      INT4 *pi4NextIssPortIsolationEgressPort)
{
    UINT2               u2VlanId = 0;
    if (ISS_SUCCESS ==
        IssPIGetNextPortIsolationNode ((UINT4) i4IssPortIsolationIngressPort,
                                       (tVlanId) i4IssPortIsolationInVlanId,
                                       (UINT4) i4IssPortIsolationEgressPort,
                                       (UINT4 *)
                                       pi4NextIssPortIsolationIngressPort,
                                       &u2VlanId,
                                       (UINT4 *)
                                       pi4NextIssPortIsolationEgressPort))
    {
        *pi4NextIssPortIsolationInVlanId = u2VlanId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}                                /* End of nmhGetNextIndexIssPortIsolationTable */

/****************************************************************************
 Function    :  nmhGetIssPortIsolationStorageType
 Input       :  The Indices
                IssPortIsolationIngressPort
                IssPortIsolationInVlanId
                IssPortIsolationEgressPort

                The Object 
                retValIssPortIsolationStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssPortIsolationStorageType (INT4 i4IssPortIsolationIngressPort,
                                   INT4 i4IssPortIsolationInVlanId,
                                   INT4 i4IssPortIsolationEgressPort,
                                   INT4 *pi4RetValIssPortIsolationStorageType)
{
    UNUSED_PARAM (i4IssPortIsolationEgressPort);

    if (ISS_SUCCESS ==
        IssPIGetPortIsolationStorageType ((UINT4) i4IssPortIsolationIngressPort,
                                          (tVlanId) i4IssPortIsolationInVlanId,
                                          (UINT1 *)
                                          pi4RetValIssPortIsolationStorageType))
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}                                /* End of nmhGetIssPortIsolationStorageType */

/****************************************************************************
 Function    :  nmhGetIssPortIsolationRowStatus
 Input       :  The Indices
                IssPortIsolationIngressPort
                IssPortIsolationInVlanId
                IssPortIsolationEgressPort

                The Object 
                retValIssPortIsolationRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIssPortIsolationRowStatus (INT4 i4IssPortIsolationIngressPort,
                                 INT4 i4IssPortIsolationInVlanId,
                                 INT4 i4IssPortIsolationEgressPort,
                                 INT4 *pi4RetValIssPortIsolationRowStatus)
{
    if (IssPIGetPortIsolationEntry ((UINT4) i4IssPortIsolationIngressPort,
                                    (tVlanId) i4IssPortIsolationInVlanId,
                                    (UINT4) i4IssPortIsolationEgressPort) ==
        ISS_SUCCESS)
    {
        *pi4RetValIssPortIsolationRowStatus = ACTIVE;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}                                /* End of nmhGetIssPortIsolationRowStatus */

/****************************************************************************
 Function    :  nmhSetIssPortIsolationRowStatus
 Input       :  The Indices
                IssPortIsolationIngressPort
                IssPortIsolationInVlanId
                IssPortIsolationEgressPort

                The Object 
                setValIssPortIsolationRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIssPortIsolationRowStatus (INT4 i4IssPortIsolationIngressPort,
                                 INT4 i4IssPortIsolationInVlanId,
                                 INT4 i4IssPortIsolationEgressPort,
                                 INT4 i4SetValIssPortIsolationRowStatus)
{
    tIssUpdtPortIsolation IssUpdtPortIsolation;
    INT4                i4RetVal = ISS_FAILURE;

    MEMSET (&IssUpdtPortIsolation, 0, sizeof (tIssUpdtPortIsolation));

    /* Allocate Memory for the Egress port */
    IssUpdtPortIsolation.pu4EgressPorts = (UINT4 *)
        &i4IssPortIsolationEgressPort;

    /* Populate the structure with the ingress and egress port
     * information.
     */
    IssUpdtPortIsolation.u4IngressPort = i4IssPortIsolationIngressPort;
    IssUpdtPortIsolation.InVlanId = (tVlanId) i4IssPortIsolationInVlanId;
    IssUpdtPortIsolation.u2NumEgressPorts = 1;

    switch (i4SetValIssPortIsolationRowStatus)
    {
        case CREATE_AND_GO:
            IssUpdtPortIsolation.u1Action = ISS_PI_ADD;
            i4RetVal = IssPIUpdtPortIsolationEntry (&IssUpdtPortIsolation);

            /* Set the Storage type to Non-Volatile since the Port Isolation
             * Entry creation comes from management.
             */
            IssPISetPortIsolationStorageType ((UINT4)
                                              i4IssPortIsolationIngressPort,
                                              (tVlanId) (UINT4)
                                              i4IssPortIsolationInVlanId,
                                              ISS_PI_NON_VOLATILE);
            break;

        case DESTROY:
            IssUpdtPortIsolation.u1Action = ISS_PI_DELETE;
            i4RetVal = IssPIUpdtPortIsolationEntry (&IssUpdtPortIsolation);
            break;

        default:
            return SNMP_FAILURE;
    }                            /* End of switch */

    if (i4RetVal == ISS_SUCCESS)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}                                /* nmhSetIssPortIsolationRowStatus */

/****************************************************************************
 Function    :  nmhTestv2IssPortIsolationRowStatus
 Input       :  The Indices
                IssPortIsolationIngressPort
                IssPortIsolationInVlanId
                IssPortIsolationEgressPort

                The Object 
                testValIssPortIsolationRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2IssPortIsolationRowStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4IssPortIsolationIngressPort,
                                    INT4 i4IssPortIsolationInVlanId,
                                    INT4 i4IssPortIsolationEgressPort,
                                    INT4 i4TestValIssPortIsolationRowStatus)
{
    UINT1               u1StorageType = 0;

    /* Only CreateAndGo and destroy are the allowed values of this
     * Row status object.
     */
    if ((i4TestValIssPortIsolationRowStatus != CREATE_AND_GO) &&
        (i4TestValIssPortIsolationRowStatus != DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate the indices to this table (ingressPort, egress port) */
    if (nmhValidateIndexInstanceIssPortIsolationTable
        (i4IssPortIsolationIngressPort, i4IssPortIsolationInVlanId,
         i4IssPortIsolationEgressPort) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Check if this entry is already present in the Port Isolation table */
    if (IssPIGetPortIsolationEntry ((UINT4) i4IssPortIsolationIngressPort,
                                    (tVlanId) i4IssPortIsolationInVlanId,
                                    (UINT4) i4IssPortIsolationEgressPort)
        == ISS_SUCCESS)
    {
        if (CREATE_AND_GO == i4TestValIssPortIsolationRowStatus)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        /* Do not allow deletion of a port isolation entry with storage type
         * as volatile. 
         */
        IssPIGetPortIsolationStorageType ((UINT4) i4IssPortIsolationIngressPort,
                                          (tVlanId) i4IssPortIsolationInVlanId,
                                          &u1StorageType);
        if ((DESTROY == i4TestValIssPortIsolationRowStatus) &&
            (ISS_PI_VOLATILE == u1StorageType))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ISS_PI_INVALID_DELETION);
            return SNMP_FAILURE;
        }

    }
    else
    {
        if (DESTROY == i4TestValIssPortIsolationRowStatus)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ISS_PI_DEL_FAILED);
            return SNMP_FAILURE;
        }
    }
   if (L2IWF_SUCCESS == L2IwfIsPortInPortChannel((UINT4)i4IssPortIsolationIngressPort))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
    }


    if (L2IWF_SUCCESS == L2IwfIsPortInPortChannel((UINT4)i4IssPortIsolationEgressPort))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
    }


    return SNMP_SUCCESS;
}                                /* End of nmhTestv2IssPortIsolationRowStatus */

/****************************************************************************
 Function    :  nmhDepv2IssPortIsolationTable
 Input       :  The Indices
                IssPortIsolationIngressPort
                IssPortIsolationInVlanId
                IssPortIsolationEgressPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2IssPortIsolationTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

#endif /* _ISSPILW_C */
