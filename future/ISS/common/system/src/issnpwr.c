/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: issnpwr.c,v 1.1
 ******************************************************************************/

/***************************************************************************
 *                                                                          
 *    Function Name       : IsssysNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tIsssysNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#ifndef ISS_SYS_NP_WR_C
#define ISS_SYS_NP_WR_C

#include "issinc.h"
#include "nputil.h"

PUBLIC UINT1
IsssysNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tIsssysNpModInfo   *pIsssysNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pIsssysNpModInfo = &(pFsHwNp->IsssysNpModInfo);

    if (NULL == pIsssysNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case ISS_HW_SET_PORT_EGRESS_STATUS:
        {
            tIsssysNpWrIssHwSetPortEgressStatus *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortEgressStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwSetPortEgressStatus (pEntry->u4IfIndex,
                                          pEntry->u1EgressEnable);
            break;
        }
        case ISS_HW_SET_PORT_STATS_COLLECTION:
        {
            tIsssysNpWrIssHwSetPortStatsCollection *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortStatsCollection;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwSetPortStatsCollection (pEntry->u4IfIndex,
                                             pEntry->u1StatsEnable);
            break;
        }
        case ISS_HW_SET_PORT_MODE:
        {
            tIsssysNpWrIssHwSetPortMode *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortMode;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = IssHwSetPortMode (pEntry->u4IfIndex, pEntry->i4Value);
            break;
        }
        case ISS_HW_SET_PORT_DUPLEX:
        {
            tIsssysNpWrIssHwSetPortDuplex *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortDuplex;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = IssHwSetPortDuplex (pEntry->u4IfIndex, pEntry->i4Value);
            break;
        }
        case ISS_HW_SET_PORT_SPEED:
        {
            tIsssysNpWrIssHwSetPortSpeed *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortSpeed;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = IssHwSetPortSpeed (pEntry->u4IfIndex, pEntry->i4Value);
            break;
        }
        case ISS_HW_SET_PORT_FLOW_CONTROL:
        {
            tIsssysNpWrIssHwSetPortFlowControl *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortFlowControl;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwSetPortFlowControl (pEntry->u4IfIndex,
                                         pEntry->u1FlowCtrlEnable);
            break;
        }
        case ISS_HW_SET_PORT_RENEGOTIATE:
        {
            tIsssysNpWrIssHwSetPortRenegotiate *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortRenegotiate;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwSetPortRenegotiate (pEntry->u4IfIndex, pEntry->i4Value);
            break;
        }
        case ISS_HW_SET_PORT_MAX_MAC_ADDR:
        {
            tIsssysNpWrIssHwSetPortMaxMacAddr *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortMaxMacAddr;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwSetPortMaxMacAddr (pEntry->u4IfIndex, pEntry->i4Value);
            break;
        }
        case ISS_HW_SET_PORT_MAX_MAC_ACTION:
        {
            tIsssysNpWrIssHwSetPortMaxMacAction *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortMaxMacAction;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwSetPortMaxMacAction (pEntry->u4IfIndex, pEntry->i4Value);
            break;
        }
        case ISS_HW_SET_PORT_MIRRORING_STATUS:
        {
            tIsssysNpWrIssHwSetPortMirroringStatus *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortMirroringStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = IssHwSetPortMirroringStatus (pEntry->i4MirrorStatus);
            break;
        }
        case ISS_HW_SET_MIRROR_TO_PORT:
        {
            tIsssysNpWrIssHwSetMirrorToPort *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetMirrorToPort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = IssHwSetMirrorToPort (pEntry->u4IfIndex);
            break;
        }
        case ISS_HW_SET_INGRESS_MIRRORING:
        {
            tIsssysNpWrIssHwSetIngressMirroring *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetIngressMirroring;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwSetIngressMirroring (pEntry->u4IfIndex,
                                          pEntry->i4IngMirroring);
            break;
        }
        case ISS_HW_SET_EGRESS_MIRRORING:
        {
            tIsssysNpWrIssHwSetEgressMirroring *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetEgressMirroring;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwSetEgressMirroring (pEntry->u4IfIndex,
                                         pEntry->i4EgrMirroring);
            break;
        }
        case ISS_HW_SET_RATE_LIMITING_VALUE:
        {
            tIsssysNpWrIssHwSetRateLimitingValue *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetRateLimitingValue;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwSetRateLimitingValue (pEntry->u4IfIndex,
                                           pEntry->u1PacketType,
                                           pEntry->i4RateLimitVal);
            break;
        }
        case ISS_HW_SET_SWITCH_MODE_TYPE:
        {
            tIsssysNpWrIssHwSetSwitchModeType *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetSwitchModeType;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                (UINT1)IssHwSetSwitchModeType (pEntry->i4IssSwitchModeType);
            break;
        }
        case ISS_HW_SET_PORT_EGRESS_PKT_RATE:
        {
            tIsssysNpWrIssHwSetPortEgressPktRate *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortEgressPktRate;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwSetPortEgressPktRate (pEntry->u4IfIndex, pEntry->i4PktRate,
                                           pEntry->i4BurstRate);
            break;
        }
        case ISS_HW_GET_PORT_EGRESS_PKT_RATE:
        {
            tIsssysNpWrIssHwGetPortEgressPktRate *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwGetPortEgressPktRate;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwGetPortEgressPktRate (pEntry->u4Port,
                                           pEntry->pi4PortPktRate,
                                           pEntry->pi4PortBurstRate);
            break;
        }
        case ISS_HW_RESTART_SYSTEM:
        {
            IssHwRestartSystem ();
            break;
        }
        case ISS_HW_INIT_FILTER:
        {
            u1RetVal = IssHwInitFilter ();
            break;
        }
        case ISS_HW_GET_PORT_DUPLEX:
        {
            tIsssysNpWrIssHwGetPortDuplex *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwGetPortDuplex;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwGetPortDuplex (pEntry->u4IfIndex,
                                    pEntry->pi4PortDuplexStatus);
            break;
        }
        case ISS_HW_GET_PORT_SPEED:
        {
            tIsssysNpWrIssHwGetPortSpeed *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwGetPortSpeed;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwGetPortSpeed (pEntry->u4IfIndex, pEntry->pi4PortSpeed);
            break;
        }
        case ISS_HW_GET_PORT_FLOW_CONTROL:
        {
            tIsssysNpWrIssHwGetPortFlowControl *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwGetPortFlowControl;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwGetPortFlowControl (pEntry->u4IfIndex,
                                         pEntry->pi4PortFlowControl);
            break;
        }
        case ISS_HW_SET_PORT_H_O_L_BLOCK_PREVENTION:
        {
            tIsssysNpWrIssHwSetPortHOLBlockPrevention *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortHOLBlockPrevention;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwSetPortHOLBlockPrevention (pEntry->u4IfIndex,
                                                pEntry->i4Value);
            break;
        }
        case ISS_HW_GET_PORT_H_O_L_BLOCK_PREVENTION:
        {
            tIsssysNpWrIssHwGetPortHOLBlockPrevention *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwGetPortHOLBlockPrevention;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwGetPortHOLBlockPrevention (pEntry->u4IfIndex,
                                                pEntry->pi4Value);
            break;
        }
        case ISS_HW_UPDATE_L2_FILTER:
        {
            tIsssysNpWrIssHwUpdateL2Filter *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwUpdateL2Filter;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwUpdateL2Filter (pEntry->pIssL2FilterEntry,
                                     pEntry->i4Value);
            break;
        }
        case ISS_HW_UPDATE_L3_FILTER:
        {
            tIsssysNpWrIssHwUpdateL3Filter *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwUpdateL3Filter;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwUpdateL3Filter (pEntry->pIssL3FilterEntry,
                                     pEntry->i4Value);
            break;
        }
        case ISS_HW_UPDATE_L4_S_FILTER:
        {
            tIsssysNpWrIssHwUpdateL4SFilter *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwUpdateL4SFilter;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwUpdateL4SFilter (pEntry->pIssL4SFilterEntry,
                                      pEntry->i4Value);
            break;
        }
        case ISS_NP_HW_UPDATE_RESERV_FRAME_ACTION:
        {
            tIsssysNpWrIssHwSetResrvFrameFilter *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetResrvFrameFilter;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwSetResrvFrameFilter (pEntry->pIssReservFrmCtrlInfo,
                                      pEntry->i4Value);
            break;
        }
        case ISS_HW_SEND_BUFFER_TO_LINUX:
        {
            tIsssysNpWrIssHwSendBufferToLinux *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSendBufferToLinux;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwSendBufferToLinux (pEntry->pBuffer, pEntry->u4Len,
                                        pEntry->i4ImageType);
            break;
        }
        case ISS_HW_GET_FAN_STATUS:
        {
            tIsssysNpWrIssHwGetFanStatus *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwGetFanStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwGetFanStatus (pEntry->u4FanIndex, pEntry->pu4FanStatus);
            break;
        }
        case ISS_HW_INIT_MIRR_DATA_BASE:
        {
            u1RetVal = IssHwInitMirrDataBase ();
            break;
        }
        case ISS_HW_SET_MIRRORING:
        {
            tIsssysNpWrIssHwSetMirroring *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetMirroring;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = IssHwSetMirroring (pEntry->pMirrorInfo);
            break;
        }
        case ISS_HW_MIRROR_ADD_REMOVE_PORT:
        {
            tIsssysNpWrIssHwMirrorAddRemovePort *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwMirrorAddRemovePort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwMirrorAddRemovePort (pEntry->u4SrcIndex,
                                          pEntry->u4DestIndex, pEntry->u1Mode,
                                          pEntry->u1MirrCfg);
            break;
        }
        case ISS_HW_SET_MAC_LEARNING_RATE_LIMIT:
        {
            tIsssysNpWrIssHwSetMacLearningRateLimit *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetMacLearningRateLimit;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwSetMacLearningRateLimit (pEntry->i4IssMacLearnLimitVal);
            break;
        }
        case ISS_HW_GET_LEARNED_MAC_ADDR_COUNT:
        {
            tIsssysNpWrIssHwGetLearnedMacAddrCount *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwGetLearnedMacAddrCount;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwGetLearnedMacAddrCount (pEntry->pi4LearnedMacAddrCount);
            break;
        }
        case ISS_HW_UPDATE_USER_DEFINED_FILTER:
        {
            tIsssysNpWrIssHwUpdateUserDefinedFilter *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwUpdateUserDefinedFilter;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwUpdateUserDefinedFilter (pEntry->pAccessFilterEntry,
                                              pEntry->pL2AccessFilterEntry,
                                              pEntry->pL3AccessFilterEntry,
                                              pEntry->i4FilterAction);
            break;
        }
        case ISS_HW_SET_PORT_AUTO_NEG_ADVT_CAP_BITS:
        {
            tIsssysNpWrIssHwSetPortAutoNegAdvtCapBits *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortAutoNegAdvtCapBits;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwSetPortAutoNegAdvtCapBits (pEntry->u4IfIndex,
                                                pEntry->pi4MauCapBits,
                                                pEntry->pi4IssCapBits);
            break;
        }
        case ISS_HW_GET_PORT_AUTO_NEG_ADVT_CAP_BITS:
        {
            tIsssysNpWrIssHwGetPortAutoNegAdvtCapBits *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwGetPortAutoNegAdvtCapBits;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwGetPortAutoNegAdvtCapBits (pEntry->u4IfIndex,
                                                pEntry->pElement);
            break;
        }
        case ISS_HW_SET_LEARNING_MODE:
        {
            tIsssysNpWrIssHwSetLearningMode *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetLearningMode;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwSetLearningMode (pEntry->u4IfIndex, pEntry->i4Status);
            break;
        }
        case ISS_HW_SET_PORT_MDI_OR_MDIX_CAP:
        {
            tIsssysNpWrIssHwSetPortMdiOrMdixCap *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortMdiOrMdixCap;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwSetPortMdiOrMdixCap (pEntry->u4IfIndex, pEntry->i4Value);
            break;
        }
        case ISS_HW_GET_PORT_MDI_OR_MDIX_CAP:
        {
            tIsssysNpWrIssHwGetPortMdiOrMdixCap *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwGetPortMdiOrMdixCap;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwGetPortMdiOrMdixCap (pEntry->u4IfIndex,
                                          pEntry->pi4PortStatus);
            break;
        }
        case ISS_HW_SET_PORT_FLOW_CONTROL_RATE:
        {
            tIsssysNpWrIssHwSetPortFlowControlRate *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwSetPortFlowControlRate;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssHwSetPortFlowControlRate (pEntry->u4IfIndex,
                                             pEntry->i4PortMaxRate,
                                             pEntry->i4PortMinRate);
            break;
        }
        case ISS_HW_CONFIG_PORT_ISOLATION_ENTRY:
        {
            tIsssysNpWrIssHwConfigPortIsolationEntry *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssHwConfigPortIsolationEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = IssHwConfigPortIsolationEntry (pEntry->pPortIsolation);
            break;
        }
        case NP_SET_TRACE:
        {
            tIsssysNpWrNpSetTrace *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpNpSetTrace;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            NpSetTrace (pEntry->u1TraceModule, pEntry->u1TraceLevel);
            break;
        }
        case NP_GET_TRACE:
        {
            tIsssysNpWrNpGetTrace *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpNpGetTrace;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            NpGetTrace (pEntry->u1TraceModule, pEntry->pu1TraceLevel);
            break;
        }
#ifdef NPAPI_WANTED
        case NP_SET_TRACE_LEVEL:
        {
            tIsssysNpWrNpSetTraceLevel *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpNpSetTraceLevel;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            NpSetTraceLevel (pEntry->u2TraceLevel);
            break;
        }
        case NP_GET_TRACE_LEVEL:
        {
            tIsssysNpWrNpGetTraceLevel *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpNpGetTraceLevel;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            NpGetTraceLevel (pEntry->pu2TraceLevel);
            break;
        }
#endif

        case ISS_HW_SET_SET_TRAFFIC_SEPERATION_CTRL:
        {
            tIsssysNpWrIssHwSetSetTrafficSeperationCtrl *pEntry = NULL;
            pEntry =
                &pIsssysNpModInfo->IsssysNpIssHwSetSetTrafficSeperationCtrl;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = IssHwSetSetTrafficSeperationCtrl (pEntry->i4CtrlStatus);
            break;
        }

#ifdef MBSM_WANTED
        case ISS_MBSM_HW_SET_PORT_EGRESS_STATUS:
        {
            tIsssysNpWrIssMbsmHwSetPortEgressStatus *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwSetPortEgressStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssMbsmHwSetPortEgressStatus (pEntry->u4IfIndex,
                                              pEntry->u1Status,
                                              pEntry->pSlotInfo);
            break;
        }
        case ISS_MBSM_HW_SET_PORT_STATS_COLLECTION:
        {
            tIsssysNpWrIssMbsmHwSetPortStatsCollection *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwSetPortStatsCollection;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssMbsmHwSetPortStatsCollection (pEntry->u4IfIndex,
                                                 pEntry->u1Status,
                                                 pEntry->pSlotInfo);
            break;
        }
        case ISS_MBSM_HW_SET_PORT_MIRRORING_STATUS:
        {
            tIsssysNpWrIssMbsmHwSetPortMirroringStatus *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwSetPortMirroringStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssMbsmHwSetPortMirroringStatus (pEntry->i4MirrorStatus,
                                                 pEntry->pSlotInfo);
            break;
        }
        case ISS_MBSM_HW_SET_MIRROR_TO_PORT:
        {
            tIsssysNpWrIssMbsmHwSetMirrorToPort *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwSetMirrorToPort;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssMbsmHwSetMirrorToPort (pEntry->u4MirrorPort,
                                          pEntry->pSlotInfo);
            break;
        }
        case ISS_MBSM_HW_UPDATE_L2_FILTER:
        {
            tIsssysNpWrIssMbsmHwUpdateL2Filter *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwUpdateL2Filter;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssMbsmHwUpdateL2Filter (pEntry->pIssL2FilterEntry,
                                         pEntry->i4Value, pEntry->pSlotInfo);
            break;
        }
        case ISS_MBSM_HW_UPDATE_L3_FILTER:
        {
            tIsssysNpWrIssMbsmHwUpdateL3Filter *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwUpdateL3Filter;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssMbsmHwUpdateL3Filter (pEntry->pIssL3FilterEntry,
                                         pEntry->i4Value, pEntry->pSlotInfo);
            break;
        }
        case ISS_P_I_MBSM_HW_CONFIG_PORT_ISOLATION_ENTRY:
        {
            tIsssysNpWrIssPIMbsmHwConfigPortIsolationEntry *pEntry = NULL;
            pEntry =
                &pIsssysNpModInfo->IsssysNpIssPIMbsmHwConfigPortIsolationEntry;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                IssPIMbsmHwConfigPortIsolationEntry (pEntry->
                                                     pHwUpdPortIsolation,
                                                     pEntry->pIssMbsmInfo);
            break;
        }
        case ISS_MBSM_HW_CPU_MIRRORING:
        {
            tIsssysNpWrIssMbsmHwSetCpuMirroring *pEntry = NULL;
                
            pEntry = &pIsssysNpModInfo->IsssysNpIssMbsmHwSetCpuMirroring;
            if ( NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = (UINT1)IssHwSetCpuMirroring (pEntry->i4IssCpuMirrorType , 
                    pEntry->u4IssCpuMirrorToPort);
            break;
        }


#endif
        case ISS_HW_ENABLE_HW_CONSOLE:
        {
            tIsssysNpWrIssHwEnableHwConsole *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpWrIssHwEnableHwConsole;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = NpUtilAccessHwConsole (pEntry->pu1Buffer);
            break;
        }
        case ISS_NP_HW_GET_CAPABILITIES:
        {
            tIsssysNpWrIssNpHwGetCapabilities *pEntry = NULL;
            pEntry = &pIsssysNpModInfo->IsssysNpIssNpHwGetCapabilities;
            if ( NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            IssNpHwGetCapabilities (pEntry->pHwGetCapabilities);
            break;
        }
        case ISS_HW_CPU_MIRRORING:
        {
            tIsssysNpWrIssHwSetCpuMirroring *pEntry = NULL;

            pEntry = &pIsssysNpModInfo->IsssysNpWrIssHwSetCpuMirroring;

            if ( NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = (UINT1)IssHwSetCpuMirroring (pEntry->i4IssCpuMirrorType , 
                    pEntry->u4IssCpuMirrorToPort);
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

#endif /* _ISS_SYS_NP_WR_C */
