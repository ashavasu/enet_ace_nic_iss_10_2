/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsisswr.c,v 1.63 2017/12/20 11:06:48 siva Exp $
*
* Description: Wrapper Functions
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "isslow.h"
# include  "fsisswr.h"
# include  "iss.h"
# include  "isspi.h"
# include  "isspiinc.h"
# include  "fsissdb.h"

INT4
IssSwitchNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssSwitchName (pMultiData->pOctetStrValue));
}

INT4
IssHardwareVersionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssHardwareVersion (pMultiData->pOctetStrValue));
}

INT4
IssFirmwareVersionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssFirmwareVersion (pMultiData->pOctetStrValue));
}

INT4
IssDefaultIpAddrCfgModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssDefaultIpAddrCfgMode (&(pMultiData->i4_SLongValue)));
}

INT4
IssDefaultIpAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssDefaultIpAddr (&(pMultiData->u4_ULongValue)));
}

INT4
IssDefaultIpSubnetMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssDefaultIpSubnetMask (&(pMultiData->u4_ULongValue)));
}

INT4
IssEffectiveIpAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssEffectiveIpAddr (&(pMultiData->u4_ULongValue)));
}

INT4
IssDefaultInterfaceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssDefaultInterface (pMultiData->pOctetStrValue));
}

INT4
IssRestartGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssRestart (&(pMultiData->i4_SLongValue)));
}

INT4
IssConfigSaveOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssConfigSaveOption (&(pMultiData->i4_SLongValue)));
}

INT4
IssConfigSaveIpAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssConfigSaveIpAddr (&(pMultiData->u4_ULongValue)));
}

INT4
IssConfigSaveFileNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssConfigSaveFileName (pMultiData->pOctetStrValue));
}

INT4
IssInitiateConfigSaveGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssInitiateConfigSave (&(pMultiData->i4_SLongValue)));
}

INT4
IssConfigSaveStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssConfigSaveStatus (&(pMultiData->i4_SLongValue)));
}

INT4
IssConfigRestoreOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssConfigRestoreOption (&(pMultiData->i4_SLongValue)));
}

INT4
IssConfigRestoreIpAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssConfigRestoreIpAddr (&(pMultiData->u4_ULongValue)));
}

INT4
IssConfigRestoreFileNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssConfigRestoreFileName (pMultiData->pOctetStrValue));
}

INT4
IssInitiateConfigRestoreGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssInitiateConfigRestore (&(pMultiData->i4_SLongValue)));
}

INT4
IssConfigRestoreStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssConfigRestoreStatus (&(pMultiData->i4_SLongValue)));
}

INT4
IssDlImageFromIpGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssDlImageFromIp (&(pMultiData->u4_ULongValue)));
}

INT4
IssDlImageNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssDlImageName (pMultiData->pOctetStrValue));
}

INT4
IssInitiateDlImageGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssInitiateDlImage (&(pMultiData->i4_SLongValue)));
}

INT4
IssLoggingOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssLoggingOption (&(pMultiData->i4_SLongValue)));
}

INT4
IssUploadLogFileToIpGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssUploadLogFileToIp (&(pMultiData->u4_ULongValue)));
}

INT4
IssLogFileNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssLogFileName (pMultiData->pOctetStrValue));
}

INT4
IssInitiateUlLogFileGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssInitiateUlLogFile (&(pMultiData->i4_SLongValue)));
}

INT4
IssRemoteSaveStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssRemoteSaveStatus (&(pMultiData->i4_SLongValue)));
}

INT4
IssDownloadStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssDownloadStatus (&(pMultiData->i4_SLongValue)));
}

INT4
IssSysContactGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssSysContact (pMultiData->pOctetStrValue));
}

INT4
IssSysLocationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssSysLocation (pMultiData->pOctetStrValue));
}

INT4
IssLoginAuthenticationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssLoginAuthentication (&(pMultiData->i4_SLongValue)));
}

INT4
IssPamLoginPrivilegeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssPamLoginPrivilege (&(pMultiData->i4_SLongValue)));
}

INT4
IssSwitchBaseMacAddressGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    pMultiData->pOctetStrValue->i4_Length = 6;
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssSwitchBaseMacAddress
            ((tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList));
}

INT4
IssPamLoginPrivilegeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssPamLoginPrivilege (pMultiData->i4_SLongValue));
}

INT4
IssOOBInterfaceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssOOBInterface (&(pMultiData->i4_SLongValue)));
}

INT4
IssSwitchDateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssSwitchDate (pMultiData->pOctetStrValue));
}

INT4
IssNoCliConsoleGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssNoCliConsole (&(pMultiData->i4_SLongValue)));
}

INT4
IssDefaultIpAddrAllocProtocolGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssDefaultIpAddrAllocProtocol (&(pMultiData->i4_SLongValue)));
}

INT4
IssHttpPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssHttpPort (&(pMultiData->i4_SLongValue)));
}

INT4
IssHttpStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssHttpStatus (&(pMultiData->i4_SLongValue)));
}

INT4
IssConfigRestoreFileVersionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssConfigRestoreFileVersion (pMultiData->pOctetStrValue));
}

INT4
IssDefaultRmIfNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssDefaultRmIfName (pMultiData->pOctetStrValue));
}

INT4
IssDefaultVlanIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssDefaultVlanId (&(pMultiData->i4_SLongValue)));
}

INT4
IssNpapiModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssNpapiMode (pMultiData->pOctetStrValue));
}

INT4
IssConfigAutoSaveTriggerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssConfigAutoSaveTrigger (&(pMultiData->i4_SLongValue)));
}

INT4
IssConfigIncrSaveFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssConfigIncrSaveFlag (&(pMultiData->i4_SLongValue)));
}

INT4
IssConfigRollbackFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssConfigRollbackFlag (&(pMultiData->i4_SLongValue)));
}

INT4
IssConfigSyncUpOperationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssConfigSyncUpOperation (&(pMultiData->i4_SLongValue)));
}

INT4
IssFrontPanelPortCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssFrontPanelPortCount (&(pMultiData->i4_SLongValue)));
}

INT4
IssAuditLogStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssAuditLogStatus (&(pMultiData->i4_SLongValue)));
}

INT4
IssAuditLogFileNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssAuditLogFileName (pMultiData->pOctetStrValue));
}

INT4
IssAuditLogFileSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssAuditLogFileSize (&(pMultiData->u4_ULongValue)));
}

INT4
IssAuditLogResetGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssAuditLogReset (&(pMultiData->i4_SLongValue)));
}

INT4
IssAuditLogRemoteIpAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssAuditLogRemoteIpAddr (&(pMultiData->u4_ULongValue)));
}

INT4
IssAuditLogInitiateTransferGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssAuditLogInitiateTransfer (&(pMultiData->i4_SLongValue)));
}

INT4
IssAuditTransferFileNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssAuditTransferFileName (pMultiData->pOctetStrValue));
}

INT4
IssDownLoadTransferModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssDownLoadTransferMode (&(pMultiData->i4_SLongValue)));
}

INT4
IssDownLoadUserNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssDownLoadUserName (pMultiData->pOctetStrValue));
}

INT4
IssDownLoadPasswordGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssDownLoadPassword (pMultiData->pOctetStrValue));
}

INT4
IssUploadLogTransferModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssUploadLogTransferMode (&(pMultiData->i4_SLongValue)));
}

INT4
IssUploadLogUserNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssUploadLogUserName (pMultiData->pOctetStrValue));
}

INT4
IssUploadLogPasswdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssUploadLogPasswd (pMultiData->pOctetStrValue));
}

INT4
IssConfigSaveTransferModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssConfigSaveTransferMode (&(pMultiData->i4_SLongValue)));
}

INT4
IssConfigSaveUserNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssConfigSaveUserName (pMultiData->pOctetStrValue));
}

INT4
IssConfigSavePasswordGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssConfigSavePassword (pMultiData->pOctetStrValue));
}

INT4
IssSwitchMinThresholdTemperatureGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssSwitchMinThresholdTemperature
            (&(pMultiData->i4_SLongValue)));
}

INT4
IssSwitchMaxThresholdTemperatureGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssSwitchMaxThresholdTemperature
            (&(pMultiData->i4_SLongValue)));
}

INT4
IssSwitchCurrentTemperatureGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssSwitchCurrentTemperature (&(pMultiData->i4_SLongValue)));
}

INT4
IssSwitchMaxCPUThresholdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssSwitchMaxCPUThreshold (&(pMultiData->i4_SLongValue)));
}

INT4
IssSwitchCurrentCPUThresholdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssSwitchCurrentCPUThreshold (&(pMultiData->i4_SLongValue)));
}

INT4
IssSwitchPowerSurgeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssSwitchPowerSurge (&(pMultiData->i4_SLongValue)));
}

INT4
IssSwitchPowerFailureGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssSwitchPowerFailure (&(pMultiData->i4_SLongValue)));
}

INT4
IssSwitchCurrentPowerSupplyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssSwitchCurrentPowerSupply (&(pMultiData->i4_SLongValue)));
}

INT4
IssSwitchMaxRAMUsageGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssSwitchMaxRAMUsage (&(pMultiData->i4_SLongValue)));
}

INT4
IssSwitchCurrentRAMUsageGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssSwitchCurrentRAMUsage (&(pMultiData->i4_SLongValue)));
}

INT4
IssSwitchMaxFlashUsageGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssSwitchMaxFlashUsage (&(pMultiData->i4_SLongValue)));
}

INT4
IssSwitchCurrentFlashUsageGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssSwitchCurrentFlashUsage (&(pMultiData->i4_SLongValue)));
}

INT4
IssConfigRestoreFileFormatVersionGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssConfigRestoreFileFormatVersion
            (pMultiData->pOctetStrValue));
}

INT4
IssDebugOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssDebugOption (pMultiData->pOctetStrValue));
}

INT4
IssConfigDefaultValueSaveOptionGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssConfigDefaultValueSaveOption
            (&(pMultiData->i4_SLongValue)));
}

INT4
IssConfigSaveIpAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssConfigSaveIpAddrType (&(pMultiData->i4_SLongValue)));
}

INT4
IssConfigSaveIpvxAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssConfigSaveIpvxAddr (pMultiData->pOctetStrValue));
}

INT4
IssConfigRestoreIpAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssConfigRestoreIpAddrType (&(pMultiData->i4_SLongValue)));
}

INT4
IssConfigRestoreIpvxAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssConfigRestoreIpvxAddr (pMultiData->pOctetStrValue));
}

INT4
IssDlImageFromIpAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssDlImageFromIpAddrType (&(pMultiData->i4_SLongValue)));
}

INT4
IssDlImageFromIpvxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssDlImageFromIpvx (pMultiData->pOctetStrValue));
}

INT4
IssUploadLogFileToIpAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssUploadLogFileToIpAddrType (&(pMultiData->i4_SLongValue)));
}

INT4
IssUploadLogFileToIpvxGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssUploadLogFileToIpvx (pMultiData->pOctetStrValue));
}

INT4
IssAuditLogRemoteIpAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssAuditLogRemoteIpAddrType (&(pMultiData->i4_SLongValue)));
}

INT4
IssAuditLogRemoteIpvxAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssAuditLogRemoteIpvxAddr (pMultiData->pOctetStrValue));
}

INT4
IssSystemTimerSpeedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssSystemTimerSpeed (&(pMultiData->u4_ULongValue)));
}

INT4
IssMgmtInterfaceRoutingGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssMgmtInterfaceRouting (&(pMultiData->i4_SLongValue)));
}

INT4
IssMacLearnRateLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssMacLearnRateLimit (&(pMultiData->i4_SLongValue)));
}

INT4
IssMacLearnRateLimitIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssMacLearnRateLimitInterval (&(pMultiData->u4_ULongValue)));
}

INT4
IssVrfUnqMacFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssVrfUnqMacFlag (&(pMultiData->i4_SLongValue)));
}

INT4
IssLoginAttemptsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssLoginAttempts (&(pMultiData->i4_SLongValue)));
}

INT4
IssLoginLockTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssLoginLockTime (&(pMultiData->i4_SLongValue)));
}

INT4
IssAuditLogSizeThresholdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssAuditLogSizeThreshold (&(pMultiData->u4_ULongValue)));
}

INT4
IssTelnetStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssTelnetStatus (&(pMultiData->i4_SLongValue)));
}

INT4
IssWebSessionTimeOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssWebSessionTimeOut (&(pMultiData->i4_SLongValue)));
}

INT4
IssWebSessionMaxUsersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssWebSessionMaxUsers (&(pMultiData->i4_SLongValue)));
}

INT4
IssHeartBeatModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssHeartBeatMode (&(pMultiData->i4_SLongValue)));
}

INT4
IssRmRTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssRmRType (&(pMultiData->i4_SLongValue)));
}

INT4
IssRmDTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssRmDType (&(pMultiData->i4_SLongValue)));
}

INT4
IssTelnetClientStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssTelnetClientStatus (&(pMultiData->i4_SLongValue)));
}

INT4
IssSshClientStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssSshClientStatus (&(pMultiData->i4_SLongValue)));
}

INT4
IssActiveTelnetClientSessionsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssActiveTelnetClientSessions (&(pMultiData->i4_SLongValue)));
}

INT4
IssActiveSshClientSessionsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssActiveSshClientSessions (&(pMultiData->i4_SLongValue)));
}

INT4
IssLogFileSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssLogFileSize (&(pMultiData->u4_ULongValue)));
}

INT4
IssLogResetGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssLogReset (&(pMultiData->i4_SLongValue)));
}

INT4
IssLogSizeThresholdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssLogSizeThreshold (&(pMultiData->u4_ULongValue)));
}

INT4
IssAutomaticPortCreateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssAutomaticPortCreate (&(pMultiData->i4_SLongValue)));
}

INT4
IssUlRemoteLogFileNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssUlRemoteLogFileName (pMultiData->pOctetStrValue));
}

INT4
IssDefaultExecTimeOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssDefaultExecTimeOut (&(pMultiData->i4_SLongValue)));
}

INT4
IssDebugTimeStampOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssDebugTimeStampOption (&(pMultiData->i4_SLongValue)));
}

INT4
IssRmStackingInterfaceTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssRmStackingInterfaceType (&(pMultiData->i4_SLongValue)));
}

INT4
IssPeerLoggingOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssPeerLoggingOption (&(pMultiData->i4_SLongValue)));
}

INT4
IssStandbyRestartGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssStandbyRestart (&(pMultiData->i4_SLongValue)));
}

INT4
IssRestoreTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssRestoreType (&(pMultiData->i4_SLongValue)));
}

INT4
IssSwitchModeTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssSwitchModeType (&(pMultiData->i4_SLongValue)));
}

INT4
IssSwitchNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssSwitchName (pMultiData->pOctetStrValue));
}

INT4
IssDefaultIpAddrCfgModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssDefaultIpAddrCfgMode (pMultiData->i4_SLongValue));
}

INT4
IssDefaultIpAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssDefaultIpAddr (pMultiData->u4_ULongValue));
}

INT4
IssDefaultIpSubnetMaskSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssDefaultIpSubnetMask (pMultiData->u4_ULongValue));
}

INT4
IssDefaultInterfaceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssDefaultInterface (pMultiData->pOctetStrValue));
}

INT4
IssRestartSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssRestart (pMultiData->i4_SLongValue));
}

INT4
IssConfigSaveOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssConfigSaveOption (pMultiData->i4_SLongValue));
}

INT4
IssConfigSaveIpAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssConfigSaveIpAddr (pMultiData->u4_ULongValue));
}

INT4
IssConfigSaveFileNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssConfigSaveFileName (pMultiData->pOctetStrValue));
}

INT4
IssInitiateConfigSaveSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssInitiateConfigSave (pMultiData->i4_SLongValue));
}

INT4
IssConfigRestoreOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssConfigRestoreOption (pMultiData->i4_SLongValue));
}

INT4
IssConfigRestoreIpAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssConfigRestoreIpAddr (pMultiData->u4_ULongValue));
}

INT4
IssConfigRestoreFileNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssConfigRestoreFileName (pMultiData->pOctetStrValue));
}

INT4
IssInitiateConfigRestoreSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssInitiateConfigRestore (pMultiData->i4_SLongValue));
}

INT4
IssDlImageFromIpSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssDlImageFromIp (pMultiData->u4_ULongValue));
}

INT4
IssDlImageNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssDlImageName (pMultiData->pOctetStrValue));
}

INT4
IssInitiateDlImageSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssInitiateDlImage (pMultiData->i4_SLongValue));
}

INT4
IssLoggingOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssLoggingOption (pMultiData->i4_SLongValue));
}

INT4
IssUploadLogFileToIpSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssUploadLogFileToIp (pMultiData->u4_ULongValue));
}

INT4
IssLogFileNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssLogFileName (pMultiData->pOctetStrValue));
}

INT4
IssInitiateUlLogFileSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssInitiateUlLogFile (pMultiData->i4_SLongValue));
}

INT4
IssSysContactSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssSysContact (pMultiData->pOctetStrValue));
}

INT4
IssSysLocationSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssSysLocation (pMultiData->pOctetStrValue));
}

INT4
IssLoginAuthenticationSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssLoginAuthentication (pMultiData->i4_SLongValue));
}

INT4
IssSwitchBaseMacAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssSwitchBaseMacAddress
            ((*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)));
}

INT4
IssPamLoginPrivilegeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssPamLoginPrivilege
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssSwitchDateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssSwitchDate (pMultiData->pOctetStrValue));
}

INT4
IssNoCliConsoleSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssNoCliConsole (pMultiData->i4_SLongValue));
}

INT4
IssDefaultIpAddrAllocProtocolSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssDefaultIpAddrAllocProtocol (pMultiData->i4_SLongValue));
}

INT4
IssHttpPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssHttpPort (pMultiData->i4_SLongValue));
}

INT4
IssHttpStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssHttpStatus (pMultiData->i4_SLongValue));
}

INT4
IssDefaultRmIfNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssDefaultRmIfName (pMultiData->pOctetStrValue));
}

INT4
IssDefaultVlanIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssDefaultVlanId (pMultiData->i4_SLongValue));
}

INT4
IssNpapiModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssNpapiMode (pMultiData->pOctetStrValue));
}

INT4
IssConfigAutoSaveTriggerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssConfigAutoSaveTrigger (pMultiData->i4_SLongValue));
}

INT4
IssConfigIncrSaveFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssConfigIncrSaveFlag (pMultiData->i4_SLongValue));
}

INT4
IssConfigRollbackFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssConfigRollbackFlag (pMultiData->i4_SLongValue));
}

INT4
IssConfigSyncUpOperationSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssConfigSyncUpOperation (pMultiData->i4_SLongValue));
}

INT4
IssFrontPanelPortCountSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssFrontPanelPortCount (pMultiData->i4_SLongValue));
}

INT4
IssAuditLogStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssAuditLogStatus (pMultiData->i4_SLongValue));
}

INT4
IssAuditLogFileNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssAuditLogFileName (pMultiData->pOctetStrValue));
}

INT4
IssAuditLogFileSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssAuditLogFileSize (pMultiData->u4_ULongValue));
}

INT4
IssAuditLogResetSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssAuditLogReset (pMultiData->i4_SLongValue));
}

INT4
IssAuditLogRemoteIpAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssAuditLogRemoteIpAddr (pMultiData->u4_ULongValue));
}

INT4
IssAuditLogInitiateTransferSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssAuditLogInitiateTransfer (pMultiData->i4_SLongValue));
}

INT4
IssAuditTransferFileNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssAuditTransferFileName (pMultiData->pOctetStrValue));
}

INT4
IssDownLoadTransferModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssDownLoadTransferMode (pMultiData->i4_SLongValue));
}

INT4
IssDownLoadUserNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssDownLoadUserName (pMultiData->pOctetStrValue));
}

INT4
IssDownLoadPasswordSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssDownLoadPassword (pMultiData->pOctetStrValue));
}

INT4
IssUploadLogTransferModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssUploadLogTransferMode (pMultiData->i4_SLongValue));
}

INT4
IssUploadLogUserNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssUploadLogUserName (pMultiData->pOctetStrValue));
}

INT4
IssUploadLogPasswdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssUploadLogPasswd (pMultiData->pOctetStrValue));
}

INT4
IssConfigSaveTransferModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssConfigSaveTransferMode (pMultiData->i4_SLongValue));
}

INT4
IssConfigSaveUserNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssConfigSaveUserName (pMultiData->pOctetStrValue));
}

INT4
IssConfigSavePasswordSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssConfigSavePassword (pMultiData->pOctetStrValue));
}

INT4
IssSwitchMinThresholdTemperatureSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssSwitchMinThresholdTemperature (pMultiData->i4_SLongValue));
}

INT4
IssSwitchMaxThresholdTemperatureSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssSwitchMaxThresholdTemperature (pMultiData->i4_SLongValue));
}

INT4
IssSwitchMaxCPUThresholdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssSwitchMaxCPUThreshold (pMultiData->i4_SLongValue));
}

INT4
IssSwitchPowerSurgeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssSwitchPowerSurge (pMultiData->i4_SLongValue));
}

INT4
IssSwitchPowerFailureSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssSwitchPowerFailure (pMultiData->i4_SLongValue));
}

INT4
IssSwitchMaxRAMUsageSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssSwitchMaxRAMUsage (pMultiData->i4_SLongValue));
}

INT4
IssSwitchMaxFlashUsageSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssSwitchMaxFlashUsage (pMultiData->i4_SLongValue));
}

INT4
IssDebugOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssDebugOption (pMultiData->pOctetStrValue));
}

INT4
IssConfigDefaultValueSaveOptionSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssConfigDefaultValueSaveOption (pMultiData->i4_SLongValue));
}

INT4
IssConfigSaveIpAddrTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssConfigSaveIpAddrType (pMultiData->i4_SLongValue));
}

INT4
IssConfigSaveIpvxAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssConfigSaveIpvxAddr (pMultiData->pOctetStrValue));
}

INT4
IssConfigRestoreIpAddrTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssConfigRestoreIpAddrType (pMultiData->i4_SLongValue));
}

INT4
IssConfigRestoreIpvxAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssConfigRestoreIpvxAddr (pMultiData->pOctetStrValue));
}

INT4
IssDlImageFromIpAddrTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssDlImageFromIpAddrType (pMultiData->i4_SLongValue));
}

INT4
IssDlImageFromIpvxSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssDlImageFromIpvx (pMultiData->pOctetStrValue));
}

INT4
IssUploadLogFileToIpAddrTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssUploadLogFileToIpAddrType (pMultiData->i4_SLongValue));
}

INT4
IssUploadLogFileToIpvxSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssUploadLogFileToIpvx (pMultiData->pOctetStrValue));
}

INT4
IssAuditLogRemoteIpAddrTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssAuditLogRemoteIpAddrType (pMultiData->i4_SLongValue));
}

INT4
IssAuditLogRemoteIpvxAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssAuditLogRemoteIpvxAddr (pMultiData->pOctetStrValue));
}

INT4
IssSystemTimerSpeedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssSystemTimerSpeed (pMultiData->u4_ULongValue));
}

INT4
IssMgmtInterfaceRoutingSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssMgmtInterfaceRouting (pMultiData->i4_SLongValue));
}

INT4
IssMacLearnRateLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssMacLearnRateLimit (pMultiData->i4_SLongValue));
}

INT4
IssMacLearnRateLimitIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssMacLearnRateLimitInterval (pMultiData->u4_ULongValue));
}

INT4
IssVrfUnqMacFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssVrfUnqMacFlag (pMultiData->i4_SLongValue));
}

INT4
IssLoginAttemptsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssLoginAttempts (pMultiData->i4_SLongValue));
}

INT4
IssLoginLockTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssLoginLockTime (pMultiData->i4_SLongValue));
}

INT4
IssAuditLogSizeThresholdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssAuditLogSizeThreshold (pMultiData->u4_ULongValue));
}

INT4
IssTelnetStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssTelnetStatus (pMultiData->i4_SLongValue));
}

INT4
IssWebSessionTimeOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssWebSessionTimeOut (pMultiData->i4_SLongValue));
}

INT4
IssWebSessionMaxUsersSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssWebSessionMaxUsers (pMultiData->i4_SLongValue));
}

INT4
IssHeartBeatModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssHeartBeatMode (pMultiData->i4_SLongValue));
}

INT4
IssRmRTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssRmRType (pMultiData->i4_SLongValue));
}

INT4
IssRmDTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssRmDType (pMultiData->i4_SLongValue));
}

INT4
IssTelnetClientStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssTelnetClientStatus (pMultiData->i4_SLongValue));
}

INT4
IssSshClientStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssSshClientStatus (pMultiData->i4_SLongValue));
}

INT4
IssLogFileSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssLogFileSize (pMultiData->u4_ULongValue));
}

INT4
IssLogResetSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssLogReset (pMultiData->i4_SLongValue));
}

INT4
IssLogSizeThresholdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssLogSizeThreshold (pMultiData->u4_ULongValue));
}

INT4
IssAutomaticPortCreateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssAutomaticPortCreate (pMultiData->i4_SLongValue));
}

INT4
IssUlRemoteLogFileNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssUlRemoteLogFileName (pMultiData->pOctetStrValue));
}

INT4
IssDefaultExecTimeOutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssDefaultExecTimeOut (pMultiData->i4_SLongValue));
}

INT4
IssRmStackingInterfaceTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssRmStackingInterfaceType (pMultiData->i4_SLongValue));
}

INT4
IssPeerLoggingOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssPeerLoggingOption (pMultiData->i4_SLongValue));
}

INT4
IssStandbyRestartSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssStandbyRestart (pMultiData->i4_SLongValue));
}

INT4
IssRestoreTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssRestoreType (pMultiData->i4_SLongValue));
}

INT4
IssSwitchModeTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssSwitchModeType (pMultiData->i4_SLongValue));
}

INT4
IssDebugTimeStampOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssDebugTimeStampOption (pMultiData->i4_SLongValue));
}

INT4
IssSwitchNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssSwitchName (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssDefaultIpAddrCfgModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssDefaultIpAddrCfgMode
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssDefaultIpAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssDefaultIpAddr (pu4Error, pMultiData->u4_ULongValue));
}

INT4
IssDefaultIpSubnetMaskTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssDefaultIpSubnetMask
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
IssDefaultInterfaceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssDefaultInterface
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssRestartTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssRestart (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssConfigSaveOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssConfigSaveOption (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssConfigSaveIpAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssConfigSaveIpAddr (pu4Error, pMultiData->u4_ULongValue));
}

INT4
IssConfigSaveFileNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssConfigSaveFileName
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssInitiateConfigSaveTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssInitiateConfigSave
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssConfigRestoreOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssConfigRestoreOption
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssConfigRestoreIpAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssConfigRestoreIpAddr
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
IssConfigRestoreFileNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssConfigRestoreFileName
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssInitiateConfigRestoreTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssInitiateConfigRestore
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssDlImageFromIpTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssDlImageFromIp (pu4Error, pMultiData->u4_ULongValue));
}

INT4
IssDlImageNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssDlImageName (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssInitiateDlImageTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssInitiateDlImage (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssLoggingOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssLoggingOption (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssUploadLogFileToIpTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssUploadLogFileToIp
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
IssLogFileNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssLogFileName (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssInitiateUlLogFileTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssInitiateUlLogFile
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssSysContactTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssSysContact (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssSysLocationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssSysLocation (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssLoginAuthenticationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssLoginAuthentication
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssSwitchBaseMacAddressTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssSwitchBaseMacAddress
            (pu4Error,
             (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)));
}

INT4
IssPamLoginPrivilegeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssPamLoginPrivilege
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssSwitchDateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssSwitchDate (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssNoCliConsoleTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssNoCliConsole (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssDefaultIpAddrAllocProtocolTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssDefaultIpAddrAllocProtocol
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssHttpPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssHttpPort (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssHttpStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssHttpStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssDefaultRmIfNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssDefaultRmIfName (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssDefaultVlanIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssDefaultVlanId (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssNpapiModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssNpapiMode (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssConfigAutoSaveTriggerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssConfigAutoSaveTrigger
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssConfigIncrSaveFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssConfigIncrSaveFlag
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssConfigRollbackFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssConfigRollbackFlag
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssConfigSyncUpOperationTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssConfigSyncUpOperation
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssFrontPanelPortCountTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssFrontPanelPortCount
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssAuditLogStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssAuditLogStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssAuditLogFileNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssAuditLogFileName
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssAuditLogFileSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssAuditLogFileSize (pu4Error, pMultiData->u4_ULongValue));
}

INT4
IssAuditLogResetTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssAuditLogReset (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssAuditLogRemoteIpAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssAuditLogRemoteIpAddr
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
IssAuditLogInitiateTransferTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssAuditLogInitiateTransfer
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssAuditTransferFileNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssAuditTransferFileName
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssDownLoadTransferModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssDownLoadTransferMode
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssDownLoadUserNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssDownLoadUserName
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssDownLoadPasswordTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssDownLoadPassword
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssUploadLogTransferModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssUploadLogTransferMode
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssUploadLogUserNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssUploadLogUserName
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssUploadLogPasswdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssUploadLogPasswd (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssConfigSaveTransferModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssConfigSaveTransferMode
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssConfigSaveUserNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssConfigSaveUserName
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssConfigSavePasswordTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssConfigSavePassword
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssSwitchMinThresholdTemperatureTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssSwitchMinThresholdTemperature
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssSwitchMaxThresholdTemperatureTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssSwitchMaxThresholdTemperature
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssSwitchMaxCPUThresholdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssSwitchMaxCPUThreshold
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssSwitchPowerSurgeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssSwitchPowerSurge (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssSwitchPowerFailureTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssSwitchPowerFailure
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssSwitchMaxRAMUsageTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssSwitchMaxRAMUsage
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssSwitchMaxFlashUsageTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssSwitchMaxFlashUsage
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssDebugOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssDebugOption (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssConfigDefaultValueSaveOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssConfigDefaultValueSaveOption
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssConfigSaveIpAddrTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssConfigSaveIpAddrType
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssConfigSaveIpvxAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssConfigSaveIpvxAddr
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssConfigRestoreIpAddrTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssConfigRestoreIpAddrType
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssConfigRestoreIpvxAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssConfigRestoreIpvxAddr
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssDlImageFromIpAddrTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssDlImageFromIpAddrType
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssDlImageFromIpvxTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssDlImageFromIpvx (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssUploadLogFileToIpAddrTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssUploadLogFileToIpAddrType
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssUploadLogFileToIpvxTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssUploadLogFileToIpvx
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssAuditLogRemoteIpAddrTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssAuditLogRemoteIpAddrType
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssAuditLogRemoteIpvxAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssAuditLogRemoteIpvxAddr
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssSystemTimerSpeedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssSystemTimerSpeed (pu4Error, pMultiData->u4_ULongValue));
}

INT4
IssMgmtInterfaceRoutingTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssMgmtInterfaceRouting
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssMacLearnRateLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssMacLearnRateLimit
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssMacLearnRateLimitIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssMacLearnRateLimitInterval
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
IssVrfUnqMacFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssVrfUnqMacFlag (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssLoginAttemptsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssLoginAttempts (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssLoginLockTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssLoginLockTime (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssAuditLogSizeThresholdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssAuditLogSizeThreshold
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
IssTelnetStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssTelnetStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssWebSessionTimeOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssWebSessionTimeOut
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssWebSessionMaxUsersTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssWebSessionMaxUsers
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssHeartBeatModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssHeartBeatMode (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssRmRTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssRmRType (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssRmDTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssRmDType (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssTelnetClientStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssTelnetClientStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssSshClientStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssSshClientStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssLogFileSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssLogFileSize (pu4Error, pMultiData->u4_ULongValue));
}

INT4
IssLogResetTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssLogReset (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssLogSizeThresholdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssLogSizeThreshold (pu4Error, pMultiData->u4_ULongValue));
}

INT4
IssAutomaticPortCreateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssAutomaticPortCreate
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssUlRemoteLogFileNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssUlRemoteLogFileName
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssDefaultExecTimeOutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssDefaultExecTimeOut
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssRmStackingInterfaceTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssRmStackingInterfaceType
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssPeerLoggingOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssPeerLoggingOption
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssStandbyRestartTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssStandbyRestart (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssRestoreTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssRestoreType (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssSwitchModeTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssSwitchModeType (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssDebugTimeStampOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssDebugTimeStampOption
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssSwitchNameDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssSwitchName (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssDefaultIpAddrCfgModeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssDefaultIpAddrCfgMode
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssDefaultIpAddrDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssDefaultIpAddr (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssDefaultIpSubnetMaskDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssDefaultIpSubnetMask
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssDefaultInterfaceDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssDefaultInterface
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssRestartDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssRestart (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssConfigSaveOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssConfigSaveOption
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssConfigSaveIpAddrDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssConfigSaveIpAddr
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssConfigSaveFileNameDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssConfigSaveFileName
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssInitiateConfigSaveDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssInitiateConfigSave
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssConfigRestoreOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssConfigRestoreOption
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssConfigRestoreIpAddrDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssConfigRestoreIpAddr
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssConfigRestoreFileNameDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssConfigRestoreFileName
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssInitiateConfigRestoreDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssInitiateConfigRestore
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssDlImageFromIpDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssDlImageFromIp (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssDlImageNameDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssDlImageName (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssInitiateDlImageDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssInitiateDlImage
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssLoggingOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssLoggingOption (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssUploadLogFileToIpDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssUploadLogFileToIp
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssLogFileNameDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssLogFileName (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssInitiateUlLogFileDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssInitiateUlLogFile
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssSysContactDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssSysContact (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssSysLocationDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssSysLocation (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssLoginAuthenticationDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssLoginAuthentication
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssSwitchBaseMacAddressDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssSwitchBaseMacAddress
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssSwitchDateDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssSwitchDate (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssNoCliConsoleDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssNoCliConsole (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssDefaultIpAddrAllocProtocolDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssDefaultIpAddrAllocProtocol
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssHttpPortDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssHttpPort (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssHttpStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssHttpStatus (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssDefaultRmIfNameDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssDefaultRmIfName
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssDefaultVlanIdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssDefaultVlanId (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssNpapiModeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssNpapiMode (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssConfigAutoSaveTriggerDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssConfigAutoSaveTrigger
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssConfigIncrSaveFlagDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssConfigIncrSaveFlag
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssConfigRollbackFlagDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssConfigRollbackFlag
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssConfigSyncUpOperationDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssConfigSyncUpOperation
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssFrontPanelPortCountDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssFrontPanelPortCount
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssAuditLogStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssAuditLogStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssAuditLogFileNameDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssAuditLogFileName
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssAuditLogFileSizeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssAuditLogFileSize
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssAuditLogResetDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssAuditLogReset (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssAuditLogRemoteIpAddrDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssAuditLogRemoteIpAddr
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssAuditLogInitiateTransferDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssAuditLogInitiateTransfer
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssAuditTransferFileNameDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssAuditTransferFileName
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssDownLoadTransferModeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssDownLoadTransferMode
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssDownLoadUserNameDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssDownLoadUserName
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssDownLoadPasswordDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssDownLoadPassword
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssUploadLogTransferModeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssUploadLogTransferMode
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssUploadLogUserNameDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssUploadLogUserName
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssUploadLogPasswdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssUploadLogPasswd
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssConfigSaveTransferModeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssConfigSaveTransferMode
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssConfigSaveUserNameDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssConfigSaveUserName
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssConfigSavePasswordDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssConfigSavePassword
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssSwitchMinThresholdTemperatureDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssSwitchMinThresholdTemperature
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssSwitchMaxThresholdTemperatureDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssSwitchMaxThresholdTemperature
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssSwitchMaxCPUThresholdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssSwitchMaxCPUThreshold
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssSwitchPowerSurgeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssSwitchPowerSurge
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssSwitchPowerFailureDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssSwitchPowerFailure
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssSwitchMaxRAMUsageDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssSwitchMaxRAMUsage
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssSwitchMaxFlashUsageDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssSwitchMaxFlashUsage
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssDebugOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssDebugOption (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssConfigDefaultValueSaveOptionDep (UINT4 *pu4Error,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssConfigDefaultValueSaveOption
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssConfigSaveIpAddrTypeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssConfigSaveIpAddrType
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssConfigSaveIpvxAddrDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssConfigSaveIpvxAddr
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssConfigRestoreIpAddrTypeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssConfigRestoreIpAddrType
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssConfigRestoreIpvxAddrDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssConfigRestoreIpvxAddr
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssDlImageFromIpAddrTypeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssDlImageFromIpAddrType
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssDlImageFromIpvxDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssDlImageFromIpvx
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssUploadLogFileToIpAddrTypeDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssUploadLogFileToIpAddrType
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssUploadLogFileToIpvxDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssUploadLogFileToIpvx
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssAuditLogRemoteIpAddrTypeDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssAuditLogRemoteIpAddrType
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssAuditLogRemoteIpvxAddrDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssAuditLogRemoteIpvxAddr
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssSystemTimerSpeedDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssSystemTimerSpeed
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssMgmtInterfaceRoutingDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssMgmtInterfaceRouting
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssMacLearnRateLimitDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssMacLearnRateLimit
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssMacLearnRateLimitIntervalDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssMacLearnRateLimitInterval
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssVrfUnqMacFlagDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssVrfUnqMacFlag (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssLoginAttemptsDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssLoginAttempts (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssLoginLockTimeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssLoginLockTime (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssAuditLogSizeThresholdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssAuditLogSizeThreshold
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssTelnetStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssTelnetStatus (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssWebSessionTimeOutDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssWebSessionTimeOut
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssWebSessionMaxUsersDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssWebSessionMaxUsers
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssHeartBeatModeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssHeartBeatMode (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssRmRTypeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssRmRType (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssRmDTypeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssRmDType (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssTelnetClientStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssTelnetClientStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssSshClientStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssSshClientStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssLogFileSizeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssLogFileSize (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssLogResetDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssLogReset (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssLogSizeThresholdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssLogSizeThreshold
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssAutomaticPortCreateDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssAutomaticPortCreate
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssUlRemoteLogFileNameDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssUlRemoteLogFileName
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssDefaultExecTimeOutDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssDefaultExecTimeOut
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssRmStackingInterfaceTypeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssRmStackingInterfaceType
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssPeerLoggingOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssPeerLoggingOption
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssStandbyRestartDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssStandbyRestart
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssRestoreTypeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssRestoreType (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssSwitchModeTypeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssSwitchModeType
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIssConfigCtrlTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIssConfigCtrlTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIssConfigCtrlTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IssConfigCtrlEgressStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssConfigCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssConfigCtrlEgressStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssConfigCtrlStatsCollectionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssConfigCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssConfigCtrlStatsCollection
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssConfigCtrlStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssConfigCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssConfigCtrlStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
IssClearConfigGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssClearConfig (&(pMultiData->i4_SLongValue)));
}

INT4
IssClearConfigFileNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssClearConfigFileName (pMultiData->pOctetStrValue));
}

INT4
IssConfigCtrlEgressStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssConfigCtrlEgressStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssConfigCtrlStatsCollectionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssConfigCtrlStatsCollection
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssConfigCtrlStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssConfigCtrlStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
IssClearConfigSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssClearConfig (pMultiData->i4_SLongValue));
}

INT4
IssClearConfigFileNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssClearConfigFileName (pMultiData->pOctetStrValue));
}

INT4
IssConfigCtrlEgressStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2IssConfigCtrlEgressStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
IssConfigCtrlStatsCollectionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2IssConfigCtrlStatsCollection (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
IssConfigCtrlStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2IssConfigCtrlStatus (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
IssClearConfigTest (UINT4 *pu4Error,
                    tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssClearConfig (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssClearConfigFileNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssClearConfigFileName
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssDebugTimeStampOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssDebugTimeStampOption
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssConfigCtrlTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssConfigCtrlTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssClearConfigDep (UINT4 *pu4Error,
                   tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssClearConfig (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssClearConfigFileNameDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssClearConfigFileName (pu4Error, pSnmpIndexList,
                                            pSnmpvarbinds));
}

INT4
GetNextIndexIssPortCtrlTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIssPortCtrlTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIssPortCtrlTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IssPortCtrlModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssPortCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssPortCtrlMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
IssPortCtrlDuplexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssPortCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssPortCtrlDuplex (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
IssPortCtrlSpeedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssPortCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssPortCtrlSpeed (pMultiIndex->pIndex[0].i4_SLongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
IssPortCtrlFlowControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssPortCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssPortCtrlFlowControl (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
IssPortCtrlRenegotiateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssPortCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssPortCtrlRenegotiate (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
IssPortCtrlMaxMacAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssPortCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssPortCtrlMaxMacAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
IssPortCtrlMaxMacActionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssPortCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssPortCtrlMaxMacAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
IssPortHOLBlockPreventionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssPortCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssPortHOLBlockPrevention
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssPortAutoNegAdvtCapBitsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssPortCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssPortAutoNegAdvtCapBits
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
IssPortCpuControlledLearningGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssPortCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssPortCpuControlledLearning
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssPortMdiOrMdixCapGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssPortCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssPortMdiOrMdixCap (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
IssPortCtrlFlowControlMaxRateGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssPortCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssPortCtrlFlowControlMaxRate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssPortCtrlFlowControlMinRateGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssPortCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssPortCtrlFlowControlMinRate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssPortCtrlModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssPortCtrlMode (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
IssPortCtrlDuplexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssPortCtrlDuplex (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
IssPortCtrlSpeedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssPortCtrlSpeed (pMultiIndex->pIndex[0].i4_SLongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
IssPortCtrlFlowControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssPortCtrlFlowControl (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
IssPortCtrlRenegotiateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssPortCtrlRenegotiate (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
IssPortCtrlMaxMacAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssPortCtrlMaxMacAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
IssPortCtrlMaxMacActionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssPortCtrlMaxMacAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
IssPortHOLBlockPreventionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssPortHOLBlockPrevention
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssPortAutoNegAdvtCapBitsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssPortAutoNegAdvtCapBits
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
IssPortCpuControlledLearningSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssPortCpuControlledLearning
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssPortMdiOrMdixCapSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssPortMdiOrMdixCap (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
IssPortCtrlFlowControlMaxRateSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetIssPortCtrlFlowControlMaxRate
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssPortCtrlFlowControlMinRateSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetIssPortCtrlFlowControlMinRate
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssPortCtrlModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2IssPortCtrlMode (pu4Error,
                                      pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
IssPortCtrlDuplexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2IssPortCtrlDuplex (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
IssPortCtrlSpeedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2IssPortCtrlSpeed (pu4Error,
                                       pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
IssPortCtrlFlowControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2IssPortCtrlFlowControl (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
IssPortCtrlRenegotiateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2IssPortCtrlRenegotiate (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
IssPortCtrlMaxMacAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2IssPortCtrlMaxMacAddr (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
IssPortCtrlMaxMacActionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2IssPortCtrlMaxMacAction (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
IssPortHOLBlockPreventionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2IssPortHOLBlockPrevention (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
IssPortAutoNegAdvtCapBitsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2IssPortAutoNegAdvtCapBits (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->pOctetStrValue));

}

INT4
IssPortCpuControlledLearningTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2IssPortCpuControlledLearning (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
IssPortMdiOrMdixCapTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2IssPortMdiOrMdixCap (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
IssPortCtrlFlowControlMaxRateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2IssPortCtrlFlowControlMaxRate (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
IssPortCtrlFlowControlMinRateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2IssPortCtrlFlowControlMinRate (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
IssPortCtrlTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssPortCtrlTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIssPortIsolationTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIssPortIsolationTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIssPortIsolationTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IssPortIsolationStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssPortIsolationTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssPortIsolationStorageType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssPortIsolationRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssPortIsolationTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssPortIsolationRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssPortIsolationRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssPortIsolationRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssPortIsolationRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2IssPortIsolationRowStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[1].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[2].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
IssPortIsolationTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssPortIsolationTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssMirrorStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssMirrorStatus (&(pMultiData->i4_SLongValue)));
}

INT4
IssMirrorToPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssMirrorToPort (&(pMultiData->i4_SLongValue)));
}

INT4
IssMirrorStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssMirrorStatus (pMultiData->i4_SLongValue));
}

INT4
IssMirrorToPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssMirrorToPort (pMultiData->i4_SLongValue));
}

INT4
IssMirrorStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssMirrorStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssMirrorToPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssMirrorToPort (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssMirrorStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssMirrorStatus (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssMirrorToPortDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssMirrorToPort (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIssMirrorCtrlTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIssMirrorCtrlTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIssMirrorCtrlTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IssMirrorCtrlIngressMirroringGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssMirrorCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssMirrorCtrlIngressMirroring
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssMirrorCtrlEgressMirroringGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssMirrorCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssMirrorCtrlEgressMirroring
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssMirrorCtrlStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssMirrorCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssMirrorCtrlStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
IssMirrorCtrlIngressMirroringSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetIssMirrorCtrlIngressMirroring
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlEgressMirroringSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssMirrorCtrlEgressMirroring
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssMirrorCtrlStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlIngressMirroringTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2IssMirrorCtrlIngressMirroring (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlEgressMirroringTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2IssMirrorCtrlEgressMirroring (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2IssMirrorCtrlStatus (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssMirrorCtrlTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssMirrorCtrlRemainingSrcRcrdsGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssMirrorCtrlRemainingSrcRcrds
            (&(pMultiData->i4_SLongValue)));
}

INT4
IssMirrorCtrlRemainingDestRcrdsGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssMirrorCtrlRemainingDestRcrds
            (&(pMultiData->i4_SLongValue)));
}

INT4
GetNextIndexIssMirrorCtrlExtnTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIssMirrorCtrlExtnTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIssMirrorCtrlExtnTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IssMirrorCtrlExtnMirrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssMirrorCtrlExtnTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssMirrorCtrlExtnMirrType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssMirrorCtrlExtnRSpanStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssMirrorCtrlExtnTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssMirrorCtrlExtnRSpanStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssMirrorCtrlExtnRSpanVlanIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssMirrorCtrlExtnTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssMirrorCtrlExtnRSpanVlanId
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssMirrorCtrlExtnRSpanContextGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssMirrorCtrlExtnTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssMirrorCtrlExtnRSpanContext
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssMirrorCtrlExtnStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssMirrorCtrlExtnTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssMirrorCtrlExtnStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
IssMirrorCtrlExtnMirrTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssMirrorCtrlExtnMirrType
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlExtnRSpanStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssMirrorCtrlExtnRSpanStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlExtnRSpanVlanIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssMirrorCtrlExtnRSpanVlanId
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlExtnRSpanContextSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetIssMirrorCtrlExtnRSpanContext
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlExtnStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssMirrorCtrlExtnStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlExtnMirrTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2IssMirrorCtrlExtnMirrType (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlExtnRSpanStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2IssMirrorCtrlExtnRSpanStatus (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlExtnRSpanVlanIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2IssMirrorCtrlExtnRSpanVlanId (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlExtnRSpanContextTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2IssMirrorCtrlExtnRSpanContext (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlExtnStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2IssMirrorCtrlExtnStatus (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlExtnTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssMirrorCtrlExtnTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIssMirrorCtrlExtnSrcTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIssMirrorCtrlExtnSrcTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIssMirrorCtrlExtnSrcTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IssMirrorCtrlExtnSrcCfgGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssMirrorCtrlExtnSrcTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssMirrorCtrlExtnSrcCfg (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
IssMirrorCtrlExtnSrcModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssMirrorCtrlExtnSrcTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssMirrorCtrlExtnSrcMode
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssMirrorCtrlExtnSrcCfgSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssMirrorCtrlExtnSrcCfg (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlExtnSrcModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssMirrorCtrlExtnSrcMode
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlExtnSrcCfgTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2IssMirrorCtrlExtnSrcCfg (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[1].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlExtnSrcModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2IssMirrorCtrlExtnSrcMode (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlExtnSrcTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssMirrorCtrlExtnSrcTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIssMirrorCtrlExtnSrcVlanTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIssMirrorCtrlExtnSrcVlanTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIssMirrorCtrlExtnSrcVlanTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IssMirrorCtrlExtnSrcVlanCfgGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssMirrorCtrlExtnSrcVlanTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssMirrorCtrlExtnSrcVlanCfg
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssMirrorCtrlExtnSrcVlanModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssMirrorCtrlExtnSrcVlanTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssMirrorCtrlExtnSrcVlanMode
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssMirrorCtrlExtnSrcVlanCfgSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssMirrorCtrlExtnSrcVlanCfg
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlExtnSrcVlanModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssMirrorCtrlExtnSrcVlanMode
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlExtnSrcVlanCfgTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2IssMirrorCtrlExtnSrcVlanCfg (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[1].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[2].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlExtnSrcVlanModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2IssMirrorCtrlExtnSrcVlanMode (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[1].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[2].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlExtnSrcVlanTableDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssMirrorCtrlExtnSrcVlanTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIssMirrorCtrlExtnDestinationTable (tSnmpIndex * pFirstMultiIndex,
                                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIssMirrorCtrlExtnDestinationTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIssMirrorCtrlExtnDestinationTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IssMirrorCtrlExtnDestCfgGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssMirrorCtrlExtnDestinationTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssMirrorCtrlExtnDestCfg
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssMirrorCtrlExtnDestCfgSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssMirrorCtrlExtnDestCfg
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlExtnDestCfgTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2IssMirrorCtrlExtnDestCfg (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
IssMirrorCtrlExtnDestinationTableDep (UINT4 *pu4Error,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssMirrorCtrlExtnDestinationTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIssIpAuthMgrTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIssIpAuthMgrTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIssIpAuthMgrTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IssIpAuthMgrPortListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssIpAuthMgrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssIpAuthMgrPortList (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
IssIpAuthMgrVlanListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssIpAuthMgrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssIpAuthMgrVlanList (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
IssIpAuthMgrOOBPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssIpAuthMgrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssIpAuthMgrOOBPort (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
IssIpAuthMgrAllowedServicesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssIpAuthMgrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssIpAuthMgrAllowedServices
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssIpAuthMgrRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssIpAuthMgrTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssIpAuthMgrRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
IssIpAuthMgrPortListSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssIpAuthMgrPortList (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
IssIpAuthMgrVlanListSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssIpAuthMgrVlanList (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
IssIpAuthMgrOOBPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssIpAuthMgrOOBPort (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
IssIpAuthMgrAllowedServicesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssIpAuthMgrAllowedServices
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
IssIpAuthMgrRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssIpAuthMgrRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
IssIpAuthMgrPortListTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2IssIpAuthMgrPortList (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
IssIpAuthMgrVlanListTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2IssIpAuthMgrVlanList (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
IssIpAuthMgrOOBPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2IssIpAuthMgrOOBPort (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
IssIpAuthMgrAllowedServicesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2IssIpAuthMgrAllowedServices (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
IssIpAuthMgrRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2IssIpAuthMgrRowStatus (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
IssIpAuthMgrTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssIpAuthMgrTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIssRateCtrlTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIssRateCtrlTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIssRateCtrlTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IssRateCtrlDLFLimitValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssRateCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssRateCtrlDLFLimitValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssRateCtrlBCASTLimitValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssRateCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssRateCtrlBCASTLimitValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssRateCtrlMCASTLimitValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssRateCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssRateCtrlMCASTLimitValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssRateCtrlPortRateLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssRateCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssRateCtrlPortRateLimit
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssRateCtrlPortBurstSizeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssRateCtrlTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssRateCtrlPortBurstSize
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssRateCtrlDLFLimitValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssRateCtrlDLFLimitValue
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssRateCtrlBCASTLimitValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssRateCtrlBCASTLimitValue
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssRateCtrlMCASTLimitValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssRateCtrlMCASTLimitValue
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssRateCtrlPortRateLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssRateCtrlPortRateLimit
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssRateCtrlPortBurstSizeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssRateCtrlPortBurstSize
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssRateCtrlDLFLimitValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2IssRateCtrlDLFLimitValue (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
IssRateCtrlBCASTLimitValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2IssRateCtrlBCASTLimitValue (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
IssRateCtrlMCASTLimitValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2IssRateCtrlMCASTLimitValue (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
IssRateCtrlPortRateLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2IssRateCtrlPortRateLimit (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
IssRateCtrlPortBurstSizeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2IssRateCtrlPortBurstSize (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
IssRateCtrlTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssRateCtrlTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIssL2FilterTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIssL2FilterTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIssL2FilterTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IssL2FilterPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL2FilterPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
IssL2FilterEtherTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL2FilterEtherType (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
IssL2FilterProtocolTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL2FilterProtocolType (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
IssL2FilterDstMacAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetIssL2FilterDstMacAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                         (tMacAddr *) pMultiData->
                                         pOctetStrValue->pu1_OctetList));

}

INT4
IssL2FilterSrcMacAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetIssL2FilterSrcMacAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                         (tMacAddr *) pMultiData->
                                         pOctetStrValue->pu1_OctetList));

}

INT4
IssL2FilterVlanIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL2FilterVlanId (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
IssL2FilterInPortListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL2FilterInPortList (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
IssL2FilterActionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL2FilterAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
IssL2FilterMatchCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL2FilterMatchCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
IssL2FilterStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL2FilterStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
IssL2FilterOutPortListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL2FilterOutPortList (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
IssL2FilterDirectionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL2FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL2FilterDirection (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
IssL2FilterPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL2FilterPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
IssL2FilterEtherTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL2FilterEtherType (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
IssL2FilterProtocolTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL2FilterProtocolType (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
IssL2FilterDstMacAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL2FilterDstMacAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                         (*(tMacAddr *) pMultiData->
                                          pOctetStrValue->pu1_OctetList)));

}

INT4
IssL2FilterSrcMacAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL2FilterSrcMacAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                         (*(tMacAddr *) pMultiData->
                                          pOctetStrValue->pu1_OctetList)));

}

INT4
IssL2FilterVlanIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL2FilterVlanId (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
IssL2FilterInPortListSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL2FilterInPortList (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
IssL2FilterActionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL2FilterAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
IssL2FilterStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL2FilterStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
IssL2FilterOutPortListSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL2FilterOutPortList (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
IssL2FilterDirectionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL2FilterDirection (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
IssL2FilterPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2IssL2FilterPriority (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
IssL2FilterEtherTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2IssL2FilterEtherType (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
IssL2FilterProtocolTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2IssL2FilterProtocolType (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
IssL2FilterDstMacAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return (nmhTestv2IssL2FilterDstMacAddr (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            (*(tMacAddr *) pMultiData->
                                             pOctetStrValue->pu1_OctetList)));

}

INT4
IssL2FilterSrcMacAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return (nmhTestv2IssL2FilterSrcMacAddr (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            (*(tMacAddr *) pMultiData->
                                             pOctetStrValue->pu1_OctetList)));

}

INT4
IssL2FilterVlanIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2IssL2FilterVlanId (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
IssL2FilterInPortListTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2IssL2FilterInPortList (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->pOctetStrValue));

}

INT4
IssL2FilterActionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2IssL2FilterAction (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
IssL2FilterStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2IssL2FilterStatus (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
IssL2FilterOutPortListTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2IssL2FilterOutPortList (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->pOctetStrValue));

}

INT4
IssL2FilterDirectionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2IssL2FilterDirection (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
IssL2FilterTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssL2FilterTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIssL3FilterTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIssL3FilterTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIssL3FilterTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IssL3FilterPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL3FilterPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
IssL3FilterProtocolGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL3FilterProtocol (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
IssL3FilterMessageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL3FilterMessageType (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
IssL3FilterMessageCodeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL3FilterMessageCode (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
IssL3FilterDstIpAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL3FilterDstIpAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
IssL3FilterSrcIpAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL3FilterSrcIpAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
IssL3FilterDstIpAddrMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL3FilterDstIpAddrMask
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IssL3FilterSrcIpAddrMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL3FilterSrcIpAddrMask
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IssL3FilterMinDstProtPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL3FilterMinDstProtPort
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IssL3FilterMaxDstProtPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL3FilterMaxDstProtPort
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IssL3FilterMinSrcProtPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL3FilterMinSrcProtPort
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IssL3FilterMaxSrcProtPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL3FilterMaxSrcProtPort
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
IssL3FilterInPortListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL3FilterInPortList (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
IssL3FilterOutPortListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL3FilterOutPortList (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
IssL3FilterAckBitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL3FilterAckBit (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
IssL3FilterRstBitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL3FilterRstBit (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
IssL3FilterTosGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL3FilterTos (pMultiIndex->pIndex[0].i4_SLongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
IssL3FilterDscpGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL3FilterDscp (pMultiIndex->pIndex[0].i4_SLongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
IssL3FilterDirectionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL3FilterDirection (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
IssL3FilterActionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL3FilterAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
IssL3FilterMatchCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL3FilterMatchCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
IssL3FilterStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL3FilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL3FilterStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
IssL3FilterPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL3FilterPriority (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
IssL3FilterProtocolSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL3FilterProtocol (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
IssL3FilterMessageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL3FilterMessageType (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
IssL3FilterMessageCodeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL3FilterMessageCode (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
IssL3FilterDstIpAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL3FilterDstIpAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
IssL3FilterSrcIpAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL3FilterSrcIpAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
IssL3FilterDstIpAddrMaskSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL3FilterDstIpAddrMask
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
IssL3FilterSrcIpAddrMaskSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL3FilterSrcIpAddrMask
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
IssL3FilterMinDstProtPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL3FilterMinDstProtPort
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
IssL3FilterMaxDstProtPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL3FilterMaxDstProtPort
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
IssL3FilterMinSrcProtPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL3FilterMinSrcProtPort
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
IssL3FilterMaxSrcProtPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL3FilterMaxSrcProtPort
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
IssL3FilterInPortListSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL3FilterInPortList (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
IssL3FilterOutPortListSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL3FilterOutPortList (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
IssL3FilterAckBitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL3FilterAckBit (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
IssL3FilterRstBitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL3FilterRstBit (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
IssL3FilterTosSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL3FilterTos (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
IssL3FilterDscpSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL3FilterDscp (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
IssL3FilterDirectionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL3FilterDirection (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
IssL3FilterActionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL3FilterAction (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
IssL3FilterStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL3FilterStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
IssL3FilterPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2IssL3FilterPriority (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
IssL3FilterProtocolTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2IssL3FilterProtocol (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
IssL3FilterMessageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2IssL3FilterMessageType (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
IssL3FilterMessageCodeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2IssL3FilterMessageCode (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
IssL3FilterDstIpAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2IssL3FilterDstIpAddr (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
IssL3FilterSrcIpAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2IssL3FilterSrcIpAddr (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
IssL3FilterDstIpAddrMaskTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2IssL3FilterDstIpAddrMask (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->u4_ULongValue));

}

INT4
IssL3FilterSrcIpAddrMaskTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2IssL3FilterSrcIpAddrMask (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->u4_ULongValue));

}

INT4
IssL3FilterMinDstProtPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2IssL3FilterMinDstProtPort (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->u4_ULongValue));

}

INT4
IssL3FilterMaxDstProtPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2IssL3FilterMaxDstProtPort (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->u4_ULongValue));

}

INT4
IssL3FilterMinSrcProtPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2IssL3FilterMinSrcProtPort (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->u4_ULongValue));

}

INT4
IssL3FilterMaxSrcProtPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2IssL3FilterMaxSrcProtPort (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->u4_ULongValue));

}

INT4
IssL3FilterInPortListTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2IssL3FilterInPortList (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->pOctetStrValue));

}

INT4
IssL3FilterOutPortListTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2IssL3FilterOutPortList (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->pOctetStrValue));

}

INT4
IssL3FilterAckBitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2IssL3FilterAckBit (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
IssL3FilterRstBitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2IssL3FilterRstBit (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
IssL3FilterTosTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2IssL3FilterTos (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
IssL3FilterDscpTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2IssL3FilterDscp (pu4Error,
                                      pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
IssL3FilterDirectionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2IssL3FilterDirection (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
IssL3FilterActionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2IssL3FilterAction (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
IssL3FilterStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2IssL3FilterStatus (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
IssL3FilterTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssL3FilterTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIssL4SwitchingFilterTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIssL4SwitchingFilterTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIssL4SwitchingFilterTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IssL4SwitchingProtocolGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL4SwitchingFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL4SwitchingProtocol (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
IssL4SwitchingPortNoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL4SwitchingFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL4SwitchingPortNo (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
IssL4SwitchingCopyToPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL4SwitchingFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL4SwitchingCopyToPort
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssL4SwitchingFilterStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssL4SwitchingFilterTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssL4SwitchingFilterStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
IssL4SwitchingProtocolSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL4SwitchingProtocol (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
IssL4SwitchingPortNoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL4SwitchingPortNo (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
IssL4SwitchingCopyToPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL4SwitchingCopyToPort
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssL4SwitchingFilterStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssL4SwitchingFilterStatus
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
IssL4SwitchingProtocolTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2IssL4SwitchingProtocol (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
IssL4SwitchingPortNoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2IssL4SwitchingPortNo (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
IssL4SwitchingCopyToPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2IssL4SwitchingCopyToPort (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
IssL4SwitchingFilterStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2IssL4SwitchingFilterStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
IssL4SwitchingFilterTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssL4SwitchingFilterTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIssModuleTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIssModuleTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIssModuleTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IssModuleSystemControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssModuleTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssModuleSystemControl (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
IssModuleSystemControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIssModuleSystemControl (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
IssModuleSystemControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2IssModuleSystemControl (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
IssModuleTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssModuleTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIssSwitchFanTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIssSwitchFanTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIssSwitchFanTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
IssSwitchFanStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIssSwitchFanTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIssSwitchFanStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
IssAuditTrapFileNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssAuditTrapFileName (pMultiData->pOctetStrValue));
}

INT4
IssLogTrapFileNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssLogTrapFileName (pMultiData->pOctetStrValue));
}

INT4
IssAclProvisionModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssAclProvisionMode (&(pMultiData->i4_SLongValue)));
}

INT4
IssAclTriggerCommitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssAclTriggerCommit (&(pMultiData->i4_SLongValue)));
}

INT4
IssAclProvisionModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssAclProvisionMode (pMultiData->i4_SLongValue));
}

INT4
IssAclTriggerCommitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssAclTriggerCommit (pMultiData->i4_SLongValue));
}

INT4
IssAclProvisionModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssAclProvisionMode (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssAclTriggerCommitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssAclTriggerCommit (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssAclProvisionModeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssAclProvisionMode
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssAclTriggerCommitDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssAclTriggerCommit
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssAclTrafficSeperationCtrlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssAclTrafficSeperationCtrl (&(pMultiData->i4_SLongValue)));
}

INT4
IssAclTrafficSeperationCtrlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssAclTrafficSeperationCtrl (pMultiData->i4_SLongValue));
}

INT4
IssAclTrafficSeperationCtrlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssAclTrafficSeperationCtrl (pu4Error,
                                                  pMultiData->i4_SLongValue));
}

INT4
IssAclTrafficSeperationCtrlDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssAclTrafficSeperationCtrl (pu4Error, pSnmpIndexList,
                                                 pSnmpvarbinds));
}

INT4
IssCpuMirrorTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssCpuMirrorType (&(pMultiData->i4_SLongValue)));
}

INT4
IssCpuMirrorToPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssCpuMirrorToPort (&(pMultiData->i4_SLongValue)));
}

INT4
IssCpuMirrorTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssCpuMirrorType (pMultiData->i4_SLongValue));
}

INT4
IssCpuMirrorToPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssCpuMirrorToPort (pMultiData->i4_SLongValue));
}

INT4
IssCpuMirrorTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssCpuMirrorType (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssCpuMirrorToPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssCpuMirrorToPort (pu4Error, pMultiData->i4_SLongValue));
}

INT4
IssCpuMirrorTypeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssCpuMirrorType (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
IssCpuMirrorToPortDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssCpuMirrorToPort
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

VOID
RegisterFSISS ()
{
    SNMPRegisterMibWithLock (&IssConfigCtrlTableOID, &IssConfigCtrlTableEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssPortCtrlTableOID, &IssPortCtrlTableEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssPortIsolationTableOID,
                             &IssPortIsolationTableEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssMirrorCtrlTableOID, &IssMirrorCtrlTableEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssMirrorCtrlExtnTableOID,
                             &IssMirrorCtrlExtnTableEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssMirrorCtrlExtnSrcTableOID,
                             &IssMirrorCtrlExtnSrcTableEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssMirrorCtrlExtnSrcVlanTableOID,
                             &IssMirrorCtrlExtnSrcVlanTableEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssMirrorCtrlExtnDestinationTableOID,
                             &IssMirrorCtrlExtnDestinationTableEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssIpAuthMgrTableOID, &IssIpAuthMgrTableEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssRateCtrlTableOID, &IssRateCtrlTableEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssL2FilterTableOID, &IssL2FilterTableEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssL3FilterTableOID, &IssL3FilterTableEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssL4SwitchingFilterTableOID,
                             &IssL4SwitchingFilterTableEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssModuleTableOID, &IssModuleTableEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssSwitchFanTableOID, &IssSwitchFanTableEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssWebSessionTimeOutOID,
                             &IssWebSessionTimeOutEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssWebSessionMaxUsersOID,
                             &IssWebSessionMaxUsersEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssDefaultExecTimeOutOID,
                             &IssDefaultExecTimeOutEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssRmStackingInterfaceTypeOID,
                             &IssRmStackingInterfaceTypeEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssPeerLoggingOptionOID,
                             &IssPeerLoggingOptionEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);

    SNMPRegisterMibWithLock (&IssStandbyRestartOID,
                             &IssStandbyRestartEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);

    SNMPRegisterMibWithLock (&IssRestoreTypeOID,
                             &IssRestoreTypeEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);

    SNMPRegisterMibWithLock (&IssSwitchModeTypeOID,
                             &IssSwitchModeTypeEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssDebugTimeStampOptionOID,
                             &IssDebugTimeStampOptionEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);

    SNMPRegisterMibWithLock (&IssCpuMirrorTypeOID,
                             &IssCpuMirrorTypeEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssCpuMirrorToPortOID,
                             &IssCpuMirrorToPortEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);

    SNMPRegisterMibWithLock (&IssHeartBeatModeOID, &IssHeartBeatModeEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&IssAutomaticPortCreateOID,
                             &IssAutomaticPortCreateEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&IssRmRTypeOID, &IssRmRTypeEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&IssRmDTypeOID, &IssRmDTypeEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&IssSwitchNameOID, &IssSwitchNameEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssHardwareVersionOID, &IssHardwareVersionEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssFirmwareVersionOID, &IssFirmwareVersionEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssDefaultIpAddrCfgModeOID,
                             &IssDefaultIpAddrCfgModeEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssDefaultIpAddrOID, &IssDefaultIpAddrEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssDefaultIpSubnetMaskOID,
                             &IssDefaultIpSubnetMaskEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssEffectiveIpAddrOID, &IssEffectiveIpAddrEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssDefaultInterfaceOID, &IssDefaultInterfaceEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssRestartOID, &IssRestartEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssConfigSaveOptionOID, &IssConfigSaveOptionEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssConfigSaveIpAddrOID, &IssConfigSaveIpAddrEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssConfigSaveFileNameOID,
                             &IssConfigSaveFileNameEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssInitiateConfigSaveOID,
                             &IssInitiateConfigSaveEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssConfigSaveStatusOID, &IssConfigSaveStatusEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssConfigRestoreOptionOID,
                             &IssConfigRestoreOptionEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssConfigRestoreIpAddrOID,
                             &IssConfigRestoreIpAddrEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssConfigRestoreFileNameOID,
                             &IssConfigRestoreFileNameEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssInitiateConfigRestoreOID,
                             &IssInitiateConfigRestoreEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssConfigRestoreStatusOID,
                             &IssConfigRestoreStatusEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssDlImageFromIpOID, &IssDlImageFromIpEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssDlImageNameOID, &IssDlImageNameEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssInitiateDlImageOID, &IssInitiateDlImageEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssLoggingOptionOID, &IssLoggingOptionEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssUploadLogFileToIpOID,
                             &IssUploadLogFileToIpEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssLogFileNameOID, &IssLogFileNameEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssInitiateUlLogFileOID,
                             &IssInitiateUlLogFileEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssRemoteSaveStatusOID, &IssRemoteSaveStatusEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssDownloadStatusOID, &IssDownloadStatusEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssSysContactOID, &IssSysContactEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssSysLocationOID, &IssSysLocationEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssLoginAuthenticationOID,
                             &IssLoginAuthenticationEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssSwitchBaseMacAddressOID,
                             &IssSwitchBaseMacAddressEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssOOBInterfaceOID, &IssOOBInterfaceEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssSwitchDateOID, &IssSwitchDateEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssNoCliConsoleOID, &IssNoCliConsoleEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssDefaultIpAddrAllocProtocolOID,
                             &IssDefaultIpAddrAllocProtocolEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssHttpPortOID, &IssHttpPortEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssHttpStatusOID, &IssHttpStatusEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssConfigRestoreFileVersionOID,
                             &IssConfigRestoreFileVersionEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssDefaultRmIfNameOID, &IssDefaultRmIfNameEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssDefaultVlanIdOID, &IssDefaultVlanIdEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssNpapiModeOID, &IssNpapiModeEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssConfigAutoSaveTriggerOID,
                             &IssConfigAutoSaveTriggerEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssConfigIncrSaveFlagOID,
                             &IssConfigIncrSaveFlagEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssConfigRollbackFlagOID,
                             &IssConfigRollbackFlagEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssConfigSyncUpOperationOID,
                             &IssConfigSyncUpOperationEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssFrontPanelPortCountOID,
                             &IssFrontPanelPortCountEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssAuditLogStatusOID, &IssAuditLogStatusEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssAuditLogFileNameOID, &IssAuditLogFileNameEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssAuditLogFileSizeOID, &IssAuditLogFileSizeEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssAuditLogResetOID, &IssAuditLogResetEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssAuditLogRemoteIpAddrOID,
                             &IssAuditLogRemoteIpAddrEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssAuditLogInitiateTransferOID,
                             &IssAuditLogInitiateTransferEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssAuditTransferFileNameOID,
                             &IssAuditTransferFileNameEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssDownLoadTransferModeOID,
                             &IssDownLoadTransferModeEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssDownLoadUserNameOID, &IssDownLoadUserNameEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssDownLoadPasswordOID, &IssDownLoadPasswordEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssUploadLogTransferModeOID,
                             &IssUploadLogTransferModeEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssUploadLogUserNameOID,
                             &IssUploadLogUserNameEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssUploadLogPasswdOID, &IssUploadLogPasswdEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssConfigSaveTransferModeOID,
                             &IssConfigSaveTransferModeEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssConfigSaveUserNameOID,
                             &IssConfigSaveUserNameEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssConfigSavePasswordOID,
                             &IssConfigSavePasswordEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssSwitchMinThresholdTemperatureOID,
                             &IssSwitchMinThresholdTemperatureEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssSwitchMaxThresholdTemperatureOID,
                             &IssSwitchMaxThresholdTemperatureEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssSwitchCurrentTemperatureOID,
                             &IssSwitchCurrentTemperatureEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssSwitchMaxCPUThresholdOID,
                             &IssSwitchMaxCPUThresholdEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssSwitchCurrentCPUThresholdOID,
                             &IssSwitchCurrentCPUThresholdEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssSwitchPowerSurgeOID, &IssSwitchPowerSurgeEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssSwitchPowerFailureOID,
                             &IssSwitchPowerFailureEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssSwitchCurrentPowerSupplyOID,
                             &IssSwitchCurrentPowerSupplyEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssSwitchMaxRAMUsageOID,
                             &IssSwitchMaxRAMUsageEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssSwitchCurrentRAMUsageOID,
                             &IssSwitchCurrentRAMUsageEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssSwitchMaxFlashUsageOID,
                             &IssSwitchMaxFlashUsageEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssSwitchCurrentFlashUsageOID,
                             &IssSwitchCurrentFlashUsageEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssConfigRestoreFileFormatVersionOID,
                             &IssConfigRestoreFileFormatVersionEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssDebugOptionOID, &IssDebugOptionEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssConfigDefaultValueSaveOptionOID,
                             &IssConfigDefaultValueSaveOptionEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssConfigSaveIpAddrTypeOID,
                             &IssConfigSaveIpAddrTypeEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssConfigSaveIpvxAddrOID,
                             &IssConfigSaveIpvxAddrEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssConfigRestoreIpAddrTypeOID,
                             &IssConfigRestoreIpAddrTypeEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssConfigRestoreIpvxAddrOID,
                             &IssConfigRestoreIpvxAddrEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssDlImageFromIpAddrTypeOID,
                             &IssDlImageFromIpAddrTypeEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssDlImageFromIpvxOID, &IssDlImageFromIpvxEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssUploadLogFileToIpAddrTypeOID,
                             &IssUploadLogFileToIpAddrTypeEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssUploadLogFileToIpvxOID,
                             &IssUploadLogFileToIpvxEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssAuditLogRemoteIpAddrTypeOID,
                             &IssAuditLogRemoteIpAddrTypeEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssAuditLogRemoteIpvxAddrOID,
                             &IssAuditLogRemoteIpvxAddrEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssSystemTimerSpeedOID, &IssSystemTimerSpeedEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssMgmtInterfaceRoutingOID,
                             &IssMgmtInterfaceRoutingEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssMacLearnRateLimitOID,
                             &IssMacLearnRateLimitEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssMacLearnRateLimitIntervalOID,
                             &IssMacLearnRateLimitIntervalEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssVrfUnqMacFlagOID, &IssVrfUnqMacFlagEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssLoginAttemptsOID, &IssLoginAttemptsEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssLoginLockTimeOID, &IssLoginLockTimeEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssAuditLogSizeThresholdOID,
                             &IssAuditLogSizeThresholdEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssTelnetStatusOID, &IssTelnetStatusEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssClearConfigOID, &IssClearConfigEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssClearConfigFileNameOID,
                             &IssClearConfigFileNameEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssTelnetClientStatusOID,
                             &IssTelnetClientStatusEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssSshClientStatusOID, &IssSshClientStatusEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssActiveTelnetClientSessionsOID,
                             &IssActiveTelnetClientSessionsEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssActiveSshClientSessionsOID,
                             &IssActiveSshClientSessionsEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssLogFileSizeOID, &IssLogFileSizeEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssLogResetOID, &IssLogResetEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssLogSizeThresholdOID, &IssLogSizeThresholdEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssUlRemoteLogFileNameOID,
                             &IssUlRemoteLogFileNameEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssMirrorStatusOID, &IssMirrorStatusEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssMirrorToPortOID, &IssMirrorToPortEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssMirrorCtrlRemainingSrcRcrdsOID,
                             &IssMirrorCtrlRemainingSrcRcrdsEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssMirrorCtrlRemainingDestRcrdsOID,
                             &IssMirrorCtrlRemainingDestRcrdsEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssAuditTrapFileNameOID,
                             &IssAuditTrapFileNameEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssAclProvisionModeOID, &IssAclProvisionModeEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssAclTriggerCommitOID, &IssAclTriggerCommitEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMibWithLock (&IssAclTrafficSeperationCtrlOID,
                             &IssAclTrafficSeperationCtrlEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&IssHealthChkStatusOID, &IssHealthChkStatusEntry,
                             IssLock, IssUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&IssHealthChkErrorReasonOID,
                             &IssHealthChkErrorReasonEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&IssHealthChkMemAllocErrPoolIdOID,
                             &IssHealthChkMemAllocErrPoolIdEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&IssHealthChkConfigRestoreStatusOID,
                             &IssHealthChkConfigRestoreStatusEntry, IssLock,
                             IssUnLock, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMibWithLock (&IssHealthChkClearCtrOID,
                             &IssHealthChkClearCtrEntry, IssLock, IssUnLock,
                             SNMP_MSR_TGR_FALSE);

    SNMPAddSysorEntry (&fsissOID, (const UINT1 *) "fsiss");
}

VOID
UnRegisterFSISS ()
{
    SNMPUnRegisterMib (&IssConfigCtrlTableOID, &IssConfigCtrlTableEntry);
    SNMPUnRegisterMib (&IssPortCtrlTableOID, &IssPortCtrlTableEntry);
    SNMPUnRegisterMib (&IssPortIsolationTableOID, &IssPortIsolationTableEntry);
    SNMPUnRegisterMib (&IssMirrorCtrlTableOID, &IssMirrorCtrlTableEntry);
    SNMPUnRegisterMib (&IssMirrorCtrlExtnTableOID,
                       &IssMirrorCtrlExtnTableEntry);
    SNMPUnRegisterMib (&IssMirrorCtrlExtnSrcTableOID,
                       &IssMirrorCtrlExtnSrcTableEntry);
    SNMPUnRegisterMib (&IssMirrorCtrlExtnSrcVlanTableOID,
                       &IssMirrorCtrlExtnSrcVlanTableEntry);
    SNMPUnRegisterMib (&IssMirrorCtrlExtnDestinationTableOID,
                       &IssMirrorCtrlExtnDestinationTableEntry);
    SNMPUnRegisterMib (&IssIpAuthMgrTableOID, &IssIpAuthMgrTableEntry);
    SNMPUnRegisterMib (&IssRateCtrlTableOID, &IssRateCtrlTableEntry);
    SNMPUnRegisterMib (&IssL2FilterTableOID, &IssL2FilterTableEntry);
    SNMPUnRegisterMib (&IssL3FilterTableOID, &IssL3FilterTableEntry);
    SNMPUnRegisterMib (&IssL4SwitchingFilterTableOID,
                       &IssL4SwitchingFilterTableEntry);
    SNMPUnRegisterMib (&IssModuleTableOID, &IssModuleTableEntry);
    SNMPUnRegisterMib (&IssSwitchFanTableOID, &IssSwitchFanTableEntry);
    SNMPUnRegisterMib (&IssWebSessionTimeOutOID, &IssWebSessionTimeOutEntry);
    SNMPUnRegisterMib (&IssWebSessionMaxUsersOID, &IssWebSessionMaxUsersEntry);
    SNMPUnRegisterMib (&IssDefaultExecTimeOutOID, &IssDefaultExecTimeOutEntry);
    SNMPUnRegisterMib (&IssRmStackingInterfaceTypeOID,
                       &IssRmStackingInterfaceTypeEntry);
    SNMPUnRegisterMib (&IssPeerLoggingOptionOID, &IssPeerLoggingOptionEntry);
    SNMPUnRegisterMib (&IssStandbyRestartOID, &IssStandbyRestartEntry);
    SNMPUnRegisterMib (&IssSwitchModeTypeOID, &IssSwitchModeTypeEntry);
    SNMPUnRegisterMib (&IssDebugTimeStampOptionOID,
                       &IssDebugTimeStampOptionEntry);

    SNMPUnRegisterMib (&IssCpuMirrorTypeOID, &IssCpuMirrorTypeEntry);
    SNMPUnRegisterMib (&IssCpuMirrorToPortOID, &IssCpuMirrorToPortEntry);

    SNMPUnRegisterMib (&IssHeartBeatModeOID, &IssHeartBeatModeEntry);
    SNMPUnRegisterMib (&IssAutomaticPortCreateOID,
                       &IssAutomaticPortCreateEntry);
    SNMPUnRegisterMib (&IssRmRTypeOID, &IssRmRTypeEntry);
    SNMPUnRegisterMib (&IssRmDTypeOID, &IssRmDTypeEntry);
    SNMPUnRegisterMib (&IssSwitchNameOID, &IssSwitchNameEntry);
    SNMPUnRegisterMib (&IssHardwareVersionOID, &IssHardwareVersionEntry);
    SNMPUnRegisterMib (&IssFirmwareVersionOID, &IssFirmwareVersionEntry);
    SNMPUnRegisterMib (&IssDefaultIpAddrCfgModeOID,
                       &IssDefaultIpAddrCfgModeEntry);
    SNMPUnRegisterMib (&IssDefaultIpAddrOID, &IssDefaultIpAddrEntry);
    SNMPUnRegisterMib (&IssDefaultIpSubnetMaskOID,
                       &IssDefaultIpSubnetMaskEntry);
    SNMPUnRegisterMib (&IssEffectiveIpAddrOID, &IssEffectiveIpAddrEntry);
    SNMPUnRegisterMib (&IssDefaultInterfaceOID, &IssDefaultInterfaceEntry);
    SNMPUnRegisterMib (&IssRestartOID, &IssRestartEntry);
    SNMPUnRegisterMib (&IssConfigSaveOptionOID, &IssConfigSaveOptionEntry);
    SNMPUnRegisterMib (&IssConfigSaveIpAddrOID, &IssConfigSaveIpAddrEntry);
    SNMPUnRegisterMib (&IssConfigSaveFileNameOID, &IssConfigSaveFileNameEntry);
    SNMPUnRegisterMib (&IssInitiateConfigSaveOID, &IssInitiateConfigSaveEntry);
    SNMPUnRegisterMib (&IssConfigSaveStatusOID, &IssConfigSaveStatusEntry);
    SNMPUnRegisterMib (&IssConfigRestoreOptionOID,
                       &IssConfigRestoreOptionEntry);
    SNMPUnRegisterMib (&IssConfigRestoreIpAddrOID,
                       &IssConfigRestoreIpAddrEntry);
    SNMPUnRegisterMib (&IssConfigRestoreFileNameOID,
                       &IssConfigRestoreFileNameEntry);
    SNMPUnRegisterMib (&IssInitiateConfigRestoreOID,
                       &IssInitiateConfigRestoreEntry);
    SNMPUnRegisterMib (&IssConfigRestoreStatusOID,
                       &IssConfigRestoreStatusEntry);
    SNMPUnRegisterMib (&IssDlImageFromIpOID, &IssDlImageFromIpEntry);
    SNMPUnRegisterMib (&IssDlImageNameOID, &IssDlImageNameEntry);
    SNMPUnRegisterMib (&IssInitiateDlImageOID, &IssInitiateDlImageEntry);
    SNMPUnRegisterMib (&IssLoggingOptionOID, &IssLoggingOptionEntry);
    SNMPUnRegisterMib (&IssUploadLogFileToIpOID, &IssUploadLogFileToIpEntry);
    SNMPUnRegisterMib (&IssLogFileNameOID, &IssLogFileNameEntry);
    SNMPUnRegisterMib (&IssInitiateUlLogFileOID, &IssInitiateUlLogFileEntry);
    SNMPUnRegisterMib (&IssRemoteSaveStatusOID, &IssRemoteSaveStatusEntry);
    SNMPUnRegisterMib (&IssDownloadStatusOID, &IssDownloadStatusEntry);
    SNMPUnRegisterMib (&IssSysContactOID, &IssSysContactEntry);
    SNMPUnRegisterMib (&IssSysLocationOID, &IssSysLocationEntry);
    SNMPUnRegisterMib (&IssLoginAuthenticationOID,
                       &IssLoginAuthenticationEntry);
    SNMPUnRegisterMib (&IssSwitchBaseMacAddressOID,
                       &IssSwitchBaseMacAddressEntry);
    SNMPUnRegisterMib (&IssOOBInterfaceOID, &IssOOBInterfaceEntry);
    SNMPUnRegisterMib (&IssSwitchDateOID, &IssSwitchDateEntry);
    SNMPUnRegisterMib (&IssNoCliConsoleOID, &IssNoCliConsoleEntry);
    SNMPUnRegisterMib (&IssDefaultIpAddrAllocProtocolOID,
                       &IssDefaultIpAddrAllocProtocolEntry);
    SNMPUnRegisterMib (&IssHttpPortOID, &IssHttpPortEntry);
    SNMPUnRegisterMib (&IssHttpStatusOID, &IssHttpStatusEntry);
    SNMPUnRegisterMib (&IssConfigRestoreFileVersionOID,
                       &IssConfigRestoreFileVersionEntry);
    SNMPUnRegisterMib (&IssDefaultRmIfNameOID, &IssDefaultRmIfNameEntry);
    SNMPUnRegisterMib (&IssDefaultVlanIdOID, &IssDefaultVlanIdEntry);
    SNMPUnRegisterMib (&IssNpapiModeOID, &IssNpapiModeEntry);
    SNMPUnRegisterMib (&IssConfigAutoSaveTriggerOID,
                       &IssConfigAutoSaveTriggerEntry);
    SNMPUnRegisterMib (&IssConfigIncrSaveFlagOID, &IssConfigIncrSaveFlagEntry);
    SNMPUnRegisterMib (&IssConfigRollbackFlagOID, &IssConfigRollbackFlagEntry);
    SNMPUnRegisterMib (&IssConfigSyncUpOperationOID,
                       &IssConfigSyncUpOperationEntry);
    SNMPUnRegisterMib (&IssFrontPanelPortCountOID,
                       &IssFrontPanelPortCountEntry);
    SNMPUnRegisterMib (&IssAuditLogStatusOID, &IssAuditLogStatusEntry);
    SNMPUnRegisterMib (&IssAuditLogFileNameOID, &IssAuditLogFileNameEntry);
    SNMPUnRegisterMib (&IssAuditLogFileSizeOID, &IssAuditLogFileSizeEntry);
    SNMPUnRegisterMib (&IssAuditLogResetOID, &IssAuditLogResetEntry);
    SNMPUnRegisterMib (&IssAuditLogRemoteIpAddrOID,
                       &IssAuditLogRemoteIpAddrEntry);
    SNMPUnRegisterMib (&IssAuditLogInitiateTransferOID,
                       &IssAuditLogInitiateTransferEntry);
    SNMPUnRegisterMib (&IssAuditTransferFileNameOID,
                       &IssAuditTransferFileNameEntry);
    SNMPUnRegisterMib (&IssDownLoadTransferModeOID,
                       &IssDownLoadTransferModeEntry);
    SNMPUnRegisterMib (&IssDownLoadUserNameOID, &IssDownLoadUserNameEntry);
    SNMPUnRegisterMib (&IssDownLoadPasswordOID, &IssDownLoadPasswordEntry);
    SNMPUnRegisterMib (&IssUploadLogTransferModeOID,
                       &IssUploadLogTransferModeEntry);
    SNMPUnRegisterMib (&IssUploadLogUserNameOID, &IssUploadLogUserNameEntry);
    SNMPUnRegisterMib (&IssUploadLogPasswdOID, &IssUploadLogPasswdEntry);
    SNMPUnRegisterMib (&IssConfigSaveTransferModeOID,
                       &IssConfigSaveTransferModeEntry);
    SNMPUnRegisterMib (&IssConfigSaveUserNameOID, &IssConfigSaveUserNameEntry);
    SNMPUnRegisterMib (&IssConfigSavePasswordOID, &IssConfigSavePasswordEntry);
    SNMPUnRegisterMib (&IssSwitchMinThresholdTemperatureOID,
                       &IssSwitchMinThresholdTemperatureEntry);
    SNMPUnRegisterMib (&IssSwitchMaxThresholdTemperatureOID,
                       &IssSwitchMaxThresholdTemperatureEntry);
    SNMPUnRegisterMib (&IssSwitchCurrentTemperatureOID,
                       &IssSwitchCurrentTemperatureEntry);
    SNMPUnRegisterMib (&IssSwitchMaxCPUThresholdOID,
                       &IssSwitchMaxCPUThresholdEntry);
    SNMPUnRegisterMib (&IssSwitchCurrentCPUThresholdOID,
                       &IssSwitchCurrentCPUThresholdEntry);
    SNMPUnRegisterMib (&IssSwitchPowerSurgeOID, &IssSwitchPowerSurgeEntry);
    SNMPUnRegisterMib (&IssSwitchPowerFailureOID, &IssSwitchPowerFailureEntry);
    SNMPUnRegisterMib (&IssSwitchCurrentPowerSupplyOID,
                       &IssSwitchCurrentPowerSupplyEntry);
    SNMPUnRegisterMib (&IssSwitchMaxRAMUsageOID, &IssSwitchMaxRAMUsageEntry);
    SNMPUnRegisterMib (&IssSwitchCurrentRAMUsageOID,
                       &IssSwitchCurrentRAMUsageEntry);
    SNMPUnRegisterMib (&IssSwitchMaxFlashUsageOID,
                       &IssSwitchMaxFlashUsageEntry);
    SNMPUnRegisterMib (&IssSwitchCurrentFlashUsageOID,
                       &IssSwitchCurrentFlashUsageEntry);
    SNMPUnRegisterMib (&IssConfigRestoreFileFormatVersionOID,
                       &IssConfigRestoreFileFormatVersionEntry);
    SNMPUnRegisterMib (&IssDebugOptionOID, &IssDebugOptionEntry);
    SNMPUnRegisterMib (&IssConfigDefaultValueSaveOptionOID,
                       &IssConfigDefaultValueSaveOptionEntry);
    SNMPUnRegisterMib (&IssConfigSaveIpAddrTypeOID,
                       &IssConfigSaveIpAddrTypeEntry);
    SNMPUnRegisterMib (&IssConfigSaveIpvxAddrOID, &IssConfigSaveIpvxAddrEntry);
    SNMPUnRegisterMib (&IssConfigRestoreIpAddrTypeOID,
                       &IssConfigRestoreIpAddrTypeEntry);
    SNMPUnRegisterMib (&IssConfigRestoreIpvxAddrOID,
                       &IssConfigRestoreIpvxAddrEntry);
    SNMPUnRegisterMib (&IssDlImageFromIpAddrTypeOID,
                       &IssDlImageFromIpAddrTypeEntry);
    SNMPUnRegisterMib (&IssDlImageFromIpvxOID, &IssDlImageFromIpvxEntry);
    SNMPUnRegisterMib (&IssUploadLogFileToIpAddrTypeOID,
                       &IssUploadLogFileToIpAddrTypeEntry);
    SNMPUnRegisterMib (&IssUploadLogFileToIpvxOID,
                       &IssUploadLogFileToIpvxEntry);
    SNMPUnRegisterMib (&IssAuditLogRemoteIpAddrTypeOID,
                       &IssAuditLogRemoteIpAddrTypeEntry);
    SNMPUnRegisterMib (&IssAuditLogRemoteIpvxAddrOID,
                       &IssAuditLogRemoteIpvxAddrEntry);
    SNMPUnRegisterMib (&IssSystemTimerSpeedOID, &IssSystemTimerSpeedEntry);
    SNMPUnRegisterMib (&IssMgmtInterfaceRoutingOID,
                       &IssMgmtInterfaceRoutingEntry);
    SNMPUnRegisterMib (&IssMacLearnRateLimitOID, &IssMacLearnRateLimitEntry);
    SNMPUnRegisterMib (&IssMacLearnRateLimitIntervalOID,
                       &IssMacLearnRateLimitIntervalEntry);
    SNMPUnRegisterMib (&IssVrfUnqMacFlagOID, &IssVrfUnqMacFlagEntry);
    SNMPUnRegisterMib (&IssLoginAttemptsOID, &IssLoginAttemptsEntry);
    SNMPUnRegisterMib (&IssLoginLockTimeOID, &IssLoginLockTimeEntry);
    SNMPUnRegisterMib (&IssAuditLogSizeThresholdOID,
                       &IssAuditLogSizeThresholdEntry);
    SNMPUnRegisterMib (&IssTelnetStatusOID, &IssTelnetStatusEntry);
    SNMPUnRegisterMib (&IssClearConfigOID, &IssClearConfigEntry);
    SNMPUnRegisterMib (&IssClearConfigFileNameOID,
                       &IssClearConfigFileNameEntry);
    SNMPUnRegisterMib (&IssTelnetClientStatusOID, &IssTelnetClientStatusEntry);
    SNMPUnRegisterMib (&IssSshClientStatusOID, &IssSshClientStatusEntry);
    SNMPUnRegisterMib (&IssActiveTelnetClientSessionsOID,
                       &IssActiveTelnetClientSessionsEntry);
    SNMPUnRegisterMib (&IssActiveSshClientSessionsOID,
                       &IssActiveSshClientSessionsEntry);
    SNMPUnRegisterMib (&IssLogFileSizeOID, &IssLogFileSizeEntry);
    SNMPUnRegisterMib (&IssLogResetOID, &IssLogResetEntry);
    SNMPUnRegisterMib (&IssLogSizeThresholdOID, &IssLogSizeThresholdEntry);
    SNMPUnRegisterMib (&IssUlRemoteLogFileNameOID,
                       &IssUlRemoteLogFileNameEntry);
    SNMPUnRegisterMib (&IssMirrorStatusOID, &IssMirrorStatusEntry);
    SNMPUnRegisterMib (&IssMirrorToPortOID, &IssMirrorToPortEntry);
    SNMPUnRegisterMib (&IssMirrorCtrlRemainingSrcRcrdsOID,
                       &IssMirrorCtrlRemainingSrcRcrdsEntry);
    SNMPUnRegisterMib (&IssMirrorCtrlRemainingDestRcrdsOID,
                       &IssMirrorCtrlRemainingDestRcrdsEntry);
    SNMPUnRegisterMib (&IssAuditTrapFileNameOID, &IssAuditTrapFileNameEntry);
    SNMPUnRegisterMib (&IssAclProvisionModeOID, &IssAclProvisionModeEntry);
    SNMPUnRegisterMib (&IssAclTriggerCommitOID, &IssAclTriggerCommitEntry);
    SNMPUnRegisterMib (&IssAclTrafficSeperationCtrlOID,
                       &IssAclTrafficSeperationCtrlEntry);

    SNMPUnRegisterMib (&IssAclTrafficSeperationCtrlOID,
                       &IssAclTrafficSeperationCtrlEntry);
    SNMPUnRegisterMib (&IssHealthChkStatusOID, &IssHealthChkStatusEntry);
    SNMPUnRegisterMib (&IssHealthChkErrorReasonOID,
                       &IssHealthChkErrorReasonEntry);
    SNMPUnRegisterMib (&IssHealthChkMemAllocErrPoolIdOID,
                       &IssHealthChkMemAllocErrPoolIdEntry);
    SNMPUnRegisterMib (&IssHealthChkConfigRestoreStatusOID,
                       &IssHealthChkConfigRestoreStatusEntry);
    SNMPUnRegisterMib (&IssHealthChkClearCtrOID, &IssHealthChkClearCtrEntry);

    SNMPDelSysorEntry (&fsissOID, (const UINT1 *) "fsiss");
}

INT4
IssHealthChkStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssHealthChkStatus (&(pMultiData->i4_SLongValue)));
}

INT4
IssHealthChkErrorReasonGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssHealthChkErrorReason (&(pMultiData->i4_SLongValue)));
}

INT4
IssHealthChkMemAllocErrPoolIdGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssHealthChkMemAllocErrPoolId (&(pMultiData->i4_SLongValue)));
}

INT4
IssHealthChkConfigRestoreStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssHealthChkConfigRestoreStatus
            (&(pMultiData->i4_SLongValue)));
}

INT4
IssHealthChkClearCtrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetIssHealthChkClearCtr (pMultiData->pOctetStrValue));
}

INT4
IssHealthChkClearCtrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetIssHealthChkClearCtr (pMultiData->pOctetStrValue));
}

INT4
IssHealthChkClearCtrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2IssHealthChkClearCtr
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
IssHealthChkClearCtrDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2IssHealthChkClearCtr
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
