#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 25/12/2002                                    |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            including for building the FutureISS          |
# |                            product.                                      |
# |                                                                          |
# +--------------------------------------------------------------------------+
#
#     CHANGE RECORD :
# +--------------------------------------------------------------------------+
# | VERSION | AUTHOR/    | DESCRIPTION OF CHANGE                             |
# |         | DATE       |                                                   |
# +---------|------------|---------------------------------------------------+
# |   1     | ISS        | Creation of makefile                              |
# |         | 25/12/2002 |                                                   |
# +--------------------------------------------------------------------------+


# Set the PROJ_BASE_DIR as the directory where you untar the project files
PROJECT_NAME		      = FutureEoid
PROJECT_BASE_DIR	      = ${ISS_COMMON_DIR}/customer/eoid
PROJECT_SOURCE_DIR	   = ${PROJECT_BASE_DIR}/src
PROJECT_INCLUDE_DIR	   = ${PROJECT_BASE_DIR}/inc
PROJECT_OBJECT_DIR	   = ${PROJECT_BASE_DIR}/obj


# Specify the project include directories and dependencies
PROJECT_INCLUDE_FILES  = $(PROJECT_INCLUDE_DIR)/eoid.h

PROJECT_FINAL_INCLUDES_DIRS	= -I$(PROJECT_INCLUDE_DIR)

PROJECT_FINAL_INCLUDE_FILES	= $(PROJECT_INCLUDE_FILES)

PROJECT_DEPENDENCIES	= $(COMMON_DEPENDENCIES) \
				$(PROJECT_FINAL_INCLUDE_FILES) \
				$(PROJECT_BASE_DIR)/Makefile \
				$(PROJECT_BASE_DIR)/make.h


