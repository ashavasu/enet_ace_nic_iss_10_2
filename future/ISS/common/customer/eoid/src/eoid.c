
/* SOURCE FILE  :
 *
 *  ---------------------------------------------------------------------------
 * |  Copyright (C) 2006 Aricent Inc . All Rights Reserved                                 |
 * |  Copyright (C) 2006 Aricent Inc . All Rights Reserved                       |
 * |                                                                           |
 * |  FILE NAME                   :  eoid.c                                    |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR            :  Aricent Inc.                       |
 * |                                                                           |
 * |  SUBSYSTEM NAME              :  ISS                                       |
 * |                                                                           |
 * |  MODULE NAME                 :  ISS                                       |
 * |                                                                           |
 * |  LANGUAGE                    :  C                                         |
 * |                                                                           |
 * |  TARGET ENVIRONMENT          :  LINUX                                     |
 * |                                                                           |
 * |  AUTHOR                      :  ISS Team                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION                 :  Enterprise OID related routines           | 
 * |                                                                           |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 *
 */

#include "eoid.h"

/*****************************************************************************/
/* Function Name      : EoidGetEnterpriseOid                                 */
/*                                                                           */
/* Description        : This function retrieves the Enterprise OID for the   */
/*                       first subbranch i.e. equivalent for 2076            */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Return Value(s)    : The Enterprise oid1                                  */
/*****************************************************************************/
unsigned char      *
EoidGetEnterpriseOid ()
{
    return ((unsigned char *) ENTERPRISE_OID_1);
}

/*****************************************************************************/
/* Function Name      : EoidGetSecondEnterpriseOid                           */
/*                                                                           */
/* Description        : This function retrieves the Enterprise OID for the   */
/*                       second subbranch i.e. equivalent for 29601          */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/* Return Value(s)    : The Enterprise oid2                                  */
/*****************************************************************************/
unsigned char      *
EoidGetSecondEnterpriseOid ()
{
    return ((unsigned char *) ENTERPRISE_OID_2);
}
