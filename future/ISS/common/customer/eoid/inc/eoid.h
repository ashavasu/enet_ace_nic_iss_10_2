/* INCLUDE FILE  :
 *
 *  ---------------------------------------------------------------------------
 * |  Copyright (C) 2006 Aricent Inc . All Rights Reserved                                 |
 * |  Copyright (C) 2006 Aricent Inc . All Rights Reserved                       |
 * |                                                                           |
 * |  FILE NAME                   :  eoid.h                                    |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR            :  Aricent Inc.                       |
 * |                                                                           |
 * |  SUBSYSTEM NAME              :  ISS                                       |
 * |                                                                           |
 * |  MODULE NAME                 :  ISS                                       |
 * |                                                                           |
 * |  LANGUAGE                    :  C                                         |
 * |                                                                           |
 * |  TARGET ENVIRONMENT          :  LINUX                                     |
 * |                                                                           |
 * |  AUTHOR                      :  ISS Team                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION                 :  Enterprise OID related prototypes/Macros  | 
 * |                                                                           |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 */

#ifndef _EOID_H 
#define _EOID_H 

#define ENTERPRISE_OID_1       "2076"
#define ENTERPRISE_OID_2       "29601"

unsigned char * EoidGetEnterpriseOid (void);
unsigned char * EoidGetSecondEnterpriseOid (void);

#endif /* _EOID_H  */
