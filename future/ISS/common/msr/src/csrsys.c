/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                         */
/* $Id: csrsys.c,v 1.19 2016/05/11 11:40:57 siva Exp $*/
/*****************************************************************************/
/*    FILE  NAME            : csrsys.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           : CSR                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*    DESCRIPTION           : This file has functions for configuration save */
/*                            and restore through CLI.                       */
/*---------------------------------------------------------------------------*/

#include "csrsys.h"

extern tMsrSavResOption gMsrSavResPriority;
/****************************************************************************/
/*                                                                          */
/*    Function Name      : CsrMemAllocation                                 */
/*    Description        : This function is invoked to Allocate memory for  */
/*                         CSR StrFlash Array.                              */
/*                                                                          */
/*    Input(s)           : u1ProtoId - Protocol Id                          */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : CSR_SUCCESS  if the allocation operation is successful*/
/****************************************************************************/

INT4
CsrMemAllocation (UINT1 u1ProtoId)
{
    if (u1ProtoId == 0 || u1ProtoId > MAX_PROTOCOLS)
    {
        return CSR_FAILURE;
    }
    /* Allocate memory pool for the save / restore array */
    if (CSR_CREATE_MEM_POOL (CSR_MEMBLK_SIZE, CSR_MEMBLK_COUNT,
                             &(CSR_POOL_ID (u1ProtoId))) != MEM_SUCCESS)
    {
        return CSR_FAILURE;
    }

    if ((gpu1Flash[u1ProtoId - 1] = CSR_ALLOC_MEM_BLOCK (u1ProtoId)) == NULL)
    {
        return CSR_FAILURE;
    }

    /*Initialise the flash array */
    CSR_MEMSET (gpu1Flash[u1ProtoId - 1], 0, CSR_MEMBLK_SIZE);

    return CSR_SUCCESS;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : CsrMemDeAllocation                               */
/*    Description        : This function is invoked to Deallcoate           */
/*                         memory for CSR StrFlash Array.                   */
/*                                                                          */
/*    Input(s)           : u1ProtoId - Protocol Id                          */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : CSR_SUCCESS if deallcoation operation is successful*/
/****************************************************************************/

INT4
CsrMemDeAllocation (UINT1 u1ProtoId)
{
    if (u1ProtoId > MAX_PROTOCOLS || u1ProtoId == 0)
    {
        return CSR_FAILURE;
    }
    if (gpu1Flash[u1ProtoId - 1] != NULL)
    {
        CSR_FREE_MEM_BLOCK (u1ProtoId, gpu1Flash[u1ProtoId - 1]);
    }
    CSR_DELETE_MEM_POOL (CSR_POOL_ID (u1ProtoId));

    return CSR_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssCsrSaveCli                                    */
/*    Description        : This function is invoked to save the current     */
/*                         configuration in the cli format to the           */
/*                         StrFlash Array.                                  */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : CSR_TRUE if the save operation is successful     */
/****************************************************************************/

INT4
IssCsrSaveCli (UINT1 *pu1TaskName)
{
    tCliHandle          gCsrCliHandle = CSR_APPLICATION_HANDLER;
    UINT1               au1StoreFile[ISS_CONFIG_FILE_NAME_LEN];
    UINT1               u1ProtoId = 0;
#if defined(OSPF_WANTED) || defined (OSPF3_WANTED)
    UINT4               u4OspfCxtId;

    UINT4               u4OspfPrevCxtId;
#endif
#ifdef ISIS_WANTED
    UINT4               u4ContextId;
    UINT4               u4TempContextId;
#endif

    MEMSET (au1StoreFile, 0, sizeof (au1StoreFile));
    gu4Len = 0;

    if (STRCMP (pu1TaskName, "OSPF") == 0)
    {
        u1ProtoId = OSPF_MODULE_ID;
        STRCPY (au1StoreFile, CSR_CONFIG_FILE);

        /*Initialization of Flash array */
        CSR_MEMSET (gpu1Flash[OSPF_MODULE_ID - 1], 0, CSR_MEMBLK_SIZE);

        gpu1Current = gpu1Flash[OSPF_MODULE_ID - 1];

#ifdef OSPF_WANTED
        /* Get the first context */

        if (UtilOspfGetFirstCxtId (&u4OspfCxtId) == CSR_OSPF_FAILURE)
        {
            return CSR_FAILURE;
        }

        /*Walk through over all, existing Ospf context */
        do
        {
            OspfShowRunningConfigInCxt (gCsrCliHandle,
                                        ISS_OSPF_SHOW_RUNNING_CONFIG,
                                        u4OspfCxtId);
            u4OspfPrevCxtId = u4OspfCxtId;
        }
        while (UtilOspfGetNextCxtId (u4OspfPrevCxtId, &u4OspfCxtId)
               != CSR_OSPF_FAILURE);

#endif
    }
#ifdef OSPF3_WANTED
    else if (STRCMP (pu1TaskName, "OSV3") == 0)
    {
        u1ProtoId = OSPF3_MODULE_ID;
        /*Initialization of Flash array */
        CSR_MEMSET (gpu1Flash[OSPF3_MODULE_ID - 1], 0, CSR_MEMBLK_SIZE);

        gpu1Current = gpu1Flash[OSPF3_MODULE_ID - 1];

        STRCPY (au1StoreFile, OSPF3_CSR_CONFIG_FILE);

        /* Get the first context */
        if (V3UtilOspfGetFirstCxtId (&u4OspfCxtId) == CSR_FAILURE)
        {
            return CSR_FAILURE;
        }

        /*Walk through over all, existing Ospf context */
        do
        {
            Ospfv3ShowRunningConfigInCxt (gCsrCliHandle,
                                          ISS_OSPF3_SHOW_RUNNING_CONFIG,
                                          u4OspfCxtId);
            u4OspfPrevCxtId = u4OspfCxtId;
        }
        while (V3UtilOspfGetNextCxtId (u4OspfPrevCxtId, &u4OspfCxtId)
               != CSR_FAILURE);
    }
#endif
#ifdef BGP_WANTED
    else if (STRCMP (pu1TaskName, "BGP4") == 0)
    {
        u1ProtoId = BGP_MODULE_ID;

        /*Initialization of Flash array */
        CSR_MEMSET (gpu1Flash[u1ProtoId - 1], 0, CSR_MEMBLK_SIZE);
        gpu1Current = gpu1Flash[u1ProtoId - 1];
        STRCPY (au1StoreFile, BGP4_CSR_CONFIG_FILE);

        /*Walk through over all, existing BGP configurations */
        BgpShowRunningConfig (gCsrCliHandle, NULL);
    }
#endif /* BGP_WANTED */
#ifdef ISIS_WANTED
    else if (STRCMP (pu1TaskName, "ISIS") == 0)
    {
        u1ProtoId = ISIS_MODULE_ID;
        CSR_MEMSET (gpu1Flash[u1ProtoId - 1], 0, CSR_MEMBLK_SIZE);
        gpu1Current = gpu1Flash[u1ProtoId - 1];
        STRCPY (au1StoreFile, ISIS_CSR_FILE);

        /* Get the first context */
        if (UtilIsisGetFirstCxtId (&u4ContextId) == CSR_ISIS_FAILURE)
        {
            return CSR_FAILURE;
        }
        do
        {
            IsIsShowRunningConfigInCxt (gCsrCliHandle,
                                        ISS_ISIS_SHOW_RUNNING_CONFIG,
                                        u4ContextId);
            u4TempContextId = u4ContextId;
        }
        while (UtilIsisGetNextCxtId (u4TempContextId, &u4ContextId)
               != CSR_ISIS_FAILURE);
    }
#endif
#ifdef MPLS_WANTED
    else if (STRCMP (pu1TaskName, "RSVPTE") == 0)
    {
        u1ProtoId = RSVPTE_MODULE_ID;

        /*Initialization of Flash array */
        CSR_MEMSET (gpu1Flash[u1ProtoId - 1], 0, CSR_MEMBLK_SIZE);
        gpu1Current = gpu1Flash[u1ProtoId - 1];
        STRCPY (au1StoreFile, RSVPTE_CSR_FILE);

        /*Walk through over all, existing RSVP-TE configurations */
        RpteCliRsvpTeShowRunnningConfig (gCsrCliHandle);
        RpteFrrFacShowRunningConfig (gCsrCliHandle);
    }
    else if (STRCMP (pu1TaskName, "LDP") == 0)
    {
        u1ProtoId = LDP_MODULE_ID;

        /*Initialization of Flash array */
        CSR_MEMSET (gpu1Flash[u1ProtoId - 1], 0, CSR_MEMBLK_SIZE);
        gpu1Current = gpu1Flash[u1ProtoId - 1];
        STRCPY (au1StoreFile, LDP_CSR_FILE);

        /*Walk through over all, existing LDP configurations */
        LdpCliShowRunningConfig (gCsrCliHandle);
    }
#endif /* MPLS_WANTED */

    else
    {
        return CSR_FAILURE;
    }
    FilePrintf (gCsrCliHandle, "\r\nend\r\n");    /*Just a delimiter */

    /*Writing to a file from flash array */
    if (FlashWrite (gpu1Flash[u1ProtoId - 1], gu4Len,
                    FLASH_ALL_CONFIG, (const char *) au1StoreFile) != 1)
    {
        return CSR_FAILURE;
    }

    /*Initialising flash array */
    CSR_MEMSET (gpu1Flash[u1ProtoId - 1], 0, CSR_MEMBLK_SIZE);

    return CSR_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssCsrCliRestore                                 */
/*                                                                          */
/*    Description        : This function is used to restore the             */
/*                         configuration saved in the gpu1Flash.         */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
INT4
IssCsrCliRestore (UINT1 *pu1TaskName)
{

    UINT1               au1StoreFile[ISS_CONFIG_FILE_NAME_LEN];

    MEMSET (au1StoreFile, 0, sizeof (au1StoreFile));

    if (STRCMP (pu1TaskName, "OSPF") == 0)
    {
        STRCPY (au1StoreFile, CSR_CONFIG_FILE);
    }
    if (STRCMP (pu1TaskName, "OSV3") == 0)
    {
        STRCPY (au1StoreFile, OSPF3_CSR_CONFIG_FILE);
    }
    if (STRCMP (pu1TaskName, "BGP4") == 0)
    {
        STRCPY (au1StoreFile, BGP4_CSR_CONFIG_FILE);
    }
    if (STRCMP (pu1TaskName, "ISIS") == 0)
    {
        STRCPY (au1StoreFile, ISIS_CSR_FILE);
    }
    if (STRCMP (pu1TaskName, "RSVPTE") == 0)
    {
        STRCPY (au1StoreFile, RSVPTE_CSR_FILE);
    }
    if (STRCMP (pu1TaskName, "LDP") == 0)
    {
        STRCPY (au1StoreFile, LDP_CSR_FILE);
    }

    /*Restoring the config file */
    if (FlashFileExists ((CONST CHR1 *) au1StoreFile) == ISS_SUCCESS)
    {
        CliRestoreConfig (au1StoreFile);
        return CSR_SUCCESS;
    }
    else
    {
        return CSR_FAILURE;
    }

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FilePrintf                                       */
/*                                                                          */
/*    Description        : This function is used to write to flash array    */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

INT4
FilePrintf (tCliHandle CliHandle, CONST CHR1 * fmt, ...)
{

    if ((CliHandle == 1) && (gpu1Current != NULL))
    {
        va_list             ap;

        va_start (ap, fmt);

        VSNPRINTF ((CHR1 *) gpu1Current, CSR_MEMBLK_SIZE - gu4Len, fmt, ap);    /*Formatting */

        gu4Len = gu4Len + CLI_STRLEN (gpu1Current);
        gpu1Current = (gpu1Current + (CLI_STRLEN (gpu1Current)));
        va_end (ap);
    }
    return CSR_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : CSRCliGetShowCmdOutputToFile                         */
/*                                                                           */
/* Description        : This function executes the 'show running-config'     */
/*                      command and the output is redirected to the file     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1FileName - The output of the show cmd is          */
/*                      redirected to this file                              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MSR_SUCCESS/MSR_FAILURE                              */
/*****************************************************************************/
INT4
CSRCliGetShowCmdOutputToFile (UINT1 *pu1FileName)
{
    if (FileStat ((const CHR1 *) pu1FileName) == OSIX_SUCCESS)
    {
        if (0 != FileDelete (pu1FileName))
        {
            return CSR_FAILURE;
        }
    }
    if (CliGetShowCmdOutputToFile (pu1FileName, (UINT1 *) CLI_SAVE_RESTORE_SHOW_CMD)
            == CLI_FAILURE)
    {
        MSR_TRC (MSR_TRACE_TYPE, "CLI based saving is failed \n");
        return CSR_FAILURE;
    }
    return CSR_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : CSRCliExecuteCmdFromFile                             */
/*                                                                           */
/* Description        : This function executes the list of commands which    */
/*                      are available in the restoration file                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : MSR_SUCCESS/MSR_FAILURE                              */
/*****************************************************************************/
INT4 
CSRCliExecuteCmdFromFile (VOID)
{
    CHR1                au1RestoreFileName[MSR_MAX_FLASH_FILE_LEN];
    UINT1              *pTemp = NULL;

    MEMSET (au1RestoreFileName, 0, sizeof (au1RestoreFileName));

    pTemp = (UINT1 *) IssGetRestoreFileNameFromNvRam ();
    if (gMsrSavResPriority == EXTERN_STORAGE)
    {
        MSR_STRCPY (au1RestoreFileName, MSR_SYS_EXTN_STRG_PATH);
    }
    else
    {
        MSR_STRCPY (au1RestoreFileName, FLASH);
    }
    MSR_STRNCAT (au1RestoreFileName, pTemp,
            (sizeof (au1RestoreFileName) -
             STRLEN (au1RestoreFileName) - 1));
    if (CliExecuteCmdFromFile ((UINT1 *) au1RestoreFileName) == CLI_FAILURE)
    {
        MSR_TRC (MSR_TRACE_TYPE, "CLI based restoration is failed \n");
        return CSR_FAILURE;
    }
    return CSR_SUCCESS;
}
