/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/* $Id: simflash.c,v 1.21                                                    */
/*****************************************************************************/
/*    FILE  NAME            : simflash.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*    DESCRIPTION           : This file has flash related functions          */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "msrsys.h"
#include "msr.h"

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FlashWrite                                       */
/*                                                                          */
/*    Description        : This function is invoked to save the             */
/*                         configuration/Image to Flash                     */
/*                                                                          */
/*    Input(s)           : u4FlashDataType : FLASH_ALL_CONFIG/              */
/*                                           FLASH_STARTUP_CONFIG/          */
/*                                           FLASH_FIRMWARE                 */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ISS_SUCCESS if saving to flash is successful     */
/****************************************************************************/
INT4
FlashWrite (UINT1 *pu1FlashData,
            UINT4 u4Size, UINT4 u4FlashDataType, CONST CHR1 * pu1FlashFileName)
{
    INT4                i4FileFd = 0;
    CHR1                au1RestoreFileName[MSR_MAX_FLASH_FILE_LEN];
    INT4                i4RetVal = 0;

    UNUSED_PARAM (u4FlashDataType);
    MSR_MEMSET (au1RestoreFileName, 0, sizeof (au1RestoreFileName));

    MSR_STRCPY (au1RestoreFileName, FLASH);
    MSR_STRNCAT (au1RestoreFileName, pu1FlashFileName,
                 (sizeof (au1RestoreFileName) -
                  STRLEN (au1RestoreFileName) - 1));

    if ((i4FileFd = FileOpen ((UINT1 *) au1RestoreFileName,
                              (OSIX_FILE_WO | OSIX_FILE_TR |
                               OSIX_FILE_CR))) < 0)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Fileopen Failed ... \n");
        return ISS_FAILURE;
    }

    i4RetVal = (INT4) FileWrite (i4FileFd, (CHR1 *) pu1FlashData, u4Size);
    if (i4RetVal < 0)
    {
        FileClose (i4FileFd);
        return ISS_FAILURE;
    }

    FileClose (i4FileFd);

#if defined(SWC)
    system ("etc flush");
#endif

    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FlashRead                                        */
/*                                                                          */
/*    Description        : This function is invoked to read the             */
/*                         configuration from Flash.                        */
/*                                                                          */
/*    Input(s)           : u4FlashDataType :  FLASH_ALL_CONFIG/             */
/*                                            FLASH_STARTUP_CONFIG          */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ISS_SUCCESS if reading from flash is successful  */
/*                                                                          */
/*    Note               : please take care of not disturbing the           */
/*                         conversion of \t and \n to \0.                   */
/****************************************************************************/
INT4
FlashRead (UINT1 **ppu1FlashData, UINT4 *u4Size, UINT4 u4FlashDataType)
{

    FILE               *Restorefp = NULL;
    UINT1              *pTemp = NULL;
    int                 u1Char;
    UINT4               u4Len = 0;
    CHR1                au1RestoreFileName[MSR_MAX_FLASH_FILE_LEN];

    UNUSED_PARAM (u4FlashDataType);

    pTemp = (UINT1 *) IssGetRestoreFileNameFromNvRam ();

    if (pTemp == NULL)
    {
        return ISS_FAILURE;
    }

    MSR_MEMSET (au1RestoreFileName, 0, sizeof (au1RestoreFileName));
    MSR_STRCPY (au1RestoreFileName, FLASH);
    MSR_STRNCAT (au1RestoreFileName, pTemp,
                 (sizeof (au1RestoreFileName) -
                  STRLEN (au1RestoreFileName) - 1));

    if ((Restorefp = FOPEN ((CONST CHR1 *) au1RestoreFileName, "r")) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "No Configuration in Flash ... \n");
        return ISS_FAILURE;
    }

    if (*ppu1FlashData == NULL)
    {
        fclose (Restorefp);
        return ISS_FAILURE;
    }

    MSR_MEMSET (*ppu1FlashData, 0, MSR_MIBENTRY_MEMBLK_SIZE);

    pTemp = *ppu1FlashData;
    while ((u1Char = fgetc (Restorefp)) != EOF)
    {
        *pTemp = (UINT1) u1Char;
        ++pTemp;
        u4Len++;
    }

    *u4Size = u4Len;
    fclose (Restorefp);
    if (u4Len == 0)
    {
        return ISS_FAILURE;
    }
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FlashFileExists                                  */
/*                                                                          */
/*    Description        : This function is invoked to check if given file  */
/*                         exists on the flash                              */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ISS_SUCCESS if reading from flash is successful  */
/*                                                                          */
/****************************************************************************/
INT4
FlashFileExists (CONST CHR1 * FileName)
{
    if (FileName != NULL)
    {
        if (FileStat (FileName) == OSIX_SUCCESS)
        {
            return ISS_SUCCESS;
        }
    }

    return ISS_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FlashRemoveFile                                  */
/*                                                                          */
/*    Description        : This function is invoked to delete a file to     */
/*                         on the flash                                     */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ISS_SUCCESS if copying is successful             */
/*                                                                          */
/****************************************************************************/
INT4
FlashRemoveFile (const char *FileName)
{
    if (FileName != NULL)
    {
        unlink (FileName);
    }
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FlashReadCksum                                   */
/*                                                                          */
/*    Description        : This function is invoked to read the checksum    */
/*                         from Flash.                                      */
/*                                                                          */
/*    Output(s)          : Flash checksum stored in iss conf file.          */
/*                                                                          */
/*    Returns            : ISS_SUCCESS if reading from flash is successful  */
/*                                                                          */
/****************************************************************************/
INT4
FlashReadCksum (UINT2 *u2Checksum)
{
#define FLASH_CKSUM_STR_LEN 8
    INT4                i4FileId = 0;
    UINT1               au1Data[FLASH_CKSUM_STR_LEN];
    UINT1               u1Counter = 0;
    UINT1               u1CkSumLen = 0;
    CHR1                au1RestoreFileName[MSR_MAX_FLASH_FILE_LEN];
    UINT1              *pNvramFileName = NULL;

    pNvramFileName = (UINT1 *) IssGetRestoreFileNameFromNvRam ();

    MSR_MEMSET (au1RestoreFileName, 0, sizeof (au1RestoreFileName));

    MSR_STRCPY (au1RestoreFileName, FLASH);
    MSR_STRNCAT (au1RestoreFileName, pNvramFileName,
                 (sizeof (au1RestoreFileName) -
                  STRLEN (au1RestoreFileName) - 1));

    if ((i4FileId = FileOpen ((UINT1 *) au1RestoreFileName, OSIX_FILE_RO)) < 0)
    {
        MSR_TRC (MSR_TRACE_TYPE, "No Configuration in Flash ... \n");
        return ISS_FAILURE;
    }

    /* If '0' is padded to the iss.conf file for module 2
     * length, the 4th byte from the last contains ':'
     * followed by 2 bytes of checksum and 1 byte padding.
     * Else, the 3rd byte from the last contains ':'
     * followed by 2 bytes of checksum
     */
    if (lseek (i4FileId, FLASH_CKSUM_OFFSET, SEEK_END) < 0)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Flash seek error... \n");
        FileClose (i4FileId);
        return ISS_FAILURE;
    }

    MEMSET (au1Data, 0, FLASH_CKSUM_STR_LEN);
    FileRead (i4FileId, (CHR1 *) au1Data, FLASH_CKSUM_STR_LEN);

    u1CkSumLen = (UINT1) STRLEN ("CKSUM");
    while (au1Data[u1Counter] != ':')
    {
        if (u1Counter > u1CkSumLen)
        {
            FileClose (i4FileId);
            return ISS_FAILURE;
        }
        u1Counter++;
    }
    u1Counter++;
    MEMCPY (&u2Checksum, &au1Data[u1Counter], FLASH_CKSUM_LEN);

    FileClose (i4FileId);
    return ISS_SUCCESS;
}
