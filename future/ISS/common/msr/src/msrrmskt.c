/********************************************************************
 * * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 * *
 * * $Id: msrrmskt.c,v 1.10 2014/07/20 11:07:48 siva Exp $
 * *
 * * Description: MSR-RM socket layer interface APIs.        
 * *******************************************************************/
#ifndef _MSR_RM_SOCKC_
#define _MSR_RM_SOCKC_

#include "lr.h"
#include "msr.h"
#include "selutil.h"
#include "rmgr.h"
#include "msrrm.h"
#include "msrtrc.h"
#include "fssocket.h"

extern UINT4        gu4MsrTrcFlag;

UINT4               gu4TxCount = 0;

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmServerInit                                  */
/*                                                                          */
/*    Description        : Active node                                      */
/*                         This function creates a server socket for RB     */
/*                         Tree sync up messages. This function is called   */
/*                         when GO_ACTIVE event is received from RM         */
/*                                                                          */
/*    Input(s)           : u4Addr   -   Self address                        */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : socket fd on success else failure                */
/****************************************************************************/

INT4
MsrRmServerInit (UINT4 u4Addr)
{
    struct sockaddr_in  serv_addr;
    INT4                i4RetVal = MSR_SUCCESS;
    INT4                i4OptVal = TRUE;
    INT4                i4Sockfd = -1;

    if ((i4Sockfd = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    {
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                 "MsrRmServerInit: Failed to create "
                 "the server socket !!!\r\n");
        perror ("MsrRmServerInit: Unable to open stream socket");
        i4Sockfd = -1;
        i4RetVal = MSR_FAILURE;
    }

    if ((i4RetVal == MSR_SUCCESS) &&
        (setsockopt (i4Sockfd, SOL_SOCKET, SO_REUSEADDR,
                     (INT4 *) &i4OptVal, sizeof (i4OptVal)) < 0))
    {
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                 "MsrRmServerInit: setsockopt failed !!!\r\n");
        perror ("MsrRmServerInit: Unable to set SO_REUSEADDR options "
                "for socket");
        i4RetVal = MSR_FAILURE;
    }

    if (i4RetVal == MSR_SUCCESS)
    {
        MEMSET (&serv_addr, 0, sizeof (serv_addr));
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_addr.s_addr = OSIX_HTONL (u4Addr);
        serv_addr.sin_port = OSIX_HTONS (MSR_SERVER_TCP_PORT);
    }

    if ((i4RetVal == MSR_SUCCESS) &&
        (bind (i4Sockfd, (struct sockaddr *) &serv_addr,
               sizeof (serv_addr)) != 0))
    {
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                 "MsrRmServerInit: Bind failure !!!\r\n");
        perror ("MsrRmServerInit: Unable to bind Address");
        i4RetVal = MSR_FAILURE;
    }

    if (i4RetVal == MSR_SUCCESS)
    {
        listen (i4Sockfd, 1);
        /* Add the fd to the select library */
        SelAddFd (i4Sockfd, MsrRmNewConnReq);
        MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmServerInit: Server socket created "
                 "successfully\r\n");
    }
    else
    {
        if (i4Sockfd != -1)
        {
            close (i4Sockfd);
        }

        i4Sockfd = -1;
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                 "MsrRmServerInit: Server socket creation failed !!!\r\n");
    }

    return i4Sockfd;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmClientInit                                  */
/*                                                                          */
/*    Description        : Standby node                                     */
/*                         This function connects to the server socket      */
/*                         for RB Tree sync up messages. This function      */
/*                         is called when MSR_RM_FILE_TRANSFER_COMPLETE     */
/*                         event is received.                               */
/*                                                                          */
/*    Input(s)           : u4Addr     -    Server node address              */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : Socket fd on success else -1                     */
/****************************************************************************/

INT4
MsrRmClientInit (UINT4 u4Addr)
{
    struct sockaddr_in  serv_addr;
    INT4                i4RetVal = MSR_SUCCESS;
    INT4                i4Sockfd = -1;
    INT4                i4Flags = 0;    /* Flags for socket fd */
    UINT4               u4SndBuf = MSR_MAX_FRAGMENT;
    UINT4               u4RcvBuf = MSR_MAX_FRAGMENT;

    /* Get the active node id from the RM module and connect */

    MEMSET ((char *) &serv_addr, 0, sizeof (serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = OSIX_NTOHL (u4Addr);
    serv_addr.sin_port = OSIX_HTONS (MSR_SERVER_TCP_PORT);

    /* Open a socket connection */
    i4Sockfd = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (i4Sockfd < 0)
    {
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                 "MsrRmClientInit: socket creation " "failed !!!\r\n");
        perror ("MsrRmClientInit: Unable to open stream socket\r\n");
        i4Sockfd = -1;
        i4RetVal = MSR_FAILURE;
    }

    /* Connect to the server using the active node id */
    if ((i4RetVal == MSR_SUCCESS) &&
        (connect (i4Sockfd, (struct sockaddr *) &serv_addr,
                  sizeof (serv_addr)) != 0))
    {
        /* ECONNREFUSED refused occurs when there is no server
         * listening on the remote address. This can occur if
         * 1. The active node goes down before creating the server
         *    socket. In this case, this node becomes the active
         * 2. The connect request is sent before the server socket
         *    is created in the active node. This can occur during
         *    simultaneous boot up. In this case start the timer
         *    and re-connect after sometime
         */
        if (errno != ECONNREFUSED)
        {
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                     "MsrRmClientInit: connect failed !!!\r\n");
            perror ("MsrRmClientInit: Unable to connect");
        }
        i4RetVal = MSR_FAILURE;
    }

    /* Set the sock opt flag as non blocking */
    if (i4RetVal == MSR_SUCCESS)
    {
        if ((i4Flags = fcntl (i4Sockfd, F_GETFL, 0)) < 0)
        {
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                     "MsrRmClientInit: Unable to get socket flags !!!");
            i4RetVal = MSR_FAILURE;
        }
    }

    if (i4RetVal == MSR_SUCCESS)
    {
        /* Set the socket is non-blocking mode */
        i4Flags |= O_NONBLOCK;
        if (fcntl (i4Sockfd, F_SETFL, i4Flags) < 0)
        {
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                     "MsrRmClientInit: Unable to set socket flags !!!");
            i4RetVal = MSR_FAILURE;
        }
    }

    if (i4RetVal == MSR_SUCCESS)
    {
        if (setsockopt (i4Sockfd, SOL_SOCKET, SO_SNDBUF,
                        (VOID *) &u4SndBuf, sizeof (UINT4)) < 0)
        {
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                     "MsrRmClientInit: Unable to set Send Buffer !!!");
            i4RetVal = MSR_FAILURE;
        }
    }

    if (i4RetVal == MSR_SUCCESS)
    {
        if (setsockopt (i4Sockfd, SOL_SOCKET, SO_RCVBUF,
                        (VOID *) &u4RcvBuf, sizeof (UINT4)) < 0)
        {
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                     "MsrRmClientInit: Unable to set Rcv Buffer !!!");
            i4RetVal = MSR_FAILURE;
        }
    }

    if (i4RetVal == MSR_SUCCESS)
    {
        /* Add the fd to the select library to accept incoming packet */
        SelAddFd (i4Sockfd, MsrRmPktArrival);
        MSR_TRC (MSR_RM_TRACE_TYPE,
                 "MsrRmClientInit: Channel creation successful\r\n");
    }
    else
    {
        if (i4Sockfd != -1)
        {
            close (i4Sockfd);
        }

        i4Sockfd = -1;
    }

    return i4Sockfd;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmServerDeInit                                */
/*                                                                          */
/*    Description        : Active/Standby node                              */
/*                         This function deletes the server socket created  */
/*                         for RB Tree sync up messages.                    */
/*                                                                          */
/*                         When GO_ACTIVE event is received, the server     */
/*                         socket  must be deleted before creating a new    */
/*                         server socket.                                   */
/*                                                                          */
/*                         When GO_STANDBY event is received, the server    */
/*                         socket should be deleted if the transition is    */
/*                         from active to standby                           */
/*                                                                          */
/*    Input(s)           : i4Sockfd   - Socket fd                           */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MsrRmServerDeInit (INT4 i4Sockfd)
{
    if (i4Sockfd != -1)
    {
        /* Remove the FD from SELECT library */
        SelRemoveFd (i4Sockfd);

        /* close the socket */
        if (close (i4Sockfd) < 0)
        {
            MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmServerDeInit: "
                     "socket close failed !!!\r\n");
            perror ("MsrRmServerDeInit: socket close failed");
        }
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmChannelDeInit                               */
/*                                                                          */
/*    Description        : Active/Standby node                              */
/*                         This function closes the socket fd used for      */
/*                         for RB Tree sync up messages.                    */
/*                                                                          */
/*                         When GO_ACTIVE event is received, the connection */
/*                         fd is closed and conn id is used for             */
/*                         accepting new connection.                        */
/*                                                                          */
/*                         When GO_STANDBY event is received, the           */
/*                         connection fd is closed and the conn id is used  */
/*                         for connecting to a new channel                  */
/*                                                                          */
/*                         The active and standby node closes the channel   */
/*                         after complete synchronization.                  */
/*                                                                          */
/*    Input(s)           : i4Sockfd   - Socket fd                           */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MsrRmChannelDeInit (INT4 i4Sockfd)
{
    if (i4Sockfd != -1)
    {
        /* Remove the FD from SELECT library */
        SelRemoveFd (i4Sockfd);
        SelRemoveWrFd (i4Sockfd);

        /* close the socket */
        if (close (i4Sockfd) < 0)
        {
            MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmChannelDeInit: "
                     "socket close failed !!!\r\n");
            perror ("MsrRmChannelDeInit: socket close failed");
        }
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmAcceptConnection                            */
/*                                                                          */
/*    Description        : Active node                                      */
/*                         This function accepts the incoming connection    */
/*                         and stores the socket descriptor in the global   */
/*                         info. This function is called when               */
/*                         MSR_RM_NEW_CONNECTION event is received          */
/*                                                                          */
/*    Input(s)           : i4SrvSockFd  -  Server socket fd                 */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : Connection id on success else failure            */
/****************************************************************************/

INT4
MsrRmAcceptConnection (INT4 i4SrvSockFd)
{
    struct sockaddr_in  CliSockAddr;
    UINT4               u4CliLen = sizeof (struct sockaddr_in);
    INT4                i4RetVal = MSR_SUCCESS;
    INT4                i4Sockfd = -1;
    INT4                i4Flags = 0;    /* Flags for socket fd */
    UINT4               u4SndBuf = MSR_MAX_FRAGMENT;
    UINT4               u4RcvBuf = MSR_MAX_FRAGMENT;

    MEMSET (&CliSockAddr, 0, u4CliLen);
    i4Sockfd = accept (i4SrvSockFd, (struct sockaddr *) &CliSockAddr,
                       (socklen_t *) & u4CliLen);

    if (i4Sockfd < 0)
    {
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                 "MsrRmAcceptConnection: accept failed !!!\r\n");
        perror ("MsrRmAcceptConnection: Unable to accept a " "new connection");
        i4RetVal = MSR_FAILURE;
    }

    /* Set the sock opt flag as non blocking */
    if (i4RetVal == MSR_SUCCESS)
    {
        if ((i4Flags = fcntl (i4Sockfd, F_GETFL, 0)) < 0)
        {
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                     "MsrRmAcceptConnection: Unable to get socket flags !!!");
            i4RetVal = MSR_FAILURE;
        }
    }

    if (i4RetVal == MSR_SUCCESS)
    {
        /* Set the socket is non-blocking mode */
        i4Flags |= O_NONBLOCK;
        if (fcntl (i4Sockfd, F_SETFL, i4Flags) < 0)
        {
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                     "MsrRmAcceptConnection: Unable to set socket flags !!!");
            i4RetVal = MSR_FAILURE;
        }
    }

    if (i4RetVal == MSR_SUCCESS)
    {
        if (setsockopt (i4Sockfd, SOL_SOCKET, SO_SNDBUF,
                        (VOID *) &u4SndBuf, sizeof (UINT4)) < 0)
        {
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                     "MsrRmClientInit: Unable to set Send Buffer !!!");
            i4RetVal = MSR_FAILURE;
        }
    }

    if (i4RetVal == MSR_SUCCESS)
    {
        if (setsockopt (i4Sockfd, SOL_SOCKET, SO_RCVBUF,
                        (VOID *) &u4RcvBuf, sizeof (UINT4)) < 0)
        {
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                     "MsrRmClientInit: Unable to set Rcv Buffer !!!");
            i4RetVal = MSR_FAILURE;
        }
    }

    if (i4RetVal == MSR_SUCCESS)
    {
        /* Wait for next connection */
        SelAddFd (i4SrvSockFd, MsrRmNewConnReq);

        /* Add the fd to the select library to accept incoming packet */
        SelAddFd (i4Sockfd, MsrRmPktArrival);
        MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmAcceptConnection: Connection "
                 "successful\r\n");
    }
    else
    {
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                 "MsrRmAcceptConnection: Connection " "failed !!!\r\n");
    }

    return i4Sockfd;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmSendPkt                                     */
/*                                                                          */
/*    Description        : Active and Standby node                          */
/*                         This function sends the packet to the peer       */
/*                         node                                             */
/*                                                                          */
/*    Input(s)           : i4Sockfd      -    Socket fd                     */
/*                         pu1Buf        -    pointer to the linear buffer  */
/*                         u2Len         -    Length of the packet          */
/*                                                                          */
/*    Output(s)          : pi4WriteBytes -    Bytes written                 */
/*                                                                          */
/*    Returns            : MSR_SUCCESS / MSR_FAILURE                        */
/****************************************************************************/

INT4
MsrRmSendPkt (INT4 i4Sockfd, UINT1 *pu1Buf, UINT2 u2Len, INT4 *pi4WriteBytes)
{
    INT4                i4WrBytes;    /* Number of bytes written */
    INT4                i4RetVal = MSR_SUCCESS;

    MSR_TRC_ARG1 (MSR_RM_TRACE_TYPE, "MsrRmSendPkt: "
                  "Sending %d bytes of data\r\n", u2Len);

    if ((gu4TxCount % 10) == 0)
    {
        OsixTskDelay (1);
    }

    /* Write the packet on the socket */
    i4WrBytes = send (i4Sockfd, pu1Buf, u2Len, MSG_NOSIGNAL);

    MSR_TRC_ARG1 (MSR_RM_TRACE_TYPE, "MsrRmSendPkt: "
                  "%d bytes of data sent\r\n", i4WrBytes);

    if ((i4WrBytes <= 0) && (errno != EWOULDBLOCK) && (errno != EAGAIN))
    {
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE, "MsrRmSendPkt: Write failed\r\n");
        perror ("MsrRmSendPkt : Write failed");
        i4RetVal = MSR_FAILURE;
    }

    *pi4WriteBytes = i4WrBytes;
    gu4TxCount++;
    return i4RetVal;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmRcvPkt                                      */
/*                                                                          */
/*    Description        : Active and Standby node                          */
/*                         This function receives the packet from the peer  */
/*                         node                                             */
/*                                                                          */
/*    Input(s)           : i4Sockfd     -    Socket fd                      */
/*                         pu1Buf       -    pointer to the linear buffer   */
/*                         u2Len        -    Length of the packet           */
/*                                                                          */
/*    Output(s)          : pi4ReadBytes -    Bytes read                     */
/*                                                                          */
/*    Returns            : MSR_SUCCESS / MSR_FAILURE                        */
/****************************************************************************/
INT4
MsrRmRcvPkt (INT4 i4Sockfd, UINT1 *pu1Buf, UINT2 u2Len, INT4 *pi4ReadBytes)
{
    INT4                i4ReadBytes;    /* Number of bytes read */
    INT4                i4RetVal = MSR_SUCCESS;

    MSR_TRC_ARG1 (MSR_RM_TRACE_TYPE, "MsrRmRcvPkt: "
                  "Reading %d bytes of data\r\n", u2Len);

    /* Read the packet on the socket */
    i4ReadBytes = recv (i4Sockfd, pu1Buf, u2Len, MSG_NOSIGNAL);

    MSR_TRC_ARG1 (MSR_RM_TRACE_TYPE, "MsrRmRcvPkt: "
                  "%d bytes of data read\r\n", i4ReadBytes);

    if ((i4ReadBytes <= 0) && (errno != EWOULDBLOCK) && (errno != EAGAIN))
    {
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE, "MsrRmRcvPkt : Read failed\r\n");
        perror ("MsrRmRcvPkt : Read failed");
        i4RetVal = MSR_FAILURE;
    }

    *pi4ReadBytes = i4ReadBytes;
    return i4RetVal;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmSelAddFd                                    */
/*                                                                          */
/*    Description        : Active and Standby node                          */
/*                         This function adds the function pointer to       */
/*                         the select library                               */
/*                                                                          */
/*    Input(s)           : i4Sockfd   -    Socket fd                        */
/*                         pCallBk    -    Call back function               */
/*                         i1ReadFlag -    MSR_TRUE for read fd             */
/*                                         MSR_FALSE for write fd           */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MsrRmSelAddFd (INT4 i4Sockfd, VOID (*pCallBk) (INT4), INT1 i1ReadFlag)
{
    if (i1ReadFlag == MSR_TRUE)
    {
        SelAddFd (i4Sockfd, pCallBk);
    }
    else
    {
        SelAddWrFd (i4Sockfd, pCallBk);
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmSelRemoveFd                                 */
/*                                                                          */
/*    Description        : Active and Standby node                          */
/*                         This function removes the function pointer from  */
/*                         the select library                               */
/*                                                                          */
/*    Input(s)           : i4Sockfd   -    Socket fd                        */
/*                         i1ReadFlag -    MSR_TRUE for read fd             */
/*                                         MSR_FALSE for write fd           */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MsrRmSelRemoveFd (INT4 i4Sockfd, INT1 i1ReadFlag)
{
    if (i1ReadFlag == MSR_TRUE)
    {
        SelRemoveFd (i4Sockfd);
    }
    else
    {
        SelRemoveWrFd (i4Sockfd);
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmSockClose                                   */
/*                                                                          */
/*    Description        : Active and Standby node                          */
/*                         This function closes the socket fd               */
/*                                                                          */
/*    Input(s)           : i4Sockfd   -    Socket fd                        */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MsrRmSockClose (INT4 i4Sockfd)
{
    close (i4Sockfd);
    MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmSockClose: " "Socket closed\r\n");
}

#endif
