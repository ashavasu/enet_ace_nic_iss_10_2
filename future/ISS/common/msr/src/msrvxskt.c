/********************************************************************
 * * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 * *
 * * $Id: msrvxskt.c,v 1.2 2008/10/23 13:35:37 prabuc-iss Exp $
 * *
 * * Description: MSR-RM socket layer interface APIs for VX WORKS
 * *******************************************************************/
#ifndef _MSR_RM_SOCKC_
#define _MSR_RM_SOCKC_

#include "lr.h"
#include "msr.h"
#include "selutil.h"
#include "rmgr.h"
#include "msrrm.h"
#include "msrtrc.h"

#include <unistd.h>

extern UINT4        gu4MsrTrcFlag;

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmServerInit                                  */
/*                                                                          */
/*    Description        : Active node                                      */
/*                         This function creates a server socket for RB     */
/*                         Tree sync up messages. This function is called   */
/*                         when GO_ACTIVE event is received from RM         */
/*                                                                          */
/*    Input(s)           : u4Addr   -   Self address                        */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : socket fd on success else failure                */
/****************************************************************************/

INT4
MsrRmServerInit (UINT4 u4Addr)
{
    UNUSED_PARAM (u4Addr);
    return (-1);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmClientInit                                  */
/*                                                                          */
/*    Description        : Standby node                                     */
/*                         This function connects to the server socket      */
/*                         for RB Tree sync up messages. This function      */
/*                         is called when MSR_RM_FILE_TRANSFER_COMPLETE     */
/*                         event is received.                               */
/*                                                                          */
/*    Input(s)           : u4Addr     -    Server node address              */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : Socket fd on success else -1                     */
/****************************************************************************/

INT4
MsrRmClientInit (UINT4 u4Addr)
{
    UNUSED_PARAM (u4Addr);
    return (-1);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmServerDeInit                                */
/*                                                                          */
/*    Description        : Active/Standby node                              */
/*                         This function deletes the server socket created  */
/*                         for RB Tree sync up messages.                    */
/*                                                                          */
/*                         When GO_ACTIVE event is received, the server     */
/*                         socket  must be deleted before creating a new    */
/*                         server socket.                                   */
/*                                                                          */
/*                         When GO_STANDBY event is received, the server    */
/*                         socket should be deleted if the transition is    */
/*                         from active to standby                           */
/*                                                                          */
/*    Input(s)           : i4Sockfd   - Socket fd                           */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MsrRmServerDeInit (INT4 i4Sockfd)
{
    UNUSED_PARAM (i4Sockfd);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmChannelDeInit                               */
/*                                                                          */
/*    Description        : Active/Standby node                              */
/*                         This function closes the socket fd used for      */
/*                         for RB Tree sync up messages.                    */
/*                                                                          */
/*                         When GO_ACTIVE event is received, the connection */
/*                         fd is closed and conn id is used for             */
/*                         accepting new connection.                        */
/*                                                                          */
/*                         When GO_STANDBY event is received, the           */
/*                         connection fd is closed and the conn id is used  */
/*                         for connecting to a new channel                  */
/*                                                                          */
/*                         The active and standby node closes the channel   */
/*                         after complete synchronization.                  */
/*                                                                          */
/*    Input(s)           : i4Sockfd   - Socket fd                           */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MsrRmChannelDeInit (INT4 i4Sockfd)
{
    UNUSED_PARAM (i4Sockfd);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmAcceptConnection                            */
/*                                                                          */
/*    Description        : Active node                                      */
/*                         This function accepts the incoming connection    */
/*                         and stores the socket descriptor in the global   */
/*                         info. This function is called when               */
/*                         MSR_RM_NEW_CONNECTION event is received          */
/*                                                                          */
/*    Input(s)           : i4SrvSockFd  -  Server socket fd                 */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : Connection id on success else failure            */
/****************************************************************************/

INT4
MsrRmAcceptConnection (INT4 i4SrvSockFd)
{
    UNUSED_PARAM (i4SrvSockFd);
    return (-1);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmSendPkt                                     */
/*                                                                          */
/*    Description        : Active and Standby node                          */
/*                         This function sends the packet to the peer       */
/*                         node                                             */
/*                                                                          */
/*    Input(s)           : i4Sockfd      -    Socket fd                     */
/*                         pu1Buf        -    pointer to the linear buffer  */
/*                         u2Len         -    Length of the packet          */
/*                                                                          */
/*    Output(s)          : pi4WriteBytes -    Bytes written                 */
/*                                                                          */
/*    Returns            : MSR_SUCCESS / MSR_FAILURE                        */
/****************************************************************************/

INT4
MsrRmSendPkt (INT4 i4Sockfd, UINT1 *pu1Buf, UINT2 u2Len, INT4 *pi4WriteBytes)
{
    UNUSED_PARAM (i4Sockfd);
    UNUSED_PARAM (pu1Buf);
    UNUSED_PARAM (u2Len);
    UNUSED_PARAM (pi4WriteBytes);
    return MSR_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmRcvPkt                                      */
/*                                                                          */
/*    Description        : Active and Standby node                          */
/*                         This function receives the packet from the peer  */
/*                         node                                             */
/*                                                                          */
/*    Input(s)           : i4Sockfd     -    Socket fd                      */
/*                         pu1Buf       -    pointer to the linear buffer   */
/*                         u2Len        -    Length of the packet           */
/*                                                                          */
/*    Output(s)          : pi4ReadBytes -    Bytes read                     */
/*                                                                          */
/*    Returns            : MSR_SUCCESS / MSR_FAILURE                        */
/****************************************************************************/
INT4
MsrRmRcvPkt (INT4 i4Sockfd, UINT1 *pu1Buf, UINT2 u2Len, INT4 *pi4ReadBytes)
{
    UNUSED_PARAM (i4Sockfd);
    UNUSED_PARAM (pu1Buf);
    UNUSED_PARAM (u2Len);
    UNUSED_PARAM (pi4ReadBytes);
    return MSR_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmSelAddFd                                    */
/*                                                                          */
/*    Description        : Active and Standby node                          */
/*                         This function adds the function pointer to       */
/*                         the select library                               */
/*                                                                          */
/*    Input(s)           : i4Sockfd   -    Socket fd                        */
/*                         pCallBk    -    Call back function               */
/*                         i1ReadFlag -    MSR_TRUE for read fd             */
/*                                         MSR_FALSE for write fd           */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MsrRmSelAddFd (INT4 i4Sockfd, VOID (*pCallBk) (INT4), INT1 i1ReadFlag)
{
    UNUSED_PARAM (i4Sockfd);
    UNUSED_PARAM (pCallBk);
    UNUSED_PARAM (i1ReadFlag);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmSelRemoveFd                                 */
/*                                                                          */
/*    Description        : Active and Standby node                          */
/*                         This function removes the function pointer from  */
/*                         the select library                               */
/*                                                                          */
/*    Input(s)           : i4Sockfd   -    Socket fd                        */
/*                         i1ReadFlag -    MSR_TRUE for read fd             */
/*                                         MSR_FALSE for write fd           */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MsrRmSelRemoveFd (INT4 i4Sockfd, INT1 i1ReadFlag)
{
    UNUSED_PARAM (i4Sockfd);
    UNUSED_PARAM (i1ReadFlag);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmSockClose                                   */
/*                                                                          */
/*    Description        : Active and Standby node                          */
/*                         This function closes the socket fd               */
/*                                                                          */
/*    Input(s)           : i4Sockfd   -    Socket fd                        */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MsrRmSockClose (INT4 i4Sockfd)
{
    UNUSED_PARAM (i4Sockfd);
}

#endif
