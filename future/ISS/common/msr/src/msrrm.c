/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: msrrm.c,v 1.81 2016/06/30 10:13:51 siva Exp $
*
* Description: MSR-RM interface APIs.
*********************************************************************/
#ifndef _MSR_RM_C_
#define _MSR_RM_C_

#include "msrsys.h"
#include "snmputil.h"
#include "fssnmp.h"

/* Declarations */
UINT1               gu1MsrNodeStatus = RM_INIT;
UINT1               gu1MsrPrevNodeStatus = RM_INIT;

/* Pointer to the global structure used for redundancy */
tMSRRmGlobalInfo    gMSRRmGlobalInfo;

/* Pointer to the index of MSR RB tree node to get the
 * next static bulk update to be sent
 */
tMSRRmLastUpdatedNode gMSRRmLastUpdatedNode;

extern UINT1        gu1RmStConfReq;
                                   /* Global flag used to distinguish
                                    * whether the MIB save is initiated
                                    * by administrator or by standby node
                                    */
#ifdef SSH_WANTED
extern void         SshArKeyGen (tOsixTaskId TaskId);
#endif

#ifdef ISS_WANTED

extern INT4         IssReadSystemInfoFromNvRam (VOID);
extern VOID         IssSetRmIfNameToNvRam (INT1 *);
extern VOID         IssSetSoftwareVersionToNvRam (INT1 *);
extern VOID         IssSetInterfaceToNvRam (INT1 *);

#ifdef MBSM_WANTED
extern INT4         MbsmGetSlotModuleInfoFromFile (VOID);
#endif
#endif

VOID MsrRmStartStaticBulkUpdate PROTO ((VOID));

VOID MsrRmStartSendStaticUpdate PROTO ((INT1 *pi1Param));

INT4 MsrRmObtainCfgAndSendUpdate PROTO ((VOID));

INT4 MsrRmUpdateAndSendBulkUpdateMsg PROTO ((UINT1 *pu1Buf, UINT4 u4BufLen));

UINT1              *MsrRmConstructAndFillRmHeader PROTO ((UINT4 *pu4Length));

INT4 MsrRmGetTableConfig PROTO ((UINT4 u4Index, tSNMP_OID_TYPE * pCurOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tMSRRbtree * pRbNode,
                                 tSNMP_MULTI_DATA_TYPE * pDefData,
                                 UINT4 u4PrevOidValue, UINT1 *pu1Flag));

INT4 MsrRmGetTableRSConfig PROTO ((UINT4 u4Index, tSNMP_OID_TYPE * pCurOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tMSRRbtree * pRbNode, UINT1 *pu1Flag));

INT4 MsrRmGetScalarConfig PROTO ((UINT4 u4Index, tMSRRbtree * pRbNode));

VOID MsrRmNvramPreSyncUp PROTO ((VOID));

UINT1              *MsrRmAllocateBuffer PROTO ((VOID));

/****************************************************************************
 *
 *    FUNCTION NAME    : MsrRmTmrInitTmrDesc
 *
 *    DESCRIPTION      : This function intializes the timer desc for all
 *                       the timers in MSR-RM module.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
VOID
MsrRmTmrInitTmrDesc (VOID)
{
    gMSRRmGlobalInfo.aTmrDesc[MSR_RM_RECONNECT_TIMER].TmrExpFn =
        MsrRmReConnectTmrExpiry;
    gMSRRmGlobalInfo.aTmrDesc[MSR_RM_RECONNECT_TIMER].i2Offset = -1;
}

/******************************************************************************
 * Function           : MsrRcvPktFromRm 
 * Input(s)           : u1Event - event sent by RM 
 *                      tRmMsg - RM data.
 *                      u2DataLen - Length of data
 * Output(s)          : None.
 * Returns            : MSR_SUCCESS/MSR_FAILURE. 
 * Action             : Call back function registered with RM to recv pkts
 *                      from RM.
 ******************************************************************************/
INT4
MsrRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tMSRRmQueueMsg     *pMSRRmQueueMsg = NULL;
    UINT4               u4MsrEvent;
    INT4                i4RetVal = MSR_SUCCESS;

    UNUSED_PARAM (u2DataLen);

    /* This function will be called in RM context. Therefore,
     * post the msg / event to MSR task */
    if (u1Event == GO_ACTIVE)
    {
        u4MsrEvent = MSR_RM_GO_ACTIVE;
    }
    else if (u1Event == GO_STANDBY)
    {
        u4MsrEvent = MSR_RM_GO_STANDBY;
    }
    else if (u1Event == RM_FILE_TRANSFER_COMPLETE)
    {
        u4MsrEvent = MSR_RM_FILE_TRANSFER_COMPLETE;
    }
    else if ((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
             (u1Event == RM_STANDBY_DOWN))
    {
        /* pData is not NULL for RM_MESSAGE event */
        u4MsrEvent = MSR_RM_MESSAGE;

        /* Allocate the memory for Q message */
        if ((pMSRRmQueueMsg =
             (tMSRRmQueueMsg *) (MemAllocMemBlk
                                 (gMSRRmGlobalInfo.MsrQPoolId))) == NULL)
        {
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE | OS_RESOURCE_TRC,
                     "Memory Block allocation failed !!!\r\n");
            i4RetVal = MSR_FAILURE;
        }
        else
        {
            pMSRRmQueueMsg->pRmMsg = pData;
            pMSRRmQueueMsg->u1Event = u1Event;
        }
        /* pData is valid only if u1Event==RM_MESSAGE or RM_STANDBY_UP or
         * RM_STANDBY_DOWN. So send to Q only for these events
         */
        if ((i4RetVal == MSR_SUCCESS) &&
            (OsixQueSend (gMSRRmGlobalInfo.MsrQId,
                          (UINT1 *) &pMSRRmQueueMsg,
                          OSIX_DEF_MSG_LEN) != OSIX_SUCCESS))
        {
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE | OS_RESOURCE_TRC,
                     "SendToQ failed in MsrRcvPktFromRm\n");

            MemReleaseMemBlock (gMSRRmGlobalInfo.MsrQPoolId,
                                (UINT1 *) pMSRRmQueueMsg);

            i4RetVal = MSR_FAILURE;
        }
        if ((gMSRIncrSaveFlag == ISS_FALSE) && (u1Event == RM_STANDBY_DOWN))
        {
            /* Send an event to the gMsRmTaskId so that it stops its 
             * current action of sending the static bulk updates */
            if (gMsRmTaskId != 0)
            {
                OsixEvtSend (gMsRmTaskId, MSR_RM_STANDBY_DOWN_EVT);
            }
        }

        if ((i4RetVal == MSR_FAILURE) && (pData != NULL))
        {
            if (u1Event == RM_MESSAGE)
            {
                RM_FREE (pData);
            }
            else
            {
                if (RmReleaseMemoryForMsg ((UINT1 *) pData) == RM_FAILURE)
                {
                    MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE | OS_RESOURCE_TRC,
                             "MsrRcvPktFromRm : Failed to release the memory "
                             "allocated by the RM module\r\n");
                }
            }
        }
    }
    else                        /* u1Event Invalid */
    {
        if (pData != NULL)
        {
            RM_FREE (pData);
        }
        MSR_TRC (MSR_RM_TRACE_TYPE,
                 "Invalid Event rcvd in MsrRcvPktFromRm\r\n");
        i4RetVal = MSR_FAILURE;
        return i4RetVal;
    }

    if (i4RetVal == MSR_SUCCESS)
    {
        OsixEvtSend (MsrId, u4MsrEvent);
    }

    return i4RetVal;
}

/******************************************************************************
 * Function           : MsrProcessRmEvt 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : MSR_SUCCESS/MSR_FAILURE. 
 * Action             : Function to process the RM_MESSAGE event received 
 *                      from RM. 
 ******************************************************************************/
VOID
MsrProcessRmEvt (VOID)
{
    tMSRRmQueueMsg     *pMSRRmQueueMsg = NULL;
    tRmNodeInfo        *pRmNode = NULL;
    UINT4               u4MsgType;
    UINT2               u2FlashCksum;
    tRmProtoAck         ProtoAck;
    UINT4               u4SeqNum = 0;
    UINT2               u2PktLen = 0;
    INT4                i4ResType = 0;

    ProtoAck.u4AppId = RM_MSR_APP_ID;
    i4ResType = IssGetRestoreTypeFromNvRam ();

    while (OsixQueRecv (gMSRRmGlobalInfo.MsrQId, (UINT1 *) &pMSRRmQueueMsg,
                        OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
    {
        if ((pMSRRmQueueMsg->u1Event == RM_STANDBY_UP) ||
            (pMSRRmQueueMsg->u1Event == RM_STANDBY_DOWN))
        {
            pRmNode = (tRmNodeInfo *) pMSRRmQueueMsg->pRmMsg;
            gMSRRmGlobalInfo.u1NumPeers = pRmNode->u1NumStandby;

            if (pMSRRmQueueMsg->u1Event == RM_STANDBY_DOWN)
            {
                MsrRmProcessStandbyDownEvent ();
            }

            RmReleaseMemoryForMsg ((UINT1 *) (pMSRRmQueueMsg->pRmMsg));
            MemReleaseMemBlock (gMSRRmGlobalInfo.MsrQPoolId,
                                (UINT1 *) pMSRRmQueueMsg);
            continue;
        }

        /* Read the sequence number from the RM Header */
        RM_PKT_GET_SEQNUM (pMSRRmQueueMsg->pRmMsg, &u4SeqNum);
        /* Remove the RM Header */
        RM_STRIP_OFF_RM_HDR (pMSRRmQueueMsg->pRmMsg, u2PktLen);
        ProtoAck.u4SeqNumber = u4SeqNum;

        /* Get the Msr data ffrom the rcvd buf. */
        RM_GET_DATA_4_BYTE (pMSRRmQueueMsg->pRmMsg, MSR_DATA_OFF_MSG_TYPE,
                            u4MsgType);
        RM_GET_DATA_2_BYTE (pMSRRmQueueMsg->pRmMsg, MSR_DATA_OFF_FLASH_CKSUM,
                            u2FlashCksum);

        /* Free the buffer after obtaining the necessary info */
        RM_FREE (pMSRRmQueueMsg->pRmMsg);

        switch (u4MsgType)
        {
                /* CONFIG_CHANGE - Peer node MSR has informed about the change 
                 * in configuration. This node has to obtain the modified 
                 * configuration to be in-sync with peer.  */
            case CONFIG_CHANGE:
#ifdef MBSM_WANTED
                if (RmRequestFile (FILE_SLOT_INFO_ID) == RM_FAILURE)
                {
                    MSR_TRC (MSR_RM_TRACE_TYPE,
                             "RmRequestFile failed for iss.conf\n");
                }
#endif /* MBSM_WANTED */

                if (MsrSendMsgToRm (FLASH_CKSUM_REQ) == MSR_FAILURE)
                {
                    MSR_TRC (MSR_RM_TRACE_TYPE,
                             "MsrSendMsgToRm FLASH_CKSUM_REQ failed\n");
                }

                gi1ConfigChange = MSR_TRUE;
                break;

                /* FLASH_CKSM_REQ - Peer node MSR has quereied about the flash 
                 * cksum of this node. This node has to reply back with the cksum. */
            case FLASH_CKSUM_REQ:
                if (MsrSendMsgToRm (FLASH_CKSUM_RESP) == MSR_FAILURE)
                {
                    MSR_TRC (MSR_RM_TRACE_TYPE,
                             "MsrSendMsgToRm FLASH_CKSUM_RESP failed\n");
                }
                break;

                /* FLASH_CKSUM_RESP - This node has rcvd a resp from peer node. 
                 * Compare the rcvd cksum with the cksum of this node and if 
                 * not equal, both are not in-sync. If both are equal, they 
                 * are in-sync. */
            case FLASH_CKSUM_RESP:
                if (i4ResType != SET_FLAG)
                {
                    /* FlashCksum sent by the peer will be 0, if the peer 
                     * doesn't have the configuration file iss.conf. */
                    if (u2FlashCksum == 0)
                    {
                        MSR_TRC (MSR_RM_TRACE_TYPE,
                                 "Peer doesn't have the config file. No need save the configuration\n");
                        break;
                    }

                    /* check if ACTIVE and STANDBY node are in-sync */
                    if ((FlashReadCksum (&gu2FlashCksum) == ISS_FAILURE) ||
                        (gu2FlashCksum != u2FlashCksum))
                    {
                        /* Either file not found (or) file not in-sync */
                        /* Send Bulk Data Request To Peer */
                        MSR_TRC (MSR_RM_TRACE_TYPE,
                                 "Configuration not in-sync. Send Bulk Data Req to save the configuration\n");

                        if (RmRequestFile (FILE_MIB_SAVE_CONF_ID) == RM_FAILURE)
                        {
                            MSR_TRC (MSR_RM_TRACE_TYPE,
                                     "RmRequestFile failed for iss.conf\n");
                        }
#ifdef ISS_WANTED
                        else
                        {
                            /* The configuration file request is sent to the active node
                             * Since the configuration file exist in the active node, set 
                             * the restoration flag to true
                             */
                            IssSetRestoreOptionToNvRam (ISS_CONFIG_RESTORE);
                            IssSetRestoreFlagToNvRam (SET_FLAG);
                            IssSetSaveFlagToNvRam (SET_FLAG);
                        }
#endif
                    }
                }
                else
                {
                    if (RmRequestFile (FILE_MIB_SAVE_CONF_ID) == RM_FAILURE)
                    {
                        MSR_TRC (MSR_RM_TRACE_TYPE,
                                "RmRequestFile failed for iss.conf\n");
                    }
#ifdef ISS_WANTED
                    else
                    {
                        /* The configuration file request is sent to the active
node
                         * Since the configuration file exist in the active nodee
, set
                         * the restoration flag to true */
                        IssSetRestoreOptionToNvRam (ISS_CONFIG_RESTORE);
                        IssSetRestoreFlagToNvRam (SET_FLAG);
                        IssSetSaveFlagToNvRam (SET_FLAG);
                    }
#endif
                }
                break;

            default:
                MSR_TRC (MSR_RM_TRACE_TYPE, "Invalid Message Type\n");
                return;
                break;
        }

        /* Sending ACK to RM */
        RmApiSendProtoAckToRM (&ProtoAck);

        MemReleaseMemBlock (gMSRRmGlobalInfo.MsrQPoolId,
                            (UINT1 *) pMSRRmQueueMsg);
    }
}

/******************************************************************************
 * Function           : MsrSendMsgToRm 
 * Input(s)           : MsgType, 
 * Output(s)          : None.
 * Returns            : MSR_SUCCESS/MSR_FAILURE. 
 * Action             : Function to send the msg from MSR to RM.
 ******************************************************************************/
INT4
MsrSendMsgToRm (UINT4 u4MsgType)
{
    tRmMsg             *pMsg;
    tMsrData            MsrData;
    UINT2               u2DataLen;
    INT4                i4RetVal = MSR_SUCCESS;

    /* Allocate memory for data to be sent to RM. This memory will be freed by 
     * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (sizeof (tMsrData))) == NULL)
    {
        MSR_TRC (MSR_RM_TRACE_TYPE, "Rm alloc failed\n");
        return (MSR_FAILURE);
    }

    MEMSET (&MsrData, 0, sizeof (tMsrData));

    /* Fill in the data */
    MsrData.u4MsgType = OSIX_HTONL (u4MsgType);

    if (u4MsgType == FLASH_CKSUM_REQ)
    {
        /* Flash cksum is yet to be calculated */
        MsrData.u2FlashCksum = 0;
    }
    else if (u4MsgType == FLASH_CKSUM_RESP)
    {
        MsrData.u2FlashCksum = OSIX_HTONS (gu2FlashCksum);
    }

    u2DataLen = (UINT2) sizeof (tMsrData);

    /* Copy MSR data to the appropriate offset */
    if ((RM_COPY_TO_OFFSET (pMsg, &MsrData, 0, sizeof (tMsrData)))
        != CRU_SUCCESS)
    {
        MSR_TRC (MSR_RM_TRACE_TYPE, "Rm copy to CRU buf failed\n");
        i4RetVal = MSR_FAILURE;
    }

    if (i4RetVal != MSR_FAILURE)
    {
        /* Call the API provided by RM to send the data to RM */
        if (RmEnqMsgToRmFromAppl (pMsg, u2DataLen, RM_MSR_APP_ID, RM_MSR_APP_ID)
            == RM_FAILURE)
        {
            MSR_TRC (MSR_RM_TRACE_TYPE, "Enq to RM from appl failed\n");
            i4RetVal = MSR_FAILURE;
        }
    }

    if (i4RetVal == MSR_FAILURE)
    {
        /* pMsg is freed only in failure case. RM will free the buf
         * in success case. */
        RM_FREE (pMsg);
    }

    return (i4RetVal);
}

/******************************************************************************
 * Function           : MsrProcessRmGoActive 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Function to process the GO_ACTIVE event received from RM. 
 ******************************************************************************/
VOID
MsrProcessRmGoActive (VOID)
{
#ifdef L2RED_WANTED
    UINT4               u4ActiveNodeId = 0;    /* Self node id to create a
                                             * server socket
                                             */
#endif
    tRmProtoEvt         ProtoEvt;
    INT1                i1Param = 0;
    if (gu1MsrNodeStatus == RM_ACTIVE)
    {
        return;
    }

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_MSR_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    gu1MsrPrevNodeStatus = gu1MsrNodeStatus;
    gu1MsrNodeStatus = RM_ACTIVE;
#ifdef L2RED_WANTED
    /* The node has become active. Create a server socket and wait for
     * new connection to standby node
     */
    MsrRmChannelDeInit (gMSRRmGlobalInfo.i4ConnFd);
    gMSRRmGlobalInfo.i4ConnFd = -1;

    MsrRmServerDeInit (gMSRRmGlobalInfo.i4SrvSockFd);
    gMSRRmGlobalInfo.i4SrvSockFd = -1;

    RmGetSelfNodeIp (&u4ActiveNodeId);

    if (u4ActiveNodeId != 0)
    {
        gMSRRmGlobalInfo.i4SrvSockFd = MsrRmServerInit (u4ActiveNodeId);
    }

    if (gMSRRmGlobalInfo.i4SrvSockFd == -1)
    {
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                 "MsrProcessRmGoActive: Server socket "
                 "creation failed !!!\r\n");
    }

    /* Clear the global partial buffer */
    if (gMSRRmGlobalInfo.pBuf != NULL)
    {
        MemReleaseMemBlock (gMSRRmGlobalInfo.BufPoolId,
                            (UINT1 *) (gMSRRmGlobalInfo.pBuf));
        gMSRRmGlobalInfo.pBuf = NULL;
        gMSRRmGlobalInfo.u4BytesSentOrRcvd = 0;
    }

#ifndef MBSM_WANTED
    /* CFA Gdd Poll timer was disabled in STANDBY node.
     * Now the STANDBY node has become ACTIVE. Therefore,
     * start CFA Gdd Poll timer by sending an event to CFA
     * task as if the poll timer is expired. */
    if (CfaSendEventToCfaTask (CFA_GDD_POLL_TMR_EXP_EVENT) != CFA_SUCCESS)
    {
        MSR_TRC (MSR_RM_TRACE_TYPE,
                 "MsrProcessRmGoActive: Send evt to CFA failure\n");
    }
#endif /* #ifndef MBSM_WANTED */
#endif
    if (gu1MsrPrevNodeStatus == RM_INIT)
    {
        /* When the node is coming up for the first time and becoming
         * active, trigger the configuration restore.
         */
        i1Param = ISS_SPAWN_MSR_RM_TASK;
        TgtCustomStartup (&i1Param);

#ifdef MBSM_WANTED
        if (MbsmGetBootFlag () == MBSM_TRUE)
        {
            if (gu1CfaBootFlag == MSR_TRUE)
#endif
            {
                /* MSR restoration should be initiated only when
                 * GO_CTIVE is rcvd after CFA_BOOT_COMPLETE_EVENT */
                IssCustMsrInit ();
            }
#ifdef MBSM_WANTED
        }
#endif
    }

#ifdef L2RED_WANTED
    if (gu1MsrPrevNodeStatus == RM_STANDBY)
    {
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;

        /* If the previous state is standby, then there can be a chance
         * that the re-connect timer is running. Stop the timer if it is
         * running
         */

        if (gMSRRmGlobalInfo.i1IsTmrRunning == MSR_TRUE)
        {
            TmrStop (gMSRRmGlobalInfo.TmrListId,
                     &(gMSRRmGlobalInfo.aTmrBlk[MSR_RM_RECONNECT_TIMER]));
            gMSRRmGlobalInfo.i1IsTmrRunning = MSR_FALSE;
        }

        if (RmGetStaticConfigStatus () == RM_STATIC_CONFIG_IN_PROGRESS)
        {
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                     "Warning!!! System may have inconsistent information "
                     "as static configuration restoration is not "
                     "completed.\r\n");
            MsrisRetoreinProgress = ISS_FALSE;
            gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
            RmSetStaticConfigStatus (RM_STATIC_CONFIG_RESTORED);
        }
    }
    else
    {
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }
    RmApiHandleProtocolEvent (&ProtoEvt);
#endif
}

/******************************************************************************
 * Function           : MsrProcessRmGoStandby 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Function to process GO_STANDBY event received from RM. 
 ******************************************************************************/
VOID
MsrProcessRmGoStandby (VOID)
{
    tRmProtoEvt         ProtoEvt;
#ifdef SSH_WANTED
    tOsixTaskId         u4SelfTaskId;
#endif
    if (gu1MsrNodeStatus == RM_STANDBY)
    {
        return;
    }

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_MSR_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    gu1MsrPrevNodeStatus = gu1MsrNodeStatus;
    gu1MsrNodeStatus = RM_STANDBY;
    MsrResumeUserConfiguration ();
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        if (gu1CfaBootFlag == MSR_TRUE)
        {
            /* When MSR processing GO_STANDBY event after
             * processing CFA_BOOT_COMPLETE_EVENT, CLI should 
             * be given here. */
            gi4MibResStatus = MIB_RESTORE_NOT_INITIATED;
            MsrSendRestorationCompleteToProtocols ();

        }
    }
#ifdef L2RED_WANTED
    /* The node state is changed to standby. Delete the server socket if
     * it is already created
     */
    MsrRmChannelDeInit (gMSRRmGlobalInfo.i4ConnFd);
    gMSRRmGlobalInfo.i4ConnFd = -1;

    MsrRmServerDeInit (gMSRRmGlobalInfo.i4SrvSockFd);
    gMSRRmGlobalInfo.i4SrvSockFd = -1;

    /* Free the memory allocated for last added RB Tree node */
    free_oid (gMSRRmLastUpdatedNode.pOidValue);
    gMSRRmLastUpdatedNode.pOidValue = NULL;

    /* Clear the global partial buffer */
    if (gMSRRmGlobalInfo.pBuf != NULL)
    {
        MemReleaseMemBlock (gMSRRmGlobalInfo.BufPoolId,
                            (UINT1 *) (gMSRRmGlobalInfo.pBuf));
        gMSRRmGlobalInfo.pBuf = NULL;
        gMSRRmGlobalInfo.u4BytesSentOrRcvd = 0;
    }

    if (RmGetForceSwitchOverFlag () == RM_FSW_OCCURED)
    {
        /* Since the nodes becomes standby due to force switchover,
         * there is no need the sync the static configurations with
         * active node */
        RmSetForceSwitchOverFlag (RM_FSW_NOT_OCCURED);
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    RmSetStaticConfigStatus (RM_STATIC_CONFIG_IN_PROGRESS);

    MSR_TRC (MSR_RM_TRACE_TYPE, "MsrProcessRmGoStandby: "
             "MSR restoration is in progress\r\n");
#ifdef SSH_WANTED
    OsixTskIdSelf (&u4SelfTaskId);
    SshArKeyGen (u4SelfTaskId);
#endif
#ifdef MBSM_WANTED
    if (RmRequestFile (FILE_SLOT_INFO_ID) == RM_FAILURE)
    {
        MSR_TRC (MSR_RM_TRACE_TYPE,
                 "RmRequestFile SlotModule.conf file request to ACTIVE failed\n");
    }
#endif /* MBSM_WANTED */

    /* Standby Node will get users info from Active Node, so that after SwitchOver
     * newly added users also can login in that Node */
    if (RmRequestFile (FILE_USERS_INFO_ID) == RM_FAILURE)
    {
        MSR_TRC (MSR_RM_TRACE_TYPE,
                 "RmRequestFile users file request to ACTIVE failed\n");
    }

    /*Newly added privil & its passwd needs to sync up to standby */
    if (RmRequestFile (FILE_PRIVIL_ID) == RM_FAILURE)
    {
        MSR_TRC (MSR_RM_TRACE_TYPE,
                 "RmRequestFile privil file request to ACTIVE failed\n");
    }

    /*Sync up issnvram.txt file with standby node */
    if (RmRequestFile (FILE_NVRAM_ID) == RM_FAILURE)
    {
        MSR_TRC (MSR_RM_TRACE_TYPE,
                 "RmRequestFile issnvram.txt file request to ACTIVE failed\n");
    }

    /*Sync up system.size file with standby node */
    if (RmRequestFile (FILE_SYSTEM_SIZE_ID) == RM_FAILURE)
    {
        MSR_TRC (MSR_TRACE_TYPE,
                 "RmRequestFile system.size file request to ACTIVE failed\n");
    }

    /*Sync up server_key_1 file with standby node */
    if (RmRequestFile (FILE_SERVER_KEY_ID1) == RM_FAILURE)
    {
        MSR_TRC (MSR_TRACE_TYPE,
                 "RmRequestFile server_key_1 file request to ACTIVE failed\n");
    }

    /*Sync up server_key_2 file with standby node */
    if (RmRequestFile (FILE_SERVER_KEY_ID2) == RM_FAILURE)
    {
        MSR_TRC (MSR_TRACE_TYPE,
                 "RmRequestFile server_key_2 file request to ACTIVE failed\n");
    }
    if (RmRequestFile (FILE_SSL_SERV_CERT) == RM_FAILURE)
    {
        MSR_TRC (MSR_TRACE_TYPE,
                 "RmRequestFile Certificate request to ACTIVE failed\n");
    }
#ifdef OSPF3_WANTED
    if (RmRequestFile (FILE_V3_CRYPT_SEQNUM) == RM_FAILURE)
    {
        MSR_TRC (MSR_TRACE_TYPE,
                 "RmRequestFile Crypt seq Num to ACTIVE failed\n");
    }

#endif
    ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
    RmApiHandleProtocolEvent (&ProtoEvt);
#endif
}

#ifdef L2RED_WANTED
/******************************************************************************
 * Function           : MsrProcessRmFileTransferComplete 
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Function to process GO_STANDBY event received from RM. 
 ******************************************************************************/
VOID
MsrProcessRmFileTransferComplete (VOID)
{
    UINT1               u1Restart = 0;
    UINT4               u4PrevBridgeMode;
    INT1                ai1PrevRmDevName[CFA_MAX_PORT_NAME_LENGTH];
    INT1                ai1PrevSoftwareVersion[ISS_STR_LEN];
    UINT4               u4Len = 0;
    UINT1               au1IssDefaultInterface[CFA_MAX_PORT_NAME_LENGTH];
#ifdef SNMP_3_WANTED
    INT1                ai1EngineID[SNMP_MAX_STR_ENGINEID_LEN + 1];

    MEMSET (ai1EngineID, 0, SNMP_MAX_STR_ENGINEID_LEN + 1);
#endif
    
    MEMSET (ai1PrevSoftwareVersion , 0 , ISS_STR_LEN);
    /* This event will be processed only by standby node */
    if (RmGetNodeState () != RM_STANDBY)
    {
        return;
    }
#ifdef SNMP_3_WANTED
    SnmpConvertOctetToString (&gSnmpEngineID, ai1EngineID);
#endif
#ifdef ISS_WANTED
    /* issnvram.txt is now transfered from Active to Standby.i
     * But the data structure is now holding old issnvram values. 
     * In that, get Bridge Mode, RmInterfaceName*/
    u4PrevBridgeMode = IssGetBridgeModeFromNvRam ();

    u4Len =
        ((STRLEN (IssGetRmIfNameFromNvRam ()) <
          (sizeof (ai1PrevRmDevName) -
           1)) ? STRLEN (IssGetRmIfNameFromNvRam ())
         : (sizeof (ai1PrevRmDevName) - 1));
    STRNCPY (ai1PrevRmDevName, IssGetRmIfNameFromNvRam (), u4Len);
    ai1PrevRmDevName[u4Len] = '\0';

   u4Len =
        ((STRLEN (IssGetSoftwareVersionFromNvRam ()) <
          (sizeof (ai1PrevSoftwareVersion) -
           1)) ? STRLEN (IssGetSoftwareVersionFromNvRam ())
         : (sizeof (ai1PrevSoftwareVersion) - 1));
    STRNCPY (ai1PrevSoftwareVersion, IssGetSoftwareVersionFromNvRam (), u4Len);
    ai1PrevSoftwareVersion[u4Len] = '\0';




    MEMSET (au1IssDefaultInterface , 0 , CFA_MAX_PORT_NAME_LENGTH);
	STRNCPY ((INT1 *) &(au1IssDefaultInterface),
			IssGetInterfaceFromNvRam (), 
			MEM_MAX_BYTES (STRLEN (IssGetInterfaceFromNvRam ()), 
				(CFA_MAX_PORT_NAME_LENGTH - 1)));

    /*Read the nvram file */
    IssReadSystemInfoFromNvRam ();

    /* Update ISS data structure with new values */
    IssInitSystemParams ();

    /* Set RmInterface in nvram file with old value */
    IssSetRmIfNameToNvRam ((INT1 *) &ai1PrevRmDevName);

    IssSetInterfaceToNvRam ((INT1*) au1IssDefaultInterface);

    IssSetSoftwareVersionToNvRam ((INT1 *) ai1PrevSoftwareVersion);  

    /*updating the snmp engine id and existing users with the
     *value read from the nvram file */
#ifdef SNMP_3_WANTED
    SnmpApiUpdateEngineId ();
    SnmpUpdateUserEntries (ai1EngineID);
#endif
    if ((gMSRIncrSaveFlag == ISS_FALSE) &&
        (gIssSysGroupInfo.IssIncrSaveFlag == ISS_TRUE))
    {
        /* Incrementtal save is enabled in the active node.
         * but disabled in the standby node. Allocate the 
         * RB Tree memory pool */

        if (MsrAllocateIncrPools () == MSR_FAILURE)
        {
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE | OS_RESOURCE_TRC,
                     "MsrProcessRmFileTransferComplete: Incremental save "
                     "memory pool creation failed. Incremental save flag is "
                     "disabled !!!\r\n");

            if (MSR_INCR_DATA_POOL_ID != -1)
            {
                MSR_FREE_INCR_BUDDY_POOL ((UINT1) MSR_INCR_DATA_POOL_ID);
                MSR_INCR_DATA_POOL_ID = -1;
            }
            if (MSR_INCR_OIDLIST_POOL_ID != -1)
            {
                MSR_FREE_INCR_BUDDY_POOL ((UINT1) MSR_INCR_OIDLIST_POOL_ID);
                MSR_INCR_OIDLIST_POOL_ID = -1;
            }

            gIssSysGroupInfo.IssIncrSaveFlag = ISS_FALSE;
        }

        /* Initialize the counter used for automatic save of configurations */
        gu4IncrLength = 0;

    }
    else if ((gMSRIncrSaveFlag == ISS_TRUE) &&
             (gIssSysGroupInfo.IssIncrSaveFlag == ISS_FALSE))
    {
        /* Incremental save is disabled in the active node but
         * enabled in the standby node. Clear the memory pool
         * for RB tree */

        if (MSR_INCR_DATA_POOL_ID != -1)
        {
            MSR_FREE_INCR_BUDDY_POOL ((UINT1) MSR_INCR_DATA_POOL_ID);
            MSR_INCR_DATA_POOL_ID = -1;
        }
        if (MSR_INCR_OIDLIST_POOL_ID != -1)
        {
            MSR_FREE_INCR_BUDDY_POOL ((UINT1) MSR_INCR_OIDLIST_POOL_ID);
            MSR_INCR_OIDLIST_POOL_ID = -1;
        }
    }

    if (gIssSysGroupInfo.IssIncrSaveFlag == ISS_TRUE)
    {
        gMSRIncrSaveFlag = ISS_TRUE;
    }
    else
    {
        gMSRIncrSaveFlag = ISS_FALSE;
    }

    /* Sync up iss.conf file with standby node if incremental save is disabled
     * Sync this file after getting Restore FileName from nvram.*/
    if (((gMSRIncrSaveFlag == ISS_FALSE) ||
         (gIssConfigSaveInfo.IssAutoConfigSave == ISS_FALSE)) &&
        (MsrSendMsgToRm (FLASH_CKSUM_REQ) == MSR_FAILURE))
    {
        MSR_TRC (MSR_RM_TRACE_TYPE, "MsrSendMsgToRm FLASH_CKSUM_REQ failed\n");
    }

    /* Check if Bridge Mode for Default Context has been changed. 
     * If so, do Protocol Restart in Standby so that protocols will use 
     * correct Bridge Mode from sync up nvram file */
    if (u4PrevBridgeMode != IssGetBridgeModeFromNvRam ())
    {
        L2IwfSetBridgeMode (0, IssGetBridgeModeFromNvRam ());

        /* Restart Modules where Bridge Mode is used */
#if defined(RSTP_WANTED) || defined(MSTP_WANTED) || defined(PVRST_WANTED)
        AstRestartModule ();
#endif

#ifdef VLAN_WANTED
        VlanRestartModule ();
#ifdef GARP_WANTED
        GarpRestartModule ();
#endif
#endif
        u1Restart = 1;
    }
#endif

    /* When a node moves to Standby state from Active State (RM link goes down 
     * and comes up again), then all the L2 Modules for needs to restored 
     * again with new static configurations */
    if (gu1MsrPrevNodeStatus == RM_ACTIVE)
    {
        /* Restart protocol modules to apply the static configurations
         * from configuration database
         */
        if (u1Restart == 1)
        {
            /* Restart all remaining modules */
#ifdef EOAM_WANTED
            EoamRestartModule ();
#endif
#ifdef LA_WANTED
            LaRestartModule ();
#endif
#ifdef ECFM_WANTED
            EcfmRestartModule ();
#endif
#ifdef ELMI_WANTED
            ElmRestartModule ();
#endif
#ifdef IGS_WANTED
            SnoopRestartModule ();
#endif
        }
        else
        {
            /* Restart all modules */
            MsrRestartProtocolModules ();
        }
        /* Clear the gpu1StrFlash, gu2FlashCksum & gu1RamState 
         * for erasing the previous configurations */
        gu2FlashCksum = 0;
        gu1RamState = MSR_RAM_EMPTY;
        gu4MsrOidCount = 0;

        /* The configurations changed will be applied, so we reset this flag
         */
        gi1ConfigChange = MSR_FALSE;
    }

    /* All the file transfer is complete and the configuration files are
     * stored to the database. Initiate the static bulk update process
     */

    MsrisRetoreinProgress = ISS_TRUE;
    gi4MibResStatus = MIB_RESTORE_IN_PROGRESS;
    if (SnmpApiGetSNMPUpdateDefault () == OSIX_TRUE)
    {
        SNMPV3DefaultDelete ();
    }

    /* During standby bootup vlan creation will be triggred   
     * from lrmain.c and all the ports will be added as the   
     * member ports. To avoid this, Default vlan is   
     * deleted here and through MSR Restoration
     */

    /* In case of OOB as default management interface, 
     * no need to delete the default interface */
    if (CFA_MGMT_PORT == FALSE) 
    {
        VlanDeleteDefaultVlan ();
    }
    /* During standby bootup Default L3 vlan will be created
     * by Cfa task on IP PRocess complete event
     * This should not happen and would have to be synced from
     * Active */
    if (CFA_MGMT_PORT == FALSE)
    {
        CfaDelDefL3VlanInterface (); 
    }

    /* During context creation vlaninit will be   
     * called and default vlan will be created if this flag   
     * is set to TRUE. This should not happen because   
     * default vlan creation for other context should   
     * happen only through nmh call.   
     * Hence this flag is set to FALSE
     */
    LrSetRestoreDefConfigFlag (OSIX_FALSE);

    /* If incremental save is enabled, clear the RB Tree */
    if (gMSRIncrSaveFlag == ISS_TRUE)
    {
        MSRDeleteAllRBNodesinRBTree ();
    }

    MsrRmSendStaticBulkRequest ();
}

/******************************************************************************
 * Function           : MsrRestartProtocolModules
 * Input(s)           : None.
 * Output(s)          : None.
 * Returns            : None.
 * Action             : Function used to restart all protocol modules to apply
 *                      static configurations.
 ******************************************************************************/
VOID
MsrRestartProtocolModules (VOID)
{
#ifdef EOAM_WANTED
    EoamRestartModule ();
#endif

#ifdef LA_WANTED
    LaRestartModule ();
#endif

#if defined(RSTP_WANTED) || defined(MSTP_WANTED) || defined(PVRST_WANTED)
    AstRestartModule ();
#endif

#ifdef VLAN_WANTED
    VlanRestartModule ();
#ifdef GARP_WANTED
    GarpRestartModule ();
#endif
#endif
#ifdef ELMI_WANTED
    ElmRestartModule ();
#endif
#ifdef IGS_WANTED
    SnoopRestartModule ();
#endif
}
#endif

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmInit                                        */
/*                                                                          */
/*    Description        : Registers the MSR module with RM.                */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : OSIX_FAILURE/OSIX_SUCCESS.                       */
/****************************************************************************/
INT4
MsrRmInit ()
{
    tRmRegParams        RmRegParams;

    gu1MsrPrevNodeStatus = RM_INIT;
    gu1MsrNodeStatus = RM_INIT;

    RmRegParams.u4EntId = RM_MSR_APP_ID;
    RmRegParams.pFnRcvPkt = MsrRcvPktFromRm;
    if (RmRegisterProtocols (&RmRegParams) == RM_FAILURE)
    {
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                 "MSR Registration with RM failed!!!\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmDeInit                                      */
/*                                                                          */
/*    Description        : This function deregister the MSR module with RM  */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
MsrRmDeInit (VOID)
{
    RmDeRegisterProtocols (RM_MSR_APP_ID);
    gu1MsrNodeStatus = RM_INIT;
    gu1MsrPrevNodeStatus = RM_INIT;
    RmSetStaticConfigStatus (RM_STATIC_CONFIG_RESTORED);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmGlobalInit                                  */
/*                                                                          */
/*    Description        : Creates a Q for interacting with RM module and   */
/*                         initialises the global structure                 */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : OSIX_FAILURE/OSIX_SUCCESS.                       */
/****************************************************************************/
INT4
MsrRmGlobalInit (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT2               u2Cntr = 0;

    /* Memset the global RM info */
    MEMSET (&gMSRRmGlobalInfo, 0, sizeof (tMSRRmGlobalInfo));
    /* Memset the last updated RB node info */
    MEMSET (&gMSRRmLastUpdatedNode, 0, sizeof (tMSRRmLastUpdatedNode));

    gMSRRmGlobalInfo.MsrQPoolId = MSRMemPoolIds[MAX_MSR_RM_QUEUE_MSG_SIZING_ID];
    gMSRRmGlobalInfo.MsrRmHdrPoolId =
        MSRMemPoolIds[MAX_MSR_RM_HDR_BLOCKS_SIZING_ID];
    gMSRRmGlobalInfo.MsrRmPktPoolId =
        MSRMemPoolIds[MAX_MSR_RM_PKT_BLOCKS_SIZING_ID];

    gu1RmStConfReq = RM_FALSE;
    gMSRRmGlobalInfo.i4SrvSockFd = -1;
    gMSRRmGlobalInfo.i4ConnFd = -1;
    gMSRRmGlobalInfo.i1IsTmrRunning = MSR_FALSE;

    /* Create a Q in MSR to listen on the msgs sent by RM */
    if ((i4RetVal == OSIX_SUCCESS) &&
        (OsixQueCrt ((UINT1 *) MSR_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                     MAX_MSR_RM_QUEUE_MSG, &gMSRRmGlobalInfo.MsrQId)
         != OSIX_SUCCESS))
    {
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE | OS_RESOURCE_TRC,
                 "MsrRmGlobalInit: Creation of Msr Q Failed !!!\r\n");
        i4RetVal = OSIX_FAILURE;
    }

    if ((i4RetVal == OSIX_SUCCESS) &&
        (TmrCreateTimerList ((const UINT1 *) MSR_TASK_NAME,
                             MSR_RM_TMR_EVENT, NULL,
                             &(gMSRRmGlobalInfo.TmrListId)) != TMR_SUCCESS))
    {
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE | OS_RESOURCE_TRC,
                 "Timer List creation failed !!!\r\n");
        OsixQueDel (gMSRRmGlobalInfo.MsrQId);
        gMSRRmGlobalInfo.MsrQId = 0;

        gMSRRmGlobalInfo.MsrQPoolId = 0;

        i4RetVal = OSIX_FAILURE;
    }

    if (i4RetVal == OSIX_SUCCESS)
    {
        MsrRmTmrInitTmrDesc ();
    }

    /* Initialize the index pools required */
    for (u2Cntr = 0; u2Cntr < MSR_MAX_INDICES; u2Cntr++)
    {
        MsrRmIndexPool1[u2Cntr].pIndex = &(MsrRmMultiPool1[u2Cntr]);
        MsrRmMultiPool1[u2Cntr].pOctetStrValue = &(MsrRmOctetPool1[u2Cntr]);
        MsrRmMultiPool1[u2Cntr].pOidValue = &(MsrRmOidPool1[u2Cntr]);
        MsrRmMultiPool1[u2Cntr].pOctetStrValue->pu1_OctetList
            = (UINT1 *) au1MsrRmData1[u2Cntr];
        MsrRmMultiPool1[u2Cntr].pOidValue->pu4_OidList
            = (UINT4 *) (VOID *) au1MsrRmData1[u2Cntr];

        MsrRmIndexPool2[u2Cntr].pIndex = &(MsrRmMultiPool2[u2Cntr]);
        MsrRmMultiPool2[u2Cntr].pOctetStrValue = &(MsrRmOctetPool2[u2Cntr]);
        MsrRmMultiPool2[u2Cntr].pOidValue = &(MsrRmOidPool2[u2Cntr]);
        MsrRmMultiPool2[u2Cntr].pOctetStrValue->pu1_OctetList
            = (UINT1 *) au1MsrRmData2[u2Cntr];
        MsrRmMultiPool2[u2Cntr].pOidValue->pu4_OidList
            = (UINT4 *) (VOID *) au1MsrRmData2[u2Cntr];
    }

    /* Initialize the oid pool */
    for (u2Cntr = MSR_RM_OID1; u2Cntr < MSR_RM_MAX_OID; u2Cntr++)
    {
        gMsrRmOidType[u2Cntr].pu4_OidList = &gau4MsrRmOidType[u2Cntr][0];
        gMsrRmOidType[u2Cntr].u4_Length = 0;
    }

    /* Initialize the multidata pool */
    for (u2Cntr = 0; u2Cntr < MSR_RM_MAX_DATA; u2Cntr++)
    {
        gMsrRmMultiData[u2Cntr].pOctetStrValue = &gMsrRmOctetString[u2Cntr];
        gMsrRmMultiData[u2Cntr].pOctetStrValue->pu1_OctetList =
            &gau1MsrRmOctetString[u2Cntr][0];
        gMsrRmMultiData[u2Cntr].pOctetStrValue->i4_Length = 0;
        gMsrRmMultiData[u2Cntr].pOidValue =
            &gMsrRmOidType[u2Cntr + MSR_RM_MULTIDATA_OID];
    }

    return i4RetVal;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmGlobalDeInit                                */
/*                                                                          */
/*    Description        : Deletes the Q for interacting with RM module     */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
MsrRmGlobalDeInit (VOID)
{
    if (gMSRRmGlobalInfo.MsrQId != 0)
    {
        OsixQueDel (gMSRRmGlobalInfo.MsrQId);
        gMSRRmGlobalInfo.MsrQId = 0;
    }
    if (gMSRRmGlobalInfo.MsrQPoolId != 0)
    {
        gMSRRmGlobalInfo.MsrQPoolId = 0;
    }
    if (gMSRRmGlobalInfo.TmrListId != 0)
    {
        TmrDeleteTimerList (gMSRRmGlobalInfo.TmrListId);
        gMSRRmGlobalInfo.TmrListId = 0;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmProcessStandbyDownEvent                     */
/*                                                                          */
/*    Description        : Called when standby down event is received       */
/*                         This function closes the socket and releases     */
/*                         the memory allocated for partial buffer and      */
/*                         last update RB Node information                  */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
MsrRmProcessStandbyDownEvent (VOID)
{
    MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmProcessStandbyDownEvent: "
             "Standby down event received\r\n");

    if (gMSRRmGlobalInfo.u1NumPeers != 0)
    {
        MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmProcessStandbyDownEvent: "
                 "Peer exists. Do not free the memory\r\n");
        return;
    }

    /* Event triggered when the standby is down
     * Close the connection
     */
    MsrRmChannelDeInit (gMSRRmGlobalInfo.i4ConnFd);
    gMSRRmGlobalInfo.i4ConnFd = -1;

    /* Clear the global partial buffer */
    if (gMSRRmGlobalInfo.pBuf != NULL)
    {
        MemReleaseMemBlock (gMSRRmGlobalInfo.BufPoolId,
                            (UINT1 *) (gMSRRmGlobalInfo.pBuf));
        gMSRRmGlobalInfo.pBuf = NULL;
        gMSRRmGlobalInfo.u4BytesSentOrRcvd = 0;
    }

    /* Clear the last update RB Tree node */
    gMSRRmLastUpdatedNode.MSRArrayIndex = 0;
    gMSRRmLastUpdatedNode.MSRRowStatusType = 0;
    free_oid (gMSRRmLastUpdatedNode.pOidValue);
    gMSRRmLastUpdatedNode.pOidValue = NULL;
    gMsRmTaskId = 0;

    if (gu1MsrNodeStatus == RM_ACTIVE)
    {
        RmSetStaticConfigStatus (RM_STATIC_CONFIG_RESTORED);
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmProcessNewConnEvent                         */
/*                                                                          */
/*    Description        : Called when a new connection event is received   */
/*                         by the active node. This function accepts a      */
/*                         new connection and establishes a channel         */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
MsrRmProcessNewConnEvent (VOID)
{
    /* Event triggered when a new connection arrives */
    gMSRRmGlobalInfo.i4ConnFd = MsrRmAcceptConnection
        (gMSRRmGlobalInfo.i4SrvSockFd);

    if (gMSRRmGlobalInfo.i4ConnFd == -1)
    {
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE |
                 ALL_FAILURE_TRC, "MsrRmProcessNewConnEvent: "
                 "New connection failed !!!\r\n");
    }
    else
    {
        RmSetStaticConfigStatus (RM_STATIC_CONFIG_IN_PROGRESS);

        /* Clear the global partial buffer */
        if (gMSRRmGlobalInfo.pBuf != NULL)
        {
            MemReleaseMemBlock (gMSRRmGlobalInfo.BufPoolId,
                                (UINT1 *) (gMSRRmGlobalInfo.pBuf));
            gMSRRmGlobalInfo.pBuf = NULL;
            gMSRRmGlobalInfo.u4BytesSentOrRcvd = 0;
        }
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MsrRmTmrHandleExpiry   
 *
 *    DESCRIPTION      : This function is called when the timer expiry event
 *                       is received
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
VOID
MsrRmTmrHandleExpiry (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = 0;

    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (gMSRRmGlobalInfo.TmrListId)) != NULL)
    {
        MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmTmrHandleExpiry: "
                 "Timer expired\r\n");

        u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;
        gMSRRmGlobalInfo.i1IsTmrRunning = MSR_FALSE;

        /* The timer function does not take any parameter. */
        (*(gMSRRmGlobalInfo.aTmrDesc[u1TimerId].TmrExpFn)) (NULL);
    }
}

/****************************************************************************
 *
 *    FUNCTION NAME    : MsrRmReConnectTmrExpiry
 *
 *    DESCRIPTION      : This function is called when the re-connect timer
 *                       expires. The function calls MsrRmSendStaticBulkRequest
 *                       which tries to re-connect to the server and sends
 *                       static bulk request on success.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
VOID
MsrRmReConnectTmrExpiry (VOID *pArg)
{
    UNUSED_PARAM (pArg);

    MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmReConnectTmrExpiry: "
             "Re-connect timer expired. Connecting to active node\r\n");

    if (gu1MsrNodeStatus == RM_STANDBY)
    {
        MsrRmSendStaticBulkRequest ();
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmSendStaticBulkRequest                       */
/*                                                                          */
/*    Description        : Standby node                                     */
/*                         This function is called when all the             */
/*                         configuration file are received from the active  */
/*                         and updated in the database.                     */
/*                                                                          */
/*                         This function creates a channel and send         */
/*                         static bulk request to the active node           */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
MsrRmSendStaticBulkRequest (VOID)
{
    UINT4               u4Addr = 0;    /* Server node address */
    INT4                i4WriteBytes = 0;    /* No of bytes written on socket */
    INT4                i4RetStatus = MSR_SUCCESS;
    /* Return value of send function */
    UINT1              *pu1Buf = NULL;    /* Static bulk request packet */

    /* Create a channel by connecting to the server socket in the
     * active node
     */
    u4Addr = RmGetActiveNodeId ();

    if (u4Addr != 0)
    {
        gMSRRmGlobalInfo.i4ConnFd = MsrRmClientInit (u4Addr);
    }

    if (gMSRRmGlobalInfo.i4ConnFd != -1)
    {
        MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmSendStaticBulkRequest: "
                 "Channel created between active MSR " "and standby MSR\r\n");
    }
    else
    {
        /* Channel creation failed. */
        /* Connect failure occurs when there is no server on the active
         * node.Start timer and try to reconnectd
         */
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                 "MsrRmSendStaticBulkRequest: "
                 "Channel creation between active MSR "
                 "and standby MSR failed. Trying to re-connect "
                 "after time out\r\n");

        if (TmrStart (gMSRRmGlobalInfo.TmrListId,
                      &(gMSRRmGlobalInfo.aTmrBlk[MSR_RM_RECONNECT_TIMER]),
                      MSR_RM_RECONNECT_TIMER, 0, MSR_RM_RECONNECT_INTERVAL)
            == TMR_FAILURE)
        {
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                     "MsrRmSendStaticBulkRequest: "
                     "Timer start failed. Channel creation between "
                     "active MSR and standby MSR failed !!!\r\n");
        }
        else
        {
            gMSRRmGlobalInfo.i1IsTmrRunning = MSR_TRUE;
        }
        return;
    }

    /* Create a bulk request packet and send to the standby node */
    pu1Buf = MsrRmConstructStaticBulkReqOrTail (RM_BULK_UPDT_REQ_MSG);

    if (pu1Buf == NULL)
    {
        MsrisRetoreinProgress = ISS_FALSE;
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE | OS_RESOURCE_TRC,
                 "MsrRmSendStaticBulkRequest: "
                 "Insufficient memory to construct static "
                 "bulk request !!!\r\n");
        return;
    }

    /* Send the packet to standby */
    i4RetStatus = MsrRmSendPkt (gMSRRmGlobalInfo.i4ConnFd,
                                pu1Buf, (UINT2) (RM_HDR_LENGTH + 1),
                                &i4WriteBytes);

    if (i4RetStatus == MSR_FAILURE)
    {
        /* Static bulk request is sent immediately after creating a channel
         * Hence, the write should not fail for lack of buffer space
         */
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                 "MsrRmSendStaticBulkRequest: "
                 "Static bulk request packet to active node failed !!!\r\n");
        MsrRmSockClose (gMSRRmGlobalInfo.i4ConnFd);
        gMSRRmGlobalInfo.i4ConnFd = -1;
    }
    else
    {
        MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmSendStaticBulkRequest: "
                 "Static bulk request packet sent to active node\r\n");
    }

    MemReleaseMemBlock (gMSRRmGlobalInfo.MsrRmHdrPoolId, (UINT1 *) pu1Buf);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmConstructStaticBulkReqOrTail                */
/*                                                                          */
/*    Description        : Active or Standby node                           */
/*                         This function constructs a static bulk request   */
/*                         packet                                           */
/*                                                                          */
/*                         Active mode calls this function to construct     */
/*                         static bulk tail message                         */
/*                                                                          */
/*                         Stanby mode calls this function to construct     */
/*                         static bulk request message                      */
/*                                                                          */
/*    Input(s)           : u1MessageType - Bulk request or Bulk Tail        */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : Pointer to the linear buffer                     */
/****************************************************************************/

UINT1              *
MsrRmConstructStaticBulkReqOrTail (UINT1 u1MessageType)
{
    tRmHdr             *pRmHdr = NULL;    /* RM header */
    UINT4               u4TotalLen = 0;    /* Total length of request packet */
    UINT2               u2ChkSum = 0;    /* packet check sum */
    UINT1              *pu1Buf = NULL;    /* pointer to the request packet */
    UINT1              *pu1CurPtr = NULL;    /* Pointer to traverse the packet */

    /* Total length = RM header length + 1 byte for message type */
    u4TotalLen = RM_HDR_LENGTH + 1;
    pu1Buf = MemAllocMemBlk (gMSRRmGlobalInfo.MsrRmHdrPoolId);

    if (pu1Buf == NULL)
    {
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE | OS_RESOURCE_TRC,
                 "MsrRmConstructStaticBulkReqOrTail: "
                 "Linear buffer memory allocation " "failed !!!\r\n");

        return NULL;
    }

    MEMSET (pu1Buf, 0, u4TotalLen);

    /* Copy the RM header to the buffer */
    pRmHdr = (tRmHdr *) (VOID *) pu1Buf;

    pRmHdr->u4TotLen = OSIX_HTONL (u4TotalLen);
    pRmHdr->u4SrcEntId = OSIX_HTONL (RM_MSR_APP_ID);
    pRmHdr->u4DestEntId = OSIX_HTONL (RM_MSR_APP_ID);

    /* Move the pointer and update the message type after RM header */
    pu1CurPtr = pu1Buf + RM_HDR_LENGTH;

    /* Add the message type */
    MSR_RM_ADD_BYTE (pu1CurPtr, &u1MessageType, sizeof (u1MessageType));

    /* Calculate the check sum for the packet and update the checksum field */
    u2ChkSum = UtlIpCSumLinBuf ((const INT1 *) pu1Buf, u4TotalLen);
    pRmHdr->u2Chksum = OSIX_HTONS (u2ChkSum);

    if (u1MessageType == RM_BULK_UPDT_REQ_MSG)
    {
        MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmConstructStaticBulkReqOrTail: "
                 "Static bulk request constructed\r\n");
    }
    else
    {
        MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmConstructStaticBulkReqOrTail: "
                 "Static bulk tail constructed\r\n");
    }

    return pu1Buf;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmConstructStaticBulkUpdate                   */
/*                                                                          */
/*    Description        : active node                                      */
/*                         This function constructs a static bulk update    */
/*                         packet                                           */
/*                                                                          */
/*    Input(s)           : pu1NodeCount - No of RB Nodes sent               */
/*                                                                          */
/*    Output(s)          : pu1NodeCount - No of RB Nodes after constructing */
/*                                        the buffer                        */
/*                         pu4PktLen    - Length of the constructed packet  */
/*                                                                          */
/*    Returns            : Pointer to the linear buffer                     */
/****************************************************************************/

UINT1              *
MsrRmConstructStaticBulkUpdate (UINT1 *pu1NodeCount, UINT4 *pu4PktLen)
{
    tRmHdr             *pRmHdr = NULL;    /* RM header */
    tMSRRbtree          CurRbNode;    /* RB node used for traversal */
    tMSRRbtree         *pMsrRbNode = NULL;    /* RB node used for traversal */
    UINT4               u4TotalLen = 0;    /* Total length of update packet */
    UINT4               u4Length = 0;    /* Length of RB info added */
    UINT2               u2ChkSum = 0;    /* packet check sum */
    UINT1              *pu1Buf = NULL;    /* pointer to the update packet */
    UINT1              *pu1CurPtr = NULL;    /* Pointer to traverse the packet */
    UINT1               u1MessageType;    /* Contains the message type */

    *pu4PktLen = 0;
    pu1Buf = MemAllocMemBlk (gMSRRmGlobalInfo.MsrRmPktPoolId);

    /* Allocate the oid in CurRbNode node */
    MEMSET (&CurRbNode, 0, sizeof (tMSRRbtree));
    CurRbNode.pOidValue = alloc_oid (MSR_MAX_OID_LEN);

    if ((pu1Buf == NULL) || (CurRbNode.pOidValue == NULL))
    {
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE | OS_RESOURCE_TRC,
                 "MsrRmConstructStaticBulkUpdate: "
                 "Linear buffer memory allocation " "failed !!!\r\n");
        if (pu1Buf != NULL)
        {
            MemReleaseMemBlock (gMSRRmGlobalInfo.MsrRmPktPoolId,
                                (UINT1 *) (pu1Buf));
        }
        if (CurRbNode.pOidValue != NULL)
        {
            free_oid (CurRbNode.pOidValue);
        }

        return NULL;
    }

    MEMSET (pu1Buf, 0, MSR_RM_MAX_PKT_SIZE);
    MEMSET (CurRbNode.pOidValue->pu4_OidList, 0, MSR_MAX_OID_LEN);
    CurRbNode.pOidValue->u4_Length = 0;

    /* Move the pointer and update the message type after RM header */
    pu1CurPtr = pu1Buf + RM_HDR_LENGTH;

    /* Add the message type */
    u1MessageType = (UINT1) RM_BULK_UPDATE_MSG;
    MSR_RM_ADD_BYTE (pu1CurPtr, &u1MessageType, sizeof (u1MessageType));
    u4TotalLen = RM_HDR_LENGTH + sizeof (UINT1);

    /* Scan the RB Tree. If the maximum nodes to be transmitted is reached
     * or the maximum size of packet is reached, return the pointer to the
     * buffer. The last added node will be updated in gMSRRmLastUpdatedNode
     */
    MSR_RM_UPDATE_INDEX ((&CurRbNode), (&gMSRRmLastUpdatedNode));

    while (((*pu1NodeCount) < MSR_RM_MAX_NODE_COUNT) &&
           ((pMsrRbNode = (tMSRRbtree *) RBTreeGetNext
             (gMSRRBTable, &CurRbNode,
              (tRBCompareFn) MSRCompareRBNodes)) != NULL))
    {
        u4Length = MsrRmAddRbNodeToBuf (pu1CurPtr, pMsrRbNode, u4TotalLen);

        /* u4Length contains the number length of the infomation added to
         * the buffer.
         * If u4Length is 0, then the maximum length is reached
         * in the buffer. Send the packet to the standby node,
         * If u4Length is not 0, update the length and current pointer,
         * increment the node count and scan the next node
         */
        if (u4Length != 0)
        {
            /* Update the current length of the buffer and the pointer
             * to the buffer. Update the CurRbNode to get the next RB Node
             */
            u4TotalLen += u4Length;
            pu1CurPtr += u4Length;

            MSR_RM_UPDATE_INDEX ((&CurRbNode), (pMsrRbNode));
            (*pu1NodeCount)++;
        }
        else
        {
            break;
        }
    }

    /* Update the global RB Tree node to be used for the
     * next update trigger. CurRbNode contains the last RB Node
     * added to the linear buffer
     */
    MSR_RM_UPDATE_INDEX ((&gMSRRmLastUpdatedNode), (&CurRbNode));

    /* Add the RM header and return the packet */
    pRmHdr = (tRmHdr *) (VOID *) pu1Buf;

    pRmHdr->u4TotLen = OSIX_HTONL (u4TotalLen);
    pRmHdr->u4SrcEntId = OSIX_HTONL (RM_MSR_APP_ID);
    pRmHdr->u4DestEntId = OSIX_HTONL (RM_MSR_APP_ID);

    /* Calculate the check sum */
    u2ChkSum = UtlIpCSumLinBuf ((const INT1 *) pu1Buf, u4TotalLen);
    pRmHdr->u2Chksum = OSIX_HTONS (u2ChkSum);

    *pu4PktLen = u4TotalLen;

    free_oid (CurRbNode.pOidValue);

    MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmConstructStaticBulkUpdate: "
             "Static bulk update constructed\r\n");

    return pu1Buf;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmSendStaticBulkUpdate                        */
/*                                                                          */
/*    Description        : active node                                      */
/*                         This function constructs a static bulk update    */
/*                         packet and send the buffer to the standby.       */
/*                         This function is called when                     */
/*                                                                          */
/*                         1. Node receives a static bulk request           */
/*                         2. Event received to send the next set of        */
/*                            static bulk updates                           */
/*                                                                          */
/*                         The static bulk updates are sent till the        */
/*                         threshold is reached. The threshold identifies   */
/*                         the maximum no of RB nodes that can be sent      */
/*                         at a stretch.                                    */
/*                                                                          */
/*                         If the write request succeeds, the an event is   */
/*                         sent to MSR for tiggerring the next set of       */
/*                         static bulk updates.                             */
/*                                                                          */
/*                         If the write is unsuccessful, the fd is added    */
/*                         to the select library and event is not posted    */
/*                         to MSR for next set of bulk updates              */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MsrRmSendStaticBulkUpdate (VOID)
{
    UINT4               u4Length = 0;    /* Length of packet */
    INT4                i4WriteBytes = 0;    /* No of bytes written on socket */
    INT4                i4RetStatus = MSR_SUCCESS;
    /* Return value of send function */
    UINT1              *pu1Buf = NULL;    /* Static bulk update */
    UINT1               u1NodeCount = 0;    /* No of RB Nodes added */
    INT1                i1SendEvent = MSR_TRUE;
    UINT4               u4Event = 0;
    UINT4               u4RemainingStatus = OSIX_TRUE;
    /* Flag to indicate whether
     * event can be triggered to
     * send the next set of static
     * bulk updates
     */

    if (gMSRRmGlobalInfo.u1NumPeers == 0)
    {
        /* Peer down event received before sending static bulk updates */
        MSR_TRC (MSR_RM_TRACE_TYPE,
                 "MsrRmSendStaticBulkUpdate: No peer exist. Static "
                 "bulk update process is not completed\r\n");
        i1SendEvent = MSR_FALSE;
    }

    while ((i1SendEvent == MSR_TRUE) && (u1NodeCount < MSR_RM_MAX_NODE_COUNT))
    {
        /* No of RB nodes added is less than the threshold.
         * Construct the static bulk update message
         */
        pu1Buf = MsrRmConstructStaticBulkUpdate (&u1NodeCount, &u4Length);

        if (pu1Buf == NULL)
        {
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE | ALL_FAILURE_TRC,
                     "MsrRmSendStaticBulkUpdate: Static "
                     "bulk update creation failed !!!\r\n");
            i1SendEvent = MSR_FALSE;
            break;
        }

        /* If the total length of the packet is equal to RM header + 1,
         * then, there are no more entries present in the RB Tree.
         * This is based on the assumption that a single RB Node does not
         * take more than max packet size.
         * Send a static bulk update tail message to standby.
         */
        if (u4Length == (RM_HDR_LENGTH + sizeof (UINT1)))
        {
            MSR_TRC (MSR_RM_TRACE_TYPE,
                     "MsrRmSendStaticBulkUpdate: All static bulk "
                     "updates sent to standby\r\n");
            MemReleaseMemBlock (gMSRRmGlobalInfo.MsrRmPktPoolId,
                                (UINT1 *) pu1Buf);
            MsrRmSendStaticBulkTail ();
            i1SendEvent = MSR_FALSE;
            break;
        }

        /* Send the packet to the standby */
        i4RetStatus = MsrRmSendPkt (gMSRRmGlobalInfo.i4ConnFd,
                                    pu1Buf, (UINT2) u4Length, &i4WriteBytes);

        if (i4WriteBytes <= 0)
        {
            if (i4RetStatus == MSR_FAILURE)
            {
                MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                         "MsrRmSendStaticBulkUpdate: "
                         "Static bulk update packet to standby "
                         "node failed !!!\r\n");
                MsrRmSockClose (gMSRRmGlobalInfo.i4ConnFd);
                gMSRRmGlobalInfo.i4ConnFd = -1;
                MemReleaseMemBlock (gMSRRmGlobalInfo.MsrRmPktPoolId,
                                    (UINT1 *) pu1Buf);
            }
            else
            {
                /* Buffer is full. No of bytes sent is 0 */
                if (MsrRmHandleFailureBuffer (pu1Buf, 0,
                                              gMSRRmGlobalInfo.MsrRmPktPoolId) == MSR_SUCCESS)
                {
                    u4RemainingStatus = OSIX_TRUE;
                    while (u4RemainingStatus == OSIX_TRUE)
                    {
                        if (OsixEvtRecv (gMsRmTaskId, MSR_RM_SEND_REM_UPDATE, OSIX_WAIT, &u4Event)
                            == OSIX_SUCCESS)
                        {
                            MsrRmSendRemainingUpdate (&u4RemainingStatus);
                        }
                    }
                }
            }

            i1SendEvent = MSR_FALSE;
        }
        else if (i4WriteBytes != (INT4) (u4Length))
        {
            MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmSendStaticBulkUpdate: "
                     "Partial Static bulk update packet "
                     "sent to standby\r\n");
            /* Handle unsuccessful write operation */
            if (MsrRmHandleFailureBuffer (pu1Buf, i4WriteBytes,
                                      gMSRRmGlobalInfo.MsrRmPktPoolId) == MSR_SUCCESS)
            {
                    u4RemainingStatus = OSIX_TRUE;
                    while (u4RemainingStatus == OSIX_TRUE)
                    {
                        if (OsixEvtRecv (gMsRmTaskId, MSR_RM_SEND_REM_UPDATE, OSIX_WAIT, &u4Event)
                            == OSIX_SUCCESS)
                        {
                            MsrRmSendRemainingUpdate (&u4RemainingStatus);
                        }

                    }
            }

            i1SendEvent = MSR_FALSE;
        }
        else
        {
            MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmSendStaticBulkUpdate: "
                     "Static bulk update packet sent to " "standby\r\n");
            /* Packet sent to the active node. Free the linear buffer */
            MemReleaseMemBlock (gMSRRmGlobalInfo.MsrRmPktPoolId,
                                (UINT1 *) pu1Buf);
        }
    }

    /* If i1SendEvent is true, then send an event to MSR */
    if (i1SendEvent == MSR_TRUE)
    {
        /* Send and event to MSR module to send the next set of
         * static bulk updates
         */
        OsixEvtSend (MsrId, MSR_RM_SUB_BULK_UPDATE);
        MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmSendStaticBulkUpdate: "
                 "sub static bulk update event sent " "to MSR\r\n");
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmAddRbNodeToBuf                              */
/*                                                                          */
/*    Description        : Active                                           */
/*                         This function adds the RB Node to the buffer     */
/*                         if the packet length does not exceed the         */
/*                         maximum packet size                              */
/*                                                                          */
/*    Input(s)           : pu1CurPtr  -    pointer to the where the RB info */
/*                                         needs to be added                */
/*                         pMsrRbNode -    tMSRRbtree node                  */
/*                         u4TotalLen -    Current packet length            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : Length of RB Node information added              */
/****************************************************************************/

UINT4
MsrRmAddRbNodeToBuf (UINT1 *pu1CurPtr, tMSRRbtree * pMsrRbNode,
                     UINT4 u4TotalLen)
{
    UINT4               u4NodeLen = 0;    /* Length of the RB Node to be added
                                         * to the buffer
                                         */
    UINT4               u4Length = 0;    /* Length to be added. This is equal
                                         * to u4NodeLen + size of length field
                                         */
    UINT4               u4Data = 0;    /* 4-byte data */
    tSNMP_OID_TYPE     *pOid = NULL;

    pOid = &gMsrRmOidType[MSR_RM_OID1];
    MSR_CLEAR_OID (pOid);

    SNMPRevertEOID (pMsrRbNode->pOidValue, pOid);

    /* The following information should be added in the buffer
     * 1. Length field (4 bytes)
     * 2. Array Index (4 bytes)
     * 3. RowStatustype (4 bytes)
     * 4. OID length (4 bytes)
     * 5. OID (OID length bytes)
     * 6. Datasize (4 bytes)
     * 7. Data (Datasize bytes)
     * 8. DataSave Flag (1 byte)
     */
    u4NodeLen = sizeof (pMsrRbNode->MSRArrayIndex) +
        sizeof (pMsrRbNode->MSRRowStatusType) +
        sizeof (pOid->u4_Length) + (pOid->u4_Length * sizeof (UINT4)) +
        sizeof (pMsrRbNode->u4pDataSize) + pMsrRbNode->u4pDataSize +
        sizeof (pMsrRbNode->u1IsDataToBeSaved);

    u4Length = u4NodeLen + sizeof (u4NodeLen);

    if ((u4TotalLen + u4Length) > MSR_RM_MAX_PKT_SIZE)
    {
        /* Packet size exceeded */
        u4Length = 0;
    }
    else
    {
        /* Add the length field */
        u4Data = OSIX_HTONL (u4NodeLen);
        MSR_RM_ADD_BYTE (pu1CurPtr, &u4Data, sizeof (u4NodeLen));

        /* Add the array index */
        u4Data = OSIX_HTONL (pMsrRbNode->MSRArrayIndex);
        MSR_RM_ADD_BYTE (pu1CurPtr, &u4Data, sizeof (u4Data));

        /* Add the row status type */
        u4Data = OSIX_HTONL (pMsrRbNode->MSRRowStatusType);
        MSR_RM_ADD_BYTE (pu1CurPtr, &u4Data, sizeof (u4Data));

        /* Add the OID length */
        u4Data = OSIX_HTONL (pOid->u4_Length);
        MSR_RM_ADD_BYTE (pu1CurPtr, &u4Data, sizeof (u4Data));

        /* Add the OID */
        MSR_RM_ADD_BYTE (pu1CurPtr, pOid->pu4_OidList,
                         (pOid->u4_Length * sizeof (UINT4)));

        /* Add the size of data */
        u4Data = OSIX_HTONL (pMsrRbNode->u4pDataSize);
        MSR_RM_ADD_BYTE (pu1CurPtr, &u4Data, sizeof (u4Data));

        /* Add the data */
        MSR_RM_ADD_BYTE (pu1CurPtr, pMsrRbNode->pData, pMsrRbNode->u4pDataSize);

        /* Add the data save flag */
        MSR_RM_ADD_BYTE (pu1CurPtr, &(pMsrRbNode->u1IsDataToBeSaved),
                         sizeof (pMsrRbNode->u1IsDataToBeSaved));
    }

    return (u4Length);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmSendStaticBulkTail                          */
/*                                                                          */
/*    Description        : Active                                           */
/*                         This function sends static bulk tail message     */
/*                         to the standby node after sending all            */
/*                         configurations                                   */
/*                                                                          */
/*                         After sending static bulk tail message,          */
/*                         the global flag is updated in the RM module      */
/*                         to allow dynamic sync up                         */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MsrRmSendStaticBulkTail (VOID)
{
    INT4                i4WriteBytes = 0;    /* No of bytes written on socket */
    INT4                i4RetStatus = MSR_SUCCESS;
    /* Return value of send function */
    UINT1              *pu1Buf = NULL;    /* Static bulk tail packet */
    UINT4               u4Event = 0;
    UINT4               u4RemainingStatus = OSIX_TRUE;

    /* Create a bulk tail packet and send to the standby node */
    pu1Buf = MsrRmConstructStaticBulkReqOrTail (RM_BULK_UPDT_TAIL_MSG);

    if (pu1Buf == NULL)
    {
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE | OS_RESOURCE_TRC,
                 "MsrRmSendStaticBulkTail: "
                 "Insufficient memory to construct static "
                 "bulk Tail message !!!\r\n");
        return;
    }

    /* Send the packet to standby */
    i4RetStatus = MsrRmSendPkt (gMSRRmGlobalInfo.i4ConnFd,
                                pu1Buf, (UINT2) (RM_HDR_LENGTH + 1),
                                &i4WriteBytes);

    if (i4WriteBytes <= 0)
    {
        if (i4RetStatus == MSR_FAILURE)
        {
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                     "MsrRmSendStaticBulkTail: "
                     "Static bulk tail message to standby node "
                     "failed !!!\r\n");
            MsrRmSockClose (gMSRRmGlobalInfo.i4ConnFd);
            gMSRRmGlobalInfo.i4ConnFd = -1;
            MemReleaseMemBlock (gMSRRmGlobalInfo.MsrRmHdrPoolId,
                                (UINT1 *) pu1Buf);
        }
        else
        {
            /* Buffer is full */
            if (MsrRmHandleFailureBuffer (pu1Buf, 0,
                                      gMSRRmGlobalInfo.MsrRmHdrPoolId) == MSR_SUCCESS)
            {
                u4RemainingStatus = OSIX_TRUE;
                while (u4RemainingStatus == OSIX_TRUE)
                {
                    if (OsixEvtRecv (gMsRmTaskId, MSR_RM_SEND_REM_UPDATE, OSIX_WAIT, &u4Event)
                        == OSIX_SUCCESS)
                    {
                        MsrRmSendRemainingUpdate (&u4RemainingStatus);
                    }
                }
            }

        }
    }
    else if (i4WriteBytes != (RM_HDR_LENGTH + 1))
    {
        MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmSendStaticBulkTail: "
                 "Partial Static bulk tail message " "sent to standby\r\n");
        /* Handle unsuccessful write operation */
        if (MsrRmHandleFailureBuffer (pu1Buf, i4WriteBytes,
                                  gMSRRmGlobalInfo.MsrRmHdrPoolId) == MSR_SUCCESS)
        {
            u4RemainingStatus = OSIX_TRUE;
            while (u4RemainingStatus == OSIX_TRUE)
            {
                if (OsixEvtRecv (gMsRmTaskId, MSR_RM_SEND_REM_UPDATE, OSIX_WAIT, &u4Event)
                    == OSIX_SUCCESS)
                {
                    MsrRmSendRemainingUpdate (&u4RemainingStatus);
                }
            }
        }

    }
    else
    {
        MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmSendStaticBulkTail: "
                 "Static bulk tail message " "sent to standby\r\n");
        /* Packet sent to the standby node. Free the linear buffer */
        MemReleaseMemBlock (gMSRRmGlobalInfo.MsrRmHdrPoolId, (UINT1 *) pu1Buf);

        /* Free the memory allocated for last added RB Tree node */
        free_oid (gMSRRmLastUpdatedNode.pOidValue);
        gMSRRmLastUpdatedNode.pOidValue = NULL;

        /* Close the socket */
        MsrRmSelRemoveFd (gMSRRmGlobalInfo.i4ConnFd, MSR_TRUE);
        MsrRmSockClose (gMSRRmGlobalInfo.i4ConnFd);
        gMSRRmGlobalInfo.i4ConnFd = -1;
        RmSetStaticConfigStatus (RM_STATIC_CONFIG_RESTORED);
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmProcessIncomingPacket                       */
/*                                                                          */
/*    Description        : Called in both Active and Standby node.          */
/*                         This function is called when the MSR receives    */
/*                         MSR_RM_PACKET_ARRIVAL event.                     */
/*                                                                          */
/*                         This function receives the packets from the      */
/*                         socket, handles partial data handling,           */
/*                         validates the checksum and calls a function      */
/*                         to process each packet                           */
/*                                                                          */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MsrRmProcessIncomingPacket (VOID)
{
    tRmHdr             *pRmHdr = NULL;    /* RM header */
    UINT4               u4PktLen = 0;    /* Packet length from RM header */
    UINT4               u4BytesToRead = 0;
    /* No of pay load length to be read */
    INT4                i4ReadBytes = 0;    /* No of bytes read */
    INT4                i4RetStatus = MSR_SUCCESS;
    /* Return value of receive function */
    UINT2               u2ChkSum = 0;    /* No of bytes read */
    UINT1              *pu1Buf = NULL;    /* Pointer to the received buffer */
    UINT1              *pu1CurPtr = NULL;
    /* Pointer to traverse the packet */
    INT1                i1Flag = MSR_TRUE;
    /* Flag to indicate whether all the
     * fd should be added to select
     */
    pu1Buf = MemAllocMemBlk (gMSRRmGlobalInfo.MsrRmPktPoolId);
    if (pu1Buf == NULL)
    {
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                 "MsrRmProcessIncomingPacket: "
                 "Unable to allocate memory for buffer !!!\r\n");
        return;
    }
    MEMSET (pu1Buf, 0, MSR_RM_MAX_PKT_SIZE);

    if (gMSRRmGlobalInfo.pBuf != NULL)
    {
        /* There is some partial data availble in the received buffer */
        if (MsrRmRcvRemainingUpdate (pu1Buf) == MSR_FALSE)
        {
            /* If the global flag is NULL, there is a recv error */
            if (gMSRRmGlobalInfo.pBuf == NULL)
            {
                gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                         "MsrRmProcessIncomingPacket: Recv failed !!!\r\n");

                /* Free the buffer */
                MemReleaseMemBlock (gMSRRmGlobalInfo.MsrRmPktPoolId,
                                    (UINT1 *) (pu1Buf));
                return;
            }

            /* Entire packet is not received. Set the flag and wait for next
             * packet
             */
            i1Flag = MSR_FALSE;
        }
    }

    /* Continuosly read the the packet from the socket. Since the exact length
     * of the packet is not known, read RM_HDR_LENGTH from the socket and
     * based on the length field, read the rest of the packet. recv call
     * returns MSR_FAILURE if there is an error while receiving data
     */
    while ((i1Flag == MSR_TRUE) && (gMSRRmGlobalInfo.i4ConnFd != -1) &&
           ((i4RetStatus = MsrRmRcvPkt (gMSRRmGlobalInfo.i4ConnFd,
                                        pu1Buf, (UINT2) RM_HDR_LENGTH,
                                        &i4ReadBytes)) == MSR_SUCCESS))
    {
        if (i4ReadBytes <= 0)
        {
            /* This occurrs when all the data are read. MsrRmRcvPkt returns
             * MSR_SUCCESS in this case
             */
            break;
        }

        pRmHdr = (tRmHdr *) (VOID *) pu1Buf;

        if (i4ReadBytes != (INT4) RM_HDR_LENGTH)
        {
            /* Did not get the RM header itself */
            MsrRmHandlePartialRcvPkt (pu1Buf, i4ReadBytes);
            i1Flag = MSR_FALSE;
            break;
        }

        /* Get the total length of the packet from the RM header
         * Length of data to be read from packet =
         * Total length - RM header length
         */
        u4PktLen = OSIX_NTOHL (pRmHdr->u4TotLen);
        pu1CurPtr = pu1Buf + RM_HDR_LENGTH;
        u4BytesToRead = u4PktLen - RM_HDR_LENGTH;

        if ((INT4) u4BytesToRead > 0)
        {
            i4RetStatus = MsrRmRcvPkt (gMSRRmGlobalInfo.i4ConnFd,
                                       pu1CurPtr,
                                       (UINT2) u4BytesToRead, &i4ReadBytes);

            if (i4ReadBytes <= 0)
            {
                if (i4RetStatus == MSR_SUCCESS)
                {
                    /* All data are read. The last packet read contains
                     * only the RM header
                     */
                    MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmProcessIncomingPacket: "
                             "Pkt received with only RM header !!!\r\n");
                    MsrRmHandlePartialRcvPkt (pu1Buf, RM_HDR_LENGTH);
                }
                else
                {
                    gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                    MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmProcessIncomingPacket: "
                             "Recv failed !!!\r\n");
                }
                break;
            }

            if (i4ReadBytes != (INT4) u4BytesToRead)
            {
                /* Partial pay load data received */
                MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmProcessIncomingPacket: "
                         "Pkt RM header with partial payload received !!!\r\n");
                MsrRmHandlePartialRcvPkt (pu1Buf,
                                          (i4ReadBytes + (INT4) RM_HDR_LENGTH));
                i1Flag = MSR_FALSE;
                break;
            }

            /* pu1Buf contains the completed packet. Verify the checksum
             * of the received packet. If checksum failed, discard the packet
             * and read the next packet. If checksum succeeds, process the
             * packet
             */
            u2ChkSum = UtlIpCSumLinBuf ((const INT1 *) pu1Buf, u4PktLen);

            if (u2ChkSum != 0)
            {
                /* Discard the packet and read the next packet */
                MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE | ALL_FAILURE_TRC,
                         "MsrRmProcessIncomingPacket: "
                         "Check sum verification failed " "for packet !!!\r\n");
            }
            else
            {
                MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmProcessIncomingPacket: "
                         "Static bulk message received\r\n");
                MsrRmProcessPacket (pu1Buf, u4PktLen);
            }
        }
        else
        {
            MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmProcessIncomingPacket: "
                     "Received RM header without " "payload !!!\r\n");
        }
        MEMSET (pu1Buf, 0, MSR_RM_MAX_PKT_SIZE);
    }

    if (i1Flag == MSR_TRUE)
    {
        if (gMSRRmGlobalInfo.i4ConnFd == -1)
        {
            MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmProcessIncomingPacket: "
                     "Static Bulk update transfer is completed\r\n");
        }
        else if (i4RetStatus == MSR_SUCCESS)
        {
            /* Add the socket fd to the select library */
            MsrRmSelAddFd (gMSRRmGlobalInfo.i4ConnFd, MsrRmPktArrival,
                           MSR_TRUE);

            /* All data are read. */
            MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmProcessIncomingPacket: "
                     "All packets are processed\r\n");
        }
        else
        {
            gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                     "MsrRmProcessIncomingPacket: Recv failed !!!\r\n");
            MsrRmSockClose (gMSRRmGlobalInfo.i4ConnFd);
            gMSRRmGlobalInfo.i4ConnFd = -1;
        }
    }
    else
    {
        /* Add the socket fd to the select library */
        MsrRmSelAddFd (gMSRRmGlobalInfo.i4ConnFd, MsrRmPktArrival, MSR_TRUE);
    }

    /* Free the buffer */
    MemReleaseMemBlock (gMSRRmGlobalInfo.MsrRmPktPoolId, (UINT1 *) (pu1Buf));
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmProcessPacket                               */
/*                                                                          */
/*    Description        : Called in both Active and Standby node.          */
/*                         This function is called from                     */
/*                         MsrRmProcessIncomingPacket                       */
/*                                                                          */
/*                         The active node, on receiving static bulk request*/
/*                         initiates static bulk updates                    */
/*                                                                          */
/*                         The standby node, on receiving static bulk       */
/*                         update, scans the buffer and stores in the DB    */
/*                                                                          */
/*                         The standby node, on receiving static bulk       */
/*                         tail message, creates the configuration file     */
/*                         if incremental save is on and auto save is on    */
/*                         and sends an event to RM for dynamic update      */
/*                         process                                          */
/*                                                                          */
/*    Input(s)           : pu1Buf    -   pointer to the packet              */
/*                         u4PktLen  -   packet length                      */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MsrRmProcessPacket (UINT1 *pu1Buf, UINT4 u4PktLen)
{
    UINT1              *pu1CurPtr = NULL;    /* Pointer to traverse the packet */
    UINT1               u1MessageType = 0;    /* Message type received */
    INT1                i1Flag = MSR_TRUE;    /* Flag to indicate whether
                                             * the packet is valid or not */

    MSR_TRC_ARG1 (MSR_RM_TRACE_TYPE, "MsrRmProcessPacket: Received a packet"
                  "with length %d\n", u4PktLen);
    /* Move the pointer to the pay load data */
    pu1CurPtr = pu1Buf + RM_HDR_LENGTH;

    /* Decrement the packet length to the length of RM header */
    u4PktLen -= RM_HDR_LENGTH;

    /* The first byte in the pay load is the type of message */
    MSR_RM_GET_BYTE (&u1MessageType, pu1CurPtr, sizeof (u1MessageType));
    u4PktLen -= sizeof (u1MessageType);

    if ((u1MessageType != RM_BULK_UPDATE_MSG) &&
        (u1MessageType != RM_BULK_UPDT_TAIL_MSG) &&
        (u1MessageType != RM_BULK_UPDT_REQ_MSG))
    {
        MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmProcessPacket: Pkt discarded "
                 "Unknown packet !!!\r\n");
        i1Flag = MSR_FALSE;
    }

    if ((i1Flag == MSR_TRUE) && (gu1MsrNodeStatus == RM_ACTIVE))
    {
        /* If the node status is active, the received packet should be
         * static bulk request
         */
        if (u1MessageType == RM_BULK_UPDATE_MSG)
        {
            MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmProcessPacket: Pkt discarded "
                     "Active node received static bulk "
                     "update message !!!\r\n");
        }
        else if (u1MessageType == RM_BULK_UPDT_TAIL_MSG)
        {
            MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmProcessPacket: Pkt discarded "
                     "Active node received static bulk "
                     "tail message !!!\r\n");
        }
        else
        {
            MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmProcessPacket: "
                     "Active node processing static bulk "
                     "request message\r\n");

            MsrRmNvramPreSyncUp ();
            MsrRmInitiateStaticBulkUpdate ();
        }
    }
    else if ((i1Flag == MSR_TRUE) && (gu1MsrNodeStatus == RM_STANDBY))
    {
        /* If the node is standby, the received packet should be
         * static bulk update ot static bulk tail
         */
        if (u1MessageType == RM_BULK_UPDATE_MSG)
        {
            MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmProcessPacket: "
                     "Standby node processing static bulk "
                     "update message\r\n");
            MsrRmProcessStaticBulkUpdate (pu1CurPtr, u4PktLen);
        }
        else if (u1MessageType == RM_BULK_UPDT_TAIL_MSG)
        {
            MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmProcessPacket: "
                     "Standby node processing static bulk tail message\r\n");

            /* Close the socket */
            MsrRmSockClose (gMSRRmGlobalInfo.i4ConnFd);
            gMSRRmGlobalInfo.i4ConnFd = -1;

            /* Since the restore flag is set to false before static bulk update
             * process for restoring the default vlan through nmh call, this
             * must be set to true to allow creation of vlan
             */
            LrSetRestoreDefConfigFlag (OSIX_TRUE);

            MsrRmSendRestorationComplete ();
        }
        else
        {
            MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmProcessPacket: Pkt discarded "
                     "Standby node received static bulk "
                     "request message !!!\r\n");
        }
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmInitiateStaticBulkUpdate                    */
/*                                                                          */
/*    Description        : Active node                                      */
/*                         This function is called from MsrRmProcessPacket  */
/*                         when it receives static bulk request message.    */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MsrRmInitiateStaticBulkUpdate (VOID)
{
    INT1                i1Flag = MSR_TRUE;    /* Flag to indicate whether
                                             * the process is successfully
                                             * completed or not
                                             */
    CHR1                ac1Time[24];

    /* Active node has received static bulk request. Allocate the memory
     * to store the last RB Tree node added to the buffer and memset to 0
     * The node will be freed after sending static bulk tail message or after
     * getting the GO_STANDBY event
     */
    UtlGetTimeStr (ac1Time);
    MSR_TRC_ARG1 (MSR_TRACE_TYPE, "%s Static bulk update "
                  "initiated\r\n", ac1Time);
    gMSRRmLastUpdatedNode.pOidValue = alloc_oid (MSR_MAX_OID_LEN);
    if (gMSRRmLastUpdatedNode.pOidValue == NULL)
    {
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE | OS_RESOURCE_TRC,
                 "MsrRmInitiateStaticBulkUpdate : "
                 "Unable to allocate OID for MSR redundancy !!!\n");
        return;
    }

    MEMSET (gMSRRmLastUpdatedNode.pOidValue->pu4_OidList, 0, MSR_MAX_OID_LEN);
    gMSRRmLastUpdatedNode.pOidValue->u4_Length = 0;

    /* When active node receives a static bulk request,
     * 1. RB Tree is not populated if incremental save is enabled
     *    and MsrisSaveComplete is set set to false. The flag
     *    is then set to true.
     * 2. RB Tree is already populated if incremental save is
     *    enabled and MsrisSaveComplete is set to true
     */
    if ((gMSRIncrSaveFlag == ISS_TRUE) && (MsrisSaveComplete == ISS_FALSE))
    {
        /* Set the gu1RmStConfReq flag to true to indicate
         * that the RB Tree should be populated but should
         * not be stored in iss.conf. If this flag is set,
         * then RB Tree will be populated when incremental
         * save is disabled instead of storing to iss.conf
         */
        gu1RmStConfReq = RM_TRUE;

        MSRDeleteAllRBNodesinRBTree ();
        if (MsrSaveMIB () == MSR_FALSE)
        {
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE | ALL_FAILURE_TRC,
                     "MsrRmInitiateStaticBulkUpdate: "
                     "RB Tree population failed !!!\r\n");

            i1Flag = MSR_FALSE;
        }
        gu1RmStConfReq = RM_FALSE;
    }
    else if (gMSRIncrSaveFlag == ISS_FALSE)
    {
        /* 
         * When incremental save is disabled, obtain the configurations from
         * the protocol database and directly send it to the standby node
         */
        MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmInitiateStaticBulkUpdate: "
                 "Active node send static bulk update request by reading"
                 " configurations\r\n");
        MsrRmStartStaticBulkUpdate ();
        i1Flag = MSR_FALSE;
    }

    if (i1Flag == MSR_TRUE)
    {
        MSR_TRC (MSR_RM_TRACE_TYPE,
                 "MsrRmInitiateStaticBulkUpdate: " "RB Tree is populated\r\n");

        /* Initiate static bulk update transmission to the standby node */
        MsrRmSendStaticBulkUpdate ();
    }
    UtlGetTimeStr (ac1Time);
    MSR_TRC_ARG1 (MSR_TRACE_TYPE, "%s Static bulk update completed\r\n",
                  ac1Time);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmSendRestorationComplete                     */
/*                                                                          */
/*    Description        : Standby node                                     */
/*                         This function is called from MsrRmProcessPacket  */
/*                         when it receives static bulk tail message or     */
/*                         when the node state changes from from standby    */
/*                         to active                                        */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MsrRmSendRestorationComplete (VOID)
{
    INT1                i1Flag = MSR_FAILURE;    /* Flag to indicate whether
                                                 * event is sent to MSR or not
                                                 */
    UINT4               u4Len = 0;

    /* Send the syslog message */
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4MsrSysLogId,
                  "Restored configuration from flash successfully!"));

    /* All the static configurations are synchronized with the standby node
     * Send an indication to the RM module that the static configuration
     * is completed.
     * If the incremental save is enabled and auto save is on, send a
     * self event to MSR to save the configurations to iss.conf
     */

    MsrisRetoreinProgress = ISS_FALSE;
    gi4MibResStatus = LAST_MIB_RESTORE_SUCCESSFUL;
    MsrSendRestorationCompleteToProtocols ();

    if (gMSRIncrSaveFlag == ISS_TRUE)
    {
        /* If incremental save is enabled, the RB Tree contains all the
         * static configurations. Set the MsrisSaveComplete flag to true
         */
        MsrisSaveComplete = ISS_TRUE;
    }

    if ((gMSRIncrSaveFlag == ISS_TRUE) &&
        (gIssConfigSaveInfo.IssAutoConfigSave == ISS_TRUE))
    {
        /* If incremental save is enabled and auto save is on, then
         * iss.conf should be available for the configured objects.
         * Trigger a self event to MSR to initiate the save process
         */

        /* The save can be flash or in startupConfig */
        if (gIssConfigSaveInfo.IssConfigSaveOption == ISS_CONFIG_FLASH_SAVE)
        {
            if (STRLEN (gIssConfigSaveInfo.au1IssConfigSaveFileName))
            {
                MSR_MEMSET (gMsrData.au1FileName, 0,
                            sizeof (gMsrData.au1FileName));
                STRNCPY (gMsrData.au1FileName,
                         gIssConfigSaveInfo.au1IssConfigSaveFileName,
                         sizeof (gMsrData.au1FileName) - 1);
            }
            else
            {
                STRCPY (gMsrData.au1FileName, ISS_CONFIG_FILE);
            }
            i1Flag = MSR_SUCCESS;
        }
        else if (gIssConfigSaveInfo.IssConfigSaveOption ==
                 ISS_CONFIG_STARTUP_SAVE)
        {
            u4Len =
                ((STRLEN (IssGetRestoreFileNameFromNvRam ()) <
                  (sizeof (gMsrData.au1FileName) -
                   1)) ? STRLEN (IssGetRestoreFileNameFromNvRam ())
                 : (sizeof (gMsrData.au1FileName) - 1));
            STRNCPY (gMsrData.au1FileName, IssGetRestoreFileNameFromNvRam (),
                     u4Len);
            gMsrData.au1FileName[u4Len] = '\0';

            i1Flag = MSR_SUCCESS;
        }

        if ((i1Flag == MSR_SUCCESS) &&
            (MSRUpdateISSConfFile (FLASH_ALL_CONFIG,
                                   (const char *) gMsrData.
                                   au1FileName) != MSR_SUCCESS))
        {
            gi4MibSaveStatus = LAST_MIB_SAVE_FAILED;
            MsrInitSaveFlag ();
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE | OS_RESOURCE_TRC,
                     "MsrRmSendRestorationComplete: Save failed !!!\r\n");
        }
        else if (i1Flag == MSR_SUCCESS)
        {
            gi4MibSaveStatus = LAST_MIB_SAVE_SUCCESSFUL;
#ifdef ISS_WANTED
            /* In incr on and auto save on, restore flag and restore option
             * should be set
             */
            IssSetRestoreOptionToNvRam (ISS_CONFIG_RESTORE);
            IssSetRestoreFlagToNvRam (SET_FLAG);
            IssSetSaveFlagToNvRam (SET_FLAG);
#endif
            MSR_TRC (MSR_RM_TRACE_TYPE,
                     "MsrRmSendRestorationComplete: Save Successful\r\n");
        }
    }
    MSR_TRC (MSR_RM_TRACE_TYPE,
             "MsrRmSendRestorationComplete: "
             "Static configurations restoration is completed\r\n");
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmProcessStaticBulkUpdate                     */
/*                                                                          */
/*    Description        : Standby node                                     */
/*                         This function is called from MsrRmProcessPacket  */
/*                         when it receives static bulk update message      */
/*                                                                          */
/*    Input(s)           : pu1Buf    -   pointer to the buffer              */
/*                         u4PktLen  -   Length of payload after message    */
/*                                       type                               */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MsrRmProcessStaticBulkUpdate (UINT1 *pu1Buf, UINT4 u4PktLen)
{
    tMSRRbtree         *pMSRRbNode = NULL;    /* RB node to be inserted in
                                             * gMSRRBTable
                                             */
    tSnmpIndex         *pCurIndex = MsrIndexPool1;
    tSNMP_MULTI_DATA_TYPE *pData = NULL;    /* Structure to store the data */
    tSNMP_OID_TYPE     *pOid = NULL;    /* OID of the MIB object */
    UINT4               u4Length = 0;    /* Length of RB node info */
    UINT4               u4Error = 0;    /* Error set during set operation */
    UINT4               u4Len = 0;    /* Length of the MIB data */
    INT4                i4RetVal = MSR_SUCCESS;
    UINT2               u2Type = 0;    /* Contains the type of MIB object
                                     */
    UINT1              *pu1DataPtr = NULL;    /* Pointer to traverse the data
                                             * in RB Node
                                             */

    MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmProcessStaticBulkUpdate: "
             "Processing Static bulk update message\r\n");

    /* Scan the complete packet */
    while (u4PktLen > 0)
    {
        /* Get the length of the next RB Node */
        MSR_RM_GET_BYTE (&u4Length, pu1Buf, sizeof (u4Length));
        u4Length = OSIX_NTOHL (u4Length);
        u4PktLen -= sizeof (u4Length);

        /* Populate the RB Tree node */
        pMSRRbNode = NULL;
        pMSRRbNode = MsrRmGetRBNodeFromBuf (pu1Buf);

        if (pMSRRbNode == NULL)
        {
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE | OS_RESOURCE_TRC,
                     "MsrRmProcessStaticBulkUpdate: "
                     "RB Tree node allocation failed !!!\r\n");
            i4RetVal = MSR_FAILURE;
            break;
        }

        /* If the incremental save is on, add the node to the RB Tree */
        if ((gMSRIncrSaveFlag == ISS_TRUE) &&
            (RBTreeAdd (gMSRRBTable, pMSRRbNode) == RB_FAILURE))
        {
            MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmProcessStaticBulkUpdate: "
                     "RB Tree addition failed !!!\r\n");
        }

        pOid = pMSRRbNode->pOidValue;
        pu1DataPtr = (UINT1 *) pMSRRbNode->pData;

        /* Get the multidata data type of the object to be restored */
        u2Type = (UINT2) ATOI (pu1DataPtr);
        pu1DataPtr += 3;
        if (u2Type == OCTETSTRING)
        {
            u4Len = (UINT4) ATOI (pu1DataPtr);
            pu1DataPtr += 5;
        }
        else
        {
            u4Len = 0;
        }
        pData = MsrConvertStringToData (pu1DataPtr, u2Type, u4Len);
        if (pData == NULL)
        {
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE | OS_RESOURCE_TRC,
                     "MsrRmProcessStaticBulkUpdate: "
                     "Memory allocation failed "
                     "while converting data to string !!!\n");
            i4RetVal = MSR_FAILURE;
            break;
        }

        if ((pMSRRbNode->MSRArrayIndex >= MSR_RES_FILE_VER_FORMAT) &&
            (RmApiConfIsOIDInFilterList (pOid) == OSIX_FALSE))

        {
            /* Array index 0 contains the file version. This is a
             * read only object. Do not issue SET request for this
             * object
             */

            if (SNMPSet (*pOid, pData, &u4Error, pCurIndex,
                         SNMP_DEFAULT_CONTEXT) == SNMP_FAILURE)
            {
                MSR_TRC (MSR_RM_TRACE_TYPE,
                         "MsrRmProcessStaticBulkUpdate: "
                         "SNMP set operation failed !!!\r\n");

                /* Add to the failure buffer */
                if (RmApiConfAddToFailBuff (pOid, pData) == RM_FAILURE)
                {
                    MSR_TRC (MSR_RM_TRACE_TYPE,
                             "MsrRmProcessStaticBulkUpdate: "
                             "Add to failure buffer failed !!!\r\n");
                }
            }
        }

        pu1Buf += u4Length;
        u4PktLen -= u4Length;
    }

    if (i4RetVal == MSR_FAILURE)
    {
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        /* free the memory allocated for RBNode */
        if (pMSRRbNode != NULL)
        {
            /* Element not present in the key */
            MSRFreeRBTreeNode (pMSRRbNode, 1);
            MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmProcessStaticBulkUpdate: "
                     "Error occurred while processing "
                     "the static bulk update !!!\r\n");
            return;
        }
    }
    else
    {
        MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmProcessStaticBulkUpdate: "
                 "Static bulk update message " "processed\r\n");
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmGetRBNodeFromBuf                            */
/*                                                                          */
/*    Description        : Standby node                                     */
/*                         This function scans the Value field in the       */
/*                         buffer based on the length passed                */
/*                                                                          */
/*    Input(s)           : pu1Buf    -   pointer to the buffer              */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : Pointer to RB Tree on success / NULL             */
/****************************************************************************/

tMSRRbtree         *
MsrRmGetRBNodeFromBuf (UINT1 *pu1Buf)
{
    tMSRRbtree         *pMsrRbNode = NULL;    /* RB Tree node */
    UINT4               u4DataSize = 0;    /* Size of data */
    UINT4               u4OidLength = 0;    /* OID length */
    UINT4               u4Data = 0;    /* 4-byte data */
    UINT1              *pu1CurPtr = NULL;    /* Pointer to traverse the
                                             * buffer */
    tSNMP_OID_TYPE     *pEntOid = NULL;

    pEntOid = &gMsrRmOidType[MSR_RM_OID1];
    MSR_CLEAR_OID (pEntOid);
    /* The following information should be present in the buffer
     * 1. Array Index (4 bytes)
     * 2. RowStatustype (4 bytes)
     * 3. OID length (4 bytes)
     * 4. OID (OID length bytes)
     * 5. Datasize (4 bytes)
     * 6. Data (Datasize bytes)
     * 7. DataSave Flag (1 byte)
     */
    pu1CurPtr = pu1Buf;

    /* Get the OID length */
    pu1CurPtr = pu1CurPtr + sizeof (UINT4) + sizeof (UINT4);
    MSR_RM_GET_BYTE (&u4Data, pu1CurPtr, sizeof (u4Data));
    u4OidLength = OSIX_NTOHL (u4Data);

    /* Get the Datasize from the buffer */
    pu1CurPtr = pu1CurPtr + (u4OidLength * sizeof (UINT4));
    MSR_RM_GET_BYTE (&u4Data, pu1CurPtr, sizeof (u4Data));
    u4DataSize = OSIX_NTOHL (u4Data);

    /* Allocate the RB Node */
    pMsrRbNode = MsrAllocRBNode (u4DataSize, u4OidLength);

    pu1CurPtr = pu1Buf;
    if (pMsrRbNode != NULL)
    {
        /* Get the array index */
        MSR_RM_GET_BYTE (&u4Data, pu1CurPtr, sizeof (u4Data));
        pMsrRbNode->MSRArrayIndex = OSIX_NTOHL (u4Data);

        /* Get the Row status type */
        MSR_RM_GET_BYTE (&u4Data, pu1CurPtr, sizeof (u4Data));
        pMsrRbNode->MSRRowStatusType = OSIX_NTOHL (u4Data);

        /* Get the OID and OID length */
        pu1CurPtr = pu1CurPtr + sizeof (u4OidLength);
        MSR_RM_GET_BYTE (pMsrRbNode->pOidValue->pu4_OidList,
                         pu1CurPtr, (u4OidLength * sizeof (UINT4)));
        pMsrRbNode->pOidValue->u4_Length = u4OidLength;

        SNMPUpdateEOID (pMsrRbNode->pOidValue, pEntOid, FALSE);
        MEMSET (pMsrRbNode->pOidValue->pu4_OidList, 0,
                (u4OidLength * sizeof (UINT4)));
        SNMPCopyOid (pMsrRbNode->pOidValue, pEntOid);

        /* Get the data and the datasize */
        pu1CurPtr = pu1CurPtr + sizeof (u4Data);
        MSR_RM_GET_BYTE (pMsrRbNode->pData, pu1CurPtr, u4DataSize);
        pMsrRbNode->u4pDataSize = u4DataSize;

        /* Get the data save flag */
        MSR_RM_GET_BYTE (&pMsrRbNode->u1IsDataToBeSaved,
                         pu1CurPtr, sizeof (pMsrRbNode->u1IsDataToBeSaved));
    }

    return pMsrRbNode;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmHandleFailureBuffer                         */
/*                                                                          */
/*    Description        : Active node                                      */
/*                         This function is used to handle write failure    */
/*                         cases due to buffer overlow                      */
/*                                                                          */
/*    Input(s)           : pu1Buf        -   pointer to the buffer          */
/*                         i4WriteBytes  -   Total bytes written on socket  */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : MSR_SUCCESS / MSR_FAILURE                        */
/****************************************************************************/

INT4
MsrRmHandleFailureBuffer (UINT1 *pu1Buf, INT4 i4WriteBytes,
                          tMemPoolId BufPoolId)
{
    UINT4               u4Event = 0;

    /* Buffer sent failed. Store the buffer in gMSRRmGlobalInfo.pBuf
     * and store the no of bytes sent in gMSRRmGlobalInfo.u4BytesSentOrRcvd
     * Add the fd to the socket library and wait for buffer space
     */
    gMSRRmGlobalInfo.pBuf = pu1Buf;
    gMSRRmGlobalInfo.u4BytesSentOrRcvd = (UINT4) i4WriteBytes;
    gMSRRmGlobalInfo.BufPoolId = BufPoolId;

    /* Add the fd to the socket library */
    MsrRmSelAddFd (gMSRRmGlobalInfo.i4ConnFd, MsrRmSelWriteCallBkFn, MSR_FALSE);

    MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmHandleFailureBuffer: "
             "Stored the buffer to be sent after the "
             "buffer space is available\r\n");
    if (gMSRIncrSaveFlag == ISS_FALSE)
    {
        MSR_TRC_ARG1 (MSR_RM_TRACE_TYPE, "MsrRmHandleFailureBuffer: "
                      "Static update sync up waiting for event "
                      "MSR_RM_SEND_REM_UPDATE for the task %d\r\n",
                      gMsRmTaskId);

        if (OsixEvtRecv (gMsRmTaskId, MSR_RM_STANDBY_DOWN_EVT, OSIX_NO_WAIT, &u4Event)
            == OSIX_SUCCESS)
        {
            if (u4Event & MSR_RM_STANDBY_DOWN_EVT)
            {
                /* Return failure, on receiving the RM_message 
                 * return failure so that the current action of 
                 * sending the static bulk update is stopped */
                return MSR_FAILURE;
            }
        }
    }
    return MSR_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmNewConnReq                                  */
/*                                                                          */
/*    Description        : Active node                                      */
/*                         This is the call back function registered with   */
/*                         the select library for new connection request.   */
/*                         This function posts an event to MSR.             */
/*                                                                          */
/*    Input(s)           : i4Sockfd - Socket fd                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MsrRmNewConnReq (INT4 i4Sockfd)
{
    UNUSED_PARAM (i4Sockfd);
    OsixEvtSend (MsrId, MSR_RM_NEW_CONNECTION);
    MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmNewConnReq: New connection "
             "indication event sent to MSR\r\n");
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmPktArrival                                  */
/*                                                                          */
/*    Description        : Called in both Active and Standby node.          */
/*                         This is the call back function registered by     */
/*                         the MSR active and standby node with the select  */
/*                         library. This call back function will be called  */
/*                         when packet arrives. This function sends         */
/*                         MSR_RM_PACKET_ARRIVAL event to the MSR  module   */
/*                                                                          */
/*    Input(s)           : i4Sockfd - Socket fd                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MsrRmPktArrival (INT4 i4Sockfd)
{
    UNUSED_PARAM (i4Sockfd);

    /* Send and event to MSR module and wait for next packet arrival */
    OsixEvtSend (MsrId, MSR_RM_PACKET_ARRIVAL);
    MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmPktArrival: Packet arrival "
             "indication event sent to MSR\r\n");
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmSelWriteCallBkFn                            */
/*                                                                          */
/*    Description        : Active node                                      */
/*                         This is the call back function registered by the */
/*                         MSR active node with the select library during   */
/*                         write failure. This call back function will be   */
/*                         called when buffer space is available            */
/*                         This function sends MSR_RM_SEND_REM_UPDATE event */
/*                                                                          */
/*    Input(s)           : i4SockFd      -   Socket fd                      */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MsrRmSelWriteCallBkFn (INT4 i4SockFd)
{
    tOsixTaskId         TaskId = 0;

    UNUSED_PARAM (i4SockFd);
    if (gMSRIncrSaveFlag == ISS_TRUE)
    {
        TaskId = MsrId;
    }
    else
    {
        /* sanity ensure gMsrRmTaskId is valid  and not reset to 0 */
        if (gMsRmTaskId == 0)
        {
            gMsRmTaskId = MsrId;
        }
        TaskId = gMsRmTaskId;
    }

    MSR_TRC_ARG1 (MSR_RM_TRACE_TYPE, "MsrRmSelWriteCallBkFn: "
                  "Sending the event MSR_RM_SEND_REM_UPDATE to the task "
                  "%d\r\n", TaskId);
    OsixEvtSend (TaskId, MSR_RM_SEND_REM_UPDATE);
    MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmSelWriteCallBkFn: Remaining update "
             "event sent to MSR\r\n");
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmSendRemainingUpdate                         */
/*                                                                          */
/*    Description        : Active node                                      */
/*                         This function is called when                     */
/*                         MSR_RM_SEND_REM_UPDATE event is received         */
/*                         This function sends the remaining update of the  */
/*                         buffer present in gMSRRmGlobalInfo.pBuf          */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : pu4RemainingStatus - OSIX_TRUE means              */
/*                         still data is remaining. OSIX_FALSE means        */
/*                         fully sent.                                      */
/*                                                                          */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MsrRmSendRemainingUpdate (UINT4 *pu4RemainingStatus)
{
    tRmHdr             *pRmHdr = NULL;    /* RM header */
    UINT4               u4Length = 0;    /* Total length of the buffer */
    INT4                i4WriteBytes = 0;    /* No if bytes written */
    INT4                i4RetStatus = MSR_SUCCESS;
    /* Return value of send function */
    UINT1              *pu1Buf = NULL;    /* Pointer to the remaining buffer
                                         * to be sent
                                         */
    UINT1               u1PktType = 0;    /* Type of the message */

    *pu4RemainingStatus = OSIX_FALSE;

    MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmSendRemainingUpdate: Sending remaining "
             "buffer\r\n");

    if (gMSRRmGlobalInfo.pBuf != NULL)
    {
        MSR_TRC_ARG1 (MSR_RM_TRACE_TYPE, "MsrRmSendRemainingUpdate: %d bytes "
                      "of partial data already sent to the standby\r\n",
                      gMSRRmGlobalInfo.u4BytesSentOrRcvd);

        /* Get the total length of the buffer */
        pRmHdr = gMSRRmGlobalInfo.pBuf;
        u4Length = OSIX_NTOHL (pRmHdr->u4TotLen);

        /* Send the remaning packet */
        pu1Buf = gMSRRmGlobalInfo.pBuf + gMSRRmGlobalInfo.u4BytesSentOrRcvd;

        i4RetStatus =
            MsrRmSendPkt (gMSRRmGlobalInfo.i4ConnFd, pu1Buf,
                          (UINT2) (u4Length -
                                   gMSRRmGlobalInfo.u4BytesSentOrRcvd),
                          &i4WriteBytes);

        if (i4WriteBytes <= 0)
        {
            if (i4RetStatus == MSR_FAILURE)
            {
                MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                         "MsrRmSendRemainingUpdate: "
                         "Sending remaining Static bulk update "
                         "packet to standby node failed !!!\r\n");
                MemReleaseMemBlock (gMSRRmGlobalInfo.BufPoolId,
                                    (UINT1 *) (gMSRRmGlobalInfo.pBuf));
                gMSRRmGlobalInfo.pBuf = NULL;
                gMSRRmGlobalInfo.u4BytesSentOrRcvd = 0;
                MsrRmSelRemoveFd (gMSRRmGlobalInfo.i4ConnFd, MSR_TRUE);
                MsrRmSockClose (gMSRRmGlobalInfo.i4ConnFd);
                gMSRRmGlobalInfo.i4ConnFd = -1;
            }
            else
            {
                /* Buffer is full */
                if (MsrRmHandleFailureBuffer (gMSRRmGlobalInfo.pBuf,
                                          (INT4) gMSRRmGlobalInfo.
                                          u4BytesSentOrRcvd,
                                          gMSRRmGlobalInfo.BufPoolId) == MSR_SUCCESS)
                {
                    *pu4RemainingStatus = OSIX_TRUE;
                }

            }
        }
        else if (i4WriteBytes !=
                 (INT4) (u4Length - gMSRRmGlobalInfo.u4BytesSentOrRcvd))
        {
            MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmSendRemainingUpdate: "
                     "Partial Static bulk update packet "
                     "sent to standby\r\n");
            /* Handle unsuccessful write operation */
            if (MsrRmHandleFailureBuffer (gMSRRmGlobalInfo.pBuf,
                                      (INT4) (gMSRRmGlobalInfo.u4BytesSentOrRcvd
                                              + (UINT4) i4WriteBytes),
                                       gMSRRmGlobalInfo.BufPoolId) == MSR_SUCCESS)
            {
                    *pu4RemainingStatus = OSIX_TRUE;
            }
        }
        else
        {
            MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmSendRemainingUpdate: "
                     "Static bulk update packet sent to standby\r\n");

            /* If the packet is static bulk tail message, destroy the RB Tree
             * if incremental save is disabled. Send an event to RM
             */
            pu1Buf = gMSRRmGlobalInfo.pBuf + RM_HDR_LENGTH;
            MSR_RM_GET_BYTE (&u1PktType, pu1Buf, sizeof (UINT1));
            if (u1PktType == RM_BULK_UPDT_TAIL_MSG)
            {
                /* Free the memory allocated for last added RB Tree node */
                free_oid (gMSRRmLastUpdatedNode.pOidValue);
                gMSRRmLastUpdatedNode.pOidValue = NULL;

                /* Close the socket */
                MsrRmSelRemoveFd (gMSRRmGlobalInfo.i4ConnFd, MSR_TRUE);
                MsrRmSockClose (gMSRRmGlobalInfo.i4ConnFd);
                gMSRRmGlobalInfo.i4ConnFd = -1;

                /* Set the configuration restoration status as restored. */
                RmSetStaticConfigStatus (RM_STATIC_CONFIG_RESTORED);
            }
            else
            {
                if (gMSRIncrSaveFlag == ISS_TRUE)
                {
                    /* Send an event to send the next set of static bulk updates */
                    OsixEvtSend (MsrId, MSR_RM_SUB_BULK_UPDATE);
                    MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmSendRemainingUpdate: "
                             "sub static bulk update event sent to MSR\r\n");
                }
            }
            /* Packet sent to the active node. Free the linear buffer */
            MemReleaseMemBlock (gMSRRmGlobalInfo.BufPoolId,
                                (UINT1 *) (gMSRRmGlobalInfo.pBuf));
            gMSRRmGlobalInfo.pBuf = NULL;
            gMSRRmGlobalInfo.u4BytesSentOrRcvd = 0;
        }
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmHandlePartialRcvPkt                         */
/*                                                                          */
/*    Description        : Standby node                                     */
/*                         This function is used to handle read failure     */
/*                         cases due to buffer overlow                      */
/*                                                                          */
/*    Input(s)           : pu1Buf        -   pointer to the buffer          */
/*                         i4ReadBytes   -   Total bytes read from socket   */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MsrRmHandlePartialRcvPkt (UINT1 *pu1Buf, INT4 i4ReadBytes)
{
    /* Buffer read failed. Store the buffer in gMSRRmGlobalInfo.pBuf
     * and store the no of bytes read in gMSRRmGlobalInfo.u4BytesSentOrRcvd
     */
    if (gMSRRmGlobalInfo.pBuf != NULL)
    {
        /* The buffer already has come partial data. Add the remaining
         * data to buffer and update the no of bytes received
         */
        if (i4ReadBytes != 0)
        {
            if ((gMSRRmGlobalInfo.u4BytesSentOrRcvd < MSR_RM_MAX_PKT_SIZE)
                && (i4ReadBytes <= (INT4) (MSR_RM_MAX_PKT_SIZE - gMSRRmGlobalInfo.u4BytesSentOrRcvd)))
            {
            MEMCPY ((gMSRRmGlobalInfo.pBuf +
                     gMSRRmGlobalInfo.u4BytesSentOrRcvd), pu1Buf,
                    MEM_MAX_BYTES (i4ReadBytes, (INT4) (MSR_RM_MAX_PKT_SIZE - gMSRRmGlobalInfo.u4BytesSentOrRcvd)));
            gMSRRmGlobalInfo.u4BytesSentOrRcvd += (UINT4) (i4ReadBytes);
            gMSRRmGlobalInfo.BufPoolId = gMSRRmGlobalInfo.MsrRmPktPoolId;
        }
            else
            {
                UtlTrcLog (1,1,"MSR", "unexpected i4ReadBytes %d"
                        "gMSRRmGlobalInfo.u4BytesSentOrRcvd %d \n",
                        i4ReadBytes, gMSRRmGlobalInfo.u4BytesSentOrRcvd);
            }
        }
    }
    else
    {
        gMSRRmGlobalInfo.pBuf =
            MemAllocMemBlk (gMSRRmGlobalInfo.MsrRmPktPoolId);
        if (gMSRRmGlobalInfo.pBuf == NULL)
        {
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE, "MsrRmHandlePartialRcvPkt: "
                     "Unable to allocate memory for partial packet !!!\r\n");
            return;
        }
        MEMSET (gMSRRmGlobalInfo.pBuf, 0, MSR_RM_MAX_PKT_SIZE);

        MEMCPY (gMSRRmGlobalInfo.pBuf, pu1Buf,
                MEM_MAX_BYTES (i4ReadBytes, (INT4) MSR_RM_MAX_PKT_SIZE));
        gMSRRmGlobalInfo.u4BytesSentOrRcvd = (UINT4) i4ReadBytes;
        gMSRRmGlobalInfo.BufPoolId = gMSRRmGlobalInfo.MsrRmPktPoolId;
    }

    MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmHandlePartialRcvPkt: "
             "Stored the partial data in the temporary buffer\r\n");
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmRcvRemainingUpdate                          */
/*                                                                          */
/*    Description        : Standby node                                     */
/*                         This function is called while receiving the      */
/*                         incoming packet and there is some partial data   */
/*                         received earlier                                 */
/*                                                                          */
/*    Input(s)           : pu1Buf   -  Allocated buffer for receiving       */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : MSR_TRUE if next buffer can be read / MSR_FALSE  */
/****************************************************************************/
INT4
MsrRmRcvRemainingUpdate (UINT1 *pu1Buf)
{
    tRmHdr             *pRmHdr = NULL;    /* RM header */
    UINT4               u4RcvLen = 0;    /* No of bytes to be received */
    UINT4               u4TotLen = 0;    /* Total length of packet */
    INT4                i4ReadBytes = 0;    /* No of bytes read */
    INT4                i4RetVal = MSR_TRUE;
    /* Return value */
    INT4                i4RetStatus = MSR_SUCCESS;
    /* Return value of receive function */
    INT4                i4Flag = MSR_TRUE;
    /* Flag to indicate whether the
     * global buffer should be destroyed
     * or not
     */
    UINT2               u2ChkSum = 0;    /* Check sum */

    MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmRcvRemainingUpdate: "
             "Receiving remaining update\r\n");

    if (gMSRRmGlobalInfo.pBuf == NULL)
    {
        return MSR_TRUE;
    }

    MSR_TRC_ARG1 (MSR_RM_TRACE_TYPE,
                  "MsrRmRcvRemainingUpdate: %d bytes of data available in "
                  "received partial buffer\r\n",
                  gMSRRmGlobalInfo.u4BytesSentOrRcvd);

    /* Calculate the bytes to be received */
    if (gMSRRmGlobalInfo.u4BytesSentOrRcvd < RM_HDR_LENGTH)
    {
        /* Get the remaninng RM header */
        u4RcvLen = RM_HDR_LENGTH - gMSRRmGlobalInfo.u4BytesSentOrRcvd;

        i4RetStatus = MsrRmRcvPkt (gMSRRmGlobalInfo.i4ConnFd,
                                   pu1Buf, (UINT2) u4RcvLen, &i4ReadBytes);

        if ((i4ReadBytes <= 0) && (i4RetStatus == MSR_SUCCESS))
        {
            /* All data are read. */
            MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmRcvRemainingUpdate: "
                     "All packets are processed\r\n");
            i4RetVal = MSR_FALSE;
            i4Flag = MSR_FALSE;
        }
        else if (i4ReadBytes <= 0)
        {
            gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                     "MsrRmRcvRemainingUpdate: Recv failed !!!\r\n");

            i4RetVal = MSR_FALSE;
            i4Flag = MSR_TRUE;
        }
        else if (i4ReadBytes != (INT4) u4RcvLen)
        {
            MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmRcvRemainingUpdate: "
                     "Partial RM header received\r\n");

            MsrRmHandlePartialRcvPkt (pu1Buf, i4ReadBytes);
            i4RetVal = MSR_FALSE;
            i4Flag = MSR_FALSE;
        }
        else
        {
            MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmRcvRemainingUpdate: "
                     "Static bulk update header received\r\n");
            /* Append the buffer read to the partial buffer stored */
            MEMCPY ((gMSRRmGlobalInfo.pBuf +
                     gMSRRmGlobalInfo.u4BytesSentOrRcvd), pu1Buf, i4ReadBytes);

            gMSRRmGlobalInfo.u4BytesSentOrRcvd = RM_HDR_LENGTH;
            gMSRRmGlobalInfo.BufPoolId = gMSRRmGlobalInfo.MsrRmPktPoolId;
            i4RetVal = MSR_TRUE;
            i4Flag = MSR_TRUE;
        }
    }

    if (i4RetVal == MSR_TRUE)
    {
        /* The received buffer now contains either empty payload with
         * RM header or incomplete payload with RM header
         */
        pRmHdr = gMSRRmGlobalInfo.pBuf;

        /* Get the total length of packet */
        u4TotLen = OSIX_NTOHL (pRmHdr->u4TotLen);

        u4RcvLen = u4TotLen - gMSRRmGlobalInfo.u4BytesSentOrRcvd;

        if ((INT4) u4RcvLen <= 0)
        {
            MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmRcvRemainingUpdate: "
                     "Error in the received packet !!!\r\n");
            i4RetVal = MSR_TRUE;
            i4Flag = MSR_TRUE;
        }
        else
        {
            /* Read the payload */
            MEMSET (pu1Buf, 0, MSR_RM_MAX_PKT_SIZE);
            i4RetStatus = MsrRmRcvPkt (gMSRRmGlobalInfo.i4ConnFd,
                                       pu1Buf, (UINT2) u4RcvLen, &i4ReadBytes);

            if ((i4ReadBytes <= 0) && (i4RetStatus == MSR_SUCCESS))
            {
                /* All data are read. */
                MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmRcvRemainingUpdate: "
                         "All packets are processed\r\n");
                i4RetVal = MSR_FALSE;
                i4Flag = MSR_FALSE;
            }
            else if (i4ReadBytes <= 0)
            {
                gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                         "MsrRmRcvRemainingUpdate: Recv failed !!!\r\n");
                MsrRmSockClose (gMSRRmGlobalInfo.i4ConnFd);
                gMSRRmGlobalInfo.i4ConnFd = -1;
                i4RetVal = MSR_FALSE;
                i4Flag = MSR_TRUE;
            }
            else if (i4ReadBytes != (INT4) u4RcvLen)
            {
                MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmRcvRemainingUpdate: "
                         "Partial RM payload received\r\n");

                MsrRmHandlePartialRcvPkt (pu1Buf, i4ReadBytes);
                i4RetVal = MSR_FALSE;
                i4Flag = MSR_FALSE;
            }
            else
            {
                /* Append the buffer read to the partial buffer stored */
                MEMCPY ((gMSRRmGlobalInfo.pBuf +
                         gMSRRmGlobalInfo.u4BytesSentOrRcvd), pu1Buf,
                        i4ReadBytes);
                /* Process the packet */
                u2ChkSum = UtlIpCSumLinBuf ((const INT1 *)
                                            gMSRRmGlobalInfo.pBuf, u4TotLen);

                if (u2ChkSum != 0)
                {
                    /* Discard the packet and read the next packet */
                    MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE | ALL_FAILURE_TRC,
                             "MsrRmRcvRemainingUpdate: "
                             "Check sum verification failed "
                             "for packet !!!\r\n");
                }
                else
                {
                    MSR_TRC (MSR_RM_TRACE_TYPE,
                             "MsrRmRcvRemainingUpdate: Static "
                             "bulk message received\r\n");
                    MsrRmProcessPacket (gMSRRmGlobalInfo.pBuf, u4TotLen);
                }
                i4RetVal = MSR_TRUE;
                i4Flag = MSR_TRUE;
            }
        }
    }

    if (i4Flag == MSR_TRUE)
    {
        MemReleaseMemBlock (gMSRRmGlobalInfo.BufPoolId,
                            (UINT1 *) (gMSRRmGlobalInfo.pBuf));
        gMSRRmGlobalInfo.pBuf = NULL;
        gMSRRmGlobalInfo.u4BytesSentOrRcvd = 0;
        MEMSET (pu1Buf, 0, MSR_RM_MAX_PKT_SIZE);
    }

    return i4RetVal;
}

/******************************************************************************
 * Function           : MsrRmFileReadForColdStandby
 * Input(s)           : None
 * Output(s)          : None.
 * Returns            : None
 * Action             : Routine used to read files in cold standby
******************************************************************************/
VOID
MsrRmFileReadForSelfAttach (VOID)
{
    /*Read the nvram file */
    IssReadSystemInfoFromNvRam ();
    /* Update ISS data structure with new values */
    IssInitSystemParams ();
    /* Clear the gpu1StrFlash, gu2FlashCksum & gu1RamState
     * for erasing the previous configurations */
    MSR_MEMSET (gpu1StrFlash, 0, MSR_MIBENTRY_MEMBLK_SIZE);
    gu2FlashCksum = 0;
    gu1RamState = MSR_RAM_EMPTY;
    gu4MsrOidCount = 0;
    /* The configurations changed will be applied, so we reset this flag
     */
    gi1ConfigChange = MSR_FALSE;
    /* Configuration database is downloaded from Active node. Trigger the MSR
     * restoration to apply the static configurations in standby node.
     */
    if (IssGetRestoreOptionFromNvRam () == ISS_RESTORE_LOCAL)
    {
        MsrProcessMibRestoreEvent ();
    }
    MsrSendRestorationCompleteToProtocols ();
    /*Read SlotModule.conf */
#ifdef MBSM_WANTED
    MbsmGetSlotModuleInfoFromFile ();
#endif
#ifdef CLI_WANTED
    /* Update CLI data structure with users in "users" file */
    CliReadUsers ();
    /* Update CLI data structure with privilages in "privil" file */
    CliReadPrivileges ();
#endif

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmStartStaticBulkUpdate                       */
/*                                                                          */
/*    Description        : Active                                           */
/*                         This function spawns a new task that obtains     */
/*                          the configurations from the protocol            */
/*                          database and sends the confiugration            */
/*                          to the stanby node.                             */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MsrRmStartStaticBulkUpdate ()
{

    /* When the static bulk update processing of getting the configurations
     * and sending it to the standby node should not affect the other 
     * MSR operations, create a new task that does the mentioned operations
     * and exit the task. 
     * The following piece of commented code can be used for creating
     * a new task for the static bulk update processing */

    /* 
       tOsixTaskId    TaskId;

       if (OsixTskCrt ((UINT1 *) "MSRM", (200 | OSIX_SCHED_RR), 
       OSIX_DEFAULT_STACK_SIZE, 
       (OsixTskEntry) MsrRmStartSendStaticUpdate,
       0, &TaskId) != OSIX_SUCCESS)
       {
       MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE | OS_RESOURCE_TRC,
       "Task creation for Processing static update in incremental"
       "off mode failed.!!!\r\n");
       return;
       }
     */

    INT1               *pUnused = NULL;

    MsrRmStartSendStaticUpdate (pUnused);

    return;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmStartSendStaticUpdate                       */
/*                                                                          */
/*    Description        : Active                                           */
/*                         This function process the static bulk update     */
/*                          request in the incremental save off mode.       */
/*                                                                          */
/*    Input(s)           : pi1Param - dummy parameter                       */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MsrRmStartSendStaticUpdate (INT1 *pi1Param)
{
    UNUSED_PARAM (pi1Param);

    gMsRmTaskId = 0;
    OsixTskIdSelf (&gMsRmTaskId);
    if (MsrRmObtainCfgAndSendUpdate () == MSR_FAILURE)
    {
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                 "MsrRmStartSendStaticUpdate: Sending static bulk updates"
                 " failed !!!\r\n");
        gMsRmTaskId = 0;
        return;

    }

    /* The MsrRmObtainCfgAndSendUpdate return only when there no more
     * configurations to be obtained from the protocol database.
     * The buffer is filled only with the RM header. Send a
     * static bulk update tail message to the standby node */
    MSR_TRC (MSR_RM_TRACE_TYPE,
             "MsrRmStartSendStaticUpdate: All static bulk "
             "updates sent to standby. Sending tail message.\r\n");
    MsrRmSendStaticBulkTail ();
    gMsRmTaskId = 0;

    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmObtainCfgAndSendUpdate                      */
/*                                                                          */
/*    Description        : Active                                           */
/*                         This function obtains the configurations from    */
/*                          the protocol database and sends the             */
/*                          configuration to the standby node               */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : MSR_SUCCESS / MSR_FAILURE                        */
/****************************************************************************/
INT4
MsrRmObtainCfgAndSendUpdate (VOID)
{
    tSNMP_OID_TYPE     *pCurOid = NULL;
    tSNMP_OID_TYPE     *pInstance = NULL;
    tSNMP_OID_TYPE     *pTmpOid = NULL;
    tSNMP_MULTI_DATA_TYPE *pDefData = NULL;
    tMSRRbtree          RbNode;
    UINT4               u4Index = 0;
    UINT4               u4PrevOidValue = 0;
    UINT4               u4Length = 0;
    UINT4               u4TotalLength = 0;
    UINT1               au1Oid[MSR_MAX_OID_LEN + 1];
    UINT1              *pu1Data = NULL;
    UINT1              *pu1Buf = NULL;
    UINT1              *pu1CurPtr = NULL;
    UINT1               u1Flag = 0;
    UINT4               u4Len = 0;

    MSR_TRC (MSR_RM_TRACE_TYPE, "Msr Obtaining protocol configurations "
             "for static bulk update is in progress\n");
    if (MSR_DATA_ALLOC_MEM_BLOCK (pu1Data) == NULL)
    {
        return MSR_FAILURE;
    }
    pCurOid = &gMsrRmOidType[MSR_RM_OID1];
    pTmpOid = &gMsrRmOidType[MSR_RM_OID2];
    pInstance = &gMsrRmOidType[MSR_RM_OID3];
    RbNode.pOidValue = &gMsrRmOidType[MSR_RM_OID4];
    RbNode.pData = pu1Data;

    pDefData = &gMsrRmMultiData[MSR_RM_DEF_MULTIDATA1];

    pu1Buf = MsrRmConstructAndFillRmHeader (&u4TotalLength);

    if (pu1Buf == NULL)
    {
        MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmSaveMibAndSendUpdate: "
                 "Allocating buffer failed\r\n");
        MSR_DATA_FREE_MEM_BLOCK (pu1Data);
        return MSR_FAILURE;
    }
    pu1CurPtr = pu1Buf + u4TotalLength;

    /* Get all the object from the gaSaveArray */
    while (gaSaveArray[u4Index].pOidString != NULL)
    {
        /* clear the data structures used for getting the previous element */
        MSR_CLEAR_OID (pCurOid);
        MSR_CLEAR_OID (pInstance);
        MSR_CLEAR_OID (pTmpOid);

        if (gaSaveArray[u4Index].u2Type == MSR_SCALAR)
        {
            if (MsrRmGetScalarConfig (u4Index, &RbNode) == MSR_FAILURE)
            {
                u4Index++;
                continue;
            }
            u4Length = MsrRmAddRbNodeToBuf (pu1CurPtr, &RbNode, u4TotalLength);
            if (u4Length != 0)
            {
                u4TotalLength += u4Length;
                pu1CurPtr += u4Length;
            }
            else
            {
                MSR_TRC (MSR_RM_TRACE_TYPE,
                         "MsrRmSaveMibAndSendUpdate: "
                         "Active node static update Msg buffer is full "
                         "Transmitting the packet to standby\n");
                /* when the buffer is full send the bulk update message 
                 * to the standby node */
                if (MsrRmUpdateAndSendBulkUpdateMsg (pu1Buf, u4TotalLength)
                    == MSR_FAILURE)
                {
                    MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                             "MsrRmSaveMibAndSendUpdate:"
                             " Sending sync message failed. Obtaining the "
                             " configuration from the protocol stopped \n");
                    MSR_DATA_FREE_MEM_BLOCK (pu1Data);
                    return MSR_FAILURE;
                }
                /* As the buffer will be cleared after sending the packet
                 * to the standby node. Fill the RM header information 
                 * alone */
                pu1Buf = MsrRmConstructAndFillRmHeader (&u4TotalLength);
                if (pu1Buf == NULL)
                {
                    MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                             "MsrRmSaveMibAndSendUpdate:"
                             "Allocating buffer failed\r\n");
                    MSR_DATA_FREE_MEM_BLOCK (pu1Data);
                    return MSR_FAILURE;
                }
                pu1CurPtr = pu1Buf + u4TotalLength;

                /* The configuration read is written to the buffer */
                /* A single RBNode will not exceed the buffer size
                 * Hence, the negative verification is avoided */
                u4Length = MsrRmAddRbNodeToBuf (pu1CurPtr, &RbNode,
                                                u4TotalLength);
                u4TotalLength += u4Length;
                pu1CurPtr += u4Length;
            }
        }
        else
            /* when table objects have to be stored */
        {
            /* If there is a rowstatus associated with the table, first
             * get the rowstatus instances and set the value of the 
             * rowstatus as CREATE_AND_WAIT. When there is a rowstatus,
             * an entry for a instance gets created only when the 
             * rowstatus is set to CREATE_AND_WAIT.
             */

            if (gaSaveArray[u4Index].pRowStatus != NULL)
            {
                u4Len =
                    ((STRLEN (gaSaveArray[u4Index].pRowStatus) <
                      (sizeof (au1Oid) -
                       1)) ? STRLEN (gaSaveArray[u4Index].
                                     pRowStatus) : (sizeof (au1Oid) - 1));
                MSR_STRNCPY (au1Oid, gaSaveArray[u4Index].pRowStatus, u4Len);
                au1Oid[u4Len] = '\0';

                SNMPConvertStringToOid (pTmpOid, au1Oid, MSR_FALSE);

                /* Get all the rowstatus objects in the table */
                while (1)
                {
                    /* Reset the pCurOid to the original OID */
                    MSR_CLEAR_OID (pCurOid);
                    SNMPCopyOid (pCurOid, pTmpOid);
                    if (MsrRmGetTableRSConfig (u4Index, pCurOid, pInstance,
                                               &RbNode, &u1Flag) == MSR_FAILURE)
                    {
                        if (u1Flag == MSR_FALSE)
                        {
                            break;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    u4Length = MsrRmAddRbNodeToBuf (pu1CurPtr,
                                                    &RbNode, u4TotalLength);
                    if (u4Length != 0)
                    {
                        u4TotalLength += u4Length;
                        pu1CurPtr += u4Length;
                    }
                    else
                    {
                        MSR_TRC (MSR_RM_TRACE_TYPE,
                                 "MsrRmSaveMibAndSendUpdate: "
                                 "Active node static update Msg buffer is full "
                                 "Transmitting the packet to standby\r\n");

                        /* when the buffer is full send the bulk update message 
                         * to the standby node */
                        if (MsrRmUpdateAndSendBulkUpdateMsg (pu1Buf,
                                                             u4TotalLength)
                            == MSR_FAILURE)
                        {
                            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                                     "MsrRmSaveMibAndSendUpdate:"
                                     " Sending sync message failed. Obtaining the "
                                     " configuration from the protocol stopped \n");
                            MSR_DATA_FREE_MEM_BLOCK (pu1Data);
                            return MSR_FAILURE;
                        }
                        /* As the buffer will be cleared after sending the
                         * packet to the standby node. Fill the RM header
                         * information alone */
                        pu1Buf = MsrRmConstructAndFillRmHeader (&u4TotalLength);
                        if (pu1Buf == NULL)
                        {
                            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                                     "MsrRmSaveMibAndSendUpdate: "
                                     "Allocating buffer failed\r\n");
                            MSR_DATA_FREE_MEM_BLOCK (pu1Data);
                            return MSR_FAILURE;
                        }
                        pu1CurPtr = pu1Buf + u4TotalLength;
                        /* A single RBNode will not exceed the buffer size
                         * Hence, the negative verification is avoided */
                        u4Length = MsrRmAddRbNodeToBuf (pu1CurPtr, &RbNode,
                                                        u4TotalLength);
                        u4TotalLength += u4Length;
                        pu1CurPtr += u4Length;
                    }
                }
            }
            /* clear the data structures used for getting the rowstatus */
            MSR_CLEAR_OID (pCurOid);
            MSR_CLEAR_OID (pInstance);
            MSR_CLEAR_OID (pTmpOid);

            /* reset the au1Oid to the current OID */
            /* Append .0 to the pCurOid so that the next valid
             * index in the table is obtained.
             * To append .0 to the pCurOid, set the length of pInstance
             * to 1 so that .0 will be appended */

            MEMSET (au1Oid, 0, MSR_MAX_OID_LEN + 1);
            u4Len =
                ((STRLEN (gaSaveArray[u4Index].pOidString) <
                  (sizeof (au1Oid) -
                   1)) ? STRLEN (gaSaveArray[u4Index].
                                 pOidString) : (sizeof (au1Oid) - 1));
            MSR_STRNCPY (au1Oid, gaSaveArray[u4Index].pOidString, u4Len);
            au1Oid[u4Len] = '\0';
            SNMPConvertStringToOid (pTmpOid, au1Oid, MSR_FALSE);
            pInstance->u4_Length = 1;
            u4PrevOidValue = 0;

            /* Get all the objects in the table */
            while (1)
            {
                /* Reset the pCurOid to the original OID */
                MSR_CLEAR_OID (pCurOid);
                SNMPCopyOid (pCurOid, pTmpOid);
                if (MsrRmGetTableConfig (u4Index, pCurOid, pInstance, &RbNode,
                                         pDefData, u4PrevOidValue, &u1Flag)
                    == MSR_FAILURE)
                {
                    if (u1Flag == MSR_FALSE)
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }

                u4PrevOidValue = pInstance->pu4_OidList[0];
                u4Length = MsrRmAddRbNodeToBuf (pu1CurPtr,
                                                &RbNode, u4TotalLength);
                if (u4Length != 0)
                {
                    u4TotalLength += u4Length;
                    pu1CurPtr += u4Length;
                }
                else
                {
                    MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmSaveMibAndSendUpdate: "
                             "Active node static update Msg buffer is full "
                             "Transmitting the packet to standby\r\n");

                    /* when the buffer is full send the bulk update message 
                     * to the standby node */
                    if (MsrRmUpdateAndSendBulkUpdateMsg (pu1Buf, u4TotalLength)
                        == MSR_FAILURE)
                    {
                        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                                 "MsrRmSaveMibAndSendUpdate:"
                                 " Sending sync message failed. Obtaining the "
                                 " configuration from the protocol stopped \n");
                        MSR_DATA_FREE_MEM_BLOCK (pu1Data);
                        return MSR_FAILURE;
                    }

                    /* As the buffer will be cleared after sending the packet
                     * to the standby node. Fill the RM header information 
                     * alone */
                    pu1Buf = MsrRmConstructAndFillRmHeader (&u4TotalLength);
                    if (pu1Buf == NULL)
                    {
                        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                                 "MsrRmSaveMibAndSendUpdate:"
                                 "Allocating buffer failed\r\n");
                        MSR_DATA_FREE_MEM_BLOCK (pu1Data);
                        return MSR_FAILURE;
                    }
                    pu1CurPtr = pu1Buf + u4TotalLength;

                    /* A single RBNode will not exceed the buffer size
                     * Hence, the negative verification is avoided */
                    u4Length = MsrRmAddRbNodeToBuf (pu1CurPtr, &RbNode,
                                                    u4TotalLength);
                    u4TotalLength += u4Length;
                    pu1CurPtr += u4Length;
                }
            }
        }
        u4Index++;
    }
    /* When all th elements of the gaSaveArray are obtained, send the 
     * remaining configurations in the buffer to the stanby node */
    if (MsrRmUpdateAndSendBulkUpdateMsg (pu1Buf, u4TotalLength) == MSR_FAILURE)
    {
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE, "MsrRmSaveMibAndSendUpdate:"
                 " Sending sync message failed.\r\n");
        MSR_DATA_FREE_MEM_BLOCK (pu1Data);
        return MSR_FAILURE;
    }
    MSR_DATA_FREE_MEM_BLOCK (pu1Data);
    return MSR_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmConstructAndFillRmHeader                    */
/*                                                                          */
/*    Description        : This function constructs a buffer and fills the  */
/*                         RM header in the buffer. It returns the length   */
/*                         of the RM Header and the pointer to the buffer   */
/****************************************************************************/
UINT1              *
MsrRmConstructAndFillRmHeader (UINT4 *pu4Length)
{
    UINT4               u4TotalLen = 0;
    UINT1              *pu1CurPtr = NULL;
    UINT1              *pu1Buf = NULL;
    UINT1               u1MessageType;    /* Contains the message type */

    *pu4Length = 0;
    if (gMSRRmGlobalInfo.u1NumPeers == 0)
    {
        /* Peer down event received before sending static bulk updates */
        MSR_TRC (MSR_RM_TRACE_TYPE,
                 "MsrRmConstructAndFillRmHeader: Peer does not exist. "
                 "Static bulk update process is not completed\r\n");
        return NULL;
    }

    pu1Buf = MsrRmAllocateBuffer ();

    if (pu1Buf == NULL)
    {
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE | OS_RESOURCE_TRC,
                 "MsrRmConstructAndFillRmHeader: Unable to allocate"
                 "memory for the linear buffer !!!\r\n");
        return NULL;
    }

    MEMSET (pu1Buf, 0, MSR_RM_MAX_PKT_SIZE);

    /* Move the pointer and update the message type after RM header */
    pu1CurPtr = pu1Buf + RM_HDR_LENGTH;

    /* Add the message type */
    u1MessageType = (UINT1) RM_BULK_UPDATE_MSG;
    MSR_RM_ADD_BYTE (pu1CurPtr, &u1MessageType, sizeof (u1MessageType));
    u4TotalLen = RM_HDR_LENGTH + sizeof (UINT1);

    *pu4Length = u4TotalLen;
    MSR_TRC (MSR_RM_TRACE_TYPE,
             "MsrRmConstructAndFillRmHeader: Allocated the buffer "
             "and updated the header info\r\n");
    return pu1Buf;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmUpdateAndSendBulkUpdateMsg                  */
/*                                                                          */
/*    Description        : This function updates the buffer, cal fills the  */
/*                         RM header in the buffer. It returns the pointer  */
/*                         to the next empty field in the buffer and the    */
/*                         length of the RM Header.                         */
/****************************************************************************/
INT4
MsrRmUpdateAndSendBulkUpdateMsg (UINT1 *pu1Buf, UINT4 u4BufLen)
{
    tRmHdr             *pRmHdr = NULL;    /* RM header */
    INT4                i4WriteBytes = 0;
    INT4                i4RetStatus = 0;
    UINT2               u2ChkSum = 0;
    UINT4               u4Event = 0;
    UINT4               u4RemainingStatus = OSIX_TRUE;

    /* Add the RM header */
    pRmHdr = (tRmHdr *) (VOID *) pu1Buf;

    pRmHdr->u4TotLen = OSIX_HTONL (u4BufLen);
    pRmHdr->u4SrcEntId = OSIX_HTONL (RM_MSR_APP_ID);
    pRmHdr->u4DestEntId = OSIX_HTONL (RM_MSR_APP_ID);

    /* Calculate the check sum */
    u2ChkSum = UtlIpCSumLinBuf ((const INT1 *) pu1Buf, u4BufLen);
    pRmHdr->u2Chksum = OSIX_HTONS (u2ChkSum);

    /* Send the packet to the standby */
    i4RetStatus = MsrRmSendPkt (gMSRRmGlobalInfo.i4ConnFd,
                                pu1Buf, (UINT2) u4BufLen, &i4WriteBytes);

    if (i4WriteBytes <= 0)
    {
        if (i4RetStatus == MSR_FAILURE)
        {
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                     "MsrRmUpdateAndSendBulkUpdateMsg: "
                     "Static bulk update packet to standby "
                     "node failed !!!\r\n");
            MemReleaseMemBlock (gMSRRmGlobalInfo.MsrRmPktPoolId,
                                (UINT1 *) (pu1Buf));
            MsrRmChannelDeInit (gMSRRmGlobalInfo.i4ConnFd);
            gMSRRmGlobalInfo.i4ConnFd = -1;
            gMsRmTaskId = 0;
            return MSR_FAILURE;
        }
        else
        {
            /* Buffer is full. No of bytes sent is 0 */
            if (MsrRmHandleFailureBuffer (pu1Buf, 0,
                                          gMSRRmGlobalInfo.MsrRmPktPoolId)
                == MSR_FAILURE)
            {
                MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                         "MsrRmUpdateAndSendBulkUpdateMsg: "
                         "Resending the remaining buffer failed.!!!\r\n");
                MemReleaseMemBlock (gMSRRmGlobalInfo.MsrRmPktPoolId,
                                    (UINT1 *) (pu1Buf));
                MsrRmChannelDeInit (gMSRRmGlobalInfo.i4ConnFd);
                gMSRRmGlobalInfo.i4ConnFd = -1;
                gMsRmTaskId = 0;
                return MSR_FAILURE;
            }
            u4RemainingStatus = OSIX_TRUE;
            while (u4RemainingStatus == OSIX_TRUE)
            {
                if (OsixEvtRecv (gMsRmTaskId, MSR_RM_SEND_REM_UPDATE, OSIX_WAIT, &u4Event)
                    == OSIX_SUCCESS)
                {
                    MsrRmSendRemainingUpdate (&u4RemainingStatus);
                }
            }

        }

    }
    else if (i4WriteBytes != (INT4) (u4BufLen))
    {
        MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmUpdateAndSendBulkUpdateMsg: "
                 "Partial Static bulk update packet " "sent to standby\r\n");
        /* Handle unsuccessful write operation */
        if (MsrRmHandleFailureBuffer (pu1Buf, i4WriteBytes,
                                      gMSRRmGlobalInfo.MsrRmPktPoolId)
            == MSR_FAILURE)
        {
            MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE,
                     "MsrRmUpdateAndSendBulkUpdateMsg: "
                     "Resending the remaining buffer failed.!!!\r\n");
            MemReleaseMemBlock (gMSRRmGlobalInfo.MsrRmPktPoolId,
                                (UINT1 *) (pu1Buf));
            MsrRmChannelDeInit (gMSRRmGlobalInfo.i4ConnFd);
            gMSRRmGlobalInfo.i4ConnFd = -1;
            gMsRmTaskId = 0;
            return MSR_FAILURE;
        }
        u4RemainingStatus = OSIX_TRUE;
        while (u4RemainingStatus == OSIX_TRUE)
        {
            if (OsixEvtRecv (gMsRmTaskId, MSR_RM_SEND_REM_UPDATE, OSIX_WAIT, &u4Event)
                == OSIX_SUCCESS)
            {
                MsrRmSendRemainingUpdate (&u4RemainingStatus);
            }
        }
    }
    else
    {
        MSR_TRC (MSR_RM_TRACE_TYPE, "MsrRmUpdateAndSendBulkUpdateMsg: "
                 "Static bulk update packet sent to standby\r\n");
        MemReleaseMemBlock (gMSRRmGlobalInfo.MsrRmPktPoolId,
                            (UINT1 *) (pu1Buf));

    }
    return MSR_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmGetScalarConfig                             */
/*                                                                          */
/*    Description        : This function obtains the scalar configuration   */
/*                         present in the given index of the gaSaveArray    */
/*                         This function return failure when the scalar     */
/*                         configuration fails in the validation to send to */
/*                         the standby node, else returns success           */
/****************************************************************************/

INT4
MsrRmGetScalarConfig (UINT4 u4Index, tMSRRbtree * pRbNode)
{
    tSNMP_OID_TYPE     *pCurOid = NULL;
    tSNMP_MULTI_DATA_TYPE *pData = NULL, *pDefData = NULL;
    tSnmpIndex         *pCurIndex = MsrRmIndexPool1;
    UINT4               u4Error = 0;
    INT4                i4RetVal = 0;
    INT4                i4SnmpVal = 0;
    INT4                i4CmpResult = MSR_FAILURE;
    UINT1               au1Oid[MSR_MAX_OID_LEN + 1];
    UINT4               u4Len = 0;

    pCurOid = &gMsrRmOidType[MSR_RM_OID5];
    pData = &gMsrRmMultiData[MSR_RM_MULTIDATA];
    pDefData = &gMsrRmMultiData[MSR_RM_DEF_MULTIDATA2];

    MSR_CLEAR_OID (pCurOid);
    MSR_CLEAR_MULTIDATA (pData);
    MSR_CLEAR_MULTIDATA (pDefData);

    u4Len = ((STRLEN (gaSaveArray[u4Index].pOidString) < (sizeof (au1Oid) - 1))
             ? STRLEN (gaSaveArray[u4Index].pOidString) : (sizeof (au1Oid) -
                                                           1));
    MSR_STRNCPY (au1Oid, gaSaveArray[u4Index].pOidString, u4Len);
    au1Oid[u4Len] = '\0';

    SNMPConvertStringToOid (pCurOid, au1Oid, MSR_FALSE);
    if (SNMPGet (*pCurOid, pData, &u4Error, pCurIndex,
                 SNMP_DEFAULT_CONTEXT) == SNMP_FAILURE)
    {
        return MSR_FAILURE;
    }

    i4RetVal = 0;
    if ((FN_VAL (u4Index)) != NULL)
    {
        i4RetVal = (FN_VAL (u4Index)) (pCurOid, NULL, pData);

        if ((i4RetVal != MSR_SAVE) && (i4RetVal != MSR_VOLATILE_SAVE))
        {
            return MSR_FAILURE;
        }
    }

    /* When the Default Value Save Option is disabled, get the
     * default value of the given Oid and compare it with the
     * value obtained in the SNMPGet
     */
    if (gIssSysGroupInfo.b1IssDefValSaveFlag == ISS_FALSE)
    {
        MSR_CLEAR_MULTIDATA (pDefData);
        i4SnmpVal = SNMPGetDefaultValueForObj (*pCurOid, pDefData, &u4Error);
        if ((i4SnmpVal == SNMP_FAILURE) &&
            (u4Error != SNMP_EXCEPTION_NO_DEF_VAL))
        {
            return MSR_FAILURE;
        }
        if (i4SnmpVal == SNMP_SUCCESS)
        {
            i4CmpResult = MSRCompareSnmpMultiDataValues (pData, pDefData);
            if (i4CmpResult == MSR_SUCCESS)
            {
                return MSR_FAILURE;
            }
        }
    }

    MsrUtilFillRbNode (pRbNode, u4Index, pCurOid, pData,
                       MSR_NONROWSTATUS, i4RetVal);

    return MSR_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmGetTableRSConfig                            */
/*                                                                          */
/*    Description        : This function obtains the table row status       */
/*                          configuration next available to the given oid   */
/*                          and instance. This function return failure      */
/*                          when the configuration fails in the validation  */
/*                          to send it to the stanby node. When entire row  */
/*                          of configuration is to be skipped the pu1Flag   */
/*                          is set to MSR_FALSE. When only the current      */
/*                          needs to be skipped, the pu1Flag is set to      */
/*                          MSR_TRUE.                                       */
/****************************************************************************/

INT4
MsrRmGetTableRSConfig (UINT4 u4Index, tSNMP_OID_TYPE * pCurOid,
                       tSNMP_OID_TYPE * pInstance, tMSRRbtree * pRbNode,
                       UINT1 *pu1Flag)
{
    tSNMP_OID_TYPE     *pOid;
    tSNMP_OID_TYPE     *pNextOid;
    tSnmpIndex         *pCurIndex = MsrRmIndexPool1;
    tSnmpIndex         *pNextIndex = MsrRmIndexPool2;
    tSNMP_MULTI_DATA_TYPE *pData;
    UINT4               u4Error = 0;
    INT4                i4RetVal = 0;
    UINT1               au1Oid[MSR_MAX_OID_LEN];
    UINT1               au1tOid[MSR_MAX_OID_LEN];
    UINT1               au1Instance[MSR_MAX_OID_LEN];

    pOid = &gMsrRmOidType[MSR_RM_OID5];
    pNextOid = &gMsrRmOidType[MSR_RM_OID6];
    pData = &gMsrRmMultiData[MSR_RM_MULTIDATA];

    MSR_CLEAR_OID (pOid);
    MSR_CLEAR_OID (pNextOid);
    MSR_CLEAR_MULTIDATA (pData);

    SNMPCopyOid (pOid, pCurOid);
    /* Append the latest instance to the pCurOid.
     * This is needed to get the next instance of
     * the object.
     */
    MsrAppendOid (pCurOid, pInstance);
    if (SNMPGetNextReadWriteOID (*pCurOid, pNextOid, pData,
                                 &u4Error, pCurIndex,
                                 pNextIndex,
                                 SNMP_DEFAULT_CONTEXT) != SNMP_SUCCESS)
    {
        *pu1Flag = MSR_FALSE;
        return MSR_FAILURE;
    }
    MsrGetInstanceOid (pOid, pNextOid, pInstance);
    if (pInstance->u4_Length == 0)
    {
        *pu1Flag = MSR_FALSE;
        return MSR_FAILURE;
    }
    i4RetVal = 0;
    pData->i2_DataType = SNMP_DATA_TYPE_INTEGER;
    pData->i4_SLongValue = MSR_CREATE_AND_WAIT;
    pData->u4_ULongValue = MSR_CREATE_AND_WAIT;
    if (FN_VAL (u4Index) != NULL)
    {
        i4RetVal = (FN_VAL (u4Index)) (pOid, pInstance, pData);
        if (i4RetVal == MSR_SKIP)
        {
            *pu1Flag = MSR_TRUE;
            return MSR_FAILURE;
        }
        else if (i4RetVal == MSR_NEXT)
        {
            *pu1Flag = MSR_FALSE;
            return MSR_FAILURE;
        }
    }
    pData->i4_SLongValue = (INT4) pData->u4_ULongValue;
    MSR_MEMSET (au1Instance, 0, MSR_MAX_OID_LEN);
    MSR_MEMSET (au1tOid, 0, MSR_MAX_OID_LEN);
    MsrConvertOidToString (pOid, au1Oid);
    MsrConvertOidToString (pInstance, au1Instance);
    sprintf ((char *) au1tOid, "%s.%s", au1Oid, au1Instance);
    MSR_CLEAR_OID (pOid);
    SNMPConvertStringToOid (pOid, au1tOid, MSR_FALSE);

    MsrUtilFillRbNode (pRbNode, u4Index, pOid, pData, MSR_ROWSTATUS, i4RetVal);

    return MSR_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmGetTableConfig                              */
/*                                                                          */
/*    Description        : This function obtains the table non row status   */
/*                          configuration next available to the given oid   */
/*                          and instance. This function return failure      */
/*                          when the configuration fails in the validation  */
/*                          to send it to the stanby node. When entire row  */
/*                          of configuration is to be skipped the pu1Flag   */
/*                          is set to MSR_FALSE. When only the current      */
/*                          needs to be skipped, the pu1Flag is set to      */
/*                          MSR_TRUE.                                       */
/****************************************************************************/
INT4
MsrRmGetTableConfig (UINT4 u4Index, tSNMP_OID_TYPE * pCurOid,
                     tSNMP_OID_TYPE * pInstance, tMSRRbtree * pRbNode,
                     tSNMP_MULTI_DATA_TYPE * pDefData,
                     UINT4 u4PrevOidValue, UINT1 *pu1Flag)
{
    tMbDbEntry         *pCur = NULL;
    tMbDbEntry         *pNext = NULL;
    tSnmpDbEntry        SnmpDbEntry;
    tSNMP_OID_TYPE     *pOid;
    tSNMP_OID_TYPE     *pNextOid;
    tSnmpIndex         *pCurIndex = MsrRmIndexPool1;
    tSnmpIndex         *pNextIndex = MsrRmIndexPool2;
    tSNMP_MULTI_DATA_TYPE *pData;
    UINT4               u4Error = 0;
    UINT4               u4Len = 0;
    INT4                i4RetVal = 0;
    static INT4         i4SnmpVal = 0;
    static UINT4        u4SnmpErr = 0;
    INT4                i4CmpResult = MSR_FAILURE;
    UINT1               au1Oid[MSR_MAX_OID_LEN];
    UINT1               au1tOid[MSR_MAX_OID_LEN];
    UINT1               au1Instance[MSR_MAX_OID_LEN];

    pOid = &gMsrRmOidType[MSR_RM_OID5];
    pNextOid = &gMsrRmOidType[MSR_RM_OID6];
    pData = &gMsrRmMultiData[MSR_RM_MULTIDATA];

    MSR_CLEAR_OID (pOid);
    MSR_CLEAR_OID (pNextOid);
    MSR_CLEAR_MULTIDATA (pData);

    SNMPCopyOid (pOid, pCurOid);
    /* Append the latest instance to the pCurOid.
     * This is needed to get the next instance of
     * the object.
     */
    MsrAppendOid (pCurOid, pInstance);
    if (SNMPGetNextReadWriteOID (*pCurOid, pNextOid, pData,
                                 &u4Error, pCurIndex,
                                 pNextIndex,
                                 SNMP_DEFAULT_CONTEXT) != SNMP_SUCCESS)
    {
        *pu1Flag = MSR_FALSE;
        return MSR_FAILURE;
    }

    MsrGetInstanceOid (pOid, pNextOid, pInstance);
    if (pInstance->u4_Length == 0)
    {
        *pu1Flag = MSR_FALSE;
        return MSR_FAILURE;
    }

    /* Read-only objects need not be sent to the standby nodes */
    GetMbDbEntry (*pNextOid, &pNext, &u4Len, &SnmpDbEntry);
    pCur = SnmpDbEntry.pMibEntry;

    if (pCur != NULL)
    {
        if (pCur->Set == NULL)
        {
            *pu1Flag = MSR_TRUE;
            return MSR_FAILURE;
        }
    }
    i4RetVal = 0;
    if (FN_VAL (u4Index) != NULL)
    {
        i4RetVal = (FN_VAL (u4Index)) (pOid, pInstance, pData);
        if (i4RetVal == MSR_SKIP)
        {
            *pu1Flag = MSR_TRUE;
            return MSR_FAILURE;
        }
        else if (i4RetVal == MSR_NEXT)
        {
            *pu1Flag = MSR_FALSE;
            return MSR_FAILURE;
        }
    }

    MSR_MEMSET (au1Instance, 0, MSR_MAX_OID_LEN);
    MSR_MEMSET (au1tOid, 0, MSR_MAX_OID_LEN);
    MsrConvertOidToString (pOid, au1Oid);
    MsrConvertOidToString (pInstance, au1Instance);
    sprintf ((char *) au1tOid, "%s.%s", au1Oid, au1Instance);
    MSR_CLEAR_OID (pOid);
    SNMPConvertStringToOid (pOid, au1tOid, MSR_FALSE);

    /* When the Default Value Save Option is disabled, get the
     * default value of the given Oid and compare it with the
     * value obtained in the SNMPGet
     */
    if (gIssSysGroupInfo.b1IssDefValSaveFlag == ISS_FALSE)
    {
        if (u4PrevOidValue != pInstance->pu4_OidList[0])
        {
            MSR_CLEAR_MULTIDATA (pDefData);

            i4SnmpVal = SNMPGetDefaultValueForObj (*pOid, pDefData, &u4SnmpErr);
            if ((i4SnmpVal == SNMP_FAILURE) &&
                (u4SnmpErr != SNMP_EXCEPTION_NO_DEF_VAL))
            {
                *pu1Flag = MSR_TRUE;
                return MSR_FAILURE;
            }
        }
        if (i4SnmpVal == SNMP_SUCCESS)
        {
            i4CmpResult = MSRCompareSnmpMultiDataValues (pData, pDefData);

            if (i4CmpResult == MSR_SUCCESS)
            {
                *pu1Flag = MSR_TRUE;
                return MSR_FAILURE;
            }
        }
    }

    MsrUtilFillRbNode (pRbNode, u4Index, pOid, pData,
                       MSR_NONROWSTATUS, i4RetVal);

    return MSR_SUCCESS;
}

/************************************************************************
 *  Function Name   : MsrRmAllocateBuffer        
 *  Description     : Allocate a Linear buffer used for sending RM messages
 *  Input           : None
 *  Output          : None
 *  Returns         : Allocated buffer pointer             
 ************************************************************************/
UINT1              *
MsrRmAllocateBuffer (VOID)
{
    UINT1              *pu1Buf = NULL;
    pu1Buf = MemAllocMemBlk (gMSRRmGlobalInfo.MsrRmPktPoolId);
    return (pu1Buf);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRMGetNodeStatus                               */
/*                                                                          */
/*    Description        : This function returns the RM node                */
/*                           status.                                        */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : returns RM_ACTIVE/RM_STANDBY                     */
/****************************************************************************/
UINT1
MsrRMGetNodeStatus (VOID)
{
    return (gu1MsrNodeStatus);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRmNvramPreSyncUp                              */
/*                                                                          */
/*    Description        : This function used to Pre-Syncup the Nvram       */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/****************************************************************************/

VOID
MsrRmNvramPreSyncUp (VOID)
{
#ifdef IGS_WANTED
    UINT4               u4SnoopFwdMode = 0;
    u4SnoopFwdMode = IssGetSnoopFwdModeFromNvRam ();
    SnoopNotifyMcastMode (u4SnoopFwdMode);
#endif
}

#endif /* _MSR_RM_C_ */
