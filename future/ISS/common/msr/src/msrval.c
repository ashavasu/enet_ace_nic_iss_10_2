/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                         */
/* $Id: msrval.c,v 1.315 2018/01/11 11:19:04 siva Exp $*/
/*****************************************************************************/
/*    FILE  NAME            : msrval.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           : MSR                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*    DESCRIPTION           : This file has validation functions             */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#ifndef _MSR_VAL_C_
#define _MSR_VAL_C_

#include "msrsys.h"
#include "msr.h"
#include "tftpc.h"
#include "ifmmacs.h"
#include "msrtrap.h"
#ifdef SNMP_WANTED
#include "snmptdfs.h"
#include "snmptrap.h"
#include "snmputil.h"
#include "fssnmp.h"
#endif
#ifdef QOSX_WANTED
#include "qosxtd.h"
#endif
#ifdef DCBX_WANTED
#include "dcbx.h"
#endif
#ifdef MBSM_WANTED
#include "mbsm.h"
#endif
#include "ipvx.h"
#include "isspi.h"
#include "bgp.h"
#include "rtm.h"
#ifdef WLC_WANTED
#include "wsswlan.h"
#include "wsscfgcli.h"
#endif
extern INT1 nmhGetIssAclL2FilterCreationMode ARG_LIST ((INT4, INT4 *));
extern INT1 nmhGetIssAclL3FilterCreationMode ARG_LIST ((INT4, INT4 *));
extern INT1         nmhGetFsMIIpv6PmtuTimeStamp (INT4,
                                                 tSNMP_OCTET_STRING_TYPE *,
                                                 INT4 *);
extern INT1         nmhGetFsSynceIfIsRxQLForced (INT4 i4FsSynceIfIndex,
                                                 INT4
                                                 *pi4RetValFsSynceIfIsRxQLForced);

extern INT1 nmhGetFsLaDLAGSystemID ARG_LIST ((tMacAddr *));

extern INT1 nmhGetFsLaMCLAGSystemID ARG_LIST ((tMacAddr *));
extern INT1         nmhGetFsLaMCLAGSystemStatus (INT4 *);
extern INT1         nmhGetFsLaDLAGSystemStatus (INT4 *);

extern INT1         nmhGetFsMIStdOspfASBdrRtrStatus (INT4, INT4 *);

extern UINT1 QosUtlGetClassMapEntryType PROTO ((UINT4 u4ClassMapId,
                                                INT4 *pi4EntryType));

extern UINT1 QosUtlGetPriMapEntryType PROTO ((UINT4 u4PriMapId,
                                              INT4 *pi4EntryType));

extern UINT1 QosUtlGetPlyMapEntryType PROTO ((UINT4 u4PlyMapId,
                                              INT4 *pi4EntryType));

#ifdef BGP_WANTED
extern INT4         BgpCheckBgpClusterId
PROTO ((UINT4 u4Context, tSNMP_OCTET_STRING_TYPE * pClusterId));
#endif

#ifdef VPLSADS_WANTED
extern INT1 nmhGetPwOwner ARG_LIST ((UINT4, INT4 *));
#endif
static tMbsmSlotInfo MbsmSlotInfo;
/************************************************************************
 *  Function Name   : MsrValidateIfIpEntry
 *  Description     : This function validates the ifIpEntry objects.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateIfIpEntry (tSNMP_OID_TYPE * pOid,
                      tSNMP_OID_TYPE * pInstance, tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetVal = 0;
    UINT1               u1RetVal = 0;
    tCfaIfInfo          CfaIfInfo;

    UNUSED_PARAM (pOid);
#ifdef WLC_WANTED
    INT4                i4ApGroupEnableStatus = APGROUP_DISABLE;
    INT4                i4RetValIfMainType = 0;

    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainType);
    if (i4RetValIfMainType == CFA_CAPWAP_DOT11_BSS)
    {
        nmhGetFsApGroupEnabledStatus (&i4ApGroupEnableStatus);
        if ((UINT4) i4ApGroupEnableStatus == APGROUP_ENABLE)
        {
            return (MSR_SKIP);
        }
    }
#endif
    if (pInstance->u4_Length == 1)
    {
        return (MSR_SKIP);
    }
    i4RetVal = CfaGetIfInfo ((UINT4) pInstance->pu4_OidList[1], &CfaIfInfo);

    /* If the interface is a bridged interface then no need to save the
       ip address allocation method */
    if ((i4RetVal != CFA_FAILURE) && (CfaIfInfo.u1BridgedIface == CFA_ENABLED)
        && (pInstance->pu4_OidList[0] == 1))
    {
        return (MSR_SKIP);
    }
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        if (pInstance->pu4_OidList[1] == CFA_DEFAULT_STACK_IFINDEX)
        {
            /* Stack IP should be unique address.
             * It will be calculated from slot num.
             * it should not be shared between Peers*/
            return (MSR_SKIP);
        }
    }

    if (CfaIfGetIpAllocMethod ((UINT4) pInstance->pu4_OidList[1],
                               &u1RetVal) == CFA_SUCCESS)
    {
        /* 1. When the address allocation method is not set 
         *    to manual and the object is not one of 
         *    ifIpAddrAllocMethod or ifIpForwardingEnable, 
         *    it will not be stored.
         */
        if ((u1RetVal != CFA_IP_ALLOC_MAN) &&
            (u1RetVal != 0) &&
            (pInstance->pu4_OidList[0] != 1) &&
            (pInstance->pu4_OidList[0] != 5) &&
            (pInstance->pu4_OidList[0] != 6))
        {
            return (MSR_SKIP);
        }
        else
        {
            /* 2. The following objects will not be stored if
             *    the IP address / mask is set to 0.0.0.0 in 
             *    incremental save off mode:
             *    IfIpAddr          : 1.3.6.1.4.1.2076.27.1.5.1.2
             *    IfIpSubnetMask    : 1.3.6.1.4.1.2076.27.1.5.1.3
             *    IfIpBroadcastAddr : 1.3.6.1.4.1.2076.27.1.5.1.4
             *    In incremental save on mode, the ipaddr, subnetmask,
             *    broadcast addr will be set to zero when an already 
             *    configured value is reset. This has to be saved.
             */
            if (gMSRIncrSaveFlag != ISS_TRUE)
            {
                if ((pData->i2_DataType == IPADDRESS) &&
                    (pData->u4_ULongValue == 0))
                {
                    return (MSR_SKIP);
                }
            }
            if ((CfaIfInfo.u1IfType == CFA_PPP) &&
                (pData->i2_DataType == IPADDRESS))
            {
                return (MSR_SKIP);
            }
        }
    }
    return (MSR_SAVE);
}

#ifdef RMON_WANTED
/************************************************************************
 *  Function Name   : MsrValidateRmonTables
 *  Description     : This function validates the RMON tables
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateRmonTables (tSNMP_OID_TYPE * pOid,
                       tSNMP_OID_TYPE * pInstance,
                       tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pInstance);

    /* RMON tables use the EntryStatus instead of RowStatus.
     * So for these tables the equivalent of a create and wait 
     * action will be done by the create request.
     */

    if (pOid->pu4_OidList[10] != 0)
    {
        pData->i2_DataType = SNMP_DATA_TYPE_INTEGER;
        pData->u4_ULongValue = MSR_CREATE_REQUEST;
    }

    return (MSR_SAVE);
}

#endif
#ifdef RIP_WANTED

/************************************************************************
 *  Function Name   : MsrValidaterip2IfConfAuthKey 
 *  Description     : This function validates the rip2IfConfAuthKey.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidaterip2IfConfAuthKey (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4IpAddress = 0;
    UINT1               au1Array[MSR_MAX_IP_ADDR_LEN];
    UINT1               au1CopyString[MSR_MAX_OID_LEN];
    UINT4               u4Count = 1;

    UNUSED_PARAM (pData);
    UNUSED_PARAM (pOid);

    MSR_MEMSET (au1Array, 0, sizeof (au1Array));
    MSR_MEMSET (au1CopyString, 0, sizeof (au1CopyString));
    /* Get the value of rip2IfConfAuthKey from the rip data structure before 
     * MSR save*/
    if (pInstance->pu4_OidList[0] == 4)
    {
        /* IP ADDRESS */

        for (u4Count = 1; u4Count < 4; u4Count++)
        {
            SPRINTF ((char *) au1Array, "%d.", pInstance->pu4_OidList[u4Count]);

            MSR_STRNCAT (au1CopyString, au1Array,
                         (sizeof (au1CopyString) - STRLEN (au1CopyString) - 1));
        }

        SPRINTF ((char *) au1Array, "%d", pInstance->pu4_OidList[u4Count]);
        MSR_STRNCAT (au1CopyString, au1Array,
                     (sizeof (au1CopyString) - STRLEN (au1CopyString) - 1));

        u4IpAddress = OSIX_NTOHL (INET_ADDR (au1CopyString));

        if (RipIfGetConfAuthKey (u4IpAddress, pData->pOctetStrValue) !=
            MSR_TRUE)
        {

            return (MSR_SKIP);
        }

    }
    return (MSR_SAVE);
}
#endif

#ifdef OSPF_WANTED

/************************************************************************
 *  Function Name   : MsrValidateFsMIStdOspfEntry
 *  Description     : This function validates the fsMIStdOspfEntry table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateFsMIStdOspfEntry (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pInstance);

    /* Currently OSPF does not support CREATE_AND_WAIT, so while 
       saving this table set the row status to CREATE_AND_GO or to
       active , so that while restoring this table will not be 
       called with CREATE_AND_WAIT, here we have used CREATE_AND_GO */

    /* skip the saving of this object, when it is ACTIVE */
    if (pOid->pu4_OidList[11] == 16)
    {
        if (pData->u4_ULongValue == MSR_ACTIVE)
        {
            return (MSR_SKIP);
        }
        pData->u4_ULongValue = MSR_CREATE_AND_GO;
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsMIOspfRouterIdPermanence
 *  Description     : This function validates the fsMIOspfEntry table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateFsMIOspfRouterIdPermanence (tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pData);
    UNUSED_PARAM (pOid);
    /* Since ospfRouterId object value depends on 
     * fsMIOspfRouterIdPermanence object, we are saving here 
     * fsMIOspfRouterIdPermanence object value before saving 
     * ospfRouterId object */
    if (pInstance->pu4_OidList[0] != 32)
    {
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsMIOspfRestartSupport
 *  Description     : This function validates the fsMIOspfEntry table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateFsMIOspfRestartSupport (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pData);
    UNUSED_PARAM (pOid);
    /* Since fsMIOspfOpaqueOption object value depends on 
     * FsMIOspfRestartSupport object, we are saving here 
     * FsMIOspfRestartSupport object value after saving 
     * fsMIOspfOpaqueOption object */
    if (pInstance->pu4_OidList[0] != 19)
    {
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsMIOspfEntry
 *  Description     : This function validates the fsMIOspfEntry table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateFsMIOspfEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pData);
    UNUSED_PARAM (pOid);
    /* All objects in the fsMIOspfTable are saving here, except 
     * fsMIOspfRouterIdPermanence object because this object 
     * already been saved */
    if ((pInstance->pu4_OidList[0] == 19) || (pInstance->pu4_OidList[0] == 32))
    {
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidatefsMIStdOspfIfAuthKey 
 *  Description     : This function validates the fsMIStdOspfIfAuthKey.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidatefsMIStdOspfIfAuthKey (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData)
{

    UNUSED_PARAM (pData);
    UNUSED_PARAM (pOid);
    OspfLock ();

    /* Get the value of fsMIStdOspfIfAuthKey from the data structure before MSR
     * save */
    if ((pInstance->pu4_OidList[0] == 16) && (pOid->u4_Length != 11))
    {
        if ((GetfsMIStdOspfIfAuthKey (pInstance->pu4_OidList[1],
                                      pInstance->pu4_OidList[2],
                                      pInstance->pu4_OidList[3],
                                      pData->pOctetStrValue)) != MSR_TRUE)
        {
            OspfUnLock ();
            return (MSR_SKIP);
        }
    }
    OspfUnLock ();
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsMIOspfAreaEntry
 *  Description     : This function validates the fsMIOspfAreaEntry table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateFsMIOspfAreaEntry (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetVal = 0;
    UINT4               u4OspfCxtId;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    OspfLock ();
    u4OspfCxtId = pInstance->pu4_OidList[1];
    /* Set the Ospf Cxt ID to Default OSPF Cxt ID */
    if (UtilOspfSetContext (u4OspfCxtId) == SNMP_FAILURE)
    {
        OspfUnLock ();
        return (MSR_SKIP);
    }
    /* The default area 0.0.0.0 is used for the backbone. When OSPF
     * is not enabled, this area entry should not be saved.
     */

    if ((nmhGetOspfAdminStat (&i4RetVal) == SNMP_SUCCESS) &&
        (i4RetVal != OSPF_ENABLED))
    {
        UtilOspfResetContext ();
        OspfUnLock ();
        return (MSR_SKIP);
    }

    UtilOspfResetContext ();
    OspfUnLock ();
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsMIStdOspfNbrEntry
 *  Description     : This function validates the fsMIStdOspfNbrEntry table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateFsMIStdOspfNbrEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{

    UINT4               u4IpAddress = 0;
    UINT1               au1Array[MSR_MAX_IP_ADDR_LEN];
    UINT1               au1CopyString[MSR_MAX_OID_LEN];
    UINT4               u4Count = 1;
    UINT4               u4EndCount = 0;
    INT4                i4NbrAddressLessIndex = 0;
    INT4                i4Permanence = 0;
    UINT4               u4OspfCxtId;

    UNUSED_PARAM (pData);
    UNUSED_PARAM (pOid);
    OspfLock ();
    MSR_MEMSET (au1Array, 0, sizeof (au1Array));
    MSR_MEMSET (au1CopyString, 0, sizeof (au1CopyString));
    if (pOid->u4_Length == 10)
    {
        u4OspfCxtId = pInstance->pu4_OidList[1];
        u4Count = 2;
    }
    else
    {
        u4OspfCxtId = pInstance->pu4_OidList[0];
    }

    u4EndCount = u4Count + 3;
    if (UtilOspfSetContext (u4OspfCxtId) == SNMP_FAILURE)
    {
        OspfUnLock ();
        return (MSR_SKIP);
    }
    /* Check whether the route type is static. If so the route should be stored. 
     * All other routes should not be saved.
     */

    /* IP ADDRESS */

    for (; u4Count < u4EndCount; u4Count++)
    {
        SPRINTF ((char *) au1Array, "%d.", pInstance->pu4_OidList[u4Count]);
        STRNCAT (au1CopyString, au1Array,
                 (sizeof (au1CopyString) - STRLEN (au1CopyString) - 1));
    }

    SPRINTF ((char *) au1Array, "%d", pInstance->pu4_OidList[u4Count]);
    STRNCAT (au1CopyString, au1Array,
             (sizeof (au1CopyString) - STRLEN (au1CopyString) - 1));

    u4IpAddress = OSIX_NTOHL (INET_ADDR (au1CopyString));

    MSR_MEMSET (&au1CopyString, 0, (sizeof (au1CopyString)));

    u4Count++;
    i4NbrAddressLessIndex = (INT4) pInstance->pu4_OidList[u4Count];

    if (nmhGetOspfNbmaNbrPermanence (u4IpAddress,
                                     i4NbrAddressLessIndex,
                                     &i4Permanence) != SNMP_FAILURE)
    {
        if (i4Permanence != OSPF_DYNAMIC)
        {
            OspfUnLock ();
            return (MSR_SAVE);
        }
    }
    OspfUnLock ();
    return (MSR_SKIP);

}

/************************************************************************
 *  Function Name   : MsrValidateFsMIStdOspfAreaEntry
 *  Description     : This function validates the fsMIStdOspfAreaEntry table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateFsMIStdOspfAreaEntry (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4Cnt1 = 1;
    UINT4               u4DefBkBoneArea[MSR_MAX_IP_ADDR_LEN] = { 0, 0, 0, 0 };

    UNUSED_PARAM (pData);
    UNUSED_PARAM (pOid);
    /* The default area 0.0.0.0 is used for the backbone. Hence                        this area entry should not be saved.
     */
    /* For fsMIStdOspfAreaTable, first object that needs to be saved is
     * fsMIStdOspfAreaStatus with value MSR_CREATE_AND_GO. When fsMIStdOspfAreaStatus
     * with value MSR_CREATE_AND_GO. needs to be saved, this routine is called
     * with pOid containing OID of fsMIStdOspfAreaStatus, pInstance containing
     * current index, pData containing value MSR_CREATE_AND_GO.. For all other objects
     * this routine is called with pOid containing OID of fsMIStdOspfAreaEntry, 
     * pInstance containing OID of particular object and current index, 
     * pData containing value.
     *
     */

    if (pOid->u4_Length != 11)
    {
        u4Cnt1 = 2;
    }

    if (MSR_MEMCMP
        ((pInstance->pu4_OidList + u4Cnt1), u4DefBkBoneArea,
         MSR_MAX_IP_ADDR_LEN) == 0)
    {
        return MSR_SKIP;
    }
    if (pOid->pu4_OidList[10] == 10)
    {
        if (pData->u4_ULongValue == MSR_ACTIVE)
        {
            return (MSR_SKIP);
        }
        pData->u4_ULongValue = MSR_CREATE_AND_GO;
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateMIOspfRRDRouteEntry
 *  Description     : This function validates the fsMIOspfRRDRouteEntry table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateMIOspfRRDRouteEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4Status = 0;
    UINT4               u4ContextId = 0;
    u4ContextId = pInstance->pu4_OidList[1];
    UNUSED_PARAM (pData);
    UNUSED_PARAM (pOid);

    if (pInstance->pu4_OidList[0] == 4)
    {                            /*if the router is not asbr router dont allow to set rrd routemap enable */
        nmhGetFsMIStdOspfASBdrRtrStatus (u4ContextId, &i4Status);

        if (i4Status != CFA_TRUE)    /*OSPF_TRUE 1 */
        {
            return MSR_SKIP;
        }
    }
    return MSR_SAVE;
}
#endif

#ifdef OSPF3_WANTED
/************************************************************************
 *  Function Name   : MsrValidateFsMIStdOspfv3Entry
 *  Description     : This function validates the fsMIStdOspfv3Entry table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateFsMIStdOspfv3Entry (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData)
{

    UNUSED_PARAM (pData);
    UINT4               u4OspfCxtId;
    INT4                i4RouterIdStatus = 0;
    u4OspfCxtId = pInstance->pu4_OidList[1];

    UtilGetOspfv3RouterIdPermanence (&i4RouterIdStatus, u4OspfCxtId);
    if ((i4RouterIdStatus == OSPFV3_ROUTERID_DYNAMIC) &&
        (pInstance->pu4_OidList[0] == 2))
    {
        /* if router id is dynamically elected no need to save 
         * the routerid, it will be save via RouterId permenance mib */
        return (MSR_SKIP);
    }
    if (pOid->u4_Length == OID_LENGTH_THIRTEEN)
    {
        if (pInstance->pu4_OidList[0] == OSPFV3_DEFAULT_CXT_ID)
        {
            return (MSR_SKIP);
        }
    }

    if (pInstance->u4_Length == 2)
    {
        /* Skip for fsMIStdOspfv3TrafficEngineeringSupport
         * fsMIStdOspfv3MulticastExtensions */
        if (pInstance->pu4_OidList[0] == 13)
        {
            return (MSR_SKIP);
        }
        if (pInstance->pu4_OidList[0] == 16)
        {
            return (MSR_SKIP);
        }
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsMIStdOspfv3AdminStat
 *  Description     : This function validates the fsMIStdOspfv3Entry table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateFsMIStdOspfv3AdminStat (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{

    UNUSED_PARAM (pData);
    UNUSED_PARAM (pOid);

    if (pInstance->pu4_OidList[0] == 3)
    {
        /* if router id is dynamically elected to restore the routerid
         * Have to save the admin status again so that dynamic router id 
         * election happens*/
        return (MSR_SAVE);
    }
    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateOspfv3AreaEntry
 *  Description     : This function validates the Ospfv3AreaEntry table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateOspfv3AreaEntry (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4Cnt1 = 1;
    UINT4               u4DefBkBoneArea[MSR_MAX_IP_ADDR_LEN] = { 0, 0, 0, 0 };

    UNUSED_PARAM (pData);
    /* The default area 0.0.0.0 is used for the backbone. Hence
     * this area entry should not be saved.
     */

    if (pOid->u4_Length != 13)
    {
        u4Cnt1 = 2;
    }

    if (MSR_MEMCMP
        ((pInstance->pu4_OidList + u4Cnt1), u4DefBkBoneArea,
         MSR_MAX_IP_ADDR_LEN) == 0)
    {
        return MSR_SKIP;
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsMIOspfv3RRDRouteEntry
 *  Description     : This function validates the Ospfv3RRDRouteEntry table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateFsMIOspfv3RRDRouteEntry (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetValOspfv3ASBdrRtrStatus;
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (pInstance->pu4_OidList[0] == 3)
    {
        nmhGetOspfv3ASBdrRtrStatus (&i4RetValOspfv3ASBdrRtrStatus);

        if (i4RetValOspfv3ASBdrRtrStatus == OSPFV3_DISABLED)
        {
            return (MSR_SKIP);
        }
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsMIOspfv3RouterIdPermanence
 *  Description     : This function saves FsMIOspfv3RouterIdPermanence
 *                    value alone
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateFsMIOspfv3RouterIdPermanence (tSNMP_OID_TYPE * pOid,
                                         tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    /* Since ospfv3RouterId object value depends on
     * fsMIOspfv3RouterIdPermanence object, we are saving here
     * fsMIOspfv3RouterIdPermanence object value before saving
     * ospfRouterId object */
    if (pInstance->pu4_OidList[0] != 19)
    {
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsMIOspfv3Entry
 *  Description     : This function saves FsMIOspfv3Entry
 *                    
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateFsMIOspfv3Entry (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    /* All objects in the fsMIOspfv3Table are saving here, except 
     *fsMIOspfv3RouterIdPermanence object because this object
     * already been saved */

    if (pInstance->pu4_OidList[0] == 19)
    {
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}
#endif

/************************************************************************
 *  Function Name   : MsrDisableHttpStatusBeforeHttpPortChange
 *  Description     : This function sets the HTTP status to disable before
 *                    setting the HTTP port 
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrDisableHttpStatusBeforeHttpPortChange (tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pInstance,
                                          tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetVal;
    INT4                i4Port = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);

    if ((nmhGetIssHttpStatus (&i4RetVal) == SNMP_SUCCESS) &&
        (i4RetVal != ISS_HTTP_STATUS_DISABLED))
    {
        if ((nmhGetIssHttpPort (&i4Port) == SNMP_SUCCESS) &&
            (i4Port != ISS_DEF_HTTP_PORT))
        {
            pData->i4_SLongValue = ISS_HTTP_STATUS_DISABLED;
            return (MSR_SAVE);
        }
    }

    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateIssMgmtInterfaceRouting
 *  Description     : This function checks whether we can Enable routing
 *                    over Management port Interface
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateIssMgmtInterfaceRouting (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pData);

    if (IssGetMgmtPortFromNvRam () != TRUE)
    {
        return (MSR_SKIP);
    }

    return (MSR_SAVE);
}

#ifdef DIFFSRV_WANTED
/************************************************************************
 *  Function Name   : MsrValidateQoSSystemControl
 *  Description     : This function validates the MPLS objects before
 *                    saving any MPLS objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None 
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateQoSSystemControl (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pData);
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateQoSSystemStatus
 *  Description     : This function validates the MPLS objects before
 *                    saving any MPLS objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateQoSSystemStatus (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pData);

    return (MSR_SAVE);
}
#endif
#ifdef QOSX_WANTED
/************************************************************************
 *  Function Name   : MsrValidateQosxSkipPriorityMapStatus
 *  Description     : This function validates the QOS objects before
 *                    saving any QOS objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateQosxSkipPriorityMapStatus (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetVal = 0;
    UNUSED_PARAM (pData);

    /* For fsQoSPolicyMapTable , fsQoSPolicyMapId is the only index.
     * In this routine we have to skip only the Rowstatus.
     * Length of the oid is 13 while for Create and Wait this will be 13+1.
     * For CnW save the RS, else skip saving the RS to allow the new object
     * added below RS (position 10) to be saved before setting the row to active.
     */
    /* SKIP the implicit entries created while configuring the PCP Model */

    if (pOid->u4_Length == 14)
    {
        if (QosUtlGetPriMapEntryType (pInstance->pu4_OidList[0], &i4RetVal)
            == QOS_SUCCESS)
        {
            if (i4RetVal == 1)
            {
                return (MSR_SKIP);
            }
        }
        return (MSR_SAVE);
    }
    else
    {
        if (pOid->u4_Length == 13)
        {
            if (QosUtlGetPriMapEntryType (pInstance->pu4_OidList[1], &i4RetVal)
                == QOS_SUCCESS)
            {
                if (i4RetVal == 1)
                {
                    return (MSR_SKIP);
                }
            }
            if (pInstance->pu4_OidList[0] == 10)
            {
                return (MSR_SKIP);
            }
        }
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateQosxSavePriorityMapStatus
 *  Description     : This function validates the QOS objects before
 *                    saving any QOS objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateQosxSavePriorityMapStatus (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetVal = 0;
    UNUSED_PARAM (pData);

    /* For fsQoSPolicyMapTable , fsQoSPolicyMapId is the only index.
     * In this routine we have to skip only the Rowstatus.
     * Length of the oid is 13 while for Create and Wait this will be 13+1.
     * For CnW save the RS, else skip saving the RS to allow the new object
     * added below RS (position 10) to be saved before setting the row to active.
     */
    /* SKIP the implicit entries created while configuring the PCP Model */

    if (pOid->u4_Length == 14)
    {
        if (QosUtlGetPriMapEntryType (pInstance->pu4_OidList[0], &i4RetVal)
            == QOS_SUCCESS)
        {
            if (i4RetVal == 1)
            {
                return (MSR_SKIP);
            }
        }
        return (MSR_SKIP);
    }
    else
    {
        if (pOid->u4_Length == 13)
        {
            if (QosUtlGetPriMapEntryType (pInstance->pu4_OidList[1], &i4RetVal)
                == QOS_SUCCESS)
            {
                if (i4RetVal == 1)
                {
                    return (MSR_SKIP);
                }
            }
            if (pInstance->pu4_OidList[0] == 10)
            {
                return (MSR_SAVE);
            }
        }
    }
    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateQoSClassMapTable
 *  Description     : This function validates the MPLS objects before
 *                    saving any MPLS objects.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateQoSClassMapTable (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4ClassId = 0;
    BOOL1               b1IsClsMapConfFromExt = FALSE;

    UNUSED_PARAM (pData);

    if (pOid->u4_Length == 14)
        u4ClassId = pInstance->pu4_OidList[0];
    else
        u4ClassId = pInstance->pu4_OidList[1];

    /* Using u4ClassId, validate the config flag done through QOS or 
     * other protocol */
    QosApiValidateClsMapConfigFlag (u4ClassId, &b1IsClsMapConfFromExt);
    if (b1IsClsMapConfFromExt == TRUE)
    {
        /* SKIP MSR_SAVE for the configurations done through other protocol */
        return MSR_SKIP;
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateQosxSkipPolicyRowStatus
 *  Description     : This function skips the storage of PG row status.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateQosxSkipPolicyRowStatus (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4PolicyId = 0;
    INT4                i4RetVal = 0;
    BOOL1               b1IsPlyMapConfFromExt = FALSE;

    UNUSED_PARAM (pData);

    /* For fsQoSPolicyMapTable , fsQoSPolicyMapId is the only index.
     * In this routine we have to skip only the Rowstatus.
     * Length of the oid is 13 while for Create and Wait this will be 13+1.
     * For CnW save the RS, else skip saving the RS to allow the new object
     * added below RS (position 37) to be saved before setting the row to active.
     */

    if (pOid->u4_Length == 14)
        u4PolicyId = pInstance->pu4_OidList[0];
    else
        u4PolicyId = pInstance->pu4_OidList[1];

    /* Using u4PolicyId, validate the config flag done through QOS or 
     * other protocol */
    QosApiValidatePlyMapConfigFlag (u4PolicyId, &b1IsPlyMapConfFromExt);
    if (b1IsPlyMapConfFromExt == TRUE)
    {
        /* SKIP MSR_SAVE for the configurations done through other protocol */
        return MSR_SKIP;
    }

    if (pOid->u4_Length == 14)
    {
        if (QosUtlGetPlyMapEntryType (u4PolicyId, &i4RetVal) == QOS_SUCCESS)
        {
            /*Skipping IMPLICIT Entries */
            if (i4RetVal == 1)
            {
                return (MSR_SKIP);
            }
        }

        return MSR_SAVE;
    }
    else
    {
        if (QosUtlGetPlyMapEntryType (u4PolicyId, &i4RetVal) == QOS_SUCCESS)
        {
            /*Skipping IMPLICIT Entries */
            if (i4RetVal == 1)
            {
                return (MSR_SKIP);
            }
        }

        if (pInstance->pu4_OidList[0] == 37)
        {
            return (MSR_SKIP);
        }
    }

    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValidateQosSkipClassToPriority
 *  Description     : This function skips the storage of Class to priority
 *                    row status configured through other applications.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SKIP if just the current instance of the object
 *                             should not be saved
 ************************************************************************/
INT4
MsrValidateQosSkipClassToPriority (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4ClassId = 0;
    BOOL1               b1IsClsMapConfFromExt = FALSE;

    UNUSED_PARAM (pData);

    if (pOid->u4_Length == 14)
        u4ClassId = pInstance->pu4_OidList[0];
    else
        u4ClassId = pInstance->pu4_OidList[1];

    /* Using u4ClassId, validate the config flag done through QOS or 
     * other protocol */
    QosApiValidateClsMapConfigFlag (u4ClassId, &b1IsClsMapConfFromExt);
    if (b1IsClsMapConfFromExt == TRUE)
    {
        /* SKIP MSR_SAVE for the configurations done through other protocol */
        return MSR_SKIP;
    }

    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValidateQosxSavePolicyRowStatus
 *  Description     : This function skips the storage of PG row status.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateQosxSavePolicyRowStatus (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4PolicyId = 0;
    INT4                i4RetVal = 0;
    BOOL1               b1IsPlyMapConfFromExt = FALSE;

    UNUSED_PARAM (pData);

    /* For fsQoSPolicyMapTable , fsQoSPolicyMapId is the only index.
     * In this routine we have to save the Rowstatus.
     */
    if (pOid->u4_Length == 14)
        u4PolicyId = pInstance->pu4_OidList[0];
    else
        u4PolicyId = pInstance->pu4_OidList[1];

    QosApiValidatePlyMapConfigFlag (u4PolicyId, &b1IsPlyMapConfFromExt);
    if (b1IsPlyMapConfFromExt == TRUE)
    {
        return MSR_SKIP;
    }

    if (pOid->u4_Length == 14)
    {
        if (QosUtlGetPlyMapEntryType (u4PolicyId, &i4RetVal) == QOS_SUCCESS)
        {
            if (i4RetVal == 1)
            {
                return (MSR_SKIP);
            }
        }

        return MSR_SKIP;
    }
    else
    {
        if (QosUtlGetPlyMapEntryType (u4PolicyId, &i4RetVal) == QOS_SUCCESS)
        {
            if (i4RetVal == 1)
            {
                return (MSR_SKIP);
            }
        }

        if (pInstance->pu4_OidList[0] == 37)
        {
            return MSR_SAVE;
        }
    }

    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidateQosxSkipQSchedRowStatus
 *  Description     : This function skips the storage of PG row status.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateQosxSkipQSchedRowStatus (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pData);

    /* For fsQoSQTable table the Indices are Interface index and QId.
     * In this routine we have to skip only the Rowstatus.
     * Length of the oid is 13 while for Create and Wait this will be 13+1.
     * For CnW skip the RS     */
    if ((pOid->u4_Length == 14) && (pInstance->pu4_OidList[1] == 1))
    {
        return MSR_SKIP;
    }
    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValidateQosxSkipQRowStatus
 *  Description     : This function skips the storage of PG row status. 
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateQosxSkipQRowStatus (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* For fsQoSQTable table the Indices are Interface index and QId.
     * In this routine we have to skip only the Rowstatus.
     * Length of the oid is 13 while for Create and Wait this will be 13+1. 
     * For CnW save the RS, else skip saving the RS to allow the new object 
     * added below RS (position 7) to be saved before setting the row to active.
     */
    if (pOid->u4_Length == 14)
    {
        return MSR_SAVE;
    }
    else
    {
        if (pInstance->pu4_OidList[0] == 7)
        {
            return MSR_SKIP;
        }
    }
    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValidateQosxSkipClassMapRowStatus
 *  Description     : This function skips the storage of PG row status.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateQosxSkipClassMapRowStatus (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetVal = 0;
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (pOid->u4_Length == 14)
    {
        if (QosUtlGetClassMapEntryType ((UINT4) pInstance->pu4_OidList[0],
                                        &i4RetVal) == QOS_SUCCESS)
        {
            if (i4RetVal == 1)
            {
                return (MSR_SKIP);
            }
        }

        return MSR_SAVE;
    }
    else
    {
        if (QosUtlGetClassMapEntryType ((UINT4) pInstance->pu4_OidList[0],
                                        &i4RetVal) == QOS_SUCCESS)
        {
            if (i4RetVal == 1)
            {
                return (MSR_SKIP);
            }
        }

        if (pInstance->pu4_OidList[0] == 9)
        {
            return MSR_SKIP;
        }
    }
    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValidateQosxSaveClassMapRowStatus
 *  Description     : This function skips the storage of PG row status. 
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateQosxSaveClassMapRowStatus (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetVal = 0;
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* For fsQoSQTable table the Indices are Interface index and QId.
     * In this routine we have to save the Rowstatus.
     */

    if (pOid->u4_Length == 14)
    {
        if (QosUtlGetClassMapEntryType ((UINT4) pInstance->pu4_OidList[0],
                                        &i4RetVal) == QOS_SUCCESS)
        {
            /* SKIPPING the IMPLICIT Entry */
            if (i4RetVal == 1)
            {
                return (MSR_SKIP);
            }
        }

        return MSR_SKIP;
    }
    else
    {
        if (QosUtlGetClassMapEntryType ((UINT4) pInstance->pu4_OidList[0],
                                        &i4RetVal) == QOS_SUCCESS)
        {
            /* SKIPPING the IMPLICIT Entry */
            if (i4RetVal == 1)
            {
                return (MSR_SKIP);
            }
        }

        if (pInstance->pu4_OidList[0] == 9)
        {
            return MSR_SAVE;
        }
    }
    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidateQosxSaveQRowStatus
 *  Description     : This function skips the storage of PG row status. 
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateQosxSaveQRowStatus (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* For fsQoSQTable table the Indices are Interface index and QId.
     * In this routine we have to save the Rowstatus.
     */

    if (pOid->u4_Length == 14)
    {
        return MSR_SKIP;
    }
    else
    {
        if (pInstance->pu4_OidList[0] == 7)
        {
            return MSR_SAVE;
        }
    }
    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidateQoSDefUserPriorityEntry
 *  Description     : This function skips the storage of Defaul user 
 *                    value zero which is the default value.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateQoSDefUserPriorityEntry (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* If the default user priority value is zero which is the default value,
       +  * skip it */
    if ((pInstance->pu4_OidList[0] == 1) && (pInstance->pu4_OidList[2] == 0))
    {
        return MSR_SKIP;
    }
    return MSR_SAVE;

}

/************************************************************************
 *  Function Name   : MsrValidateQosHwCpuRateLimitEntry
 *  Description     : This function validates QosHwCpuRateLimitEntry the table
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateQosHwCpuRateLimitEntry (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pInstance);
    if (pOid->pu4_OidList[13] == 4)
    {
        /*qoshwcpurowstatus should not be allowed to set as create and wait */
        if (pData->i4_SLongValue == RTM_CREATE_AND_WAIT)
        {
            return MSR_SKIP;
        }
    }
    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValidateQoSRandomDetectCfgEntry
 *  Description     : This function validates fsQoSRandomDetectCfgEntry table
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateQoSRandomDetectCfgEntry (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);

    if ((pInstance->pu4_OidList[0] == 2) || (pInstance->pu4_OidList[0] == 3))
    {
        /*Skip saving of the MinTh and MaxTh as the values are dependent on
         * DropThresType. These value will be saved after saving DropThreshType */
        return MSR_SKIP;
    }

    if ((pInstance->pu4_OidList[0] == 7) && (pData->i4_SLongValue == ACTIVE))
    {
        /* Skip ACTIVE Row Status of fsQoSRandomDetectCfgEntry to allow saving of 
         * other objects introduced after RowStatus object */
        return MSR_SKIP;
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateQoSRandomDetectCfgSaveRowStatus 
 *  Description     : This function save the RowStatus of fsQoSRandomDetectCfgEntry table
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateQoSRandomDetectCfgSaveRowStatus (tSNMP_OID_TYPE * pOid,
                                            tSNMP_OID_TYPE * pInstance,
                                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    if ((pInstance->pu4_OidList[0] == 7) ||
        (pInstance->pu4_OidList[0] == 2) || (pInstance->pu4_OidList[0] == 3))

    {
        /* Save RowStatus, MinTH and MaxTH of fsQoSRandomDetectCfgEntry after all the other objects
         * of the table have been restored */
        return MSR_SAVE;
    }
    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidateQosPolicerStatsStatus 
 *  Description     : This function save the Status of Policer statistics table
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateQosPolicerStatsStatus (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* Saving the customer vlan counter status alone
     * others are skipped*/
    if (pInstance->pu4_OidList[0] == 7)
    {
        return MSR_SAVE;
    }

    return MSR_SKIP;
}

#endif
#ifdef EOAM_WANTED
/************************************************************************
 *  Function Name   : MsrValidateDot3OamEntry
 *  Description     : This function validates the Dot3Oam Entry
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateDot3OamEntry (tSNMP_OID_TYPE * pOid,
                         tSNMP_OID_TYPE * pInstance,
                         tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4IfIndex;
    UINT1               u1EthernetType;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* No need to save the Eoam Entry for StackPorts */
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        u4IfIndex = pInstance->pu4_OidList[1];
        CfaGetEthernetType (u4IfIndex, &u1EthernetType);
        if (u1EthernetType == CFA_STACK_ENET)
        {
            return (MSR_SKIP);
        }
        /* To avoid XE ports configurations in Stacking case */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateDot3OamLoopbackEntry 
 *  Description     : This function validates the Dot3OamLoopback Entry
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateDot3OamLoopbackEntry (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* Only dot3OamLoopbackIgnoreRx is saved, other objects 
     * are skipped, as this is the only object whose value can be changed*/

    if ((pInstance->pu4_OidList[0] != 2))
    {
        return (MSR_SKIP);
    }

    return (MSR_SAVE);

}

#ifdef EOAM_FM_WANTED
/************************************************************************
 *  Function Name   : MsrValidateFmLoopbackEntry 
 *  Description     : This function validates the EOAM-FM Loopback Entry
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateFmLoopbackEntry (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* Only the objects fsFmLBTestPattern, fsFmLBTestPktSize,  
     * fsFmLBTestCount and fsFmLBTestWaitTime are saved others are skipped.
     * Most of the objects in fsFmLoopBackEntryTable are read-only so they 
     * are skipped here itself, instead of letting msr to take care
     * and fsFmLBTestCommand, a read-write object is also not saved 
     * as the object returns same value on a read*/

    if ((pInstance->pu4_OidList[0] != 2) && (pInstance->pu4_OidList[0] != 3) &&
        (pInstance->pu4_OidList[0] != 4) && (pInstance->pu4_OidList[0] != 5))
    {
        return (MSR_SKIP);
    }

    return (MSR_SAVE);

}

/************************************************************************
 *  Function Name   : MsrValidateFmVarRetrievalEntry 
 *  Description     : This function validates EOAM-FM VarRetrievalEntry
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateFmVarRetrievalEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* Only the objects fsFmVarRetrievalMaxVar and fsFmVarRetrievalRequest
     * are saved. fsFmVarRetrievalClearResponse is not saved as this 
     * attribute always returns the same value on a read.
     */
    if ((pInstance->pu4_OidList[0] != 1) && (pInstance->pu4_OidList[0] != 2))
    {
        return (MSR_SKIP);
    }

    if (pInstance->pu4_OidList[0] == 2)
    {
        /* Don't save the fsFmVarRetrievalRequest object when
         * this object's value is 0.
         */
        if (pData->pOctetStrValue->i4_Length == 0)
        {
            return (MSR_SKIP);
        }
    }

    return (MSR_SAVE);

}

/************************************************************************
 *  Function Name   : MsrValidateFsFmModuleStatus
 *  Description     : This function validates EOAM FM Module status.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateFsFmModuleStatus (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{

    INT4                i4RetValFsFmSystemControl;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pData);

    nmhGetFsFmSystemControl (&i4RetValFsFmSystemControl);

    if (i4RetValFsFmSystemControl == FM_START)
    {
        return MSR_SAVE;
    }
    return MSR_SKIP;
}
#endif /* EOAM_FM_WANTED */
#endif /* EOAM_WANTED */

#ifdef RRD_WANTED
/************************************************************************
 *  Function Name   : MsrValidateRtmContextEntry 
 *  Description     : This function validates the fsMIRtmEntry table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateRtmContextEntry (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4RtmCxtId;
    UINT4               u4RtrId = 0;
    INT4                i4RtrASNo = 0;
    UINT1               u1Flag = 0;
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    u4RtmCxtId = pInstance->pu4_OidList[1];

    /* To Store the Default Administrative Distance 
     * for static IPv4 route stored in RTM contex
     * structure
     */
    if (pInstance->pu4_OidList[0] == 9)
    {
        return MSR_SAVE;
    }

    nmhGetFsMIRrdRouterId ((INT4) u4RtmCxtId, &u4RtrId);
    nmhGetFsMIRrdRouterASNumber ((INT4) u4RtmCxtId, &i4RtrASNo);
    if ((RtmGetRtrIdConfigType ((INT4) u4RtmCxtId, &u1Flag) == IP_SUCCESS)
        && (u1Flag == RTM_RTRID_CONFIG_TYPE_DYNAMIC))
    {
        if (((u4RtrId == 0) && (i4RtrASNo == 0)) || (u4RtrId != 0))
        {
            return MSR_SKIP;
        }
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateRtmControlEntry
 *  Description     : This function validates the fsMIRrdControlEntry table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateRtmControlEntry (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4IpAddress;
    UINT4               u4Mask;
    UINT1               au1Array[MSR_MAX_IP_ADDR_LEN];
    UINT1               au1CopyString[MSR_MAX_FILE_NME_LEN];
    UINT4               u4Count = 0;

    UNUSED_PARAM (pData);

    MSR_MEMSET (&au1Array, 0, sizeof (au1Array));
    MSR_MEMSET (&au1CopyString, 0, sizeof (au1CopyString));

    /* Skip CREATE_AND_WAIT for the default index */

    if (pOid->u4_Length == OID_LENGTH_TWELVE)
    {
        if (pInstance->pu4_OidList[0] == RTM_DEFAULT_CXT_ID)
        {
            /* Dest. IP */
            for (u4Count = 1; u4Count < 4; u4Count++)
            {
                SPRINTF ((char *) au1Array, "%d.",
                         pInstance->pu4_OidList[u4Count]);
                MSR_STRCAT (au1CopyString, au1Array);
            }

            SPRINTF ((char *) au1Array, "%d", pInstance->pu4_OidList[u4Count]);
            MSR_STRCAT (au1CopyString, au1Array);

            u4IpAddress = OSIX_NTOHL (INET_ADDR (au1CopyString));

            MSR_MEMSET (&au1CopyString, 0, sizeof (au1CopyString));

            u4Count++;

            /* Net MASK */
            for (; u4Count < 8; u4Count++)
            {
                SPRINTF ((char *) au1Array, "%d.",
                         pInstance->pu4_OidList[u4Count]);
                MSR_STRCAT (au1CopyString, au1Array);
            }
            SPRINTF ((char *) au1Array, "%d", pInstance->pu4_OidList[u4Count]);
            MSR_STRCAT (au1CopyString, au1Array);

            u4Mask = OSIX_NTOHL (INET_ADDR (au1CopyString));

            if ((u4IpAddress == IP_ANY_ADDR) && (u4Mask == IP_GEN_BCAST_ADDR))
            {
                return (MSR_SKIP);
            }
        }
    }
    return (MSR_SAVE);
}

#endif
#ifdef DHCP_RLY_WANTED
INT4
MsrValidatedhcpConfigDhcpCircuitOption (tSNMP_OID_TYPE * pOid,
                                        tSNMP_OID_TYPE * pInstance,
                                        tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetValDhcpRelayRAICircuitIDSubOptionControl = 0;
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    UNUSED_PARAM (pInstance);

    nmhGetDhcpRelayRAICircuitIDSubOptionControl
        (&i4RetValDhcpRelayRAICircuitIDSubOptionControl);

    /* Dont Save the DhcpConfigDhcpCircuitOption  value if
       CircuitIDSubOption is DHCP_DISABLE. */

    if (i4RetValDhcpRelayRAICircuitIDSubOptionControl == DHCP_DISABLE)
    {
        return (MSR_SKIP);
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsMIDhcpRelaySrvAddressTable
 *  Description     : This function validates the Dhcpv6 Relay server address
 *                    table objects before saving those objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateFsMIDhcpRelaySrvAddressTable (tSNMP_OID_TYPE * pOid,
                                         tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pData);
    if (pOid->u4_Length == 13)
    {
        /* For default context no need to store the MI server address
         * table entry for default context. It would have been stored
         * using the SI server address table*/
        if (pInstance->pu4_OidList[0] == 0)
        {
            return MSR_SKIP;
        }
    }
    return MSR_SAVE;
}

#endif

#ifdef ECFM_WANTED
/************************************************************************
 *  Function Name   : MsrValidateSaveEcfmMepEntry
 *  Description     : This function saves the VLAN based Mep table 
 *                    entries
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateSaveEcfmMepEntry (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4MdIndex = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4MaIndex = 0;
    UINT4               u4NextContextId = 0;
    UINT4               u4NextMdIndex = 0;
    UINT4               u4NextMaIndex = 0;
    INT4                i4NextSelectorType = 0;
    UINT4               u4NextSelectorOrNone = 0;

    UNUSED_PARAM (pData);

/*Only changed values are to be stored */
    if (pOid->u4_Length == 12)    /* For Row Status other then Create and wait
                                   Event length will be No Of index +1 */
    {
        u4ContextId = pInstance->pu4_OidList[1];
        u4MdIndex = pInstance->pu4_OidList[2];
        u4MaIndex = pInstance->pu4_OidList[3];
    }
    else
    {                            /* For Row Status for Create and wait
                                   Event length will be No Of index */

        u4ContextId = pInstance->pu4_OidList[0];
        u4MdIndex = pInstance->pu4_OidList[1];
        u4MaIndex = pInstance->pu4_OidList[2];
    }

    ECFM_CC_LOCK ();

    if (nmhGetNextIndexFsMIEcfmExtMaTable
        (u4ContextId, &u4NextContextId, u4MdIndex, &u4NextMdIndex,
         u4MaIndex, &u4NextMaIndex, 0, &i4NextSelectorType,
         0, &u4NextSelectorOrNone) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return (MSR_SKIP);
    }
    if ((u4ContextId == u4NextContextId) &&
        (u4MdIndex == u4NextMdIndex) &&
        (u4MaIndex == u4NextMaIndex) &&
        (i4NextSelectorType != ECFM_SERVICE_SELECTION_VLAN))
    {
        ECFM_CC_UNLOCK ();
        return (MSR_SKIP);
    }

    ECFM_CC_UNLOCK ();

    if (pOid->u4_Length == 12)
    {
        switch (pInstance->pu4_OidList[0])
        {
            case 27:            /*fsMIEcfmMepTransmitLbmDestMacAddress */
            case 28:            /*fsMIEcfmMepTransmitLbmDestMepId */
            case 29:            /*fsMIEcfmMepTransmitLbmDestIsMepId */
            case 31:            /*fsMIEcfmMepTransmitLbmDataTlv */
            case 32:            /*fsMIEcfmMepTransmitLbmVlanPriority */
            case 33:            /*fsMIEcfmMepTransmitLbmVlanDropEnable */
            case 37:            /*fsMIEcfmMepTransmitLtmFlags */
            case 38:            /*fsMIEcfmMepTransmitLtmTargetMacAddress */
            case 39:            /*fsMIEcfmMepTransmitLtmTargetMepId */
            case 40:            /*fsMIEcfmMepTransmitLtmTargetIsMepId */
            case 41:            /*fsMIEcfmMepTransmitLtmTtl */
            case 44:            /*fsMIEcfmMepTransmitLtmEgressIdentifier */
            {
                return (MSR_SAVE);
            }
            default:
            {
                return (MSR_SKIP);
            }
        }
    }
    return (MSR_SKIP);
}

 /************************************************************************
 *  Function Name   : MsrValidateSaveY1731MepEntry
 *  Description     : This function validates the Mep table
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateSaveY1731MepEntry (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pData);
    if (pOid->u4_Length == 13)
    {
        switch (pInstance->pu4_OidList[0])
        {
            case 31:            /*fsMIY1731MepTransmitLtmFlags */
            case 32:            /*fsMIY1731MepTransmitLtmTargetMacAddress */
            case 33:            /*fsMIY1731MepTransmitLtmTargetMepId */
            case 34:            /*fsMIY1731MepTransmitLtmTargetIsMepId */
            case 35:            /*fsMIY1731MepTransmitLtmTtl */
            case 36:            /*fsMIY1731MepTransmitLtmDropEnable */
            case 37:            /*fsMIY1731MepTransmitLtmPriority */
            case 39:            /*fsMIY1731MepTransmitLtmTimeout */
            {
                return (MSR_SAVE);
            }
            default:
            {
                break;
            }
        }
    }
    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateEcfmDefaultMdEntry
 *  Description     : This function validates the DefaultMd table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateEcfmDefaultMdEntry (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    /*Only changed values are to be stored */
    switch (pInstance->pu4_OidList[0])
    {
        case 3:                /*fsMIEcfmDefaultMdLevel */
        {
            if (pData->i4_SLongValue != -1)
            {
                break;
            }
            return (MSR_SKIP);
        }
        case 4:                /*fsMIEcfmDefaultMdMhfCreation */
        {
            if (pData->i4_SLongValue != 4)
            {
                break;
            }
            return (MSR_SKIP);
        }
        case 5:                /*fsMIEcfmDefaultMdIdPermission */
        {
            if (pData->i4_SLongValue != 5)
            {
                break;
            }
            return (MSR_SKIP);
        }
        default:
        {
            return (MSR_SKIP);
        }
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateEcfmExtDefaultMdEntry
 *  Description     : This function validates the DefaultMd table of 
 *                    ECFM Extn Mib.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateEcfmExtDefaultMdEntry (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4SelectorType = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /*Only changed values are to be stored */

    if (pInstance->u4_Length == 4)
    {
        i4SelectorType = (INT4) pInstance->pu4_OidList[2];
    }
    else
    {
        i4SelectorType = (INT4) pInstance->pu4_OidList[1];
    }

    if (i4SelectorType != 2)
    {
        return MSR_SKIP;
    }

    switch (pInstance->pu4_OidList[0])
    {
        case 4:                /*fsMIEcfmExtDefaultMdLevel */
        {
            if (pData->i4_SLongValue != -1)
            {
                break;
            }
            return (MSR_SKIP);
        }
        case 5:                /*fsMIEcfmExtDefaultMdMhfCreation */
        {
            if (pData->i4_SLongValue != 4)
            {
                break;
            }
            return (MSR_SKIP);
        }
        case 6:                /*fsMIEcfmExtDefaultMdIdPermission */
        {
            if (pData->i4_SLongValue != 5)
            {
                break;
            }
            return (MSR_SKIP);
        }
        default:
        {
            return (MSR_SKIP);
        }
    }
    return (MSR_SAVE);
}

/************************************************************************
 * Function Name   : MsrValidateEcfmContextEntry
 * Description     : This function validates the Context table
 * Input           : pOid - pointer to the OID 
 *                   pInstance - pointer to the instance OID 
 *                   pData - pointer to the data to be saved
 * Output          : None
 * Returns         : MSR_SAVE if the object should be saved
 *                   MSR_SKIP if just the current instance of the object
 *                   should not be saved   
 *                   MSR_NEXT if the current scalar/table should not be
 *                   saved 
 ************************************************************************/

INT4
MsrValidateEcfmContextEntry (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    /*Only changed values are to be stored */
    switch (pInstance->pu4_OidList[0])
    {
        case 16:                /* fsMIEcfmMemoryFailureCount */
        case 17:                /* fsMIEcfmBufferFailureCount */
        case 18:                /* fsMIEcfmUpCount */
        case 19:                /* fsMIEcfmDownCount */
        case 20:                /* fsMIEcfmNoDftCount */
        case 21:                /* fsMIEcfmRdiDftCount */
        case 22:                /* fsMIEcfmMacStatusDftCount */
        case 23:                /* fsMIEcfmRemoteCcmDftCount */
        case 24:                /* fsMIEcfmErrorCcmDftCount */
        case 25:                /* fsMIEcfmXconDftCount */
        case 27:                /* fsMIEcfmMipDynamicEvaluationStatus
                                   Skip this mib object and restore after 
                                   the sucessful restoration of MEPs */
        {
            return (MSR_SKIP);
        }
        default:
        {
            break;
        }
    }
    return (MSR_SAVE);
}

/************************************************************************
 * Function Name   : MsrValidateSaveMipDynEvalStatus
 * Description     : This function validates the Context table
 * Input           : pOid - pointer to the OID 
 *                   pInstance - pointer to the instance OID 
 *                   pData - pointer to the data to be saved
 * Output          : None
 * Returns         : MSR_SAVE if the object should be saved
 *                   MSR_SKIP if just the current instance of the object
 *                   should not be saved   
 *                   MSR_NEXT if the current scalar/table should not be
 *                   saved 
 ************************************************************************/

INT4
MsrValidateSaveMipDynEvalStatus (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    /*Only changed values are to be stored */
    if (pInstance->pu4_OidList[0] == 27)
    {
        /* fsMIEcfmMipDynamicEvaluationStatus */
        return (MSR_SAVE);
    }

    return (MSR_SKIP);
}

/************************************************************************
 * Function Name   : MsrValidateEcfmPortEntry
 * Description     : This function validates the Port table
 * Input           : pOid - pointer to the OID 
 *                   pInstance - pointer to the instance OID 
 *                   pData - pointer to the data to be saved
 * Output          : None
 * Returns         : MSR_SAVE if the object should be saved
 *                   MSR_SKIP if just the current instance of the object
 *                   should not be saved   
 *                   MSR_NEXT if the current scalar/table should not be
 *                   saved 
 *************************************************************************/

INT4
MsrValidateEcfmPortEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    /*Only changed values are to be stored */
    switch (pInstance->pu4_OidList[0])
    {
        case 4:                /* fsMIEcfmPortTxCfmPduCount */
        case 5:                /* fsMIEcfmPortTxCcmCount */
        case 6:                /* fsMIEcfmPortTxLbmCount */
        case 7:                /* fsMIEcfmPortTxLbrCount */
        case 8:                /* fsMIEcfmPortTxLtmCount */
        case 9:                /* fsMIEcfmPortTxLtrCount */
        case 10:                /* fsMIEcfmPortTxFailedCount  */
        case 11:                /* fsMIEcfmPortRxCfmPduCount */
        case 12:                /* fsMIEcfmPortRxCcmCount */
        case 13:                /* fsMIEcfmPortRxLbmCount */
        case 14:                /* fsMIEcfmPortRxLbrCount */
        case 15:                /* fsMIEcfmPortRxLtmCount */
        case 16:                /* fsMIEcfmPortRxLtrCount */
        case 17:                /* fsMIEcfmPortRxBadCfmPduCount */
        case 18:                /* fsMIEcfmPortFrwdCfmPduCount */
        case 19:                /* fsMIEcfmPortDsrdCfmPduCount */

        {
            return (MSR_SKIP);
        }
        default:
        {
            break;
        }
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateEcfmMepEntry
 *  Description     : This function saves the VLAN based Mep table
 *                    entries
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateEcfmMepEntry (tSNMP_OID_TYPE * pOid,
                         tSNMP_OID_TYPE * pInstance,
                         tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4MdIndex = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4MaIndex = 0;
    INT4                i4RetPrimaryVid = 0;
    /*
       UINT4               u4NextContextId = 0; 
       UINT4               u4NextMdIndex =0;
       UINT4               u4NextMaIndex = 0;
       INT4                i4NextSelectorType = 0;
       UINT4               u4NextSelectorOrNone = 0;
     */

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /*Only changed values are to be stored */
    if (pInstance->u4_Length == 5)    /* For Row Status other then Create and wait
                                       Event length will be No Of index +1 */
    {
        u4ContextId = pInstance->pu4_OidList[1];
        u4MdIndex = pInstance->pu4_OidList[2];
        u4MaIndex = pInstance->pu4_OidList[3];
    }
    else
    {                            /* For Row Status for Create and wait
                                   Event length will be No Of index */

        u4ContextId = pInstance->pu4_OidList[0];
        u4MdIndex = pInstance->pu4_OidList[1];
        u4MaIndex = pInstance->pu4_OidList[2];
    }

    ECFM_CC_LOCK ();

    if (nmhGetFsMIEcfmMaPrimaryVlanId (u4ContextId, u4MdIndex, u4MaIndex,
                                       &i4RetPrimaryVid) == SNMP_FAILURE)
    {
        ECFM_CC_UNLOCK ();
        return (MSR_SKIP);
    }
    if ((IS_MEP_ISID_AWARE (i4RetPrimaryVid)))
    {
        ECFM_CC_UNLOCK ();
        return (MSR_SKIP);
    }
    /* 
       if (nmhGetNextIndexFsMIEcfmExtMaTable 
       (u4ContextId, &u4NextContextId, u4MdIndex, &u4NextMdIndex,
       u4MaIndex, &u4NextMaIndex, 0, &i4NextSelectorType,
       0, &u4NextSelectorOrNone) != SNMP_SUCCESS)
       {
       ECFM_CC_UNLOCK ();
       return (MSR_SKIP);
       }
       if ((u4ContextId == u4NextContextId) &&
       (u4MdIndex == u4NextMdIndex) &&
       (u4MaIndex == u4NextMaIndex) &&
       (i4NextSelectorType != ECFM_SERVICE_SELECTION_VLAN))
       {
       ECFM_CC_UNLOCK ();
       return (MSR_SKIP);
       }
     */

    /* Save Only Objects related to VLAN Only */
    if (pOid->u4_Length == 12)
    {
        switch (pInstance->pu4_OidList[0])
        {
            case 17:            /*fsMIEcfmMepCcmSequenceErrors */
            case 18:            /*fsMIEcfmMepCciSentCcms */
            case 20:            /*fsMIEcfmMepLbrIn */
            case 21:            /*fsMIEcfmMepLbrInOutOfOrder */
            case 22:            /*fsMIEcfmMepLbrBadMsdu */
            case 24:            /*fsMIEcfmMepUnexpLtrIn */
            case 25:            /*fsMIEcfmMepLbrOut */
            case 26:            /*fsMIEcfmMepTransmitLbmStatus */
            case 27:            /*fsMIEcfmMepTransmitLbmDestMacAddress */
            case 28:            /*fsMIEcfmMepTransmitLbmDestMepId */
            case 29:            /*fsMIEcfmMepTransmitLbmDestIsMepId */
            case 30:            /*fsMIEcfmMepTransmitLbmMessages */
            case 31:            /*fsMIEcfmMepTransmitLbmDataTlv */
            case 32:            /*fsMIEcfmMepTransmitLbmVlanPriority */
            case 33:            /*fsMIEcfmMepTransmitLbmVlanDropEnable */
            case 36:            /*fsMIEcfmMepTransmitLtmStatus */
            case 37:            /*fsMIEcfmMepTransmitLtmFlags */
            case 38:            /*fsMIEcfmMepTransmitLtmTargetMacAddress */
            case 39:            /*fsMIEcfmMepTransmitLtmTargetMepId */
            case 40:            /*fsMIEcfmMepTransmitLtmTargetIsMepId */
            case 41:            /*fsMIEcfmMepTransmitLtmTtl */
            case 44:            /*fsMIEcfmMepTransmitLtmEgressIdentifier */
            {
                ECFM_CC_UNLOCK ();
                return (MSR_SKIP);
            }
            default:
            {
                break;
            }
        }
    }
    ECFM_CC_UNLOCK ();
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateEcfmIsidMplstpMepEntry
 *  Description     : This function saves the ISID and MPLSTP based
 *                    Mep Entries.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateEcfmIsidMplstpMepEntry (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4MdIndex = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4MaIndex = 0;
    UINT4               u4NextContextId = 0;
    UINT4               u4NextMdIndex = 0;
    UINT4               u4NextMaIndex = 0;
    INT4                i4NextSelectorType = 0;
    UINT4               u4NextSelectorOrNone = 0;

    if (pInstance->u4_Length == 5)
    {
        u4ContextId = pInstance->pu4_OidList[1];
        u4MdIndex = pInstance->pu4_OidList[2];
        u4MaIndex = pInstance->pu4_OidList[3];
    }
    else
    {
        u4ContextId = pInstance->pu4_OidList[0];
        u4MdIndex = pInstance->pu4_OidList[1];
        u4MaIndex = pInstance->pu4_OidList[2];
    }
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    ECFM_CC_LOCK ();

    if (nmhGetNextIndexFsMIEcfmExtMaTable
        (u4ContextId, &u4NextContextId, u4MdIndex, &u4NextMdIndex,
         u4MaIndex, &u4NextMaIndex, 0, &i4NextSelectorType,
         0, &u4NextSelectorOrNone) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return (MSR_SKIP);
    }

    if ((u4ContextId == u4NextContextId) &&
        (u4MdIndex == u4NextMdIndex) &&
        (u4MaIndex == u4NextMaIndex) &&
        (i4NextSelectorType == ECFM_SERVICE_SELECTION_VLAN))
    {
        ECFM_CC_UNLOCK ();
        return (MSR_SKIP);
    }

    /* Common entries for both ISID and MPLSTP */
    switch (pInstance->pu4_OidList[0])
    {
        case 17:                /*fsMIEcfmMepCcmSequenceErrors */
        case 18:                /*fsMIEcfmMepCciSentCcms */
        case 20:                /*fsMIEcfmMepLbrIn */
        case 21:                /*fsMIEcfmMepLbrInOutOfOrder */
        case 22:                /*fsMIEcfmMepLbrBadMsdu */
        case 24:                /*fsMIEcfmMepUnexpLtrIn */
        case 25:                /*fsMIEcfmMepLbrOut */
        case 26:                /*fsMIEcfmMepTransmitLbmStatus */
        case 27:                /*fsMIEcfmMepTransmitLbmDestMacAddress */
        case 28:                /*fsMIEcfmMepTransmitLbmDestMepId */
        case 29:                /*fsMIEcfmMepTransmitLbmDestIsMepId */
        case 30:                /*fsMIEcfmMepTransmitLbmMessages */
        case 31:                /*fsMIEcfmMepTransmitLbmDataTlv */
        case 32:                /*fsMIEcfmMepTransmitLbmVlanPriority */
        case 33:                /*fsMIEcfmMepTransmitLbmVlanDropEnable */
        case 36:                /*fsMIEcfmMepTransmitLtmStatus */
        case 37:                /*fsMIEcfmMepTransmitLtmFlags */
        case 38:                /*fsMIEcfmMepTransmitLtmTargetMacAddress */
        case 39:                /*fsMIEcfmMepTransmitLtmTargetMepId */
        case 40:                /*fsMIEcfmMepTransmitLtmTargetIsMepId */
        case 41:                /*fsMIEcfmMepTransmitLtmTtl */
        case 44:                /*fsMIEcfmMepTransmitLtmEgressIdentifier */
        {
            ECFM_CC_UNLOCK ();
            return (MSR_SKIP);
        }
        default:
        {
            break;
        }
    }

    if ((i4NextSelectorType == ECFM_SERVICE_SELECTION_LSP) ||
        (i4NextSelectorType == ECFM_SERVICE_SELECTION_PW))
    {
        switch (pInstance->pu4_OidList[0])
        {
            case 2:            /*fsMIEcfmMepIfIndex */
            case 4:            /*fsMIEcfmMepPrimaryVid */
            case 8:            /*fsMIEcfmMepCcmLtmPriority */
            {
                ECFM_CC_UNLOCK ();
                return (MSR_SKIP);
            }
            default:
            {
                break;
            }
        }
    }

    ECFM_CC_UNLOCK ();
    return (MSR_SAVE);
}

/************************************************************************
 * Function Name   : MsrValidateY1731PortEntry
 * Description     : This function validates the Port table
 * Input           : pOid - pointer to the OID 
 *                   pInstance - pointer to the instance OID 
 *                   pData - pointer to the data to be saved
 * Output          : None
 * Returns         : MSR_SAVE if the object should be saved
 *                   MSR_SKIP if just the current instance of the object
 *                   should not be saved   
 *                   MSR_NEXT if the current scalar/table should not be
 *                   saved 
 *************************************************************************/

INT4
MsrValidateY1731PortEntry (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    /*Only changed values are to be stored */
    switch (pInstance->pu4_OidList[0])
    {
        case 2:                /* fsMIY1731PortAisOut */
        case 3:                /* fsMIY1731PortAisIn */
        case 4:                /* fsMIY1731PortLckOut */
        case 5:                /* fsMIY1731PortLckIn */
        case 6:                /* fsMIY1731PortTstOut */
        case 7:                /* fsMIY1731PortTstIn */
        case 8:                /* fsMIY1731PortLmmOut */
        case 9:                /* fsMIY1731PortLmmIn */
        case 10:                /* fsMIY1731PortLmrOut  */
        case 11:                /* fsMIY1731PortLmrIn */
        case 12:                /* fsMIY1731Port1DmOut */
        case 13:                /* fsMIY1731Port1DmIn */
        case 14:                /* fsMIY1731PortDmmOut */
        case 15:                /* fsMIY1731PortDmmtIn */
        case 16:                /* fsMIY1731PortDmrOut */
        case 17:                /* fsMIY1731PortDmrIn */
        case 18:                /* fsMIY1731PortApsOut */
        case 19:                /* fsMIY1731PortApsIn */
        case 20:                /* fsMIY1731PortMccOut */
        case 21:                /* fsMIY1731PortMccIn */
        case 22:                /* fsMIY1731PortVsmOut */
        case 23:                /* fsMIY1731PortVsmIn */
        case 24:                /* fsMIY1731PortVsrOut */
        case 25:                /* fsMIY1731PortVsrIn */
        case 26:                /* fsMIY1731PortExmOut */
        case 27:                /* fsMIY1731PortExmIn */
        case 28:                /* fsMIY1731PortExrOut */
        case 29:                /* fsMIY1731PortExrIn */

        {
            return (MSR_SKIP);
        }
        default:
        {
            break;
        }
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateY1731MegEntry
 *  Description     : This function validates the Meg table
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateY1731MegEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData)
{

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    /*Only changed values are to be stored */
    switch (pInstance->pu4_OidList[0])
    {
        case 1:                /*fsMIY1731MegIndex */
        {
            return (MSR_SKIP);
        }
        default:
        {
            break;
        }
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateY1731MepEntry
 *  Description     : This function validates the Mep table
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateY1731MepEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    /*Only changed values are to be stored */
    switch (pInstance->pu4_OidList[0])
    {
        case 1:                /*fsMIY1731MepIdentifier */
        case 9:                /*fsMIY1731MepLbmCurrentTransId */
        case 10:                /*fsMIY1731MepTransmitLbmStatus */
        case 28:                /*fsMIY1731MepBitErroredLbrIn */
        case 29:                /*fsMIY1731MepTransmitLtmStatus */
        case 51:                /*fsMIY1731MepTransmitTstStatus */
        case 52:                /*fsMIY1731MepTransmitTstResultOK */
        case 56:                /*fsMIY1731MepBitErroredTstIn */
        case 57:                /*fsMIY1731MepValidTstIn */
        case 58:                /*fsMIY1731MepTstOut */
        case 59:                /*fsMIY1731MepTransmitLmmStatus */
        case 60:                /*fsMIY1731MepTransmitLmmResultOK */
        case 69:                /*fsMIY1731MepTxFCf */
        case 70:                /*fsMIY1731MepRxFCb */
        case 71:                /*fsMIY1731MepTxFCb */
        case 74:                /*fsMIY1731MepTransmitDmStatus */
        case 75:                /*fsMIY1731MepTransmitDmResultOK */
        case 91:                /*fsMIY1731MepAisCondition */
        case 100:                /*fsMIY1731MepLckCondition */

        case 31:                /*fsMIY1731MepTransmitLtmFlags */
        case 32:                /*fsMIY1731MepTransmitLtmTargetMacAddress */
        case 33:                /*fsMIY1731MepTransmitLtmTargetMepId */
        case 34:                /*fsMIY1731MepTransmitLtmTargetIsMepId */
        case 35:                /*fsMIY1731MepTransmitLtmTtl */
        case 36:                /*fsMIY1731MepTransmitLtmDropEnable */
        case 37:                /*fsMIY1731MepTransmitLtmPriority */
        case 39:                /*fsMIY1731MepTransmitLtmTimeout */
        case 118:                /*fsMIY1731MepTransmitThStatus */
        case 133:                /* fsMIY1731MepLbmTTL */
        case 134:                /* fsMIY1731MepLbmIcc */
        case 135:                /* fsMIY1731MepLbmNodeId */
        case 136:                /* fsMIY1731MepLbmIfNum */
        {
            return (MSR_SKIP);
        }
        default:
        {
            break;
        }
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateEcfmMaEntry
 *  Description     : This function saves the VLAN based MA table entries
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateEcfmMaEntry (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4MdIndex = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4MaIndex = 0;
    UINT4               u4NextContextId = 0;
    UINT4               u4NextMdIndex = 0;
    UINT4               u4NextMaIndex = 0;
    INT4                i4NextSelectorType = 0;
    UINT4               u4NextSelectorOrNone = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if ((pInstance->u4_Length == 3) && (pOid->pu4_OidList[12] == 9)
        && (pData->i4_SLongValue == CREATE_AND_WAIT))
    {
        return (MSR_SKIP);
    }
    /*Only changed values are to be stored */
    if (pInstance->u4_Length == 4)
    {
        u4ContextId = pInstance->pu4_OidList[1];
        u4MdIndex = pInstance->pu4_OidList[2];
        u4MaIndex = pInstance->pu4_OidList[3];
    }
    else
    {
        u4ContextId = pInstance->pu4_OidList[0];
        u4MdIndex = pInstance->pu4_OidList[1];
        u4MaIndex = pInstance->pu4_OidList[2];
    }

    ECFM_CC_LOCK ();

    if (nmhGetNextIndexFsMIEcfmExtMaTable
        (u4ContextId, &u4NextContextId, u4MdIndex, &u4NextMdIndex,
         u4MaIndex, &u4NextMaIndex, 0, &i4NextSelectorType,
         0, &u4NextSelectorOrNone) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return (MSR_SKIP);
    }
    if ((u4ContextId == u4NextContextId) &&
        (u4MdIndex == u4NextMdIndex) &&
        (u4MaIndex == u4NextMaIndex) &&
        (i4NextSelectorType != ECFM_SERVICE_SELECTION_VLAN))
    {
        ECFM_CC_UNLOCK ();
        return (MSR_SKIP);
    }

    ECFM_CC_UNLOCK ();

    /*Only changed values are to be stored */
    switch (pInstance->pu4_OidList[0])
    {
        case 2:                /* FsMIEcfmMaPrimaryVlanId */
        case 3:                /* FsMIEcfmMaFormat */
        case 4:                /* FsMIEcfmMaName */
        case 5:                /* FsMIEcfmMaMhfCreation */
        case 6:                /* FsMIEcfmMaIdPermission */
        case 8:                /* FsMIEcfmMaNumberOfVids */
        {
            return (MSR_SKIP);
        }
        default:
        {
            break;
        }
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateEcfmMaVlanEntry
 *  Description     : This function saves the VLAN based MA table entries
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateEcfmMaVlanEntry (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4MdIndex = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4MaIndex = 0;
    UINT4               u4NextContextId = 0;
    UINT4               u4NextMdIndex = 0;
    UINT4               u4NextMaIndex = 0;
    INT4                i4NextSelectorType = 0;
    UINT4               u4NextSelectorOrNone = 0;

    /*Only changed values are to be stored */
    if (pInstance->u4_Length == 4)
    {
        u4ContextId = pInstance->pu4_OidList[1];
        u4MdIndex = pInstance->pu4_OidList[2];
        u4MaIndex = pInstance->pu4_OidList[3];
    }
    else
    {
        u4ContextId = pInstance->pu4_OidList[0];
        u4MdIndex = pInstance->pu4_OidList[1];
        u4MaIndex = pInstance->pu4_OidList[2];
    }
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    ECFM_CC_LOCK ();

    if (nmhGetNextIndexFsMIEcfmExtMaTable
        (u4ContextId, &u4NextContextId, u4MdIndex, &u4NextMdIndex,
         u4MaIndex, &u4NextMaIndex, 0, &i4NextSelectorType,
         0, &u4NextSelectorOrNone) != SNMP_SUCCESS)
    {
        ECFM_CC_UNLOCK ();
        return (MSR_SKIP);
    }
    /* Save the MA entry only if the selector type is VLAN */
    if ((u4ContextId == u4NextContextId) &&
        (u4MdIndex == u4NextMdIndex) &&
        (u4MaIndex == u4NextMaIndex) &&
        (i4NextSelectorType == ECFM_SERVICE_SELECTION_VLAN))
    {
        ECFM_CC_UNLOCK ();
        return (MSR_SAVE);
    }

    ECFM_CC_UNLOCK ();
    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateMeTable                    *
 *  Description     : This function skip the RowStatus of MA Table    *
 *                    with the value of CREATE and WAIT                 *
 *  Input           : pOid - pointer to the OID             *    
 *                    pInstance - pointer to the instance OID         *
 *                    pData - pointer to the data to be saved        *
 *  Output          : None                        *
 *  Returns         : MSR_SAVE if the object should be saved        *
 *                    MSR_SKIP if just the current instance of the     *
 *                    object should not be saved                *
 *                    MSR_NEXT if the current scalar/table should not be*
 *                             saved                    *
 ************************************************************************/

INT4
MsrValidateMeTable (tSNMP_OID_TYPE * pOid,
                    tSNMP_OID_TYPE * pInstance, tSNMP_MULTI_DATA_TYPE * pData)
{

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if ((pInstance->u4_Length == 3) && (pOid->pu4_OidList[12] == 9)
        && (pData->i4_SLongValue == CREATE_AND_WAIT))
    {
        return (MSR_SKIP);
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateEcfmMaIsidMplstpEntry
 *  Description     : This function saves ISID and MPLSTP based MA 
 *                    table entries
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateEcfmMaIsidMplstpEntry (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4SelectorType = 0;

/* Save all the objects of MaExtTable except the CREATE and WAIT
 * Because memory allocation already done in MaNetRowstatus CREATE and WAIT*/
    if (pInstance->pu4_OidList[3] == ECFM_SERVICE_SELECTION_VLAN)
    {
        if ((pInstance->u4_Length == 5) && (pOid->pu4_OidList[13] == 10)
            && (pData->i4_SLongValue == CREATE_AND_WAIT))
        {
            return (MSR_SKIP);
        }
    }
    /*Only changed values are to be stored */

    if (pInstance->u4_Length == 6)
    {
        i4SelectorType = (INT4) pInstance->pu4_OidList[4];
    }
    else
    {
        i4SelectorType = (INT4) pInstance->pu4_OidList[3];
    }

    /* Skip the below specific MPLSTP objects */
    if ((i4SelectorType == ECFM_SERVICE_SELECTION_LSP) ||
        (i4SelectorType == ECFM_SERVICE_SELECTION_PW))
    {
        if (pInstance->u4_Length == 6)
        {
            switch (pInstance->pu4_OidList[0])
            {
                case 6:        /*fsMIEcfmExtMaMhfCreation */
                case 7:        /*fsMIEcfmExtMaIdPermission */
                case 9:        /*fsMIEcfmExtMaNumberOfVids */
                {
                    return (MSR_SKIP);
                }
                default:
                {
                    break;
                }
            }
        }
    }

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* Save all the objects for ISID and remaining MPLSTP objects */
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateEcfmMipIsidEntry
 *  Description     : This function validates the ExtMip table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateEcfmMipIsidEntry (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4SelectorType = 0;

    /*Only changed values are to be stored */

    if (pInstance->u4_Length == 5)
    {
        /*VLAN/ISID For Events other then Create and Wait */
        i4SelectorType = (INT4) pInstance->pu4_OidList[3];
    }
    else
    {
        /*VLAN/ISID For Create and Wait Event */
        i4SelectorType = (INT4) pInstance->pu4_OidList[2];
    }
    /* Save the Objects if Selector type is ISID */
    if (i4SelectorType != ECFM_SERVICE_SELECTION_ISID)
    {
        return MSR_SKIP;
    }

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateEcfmMipVlanEntry
 *  Description     : This function validates the MIP table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateEcfmMipVlanEntry (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4IsVlanId = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* Save Only Objects related to VLAN Only */
    /*Only changed values are to be stored */
    if (pInstance->u4_Length == 4)
    {
        u4IsVlanId = pInstance->pu4_OidList[3];
        /*VLAN Index For Events othher then Create and Wait */
    }
    else
    {
        u4IsVlanId = pInstance->pu4_OidList[2];
        /*VLAN Index For Create and Wait Event */
    }
    if ((IS_MEP_ISID_AWARE (u4IsVlanId)))
    {
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}

#endif
#ifdef ERPS_WANTED
/************************************************************************
 *  Function Name   : MsrValErpsSkipRingRowStatus
 *  Description     : This function validates the ERPS table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValErpsSkipRingRowStatus (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* In MSR while storing the objects in the table, the object's 
     * position in the table is filled in the pInstance. 
     * For Ring table the Indices are ContextId and RingId.
     * In this routine we have to save all the object except the 
     * Rowstatus. So at first we have to check for the length of
     * the Indices (3) and then we have look for the object's position 
     * in the table(2).*/

    if (pInstance->u4_Length == 3)
    {
        if (pInstance->pu4_OidList[0] == 15)
        {
            return MSR_SKIP;
        }
    }
    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValErpsRingConfigClear
 *  Description     : This function validates the ERPS table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValErpsRingConfigClear (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData)
{

    UINT4               u4FsErpsContextId = 0;
    UINT4               u4FsErpsRingId = 0;
    INT4                i4RetValFsErpsRingConfigSwitchCmd = 0;

    u4FsErpsContextId = pInstance->pu4_OidList[1];
    u4FsErpsRingId = pInstance->pu4_OidList[2];

    /*This Routine is Specifically added for incremental save,
     *to store the clear value as 2 or 1 depends upon the 
     *Ring state(idle means store the value as 2,FS or MS means 
     *store the value as 1 before doing MIB save).While storing 
     *the objects in the table,the object's position in the table 
     *is filled in the pInstance. 
     *For Config table the Indices are ContextId and Ring Id.*/

    /* Since DistributingPort,Port1Present and Port2Present
     * objects are stored in MsrValErpsRingConfigDistPortSave
     * routine we are skipping those objects here */

    if ((pInstance->u4_Length == 3) &&
        ((pInstance->pu4_OidList[0] == 15) ||
         (pInstance->pu4_OidList[0] == 14) ||
         (pInstance->pu4_OidList[0] == 13)))
    {
        return MSR_SKIP;
    }

    if ((pInstance->u4_Length == 3) && (pInstance->pu4_OidList[0] == 10))
    {

        ErpsGetClearValue (u4FsErpsContextId, u4FsErpsRingId,
                           &i4RetValFsErpsRingConfigSwitchCmd);

        if (i4RetValFsErpsRingConfigSwitchCmd)
        {
            pData->i4_SLongValue = i4RetValFsErpsRingConfigSwitchCmd;
        }
    }

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    return MSR_SAVE;

}

/************************************************************************
 *  Function Name   : MsrValErpsSaveRingRowStatus
 *  Description     : This function validates the ERPS table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValErpsSaveRingRowStatus (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* In MSR while storing the objects in the table, the object's 
     * position in the table is filled in the pInstance. 
     * For VlanStatic table the Indices are ContextId and VlanId.
     * In this routine we have to save only the Rowstatus. 
     * So at first we have to check for the length of the Indices (3) 
     * and then we have look for the object's position in the table(2).*/

    if (pInstance->u4_Length == 3)
    {
        if (pInstance->pu4_OidList[0] == 15)
        {
            return MSR_SAVE;
        }
    }
    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValErpsRingConfigDistPortSave
 *  Description     : This function validates the DistributingPort
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValErpsRingConfigDistPortSave (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    /* Since DistributingPort,Port1Present and Port2Present 
     * object value depends on fsErpsRingRowStatus object, 
     * we are saving fsErpsRingConfigEntry object value 
     * before saving fsErpsRingRowStatus object */
    if (pInstance->u4_Length == 3)
    {
        if ((pInstance->pu4_OidList[0] == 15) ||
            (pInstance->pu4_OidList[0] == 14) ||
            (pInstance->pu4_OidList[0] == 13))
        {
            return MSR_SAVE;
        }
    }
    return MSR_SKIP;
}

#endif /*ERPS_WANTED */

#ifdef ELPS_WANTED
/************************************************************************
 *  Function Name   : MsrValidateElpsContextEntry
 *  Description     : This function skips the storage of PG row status.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateElpsContextEntry (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4ContextId = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    u4ContextId = pInstance->pu4_OidList[1];

    if ((pInstance->pu4_OidList[0] >= 3) && (pInstance->pu4_OidList[0] <= 5))
    {
        if (ElpsApiIsElpsStartedInContext (u4ContextId) == OSIX_FALSE)
        {
            return MSR_SKIP;
        }
    }

    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValidateElpsSkipPgCfmRowStatus
 *  Description     : This function skips the storage of PG CFM row status.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 ************************************************************************/
INT4
MsrValidateElpsSkipPgCfmRowStatus (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* For PGCfm table the Indices are ContextId and PGId.
     * In this routine we have to save only the Rowstatus.
     * So at first we have to check for the length of the Indices (3)
     * and then we have look for the object's position in the table(7).*/

    if (pInstance->u4_Length == 3)
    {
        if (pInstance->pu4_OidList[0] == ELPS_CFM_TABLE_ROWSTATUS)
        {
            return MSR_SKIP;
        }
    }
    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValidateElpsSavePgCfmRowStatus
 *  Description     : This function save the row status object alone in the 
 *                    ELPS PG CFM table.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 ************************************************************************/
INT4
MsrValidateElpsSavePgCfmRowStatus (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* For PGCfm table the Indices are ContextId and PGId.
     * In this routine we have to save only the Rowstatus.
     * So at first we have to check for the length of the Indices (3)
     * and then we have look for the object's position in the table(7).*/

    if (pInstance->u4_Length == 3)
    {
        if (pInstance->pu4_OidList[0] == ELPS_CFM_TABLE_ROWSTATUS)
        {
            return MSR_SAVE;
        }
    }
    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidateElpsSkipPgRowStatus
 *  Description     : This function skips the storage of PG row status. 
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateElpsSkipPgRowStatus (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* For PGConfig table the Indices are ContextId and PGId.
     * In this routine we have to save only the Rowstatus.
     * So at first we have to check for the length of the Indices (3)
     * and then we have look for the object's position in the table(12).*/

    if (pInstance->u4_Length == 3)
    {
        if (pInstance->pu4_OidList[0] == 13)
        {
            return MSR_SKIP;
        }
    }
    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValidateElpsSavePgRowStatus
 *  Description     : This function save the row status object alone in the 
 *                    ELPS PG Config table.       
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateElpsSavePgRowStatus (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* For PGConfig table the Indices are ContextId and PGId.
     * In this routine we have to save only the Rowstatus.
     * So at first we have to check for the length of the Indices (3)
     * and then we have look for the object's position in the table(12).*/

    if (pInstance->u4_Length == 3)
    {
        if (pInstance->pu4_OidList[0] == 13)
        {
            return MSR_SAVE;
        }
    }
    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidateElpsSkipPgCmdFreeze
 *  Description     : This function skips the storage of
 *                    fsElpsPgCmdExtCmd - freeze (6) for a PG 
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateElpsSkipPgCmdFreeze (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);

    /* We have to skip fsElpsPgCmdExtCmd if the value is freeze (6). */

    if (pInstance->pu4_OidList[0] == 3)
    {
        if (pData->i4_SLongValue == 6)    /* Here freeze is 6 */
        {
            return MSR_VOLATILE_SAVE;
        }
    }
    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValElpsSetPgServListRowStatus 
 *  Description     : This function saves the Row Status of Pg Service List
 *                    Table as CREATE_AND_GO instead of CREATE_AND_WAIT. This
 *                    would ensure that the entry is created as per the MIB.
 *
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValElpsSetPgServListRowStatus (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);

    /* Identify the Pg Service List Row Status CREATE_AND_WAIT value by the 
     * following check. Only in this case  
     * a. Entire RowStatus OID is stored in pOid. So check the length of the
     * pOid equal to the exact RowStatus OID length. Here it is 13
     */

    if (pOid->u4_Length == 13)
    {
        pData->u4_ULongValue = MSR_CREATE_AND_GO;
    }
    return MSR_SAVE;
}
#endif /* ELPS_WANTED */

#ifdef BFD_WANTED
/************************************************************************
 *  Function Name   : MsrValidateBfdSkipSessRowStatus
 *  Description     : This routine  validates the Session Entry Table. 
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateBfdSkipSessRowStatus (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* For Session table the Indices are ContextId and SessionIndex.
     * In this routine we have to skip Row status.
     * So at first we have to check for the length of the Indices (3)
     * and then we have look for the object's position in the table(36).*/

    if (pInstance->u4_Length == 3)
    {
        if ((pInstance->pu4_OidList[0] == 36))
        {
            return MSR_SKIP;
        }
    }
    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValidateBfdSaveSessRowStatus
 *  Description     : This routine  validates the Session Entry. 
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateBfdSaveSessRowStatus (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* For Session table the Indices are ContextId and Session Index.
     * In this routine we have to save only the Rowstatus.
     * So at first we have to check for the length of the Indices (3)
     * and then we have look for the object's position in the table(36).*/

    if (pInstance->u4_Length == 3)
    {
        if (pInstance->pu4_OidList[0] == 36)
        {
            if (pData->i4_SLongValue == NOT_READY)
            {
                pData->i4_SLongValue = NOT_IN_SERVICE;
            }
            return MSR_SAVE;
        }
    }
    return MSR_SKIP;
}
#endif /* BFD_WANTED */

#ifdef CFA_WANTED
/************************************************************************
 *  Function Name   : MsrValIfTypeProtoDenyTable 
 *  Description     : This function saves the Row Status of IfType Proto Deny 
 *                    Table as CREATE_AND_GO instead of CREATE_AND_WAIT. This
 *                    would ensure that the entry is created as per the MIB.
 *
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValIfTypeProtoDenyTable (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);

    pData->u4_ULongValue = MSR_CREATE_AND_GO;
    return MSR_SAVE;
}

#endif

#ifdef LA_WANTED
/************************************************************************
 *  Function Name   : MsrValidateLAPortActorAdminState 
 *  Description     : This function validates the LAPortActorAdminState
 *                    and LAPortPartnerAdminState objects.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateLAPortActorAdminState (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);

    /* LAPortActorAdminState and LAPortPartnerAdminState has 5 read-only bits. 
     * These values cannot be set. Hence here those 5 bits are masked and only 
     * read-write bits are saved. */
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To avoid XE ports configurations in Stacking case */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }

    if ((pInstance->pu4_OidList[0] == 20) || (pInstance->pu4_OidList[0] == 22))
    {
        pData->pOctetStrValue->pu1_OctetList[0] = MSR_LA_PORT_STATE_MASK &
            pData->pOctetStrValue->pu1_OctetList[0];
    }

    return (MSR_SAVE);
}

 /************************************************************************
 *  Function Name   : MsrValidateSkipLAPortActorAdminState
 *  Description     : This function validates the LAPortActorAdminState
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateSkipLAPortActorAdminState (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4IfIndex;
    UINT2               u2AggId;
    UINT2               au2ConfPorts[L2IWF_MAX_PORTS_PER_CONTEXT];
    UINT2               u2NumPorts = 0;
    INT4                i4RetVal = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    u4IfIndex = pInstance->pu4_OidList[1];

    L2IwfGetPortChannelForPort (u4IfIndex, &u2AggId);

    L2IwfGetConfiguredPortsForPortChannel (u2AggId, au2ConfPorts, &u2NumPorts);

    if (u2NumPorts != 0)
    {
        if ((pInstance->pu4_OidList[0] == 4) && (au2ConfPorts[0] != u4IfIndex))
        {
            return (MSR_SKIP);
        }
    }

    i4RetVal = MsrValidateLAPortActorAdminState (pOid, pInstance, pData);

    return i4RetVal;
}

/************************************************************************
 *  Function Name   : MsrValidateSkipLAAdminMacAddressAndMacSelection
 *  Description     : This function validates fsLaPortChannelAdminMacAddress
 *                    and fsLaPortChannelMacSelection objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateSkipLAAdminMacAddressAndMacSelection (tSNMP_OID_TYPE * pOid,
                                                 tSNMP_OID_TYPE * pInstance,
                                                 tSNMP_MULTI_DATA_TYPE * pData)
{

    tMacAddr            BaseMac;
    tMacAddr            TempMacAddr;

    MEMSET (BaseMac, 0, sizeof (tMacAddr));
    MEMSET (TempMacAddr, 0, sizeof (tMacAddr));

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if ((pInstance->pu4_OidList[0] == 4) || (pInstance->pu4_OidList[0] == 3)
        || (pInstance->pu4_OidList[0] == 18))
    {
        return (MSR_SKIP);
    }

#ifdef ICCH_WANTED
    if ((nmhGetFsLaMCLAGSystemID ((tMacAddr *) & BaseMac)) == SNMP_SUCCESS)
    {
        /* Display the command, if the system mac id is not zero */
        if ((MSR_MEMCMP (&BaseMac, TempMacAddr, STRLEN (BaseMac))) == 0)
        {
            /* Skip the following Port Channel MCLAG properties here
             * 
             * FsLaPortChannelMCLAGStatus
             * FsLaPortChannelMCLAGSystemID
             * FsLaPortChannelMCLAGSystemPriority
             * FsLaPortChannelMCLAGPeriodicSyncTime
             */
            if ((pInstance->pu4_OidList[0] == 31)
                || (pInstance->pu4_OidList[0] == 32)
                || (pInstance->pu4_OidList[0] == 30)
                || (pInstance->pu4_OidList[0] == 29))
            {
                return (MSR_SKIP);
            }
        }
    }
#endif
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateSkipDLAGProperties
 *  Description     : This function validates DLAG Port Channel
 *                    Objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateSkipDLAGProperties (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData)
{
    tMacAddr            BaseMac;
    tMacAddr            TempMacAddr;
#ifdef ICCH_WANTED
    INT4                i4MCLAGSystemStatus = 0;
#endif
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    MEMSET (BaseMac, 0, sizeof (tMacAddr));
    MEMSET (TempMacAddr, 0, sizeof (tMacAddr));

#ifdef DLAG_WANTED
    if ((nmhGetFsLaDLAGSystemID ((tMacAddr *) & BaseMac)) == SNMP_SUCCESS)
    {
        /* Display the command, if the system mac id is not zero */
        if ((MSR_MEMCMP (&BaseMac, TempMacAddr, STRLEN (BaseMac))) == 0)
        {
            /* Skip the following Port Channel DLAG properties when 
             * DLAG is enabled globally.
             * The port channel will inherit the global DLAG properties
             * fsLaPortChannelDLAGDistributingPortList
             * fsLaPortChannelDLAGPeriodicSyncTime
             * fsLaPortChannelDLAGSystemPriority
             * fsLaPortChannelDLAGSystemID
             * fsLaPortChannelDLAGDistributingPortIndex
             * fsLaPortChannelDLAGStatus                      */
            if ((pInstance->pu4_OidList[0] == 12)
                || (pInstance->pu4_OidList[0] == 13)
                || (pInstance->pu4_OidList[0] == 14)
                || (pInstance->pu4_OidList[0] == 15)
                || (pInstance->pu4_OidList[0] == 18)
                || (pInstance->pu4_OidList[0] == 28))
            {
                return (MSR_SKIP);
            }
        }
    }
#endif
#ifdef ICCH_WANTED
    nmhGetFsLaMCLAGSystemStatus (&i4MCLAGSystemStatus);
    if (i4MCLAGSystemStatus == ISS_TRUE)
    {
        MEMSET (TempMacAddr, 0, sizeof (tMacAddr));
        if ((nmhGetFsLaMCLAGSystemID ((tMacAddr *) & BaseMac)) == SNMP_SUCCESS)
        {
            /* Skip the following Port Channel MCLAG properties here
             *
             * FsLaPortChannelMCLAGStatus
             * FsLaPortChannelMCLAGSystemID
             * FsLaPortChannelMCLAGSystemPriority
             * FsLaPortChannelMCLAGPeriodicSyncTime
             */
            /* Display the command, if the system mac id is not zero */
            if ((MSR_MEMCMP (&BaseMac, TempMacAddr, STRLEN (BaseMac))) == 0)
            {
                if ((pInstance->pu4_OidList[0] == 31)
                    || (pInstance->pu4_OidList[0] == 32)
                    || (pInstance->pu4_OidList[0] == 30)
                    || (pInstance->pu4_OidList[0] == 29))
                {
                    return (MSR_SKIP);
                }
            }
        }

    }
#endif
    UNUSED_PARAM (pInstance);
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateSkipLADot3adAggActorAdminKey
 *  Description     : This function validates fsLaPortChannelAdminMacAddress
 *                    and fsLaPortChannelMacSelection objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateSkipLADot3adAggActorAdminKey (tSNMP_OID_TYPE * pOid,
                                         tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if ((pInstance->pu4_OidList[0] == 6))
    {
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateLASaveDefaultPort
 *  Description     : This function validates the LAPortActorAdminState
 *                    and LAPortPartnerAdminState objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateLASaveDefaultPort (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if ((pInstance->pu4_OidList[0] == 9))
    {
        return (MSR_SAVE);
    }
    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateLAPortResetAdminState 
 *  Description     : This function validates the LAResetActorAdminState.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateLAPortResetAdminState (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData)
{
    tSNMP_OCTET_STRING_TYPE RetPortState;
    UINT1               u1PortState = 0;

    UNUSED_PARAM (pOid);
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To avoid XE ports configurations in Stacking case */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    /*
     * Dot3adAggPortActorAdminState & fsLaPortActorResetAdminState used to set
     * and reset the Admin State values. The former can only set the states and
     * the latter can only reset the states. Also the read of the latter is no
     * supported. Hence, here the former's value is obtained and the bits that
     * are to be reset are masked and stored in the latter's value.
     */

    RetPortState.pu1_OctetList = &u1PortState;
    RetPortState.i4_Length = sizeof (UINT1);

    if (pInstance->pu4_OidList[0] == 4)
    {
        LA_LOCK ();

        if (nmhGetDot3adAggPortActorAdminState
            ((INT4) pInstance->pu4_OidList[1], &RetPortState) != SNMP_SUCCESS)
        {
            LA_UNLOCK ();
            return (MSR_SKIP);
        }

        LA_UNLOCK ();

        pData->pOctetStrValue->pu1_OctetList[0] =
            (UINT1) (MSR_LA_PORT_STATE_MASK & ~(RetPortState.pu1_OctetList[0]));
    }

    u1PortState = 0;
    /*
     * Dot3adAggPortPartnerAdminState & fsLaPortPartnerResetAdminState are
     * used to set and reset the Admin State values.
     * The former can only set the states and
     * the latter can only reset the states. Also the read of the latter is no
     * supported. Hence, here the former's value is obtained and the bits that
     * are to be reset are masked and stored in the latter's value.
     */

    RetPortState.pu1_OctetList = &u1PortState;
    RetPortState.i4_Length = sizeof (UINT1);

    if (pInstance->pu4_OidList[0] == 6)
    {
        LA_LOCK ();

        if (nmhGetDot3adAggPortPartnerAdminState
            ((INT4) pInstance->pu4_OidList[1], &RetPortState) != SNMP_SUCCESS)
        {
            LA_UNLOCK ();
            return (MSR_SKIP);
        }

        LA_UNLOCK ();

        pData->pOctetStrValue->pu1_OctetList[0] =
            (UINT1) (MSR_LA_PORT_STATE_MASK & ~(RetPortState.pu1_OctetList[0]));
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateLaSkipLacpPortMode
 *  Description     : This function skips saving the LacpPortMode of 
 *                    fsLaPortTable 
 *                    members of the table.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateLaSkipLacpPortMode (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To avoid XE ports configurations in Stacking case */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }

    if (pInstance->pu4_OidList[0] == 2)
    {
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateLaSaveLacpPortMode
 *  Description     : This function saves only LacpPortMode and skip all 
 *                    other members in the fsLaPorttable.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateLaSaveLacpPortMode (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData)
{

    UINT4               u4IfIndex = 0;
    UINT2               u2AggId = 0;
    UINT2               au2ConfPorts[L2IWF_MAX_PORTS_PER_CONTEXT];
    UINT2               u2NumPorts = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    u4IfIndex = pInstance->pu4_OidList[1];

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To avoid XE ports configurations in Stacking case */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }

    L2IwfGetPortChannelForPort (u4IfIndex, &u2AggId);

    L2IwfGetConfiguredPortsForPortChannel (u2AggId, au2ConfPorts, &u2NumPorts);

    if (u2NumPorts != 0)
    {
        if (pInstance->pu4_OidList[0] != 2)
        {
            return (MSR_SKIP);
        }
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateLaActorSystemId
 *  Description     : This function skips the save when the switch mac 
 *                    address is equal to LACP system identifier.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateLaActorSystemId (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData)
{

    tMacAddr            SystemID;
    tMacAddr            MacAddress;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pData);

    MSR_MEMSET (SystemID, 0, sizeof (tMacAddr));
    MSR_MEMSET (MacAddress, 0, sizeof (tMacAddr));

    LA_LOCK ();
    /* If the system identifier name and the mac-address of the switch are equal, it is skipped here */
    nmhGetFsLaActorSystemID (&SystemID);
    nmhGetIssSwitchBaseMacAddress (&MacAddress);

    if (ISS_ARE_MAC_ADDR_EQUAL (MacAddress, SystemID) == ISS_TRUE)
    {
        LA_UNLOCK ();
        return MSR_SKIP;
    }

    LA_UNLOCK ();
    return (MSR_SAVE);

}
#endif

#ifdef PNAC_WANTED
/************************************************************************
 *  Function Name   : MsrValidatePNACObjects
 *  Description     : This function validates the PNAC tables
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidatePNACObjects (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               au1UserName[PNAC_MAX_STR_LEN + 1];
    UINT1               au1Passwd[PNAC_MAX_STR_LEN + 1];
    UINT1               u1Count = 0;
    UNUSED_PARAM (pOid);

    /* Storing the unencrypted password from the table. */

    if (pInstance->pu4_OidList[0] == 2 && pOid->u4_Length != 12)
    {
        /* The pOid->u4_Length!=12 check is done so as to avoid this validation
           in case the object is  rowstatus with index length 2 */
        for (u1Count = 0; u1Count < pInstance->pu4_OidList[1]; u1Count++)
        {
            au1UserName[u1Count] =
                (UINT1) (pInstance->pu4_OidList[u1Count + 2]);
        }
        au1UserName[u1Count] = '\0';

        if (PnacGetLocAuthServPassword (au1UserName, au1Passwd) == PNAC_FAILURE)
        {
            return (MSR_SKIP);
        }
        pData->pOctetStrValue->i4_Length = (INT4) STRLEN (au1Passwd);
        MSR_MEMCPY (pData->pOctetStrValue->pu1_OctetList, au1Passwd,
                    pData->pOctetStrValue->i4_Length);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidatePnacMacBasedObjects
 *  Description     : This function validates the PNAC objects before
 *                    saving any PNAC objects.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidatePnacMacBasedObjects (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetVal = 0;
    INT4                i4IfIndex = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    i4IfIndex = (INT4) pInstance->pu4_OidList[1];

    PNAC_LOCK ();
    nmhGetFsPnacPaePortAuthMode (i4IfIndex, &i4RetVal);
    if (i4RetVal == PNAC_PORT_AUTHMODE_MACBASED)
    {
        PNAC_UNLOCK ();
        return (MSR_SKIP);
    }

    PNAC_UNLOCK ();
    return (MSR_SAVE);

}
#endif

#if defined ( PIM_WANTED) || defined (PIMV6_WANTED)
/************************************************************************
 *  Function Name   : MsrValidatePimComponentEntry
 *  Description     : This function validates the pimComponentEntry table.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidatePimComponentEntry (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData)
{
    tSNMP_OCTET_STRING_TYPE ScopeZoneName;
    UINT1               au1ScopeZoneName[IP6_SCOPE_ZONE_NAME_LEN];

    MEMSET (au1ScopeZoneName, 0, IP6_SCOPE_ZONE_NAME_LEN);
    ScopeZoneName.pu1_OctetList = au1ScopeZoneName;

    /* Scope Zone Name with length equal to zero should not be saved. */
    if ((pOid->u4_Length != 13) && (pInstance->pu4_OidList[0] == 5))
    {
        nmhGetFsPimStdComponentScopeZoneName ((INT4) pInstance->pu4_OidList[1],
                                              &ScopeZoneName);

        if (ScopeZoneName.i4_Length == 0)
        {
            return (MSR_SKIP);
        }
    }

    /* The pimComponentEntry table uses a CREATE AND GO to 
     * create a row in the table instead of the CREATE AND 
     * WAIT. So for this table the value of the RowStatus, 
     * to begin with, should be set to CREATE AND GO.
     */

    if (pOid->pu4_OidList[pOid->u4_Length - 1] != 0)
    {
        pData->i2_DataType = SNMP_DATA_TYPE_INTEGER;
        pData->u4_ULongValue = MSR_CREATE_AND_GO;
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidatePimStatus 
 *  Description     : This function make sure that all the static 
 *                    configurations like components and interfaces 
 *                    created are restored if pim status has
 *                    been disabled.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 ************************************************************************/

INT4
MsrValidatePimStatus (tSNMP_OID_TYPE * pOid,
                      tSNMP_OID_TYPE * pInstance, tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);

    /* If previously the PIM status was disable with
     * interfaces and components created and if router is again
     * coming up, then the created components and interfaces are not
     * restored because the PIM Status was Globally disabled*/

    /* If the Pim status was disable then it makes the status as
     * enable*/

    if (pData->i4_SLongValue == 2)
    {
        pData->i4_SLongValue = 1;
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsPimCmnStaticRowStatusSkip 
 *  Description     : This function validates the RowStatus for the 
 *                    PimCmnStaticRPSetEntry table.If the value of 
 *                    rowstatus is skipped when it is not creat and wait
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateFsPimCmnStaticRowStatusSkip (tSNMP_OID_TYPE * pOid,
                                        tSNMP_OID_TYPE * pInstance,
                                        tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pData);
    if ((pInstance->pu4_OidList[0] == 6) && (pOid->u4_Length == 12))
    {
        return MSR_SKIP;
    }
    /* Save the rowstatus if its value is creat and wait */
    else
    {
        return MSR_SAVE;
    }
    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidateFsPimCmnStaticRowStatusSave
 *  Description     : This function validates RowStatus for the 
 *                    PimCmnStaticRPSetEntry table. The rowstatus value
 *                    is saved if its value is other than creat and wait.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateFsPimCmnStaticRowStatusSave (tSNMP_OID_TYPE * pOid,
                                        tSNMP_OID_TYPE * pInstance,
                                        tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pData);
    /* Save the rowstatus if its value is not creat and wait */
    if ((pInstance->pu4_OidList[0] == 6) && (pOid->u4_Length == 12))
    {
        return MSR_SAVE;
    }
    return MSR_SKIP;
}
#endif
#if defined(IGS_WANTED) ||  defined (MLDS_WANTED)
/************************************************************************
 *  Function Name   : MsrValidateRtrPortEntry
 *  Description     : This function makes sure that only the static router 
 *                    port entries are saved and restored.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateRtrPortEntry (tSNMP_OID_TYPE * pOid,
                         tSNMP_OID_TYPE * pInstance,
                         tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4PhyPortNum = 0;
    UINT4               u4VlanId = 0;
    UINT1               u1VarCount = 0;
    UINT1               u1AddrType = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    u4PhyPortNum = (UINT4) pInstance->pu4_OidList[++u1VarCount];
    u4VlanId = (UINT4) pInstance->pu4_OidList[++u1VarCount];
    u1AddrType = (UINT1) pInstance->pu4_OidList[++u1VarCount];

    /* Skip the saving if it is not a static router port */
    if (SnoopUtilIsStaticEntry (u4PhyPortNum, u4VlanId, u1AddrType) == FALSE)
    {
        return (MSR_VOLATILE_SAVE);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsSnoopSendLeaveOnTopoChangeObj
 *  Description     : This function blocks the saving of this object
 *                    fsSnoopSendLeaveOnTopoChange.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateFsSnoopSendLeaveOnTopoChangeObj (tSNMP_OID_TYPE * pOid,
                                            tSNMP_OID_TYPE * pInstance,
                                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (pInstance->pu4_OidList[0] == 14)
    {
        return (MSR_SKIP);
    }

    return (MSR_SAVE);
}
#endif
#ifdef IGMP_WANTED
/************************************************************************
 *  Function Name   : MsrValidateIsStaticEntry
 *  Description     : This function makes sure that only static entries
 *                    are saved and restored.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateIsStaticEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               au1IpAddr[4] = { 0 };
    UINT4               u4IpAddr = 0;
    UINT4               u4IfIndex = 0;
    UINT1               u1LoopVar = 0;
    UINT1               u1TempRowStat = 1;

    UNUSED_PARAM (pData);
    /*Currently IGMP does not support CREATE_AND_WAIT, so while
       saving this table set the row status to CREATE_AND_GO or to
       active , so that while restoring this table will not be
       called with CREATE_AND_WAIT, here we have used CREATE_AND_GO */

    /* skip the saving of the row status object, when it is ACTIVE */
    if (pOid->pu4_OidList[10] == 7)
    {
        if (pData->u4_ULongValue == MSR_ACTIVE)
        {
            return (MSR_SKIP);
        }
        pData->u4_ULongValue = MSR_CREATE_AND_GO;
    }

    if (pOid->u4_Length == OID_LENGTH_ELEVEN)
    {
        u1TempRowStat = 0;
    }
    for (u1LoopVar = 0; u1LoopVar < 4; u1LoopVar++)
    {
        au1IpAddr[u1LoopVar] = (UINT1)
            (pInstance->pu4_OidList[u1LoopVar + u1TempRowStat]);
    }

    MEMCPY (&u4IpAddr, au1IpAddr, 4);
    u4IpAddr = OSIX_NTOHL (u4IpAddr);

    u4IfIndex = pInstance->pu4_OidList[u1LoopVar];

    if (IgmpUtilIsStaticEntry (u4IpAddr, u4IfIndex,
                               IPVX_ADDR_FMLY_IPV4, OSIX_TRUE) != TRUE)
    {
        return (MSR_SKIP);
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateIsStaticSrcEntry
 *  Description     : This function makes sure that only static entries
 *                    are saved and restored.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateIsStaticSrcEntry (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               au1IpAddr[4] = { 0 };
    UINT1               au1SrcAddr[4] = { 0 };
    UINT4               u4IpAddr = 0;
    UINT4               u4SrcAddr = 0;
    UINT4               u4IfIndex = 0;
    UINT1               u1LoopVar = 0;
    UINT1               u1SrcIndex = 0;
    UINT1               u1StartOfSrcIndex = 0;
    UINT1               u1TempRowStat = 1;

    UNUSED_PARAM (pData);
    /*Currently IGMP does not support CREATE_AND_WAIT, so while
       saving this table set the row status to CREATE_AND_GO or to
       active , so that while restoring this table will not be
       called with CREATE_AND_WAIT, here we have used CREATE_AND_GO */

    /* skip the saving of row status object, when it is ACTIVE */
    if (pOid->pu4_OidList[10] == 4)
    {
        if (pData->u4_ULongValue == MSR_ACTIVE)
        {
            return (MSR_SKIP);
        }
        pData->u4_ULongValue = MSR_CREATE_AND_GO;
    }

    if (pOid->u4_Length == OID_LENGTH_ELEVEN)
    {
        u1TempRowStat = 0;
    }
    for (u1LoopVar = 0; u1LoopVar < 4; u1LoopVar++)
    {
        au1IpAddr[u1LoopVar] = (UINT1)
            (pInstance->pu4_OidList[u1LoopVar + u1TempRowStat]);
    }

    MEMCPY (&u4IpAddr, au1IpAddr, 4);
    u4IpAddr = OSIX_NTOHL (u4IpAddr);

    u4IfIndex = pInstance->pu4_OidList[u1LoopVar];

    u1StartOfSrcIndex = (UINT1) (u1LoopVar + u1TempRowStat + 1);

    for (u1LoopVar = u1StartOfSrcIndex; u1LoopVar < (u1StartOfSrcIndex + 4);
         u1LoopVar++)
    {
        au1SrcAddr[u1SrcIndex++] = (UINT1) (pInstance->pu4_OidList[u1LoopVar]);
    }

    MEMCPY (&u4SrcAddr, au1SrcAddr, 4);
    u4SrcAddr = OSIX_NTOHL (u4SrcAddr);

    if (IgmpUtilIsStaticEntry (u4IpAddr, u4IfIndex,
                               IPVX_ADDR_FMLY_IPV4, OSIX_FALSE) != TRUE)
    {
        return (MSR_SKIP);
    }
    if (IgmpUtilIsSourceConfigured (u4IpAddr, u4SrcAddr,
                                    (INT4) u4IfIndex,
                                    IPVX_ADDR_FMLY_IPV4) != TRUE)
    {
        return (MSR_SKIP);
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateIgmpSSMMapRowStatus
 *  Description     : This function makes sure that SSM map entries
 *                    are saved and restored only for Row Status ACTIVE.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateIgmpSSMMapRowStatus (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    if (pOid->u4_Length != 11)
    {
        if (pData->i4_SLongValue == 5)
        {
            return MSR_SKIP;
        }
    }
    UNUSED_PARAM (pInstance);
    return MSR_SAVE;
}

#endif

/************************************************************************
 *  Function Name   : MsrValidateSecondaryIp
 *  Description     : This function validates the Secondary IP Address.
 *                    Only User created entries are saved.
 *                   
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateSecondaryIp (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4Offset = 0;
    UINT4               u4SecondaryIp = 0;
    UINT1               u1AddrAllocMethod = 0;

    UNUSED_PARAM (pData);

    if (pOid->u4_Length != 12)
    {
        u4Offset = 1;
    }

    u4SecondaryIp = (UINT4) ((pInstance->pu4_OidList[u4Offset + 1] << 24) |
                             (pInstance->pu4_OidList[u4Offset + 2] << 16) |
                             (pInstance->pu4_OidList[u4Offset + 3] << 8) |
                             pInstance->pu4_OidList[u4Offset + 4]);

    CfaGetAddrAllocMethodForSecIp (pInstance->pu4_OidList[u4Offset],
                                   u4SecondaryIp, &u1AddrAllocMethod);

    if ((CfaIsMgmtPort (pInstance->pu4_OidList[u4Offset]) == TRUE) &&
        (CfaIsDualOobEnabled () == OSIX_TRUE))
    {
        return MSR_SKIP;
    }

    if (u1AddrAllocMethod == CFA_IP_IF_MANUAL)
    {
        return MSR_SAVE;
    }

    return MSR_SKIP;
}

#ifdef VRRP_WANTED
/************************************************************************
 *  Function Name   : MsrValidateVrrpSkipAdminState 
 *  Description     : This function skips saving the Admin State of the VRRP
 *                    Oper Table Entry.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateVrrpSkipAdminStateAndPriority (tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pInstance,
                                          tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4Version = 0;

    nmhGetFsVrrpVersionSupported (&i4Version);

    if (i4Version != VRRP_VERSION_2)
    {
        return (MSR_SKIP);
    }

    if (pOid->u4_Length != 11)
    {
        /* Skip AdminState */
        if (pInstance->pu4_OidList[0] == 4)
        {
            return (MSR_SKIP);
        }

        /* Skip RowStatus as ACTIVE */
        if ((pInstance->pu4_OidList[0] == 15) && (pData->i4_SLongValue == 1))
        {
            return (MSR_SKIP);
        }

        /* Get the value of vrrpOperAuthKey from the data structure
         * before MSR save */
        if (pInstance->pu4_OidList[0] == 10)
        {
            if ((GetVrrpOperAuthKey (pInstance->pu4_OidList[1],
                                     (INT4) pInstance->pu4_OidList[2],
                                     pData->pOctetStrValue)) != MSR_TRUE)
            {
                return (MSR_SKIP);
            }
        }

        /* Skip OperAdverTime */
        if (pInstance->pu4_OidList[0] == 11)
        {
            return (MSR_SKIP);
        }
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateVrrpSaveAdminState 
 *  Description     : This function saves the Admin State of the VRRP
 *                    Oper Table Entry.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateVrrpSaveAdminState (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4Version = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    nmhGetFsVrrpVersionSupported (&i4Version);

    if (i4Version != VRRP_VERSION_2)
    {
        return (MSR_SKIP);
    }

    if (((pInstance->pu4_OidList[0] == 4) && (pData->i4_SLongValue == 1)) ||
        ((pInstance->pu4_OidList[0] == 15) && (pData->i4_SLongValue == 1)))
    {
        return (MSR_SAVE);
    }

    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateFsVrrpTable
 *  Description     : This function validates of the FS VRRP
 *                    Table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateFsVrrpTable (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4Version = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    nmhGetFsVrrpVersionSupported (&i4Version);

    if (i4Version != VRRP_VERSION_2)
    {
        return (MSR_SKIP);
    }

    /* Skip Admin Priority */
    if (pInstance->pu4_OidList[0] == 1)
    {
        return (MSR_SKIP);
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateVrrpAssoIpAddrSetCreateAndGo
 *  Description     : This function saves the RowStatus of the VRRP
 *                    AssoIpAddrTable with value CreateAndGo.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateVrrpAssoIpAddrSetCreateAndGo (tSNMP_OID_TYPE * pOid,
                                         tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4Version = 0;

    UNUSED_PARAM (pInstance);

    nmhGetFsVrrpVersionSupported (&i4Version);

    if (i4Version != VRRP_VERSION_2)
    {
        return MSR_SKIP;
    }

    /* Save the rowstatus value with createAndGo */
    if (pOid->u4_Length == 11)
    {
        pData->u4_ULongValue = MSR_CREATE_AND_GO;
        return MSR_SAVE;
    }

    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidateVrrpv3SkipRowStatus
 *  Description     : This function skips saving the Row Status of the VRRP
 *                    V3 Oper Table Entry with ACTIVE value.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateVrrpv3SkipRowStatus (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4Version = 0;

    nmhGetFsVrrpVersionSupported (&i4Version);

    if (i4Version == VRRP_VERSION_2)
    {
        return (MSR_SKIP);
    }

    if (pOid->u4_Length != 12)
    {
        /* Skip RowStatus as ACTIVE */
        if ((pInstance->pu4_OidList[0] == 13) && (pData->i4_SLongValue == 1))
        {
            return (MSR_SKIP);
        }
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateVrrpv3SaveRowStatus
 *  Description     : This function saves the Row Status of the VRRP V3
 *                    Oper Table Entry.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateVrrpv3SaveRowStatus (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4Version = 0;

    UNUSED_PARAM (pOid);

    nmhGetFsVrrpVersionSupported (&i4Version);

    if (i4Version == VRRP_VERSION_2)
    {
        return (MSR_SKIP);
    }

    /* Skip RowStatus as ACTIVE */
    if ((pInstance->pu4_OidList[0] == 13) && (pData->i4_SLongValue == 1))
    {
        return (MSR_SAVE);
    }

    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateFsVrrpv3Table
 *  Description     : This function validates of the FS VRRP V3
 *                    Table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateFsVrrpv3Table (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4Version = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pData);

    nmhGetFsVrrpVersionSupported (&i4Version);

    if (i4Version == VRRP_VERSION_2)
    {
        return (MSR_SKIP);
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateVrrpv3AssoIpAddrSetCreateAndGo
 *  Description     : This function saves the RowStatus of the VRRP V3
 *                    AssoIpAddrTable with value CreateAndGo.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateVrrpv3AssoIpAddrSetCreateAndGo (tSNMP_OID_TYPE * pOid,
                                           tSNMP_OID_TYPE * pInstance,
                                           tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4Version = 0;

    UNUSED_PARAM (pInstance);

    nmhGetFsVrrpVersionSupported (&i4Version);

    if (i4Version == VRRP_VERSION_2)
    {
        return MSR_SKIP;
    }

    /* Save the rowstatus value with createAndGo */
    if (pOid->u4_Length == 12)
    {
        pData->u4_ULongValue = MSR_CREATE_AND_GO;
        return MSR_SAVE;
    }

    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidateVrrpSaveOnlyTrackRowStatusCW
 *  Description     : This function saves the RowStatus of the VRRP V2
 *                    Track RowStatus with Create And Wait alone.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateVrrpSaveOnlyTrackRowStatusCW (tSNMP_OID_TYPE * pOid,
                                         tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4Version = 0;

    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pData);

    nmhGetFsVrrpVersionSupported (&i4Version);

    if (i4Version != VRRP_VERSION_2)
    {
        return MSR_SKIP;
    }

    /* Save the rowstatus value with createAndWait */
    if (pOid->u4_Length == 12)
    {
        return MSR_SAVE;
    }

    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidateVrrpTrackIfSetCreateAndGo
 *  Description     : This function saves the RowStatus of the VRRP V3
 *                    Track If Table with value CreateAndGo.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateVrrpTrackIfSetCreateAndGo (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4Version = 0;

    UNUSED_PARAM (pInstance);

    nmhGetFsVrrpVersionSupported (&i4Version);

    if (i4Version != VRRP_VERSION_2)
    {
        return MSR_SKIP;
    }

    /* Save the rowstatus value with createAndGo */
    if (pOid->u4_Length == 12)
    {
        pData->u4_ULongValue = MSR_CREATE_AND_GO;
        return MSR_SAVE;
    }

    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidateVrrpSaveTrack
 *  Description     : This function saves the Tracked Links and Row
 *                    Status as ACTIVE.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateVrrpSaveTrack (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4Version = 0;

    UNUSED_PARAM (pOid);

    nmhGetFsVrrpVersionSupported (&i4Version);

    if (i4Version != VRRP_VERSION_2)
    {
        return MSR_SKIP;
    }

    if ((pInstance->pu4_OidList[0] == 3) && (pData->i4_SLongValue == 1))
    {
        return MSR_SAVE;
    }

    if (pInstance->pu4_OidList[0] == 2)
    {
        return MSR_SAVE;
    }

    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidateVrrpv3SaveOnlyTrackRowStatusCW
 *  Description     : This function saves the RowStatus of the VRRP V3
 *                    Track RowStatus with Create And Wait alone.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateVrrpv3SaveOnlyTrackRowStatusCW (tSNMP_OID_TYPE * pOid,
                                           tSNMP_OID_TYPE * pInstance,
                                           tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4Version = 0;

    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pData);

    nmhGetFsVrrpVersionSupported (&i4Version);

    if (i4Version == VRRP_VERSION_2)
    {
        return MSR_SKIP;
    }

    /* Save the rowstatus value with createAndWait */
    if (pOid->u4_Length == 14)
    {
        return MSR_SAVE;
    }

    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidateVrrpv3TrackIfSetCreateAndGo
 *  Description     : This function saves the RowStatus of the VRRP V3
 *                    Track If Table with value CreateAndGo.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateVrrpv3TrackIfSetCreateAndGo (tSNMP_OID_TYPE * pOid,
                                        tSNMP_OID_TYPE * pInstance,
                                        tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4Version = 0;

    UNUSED_PARAM (pInstance);

    nmhGetFsVrrpVersionSupported (&i4Version);

    if (i4Version == VRRP_VERSION_2)
    {
        return MSR_SKIP;
    }

    /* Save the rowstatus value with createAndGo */
    if (pOid->u4_Length == 14)
    {
        pData->u4_ULongValue = MSR_CREATE_AND_GO;
        return MSR_SAVE;
    }

    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidatev3VrrpSaveTrack
 *  Description     : This function saves the V3 Tracked Links and Row
 *                    Status as ACTIVE.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateVrrpv3SaveTrack (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4Version = 0;

    UNUSED_PARAM (pOid);

    nmhGetFsVrrpVersionSupported (&i4Version);

    if (i4Version == VRRP_VERSION_2)
    {
        return MSR_SKIP;
    }

    if ((pInstance->pu4_OidList[0] == 3) && (pData->i4_SLongValue == 1))
    {
        return MSR_SAVE;
    }

    if (pInstance->pu4_OidList[0] == 2)
    {
        return MSR_SAVE;
    }

    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidateVrrpv3NotificationCntl
 *  Description     : This function validates the fsVrrpv3NotificationCntl.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateVrrpv3NotificationCntl (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4Version = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pData);

    nmhGetFsVrrpVersionSupported (&i4Version);

    if (i4Version != VRRP_VERSION_3 && i4Version != VRRP_VERSION_2_3)
    {
        return MSR_SKIP;
    }

    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValidateVrrpv3Status
 *  Description     : This function validates the fsVrrpv3Status.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateVrrpv3Status (tSNMP_OID_TYPE * pOid,
                         tSNMP_OID_TYPE * pInstance,
                         tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4Version = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pData);

    nmhGetFsVrrpVersionSupported (&i4Version);

    if (i4Version != VRRP_VERSION_3 && i4Version != VRRP_VERSION_2_3)
    {
        return MSR_SKIP;
    }

    return MSR_SAVE;
}

#endif

#ifdef IP6_WANTED
/************************************************************************
 *  Function Name   : MsrValidateIpv6AddrEntry
 *  Description     : This function validates the StaticIpv6RouteEntry
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateIpv6AddrEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4Fsipv6AddrPrefixLen = 0;
    INT4                i4RetValFsipv6AddrType = 0;
    UINT4               u4Count = 0;
    UINT4               u4Cnt = 0;
    UINT4               u4Fsipv6AddrIndex = 0;
    UINT1               au1Array[MSR_MAX_IP_ADDR_LEN];
    UINT1               au1CopyString[MSR_MAX_OID_LEN];
    UINT1               u1CfgMethod = 0;
    tSNMP_OCTET_STRING_TYPE *pFsipv6AddrAddress;
    tIp6Addr            Fsipv6Address;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    MSR_MEMSET (&au1Array, 0, sizeof (au1Array));
    MSR_MEMSET (&au1CopyString, 0, sizeof (au1CopyString));

    /* Check for other objects other than the "AddrAdminStatus"
     * in the table, and set the offset to get the IP6 address for
     * validation.
     */

    if (pInstance->u4_Length == 19)
    {
        u4Cnt = 2;
    }
    else
    {
        u4Cnt = 1;
    }

    pFsipv6AddrAddress = allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
    if (pFsipv6AddrAddress == NULL)
    {
        return MSR_SKIP;
    }

    u4Fsipv6AddrIndex = pInstance->pu4_OidList[u4Cnt - 1];
    i4Fsipv6AddrPrefixLen =
        (INT4) pInstance->pu4_OidList[pInstance->u4_Length - 1];

    for (u4Count = u4Cnt; u4Count <= u4Cnt + 14; u4Count++)
    {
        SPRINTF ((char *) (au1Array), "%d.", (pInstance->pu4_OidList[u4Count]));
        STRNCAT (au1CopyString, au1Array, (sizeof (au1CopyString)
                                           - STRLEN (au1CopyString) - 1));
    }
    SPRINTF ((char *) au1Array, "%d", pInstance->pu4_OidList[u4Count]);
    STRNCAT (au1CopyString, au1Array, (sizeof (au1CopyString)
                                       - STRLEN (au1CopyString) - 1));

    WebnmConvertStringToOctet (pFsipv6AddrAddress, au1CopyString);

    Ip6AddrCopy (&Fsipv6Address,
                 (tIp6Addr *) (VOID *) pFsipv6AddrAddress->pu1_OctetList);

    if ((CfaIsTunnelInterface (u4Fsipv6AddrIndex) == CFA_TRUE) &&
        (Ip6AddrType (&Fsipv6Address) == ADDR6_V4_COMPAT))
    {
        MSR_CLEAR_OCTET_STRING (pFsipv6AddrAddress);
        free_octetstring (pFsipv6AddrAddress);
        return (MSR_SKIP);
    }
    IP6_TASK_LOCK ();
    nmhGetFsipv6AddrType ((INT4) u4Fsipv6AddrIndex,
                          pFsipv6AddrAddress,
                          i4Fsipv6AddrPrefixLen, &i4RetValFsipv6AddrType);
#ifdef LNXIP6_WANTED
    if (i4RetValFsipv6AddrType == ADDR6_LLOCAL)
    {
        Ipv6UtilGetConfigMethod (u4Fsipv6AddrIndex, pFsipv6AddrAddress,
                                 &u1CfgMethod);
    }
#else
    Ipv6UtilGetConfigMethod (u4Fsipv6AddrIndex, pFsipv6AddrAddress,
                             (UINT1) i4Fsipv6AddrPrefixLen,
                             (UINT1) i4RetValFsipv6AddrType, &u1CfgMethod);
#endif
    IP6_TASK_UNLOCK ();
    MSR_CLEAR_OCTET_STRING (pFsipv6AddrAddress);
#ifndef LNXIP6_WANTED
    if ((i4RetValFsipv6AddrType != ADDR6_LLOCAL) &&
        (i4RetValFsipv6AddrType != ADDR6_UNICAST))
    {
        free_octetstring (pFsipv6AddrAddress);
        return MSR_SAVE;
    }
#else
    if (i4RetValFsipv6AddrType != ADDR6_LLOCAL)
    {
        free_octetstring (pFsipv6AddrAddress);
        return MSR_SAVE;
    }
#endif
    else if (u1CfgMethod == IP6_ADDR_STATIC)
    {
        free_octetstring (pFsipv6AddrAddress);
        return MSR_SAVE;
    }
    else
    {
        free_octetstring (pFsipv6AddrAddress);
        return MSR_SKIP;
    }
}

/************************************************************************
 *  Function Name   : MsrValidateFsMIIpv6IfScopeZoneEntry
 *  Description     : This function validates the default entries in 
                      FsMIIpv6IfScopeZone table
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateFsMIIpv6IfScopeZoneEntry (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);

    if ((pInstance->pu4_OidList[0] == 2) || (pInstance->pu4_OidList[0] == 3))
        return MSR_SKIP;

    if (STRCMP (pData->pOctetStrValue->pu1_OctetList, "invalid") == 0)
    {
        return MSR_SKIP;
    }

    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValidateRtm6ControlEntry
 *  Description     : This function validates the default entries in 
                      fsMIRrd6ControlEntry table
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateRtm6ControlEntry (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4Count = 0;
    UINT1               u1IsDefAddr = TRUE;

    UNUSED_PARAM (pData);

    /* Skip CREATE_AND_WAIT for the default index */
    if (pOid->u4_Length == OID_LENGTH_TWELVE)
    {
        if (pInstance->pu4_OidList[0] == VCM_DEFAULT_CONTEXT)    /* RTM6_DEFAULT_CXT_ID */
        {
            for (u4Count = 1; u4Count < 17; u4Count++)
            {
                if (pInstance->pu4_OidList[u4Count] == 0)
                {
                    continue;
                }
                u1IsDefAddr = FALSE;
                break;
            }

            if ((u1IsDefAddr == TRUE)
                && (pInstance->pu4_OidList[u4Count] == IP6_ADDR_MAX_PREFIX))
            {
                return (MSR_SKIP);
            }
        }
    }

    return (MSR_SAVE);
}
#endif

#ifndef SNMP_3_WANTED
/************************************************************************
 *  Function Name   : MsrValidateSnmpTables
 *  Description     : This function validates the SNMP tables
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateSnmpTables (tSNMP_OID_TYPE * pOid,
                       tSNMP_OID_TYPE * pInstance,
                       tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pInstance);

    /* SNMP_2 tables use the EntryStatus instead of RowStatus.
     * So for these tables the equivalent of a create and wait 
     * action will be done by the create request.
     */

    if (pOid->pu4_OidList[10] != 0)
    {
        pData->i2_DataType = SNMP_DATA_TYPE_INTEGER;
        pData->u4_ULongValue = MSR_CREATE_REQUEST;
    }

    return (MSR_SAVE);
}

#endif

#ifdef SNMP_3_WANTED
/************************************************************************
 *  Function Name   : MsrValidateUsmEntry 
 *  Description     : To fill the USM user Auth and Priv passwords 
 *                    since the low level get routines for these 
 *                    objects return NULL string
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateUsmEntry (tSNMP_OID_TYPE * pOid,
                     tSNMP_OID_TYPE * pInstance, tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4Count;
    UINT1               u1TmpCount;
    UINT1               u1TempRowStat = 1;
    UINT1               au1AuthPassWord[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1PrivPassWord[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1UsmUserEngineId[SNMP_MAX_ENGINE_ID_LEN];
    UINT1               au1UsmUserPrivKeyChange[SNMP_MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE UsmUserEngineId;
    tSNMP_OCTET_STRING_TYPE *pUsmUserName = NULL;
    tSNMP_OCTET_STRING_TYPE *pUsmUserAuthKeyChange;
    tSNMP_OCTET_STRING_TYPE UsmUserPrivKeyChange;
    INT4                i4UsmUserStorageType;
    UINT4               u4UsmUserAuthProtocol = 0;
    UINT4               u4UsmUserPrivProtocol = 0;

    UsmUserEngineId.pu1_OctetList = au1UsmUserEngineId;
    UsmUserPrivKeyChange.pu1_OctetList = au1UsmUserPrivKeyChange;
    UsmUserPrivKeyChange.i4_Length = 0;

    MSR_MEMSET (UsmUserEngineId.pu1_OctetList, 0, SNMP_MAX_ENGINE_ID_LEN);
    MSR_MEMSET (UsmUserPrivKeyChange.pu1_OctetList, 0,
                SNMP_MAX_OCTETSTRING_SIZE);

    MSR_MEMSET (au1AuthPassWord, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    MSR_MEMSET (au1PrivPassWord, 0, SNMP_MAX_ADMIN_STR_LENGTH);
    /* If the table object entry  is not rowstatus, skip 
     * pInstance->pu4_OidList [0], as this indicates the object type
     * and pInstance->pu4_OidList [1], as this indicates the length of
     * the First index.
     * If this is a rowstatus then pInstance->pu4_OidList [0] will
     * indicate the length of first index and there will be no 
     * indication of object type
     */
    if (pOid->u4_Length == OID_LENGTH_TWELVE)
    {
        u1TempRowStat = 0;
    }
    else if ((pInstance->pu4_OidList[0] == 7)
             || (pInstance->pu4_OidList[0] == 10))
    {
        return MSR_SKIP;
    }

    UsmUserEngineId.i4_Length = (INT4) pInstance->pu4_OidList[u1TempRowStat];

    /* Copy the Engine ID till the length of Engine ID as given at 
     * pInstance->pu4_OidList [1 or 0], (0 in case of rowsatatus) 
     */
    for (u4Count = 0; (u4Count < (UINT4) UsmUserEngineId.i4_Length)
         && (u4Count < SNMP_MAX_ENGINE_ID_LEN); u4Count++)
    {
        UsmUserEngineId.pu1_OctetList[u4Count] = (UINT1)
            (pInstance->pu4_OidList[u4Count + 1 + u1TempRowStat]);
    }

    /* Value of u4Count will now be the length of Engine ID as
     * present in pInstance->pu4_OidList [1 or 0]. This does not 
     * include the first two(one) positions (one for object type)
     * and the other for the length of Engine ID. Increment 
     * this by 2 or 1 to get the length of User name .
     */

    pUsmUserName = allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
    if (pUsmUserName == NULL)
    {
        return (MSR_SKIP);
    }
    MSR_MEMSET (pUsmUserName->pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
    pUsmUserName->i4_Length = 0;

    u1TmpCount = (UINT1) (u4Count + 1 + u1TempRowStat);

    pUsmUserName->i4_Length = (INT4) pInstance->pu4_OidList[u1TmpCount];

    for (u4Count = 1; (u4Count <= (UINT4) pUsmUserName->i4_Length)
         && (u4Count <= SNMP_MAX_OCTETSTRING_SIZE); u4Count++)
    {
        pUsmUserName->pu1_OctetList[u4Count - 1] = (UINT1)
            (pInstance->pu4_OidList[u4Count + u1TmpCount]);
    }

    if (nmhGetUsmUserStorageType (&UsmUserEngineId, pUsmUserName,
                                  &i4UsmUserStorageType) == SNMP_FAILURE)
    {
        free_octetstring (pUsmUserName);
        return (MSR_SKIP);
    }
    pUsmUserAuthKeyChange = allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
    if (pUsmUserAuthKeyChange == NULL)
    {
        free_octetstring (pUsmUserName);
        return (MSR_SKIP);
    }
    MSR_MEMSET (pUsmUserAuthKeyChange->pu1_OctetList, 0,
                SNMP_MAX_OCTETSTRING_SIZE);
    pUsmUserAuthKeyChange->i4_Length = 0;

    if ((pInstance->pu4_OidList[0] == 6) || (pInstance->pu4_OidList[0] == 9))
    {
        SnmpGetUsmUserPassword (&UsmUserEngineId, pUsmUserName,
                                au1AuthPassWord, au1PrivPassWord);

        SnmpGetUsmUserProtocol (&UsmUserEngineId, pUsmUserName,
                                &u4UsmUserAuthProtocol, &u4UsmUserPrivProtocol);

        SnmpGetUsmUserAuthPrivKeyChange (&UsmUserEngineId, pUsmUserName,
                                         pUsmUserAuthKeyChange,
                                         &UsmUserPrivKeyChange);

        if ((pInstance->pu4_OidList[0] == 6) &&
            (MSR_STRLEN (au1AuthPassWord) == 0))
        {
            if (pUsmUserAuthKeyChange->i4_Length != 0)
            {
                pData->pOctetStrValue->i4_Length =
                    pUsmUserAuthKeyChange->i4_Length;
                MSR_MEMCPY (pData->pOctetStrValue->pu1_OctetList,
                            pUsmUserAuthKeyChange->pu1_OctetList,
                            pData->pOctetStrValue->i4_Length);
            }
        }
        else if ((pInstance->pu4_OidList[0] == 9) &&
                 (MSR_STRLEN (au1PrivPassWord) == 0))
        {
            if (UsmUserPrivKeyChange.i4_Length != 0)
            {
                pData->pOctetStrValue->i4_Length =
                    UsmUserPrivKeyChange.i4_Length;
                MSR_MEMCPY (pData->pOctetStrValue->pu1_OctetList,
                            UsmUserPrivKeyChange.pu1_OctetList,
                            pData->pOctetStrValue->i4_Length);
            }
        }
        else
        {
            if (pInstance->pu4_OidList[0] == 6)
            {
                SNMPKeyGenerator (au1AuthPassWord,
                                  (INT4) MSR_STRLEN (au1AuthPassWord),
                                  u4UsmUserAuthProtocol,
                                  &UsmUserEngineId, pUsmUserAuthKeyChange);
                pData->pOctetStrValue->i4_Length =
                    pUsmUserAuthKeyChange->i4_Length;

                if (pData->pOctetStrValue->i4_Length != 0)
                {
                    MSR_MEMCPY (pData->pOctetStrValue->pu1_OctetList,
                                pUsmUserAuthKeyChange->pu1_OctetList,
                                pUsmUserAuthKeyChange->i4_Length);
                }
            }
            else
            {
                SNMPKeyGenerator (au1PrivPassWord,
                                  (INT4) MSR_STRLEN (au1PrivPassWord),
                                  u4UsmUserAuthProtocol,
                                  &UsmUserEngineId, &UsmUserPrivKeyChange);
                pData->pOctetStrValue->i4_Length =
                    UsmUserPrivKeyChange.i4_Length;
                if (pData->pOctetStrValue->i4_Length != 0)
                {
                    MSR_MEMCPY (pData->pOctetStrValue->pu1_OctetList,
                                UsmUserPrivKeyChange.pu1_OctetList,
                                UsmUserPrivKeyChange.i4_Length);
                }
            }
        }
    }
    free_octetstring (pUsmUserName);
    free_octetstring (pUsmUserAuthKeyChange);
    if ((pInstance->pu4_OidList[0] == 4) &&
        (pData->pOctetStrValue->i4_Length == 0))
    {
        return (MSR_SKIP);
    }

    /* If the storage type is volatile, return MSR_VOLATILE_SAVE to indicate
     * that it should be sent to the standby node
     */
    if (i4UsmUserStorageType == SNMP3_STORAGE_TYPE_VOLATILE)
    {
        return (MSR_VOLATILE_SAVE);
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateSnmpCommunityEntry
 *  Description     : This routines saves table only if corresponding  
 *                    StorageType is NON-VOLATILE .If VOLATILE, table
 *                    is skipped
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateSnmpCommunityEntry (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1Count;
    UINT1               u1TempRowStat = 1;
    tSNMP_OCTET_STRING_TYPE CommIndex;
    UINT1               au1CommunityIndex[SNMP_MAX_ADMIN_STR_LENGTH];
    INT4                i4SnmpCommunityStorageType;

    UNUSED_PARAM (pData);
    CommIndex.pu1_OctetList = au1CommunityIndex;

    MSR_MEMSET (CommIndex.pu1_OctetList, 0, SNMP_MAX_ADMIN_STR_LENGTH);

    if (pOid->u4_Length == OID_LENGTH_ELEVEN)
    {
        u1TempRowStat = 0;
    }
    CommIndex.i4_Length = (INT4) pInstance->u4_Length - u1TempRowStat;

    for (u1Count = 0; (u1Count < CommIndex.i4_Length)
         && (u1Count < SNMP_MAX_ADMIN_STR_LENGTH); u1Count++)
    {
        CommIndex.pu1_OctetList[u1Count] = (UINT1)
            (pInstance->pu4_OidList[u1Count + u1TempRowStat]);
    }

    if (nmhGetSnmpCommunityStorageType (&CommIndex,
                                        &i4SnmpCommunityStorageType) ==
        SNMP_FAILURE)
    {
        return (MSR_SKIP);
    }

    /* If the storage type is volatile, return MSR_VOLATILE_SAVE to indicate
     * that it should be sent to the standby node
     */
    if (i4SnmpCommunityStorageType == SNMP3_STORAGE_TYPE_VOLATILE)
    {
        return (MSR_VOLATILE_SAVE);
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateVacmSecurityToGroupEntry
 *  Description     : This routines saves table only if corresponding  
 *                    StorageType is NON-VOLATILE .If VOLATILE, table
 *                    is skipped
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateVacmSecurityToGroupEntry (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4Count;
    UINT1               u1TempRowStat = 1;
    UINT1               au1SecName[SNMP_MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE SecName;
    INT4                i4SecModel;
    INT4                i4VacmSecurityToGroupStorageType;

    UNUSED_PARAM (pData);

    SecName.pu1_OctetList = au1SecName;
    MSR_MEMSET (SecName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

    if (pOid->u4_Length == OID_LENGTH_ELEVEN)
    {
        u1TempRowStat = 0;
    }
    i4SecModel = (INT4) pInstance->pu4_OidList[u1TempRowStat];

    SecName.i4_Length = (INT4) pInstance->pu4_OidList[u1TempRowStat + 1];
    for (u4Count = 0; (u4Count < (UINT4) SecName.i4_Length)
         && (u4Count < SNMP_MAX_OCTETSTRING_SIZE); u4Count++)
    {
        SecName.pu1_OctetList[u4Count] = (UINT1)
            (pInstance->pu4_OidList[u4Count + 2 + u1TempRowStat]);
    }
    if (nmhGetVacmSecurityToGroupStorageType (i4SecModel, &SecName,
                                              &i4VacmSecurityToGroupStorageType)
        == SNMP_FAILURE)
    {
        return (MSR_SKIP);
    }

    /* If the storage type is volatile, return MSR_VOLATILE_SAVE to indicate
     * that it should be sent to the standby node
     */
    if (i4VacmSecurityToGroupStorageType == SNMP3_STORAGE_TYPE_VOLATILE)
    {
        return (MSR_VOLATILE_SAVE);
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateSnmpNotifyEntry
 *  Description     : This routines saves table only if corresponding  
 *                    StorageType is NON-VOLATILE .If VOLATILE, table
 *                    is skipped
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateSnmpNotifyEntry (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1Count;
    UINT1               u1TempRowStat = 1;
    UINT1               au1NotifyName[SNMP_MAX_ADMIN_STR_LENGTH];
    tSNMP_OCTET_STRING_TYPE NotifyName;
    INT4                i4SnmpNotifyStorageType;

    UNUSED_PARAM (pData);

    NotifyName.pu1_OctetList = au1NotifyName;

    MSR_MEMSET (NotifyName.pu1_OctetList, 0, SNMP_MAX_ADMIN_STR_LENGTH);

    if (pOid->u4_Length == OID_LENGTH_ELEVEN)
    {
        u1TempRowStat = 0;
    }

    NotifyName.i4_Length = (INT4) pInstance->u4_Length - u1TempRowStat;

    for (u1Count = 0; (u1Count < NotifyName.i4_Length)
         && (u1Count < SNMP_MAX_ADMIN_STR_LENGTH); u1Count++)
    {
        NotifyName.pu1_OctetList[u1Count] = (UINT1)
            (pInstance->pu4_OidList[u1Count + u1TempRowStat]);
    }

    if (nmhGetSnmpNotifyStorageType (&NotifyName,
                                     &i4SnmpNotifyStorageType) == SNMP_FAILURE)
    {
        return (MSR_SKIP);
    }

    /* If the storage type is volatile, return MSR_VOLATILE_SAVE to indicate
     * that it should be sent to the standby node
     */
    if (i4SnmpNotifyStorageType == SNMP3_STORAGE_TYPE_VOLATILE)
    {
        return (MSR_VOLATILE_SAVE);
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateSnmpNotifyFilterEntry
 *  Description     : This routines saves table only if corresponding  
 *                    StorageType is NON-VOLATILE .If VOLATILE, table
 *                    is skipped
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateSnmpNotifyFilterEntry (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1Count = 0;
    UINT1               u1TempCount = 0;
    UINT1               u1TempRowStat = 1;
    UINT1               au1Name[SNMP_MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE Name;
    tSNMP_OID_TYPE     *pSubTree = NULL;
    INT4                i4SnmpFilterStorageType = 0;

    UNUSED_PARAM (pData);

    Name.pu1_OctetList = au1Name;

    MSR_MEMSET (Name.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

    if (pOid->u4_Length == OID_LENGTH_ELEVEN)
    {
        u1TempRowStat = 0;
    }
    Name.i4_Length = (INT4) pInstance->pu4_OidList[u1TempRowStat];

    for (u1Count = 0; u1Count < Name.i4_Length; u1Count++)
    {
        Name.pu1_OctetList[u1Count] = (UINT1)
            (pInstance->pu4_OidList[u1Count + 1 + u1TempRowStat]);
    }
    u1TempCount = (UINT1) (u1Count + u1TempRowStat);

    pSubTree = alloc_oid (MAX_OID_LENGTH);
    if (pSubTree == NULL)
    {
        return (MSR_SKIP);
    }
    MSR_MEMSET (pSubTree->pu4_OidList, 0, sizeof (UINT4) * MAX_OID_LENGTH);
    pSubTree->u4_Length = 0;
    if (pOid->u4_Length == OID_LENGTH_ELEVEN)
    {
        pSubTree->u4_Length = pInstance->u4_Length -
            (pInstance->pu4_OidList[u1TempRowStat] + 1);
    }
    else
    {
        pSubTree->u4_Length = pInstance->u4_Length -
            (pInstance->pu4_OidList[u1TempRowStat] + 2);
    }

    for (u1Count = 0; u1Count < pSubTree->u4_Length; u1Count++)
    {
        pSubTree->pu4_OidList[u1Count] =
            pInstance->pu4_OidList[u1Count + 1 + u1TempCount];
    }

    if (nmhGetSnmpNotifyFilterStorageType (&Name, pSubTree,
                                           &i4SnmpFilterStorageType)
        == SNMP_FAILURE)
    {
        free_oid (pSubTree);
        return (MSR_SKIP);
    }

    /* All objects irrespective of storage type need to be saved while 
     * generating red.conf */

    free_oid (pSubTree);
    if (i4SnmpFilterStorageType == SNMP3_STORAGE_TYPE_VOLATILE)
    {
        return (MSR_VOLATILE_SAVE);
    }

    return (MSR_SAVE);

}

/************************************************************************
 *  Function Name   : MsrValidateSnmpNotifyFilterProfileEntry
 *  Description     : This routines saves table only if corresponding  
 *                    StorageType is NON-VOLATILE .If VOLATILE, table
 *                    is skipped
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateSnmpNotifyFilterProfileEntry (tSNMP_OID_TYPE * pOid,
                                         tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1Count = 0;
    UINT1               u1TempRowStat = 1;
    UINT1               au1TargetParamsName[SNMP_MAX_ADMIN_STR_LENGTH];
    tSNMP_OCTET_STRING_TYPE ParamName;
    INT4                i4SnmpFilterPrfStorageType = 0;

    UNUSED_PARAM (pData);

    ParamName.pu1_OctetList = au1TargetParamsName;

    MSR_MEMSET (ParamName.pu1_OctetList, 0, SNMP_MAX_ADMIN_STR_LENGTH);

    if (pOid->u4_Length == OID_LENGTH_ELEVEN)
    {
        u1TempRowStat = 0;
    }

    ParamName.i4_Length = (INT4) pInstance->u4_Length - u1TempRowStat;

    for (u1Count = 0; (u1Count < ParamName.i4_Length)
         && (u1Count < SNMP_MAX_ADMIN_STR_LENGTH); u1Count++)
    {
        ParamName.pu1_OctetList[u1Count] = (UINT1)
            (pInstance->pu4_OidList[u1Count + u1TempRowStat]);
    }

    if (nmhGetSnmpNotifyFilterProfileStorType (&ParamName,
                                               &i4SnmpFilterPrfStorageType) ==
        SNMP_FAILURE)
    {
        return (MSR_SKIP);
    }

    /* All objects irrespective of storage type need to be saved while 
     * generating red.conf */

    if (i4SnmpFilterPrfStorageType == SNMP3_STORAGE_TYPE_VOLATILE)
    {
        return (MSR_VOLATILE_SAVE);
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateSnmpTargetParamsEntry
 *  Description     : This routines saves table only if corresponding  
 *                    StorageType is NON-VOLATILE .If VOLATILE, table
 *                    is skipped
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateSnmpTargetParamsEntry (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1Count;
    UINT1               u1TempRowStat = 1;
    UINT1               au1ParamName[SNMP_MAX_ADMIN_STR_LENGTH];
    tSNMP_OCTET_STRING_TYPE ParamName;
    INT4                i4SnmpTargetParamsStorageType;

    UNUSED_PARAM (pData);

    ParamName.pu1_OctetList = au1ParamName;

    MSR_MEMSET (ParamName.pu1_OctetList, 0, SNMP_MAX_ADMIN_STR_LENGTH);

    if (pOid->u4_Length == OID_LENGTH_ELEVEN)
    {
        u1TempRowStat = 0;
    }
    ParamName.i4_Length = (INT4) pInstance->u4_Length - u1TempRowStat;

    for (u1Count = 0;
         u1Count < ParamName.i4_Length && u1Count < SNMP_MAX_ADMIN_STR_LENGTH;
         u1Count++)
    {
        ParamName.pu1_OctetList[u1Count] = (UINT1)
            (pInstance->pu4_OidList[u1Count + u1TempRowStat]);
    }

    if (nmhGetSnmpTargetParamsStorageType (&ParamName,
                                           &i4SnmpTargetParamsStorageType) ==
        SNMP_FAILURE)
    {
        return (MSR_SKIP);
    }

    /* If the storage type is volatile, return MSR_VOLATILE_SAVE to indicate
     * that it should be sent to the standby node
     */
    if (i4SnmpTargetParamsStorageType == SNMP3_STORAGE_TYPE_VOLATILE)
    {
        return (MSR_VOLATILE_SAVE);
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateSnmpTargetAddrEntry
 *  Description     : This routines saves table only if corresponding  
 *                    StorageType is NON-VOLATILE .If VOLATILE, table
 *                    is skipped
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateSnmpTargetAddrEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1Count;
    UINT1               u1TempRowStat = 1;
    UINT1               au1AddrName[SNMP_MAX_ADMIN_STR_LENGTH];
    tSNMP_OCTET_STRING_TYPE AddrName;
    INT4                i4SnmpTargetAddrStorageType;

    UNUSED_PARAM (pData);

    AddrName.pu1_OctetList = au1AddrName;

    MSR_MEMSET (AddrName.pu1_OctetList, 0, SNMP_MAX_ADMIN_STR_LENGTH);

    if (pOid->u4_Length == OID_LENGTH_ELEVEN)
    {
        u1TempRowStat = 0;
    }

    AddrName.i4_Length = (INT4) pInstance->u4_Length - u1TempRowStat;

    for (u1Count = 0; (u1Count < AddrName.i4_Length)
         && (u1Count < SNMP_MAX_ADMIN_STR_LENGTH); u1Count++)
    {
        AddrName.pu1_OctetList[u1Count] = (UINT1)
            (pInstance->pu4_OidList[u1Count + u1TempRowStat]);
    }

    if (nmhGetSnmpTargetAddrStorageType (&AddrName,
                                         &i4SnmpTargetAddrStorageType) ==
        SNMP_FAILURE)
    {
        return (MSR_SKIP);
    }

    /* If the storage type is volatile, return MSR_VOLATILE_SAVE to indicate
     * that it should be sent to the standby node
     */
    if (i4SnmpTargetAddrStorageType == SNMP3_STORAGE_TYPE_VOLATILE)
    {
        return (MSR_VOLATILE_SAVE);
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateVacmViewTreeFamilyEntry
 *  Description     : This routines saves table only if corresponding  
 *                    StorageType is NON-VOLATILE .If VOLATILE, table
 *                    is skipped
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateVacmViewTreeFamilyEntry (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1Count;
    UINT1               u1TempCount;
    UINT1               u1TempRowStat = 1;
    UINT1               au1Name[SNMP_MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE Name;
    tSNMP_OID_TYPE     *pSubTree = NULL;
    INT4                i4VacmViewTreeFamilyStorageType;

    UNUSED_PARAM (pData);

    Name.pu1_OctetList = au1Name;

    MSR_MEMSET (Name.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

    if (pOid->u4_Length == OID_LENGTH_TWELVE)
    {
        u1TempRowStat = 0;
    }
    Name.i4_Length = (INT4) pInstance->pu4_OidList[u1TempRowStat];

    for (u1Count = 0; u1Count < Name.i4_Length; u1Count++)
    {
        Name.pu1_OctetList[u1Count] = (UINT1)
            (pInstance->pu4_OidList[u1Count + 1 + u1TempRowStat]);
    }
    u1TempCount = (UINT1) (u1Count + 1 + u1TempRowStat);
    pSubTree = alloc_oid (MAX_OID_LENGTH);
    if (pSubTree == NULL)
    {
        return (MSR_SKIP);
    }
    MSR_MEMSET (pSubTree->pu4_OidList, 0, (sizeof (UINT4) * MAX_OID_LENGTH));
    pSubTree->u4_Length = pInstance->pu4_OidList[u1TempCount];

    for (u1Count = 0; u1Count < pSubTree->u4_Length; u1Count++)
    {
        pSubTree->pu4_OidList[u1Count] =
            pInstance->pu4_OidList[u1Count + 1 + u1TempCount];
    }

    if (nmhGetVacmViewTreeFamilyStorageType (&Name, pSubTree,
                                             &i4VacmViewTreeFamilyStorageType)
        == SNMP_FAILURE)
    {
        free_oid (pSubTree);
        return (MSR_SKIP);
    }
    free_oid (pSubTree);
    /* If the storage type is volatile, return MSR_VOLATILE_SAVE to indicate
     * that it should be sent to the standby node
     */
    if (i4VacmViewTreeFamilyStorageType == SNMP3_STORAGE_TYPE_VOLATILE)
    {
        return (MSR_VOLATILE_SAVE);
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateVacmAccessEntry
 *  Description     : This routines saves table only if corresponding  
 *                    StorageType is NON-VOLATILE .If VOLATILE, table
 *                    is skipped
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateVacmAccessEntry (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1Count;
    UINT1               u1TempCount;
    UINT1               u1TempRowStat = 1;
    UINT1               au1GroupName[SNMP_MAX_ADMIN_STR_LENGTH];
    UINT1               au1ContextName[SNMP_MAX_ADMIN_STR_LENGTH];
    tSNMP_OCTET_STRING_TYPE VacmGroupName;
    tSNMP_OCTET_STRING_TYPE VacmAccessContextPrefix;
    INT4                i4VacmAccessSecurityModel;
    INT4                i4VacmAccessSecurityLevel;
    INT4                i4RetValVacmAccessStorageType;

    UNUSED_PARAM (pData);

    VacmGroupName.pu1_OctetList = au1GroupName;

    MSR_MEMSET (VacmGroupName.pu1_OctetList, 0, SNMP_MAX_ADMIN_STR_LENGTH);

    if (pOid->u4_Length == OID_LENGTH_ELEVEN)
    {
        u1TempRowStat = 0;
    }

    VacmGroupName.i4_Length = (INT4) pInstance->pu4_OidList[u1TempRowStat];

    for (u1Count = 0; (u1Count < VacmGroupName.i4_Length)
         && (u1Count < SNMP_MAX_ADMIN_STR_LENGTH); u1Count++)
    {
        VacmGroupName.pu1_OctetList[u1Count] = (UINT1)
            (pInstance->pu4_OidList[u1Count + 1 + u1TempRowStat]);
    }

    u1TempCount = (UINT1) (u1Count + 1 + u1TempRowStat);

    VacmAccessContextPrefix.pu1_OctetList = au1ContextName;

    MSR_MEMSET (VacmAccessContextPrefix.pu1_OctetList, 0,
                SNMP_MAX_ADMIN_STR_LENGTH);

    VacmAccessContextPrefix.i4_Length =
        (INT4) pInstance->pu4_OidList[u1TempCount];

    for (u1Count = 0; (u1Count < VacmAccessContextPrefix.i4_Length)
         && (u1Count < SNMP_MAX_ADMIN_STR_LENGTH); u1Count++)
    {
        VacmAccessContextPrefix.pu1_OctetList[u1Count] = (UINT1)
            (pInstance->pu4_OidList[u1Count + 1 + u1TempCount]);
    }
    u1TempCount = (UINT1) (u1Count + u1TempCount + 1);

    i4VacmAccessSecurityModel = (INT4) pInstance->pu4_OidList[u1TempCount];
    i4VacmAccessSecurityLevel = (INT4) pInstance->pu4_OidList[u1TempCount + 1];

    if (nmhGetVacmAccessStorageType (&VacmGroupName, &VacmAccessContextPrefix,
                                     i4VacmAccessSecurityModel,
                                     i4VacmAccessSecurityLevel,
                                     &i4RetValVacmAccessStorageType)
        == SNMP_FAILURE)
    {
        return (MSR_SKIP);
    }

    /* If the storage type is volatile, return MSR_VOLATILE_SAVE to indicate
     * that it should be sent to the standby node
     */
    if (i4RetValVacmAccessStorageType == SNMP3_STORAGE_TYPE_VOLATILE)
    {
        return (MSR_VOLATILE_SAVE);
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateSnmpProxyEntry
 *  Description     : This routines saves table only if corresponding  
 *                    StorageType is NON-VOLATILE .If VOLATILE, table
 *                    is skipped
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateSnmpProxyEntry (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1Count;
    UINT1               u1TempRowStat = 1;
    UINT1               au1ProxyName[SNMP_MAX_ADMIN_STR_LENGTH];
    tSNMP_OCTET_STRING_TYPE ProxyName;
    INT4                i4SnmpProxyStorageType;

    UNUSED_PARAM (pData);

    ProxyName.pu1_OctetList = au1ProxyName;

    MSR_MEMSET (au1ProxyName, 0, SNMP_MAX_ADMIN_STR_LENGTH);

    if (pOid->u4_Length == OID_LENGTH_ELEVEN)
    {
        u1TempRowStat = 0;
    }

    ProxyName.i4_Length = (INT4) pInstance->u4_Length - u1TempRowStat;

    for (u1Count = 0; (u1Count < ProxyName.i4_Length)
         && (u1Count < SNMP_MAX_ADMIN_STR_LENGTH); u1Count++)
    {
        ProxyName.pu1_OctetList[u1Count] = (UINT1)
            (pInstance->pu4_OidList[u1Count + u1TempRowStat]);
    }

    if (nmhGetSnmpProxyStorageType (&ProxyName, &i4SnmpProxyStorageType)
        == SNMP_FAILURE)
    {
        return (MSR_SKIP);
    }

    /* If the storage type is volatile, return MSR_VOLATILE_SAVE to indicate
     * that it should be sent to the standby node
     */
    if (i4SnmpProxyStorageType == SNMP3_STORAGE_TYPE_VOLATILE)
    {
        return (MSR_VOLATILE_SAVE);
    }

    return (MSR_SAVE);
}
#endif /* SNMP_3_WANTED */
#ifdef ISS_WANTED
INT4
MsrValidateIssLogging (tSNMP_OID_TYPE * pOid,
                       tSNMP_OID_TYPE * pInstance,
                       tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pData);
    /* RM trace messages are skipped now, when the requirements 
     * comes for saving and syncing this object we can change 
     * the below MSR_SKIP to MSR_SAVE */

    /* Currently we avoid unwanted MIB object sync-up by 
     * filtering in the rmconf.c file in the standby node HA steady state. 
     * If we change the MSR_SKIP to MSR_SAVE then there is a 
     * need to remove the filter entry for this object OID to be 
     * removed from rmconf.c [gaRmOidList] */
    return (MSR_SKIP);
}

INT4
MsrValidateIssMirrorToPort (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pData);
    return (MSR_SKIP);
}

INT4
MsrValidateIssSwitchModeType (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pData);
    return (MSR_SAVE);
}

INT4
MsrValidateEntPhysicalTable (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4EntPhyIsFRU = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (nmhGetEntPhysicalIsFRU
        ((INT4) pInstance->pu4_OidList[1], &i4EntPhyIsFRU) == SNMP_SUCCESS)
    {
        if (i4EntPhyIsFRU == ISS_TRUE)
        {
            return (MSR_SAVE);
        }
    }
    return (MSR_SKIP);
}

#endif
#ifdef RM_WANTED
INT4
MsrValidateRmTrc (tSNMP_OID_TYPE * pOid,
                  tSNMP_OID_TYPE * pInstance, tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pData);
    /* RM trace messages are skipped now, when the requirements 
     * comes for saving and syncing this object we can change 
     * the below MSR_SKIP to MSR_SAVE */

    /* Currently we avoid unwanted MIB object sync-up by 
     * filtering in the rmconf.c file in the standby node HA steady state. 
     * If we change the MSR_SKIP to MSR_SAVE then there is a 
     * need to remove the filter entry for this object OID to be 
     * removed from rmconf.c [gaRmOidList] */
    return (MSR_SKIP);
}
#endif

#ifdef VCM_WANTED
#ifdef VRF_WANTED

/************************************************************************
 *  Function Name   : MsrValidateVrfL2CxtIdIfMapEntry   
 *  Description     : This function validates the VCM objects before  
 *                    saving.                     
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateVrfL2CxtIdIfMapEntry (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4IfIndex = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    /* This function takes care of saving the configured l2 context id of the
     * IP interface mapping.
     */
    if ((VcmGetL2ModeExt () == VCM_SI_MODE)
        && (VcmGetL3ModeExt () == VCM_SI_MODE))
    {
        /* If both L2SI and L3SI, don't save */
        return (MSR_NEXT);
    }

    if (pOid->u4_Length == IPVX_OID_LEN_TWELVE)
    {
        /* If OID length is 12, then the object is Row status object with vlaue
         * as create and wait. We need not save row status here. 
         */
        return (MSR_SKIP);
    }

    if (pInstance->pu4_OidList[0] != 4)
    {
        /* This functions saves only l2 context id. So any object other than
         * this l2contextId need not be saved.
         */
        return MSR_SKIP;
    }

    u4IfIndex = pInstance->pu4_OidList[IPVX_ONE];

    if (VcmIsL3Interface (u4IfIndex) == VCM_FALSE)
    {
        return (MSR_SKIP);
    }

    if (VcmGetL3ModeExt () == VCM_SI_MODE)
    {
        return (MSR_NEXT);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateVrfIfMapRowStatus   
 *  Description     : This function validates the VCM objects before  
 *                    saving.                     
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateVrfIfMapRowStatus (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4IfIndex = 0;
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* This function takes care of saving the configured row status as 
     * not-in-service for IP interface mapping.
     */
    if ((VcmGetL2ModeExt () == VCM_SI_MODE)
        && (VcmGetL3ModeExt () == VCM_SI_MODE))
    {
        /* If both L2SI and L3SI, don't save */
        return (MSR_NEXT);
    }

    if (pOid->u4_Length == IPVX_OID_LEN_TWELVE)
    {
        /* Row status object with value create and wait. So skip */
        return (MSR_SKIP);
    }

    if (pInstance->pu4_OidList[0] != 5)
    {
        /* To save only row status object here. Skip all other objects. */
        return MSR_SKIP;
    }

    u4IfIndex = pInstance->pu4_OidList[IPVX_ONE];

    if (VcmIsL3Interface (u4IfIndex) == VCM_FALSE)
    {
        return (MSR_SKIP);
    }

    if (VcmGetL3ModeExt () == VCM_SI_MODE)
    {
        return (MSR_NEXT);
    }
    /* For L3 interfaces, whenever the interface gets created in CFA, mapping
     * to default context will be internally created. Further mapping and 
     * unmapping to various contexts is done by making row status to NIS and 
     * then setting the VcId and then making row status active. So save NIS
     * from here.
     */
    pData->i4_SLongValue = MSR_NOT_IN_SERVICE;
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateVrfIfMapEntry   
 *  Description     : This function validates the VCM objects before  
 *                    saving.                     
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateVrfIfMapEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4CfgCxtId = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    /* This function takes care of saving the configured vc id and the 
     * row status for IP interface mapping.
     */
    if ((VcmGetL2ModeExt () == VCM_SI_MODE)
        && (VcmGetL3ModeExt () == VCM_SI_MODE))
    {
        /* If both L2SI and L3SI, don't save */
        return (MSR_NEXT);
    }

    if (pOid->u4_Length == IPVX_OID_LEN_TWELVE)
    {
        /* Row status object with value create and wait. So skip */
        return (MSR_SKIP);
    }

    u4IfIndex = pInstance->pu4_OidList[IPVX_ONE];

    if (VcmIsL3Interface (u4IfIndex) == VCM_FALSE)
    {
        return (MSR_SKIP);
    }

    if (VcmGetL3ModeExt () == VCM_SI_MODE)
    {
        return (MSR_NEXT);
    }

    if (pInstance->pu4_OidList[0] == 5)
    {
        /* Row status object. So save. */
        return (MSR_SAVE);
    }

    if (pInstance->pu4_OidList[0] == 2)
    {
        /* Save the VCId value */
        VcmGetIfMapCfgVcId (u4IfIndex, &u4CfgCxtId);
        pData->i4_SLongValue = u4CfgCxtId;
        return (MSR_SAVE);
    }
    return (MSR_SKIP);
}
#endif

/************************************************************************
 *  Function Name   : MsrValidateVcmIfMapEntry   
 *  Description     : This function validates the VCM objects before  
 *                    saving.                     
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateVcmIfMapEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4IfIndex = 0;
    UINT1               u1Pos = 0;

    UNUSED_PARAM (pData);

    if ((VcmGetL2ModeExt () == VCM_SI_MODE)
        && (VcmGetL3ModeExt () == VCM_SI_MODE))
    {
        /* If both L2SI and L3SI, don't save */
        return (MSR_NEXT);
    }

    if (pOid->u4_Length == IPVX_OID_LEN_TWELVE)
    {
        u1Pos = IPVX_ZERO;
    }
    else
    {
        u1Pos = IPVX_ONE;
    }

    u4IfIndex = pInstance->pu4_OidList[u1Pos];

    if (VcmIsL3Interface (u4IfIndex) == VCM_TRUE)
    {
        /* Don't save IP interface from here. It will be already saved. */
        return (MSR_SKIP);
    }

    if (VcmGetL2ModeExt () == VCM_SI_MODE)
    {
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateSispPortCtrlEntry
 *  Description     : This function validates the SISP Port Ctrl entry 
 *                    before saving.                     
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateSispPortCtrlEntry (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4Status;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    nmhGetFsSispPortCtrlStatus (pInstance->pu4_OidList[1], &i4Status);

    if (i4Status == SISP_ENABLE)
        return (MSR_SAVE);

    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateSkipDefaultVCStatus
 *  Description     : This function skips the VC Status of the default
 *                    context.                    
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateSkipDefaultVCStatus (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pData);

    if ((pOid->u4_Length == 12) && (pInstance->pu4_OidList[0] == 0))
    {
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}
#endif

/************************************************************************
 *  Function Name   : MsrValidateIfMainEntry
 *  Description     : This function validates the ifMainEnty table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateIfMainEntry (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetValIfMainType = CFA_INVALID_TYPE;
    INT4                i4RetValIfMainSubType = CFA_INVALID_TYPE;
    INT4                i4RetValIfMainMtu;
    INT4                i4RetValIfMainEncapType;
    UINT1               u1EtherType = 0;
    UINT1               u1OffSet = 0;
    INT4                i4BridgedIfaceStatus = CFA_DISABLED;
    INT4                i4StorageType = 0;
    INT4                i4BrgPortType = 0;

    UNUSED_PARAM (pOid);
#ifndef MPLS_WANTED
    UNUSED_PARAM (pData);
#endif
#ifdef WLC_WANTED
    INT4                i4ApGroupEnableStatus = APGROUP_DISABLE;
    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainType);
    if (i4RetValIfMainType == CFA_CAPWAP_DOT11_BSS)
    {
        nmhGetFsApGroupEnabledStatus (&i4ApGroupEnableStatus);
        if ((UINT4) i4ApGroupEnableStatus == APGROUP_ENABLE)
        {
            return (MSR_SKIP);
        }
    }
#endif
    /*For the ifMainRowStatus CREATE_AND_WAIT case alone we get a pOid->u4_Length as 12 */
    if (pOid->u4_Length != 12)
    {
        u1OffSet = 1;
    }

    if (pOid->u4_Length == 12)
    {
        return MSR_SKIP;
    }

    if (nmhGetIfMainType ((INT4) pInstance->pu4_OidList[u1OffSet],
                          &i4RetValIfMainType) == SNMP_FAILURE)
    {
        return (MSR_SKIP);
    }

#ifdef WTP_WANTED
    if (i4RetValIfMainType == 3)
    {
        return (MSR_SKIP);
    }
#endif
    if (nmhGetIfMainSubType ((INT4) pInstance->pu4_OidList[u1OffSet],
                             &i4RetValIfMainSubType) == SNMP_FAILURE)
    {
        return (MSR_SKIP);
    }
    if ((pInstance->pu4_OidList[0] == 14) && (pOid->u4_Length == 11))
    {                            /*value of ifMainExtSubType should be 0 or 1 */
        if ((pData->i4_SLongValue != CFA_SUBTYPE_SISP_INTERFACE) &&
            (pData->i4_SLongValue != CFA_SUBTYPE_AC_INTERFACE) &&
            (pData->i4_SLongValue != CFA_SUBTYPE_OPENFLOW_INTERFACE))
        {
            return (MSR_SKIP);
        }
    }

    if ((pInstance->pu4_OidList[0] == 21) &&
        (i4RetValIfMainType == CFA_L3SUB_INTF))
    {
        /* Skip the Dot1q encap mib for L3Subinterface for now 
         * and save it after the VcId is object is Saved */
        return MSR_SKIP;
    }

    if ((pInstance->pu4_OidList[0] == 2) && (pOid->u4_Length == 11))
    {
        if ((i4RetValIfMainType == CFA_PPP) || (i4RetValIfMainType == CFA_HDLC))
        {
            return MSR_SKIP;
        }
    }

    nmhGetIfMainBrgPortType ((INT4) pInstance->pu4_OidList[u1OffSet],
                             &i4BrgPortType);

    /* when the interface is SBP, dont save the object */
    if (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
    {
        return MSR_SKIP;
    }

    nmhGetIfIvrBridgedIface ((INT4) pInstance->pu4_OidList[u1OffSet],
                             &i4BridgedIfaceStatus);

    if (((pOid->u4_Length != 12) &&
         ((pInstance->pu4_OidList[0] == 15)
          || (pInstance->pu4_OidList[0] == 20))
         && (i4BridgedIfaceStatus != CFA_ENABLED)))
    {
        return MSR_SKIP;
    }

    if (VcmGetSystemModeExt (ISS_PROTOCOL_ID) == VCM_SI_MODE)
    {
        /* In SI, when the interface is a physical interface then dont 
         * save the object itself.
         */
        if (pOid->u4_Length != 12)
        {
            if ((pInstance->pu4_OidList[0] == 10)
                || (pInstance->pu4_OidList[0] == 11)
                || (pInstance->pu4_OidList[0] == 12)
                || ((pInstance->pu4_OidList[0] == 15) &&
                    (i4BridgedIfaceStatus == CFA_ENABLED)))
            {
                return (MSR_SAVE);
            }
            /* openflow interfaces has to be saved */
            if ((CfaIsPhysicalInterface (pInstance->pu4_OidList[1]) == CFA_TRUE)
                && (pInstance->pu4_OidList[0] != 3)
                && (i4RetValIfMainSubType != CFA_SUBTYPE_OPENFLOW_INTERFACE))
            {
                return (MSR_SKIP);
            }
            if ((CFA_MGMT_PORT == TRUE)
                && (CfaIsMgmtPort (pInstance->pu4_OidList[1]) == CFA_TRUE)
                && (pInstance->pu4_OidList[0] != 3)
                && (i4RetValIfMainSubType != CFA_SUBTYPE_OPENFLOW_INTERFACE))
            {
                return (MSR_SKIP);
            }
        }
        else
        {
            /* openflow interfaces has to be saved */
            if ((CfaIsPhysicalInterface (pInstance->pu4_OidList[0]) == CFA_TRUE)
                && (i4RetValIfMainSubType != CFA_SUBTYPE_OPENFLOW_INTERFACE))
            {
                return (MSR_SKIP);
            }
            if ((CFA_MGMT_PORT == TRUE)
                && (CfaIsMgmtPort (pInstance->pu4_OidList[0]) == CFA_TRUE)
                && (i4RetValIfMainSubType != CFA_SUBTYPE_OPENFLOW_INTERFACE))
            {
                return (MSR_SKIP);
            }
        }
    }
    if (pInstance->pu4_OidList[0] == 4)
    {
        return (MSR_SKIP);
    }

    /* SNMP Set error: Save IfMainSubType & IfMainEncapType
       object value only for Ethernet interfaces and Openflow logical interfaces. */
    if (pOid->u4_Length != 12)
    {
        if ((pInstance->pu4_OidList[0] == 9)
            || (pInstance->pu4_OidList[0] == 6))
        {

            if (i4RetValIfMainType == CFA_ENET)
            {
                return (MSR_SAVE);
            }
            else if (((i4RetValIfMainType == CFA_PROP_VIRTUAL_INTERFACE) &&
                      (i4RetValIfMainSubType == CFA_SUBTYPE_AC_INTERFACE)) ||
                     ((i4RetValIfMainType == CFA_L2VLAN) &&
                      (i4RetValIfMainSubType ==
                       CFA_SUBTYPE_OPENFLOW_INTERFACE)))
            {
                return (MSR_SAVE);
            }
            else
            {
                return (MSR_SKIP);
            }
        }
    }

/*Added skipping admin status, rowstatus and oper status for Ac before setting the sub type*/

    if (pOid->u4_Length != 12)
    {
        if ((((i4RetValIfMainType == CFA_PROP_VIRTUAL_INTERFACE) &&
              (i4RetValIfMainSubType == CFA_SUBTYPE_AC_INTERFACE)) ||
             ((i4RetValIfMainType == CFA_L2VLAN) &&
              (i4RetValIfMainSubType == CFA_SUBTYPE_AC_INTERFACE))) &&
            ((pInstance->pu4_OidList[0] == 4) ||
             (pInstance->pu4_OidList[0] == 5) ||
             (pInstance->pu4_OidList[0] == 8)))
        {
            return (MSR_SKIP);
        }
    }
    /* Added skipping admin-status, oper-status for RADIO, WLAN-PROFILE &
     * WLAN-BSS interfaces. these will be enabled once the corresponding
     * initialization of databases are done. */
#ifdef WLC_WANTED
    if (pOid->u4_Length != 12)
    {
        if (((i4RetValIfMainType == CFA_RADIO) ||
             (i4RetValIfMainType == CFA_CAPWAP_VIRT_RADIO) ||
             (i4RetValIfMainType == CFA_CAPWAP_DOT11_PROFILE) ||
             (i4RetValIfMainType == CFA_CAPWAP_DOT11_BSS)) &&
            ((pInstance->pu4_OidList[0] == 4) ||
             (pInstance->pu4_OidList[0] == 5)))
        {
            return (MSR_SKIP);
        }
    }
#endif /* WLC_WANTED */
#ifdef LA_WANTED
/* Do not set the AdminStatus till the RowStatus 
 * is set for portChannel  Interface */
    /*For the ifMainRowStatus CREATE_AND_WAIT case alone ,pOid->u4_Length is 12 */
    if (pOid->u4_Length != 12)
    {
        if ((CfaIsLaggInterface (pInstance->pu4_OidList[1]) == CFA_TRUE) &&
            (pInstance->pu4_OidList[0] == 4))
        {
            return (MSR_SKIP);
        }
    }
#endif
#ifdef MPLS_WANTED
/* Save only the CFA_MPLS_TUNNEL interfaces created by administrator */
    if (MplsCheckIsAdminCreatedMplsTnlIf (pInstance->pu4_OidList[u1OffSet])
        == MPLS_FAILURE)
    {
        return (MSR_SKIP);
    }
    /* Save Row Status  only if it is not equal to ACTIVE
     * Reason : ifStackStatus must be set to Active before 
     * making ifMainRowStatus Active in case of MPLS*/

    /* Skip 8 ->> ifMainRowStatus->ACTIVE case */
    if (pInstance->pu4_OidList[0] == 8)
    {
        if ((i4RetValIfMainType == CFA_MPLS)
            || (i4RetValIfMainType == CFA_MPLS_TUNNEL))
        {
            if (pData->i4_SLongValue == ACTIVE)
            {
                return (MSR_SKIP);
            }
        }
    }
    /* Skip 4 ->> ifMainAdminStatus */
    if (pInstance->pu4_OidList[0] == 4)
    {
        if ((i4RetValIfMainType == CFA_MPLS)
            || (i4RetValIfMainType == CFA_MPLS_TUNNEL))
        {
            if (pData->i4_SLongValue == CFA_IF_UP)
            {
                return (MSR_SKIP);
            }
        }
    }
#endif

    if ((i4RetValIfMainType == CFA_TELINK) || (i4RetValIfMainType == CFA_MPLS))
    {
        CfaGetIfMainStorageType (pInstance->pu4_OidList[u1OffSet],
                                 &i4StorageType);

        if (i4StorageType == 2)
        {
            return (MSR_SKIP);
        }
    }

    /* Done set the Row Status as Active for ILAN before creating the 
       stack entry */
    if (pInstance->pu4_OidList[0] == 8)
    {
        if ((i4RetValIfMainType == CFA_ILAN))
        {
            if (pData->i4_SLongValue == ACTIVE)
            {
                return (MSR_SKIP);
            }

        }
    }

/* Do not set the AdminStatus till the RowStatus 
 * is set for a Tunnel Interface */

    /*For the ifMainRowStatus CREATE_AND_WAIT case alone ,pOid->u4_Length is 12 */

    if (pOid->u4_Length != 12)
    {
        if ((CfaIsTunnelInterface (pInstance->pu4_OidList[1]) == CFA_TRUE) &&
            (pInstance->pu4_OidList[0] == 4))
        {
            return (MSR_SKIP);
        }

        if (CfaValidateIfMainTableIndex ((INT4) pInstance->pu4_OidList[1]) ==
            CFA_FAILURE)
        {
            return (MSR_SKIP);
        }
        nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1],
                          &i4RetValIfMainType);
        nmhGetIfMainMtu ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainMtu);
        nmhGetIfMainEncapType ((INT4) pInstance->pu4_OidList[1],
                               &i4RetValIfMainEncapType);

        if (((i4RetValIfMainType == CFA_INVALID_TYPE)
             && (pInstance->pu4_OidList[0] == 2))
            || ((i4RetValIfMainMtu == 0)
                && (pInstance->pu4_OidList[0] == 3))
            || ((i4RetValIfMainEncapType == CFA_ENCAP_NONE)
                && (pInstance->pu4_OidList[0] == 6)))
        {
            return (MSR_SKIP);
        }

        /* Don't restore the Bridge port type, before setting the bridge mode.
         * So store the bridge port type after bridge mode save. */
        if (pInstance->pu4_OidList[0] == 7)
        {
            return (MSR_SKIP);
        }

        if (pInstance->pu4_OidList[0] == 4)
        {
            return (MSR_SKIP);
        }
        if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
        {
            /* To skip the stack ports */
            CfaGetEthernetType (pInstance->pu4_OidList[1], &u1EtherType);
            if (u1EtherType == CFA_STACK_ENET)
            {
                return (MSR_SKIP);
            }
            /* To avoid XE ports configurations in Stacking case */
            if (IssGetStackPortCountFromNvRam () != 0)
            {
                if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                    CFA_SUCCESS)
                {
                    return (MSR_SKIP);
                }
            }
        }
    }

#ifdef PPP_WANTED
    /* Don't save the PPP/MP interface row status as it will be saved
     * separately after all interfaces. This is to make sure that the
     * row status of the lower layer interface is restored fist*/
    nmhGetIfMainType (pInstance->pu4_OidList[1], &i4RetValIfMainType);
    if ((i4RetValIfMainType == CFA_PPP) || (i4RetValIfMainType == CFA_MP))
    {
        if (pInstance->pu4_OidList[0] == 11)
        {
            return (MSR_SKIP);
        }
    }
#endif
    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainType);
    if (i4RetValIfMainType == CFA_LAGG)
    {
        if (pInstance->pu4_OidList[0] == 3)
        {
            return (MSR_SKIP);
        }
    }
    if (pInstance->pu4_OidList[0] == 17)
    {
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}

 /************************************************************************
 *  Function Name   : MsrValidateMtuIfMainEntry
 *  Description     : This function validates the ifMainEnty table
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateMtuIfMainEntry (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetValIfMainType = CFA_INVALID_TYPE;
    UNUSED_PARAM (pData);
    UNUSED_PARAM (pOid);
#ifdef WLC_WANTED
    INT4                i4ApGroupEnableStatus = APGROUP_DISABLE;

    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainType);
    if (i4RetValIfMainType == CFA_CAPWAP_DOT11_BSS)
    {
        nmhGetFsApGroupEnabledStatus (&i4ApGroupEnableStatus);
        if ((UINT4) i4ApGroupEnableStatus == APGROUP_ENABLE)
        {
            return (MSR_SKIP);
        }
    }
#endif
    /*For the ifMainRowStatus CREATE_AND_WAIT case alone, pOid->u4_Length is 12 */
    if (pOid->u4_Length != 12)
    {
        nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1],
                          &i4RetValIfMainType);
#ifdef WTP_WANTED
        if (i4RetValIfMainType == 3)
        {
            return (MSR_SKIP);
        }
#endif
        if (((L2IwfIsPortInPortChannel (pInstance->pu4_OidList[1]) ==
              L2IWF_SUCCESS) || (i4RetValIfMainType == CFA_LAGG))
            && (pInstance->pu4_OidList[0] == 3))
        {
            return (MSR_SAVE);
        }
    }
    return (MSR_SKIP);
}

 /************************************************************************
 *  Function Name   : MsrValidateIfMainEntrySaveEncapDot1q
 *  Description     : This function validates the L3Subinterface`s 
                      EncapDot1q object and will do an MSR Save
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateIfMainEntrySaveEncapDot1q (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetValIfMainType = CFA_INVALID_TYPE;
    UNUSED_PARAM (pData);
    UNUSED_PARAM (pOid);
#ifdef WLC_WANTED
    INT4                i4ApGroupEnableStatus = APGROUP_DISABLE;
    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainType);
    if (i4RetValIfMainType == WLC_OFFSET_3)
    {
        nmhGetFsApGroupEnabledStatus (&i4ApGroupEnableStatus);
        if ((UINT4) i4ApGroupEnableStatus == APGROUP_ENABLE)
        {
            return (MSR_SKIP);
        }
    }
#endif
#ifdef WTP_WANTED
    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainType);
    if (i4RetValIfMainType == 3)
    {
        return (MSR_SKIP);
    }
#endif

    /* If the object is EncapDot1q mib of L3Suninteface */
    if ((pInstance->pu4_OidList[0] == 21))
    {
        nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1],
                          &i4RetValIfMainType);
        if (CFA_L3SUB_INTF == i4RetValIfMainType)
        {
            return (MSR_SAVE);
        }
    }

    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateIfEntrySkipAdminStatus
 *  Description     : This function validates the ifEntry table. It saves
 *                    all the objects in this table except ifAdminStatus.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateIfEntrySkipAdminStatus (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4StorageType = 0;
    INT4                i4IfType = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
#ifdef WLC_WANTED
    INT4                i4ApGroupEnableStatus = APGROUP_DISABLE;
    INT4                i4RetValIfMainType = 0;

    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainType);
    if (i4RetValIfMainType == CFA_CAPWAP_DOT11_BSS)
    {
        nmhGetFsApGroupEnabledStatus (&i4ApGroupEnableStatus);
        if ((UINT4) i4ApGroupEnableStatus == APGROUP_ENABLE)
        {
            return (MSR_SKIP);
        }
    }
#endif
    if (pInstance->pu4_OidList[0] == 7)
    {
        return (MSR_SKIP);
    }

#ifdef MPLS_WANTED
    if (MplsCheckIsAdminCreatedMplsTnlIf (pInstance->pu4_OidList[1]) ==
        MPLS_FAILURE)
    {
        return (MSR_SKIP);
    }
#endif

    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4IfType);

    if (i4IfType == CFA_TELINK)
    {
        CfaGetIfMainStorageType (pInstance->pu4_OidList[1], &i4StorageType);

        if (i4StorageType == 2)
        {
            return (MSR_SKIP);
        }
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateIfMainEntrySaveAdminStatus
 *  Description     : This function validates the ifMainEntry table. 
 *                    It skips all the objects in this table except 
 *                    ifMainAdminStatus.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateIfMainEntrySaveAdminStatus (tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4IfType = 0;
    INT4                i4StorageType = 0;
    UINT1               u1IfUfdOperStatus = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
#ifdef WLC_WANTED
    INT4                i4ApGroupEnableStatus = APGROUP_DISABLE;
    INT4                i4RetValIfMainType = 0;

    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainType);
    if (i4RetValIfMainType == CFA_CAPWAP_DOT11_BSS)
    {
        nmhGetFsApGroupEnabledStatus (&i4ApGroupEnableStatus);
        if ((UINT4) i4ApGroupEnableStatus == APGROUP_ENABLE)
        {
            return (MSR_SKIP);
        }
    }
#endif
    if (pInstance->pu4_OidList[0] == 4)
    {
#ifdef MPLS_WANTED
        if (MplsCheckIsAdminCreatedMplsTnlIf (pInstance->pu4_OidList[1])
            == MPLS_FAILURE)
        {
            return (MSR_SKIP);
        }
#endif

        nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4IfType);
#ifdef WTP_WANTED
        if (i4IfType == 3)
        {
            return (MSR_SKIP);
        }
#endif

        if ((i4IfType == CFA_TELINK) || (i4IfType == CFA_MPLS))
        {
            CfaGetIfMainStorageType (pInstance->pu4_OidList[1], &i4StorageType);

            if (i4StorageType == 2)
            {
                return (MSR_SKIP);
            }
        }

#ifdef WLC_WANTED
        if (pOid->u4_Length != 12)
        {
            if ((i4IfType == CFA_RADIO) ||
                (i4IfType == CFA_CAPWAP_VIRT_RADIO) ||
                (i4IfType == CFA_CAPWAP_DOT11_PROFILE) ||
                (i4IfType == CFA_CAPWAP_DOT11_BSS))
            {
                return (MSR_SKIP);
            }
        }
#endif /* WLC_WANTED */

        if (i4IfType == CFA_ENET || i4IfType == CFA_LAGG)
        {
            if (CfaGetIfUfdOperStatus
                ((UINT4) pInstance->pu4_OidList[1],
                 &u1IfUfdOperStatus) == CFA_SUCCESS)
            {

                if ((pData->i4_SLongValue == CFA_IF_DOWN) &&
                    (u1IfUfdOperStatus == CFA_IF_UFD_ERR_DISABLED))
                {
                    pData->i4_SLongValue = CFA_IF_UP;
                }
            }
        }
        return (MSR_SAVE);
    }

    return (MSR_SKIP);
}

#ifdef WLC_WANTED
/************************************************************************
 *  Function Name   : MsrValidateApGroupIfMainEntry 
 *  Description     : This function validates the ifMainEntry table.
 *                    It skips all the objects in this table except
 *                    ifMainAdminStatus.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateApGroupIfMainEntry (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4IfType = 0;
    INT4                i4ApGroupEnableStatus = APGROUP_DISABLE;

    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4IfType);
    nmhGetFsApGroupEnabledStatus (&i4ApGroupEnableStatus);
    if ((UINT4) i4ApGroupEnableStatus == APGROUP_ENABLE)
    {
        if (i4IfType == CFA_CAPWAP_DOT11_BSS)
        {
            return (MSR_SKIP);
        }
    }
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    UNUSED_PARAM (pInstance);
    return MSR_SAVE;
}

#endif
/************************************************************************
 *  Function Name   : MsrValidateSysSpecificPortID 
 *  Description     : This function validates the system specific PortID 
 *                    in ifMainExtEntry table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 ************************************************************************/

INT4
MsrValidateSysSpecificPortID (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4IfType = 0;
    INT4                i4StorageType = 0;
    UINT4               u4Offset = 0;
    INT4                i4BrgPortType = 0;
#ifdef WLC_WANTED
    INT4                i4ApGroupEnableStatus = APGROUP_DISABLE;
    INT4                i4RetValIfMainType = 0;

    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainType);
    if (i4RetValIfMainType == CFA_CAPWAP_DOT11_BSS)
    {
        nmhGetFsApGroupEnabledStatus (&i4ApGroupEnableStatus);
        if ((UINT4) i4ApGroupEnableStatus == APGROUP_ENABLE)
        {
            return (MSR_SKIP);
        }
    }
#endif
    if (pOid->u4_Length != 12)
    {
        u4Offset = 1;
    }

    nmhGetIfMainBrgPortType ((INT4) pInstance->pu4_OidList[u4Offset],
                             &i4BrgPortType);

    /* ifMainExtEntry is augmented table of ifMainEntry. 
     * The entries from IfMainEntry will not be stored for 
     * S-Channel interface and hence ifMainExtEntry 
     * for S-Channel interfaces are also skipped.  */

    if (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
    {
        return MSR_SKIP;
    }

    UNUSED_PARAM (pOid);

    if ((pInstance->pu4_OidList[0] == 9) && (pData->u4_ULongValue == 0))
    {
        return MSR_SKIP;
    }
    if (pInstance->pu4_OidList[0] == 8)
    {
        /* Port MAC address not to be saved, since it will be acquired
         * based on Base MAC address
         */
        return MSR_SKIP;
    }
#ifdef MPLS_WANTED
    if (MplsCheckIsAdminCreatedMplsTnlIf (pInstance->pu4_OidList[1]) ==
        MPLS_FAILURE)
    {
        return (MSR_SKIP);
    }
#endif

    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4IfType);
#ifdef WTP_WANTED
    if (i4IfType == 3)
    {
        return (MSR_SKIP);
    }
#endif

    if (i4IfType == CFA_TELINK)
    {
        CfaGetIfMainStorageType (pInstance->pu4_OidList[1], &i4StorageType);

        if (i4StorageType == 2)
        {
            return (MSR_SKIP);
        }
    }

    return MSR_SAVE;
}

#ifdef RSTP_WANTED
/************************************************************************
 *  Function Name   : MsrValidateRstpSavePortInfo
 *  Description     : This function validates the RSTP objects and save 
 *                    only the non pseudo wire/ac interface objects.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateRstpSavePortInfo (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    tCfaIfInfo          IfInfo;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    /* In MSR while storing the objects in the table, the object's 
     * position in the table is filled in the pInstance. In this 
     * routine we have to save only the non-pseudo wire/ac interface
     * information. 
     * That is in the table "ieee8021SpanningTreePortEntry" if the 
     * ieee8021SpanningTreePort is not a pseudo wire/ac interface, 
     * then the entry is stored. 
     * If that if-index is pseudo wire/ac interface, then the port
     * information will be skipped here.
     */

    if (CfaGetIfInfo (pInstance->pu4_OidList[2], &IfInfo) == CFA_SUCCESS)
    {
        if (!((IfInfo.u1IfType == CFA_PSEUDO_WIRE) ||
              ((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
               (IfInfo.u1IfSubType == CFA_SUBTYPE_AC_INTERFACE))))
        {
            return MSR_SAVE;
        }
    }
    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidateRstpSaveExtPortInfo
 *  Description     : This function validates the RSTP objects and save 
 *                    only the pseudo wire interface/ac objects.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateRstpSaveExtPortInfo (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    tCfaIfInfo          IfInfo;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    /* In MSR while storing the objects in the table, the object's 
     * position in the table is filled in the pInstance. In this 
     * routine we have to save only the pseudo wire/ac interface
     * information. 
     * That is in the table "ieee8021SpanningTreePortEntry" if the 
     * ieee8021SpanningTreePort is a pseudo wire/ac interface, 
     * then the entry is stored. 
     * If that if-index is not pseudo wire/ac interface, then the port
     * information will be skipped here.
     */

    if (CfaGetIfInfo (pInstance->pu4_OidList[2], &IfInfo) == CFA_SUCCESS)
    {
        if (((IfInfo.u1IfType == CFA_PSEUDO_WIRE) ||
             ((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
              (IfInfo.u1IfSubType == CFA_SUBTYPE_AC_INTERFACE))))
        {
            return MSR_SAVE;
        }
    }
    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidateRstpSaveSystemControl
 *  Description     : This function validates the MSTP objects and save 
 *                    only the system-control object.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateRstpSaveSystemControl (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* In MSR while storing the objects in the table, the object's 
     * position in the table is filled in the pInstance. In this 
     * routine we have to save only the system control object. So we 
     * have look for the object's position in the table(2).*/

    if (pInstance->pu4_OidList[0] == 2)
    {
        return MSR_SAVE;
    }

    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidateRstpSkipSystemControl
 *  Description     : This function skips the Rstp System control objects 
 *                     and save all other FutureRstp Table objects
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateRstpSkipSystemControl (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4ContextId;
    INT4                i4RetVal = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pData);

    u4ContextId = pInstance->pu4_OidList[1];

    /* As the RSTP system control object is already saved, it is skipped now */

    if (pInstance->pu4_OidList[0] == 2)
    {
        return MSR_SKIP;
    }

    /* When RSTP module is not started, the object will not be saved */
    AST_LOCK ();

    if ((nmhGetFsMIRstSystemControl ((INT4) u4ContextId, &i4RetVal) ==
         SNMP_SUCCESS) && (i4RetVal != RST_SNMP_START))
    {
        AST_UNLOCK ();
        return (MSR_SKIP);
    }

    AST_UNLOCK ();
    return (MSR_SAVE);

}

/************************************************************************
 *  Function Name   : MsrValidateRstpObjects
 *  Description     : This function validates the RSTP objects before
 *                    saving any RSTP objects.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateRstpObjects (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4ContextId;
    INT4                i4RetVal = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pData);

    u4ContextId = pInstance->pu4_OidList[1];
    /* When RSTP module is not started, the object will not be saved */
    AST_LOCK ();

    if ((nmhGetFsMIRstSystemControl ((INT4) u4ContextId, &i4RetVal) ==
         SNMP_SUCCESS) && (i4RetVal != RST_SNMP_START))
    {
        AST_UNLOCK ();
        return (MSR_SKIP);
    }

    AST_UNLOCK ();
    return (MSR_SAVE);

}

/************************************************************************
 *  Function Name   : MsrValidateRstpPortSkipUnCfgPseudoRootId
 *  Description     : This function validates the RSTP objects before
 *                    saving any RSTP Port Extn Entry Objects
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateRstpPortSkipUnCfgPseudoRootId (tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pInstance,
                                          tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4ContextId;
    INT4                i4RetVal = 0;
    INT4                i4Index = 0;
    UINT2               u2LocalPortId = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    AST_LOCK ();

    /* The index for this table is interface index. Obtain the context and verify
     * the system status */

    if (pData->i4_SLongValue == CREATE_AND_WAIT)
    {
        i4Index = (INT4) pInstance->pu4_OidList[0];
    }
    else
    {
        i4Index = (INT4) pInstance->pu4_OidList[1];
    }
    /* When RSTP module is not started, the object will not be saved */

    if (AstGetContextInfoFromIfIndex ((UINT4) i4Index, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        AST_UNLOCK ();
        return (MSR_NEXT);
    }

    if ((nmhGetFsMIRstSystemControl ((INT4) u4ContextId, &i4RetVal) ==
         SNMP_SUCCESS) && (i4RetVal != RST_SNMP_START))
    {
        AST_UNLOCK ();
        return (MSR_SKIP);
    }

    /* When the pseudo root id is not configured and is only dynamically
     * calculated, do not save the mib object */

    if (pInstance->pu4_OidList[0] == 26)
    {
        if (AstGetPseudoRootIdConfigStatus (u2LocalPortId, RST_DEFAULT_INSTANCE,
                                            u4ContextId) == RST_FALSE)
        {
            AST_UNLOCK ();
            return (MSR_SKIP);
        }
    }

    AST_UNLOCK ();
    return (MSR_SAVE);

}

/************************************************************************
 *  Function Name   : MsrValidateRstpPortBasedObjects
 *  Description     : This function validates the RSTP objects before
 *                    saving any RSTP port based objects.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateRstpPortBasedObjects (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetVal = 0;
    INT4                i4IfIndex = 0;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pData);

    /* When RSTP module is not started, the object will not be saved */
    AST_LOCK ();
    if (pData->i4_SLongValue == CREATE_AND_WAIT)
    {
        i4IfIndex = (INT4) pInstance->pu4_OidList[0];
    }
    else
    {
        i4IfIndex = (INT4) pInstance->pu4_OidList[1];
    }

    if (AstGetContextInfoFromIfIndex ((UINT4) i4IfIndex, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        AST_UNLOCK ();
        return (MSR_NEXT);
    }

    if ((nmhGetFsMIRstSystemControl ((INT4) u4ContextId, &i4RetVal) ==
         SNMP_SUCCESS) && (i4RetVal != RST_SNMP_START))
    {
        AST_UNLOCK ();
        return (MSR_SKIP);
    }

    if (pInstance->pu4_OidList[0] == 5)
    {
        if ((AstIsPathcostConfigured (pInstance->pu4_OidList[1],
                                      RST_DEFAULT_INSTANCE) == RST_FALSE))
        {
            AST_UNLOCK ();
            return (MSR_SKIP);
        }
    }

    AST_UNLOCK ();
    return (MSR_SAVE);

}
#endif

#ifdef MSTP_WANTED

/************************************************************************
 *  Function Name   : MsrValidateMstpSaveSystemControl
 *  Description     : This function validates the MSTP objects and save 
 *                    only the system-control object.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMstpSaveSystemControl (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1EtherType = 0;
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* In MSR while storing the objects in the table, the object's 
     * position in the table is filled in the pInstance. In this 
     * routine we have to save only the system control object. So we 
     * have look for the object's position in the table(2).*/
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip the stack ports configurations */
        CfaGetEthernetType (pInstance->pu4_OidList[1], &u1EtherType);
        if (u1EtherType == CFA_STACK_ENET)
        {
            return (MSR_SKIP);
        }
    }

    if (pInstance->pu4_OidList[0] == 2)
    {
        return MSR_SAVE;
    }
    return MSR_SKIP;
}

/* Fix for restoration of the spanning tree mode */
/************************************************************************
 *  Function Name   : MsrValidateMstpSkipSystemControl
 *  Description     : This function validates the MSTP objects before
 *                    saving any MSTP objects.Also, it skips the MSTP
 *                    system control object
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMstpSkipSystemControl (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData)
{
    tSNMP_OCTET_STRING_TYPE Name;
    tMacAddr            IssContextMacAddress;
    tMacAddr            MacAddress;
    UINT1               au1DefName[MST_CONFIG_NAME_LEN + 1];
    UINT4               u4ContextId;
    INT4                i4RetVal = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    /* When MSTP module is not started, the object will not be saved */
    u4ContextId = pInstance->pu4_OidList[1];

    /* As the MSTP system control object is already saved, it is skipped here */
    if (pInstance->pu4_OidList[0] == 2)
    {
        return MSR_SKIP;
    }

    /* If the region name and the mac-address of the context are equal, it is skipped here */

    MSR_MEMSET (au1DefName, 0, MST_CONFIG_NAME_LEN + 1);
    Name.pu1_OctetList = &au1DefName[0];

    Name.i4_Length = MST_CONFIG_NAME_LEN;
    MSR_MEMSET (Name.pu1_OctetList, 0, MST_CONFIG_NAME_LEN + 1);
    MSR_MEMSET (IssContextMacAddress, 0, sizeof (tMacAddr));
    MSR_MEMSET (MacAddress, 0, sizeof (tMacAddr));
    /*If the value of Clear bridge status is false skip */
    if (pInstance->pu4_OidList[0] == 42)
    {
        if (pData->i4_SLongValue != CFA_TRUE)
        {
            return MSR_SKIP;
        }
    }

    AST_LOCK ();
    if (pInstance->pu4_OidList[0] == 27)
    {
        IssGetContextMacAddress (u4ContextId, IssContextMacAddress);
        nmhGetFsMIMstMstiRegionName ((INT4) u4ContextId, &Name);
        StrToMac (Name.pu1_OctetList, MacAddress);

        if (ISS_ARE_MAC_ADDR_EQUAL (MacAddress, IssContextMacAddress) ==
            ISS_TRUE)
        {
            AST_UNLOCK ();
            return MSR_SKIP;
        }
    }

    if ((nmhGetFsMIMstSystemControl ((INT4) u4ContextId, &i4RetVal) ==
         SNMP_SUCCESS) && (i4RetVal != MST_SNMP_START))
    {
        AST_UNLOCK ();
        return (MSR_SKIP);
    }

    if (pInstance->pu4_OidList[0] == AST_TE_MSTID)
    {
        AST_UNLOCK ();
        return (MSR_SKIP);
    }

    AST_UNLOCK ();
    return (MSR_SAVE);

}

/************************************************************************
 *  Function Name   : MsrValidateMstpObjects
 *  Description     : This function validates the MSTP objects before
 *                    saving any MSTP objects.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMstpObjects (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4ContextId;
    INT4                i4RetVal = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    /* When MSTP module is not started, the object will not be saved */
    u4ContextId = pInstance->pu4_OidList[1];

    AST_LOCK ();

    if ((nmhGetFsMIMstSystemControl ((INT4) u4ContextId, &i4RetVal) ==
         SNMP_SUCCESS) && (i4RetVal != MST_SNMP_START))
    {
        AST_UNLOCK ();
        return (MSR_SKIP);
    }

    if (pInstance->pu4_OidList[0] == AST_TE_MSTID)
    {
        AST_UNLOCK ();
        return (MSR_SKIP);
    }

    AST_UNLOCK ();
    return (MSR_SAVE);

}

/************************************************************************
 *  Function Name   : MsrValidateMstpMapTableObjects
 *  Description     : This function validates the MSTP map table 
 *                    and retrieves the value of fsMstSetVlanList from the 
 *                    fsMstInstanceVlanMapped objects and saves it.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateMstpMapTableObjects (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetVal = 0;
    tSNMP_OCTET_STRING_TYPE *pTmpVlanMap = NULL;
    tSNMP_OCTET_STRING_TYPE *pVlanMap = NULL;
    UINT4               u4ContextId;

    UNUSED_PARAM (pOid);
    /* When MSTP module is not started, the object will not be saved */

    AST_LOCK ();

    if (pInstance != NULL)
    {
        u4ContextId = pInstance->pu4_OidList[1];

        if ((nmhGetFsMIMstSystemControl ((INT4) u4ContextId, &i4RetVal) ==
             SNMP_SUCCESS) && (i4RetVal != MST_SNMP_START))
        {
            AST_UNLOCK ();
            return (MSR_SKIP);
        }

        if (pInstance->pu4_OidList[0] == 4)
        {
            pTmpVlanMap =
                (tSNMP_OCTET_STRING_TYPE *)
                allocmem_octetstring (MST_VLANMAP_LIST_SIZE);
            if (pTmpVlanMap == NULL)
            {
                AST_UNLOCK ();
                return (MSR_NEXT);
            }
            MSR_MEMSET (pTmpVlanMap->pu1_OctetList, 0, pTmpVlanMap->i4_Length);

            pVlanMap =
                (tSNMP_OCTET_STRING_TYPE *)
                allocmem_octetstring (MST_VLAN_LIST_SIZE);
            if (pVlanMap == NULL)
            {
                free_octetstring (pTmpVlanMap);
                AST_UNLOCK ();
                return (MSR_NEXT);
            }
            MSR_MEMSET (pVlanMap->pu1_OctetList, 0, pVlanMap->i4_Length);
            nmhGetFsMIMstSetVlanList ((INT4) u4ContextId,
                                      (INT4) pInstance->pu4_OidList[2],
                                      pVlanMap);
            nmhGetFsMIMstInstanceVlanMapped ((INT4) u4ContextId,
                                             (INT4) pInstance->pu4_OidList[2],
                                             pTmpVlanMap);
            MSR_MEMCPY (pVlanMap->pu1_OctetList, pTmpVlanMap->pu1_OctetList,
                        pTmpVlanMap->i4_Length);

            MSR_MEMSET (pTmpVlanMap->pu1_OctetList, 0, pTmpVlanMap->i4_Length);
            nmhGetFsMIMstInstanceVlanMapped2k ((INT4) u4ContextId,
                                               (INT4) pInstance->pu4_OidList[2],
                                               pTmpVlanMap);
            MSR_MEMCPY (pVlanMap->pu1_OctetList + MST_VLANMAP_LIST_SIZE,
                        pTmpVlanMap->pu1_OctetList, pTmpVlanMap->i4_Length);

            MSR_MEMSET (pTmpVlanMap->pu1_OctetList, 0, pTmpVlanMap->i4_Length);
            nmhGetFsMIMstInstanceVlanMapped3k ((INT4) u4ContextId,
                                               (INT4) pInstance->pu4_OidList[2],
                                               pTmpVlanMap);
            MSR_MEMCPY (pVlanMap->pu1_OctetList + 2 * MST_VLANMAP_LIST_SIZE,
                        pTmpVlanMap->pu1_OctetList, pTmpVlanMap->i4_Length);

            MSR_MEMSET (pTmpVlanMap->pu1_OctetList, 0, pTmpVlanMap->i4_Length);
            nmhGetFsMIMstInstanceVlanMapped4k ((INT4) u4ContextId,
                                               (INT4) pInstance->pu4_OidList[2],
                                               pTmpVlanMap);
            MSR_MEMCPY (pVlanMap->pu1_OctetList + 3 * MST_VLANMAP_LIST_SIZE,
                        pTmpVlanMap->pu1_OctetList, pTmpVlanMap->i4_Length);

            MSR_MEMSET (pTmpVlanMap->pu1_OctetList, 0, pTmpVlanMap->i4_Length);
            pData->pOctetStrValue->i4_Length = pVlanMap->i4_Length;
            MSR_MEMCPY (pData->pOctetStrValue->pu1_OctetList,
                        pVlanMap->pu1_OctetList, pVlanMap->i4_Length);
        }
    }

    if (pTmpVlanMap != NULL)
    {
        free_octetstring (pTmpVlanMap);
    }
    if (pVlanMap != NULL)
    {
        free_octetstring (pVlanMap);
    }

    AST_UNLOCK ();
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateMstpCistPortBasedObjects
 *  Description     : This function validates the MSTP port based objects 
 *                    before saving any MSTP objects.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMstpCistPortBasedObjects (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetVal = 0;
    INT4                i4IfIndex = 0;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    UNUSED_PARAM (pOid);
    /* When MSTP module is not started, the object will not be saved */
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }

    AST_LOCK ();

    i4IfIndex = (INT4) pInstance->pu4_OidList[1];

    if (AstGetContextInfoFromIfIndex ((UINT4) i4IfIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        AST_UNLOCK ();
        return (MSR_NEXT);
    }

    if ((nmhGetFsMIMstSystemControl ((INT4) u4ContextId, &i4RetVal) ==
         SNMP_SUCCESS) && (i4RetVal != MST_SNMP_START))
    {
        AST_UNLOCK ();
        return (MSR_SKIP);
    }

    if (pInstance->pu4_OidList[0] == 2)
    {
        if ((AstIsPathcostConfigured (pInstance->pu4_OidList[1],
                                      MST_CIST_CONTEXT) == RST_FALSE))
        {
            AST_UNLOCK ();
            return (MSR_SKIP);
        }
    }

    if (pInstance->pu4_OidList[0] == 49)
    {
        /* When the pseudo root id is not configured and is only dynamically
         * calculated, do not save the mib object */

        if (AstGetPseudoRootIdConfigStatus (u2LocalPortId, MST_CIST_CONTEXT,
                                            u4ContextId) == RST_FALSE)
        {
            AST_UNLOCK ();
            return (MSR_SKIP);
        }
    }

    AST_UNLOCK ();

    if (pInstance->pu4_OidList[0] == 52)
    {
        if (pData->i4_SLongValue == 0)
        {
            return (MSR_SKIP);
        }
    }

    return (MSR_SAVE);

}

/************************************************************************
 *  Function Name   : MsrValidateMstpSavePortInfo
 *  Description     : This function validates the MSTP objects and save 
 *                    only the non pseudo wire/ac interface objects.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMstpSavePortInfo (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    tCfaIfInfo          IfInfo;

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
    /* In MSR while storing the objects in the table, the object's 
     * position in the table is filled in the pInstance. In this 
     * routine we have to save non pseudo wire/ac interface
     * information. 
     * That is in the table ""fsMIMstMstiPortEntry" if the 
     * fsMIMstMstiPort is a pseudo wire/ac interface, 
     * then the entry is skipped. 
     * If that if-index is not pseudo wire/ac interface, then the port
     * information will be stored.
     */
    if (CfaGetIfInfo (pInstance->pu4_OidList[1], &IfInfo) == CFA_SUCCESS)
    {
        if (!((IfInfo.u1IfType == CFA_PSEUDO_WIRE) ||
              ((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
               (IfInfo.u1IfSubType == CFA_SUBTYPE_AC_INTERFACE))))
        {
            return (MsrValidateMstpMstiPortBasedObjects
                    (pOid, pInstance, pData));
        }
    }
    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateMstpSaveExtPortInfo
 *  Description     : This function validates the MSTP objects and save 
 *                    only the pseudo wire interface/ac objects.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMstpSaveExtPortInfo (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    tCfaIfInfo          IfInfo;

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));
    /* In MSR while storing the objects in the table, the object's 
     * position in the table is filled in the pInstance. In this 
     * routine we have to save only pseudo wire/ac interface
     * information. 
     * That is in the table ""fsMIMstMstiPortEntry" if the 
     * fsMIMstMstiPort is a pseudo wire/ac interface, 
     * then the entry is stored. 
     * If that if-index is not pseudo wire/ac interface, then the port
     * information will be skipped.
     */
    if (CfaGetIfInfo (pInstance->pu4_OidList[1], &IfInfo) == CFA_SUCCESS)
    {
        if (((IfInfo.u1IfType == CFA_PSEUDO_WIRE) ||
             ((IfInfo.u1IfType == CFA_PROP_VIRTUAL_INTERFACE) &&
              (IfInfo.u1IfSubType == CFA_SUBTYPE_AC_INTERFACE))))
        {
            return (MsrValidateMstpMstiPortBasedObjects
                    (pOid, pInstance, pData));
        }
    }
    return (MSR_SKIP);
}

 /************************************************************************
 *  Function Name   : MsrValidateMstpMstiPortBasedObjects
 *  Description     : This function validates the MSTP port based objects 
 *                    before saving any MSTP objects.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMstpMstiPortBasedObjects (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetVal = 0;
    INT4                i4IfIndex = 0;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* When MSTP module is not started, the object will not be saved */

    AST_LOCK ();

    i4IfIndex = (INT4) pInstance->pu4_OidList[1];

    if (AstGetContextInfoFromIfIndex ((UINT4) i4IfIndex,
                                      &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        AST_UNLOCK ();
        return (MSR_NEXT);
    }

    if ((nmhGetFsMIMstSystemControl ((INT4) u4ContextId, &i4RetVal) ==
         SNMP_SUCCESS) && (i4RetVal != MST_SNMP_START))
    {
        AST_UNLOCK ();
        return (MSR_SKIP);
    }

    if (pInstance->pu4_OidList[2] == AST_TE_MSTID)
    {
        AST_UNLOCK ();
        return (MSR_SKIP);
    }

    if (pInstance->pu4_OidList[0] == 2)
    {
        if ((AstIsPathcostConfigured (pInstance->pu4_OidList[1],
                                      (UINT2) pInstance->pu4_OidList[2]) ==
             RST_FALSE))
        {
            AST_UNLOCK ();
            return (MSR_SKIP);
        }
    }

    if (pInstance->pu4_OidList[0] == 26)
    {
        /* When the pseudo root id is not configured and is only dynamically
         * calculated, do not save the mib object */

        if (AstGetPseudoRootIdConfigStatus (u2LocalPortId,
                                            (UINT2) pInstance->pu4_OidList[2],
                                            u4ContextId) == RST_FALSE)
        {
            AST_UNLOCK ();
            return (MSR_SKIP);
        }
    }

    AST_UNLOCK ();
    return (MSR_SAVE);

}

/************************************************************************
 *  Function Name   : MsrValidateMstPortExtTable
 *  Description     : This function saves the Row Status as Create and Go if 
                      active. This would ensure that the entry is created 
                      as per the MIB.
 *
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateMstPortExtTable (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);

    if (pInstance->pu4_OidList[0] == 2)
    {
        if (pData->i4_SLongValue == ACTIVE)
        {
            pData->i4_SLongValue = MSR_CREATE_AND_GO;
        }
    }
    if (IssGetAutoPortCreateFlag () == ISS_ENABLE)
    {
        if ((pData->i4_SLongValue < ACTIVE) || (pData->i4_SLongValue > DESTROY))
        {
            return (MSR_SKIP);
        }
    }
    return (MSR_SAVE);
}

#endif

#ifdef PVRST_WANTED
/************************************************************************
 *  Function Name   : MsrValidatePvrstSaveSystemControl
 *  Description     : This function validates the PVRST objects and save 
 *                    only the system-control object.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidatePvrstSaveSystemControl (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* In MSR while storing the objects in the table, the object's 
     * position in the table is filled in the pInstance. In this 
     * routine we have to save only the system control object. So we 
     * have look for the object's position in the table(2).*/

    if (pInstance->pu4_OidList[0] == 2)
    {
        return MSR_SAVE;
    }
    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidatePvrstSkipSystemControl
 *  Description     : This function skips the Pvrst System control objects 
 *                     and save all other Pvrst Table objects
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidatePvrstSkipSystemControl (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4ContextId;
    INT4                i4RetVal = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pData);

    u4ContextId = pInstance->pu4_OidList[1];

    /* As the PVRST system control object is already saved, it is skipped now */

    if (pInstance->pu4_OidList[0] == 2)
    {
        return MSR_SKIP;
    }

    /* When PVRST module is not started, the object will not be saved */
    AST_LOCK ();

    if ((nmhGetFsMIPvrstSystemControl (u4ContextId, &i4RetVal) == SNMP_SUCCESS)
        && (i4RetVal != PVRST_SNMP_START))
    {
        AST_UNLOCK ();
        return (MSR_SKIP);
    }
    AST_UNLOCK ();
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidatePvrstObjects
 *  Description     : This function validates the PVRST objects before
 *                    saving any PVRST objects.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidatePvrstObjects (tSNMP_OID_TYPE * pOid,
                         tSNMP_OID_TYPE * pInstance,
                         tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    u4ContextId = pInstance->pu4_OidList[1];

    AST_LOCK ();

    /* When PVRST module is not started, the object will not be saved */

    if ((nmhGetFsMIPvrstSystemControl (u4ContextId, &i4RetVal) == SNMP_SUCCESS)
        && (i4RetVal != PVRST_SNMP_START))
    {
        AST_UNLOCK ();
        return (MSR_SKIP);
    }

    AST_UNLOCK ();
    return (MSR_SAVE);

}

/************************************************************************
 *  Function Name   : MsrValidatePvrstPortBasedObjects
 *  Description     : This function validates the PVRST objects before
 *                    saving any PVRST port based objects.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidatePvrstPortBasedObjects (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetVal = 0;
    INT4                i4IfIndex = 0;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* When PVRST module is not started, the object will not be saved */
    AST_LOCK ();

    i4IfIndex = (INT4) pInstance->pu4_OidList[1];

    if (AstGetContextInfoFromIfIndex ((UINT4) i4IfIndex, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        AST_UNLOCK ();
        return (MSR_NEXT);
    }

    if ((nmhGetFsMIPvrstSystemControl (u4ContextId, &i4RetVal) == SNMP_SUCCESS)
        && (i4RetVal != PVRST_SNMP_START))
    {
        AST_UNLOCK ();
        return (MSR_SKIP);
    }

    AST_UNLOCK ();
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidatePvrstInstPortBasedObjects
 *  Description     : This function validates the PVRST Instance specific
 *                    port based objects before saving any PVRST objects.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidatePvrstInstPortBasedObjects (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetVal = 0;
    INT4                i4IfIndex = 0;
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* When PVRST module is not started, the object will not be saved */
    AST_LOCK ();

    i4IfIndex = (INT4) pInstance->pu4_OidList[2];

    if (AstGetContextInfoFromIfIndex ((UINT4) i4IfIndex, &u4ContextId,
                                      &u2LocalPortId) != RST_SUCCESS)
    {
        AST_UNLOCK ();
        return (MSR_NEXT);
    }

    if ((nmhGetFsMIPvrstSystemControl (u4ContextId, &i4RetVal) == SNMP_SUCCESS)
        && (i4RetVal != PVRST_SNMP_START))
    {
        AST_UNLOCK ();
        return (MSR_NEXT);
    }

    if (pInstance->pu4_OidList[0] == 3)
    {
        if ((PvrstIsPathcostConfigured ((UINT2) pInstance->pu4_OidList[2],
                                        (UINT2) pInstance->pu4_OidList[1]) ==
             PVRST_FALSE))
        {
            AST_UNLOCK ();
            return (MSR_SKIP);
        }
    }

    AST_UNLOCK ();
    return (MSR_SAVE);

}

/************************************************************************
 *  Function Name   : MsrValidatePvrstInstBrgBasedObjects
 *  Description     : This function validates the PVRST Instance specific
 *                    bridge based objects before saving any PVRST objects.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidatePvrstInstBrgBasedObjects (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetVal = 0;
    UINT4               u4ContextId;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    /* When PVRST module is not started, the object will not be saved */

    u4ContextId = pInstance->pu4_OidList[1];

    AST_LOCK ();

    if ((nmhGetFsMIPvrstSystemControl (u4ContextId, &i4RetVal) == SNMP_SUCCESS)
        && (i4RetVal != PVRST_SNMP_START))
    {
        AST_UNLOCK ();
        return (MSR_NEXT);
    }

    AST_UNLOCK ();
    return (MSR_SAVE);

}
#endif

#ifdef VLAN_WANTED

INT4
MsrValidateVlanFidMapEntry (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData)
{
    /* We save the dot1qFutureVlanFidMapTable only when
     * the VLAN learning mode is HYBRID.*/

    INT4                i4VlanLearningMode;
    UINT4               u4ContextId;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    VLAN_LOCK ();

    u4ContextId = pInstance->pu4_OidList[1];

    nmhGetFsMIDot1qFutureVlanLearningMode ((INT4) u4ContextId,
                                           &i4VlanLearningMode);

    VLAN_UNLOCK ();

    if (i4VlanLearningMode == VLAN_HYBRID_LEARNING)
    {
        return (MSR_SAVE);
    }
    else
    {
        return (MSR_SKIP);
    }
}

INT4
MsrValidateBrgModeObjects (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /*For Default Context, BrgMode is get from the issnvram.txt */
    /*pInstance->pu4_OidList[1] gives the context Id */

    /* The fsMIVlanBridgeInfoTable contains only the bridge mode object
     * and it is skipped for the default context */

    if (pInstance->pu4_OidList[1] == L2IWF_DEFAULT_CONTEXT)
    {
        return (MSR_SKIP);
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateDot1dTpAgingTime
 *  Description     : This function restores the Dot1dTpAgingTime object
 *
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateDot1dTpAgingTime (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (VlanGetStartedStatus (pInstance->pu4_OidList[1]) == VLAN_TRUE)
    {
        return (MSR_SAVE);
    }
    return (MSR_SKIP);
}

INT4
MsrValidateVlanObjects (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* If the Object is GarpShutDownStatus or Base bridge mode 
     * or VLanShutDownStatus, then it is not saved */
    if ((pInstance->pu4_OidList[0] == 5) ||
        (pInstance->pu4_OidList[0] == 6) ||
        (pInstance->pu4_OidList[0] == 7) ||
        (pInstance->pu4_OidList[0] == 16) || (pInstance->pu4_OidList[0] == 14))
    {
        return MSR_SKIP;
    }
    return MSR_SAVE;

}

/************************************************************************
 *  Function Name   : MsrValidateVlanShutdownStatus
 *  Description     : This function restores the 
 *                    fsMIDot1qFutureVlanShutdownStatus object
 *
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateVlanShutdownStatus (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* Skipping the mib object - fsMIDot1qFutureVlanDebug which is used
     * to set the debug command for EVB */
    if (pInstance->pu4_OidList[0] == 7)
    {
        return (MSR_SKIP);
    }
    /* If the Object is VlanShutDownStatus, 
     * save it */
    if (pInstance->pu4_OidList[0] == 5)
    {
        return MSR_SAVE;
    }
    return MSR_SKIP;
}

INT4
MsrValidateDefaultContextGarpStatus (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* If the Object is GarpShutDownStatus for default context, 
     * then it is  saved */
    if (((pInstance->pu4_OidList[0] == 6) && (pInstance->pu4_OidList[1] == 0))
        || (pInstance->pu4_OidList[0] == 14))
    {
#ifdef GARP_WANTED
        if (pInstance->pu4_OidList[0] == 14)
        {
            if (GarpIsGarpEnabledInContext (pInstance->pu4_OidList[1]) !=
                GARP_TRUE)
            {
                return (MSR_SKIP);
            }
        }
#endif
        return MSR_SAVE;
    }
    return MSR_SKIP;
}

INT4
MsrValidateGarpStatus (tSNMP_OID_TYPE * pOid,
                       tSNMP_OID_TYPE * pInstance,
                       tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* If the Object is GarpShutDownStatus for non default context, 
     * then it is  saved */
    if ((pInstance->pu4_OidList[0] == 6) && (pInstance->pu4_OidList[1] != 0))
    {
        return MSR_SAVE;
    }
#ifdef GARP_WANTED
    if (pInstance->pu4_OidList[0] == 14)
    {
        if (GarpIsGarpEnabledInContext (pInstance->pu4_OidList[1]) == GARP_TRUE)
        {
            return (MSR_SAVE);
        }
    }
#endif
    return MSR_SKIP;
}

INT4
MsrValidateSaveBaseBridgeMode (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* Save the base bridge mode */
    if (pInstance->pu4_OidList[0] == 16)
    {
        return MSR_SAVE;
    }
    return MSR_SKIP;

}

INT4
MsrValidateSaveCVlanCounterEntry (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pData);

    /* Saving the customer vlan counter status alone 
     * others are skipped*/
    if ((pOid->u4_Length == OID_LENGTH_ELEVEN) &&
        (pInstance->pu4_OidList[0] == 7))
    {
        return MSR_SAVE;
    }
    return MSR_SKIP;

}

         /*   To be verified all below */

INT4
MsrValidateStaticUnicastPortEntry (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4ContextId = 0;
    UINT4               u4Dot1qFdbId = 0;
    INT4                i4Dot1qStaticUnicastReceivePort = 0;
    tMacAddr            Dot1qStaticUnicastAddress;
    UINT4               u4Count = 1;
    INT4                i4Status = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    MSR_MEMSET (Dot1qStaticUnicastAddress, 0, sizeof (tMacAddr));

    u4ContextId = pInstance->pu4_OidList[u4Count++];
    u4Dot1qFdbId = pInstance->pu4_OidList[u4Count];
    /* skip the next element in the pu4_OidList as it is the size of 
     * mac address stored (6).
     */
    for (u4Count = 3; u4Count < 9; u4Count++)
    {
        Dot1qStaticUnicastAddress[u4Count - 3]
            = (UINT1) (pInstance->pu4_OidList[u4Count]);
    }

    i4Dot1qStaticUnicastReceivePort = (INT4) pInstance->pu4_OidList[u4Count];

    /* If the storage type is volatile, return MSR_VOLATILE_SAVE to indicate
     * that it should be sent to the standby node
     */
    if (VlanGetBaseBridgeMode () == DOT_1Q_VLAN_MODE)
    {
        VLAN_LOCK ();

        if (nmhGetFsDot1qStaticUnicastStatus ((INT4) u4ContextId, u4Dot1qFdbId,
                                              Dot1qStaticUnicastAddress,
                                              i4Dot1qStaticUnicastReceivePort,
                                              &i4Status) != SNMP_FAILURE)
        {
            if (i4Status != VLAN_PERMANENT)
            {
                VLAN_UNLOCK ();
                return (MSR_VOLATILE_SAVE);
            }
        }
        VLAN_UNLOCK ();
    }
    else
    {
        VLAN_LOCK ();
        if (nmhGetDot1dStaticStatus (Dot1qStaticUnicastAddress,
                                     i4Dot1qStaticUnicastReceivePort,
                                     &i4Status) != SNMP_FAILURE)
        {
            if (i4Status != VLAN_PERMANENT)
            {
                VLAN_UNLOCK ();
                return (MSR_VOLATILE_SAVE);
            }
        }
        VLAN_UNLOCK ();
    }

    return MSR_SAVE;

}

INT4
MsrValidateStaticUnicastExtnEntry (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4ContextId = 0;
    UINT4               u4Dot1qFdbId = 0;
    INT4                i4Dot1qStaticUnicastReceivePort = 0;
    tMacAddr            Dot1qStaticUnicastAddress;
    UINT4               u4Count = 1;
    INT4                i4Status = 0;

    UNUSED_PARAM (pData);

    /* Don't save the fsDot1qStaticUnicastRowStatus value indication come
     * from MSR frame-work*/
    if (pOid->u4_Length == 14)
    {
        return MSR_SKIP;
    }

    MSR_MEMSET (Dot1qStaticUnicastAddress, 0, sizeof (tMacAddr));

    u4ContextId = pInstance->pu4_OidList[u4Count++];
    u4Dot1qFdbId = pInstance->pu4_OidList[u4Count];
    /* skip the next element in the pu4_OidList as it is the size of 
     * mac address stored (6).
     */
    for (u4Count = 3; u4Count < 9; u4Count++)
    {
        Dot1qStaticUnicastAddress[u4Count - 3]
            = (UINT1) (pInstance->pu4_OidList[u4Count]);
    }

    i4Dot1qStaticUnicastReceivePort = (INT4) pInstance->pu4_OidList[u4Count];

    VLAN_LOCK ();

    /* If the storage type is volatile, return MSR_VOLATILE_SAVE to indicate
     * that it should be sent to the standby node
     */
    if (nmhGetFsDot1qStaticUnicastStatus ((INT4) u4ContextId, u4Dot1qFdbId,
                                          Dot1qStaticUnicastAddress,
                                          i4Dot1qStaticUnicastReceivePort,
                                          &i4Status) != SNMP_FAILURE)
    {
        if (i4Status != VLAN_PERMANENT)
        {
            VLAN_UNLOCK ();
            return (MSR_VOLATILE_SAVE);
        }
    }

    VLAN_UNLOCK ();
    return MSR_SAVE;

}

INT4
MsrValidateStaticUnicastSetStatusCreate (tSNMP_OID_TYPE * pOid,
                                         tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4ContextId = 0;
    UINT4               u4Dot1qFdbId = 0;
    INT4                i4Dot1qStaticUnicastReceivePort = 0;
    tMacAddr            Dot1qStaticUnicastAddress;
    UINT4               u4Count = 1;
    INT4                i4Status = 0;
    INT4                i4RowStatus = 0;

    UNUSED_PARAM (pData);

    /* Don't save the fsDot1qStaticUnicastRowStatus value indication come
     * from MSR frame-work*/
    if (pOid->u4_Length == 14)
    {
        return MSR_SKIP;
    }

    if (pInstance->pu4_OidList[0] == 3)
    {
        MSR_MEMSET (Dot1qStaticUnicastAddress, 0, sizeof (tMacAddr));

        u4ContextId = pInstance->pu4_OidList[u4Count++];
        u4Dot1qFdbId = pInstance->pu4_OidList[u4Count];
        /* skip the next element in the pu4_OidList as it is the size of 
         * mac address stored (6).
         */
        for (u4Count = 3; u4Count < 9; u4Count++)
        {
            Dot1qStaticUnicastAddress[u4Count - 3]
                = (UINT1) (pInstance->pu4_OidList[u4Count]);
        }

        i4Dot1qStaticUnicastReceivePort =
            (INT4) pInstance->pu4_OidList[u4Count];

        if (VlanGetBaseBridgeMode () == DOT_1Q_VLAN_MODE)
        {
            VLAN_LOCK ();

            if (nmhGetFsDot1qStaticUnicastRowStatus
                ((INT4) u4ContextId, u4Dot1qFdbId, Dot1qStaticUnicastAddress,
                 i4Dot1qStaticUnicastReceivePort, &i4RowStatus) != SNMP_FAILURE)
            {
                if (i4RowStatus != VLAN_ACTIVE)
                {
                    VLAN_UNLOCK ();
                    return (MSR_SKIP);
                }
            }

            /* If the storage type is volatile, return MSR_VOLATILE_SAVE to indicate
             * that it should be sent to the standby node
             */
            if (nmhGetFsDot1qStaticUnicastStatus
                ((INT4) u4ContextId, u4Dot1qFdbId, Dot1qStaticUnicastAddress,
                 i4Dot1qStaticUnicastReceivePort, &i4Status) != SNMP_FAILURE)
            {
                if (i4Status != VLAN_PERMANENT)
                {
                    VLAN_UNLOCK ();
                    pData->i4_SLongValue = MSR_CREATE_AND_WAIT;
                    return (MSR_VOLATILE_SAVE);
                }
            }
            VLAN_UNLOCK ();
        }
        else
        {
            VLAN_LOCK ();
            if (nmhGetDot1dStaticStatus (Dot1qStaticUnicastAddress,
                                         i4Dot1qStaticUnicastReceivePort,
                                         &i4Status) != SNMP_FAILURE)
            {
                if (i4Status != VLAN_PERMANENT)
                {
                    VLAN_UNLOCK ();
                    pData->i4_SLongValue = MSR_CREATE_AND_WAIT;
                    return (MSR_VOLATILE_SAVE);
                }
            }
            VLAN_UNLOCK ();
        }
        /* Setting CREATE_AND_WAIT for rowstatus */

        pData->i4_SLongValue = MSR_CREATE_AND_WAIT;
        return MSR_SAVE;
    }

    /* If the object is unicast status, return MSR_VOLATILE_SAVE to
     * indicate that it should be sent to the standby node
     */
    if (pInstance->pu4_OidList[0] == 4)
    {
        return MSR_VOLATILE_SAVE;
    }

    return MSR_SKIP;
}

INT4
MsrValidateVlanStaticSetStatusCreate (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* In MSR while storing the objects in the table, the object's 
     * position in the table is filled in the pInstance. 
     * For VlanStatic table the Indices are ContextId and VlanId.
     * In this routine we have to save all the object except the 
     * Rowstatus. So at first we have to check for the length of
     * the Indices (3) and then we have look for the object's position 
     * in the table(2).*/

    if (pInstance->u4_Length == 3)
    {
        if (pInstance->pu4_OidList[0] == 2)
        {
            return MSR_SKIP;
        }
    }
    return MSR_SAVE;
}

INT4
MsrValidateVlanStaticSetStatusActive (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* In MSR while storing the objects in the table, the object's 
     * position in the table is filled in the pInstance. 
     * For VlanStatic table the Indices are ContextId and VlanId.
     * In this routine we have to save only the Rowstatus. 
     * So at first we have to check for the length of the Indices (3) 
     * and then we have look for the object's position in the table(2).*/

    if (pInstance->u4_Length == 3)
    {
        if (pInstance->pu4_OidList[0] == 2)
        {
            return MSR_SAVE;
        }
    }
    return MSR_SKIP;
}

INT4
MsrValidateStaticUnicastSetStatusActive (tSNMP_OID_TYPE * pOid,
                                         tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4ContextId = 0;
    UINT4               u4Dot1qFdbId = 0;
    INT4                i4Dot1qStaticUnicastReceivePort = 0;
    tMacAddr            Dot1qStaticUnicastAddress;
    UINT4               u4Count = 1;
    INT4                i4Status = 0;
    INT4                i4RowStatus = 0;

    UNUSED_PARAM (pData);

    /* Don't save the fsDot1qStaticUnicastRowStatus value indication come
     * from MSR frame-work*/
    if (pOid->u4_Length == 14)
    {
        return MSR_SKIP;
    }

    if (pInstance->pu4_OidList[0] == 3)
    {
        MSR_MEMSET (Dot1qStaticUnicastAddress, 0, sizeof (tMacAddr));

        u4ContextId = pInstance->pu4_OidList[u4Count++];
        u4Dot1qFdbId = pInstance->pu4_OidList[u4Count];
        /* skip the next element in the pu4_OidList as it is the size of 
         * mac address stored (6).
         */
        for (u4Count = 3; u4Count < 9; u4Count++)
        {
            Dot1qStaticUnicastAddress[u4Count - 3]
                = (UINT1) (pInstance->pu4_OidList[u4Count]);
        }

        i4Dot1qStaticUnicastReceivePort =
            (INT4) pInstance->pu4_OidList[u4Count];

        if (VlanGetBaseBridgeMode () == DOT_1Q_VLAN_MODE)
        {
            VLAN_LOCK ();

            if (nmhGetFsDot1qStaticUnicastRowStatus
                ((INT4) u4ContextId, u4Dot1qFdbId, Dot1qStaticUnicastAddress,
                 i4Dot1qStaticUnicastReceivePort, &i4RowStatus) != SNMP_FAILURE)
            {
                if (i4RowStatus != VLAN_ACTIVE)
                {
                    VLAN_UNLOCK ();
                    return (MSR_SKIP);
                }
            }

            /* If the storage type is volatile, return MSR_VOLATILE_SAVE to
             * indicate that it should be sent to the standby node
             */
            if (nmhGetFsDot1qStaticUnicastStatus
                ((INT4) u4ContextId, u4Dot1qFdbId, Dot1qStaticUnicastAddress,
                 i4Dot1qStaticUnicastReceivePort, &i4Status) != SNMP_FAILURE)
            {
                if (i4Status != VLAN_PERMANENT)
                {
                    VLAN_UNLOCK ();
                    return (MSR_VOLATILE_SAVE);
                }
            }

            VLAN_UNLOCK ();
        }
        else
        {
            VLAN_LOCK ();
            if (nmhGetDot1dStaticStatus (Dot1qStaticUnicastAddress,
                                         i4Dot1qStaticUnicastReceivePort,
                                         &i4Status) != SNMP_FAILURE)
            {
                if (i4Status != VLAN_PERMANENT)
                {
                    VLAN_UNLOCK ();
                    return (MSR_VOLATILE_SAVE);
                }
            }
            VLAN_UNLOCK ();
        }
        /* Setting ACTIVE for rowstatus */

        pData->i4_SLongValue = MSR_ACTIVE;
        return MSR_SAVE;
    }
    return MSR_SKIP;
}

INT4
MsrValidateStaticMulticastSetStatusCreate (tSNMP_OID_TYPE * pOid,
                                           tSNMP_OID_TYPE * pInstance,
                                           tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4ContextId = 0;
    UINT4               u4Dot1qVlanIndex = 0;
    INT4                i4Dot1qStaticMulticastReceivePort = 0;
    tMacAddr            Dot1qStaticMulticastAddress;
    UINT4               u4Count = 1;
    INT4                i4Status = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (pInstance->pu4_OidList[0] == 3)
    {
        /* Save fsDot1qStaticMulticastRowStatus object's value as destroy
         * when fsDot1qStaticMulticastStatus is deleteOnTimeout.
         */
        if (pData->i4_SLongValue == MSR_NOT_READY)
        {
            pData->i4_SLongValue = MSR_DESTROY;
            return MSR_SAVE;
        }

        MSR_MEMSET (Dot1qStaticMulticastAddress, 0, sizeof (tMacAddr));

        u4ContextId = pInstance->pu4_OidList[u4Count++];
        u4Dot1qVlanIndex = pInstance->pu4_OidList[u4Count];
        /* skip the next element in the pu4_OidList as it is the size of 
         * mac address stored (6).
         */
        for (u4Count = 3; u4Count < 9; u4Count++)
        {
            Dot1qStaticMulticastAddress[u4Count - 3]
                = (UINT1) (pInstance->pu4_OidList[u4Count]);
        }

        i4Dot1qStaticMulticastReceivePort =
            (INT4) pInstance->pu4_OidList[u4Count];

        if (VlanGetBaseBridgeMode () == DOT_1Q_VLAN_MODE)
        {
            VLAN_LOCK ();

            if (nmhGetFsDot1qStaticMulticastStatus
                ((INT4) u4ContextId, u4Dot1qVlanIndex,
                 Dot1qStaticMulticastAddress, i4Dot1qStaticMulticastReceivePort,
                 &i4Status) != SNMP_FAILURE)
            {
                if (i4Status != VLAN_PERMANENT)
                {
                    VLAN_UNLOCK ();
                    return (MSR_VOLATILE_SAVE);
                }
            }
            VLAN_UNLOCK ();
        }
        else
        {
            VLAN_LOCK ();
            if (nmhGetDot1dStaticStatus (Dot1qStaticMulticastAddress,
                                         i4Dot1qStaticMulticastReceivePort,
                                         &i4Status) != SNMP_FAILURE)
            {
                if (i4Status != VLAN_PERMANENT)
                {
                    VLAN_UNLOCK ();
                    return (MSR_VOLATILE_SAVE);
                }
            }
            VLAN_UNLOCK ();
        }
        /* If the Object is Rowstatus we have to set it as CREATE_AND_WAIT 
         * and then save the object */

        pData->i4_SLongValue = MSR_CREATE_AND_WAIT;
        return MSR_SAVE;
    }

    if (pInstance->pu4_OidList[0] == 4)
    {
        return MSR_VOLATILE_SAVE;
    }

    return MSR_SKIP;
}

INT4
MsrValidateStaticMulticastPortEntry (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4ContextId = 0;
    UINT4               u4Dot1qVlanIndex = 0;
    INT4                i4Dot1qStaticMulticastReceivePort = 0;
    tMacAddr            Dot1qStaticMulticastAddress;
    UINT4               u4Count = 1;
    INT4                i4Status = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    MSR_MEMSET (Dot1qStaticMulticastAddress, 0, sizeof (tMacAddr));

    u4ContextId = pInstance->pu4_OidList[u4Count++];
    u4Dot1qVlanIndex = pInstance->pu4_OidList[u4Count];
    /* skip the next element in the pu4_OidList as it is the size of 
     * mac address stored (6).
     */
    for (u4Count = 3; u4Count < 9; u4Count++)
    {
        Dot1qStaticMulticastAddress[u4Count - 3]
            = (UINT1) pInstance->pu4_OidList[u4Count];
    }

    i4Dot1qStaticMulticastReceivePort = (INT4) pInstance->pu4_OidList[u4Count];

    if (VlanGetBaseBridgeMode () == DOT_1Q_VLAN_MODE)
    {
        VLAN_LOCK ();

        if (nmhGetFsDot1qStaticMulticastStatus
            ((INT4) u4ContextId, u4Dot1qVlanIndex, Dot1qStaticMulticastAddress,
             i4Dot1qStaticMulticastReceivePort, &i4Status) != SNMP_FAILURE)
        {
            if (i4Status != VLAN_PERMANENT)
            {
                VLAN_UNLOCK ();
                return (MSR_VOLATILE_SAVE);
            }
        }
        VLAN_UNLOCK ();
    }
    else
    {
        VLAN_LOCK ();
        if (nmhGetDot1dStaticStatus (Dot1qStaticMulticastAddress,
                                     i4Dot1qStaticMulticastReceivePort,
                                     &i4Status) != SNMP_FAILURE)
        {
            if (i4Status != VLAN_PERMANENT)
            {
                VLAN_UNLOCK ();
                return (MSR_VOLATILE_SAVE);
            }
        }
        VLAN_UNLOCK ();
    }

    return MSR_SAVE;

}

INT4
MsrValidateStaticMulticastSetStatusActive (tSNMP_OID_TYPE * pOid,
                                           tSNMP_OID_TYPE * pInstance,
                                           tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4ContextId = 0;
    UINT4               u4Dot1qVlanIndex = 0;
    INT4                i4Dot1qStaticMulticastReceivePort = 0;
    tMacAddr            Dot1qStaticMulticastAddress;
    UINT4               u4Count = 1;
    INT4                i4Status = 0;
    INT4                i4RowStatus = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (pInstance->pu4_OidList[0] == 3)
    {
        /* Don't save fsDot1qStaticMulticastRowStatus object's value
         * (NOT_READY) when fsDot1qStaticMulticastStatus is deleteOnTimeout.
         */

        if (pData->i4_SLongValue == MSR_NOT_READY)
        {
            return MSR_SKIP;
        }

        MSR_MEMSET (Dot1qStaticMulticastAddress, 0, sizeof (tMacAddr));

        u4ContextId = pInstance->pu4_OidList[u4Count++];
        u4Dot1qVlanIndex = pInstance->pu4_OidList[u4Count];
        /* skip the next element in the pu4_OidList as it is the size of 
         * mac address stored (6).
         */
        for (u4Count = 3; u4Count < 9; u4Count++)
        {
            Dot1qStaticMulticastAddress[u4Count - 3]
                = (UINT1) (pInstance->pu4_OidList[u4Count]);
        }

        i4Dot1qStaticMulticastReceivePort =
            (INT4) pInstance->pu4_OidList[u4Count];

        if (VlanGetBaseBridgeMode () == DOT_1Q_VLAN_MODE)
        {
            VLAN_LOCK ();

            if (nmhGetFsDot1qStaticMulticastRowStatus
                ((INT4) u4ContextId, u4Dot1qVlanIndex,
                 Dot1qStaticMulticastAddress, i4Dot1qStaticMulticastReceivePort,
                 &i4RowStatus) != SNMP_FAILURE)
            {
                if (i4RowStatus != VLAN_ACTIVE)
                {
                    VLAN_UNLOCK ();
                    return (MSR_SKIP);
                }
            }

            if (nmhGetFsDot1qStaticMulticastStatus
                ((INT4) u4ContextId, u4Dot1qVlanIndex,
                 Dot1qStaticMulticastAddress, i4Dot1qStaticMulticastReceivePort,
                 &i4Status) != SNMP_FAILURE)
            {
                if (i4Status != VLAN_PERMANENT)
                {
                    VLAN_UNLOCK ();
                    return (MSR_VOLATILE_SAVE);
                }
            }
            VLAN_UNLOCK ();
        }
        else
        {
            VLAN_LOCK ();
            if (nmhGetDot1dStaticStatus (Dot1qStaticMulticastAddress,
                                         i4Dot1qStaticMulticastReceivePort,
                                         &i4Status) != SNMP_FAILURE)
            {
                if (i4Status != VLAN_PERMANENT)
                {
                    VLAN_UNLOCK ();
                    return (MSR_VOLATILE_SAVE);
                }
            }
            VLAN_UNLOCK ();
        }

        /* If the Object is Rowstatus we have to set it as ACTIVE 
         * and then save the object */

        pData->i4_SLongValue = MSR_ACTIVE;
        return MSR_SAVE;
    }
    return MSR_SKIP;
}

INT4
MsrValidateForwardAllSetStatusCreate (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    UNUSED_PARAM (pInstance);

    /* Set the status to NOT_IN_SERVICE */
    pData->i4_SLongValue = 2;

    return MSR_SAVE;

}

INT4
MsrValidateForwardAllSetStatusActive (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    UNUSED_PARAM (pInstance);

    /* Set the status to ACTIVE */
    pData->i4_SLongValue = MSR_ACTIVE;

    return MSR_SAVE;

}

INT4
MsrValidateForwardUnregSetStatusCreate (tSNMP_OID_TYPE * pOid,
                                        tSNMP_OID_TYPE * pInstance,
                                        tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    UNUSED_PARAM (pInstance);

    /* Get the forward unregistered static port list.
     * If it has the default values all ports set, then 
     * save the rowstatus object. */

    VLAN_LOCK ();
    if (VlanIsUnRegForwardPortsConfigured (pInstance->pu4_OidList[1],
                                           pInstance->pu4_OidList[2])
        == VLAN_FALSE)
    {
        VLAN_UNLOCK ();
        return MSR_SKIP;

    }

    VLAN_UNLOCK ();

    /* Set the status to CREATE_AND_WAIT */
    pData->i4_SLongValue = MSR_CREATE_AND_WAIT;

    return MSR_SAVE;

}

INT4
MsrValidateForwardUnregPortEntry (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* Get the forward unregistered static port list.
     * If it has the default values ie.) all ports set, then do
     * save the ForwardUnregPortEntry */

    VLAN_LOCK ();
    if (VlanIsUnRegForwardPortsConfigured (pInstance->pu4_OidList[1],
                                           pInstance->pu4_OidList[2])
        == VLAN_FALSE)
    {
        VLAN_UNLOCK ();
        return MSR_SKIP;

    }

    VLAN_UNLOCK ();
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[3]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }

    return MSR_SAVE;
}

INT4
MsrValidateForwardUnregSetStatusActive (tSNMP_OID_TYPE * pOid,
                                        tSNMP_OID_TYPE * pInstance,
                                        tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4ContextId = 0;
    UINT4               u4Dot1qVlanIndex = 0;
    INT4                i4RowStatus = 0;
    UINT4               u4Count = 1;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    UNUSED_PARAM (pInstance);
    u4ContextId = pInstance->pu4_OidList[u4Count++];
    u4Dot1qVlanIndex = pInstance->pu4_OidList[u4Count];

    VLAN_LOCK ();

    if (nmhGetFsDot1qForwardUnregRowStatus (u4ContextId, u4Dot1qVlanIndex,
                                            &i4RowStatus) != SNMP_FAILURE)
    {
        if (i4RowStatus == VLAN_ACTIVE)
        {
            /* Get the forward unregistered static port list.
             * If it has the default values all ports set, then 
             * save the rowstatus object. */

            if (VlanIsUnRegForwardPortsConfigured (u4ContextId,
                                                   u4Dot1qVlanIndex)
                == VLAN_TRUE)
            {

                VLAN_UNLOCK ();
                /* Set the status to ACTIVE */
                pData->i4_SLongValue = MSR_ACTIVE;

                return MSR_SAVE;
            }
        }
    }

    VLAN_UNLOCK ();
    return (MSR_SKIP);

}

/************************************************************************
 *  Function Name   : MsrValidateDot1adMIPortEntry
 *  Description     : This function validates the table entry
                      dot1adMIPortEntry
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateDot1adMIPortEntry (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4IfIndex = 0;
    INT4                i4BrgPortType = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    u4IfIndex = pInstance->pu4_OidList[1];

    if (pInstance->pu4_OidList[0] == 5)
    {
        /* Save the obj dot1adMIPortSVlanPriorityType only for CNP 
           S-Tagged and PortBased ports. */

        nmhGetIfMainBrgPortType ((INT4) u4IfIndex, &i4BrgPortType);

        if ((i4BrgPortType != CFA_CNP_PORTBASED_PORT) &&
            (i4BrgPortType != CFA_CNP_STAGGED_PORT))
        {
            return MSR_SKIP;
        }
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidatedot1adMICVidRegistrationEntry
 *  Description     : This function validates the table entry
                      dot1adMICVidRegistrationEntry
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidatedot1adMICVidRegistrationEntry (tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pInstance,
                                          tSNMP_MULTI_DATA_TYPE * pData)
{

    UNUSED_PARAM (pOid);

    if ((pInstance->pu4_OidList[0] == 5) && (pData->i4_SLongValue == ACTIVE))
    {
        return MSR_SKIP;
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidatedot1adMICVidRegistrationEntrySaveRowStatus
 *  Description     : This function validates the table entry
                      dot1adMICVidRegistrationEntry
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidatedot1adMICVidRegistrationEntrySaveRowStatus (tSNMP_OID_TYPE * pOid,
                                                       tSNMP_OID_TYPE *
                                                       pInstance,
                                                       tSNMP_MULTI_DATA_TYPE *
                                                       pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    if (pInstance->pu4_OidList[0] != 5)
    {
        return MSR_SKIP;
    }

    return MSR_SAVE;

}

/************************************************************************
 *  Function Name   : MsrValidateFsMIPbContextInfoEntry
 *  Description     : This function validates the table entry
                      fsMIPbContextInfoEntry
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateFsMIPbContextInfoEntry (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if ((pInstance->pu4_OidList[0] >= 3) && (pInstance->pu4_OidList[0] <= 7))
    {
        return (MSR_SKIP);
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsMIPbPortInfoEntry
 *  Description     : This function validates the table entry
                      fsMIPbPortInfoEntry
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateFsMIPbPortInfoEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4IfIndex = 0;
    INT4                i4BrgPortType = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    u4IfIndex = pInstance->pu4_OidList[1];

    if (pInstance->pu4_OidList[0] == 6)
    {
        /* For CBP and PIP ports don't save this
         * fsMIPbPortSVlanTranslationStatus obj.*/

        nmhGetIfMainBrgPortType ((INT4) u4IfIndex, &i4BrgPortType);

        if ((i4BrgPortType == CFA_PROVIDER_INSTANCE_PORT) ||
            (i4BrgPortType == CFA_CUSTOMER_BACKBONE_PORT))
        {
            return MSR_SKIP;
        }
    }

    /* Skip saving of deprecated objects fsMIPbPortSVlanIngressEtherType
     * and fsMIPbPortSVlanEgressEtherType */

    if ((pInstance->pu4_OidList[0] == 3) || (pInstance->pu4_OidList[0] == 4))
    {
        return MSR_SKIP;
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateVlanCounterEntry
 *  Description     : This function validates the table entry
                      fsMIDot1qFutureVlanCounterEntry
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateVlanCounterEntry (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pData);

    if ((pInstance->pu4_OidList[0] == 12) && (pOid->u4_Length == 11))
    {
        /* fsMIDot1qFutureVlanCounterStatus object is
         * alone saved, all other objects are not saved */
        return (MSR_SAVE);
    }
    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateUnicastMacControlEntry
 *  Description     : This function validates the table entry
                      fsMIDot1qFutureVlanUnicastMacControlEntry
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateUnicastMacControlEntry (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4Count = 1;
    UINT4               u4VlanStatus = 0;
    UINT4               u4TimeMark = 0;
    UINT4               u4VlanId;
    UINT4               u4ContextId;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    u4ContextId = pInstance->pu4_OidList[u4Count++];
    u4VlanId = pInstance->pu4_OidList[u4Count];

    VLAN_LOCK ();

    /* If the VLAN is dynamically learned then MAC learning params
     * should not be saved.*/

    /* Since u4TimeMark is unused inside nmhGetFsDot1qVlanStatus dummy value
     * is being passed.*/
    nmhGetFsDot1qVlanStatus ((INT4) u4ContextId, u4TimeMark, u4VlanId,
                             (INT4 *) &u4VlanStatus);

    if (u4VlanStatus == VLAN_CURR_ENTRY_DYNAMIC)
    {
        VLAN_UNLOCK ();
        return (MSR_SKIP);
    }

    VLAN_UNLOCK ();
    return (MSR_SAVE);
}

#endif
#ifdef PBBTE_WANTED
INT4
MsrValidateTeSidSetStatusActive (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4TeSid = 0;
    UINT4               u4EspId = 0;
    INT4                i4Status = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* If the Length obtained is 3 then it means that the Instance OID list 
     * contains the object type first followed by the table indexes.
     */

    /* Skip the saving of Create and Wait for rowstatus object */
    if (pInstance->u4_Length == 2)
    {
        return MSR_SKIP;
    }

    if (pInstance->u4_Length == 3)
    {
        u4TeSid = pInstance->pu4_OidList[1];
        u4EspId = pInstance->pu4_OidList[2];
    }

    if (nmhGetIeee8021PbbTeTeSidStorageType (u4TeSid, u4EspId,
                                             &i4Status) != SNMP_FAILURE)
    {
        if (i4Status == PBBTE_STORAGE_VOLATILE)
        {
            return (MSR_SKIP);
        }
    }
    return MSR_SAVE;
}

INT4
MsrValidateTeSidSetStatusCreate (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4TeSid = 0;
    UINT4               u4EspId = 0;
    INT4                i4Status = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* If the Length obtained is 3 then it means that the Instance OID list 
     * contains the object type first followed by the table indexes.
     *
     * Otherwise if the length is 2 then it means that the Validate routine
     * has been called for the Row Status object and that the Instance OID
     * list has only the table indexes.
     */
    if (pInstance->u4_Length == 3)
    {
        u4TeSid = pInstance->pu4_OidList[1];
        u4EspId = pInstance->pu4_OidList[2];
        if ((pInstance->pu4_OidList[0] != 5) ||
            (pData->i4_SLongValue != MSR_CREATE_AND_WAIT))
        {
            return (MSR_SKIP);
        }
    }
    else
    {
        if (pData->i4_SLongValue != MSR_CREATE_AND_WAIT)
        {
            return (MSR_SKIP);
        }
        u4TeSid = pInstance->pu4_OidList[0];
        u4EspId = pInstance->pu4_OidList[1];
    }

    if (nmhGetIeee8021PbbTeTeSidStorageType (u4TeSid, u4EspId,
                                             &i4Status) != SNMP_FAILURE)
    {
        if (i4Status == PBBTE_STORAGE_VOLATILE)
        {
            return (MSR_SKIP);
        }
    }
    return MSR_SAVE;
}

INT4
MsrValidateTeSidStatus (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4TeSid = 0;
    UINT4               u4EspId = 0;
    INT4                i4Status = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* If the Length obtained is 3 then it means that the Instance OID list 
     * contains the object type first followed by the table indexes.
     */

    /* Skip the saving of Create and Wait for rowstatus object */
    if (pInstance->u4_Length == 2)
    {
        return MSR_SKIP;
    }

    if (pInstance->u4_Length == 3)
    {
        u4TeSid = pInstance->pu4_OidList[1];
        u4EspId = pInstance->pu4_OidList[2];
    }

    if (nmhGetIeee8021PbbTeTeSidStorageType (u4TeSid, u4EspId,
                                             &i4Status) != SNMP_FAILURE)
    {
        if (i4Status == PBBTE_STORAGE_VOLATILE)
        {
            return (MSR_SKIP);
        }
    }
    return MSR_SAVE;
}
#endif
#if defined (PB_WANTED) || defined (EVB_WANTED)
INT4
MsrValidateSetBrgPortType (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetValIfMainType = 0;
    INT4                i4SubType = -1;
    UINT1               u1EtherType = 0;
    INT4                i4BrgPortType = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
#ifdef WLC_WANTED
    INT4                i4ApGroupEnableStatus = APGROUP_DISABLE;
    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainType);
    if (i4RetValIfMainType == CFA_CAPWAP_DOT11_BSS)
    {
        nmhGetFsApGroupEnabledStatus (&i4ApGroupEnableStatus);
        if ((UINT4) i4ApGroupEnableStatus == APGROUP_ENABLE)
        {
            return (MSR_SKIP);
        }
    }
#endif

    /* Bridge port type should be set after bridge mode is
     * set. As all the port related configurations are
     * already set, just set the bridge port type. */

    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainType);
#ifdef WTP_WANTED
    if (i4RetValIfMainType == 3)
    {
        return (MSR_SKIP);
    }
#endif
    nmhGetIfMainSubType ((INT4) pInstance->pu4_OidList[1], &i4SubType);
#ifdef MPLS_WANTED
    if ((i4RetValIfMainType == CFA_MPLS) ||
        (i4RetValIfMainType == CFA_MPLS_TUNNEL))
    {
        return (MSR_SKIP);
    }
#endif

    /* Dont Save the bridge port type value for the following
       IfMainType values. */

    if ((i4RetValIfMainType != CFA_LAGG) &&
        (i4RetValIfMainType != CFA_ENET) &&
        (i4RetValIfMainType != CFA_PIP) &&
        (i4RetValIfMainType != CFA_BRIDGED_INTERFACE) &&
        (!((i4RetValIfMainType == CFA_PROP_VIRTUAL_INTERFACE) &&
           (i4SubType == CFA_SUBTYPE_SISP_INTERFACE))))
    {
        return (MSR_SKIP);
    }
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip the stack ports */
        CfaGetEthernetType (pInstance->pu4_OidList[1], &u1EtherType);
        if (u1EtherType == CFA_STACK_ENET)
        {
            return (MSR_SKIP);
        }
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    nmhGetIfMainBrgPortType ((INT4) pInstance->pu4_OidList[1], &i4BrgPortType);

    /* The brigde port type can't be for SBP interface 
       CFA_STATION_FACING_BRIDGE_PORT is set in CFA */

    if (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
    {
        return MSR_SKIP;
    }

    if (pInstance->pu4_OidList[0] == 7)
    {
        return (MSR_SAVE);
    }

    return (MSR_SKIP);
}
#endif /*PB_WANTED */

#ifdef OPENFLOW_WANTED
/************************************************************************
 *  Function Name   : MsrValidateOfcCfgEntry
 *  Description     : This function validates the fsofcCfgEntry table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateOfcCfgEntry (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pData);

    /* Skip when the RowStatus is CREATE_AND_WAIT for the default context */
    if (pOid->u4_Length == OID_LENGTH_THIRTEEN)
    {
        if (pInstance->pu4_OidList[0] == 0)    /* OFC_DEFAULT_CONTEXT */
        {
            return (MSR_SKIP);
        }
    }
    return (MSR_SAVE);
}

#endif /* OPENFLOW_WANTED */

/************************************************************************
 *  Function Name   : MsrCheckAliasName
 *  Description     : This function validates the ifMainEnty table
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrCheckAliasName (tSNMP_OID_TYPE * pOid,
                   tSNMP_OID_TYPE * pInstance, tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1IfType = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (pInstance->pu4_OidList[0] == 18)
    {
        if (CfaGetIfType (pInstance->pu4_OidList[1], &u1IfType) == CFA_SUCCESS)
        {
            if ((u1IfType == CFA_L3IPVLAN) || (u1IfType == CFA_LOOPBACK)
                || (u1IfType == CFA_L3SUB_INTF))
            {
                return MSR_SAVE;
            }
        }
    }
    return MSR_SKIP;

}

/************************************************************************
* Function Name   : MsrValidateAliasName
* Description     : This function validates the ifMainEnty table
* Input           : pOid - pointer to the OID
* pInstance - pointer to the instance OID
* pData - pointer to the data to be saved
* Output          : None
* Returns         : MSR_SAVE if the object should be saved
* MSR_SKIP if just the current instance of the object
* should not be saved
* MSR_NEXT if the current scalar/table should not be
* saved
*************************************************************************/

INT4
MsrValidateAliasName (tSNMP_OID_TYPE * pOid,
                      tSNMP_OID_TYPE * pInstance, tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1IfType = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
#ifdef WLC_WANTED
    INT4                i4ApGroupEnableStatus = APGROUP_DISABLE;
    INT4                i4RetValIfMainType = 0;
    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainType);

    if (i4RetValIfMainType == CFA_CAPWAP_DOT11_BSS)
    {
        nmhGetFsApGroupEnabledStatus (&i4ApGroupEnableStatus);
        if ((UINT4) i4ApGroupEnableStatus == APGROUP_ENABLE)
        {
            return (MSR_SKIP);
        }
    }
#endif
    if (pInstance->pu4_OidList[0] == 18)
    {
        if (CfaGetIfType (pInstance->pu4_OidList[1], &u1IfType) == CFA_SUCCESS)
        {
            if (u1IfType == CFA_ENET)
            {
                return MSR_SAVE;
            }
        }
    }
    return MSR_SKIP;

}

/************************************************************************
 *  Function Name   : MsrValidateIfEntry
 *  Description     : This function validates the ifMainEnty table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateIfEntry (tSNMP_OID_TYPE * pOid,
                    tSNMP_OID_TYPE * pInstance, tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1EtherType = 0;
    INT4                i4RetValIfMainType = 0;
    INT4                i4IfIvrBridgeIface = 0;
    INT4                i4StorageType = 0;
    INT4                i4SlotNum = 0;
    INT4                i4Port = 0;
    UINT1               u1OffSet = 0;
    UINT1               u1IfType = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { 0 };
    UINT1               au1IfAliasName[CFA_MAX_IFALIAS_LENGTH] = { 0 };
    INT4                i4BrgPortType = 0;

    MEMSET (&MbsmSlotInfo, 0, sizeof (tMbsmSlotInfo));

    if (pOid->u4_Length != 12)
    {
        u1OffSet = 1;
    }
    nmhGetIfMainBrgPortType ((INT4) pInstance->pu4_OidList[u1OffSet],
                             &i4BrgPortType);

    /* when the interface is SBP, dont save the object */
    if (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
    {
        return MSR_SKIP;
    }

    if (pInstance->pu4_OidList[0] == 18)
    {
        if (CfaGetIfType (pInstance->pu4_OidList[1], &u1IfType) == CFA_SUCCESS)
        {
            if ((u1IfType == CFA_L3IPVLAN) || (u1IfType == CFA_LOOPBACK)
                || (u1IfType == CFA_L3SUB_INTF))
            {
                CfaMsrGetIfName (pInstance->pu4_OidList[1], (INT1 *) au1IfName);
                pData->pOctetStrValue->i4_Length = (INT4) STRLEN (au1IfName);
                MEMCPY (pData->pOctetStrValue->pu1_OctetList,
                        au1IfName, pData->pOctetStrValue->i4_Length);
                return (MSR_SAVE);
            }
            if (u1IfType == CFA_ENET)
            {
                if ((CFA_MGMT_PORT == TRUE) &&
                    (CfaIsMgmtPort (pInstance->pu4_OidList[1]) == CFA_TRUE))
                {
                    return (MSR_SKIP);
                }
                else
                {
#ifdef MBSM_WANTED
                    MbsmGetSlotFromPort (pInstance->pu4_OidList[1], &i4SlotNum);
                    MbsmGetSlotInfo (i4SlotNum, &MbsmSlotInfo);
                    i4Port =
                        (pInstance->pu4_OidList[1] -
                         MbsmSlotInfo.u4StartIfIndex + 1);
#else
                    CfaGetSlotAndPortFromIfIndex (pInstance->pu4_OidList[1],
                                                  &i4SlotNum, &i4Port);
#endif
                    SNPRINTF ((CHR1 *) au1IfAliasName, CFA_MAX_IFALIAS_LENGTH,
                              "%s%d/%d", ISS_ALIAS_PREFIX, i4SlotNum, i4Port);
                    pData->pOctetStrValue->i4_Length =
                        (INT4) STRLEN (au1IfAliasName);
                    MEMCPY (pData->pOctetStrValue->pu1_OctetList,
                            au1IfAliasName, pData->pOctetStrValue->i4_Length);
                    return (MSR_SAVE);
                }
            }
        }
    }

    /*For the ifMainRowStatus CREATE_AND_WAIT case alone we get a pOid->u4_Length as 12 */
    if (pOid->u4_Length != 12)
    {
        u1OffSet = 1;
    }
    else
    {
        if (VcmGetSystemModeExt (ISS_PROTOCOL_ID) == VCM_SI_MODE)
        {
            /* In SI, when the interface is a physical interface then dont 
             * save the Rowstatus object */
            if (CfaIsPhysicalInterface (pInstance->pu4_OidList[0]) == CFA_TRUE)
            {
                return (MSR_SKIP);
            }
        }
    }

    if ((CFA_MGMT_PORT == TRUE) &&
        (CfaIsMgmtPort (pInstance->pu4_OidList[u1OffSet]) == CFA_TRUE))
    {
        return (MSR_SKIP);
    }
    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[u1OffSet],
                      &i4RetValIfMainType);
#ifdef WTP_WANTED
    if (i4RetValIfMainType == 3)
    {
        return (MSR_SKIP);
    }
#endif
    nmhGetIfIvrBridgedIface ((INT4) pInstance->pu4_OidList[u1OffSet],
                             &i4IfIvrBridgeIface);
#ifdef WLC_WANTED
    INT4                i4ApGroupEnableStatus = APGROUP_DISABLE;
    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[u1OffSet],
                      &i4RetValIfMainType);
    if (i4RetValIfMainType == CFA_CAPWAP_DOT11_BSS)
    {
        nmhGetFsApGroupEnabledStatus (&i4ApGroupEnableStatus);
        if ((UINT4) i4ApGroupEnableStatus == APGROUP_ENABLE)
        {
            return (MSR_SKIP);
        }
    }
#endif
    if (u1OffSet == 1)
    {
        /* To avoid ifLinkUpDownTrapEnable & ifPromiscuousMode objects. */

        if ((pInstance->pu4_OidList[0] == 14) ||
            (pInstance->pu4_OidList[0] == 16))

        {
            if ((pInstance->pu4_OidList[1] >= CFA_MIN_SISP_IF_INDEX)
                && (pInstance->pu4_OidList[1] <= CFA_MAX_VIP_IF_INDEX))
            {
                return MSR_SKIP;
            }
        }
    }

#ifdef LNXIP4_WANTED
    if ((i4RetValIfMainType == CFA_ENET) &&
        (i4IfIvrBridgeIface == CFA_DISABLED))
    {
        /* When an interface (Port) is created, ifAlias, ifName and GddPortName 
         * GddEntry is set to Slot{Slot No./Port No.}. In Case of LINUX_IP,
         * ifAlias, ifName is set to rport{Port No.} if a Port is configured as
         * Router Port and GddPortName in GddEntry is retained as
         * Slot{Slot No./Port No.}. When ifMainType is set ifName and ifAlias is
         * set to a value mentioned in GddPortName. During MSR restoration,
         * the order of restoration is ifXEntry(ifAlias/ifName)-->ifMainType. 
         * After MSR Restoration, all the three values mentioned is filled with
         * rport{Port No.} which makes unable to enter interface configuration
         * mode through CLI. So, ifName/ifAlias is restored from GddPortName 
         * here.
         */
        if (pInstance->pu4_OidList[0] == 18)
        {
            CfaGddGetPortName (pInstance->pu4_OidList[u1OffSet], au1IfName);

            pData->pOctetStrValue->i4_Length = (INT4) (STRLEN (au1IfName));
            MEMCPY (pData->pOctetStrValue->pu1_OctetList,
                    au1IfName, pData->pOctetStrValue->i4_Length);
        }
    }
#else
    UNUSED_PARAM (pData);
    UNUSED_PARAM (au1IfName);
#endif
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        CfaGetEthernetType (pInstance->pu4_OidList[1], &u1EtherType);
        if (u1EtherType == CFA_STACK_ENET)
        {
            return (MSR_SKIP);
        }
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[0]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }

#ifdef MPLS_WANTED
    UNUSED_PARAM (pData);

    /* Problem: An MPLS Tunnel Interface(Idx=1285) is created with Invalid IfType 
     * while processing RSVP Resv Msg. This interface should not be saved
     * in MSR as it's a dynamically created port. Only the MPLS Tunnel
     * Interfaces created by administrator should be saved. */
    /* Save only the CFA_MPLS_TUNNEL interfaces created by administrator */
    if (MplsCheckIsAdminCreatedMplsTnlIf (pInstance->pu4_OidList[u1OffSet])
        == MPLS_FAILURE)
    {
        return (MSR_SKIP);
    }
#endif

    if ((i4RetValIfMainType == CFA_TELINK) || (i4RetValIfMainType == CFA_MPLS))
    {
        CfaGetIfMainStorageType (pInstance->pu4_OidList[u1OffSet],
                                 &i4StorageType);

        if (i4StorageType == 2)
        {
            return (MSR_SKIP);
        }
    }

    return (MSR_SAVE);
}

#ifdef MPLS_WANTED
/************************************************************************
 *  Function Name   : MsrValidateMplsFsMplsL2VpnMaxPwVcEntries
 *  Description     : This function validates the MPLS objects before
 *                    saving any MPLS objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateMplsFsMplsL2VpnMaxPwVcEntries (tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pInstance,
                                          tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4FsMplsL2VpnAdminStatus = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pData);

    nmhGetFsMplsL2VpnAdminStatus (&i4FsMplsL2VpnAdminStatus);

    /* L2VPN_ADMIN_UP = 1 */
    if (i4FsMplsL2VpnAdminStatus == 1)
    {
        return MSR_SKIP;
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsRsvpTeGenPduDumpMsgType
 *  Description     : This function validates the MPLS objects before
 *                    saving any MPLS objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateMplsRsvpTeGenPduDumpMsgType (tSNMP_OID_TYPE * pOid,
                                        tSNMP_OID_TYPE * pInstance,
                                        tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);

    /* 137 implies (RSVPTE_DUMP_PATH + RSVPTE_DUMP_RESV + RSVPTE_DUMP_HELLO */
    if (pData->i4_SLongValue == 137)
    {
        return (MSR_NEXT);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsIfMainEntryDoActive
 *  Description     : This function validates the ifMainEntry table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMplsIfMainEntryDoActive (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetValIfMainType;
    INT4                i4StorageType = 0;
    UINT1               u1EtherType = 0;

    UNUSED_PARAM (pOid);
#ifdef WLC_WANTED
    INT4                i4ApGroupEnableStatus = APGROUP_DISABLE;
    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainType);
    if (i4RetValIfMainType == CFA_CAPWAP_DOT11_BSS)
    {
        nmhGetFsApGroupEnabledStatus (&i4ApGroupEnableStatus);
        if ((UINT4) i4ApGroupEnableStatus == APGROUP_ENABLE)
        {
            return (MSR_SKIP);
        }
    }
#endif
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip the stack ports */
        CfaGetEthernetType (pInstance->pu4_OidList[1], &u1EtherType);
        if (u1EtherType == CFA_STACK_ENET)
        {
            return (MSR_SKIP);
        }
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }

    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainType);
#ifdef WTP_WANTED
    if (i4RetValIfMainType == 3)
    {
        return (MSR_SKIP);
    }
#endif

    /* Save ifMainRowStatus  only if it is equal to ACTIVE */
    /* 8 corresponds to ifMainRowStatus */
    if (pInstance->pu4_OidList[0] == 8)
    {
        if ((i4RetValIfMainType == CFA_MPLS) &&
            (pData->i4_SLongValue == ACTIVE))
        {
            return (MSR_SAVE);
        }
        /* Save only the CFA_MPLS_TUNNEL interfaces created by administrator */
        else if ((i4RetValIfMainType == CFA_MPLS_TUNNEL) &&
                 (MplsCheckIsAdminCreatedMplsTnlIf (pInstance->pu4_OidList[1])
                  == MPLS_SUCCESS) && (pData->i4_SLongValue == ACTIVE))
        {
            return (MSR_SAVE);
        }
        else if ((i4RetValIfMainType == CFA_TELINK) ||
                 (i4RetValIfMainType == CFA_MPLS))
        {
            CfaGetIfMainStorageType (pInstance->pu4_OidList[1], &i4StorageType);

            if (i4StorageType == 2)
            {
                return (MSR_SKIP);
            }
        }
    }
    /* Skip entries other than ifMainRowStatus->ACTIVE */
    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsIfMainEntryDoAdminUp
 *  Description     : This function validates the ifMainEntry table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMplsIfMainEntryDoAdminUp (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetValIfMainType;
    INT4                i4StorageType = 0;
    UINT1               u1EtherType = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
#ifdef WLC_WANTED
    INT4                i4ApGroupEnableStatus = APGROUP_DISABLE;
    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainType);
    if (i4RetValIfMainType == CFA_CAPWAP_DOT11_BSS)
    {
        nmhGetFsApGroupEnabledStatus (&i4ApGroupEnableStatus);
        if ((UINT4) i4ApGroupEnableStatus == APGROUP_ENABLE)
        {
            return (MSR_SKIP);
        }
    }
#endif
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip the stack ports */
        CfaGetEthernetType (pInstance->pu4_OidList[1], &u1EtherType);
        if (u1EtherType == CFA_STACK_ENET)
        {
            return (MSR_SKIP);
        }
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }

    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainType);
#ifdef WTP_WANTED
    if (i4RetValIfMainType == 3)
    {
        return (MSR_SKIP);
    }
#endif

    /* 4 corresponds to ifMainAdminStatus */
    if (pInstance->pu4_OidList[0] == 4)
    {
        if (i4RetValIfMainType == CFA_MPLS)
        {
            return (MSR_SKIP);
        }
        /* Save only the CFA_MPLS_TUNNEL interfaces created by administrator */
        else if ((i4RetValIfMainType == CFA_MPLS_TUNNEL))
        {
            if (MplsCheckIsAdminCreatedMplsTnlIf (pInstance->pu4_OidList[1])
                == MPLS_SUCCESS)
            {
                return (MSR_SAVE);
            }
        }
        else if (i4RetValIfMainType == CFA_TELINK)
        {
            CfaGetIfMainStorageType (pInstance->pu4_OidList[1], &i4StorageType);

            if (i4StorageType == 2)
            {
                return (MSR_SKIP);
            }
        }
    }
    /* Skip entries other than ifMainAdminStatus */
    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   :MsrValidateMplsIfStackEntry 
 *  Description     : This routine validates the ifStackTable.  
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateMplsIfStackEntry (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetValIfMainType = 0;
    INT4                i4StorageType = 0;
    UINT1               u1OffSet = 0;
    INT4                i4RetVal = 0;
    tCfaIfInfo          CfaIfInfo;

    /* For the ifStackStatus CREATE_AND_WAIT case alone we get a pOid->u4_Length as 11 */
    /* Msr-save only if ifStackHigherLayer and ifStackLowerLayer are present  */
    if (pOid->u4_Length != 11)
    {
        u1OffSet = 1;
    }
    if ((pInstance->pu4_OidList[u1OffSet] == 0)
        || (pInstance->pu4_OidList[u1OffSet + 1] == 0))
    {
        return (MSR_SKIP);
    }
    /* Higher Layer could be either MPLS or MPLS Tunnel Interface */
    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[u1OffSet],
                      &i4RetValIfMainType);
    if (!
        ((i4RetValIfMainType == CFA_MPLS)
         || (i4RetValIfMainType == CFA_MPLS_TUNNEL)))
    {
        return (MSR_SKIP);
    }
    /* Save only the CFA_MPLS_TUNNEL interfaces created by administrator */
    if (i4RetValIfMainType == CFA_MPLS_TUNNEL)
    {
        if (MplsCheckIsAdminCreatedMplsTnlIf (pInstance->pu4_OidList[u1OffSet])
            == MPLS_FAILURE)
        {
            return (MSR_SKIP);
        }
    }
    else if ((i4RetValIfMainType == CFA_TELINK)
             || (i4RetValIfMainType == CFA_MPLS))
    {
        CfaGetIfMainStorageType (pInstance->pu4_OidList[u1OffSet],
                                 &i4StorageType);

        if (i4StorageType == 2)
        {
            return (MSR_SKIP);
        }
    }

    /* Lower Layer could be either L3IPVLAN, Router port or 
     * MPLS Interface or MPLS tnl interface */
    /* Lower layer would be MPLS tunnel interface in case of service tunnel 
     * stacked over HLSP */
    i4RetVal =
        CfaGetIfInfo ((UINT4) pInstance->pu4_OidList[u1OffSet + 1], &CfaIfInfo);
    if (i4RetVal == CFA_FAILURE)
    {
        return (MSR_SKIP);
    }
    if (!
        (((CfaIfInfo.u1BridgedIface != CFA_ENABLED) &&
          (CfaIfInfo.u1IfType == CFA_ENET)) ||
         (CfaIfInfo.u1IfType == CFA_L3IPVLAN)
         || (CfaIfInfo.u1IfType == CFA_MPLS)
         || (CfaIfInfo.u1IfType == CFA_MPLS_TUNNEL)
         || (CfaIfInfo.u1IfType == CFA_TELINK)))
    {
        return (MSR_SKIP);
    }

    if (pData->i4_SLongValue == ACTIVE ||
        pData->i4_SLongValue == CREATE_AND_WAIT)
    {
        return (MSR_SAVE);
    }
    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsLdpEntityEntrySkipActive
 *  Description     : This routine  validates the mplsLdpEntityTable. 
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateMplsLdpEntityEntrySkipActive (tSNMP_OID_TYPE * pOid,
                                         tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);

    /* In case of mplsLdpEntityRowStatus , then check whether the value of Row Status is Active or not.
     * If Active, skip else save */

    /* Reason : mplsLdpGenericLRRowStatus must be set to Active before making mplsLdpEntityRowStatus 
     * Active.*/

    if (pInstance->pu4_OidList[0] == 23)
    {
        if (pData->i4_SLongValue == ACTIVE)
        {
            return (MSR_SKIP);

        }
    }
    /* Save entries other than mplsLdpEntityRowStatus */

    return (MSR_SAVE);

}

/************************************************************************
 *  Function Name   : MsrValidateMplsLdpEntityEntryDoActive
 *  Description     : This routine  validates the MplsLdpEntityTable. 
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateMplsLdpEntityEntryDoActive (tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);

    /* If entry corresponds to  mplsLdpEntityRowStatus , then check Row Status is Active or not.
     * If Active, save  */

    if (pInstance->pu4_OidList[0] == 23)
    {
        if (pData->i4_SLongValue == ACTIVE)
        {
            return (MSR_SAVE);
        }
    }
    /* Save entries other than mplsLdpEntityRowStatus */

    return (MSR_SKIP);

}

/************************************************************************
 *  Function Name   : MsrValidateMplsOutSegmentEntryOwner
 *  Description     : This routine  validates the MplsLdpEntityTable. 
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateMplsOutSegmentEntryOwner (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData)
{
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    static UINT1        au1OutSegIndex[5];
    INT4                i4OutSegOwner = 0;
    UINT1               u1OffSet = 0;

    UNUSED_PARAM (pData);
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;

    /* For the mplsOutSegmentRowStatus CREATE_AND_WAIT case alone
     * we get a pOid->u4_Length as 13*/
    if (pOid->u4_Length != 13)
    {
        u1OffSet = 1;
    }
    MPLS_INSTANCE_TO_SEGMENT_COPY (&OutSegmentIndex, pInstance, u1OffSet);
    if (nmhGetMplsOutSegmentOwner (&OutSegmentIndex, &i4OutSegOwner)
        == SNMP_FAILURE)
    {
        /* Failure Condition is not handled here */
    }
    /* 3 implies entry created by snmp(administrator) */
    if (i4OutSegOwner != 3)
    {
        return (MSR_SKIP);
    }

    /* The rowstatus should be made active only after
     * the augment table fsMplsOutSegmentEntry entries
     * are populated. */
    if (pInstance->pu4_OidList[0] == 11)
    {
        /* If Row Status = ACTIVE , then skip , else Save */
        if (pData->i4_SLongValue == ACTIVE)
        {
            return (MSR_SKIP);
        }
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsOutSegmentEntryDoActive
 *  Description     : This routine  validates the MplsLdpEntityTable.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateMplsOutSegmentEntryDoActive (tSNMP_OID_TYPE * pOid,
                                        tSNMP_OID_TYPE * pInstance,
                                        tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    /* This routine saves the mplsOutSegmentRowStatus = ACTIVE */
    if (pInstance->pu4_OidList[0] == 11)
    {
        /* If Row Status = ACTIVE , then skip , else Save */
        if (pData->i4_SLongValue == ACTIVE)
        {
            return (MSR_SAVE);
        }
    }
    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsInSegmentEntryOwner
 *  Description     : This routine  validates the MplsLdpEntityTable. 
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateMplsInSegmentEntryOwner (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData)
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    static UINT1        au1InSegIndex[5];
    INT4                i4InSegOwner = 0;
    UINT1               u1OffSet = 0;

    UNUSED_PARAM (pData);
    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    /* For the mplsInSegmentRowStatus CREATE_AND_WAIT case alone
     * we get a pOid->u4_Length as 13*/
    if (pOid->u4_Length != 13)
    {
        u1OffSet = 1;
    }
    MPLS_INSTANCE_TO_SEGMENT_COPY (&InSegmentIndex, pInstance, u1OffSet);
    if (nmhGetMplsInSegmentOwner (&InSegmentIndex, &i4InSegOwner)
        == SNMP_FAILURE)
    {
        /*Failure Condition is not handled here */
    }
    /* 3 implies entry created by snmp(administrator) */
    if (i4InSegOwner != 3)
    {
        return (MSR_SKIP);
    }

    /* The rowstatus should be made active only after
     * the augment table fsMplsInSegmentTable entries
     * are populated. */
    if (pInstance->pu4_OidList[0] == 10)
    {
        /* If Row Status = ACTIVE , then skip , else Save */
        if (pData->i4_SLongValue == ACTIVE)
        {
            return (MSR_SKIP);
        }
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsInSegmentEntryDoActive
 *  Description     : This routine  validates the MplsLdpEntityTable.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateMplsInSegmentEntryDoActive (tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    /* This routine saves the mplsInSegmentRowStatus = ACTIVE */
    if (pInstance->pu4_OidList[0] == 10)
    {
        /* If Row Status = ACTIVE , then save , else Skip */
        if (pData->i4_SLongValue == ACTIVE)
        {
            return (MSR_SAVE);
        }
    }
    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsXCEntryOwner
 *  Description     : This routine  validates the MplsLdpEntityTable. 
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateMplsXCEntryOwner (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE InIndex;
    tSNMP_OCTET_STRING_TYPE OutIndex;
    static UINT1        au1XCIndex[5];
    static UINT1        au1InIndex[5];
    static UINT1        au1OutIndex[5];
    INT4                i4XCOwner = 0;
    UINT1               u1OffSet = 0;

    UNUSED_PARAM (pData);
    XCIndex.pu1_OctetList = au1XCIndex;
    InIndex.pu1_OctetList = au1InIndex;
    OutIndex.pu1_OctetList = au1OutIndex;

    /* For the mplsXCRowStatus->CREATE_AND_WAIT case alone
     * we get a pOid->u4_Length as 13*/
    if (pOid->u4_Length != 13)
    {
        u1OffSet = 1;
    }

    MPLS_INSTANCE_TO_SEGMENT_COPY (&XCIndex, pInstance, u1OffSet);
    MPLS_INSTANCE_TO_SEGMENT_COPY (&InIndex, pInstance, u1OffSet);
    MPLS_INSTANCE_TO_SEGMENT_COPY (&OutIndex, pInstance, u1OffSet);
    if (nmhGetMplsXCOwner (&XCIndex, &InIndex, &OutIndex, &i4XCOwner)
        == SNMP_FAILURE)
    {
        /*Failure Condition not handled here */
    }
    /* 3 implies entry created by snmp(administrator) */
    if (i4XCOwner != 3)
    {
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateLblStackTable
 *  Description     : This routine  validates the Label Stack Table. Saves
 *                    the entry if the storage type is NON_VOLATILE. Skips
 *                    otherwise.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateLblStackTable (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData)
{
    tSNMP_OCTET_STRING_TYPE LblStackIndex;
    static UINT1        au1LblStackIndex[4];
    INT4                i4StorageType = 0;
    UINT1               u1OffSet = 0;

    UNUSED_PARAM (pData);

    LblStackIndex.pu1_OctetList = au1LblStackIndex;

    MEMSET (au1LblStackIndex, 0, 4);

    /* For the mplsLabelStackRowStatus->CREATE_AND_WAIT case alone
     * we get a pOid->u4_Length as 13*/
    if (pOid->u4_Length != 13)
    {
        u1OffSet = 1;
    }

    MPLS_INSTANCE_TO_SEGMENT_COPY (&LblStackIndex, pInstance, u1OffSet);

    if (nmhGetMplsLabelStackStorageType (&LblStackIndex,
                                         pInstance->pu4_OidList[u1OffSet],
                                         &i4StorageType) == SNMP_FAILURE)
    {
        /* Failure Condition is not handled here */
    }

    /* Skip the entries with storage type as VOLATILE */
    if (i4StorageType == 2)
    {
        return (MSR_SKIP);
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsFTNEntrySkipActive
 *  Description     : This function validates the mplsFTNMainEnty table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMplsFTNEntrySkipActive (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1OffSet = 0;
    INT4                i4ActionType = 0;
    INT4                i4OutSegOwner = 0;
    INT4                i4InSegOwner = 0;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OID_TYPE      FtnActionPtr;
    static UINT1        au1OutSegIndex[5];
    static UINT1        au1InSegIndex[5];
    static UINT4        au4FtnActionPtr[30];
    UINT1               u1Length = 0;
    UINT1               u1Index = 0;

    UNUSED_PARAM (pData);
    FtnActionPtr.pu4_OidList = au4FtnActionPtr;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    /* For the mplsFTNRowStatus->CREATE_AND_WAIT case alone
     * we get a pOid->u4_Length as 13*/
    if (pOid->u4_Length != 13)
    {
        u1OffSet = 1;
    }

    /* 2 implies mplsFTNRowStatus */
    if ((u1OffSet == 1) && (pInstance->pu4_OidList[0] == 2)
        && (pData->i4_SLongValue == ACTIVE))
    {
        return (MSR_SKIP);
    }
    if (nmhGetMplsFTNActionType (pInstance->pu4_OidList[u1OffSet],
                                 &i4ActionType) == SNMP_FAILURE)
    {
        /* Failure condition is not handled here */
    }

    /* 2 implies redirectTunnel */
    if (i4ActionType == 2)
    {
        return (MSR_SAVE);
    }
    else
    {
        /* redirectLsp case */

        /* when MsrisSaveComplete is ISS_TRUE, the validation routine is
         * called when MplsFTN table is manually configured by the user.
         * When MsrisSaveComplete is ISS_FALSE, the MplsFTN table configurations
         * are available for verification and also, the configurations can be
         * added because of signalling. Hence it has to be verified
         * before saving the configurations. */
        if (MsrisSaveComplete == ISS_TRUE)
        {
            return MSR_SAVE;
        }

        if (nmhGetMplsFTNActionPointer
            (pInstance->pu4_OidList[u1OffSet], &FtnActionPtr) == SNMP_SUCCESS)
        {
            u1OffSet = 23;        /*Out Index Length starts here */
            u1Length = (UINT1) FtnActionPtr.pu4_OidList[u1OffSet];
            for (u1Index = 0, u1OffSet = (UINT1) (u1OffSet + 1);
                 u1Index < u1Length; u1Index++, u1OffSet++)
            {
                OutSegmentIndex.pu1_OctetList[u1Index] =
                    (UINT1) FtnActionPtr.pu4_OidList[u1OffSet];
            }
            OutSegmentIndex.i4_Length = (INT4) u1Length;

            /*InSegment Label */
            u1OffSet = 18;
            u1Length = 0;
            u1Length = (UINT1) FtnActionPtr.pu4_OidList[u1OffSet];
            for (u1Index = 0, u1OffSet = (UINT1) (u1OffSet + 1);
                 u1Index < u1Length; u1Index++, u1OffSet++)

            {
                InSegmentIndex.pu1_OctetList[u1Index] =
                    (UINT1) FtnActionPtr.pu4_OidList[u1OffSet];
            }
            InSegmentIndex.i4_Length = (INT4) u1Length;

            if (nmhGetMplsOutSegmentOwner (&OutSegmentIndex, &i4OutSegOwner)
                == SNMP_FAILURE)
            {
                /* failure condition is not handled here */
            }

            if (nmhGetMplsInSegmentOwner (&InSegmentIndex, &i4InSegOwner)
                == SNMP_FAILURE)
            {
                /* failure condition is not handled here */
            }

            /* 3 implies entry created by snmp(administrator) */
            if ((i4OutSegOwner == 3) || (i4InSegOwner == 3))
            {
                return (MSR_SAVE);
            }
        }
    }
    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsFTNMapEntryDoCreateAndGo
 *  Description     : This function validates the mplsFTNMainEnty table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMplsFTNMapEntryDoCreateAndGo (tSNMP_OID_TYPE * pOid,
                                         tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pOid);

    /* The mplsFTNMapEntry table uses a CREATE AND GO to
     * create a row in the table instead of the CREATE AND
     * WAIT. So for this table the value of the RowStatus,
     * to begin with, should be set to CREATE AND GO.
     */

    pData->u4_ULongValue = MSR_CREATE_AND_GO;

    return (MSR_SAVE);

}

/************************************************************************
 *  Function Name   : MsrValidateMplsTunnelCRLDPResEntrySkipActive 
 *  Description     : This function validates the mplsTunnelCRLDPResEntry 
 *                    table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMplsTunnelCRLDPResEntrySkipActive (tSNMP_OID_TYPE * pOid,
                                              tSNMP_OID_TYPE * pInstance,
                                              tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1OffSet = 0;
    /* Save mplsTunnelCRLDPResRowStatus  only if it is not equal to ACTIVE */

    /* Reason : fsMplsTunnelCRLDPResTable Entries must be populated before making 
     * mplsTunnelCRLDPResRowStatus ACTIVE */

    /* 6 corresponds to mplsTunnelCRLDPResRowStatus */
    /* Resource ROWSTATUS->CREATE_AND_WAIT case */
    if (pOid->u4_Length != 13)
    {
        u1OffSet = 1;
    }
    /* Default resource index is 1, so need not store it */
    if (pInstance->pu4_OidList[u1OffSet] == 1)
    {
        return (MSR_SKIP);
    }
    if (MplsTeTunnelIsRsrcRoleIngress (pInstance->pu4_OidList[u1OffSet]) !=
        TE_SUCCESS)
    {
        return (MSR_SKIP);
    }
    /* 9 corresponds to mplsTunnelResourceRowStatus */
    if ((u1OffSet == 1) && (pInstance->pu4_OidList[0] == 6))
    {
        /* If Row Status = ACTIVE , then skip , else Save */
        if (pData->i4_SLongValue == ACTIVE)
        {
            return (MSR_SKIP);
        }
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsTunnelHopEntryIngress 
 *  Description     : This function validates the mplsTunnelHopEntry 
 *                    table.
 *                    It is to skip the storing of entries if the tunnel
 *                    role is ingress and to skip the rowstatus entry
 *                    for configuring gmplsTunnelHopEntry table.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMplsTunnelHopEntryIngress (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1OffSet = 0;

    /* HOP ROWSTATUS->CREATE_AND_WAIT case */
    if (pOid->u4_Length != 13)
    {
        u1OffSet = 1;
    }
    if (MplsTeTunnelIsHopListRoleIngress (pInstance->pu4_OidList[u1OffSet]) !=
        TE_SUCCESS)
    {
        return (MSR_SKIP);
    }
    /* The rowstatus should be made active only after
     * gmplsTunnelHopEntry is populated. */
    /* If Row Status = ACTIVE , then skip , else Save */
    if ((pInstance->pu4_OidList[0] == 14) && (pData->i4_SLongValue == ACTIVE))
    {
        return (MSR_SKIP);
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsTunnelHopEntryDoActive
 *  Description     : This function makes the mplsTunnelHopRowStatus 
 *                    as active and skips all other objects of that table
 *                    for configuring gmplsTunnelHopEntry table.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMplsTunnelHopEntryDoActive (tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    /* The rowstatus should be made active only after
     * gmplsTunnelHopEntry is populated. */
    if ((pInstance->pu4_OidList[0] == 14) && (pData->i4_SLongValue == ACTIVE))
    {
        return (MSR_SAVE);
    }

    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsTunnelCRLDPResEntryDoActive
 *  Description     : This function validates the mplsTunnelCRLDPResEntry 
 *                    table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMplsTunnelCRLDPResEntryDoActive (tSNMP_OID_TYPE * pOid,
                                            tSNMP_OID_TYPE * pInstance,
                                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    /* 6 here corresponds to mplsTunnelCRLDPResRowStatus.
     * This routine saves mplsTunnelCRLDPResRowStatus = ACTIVE. */

    /* Reason : This must be done only if fsMplsTunnelCRLDPResTable 
     *          entries are populated */
    /* Default resource index is 1, so need not store it */
    if (pInstance->pu4_OidList[1] == 1)
    {
        return (MSR_SKIP);
    }
    if (MplsTeTunnelIsRsrcRoleIngress (pInstance->pu4_OidList[1]) != TE_SUCCESS)
    {
        return (MSR_SKIP);
    }
    if (pInstance->pu4_OidList[0] == 6)
    {
        /* If Row Status = ACTIVE , then skip , else Save */
        if (pData->i4_SLongValue == ACTIVE)
        {
            return (MSR_SAVE);
        }
    }
    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsTunnelEntrySkipActive 
 *  Description     : This function validates the mplsTunnel Table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMplsTunnelEntrySkipActive (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4TeOwner = 0;
    INT4                i4TeSigProto = 0;
    UINT1               u1OffSet = 0;
    /* Reason : Dependency exists between the mplsTunnelTable and fsMplsDiffServElspMapTable, 
     * fsMplsDiffServElspInfoTable, fsMplsDiffServTable*/

    /* 13 corresponds to Oid length when TunnelRowStatus->CREATE_AND_WAIT 
     * entry is coming inside */
    if (pOid->u4_Length != 13)
    {
        u1OffSet = 1;
    }
    if (nmhGetMplsTunnelOwner (pInstance->pu4_OidList[u1OffSet],
                               pInstance->pu4_OidList[u1OffSet + 1],
                               pInstance->pu4_OidList[u1OffSet + 2],
                               pInstance->pu4_OidList[u1OffSet + 3],
                               &i4TeOwner) == SNMP_FAILURE)
    {
        /* failure condition is not handled here */
    }
    /* 3 implies entry created by snmp(administrator) */
    if (i4TeOwner != 3)
    {
        return (MSR_SKIP);
    }
    /* 36 implies mplsTunnelRowStatus */
    if ((u1OffSet == 1) && (pInstance->pu4_OidList[0] == 36)
        && (pData->i4_SLongValue == ACTIVE))
    {
        return (MSR_SKIP);
    }
    /* 34 implies mplsTunnelAdminStatus */
    if ((u1OffSet == 1) && (pInstance->pu4_OidList[0] == 34))
    {
        return (MSR_SKIP);
    }
    /* Skip the mplsTunnelXCPointer, if signalling protocol is not none(Static Tunnel) */
    /* 11 implies mplsTunnelXCPointer */
    if ((u1OffSet == 1) && (pInstance->pu4_OidList[0] == 11))
    {
        if (nmhGetMplsTunnelSignallingProto (pInstance->pu4_OidList[u1OffSet],
                                             pInstance->pu4_OidList[u1OffSet +
                                                                    1],
                                             pInstance->pu4_OidList[u1OffSet +
                                                                    2],
                                             pInstance->pu4_OidList[u1OffSet +
                                                                    3],
                                             &i4TeSigProto) == SNMP_FAILURE)
        {
            /* failure condition is not handled here */
        }
        /* 1 implies TE signalling protocol 'none' */
        if (i4TeSigProto != 1)
        {
            return (MSR_SKIP);
        }
    }
    /* Save all entries except above mentioned entries */
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsDiffServEntryTnlOwnerCheck 
 *  Description     : This function validates the mplsTunnel Table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMplsDiffServEntryTnlOwnerCheck (tSNMP_OID_TYPE * pOid,
                                           tSNMP_OID_TYPE * pInstance,
                                           tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4TeOwner = 0;
    UINT1               u1OffSet = 0;

    UNUSED_PARAM (pData);
    /* Reason : Dependency exists between the mplsTunnelTable and fsMplsDiffServElspMapTable, 
     * fsMplsDiffServElspInfoTable, fsMplsDiffServTable*/

    /* 13 corresponds to Oid length when TunnelRowStatus->CREATE_AND_WAIT 
     * entry is coming inside */
    if (pOid->u4_Length != 13)
    {
        u1OffSet = 1;
    }
    if (nmhGetMplsTunnelOwner (pInstance->pu4_OidList[u1OffSet],
                               pInstance->pu4_OidList[u1OffSet + 1],
                               pInstance->pu4_OidList[u1OffSet + 2],
                               pInstance->pu4_OidList[u1OffSet + 3],
                               &i4TeOwner) == SNMP_FAILURE)
    {
        /* failure condition is not handled here */
    }
    /* 3 implies entry created by snmp(administrator) */
    if (i4TeOwner != 3)
    {
        return (MSR_SKIP);
    }
    /* Save all entries except above mentioned entry */
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsDiffServElspPreConfExpPhbMapIndex 
 *  Description     : This function validates the mplsTunnel Table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMplsDiffServElspPreConfExpPhbMapIndex (tSNMP_OID_TYPE * pOid,
                                                  tSNMP_OID_TYPE * pInstance,
                                                  tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);
    /* -1 implies MPLS_INVALID_EXP_PHB_MAP_INDEX */
    if (pData->i4_SLongValue == -1)
    {
        return (MSR_NEXT);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsRsvpTeRMDPolicyObject 
 *  Description     : This function validates the mplsTunnel Table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMplsRsvpTeRMDPolicyObject (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);
    if ((pData->pOctetStrValue->i4_Length == 1)
        || (pData->pOctetStrValue->pu1_OctetList[0] == 0))
    {
        return (MSR_NEXT);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsTunnelEntryDoActive 
 *  Description     : This function validates the mplsTunnel table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMplsTunnelEntryDoActive (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4TeOwner = 0;

    UNUSED_PARAM (pOid);
    /* 36 corresponds to mplsTunnelRowStatus */

    if (pInstance->pu4_OidList[0] == 36)
    {
        if (nmhGetMplsTunnelOwner (pInstance->pu4_OidList[1],
                                   pInstance->pu4_OidList[2],
                                   pInstance->pu4_OidList[3],
                                   pInstance->pu4_OidList[4],
                                   &i4TeOwner) == SNMP_FAILURE)
        {
            /* failure condition is not handled here */
        }
        /* 3 implies entry created by snmp(administrator) */
        if ((i4TeOwner == 3) && (pData->i4_SLongValue == ACTIVE))
        {
            return (MSR_SAVE);
        }
    }

    /* Skip  all entries other than Row Status */
    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsFTNEntryDoActive
 *  Description     : This function validates the mpls FTN table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMplsFTNEntryDoActive (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData)
{

    UNUSED_PARAM (pOid);
    /* 2 corresponds to MplsFTNRowStatus */

    if (pInstance->pu4_OidList[0] == 2)
    {
        if (pData->i4_SLongValue == ACTIVE)
        {
            return (MSR_SAVE);
        }
    }

    /* Skip  all entries other than Row Status */
    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsTunnelEntryDoAdminUp 
 *  Description     : This function validates the mplsTunnel table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMplsTunnelEntryDoAdminUp (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4TeOwner = 0;
    INT4                i4AdminStat = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    /* 34 corresponds to mplsTunnelAdminStatus */
    if (pInstance->pu4_OidList[0] == 34)
    {
        if (nmhGetMplsTunnelOwner (pInstance->pu4_OidList[1],
                                   pInstance->pu4_OidList[2],
                                   pInstance->pu4_OidList[3],
                                   pInstance->pu4_OidList[4],
                                   &i4TeOwner) == SNMP_FAILURE)
        {
            /* failure condition is not handled here */
        }
        if (nmhGetMplsTunnelAdminStatus (pInstance->pu4_OidList[1],
                                         pInstance->pu4_OidList[2],
                                         pInstance->pu4_OidList[3],
                                         pInstance->pu4_OidList[4],
                                         &i4AdminStat) == SNMP_FAILURE)
        {
            /* failure condition is not handled here */
        }
        /* 3 implies entry created by snmp(administrator) */
        /* 1 implies TE_ADMIN_UP */
        if (i4TeOwner == 3)
        {
            return (MSR_SAVE);
        }
    }
    /* Skip  all entries other than Row Status */
    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsTunnelResourceEntrySkipActive 
 *  Description     : This function validates the mplsTunnelResource table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMplsTunnelResourceEntrySkipActive (tSNMP_OID_TYPE * pOid,
                                              tSNMP_OID_TYPE * pInstance,
                                              tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1OffSet = 0;
    /* Reason : Dependency exists between the mplsTunnelResourceTable and 
     * fsMplsTunnelRSVPResTable */

    /* Resource ROWSTATUS->CREATE_AND_WAIT case */
    if (pOid->u4_Length != 13)
    {
        u1OffSet = 1;
    }
    /* Default resource index is 1, so need not store it */
    if (pInstance->pu4_OidList[u1OffSet] == 1)
    {
        return (MSR_SKIP);
    }
    if (MplsTeTunnelIsRsrcRoleIngress (pInstance->pu4_OidList[u1OffSet]) !=
        TE_SUCCESS)
    {
        return (MSR_SKIP);
    }
    /* 9 corresponds to mplsTunnelResourceRowStatus */
    if ((u1OffSet == 1) && (pInstance->pu4_OidList[0] == 9))
    {

        /* If Row Status = ACTIVE , then skip , else Save */

        if (pData->i4_SLongValue == ACTIVE)
        {
            return (MSR_SKIP);
        }
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsTunnelResEntrySkipDefaultIndex 
 *  Description     : This function validates the mplsTunnelResource table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMplsTunnelResEntrySkipDefaultIndex (tSNMP_OID_TYPE * pOid,
                                               tSNMP_OID_TYPE * pInstance,
                                               tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    /* Default resource index is 1, so need not store it */
    if (pInstance->pu4_OidList[1] == 1)
    {
        return (MSR_SKIP);
    }
    if (MplsTeTunnelIsRsrcRoleIngress (pInstance->pu4_OidList[1]) != TE_SUCCESS)
    {
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsTunnelResourceEntryDoActive 
 *  Description     : This function validates the mplsTunnelResource table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateMplsTunnelResourceEntryDoActive (tSNMP_OID_TYPE * pOid,
                                            tSNMP_OID_TYPE * pInstance,
                                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);

    /* Default resource index is 1, so need not store it */
    if (pInstance->pu4_OidList[1] == 1)
    {
        return (MSR_SKIP);
    }
    if (MplsTeTunnelIsRsrcRoleIngress (pInstance->pu4_OidList[1]) != TE_SUCCESS)
    {
        return (MSR_SKIP);
    }
    /* 9 corresponds to mplsTunnelResourceRowStatus */

    if (pInstance->pu4_OidList[0] == 9)
    {

        /* If Row Status = ACTIVE , then skip , else Save */

        if (pData->i4_SLongValue == ACTIVE)
        {
            return (MSR_SAVE);
        }
    }
    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsPwEntrySkipAdmin
 *  Description     : This routine validates the pwTable  
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateMplsPwEntrySkipAdmin (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData)
{
    BOOL1               bIsStatic = FALSE;
#ifdef VPLSADS_WANTED
    INT4                i4PwOwner = 0;

    /* Skip Auto Discovered pw Row */
    if (pOid->u4_Length == 12)
    {
        nmhGetPwOwner (pInstance->pu4_OidList[1], &i4PwOwner);
    }
    else if (pOid->u4_Length == 13)
    {
        nmhGetPwOwner (pInstance->pu4_OidList[0], &i4PwOwner);
    }
    if (L2VPN_PWVC_OWNER_OTHER == i4PwOwner)
    {
        return (MSR_SKIP);
    }
#endif

    /* Reason :
     * Pre-requisite for saving AdminStatus is that pwMplsTable , pwMplsOutBound Table must have been configured , before 
     * making AdminStatus to UP.*/

    /* 37 corresponds to pwAdminStatus. */
    /* Checking if the value of pwAdminStatus is equal to UP */
    if ((pOid->u4_Length == 12) && (pInstance->pu4_OidList[0] == 37)
        && (pData->i4_SLongValue == L2VPN_PWVC_ADMIN_UP))
    {
        return (MSR_SKIP);
    }
    /* 44 corresponds to pwRowStatus. */
    /* Checking if the value of pwRowStatus. is equal to Active */
    if ((pOid->u4_Length == 12) && (pInstance->pu4_OidList[0] == 44)
        && (pData->i4_SLongValue == ACTIVE))
    {
        return (MSR_SKIP);
    }
    /* 30 corresponds to pwOutboundLabel and 31 corresponds to pwInboundLabel */
    /* pwOutboundLabel and pwInboundLabel should be skipped if the PW created 
       is Signalling */
    bIsStatic = L2VpnApiIsPwStatic (pInstance->pu4_OidList[1]);
    if (((pOid->u4_Length == 12) && (pInstance->pu4_OidList[0] == 30)) ||
        ((pOid->u4_Length == 12) && (pInstance->pu4_OidList[0] == 31)))
    {
        if (bIsStatic == FALSE)
        {
            return (MSR_SKIP);
        }
    }

    return (MSR_SAVE);
}

#ifdef VPLSADS_WANTED
/************************************************************************
 *  Function Name   : MsrSkipAutoDiscoveredPw
 *  Description     : This routine validates the pwTable
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrSkipAutoDiscoveredPw (tSNMP_OID_TYPE * pOid,
                         tSNMP_OID_TYPE * pInstance,
                         tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4PwOwner = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    /* Skip Auto Discovered pw Row */
    nmhGetPwOwner (pInstance->pu4_OidList[1], &i4PwOwner);
    if (L2VPN_PWVC_OWNER_OTHER == i4PwOwner)
    {
        return (MSR_SKIP);
    }

    return (MSR_SAVE);
}
#endif

/************************************************************************
 *  Function Name   : MsrValidateMplsPwEntrySaveRowStatus
 *  Description     : This routine validates the pwTable  
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateMplsPwEntrySaveRowStatus (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData)
{
#ifdef VPLSADS_WANTED
    INT4                i4PwOwner = 0;

    /* Skip Auto Discovered pw Row */
    nmhGetPwOwner (pInstance->pu4_OidList[1], &i4PwOwner);
    if (L2VPN_PWVC_OWNER_OTHER == i4PwOwner)
    {
        return (MSR_SKIP);
    }
#endif

    UNUSED_PARAM (pOid);
    /* Reason :
     * Pre-requisite for saving AdminStatus is that pwMplsTable , pwMplsOutBound Table must have been configured , before 
     * making AdminStatus to UP.*/

    /* 44 corresponds to pwRowStatus. */
    /* Checking if the value of pwRowStatus. is equal to Active */
    if ((pInstance->pu4_OidList[0] == 44) && (pData->i4_SLongValue == ACTIVE))
    {
        return (MSR_SAVE);
    }
    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsPwEntrySkipVplsInstance
 *  Description     : This routine validates the fsMPlspwTable
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateMplsPwEntrySkipVplsInstance (tSNMP_OID_TYPE * pOid,
                                        tSNMP_OID_TYPE * pInstance,
                                        tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4PwMode = 0;
#ifdef VPLSADS_WANTED
    INT4                i4PwOwner = 0;

    /* Skip Auto Discovered pw Row */
    nmhGetPwOwner (pInstance->pu4_OidList[1], &i4PwOwner);
    if (L2VPN_PWVC_OWNER_OTHER == i4PwOwner)
    {
        return (MSR_SKIP);
    }
#endif

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (pInstance->pu4_OidList[0] == 2)
    {
        nmhGetFsMplsL2VpnPwMode (pInstance->pu4_OidList[1], &i4PwMode);
        if (i4PwMode == 1)
        {
            return (MSR_SKIP);
        }
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateMplsPwEntrySaveAdmin
 *  Description     : This routines validates the pwTable. 
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateMplsPwEntrySaveAdmin (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData)
{
#ifdef VPLSADS_WANTED
    INT4                i4PwOwner = 0;

    /* Skip Auto Discovered pw Row */
    nmhGetPwOwner (pInstance->pu4_OidList[1], &i4PwOwner);
    if (L2VPN_PWVC_OWNER_OTHER == i4PwOwner)
    {
        return (MSR_SKIP);
    }
#endif

    UNUSED_PARAM (pOid);
    /*
     * Pre-requisite for saving AdminStatus : pwMplsTable , OutBound Table must have been configured , before 
     * making AdminStatus to UP.
     * 37 corresponds to pwAdminStatus.
     */

    if ((pInstance->pu4_OidList[0] == 37)
        && (pData->i4_SLongValue == L2VPN_PWVC_ADMIN_UP))
    {
        return (MSR_SAVE);
    }
    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateSkipAdminActivePw
 *  Description     : This routines validates the fsL2VpnPwRedGroupTable
 *                    and skips the value stored in the MIB object 
 *                    fsL2VpnPwRedGroupAdminActivePw
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateSkipAdminActivePw (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    /*
     * 12 corresponds to fsL2VpnPwRedGroupAdminActivePw
     */

    if (pInstance->pu4_OidList[0] == 12)
    {
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}

#endif

/************************************************************************
 *  Function Name   : MsrValidateIlanIfMainEntryDoActive
 *  Description     : This function validates the ifMainEntry table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateIlanIfMainEntryDoActive (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetValIfMainType;

    UNUSED_PARAM (pOid);
#ifdef WLC_WANTED
    INT4                i4ApGroupEnableStatus = APGROUP_DISABLE;
    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainType);
    if (i4RetValIfMainType == CFA_CAPWAP_DOT11_BSS)
    {
        nmhGetFsApGroupEnabledStatus (&i4ApGroupEnableStatus);
        if ((UINT4) i4ApGroupEnableStatus == APGROUP_ENABLE)
        {
            return (MSR_SKIP);
        }
    }
#endif
    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainType);
#ifdef WTP_WANTED
    if (i4RetValIfMainType == 3)
    {
        return (MSR_SKIP);
    }
#endif

    /* Save ifMainRowStatus  only if it is equal to ACTIVE */
    /* 8 corresponds to ifMainRowStatus */
    if (pInstance->pu4_OidList[0] == 8)
    {
        if ((i4RetValIfMainType == CFA_ILAN) &&
            (pData->i4_SLongValue == ACTIVE))
        {
            return (MSR_SAVE);
        }
    }

    /* Skip entries other than ifMainRowStatus->ACTIVE */
    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateIfStackEntry 
 *  Description     : This routine validates the ifStackTable.  
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateIfStackEntry (tSNMP_OID_TYPE * pOid,
                         tSNMP_OID_TYPE * pInstance,
                         tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetValIfMainType = 0;
    UINT1               u1OffSet = 0;
    INT4                i4RetVal = 0;
    INT4                i4StorageType = 0;
    tCfaIfInfo          CfaIfInfo;

    /* For the ifStackStatus CREATE_AND_WAIT case alone we get a pOid->u4_Length as 11 */
    /* Msr-save only if ifStackHigherLayer and ifStackLowerLayer are present  */
    if (pOid->u4_Length != 11)
    {
        u1OffSet = 1;
    }
    if ((pInstance->pu4_OidList[u1OffSet] == 0)
        || (pInstance->pu4_OidList[u1OffSet + 1] == 0))
    {
        return (MSR_SKIP);
    }

    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[u1OffSet],
                      &i4RetValIfMainType);

#ifdef MPLS_WANTED
    /* If the MainType is MPLS then it is a MPLS entry */
    if (((i4RetValIfMainType == CFA_MPLS) ||
         (i4RetValIfMainType == CFA_MPLS_TUNNEL)))
    {
        i4RetVal = MsrValidateMplsIfStackEntry (pOid, pInstance, pData);

        return (i4RetVal);
    }
#endif

    if ((i4RetValIfMainType == CFA_TELINK) || (i4RetValIfMainType == CFA_MPLS))
    {
        i4RetVal =
            CfaGetIfInfo ((UINT4) pInstance->pu4_OidList[u1OffSet + 1],
                          &CfaIfInfo);
        if (i4RetVal == CFA_FAILURE)
        {
            return (MSR_SKIP);
        }

        CfaGetIfMainStorageType (pInstance->pu4_OidList[u1OffSet + 1],
                                 &i4StorageType);
        if (i4StorageType == 2)
        {
            return (MSR_SKIP);
        }

        if (!(((CfaIfInfo.u1BridgedIface != CFA_ENABLED) &&
               (CfaIfInfo.u1IfType == CFA_ENET)) ||
              (CfaIfInfo.u1IfType == CFA_L3IPVLAN) ||
              (CfaIfInfo.u1IfType == CFA_TELINK)))
        {
            return (MSR_SKIP);
        }

        if (pData->i4_SLongValue == ACTIVE ||
            pData->i4_SLongValue == CREATE_AND_WAIT)
        {
            return (MSR_SAVE);
        }
    }

    if (i4RetValIfMainType == CFA_PPP)
    {
        /* if the ppp interface is saved, directly save it as
         * create_and_go. Ignore all other combinations
         */
        if (pOid->u4_Length != 11)
        {
            pData->i4_SLongValue = CREATE_AND_GO;
            return (MSR_SAVE);
        }
        return MSR_SKIP;
    }

    if (CfaIsVirtualInterface (pInstance->pu4_OidList[u1OffSet]) == CFA_FALSE)
    {
        return (MSR_SKIP);
    }
    /* Lower Layer could be only I-LAN */
    i4RetVal =
        CfaGetIfInfo ((UINT4) pInstance->pu4_OidList[u1OffSet + 1], &CfaIfInfo);
    if (i4RetVal == CFA_FAILURE)
    {
        return (MSR_SKIP);
    }
    if (CfaIfInfo.u1IfType != CFA_ILAN)
    {
        return (MSR_SKIP);
    }
    if (pData->i4_SLongValue == ACTIVE ||
        pData->i4_SLongValue == CREATE_AND_WAIT)
    {
        return (MSR_SAVE);
    }
    return (MSR_SKIP);
}

#ifdef DHCP6_CLNT_WANTED
/************************************************************************
 *  Function Name   : MsrValidateInterfaceD6ClObjects
 *  Description     : This function validates the Interface objects before
 *                    saving any DHCP6_CLNT objects.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateInterfaceD6ClObjects (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    switch (pInstance->pu4_OidList[0])
    {
        case 20:
            return (MSR_SKIP);
        default:
            break;
    }
    return (MSR_SAVE);
}

#endif

#ifdef DHCP6_RLY_WANTED
/************************************************************************
 *  Function Name   : MsrValidateInterfaceD6RlObjects
 *  Description     : This function validates the Interface objects before
 *                    saving any DHCP6_RLY objects.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateInterfaceD6RlObjects (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    switch (pInstance->pu4_OidList[0])
    {
        case 11:
            return (MSR_SKIP);
        default:
            break;
    }
    return (MSR_SAVE);
}

#endif

#ifdef ELMI_WANTED
/************************************************************************
 *  Function Name   : MsrValidateElmiObjects
 *  Description     : This function validates the ELMI objects before
 *                    saving any ELMI objects.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateElmiObjects (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetVal = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pData);
    /* When ELMI module is not started, the object will not be saved */

    ELM_LOCK ();

    if ((nmhGetFsElmiSystemControl (&i4RetVal) == SNMP_SUCCESS) &&
        (i4RetVal != ELM_SNMP_START))
    {
        ELM_UNLOCK ();
        return (MSR_NEXT);
    }

    ELM_UNLOCK ();
    return (MSR_SAVE);

}

#endif

#ifdef VLAN_WANTED
INT4
MsrValidateWildCardSetStatusCreate (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);

    pData->i4_SLongValue = MSR_CREATE_AND_WAIT;

    return MSR_SAVE;
}

INT4
MsrValidateWildCardSetStatusActive (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);

    pData->i4_SLongValue = MSR_ACTIVE;

    return MSR_SAVE;
}
#endif
/************************************************************************
 *  Function Name   : MsrValidateLldpXDot3PortConfigEntry
 *  Description     : This function validates the LLDP objects before
 *                    saving any LLDP objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateLldpXDot3PortConfigEntry (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateLldpV2Xdot3PortConfigEntry
 *  Description     : This function validates the LLDP objects before
 *                    saving any LLDP objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateLldpV2Xdot3PortConfigEntry (tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateLldpXDot1ConfigProtoVlanEntry
 *  Description     : This function validates the LLDP objects before
 *                    saving any LLDP objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateLldpXDot1ConfigProtoVlanEntry (tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pInstance,
                                          tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateLldpV2Xdot1ConfigProtoVlanEntry
 *  Description     : This function validates the LLDP objects before
 *                    saving any LLDP objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateLldpV2Xdot1ConfigProtoVlanEntry (tSNMP_OID_TYPE * pOid,
                                            tSNMP_OID_TYPE * pInstance,
                                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/******************************************************************************
 * Function           : MsrSetPhysicalPortAdminStatus
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None
 * Action             : To set all physical port admin status as UP
 ******************************************************************************/
VOID
MsrSetPhysicalPortAdminStatus ()
{
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4IntIndex = 0;
    UINT1               u1EtherType = 0;

    for (u4IntIndex = 1; u4IntIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES;
         u4IntIndex++)
    {
        CfaGetEthernetType (u4IntIndex, &u1EtherType);
        if (u1EtherType != CFA_STACK_ENET)
        {
            if (CfaGetIfInfo (u4IntIndex, &CfaIfInfo) == CFA_SUCCESS)
            {
                nmhSetIfMainAdminStatus ((INT4) u4IntIndex, CFA_IF_UP);
            }
        }
    }
}

/************************************************************************
 *  Function Name   : MsrValidateLldpXDot1ConfigPortVlanEntry
 *  Description     : This function validates the LLDP objects before
 *                    saving any LLDP objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateLldpXDot1ConfigPortVlanEntry (tSNMP_OID_TYPE * pOid,
                                         tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateLldpV2Xdot1ConfigPortVlanEntry
 *  Description     : This function validates the LLDP objects before
 *                    saving any LLDP objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateLldpV2Xdot1ConfigPortVlanEntry (tSNMP_OID_TYPE * pOid,
                                           tSNMP_OID_TYPE * pInstance,
                                           tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateLldpPortConfigEntry
 *  Description     : This function validates the LLDP objects before
 *                    saving any LLDP objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateLldpPortConfigEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFslldpv2ConfigPortMapEntry
 *  Description     : This function validates the LLDP objects before
 *                    saving any LLDP objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateFslldpv2ConfigPortMapEntry (tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if ((pInstance->pu4_OidList[1] == 1) && (pInstance->pu4_OidList[2] == 128)
        && (pInstance->pu4_OidList[3] == 194)
        && (pInstance->pu4_OidList[4] == 0) && (pInstance->pu4_OidList[5] == 0)
        && (pInstance->pu4_OidList[6] == 14))
    {
        return (MSR_SKIP);
    }
    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFslldpV2DestAddressTableEntry
 *  Description     : This function validates the LLDP objects before
 *                    saving any LLDP objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateFslldpV2DestAddressTableEntry (tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pInstance,
                                          tSNMP_MULTI_DATA_TYPE * pData)
{
    BOOL1               bFlag = OSIX_FALSE;

    UNUSED_PARAM (pOid);

    if (pInstance->pu4_OidList[0] == 1)
    {
        bFlag = OSIX_TRUE;
    }

    if ((pData->pOctetStrValue->pu1_OctetList[0] == 1) &&
        (pData->pOctetStrValue->pu1_OctetList[1] == 128) &&
        (pData->pOctetStrValue->pu1_OctetList[2] == 194) &&
        (pData->pOctetStrValue->pu1_OctetList[3] == 0) &&
        (pData->pOctetStrValue->pu1_OctetList[4] == 0) &&
        (pData->pOctetStrValue->pu1_OctetList[5] == 14) && (bFlag == OSIX_TRUE))
    {
        return (MSR_SKIP);
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateLldpV2PortConfigEntry
 *  Description     : This function validates the LLDP objects before
 *                    saving any LLDP objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateLldpV2PortConfigEntry (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateDot3PauseEntry
 *  Description     : This function validates the Dot3PauseEntry objects
 *                    before saving any  Dot3PauseEntry objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateDot3PauseEntry (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1EtherType = 0;
#ifdef NPAPI_WANTED
    INT4                i4PauseAdminMode;
#endif

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

#ifdef NPAPI_WANTED
    nmhGetDot3PauseAdminMode (pInstance->pu4_OidList[1], &i4PauseAdminMode);

    /*Do not save the default values (PauseAdminMode ETH_PAUSE_ENABLED_XMIT_AND_RCV (4)) */
    if (i4PauseAdminMode == 4)
    {
        return (MSR_SKIP);
    }
#endif

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        CfaGetEthernetType (pInstance->pu4_OidList[1], &u1EtherType);
        if (u1EtherType == CFA_STACK_ENET)
        {
            return (MSR_SKIP);
        }
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateIfMauAutoNegEntry
 *  Description     : This function validates the IfMauAutoNegEntry objects
 *                    before saving any IfMauAutoNegEntry objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateIfMauAutoNegEntry (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1EtherType = 0;
    tIfMauStruct        IfMauEntry;
    UINT4               u4IfIndex;
    UINT4               u4MauIndex;
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    MEMSET (&IfMauEntry, 0, sizeof (tIfMauStruct));
    /* Deprecated ifMauAutoNegCapAdvertised object needs to skipped */

    if (pInstance->pu4_OidList[0] == 6)
    {
        return (MSR_SKIP);
    }

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        CfaGetEthernetType (pInstance->pu4_OidList[1], &u1EtherType);
        if (u1EtherType == CFA_STACK_ENET)
        {
            return (MSR_SKIP);
        }
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    u4IfIndex = pInstance->pu4_OidList[1];
    u4MauIndex = pInstance->pu4_OidList[2];

    if (CfaGetIfAutoNegEntry (u4IfIndex, u4MauIndex, &IfMauEntry)
        != CFA_SUCCESS)
    {
        return (MSR_SKIP);
    }

    switch (pInstance->pu4_OidList[0])
    {
        case 2:
        {
            /* ifMauAutoNegAdminStatus */
            if (IfMauEntry.i4ANStatus == 0)
            {
                return (MSR_SKIP);
            }
            pData->i4_SLongValue = IfMauEntry.i4ANStatus;
            break;
        }
        case 8:
        {
            /* ifMauAutoNegRestart */
            if (IfMauEntry.i4ANRestart == 0)
            {
                return (MSR_SKIP);
            }
            pData->i4_SLongValue = IfMauEntry.i4ANRestart;
            break;
        }
        case 10:
        {
            /* ifMauAutoNegCapAdvertisedBits */
            if (IfMauEntry.i4ANCapAdvtBits == 0)
            {
                return (MSR_SKIP);
            }
            MEMCPY (pData->pOctetStrValue->pu1_OctetList,
                    &(IfMauEntry.i4ANCapAdvtBits),
                    pData->pOctetStrValue->i4_Length);
            break;
        }
        case 12:
        {
            /* ifMauAutoNegRemoteFaultAdvertised */
            if (IfMauEntry.i4ANRemFltAdvt == 0)
            {
                return (MSR_SKIP);
            }
            pData->i4_SLongValue = IfMauEntry.i4ANRemFltAdvt;
            break;
        }
        default:
        {
            return MSR_SKIP;
        }

    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsDot1qVlanStaticPortConfigEntry
 *  Description     : This function validates the VLAN objects before
 *                    saving any VLAN objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateFsDot1qVlanStaticPortConfigEntry (tSNMP_OID_TYPE * pOid,
                                             tSNMP_OID_TYPE * pInstance,
                                             tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[3]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 * + *  Function Name   : MsrValidateFsMIDot1qFutureStVlanExtEntry
 * + *  Description     : This function validates the VLAN extension table 
 * + *                    before saving any of its objects.
 * + *  Input           : pOid - pointer to the OID
 * + *                    pInstance - pointer to the instance OID
 * + *                    pData - pointer to the data to be saved
 * + *  Output          : None
 * + *  Returns         : MSR_SAVE if the object should be saved
 * + *                    MSR_SKIP if just the current instance of the object
 * + *                             should not be saved
 * + *                    MSR_NEXT if the current scalar/table should not be
 * + *                             saved
 * + ************************************************************************/

INT4
MsrValidateFsMIDot1qFutureStVlanExtEntry (tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pInstance,
                                          tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pData);

    if (pInstance->u4_Length == 2)
    {
        return (MSR_SKIP);
    }

    if ((pInstance->pu4_OidList[0] == 5) && (pOid->pu4_OidList[0] == 4))
    {
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsDot1qPortGvrpObjects
 *  Description     : This function validates the dot1q objects before
 *                    saving any dot1q objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateFsDot1qPortGvrpObjects (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{
#ifdef GARP_WANTED
    UINT4               u4ContextId;
    UINT2               u2LocalPortId;

    if ((pInstance->pu4_OidList[0] == 4) || (pInstance->pu4_OidList[0] == 7))
    {

        if (GarpGetContextInfoFromIfIndex ((UINT2) pInstance->pu4_OidList[1],
                                           &u4ContextId,
                                           &u2LocalPortId) == GARP_SUCCESS)
        {
            if (GarpIsGarpEnabledInContext (u4ContextId) == GARP_TRUE)
            {
                return (MSR_SAVE);
            }
        }

    }

#else
    UNUSED_PARAM (pInstance);
#endif

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateFsDot1qPortVlanEntry
 *  Description     : This function validates the dot1q objects before
 *                    saving any dot1q objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateFsDot1qPortVlanEntry (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
#ifdef GARP_WANTED
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
#endif

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
#ifdef GARP_WANTED
    if (pInstance->pu4_OidList[0] == 4)
    {
        if (GarpGetContextInfoFromIfIndex ((UINT2) pInstance->pu4_OidList[1],
                                           &u4ContextId,
                                           &u2LocalPortId) == GARP_SUCCESS)
        {
            if (GarpIsGarpEnabledInContext (u4ContextId) == GARP_TRUE)
            {
                return (MSR_SAVE);
            }
        }
        return (MSR_SKIP);
    }
#endif
    UNUSED_PARAM (pInstance);
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsMIDot1qFutureVlanPortEntry
 *  Description     : This function validates the dot1q objects before
 *                    saving any dot1q objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateFsMIDot1qFutureVlanPortEntry (tSNMP_OID_TYPE * pOid,
                                         tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsDot1dPortPriorityEntry
 *  Description     : This function validates the dot1q objects before
 *                    saving any dot1q objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateFsDot1dPortPriorityEntry (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsSyslogFwdAddr 
 *  Description     : This function validates Syslog Forward Entry objects 
 *                    before saving any Syslog objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateFsSyslogFwdAddr (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1OffSet = 2;
    UNUSED_PARAM (pData);
    /* For the CREATE_AND_WAIT case alone we get a pOid->u4_Length as 11 */
    if (pOid->u4_Length != 11)
    {
        u1OffSet = 1;
    }
    if ((pInstance->pu4_OidList[u1OffSet] != IPVX_ADDR_FMLY_IPV4)
        && (pInstance->pu4_OidList[u1OffSet] != IPVX_ADDR_FMLY_IPV6)
        && (pInstance->pu4_OidList[u1OffSet] != IPVX_DNS_FAMILY))
    {
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsSyslogConfigEntry
 *  Description     : This function validates Syslog  objects before
 *                    saving any Syslog objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateFsSyslogConfigEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsSyslogLogSrvAddr
 *  Description     : This function validates Syslog  objects before
 *                    saving any Syslog objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateFsSyslogLogSrvAddr (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);

    if (pData->u4_ULongValue == 0)
    {
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsDot1dUserPriorityRegenEntry
 *  Description     : This function validates user priority objects before
 *                    saving any user priority objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateFsDot1dUserPriorityRegenEntry (tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pInstance,
                                          tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsDot1dTrafficClassEntry
 *  Description     : This function validates traffic class objects before
 *                    saving any traffic class objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateFsDot1dTrafficClassEntry (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsDot1qPortConfigEntry
 *  Description     : This function validates PortConfigEntry objects before
 *                    saving any PortConfigEntry objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateFsDot1qPortConfigEntry (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[3]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsDot1dPortGarpEntry
 *  Description     : This function validates PortGarpEntry objects before
 *                    saving any PortGarpEntry objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateFsDot1dPortGarpEntry (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsDot1dPortGmrpEntry
 *  Description     : This function validates PortGmrpEntry objects before
 *                    saving any PortGmrpEntry objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateFsDot1dPortGmrpEntry (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateDot1xPaePortEntry
 *  Description     : This function validates dot1x objects before
 *                    saving any dot1x objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateDot1xPaePortEntry (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateDot1xAuthConfigEntry
 *  Description     : This function validates the dot1x objects before
 *                    saving any dot1x objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateDot1xAuthConfigEntry (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsPnacPaePortEntry
 *  Description     : This function validates the PNAC objects before
 *                    saving any PNAC objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateFsPnacPaePortEntry (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateIssConfigCtrlEntry
 *  Description     : This function validates ConfigCtrlEntry objects before
 *                    saving any ConfigCtrlEntry objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateIssConfigCtrlEntry (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateIssMirrorCtrlEntry
 *  Description     : This function validates MirrorCtrlEntry objects before
 *                    saving any MirrorCtrlEntry objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateIssMirrorCtrlEntry (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateSkipIssMirrorCtrlExtnEntry
 *  Description     : This function validates MirrorCtrlEntry objects before
 *                    saving any MirrorCtrlEntry objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateSkipIssMirrorCtrlExtnEntry (tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    if ((pInstance->u4_Length == 2) && (pInstance->pu4_OidList[0] == 6))
    {
        return MSR_SKIP;
    }
    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValidateSaveIssMirrorCtrlExtnEntry
 *  Description     : This function validates MirrorCtrlEntry objects before
 *                    saving any MirrorCtrlEntry objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateSaveIssMirrorCtrlExtnEntry (tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    if ((pInstance->u4_Length == 2) && (pInstance->pu4_OidList[0] == 6))
    {

        return MSR_SAVE;
    }
    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidateIssRateCtrlEntry
 *  Description     : This function validates RateCtrlEntry objects before
 *                    saving any RateCtrlEntry objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateIssRateCtrlEntry (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateIssPortCtrlEntry
 *  Description     : This function validates the PortCtrl objects before
 *                    saving any PortCtrl objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateIssPortCtrlEntry (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4IssPortCtrlIndex = 0;
    INT4                i4IssPortCtrlMode = 0;
    INT4                i4IssPortCtrlSpeed = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    i4IssPortCtrlIndex = (INT4) pInstance->pu4_OidList[1];

    nmhGetIssPortCtrlMode (i4IssPortCtrlIndex, &i4IssPortCtrlMode);
    nmhGetIssPortCtrlSpeed (i4IssPortCtrlIndex, &i4IssPortCtrlSpeed);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }

    /* Don't save the following objects when PortCtrl Mode is AUTO.
       1. issPortCtrlDuplex
       2. issPortCtrlSpeed
       3. issPortCtrlFlowControl
     */
    if (i4IssPortCtrlMode == ISS_AUTO)
    {
        if ((pInstance->pu4_OidList[0] == 3) ||
            (pInstance->pu4_OidList[0] == 4) ||
            (pInstance->pu4_OidList[0] == 5))

        {
            return (MSR_SKIP);
        }
    }

    /* Don't save the following objects when PortCtrl Speed is 10GB.
       1. issPortCtrlMode
       2. issPortCtrlSpeed (for XCAT)
     */
    if (i4IssPortCtrlSpeed == ISS_10GB)
    {
#if defined (XCAT) || defined (XCAT3)
        if ((pInstance->pu4_OidList[0] == 2) || (pInstance->pu4_OidList[0] == 4))    /* For Marvell chipset configuring */
            /* speed for 10 GB port is not allowed */
#else
        if (pInstance->pu4_OidList[0] == 2)
#endif
        {
            return (MSR_SKIP);
        }

    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateIssAclL2FilterEntry
 *  Description     : This function validates the L2 filter objects before
 *                    saving any L2 filter objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateIssAclL2FilterEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
#if defined (NPSIM_WANTED) || defined (LINUXSIM_WANTED)
    INT4                i4IssAclFilterIndex = ISS_ZERO_ENTRY;
    INT4                i4IssAclFilterCreationMode = ISS_ZERO_ENTRY;
#endif
    INT4                i4IssAclIndex = ISS_ZERO_ENTRY;
    BOOL1               b1IsL2AclConfFromExt = FALSE;

    UNUSED_PARAM (pOid);

    if (pOid->u4_Length == OID_LENGTH_THIRTEEN)
        i4IssAclIndex = (INT4) pInstance->pu4_OidList[0];
    if (pOid->u4_Length == OID_LENGTH_TWELVE)
        i4IssAclIndex = (INT4) pInstance->pu4_OidList[1];

    /* issAclL2FilterStatus -- Rowstatus */
    if ((pInstance->u4_Length == 2) &&
        (pInstance->pu4_OidList[0] == 11) && (pData->i4_SLongValue == ACTIVE))
    {
        return MSR_SKIP;
    }
#ifdef BCMX_WANTED
    if ((pOid->u4_Length == OID_LENGTH_TWELVE)
        && (pInstance->pu4_OidList[0] == 18))
    {
        return MSR_SKIP;
    }
#endif

    ISS_LOCK ();

    /* Validate the issAclL2FilterEntry table
     * and skip the save of filter no btw 65400 to 65535
     */
    /* Rowstatus OID in L2 ACL filter table */
    if (pOid->u4_Length == OID_LENGTH_THIRTEEN)
    {
#if defined (NPSIM_WANTED) || defined (LINUXSIM_WANTED)
        i4IssAclFilterIndex = (INT4) pInstance->pu4_OidList[0];
        if (nmhGetIssAclL2FilterCreationMode (i4IssAclFilterIndex,
                                              &i4IssAclFilterCreationMode) ==
            SNMP_SUCCESS)
        {
            if (i4IssAclFilterCreationMode == ISS_ACL_CREATION_INTERNAL)
            {
                ISS_UNLOCK ();
                return (MSR_NEXT);
            }
        }
#endif
    }

    /* objects OID in L2 ACL filter table */
    if (pOid->u4_Length == OID_LENGTH_TWELVE)
    {
#if defined (NPSIM_WANTED) || defined (LINUXSIM_WANTED)
        i4IssAclFilterIndex = (INT4) pInstance->pu4_OidList[1];
        if (nmhGetIssAclL2FilterCreationMode (i4IssAclFilterIndex,
                                              &i4IssAclFilterCreationMode) ==
            SNMP_SUCCESS)
        {
            if (i4IssAclFilterCreationMode == ISS_ACL_CREATION_INTERNAL)
            {
                ISS_UNLOCK ();
                return (MSR_NEXT);
            }
        }
#endif
    }

    /* Using i4IssAclIndex, validate the config flag done through ACL or 
     * other protocol */
    IssACLApiValidateL2AclConfigFlag (i4IssAclIndex, &b1IsL2AclConfFromExt);
    if (b1IsL2AclConfFromExt == TRUE)
    {
        ISS_UNLOCK ();
        /* SKIP MSR_SAVE for the configurations done through other protocol */
        return MSR_SKIP;
    }

    ISS_UNLOCK ();

    return MSR_SAVE;

}

/************************************************************************
 *  Function Name   : MsrValidateIssAclL3FilterEntry
 *  Description     : This function validates the L2 filter objects before
 *                    saving any L2 filter objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateIssAclL3FilterEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
#if defined (NPSIM_WANTED) || defined (LINUXSIM_WANTED)
    INT4                i4IssAclFilterIndex = ISS_ZERO_ENTRY;
    INT4                i4IssAclFilterCreationMode = ISS_ZERO_ENTRY;
#endif
    INT4                i4IssAclIndex = ISS_ZERO_ENTRY;
    BOOL1               b1IsL3AclConfFromExt = FALSE;

    UNUSED_PARAM (pOid);

    if (pOid->u4_Length == OID_LENGTH_THIRTEEN)
        i4IssAclIndex = (INT4) pInstance->pu4_OidList[0];
    if (pOid->u4_Length == OID_LENGTH_TWELVE)
        i4IssAclIndex = (INT4) pInstance->pu4_OidList[1];

    /* issAclL2FilterStatus -- Rowstatus */
    if ((pInstance->u4_Length == 2) &&
        (pInstance->pu4_OidList[0] == 25) && (pData->i4_SLongValue == ACTIVE))
    {
        return MSR_SKIP;
    }
#ifdef BCMX_WANTED
    if ((pOid->u4_Length == OID_LENGTH_TWELVE)
        && (pInstance->pu4_OidList[0] == 30))
    {
        return MSR_SKIP;
    }
#endif

    ISS_LOCK ();

    /* Validate the issAclL3FilterEntry table
     * and skip the save of filter no btw 65400 to 65535
     */
    /* Rowstatus OID in L3 ACL filter table */
    if (pOid->u4_Length == OID_LENGTH_THIRTEEN)
    {
#if defined (NPSIM_WANTED) || defined (LINUXSIM_WANTED)
        i4IssAclFilterIndex = (INT4) pInstance->pu4_OidList[0];
        if (nmhGetIssAclL3FilterCreationMode (i4IssAclFilterIndex,
                                              &i4IssAclFilterCreationMode) ==
            SNMP_SUCCESS)
        {
            if (i4IssAclFilterCreationMode == ISS_ACL_CREATION_INTERNAL)
            {
                ISS_UNLOCK ();
                return (MSR_NEXT);
            }
        }
#endif
    }

    /* objects OID in L3 ACL filter table */
    if (pOid->u4_Length == OID_LENGTH_TWELVE)
    {
#if defined (NPSIM_WANTED) || defined (LINUXSIM_WANTED)
        i4IssAclFilterIndex = (INT4) pInstance->pu4_OidList[1];
        if (nmhGetIssAclL3FilterCreationMode (i4IssAclFilterIndex,
                                              &i4IssAclFilterCreationMode) ==
            SNMP_SUCCESS)
        {
            if (i4IssAclFilterCreationMode == ISS_ACL_CREATION_INTERNAL)
            {
                ISS_UNLOCK ();
                return (MSR_NEXT);
            }
        }
#endif
    }

    /* Using i4IssAclIndex, validate the config flag done through ACL or 
     * other protocol */
    IssACLApiValidateL3AclConfigFlag (i4IssAclIndex, &b1IsL3AclConfFromExt);
    if (b1IsL3AclConfFromExt == TRUE)
    {
        ISS_UNLOCK ();
        /* SKIP MSR_SAVE for the configurations done through other protocol */
        return MSR_SKIP;
    }

    ISS_UNLOCK ();

    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValidateIssAclUserDefinedFilterEntry
 *  Description     : This function validates the Udb filter objects before
 *                    saving any Udb filter objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateIssAclUserDefinedFilterEntry (tSNMP_OID_TYPE * pOid,
                                         tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);

    /* issAclUserDefinedFilterStatus -- Rowstatus */
    if ((pInstance->u4_Length == 2) &&
        (pInstance->pu4_OidList[0] == 16) && (pData->i4_SLongValue == ACTIVE))
    {
        return MSR_SKIP;
    }
#ifdef BCMX_WANTED
    if ((pOid->u4_Length == OID_LENGTH_TWELVE)
        && (pInstance->pu4_OidList[0] == 18))
    {
        return MSR_SKIP;
    }
#endif

    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValidateIssAclL2SaveRowStatus 
 *  Description     : This function validates the L2 filter objects before
 *                    saving any L2 filter objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateIssAclL2SaveRowStatus (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData)
{

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    if (pInstance->pu4_OidList[0] == 11)
    {
        return MSR_SAVE;
    }
#ifdef BCMX_WANTED
    if (pInstance->pu4_OidList[0] == 18)
    {
        return MSR_SAVE;
    }
#endif

    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidateIssAclL3SaveRowStatus 
 *  Description     : This function validates the L3 filter objects before
 *                    saving any L3 filter objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateIssAclL3SaveRowStatus (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    if (pInstance->pu4_OidList[0] == 25)
    {
        return MSR_SAVE;
    }
#ifdef BCMX_WANTED
    if (pInstance->pu4_OidList[0] == 30)
    {
        return MSR_SAVE;
    }
#endif

    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidateIssAclUserDefinedSaveRowStatus
 *  Description     : This function validates the Udb filter objects before
 *                    saving any Udb filter objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateIssAclUserDefinedSaveRowStatus (tSNMP_OID_TYPE * pOid,
                                           tSNMP_OID_TYPE * pInstance,
                                           tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    if (pInstance->pu4_OidList[0] == 16)
    {
        return MSR_SAVE;
    }
#ifdef BCMX_WANTED
    if (pInstance->pu4_OidList[0] == 18)
    {
        return MSR_SAVE;
    }
#endif

    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidateFsDiffServAlgorithmEntry
 *  Description     : This function validates the DiffSrv objects before
 *                    saving any Diffsrv objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateFsDiffServAlgorithmEntry (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsDiffServWeightBwEntry
 *  Description     : This function validates the Diffsrv objects before
 *                    saving any Diffsrv objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateFsDiffServWeightBwEntry (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }

    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateLldpdot1ConfigEntry
 *  Description     : This function validates the LLDP objects before
 *                    saving any LLDP objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateLldpdot1ConfigEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateLldpV2Xdot1ConfigVlanNameEntry
 *  Description     : This function validates the LLDP objects before
 *                    saving any LLDP objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateLldpV2Xdot1ConfigVlanNameEntry (tSNMP_OID_TYPE * pOid,
                                           tSNMP_OID_TYPE * pInstance,
                                           tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

#if defined(IGS_WANTED) ||  defined (MLDS_WANTED)
INT4
MsrValidateSkipVlanFilterRowStatus (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* Skip the entry that saves the VlanFilterRowStatus object */
    if ((pInstance->u4_Length == 4) && (pInstance->pu4_OidList[0] == 12))
    {
        return MSR_SKIP;
    }
    return MSR_SAVE;

}

INT4
MsrValidateVlanExtensionTableActive (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (pInstance->u4_Length == 4)
    {
        if (pInstance->pu4_OidList[0] == 12)
        {
            return MSR_SAVE;
        }
    }

    return MSR_SKIP;
}

INT4
MsrValidateSkipReadonlyEntries (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* Skip the entry that saves the VlanFilterRowStatus object */
    if (pInstance->u4_Length == 3)
    {
        return MSR_SKIP;
    }

    if (pInstance->pu4_OidList[0] == 7)
    {
        return MSR_SKIP;
    }

    /* Don't save the following stub objects
     *   1. fsSnoopVlanFilterMaxLimitType
     *   2. fsSnoopVlanFilterMaxLimit
     *   3. fsSnoopVlanFilter8021pPriority
     *   4. fsSnoopVlanFilterDropReports
     */
    if ((pInstance->pu4_OidList[0] == 2) ||
        (pInstance->pu4_OidList[0] == 3) ||
        (pInstance->pu4_OidList[0] == 4) || (pInstance->pu4_OidList[0] == 5)
        || (pInstance->pu4_OidList[0] == 9))
    {
        return MSR_SKIP;
    }

    return MSR_SAVE;
}
#endif

#ifdef TAC_WANTED
/************************************************************************
 *  Function Name   : MsrValidateTacPrfEntrySkipRowStatus
 *  Description     : This routines validates the profile table to skip
 *                    the row status 
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateTacPrfEntrySkipRowStatus (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /*  In MSR while storing the objects in the table, the object's
     *  position in the table is filled in the pInstance.
     *  fsTacMcastPrfFilterEntry can be populated only when
     *  fsTacMcastProfileEntry's row status is not in service.
     *  In this routine we have to save all the object except the
     *  Rowstatus. So at first we have to check for the length of
     *  the Indices (number of indices for this table + 1) and
     *  then we have look for the object's position 
     *  in the table(5))
     *  pInstance contains the indices for the first time, the entry is
     *  stored and pOid contains the OID of row status
     *  While saving subsequent objects, pOid contains the OID of
     *  entry and pInstance contains the last digit of the objects OID
     *  followed by the indices (length = number of indices + 1)
     */

    if (pInstance->u4_Length == 3)
    {
        if (pInstance->pu4_OidList[0] == 5)
        {
            return MSR_SKIP;
        }
    }
    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValidateTacPrfEntrySaveRowStatus
 *  Description     : This routines validates the profile table to save
 *                    the row status
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateTacPrfEntrySaveRowStatus (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /*  In MSR while storing the objects in the table, the object's
     *  position in the table is filled in the pInstance.
     *  fsTacMcastPrfFilterEntry can be populated only when
     *  fsTacMcastProfileEntry's row status is not in service.
     *  In this routine we have to save all the object except the
     *  Rowstatus. So at first we have to check for the length of
     *  the Indices (number of indices for this table + 1) and
     *  then we have look for the object's position
     *  in the table(5))
     *  pInstance contains the indices for the first time, the entry is
     *  stored and pOid contains the OID of row status
     *  While saving subsequent objects, pOid contains the OID of
     *  entry and pInstance contains the last digit of the objects OID
     *  followed by the indices (length = number of indices + 1)
     */

    if (pInstance->u4_Length == 3)
    {
        if (pInstance->pu4_OidList[0] == 5)
        {
            return MSR_SAVE;
        }
    }
    return MSR_SKIP;
}
#endif
#ifdef LLDP_WANTED
/************************************************************************
 *  Function Name   : MsrValidateFsLldpLocPortEntry
 *  Description     : a) If the port ID subtype value is other than
 *                    LLDP_PORT_ID_SUB_PORT_COMP/LLDP_PORT_ID_SUB_LOCAL,
 *                    skip the objects. Else save the port id object
 *                    "FsLldpLocPortId" values.
 *                    b) To Skip XE port configurations.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateFsLldpLocPortEntry (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4LocPortIdSubtype = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    /*  If the port ID subtype value is other than LLDP_PORT_ID_SUB_PORT_COMP and
     *  LLDP_PORT_ID_SUB_LOCAL, skip the objects.Else save the port id object
     *  "FsLldpLocPortId" */
    if (pInstance->pu4_OidList[0] == 2)
    {
        nmhGetFsLldpLocPortIdSubtype ((INT4) pInstance->pu4_OidList[1],
                                      &i4LocPortIdSubtype);

        if ((i4LocPortIdSubtype != LLDP_PORT_ID_SUB_PORT_COMP) &&
            (i4LocPortIdSubtype != LLDP_PORT_ID_SUB_LOCAL))
        {
            return (MSR_SKIP);
        }
    }
    return (MSR_SAVE);
}

/************************************************************************
 * 
 *  Function Name   : MsrValidateFsLldpTraceInput 
 *
 *  Description     : This function validates the given trace input and 
 *                    prepends the string "enable " to the given trace input.
 *                    
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *                    
 *  Output          : None
 *  
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 *
 ************************************************************************/
INT4
MsrValidateFsLldpTraceInput (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               au1TraceInput[LLDP_TRC_MAX_SIZE] = "enable ";

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);

    /* This function saves only enabled traces and doesn't save disabled 
     * traces. So,if no traces are enbaled then skip the object, otherwise 
     * save the object */
    if (pData->pOctetStrValue->i4_Length == 0)
    {
        return (MSR_SKIP);
    }

    STRNCAT (au1TraceInput, pData->pOctetStrValue->pu1_OctetList,
             (UINT4) pData->pOctetStrValue->i4_Length);
    pData->pOctetStrValue->i4_Length = (INT4) STRLEN (au1TraceInput);

    STRNCPY (pData->pOctetStrValue->pu1_OctetList, au1TraceInput,
             (UINT4) pData->pOctetStrValue->i4_Length);
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateFsLldpLocChassisId
 *  Description     : This function validates the LLDP object
 *                    "fsLldpLocChassisIdSubtype" value and Skip
 *                    saving of "fsLldpLocChassisId" object if subtype is
 *                    IF_ALIAS/MAC_ADDR/NW_ADDR/IF_NAME.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateFsLldpLocChassisId (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4SetValFsLldpLocChassisIdSubtype = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pData);

    nmhGetFsLldpLocChassisIdSubtype (&i4SetValFsLldpLocChassisIdSubtype);

    /* Dont Save the MsrValidateFsLldpLocChassisId value if ChassisIdSubtype
     * is IF_ALIAS/MAC_ADDR/NW_ADDR/IF_NAME. */
    if ((i4SetValFsLldpLocChassisIdSubtype == LLDP_CHASS_ID_SUB_IF_ALIAS) ||
        (i4SetValFsLldpLocChassisIdSubtype == LLDP_CHASS_ID_SUB_MAC_ADDR) ||
        (i4SetValFsLldpLocChassisIdSubtype == LLDP_CHASS_ID_SUB_NW_ADDR) ||
        (i4SetValFsLldpLocChassisIdSubtype == LLDP_CHASS_ID_SUB_IF_NAME))
    {
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValLldpMedSkipLocRowStatus
 *  Description     : This function validates the LLDP-MED Location Table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValLldpMedSkipLocRowStatus (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);

    /* In MSR while storing the objects in the table, the object's 
     * position in the table is filled in the pInstance. 
     * For Location table the Indices are lldpLocPortNum and lldpXMedLocLocationSubtype.
     * In this routine we have to save all the object except the 
     * Rowstatus. So at first we have to check for the length of
     * the Indices (3) and then we have look for the object's position 
     * in the table.*/

    if (pInstance->u4_Length == 3)
    {
        if (pInstance->pu4_OidList[0] == 1)
        {
            if (pData->i4_SLongValue == MSR_ACTIVE)
            {
                return (MSR_SKIP);
            }
        }
    }
    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValLldpMedSaveLocRowStatus
 *  Description     : This function validates the LLDP-MED Location Table 
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValLldpMedSaveLocRowStatus (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);

    /* In MSR while storing the objects in the table, the object's 
     * position in the table is filled in the pInstance. 
     * For Location table the Indices are lldpLocPortNum and lldpXMedLocLocationSubtype.
     * In this routine we have to save only the Rowstatus. 
     * So at first we have to check for the length of the Indices (3) 
     * and then we have look for the object's position in the table.*/

    if (pInstance->u4_Length == 2)
    {
        if (pInstance->pu4_OidList[0] == 1)
        {
            if (pData->i4_SLongValue == MSR_CREATE_AND_WAIT)
            {
                return MSR_SKIP;
            }
        }
    }
    return MSR_SAVE;
}
#endif

/************************************************************************
 *  Function Name   : MsrValidateSavePrivateRoute
 *  Description     : This function validates the netIpCidrRouteEntry 
 *                    and save static ip route objects except the rowstatus.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateSavePrivateRoute (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);

    if ((pInstance->pu4_OidList[0] == 12) && (pData->i4_SLongValue == 1))
    {
        return MSR_SAVE;
    }
    /* To restore Route Preference */
    if ((pInstance->pu4_OidList[0] == 14))
    {
        return MSR_SAVE;
    }
    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidateSkipRouteRowStatus
 *  Description     : This function validates the netIpCidrRouteEntry 
 *                    and save static ip route objects except the rowstatus.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateSkipRouteRowStatus (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               au1InetCidrRouteDest[MAX_OID_LEN];
    UINT1               au1InetCidrRouteNextHop[MAX_OID_LEN];
    tSNMP_OCTET_STRING_TYPE InetCidrRouteDest;
    tSNMP_OID_TYPE      InetCidrRoutePolicy;
    tSNMP_OCTET_STRING_TYPE InetCidrRouteNextHop;
    INT4                i4InetCidrRouteDestType = IPVX_ZERO;
    UINT4               u4InetCidrRoutePfxLen = IPVX_ZERO;
    INT4                i4InetCidrRouteNextHopType = IPVX_ZERO;
    INT4                i4InetCidrRouteProto = IPVX_ZERO;
    UINT4               u4Len = IPVX_ZERO;
    UINT4               u4ContextId = 0;
    UINT1               u1Pos = IPVX_ZERO;
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4RetMetricTypeVal = IPVX_ZERO;
    UINT1               u1Cnt = IPVX_ZERO;

    UNUSED_PARAM (pData);

    MEMSET (&(au1InetCidrRouteDest[IPVX_ZERO]), IPVX_ZERO, MAX_OID_LEN);
    MEMSET (&(au1InetCidrRouteNextHop[IPVX_ZERO]), IPVX_ZERO, MAX_OID_LEN);

    InetCidrRouteDest.pu1_OctetList = &(au1InetCidrRouteDest[IPVX_ZERO]);
    InetCidrRouteNextHop.pu1_OctetList = &(au1InetCidrRouteNextHop[IPVX_ZERO]);

    /* Check whether the route type is static. 
     * If so the route should be stored.  All other routes should not be saved.
     */

    /* If (pOid == RowStatus) 
     *    pOid->Len == 11
     *    pInstance  = Indices
     *    pData      = Values of the MemObj
     * else 
     *    pOid->Len == 10
     *    pInstance  = MemObj.Indices
     *    pData      = Values of the MemObj
     */
    if (pOid->u4_Length == IPVX_OID_LEN_ELEVEN)
    {
        u1Pos = IPVX_ONE;
    }
    else
    {
        u1Pos = IPVX_ZERO;
    }

    /*  In MSR while storing the objects in the table, the object's
     *  position in the table is filled in the pInstance.
     *  Static Route private route configuration should be saved
     *  when fsMIStdInetCidrRouteStatus row status is not in service.
     *  In this routine we have to save all the object except the
     *  Rowstatus. So at first we have to check for the length of
     *  the Indices (number of indices for this table + 1) and
     *  then we have look for the object's position 
     *  in the table(17))
     *  pInstance contains the indices for the first time, the entry is
     *  stored and pOid contains the OID of row status
     *  While saving subsequent objects, pOid contains the OID of
     *  entry and pInstance contains the last digit of the objects OID
     *  followed by the indices (length = number of indices + 1)
     */
    if (pInstance->u4_Length == 40)
    {
        if (pInstance->pu4_OidList[0] == 17)
        {
            return MSR_SKIP;
        }
    }

    /* RtType.RteDest(L,V).RtPfxLen.RtPolicy(L,V).RtNHType.RtNH(L,V) */

    u4ContextId = pInstance->pu4_OidList[u1Pos++];
    i4InetCidrRouteDestType = (INT4) pInstance->pu4_OidList[u1Pos];
    u1Pos++;

    u4Len = MSR_MAX_IPVX_ADDR_LEN;

    InetCidrRouteDest.i4_Length = (INT4) u4Len;
    for (u1Cnt = IPVX_ZERO; u4Len > u1Cnt; u1Cnt++)
    {
        InetCidrRouteDest.pu1_OctetList[u1Cnt] =
            (UINT1) (pInstance->pu4_OidList[u1Pos++]);
    }

    u4InetCidrRoutePfxLen = pInstance->pu4_OidList[u1Pos];
    u1Pos++;

    u4Len = pInstance->pu4_OidList[u1Pos];
    u1Pos++;

    InetCidrRoutePolicy.u4_Length = u4Len;
    InetCidrRoutePolicy.pu4_OidList = &(pInstance->pu4_OidList[u1Pos]);
    u1Pos = (UINT1) (u1Pos + u4Len);

    i4InetCidrRouteNextHopType = (INT4) pInstance->pu4_OidList[u1Pos];
    u1Pos++;

    u4Len = MSR_MAX_IPVX_ADDR_LEN;

    InetCidrRouteNextHop.i4_Length = (INT4) u4Len;
    for (u1Cnt = IPVX_ZERO; u4Len > u1Cnt; u1Cnt++)
    {
        InetCidrRouteNextHop.pu1_OctetList[u1Cnt] =
            (UINT1) (pInstance->pu4_OidList[u1Pos++]);
    }

    IpvxLock ();
    i1RetVal = nmhGetFsMIStdInetCidrRouteProto ((INT4) u4ContextId,
                                                i4InetCidrRouteDestType,
                                                &InetCidrRouteDest,
                                                u4InetCidrRoutePfxLen,
                                                &InetCidrRoutePolicy,
                                                i4InetCidrRouteNextHopType,
                                                &InetCidrRouteNextHop,
                                                &i4InetCidrRouteProto);

    nmhGetFsMIStdInetCidrRouteMetric5 ((INT4) u4ContextId,
                                       i4InetCidrRouteDestType,
                                       &InetCidrRouteDest,
                                       u4InetCidrRoutePfxLen,
                                       &InetCidrRoutePolicy,
                                       i4InetCidrRouteNextHopType,
                                       &InetCidrRouteNextHop,
                                       &i4RetMetricTypeVal);

    IpvxUnLock ();
    if (i1RetVal == SNMP_FAILURE)
    {
        return (MSR_SKIP);
    }

    if (i4RetMetricTypeVal == IPVX_ONE)
    {
        return (MSR_SKIP);
    }

    if (i4InetCidrRouteProto == MSR_ROUTE_PROTO_MGMT)
    {
        return (MSR_SAVE);
    }

    return (MSR_SKIP);

}

/************************************************************************
 *  Function Name   : MsrValidateSaveRouteRowStatus
 *  Description     : This function validates the netIpCidrRouteEntry 
 *                    and save static ip route objects except the rowstatus.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateSaveRouteRowStatus (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               au1InetCidrRouteDest[MAX_OID_LEN];
    UINT1               au1InetCidrRouteNextHop[MAX_OID_LEN];
    tSNMP_OCTET_STRING_TYPE InetCidrRouteDest;
    tSNMP_OID_TYPE      InetCidrRoutePolicy;
    tSNMP_OCTET_STRING_TYPE InetCidrRouteNextHop;
    INT4                i4InetCidrRouteDestType = IPVX_ZERO;
    UINT4               u4InetCidrRoutePfxLen = IPVX_ZERO;
    INT4                i4InetCidrRouteNextHopType = IPVX_ZERO;
    INT4                i4InetCidrRouteProto = IPVX_ZERO;
    UINT4               u4Len = IPVX_ZERO;
    UINT4               u4ContextId = 0;
    UINT1               u1Pos = IPVX_ZERO;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               u1Cnt = IPVX_ZERO;

    UNUSED_PARAM (pData);

    MEMSET (&(au1InetCidrRouteDest[IPVX_ZERO]), IPVX_ZERO, MAX_OID_LEN);
    MEMSET (&(au1InetCidrRouteNextHop[IPVX_ZERO]), IPVX_ZERO, MAX_OID_LEN);

    InetCidrRouteDest.pu1_OctetList = &(au1InetCidrRouteDest[IPVX_ZERO]);
    InetCidrRouteNextHop.pu1_OctetList = &(au1InetCidrRouteNextHop[IPVX_ZERO]);

    /* Check whether the route type is static. 
     * If so the route should be stored.  All other routes should not be saved.
     */

    /* If (pOid == RowStatus) 
     *    pOid->Len == 11
     *    pInstance  = Indices
     *    pData      = Values of the MemObj
     * else 
     *    pOid->Len == 10
     *    pInstance  = MemObj.Indices
     *    pData      = Values of the MemObj
     */
    if (pOid->u4_Length == IPVX_OID_LEN_ELEVEN)
    {
        u1Pos = IPVX_ONE;
    }
    else
    {
        u1Pos = IPVX_ZERO;
    }

    /* RtType.RteDest(L,V).RtPfxLen.RtPolicy(L,V).RtNHType.RtNH(L,V) */

    u4ContextId = pInstance->pu4_OidList[u1Pos++];
    i4InetCidrRouteDestType = (INT4) pInstance->pu4_OidList[u1Pos];
    u1Pos++;

    u4Len = MSR_MAX_IPVX_ADDR_LEN;

    InetCidrRouteDest.i4_Length = (INT4) u4Len;
    for (u1Cnt = IPVX_ZERO; u4Len > u1Cnt; u1Cnt++)
    {
        InetCidrRouteDest.pu1_OctetList[u1Cnt] =
            (UINT1) (pInstance->pu4_OidList[u1Pos++]);
    }

    u4InetCidrRoutePfxLen = pInstance->pu4_OidList[u1Pos];
    u1Pos++;

    u4Len = pInstance->pu4_OidList[u1Pos];
    u1Pos++;

    InetCidrRoutePolicy.u4_Length = u4Len;
    InetCidrRoutePolicy.pu4_OidList = &(pInstance->pu4_OidList[u1Pos]);
    u1Pos = (UINT1) (u1Pos + u4Len);

    i4InetCidrRouteNextHopType = (INT4) pInstance->pu4_OidList[u1Pos];
    u1Pos++;

    u4Len = MSR_MAX_IPVX_ADDR_LEN;

    InetCidrRouteNextHop.i4_Length = (INT4) u4Len;
    for (u1Cnt = IPVX_ZERO; u4Len > u1Cnt; u1Cnt++)
    {
        InetCidrRouteNextHop.pu1_OctetList[u1Cnt] =
            (UINT1) (pInstance->pu4_OidList[u1Pos++]);
    }

    IpvxLock ();
    i1RetVal = nmhGetFsMIStdInetCidrRouteProto ((INT4) u4ContextId,
                                                i4InetCidrRouteDestType,
                                                &InetCidrRouteDest,
                                                u4InetCidrRoutePfxLen,
                                                &InetCidrRoutePolicy,
                                                i4InetCidrRouteNextHopType,
                                                &InetCidrRouteNextHop,
                                                &i4InetCidrRouteProto);
    IpvxUnLock ();
    if (i1RetVal == SNMP_FAILURE)
    {
        return (MSR_SKIP);
    }

    if (i4InetCidrRouteProto == MSR_ROUTE_PROTO_MGMT)
    {
        if (pInstance->u4_Length == 40)
        {
            if (pInstance->pu4_OidList[0] == 17)
            {
                return MSR_SAVE;
            }
        }
    }

    return (MSR_SKIP);

}

/************************************************************************
 *  Function Name   : MsrValidateIpvxNetToPhyEntry 
 *  Description     : This function validates the "ipNetToPhysicalEntry"
 *                    it store Static Entry only.    
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateIpvxNetToPhyEntry (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               au1NetToPhyAddr[MAX_OID_LEN];
    tSNMP_OCTET_STRING_TYPE NetToPhysNetAddr;
    INT4                i4NetToPhyType = IPVX_ZERO;
    INT4                i4NetAddrType = IPVX_ZERO;
    INT4                i4IfIndex = IPVX_ZERO;
    UINT4               u4Len = IPVX_ZERO;
    UINT1               u1Pos = IPVX_ZERO;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               u1Cnt = IPVX_ZERO;

    UNUSED_PARAM (pData);

    MEMSET (&(au1NetToPhyAddr), IPVX_ZERO, MAX_OID_LEN);
    NetToPhysNetAddr.pu1_OctetList = &(au1NetToPhyAddr[IPVX_ZERO]);

    /* Check whether the NetToPhyType is static. 
     * If so the Entry should be stored.  All other Entry should not be saved.
     */

    /* If (pOid == RowStatus) 
     *    pOid->Len == 12
     *    pInstance  = Indices
     *    pData      = Values of the MemObj
     * else 
     *    pOid->Len == 11
     *    pInstance  = MemObj.Indices
     *    pData      = Values of the MemObj
     */

    if (pOid->u4_Length == IPVX_OID_LEN_ELEVEN)
    {
        u1Pos = IPVX_ONE;
    }
    else
    {
        u1Pos = IPVX_ZERO;
    }

    /* IfIndex.NetAddrType(Ipv4/Ipv6).NetAddr(L,V) */

    i4IfIndex = (INT4) pInstance->pu4_OidList[u1Pos];
    u1Pos++;

    i4NetAddrType = (INT4) pInstance->pu4_OidList[u1Pos];
    u1Pos++;

    if (i4NetAddrType == INET_VERSION_IPV4)
        u4Len = IPADDRESS_LEN;
    else
        u4Len = MSR_MAX_IPVX_ADDR_LEN;

    NetToPhysNetAddr.i4_Length = (INT4) u4Len;
    for (u1Cnt = IPVX_ZERO; u4Len > u1Cnt; u1Cnt++)
    {
        NetToPhysNetAddr.pu1_OctetList[u1Cnt] =
            (UINT1) (pInstance->pu4_OidList[u1Pos++]);
    }

    i1RetVal = nmhGetIpNetToPhysicalType (i4IfIndex, i4NetAddrType,
                                          &NetToPhysNetAddr, &i4NetToPhyType);
    /* When the Physical Type is set as invalid, the cache entry will be
     * purged. Hence, the entry will not exist in thist case. So directly save
     * the entry */
    if (i1RetVal == SNMP_FAILURE)
    {
        return (MSR_SAVE);
    }

    if (i4NetToPhyType == IPVX_NET_TO_PHY_TYPE_STATIC)
    {
        return (MSR_SAVE);
    }

    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateSkipIpAddressIfIndex
 *  Description     : This function skips the IpAddressIfIndex entry as
 *                     it will be saved and restored via the ifIpTable
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateSkipIpAddressIfIndex (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pData);
    if ((pOid->u4_Length == 11) && (pInstance->pu4_OidList[0] == 4))
    {
        return MSR_SKIP;
    }
    return MSR_SAVE;
}

#ifdef BGP_WANTED
/************************************************************************
 * 
 *  Function Name   : MsrValidateVerifyRouterId               
 *  Description     : This function validates the bgp Router Id and saves
 *                     the object only when it is statically configured.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateVerifyRouterId (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1Flag = 0;

    UNUSED_PARAM (pData);
    if ((pOid->u4_Length == 11) && (pInstance->pu4_OidList[0] == 3))
    {
        if ((Bgp4GetBgpIdConfigType (pInstance->pu4_OidList[1], &u1Flag)
             == BGP4_SUCCESS) && (u1Flag == BGP4_FALSE))
        {
            return MSR_SKIP;
        }
    }
    if ((pOid->u4_Length == 11) && (pInstance->pu4_OidList[0] == 25))
    {
        if (BgpCheckBgpClusterId (pInstance->pu4_OidList[1],
                                  pData->pOctetStrValue) == BGP4_SUCCESS)
        {
            return MSR_SKIP;
        }
    }
    if (pInstance->pu4_OidList[1] != BGP4_DFLT_VRFID)
    {
        if ((pOid->u4_Length == 11) && ((pInstance->pu4_OidList[0] == 2) ||
                                        (pInstance->pu4_OidList[0] == 35) ||
                                        (pInstance->pu4_OidList[0] == 16)))
        {
            return MSR_SKIP;
        }
    }
    if ((pOid->u4_Length == 11) && (pInstance->pu4_OidList[0] == 54))
    {
        return MSR_SKIP;
    }

    return MSR_SAVE;
}

/************************************************************************
 * 
 *  Function Name   : MsrValidateVerifyAdminStatus               
 *  Description     : This function Allows only the AdminStatus object to restore.
 *
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateVerifyAdminStatus (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pData);

    if (pInstance->pu4_OidList[1] != BGP4_DFLT_VRFID)
    {
        if ((pOid->u4_Length == 11) && ((pInstance->pu4_OidList[0] == 2) ||
                                        (pInstance->pu4_OidList[0] == 35) ||
                                        (pInstance->pu4_OidList[0] == 16)))
        {
            return MSR_SAVE;
        }
    }
    return MSR_SKIP;
}

/************************************************************************
 *
 *  Function Name   : MsrValidateVerifyLocalAsNo
 *  Description     : This function Allows only the Local As No. object
 *                    to restore.
 *
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateVerifyLocalAsNo (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1Flag = 0;
    UNUSED_PARAM (pData);

    if ((pOid->u4_Length == 11) && ((pInstance->pu4_OidList[0] == 54)))
    {
        /* For all VRs verify if Local ASN is static/dynamic configuration */
        if ((Bgp4GetLocalAsCfgStatus (pInstance->pu4_OidList[1], &u1Flag)
             == BGP4_SUCCESS) && (u1Flag == BGP4_TRUE))
        {
            return MSR_SAVE;
        }
    }
    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidateVerifyPeerLocalAs            
 *  Description     : This function validates the peer local as and saves
 *                     the object only when it is statically configured.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateVerifyPeerLocalAs (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData)
{
    tSNMP_OCTET_STRING_TYPE IpAddr;
    UINT4               u4Count = 0;
    UINT1               au1IpAddr[MSR_MAX_IP_ADDR_LEN];
    UINT1               u1Flag = 0;

    UNUSED_PARAM (pData);
    if ((pOid->u4_Length == 11) && (pInstance->pu4_OidList[0] == 3))
    {
        IpAddr.i4_Length = (INT4) pInstance->pu4_OidList[3];
        IpAddr.pu1_OctetList = au1IpAddr;
        for (u4Count = 0; u4Count < (UINT4) IpAddr.i4_Length; u4Count++)
        {
            IpAddr.pu1_OctetList[u4Count] =
                (UINT1) pInstance->pu4_OidList[4 + u4Count];
        }

        if ((Bgp4GetPeerLocalAsCfgStatus (pInstance->pu4_OidList[1],
                                          (INT4) pInstance->pu4_OidList[2],
                                          &IpAddr, &u1Flag)
             == BGP4_SUCCESS) && (u1Flag == BGP4_FALSE))
        {
            return MSR_SKIP;
        }
    }
    if ((pOid->u4_Length == 11) && (pInstance->pu4_OidList[0] == 5))
    {
        return MSR_SKIP;
    }
    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValidateVerifyPeerAdminStatus           
 *  Description     : This function saves only PeerAdminstatus 
 *                    and skips rest of mib objects.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateVerifyPeerAdminStatus (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData)
{

    UNUSED_PARAM (pData);
    if ((pOid->u4_Length == 11) && (pInstance->pu4_OidList[0] == 5))
    {
        return MSR_SAVE;
    }
    return MSR_SKIP;
}

/************************************************************************
 * 
 *  Function Name   : MsrValBgp4AggregateSaveAdminStateAsDown
 *
 *  Description     : This function validates and save the objects other
 *                    than fsbgp4AggregateAdminStatus. And also save the 
 *                    fsbgp4AggregateAdminStatus as DOWN.
 *                    
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *                    
 *  Output          : None
 *  
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 *
 ************************************************************************/
INT4
MsrValBgp4AggregateSaveAdminStateAsDown (tSNMP_OID_TYPE * pOid,
                                         tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);

    if (pInstance->pu4_OidList[0] == 2)
    {
        pData->i4_SLongValue = BGP_ADMINDOWN;
    }

    return MSR_SAVE;
}

/************************************************************************
 * 
 *  Function Name   : MsrValBgp4AggregateSaveAdminState
 *
 *  Description     : This function saves the original value of the object 
 *                    fsbgp4AggregateAdminStatus alone.                       
 *                    
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *                    
 *  Output          : None
 *  
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 *
 ************************************************************************/
INT4
MsrValBgp4AggregateSaveAdminState (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (pInstance->pu4_OidList[0] != 2)
    {
        return (MSR_SKIP);
    }

    return MSR_SAVE;
}

/************************************************************************
 * 
 *  Function Name   : MsrValidateFsBgpPassword 
 *
 *  Description     : This function skips restoration of 
 *                    fsbgp4TCPMD5AuthPassword MIB object until  
 *                    fsbgp4TCPMD5AuthPwdSet MIB object is configured to
 *                    set state.
 *                    
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *                    
 *  Output          : None
 *  
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 *
 ************************************************************************/
INT4
MsrValidateFsBgpPassword (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (pInstance->pu4_OidList[0] == 3)
    {
        return (MSR_SKIP);
    }

    return (MSR_SAVE);
}

/************************************************************************
 * 
 *  Function Name   : MsrValidateFsBgpPwdSet 
 *
 *  Description     : This function does the restoration of 
 *                    fsbgp4TCPMD5AuthPassword MIB object after   
 *                    fsbgp4TCPMD5AuthPwdSet MIB object is configured to
 *                    set state.
 *                    
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *                    
 *  Output          : None
 *  
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 *
 ************************************************************************/
INT4
MsrValidateFsBgpPwdSet (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData)
{
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    UINT1               au1Addr[MSR_MAX_IP_ADDR_LEN];
    UINT4               u4Count;

    UNUSED_PARAM (pOid);
    if (pInstance->pu4_OidList[0] == 3)
    {
        MEMSET (au1Addr, 0, MSR_MAX_IP_ADDR_LEN);
        PeerAddr.pu1_OctetList = au1Addr;
        PeerAddr.i4_Length = (INT4) pInstance->pu4_OidList[3];
        for (u4Count = 0; u4Count < pInstance->pu4_OidList[3]; u4Count++)
        {
            PeerAddr.pu1_OctetList[u4Count] =
                (UINT1) pInstance->pu4_OidList[u4Count + 4];
        }
        /*context, Addr type, peeraddr, password */
        if (Bgp4GetNeighborPasswd (pInstance->pu4_OidList[1],
                                   (INT4) pInstance->pu4_OidList[2],
                                   &PeerAddr,
                                   pData->pOctetStrValue) == BGP4_SUCCESS)
        {
            return (MSR_SAVE);
        }
    }

    return (MSR_SKIP);
}

/************************************************************************
 * 
 *  Function Name   : MsrValidateFsTCPAOMKT
 *
 *  Description     : This function does the saving of actual password
 *                    for TCP-AO
 *                    
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *                    
 *  Output          : None
 *  
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT  current scalar/table should not be
 *                             saved 
 *
 ************************************************************************/

INT4
MsrValidateFsTCPAOMKT (tSNMP_OID_TYPE * pOid,
                       tSNMP_OID_TYPE * pInstance,
                       tSNMP_MULTI_DATA_TYPE * pData)
{

    UNUSED_PARAM (pOid);
    if ((pInstance->pu4_OidList[0] == 3) && (pOid->u4_Length == 12))
    {
        /*Need to get the password key for saving */

        if (Bgp4GetTcpAoMKTPasswd (pInstance->pu4_OidList[1],
                                   (INT4) pInstance->pu4_OidList[2],
                                   pData->pOctetStrValue) == BGP4_SUCCESS)
        {
            return (MSR_SAVE);
        }

        else
        {
            return (MSR_SKIP);
        }
    }
    return (MSR_SAVE);
}

/************************************************************************
 * 
 *  Function Name   : MsrValidateFsBgpAggrAdmin 
 *
 *  Description     : This function saves fsbgp4AggregateAdminStatus 
 *                    MIB object if it is down. If it is UP, then  
 *                    sets the global value to one and adminstate to
 *                    down and then continue saving all other MIB objects.
 *                    
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *                    
 *  Output          : None
 *  
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 *
 ************************************************************************/
INT4
MsrValidateFsBgpAggrAdmin (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    if (pInstance->pu4_OidList[0] == 2)
    {
        if (pData->i4_SLongValue == 1)
        {
            pData->i4_SLongValue = 2;
            return (MSR_SAVE);
        }
    }
    return (MSR_SAVE);
}

/************************************************************************
 * 
 *  Function Name   : MsrValidateFsBgpAggrAdminSet 
 *
 *  Description     : This function does the restoration of 
 *                    fsbgp4AggregateAdminStatus MIB object if   
 *                    global flag was set to 1 when admin status was up.
 *                    
 *                    
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *                    
 *  Output          : None
 *  
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 *
 ************************************************************************/
INT4
MsrValidateFsBgpAggrAdminSet (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);

    if ((pInstance->pu4_OidList[0] == 2) && (pData->i4_SLongValue == 1))
    {
        return (MSR_SAVE);
    }

    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateSavePeerRemoteAs  
 *  Description     : This function saves only peer remote as object   
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 ************************************************************************/
INT4
MsrValidateSavePeerRemoteAs (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* save only the fsbgp4mpePeerExtPeerRemoteAs object now */
    if (pInstance->pu4_OidList[0] == 4)
    {
        return MSR_SAVE;
    }
    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrValidateSkipPeerRemoteAs  
 *  Description     : This function saves all the objects of the peer ext
 *                     table other than the peer remote as configuration.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 ************************************************************************/
INT4
MsrValidateSkipPeerRemoteAs (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    tSNMP_OCTET_STRING_TYPE IpAddr;
    tAddrPrefix         Address;
    UINT4               u4Context = 0;
    UINT4               u4Count = 0;
    UINT4               u4PeerAddress;
    UINT1               au1IpAddr[MSR_MAX_IP_ADDR_LEN];

    UNUSED_PARAM (pData);
    MEMSET (&Address, 0, sizeof (tAddrPrefix));

    /* save all other objects other than fsbgp4mpePeerExtPeerRemoteAs as this
     * object was already saved before peer group list table */
    if (pInstance->pu4_OidList[0] == 4)
    {
        return MSR_SKIP;
    }
    if ((pOid->u4_Length == 11) && (pInstance->pu4_OidList[0] == 11))
    {
        /* for Lcl Address object */
        u4Context = pInstance->pu4_OidList[1];
        IpAddr.i4_Length = (INT4) pInstance->pu4_OidList[3];
        IpAddr.pu1_OctetList = au1IpAddr;
        for (u4Count = 0; u4Count < (UINT4) IpAddr.i4_Length; u4Count++)
        {
            IpAddr.pu1_OctetList[u4Count] =
                (UINT1) pInstance->pu4_OidList[4 + u4Count];
        }
        Address.u2Afi = pInstance->pu4_OidList[2];
        Address.u2AddressLen = IpAddr.i4_Length;
        if (pInstance->pu4_OidList[2] == 1)
        {
            MEMCPY (&u4PeerAddress, IpAddr.pu1_OctetList, sizeof (UINT4));
            u4PeerAddress = OSIX_NTOHL (u4PeerAddress);
            MEMCPY (Address.au1Address, &u4PeerAddress, sizeof (UINT4));
        }
        else
        {
            MEMCPY (Address.au1Address, IpAddr.pu1_OctetList,
                    Address.u2AddressLen);
        }
        if (Bgp4GetPeerLocalAddrCfgStatus (u4Context, Address) == BGP4_FALSE)
        {
            return MSR_SKIP;
        }
    }

    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValidatePeerSRCAddr   
 *  Description     : This function saves all the objects of the peer ext
 *                     table other than the peer remote as configuration.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 ************************************************************************/
INT4
MsrValidatePeerSRCAddr (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData)
{
    tSNMP_OCTET_STRING_TYPE IpAddr;
    tAddrPrefix         Address;
    UINT4               u4Context = 0;
    UINT4               u4Count = 0;
    UINT4               u4PeerAddress;
    UINT1               au1IpAddr[MSR_MAX_IP_ADDR_LEN];

    UNUSED_PARAM (pData);
    MEMSET (&Address, 0, sizeof (tAddrPrefix));
    if ((pOid->u4_Length == 11) && (pInstance->pu4_OidList[0] == 11))
    {
        /* for Lcl Address object */
        u4Context = pInstance->pu4_OidList[1];
        IpAddr.i4_Length = (INT4) pInstance->pu4_OidList[3];
        IpAddr.pu1_OctetList = au1IpAddr;
        for (u4Count = 0; u4Count < (UINT4) IpAddr.i4_Length; u4Count++)
        {
            IpAddr.pu1_OctetList[u4Count] =
                (UINT1) pInstance->pu4_OidList[4 + u4Count];
        }
        Address.u2Afi = pInstance->pu4_OidList[2];
        Address.u2AddressLen = IpAddr.i4_Length;
        if (pInstance->pu4_OidList[2] == 1)
        {
            MEMCPY (&u4PeerAddress, IpAddr.pu1_OctetList, sizeof (UINT4));
            u4PeerAddress = OSIX_NTOHL (u4PeerAddress);
            MEMCPY (Address.au1Address, &u4PeerAddress, sizeof (UINT4));
        }
        else
        {
            MEMCPY (Address.au1Address, IpAddr.pu1_OctetList,
                    Address.u2AddressLen);
        }
        if (Bgp4GetPeerLocalAddrCfgStatus (u4Context, Address) == BGP4_FALSE)
        {
            return MSR_SKIP;
        }
    }
    return MSR_SAVE;
}

#endif /* BGP_WANTED */

#ifdef PBB_WANTED

/************************************************************************
 *  Function Name   : MsrValidatePbbInstanceEntry
 *  Description     : This function validates the PBB backbone instance 
 *                    table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidatePbbInstanceEntry (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1InstanceMacAddrType = 0;
    UINT1               u1OffSet = 0;

    UNUSED_PARAM (pData);

    /*For the fsPbbInstanceRowStatus CREATE_AND_WAIT case alone
     * we get a pOid->u4_Length as 13 */
    if (pOid->u4_Length != 13)
    {
        u1OffSet = 1;
    }

    if ((pInstance->pu4_OidList[u1OffSet] == PBB_DEF_INSTANCE_ID)
        && (u1OffSet == 0))
    {
        return MSR_SKIP;
    }
    if (u1OffSet == 1)
    {
        /* Skip for the read only objects in Instance entry i.e for 
           fsPbbInstanceIComponents, fsPbbInstanceBComponents and 
           fsPbbInstanceBebPorts */
        if ((pOid->pu4_OidList[0] == 4) ||
            (pOid->pu4_OidList[0] == 5) || (pOid->pu4_OidList[0] == 6))
        {
            return (MSR_SKIP);
        }

        /* Check if the value of Mac address is the derived one, 
           then it is not saved */
        if (pOid->pu4_OidList[0] == 2)
        {
            PbbGetDerivedInstanceMacAddress (pInstance->pu4_OidList[1],
                                             &u1InstanceMacAddrType);
            if (u1InstanceMacAddrType == PBB_INSTANCE_MAC_DERIVED)
            {
                return (MSR_SKIP);
            }
        }
    }
    /* Save all other objects */
    return (MSR_SAVE);
}

#endif
/************************************************************************
 *  Function Name   : MsrValidateIssExtRateCtrlEntry
 *  Description     : This function validates RateCtrlEntry objects before
 *                    saving any RateCtrlEntry objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateIssExtRateCtrlEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
    {
        /* To skip XE ports configurations */
        if (IssGetStackPortCountFromNvRam () != 0)
        {
            if (CfaCheckIsHgIndex ((UINT2) pInstance->pu4_OidList[1]) ==
                CFA_SUCCESS)
            {
                return (MSR_SKIP);
            }
        }
    }
    else
    {
        UNUSED_PARAM (pInstance);
    }
    return (MSR_SAVE);
}

#ifdef IP6_WANTED
/************************************************************************
 *   Function Name   : MsrValidateSavePmtuAdminStatus
 *  Description     : This function saves pmtu admin status alone
 *   Input           : pOid - pointer to the OID
 *                     pInstance - pointer to the instance OID
 *                     pData - pointer to the data to be saved
 *   Output          : None
 *   Returns         : MSR_SAVE if the object should be saved
 *                     MSR_SKIP if just the current instance of the object
 *                     should not be saved
 *                     MSR_NEXT if the current scalar/table should not be
 *                     saved
 **************************************************************************/
INT4
MsrValidateSavePmtuAdminStatus (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4Count = 0;
    UINT4               u4Cnt = 1;
    INT4                i4TimeStamp = 0;
    UINT1               au1Array[MSR_MAX_IP_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE Fsipv6AddrAddress;

    UNUSED_PARAM (pData);

    MSR_MEMSET (&au1Array, 0, sizeof (au1Array));

    /* update the array index position for getting IPv6 address */
    if (pOid->u4_Length == 12)
    {
        u4Cnt = 2;
    }
    if (pInstance->u4_Length < (MSR_MAX_IP_ADDR_LEN + u4Cnt))
    {
        return MSR_SKIP;
    }

    Fsipv6AddrAddress.pu1_OctetList = au1Array;
    Fsipv6AddrAddress.i4_Length = MSR_MAX_IP_ADDR_LEN;

    for (u4Count = 0; u4Count < MSR_MAX_IP_ADDR_LEN; u4Count++)
    {
        Fsipv6AddrAddress.pu1_OctetList[u4Count]
            = (UINT1) pInstance->pu4_OidList[u4Count + u4Cnt];
    }
    /* No need to save dynamic pmtu entries */
    nmhGetFsMIIpv6PmtuTimeStamp ((INT4) pInstance->pu4_OidList[1],
                                 &Fsipv6AddrAddress, &i4TimeStamp);
    if ((UINT4) i4TimeStamp != IP6_MAX_SYS_TIME)
    {
        return MSR_SKIP;
    }

    if (pInstance->pu4_OidList[0] != 4)
    {
        return (MSR_SKIP);
    }

    return MSR_SAVE;
}

/************************************************************************
 *   Function Name   : MsrValidateSkipPmtuAdminStatus
 *   Description     : This function skips pmtu admin status alone
 *   Input           : pOid - pointer to the OID
 *                     pInstance - pointer to the instance OID
 *                     pData - pointer to the data to be saved
 *   Output          : None
 *   Returns         : MSR_SAVE if the object should be saved
 *                     MSR_SKIP if just the current instance of the object
 *                    should not be saved
 *                     MSR_NEXT if the current scalar/table should not be
 *                     saved
 *************************************************************************/
INT4
MsrValidateSkipPmtuAdminStatus (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4Count = 0;
    UINT4               u4Cnt = 1;
    INT4                i4TimeStamp = 0;
    UINT1               au1Array[MSR_MAX_IP_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE Fsipv6AddrAddress;

    UNUSED_PARAM (pData);

    MSR_MEMSET (&au1Array, 0, sizeof (au1Array));

    /* update the array index position for getting IPv6 address */
    if (pOid->u4_Length == 12)
    {
        u4Cnt = 2;
    }
    if (pInstance->u4_Length < (MSR_MAX_IP_ADDR_LEN + u4Cnt))
    {
        return MSR_SKIP;
    }

    Fsipv6AddrAddress.pu1_OctetList = au1Array;
    Fsipv6AddrAddress.i4_Length = MSR_MAX_IP_ADDR_LEN;

    for (u4Count = 0; u4Count < MSR_MAX_IP_ADDR_LEN; u4Count++)
    {
        Fsipv6AddrAddress.pu1_OctetList[u4Count]
            = (UINT1) pInstance->pu4_OidList[u4Count + u4Cnt];
    }

    /* No need to save dynamic pmtu entries */
    nmhGetFsMIIpv6PmtuTimeStamp ((INT4) pInstance->pu4_OidList[1],
                                 &Fsipv6AddrAddress, &i4TimeStamp);
    if ((UINT4) i4TimeStamp != IP6_MAX_SYS_TIME)
    {
        return MSR_SKIP;
    }

    if (pInstance->pu4_OidList[0] == 4)
    {
        return (MSR_SKIP);
    }

    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValidateIpv6NdLanCacheEntry
 *  Description     : This function validates the "ipNetToPhysicalEntry"
 *                    it store Static Entry only.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateIpv6NdLanCacheEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               au1NdLanCacheAddr[MAX_OID_LEN];
    tSNMP_OCTET_STRING_TYPE NdLanCacheIPv6Addr;
    INT4                i4IfIndex = IPVX_ZERO;
    INT4                i4NdLanCacheState = 0;
    UINT1               u1Pos = IPVX_ONE;
    INT1                i1RetVal = SNMP_FAILURE;
    UINT1               u1Cnt = IPVX_ZERO;

    UNUSED_PARAM (pData);
    UNUSED_PARAM (pOid);

    MEMSET (&(au1NdLanCacheAddr), IPVX_ZERO, MAX_OID_LEN);
    NdLanCacheIPv6Addr.pu1_OctetList = &(au1NdLanCacheAddr[IPVX_ZERO]);

    /* Check whether the CacheState is static.
     * If so the Entry should be stored.  All other Entry should not be saved.
     */

    i4IfIndex = (INT4) pInstance->pu4_OidList[u1Pos];
    u1Pos++;

    NdLanCacheIPv6Addr.i4_Length = MSR_MAX_IPVX_ADDR_LEN;
    for (u1Cnt = IPVX_ZERO; MSR_MAX_IPVX_ADDR_LEN > u1Cnt; u1Cnt++)
    {
        NdLanCacheIPv6Addr.pu1_OctetList[u1Cnt] =
            (UINT1) (pInstance->pu4_OidList[u1Pos++]);
    }

    i1RetVal = nmhGetFsipv6NdLanCacheState (i4IfIndex,
                                            &NdLanCacheIPv6Addr,
                                            &i4NdLanCacheState);
    /* When the Physical Type is set as invalid, the cache entry will be
     * purged. Hence, the entry will not exist in thist case. So directly save
     * the entry */
    if (i1RetVal == SNMP_FAILURE)
    {
        return (MSR_SAVE);
    }

    if (i4NdLanCacheState == 1)
    {
        return (MSR_SAVE);
    }

    return (MSR_SKIP);
}

/************************************************************************
 *   Function Name   : MsrValidateIpv6RouteEntry
 *   Description     : This function validates the StaticIpv6RouteEntry
 *   Input           : pOid - pointer to the OID
 *                     pInstance - pointer to the instance OID
 *                     pData - pointer to the data to be saved
 *   Output          : None
 *   Returns         : MSR_SAVE if the object should be saved
 *                     MSR_SKIP if just the current instance of the object
 *                     should not be saved
 *                     MSR_NEXT if the current scalar/table should not be
 *                     saved
 ************************************************************************/
INT4
MsrValidateIpv6RouteEntry (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pData);
    UNUSED_PARAM (pOid);

    /*The "Route Protocol" object is checked to save the statically
     *      * added routes. The indexing offset of the route protocol object
     *           * is modified accordingly.
     *                */

    if (pInstance->u4_Length == 35)
    {
        if (pInstance->pu4_OidList[18] == 3)
        {
            return MSR_SAVE;
        }
        else
        {
            return MSR_SKIP;
        }
    }
    else
    {
        if (pInstance->pu4_OidList[17] == 3)
        {
            return MSR_SAVE;
        }
        else
        {
            return MSR_SKIP;
        }
    }

}

/************************************************************************
*    Function Name   : MsrValidateIpv6AddrSelPolicyEntry
*    Description     : This function validates the Ipv6AddrSelPolicyEntry
*    Input           : pOid - pointer to the OID
*                      pInstance - pointer to the instance OID
*                      pData - pointer to the data to be saved
*    Output          : None
*    Returns         : MSR_SAVE if the object should be saved
*                      MSR_SKIP if just the current instance of the object
*                      should not be saved
*                      MSR_NEXT if the current scalar/table should not be
*                      saved
*************************************************************************/

INT4
MsrValidateIpv6AddrSelPolicyEntry (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    /* checking interface index value */
    if (pInstance->pu4_OidList[pInstance->u4_Length - 1] == 0)
    {
        return MSR_SKIP;
    }
    return MSR_SAVE;
}

#endif
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
/************************************************************************
 *  Function Name   : MsrValidateIpNetToMediaEntry
 *  Description     : This function validates the ipNetToMediaEntry
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateIpNetToMediaEntry (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4IpAddress = 0;
    INT4                i4IfIndex = 0;
    UINT1               au1Array[MSR_MAX_IP_ADDR_LEN];
    UINT1               au1CopyString[MSR_MAX_OID_LEN];
    UINT4               u4Count = 0;
    INT4                i4MediaType = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    MSR_MEMSET (&au1Array, 0, sizeof (au1Array));
    MSR_MEMSET (&au1CopyString, 0, sizeof (au1CopyString));

    i4IfIndex = (INT4) pInstance->pu4_OidList[1];

    for (u4Count = 2; u4Count < 5; u4Count++)
    {
        SPRINTF ((char *) au1Array, "%d.", pInstance->pu4_OidList[u4Count]);

        MSR_STRNCAT (au1CopyString, au1Array,
                     (sizeof (au1CopyString) - STRLEN (au1CopyString) - 1));
    }

    SPRINTF ((char *) au1Array, "%d", pInstance->pu4_OidList[u4Count]);
    MSR_STRNCAT (au1CopyString, au1Array,
                 (sizeof (au1CopyString) - STRLEN (au1CopyString) - 1));

    u4IpAddress = OSIX_NTOHL (INET_ADDR (au1CopyString));

    /* Take a ARP Protocol Lock */
    ARP_PROT_LOCK ();

    if (nmhGetIpNetToMediaType (i4IfIndex, u4IpAddress, &i4MediaType)
        != SNMP_FAILURE)
    {
        if (i4MediaType != ARP_STATIC)
        {
            /* Unlock it */
            ARP_PROT_UNLOCK ();

            return (MSR_SKIP);
        }
        else
        {
            /* Unlock it */
            ARP_PROT_UNLOCK ();

            return (MSR_SAVE);
        }
    }

    /* Unlock it */
    ARP_PROT_UNLOCK ();
    return (MSR_SAVE);
}
#endif
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
/************************************************************************
 *  Function Name   : MsrValidateIpRouteEntry
 *  Description     : This function validates the fsIpCommonRoutingEntry 
 *                    and save static ip route objects except the rowstatus.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateIpRouteEntry (tSNMP_OID_TYPE * pOid,
                         tSNMP_OID_TYPE * pInstance,
                         tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4IpAddress;
    UINT4               u4Mask;
    UINT4               u4Tos;
    UINT4               u4NextHop;
    UINT4               u4RtProto;
    UINT4               u4Count;
    UINT4               u4Cnt1;
    UINT1               au1Array[MSR_MAX_IP_ADDR_LEN];
    UINT1               au1CopyString[MSR_MAX_OID_LEN];

    UNUSED_PARAM (pData);

    MSR_MEMSET (&au1Array, 0, sizeof (au1Array));
    MSR_MEMSET (&au1CopyString, 0, sizeof (au1CopyString));

    /* Check whether the route type is static. If so the route should be stored. 
     * All other routes should not be saved.
     */

    /* IP ADDRESS */

    if (pOid->pu4_OidList[10] != 0)
    {
        /* Check if OID is for RowStatus */
        u4Cnt1 = 0;
    }
    else
    {
        u4Cnt1 = 1;
    }

    for (u4Count = u4Cnt1; u4Count < (u4Cnt1 + 3); u4Count++)
    {
        SPRINTF ((char *) au1Array, "%d.", pInstance->pu4_OidList[u4Count]);

        MSR_STRCAT (au1CopyString, au1Array);
    }

    SPRINTF ((char *) au1Array, "%d", pInstance->pu4_OidList[u4Count]);
    MSR_STRCAT (au1CopyString, au1Array);

    u4IpAddress = OSIX_NTOHL (INET_ADDR (au1CopyString));

    MSR_MEMSET (&au1CopyString, 0, sizeof (au1CopyString));

    u4Count++;

    /* Net MASK  */
    for (; u4Count < (u4Cnt1 + 7); u4Count++)
    {
        SPRINTF ((char *) au1Array, "%d.", pInstance->pu4_OidList[u4Count]);
        MSR_STRCAT (au1CopyString, au1Array);
    }
    SPRINTF ((char *) au1Array, "%d", pInstance->pu4_OidList[u4Count]);
    MSR_STRCAT (au1CopyString, au1Array);

    u4Mask = OSIX_NTOHL (INET_ADDR (au1CopyString));

    MSR_MEMSET (&au1CopyString, 0, sizeof (au1CopyString));

    u4Count++;
    u4Tos = pInstance->pu4_OidList[u4Count];

    u4Count++;

    /* GW ADDRESS */
    for (; u4Count < (u4Cnt1 + 12); u4Count++)
    {
        SPRINTF ((char *) au1Array, "%d.", pInstance->pu4_OidList[u4Count]);
        MSR_STRCAT (au1CopyString, au1Array);
    }
    SPRINTF ((char *) au1Array, "%d", pInstance->pu4_OidList[u4Count]);
    MSR_STRCAT (au1CopyString, au1Array);

    u4NextHop = OSIX_NTOHL (INET_ADDR (au1CopyString));

    if (nmhGetIpCidrRouteProto (u4IpAddress, u4Mask, (INT4) u4Tos, u4NextHop,
                                (INT4 *) &u4RtProto) == SNMP_FAILURE)
    {
        return (MSR_SKIP);
    }

    if (u4RtProto != MSR_ROUTE_PROTO_MGMT)
    {
        return (MSR_SKIP);

    }

    return (MSR_SAVE);
}
#endif
#ifdef DVMRP_WANTED
/************************************************************************
 *  Function Name   : MsrValidateDvmrpInterfaceEntryDoCreateAndGo
 *  Description     : This function validates the DvmrpInterfaceEntry table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateDvmrpInterfaceEntryDoCreateAndGo (tSNMP_OID_TYPE * pOid,
                                             tSNMP_OID_TYPE * pInstance,
                                             tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pOid);

    /* The mplsFTNMapEntry table uses a CREATE AND GO to
     * create a row in the table instead of the CREATE AND
     * WAIT. So for this table the value of the RowStatus,
     * to begin with, should be set to CREATE AND GO.
     */

    pData->u4_ULongValue = MSR_CREATE_AND_GO;

    return (MSR_SAVE);

}

/************************************************************************
 *  Function Name   : MsrValidateDvmrpLogEnabled
 *  Description     : This function validates the DvmrpLogEnabled
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateDvmrpLogEnabled (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetValDvmrpStatus;
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    nmhGetDvmrpStatus (&i4RetValDvmrpStatus);

#ifdef FS_NPAPI
    if (i4RetValDvmrpStatus == DVMRP_DISABLED)
    {
        return (MSR_SKIP);
    }
#endif
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateDvmrpLogMask
 *  Description     : This function validates the DvmrpLogMask
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateDvmrpLogMask (tSNMP_OID_TYPE * pOid,
                         tSNMP_OID_TYPE * pInstance,
                         tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetValDvmrpStatus;
    UNUSED_PARAM (pInstance);
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    nmhGetDvmrpStatus (&i4RetValDvmrpStatus);

#ifdef FS_NPAPI
    if (i4RetValDvmrpStatus == DVMRP_DISABLED)
    {
        return (MSR_SKIP);
    }
#endif
    return (MSR_SAVE);
}

#endif
#ifdef ISIS_WANTED
INT4
MsrValidateIsIsSetAdminStatusCreate (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);

    if (pInstance->pu4_OidList[0] == 13)
    {
        return MSR_SKIP;
    }
    if (pInstance->pu4_OidList[0] == 20 && pData->i4_SLongValue == ACTIVE)
    {
        return MSR_SKIP;
    }

    return MSR_SAVE;
}

INT4
MsrValidateIsIsSetAdminStatusActive (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (pInstance->pu4_OidList[0] == 13 || pInstance->pu4_OidList[0] == 20)
    {
        return MSR_SAVE;
    }

    return MSR_SKIP;
}

INT4
MsrValidateIsIsSetProtSuppRowStatus (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);
    if (pData->u4_ULongValue == MSR_ACTIVE)
    {
        return (MSR_SKIP);
    }
    pData->u4_ULongValue = MSR_CREATE_AND_GO;
    return (MSR_SAVE);

}

INT4
MsrValidateIsIsSetMAARowStatus (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);

    if (pData->u4_ULongValue == MSR_ACTIVE)
    {
        return (MSR_SKIP);
    }
    pData->u4_ULongValue = MSR_CREATE_AND_GO;
    return (MSR_SAVE);

}

INT4
MsrValidateIsIsSetCircAdminCreate (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);

    if (pInstance->pu4_OidList[0] == 5)
    {
        return (MSR_SKIP);
    }
    if (pInstance->pu4_OidList[0] == 6 && pData->i4_SLongValue == ACTIVE)
    {
        return (MSR_SKIP);
    }

    return MSR_SAVE;
}

INT4
MsrValidateIsIsSetCircAdminActive (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (pInstance->pu4_OidList[0] == 6)
    {
        return (MSR_SAVE);
    }
    return MSR_SKIP;
}

INT4
MsrValidateIsIsSetCircAdminActive1 (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (pInstance->pu4_OidList[0] == 5)
    {
        return (MSR_SAVE);
    }
    return MSR_SKIP;
}

INT4
MsrValidateIsIsSetRowStatusChange (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);
    if (pData->u4_ULongValue == MSR_ACTIVE)
    {
        return (MSR_SKIP);
    }
    pData->u4_ULongValue = MSR_CREATE_AND_GO;
    return (MSR_SAVE);

}

INT4
MsrValidateIsIsSetExtIPIfAddr (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);

    if (pInstance->pu4_OidList[0] == 5 && pData->i4_SLongValue == ACTIVE)
    {
        pData->i4_SLongValue = CREATE_AND_GO;
    }
    return MSR_SAVE;
}

INT4
MsrValidateIsIsSetSummAddrExistCreate (tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);

    if (pInstance->pu4_OidList[0] == 4 && pData->i4_SLongValue == ACTIVE)
    {
        return MSR_SKIP;
    }
    if (pInstance->pu4_OidList[0] == 5)
    {
        return MSR_SKIP;
    }
    return MSR_SAVE;
}

INT4
MsrValidateIsIsSetSummAddrExistActive (tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);

    if (pInstance->pu4_OidList[0] == 4 && pData->i4_SLongValue == ACTIVE)
    {
        return MSR_SAVE;
    }
    if (pInstance->pu4_OidList[0] == 5)
    {
        return MSR_SAVE;
    }

    return MSR_SKIP;
}

INT4
MsrValidateIsIsSetIPRAExistCreate (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);

    if (pInstance->pu4_OidList[0] == 6 && pData->i4_SLongValue == ACTIVE)
    {
        return MSR_SKIP;
    }
    if (pInstance->pu4_OidList[0] == 7)
    {
        return MSR_SKIP;
    }
    if (((pInstance->u4_Length == 3) && (pInstance->pu4_OidList[1] == 2)) ||
        ((pInstance->u4_Length == 4) && (pInstance->pu4_OidList[2] == 2)))
    {
        return MSR_SKIP;
    }

    return MSR_SAVE;
}

INT4
MsrSkipProtoMaskSysTable (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    if ((pInstance->pu4_OidList[0] == 40) || (pInstance->pu4_OidList[0] == 41))
    {
        return MSR_SKIP;
    }
    return MSR_SAVE;
}

INT4
MsrSaveProtoMaskSysTable (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    if ((pInstance->pu4_OidList[0] == 40) || (pInstance->pu4_OidList[0] == 41))
    {
        return MSR_SAVE;
    }
    return MSR_SKIP;
}

INT4
MsrValidateIsIsSkipAutoIPRA (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (pInstance->pu4_OidList[2] == 2)
    {
        return MSR_SKIP;
    }

    return MSR_SAVE;
}

INT4
MsrValidateIsIsSetIPRAExistActive (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);

    if (((pInstance->u4_Length == 3) && (pInstance->pu4_OidList[1] == 2)) ||
        ((pInstance->u4_Length == 4) && (pInstance->pu4_OidList[2] == 2)))
    {
        return MSR_SKIP;
    }

    if (pInstance->pu4_OidList[0] == 6 && pData->i4_SLongValue == ACTIVE)
    {
        return MSR_SAVE;
    }
    if (pInstance->pu4_OidList[0] == 7)
    {
        return MSR_SAVE;
    }

    return MSR_SKIP;

}
#endif
#ifdef MRP_WANTED
/************************************************************************
 *  Function Name   : MsrValidateIeee8021BridgePortMmrpEntry
 *  Description     : This function validates 8021BridgeBase Port Entry
 *                    objects before saving any objects.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateIeee8021BridgePortMmrpEntry (tSNMP_OID_TYPE * pOid,
                                        tSNMP_OID_TYPE * pInstance,
                                        tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* From this table we have to save the following objects
       only,
       1)  ieee8021BridgePortMmrpEnabledStatus
       2)  ieee8021BridgePortRestrictedGroupRegistration
     */
    if ((pInstance->pu4_OidList[0] == 1) || (pInstance->pu4_OidList[0] == 4))
    {
        return (MSR_SAVE);
    }

    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateMRPInstanceEntry
 *  Description     : This function validates MRPInstanceEntry the table
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateMRPInstanceEntry (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4Status;
    UNUSED_PARAM (pData);
    UNUSED_PARAM (pOid);
    if (pInstance->pu4_OidList[0] > 1)
    {
        /*system control to be checked before setting the tableMRPInstanceEntry */
        nmhGetFsMrpInstanceSystemControl (pInstance->pu4_OidList[1], &i4Status);
        if (i4Status == CFA_FALSE)
        {
            return MSR_SKIP;
        }
    }
    return MSR_SAVE;
}
#endif
#ifdef RIP_WANTED

/************************************************************************
 *  Function Name   : MsrValidateFsMIRip2Entry
 *  Description     : This function validates the fsMIRip2GlobalEntry table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateFsMIRip2Entry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pData);

    /* Skip CREATE_AND_WAIT for default context */
    if (pOid->u4_Length == OID_LENGTH_TWELVE)
    {
        if (pInstance->pu4_OidList[0] == RIP_DEFAULT_CXT)
        {
            return (MSR_SKIP);
        }
    }
    return (MSR_SAVE);
}
#endif
#ifdef IPVX_WANTED
INT4
MsrValidateIpGlobalEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

#ifndef IP6_WANTED
    if ((pInstance->pu4_OidList[0] == 5) || (pInstance->pu4_OidList[0] == 6))
    {
        return MSR_SKIP;
    }
#else
    UNUSED_PARAM (pInstance);
#endif
    return (MSR_SAVE);
}
#endif
/************************************************************************
 *  Function Name   : MsrValidateIssPIStorageType
 *  Description     : This function checks whether we can restore this  
 *                    mib object entries, based on the storage type of the 
 *                    entries in Port Isolation Table. 
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateIssPIStorageType (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4IngressPort = 0;
    tVlanId             VlanId = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    u4IngressPort = pInstance->pu4_OidList[0];
    VlanId = (tVlanId) pInstance->pu4_OidList[1];

    if (ISS_SUCCESS == IssPIApiIsPIStorageTypeVolatile (u4IngressPort, VlanId))
    {
        return MSR_SKIP;
    }
    pData->u4_ULongValue = MSR_CREATE_AND_GO;
    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValidateIssAclTriggerCommit
 *  Description     : This function validates commit Trigger action.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateIssAclTriggerCommit (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{

    INT4                i4ProvisionMode = ISS_COMMIT_ACTION_FALSE;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);

    nmhGetIssAclProvisionMode (&i4ProvisionMode);

    if (i4ProvisionMode == ISS_CONSOLIDATED)
    {
        pData->i4_SLongValue = ISS_COMMIT_ACTION_TRUE;
        return MSR_SAVE;
    }
    UNUSED_PARAM (pData);
    return MSR_SKIP;
}

#ifdef FIREWALL_WANTED
/************************************************************************
 *  Function Name   : MsrValidateFwlStatIfEntry
 *  Description     : The function saves no other entries of the Firewall
 *                    Interface Statistics table except fwlIfTrapThreshold.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateFwlStatIfEntry (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* 13 corresponds to fwlIfTrapThreshold. If the incoming oid
     * corresponds to the above object, then save it */
    /* All other entries  are skipped */
    if (pInstance->pu4_OidList[0] == 13)
    {
        /* Saving fwlIfTrapThreshold Object */
        return (MSR_SAVE);
    }
    else
    {
        /* Skipping fwlStatIfClear Object */
        return (MSR_SKIP);
    }
}

/************************************************************************
 *
 *  Function Name   : MsrValSkipFwlFilterAddrInfo
 *
 *  Description     : This function skips the restoration of
 *                    fwlFilterSrcAddress - Source IP address information
 *                    fwlFilterDestAddress - Destination IP address info
 *                    until fwlFilterAddrType is saved. 
 *
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *
 *  Output          : None
 *
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 *
 ************************************************************************/
INT4
MsrValSkipFwlFilterAddrInfo (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* 1.3.6.1.4.1.2076.16.2.3.1.2 corresponds to FwlFilterSrcAddress.
     * 1.3.6.1.4.1.2076.16.2.3.1.3 corresponds to  FwlFilterDestAddress
     * The incoming OID is skipped if it belongs to second/third object
     * in fwlDefnFilterTable, which is FwlFilterSrcAddress/
     * FwlFilterDestAddress.
     * Reason : The configuration of Source and Destination address 
     *          info of firewall filters is dependent on fwlFilterAddrType.
     *          Hence, the Src/Dest Address info is skipped until the 
     *          Address type information is saved.
     */
    if ((pInstance->pu4_OidList[0] == 3) || (pInstance->pu4_OidList[0] == 2))
    {
        return (MSR_SKIP);
    }

    return (MSR_SAVE);
}

/************************************************************************
 *
 *  Function Name   : MsrValSetFwlFilterAddrInfo
 *
 *  Description     : This function does the restoration of
 *                    fwlFilterSrcAddress - Source IP address information
 *                    fwlFilterDestAddress - Destination IP address info
 *
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *
 *  Output          : None
 *
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 *
 ************************************************************************/
INT4
MsrValSetFwlFilterAddrInfo (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* 1.3.6.1.4.1.2076.16.2.3.1.2 corresponds to FwlFilterSrcAddress.
     * 1.3.6.1.4.1.2076.16.2.3.1.3 corresponds to  FwlFilterDestAddress
     * 1.3.6.1.4.1.2076.16.2.3.1.16 corresponds to  fwlFilterRowStatus
     * The entry is saved if the incoming IOD is any of the one mentioned 
     * above. 
     * MsrValSkipFwlFilterAddrInfo skipped the Src/Destination Address
     * information and that has to be saved. (Entries 2 and 3) 
     * The Src/Destination address configuration
     * puts the row-status in not-in-service state. Hence to retain the state
     * of row status, it has be saved again.
     */
    if ((pInstance->pu4_OidList[0] == 3) ||
        (pInstance->pu4_OidList[0] == 2) || (pInstance->pu4_OidList[0] == 16)
        || (pInstance->pu4_OidList[0] == 13))
    {
        return (MSR_SAVE);
    }

    return (MSR_SKIP);
}

#endif /* FIREWALL_WANTED */

#ifdef PPP_WANTED
/************************************************************************
 *  Function Name   : MsrValidatePppIpObjects
 *  Description     : This function validates the pppExtIpEntry table
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidatePppIpObjects (tSNMP_OID_TYPE * pOid,
                         tSNMP_OID_TYPE * pInstance,
                         tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* As the Objects other than  Peer Ip address and peer pri-dns-address are
     * presently not configurable and are not used, store only these two */
    if ((pInstance->pu4_OidList[0] == 2) || (pInstance->pu4_OidList[0] == 10))
    {
        return (MSR_SAVE);
    }
    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrSavePppRowStatus
 *  Description     : This function is used to save the row status of
 *                    PPP interface
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrSavePppRowStatus (tSNMP_OID_TYPE * pOid,
                     tSNMP_OID_TYPE * pInstance, tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetValIfMainType = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    /* The IfMainType object in this table has already been saved.
     * Skip it */
    /* Do not set the admin status until the MSR is complete. so skip the
     * Admin status object, irrespective of the type or state */
    if ((pInstance->pu4_OidList[0] == 2) || (pInstance->pu4_OidList[0] == 4))
    {
        return (MSR_SKIP);
    }
    nmhGetIfMainType (pInstance->pu4_OidList[1], &i4RetValIfMainType);
    /* Save the row satus of PPP/MP interfaces only. Row status of all other
     * interfaces are taken care separately */
    if ((i4RetValIfMainType == CFA_PPP) || (i4RetValIfMainType == CFA_MP))
    {
        if (pInstance->pu4_OidList[0] == 11)
        {
            return (MSR_SAVE);
        }
    }
    return (MSR_SKIP);
}

INT4
MsrValidatePPPDependency (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    INT4                i4RetValIfMainType;
#ifdef WLC_WANTED
    INT4                i4ApGroupEnableStatus = APGROUP_DISABLE;
    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainType);
    if (i4RetValIfMainType == CFA_CAPWAP_DOT11_BSS)
    {
        nmhGetFsApGroupEnabledStatus (&i4ApGroupEnableStatus);
        if ((UINT4) i4ApGroupEnableStatus == APGROUP_ENABLE)
        {
            return (MSR_SKIP);
        }
    }
#endif
    nmhGetIfMainType (pInstance->pu4_OidList[1], &i4RetValIfMainType);
#ifdef WTP_WANTED
    if (i4RetValIfMainType == 3)
    {
        return (MSR_SKIP);
    }
#endif

    if ((pInstance->pu4_OidList[0] == 2) && (pOid->u4_Length == 11))
    {
        if ((i4RetValIfMainType == CFA_PPP) || (i4RetValIfMainType == CFA_HDLC))
        {
            /* if type is ppp then save */
            return MSR_SAVE;
        }
    }
    if ((pInstance->pu4_OidList[0] == 3) && (pOid->u4_Length == 11))
    {
        /* Save MTU Value in case of PPP */
        if (i4RetValIfMainType == CFA_PPP)
        {
            return MSR_SAVE;
        }
    }

    return MSR_SKIP;
}

INT4
MsrValidateIfPppEntry (tSNMP_OID_TYPE * pOid,
                       tSNMP_OID_TYPE * pInstance,
                       tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    INT4                i4RetValIfMainType = 0;
#ifdef WLC_WANTED
    INT4                i4ApGroupEnableStatus = APGROUP_DISABLE;
    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainType);
    if (i4RetValIfMainType == CFA_CAPWAP_DOT11_BSS)
    {
        nmhGetFsApGroupEnabledStatus (&i4ApGroupEnableStatus);
        if ((UINT4) i4ApGroupEnableStatus == APGROUP_ENABLE)
        {
            return (MSR_SKIP);
        }
    }
#endif
    if (pOid->u4_Length != 11)
    {
        return MSR_SKIP;
    }
    nmhGetIfMainType (pInstance->pu4_OidList[1], &i4RetValIfMainType);
#ifdef WTP_WANTED
    if (i4RetValIfMainType == 3)
    {
        return (MSR_SKIP);
    }
#endif
    /* Save the row satus of PPP interfaces. */
    if (pInstance->pu4_OidList[0] == 8)
    {
        if ((i4RetValIfMainType == CFA_PPP) || (i4RetValIfMainType == CFA_HDLC))
        {
            return (MSR_SAVE);
        }
    }

    return MSR_SKIP;
}

INT4
MsrValidateIfIpPppEntry (tSNMP_OID_TYPE * pOid,
                         tSNMP_OID_TYPE * pInstance,
                         tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetVal = 0;
    UINT1               u1RetVal = 0;
    tCfaIfInfo          CfaIfInfo;

    UNUSED_PARAM (pOid);
#ifdef WLC_WANTED
    INT4                i4ApGroupEnableStatus = APGROUP_DISABLE;
    INT4                i4RetValIfMainType = 0;
    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainType);
    if (i4RetValIfMainType == CFA_CAPWAP_DOT11_BSS)
    {
        nmhGetFsApGroupEnabledStatus (&i4ApGroupEnableStatus);
        if ((UINT4) i4ApGroupEnableStatus == APGROUP_ENABLE)
        {
            return (MSR_SKIP);
        }
    }
#endif

    i4RetVal = CfaGetIfInfo ((UINT4) pInstance->pu4_OidList[1], &CfaIfInfo);

    /* If the interface is not PPP, then no need to save the
       ifIpAddr, ifIpSubnetMask & ifIpBroadcastAddr */
    if ((i4RetVal != CFA_FAILURE) && (CfaIfInfo.u1IfType != CFA_PPP))
    {
        return (MSR_SKIP);
    }

    if (CfaIfGetIpAllocMethod ((UINT4) pInstance->pu4_OidList[1],
                               &u1RetVal) == CFA_SUCCESS)
    {
        /* 1. When the address allocation method is set 
         *    to manual and the object is ifIpAddr or ifIpSubnetMask 
         *    or ifIpBroadcastAddr, it will be stored. */
        if ((u1RetVal == CFA_IP_ALLOC_MAN) && (pData->i2_DataType == IPADDRESS))
        {
            return (MSR_SAVE);
        }
    }
    return (MSR_SKIP);

}
#endif

#ifdef TLM_WANTED
/************************************************************************
*  Function Name   : MsrValidateSkipTeLinkEntry
*  Description     : This function validates the teLinkEntry table
*  Input           : pOid - pointer to the OID 
*                    pInstance - pointer to the instance OID 
*                    pData - pointer to the data to be saved
*  Output          : None
*  Returns         : MSR_SAVE if the object should be saved
*                    MSR_SKIP if just the current instance of the object
*                             should not be saved   
*                    MSR_NEXT if the current scalar/table should not be
*                             saved 
************************************************************************/
INT4
MsrValidateSkipTeLinkEntry (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4StorageType = 0;
    UINT1               u1OffSet = 0;

    /* For teLinkTable, first object that needs to be saved is
     * teLinkRowStatus with value CREATE_AND_WAIT. When teLinkRowStatus 
     * with value CREATE_AND_WAIT needs to be saved, this routine is called
     * with pOid containing OID of teLinkRowStatus, pInstance containing
     * current index, pData containing value 5. For all other objects
     * and teLinkRowStatus as ACTIVE, this routine is called with pOid 
     * containing OID of teLinkEntry, pInstance containing OID of 
     * particular object and current index, pData containing value. 
     *
     * The below logic saves the below objects in the order, 
     * 1. teLinkRowStatus with value CREATE_AND_WAIT
     * 2. other objects of this table teLinkTable
     *
     * and skips teLinkRowStatus with value ACTIVE and teLinkStorageType
     * as VOLATILE
     */

    if (pOid->u4_Length != 12)
    {
        u1OffSet = 1;
    }

    nmhGetTeLinkStorageType (pInstance->pu4_OidList[u1OffSet], &i4StorageType);

    if (i4StorageType == 2)
    {
        return (MSR_SKIP);
    }

    if ((pOid->u4_Length != 12) && (pInstance->pu4_OidList[0] == 11))
    {
        /* If Row Status = ACTIVE , then Skip , else Save */
        if (pData->i4_SLongValue == ACTIVE)
        {
            return (MSR_SKIP);
        }
    }

    return (MSR_SAVE);
}

/************************************************************************
*  Function Name   : MsrValidateTeLinkEntryDoActive
*  Description     : This function validates the teLinkEntry table
*  Input           : pOid - pointer to the OID 
*                    pInstance - pointer to the instance OID 
*                    pData - pointer to the data to be saved
*  Output          : None
*  Returns         : MSR_SAVE if the object should be saved
*                    MSR_SKIP if just the current instance of the object
*                             should not be saved   
*                    MSR_NEXT if the current scalar/table should not be
*                             saved 
************************************************************************/
INT4
MsrValidateTeLinkEntryDoActive (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4StorageType = 0;
    UINT1               u1OffSet = 0;

    /* For teLinkTable, first object that needs to be saved is
     * teLinkRowStatus with value CREATE_AND_WAIT. When teLinkRowStatus 
     * with value CREATE_AND_WAIT needs to be saved, this routine is called
     * with pOid containing OID of teLinkRowStatus, pInstance containing
     * current index, pData containing value 5. For all other objects
     * and teLinkRowStatus as ACTIVE, this routine is called with pOid 
     * containing OID of teLinkEntry, pInstance containing OID of 
     * particular object and current index, pData containing value. 
     *
     * The below logic skips the below objects in the order,
     * 1. All the objects with storage type as VOLATILE
     * 2. teLinkRowStatus with value CREATE_AND_WAIT
     * 3. other objects of this table teLinkTable
     *
     * and saves teLinkRowStatus with value ACTIVE alone
     */

    if (pOid->u4_Length != 12)
    {
        u1OffSet = 1;
    }

    nmhGetTeLinkStorageType (pInstance->pu4_OidList[u1OffSet], &i4StorageType);

    if (i4StorageType == 2)
    {
        return (MSR_SKIP);
    }

    if ((pOid->u4_Length != 12) && (pInstance->pu4_OidList[0] == 11))
    {
        /* If Row Status = ACTIVE , then Save , else Skip */
        if (pData->i4_SLongValue == ACTIVE)
        {
            return (MSR_SAVE);
        }
    }

    return (MSR_SKIP);
}

/************************************************************************
*  Function Name   : MsrValidateSkipComponentLinkEntry
*  Description     : This function validates the ComponentLinkEntry table
*  Input           : pOid - pointer to the OID 
*                    pInstance - pointer to the instance OID 
*                    pData - pointer to the data to be saved
*  Output          : None
*  Returns         : MSR_SAVE if the object should be saved
*                    MSR_SKIP if just the current instance of the object
*                             should not be saved   
*                    MSR_NEXT if the current scalar/table should not be
*                             saved 
************************************************************************/
INT4
MsrValidateSkipComponentLinkEntry (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4StorageType = 0;
    UINT1               u1OffSet = 0;

    /* For componentLinkTable, first object that needs to be saved is
     * componentLinkRowStatus with value CREATE_AND_WAIT. When 
     * componentLinkRowStatus with value CREATE_AND_WAIT needs to be saved,
     * this routine is called with pOid containing OID of 
     * componentLinkRowStatus, pInstance containing current index, 
     * pData containing value 5. For all other objects
     * and componentLinkRowStatus as ACTIVE, this routine is called with pOid 
     * containing OID of componentLinkEntry, pInstance containing OID of 
     * particular object and current index, pData containing value. 
     *
     * The below logic saves the below objects in the order,
     * 1. componentLinkRowStatus with value CREATE_AND_WAIT
     * 2. other objects of this table componentLinkTable
     *
     * and skips componentLinkRowStatus with value ACTIVE alone and
     * entries with storage type as VOLATILE
     */

    if (pOid->u4_Length != 12)
    {
        u1OffSet = 1;
    }

    nmhGetComponentLinkStorageType (pInstance->pu4_OidList[u1OffSet],
                                    &i4StorageType);
    if (i4StorageType == 2)
    {
        return (MSR_SKIP);
    }

    if ((pOid->u4_Length != 12) && (pInstance->pu4_OidList[0] == 4))
    {
        /* If Row Status = ACTIVE , then Skip , else Save */
        if (pData->i4_SLongValue == ACTIVE)
        {
            return (MSR_SKIP);
        }
    }

    return (MSR_SAVE);
}

/************************************************************************
*  Function Name   : MsrValidateComponentLinkEntryDoActive
*  Description     : This function validates the ComponentLinkEntry table
*  Input           : pOid - pointer to the OID 
*                    pInstance - pointer to the instance OID 
*                    pData - pointer to the data to be saved
*  Output          : None
*  Returns         : MSR_SAVE if the object should be saved
*                    MSR_SKIP if just the current instance of the object
*                             should not be saved   
*                    MSR_NEXT if the current scalar/table should not be
*                             saved 
************************************************************************/
INT4
MsrValidateComponentLinkEntryDoActive (tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4StorageType = 0;
    UINT1               u1OffSet = 0;

    /* For componentLinkTable, first object that needs to be saved is
     * componentLinkRowStatus with value CREATE_AND_WAIT. When 
     * componentLinkRowStatus with value CREATE_AND_WAIT needs to be saved,
     * this routine is called with pOid containing OID of 
     * componentLinkRowStatus, pInstance containing current index, 
     * pData containing value 5. For all other objects
     * and componentLinkRowStatus as ACTIVE, this routine is called with pOid 
     * containing OID of componentLinkEntry, pInstance containing OID of 
     * particular object and current index, pData containing value. 
     *
     * The below logic skips the below objects in the order,
     * 1. All the entries with storage type as VOLATILE
     * 2. componentLinkRowStatus with value CREATE_AND_WAIT
     * 3. other objects of this table componentLinkTable
     *
     * and saves componentLinkRowStatus with value ACTIVE alone
     */

    if (pOid->u4_Length != 12)
    {
        u1OffSet = 1;
    }

    nmhGetComponentLinkStorageType (pInstance->pu4_OidList[u1OffSet],
                                    &i4StorageType);
    if (i4StorageType == 2)
    {
        return (MSR_SKIP);
    }

    if ((pOid->u4_Length != 12) && (pInstance->pu4_OidList[0] == 4))
    {
        /* If Row Status = ACTIVE , then Save , else Skip */
        if (pData->i4_SLongValue == ACTIVE)
        {
            return (MSR_SAVE);
        }
    }

    return (MSR_SKIP);
}

/************************************************************************
*  Function Name   : MsrValidateSkipTeLinkDescriptorEntry
*  Description     : This function validates the TeLinkDescriptor
*                    Entry table
*  Input           : pOid - pointer to the OID 
*                    pInstance - pointer to the instance OID 
*                    pData - pointer to the data to be saved
*  Output          : None
*  Returns         : MSR_SAVE if the object should be saved
*                    MSR_SKIP if just the current instance of the object
*                             should not be saved   
*                    MSR_NEXT if the current scalar/table should not be
*                             saved 
************************************************************************/
INT4
MsrValidateSkipTeLinkDescriptorEntry (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4StorageType = 0;
    UINT1               u1OffSet = 0;

    UNUSED_PARAM (pData);

    /* For teLinkDescriptorTable, first object that needs to be saved is
     * teLinkDescrRowStatus with value CREATE_AND_WAIT. When 
     * teLinkDescrRowStatus with value CREATE_AND_WAIT needs to be saved, this
     * routine is called with pOid containing OID of teLinkDescrRowStatus,
     * pInstance containing current index, pData containing value 5. For all
     * other objects and teLinkDescrRowStatus as ACTIVE, this routine is called
     * with pOid containing OID of teLinkDescriptorEntry, pInstance containing
     * OID of particular object and current index, pData containing value. 
     *
     * The below logic saves the below objects in the order,
     * 1. teLinkDescrRowStatus with value CREATE_AND_WAIT
     * 2. teLinkDescrRowStatus with value ACTIVE
     *
     * and skips all other objects and entries with storage type as VOLATILE
     */

    if (pOid->u4_Length != 12)
    {
        u1OffSet = 1;
    }

    nmhGetTeLinkDescrStorageType (pInstance->pu4_OidList[u1OffSet],
                                  pInstance->pu4_OidList[u1OffSet + 1],
                                  &i4StorageType);
    if (i4StorageType == 2)
    {
        return (MSR_SKIP);
    }

    if ((pOid->u4_Length == 12) || (pInstance->pu4_OidList[0] == 15))
    {
        return (MSR_SAVE);
    }

    return (MSR_SKIP);
}

/************************************************************************
*  Function Name   : MsrValidateSkipTeLinkSrlgEntry
*  Description     : This function validates the TeLinkSrlgEntry
*                    Entry table
*  Input           : pOid - pointer to the OID
*                    pInstance - pointer to the instance OID
*                    pData - pointer to the data to be saved
*  Output          : None
*  Returns         : MSR_SAVE if the object should be saved
*                    MSR_SKIP if just the current instance of the object
*                             should not be saved
*                    MSR_NEXT if the current scalar/table should not be
*                             saved
************************************************************************/
INT4
MsrValidateSkipTeLinkSrlgEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4StorageType = 0;
    UINT1               u1OffSet = 0;

    UNUSED_PARAM (pData);

    /* For teLinkSrlgTable, all objects needs to be restored only when the 
     * storage type is NON_VOLATILE.As the entries will be created dynamically
     * and are not created by the administrator.For all objects,this routine 
     * is called with pOid containing OID of teLinkSrlgEntry , pInstance
     * containing OID of particular object and current index, pData 
     * containing value.
     * The below logic saves all objects in the table only when storage type is
     * NON_VOLATILE and skips when its VOLATILE.
     */

    if (pOid->u4_Length != 12)
    {
        u1OffSet = 1;
    }

    nmhGetTeLinkSrlgStorageType (pInstance->pu4_OidList[u1OffSet],
                                 pInstance->pu4_OidList[u1OffSet + 1],
                                 &i4StorageType);
    if (i4StorageType == 2)
    {
        return (MSR_SKIP);
    }

    return (MSR_SAVE);
}

/************************************************************************
*  Function Name   : MsrValidateSkipTeLinkBandwidthEntry
*  Description     : This function validates the TeLinkSrlgEntry
*                    Entry table
*  Input           : pOid - pointer to the OID
*                    pInstance - pointer to the instance OID
*                    pData - pointer to the data to be saved
*  Output          : None
*  Returns         : MSR_SAVE if the object should be saved
*                    MSR_SKIP if just the current instance of the object
*                             should not be saved
*                    MSR_NEXT if the current scalar/table should not be
*                             saved
************************************************************************/
INT4
MsrValidateSkipTeLinkBandwidthEntry (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4StorageType = 0;
    UINT1               u1OffSet = 0;

    UNUSED_PARAM (pData);

    /* For teLinkSrlgTable, all objects needs to be restored only when the
     * storage type is NON_VOLATILE.As the entries will be created dynamically
     * and are not created by the administrator.For all objects,this routine
     * is called with pOid containing OID of teLinkSrlgEntry , pInstance
     * containing OID of particular object and current index, pData
     * containing value.
     * The below logic saves all objects in the table only when storage type is
     * NON_VOLATILE and skips when its VOLATILE.
     */

    if (pOid->u4_Length != 12)
    {
        u1OffSet = 1;
    }

    nmhGetTeLinkBandwidthStorageType (pInstance->pu4_OidList[u1OffSet],
                                      pInstance->pu4_OidList[u1OffSet + 1],
                                      &i4StorageType);

    if (i4StorageType == 2)
    {
        return (MSR_SKIP);
    }

    return (MSR_SAVE);
}

/************************************************************************
*  Function Name   : MsrValidateSkipCompLinkDescriptorEntry
*  Description     : This function validates the ComponentLinkDescriptor
*                    Entry table
*  Input           : pOid - pointer to the OID 
*                    pInstance - pointer to the instance OID 
*                    pData - pointer to the data to be saved
*  Output          : None
*  Returns         : MSR_SAVE if the object should be saved
*                    MSR_SKIP if just the current instance of the object
*                             should not be saved   
*                    MSR_NEXT if the current scalar/table should not be
*                             saved 
************************************************************************/
INT4
MsrValidateSkipCompLinkDescriptorEntry (tSNMP_OID_TYPE * pOid,
                                        tSNMP_OID_TYPE * pInstance,
                                        tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4StorageType = 0;
    UINT1               u1OffSet = 0;

    UNUSED_PARAM (pData);

    /* For componentLinkDescriptorTable, first object that needs to be saved is
     * componentLinkDescrRowStatus with value CREATE_AND_WAIT. When 
     * componentLinkDescrRowStatus with value CREATE_AND_WAIT needs to be saved,
     * this routine is called with pOid containing OID of 
     * componentLinkDescrRowStatus, pInstance containing current index, pData
     * containing value 5. For all other objects and componentLinkDescrRowStatus
     * as ACTIVE, this routine is called with pOid containing OID of
     * componentLinkDescriptorEntry, pInstance containing
     * OID of particular object and current index, pData containing value. 
     *
     * The below logic saves the below objects in the order,
     * 1. componentLinkDescrRowStatus with value CREATE_AND_WAIT
     * 2. componentLinkDescrRowStatus with value ACTIVE
     *
     * and skips all other objects and entries with storage type as VOLATILE
     */

    if (pOid->u4_Length != 12)
    {
        u1OffSet = 1;
    }

    nmhGetComponentLinkDescrStorageType (pInstance->pu4_OidList[u1OffSet],
                                         pInstance->pu4_OidList[u1OffSet + 1],
                                         &i4StorageType);
    if (i4StorageType == 2)
    {
        return (MSR_SKIP);
    }

    if ((pOid->u4_Length != 12) &&
        (pInstance->pu4_OidList[0] >= 5) && (pInstance->pu4_OidList[0] <= 12))
    {
        return (MSR_SKIP);
    }

    return (MSR_SAVE);
}

/************************************************************************
*  Function Name   : MsrValidateSkipCompLinkBandwidthEntry
*  Description     : This function validates the CompLinkBandwidthEntry 
*                    Entry table
*  Input           : pOid - pointer to the OID
*                    pInstance - pointer to the instance OID
*                    pData - pointer to the data to be saved
*  Output          : None
*  Returns         : MSR_SAVE if the object should be saved
*                    MSR_SKIP if just the current instance of the object
*                             should not be saved
*                    MSR_NEXT if the current scalar/table should not be
*                             saved
************************************************************************/
INT4
MsrValidateSkipComponentLinkBandwidthEntry (tSNMP_OID_TYPE * pOid,
                                            tSNMP_OID_TYPE * pInstance,
                                            tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4StorageType = 0;
    UINT1               u1OffSet = 0;

    UNUSED_PARAM (pData);

    /* For componenLinkBandwidthEntry , all objects needs to be restored only when the
     * storage type is NON_VOLATILE.As the entries will be created dynamically
     * and are not created by the administrator.For all objects,this routine
     * is called with pOid containing OID of componenLinkBandwidthEntry, pInstance
     * containing OID of particular object and current index, pData
     * containing value.
     * The below logic saves all objects in the table only when storage type is
     * NON_VOLATILE and skips when its VOLATILE.
     */

    if (pOid->u4_Length != 12)
    {
        u1OffSet = 1;
    }

    nmhGetComponentLinkBandwidthStorageType (pInstance->pu4_OidList[u1OffSet],
                                             pInstance->pu4_OidList[u1OffSet +
                                                                    1],
                                             &i4StorageType);
    if (i4StorageType == 2)
    {
        return (MSR_SKIP);
    }

    return (MSR_SAVE);
}
#endif
/************************************************************************
 *  Function Name   : MsrValidateAcIfMainEntry
 *  Description     : This function validates the ifMainEnty table for AC only
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 ************************************************************************/

INT4
MsrValidateACIfMainEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4RetValIfMainType = CFA_INVALID_TYPE;
    INT4                i4RetValIfMainSubType = CFA_INVALID_TYPE;
    UINT1               u1OffSet = 0;

    UNUSED_PARAM (pData);
#ifdef WLC_WANTED
    INT4                i4ApGroupEnableStatus = APGROUP_DISABLE;
    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainType);
    if (i4RetValIfMainType == CFA_CAPWAP_DOT11_BSS)
    {
        nmhGetFsApGroupEnabledStatus (&i4ApGroupEnableStatus);
        if ((UINT4) i4ApGroupEnableStatus == APGROUP_ENABLE)
        {
            return (MSR_SKIP);
        }
    }
#endif
    /*For the ifMainRowStatus CREATE_AND_WAIT case alone we get a pOid->u4_Length as 12 */
    if (pOid->u4_Length != 12)
    {
        u1OffSet = 1;
    }
    if (pOid->u4_Length == 12)
    {
        return MSR_SKIP;
    }

    if (nmhGetIfMainType ((INT4) pInstance->pu4_OidList[u1OffSet],
                          &i4RetValIfMainType) == SNMP_FAILURE)
    {
        return (MSR_SKIP);
    }
#ifdef WTP_WANTED
    if (i4RetValIfMainType == 3)
    {
        return (MSR_SKIP);
    }
#endif

    if (nmhGetIfMainSubType ((INT4) pInstance->pu4_OidList[u1OffSet],
                             &i4RetValIfMainSubType) == SNMP_FAILURE)
    {
        return (MSR_SKIP);
    }

    if (pOid->u4_Length != 12)
    {
        if (((i4RetValIfMainType == CFA_PROP_VIRTUAL_INTERFACE) &&
             (i4RetValIfMainSubType == CFA_SUBTYPE_AC_INTERFACE)) &&
            ((pInstance->pu4_OidList[0] == 5) ||
             (pInstance->pu4_OidList[0] == 8)))
        {
            return (MSR_SAVE);
        }
    }
    return (MSR_SKIP);
}

/************************************************************************
 *  Function Name   : MsrValidateACSkipSbpInterfaces 
 *  Description     : This function validates the ifMainEnty table for AC only
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 ************************************************************************/

INT4
MsrValidateACSkipSbpInterfaces (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4BrgPortType = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    nmhGetIfMainBrgPortType ((INT4) pInstance->pu4_OidList[1], &i4BrgPortType);

    /* The entries from ifACEntry will not be stored for S-Channel interface 
     * and hence the entries are skipped for S-Channel interfaces */

    if (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
    {
        return MSR_SKIP;
    }

    return (MSR_SAVE);
}

#ifdef MPLS_WANTED
/************************************************************************
 *  Function Name   : MsrValidateVplsConfigTableSkip
 *  Description     : This function skips (Entry created for VPWS PW,
 *                    Other objects of VplsConfigTable including RowStatus
 *                    object with value 1) and saves RowStatus Object of
 *                    VplsConfigTable with value 5 alone.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateVplsConfigTableSkip (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1StorageType = 0;
    UINT1               u1OffSet = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (pOid->u4_Length != 14)
    {
        u1OffSet = 1;
    }

    L2VpnApiGetVplsStorageType (pInstance->pu4_OidList[u1OffSet],
                                &u1StorageType);

    if (u1StorageType == 2)
    {
        return (MSR_SKIP);
    }

    /* For fsMplsVplsConfigTable, the Index is VplsInstanceIndex.
       Length of the entry oid is 13 and for Create and Wait this will be 13+1.
       Save the rowstatus if it is in Create and Wait. For rest the length will
       be 13, save those objects and skip if the object is rowstatus as there
       are more objets after rowstatus. */

    if (pOid->u4_Length != 14)
    {
        if (pInstance->pu4_OidList[0] == 8)
        {
            return MSR_SKIP;
        }
    }
    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValidateVplsConfigTableSave
 *  Description     : This function skips (Entry created for VPWS PW,
 *                    Value of RowStatus of VplsConfig as 1) and saves
 *                    other objects / cases of VplsConfigTable.
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateVplsConfigTableSave (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1StorageType = 0;
    UINT1               u1OffSet = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (pOid->u4_Length != 14)
    {
        u1OffSet = 1;
    }

    L2VpnApiGetVplsStorageType (pInstance->pu4_OidList[u1OffSet],
                                &u1StorageType);

    if (u1StorageType == 2)
    {
        return (MSR_SKIP);
    }

    /* For fsMplsVplsConfigTable, the Index is VplsInstanceIndex.
       Save Only the Row Status object instance, as all the Instances
       are saved already. */

    if (pOid->u4_Length != 14)
    {
        if (pInstance->pu4_OidList[0] == 8)
        {
            return MSR_SAVE;
        }
    }
    return MSR_SKIP;
}

/*******************************************************************************
 *  Function Name   : MsrValidateFsMplsBypassTunnelIfEntryDoCreateAndGo
 *  Description     : This routine  validates the FsMplsBypassTunnelTable. 
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 *******************************************************************************/
INT4
MsrValidateFsMplsBypassTunnelIfEntryDoCreateAndGo (tSNMP_OID_TYPE * pOid,
                                                   tSNMP_OID_TYPE * pInstance,
                                                   tSNMP_MULTI_DATA_TYPE *
                                                   pData)
{
    UNUSED_PARAM (pOid);

    /* Currently FsMplsBypassTunnelIfEntry does not support CREATE_AND_WAIT and ACTIVE, 
     * so while saving this table set the row status to CREATE_AND_GO, 
     * so that while restoring this table will not be
     * called with ACTIVE, here we have used CREATE_AND_GO */

    /* Total entries in FsMplsBypassTunnelTable. */
    if (pInstance->pu4_OidList[0] == 5)
    {
        if ((pData->i4_SLongValue == MSR_ACTIVE))
        {
            pData->i4_SLongValue = MSR_CREATE_AND_GO;
            return (MSR_SAVE);
        }
    }
    return (MSR_SKIP);
}
#endif

/************************************************************************
 *  Function Name   : MsrValidateMtuForceAgain
 *  Description     : This function validates the ifMainEnty table
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/

INT4
MsrValidateMtuForceAgain (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData)
{
#ifdef WTP_WANTED
    INT4                i4RetValIfMainType = CFA_INVALID_TYPE;
    INT4                u1OffSet = 0;
#endif

    UNUSED_PARAM (pData);
    UNUSED_PARAM (pOid);
#ifdef WLC_WANTED
    INT4                i4ApGroupEnableStatus = APGROUP_DISABLE;
    INT4                i4RetValIfMainType = 0;
    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainType);
    if (i4RetValIfMainType == CFA_CAPWAP_DOT11_BSS)
    {
        nmhGetFsApGroupEnabledStatus (&i4ApGroupEnableStatus);
        if ((UINT4) i4ApGroupEnableStatus == APGROUP_ENABLE)
        {
            return (MSR_SKIP);
        }
    }
#endif

#ifdef WTP_WANTED
    if (pOid->u4_Length != 12)
    {
        u1OffSet = 1;
    }
    if (pOid->u4_Length == 12)
    {
        return MSR_SKIP;
    }

    if (nmhGetIfMainType ((INT4) pInstance->pu4_OidList[u1OffSet],
                          &i4RetValIfMainType) == SNMP_FAILURE)
    {
        return (MSR_SKIP);
    }

    if (i4RetValIfMainType == 3)
    {
        return (MSR_SKIP);
    }
#endif

    if (pInstance->pu4_OidList[0] == 3)
    {
        return (MSR_SAVE);
    }
    return (MSR_SKIP);
}

#ifdef ROUTEMAP_WANTED
/************************************************************************
 *  Function Name   : MsrValidateRMapSkipRowStatus 
 *  Description     : This function skips the route map ROW status "fsRMapRowStatus"
 *                    if it is ACTIVE and saves all other values
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 ************************************************************************/
INT4
MsrValidateRMapSkipRowStatus (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pData);
    if (pOid->u4_Length == 11)
    {
        if (pInstance->pu4_OidList[0] == 4)
        {
            return (MSR_SKIP);
        }
    }
    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateRMapSaveRowStatus 
 *  Description     : This function saves the route map ROW status "fsRMapRowStatus"
 *                    if it is ACTIVE and skips all other values
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 ************************************************************************/
INT4
MsrValidateRMapSaveRowStatus (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pData);
    if (pOid->u4_Length == 11)
    {
        if (pInstance->pu4_OidList[0] == 4)
        {
            return (MSR_SAVE);
        }
    }
    return (MSR_SKIP);
}

#endif

#ifdef SYNCE_WANTED
/************************************************************************
 *  Function Name   : MsrValidateSynceQLValue
 *  Description     : This function saves the fsSynceIfQLValue vale set
 *                    by user if manual forced ocnfiguration is enabled on
 *                    the interface.Else the fsSynceIfQLValue is reset to
 *                    zero and saved to start-up config file.                    
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 ************************************************************************/
INT4
MsrValidateSynceQLValue (tSNMP_OID_TYPE * pOid,
                         tSNMP_OID_TYPE * pInstance,
                         tSNMP_MULTI_DATA_TYPE * pData)
{

    INT4                i4IfIsRxQlForced = 0;
    UNUSED_PARAM (pOid);

    if ((pInstance->pu4_OidList[0] == 5) && (pInstance->pu4_OidList[1] != 0))
    {
        nmhGetFsSynceIfIsRxQLForced (pInstance->pu4_OidList[1],
                                     &i4IfIsRxQlForced);

        if (i4IfIsRxQlForced != 1)
        {
            /*Reset QL value to zero  during MSR if fsSynceIfIsRxQLForced
             * is not set on the interface.
             */
            pData->i4_SLongValue = 0;
        }

    }

    return (MSR_SAVE);
}
#endif
/************************************************************************
 *  Function Name   : MsrValidateIfMainEntrySaveAdminDownStatus
 *  Description     : This function validates the ifMainEntry table. It
 *                    sets all entries in the ifMainEntry table to ADMIN
 *                    DOWN. The interfaces will be enabled only after
 *                    all other mibs have been restored.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateIfMainEntrySaveAdminDownStatus (tSNMP_OID_TYPE * pOid,
                                           tSNMP_OID_TYPE * pInstance,
                                           tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4IfType = 0;
    INT4                i4StorageType = 0;
    INT4                i4BrgPortType = 0;
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
#ifdef WLC_WANTED
    INT4                i4ApGroupEnableStatus = APGROUP_DISABLE;
    INT4                i4RetValIfMainType = 0;
    nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4RetValIfMainType);
    if (i4RetValIfMainType == CFA_CAPWAP_DOT11_BSS)
    {
        nmhGetFsApGroupEnabledStatus (&i4ApGroupEnableStatus);
        if ((UINT4) i4ApGroupEnableStatus == APGROUP_ENABLE)
        {
            return (MSR_SKIP);
        }
    }
#endif
    if (pInstance->pu4_OidList[0] == 4)
    {
#ifdef MPLS_WANTED
        if (MplsCheckIsAdminCreatedMplsTnlIf (pInstance->pu4_OidList[1])
            == MPLS_FAILURE)
        {
            return (MSR_SKIP);
        }
#endif

        nmhGetIfMainBrgPortType ((INT4) pInstance->pu4_OidList[1],
                                 &i4BrgPortType);

        /* ifAdminStatus is stored for S-Channels only after 
         * S-Channel configurations are stored. 
         * (i.e) only after storing the EvbCAPConfigEntry */

        if (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
        {
            return MSR_SKIP;
        }

        nmhGetIfMainType ((INT4) pInstance->pu4_OidList[1], &i4IfType);
#ifdef WTP_WANTED
        if (i4IfType == 3)
        {
            return (MSR_SKIP);
        }
#endif

        if ((i4IfType == CFA_MPLS))
        {
            CfaGetIfMainStorageType (pInstance->pu4_OidList[1], &i4StorageType);

            if (i4StorageType == CFA_STORAGE_TYPE_VOLATILE)
            {
                return (MSR_SKIP);
            }
        }

        pData->i4_SLongValue = CFA_IF_DOWN;
        return (MSR_SAVE);
    }
    return (MSR_SKIP);
}

#ifdef WLC_WANTED
#ifdef RFMGMT_WANTED
/************************************************************************
 *  Function Name   : MsrValidateFsRrmAPConfigEntry 
 *  Description     : This function skips (Channel related information
 *                    if the entry is created dynamically
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateFsRrmAPConfigEntry (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4ChannelMode = 0;
    INT4                i4DcaMode = 0;
    INT4                i4DcaSelection = 0;
    INT4                i4IfIndex = 0;
    INT4                i4RadioIfType = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (pInstance->pu4_OidList[0] == 9)
    {
        i4IfIndex = (INT4) pInstance->pu4_OidList[1];

        if (nmhGetFsDot11RadioType (i4IfIndex, &i4RadioIfType) == SNMP_SUCCESS)
        {
            /* Do Nothing */
        }
        nmhGetFsRrmDcaMode (i4RadioIfType, &i4DcaMode);
        nmhGetFsRrmDcaSelectionMode (i4RadioIfType, &i4DcaSelection);
        nmhGetFsRrmAPChannelMode (i4IfIndex, &i4ChannelMode);

        if (((i4DcaMode == RFMGMT_DCA_MODE_GLOBAL) &&
             (i4DcaSelection != RFMGMT_DCA_SELECTION_OFF)) ||
            ((i4DcaMode == RFMGMT_DCA_MODE_PER_AP) &&
             (i4ChannelMode != RFMGMT_PER_AP_DCA_MANUAL)))
        {
            return MSR_SKIP;
        }
    }
    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValidateFsRrmAPTpcConfigEntry 
 *  Description     : This function skips (Power Level related information
 *                    if the entry is created dynamically
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateFsRrmTpcConfigEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4PowerMode = 0;
    INT4                i4TpcMode = 0;
    INT4                i4TpcSelection = 0;
    INT4                i4IfIndex = 0;
    INT4                i4RadioIfType = 0;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    if (pInstance->pu4_OidList[0] == 10)
    {
        i4IfIndex = (INT4) pInstance->pu4_OidList[1];

        if (nmhGetFsDot11RadioType (i4IfIndex, &i4RadioIfType) == SNMP_SUCCESS)
        {
            /*Do Nothing */
        }
        nmhGetFsRrmTpcMode (i4RadioIfType, &i4TpcMode);
        nmhGetFsRrmTpcSelectionMode (i4RadioIfType, &i4TpcSelection);
        nmhGetFsRrmAPTpcMode (i4IfIndex, &i4PowerMode);

        if (((i4TpcMode == RFMGMT_TPC_MODE_GLOBAL) &&
             (i4TpcSelection != RFMGMT_TPC_SELECTION_OFF)) ||
            ((i4TpcMode == RFMGMT_TPC_MODE_PER_AP) &&
             (i4PowerMode != RFMGMT_PER_AP_TPC_MANUAL)))
        {
            return MSR_SKIP;
        }
    }
    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValidateApGroupBindingEntry 
 *  Description     : This function skips (Power Level related information
 *                    if the entry is created dynamically
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateApGroupBindingEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
#ifdef WLC_WANTED
    INT4                i4ApGroupEnableStatus = APGROUP_DISABLE;

    if (pInstance->pu4_OidList[0] == WLC_OFFSET_3)
    {
        nmhGetFsApGroupEnabledStatus (&i4ApGroupEnableStatus);
        if ((UINT4) i4ApGroupEnableStatus == APGROUP_ENABLE)
        {
            return MSR_SKIP;
        }
    }
#endif
    return MSR_SAVE;
}
#endif

#ifdef WSSUSER_WANTED
/************************************************************************
 *  Function Name   : MsrValWssUserSkipGroupDefaultGroup
 *  Description     : This function validates the User Group table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValWssUserSkipGroupDefaultGroup (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pData);

    /* In MSR while storing the objects in the table, the object's 
     * position in the table is filled in the pInstance. 
     * For User Group table the Index is Group Id.
     * In this routine we have to save all the object except the 
     * Default Group.*/

    if (((pOid->u4_Length == OID_LENGTH_THIRTEEN) &&
         (pInstance->pu4_OidList[0] == WSSUSER_DEFAULT_GROUP_ID)) ||
        ((pOid->u4_Length == OID_LENGTH_TWELVE) &&
         (pInstance->pu4_OidList[1] == WSSUSER_DEFAULT_GROUP_ID)))
    {
        return MSR_SKIP;
    }

    return MSR_SAVE;
}
#endif
#endif

#ifdef Y1564_WANTED
/************************************************************************
 *  Function Name   : MsrValY1564SlaEntry
 *  Description     : This function validates the Y1564 SLA table
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValY1564SlaEntry (tSNMP_OID_TYPE * pOid,
                     tSNMP_OID_TYPE * pInstance, tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    /* In MSR while storing the objects in the table, the object's
     * position in the table is filled in the pInstance.
     * For SLA table the Indices is SLA Id.
     * In this routine we have to save all the object except the
     * fsY1564SlaTestStatus. Look for the object's position
     * in the table(10) and skip the same.*/

    if (pInstance->pu4_OidList[0] == 10)
    {
        return MSR_SKIP;
    }
    return MSR_SAVE;

}

/************************************************************************
 *  Function Name   : MsrValY1564PerfEntry
 *  Description     : This function validates the Y1564 Performance Test 
 *                    table
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValY1564PerfEntry (tSNMP_OID_TYPE * pOid,
                      tSNMP_OID_TYPE * pInstance, tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    /* In MSR while storing the objects in the table, the object's
     * position in the table is filled in the pInstance.
     * For Performance test table the Indices is Perf Id.
     * In this routine we have to save all the object except the
     * fsY1564PerformanceTestStatus. Look for the object's position
     * in the table(4) and skip the same.*/

    if (pInstance->pu4_OidList[0] == 4)
    {
        return MSR_SKIP;
    }
    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrValY1564TrafProfEntry
 *  Description     : This function validates the Y1564 Traffic Profile
 *                    table
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValY1564TrafProfEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4ContextId = 0;
    UINT4               u4TrafProfId = 0;
    UINT1               u1EmixSelected = 0;

    u4ContextId = pInstance->pu4_OidList[1];
    u4TrafProfId = pInstance->pu4_OidList[2];

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* In MSR while storing the objects in the table, the object's
     * position in the table is filled in the pInstance.
     * For Traffic profile Table the indics is Traf Id.
     * In this routine we have to save all the object except the
     * fsY1564TrafProfOptEmixPktSize only if Emix Pattern is 
     * Y1564_SNMP_FALSE else save fsY1564TrafProfOptEmixPktSize
     * value also.
     * Look for the object's position
     * in the table(5) and skip the same.*/

    if (pInstance->pu4_OidList[0] == 5)
    {
        Y1564UtilGetEmixStatus (u4ContextId, u4TrafProfId, &u1EmixSelected);
        /*Y1564_SNMP_FALSE = 2
         * Y1564_SNMP_TRUE = 1 */
        if (u1EmixSelected != 1)
        {
            return MSR_SKIP;
        }
    }
    return MSR_SAVE;
}
#endif

#ifdef VXLAN_WANTED
/************************************************************************
*  Function Name   : MsrValidateSkipVxlanNveEntry
*  Description     : This function validates the VxlanNveEntry table
*  Input           : pOid - pointer to the OID 
*                    pInstance - pointer to the instance OID 
*                    pData - pointer to the data to be saved
*  Output          : None
*  Returns         : MSR_SAVE if the object should be saved
*                    MSR_SKIP if just the current instance of the object
*                             should not be saved   
*                    MSR_NEXT if the current scalar/table should not be
*                             saved 
************************************************************************/
INT4
MsrValidateSkipVxlanNveEntry (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT1               u1OffSet = 0;
    INT4                i4StorageType = 0;
    INT4                i4NveIfIndex = 0;
    UINT4               u4VniNumber = 0;
    UINT4               u4Count = 0;
    UINT1               u1MacStart = 0;
    UINT1               u1MacEnd = 0;
    tMacAddr            DestMacAddr;

    UNUSED_PARAM (pData);

    MSR_MEMSET (DestMacAddr, 0, sizeof (tMacAddr));

    if (pOid->u4_Length != 14)
    {
        u1OffSet = 1;
    }

    i4NveIfIndex = pInstance->pu4_OidList[u1OffSet];

    u4VniNumber = pInstance->pu4_OidList[u1OffSet + 1];

    u1MacStart = u1OffSet + 2;

    u1MacEnd = u1OffSet + 8;

    for (u4Count = u1MacStart; u4Count < u1MacEnd; u4Count++)
    {
        DestMacAddr[u4Count - u1MacStart]
            = (UINT1) (pInstance->pu4_OidList[u4Count]);
    }
    /* Dynamically created NVE entry shoud be skipped. Storage type for dynamic
     * entry is VOLATILE (2). Hence get the storage type object 
     * (fsVxlanNveStorageType)* and skip the entry, if the value is 2
     * */

    nmhGetFsVxlanNveStorageType (i4NveIfIndex, u4VniNumber, DestMacAddr,
                                 &i4StorageType);

    if (i4StorageType == 2)
    {
        return (MSR_SKIP);
    }

    return (MSR_SAVE);
}

/************************************************************************
 *  Function Name   : MsrValidateSkipVxlanStatistics
 *  Description     : This function validates the VLAN- VNI table
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateSkipVxlanStatistics (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* In MSR while storing the objects in the table, the object's
     * position in the table is filled in the pInstance.
     * For VLAN VNI mapping table we have to save all the object except the
     * statistics. Look for the object's position
     * in the table(3, 4, 5) and skip the same.*/

    if ((pInstance->pu4_OidList[0] == 3) || (pInstance->pu4_OidList[0] == 4) ||
        (pInstance->pu4_OidList[0] == 5))
    {
        return MSR_SKIP;
    }
    return MSR_SAVE;
}

#ifdef EVPN_VXLAN_WANTED

/************************************************************************
 *  Function Name   : MsrValidateSkipVxlanEvpnStatistics
 *  Description     : This function validates the VLAN- VNI table
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidateSkipVxlanEvpnStatistics (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* In MSR while storing the objects in the table, the object's
     * position in the table is filled in the pInstance.
     * For EVI table we have to save all the object except the
     * statistics. Look for the object's position
     * in the table(5, 6, 7) and skip the same.*/

    if ((pInstance->pu4_OidList[0] == 5) || (pInstance->pu4_OidList[0] == 6) ||
        (pInstance->pu4_OidList[0] == 7))
    {
        return MSR_SKIP;
    }
    return MSR_SAVE;
}
#endif
#endif
/************************************************************************
*  Function Name   : MsrValidateL2SkipRowStatus
*  Description     : This function validates the issExtL2FilterTable table
*  Input           : pOid - pointer to the OID 
*                    pInstance - pointer to the instance OID 
*                    pData - pointer to the data to be saved
*  Output          : None
*  Returns         : MSR_SAVE if the object should be saved
*                    MSR_SKIP if just the current instance of the object
*                             should not be saved   
*                    MSR_NEXT if the current scalar/table should not be
*                             saved 
************************************************************************/

INT4
MsrValidateL2SkipRowStatus (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* 12 corresponds to issExtL2FilterStatus. If the incoming oid
     * corresponds to the above object, then save it */
    /* All other entries  are skipped */
    if ((pInstance->pu4_OidList[0] == 12) && (pOid->u4_Length != 13))
    {
        /* Skiping issExtL2FilterStatus Object */
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}

/************************************************************************
*  Function Name   : MsrValidateL2SaveRowStatus
*  Description     : This function validates the issExtL2FilterTable table
*  Input           : pOid - pointer to the OID 
*                    pInstance - pointer to the instance OID 
*                    pData - pointer to the data to be saved
*  Output          : None
*  Returns         : MSR_SAVE if the object should be saved
*                    MSR_SKIP if just the current instance of the object
*                             should not be saved   
************************************************************************/
INT4
MsrValidateL2SaveRowStatus (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* 12 corresponds to issExtL2FilterStatus. If the incoming oid
     * corresponds to the above object, then save it */
    /* All other entries  are skipped */
    if ((pInstance->pu4_OidList[0] == 12) && (pOid->u4_Length != 13))
    {
        /* Saving issExtL2FilterStatus Object */
        return (MSR_SAVE);
    }
    return (MSR_SKIP);
}

/************************************************************************
*  Function Name   : MsrValidateL3SkipRowStatus
*  Description     : This function validates the issExtL3FilterTable table
*  Input           : pOid - pointer to the OID 
*                    pInstance - pointer to the instance OID 
*                    pData - pointer to the data to be saved
*  Output          : None
*  Returns         : MSR_SAVE if the object should be saved
*                    MSR_SKIP if just the current instance of the object
*                             should not be saved   
************************************************************************/
INT4
MsrValidateL3SkipRowStatus (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* 23 corresponds to issExtL3FilterStatus. If the incoming oid
     * corresponds to the above object, then save it */
    /* All other entries  are skipped */
    if ((pInstance->pu4_OidList[0] == 23) && (pOid->u4_Length != 13))
    {
        /* Skiping issExtL3FilterStatus Object */
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}

/************************************************************************
*  Function Name   : MsrValidateL3SaveRowStatus
*  Description     : This function validates the issExtL3FilterTable table
*  Input           : pOid - pointer to the OID 
*                    pInstance - pointer to the instance OID 
*                    pData - pointer to the data to be saved
*  Output          : None
*  Returns         : MSR_SAVE if the object should be saved
*                    MSR_SKIP if just the current instance of the object
*                             should not be saved   
************************************************************************/
INT4
MsrValidateL3SaveRowStatus (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* 23 corresponds to issExtL3FilterStatus. If the incoming oid
     * corresponds to the above object, then save it */
    /* All other entries  are skipped */
    if ((pInstance->pu4_OidList[0] == 23) && (pOid->u4_Length != 13))
    {
        /* Saving issExtL3FilterStatus Object */
        return (MSR_SAVE);
    }
    return (MSR_SKIP);
}

#ifdef EVB_WANTED
/************************************************************************
* Function Name   : MsrValidateEvbSystemEntry 
* Description     : This function validates the 
*                   UAP Config Table
* Input           : pOid - pointer to the OID
*                   pInstance - pointer to the instance OID
*                   pData - pointer to the data to be saved
* Output          : None
* Returns         : MSR_SAVE if the object should be saved
* MSR_SKIP if just the current instance of the object
* should not be saved
* MSR_NEXT if the current scalar/table should not be
* saved
*************************************************************************/
INT4
MsrValidateEvbSystemEntry (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pData);
    UNUSED_PARAM (pOid);

    /* Skipping the mib object - fsMIEvbSystemTraceLevel which is used
     * to set the debug command for EVB */
    if (pInstance->pu4_OidList[0] == 4)
    {
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}

/************************************************************************
* Function Name   : MsrValidateEvbUapConfigEntry
* Description     : This function validates the 
*                   UAP Config Table
* Input           : pOid - pointer to the OID
*                   pInstance - pointer to the instance OID
*                   pData - pointer to the data to be saved
* Output          : None
* Returns         : MSR_SAVE if the object should be saved
* MSR_SKIP if just the current instance of the object
* should not be saved
* MSR_NEXT if the current scalar/table should not be
* saved
*************************************************************************/
INT4
MsrValidateEvbUapConfigEntry (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4UapStorageType = 0;
    UINT1               u1OffSet = 0;

    UNUSED_PARAM (pData);

    /* For the ieee8021BridgeEvbUAPConfigRowStatus CREATE_AND_WAIT case (5)
     * alone we get a pOid->u4_Length as 13. For other objects, 
     * Length of the oid is 12.
     * So in MSR while storing the objects in the table, only for 
     * ieee8021BridgeEvbUAPConfigRowStatus position in the table is 
     * filled in the pInstance[0] and all other object's
     * position in the table is filled in the pInstance[1].*/

    if (pOid->u4_Length != 13)
    {
        u1OffSet = 1;
    }

    /* Skipping the ieee8021BridgeEvbUAPConfigRowStatus  
     *  for CREATE_AND_WAIT 
     *  pInstance->u4_Length  value remains 1 for CREATE_AND_WAIT */

    if (pInstance->u4_Length == 1)
    {
        return (MSR_SKIP);
    }

    if ((pInstance->u4_Length == 2) &&
        /*ieee8021BridgeEvbUAPConfigRowStatus */
        (pInstance->pu4_OidList[0] == 14))
    {
        return (MSR_SKIP);
    }

    VLAN_LOCK ();

    nmhGetIeee8021BridgeEvbUAPConfigStorageType (pInstance->
                                                 pu4_OidList[u1OffSet],
                                                 &i4UapStorageType);

    VLAN_UNLOCK ();

    /* Skip the entries with storage type as VOLATILE */
    if (i4UapStorageType == SNMP3_STORAGE_TYPE_VOLATILE)
    {
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}

/************************************************************************
* Function Name   : MsrValidateStdEvbSbpConfigEntry
* Description     : This function validates the S-Channel Interface table
*                   
* Input           : pOid - pointer to the OID
*                   pInstance - pointer to the instance OID
*                   pData - pointer to the data to be saved
* Output          : None
* Returns         : MSR_SAVE if the object should be saved
* MSR_SKIP if just the current instance of the object
* should not be saved
* MSR_NEXT if the current scalar/table should not be
* saved
*************************************************************************/
INT4
MsrValidateStdEvbSbpConfigEntry (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4UapStorageType = 0;
    UINT1               u1OffSet = 0;
    INT4                i4EvbUAPSchCdcpMode = 0;
    INT4                i4EvbUAPSchCdcpAdminEnable = 0;

    /* If ieee8021BridgeEvbCAPRowStatus == CREATE_AND_WAIT(5) case 
     * we get pOid->u4_Length as 12*/

    if (pOid->u4_Length == 13)
    {
        u1OffSet = 0;
    }
    else
    {
        u1OffSet = 1;
    }

    /* When ieee8021BridgeEvbCAPRowStatus == CREATE_AND_WAIT (5), 
     * pOid->u4_Length = 13 (OID length)
     * pData->i4_SLongValue = 5
     * pInstance->pu4_OidList[1] - SVID (default SVID == 1) 
     * Row status is not saved when ieee8021BridgeEvbCAPRowStatus == CREATE_AND_WAIT*/

    if ((pOid->u4_Length == 13) &&
        (pData->i4_SLongValue == 5) && (pInstance->pu4_OidList[1] == 1))
    {
        return (MSR_SKIP);
    }

    /* When ieee8021BridgeEvbCAPRowStatus == ACTIVE(1), pOid->u4_Length = 12
     * and pInstance->u4_Length == 3. 
     * pInstance->pu4_OidList[2] refers to SVID - secondary index
     * of CAP config table. For default SVID in a UAP port, 
     * the entries are skipped */

    if ((pOid->u4_Length == 12) &&
        (pInstance->u4_Length == 3) && (pInstance->pu4_OidList[2] == 1))
    {
        return (MSR_SKIP);
    }

    /* Skipping the ROW Status When Active 
     * In Active, pOid->u4_Length = 12, 
     * pInstance->u4_Length       = 3,
     * pInstance->pu4_OidList[0]  = ieee8021BridgeEvbCAPRowStatus (8) */

    if ((pOid->u4_Length == 12) &&
        (pInstance->u4_Length == 3) && (pInstance->pu4_OidList[0] == 8))
    {
        return (MSR_SKIP);
    }

    VLAN_LOCK ();
    /* CDCP Mode and Cdcp State is Fetched */
    nmhGetFsMIEvbUAPSchCdcpMode (pInstance->pu4_OidList[u1OffSet],
                                 &i4EvbUAPSchCdcpMode);

    nmhGetIeee8021BridgeEvbUAPSchCdcpAdminEnable (pInstance->
                                                  pu4_OidList[u1OffSet],
                                                  &i4EvbUAPSchCdcpAdminEnable);

    nmhGetIeee8021BridgeEvbUAPConfigStorageType (pInstance->
                                                 pu4_OidList[u1OffSet],
                                                 &i4UapStorageType);

    VLAN_UNLOCK ();

    /* To skip the entries when mode is dynamic and cdcp is enable */
    if (((pOid->u4_Length == 13) ||
         (pOid->u4_Length == 12)) &&
        (i4EvbUAPSchCdcpMode == 1) && (i4EvbUAPSchCdcpAdminEnable == 1))
    {
        return (MSR_SKIP);
    }

    /* Skip the entries with storage type as VOLATILE */
    if (i4UapStorageType == SNMP3_STORAGE_TYPE_VOLATILE)
    {
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}

/************************************************************************
* Function Name   : MsrValidateFsEvbSbpConfigEntry
* Description     : This function validates the S-Channel Interface table
*                   
* Input           : pOid - pointer to the OID
*                   pInstance - pointer to the instance OID
*                   pData - pointer to the data to be saved
* Output          : None
* Returns         : MSR_SAVE if the object should be saved
* MSR_SKIP if just the current instance of the object
* should not be saved
* MSR_NEXT if the current scalar/table should not be
* saved
*************************************************************************/
INT4
MsrValidateFsEvbSbpConfigEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4UapStorageType = 0;
    UINT1               u1OffSet = 1;
    INT4                i4EvbUAPSchCdcpMode = 0;
    INT4                i4EvbUAPSchCdcpAdminEnable = 0;

    UNUSED_PARAM (pData);

    /* Default SVID and SCID are not stored since they are created
     * when port is made up UAP 
     * pOid->u4_Length == 13 - Represents the Oid length when Active 
     * pInstance->u4_Length == 3 - Represents the Instance length 
     * pInstance->pu4_OidList[2] - Represents the second index 
     *                             of CAP config table.
     *                             default SVID - 1 */

    if ((pOid->u4_Length == 13) &&
        (pInstance->u4_Length == 3) && (pInstance->pu4_OidList[2] == 1))
    {
        return (MSR_SKIP);
    }

    VLAN_LOCK ();

    /* CDCP Mode and Cdcp State is Fetched */
    nmhGetFsMIEvbUAPSchCdcpMode (pInstance->pu4_OidList[1],
                                 &i4EvbUAPSchCdcpMode);

    nmhGetIeee8021BridgeEvbUAPSchCdcpAdminEnable (pInstance->pu4_OidList[1],
                                                  &i4EvbUAPSchCdcpAdminEnable);

    nmhGetIeee8021BridgeEvbUAPConfigStorageType (pInstance->
                                                 pu4_OidList[u1OffSet],
                                                 &i4UapStorageType);

    VLAN_UNLOCK ();

    /* To skip the entries when mode is dynamic and cdcp is enable */
    if ((pOid->u4_Length == 13) &&
        (i4EvbUAPSchCdcpMode == 1) && (i4EvbUAPSchCdcpAdminEnable == 1))
    {
        return (MSR_SKIP);
    }

    /* Skip the entries with storage type as VOLATILE */
    if (i4UapStorageType == SNMP3_STORAGE_TYPE_VOLATILE)
    {
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}

/************************************************************************
* Function Name   : MsrValidateEvbSbpSaveRowStatus
* Description     : This function validates the S-Channel Interface table
*
* Input           : pOid - pointer to the OID
*                   pInstance - pointer to the instance OID
*                   pData - pointer to the data to be saved
* Output          : None
* Returns         : MSR_SAVE if the object should be saved
* MSR_SKIP if just the current instance of the object
* should not be saved
* MSR_NEXT if the current scalar/table should not be
* saved
*************************************************************************/
INT4
MsrValidateEvbSbpSaveRowStatus (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData)
{
    INT4                i4UapStorageType = 0;
    INT4                i4EvbUAPSchCdcpMode = 0;
    INT4                i4EvbUAPSchCdcpAdminEnable = 0;
    UINT1               u1OffSet = 0;

    /* For ieee8021BridgeEvbCAPRowStatus CREATE_AND_WAIT case alone we get a
     * pOid->u4_Length as 13 */

    if (pOid->u4_Length == 13)
    {
        u1OffSet = 0;
    }
    else
    {
        u1OffSet = 1;
    }

    /* When ieee8021BridgeEvbCAPRowStatus == CREATE_AND_WAIT (5),
     * pOid->u4_Length = 13
     * pData->i4_SLongValue = 5
     * Row status is not saved when ieee8021BridgeEvbCAPRowStatus is
     * CREATE_AND_WAIT */

    if ((pOid->u4_Length == 13) && (pData->i4_SLongValue == 5))
    {
        return (MSR_SKIP);
    }

    /* When ieee8021BridgeEvbCAPRowStatus == ACTIVE, pOid->u4_Length = 12
     * and pInstance->u4_Length == 3 - Instance length 
     * pInstance->pu4_OidList[2] refers to SVID - secondary index
     * of CAP config table. For default SVID in a UAP port,
     * the entries are skipped */

    if ((pOid->u4_Length == 12) &&
        (pInstance->u4_Length == 3) && (pInstance->pu4_OidList[2] == 1))
    {
        return (MSR_SKIP);
    }

    /* All objects other than CAPRowStatus are skipped */

    /* Only in case of Ieee8021BridgeEvbCAPRowStatus ACTIVE pOid->u4_Length is 12 
     * and pInstance->u4_Length is 3 */

    if ((pOid->u4_Length == 12) && (pInstance->u4_Length == 3) && (pInstance->pu4_OidList[0] != 8))    /* Ieee8021BridgeEvbCAPRowStatus is 8 */
    {
        return (MSR_SKIP);
    }

    VLAN_LOCK ();

    /* CDCP Mode and Cdcp State is Fetched */
    nmhGetFsMIEvbUAPSchCdcpMode (pInstance->pu4_OidList[u1OffSet],
                                 &i4EvbUAPSchCdcpMode);

    nmhGetIeee8021BridgeEvbUAPSchCdcpAdminEnable (pInstance->
                                                  pu4_OidList[u1OffSet],
                                                  &i4EvbUAPSchCdcpAdminEnable);

    nmhGetIeee8021BridgeEvbUAPConfigStorageType (pInstance->
                                                 pu4_OidList[u1OffSet],
                                                 &i4UapStorageType);

    VLAN_UNLOCK ();

    /* To skip the entries when mode is dynamic and cdcp is enable */
    if (((pOid->u4_Length == 13) ||
         (pOid->u4_Length == 12)) &&
        (i4EvbUAPSchCdcpMode == 1) && (i4EvbUAPSchCdcpAdminEnable == 1))
    {
        return (MSR_SKIP);
    }

    /* Skip the entries with storage type as VOLATILE */
    if (i4UapStorageType == SNMP3_STORAGE_TYPE_VOLATILE)
    {
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}
#endif /* EVB_WANTED */

#ifdef FSB_WANTED
/************************************************************************
 *  Function Name   : MsrHandleFsbSkipCxtModuleStatus
 *  Description     : This function validates the FSB Context table
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrHandleFsbSkipCxtModuleStatus (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pData);

    /* In MSR while storing the objects in the table, the object's
     * position in the table is filled in the pInstance.
     * For FSB Context Table the Index is ContextId.
     * In this routine we have to save all the object except the
     * Module Status and Clear Stats. So we have to check for the
     * object's position in the table which is 3 for Module Status
     * and 9 for ClearStats.*/

    if (((pOid->u4_Length == OID_LENGTH_TWELVE) &&
         (pInstance->pu4_OidList[0] == 3)) ||
        ((pOid->u4_Length == OID_LENGTH_TWELVE) &&
         (pInstance->pu4_OidList[0] == 9)))
    {
        return MSR_SKIP;
    }
    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : MsrHandleFsbSaveCxtModuleStatus
 *  Description     : This function handles the saving of Module Status of
 *                    FSB Context table
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrHandleFsbSaveCxtModuleStatus (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pData);

    /* In MSR while storing the objects in the table, the object's
     * position in the table is filled in the pInstance.
     * For FSB Context Table  the Index is ContextId.
     * In this routine we have to skip all objects (since they were already saved)
     * except Module Status. So we have to check for the object's position
     * in the table(3).*/
    if (((pOid->u4_Length == OID_LENGTH_TWELVE) &&
         (pInstance->pu4_OidList[0] == 3)))
    {
        return MSR_SAVE;
    }
    return MSR_SKIP;
}

/************************************************************************
 *  Function Name   : MsrHandleFsbSaveIntfPortRole
 *  Description     : This function validates the FSB table
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrHandleFsbSaveIntfPortRole (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* In MSR while storing the objects in the table, the object's
     * position in the table is filled in the pInstance.
     * For FSB IntfTable the Index is VlanId and Interface Index.
     * In this routine we have to skip RowStatus and save only the
     * Port Role. So we have to check for the object's position
     * in the table(1).*/

    if ((pOid->u4_Length == OID_LENGTH_TWELVE) &&
        ((pInstance->pu4_OidList[0] == 3)))
    {
        return MSR_SAVE;
    }
    return MSR_SKIP;
}
#endif
#ifdef RFC2544_WANTED
/************************************************************************
 *  Function Name   : MsrValidate2544SkipSlaTestStatus 
 *  Description     : This routine  validates the Session Entry Table.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidate2544SkipSlaTestStatus (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* For SLA table the Indices are ContextId and SLAId.
     * In this routine we have to skip Test status.
     * So at first we have to check for the length of the Indices (3)
     * and then we have look for the object's position in the table(8).*/

    if (pInstance->u4_Length == 3)
    {
        if ((pInstance->pu4_OidList[0] == 7))
        {
            return MSR_SKIP;
        }
    }
    return MSR_SAVE;
}
#endif

/************************************************************************
 *  Function Name   : MsrValidateIfIvrEntry
 *  Description     : This function validates the IfIvrEntry  table
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/

INT4
MsrValidateIfIvrEntry (tSNMP_OID_TYPE * pOid,
                       tSNMP_OID_TYPE * pInstance,
                       tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);
    INT4                i4BrgPortType = 0;
    UINT1               u1OffSet = 1;

    /* The entries in ifIvrEntry will not be saved for S-Channel interfaces.
     * Hence ifIvrEntry is skipped for S-Channel interfaces */

    nmhGetIfMainBrgPortType ((INT4) pInstance->pu4_OidList[u1OffSet],
                             &i4BrgPortType);

    if (i4BrgPortType == CFA_STATION_FACING_BRIDGE_PORT)
    {
        return MSR_SKIP;
    }

    UNUSED_PARAM (pInstance);
    return MSR_SAVE;

}

#ifdef LLDP_WANTED
/************************************************************************
 * Function Name   : MsrValidateFsLldpConfiguredMgmtIpv4Address
 * Description     : This function validates the Mgmt IPv4 Address
 *
 * Input           : pOid - pointer to the OID
 *                   pInstance - pointer to the instance OID
 *                   pData - pointer to the data to be saved
 * Output          : None
 * Returns         : MSR_SAVE if the object should be saved
 * MSR_SKIP if just the current instance of the object
 * should not be saved
 * MSR_NEXT if the current scalar/table should not be
 * saved
 *************************************************************************/
INT4
MsrValidateFsLldpConfiguredMgmtIpv4Address (tSNMP_OID_TYPE * pOid,
                                            tSNMP_OID_TYPE * pInstance,
                                            tSNMP_MULTI_DATA_TYPE * pData)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4Ipv4Addr = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4Port = 0;
    tNetIpv4IfInfo      NetIpv4IfInfo;

    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pInstance);

    u4IfIndex = CfaGetDefaultRouterIfIndex ();
    MEMSET (&NetIpv4IfInfo, 0, sizeof (tNetIpv4IfInfo));

    /* Get the corresponding layer3 port number for the given layer2 ifindex */
    if (NetIpv4GetPortFromIfIndex (u4IfIndex, &u4Port) != NETIPV4_SUCCESS)
    {
        return (MSR_SAVE);
    }

    if (NetIpv4GetIfInfo (u4Port, &NetIpv4IfInfo) != NETIPV4_SUCCESS)
    {
        return (MSR_SAVE);
    }

    u4Ipv4Addr = NetIpv4IfInfo.u4Addr;

    MEMCPY (&u4IpAddr, pData->pOctetStrValue->pu1_OctetList,
            pData->pOctetStrValue->i4_Length);

    u4IpAddr = OSIX_HTONL (u4IpAddr);
    if (u4IpAddr == u4Ipv4Addr)
    {
        return (MSR_SKIP);
    }
    return (MSR_SAVE);
}
#endif

#ifdef RFC6374_WANTED

/************************************************************************
 *  Function Name   : MsrValidate6374ParamsConfigTable
 *  Description     : This routine  validates the Lm/DM ConfigParam Table
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved
 ************************************************************************/
INT4
MsrValidate6374ParamsConfigTable (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData)
{
    UNUSED_PARAM (pOid);
    UNUSED_PARAM (pData);

    /* Skip the below MIB Objects.
     * 1.fs6374LMTransmitStatus
     * 2.fs6374DMTransmitStatus 
     * 3.fs6374CmbLMDMTransmitStatus */

    if ((pInstance->pu4_OidList[0] == 7) ||
        (pInstance->pu4_OidList[0] == 13) || (pInstance->pu4_OidList[0] == 20))
    {
        return MSR_SKIP;
    }

    return MSR_SAVE;
}
#endif

#endif /* _MSR_VAL_C_ */
