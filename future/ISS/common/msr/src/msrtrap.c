/*****************************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: msrtrap.c,v 1.20 2014/02/14 13:56:35 siva Exp $
 *
 * Description: This file contains RM Trap related Functions
 *****************************************************************************/
#ifndef  MSR_TRAP_C
#define  MSR_TRAP_C
#include "msrsys.h"
#include "msr.h"
#include "fsiss.h"
#include "msrtrap.h"

static INT1         ai1TempBuffer[257];

#ifdef SNMP_2_WANTED
CONST CHR1         *gau1TrapEvent[] =
    { "OPEN-FAILED", "WRITE-FAILED", "SIZE-EXCEEDED", "SIZE-THRESHOLD-HIT" };
#endif

#ifdef SNMP_2_WANTED
/******************************************************************************
* Function :   MsrMakeObjIdFrmDotNew
*
* Description: This Function retuns the OID  of the given string for the 
*              proprietary MIB.
*
* Input    :   pi1TextStr - pointer to the string.
*
* Output   :   None.
*
* Returns  :   pOidPtr or NULL
*******************************************************************************/

tSNMP_OID_TYPE     *
MsrMakeObjIdFrmDotNew (INT1 *pi1TextStr)
{
    tSNMP_OID_TYPE     *pOidPtr;
    INT1               *pi1TempPtr, *pi1DotPtr;
    UINT2               u2Index;
    UINT2               u2DotCount;
    UINT4               u4BufferLen = 0;

    /* see if there is an alpha descriptor at begining */
    if (isalpha (*pi1TextStr))
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pi1TempPtr = pi1TextStr;

        for (u2Index = 0; ((pi1TempPtr < pi1DotPtr) && (u2Index < 256));
             u2Index++)
        {
            ai1TempBuffer[u2Index] = *pi1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';
        for (u2Index = 0;
             ((u2Index <
               (sizeof (fs_iss_mib_oid_table) / sizeof (struct MIB_OID)))
              && (fs_iss_mib_oid_table[u2Index].pName != NULL)); u2Index++)
        {
            if ((STRCMP
                 (fs_iss_mib_oid_table[u2Index].pName,
                  (INT1 *) ai1TempBuffer) == 0)
                && (STRLEN ((INT1 *) ai1TempBuffer) ==
                    STRLEN (fs_iss_mib_oid_table[u2Index].pName)))
            {
                STRNCPY ((INT1 *) ai1TempBuffer,
                         fs_iss_mib_oid_table[u2Index].pNumber,
                         (sizeof (ai1TempBuffer) - 1));
                ai1TempBuffer[(sizeof (ai1TempBuffer) - 1)] = '\0';
                break;
            }
        }
        if (u2Index < (sizeof (fs_iss_mib_oid_table) / sizeof (struct MIB_OID)))
        {
            if (fs_iss_mib_oid_table[u2Index].pName == NULL)
            {
                return (NULL);
            }
        }
        /* now concatenate the non-alpha part to the begining */
        u4BufferLen = sizeof (ai1TempBuffer) - STRLEN (ai1TempBuffer);
        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr,
                 (u4BufferLen <
                  STRLEN (pi1DotPtr) ? u4BufferLen : STRLEN (pi1DotPtr)));
    }
    else
    {                            /* is not alpha, so just copy into ai1TempBuffer */
        STRCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr);
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2DotCount = 0;
    for (u2Index = 0;
         ((u2Index < 256) && (ai1TempBuffer[u2Index] != '\0')); u2Index++)
    {
        if (ai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }
    pOidPtr = alloc_oid (SNMP_TRAP_OID_LEN);
    if (pOidPtr == NULL)
    {
        return (NULL);
    }

    pOidPtr->u4_Length = (UINT4) (u2DotCount + 1);

    /* now we convert number.number.... strings */
    pi1TempPtr = ai1TempBuffer;
    for (u2Index = 0; u2Index < u2DotCount + 1; u2Index++)
    {
        if ((pi1TempPtr = (INT1 *) MsrParseSubIdNew
             ((UINT1 *) pi1TempPtr, &(pOidPtr->pu4_OidList[u2Index]))) == NULL)
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }

        if (*pi1TempPtr == '.')
        {
            pi1TempPtr++;        /* to skip over dot */
        }
        else if (*pi1TempPtr != '\0')
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
}

/******************************************************************************
* Function :   MsrParseSubIdNew
* 
* Description : Parse the string format in number.number..format.
*
* Input       : pu1TempPtr - pointer to the string.
*               pu4Value    - Pointer the OID List value.
*               
* Output      : value of ppu1TempPtr
*
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*******************************************************************************/

UINT1              *
MsrParseSubIdNew (UINT1 *pu1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = 0;
    UINT1              *pu1Tmp = NULL;

    for (pu1Tmp = pu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                               ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                               ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        u4Value = (u4Value * 10) + (*pu1Tmp & 0xf);
    }

    if (pu1TempPtr == pu1Tmp)
    {
        pu1Tmp = NULL;
    }
    *pu4Value = u4Value;
    return pu1Tmp;
}
#endif /* SNMP_2_WANTED */

/******************************************************************************
 * Function Name      : MsrTrapSendNotifications 
 *
 * Description        : This is the function used by MSR to send 
 *                      notifications. MSR sends the notification 
 *                      directly to the management application through 
 *                      fault management. 
 *
 * Input(s)           : pNotifyInfo - Pointer to the tMsrNotificationMsg 
 *                      structure
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
VOID
MsrTrapSendNotifications (tMsrNotifyMsg * pNotifyMsg)
{
#ifdef SNMP_3_WANTED
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_OID_TYPE      MsrPrefixTrapOid;
    tSNMP_OID_TYPE      MsrSuffixTrapOid;
    tSNMP_OID_TYPE     *pMsrTrapOid = NULL;
    UINT4               au4MsrPrefixTrapOid[] = { 1, 3, 6, 1, 4, 1 };
    UINT4               au4MsrSuffixTrapOid[] = { 81, 0 };
    UINT4               au4SnmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    UINT1               au1Buf[SNMP_MAX_OID_LENGTH];
#endif /* SNMP_3_WANTED */
    UINT1               au1LogBuf[MSR_MAX_STR_LEN];
#ifdef FM_WANTED
    tFmFaultMsg         FmMsg;

    MEMSET (&FmMsg, 0, sizeof (tFmFaultMsg));
#endif /* FM_WANTED */
    MEMSET (au1LogBuf, 0, MSR_MAX_STR_LEN);

#ifdef SNMP_3_WANTED

    MsrPrefixTrapOid.pu4_OidList = au4MsrPrefixTrapOid;
    MsrPrefixTrapOid.u4_Length = sizeof (au4MsrPrefixTrapOid) / sizeof (UINT4);

    MsrSuffixTrapOid.pu4_OidList = au4MsrSuffixTrapOid;
    MsrSuffixTrapOid.u4_Length = sizeof (au4MsrSuffixTrapOid) / sizeof (UINT4);

    pMsrTrapOid = alloc_oid (SNMP_MAX_OID_LENGTH);
    if (pMsrTrapOid == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE,
                 "MsrTrapSendNotifications: OID Memory Allocation Failed\n");
        return;
    }
    MEMSET (pMsrTrapOid->pu4_OidList, 0,
            (sizeof (UINT4) * SNMP_MAX_OID_LENGTH));
    pMsrTrapOid->u4_Length = 0;

    if (SNMPAddEnterpriseOid (&MsrPrefixTrapOid, &MsrSuffixTrapOid,
                              pMsrTrapOid) == OSIX_FAILURE)
    {
        free_oid (pMsrTrapOid);
        MSR_TRC (MSR_TRACE_TYPE,
                 "MsrTrapSendNotifications: Adding EnterpriseOid failed \n");
        return;
    }

    MEMSET (au1Buf, 0, sizeof (SNMP_MAX_OID_LENGTH));
    pEnterpriseOid = alloc_oid (SNMP_MAX_OID_LENGTH);

    if (pEnterpriseOid == NULL)
    {
        free_oid (pMsrTrapOid);
        MSR_TRC (MSR_TRACE_TYPE,
                 "MsrTrapSendNotifications: OID Memory Allocation Failed\n");
        return;
    }

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    MEMCPY (pEnterpriseOid->pu4_OidList, pMsrTrapOid->pu4_OidList,
            pMsrTrapOid->u4_Length * sizeof (UINT4));
    pEnterpriseOid->u4_Length = pMsrTrapOid->u4_Length;
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] =
        MSR_CFG_RESTORE_TRAP;
    free_oid (pMsrTrapOid);

    pSnmpTrapOid = alloc_oid (sizeof (au4SnmpTrapOid) / sizeof (UINT4));
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    MEMCPY (pSnmpTrapOid->pu4_OidList, au4SnmpTrapOid,
            SNMP_TRAP_OID_LEN * sizeof (UINT4));
    pSnmpTrapOid->u4_Length = SNMP_TRAP_OID_LEN;

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0, 0, NULL,
                                                       pEnterpriseOid,
                                                       SnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    pStartVb = pVbList;

    SPRINTF ((CHR1 *) au1Buf, "%s", "issConfigRestoreStatus");

    pOid = MsrMakeObjIdFrmDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        MSR_TRC (MSR_TRACE_TYPE, "MsrTrapSendNotifications: OID Not Found\r\n");
        return;
    }

    pVbList->pNextVarBind = SNMP_AGT_FormVarBind
        (pOid, SNMP_DATA_TYPE_INTEGER, 0,
         (INT4) pNotifyMsg->u4ConfigRestoreStatus, NULL, NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        MSR_TRC (MSR_TRACE_TYPE,
                 "MsrTrapSendNotifications: Variable Binding Failed\r\n");
        return;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;
#endif /* SNMP_3_WANTED */

    if (pNotifyMsg->u4ConfigRestoreStatus == LAST_MIB_RESTORE_SUCCESSFUL)
    {
        SPRINTF ((CHR1 *) au1LogBuf, "%s",
                 "Configuration restored successfully.");
    }
    else
    {
        SPRINTF ((CHR1 *) au1LogBuf, "%s", "Configuration restoration failed.");
    }
#ifdef FM_WANTED
#ifdef SNMP_3_WANTED
    FmMsg.pTrapMsg = pStartVb;
#endif
    FmMsg.pSyslogMsg = au1LogBuf;
    FmMsg.u4ModuleId = FM_NOTIFY_MOD_ID_MSR;
    FmApiNotifyFaults (&FmMsg);
#endif /* FM_WANTED */
}

/******************************************************************************
 * Function Name      : AuditTrapSendNotifications 
 *
 * Description        : This function is used to send Audit notifications. 
 *                      During Audit when the following occurs
 *                      unable to open Audit file ,when any
 *                      write error occurs, size of the audit file
 *                      exceeds to  generate trap message .
 *
 * Input(s)           : pNotifyInfo - Pointer to the tAuditNotificationMsg 
 *                      structure
 *
 * Output(s)          : None
 *
 * Return Value(s)    : None
 *****************************************************************************/
VOID
AuditTrapSendNotifications (tAuditTrapMsg * pNotifyInfo)
{
#ifdef SNMP_3_WANTED
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_OID_TYPE      AuditPrefixTrapOid;
    tSNMP_OID_TYPE      AuditSuffixTrapOid;
    tSNMP_OID_TYPE     *pAuditTrapOid = NULL;
    UINT4               au4AuditPrefixTrapOid[] = { 1, 3, 6, 1, 4, 1 };
    UINT4               au4AuditSuffixTrapOid[] = { 81, 0 };
    UINT4               au4SnmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    UINT1               au1Buf[SNMP_MAX_OID_LENGTH];
#endif /* SNMP_3_WANTED */

    if (pNotifyInfo == NULL)
    {

        return;
    }

#ifdef SNMP_3_WANTED

    AuditPrefixTrapOid.pu4_OidList = au4AuditPrefixTrapOid;
    AuditPrefixTrapOid.u4_Length =
        sizeof (au4AuditPrefixTrapOid) / sizeof (UINT4);

    AuditSuffixTrapOid.pu4_OidList = au4AuditSuffixTrapOid;
    AuditSuffixTrapOid.u4_Length =
        sizeof (au4AuditSuffixTrapOid) / sizeof (UINT4);

    pAuditTrapOid = alloc_oid (SNMP_MAX_OID_LENGTH);
    if (pAuditTrapOid == NULL)
    {
        PRINTF ("AuditTrapSendNotifications: OID Memory Allocation Failed\n");
        return;
    }

    MEMSET (pAuditTrapOid->pu4_OidList, 0,
            (sizeof (UINT4) * SNMP_MAX_OID_LENGTH));
    pAuditTrapOid->u4_Length = 0;

    if (SNMPAddEnterpriseOid (&AuditPrefixTrapOid, &AuditSuffixTrapOid,
                              pAuditTrapOid) == OSIX_FAILURE)
    {
        free_oid (pAuditTrapOid);
        MSR_TRC (MSR_TRACE_TYPE,
                 "AuditTrapSendNotifications: Adding EnterpriseOid failed \n");
        return;
    }

    MEMSET (au1Buf, 0, sizeof (SNMP_MAX_OID_LENGTH));

    pEnterpriseOid = alloc_oid (SNMP_MAX_OID_LENGTH);

    if (pEnterpriseOid == NULL)
    {
        free_oid (pAuditTrapOid);
        PRINTF ("AuditTrapSendNotifications: OID Memory Allocation Failed\n");
        return;
    }

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    MEMCPY (pEnterpriseOid->pu4_OidList, pAuditTrapOid->pu4_OidList,
            pAuditTrapOid->u4_Length * sizeof (UINT4));
    pEnterpriseOid->u4_Length = pAuditTrapOid->u4_Length;
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = AUDIT_TRAP_EVENT;

    free_oid (pAuditTrapOid);
    pSnmpTrapOid = alloc_oid (sizeof (au4SnmpTrapOid) / sizeof (UINT4));
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        MSR_TRC (MSR_TRACE_TYPE,
                 "AuditTrapSendNotifications: OID Memory Allocation Failed\n");
        return;
    }
    MEMCPY (pSnmpTrapOid->pu4_OidList, au4SnmpTrapOid, sizeof (au4SnmpTrapOid));
    pSnmpTrapOid->u4_Length = sizeof (au4SnmpTrapOid) / sizeof (UINT4);

    pVbList = SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                    SNMP_DATA_TYPE_OBJECT_ID,
                                    0, 0, NULL, pEnterpriseOid, SnmpCnt64Type);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        MSR_TRC (MSR_TRACE_TYPE,
                 "AuditTrapSendNotifications: Variable Binding Failed\n");

        return;
    }

    pStartVb = pVbList;

    SPRINTF ((CHR1 *) au1Buf, "%s", "issAuditTrapFileName");

    pOid = MsrMakeObjIdFrmDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        MSR_TRC (MSR_TRACE_TYPE,
                 "AuditTrapSendNotifications: OID Not Found\r\n");

        return;
    }

    MEMSET (au1Buf, 0, SNMP_MAX_OID_LENGTH);
    STRNCPY (au1Buf, pNotifyInfo->au1IssLogFileName, (sizeof (au1Buf) - 1));
    au1Buf[(sizeof (au1Buf) - 1)] = '\0';
    pOstring =
        SNMP_AGT_FormOctetString ((UINT1 *) au1Buf, (INT4) STRLEN (au1Buf));
    if (pOstring == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        MSR_TRC (MSR_TRACE_TYPE,
                 "AuditTrapSendNotifications: Form Octet string Failed\n");

        return;
    }

    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_OCTET_PRIM, 0,
                                                  0, pOstring, NULL,
                                                  SnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        return;

    }

    pVbList = pVbList->pNextVarBind;
    SPRINTF ((CHR1 *) au1Buf, "%s", "issAuditTrapEvent");

    pOid = MsrMakeObjIdFrmDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        MSR_TRC (MSR_TRACE_TYPE,
                 "AuditTrapSendNotifications: OID Not Found\r\n");

        return;
    }

    MEMSET (au1Buf, 0, SNMP_MAX_OID_LENGTH);
    STRNCPY (au1Buf, gau1TrapEvent[(pNotifyInfo->u4Event) - 1],
             (sizeof (au1Buf) - 1));
    au1Buf[(sizeof (au1Buf) - 1)] = '\0';
    pOstring =
        SNMP_AGT_FormOctetString ((UINT1 *) au1Buf, (INT4) STRLEN (au1Buf));
    if (pOstring == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        MSR_TRC (MSR_TRACE_TYPE,
                 "AuditTrapSendNotifications: Form Octet string Failed\n");

        return;
    }

    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_OCTET_PRIM, 0,
                                                  0, pOstring, NULL,
                                                  SnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        MSR_TRC (MSR_TRACE_TYPE,
                 "AuditTrapSendNotifications: Variable Binding Failed\r\n");

        return;
    }

    pVbList = pVbList->pNextVarBind;
    SPRINTF ((CHR1 *) au1Buf, "%s", "issAuditTrapEventTime");

    pOid = MsrMakeObjIdFrmDotNew ((INT1 *) au1Buf);
    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        MSR_TRC (MSR_TRACE_TYPE,
                 "AuditTrapSendNotifications: OID Not Found\r\n");

        return;
    }

    pOstring = SNMP_AGT_FormOctetString ((UINT1 *) pNotifyInfo->ac1DateTime,
                                         (INT4) AUDIT_TRAP_TIME_STR_LEN);
    if (pOstring == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_FreeOid (pOid);
        MSR_TRC (MSR_TRACE_TYPE,
                 "AuditTrapSendNotifications: Form Octet string Failed\n");

        return;
    }

    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_OCTET_PRIM,
                                                  0, 0, pOstring,
                                                  NULL, SnmpCnt64Type);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        MSR_TRC (MSR_TRACE_TYPE,
                 "AuditTrapSendNotifications: Variable Binding Failed\r\n");

        return;
    }
    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;
    /* sending the trap message */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);

#endif

    return;
}

#endif /* MSR_TRAP_C */
