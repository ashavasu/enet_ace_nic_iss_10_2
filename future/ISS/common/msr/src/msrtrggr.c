/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: msrtrggr.c,v 1.7 2014/03/15 14:32:49 siva Exp $
*
* Description: MSR-RM interface APIs.
*********************************************************************/
#include "fssnmp.h"
#include "msr.h"
#include "rmgr.h"

#if (defined (L2RED_WANTED) && defined (CLI_WANTED))
extern INT4         gCliRmRole;
extern UINT1        gu1CliNoOfPeers;
#endif

#ifdef RM_WANTED
extern INT4         FsRmCopyPeerSyLogFileSet (tSnmpIndex *, tRetVal *);
#endif
/******************************************************************************
*      function Name        : MsrSetCmn                                       *
*      Role of the function : This fn call all the nmhset functions and sends *
*                             an update trigger in case object status is not  *
*                             deprecated                                       *    
*      Formal Parameters    : Objectid, Wrapperfn,  isDeprecated, isRowStatus *
*                             no_of_indices, fmt string, variable no args     *
*      Use of Recursion     : None                                            *
*      Return Value         : SUCCESS or FAILURE                              *
******************************************************************************/
INT1
MsrSetCmn (UINT4 au4ObjectID[], INT4 i4OidLen,
           INT4 (*Wrapperfn) (tSnmpIndex *, tSNMP_MULTI_DATA_TYPE *),
           INT4 (*LockPtr) (VOID),
           INT4 (*UnlockPtr) (VOID),
           UINT4 u4Deprecated,
           UINT4 u4RowStatus, INT4 i4Indices, CHR1 * fmt, ...)
{
    va_list             ap;
    INT1                i1RetVal = 0;

    va_start (ap, fmt);
    i1RetVal = MsrSetWithNotify (au4ObjectID, i4OidLen, Wrapperfn,
                                 LockPtr, UnlockPtr, u4Deprecated,
                                 u4RowStatus, i4Indices, fmt, ap);
    va_end (ap);
    return i1RetVal;
}

/******************************************************************************
*      function Name        : MsrSetCmnNew                                    *
*      Role of the function : This fn call sends an update trigger            *
*                             in case object status is not deprecated.        *
*                             This fn is used for the generated code were the *
*                             Set operation will happen and then the          *
*                             trigger called                                  *
*      Formal Parameters    : Objectid, isDeprecated, isRowStatus             *
*                             no_of_indices, ret_val status fmt string,       * 
*                             variable no args                                *
*      Use of Recursion     : None                                            *
*      Return Value         : SUCCESS or FAILURE                              *
******************************************************************************/
INT1
MsrSetCmnNew (UINT4 au4ObjectID[], INT4 i4OidLen,
              INT4 (*LockPtr) (VOID),
              INT4 (*UnlockPtr) (VOID),
              UINT4 u4Deprecated,
              UINT4 u4RowStatus, INT4 i4Indices, INT4 i4RetVal, CHR1 * fmt, ...)
{
    va_list             ap;
    INT1                i1RetVal = 0;

    va_start (ap, fmt);
    i1RetVal = MsrSetWithNotifyNew (au4ObjectID, i4OidLen, LockPtr, UnlockPtr,
                                    u4Deprecated, u4RowStatus, i4Indices,
                                    i4RetVal, fmt, ap);
    va_end (ap);
    return i1RetVal;
}

/******************************************************************************
*      function Name        : MsrSetCmnWithLock                               *
*      Role of the function : This fn call is used to release the protocol    *
*                             lock before calling MsrSetCmn and retake the    *
*                             lock after the MsrSet function. This function   *
*                             is primarily used for set routines for which    *
*                             the protocol lock is taken within the wrapper   *
*                             routines.                                       *
*      Formal Parameters    : Objectid, Wrapperfn,  isDeprecated, isRowStatus *
*                             no_of_indices, fmt string, variable no args     *
*      Use of Recursion     : None                                            *
*      Return Value         : SUCCESS or FAILURE                              *
******************************************************************************/
INT1
MsrSetCmnWithLock (UINT4 au4ObjectID[], INT4 i4OidLen,
                   INT4 (*Wrapperfn) (tSnmpIndex *, tSNMP_MULTI_DATA_TYPE *),
                   INT4 (*LockPtr) (VOID),
                   INT4 (*UnlockPtr) (VOID),
                   UINT4 u4Deprecated, UINT4 u4RowStatus,
                   INT4 i4Indices, CHR1 * fmt, ...)
{
    va_list             ap;
    INT1                i1RetVal = 0;

    va_start (ap, fmt);

    if (UnlockPtr != NULL)
    {
        UnlockPtr ();
    }

    i1RetVal = MsrSetWithNotify (au4ObjectID, i4OidLen, Wrapperfn, NULL, NULL,
                                 u4Deprecated, u4RowStatus, i4Indices, fmt, ap);

    if (LockPtr != NULL)
    {
        LockPtr ();
    }
    va_end (ap);

    return i1RetVal;
}

/******************************************************************************
*      function Name        : MsrSetWithNotify                                *
*      Role of the function : This fn call all the nmhset functions and sends *
*                             an update trigger in case object status is not  *
*                             deprecated                                      * 
*      Formal Parameters    : Objectid, Wrapperfn,  isDeprecated, isRowStatus *
*                             no_of_indices, fmt string, variable no args     *
*      Use of Recursion     : None                                            *
*      Return Value         : SUCCESS or FAILURE                              *
******************************************************************************/

INT1
MsrSetWithNotify (UINT4 au4ObjectID[],
                  INT4 i4OidLen,
                  INT4 (*Wrapperfn) (tSnmpIndex *, tSNMP_MULTI_DATA_TYPE *),
                  INT4 (*LockPtr) (VOID),
                  INT4 (*UnlockPtr) (VOID),
                  UINT4 u4Deprecated,
                  UINT4 u4RowStatus, INT4 i4Indices, CHR1 * fmt, va_list ap)
{
    tSNMP_OID_TYPE      Oid;
    INT4                Index = 0;
    INT1                ret_val = SNMP_FAILURE;
    UINT1               au1Temp[SNMP_MAX_OID_LENGTH], *pau1Temp = NULL;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;
#ifdef L2RED_WANTED
    tRmNotifConfChg     RmNotifConfChg;
    UINT4               u4SeqNo = 0;
    UINT4               u4IgnoreAbortSeqNo = 0;
    UINT4               u4AcceptAbortSeqNo = 0;
#endif
    INT4                (*Wfnptr) (tSnmpIndex *, tSNMP_MULTI_DATA_TYPE *);
    BOOL1               bMsrTgr = OSIX_FALSE;

    UNUSED_PARAM (u4Deprecated);

    MEMSET (au1Temp, 0, SNMP_MAX_OID_LENGTH);
#ifdef L2RED_WANTED
    MEMSET (&RmNotifConfChg, 0, sizeof (tRmNotifConfChg));
#endif
    pau1Temp = au1Temp;
    for (Index = 0; Index < (i4OidLen - 1); Index++)
    {
        SPRINTF ((CHR1 *) pau1Temp, "%u.", au4ObjectID[Index]);
        pau1Temp += STRLEN (pau1Temp);
    }
    SPRINTF ((CHR1 *) pau1Temp, "%u", au4ObjectID[Index]);
    pau1Temp += STRLEN (pau1Temp);

#ifdef ISS_WANTED
    if (SnmpAllocMultiIndexAndData
        (&pMultiIndex, (UINT4) i4Indices, &pMultiData, &pOid,
         STRLEN (au1Temp)) == FAILURE)
    {
        printf ("MsrSetWithNotify: SnmpAllocMultiIndexAndData returned "
                "failure!!\n");
        return SNMP_FAILURE;
    }
    STRCPY (pOid, au1Temp);
#endif

#ifdef L2RED_WANTED
#ifdef CLI_WANTED
    if ((gu1CliNoOfPeers != 0) && (gCliRmRole == RM_ACTIVE))
    {
        RM_GET_SEQ_NUM (&u4SeqNo);
    }
#endif
    if (u4SeqNo != 0)
    {
        if (SnmpAllocMultiIndexAndData
            (&(RmNotifConfChg.pMultiIndex), (UINT4) i4Indices,
             &(RmNotifConfChg.pMultiData), &(RmNotifConfChg.pu1ObjectId),
             STRLEN (au1Temp)) == FAILURE)
        {
            printf ("MsrSetWithNotify: SnmpAllocMultiIndexAndData returned "
                    "failure for HA sync-message!!\n");
#ifdef ISS_WANTED
            MSRUpdateMemFree (pMultiIndex, pMultiData, pOid);
#endif
            return SNMP_FAILURE;
        }

#ifdef ISS_WANTED
        SNMPFillTwoInstOfMultiIndexandData ((UINT4) i4Indices, fmt,
                                            pMultiIndex, pMultiData,
                                            RmNotifConfChg.pMultiIndex,
                                            RmNotifConfChg.pMultiData, ap);
#else
        SNMPFillMultiIndexandData (i4Indices, fmt,
                                   RmNotifConfChg.pMultiIndex,
                                   RmNotifConfChg.pMultiData, ap);
#endif
        STRCPY (RmNotifConfChg.pu1ObjectId, au1Temp);
    }
    else
#endif
    {
#ifdef ISS_WANTED
        /* Updating pMultiIndex and pMultiData from va_list */
        SNMPFillMultiIndexandData ((UINT4) i4Indices, fmt, pMultiIndex,
                                   pMultiData, ap);
#endif
    }

#ifdef L2RED_WANTED
    /* Setting row status of an entry to destroy/not-in-service, may cause 
     * dynamic information change and may result in syncing up dynamic infomration.
     * This may lead to the following issue:
     *          In standby, the dynamic messages will be applied after static 
     *          message is applied (as the sequence number is reserved before 
     *          configuration in active node).
     *          But if the static message is applied, the entry will be deleted in 
     *          the standby node.
     *          After this when the dynamic messages for this entry is applied, it 
     *          will lead to a failure in standby node.
     *
     * To avoid this failure the following is done for row status objects, when 
     * the value is set to destroy/not-in-service:
     *        - After reserving the seq no for the configuration, reserve a one 
     *          more seq no for new message ("Ignore-Abort").
     *          After the configuration is applied in active, send the 
     *          ignore-abort message.
     *          Then get a new sequence no and send accept-abort message.
     *
     *       By this way, the standby will get the static sync up message first and 
     *       will apply it.
     *       Then it will get the ignore-abort message (before the dynamic sync 
     *       generated due to this configuration).
     *       After getting the ignore-abort message, standby will start ignoring the 
     *       sync=up apply failures from the protocols until the accept-abort message 
     *       comes. By this way the harmless sync up failures will be ignored.
     *
     */
    if (u4SeqNo != 0)
    {
        if ((u4RowStatus == OSIX_TRUE)
            && ((pMultiData->i4_SLongValue == DESTROY)
                || (pMultiData->i4_SLongValue == NOT_IN_SERVICE)))
        {
            /* Allocating ignore abort sequence number in case of rowstatus 
             * destroy/not-in-service before calling MsrSet */
            RM_GET_SEQ_NUM (&u4IgnoreAbortSeqNo);
        }

        if (Wrapperfn == IfMainBrgPortTypeSet)
        {
            /* In case of bridge port type configuration, deletion/creation 
             * will happen. since it is not a rowstatus object, explicit sequencing should
             * be done for this.*/
            RM_GET_SEQ_NUM (&u4IgnoreAbortSeqNo);
        }

    }

#endif

    if (IssCustValidateAction (pOid, pMultiIndex, pMultiData) == ISS_FAILURE)
    {
#ifdef L2RED_WANTED
        if (u4SeqNo != 0)
        {
            RmNotifConfChg.u4SeqNo = u4SeqNo;
            RmNotifConfChg.i4OidLen = i4OidLen;
            RmNotifConfChg.i1ConfigSetStatus = SNMP_FAILURE;
            RmNotifConfChg.i1MsgType = RM_UNUSED;

            RmApiSyncStaticConfig (&RmNotifConfChg);

            RmNotifConfChg.pMultiIndex = NULL;
            RmNotifConfChg.pMultiData = NULL;
            RmNotifConfChg.pu1ObjectId = NULL;
            RmNotifConfChg.i4OidLen = 0;
        }
#endif
#ifdef ISS_WANTED
        MSRUpdateMemFree (pMultiIndex, pMultiData, pOid);
#endif
        return SNMP_FAILURE;
    }

    Wfnptr = Wrapperfn;
    ret_val = (INT1) Wfnptr (pMultiIndex, pMultiData);

    Oid.u4_Length = (UINT4) i4OidLen;
    Oid.pu4_OidList = au4ObjectID;

    /* If the function returns failure, the value of bMsrTgr
     * will be OSIX_FALSE by default
     */

    SNMPGetMbDbTgrValue (Oid, &bMsrTgr);
#ifdef RM_WANTED
    if ((bMsrTgr == OSIX_FALSE) && (Wrapperfn != FsRmCopyPeerSyLogFileSet))
#else
    if (bMsrTgr == OSIX_FALSE)
#endif
    {
        /* The mib is registered to not sent triggers from the framework.
         * Hence, skip sending the Update message to MSR and RM */
#ifdef L2RED_WANTED
        if (u4SeqNo != 0)
        {
            MSRUpdateMemFree (RmNotifConfChg.pMultiIndex,
                              RmNotifConfChg.pMultiData,
                              RmNotifConfChg.pu1ObjectId);
        }
#endif
#ifdef ISS_WANTED
        MSRUpdateMemFree (pMultiIndex, pMultiData, pOid);
#endif
        return ret_val;
    }

    if (UnlockPtr != NULL)
    {
        UnlockPtr ();
    }

#ifdef L2RED_WANTED
    if (u4SeqNo != 0)
    {
        RmNotifConfChg.u4SeqNo = u4SeqNo;
        RmNotifConfChg.i4OidLen = i4OidLen;
        RmNotifConfChg.i1ConfigSetStatus = ret_val;
        RmNotifConfChg.i1MsgType = RM_UNUSED;

        RmApiSyncStaticConfig (&RmNotifConfChg);

        RmNotifConfChg.pMultiIndex = NULL;
        RmNotifConfChg.pMultiData = NULL;
        RmNotifConfChg.pu1ObjectId = NULL;
        RmNotifConfChg.i4OidLen = 0;

        if ((ret_val != SNMP_SUCCESS) && (u4IgnoreAbortSeqNo != 0))
        {
            /* In case of rowstatus destroy, the snmpset has been failed.
             * so just send across the dummy message since we had already
             * allocated the sequence number */
            RmNotifConfChg.u4SeqNo = u4IgnoreAbortSeqNo;
            RmNotifConfChg.i1ConfigSetStatus = ret_val;
            RmNotifConfChg.i1MsgType = RM_UNUSED;
            RmApiSyncStaticConfig (&RmNotifConfChg);
        }

        if ((ret_val == SNMP_SUCCESS) && (u4IgnoreAbortSeqNo != 0))
        {
            /* In case of sucessful row status destroy, we need to send two
             * extra messages namely ignore abort to get rid of failure of dynamic
             * sync up messages and accept abort to resume the ignore messages
             * in the standby node */
            RmNotifConfChg.u4SeqNo = u4IgnoreAbortSeqNo;
            RmNotifConfChg.i1ConfigSetStatus = ret_val;
            RmNotifConfChg.i1MsgType = RM_CONFIG_IGNORE_ABORT;
            RmApiSyncStaticConfig (&RmNotifConfChg);

            RM_GET_SEQ_NUM (&u4AcceptAbortSeqNo);
            RmNotifConfChg.u4SeqNo = u4AcceptAbortSeqNo;
            RmNotifConfChg.i1ConfigSetStatus = ret_val;
            RmNotifConfChg.i1MsgType = RM_CONFIG_ACCEPT_ABORT;
            RmApiSyncStaticConfig (&RmNotifConfChg);
        }
    }
#endif
#ifdef ISS_WANTED
    /* When Incremental save is disabled, no need to send notification 
     * to MSR. */
    if ((MsrGetIncrementalSaveStatus () == ISS_TRUE) &&
        (SNMP_SUCCESS == ret_val))
    {
        /* Sending MSR trigger */
        MSRNotifyConfChg (pMultiIndex, (BOOL1) u4RowStatus, pMultiData, pOid);
    }
    else                        /*freeing the allocated memory for MSR case */
    {
        MSRUpdateMemFree (pMultiIndex, pMultiData, pOid);
    }
#endif
    if (LockPtr != NULL)
    {
        LockPtr ();
    }
    return ret_val;
}

/******************************************************************************
*      function Name        : MsrSetWithNotifyNew                             *
*      Role of the function : This fn call sends                              *
*                             an update trigger in case object status is not  *
*                             deprecated. This will be used for generated     *
*                             code were set operation is done and             *
*                             then the trigger is initiated                   * 
*      Formal Parameters    : Objectid, isDeprecated, isRowStatus             *
*                             no_of_indices, ret_val status, fmt string,      *
*                             variable no args                                *
*      Use of Recursion     : None                                            *
*      Return Value         : SUCCESS or FAILURE                              *
******************************************************************************/

INT1
MsrSetWithNotifyNew (UINT4 au4ObjectID[],
                     INT4 i4OidLen,
                     INT4 (*LockPtr) (VOID),
                     INT4 (*UnlockPtr) (VOID),
                     UINT4 u4Deprecated,
                     UINT4 u4RowStatus, INT4 i4Indices, INT4 i4RetVal,
                     CHR1 * fmt, va_list ap)
{
    tSNMP_OID_TYPE      Oid;
    INT4                Index = 0;
    INT1                ret_val = (INT1) i4RetVal;
    UINT1               au1Temp[SNMP_MAX_OID_LENGTH], *pau1Temp = NULL;
    UINT1              *pOid = NULL;
    tSnmpIndex         *pMultiIndex = NULL;
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;
#ifdef L2RED_WANTED
    tRmNotifConfChg     RmNotifConfChg;
    UINT4               u4SeqNo = 0;
    UINT4               u4IgnoreAbortSeqNo = 0;
    UINT4               u4AcceptAbortSeqNo = 0;
#endif
    BOOL1               bMsrTgr;

    UNUSED_PARAM (u4Deprecated);

    MEMSET (au1Temp, 0, SNMP_MAX_OID_LENGTH);
#ifdef L2RED_WANTED
    MEMSET (&RmNotifConfChg, 0, sizeof (tRmNotifConfChg));
#endif
    pau1Temp = au1Temp;
    for (Index = 0; Index < (i4OidLen - 1); Index++)
    {
        SPRINTF ((CHR1 *) pau1Temp, "%u.", au4ObjectID[Index]);
        pau1Temp += STRLEN (pau1Temp);
    }
    SPRINTF ((CHR1 *) pau1Temp, "%u", au4ObjectID[Index]);
    pau1Temp += STRLEN (pau1Temp);

#ifdef ISS_WANTED
    if (SnmpAllocMultiIndexAndData
        (&pMultiIndex, (UINT4) i4Indices, &pMultiData, &pOid,
         STRLEN (au1Temp)) == FAILURE)
    {
        return SNMP_FAILURE;
    }
    STRCPY (pOid, au1Temp);
#endif

#ifdef L2RED_WANTED
#ifdef CLI_WANTED
    if ((gu1CliNoOfPeers != 0) && (gCliRmRole == RM_ACTIVE))
    {
        RM_GET_SEQ_NUM (&u4SeqNo);
    }
#endif
    if (u4SeqNo != 0)
    {
        if (SnmpAllocMultiIndexAndData
            (&(RmNotifConfChg.pMultiIndex), (UINT4) i4Indices,
             &(RmNotifConfChg.pMultiData), &(RmNotifConfChg.pu1ObjectId),
             STRLEN (au1Temp)) == FAILURE)
        {
#ifdef ISS_WANTED
            MSRUpdateMemFree (pMultiIndex, pMultiData, pOid);
#endif
            return SNMP_FAILURE;
        }

#ifdef ISS_WANTED
        SNMPFillTwoInstOfMultiIndexandData ((UINT4) i4Indices, fmt,
                                            pMultiIndex, pMultiData,
                                            RmNotifConfChg.pMultiIndex,
                                            RmNotifConfChg.pMultiData, ap);
#else
        SNMPFillMultiIndexandData (i4Indices, fmt,
                                   RmNotifConfChg.pMultiIndex,
                                   RmNotifConfChg.pMultiData, ap);
#endif
        STRCPY (RmNotifConfChg.pu1ObjectId, au1Temp);
    }
    else
#endif
    {
#ifdef ISS_WANTED
        /* Updating pMultiIndex and pMultiData from va_list */
        SNMPFillMultiIndexandData ((UINT4) i4Indices, fmt, pMultiIndex,
                                   pMultiData, ap);
#endif
    }

#ifdef L2RED_WANTED
    /* Setting row status of an entry to destroy/not-in-service, may cause 
     * dynamic information change and may result in syncing up dynamic 
     * infomration.
     * This may lead to the following issue:
     *          In standby, the dynamic messages will be applied after static 
     *          message is applied (as the sequence number is reserved before 
     *          configuration in active node).
     *          But if the static message is applied, the entry 
     *          will be deleted in 
     *          the standby node.
     *          After this when the dynamic messages for this entry 
     *          is applied, it 
     *          will lead to a failure in standby node.
     *
     * To avoid this failure the following is done for row status objects, when 
     * the value is set to destroy/not-in-service:
     *        - After reserving the seq no for the configuration, reserve a one 
     *          more seq no for new message ("Ignore-Abort").
     *          After the configuration is applied in active, send the 
     *          ignore-abort message.
     *          Then get a new sequence no and send accept-abort message.
     *
     *       By this way, the standby will get the static sync up 
     *       message first and 
     *       will apply it.
     *       Then it will get the ignore-abort message (before the dynamic sync 
     *       generated due to this configuration).
     *       After getting the ignore-abort message, standby will start 
     *       ignoring the 
     *       sync=up apply failures from the protocols until the 
     *       accept-abort message 
     *       comes. By this way the harmless sync up failures will be ignored.
     *
     */
    if (u4SeqNo != 0)
    {
        if ((u4RowStatus == OSIX_TRUE)
            && ((pMultiData->i4_SLongValue == DESTROY)
                || (pMultiData->i4_SLongValue == NOT_IN_SERVICE)))
        {
            /* Allocating ignore abort sequence number in case of rowstatus 
             * destroy/not-in-service before calling MsrSet */
            RM_GET_SEQ_NUM (&u4IgnoreAbortSeqNo);
        }

    }

#endif

    Oid.u4_Length = (UINT4) i4OidLen;
    Oid.pu4_OidList = au4ObjectID;

    SNMPGetMbDbTgrValue (Oid, &bMsrTgr);
    if (bMsrTgr == OSIX_FALSE)
    {
        /* The mib is registered to not sent triggers from the framework.
         * Hence, skip sending the Update message to MSR and RM */
#ifdef L2RED_WANTED
        if (u4SeqNo != 0)
        {
            MSRUpdateMemFree (RmNotifConfChg.pMultiIndex,
                              RmNotifConfChg.pMultiData,
                              RmNotifConfChg.pu1ObjectId);
        }
#endif
#ifdef ISS_WANTED
        MSRUpdateMemFree (pMultiIndex, pMultiData, pOid);
#endif
        return ret_val;
    }

    if (UnlockPtr != NULL)
    {
        UnlockPtr ();
    }

#ifdef L2RED_WANTED
    if (u4SeqNo != 0)
    {
        RmNotifConfChg.u4SeqNo = u4SeqNo;
        RmNotifConfChg.i4OidLen = i4OidLen;
        RmNotifConfChg.i1ConfigSetStatus = ret_val;
        RmNotifConfChg.i1MsgType = RM_UNUSED;

        RmApiSyncStaticConfig (&RmNotifConfChg);

        RmNotifConfChg.pMultiIndex = NULL;
        RmNotifConfChg.pMultiData = NULL;
        RmNotifConfChg.pu1ObjectId = NULL;
        RmNotifConfChg.i4OidLen = 0;

        if ((ret_val != SNMP_SUCCESS) && (u4IgnoreAbortSeqNo != 0))
        {
            /* In case of rowstatus destroy, the snmpset has been failed.
             * so just send across the dummy message since we had already
             * allocated the sequence number */
            RmNotifConfChg.u4SeqNo = u4IgnoreAbortSeqNo;
            RmNotifConfChg.i1ConfigSetStatus = ret_val;
            RmNotifConfChg.i1MsgType = RM_UNUSED;
            RmApiSyncStaticConfig (&RmNotifConfChg);
        }

        if ((ret_val == SNMP_SUCCESS) && (u4IgnoreAbortSeqNo != 0))
        {
            /* In case of sucessful row status destroy, we need to send two
             * extra messages namely ignore abort to get rid of failure 
             * of dynamic sync up messages and accept abort to 
             * resume the ignore messages
             * in the standby node */
            RmNotifConfChg.u4SeqNo = u4IgnoreAbortSeqNo;
            RmNotifConfChg.i1ConfigSetStatus = ret_val;
            RmNotifConfChg.i1MsgType = RM_CONFIG_IGNORE_ABORT;
            RmApiSyncStaticConfig (&RmNotifConfChg);

            RM_GET_SEQ_NUM (&u4AcceptAbortSeqNo);
            RmNotifConfChg.u4SeqNo = u4AcceptAbortSeqNo;
            RmNotifConfChg.i1ConfigSetStatus = ret_val;
            RmNotifConfChg.i1MsgType = RM_CONFIG_ACCEPT_ABORT;
            RmApiSyncStaticConfig (&RmNotifConfChg);
        }
    }
#endif
#ifdef ISS_WANTED
    /* When Incremental save is disabled, no need to send notification 
     * to MSR. */
    if ((MsrGetIncrementalSaveStatus () == ISS_TRUE) &&
        (SNMP_SUCCESS == ret_val))
    {
        /* Sending MSR trigger */
        MSRNotifyConfChg (pMultiIndex, (BOOL1) u4RowStatus, pMultiData, pOid);
    }
    else                        /*freeing the allocated memory for MSR case */
    {
        MSRUpdateMemFree (pMultiIndex, pMultiData, pOid);
    }
#endif
    if (LockPtr != NULL)
    {
        LockPtr ();
    }
    return ret_val;
}
