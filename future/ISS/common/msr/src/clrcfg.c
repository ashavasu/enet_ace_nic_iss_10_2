/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/* $Id: clrcfg.c,v 1.48 2016/05/31 10:01:03 siva Exp $                   */
/*****************************************************************************/
/*    FILE  NAME            : clrcfg.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           : MSR                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*    DESCRIPTION           : This file has functions releted to             */
/*                            clear-config feature                           */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#ifndef _CLEARCONFIG_C_
#define _CLEARCONFIG_C_
#include "msrsys.h"
#include "clrcfg.h"
#ifdef SNMP_WANTED
#include "snmputil.h"
#include "fssnmp.h"
#endif
#include "nputil.h"
extern UINT1        CfaGddGetLinkStatus (UINT2 u2IfIndex);

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrClearConfig                                   */
/*                                                                          */
/*    Description        : This function is used to clear all the           */
/*                         configurations that are made in the ISS.         */
/*                         It uses the function provided by                 */
/*                         the SNMP Manager to get the values of all the    */
/*                         mandatory objects stored in the SaveArray.       */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MSR_TRUE if the clear operation is successful    */
/****************************************************************************/

INT1
MsrClearConfig (VOID)
{
    INT1               *pi1Dummy = NULL;    /* Dummy pointer */
    UINT1               u1IsMcastEntryPresent = OSIX_FALSE;
    UINT1               u1LinkStatus = CFA_IF_DOWN;
    UINT1               u1AdminStatus = CFA_IF_DOWN;
    static UINT1        au1Oid[MSR_MAX_OID_LEN + 1];
    UINT1               u1AppendLastInstance = OSIX_TRUE;
    static UINT1        au1Instance[MSR_MAX_OID_LEN];
    static UINT1        au1tOid[MSR_MAX_OID_LEN + 1];
    UINT4               u4IfIndex = 0;
    UINT4               u4Index = 0;
    UINT4               u4NoIndex = 0;
    UINT4               u4Error = 0;
    UINT4               u4RowStatus = 0;
    UINT4               u4IpAddress = 0;
    UINT4               u4SubnetMask = 0;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;
    tSnmpIndex         *pCurIndex = MsrIndexPool1;
    tSnmpIndex         *pNextIndex = MsrIndexPool2;
    tSNMP_OID_TYPE     *pCurOid = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pNextOid = NULL;
    tSNMP_OID_TYPE     *pInstance = NULL;
    tSNMP_OID_TYPE     *pTmpOid = NULL;
    tSNMP_MULTI_DATA_TYPE *pData = NULL, *pdefData = NULL;

#ifdef NPAPI_WANTED
    tHwPortInfo         HwPortInfo;
    INT4                i4LocalStackPort = 0;
    INT4                i4RemoteStackPort = 0;
    INT4                i4Index = 0;
    UINT4               u4PortCount = 0;
    MEMSET (&HwPortInfo, 0, sizeof (tHwPortInfo));
    CfaGetLocalUnitPortInformation (&HwPortInfo);

    i4LocalStackPort = NpUtilGetStackingPortIndex (&HwPortInfo, i4Index);
    i4RemoteStackPort = NpUtilGetRemoteStackingPortIndex (&HwPortInfo);
#endif

    MSR_TRC (MSR_TRACE_TYPE, "Clearing configurations ........\n");

    MSR_MEMSET (au1Oid, 0, MSR_MAX_OID_LEN + 1);
    MSR_MEMSET (au1Instance, 0, MSR_MAX_OID_LEN);
    MSR_MEMSET (au1tOid, 0, MSR_MAX_OID_LEN + 1);

    if ((pCurOid = MsrClrCfgAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrClrCfgOidCount = 0;
        return MSR_FALSE;
    }
    if ((pNextOid = MsrClrCfgAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrClrCfgOidCount = 0;
        return MSR_FALSE;
    }
    if ((pInstance = MsrClrCfgAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrClrCfgOidCount = 0;
        return MSR_FALSE;
    }
    if ((pTmpOid = MsrClrCfgAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrClrCfgOidCount = 0;
        return MSR_FALSE;
    }
    if ((pOid = MsrClrCfgAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrClrCfgOidCount = 0;
        return MSR_FALSE;
    }
    if ((pData = MsrAllocMultiData ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrClrCfgOidCount = 0;
        gu4MsrOidCount = 0;
        gu4MsrMultiDataCount = 0;
        gu4MsrOctetStrCount = 0;
        return MSR_FALSE;
    }
    if ((pdefData = MsrAllocMultiData ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrClrCfgOidCount = 0;
        gu4MsrOidCount = 0;
        gu4MsrMultiDataCount = 0;
        gu4MsrOctetStrCount = 0;
        return MSR_FALSE;
    }

    /* Objects in gaSaveArray and gaClrBridgeArray should be cleared only 
       in Active Node */
    /* When an object is cleared in Active Node , configuration will be 
       synched up with Stand-by Node */

    /* Get number of items in the array */
    while (gaSaveArray[u4Index].u2Type != 0)
    {
        u4Index++;
    }

    /* Get all the object from the gaSaveArray */
    while (u4Index >= 1)
    {
        /* clear the data structures used for getting the previous element */
        MSR_CLEAR_OID (pCurOid);
        MSR_CLEAR_OID (pNextOid);
        MSR_CLEAR_OID (pInstance);
        MSR_CLEAR_OID (pTmpOid);

        /* The contents of the gpu1StrFlash array will be in the
         * following format:
         * OID   data-type   value
         * Here OID is the OID string
         *      data-type is the type in the tSNMP_MULTI_DATA_TYPE
         *      value is the actual value of the object.
         */

        /* Check for scalar object */
        if (gaSaveArray[u4Index].u2Type == MSR_SCALAR)
        {
            if (STRLEN (gaSaveArray[u4Index].pOidString) > MSR_MAX_OID_LEN)
            {
                MSR_TRC (MSR_TRACE_TYPE,
                         "OID greater than Maximum OID length\n");
                continue;
            }
            MSR_STRCPY (au1Oid, gaSaveArray[u4Index].pOidString);
            WebnmConvertStringToOid (pCurOid, au1Oid, MSR_FALSE);

            MSR_CLEAR_MULTIDATA (pData);

            if (SNMPGet (*pCurOid, pData, &u4Error, pCurIndex,
                         SNMP_DEFAULT_CONTEXT) == SNMP_FAILURE)
            {
                SYS_LOG_MSG ((SYSLOG_WARN_LEVEL, gu4MsrSysLogId,
                              "SNMP Get failed for OID %s", au1Oid));
                MSR_TRC_ARG1 (MSR_TRACE_TYPE,
                              "SNMP Get failed for OID %s \r\n", au1Oid);
                u4Index--;
                continue;
            }

            /* Custom function to decide whether the OID should be cleared */

            if (IssCustClearConfig (OSIX_FALSE, gaSaveArray[u4Index].u2Type,
                                    au1Oid, NULL, pData) == ISS_FAILURE)
            {
                MSR_TRC_ARG1 (MSR_TRACE_TYPE,
                              "Custom Function failed for OID %s\r\n", au1Oid);
                u4Index--;
                continue;
            }

            if (MsrSetDefaultValue (pCurOid, pData,
                                    pdefData, pCurIndex) != MSR_TRUE)
            {
                MSR_TRC_ARG1 (MSR_TRACE_TYPE,
                              "Setting default value for OID %s failed\r\n",
                              au1Oid);
                u4Index--;
                continue;
            }
        }                        /* end of if loop for scalar object */

        /* Check for tabular object */
        else
        {
            /* If there is a rowstatus associated with the table, get
             * all the rowstatus instances and set the value of the
             * rowstatus as DESTROY.
             */

            /* u2Type - 0 : Invalid Type */
            /* Copy the OID string to Temporary Buffer if u2Type is 
             * not invalid */
            if (gaSaveArray[u4Index].u2Type != 0)
            {
                if (STRCMP
                    (gaSaveArray[u4Index].pOidString,
                     STATIC_MCAST_ENTRY_OID) == 0)
                {
                    u1IsMcastEntryPresent = OSIX_TRUE;
                }
            }

            if ((gaSaveArray[u4Index].pRowStatus != NULL) ||
                (u1IsMcastEntryPresent == OSIX_TRUE))
            {
                if (u1IsMcastEntryPresent == OSIX_TRUE)
                {
                    MSR_STRCPY (au1Oid, STATIC_MCAST_STATUS_OID);
                    u1IsMcastEntryPresent = OSIX_FALSE;
                }
                else
                {
                    STRNCPY (au1Oid, gaSaveArray[u4Index].pRowStatus,
                             (sizeof (au1Oid) - 1));
                    au1Oid[(sizeof (au1Oid) - 1)] = '\0';
                }
                WebnmConvertStringToOid (pTmpOid, au1Oid, MSR_FALSE);
                WebnmConvertStringToOid (pCurOid, au1Oid, MSR_FALSE);
                WebnmConvertOidToString (pTmpOid, au1Oid);

                /* Get all the rowstatus objects in the table */
                while (1)
                {
                    /* Append the latest instance to the pCurOid.
                     * This is needed to get the next instance of
                     * the object.
                     */

                    /* If the latest Instance is appended,only first Entry is deleted 
                       for few objects .Appending the latest Instance should be
                       avoided for these objetcs */

                    if ((STRCMP (au1Oid, SNMP_USER_OID) == 0) ||
                        (STRCMP (au1Oid, SYSLOG_FILE_OID) == 0) ||
                        (STRCMP (au1Oid, ISS_PORT_ISOLATION_OID) == 0) ||
                        (STRCMP (au1Oid, DNS_DOMAIN_NAME_OID) == 0) ||
                        (STRCMP (au1Oid, DNS_NAME_SERVER_OID) == 0) ||
                        (STRCMP (au1Oid, SYSLOG_LOGGING_SERVER_OID)) == 0)
                    {
                        u1AppendLastInstance = OSIX_FALSE;
                    }

                    if (u1AppendLastInstance == OSIX_TRUE)
                    {
                        WebnmCopyOid (pCurOid, pInstance);
                    }
                    else
                    {
                        /* Reset to the Original Value */
                        u1AppendLastInstance = OSIX_TRUE;
                    }

                    MSR_CLEAR_MULTIDATA (pData);
                    if (SNMPGetNextReadWriteOID (*pCurOid, pNextOid, pData,
                                                 &u4Error, pCurIndex,
                                                 pNextIndex,
                                                 SNMP_DEFAULT_CONTEXT) !=
                        SNMP_SUCCESS)
                    {
                        SYS_LOG_MSG ((SYSLOG_WARN_LEVEL, gu4MsrSysLogId,
                                      "SNMP Get Next failed for OID %s",
                                      au1Oid));
                        break;
                    }
                    WebnmGetInstanceOid (pTmpOid, pNextOid, pInstance);

                    MSR_MEMSET (au1Instance, 0, MSR_MAX_OID_LEN);
                    WebnmConvertOidToString (pInstance, au1Instance);

                    if (pInstance->u4_Length == 0)
                    {
                        break;
                    }

                    /* Custom function to decide whether the OID should be cleared */
                    if (IssCustClearConfig
                        (OSIX_FALSE, gaSaveArray[u4Index].u2Type, au1Oid,
                         au1Instance, pData) == ISS_FAILURE)
                    {
                        MSR_TRC_ARG1 (MSR_TRACE_TYPE,
                                      "Custom Function failed for OID %s \r\n",
                                      au1Oid);
                        continue;

                    }
                    u4RowStatus = 1;
                    if (MsrValidateTableRowStatus (pTmpOid,
                                                   pInstance,
                                                   pData,
                                                   u4RowStatus) == MSR_SKIP)
                    {
                        MSR_CLEAR_OID (pCurOid);
                        WebnmCopyOid (pCurOid, pTmpOid);
                        continue;
                    }
                    /* Destroy the rowstatus instances */
                    if (SNMPSet (*pNextOid, pData, &u4Error, pNextIndex,
                                 SNMP_DEFAULT_CONTEXT) == SNMP_FAILURE)
                    {
                        WebnmConvertOidToString (pNextOid, au1tOid);
                        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4MsrSysLogId,
                                      "SNMP Set failed for OID %s", au1tOid));
                        MSR_TRC_ARG1 (MSR_TRACE_TYPE,
                                      "SNMP Set failed to Destroy row status instances for OID %s\r\n",
                                      au1tOid);
                        u4Error++;
                        MSR_CLEAR_OID (pCurOid);
                        WebnmCopyOid (pCurOid, pTmpOid);
                        continue;
                    }

                    /* Reset the pCurOid to the original OID */
                    MSR_CLEAR_OID (pCurOid);
                    WebnmCopyOid (pCurOid, pTmpOid);
                }                /* end of while to get all the rowstatus instances */
                MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);
            }                    /* end of if loop check for rowstatus */
            if ((gaSaveArray[u4Index].u2Type != 0) &&
                (gaSaveArray[u4Index].pRowStatus == NULL))
            {
                MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);
                STRNCPY (au1Oid, gaSaveArray[u4Index].pOidString,
                         MSR_MAX_OID_LEN);
                WebnmConvertStringToOid (pTmpOid, au1Oid, MSR_FALSE);
                WebnmConvertStringToOid (pCurOid, au1Oid, MSR_FALSE);
                MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);
                WebnmConvertOidToString (pTmpOid, au1Oid);

                /* Get all the objects in the table */
                while (1)
                {
                    /* Append the latest instance to the pCurOid.
                     * This is needed to get the next instance of
                     * the object.
                     */
                    MSR_CLEAR_MULTIDATA (pData);
                    u4Error = 0;
                    WebnmCopyOid (pCurOid, pInstance);
                    if (SNMPGetNextReadWriteOID (*pCurOid, pNextOid, pData,
                                                 &u4Error, pCurIndex,
                                                 pNextIndex,
                                                 SNMP_DEFAULT_CONTEXT) !=
                        SNMP_SUCCESS)
                    {
                        SYS_LOG_MSG ((SYSLOG_WARN_LEVEL, gu4MsrSysLogId,
                                      "SNMP Get Next failed for OID %s",
                                      au1Oid));
                        break;
                    }
                    WebnmGetInstanceOid (pTmpOid, pNextOid, pInstance);
                    if (pInstance->u4_Length == 0)
                    {
                        break;
                    }
                    MSR_MEMSET (au1Instance, 0, MSR_MAX_OID_LEN);
                    MSR_MEMSET (au1tOid, 0, MSR_MAX_OID_LEN);
                    WebnmConvertOidToString (pInstance, au1Instance);
                    SNPRINTF ((char *) au1tOid, MSR_MAX_OID_LEN,
                              "%s.%s", au1Oid, au1Instance);
                    MSR_CLEAR_OID (pOid);
                    WebnmConvertStringToOid (pOid, au1tOid, MSR_FALSE);

                    /* Custom function to decide whether the OID should be cleared */

                    if (IssCustClearConfig
                        (OSIX_FALSE, gaSaveArray[u4Index].u2Type, au1Oid,
                         au1Instance, pData) == ISS_FAILURE)
                    {
                        MSR_TRC_ARG1 (MSR_TRACE_TYPE,
                                      "Custom Function failed for OID %s\r\n",
                                      au1Oid);
                        continue;
                    }

                    /* Skip ifMainExtInterfaceType entry */
                    u4RowStatus = 0;
                    if (MsrValidateTableRowStatus (pTmpOid,
                                                   pInstance,
                                                   pData,
                                                   u4RowStatus) == MSR_SKIP)
                    {
                        MSR_CLEAR_OID (pCurOid);
                        MSR_CLEAR_MULTIDATA (pData);
                        WebnmCopyOid (pCurOid, pTmpOid);
                        continue;
                    }
                    if (MsrSetDefaultValue (pOid, pData,
                                            pdefData, pCurIndex) != MSR_TRUE)
                    {
                        MSR_TRC_ARG1 (MSR_TRACE_TYPE,
                                      "Setting default value for OID %s failed\r\n",
                                      au1tOid);
                        MSR_CLEAR_OID (pCurOid);
                        MSR_CLEAR_MULTIDATA (pData);
                        WebnmCopyOid (pCurOid, pTmpOid);
                        continue;
                    }
                    /* Reset the pCurOid to the original OID */
                    MSR_CLEAR_OID (pCurOid);
                    WebnmCopyOid (pCurOid, pTmpOid);
                }                /* end of while to get all the instaces */
            }                    /* end of if loop check for tables do not have rowstatus */
        }                        /* end of if loop check for tabular object */

        /*Clearing off the indices present in the Multi-Data Array, to 
         * avoid invalid values being passed to the next iteration. */

        for (u4NoIndex = 0; u4NoIndex < pNextIndex->u4No; u4NoIndex++)
        {
           if ((pNextIndex->pIndex != NULL) && 
               ((pNextIndex->pIndex + u4NoIndex) != NULL))
			{		
            	MSR_CLEAR_MULTIDATA ((pNextIndex->pIndex + u4NoIndex));
			}
        }
        u4Index--;
    }

    if (ClearConfigSetValueForArray (gaClrBridgeArray) != MSR_SUCCESS)
    {
        MSR_TRC (MSR_TRACE_TYPE,
                 "Setting default value failed for MSR unsupported MIBs\r\n");

        gu4MsrClrCfgOidCount = 0;
        gu4MsrMultiDataCount = 0;
        gu4MsrOctetStrCount = 0;
        gu4MsrOidCount = 0;

        return MSR_FALSE;
    }

#ifdef SNTP_WANTED
    SntpLock ();
    SntpSetDstDefaults ();
    SntpGblParamsInit ();
    SntpUnLock ();
#endif

#ifdef LA_WANTED
    LA_LOCK ();
    LaDefaultSettings ();
    LA_UNLOCK ();
#endif

    CFA_LOCK ();
    CfaCreateDefaultIVRInterface ();
    CfaIfmDefaultInterfaceInit ();
    u4IpAddress = IssGetIpAddrFromNvRam ();
    u4SubnetMask = IssGetSubnetMaskFromNvRam ();
    CfaIPVXSetIpAddress (CFA_DEFAULT_ROUTER_VLAN_IFINDEX, u4IpAddress,
                         u4SubnetMask);
    CfaSetIfMainAdminStatus (CFA_ROUTER_IFINDEX, CFA_IF_UP);
    CfaSetIfMainAdminStatus (CFA_DEFAULT_ROUTER_VLAN_IFINDEX, CFA_IF_UP);

    CfaSetIfBridgedIfaceStatus (CFA_ROUTER_IFINDEX, CFA_ENABLED);

    CFA_UNLOCK ();
#if defined (SNMP_3_WANTED)  && defined (CLI_WANTED)
    CliContextInitToDefault ();
#endif

#ifndef MBSM_WANTED
    CfaIfmBringupAllInterfaces (pi1Dummy);
#else
    /* If MBSM is defined , Interface deletion will be skipped first */
    /* Delete and create the Interfaces for all slots in the system */

    CfaMbsmDeleteInterfacesForAllSlot ();
    CfaMbsmCreateInterfacesForAllSlot ();
    UNUSED_PARAM (pi1Dummy);
#endif
    CFA_LOCK ();
#ifdef NPAPI_WANTED
    if ((ISS_GET_STACKING_MODEL () == ISS_CTRL_PLANE_STACKING_MODEL) ||
        (ISS_GET_STACKING_MODEL () == ISS_DISS_STACKING_MODEL))
    {
        /* Update the port admin status of all ports except default interface 
           in Hardware */
        u4PortCount = (SYS_DEF_MAX_DATA_PORT_COUNT +
                       (HwPortInfo.u4NumConnectingPorts) * 2);
        for (u4IfIndex = 1; u4IfIndex < u4PortCount; u4IfIndex++)
        {
            if ((u4IfIndex != (UINT4) i4LocalStackPort)
                && (u4IfIndex != (UINT4) i4RemoteStackPort)
                && (u4IfIndex != CFA_DEFAULT_ROUTER_IFINDEX))
            {
                CfaFsHwUpdateAdminStatusChange (u4IfIndex, FNP_FALSE);
            }
        }
    }
#endif
    if (((ISS_GET_STACKING_MODEL () == ISS_CTRL_PLANE_STACKING_MODEL) ||
         (ISS_GET_STACKING_MODEL () == ISS_DISS_STACKING_MODEL)) &&
        ((VcmGetSystemModeExt (CFA_PROTOCOL_ID) == VCM_SI_MODE) ? 1 : 0))
    {
        if ((IssGetRestoreFlagFromNvRam () != ISS_CONFIG_RESTORE) ||
            (IssGetRestoreFlagFromNvRam () != ISS_CONFIG_REMOTE_RESTORE))
        {
            u1AdminStatus = CFA_IF_UP;
        }
        else
        {
            u1AdminStatus = CFA_IF_DOWN;
        }
        CfaIfmHandleInterfaceStatusChange (CFA_DEFAULT_ROUTER_IFINDEX,
                                           u1AdminStatus, u1LinkStatus,
                                           CFA_MEMBER_LINK_UP);
    }
    CFA_UNLOCK ();
#ifdef OSPF_WANTED
    OspfShutdownProtocol ();
#endif
#ifdef OSPF3_WANTED
    V3OspfApiModuleShutDown ();
#endif
#ifdef OSPFTE_WANTED
    OspfTeLock ();
    OspfTeShutDown ();
    OspfTeUnLock ();
#endif
#ifdef VRRP_WANTED
    VrrpLock ();
    VrrpShutDown ();
    VrrpUnLock ();
#endif
#ifdef IGMP_WANTED
    IGMP_LOCK ();
    IgpMainStatusDisable ();
    IGMP_UNLOCK ();
#endif

#if defined (PIM_WANTED) || defined (PIMV6_WANTED)
    PimModuleShut ();
#endif
#ifdef DVMRP_WANTED
    DVMRP_MUTEX_LOCK ();
    DvmrpIhmHandleShutdown ();
    DVMRP_MUTEX_UNLOCK ();
#endif
#ifdef BGP4_WANTED
    BgpLock ();
    Bgp4DeInit ();
    BgpUnLock ();
#endif

#ifdef SNMP_3_WANTED
    if (SnmpApiGetSNMPUpdateDefault () == OSIX_TRUE)
    {
        SNMPV3DefaultInit ();
    }
    Snmp3DefCommunityConfig ();
    Snmp3Def1CommunityConfig ();
    Snmp3DefGroupConfig ();
    Snmp3Def1GroupConfig ();
    Snmp3DefAccessConfig ();
    Snmp3Def1AccessConfig ();
    Snmp3DefTargetParamConfig ();
    Snmp3Def1TargetParamConfig ();
    Snmp3DefNotifyConfig ();

#endif
#ifdef LLDP_WANTED
    LldpApiResetAgentVariables ();
#endif
#ifdef QOSX_WANTED
    QoSLock ();
    QoSDeleteTables ();
    QoSCreateTables ();
    QoSAddDefTblEntries ();
    QoSSetDefCpuQTblEntries ();
    QoSUnLock ();
#endif

    for (u4IfIndex = 1; u4IfIndex < IssGetFrontPanelPortCountFromNvRam ();
         u4IfIndex++)
    {
        VLAN_LOCK ();
        if (VlanValidatePortEntry (u4IfIndex) != VLAN_FALSE)
        {
            if (VcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                              &u2LocalPortId) == VCM_SUCCESS)
            {
                VlanSetDefPortInfo (u2LocalPortId);
            }

        }
        VLAN_UNLOCK ();
    }

#ifdef ISS_WANTED
    IssInitSystemParams ();
#endif

#ifdef OPENSSL_WANTED
    SslArResetRsaKeyPair ();
#endif
    /* To Set the port speed */
    IssInitPortSpeedParams();

    /* To Perform customizations ,once clear-config is completed */
    /* First Argument Indicates Clear-config is completed */
    /* This is not specific to any OID . So rest of the arguments are not used */

    IssCustClearConfig (OSIX_TRUE, 0, NULL, NULL, NULL);

    gu4MsrClrCfgOidCount = 0;
    gu4MsrMultiDataCount = 0;
    gu4MsrOctetStrCount = 0;
    gu4MsrOidCount = 0;

    return MSR_TRUE;
}

/*****************************************************************************/
/* Function Name      : MsrSetDefaultValue                                   */
/*                                                                           */
/* Description        : This function is called to set the value             */
/*                      for the object whose oid is paased to function       */
/*                                                                           */
/* Input(s)           : pOid, pIndex                                         */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : MSR_TRUE                                             */
/*****************************************************************************/
INT4
MsrSetDefaultValue (tSNMP_OID_TYPE * pOid,
                    tSNMP_MULTI_DATA_TYPE * pData,
                    tSNMP_MULTI_DATA_TYPE * pdefData, tSnmpIndex * pCurIndex)
{
    INT4                i4Result = 0, i4SnmpRetval = SNMP_FAILURE;
    UINT4               u4Error = 0;
    UINT1               au1Oid[MSR_MAX_OID_LEN + 1];

    MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);
    WebnmConvertOidToString (pOid, au1Oid);
    MSR_CLEAR_MULTIDATA (pData);

    /*  OSPF Area Routes and OSPF Ext Routes are set only if the Protocol ID
     *  is either BGP or RIP. In other cases it throws an error stating invalid 
     *  destination protocol ID. Thus for compliance the other protocol IDS are 
     *  skipped during clear-config */

    if ((STRCMP (OSPF_AREA_ROUTES_OID_LOCAL, au1Oid) == 0) ||
        (STRCMP (OSPF_AREA_ROUTES_OID_NETMGMT, au1Oid) == 0) ||
        (STRCMP (OSPF_EXT_ROUTES_OID_LOCAL, au1Oid) == 0) ||
        (STRCMP (OSPF_EXT_ROUTES_OID_NETMGMT, au1Oid) == 0))
    {
        return MSR_SKIP;
    }

    if (SNMPGet (*pOid, pData, &u4Error, pCurIndex,
                 SNMP_DEFAULT_CONTEXT) == SNMP_SUCCESS)
    {
        MSR_CLEAR_MULTIDATA (pdefData);
        i4SnmpRetval = SNMPGetDefaultValueForObj (*pOid, pdefData, &u4Error);
        if (((i4SnmpRetval == SNMP_FAILURE) &&
             (u4Error == SNMP_EXCEPTION_NO_DEF_VAL))
            || (STRCMP (DHCP_CIRCUIT_OPTION_OID, au1Oid) == 0))
        {
            /* To Set the default value of the mib object  */
            if (ClearConfigDefaultValueForObj (pOid) != MSR_SUCCESS)
            {
                MSR_TRC_ARG1 (MSR_TRACE_TYPE,
                              "Setting default value for OID failed\r\n",
                              au1Oid);
                return MSR_FALSE;
            }
        }
        if ((i4SnmpRetval == SNMP_FAILURE) &&
            (u4Error != SNMP_EXCEPTION_NO_DEF_VAL))
        {
            return MSR_FALSE;
        }

        else if (STRCMP (DHCP_CIRCUIT_OPTION_OID, au1Oid) != 0)
        {

            i4Result = MSRCompareSnmpMultiDataValues (pData, pdefData);
            if (i4Result == MSR_SUCCESS)
            {
                MSR_TRC_ARG1 (MSR_TRACE_TYPE,
                              "Default and Original values are same for OID %s\r\n",
                              au1Oid);
                return MSR_FALSE;
            }
            else
            {
                if (SNMPTest (*pOid, pdefData,
                              &u4Error, pCurIndex,
                              SNMP_DEFAULT_CONTEXT) == SNMP_FAILURE)
                {
                    MSR_TRC_ARG1 (MSR_TRACE_TYPE,
                                  "SNMP Test for OID %s failed\r\n", au1Oid);
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4MsrSysLogId,
                                  "SNMP Test failed for OID %s", au1Oid));
                    return MSR_FALSE;
                }
                /* Set the value of object with default value */
                if (SNMPSet (*pOid, pdefData,
                             &u4Error, pCurIndex,
                             SNMP_DEFAULT_CONTEXT) == SNMP_FAILURE)
                {
                    SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4MsrSysLogId,
                                  "SNMP Set failed for OID %s", au1Oid));
                    MSR_TRC_ARG1 (MSR_TRACE_TYPE,
                                  "SNMP Set for OID %s failed.\r\n", au1Oid);
                    return MSR_FALSE;
                }
            }
        }                        /* end of if get pdefData */
    }                            /* end of if get pData */
    else
    {
        SYS_LOG_MSG ((SYSLOG_WARN_LEVEL, gu4MsrSysLogId,
                      "SNMP Get failed for OID %s", au1Oid));
        return MSR_FALSE;
    }

    return MSR_TRUE;
}

/************************************************************************
 *  Function Name   : MsrValidateTableRowStatus
 *  Description     : This function validates the behaviour of ClearConfig
 *                    over the instance row Status.
 *  Input           : pOid - pointer to the OID
 *                    pInstance - pointer to the instance OID
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be cleared
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be cleared
 *                    MSR_NEXT if the current scalar/table should not be
 *                             clear
/ ************************************************************************/

INT4
MsrValidateTableRowStatus (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData, UINT4 u4RowStatus)
{
    UINT1               au1Oid[MSR_MAX_OID_LEN + 1];
    UINT4               u4HigherIndex = 0;

    MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);
    WebnmConvertOidToString (pOid, au1Oid);

    /* Skip Deleting Switch Context parameters */
    if ((STRCMP (VCM_IF_MAIN_OID, au1Oid) == 0) ||
        (STRCMP (IF_STACK_OID, au1Oid) == 0))
    {
        return MSR_SKIP;
    }

	if (STRCMP (OFCL_CFG_ENTRY_ROW_STATUS, au1Oid) == 0)
    {
        /* Check for default Entry */
	    if(pInstance->pu4_OidList[0] == SNMP_DEFAULT_CONTEXT) 
        {
            return MSR_SKIP;
        }
    }


    /* Skip deleting default interfaces */
    if ((STRCMP (IF_MAIN_OID, au1Oid) == 0))
    {
#ifdef MBSM_WANTED
        /* Skip Interface deletion if MBSM is defined . */
        /* Later an API will be invoked to delete and create Interfaces
           for all slots in the system */
        /* This is done to ensure Interfaces are created with correct
           slot numbers */
        UNUSED_PARAM (u4HigherIndex);
        if (pInstance->pu4_OidList[0] <= SYS_DEF_MAX_PHYSICAL_INTERFACES)
        {
            return MSR_SKIP;
        }
#else
        if (pInstance->pu4_OidList[0] == CFA_DEFAULT_ROUTER_VLAN_IFINDEX)
        {
            return MSR_SKIP;
        }
        if (CfaGetHLIfIndex (pInstance->pu4_OidList[0], &u4HigherIndex) ==
            CFA_SUCCESS)
        {
            return MSR_SKIP;
        }
#endif
    }

    if ((STRCMP (IF_MAIN_EXT_OID, au1Oid) == 0) &&
        (pInstance->pu4_OidList[0] == 10) &&
        (pInstance->pu4_OidList[1] > SYS_DEF_MAX_PHYSICAL_INTERFACES))
    {
        return MSR_SKIP;
    }

    if (u4RowStatus == 1)
    {
        /* For the table having entryStatus instead of RowStatus */
        if ((STRCMP (EVENT_STATUS_OID, au1Oid) == 0) ||
            (STRCMP (ALARM_STATUS_OID, au1Oid) == 0) ||
            (STRCMP (ETHER_STATUS_OID, au1Oid) == 0) ||
            (STRCMP (HISTORY_STATUS_OID, au1Oid) == 0))
        {
            pData->i2_DataType = SNMP_DATA_TYPE_INTEGER;
            pData->i4_SLongValue = MSR_INVALID;
            pData->u4_ULongValue = MSR_INVALID;
        }
        else if ((STRCMP (au1Oid, STATIC_MCAST_STATUS_OID) == 0))
        {
            pData->i2_DataType = SNMP_DATA_TYPE_INTEGER;
            pData->i4_SLongValue = VLAN_MGMT_INVALID;
            pData->u4_ULongValue = VLAN_MGMT_INVALID;
        }
        else
        {
            pData->i2_DataType = SNMP_DATA_TYPE_INTEGER;
            pData->i4_SLongValue = MSR_DESTROY;
            pData->u4_ULongValue = MSR_DESTROY;
        }
    }
    return MSR_SAVE;
}

/************************************************************************
 *  Function Name   : ClearConfigDefaultValueForObj
 *  Description     : This is function is used to set the default value
 *                    for any Scalar onject which is not having default
 *                    value.
 *  Input           : OID - Oid of the scalar object whose value has to be
 *                          set.
 *  Output          : None
 *  Returns         : MSR_SUCCESS/ MSR_FAILURE
 ************************************************************************/
INT4
ClearConfigDefaultValueForObj (tSNMP_OID_TYPE * pOID)
{
    INT4                u4Index = 0;
    INT4                u4MaxIndex = 0;
    UINT4               u4Error = 0;
    UINT4               u4Len = 0;
    UINT4               u4Count = 0;
    UINT1               u1OidType = 0;
    UINT1               au1Val[MSR_MAX_OID_LEN];
    UINT1               au1Oid[MSR_MAX_OID_LEN + 1];
    UINT1               au1MacHex[CLR_MAX_MAC_LEN];
    UINT1               au1Temp[CLR_MAX_MAC_LEN];
    tSNMP_MULTI_DATA_TYPE *pData = NULL;
    tSNMP_MULTI_DATA_TYPE *pTempData = NULL;
    tSNMP_OID_TYPE     *pTempOid = NULL;
    tSnmpIndex         *pIndex = NULL;
    tSnmpIndex         *pTempIndex = NULL;
    tSnmpDbEntry        SnmpDbEntry;
    tMbDbEntry         *pCur = NULL, *pNext = NULL;
    tMacAddr            au1SystemMac;
    MEMSET (&SnmpDbEntry, 0, sizeof (tSnmpDbEntry));
    MEMSET (au1Oid, 0, MSR_MAX_OID_LEN + 1);
    MEMSET (au1Val, 0, MSR_MAX_OID_LEN);
    MEMSET (au1MacHex, 0, CLR_MAX_MAC_LEN);
    MEMSET (au1Temp, 0, CLR_MAX_MAC_LEN);
    MEMSET (au1SystemMac, 0, sizeof (tMacAddr));
#ifdef SNMP_3_WANTED
    pIndex = SNMPGetFirstIndexPool ();
#endif

    while (gaClearArray[u4MaxIndex].pu1DefVal != NULL)
    {
        u4MaxIndex++;
    }

    GetMbDbEntry (*pOID, &pNext, &u4Len, &SnmpDbEntry);
    pCur = SnmpDbEntry.pMibEntry;

    if (pCur == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Invalid OID\r\n");
        return MSR_FAILURE;
    }

    u1OidType = (UINT1) pCur->u4OIDType;

    pTempOid = &pCur->ObjectID;
    WebnmConvertOidToString (pTempOid, au1Oid);

    while (gaClearArray[u4Index].pu1DefVal != NULL)
    {
        if (STRCMP (au1Oid, gaClearArray[u4Index].pScalarOidString) == 0)
        {
            if ((STRCMP (au1Oid, PB_MUL_LIMIT_OID) == 0))
            {
                SPRINTF ((CHR1 *) au1Val, "%d", VLAN_DEV_MAX_MCAST_TABLE_SIZE);
            }
            else if ((STRCMP (au1Oid, EOAM_OUI_OID) == 0) ||
                     (STRCMP (au1Oid, ECFM_OUI_OID) == 0))
            {
                MEMCPY (au1SystemMac, IssGetSwitchMacFromNvRam (),
                        sizeof (tMacAddr));
                /* OUI is the first the bytes of the MAC address */
                CLI_CONVERT_MAC_TO_DOT_STR (au1SystemMac, au1Temp);
                MEMCPY (au1Val, au1Temp, CLR_OUI_STR_LEN);
            }
            else if ((STRCMP (au1Oid, LLDP_SYSTEM_OID) == 0) ||
                     (STRCMP (au1Oid, LA_SYSTEM_OID) == 0))
            {
                MEMCPY (au1SystemMac, IssGetSwitchMacFromNvRam (),
                        sizeof (tMacAddr));
                CLI_CONVERT_MAC_TO_DOT_STR (au1SystemMac, au1Val);
            }
            else if ((STRCMP (au1Oid, MST_REGION_OID) == 0))
            {
                MEMCPY (au1SystemMac, IssGetSwitchMacFromNvRam (),
                        sizeof (tMacAddr));
                for (u4Count = 0; u4Count < sizeof (tMacAddr); u4Count++)
                {
                    if (u4Count == (sizeof (tMacAddr) - 1))
                    {
                        SPRINTF ((CHR1 *) au1Temp, "%02x",
                                 au1SystemMac[u4Count]);
                        STRCAT (au1MacHex, au1Temp);
                    }
                    else
                    {
                        SPRINTF ((CHR1 *) au1Temp, "%02x",
                                 au1SystemMac[u4Count]);
                        STRCAT (au1MacHex, au1Temp);
                        STRCAT (au1MacHex, ":");
                    }
                }
                MEMSET (au1Temp, 0, CLR_MAX_MAC_LEN);
                for (u4Count = 0; u4Count < STRLEN (au1MacHex); u4Count++)
                {
                    if (u4Count == (STRLEN (au1MacHex) - 1))
                    {
                        SPRINTF ((CHR1 *) au1Temp, "%02x", au1MacHex[u4Count]);
                        STRCAT (au1Val, au1Temp);
                    }
                    else
                    {
                        SPRINTF ((CHR1 *) au1Temp, "%02x", au1MacHex[u4Count]);
                        STRCAT (au1Val, au1Temp);
                        STRCAT (au1Val, ":");
                    }

                }
            }
            else if ((STRCMP (au1Oid, SYS_LOCATION_OID) == 0))
            {
                MEMCPY (au1Val, SNMP_SYS_LOCATION, STRLEN (SNMP_SYS_LOCATION));
            }
            else if ((STRCMP (au1Oid, SYS_CONTACT_OID) == 0))
            {
                MEMCPY (au1Val, SNMP_SYS_CONTACT, STRLEN (SNMP_SYS_CONTACT));
            }
            else if ((STRCMP (au1Oid, SYS_NAME_OID) == 0))
            {
                MEMCPY (au1Val, SNMP_SYS_NAME, STRLEN (SNMP_SYS_NAME));
            }

            /* Condition to check whether default value is already set 
             * for DHCP_CIRCUIT_OPTION_OID else continues to set the 
             * default for DhcpCircuitOption */
            else if ((STRCMP (au1Oid, DHCP_CIRCUIT_OPTION_OID) == 0))
            {
                pTempData = MsrAllocMultiData ();
                pTempIndex = pIndex;
                if (pTempData != NULL)
                {
                    if (SNMPGet (*pOID, pTempData, &u4Error, pTempIndex,
                                 SNMP_DEFAULT_CONTEXT) == SNMP_SUCCESS)
                    {
                        if (STRCMP
                            (pTempData->pOctetStrValue->pu1_OctetList,
                             DHCP_CIRCUIT_OPTION_VALUE) == 0)
                        {
                            MSR_CLEAR_MULTIDATA (pTempData);
                            if (gu4MsrOidCount > 0)
                            {
                                gu4MsrOidCount--;
                            }
                            return MSR_FAILURE;
                        }

                        else
                        {
                            MSR_CLEAR_MULTIDATA (pTempData);
                            STRNCPY (au1Val,
                                     (UINT1 *) gaClearArray[u4Index].pu1DefVal,
                                     (sizeof (au1Val) - 1));
                            au1Val[(sizeof (au1Val) - 1)] = '\0';
                        }
                    }
                }
                if (gu4MsrOidCount > 0)
                {
                    gu4MsrOidCount--;
                }
            }
            else
            {
                STRNCPY (au1Val, (UINT1 *) gaClearArray[u4Index].pu1DefVal,
                         (sizeof (au1Val) - 1));
                au1Val[(sizeof (au1Val) - 1)] = '\0';
            }
            break;
        }
        u4Index++;
        if (u4Index >= u4MaxIndex)
        {
            return MSR_FAILURE;
        }
    }

#if defined (SNMP_3_WANTED)  && defined (CLI_WANTED)
    pData = Snmp3CliConvertStringToData ((UINT1 *) au1Val, u1OidType);
#endif

    if (pData == NULL)
    {
        MSR_TRC_ARG1 (MSR_TRACE_TYPE, "No default value found for OID %s\r\n",
                      au1Oid);
        return MSR_FAILURE;
    }

    /* OidType for SYS_LOCATION_OID and SYS_CONTACT_OID is SNMP_DATA_TYPE_OCTET_PRIM
     * For SYS_LOCATION_OID and SYS_CONTACT_OID , the correct values are copied 
     * without any colon delimiters. so no need to do the Colon string to Octect
     * Conversion */
    if ((u1OidType == SNMP_DATA_TYPE_OCTET_PRIM)
        && ((STRCMP (au1Oid, SYS_LOCATION_OID) != 0)
            && (STRCMP (au1Oid, SYS_CONTACT_OID) != 0)
            && (STRCMP (au1Oid, SYS_NAME_OID) != 0)))
    {
        WebnmConvertColonStringToOctet (pData->pOctetStrValue, au1Val);
    }
    if (SNMP_SUCCESS == SNMPTest (*pOID, pData, &u4Error,
                                  pIndex, SNMP_DEFAULT_CONTEXT))
    {
        if (SNMP_FAILURE == SNMPSet (*pOID, pData, &u4Error,
                                     pIndex, SNMP_DEFAULT_CONTEXT))
        {
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4MsrSysLogId,
                          "SNMP Set failed for OID %s", au1Oid));
            MSR_TRC_ARG1 (MSR_TRACE_TYPE, "SNMP set failed for OID %s\r\n",
                          au1Oid);
            return MSR_FAILURE;
        }
    }
    else
    {
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4MsrSysLogId,
                      "SNMP Test failed for OID %s", au1Oid));
        MSR_TRC_ARG1 (MSR_TRACE_TYPE, "SNMP Test failed for OID %s\r\n",
                      au1Oid);
        return MSR_FAILURE;
    }

    return MSR_SUCCESS;
}

/************************************************************************
 *  Function Name   : ClearConfigSetValueForArray
 *  Description     : This is function is used to set the value for  
 *                    for any Scalar object which is defined in an array.
 *  Input           : pointer to tScalarObject which contains the object
                      and the value to be set.
 *  Output          : None
 *  Returns         : MSR_SUCCESS/ MSR_FAILURE
 ************************************************************************/

INT4
ClearConfigSetValueForArray (tScalarObject * pArray)
{
    INT4                u4Index = 0;
    UINT4               u4Error = 0;
    UINT4               u4Len = 0;
    UINT1               u1OidType = 0;
    UINT1               au1Oid[MSR_MAX_OID_LEN + 1];
    UINT1               au1Val[MSR_MAX_OID_LEN];
    tSNMP_MULTI_DATA_TYPE *pData = NULL;
    tSnmpIndex         *pIndex = NULL;
    tSnmpDbEntry        SnmpDbEntry;
    tMbDbEntry         *pCur = NULL, *pNext = NULL;
    tSNMP_OID_TYPE     *pOID;

    MEMSET (&SnmpDbEntry, 0, sizeof (tSnmpDbEntry));

    if ((pOID = MsrClrCfgAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        return MSR_FALSE;
    }

#ifdef SNMP_3_WANTED
    pIndex = SNMPGetFirstIndexPool ();
#endif

    while (pArray[u4Index].pu1DefVal != NULL)
    {
        MEMSET (au1Val, 0, MSR_MAX_OID_LEN);
        MEMSET (au1Oid, 0, MSR_MAX_OID_LEN + 1);
        STRNCPY (au1Oid, pArray[u4Index].pScalarOidString,
                 (sizeof (au1Oid) - 1));
        au1Oid[(sizeof (au1Oid) - 1)] = '\0';
        WebnmConvertStringToOid (pOID, au1Oid, MSR_FALSE);
        GetMbDbEntry (*pOID, &pNext, &u4Len, &SnmpDbEntry);
        pCur = SnmpDbEntry.pMibEntry;

        if (pCur == NULL)
        {
            u4Index++;
            continue;
        }

        u1OidType = (UINT1) pCur->u4OIDType;
        STRNCPY (au1Val, pArray[u4Index].pu1DefVal, (sizeof (au1Val) - 1));
        au1Val[(sizeof (au1Val) - 1)] = '\0';
#if defined (SNMP_3_WANTED)  && defined (CLI_WANTED)
        pData =
            Snmp3CliConvertStringToData ((UINT1 *) pArray[u4Index].pu1DefVal,
                                         u1OidType);
#endif

        if (pData == NULL)
        {
            u4Index++;
            continue;
        }

        if (u1OidType == SNMP_DATA_TYPE_OCTET_PRIM)
        {
            WebnmConvertColonStringToOctet (pData->pOctetStrValue, au1Val);
        }
        if (SNMP_SUCCESS == SNMPTest (*pOID, pData, &u4Error,
                                      pIndex, SNMP_DEFAULT_CONTEXT))
        {
            if (SNMP_FAILURE == SNMPSet (*pOID, pData, &u4Error,
                                         pIndex, SNMP_DEFAULT_CONTEXT))
            {
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4MsrSysLogId,
                              "SNMP Set failed for OID %s", au1Oid));
                u4Index++;
                continue;
            }
        }
        else
        {
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4MsrSysLogId,
                          "SNMP Test failed for OID %s", au1Oid));
            u4Index++;
            continue;
        }
        u4Index++;
    }
    if (gu4MsrClrCfgOidCount > 0)
    {
        gu4MsrClrCfgOidCount--;
    }
    return MSR_SUCCESS;

}
#endif
