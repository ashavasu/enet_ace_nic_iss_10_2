/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                         */
/* $Id: msrsys.c,v 1.303 2017/09/15 13:57:06 siva Exp $*/
/*****************************************************************************/
/*    FILE  NAME            : msrsys.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           : MSR                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*    DESCRIPTION           : This file has functions for configuration save */
/*                            and restore through SNMP.                      */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#ifndef _MSR_C_
#define _MSR_C_
#include <time.h>
#include "msrsys.h"
#include "msrglb.h"
#include "fsiss.h"
#include "msr.h"
#include "fswebnm.h"
#include "ifmmacs.h"
#include "mibsave.h"
#include "msrtrap.h"
#include "rmgr.h"
#ifdef SNMP_WANTED
#include "snmptdfs.h"
#include "snmptrap.h"
#include "snmputil.h"
#include "fssnmp.h"
#endif
#ifdef FIREWALL_WANTED
#include "firewall.h"
#endif
#ifdef SNMP_2_WANTED
#include "csr.h"
#endif
#ifdef QOSX_WANTED
#include "qosxtd.h"
#endif
#ifdef MBSM_WANTED
#include "mbsm.h"
#endif
#include "sftpc.h"
#include "hb.h"

extern INT4         gi4AuditStatus;
extern INT4         gi4ClearConfig;
extern UINT1        gau1IssSyslog1FileName[ISS_CONFIG_FILE_NAME_LEN];
extern UINT1        gau1IssSyslog2FileName[ISS_CONFIG_FILE_NAME_LEN];
extern UINT1        gau1IssSyslog3FileName[ISS_CONFIG_FILE_NAME_LEN];
extern UINT1       *EoidGetEnterpriseOid (void);
#ifdef MBSM_WANTED
extern VOID         MbsmInformRestorationComplete (VOID);
#endif

#ifdef RM_WANTED
extern tMSRRmGlobalInfo gMSRRmGlobalInfo;
#endif

UINT1               gArrayIndxcount[sizeof (gaSaveArray) /
                                    sizeof (tSaveObject)][3];
UINT1               gu1RmStConfReq = RM_FALSE;
                                   /* Global flag used to distinguish
                                    * whether the MIB save is initiated
                                    * by administrator or by standby node
                                    */
static UINT1        gau1LogMsg[ISS_MAX_LOG_MSG_LENGTH];

tMsrRegParams       gMsrRegTbl[ISS_MAX_PROTOCOLS];
/***********************************************************************/
/*                   PUBLIC FUNCTIONS OF MSR MODULE                    */
/***********************************************************************/

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MibSaveTaskMain                                  */
/*                                                                          */
/*    Description        : This is the main function of the MIB save/restore*/
/*                         task. This task waits for events and processes   */
/*                         the received events.                             */
/*                                                                          */
/*    Input(s)           : pi1Param - dummy parameter                       */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
MibSaveTaskMain (INT1 *pi1Param)
{
    tIPvXAddr          *pRestoreIp = NULL;
    UINT4               u4Event = 0;
    UINT4               u4Count = 0;
    INT4                i4Ret;
    tHttpMsgNode       *pHttpMsgNode = NULL;
    INT4                i4RetVal = FALSE;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2Port;

    /* Dummy pointers for system sizing during run time */
    tMsrMibEntryBlock  *pMsrMibEntryBlock = NULL;
    tMsrOidArrBlock    *pMsrOidArrBlock = NULL;
    tMsrOidArrListBlock *pMsrOidArrListBlock = NULL;
    tMsrRmHdrBlock     *pMsrRmHdrBlock = NULL;
    tMsrRmPktBlock     *pMsrRmPktBlock = NULL;
#ifdef RM_WANTED
    UINT4               u4RemainingStatus = OSIX_FALSE;
#endif
    UINT1               u1IssUnlockFlag = FALSE;
    UNUSED_PARAM (pMsrMibEntryBlock);
    UNUSED_PARAM (pMsrOidArrBlock);
    UNUSED_PARAM (pMsrOidArrListBlock);
    UNUSED_PARAM (pMsrRmHdrBlock);
    UNUSED_PARAM (pMsrRmPktBlock);

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_MSR_APP_ID;
    ProtoEvt.u4Error = RM_NONE;
    UNUSED_PARAM (pi1Param);
    gu4MsrTrcFlag = MSR_RM_CRITICAL_TRACE_TYPE | MSR_GENERAL_TRACE_TYPE;

    if (OsixTskIdSelf (&MsrId) != OSIX_SUCCESS)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Retrieving Task ID for MSR failed!!!\n");
        MSR_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (OsixCreateSem ((CONST UINT1 *) "MSR", 0, 0, &MsrSemId) != OSIX_SUCCESS)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Unable to create a semaphore for MSR!!!\n");
        /* Indicate the status of initialization to the main routine */
        MSR_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    if (OsixCreateSem ((CONST UINT1 *) "TFT", 0, 0, &TftSemId) != OSIX_SUCCESS)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Unable to create a semaphore for TFT!!!\n");
        MsrDeleteMsrResources ();
        /* Indicate the status of initialization to the main routine */
        MSR_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    if (OsixCreateSem ((CONST UINT1 *) "MSRU", 0, 0, &MsrUpdateQsemId) !=
        OSIX_SUCCESS)
    {
        MSR_TRC (MSR_TRACE_TYPE,
                 "Unable to create a semaphore for MSR update Q!!!\n");
        MsrDeleteMsrResources ();
        /* Indicate the status of initialization to the main routine */
        MSR_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    if (OsixCreateSem ((CONST UINT1 *) "MSRQS", 0, 0, &MsrUpdateQSsemId) !=
        OSIX_SUCCESS)
    {
        MSR_TRC (MSR_TRACE_TYPE,
                 "Unable to create a semaphore for MSR update Q status!!!\n");
        MsrDeleteMsrResources ();
        /* Indicate the status of initialization to the main routine */
        MSR_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    gu4MsrSysLogId =
        (UINT4) SYS_LOG_REGISTER ((CONST UINT1 *) "MSR", SYSLOG_CRITICAL_LEVEL);
    gi4TftpSysLogId =
        SYS_LOG_REGISTER ((CONST UINT1 *) "TFTP", SYSLOG_CRITICAL_LEVEL);

    /* The Flash Array is used for saving the configurations to the remote
     * location, and also for copying logs from remote location */
    /* The size of the array is reduced to avoid allocating a single large 
     * block of memory and the configurations are saved to the remote host
     * through multiple tftp send operations */
    if (MsrSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Memory allocation for MSR failed!!!\n");
        MsrDeleteMsrResources ();
        /* Indicate the status of initialization to the main routine */
        MSR_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    MSR_MIBENTRY_POOL_ID = MSRMemPoolIds[MAX_MSR_MIBENTRY_BLOCKS_SIZING_ID];
    gAuditQMsgPoolId = MSRMemPoolIds[MAX_MSR_Q_DEPTH_SIZING_ID];
    gMsrQUpdDataPoolId = MSRMemPoolIds[MAX_MSR_UPDATE_QUEUE_SIZE_SIZING_ID];
    gMsrQConDataPoolId = MSRMemPoolIds[MAX_MSR_CONFIG_DATA_BLOCKS_SIZING_ID];
    gMsrOidListPoolId = MSRMemPoolIds[MAX_MSR_OID_LIST_BLOCKS_SIZING_ID];
    gMsrOidArrPoolId = MSRMemPoolIds[MAX_MSR_OID_ARR_BLOCKS_SIZING_ID];
    gMsrOidArrListPoolId = MSRMemPoolIds[MAX_MSR_OID_ARR_LIST_BLOCKS_SIZING_ID];
    MSR_INCR_RB_POOL_ID = MSRMemPoolIds[MAX_MSR_INCR_RB_NODES_SIZING_ID];
    MSR_INCR_OID_POOL_ID = MSRMemPoolIds[MAX_MSR_INCR_OID_BLOCKS_SIZING_ID];
    MSR_DATA_POOL_ID = MSRMemPoolIds[MAX_MSR_DATA_BLOCKS_SIZING_ID];

    /* Allocate a block for the MSR save / restore array */
    if (MSR_MIBENTRY_ALLOC_MEM_BLOCK (&gpu1StrFlash) != MEM_SUCCESS)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Memory allocation for MSR failed!!!\n");
        MsrDeleteMsrResources ();
        /* Indicate the status of initialization to the main routine */
        MSR_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    gu4MibEntryBlkSize = MSR_MIBENTRY_MEMBLK_SIZE;

    /* The gpu1StrEnd pointer will be used for checking for access across the
     * array boundary. When gpu1StrCurrent pointer crosses gpu1StrEnd, no space 
     * is left in the gpu1StrFlash array to store data.
     */
    gpu1StrEnd = gpu1StrFlash + gu4MibEntryBlkSize;

    /* Initialize the index pools required for SNMP sets and gets */
    for (u4Count = 0; u4Count < MSR_MAX_INDICES; u4Count++)
    {
        MsrIndexPool1[u4Count].pIndex = &(MsrMultiPool1[u4Count]);
        MsrMultiPool1[u4Count].pOctetStrValue = &(MsrOctetPool1[u4Count]);
        MsrMultiPool1[u4Count].pOidValue = &(MsrOIDPool1[u4Count]);
        MsrMultiPool1[u4Count].pOctetStrValue->pu1_OctetList
            = (UINT1 *) au1MsrData1[u4Count];
        MsrMultiPool1[u4Count].pOidValue->pu4_OidList
            = (UINT4 *) (VOID *) au1MsrData1[u4Count];

        MsrIndexPool2[u4Count].pIndex = &(MsrMultiPool2[u4Count]);
        MsrMultiPool2[u4Count].pOctetStrValue = &(MsrOctetPool2[u4Count]);
        MsrMultiPool2[u4Count].pOidValue = &(MsrOIDPool2[u4Count]);
        MsrMultiPool2[u4Count].pOctetStrValue->pu1_OctetList
            = (UINT1 *) au1MsrData2[u4Count];
        MsrMultiPool2[u4Count].pOidValue->pu4_OidList
            = (UINT4 *) (VOID *) au1MsrData2[u4Count];
    }

    /* Create a Q in MSR to listen on the msgs sent by HTTP */
    if (OsixQueCrt ((UINT1 *) "MSHQ", OSIX_MAX_Q_MSG_LEN,
                    MAX_MSR_Q_DEPTH, &MsrHttpQId) != OSIX_SUCCESS)
    {
        MsrDeleteMsrResources ();

        MSR_TRC (MSR_TRACE_TYPE,
                 "Creation of Msr Q Failed in MibSaveTaskMain\n");
        /* Indicate the status of initialization to the main routine */
        MSR_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* create the timer lists for MSR notification */
    if (TmrCreateTimerList ((const UINT1 *) MSR_TASK_NAME,
                            MSR_SYS_DEF_IFUP_TMR_EXP_EVT,
                            NULL, &gMsrDefSysIfUpTmrId) != TMR_SUCCESS)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Exiting MibSaveTaskMain - create timer"
                 "list for system default interface up timer fail\n");
        MsrDeleteMsrResources ();
        /* Indicate the status of initialization to the main routine */
        MSR_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (TmrCreateTimerList ((const UINT1 *) MSR_TASK_NAME,
                            MSR_REMOTE_RESTORE_EVENT,
                            NULL, &gIssMsrTimerListId) != TMR_SUCCESS)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Exiting MibSaveTaskMain - create timer"
                 "list for remote restore fail\n");
        MsrDeleteMsrResources ();
        /* Indicate the status of initialization to the main routine */
        MSR_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    /* Create a Q in MSR to listen on the msgs sent for Audit */
    if (OsixQueCrt ((UINT1 *) MSR_Q_AUDIT, OSIX_MAX_Q_MSG_LEN,
                    MAX_MSR_Q_DEPTH, &gAuditQId) != OSIX_SUCCESS)
    {
        MsrDeleteMsrResources ();
        MSR_TRC (MSR_TRACE_TYPE,
                 "Creation of Msr AUDIT Q Failed in MibSaveTaskMain\n");
        MSR_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

#ifdef RM_WANTED
    if (MsrRmGlobalInit () == OSIX_FAILURE)
    {
        MSR_TRC (MSR_RM_CRITICAL_TRACE_TYPE | ALL_FAILURE_TRC,
                 "MSR RM Q creation failed !!!\r\n");
        MsrDeleteMsrResources ();
        /* Indicate the status of initialization to the main routine */
        MSR_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (MsrRmInit () == OSIX_FAILURE)
    {
        MSR_TRC (MSR_TRACE_TYPE, "MSR Registration with RM failed!!!\n");
        MsrRmGlobalDeInit ();
        MsrDeleteMsrResources ();
        /* Indicate the status of initialization to the main routine */
        MSR_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
#endif
    /* Create a Q in MSR to listen on the msgs sent by SNMP agent as part of update event */

    if (OsixQueCrt ((UINT1 *) MSR_SNMPQ_NAME, OSIX_MAX_Q_MSG_LEN,
                    MSR_MAX_UPDATE_QUEUE_SIZE, &MsrsnmpQId) != OSIX_SUCCESS)
    {
        MSR_TRC (MSR_TRACE_TYPE,
                 "Creation of Msr SNMP Q Failed in MibSaveTaskMain\n");
        MsrDeleteMsrResources ();
#ifdef RM_WANTED
        MsrRmDeInit ();
        MsrRmGlobalDeInit ();
#endif
        /* Indicate the status of initialization to the main routine */
        MSR_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    MsrisRetoreinProgress = ISS_FALSE;
#ifdef MBSM_WANTED
    if (MbsmGetBootFlag () != MBSM_TRUE)
    {
        MsrisRetoreinProgress = ISS_TRUE;
    }
#endif

    /* The flag used to indicate whether the Rbtree is populated with all
     * the initial configurations (system default configurations) in the 
     * incremental save mode.*/
    /* By default it is set to false and it is reset to true when the
     * Rbtree is fully populated by restoration Or by reading the default
     * configurations when the system comes up without any restoration */
    MsrisSaveComplete = ISS_FALSE;

    /* Update the Restore File Version Oid */
    SPRINTF ((CHR1 *) au1RestoreFileVersionOid,
             ISS_FILE_VERSION_OID_BEFORE_ENT_ID "%s"
             ISS_FILE_VERSION_OID_AFTER_ENT_ID, EoidGetEnterpriseOid ());

    if (gIssSysGroupInfo.IssIncrSaveFlag == ISS_TRUE)
    {
        gMSRIncrSaveFlag = ISS_TRUE;
    }
    else
    {
        gMSRIncrSaveFlag = ISS_FALSE;
    }
    /* Create the RbTree and the mem pool for allocation of
     * node to the Rbtree.
     * The RbTree and mempool are created only in the incremental save mode */

    if (gMSRIncrSaveFlag == ISS_TRUE)
    {
        /*Create a Rbtree to save the configurations done in the protocol 
         * in the incremental save on mode */
        gMSRRBTable =
            RBTreeCreateEmbedded (MSR_RBNODE_OFFSET (tMSRRbtree, RBNode),
                                  (tRBCompareFn) MSRCompareRBNodes);

        if (gMSRRBTable == NULL)
        {
            MSR_TRC (MSR_TRACE_TYPE,
                     "Creation of RBTree Failed in MibSaveTaskMain\n");
            MsrDeleteMsrResources ();
#ifdef RM_WANTED
            MsrRmDeInit ();
            MsrRmGlobalDeInit ();
#endif
            /* Indicate the status of initialization to the main routine */
            MSR_INIT_COMPLETE (OSIX_FAILURE);
            return;
        }
        if (MsrAllocateIncrPools () == MSR_FAILURE)
        {
            MSR_TRC (MSR_TRACE_TYPE,
                     "Allocation of mempools for RB nodes failed!!!\n");
            MsrDeleteMsrResources ();
#ifdef RM_WANTED
            MsrRmDeInit ();
            MsrRmGlobalDeInit ();
#endif
            /* Indicate the status of initialization to the main routine */
            MSR_INIT_COMPLETE (OSIX_FAILURE);
            return;
        }

        if (MSRInsertVerOrFormatInfoToRBTree () == MSR_FAILURE)
        {
            MSR_TRC (MSR_TRACE_TYPE,
                     "Inserting the Version Info node to RBtree failed\n");
            MsrDeleteMsrResources ();
#ifdef RM_WANTED
            MsrRmDeInit ();
            MsrRmGlobalDeInit ();
#endif
            /* Indicate the status of initialization to the main routine */
            MSR_INIT_COMPLETE (OSIX_FAILURE);
            return;
        }

        /* initialize the counter used for automatic save of configurations */
        gu4IncrLength = 0;

        if (gIssConfigSaveInfo.IssAutoConfigSave == ISS_TRUE)
        {
            /* When the incremental save and auto save option
             * is enabled then by default, the Config Save option
             * is set to startup save. 
             * If the config save option is saved in the configuration file,
             * then the saved value is used for further configurations.*/
            gIssConfigSaveInfo.IssConfigSaveOption = ISS_CONFIG_STARTUP_SAVE;
        }
    }

    /* Indicate the status of initialization to the main routine */
    MSR_INIT_COMPLETE (OSIX_SUCCESS);
    MsrUpdateQCountUnLock ();
    MsrUpdateQStatusUnLock ();

#ifdef ISS_WANTED
#if ! defined (MBSM_WANTED) && ! defined (RM_WANTED)
    /* Triggers the configuration restore from here */

    IssCustMsrInit ();
#endif /* # !RM_WANTED and !MSBM_WANTED */
#if defined (MBSM_WANTED) && ! defined (RM_WANTED)
    {
        if (MbsmGetBootFlag () == MBSM_TRUE)
        {
            IssCustMsrInit ();
        }
    }
#endif /* MBSM_WANTED && ! defined (RM_WANTED) */
#endif /* #ifdef ISS_WANTED */
    /*calculation of repeated OID and row status in gSaveArray */
    InitMSRArryOIDStatus ();

    /* MSR main wait loop : checks for receipt of any events */
    while (1)
    {
        if (OsixEvtRecv (MsrId, MIB_SAVE_GEN_MASK,
                         OSIX_WAIT, &u4Event) == OSIX_SUCCESS)
        {
#ifdef RM_WANTED
            if (u4Event & MSR_RM_MESSAGE)
            {
                MsrProcessRmEvt ();
            }

            if (u4Event & MSR_RM_GO_ACTIVE)
            {
                MsrProcessRmGoActive ();
            }

            if (u4Event & MSR_RM_GO_STANDBY)
            {
                MsrProcessRmGoStandby ();
            }
#ifdef L2RED_WANTED
            if (u4Event & MSR_RM_FILE_TRANSFER_COMPLETE)
            {
                MsrProcessRmFileTransferComplete ();
            }
#endif
            if (u4Event & MSR_RM_NEW_CONNECTION)
            {
                if (gu1MsrNodeStatus == RM_ACTIVE)
                {
                    MsrRmProcessNewConnEvent ();
                }
            }

            if (u4Event & MSR_RM_PACKET_ARRIVAL)
            {
                /* Event triggered when a new packet arrives */
                MsrRmProcessIncomingPacket ();
            }

            if (u4Event & MSR_RM_SUB_BULK_UPDATE)
            {
                if (gu1MsrNodeStatus == RM_ACTIVE)
                {
                    /* Event triggered by MSR to send the next set of static
                     * bulk updates
                     */
                    MsrRmSendStaticBulkUpdate ();
                }
            }

            if (u4Event & MSR_RM_SEND_REM_UPDATE)
            {
                if (gu1MsrNodeStatus == RM_ACTIVE)
                {
                    /* Event triggered to send the remaining packet which
                     * resulted due to unsuccessful socket write
                     */
                    u4RemainingStatus = OSIX_FALSE;
                    MsrRmSendRemainingUpdate (&u4RemainingStatus);
                    while (u4RemainingStatus == OSIX_TRUE)
                    {
                        if (OsixEvtRecv
                            (gMsRmTaskId, MSR_RM_SEND_REM_UPDATE, OSIX_WAIT,
                             &u4Event) == OSIX_SUCCESS)
                        {
                            MsrRmSendRemainingUpdate (&u4RemainingStatus);
                        }
                    }

                }
            }
            if (u4Event & MSR_RM_TMR_EVENT)
            {
                MsrRmTmrHandleExpiry ();
            }
#endif

#ifdef MBSM_WANTED
            /* Restore operation is done only after interface creation in CFA 
             * RM GO_ACTIVE*/
            if (u4Event & CFA_BOOT_COMPLETE_EVENT)
            {
                /* If gu1MsrNodeStatus is in INIT state,
                 * MSR Initaialisation should get started */
                gu1CfaBootFlag = MSR_TRUE;
#ifdef RM_WANTED
                if (gu1MsrNodeStatus == RM_ACTIVE)
                {
                    IssCustMsrInit ();
                }
                else if (gu1MsrNodeStatus == RM_STANDBY)
                {
                    if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
                    {
                        gi4MibResStatus = MIB_RESTORE_NOT_INITIATED;

                        ISSSetHealthConfigRestoreStatus ((UINT1)
                                                         (gi4MibResStatus));
                        /*If the gi4MibResStatus is failed, update the ISSHealthErrReason */
                        if (LAST_MIB_RESTORE_FAILED == gi4MibResStatus)
                        {
                            ISSSetHealthChkStatus
                                (ISS_UP_RECOVERABLE_RUNTIME_ERR);
                            ISSSetHealthChkErrReason
                                (RECOV_CONFIG_RESTORE_FAILED);
                        }

                        MsrSendRestorationCompleteToProtocols ();
                        MsrResumeUserConfiguration ();
                    }
                }
#else
                /* In chassis case, MSR Initialisation */
                IssCustMsrInit ();
#endif
            }
#endif /* MBSM_WANTED */
            if ((u4Event & MSR_SYS_DEF_IFUP_TMR_EXP_EVT)
#ifdef RM_WANTED
                && (gu1MsrNodeStatus != RM_STANDBY)    /* Dont send restoration 
                                                       completion trap.
                                                       This will 
                                                       happen in STANDBY node.
                                                       Hence block it. */
#endif
                )
            {
                /* Send the trap on timer expiry */
                MsrTrapSendNotifications (&gNotifMsg);
#ifdef SNMP_3_WANTED
                SnmpSendColdStartTrap ();
#endif
                /* Delay is introduced to send Trap packets after
                 *  Arp response arrives for arp request.Till then
                 *  it will be hold in arp enqueue in PENDING state */
                OsixTskDelay (MSR_DELAY);

                for (u2Port = 1; u2Port <= SYS_DEF_MAX_PHYSICAL_INTERFACES;
                     u2Port++)
                {
                    CfaCheckStatusAndSendTrap (u2Port);
                }
            }

#ifdef SNMP_2_WANTED
            /*new event for SNMP update trigger */
            if (u4Event & MSR_UPDATE_EVENT)
            {
                MsrProcessUpdateEvent ();
            }
            if (u4Event & MIB_SAVE_EVENT)
            {
                gu1MibSaveLocal = MSR_TRUE;
                if (NULL !=
                    MSR_CALLBACK[MSR_SECURE_PROCESS].pMsrCustSecureStorage)
                {
                    MSR_CALLBACK[MSR_SECURE_PROCESS].pMsrCustSecureStorage
                        (ISS_SECURE_MEM_INITIATE, ISS_MIB_OBJECT);
                }
#ifdef RM_WANTED
                if (gu1MsrNodeStatus == RM_STANDBY)
                {
                    /* Release IssLock, as the lock is taken while
                     * doing MIBSave.
                     */
                    u1IssUnlockFlag = TRUE;
                    IssUnLock ();
                }
#endif

                i4RetVal = MsrProcessMibSaveEvt ();
                if (u1IssUnlockFlag == TRUE)
                {
                    IssLock ();
                    u1IssUnlockFlag = FALSE;
                }

#ifdef RM_WANTED
                if (i4RetVal == TRUE)
                {
                    /* Configuration is saved sucessfully.
                     * Set the restore option to FlashRestore. */
                    gIssConfigRestoreInfo.IssConfigRestoreOption =
                        ISS_CONFIG_RESTORE;

                    /* Call the NvRam Write routine to change the 
                     * RestoreOption & RestoreFlag */
                    IssSetRestoreOptionToNvRam (ISS_CONFIG_RESTORE);
                    IssSetRestoreFlagToNvRam (SET_FLAG);
                }
                if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
                {
                    ProtoEvt.u4Event = RM_PROTOCOL_SEND_EVENT;
                    ProtoEvt.u4SendEvent = RM_SEND_TO_FILES_PEERS;
                    RmApiHandleProtocolEvent (&ProtoEvt);
                }
#else
                UNUSED_PARAM (i4RetVal);
#endif /* RM_WANTED */

            }
            if (u4Event & MIB_RESTORE_EVENT)
            {
                MsrisRetoreinProgress = ISS_TRUE;
                gu1MibRestoreLocal = MSR_TRUE;
                /*Updated the ISS health ConfigRestore status as Restore in progress */
                ISSSetHealthConfigRestoreStatus
                    (ISS_CONFIG_RESTORE_IN_PROGRESS);

#ifdef RM_WANTED
                if (gu1MsrNodeStatus == RM_ACTIVE)
                {
                    MsrProcessMibRestoreEvent ();

                    ISSSetHealthConfigRestoreStatus ((UINT1) (gi4MibResStatus));
                    /*If the gi4MibResStatus is failed, update the ISSHealthErrReason */
                    if (LAST_MIB_RESTORE_FAILED == gi4MibResStatus)
                    {
                        ISSSetHealthChkStatus (ISS_UP_RECOVERABLE_RUNTIME_ERR);
                        ISSSetHealthChkErrReason (RECOV_CONFIG_RESTORE_FAILED);
                    }
                    MsrSendRestorationCompleteToProtocols ();
                    MsrResumeUserConfiguration ();
                }
#else
                MsrProcessMibRestoreEvent ();
                if ((LAST_MIB_RESTORE_SUCCESSFUL == gi4MibResStatus) &&
                    (NULL !=
                     MSR_CALLBACK[MSR_SECURE_PROCESS].pMsrCustSecureStorage))
                {
                    MSR_CALLBACK[MSR_SECURE_PROCESS].pMsrCustSecureStorage
                        (ISS_SECURE_MEM_INITIATE, ISS_FALSE);
                    MSR_CALLBACK[MSR_SECURE_PROCESS].pMsrCustSecureStorage
                        (ISS_SECURE_MEM_RESTORE, ISS_MIB_OBJECT);
                }

                MsrSendRestorationCompleteToProtocols ();
                MsrResumeUserConfiguration ();
                /*Updated the ISS health ConfigRestore status */
                ISSSetHealthConfigRestoreStatus ((UINT1) (gi4MibResStatus));
                /*If the gi4MibResStatus is failed, update the ISSHealthErrReason */
                if (LAST_MIB_RESTORE_FAILED == gi4MibResStatus)
                {
                    ISSSetHealthChkStatus (ISS_UP_RECOVERABLE_RUNTIME_ERR);
                    ISSSetHealthChkErrReason (RECOV_CONFIG_RESTORE_FAILED);
                }

#endif
                MsrisRetoreinProgress = ISS_FALSE;
            }
            if (u4Event & MIB_SAVE_TO_REMOTE_EVENT)
            {
                gu1MibSaveLocal = MSR_FALSE;
#ifdef RM_WANTED
                if (gu1MsrNodeStatus == RM_STANDBY)
                {
                    /* Release IssLock, as the lock is taken while
                     * doing MIBSave.
                     */
                    u1IssUnlockFlag = TRUE;
                    IssUnLock ();
                }
#endif

                MsrProcessMibSaveEvt ();
                if (u1IssUnlockFlag == TRUE)
                {
                    IssLock ();
                    u1IssUnlockFlag = FALSE;
                }

            }
            if (u4Event & MIB_RESTORE_FROM_REMOTE_EVENT)
            {
                /* For Syslog */
                MsrisRetoreinProgress = ISS_TRUE;
                /*Updated the ISS health ConfigRestore status as Restore in progress */
                ISSSetHealthConfigRestoreStatus
                    (ISS_CONFIG_RESTORE_IN_PROGRESS);
                pRestoreIp = IssGetRestoreIpAddrFromNvRam ();
                if ((i4Ret =
                     tftpcRecvFileToBuf (*pRestoreIp,
                                         (const UINT1 *)
                                         IssGetRestoreFileNameFromNvRam (),
                                         gpu1StrFlash,
                                         (gpu1StrEnd - gpu1StrFlash),
                                         &gu4Length)) != TFTPC_OK)
                {
                    gi4MibResStatus = LAST_MIB_RESTORE_FAILED;

                    SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gu4MsrSysLogId,
                                  "Configuration restore from remote host "
                                  "failed [TFTP:%s]", TFTPC_ERR_MSG (i4Ret)));
                }
                else
                {
                    gu1MibRestoreLocal = MSR_FALSE;
                    /* Delete the default VLAN index once we have downloaded the
                     * restoration file */
                    VlanDeleteDefaultVlan ();
                    CfaSetIfMainRowStatus (CFA_DEFAULT_ROUTER_VLAN_IFINDEX,
                                           DESTROY);
                    MsrProcessMibRestoreEvent ();
                    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4MsrSysLogId,
                                  "Successfully restored configurations"
                                  "from remote host"));
                }
                MsrisRetoreinProgress = ISS_FALSE;
                MsrSendRestorationCompleteToProtocols ();
                MsrResumeUserConfiguration ();
                /*Updated the ISS health ConfigRestore status */
                ISSSetHealthConfigRestoreStatus ((UINT1) gi4MibResStatus);
                /*If the gi4MibResStatus is failed, update the ISSHealthErrReason */
                if (LAST_MIB_RESTORE_FAILED == gi4MibResStatus)
                {
                    ISSSetHealthChkStatus (ISS_UP_RECOVERABLE_RUNTIME_ERR);
                    ISSSetHealthChkErrReason (RECOV_CONFIG_RESTORE_FAILED);
                }

            }
            if (u4Event & MIB_ERASE_EVENT)
            {
                MsrProcessMibEraseEvent ();
                ISSSetHealthConfigRestoreStatus (ISS_CONFIG_RESTORE_DEFAULT);
            }
            if (u4Event & MSR_SYNCUP_EVENT)
            {
                MsrProcessSyncSaveEvent (MSR_SYNCUP_EVENT);
            }
#endif
            /* ISS restart */
            if (u4Event & ISS_RESTART_EVENT)
            {
                /* The delay is introduced to enable SNMP to send the 
                 * response for ISS restart to the WebNM/SNMP manager.
                 */
                OsixTskDelay (MSR_DELAY);
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gu4MsrSysLogId,
                              "System is rebooting...!!!"));
                IssSystemRestart ();
            }
            /* ISS restart on ISSU */
            if (u4Event & ISS_LOAD_VERSION_RESTART_EVENT)
            {
                /* The delay is introduced to enable SNMP to send the 
                 * response for ISS restart to the WebNM/SNMP manager.
                 */
                OsixTskDelay (MSR_DELAY);
                /* In case of failure on Loadversion Api , return without 
                 * rebooting the system */
                if (IssuApiPerformLoadVersionNPCmds () == OSIX_FAILURE)
                {
                    continue;
                }
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gu4MsrSysLogId,
                              "System is rebooting...!!!"));
                IssSystemRestart ();
            }
            if (u4Event & ISS_IMAGE_DOWNLOAD_EVENT)
            {
                /* Start Downloading using tftp or sftp */
                IssImageDownload (NULL);
            }
            if (u4Event & ISS_UPLOAD_LOG_EVENT)
            {
                IssUploadLogs (NULL);
            }
            if (u4Event & ISS_UPLOAD_FILE_EVENT)
            {
                IssUploadFile (NULL);
            }
            if (u4Event & ISS_DWLOAD_FILE_EVENT)
            {
                IssDownloadFile (NULL);
            }

#ifdef WEBNM_WANTED
            if (u4Event & HTTP_MULTI_DATA_EVENT)
            {
                if (OsixQueRecv (MsrHttpQId, (UINT1 *) &pHttpMsgNode,
                                 OSIX_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
                {
                    HttpHandleUpgradeFromMSR (pHttpMsgNode);
                }
            }
#else
            UNUSED_PARAM (pHttpMsgNode);
#endif
            if (u4Event & AUDIT_EVENT)
            {
                MsrProcessAuditLogEvent ();

            }

            if (u4Event & SYSLOG_FILEWRITE_EVENT)
            {
                MsrProcessSysLogEvent ();
            }
            if (u4Event & MSR_REMOTE_RESTORE_EVENT)
            {
                MsrRemoteRestore ();
                /*Updated the ISS health ConfigRestore status */
                ISSSetHealthConfigRestoreStatus ((UINT1) gi4MibResStatus);
                /*If the gi4MibResStatus is failed, update the ISSHealthErrReason */
                if (gi4MibResStatus == LAST_MIB_RESTORE_FAILED)
                {
                    SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gu4MsrSysLogId,
                                  "Configuration restore from remote host failed"));
                    MsrSendRestorationCompleteToProtocols ();
                    MsrResumeUserConfiguration ();
                    ISSSetHealthChkStatus (ISS_UP_RECOVERABLE_RUNTIME_ERR);
                    ISSSetHealthChkErrReason (RECOV_CONFIG_RESTORE_FAILED);
                }
            }

            /* MSR_RM_STANDBY_DOWN_EVT is added to stop the static bulk
             * update process when the peer node has gone down. This 
             * event is received here without any processing so that, 
             * when the msr task performing the static bulk update fails 
             * because of socket errors before processing this event, 
             * the event is received here and it not does not stop the
             * next static bulk update process. When a separate task is 
             * spawned to perform the static bulk update, then this event
             * will never be sent to the msr task. */

        }                        /* event check */

        /* Mib Restoration failed */

        if (LAST_MIB_RESTORE_FAILED == gi4MibResStatus)
        {
            if ((u4Event & MIB_RESTORE_EVENT) ||
                (u4Event & MIB_RESTORE_FROM_REMOTE_EVENT))
            {
#ifdef BRIDGE_WANTED
                if (nmhSetDot1dBaseBridgeStatus (UP) != SNMP_SUCCESS)
                {
                    MSR_TRC (MSR_TRACE_TYPE,
                             "ERROR: Enabling Bridge. Failed!!\r\n");
                }
#endif

            }
        }
    }                            /* end of while */
}                                /* end of function */

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrInitRestoreFlag                               */
/*                                                                          */
/*    Description        : This function initialises the restore flag in    */
/*                         the NVRAM. The default value of the restore flag */
/*                         is 0 (UNSET_FLAG).                               */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
MsrInitRestoreFlag ()
{
    IssSetRestoreFlagToNvRam (UNSET_FLAG);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrInitSaveFlag                                  */
/*                                                                          */
/*    Description        : This function initialises the save flag in the   */
/*                         NVRAM. The default value of the save flag        */
/*                         is 0 (UNSET_FLAG).                               */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
MsrInitSaveFlag ()
{
    IssSetSaveFlagToNvRam (UNSET_FLAG);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrStoreSaveFlagNvRam                            */
/*                                                                          */
/*    Description        : This function is called after saving the current */
/*                         configuration to the flash. It sets the save flag*/
/*                         to indicate that configuration is present in the */
/*                         flash.                                           */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
MsrStoreSaveFlagNvRam ()
{
    IssSetSaveFlagToNvRam (ISS_CONFIG_STARTUP_SAVE);
}

#ifdef SNMP_2_WANTED
/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrProcessMibRestoreEvent                        */
/*                                                                          */
/*    Description        : This function is invoked when a MIB_RESTORE_EVENT*/
/*                         or a MIB_RESTORE_FROM_REMOTE event is received.  */
/*                         It processes the received events.                */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
MsrProcessMibRestoreEvent ()
{
    INT1                i1ResFlag = 0;
    UINT4               u4FileVersion = 0;
    INT4                i4Retval = 0;
    INT4                i4ConfFd;
    CHR1                ac1Time[24];
    UINT1               u1AltConfFile = MSR_FALSE;
    INT4                i4ResType = 0;

    MEMSET (&gNotifMsg, 0, sizeof (tMsrNotifyMsg));
    UtilSetChkSumLastStat (0);
    /* Get Restoration Type - whether CSR or CSR */
    i4ResType = IssGetRestoreTypeFromNvRam ();
    if (i4ResType != SET_FLAG)
    {

        if (gu1MibRestoreLocal == MSR_TRUE)
        {
            /* Configuration must be restored from flash. So,
             * check if configuration is present in the flash. 
             */
            i1ResFlag = IssGetRestoreFlagFromNvRam ();

            if (i1ResFlag == SET_FLAG)
            {
                MSR_TRC (MSR_TRACE_TYPE, "Trying to restore MIB\n");

                if (MsrValidateVersionAndCheckSum (&u4FileVersion,
                                                   &u1AltConfFile) ==
                    MSR_FAILURE)
                {
                    gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                    return;
                }

                if ((u4FileVersion == MSR_TRUE)
                    || (gMSRIncrSaveFlag == ISS_FALSE))
                {
                    UtlGetTimeStr (ac1Time);
                    MSR_TRC_ARG1 (MSR_TRACE_TYPE, "%s Restoration "
                                  "initiated\r\n", ac1Time);
                    /* Old version MSR conf file. So file contains
                       <OID> <data type> <value> fields only */

                    /* Read the configurations from the file and
                     * restore it. */
                    if (u1AltConfFile == MSR_FALSE)
                    {
                        if (MsrObtainFilePointer (&i4ConfFd) != ISS_SUCCESS)
                        {
                            MSR_TRC (MSR_TRACE_TYPE, "Unable to open original"
                                     "restoration file!!!\n");
                            gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                            return;
                        }
                    }
                    else
                    {
                        if (MsrObtainAltResFilefd (&i4ConfFd) != ISS_SUCCESS)
                        {
                            MSR_TRC (MSR_TRACE_TYPE, "Unable to open alternate"
                                     "restoration file!!!\n");
                            gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                            return;
                        }
                    }
                    if (SnmpApiGetSNMPUpdateDefault () == OSIX_TRUE)
                    {
                        SNMPV3DefaultDelete ();
                    }
                    if (MsrMibRestore (i4ConfFd) == MSR_FAILURE)
                    {
                        MSR_TRC (MSR_TRACE_TYPE, "MIB restore failed!!!\n");
                        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                        SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                                      "Restore from Conf file failed...!!!"));
                        return;
                    }
                    /* Set the Notification information */
                    gNotifMsg.u4ConfigRestoreStatus = (UINT4) gi4MibResStatus;
                    /* In the incremental save on mode, after restoration,
                     * the Rbtree is populated by reading the protocol
                     * database */
                    if (gMSRIncrSaveFlag == ISS_TRUE)
                    {
                        /* now save the data into RB tree by reading from the
                         * protocols */
                        i4Retval = MsrSaveMIB ();

                        if (i4Retval == MSR_FALSE)
                        {
                            MSR_TRC (MSR_TRACE_TYPE, "MIB Save failed during"
                                     "the restoration of old conf file!!!\n");
                        }
                        else
                        {
                            /* Update the iss.conf file to be in new format
                             * if both Incremental Save and Auto Save flag is
                             * on.
                             */
                            if (gIssConfigSaveInfo.IssAutoConfigSave ==
                                ISS_TRUE)
                            {
                                /* Processing issinc.conf file when Incremental
                                 * and Auto Save flag is enabled.*/

                                MSRUpdateRBtreeWithIncrFile ();
                                MsrisSaveComplete = ISS_TRUE;

                                if (gi4MibSaveStatus == MIB_SAVE_IN_PROGRESS)
                                {
                                    i4Retval = MSR_SUCCESS;
                                }
                                i4Retval = MsrSaveDatatoIncrFile
                                    (NULL, MSR_INCREMENTAL_SAVE_BUFF_SIZE);

                                if (i4Retval != MSR_SUCCESS)
                                {
                                    MSR_TRC (MSR_TRACE_TYPE,
                                             "Saving the configurations to the "
                                             "conf file in new format failed!!!\n");
                                    SYS_LOG_MSG
                                        ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                                          "Saving the configurations to the "
                                          "conf file in new format failed!!!\n"));
                                }
                            }
                        }
                    }
                    UtlGetTimeStr (ac1Time);
                    MSR_TRC_ARG1 (MSR_GENERAL_TRACE_TYPE, "%s Restoration "
                                  "successfully completed\r\n", ac1Time);
                }
                else
                {
                    UtlGetTimeStr (ac1Time);
                    MSR_TRC_ARG1 (MSR_TRACE_TYPE, "%s Restoration "
                                  "initiated\r\n", ac1Time);
                    if (u1AltConfFile == MSR_FALSE)
                    {
                        if (MsrObtainFilePointer (&i4ConfFd) != ISS_SUCCESS)
                        {
                            MSR_TRC (MSR_TRACE_TYPE, "Unable to open original"
                                     "restoration file!!!\n");
                            gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                            return;
                        }
                    }
                    else
                    {
                        if (MsrObtainAltResFilefd (&i4ConfFd) != ISS_SUCCESS)
                        {
                            MSR_TRC (MSR_TRACE_TYPE, "Unable to open alternate"
                                     "restoration file!!!\n");
                            gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                            return;
                        }
                    }
                    /* Before restoration only the ISS_RESTORE_FILE_VERSION_OID
                     * will be present in the RBTree. Hence no need to delete
                     * the RBtree nodes */
                    if (MSRPopulateRBtreefromISSConf (i4ConfFd) == ISS_FAILURE)
                    {
                        /* reading configuration file failure return failure 
                         * and if any node present in the RB tree delete them*/
                        MSRDeleteAllRBNodesinRBTree ();
                        MSR_TRC (MSR_TRACE_TYPE, "MIB restore failed!!!\n");
                        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                        gi4MibResFailureReason =
                            MIB_RES_RBTREE_POPULATION_FAILURE;
                        SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                                      "MSR RBTree population failed during restoration...!!!"));
                    }
                    else
                    {
                        /* Change the state of RAM. */
                        gu1RamState = MSR_RAM_IN_SYNC_WITH_FLASH;

                        if ((gMSRIncrSaveFlag == ISS_TRUE) &&
                            (gIssConfigSaveInfo.IssAutoConfigSave == ISS_TRUE))
                        {
                            MSRUpdateRBtreeWithIncrFile ();
                        }
                        if (SnmpApiGetSNMPUpdateDefault () == OSIX_TRUE)
                        {
                            SNMPV3DefaultDelete ();
                        }
                        MSRMibRestorefromRBtree ();
                        /* Set the Notification information */
                        gNotifMsg.u4ConfigRestoreStatus =
                            (UINT4) gi4MibResStatus;
                        MsrisSaveComplete = ISS_TRUE;

                        if ((gMSRIncrSaveFlag == ISS_TRUE) &&
                            (gIssConfigSaveInfo.IssAutoConfigSave == ISS_TRUE))
                        {
                            /* update the configuration data present in the
                             * incremental file to the iss.conf file */
                            if ((gIssConfigSaveInfo.IssConfigSaveOption ==
                                 ISS_CONFIG_FLASH_SAVE) ||
                                (gIssConfigSaveInfo.IssConfigSaveOption ==
                                 ISS_CONFIG_STARTUP_SAVE))
                            {
                                if (gi4MibSaveStatus == MIB_SAVE_IN_PROGRESS)
                                {
                                    i4Retval = MSR_SUCCESS;
                                }
                                i4Retval = MsrSaveDatatoIncrFile
                                    (NULL, MSR_INCREMENTAL_SAVE_BUFF_SIZE);

                            }
                            /* With the autosave on for remote save operation,
                             * the incremental configurations are saved with 
                             * the MSR task and only on filling of the
                             * incremental buffer the configurations are saved
                             * to the remote host. Hence, there is no 
                             * incremental conf file in the remote host */
                        }
                        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4MsrSysLogId,
                                      "Restored configuration from flash successfully!"));
                    }
                    UtlGetTimeStr (ac1Time);
                    MSR_TRC_ARG1 (MSR_GENERAL_TRACE_TYPE,
                                  "%s Restoration successfully"
                                  " completed \r\n", ac1Time);
                }
            }
            else
            {
                MSR_TRC (MSR_TRACE_TYPE,
                         "MIB not restored. Restore flag is FALSE!\n");
                gi4MibResFailureReason = MIB_RES_FLAG_UNSET;
                gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                              "Flash empty or not set to restore configurations!"));
            }
        }
        else
        {
            /* Configuration must be restored from remote */
            /* Currently Remote Restoration is not supported
             * by ISS */
            /* For Remote Restoration, the configuration present in the remote
             * conf file has to be read using smaller buffer and then restored */

            /* Remote restore from alternate restoration file is not supported */

            /* Before restoration only the ISS_RESTORE_FILE_VERSION_OID
             * will be present in the RBTree. Hence no need to delete the 
             * RBtree nodes */
            if (MsrValidRemFileCheckSum () == ISS_FAILURE)
            {
                gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                gNotifMsg.u4ConfigRestoreStatus = (UINT4) gi4MibResStatus;
                SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                              "Checksum verification failed for remote file!!!"));
                return;
            }
            if (gMSRIncrSaveFlag == ISS_FALSE)
            {
                if (SnmpApiGetSNMPUpdateDefault () == OSIX_TRUE)
                {
                    SNMPV3DefaultDelete ();
                }
                if (MSRRestoreFromRestoreArray () == ISS_FAILURE)
                {

                    MSR_TRC (MSR_TRACE_TYPE, "MIB remote restore failed!!!\n");
                    gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                    SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                                  "Restore from remote file failure...!!!"));
                    return;
                }
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4MsrSysLogId,
                              "Restored configuration from"
                              "remote file successfully!"));
            }
            else
            {
                if (MSRPopulateRBtreefromFlashArrray () == ISS_FAILURE)
                {
                    MSR_TRC (MSR_TRACE_TYPE, "MIB restore failed!!!\n");
                    gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                    SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                                  "Populating RB tree from remote file failed!!!"));
                }
                else
                {
                    /* Change the state of RAM. */
                    gu1RamState = MSR_RAM_IN_SYNC_WITH_FLASH;
                    if (SnmpApiGetSNMPUpdateDefault () == OSIX_TRUE)
                    {
                        SNMPV3DefaultDelete ();
                    }
                    MSRMibRestorefromRBtree ();
                    /* Set the Notification information */
                    gNotifMsg.u4ConfigRestoreStatus = (UINT4) gi4MibResStatus;
                    MsrisSaveComplete = ISS_TRUE;

                    if ((gMSRIncrSaveFlag == ISS_TRUE) &&
                        (gIssConfigSaveInfo.IssAutoConfigSave == ISS_TRUE))
                    {
                        /* update the configuration data present in the
                         * incremental file to the iss.conf file */
                        if ((gIssConfigSaveInfo.IssConfigSaveOption ==
                             ISS_CONFIG_FLASH_SAVE) ||
                            (gIssConfigSaveInfo.IssConfigSaveOption ==
                             ISS_CONFIG_STARTUP_SAVE))
                        {
                            if (gi4MibSaveStatus == MIB_SAVE_IN_PROGRESS)
                            {
                                i4Retval = MSR_SUCCESS;
                            }
                            i4Retval = MsrSaveDatatoIncrFile
                                (NULL, MSR_INCREMENTAL_SAVE_BUFF_SIZE);

                        }
                        /* With the autosave on for remote save operation,
                         * the incremental configurations are saved with 
                         * the MSR task and only on filling of the
                         * incremental buffer the configurations are saved
                         * to the remote host. Hence, there is no 
                         * incremental conf file in the remote host */
                    }
                    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4MsrSysLogId,
                                  "Restored configuration from remote file successfully!"));
                }
            }
        }
    }
#ifdef CLI_WANTED
    else
    {
        UtlGetTimeStr (ac1Time);
        MSR_TRC_ARG1 (MSR_TRACE_TYPE, "%s Restoration "
                      "initiated\r\n", ac1Time);

        /* when the restoration type is CLI based, list of CLI commands
         * will be executed from the iss.conf file */
        if (CSRCliExecuteCmdFromFile () != CSR_SUCCESS)
        {
            MSR_TRC (MSR_TRACE_TYPE, "MIB restore failed!!!\n");
            gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                          "Populating RB tree from remote file failed!!!"));
            return;
        }
        /* Restore operation completed successfully */
        gi4MibResStatus = LAST_MIB_RESTORE_SUCCESSFUL;

        UtlGetTimeStr (ac1Time);
        MSR_TRC_ARG1 (MSR_TRACE_TYPE, "%s Restoration successfully"
                      " completed \r\n", ac1Time);

    }
#endif
    /* Since at system restart, only if the trap manager configurations
     * are restored, traps will go out. Hence initiate traps only if the
     * configurations are successfully restored
     * - Send Link up/down traps next 
     */
    if (LAST_MIB_RESTORE_SUCCESSFUL == gi4MibResStatus)
    {
        /* Set the notification information */
        gNotifMsg.u4ConfigRestoreStatus = (UINT4) (gi4MibResStatus);
        /* Expected interval for the protocol modules to process 
         * interface up indication. Start the timer. */
        if (TmrStartTimer (gMsrDefSysIfUpTmrId, &gSysDefIfUpTimer,
                           SYS_DEF_IF_UP_TIME * SYS_NUM_OF_TIME_UNITS_IN_A_SEC)
            != TMR_SUCCESS)
        {
            MSR_TRC (MSR_TRACE_TYPE, "In MsrMibRestore system interface"
                     "up time start fail.\n");
        }
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrProcessMibSaveEvt                             */
/*                                                                          */
/*    Description        : This function is invoked when a MIB_SAVE_EVENT   */
/*                         or MIB_SAVE_TO_REMOTE_EVENT event is received.   */
/*                         It processes the recieved events.                */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
INT4
MsrProcessMibSaveEvt (VOID)
{
    INT4                i4Retval = 0;
    FILE               *fp = NULL;
    CHR1                u1FileName[MSR_MAX_FILE_NME_LEN], *pu1FileName = NULL;
    UINT1               au1LocalFileName[ISS_CONFIG_FILE_NAME_LEN + 1];
    CHR1                ac1Time[24];
    BOOL1               b1LocalIssAutoConfigSave = ISS_FALSE;
    UINT1               au1LocalIssIncrSaveFileName[ISS_CONFIG_FILE_NAME_LEN +
                                                    4];
    INT4                i4ResType = 0;
    UINT1               u1MsrNodeStandbyState = FALSE;

#ifdef RM_WANTED
    if (RM_STANDBY == gu1MsrNodeStatus)
    {
        /* This flag will be used to ensure MSR semaphore is released,
         * only after MIB save process is completed.
         * which is needed only in case of Standby node */
        u1MsrNodeStandbyState = TRUE;
    }
#endif

    MSR_MEMSET (u1FileName, 0, MSR_MAX_FILE_NME_LEN);
    MSR_MEMSET (au1LocalFileName, 0, ISS_CONFIG_FILE_NAME_LEN + 1);
    /* If MIB save is already in progress then do not 
     * start another save operation */
    if (gi4MibSaveStatus == MIB_SAVE_IN_PROGRESS)
    {
        if (OsixSemGive (MsrSemId) != OSIX_SUCCESS)
        {
            MSR_TRC (MSR_TRACE_TYPE, "Unable to give semaphore!\n");
        }
        return (FALSE);
    }
    UtilSetChkSumLastStat (0);
    if (gu1MibSaveLocal == MSR_TRUE)
    {
        gi4MibSaveStatus = MIB_SAVE_IN_PROGRESS;
    }
    else
    {
        gi4RemoteSaveStatus = MIB_SAVE_IN_PROGRESS;
    }

    if (MSR_MEMCMP (gMsrData.au1FileName, u1FileName,
                    (ISS_CONFIG_FILE_NAME_LEN + 1)) == 0)
    {
        ISS_STRNCPY (gMsrData.au1FileName,
                     gIssConfigSaveInfo.au1IssConfigSaveFileName,
                     sizeof (gMsrData.au1FileName) - 1);
        gMsrData.au1FileName[sizeof (gMsrData.au1FileName) - 1] = '\0';
    }

    /* Copying the global variable data into local variable in-order to avoid locking issues */
    ISS_STRCPY (au1LocalFileName, gMsrData.au1FileName);
    ISS_STRCPY (au1LocalIssIncrSaveFileName,
                gIssConfigSaveInfo.au1IssIncrSaveFileName);
    b1LocalIssAutoConfigSave = gIssConfigSaveInfo.IssAutoConfigSave;

    /* Backup the iss.conf local file */
    issCopyLocalFile ((UINT1 *) au1LocalFileName, (UINT1 *) ISS_BKP_CONF_FILE);

    /* Get Restoration Type - whether CSR or CSR */
    i4ResType = IssGetRestoreTypeFromNvRam ();

    /* Save the configurations */
    if ((gu1MibSaveLocal == MSR_TRUE) &&
        (MsrValidateConfigSaveRestoreFileName () == MSR_FALSE))

    {
        if (OsixSemGive (MsrSemId) != OSIX_SUCCESS)
        {
            MSR_TRC (MSR_TRACE_TYPE, "Unable to give semaphore!\n");
        }

        /* Saving the configuration failed. So update
         * the status and UNSET the save flag. 
         */
        gi4MibSaveStatus = LAST_MIB_SAVE_FAILED;
        MsrInitSaveFlag ();
        if (gu1MibSaveLocal == MSR_TRUE)
        {
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                          "Saving configuration to flash failed!"));
        }
        issMoveLocalFile ((UINT1 *) ISS_BKP_CONF_FILE,
                          (UINT1 *) au1LocalFileName);
        return (FALSE);
    }
    else
    {
        UtlGetTimeStr (ac1Time);
        MSR_TRC_ARG1 (MSR_TRACE_TYPE, "%s Save process "
                      "initiated\r\n", ac1Time);
        if (i4ResType != SET_FLAG)
        {

            if (((gMSRIncrSaveFlag == ISS_TRUE)
                 && (MsrisSaveComplete == ISS_FALSE))
                || ((gMSRIncrSaveFlag == ISS_TRUE)
                    && (gIssSysGroupInfo.b1IssDefValSaveFlag == ISS_TRUE)))
            {
                /* The RBtree does not contain the complete system configuration,
                 * ie, the protocol database is not read fully at system startup
                 * or the restoration from the configuration failed. Hence,
                 * populate the RBTree completly. */
                MSRDeleteAllRBNodesinRBTree ();
                /* now save the data into RB tree by reading from the
                   snmp */
                i4Retval = MsrSaveMIB ();

                if (i4Retval == MSR_FALSE)
                {
                    if (OsixSemGive (MsrSemId) != OSIX_SUCCESS)
                    {
                        MSR_TRC (MSR_TRACE_TYPE,
                                 "Unable to give " "semaphore!\n");
                    }

                    MSR_TRC (MSR_TRACE_TYPE, "MIB Save failed!!!\n");
                    gi4MibSaveStatus = LAST_MIB_SAVE_FAILED;
                    SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                                  "MIB Save failed!!!"));
                    issMoveLocalFile ((UINT1 *) ISS_BKP_CONF_FILE,
                                      (UINT1 *) au1LocalFileName);
                    return (FALSE);
                }
                MsrisSaveComplete = ISS_TRUE;
            }
            if (u1MsrNodeStandbyState != TRUE)
            {
                MSR_TRC (MSR_TRACE_TYPE, "Unable to give semaphore!\n");

                if (OsixSemGive (MsrSemId) != OSIX_SUCCESS)
                {
                    MSR_TRC (MSR_TRACE_TYPE, "Unable to give semaphore!\n");
                }
            }
            if (gu1MibSaveLocal == MSR_TRUE)
            {
                /* check for INCR_SAVE_ENABLED == FALSE 
                 * add the data into RB tree by reading the data from SNMPget*/
                if (gMSRIncrSaveFlag == ISS_FALSE)
                {
                    /* in case of Incremental save off RB Trees are not 
                     * used. Data is obtained from protocols and updated directly
                     * into file. */
                    pu1FileName = u1FileName;
                    if (gMsrSavResPriority == EXTERN_STORAGE)
                    {
                        SPRINTF (pu1FileName, "%s%s", MSR_SYS_EXTN_STRG_PATH,
                                 au1LocalFileName);
                    }
                    else
                    {
                        SPRINTF (pu1FileName, "%s%s", FLASH, au1LocalFileName);
                    }
                    if ((gi4SaveFd = FileOpen ((UINT1 *) pu1FileName,
                                               (OSIX_FILE_CR | OSIX_FILE_WO |
                                                OSIX_FILE_TR))) < 0)
                    {
                        if (gMsrSavResPriority == EXTERN_STORAGE)
                        {
                            MSR_TRC (MSR_TRACE_TYPE,
                                     "fileopen Failed from External Storage... \n");
                            SPRINTF (pu1FileName, "%s%s", FLASH,
                                     au1LocalFileName);
                            if ((gi4SaveFd =
                                 FileOpen ((UINT1 *) pu1FileName,
                                           (OSIX_FILE_CR | OSIX_FILE_WO |
                                            OSIX_FILE_TR))) < 0)
                            {
                                if (u1MsrNodeStandbyState == TRUE)
                                {
                                    if (OsixSemGive (MsrSemId) != OSIX_SUCCESS)
                                    {
                                        MSR_TRC (MSR_TRACE_TYPE,
                                                 "Unable to give semaphore!\n");
                                    }
                                }

                                MsrInitSaveFlag ();
                                MSR_TRC (MSR_TRACE_TYPE,
                                         "fileopen Failed ... \n");
                                gi4MibSaveStatus = LAST_MIB_SAVE_FAILED;
                                SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL,
                                              gu4MsrSysLogId,
                                              "MIB Save failed!!!"));
                                issMoveLocalFile ((UINT1 *) ISS_BKP_CONF_FILE,
                                                  (UINT1 *) au1LocalFileName);
                                return (FALSE);
                            }
                        }
                        else
                        {
                            if (u1MsrNodeStandbyState == TRUE)
                            {
                                if (OsixSemGive (MsrSemId) != OSIX_SUCCESS)
                                {
                                    MSR_TRC (MSR_TRACE_TYPE,
                                             "Unable to give semaphore!\n");
                                }
                            }

                            MsrInitSaveFlag ();
                            MSR_TRC (MSR_TRACE_TYPE, "fileopen Failed ... \n");
                            gi4MibSaveStatus = LAST_MIB_SAVE_FAILED;
                            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                                          "MIB Save failed!!!"));
                            issMoveLocalFile ((UINT1 *) ISS_BKP_CONF_FILE,
                                              (UINT1 *) au1LocalFileName);
                            return (FALSE);
                        }
                    }

                    i4Retval = MsrSaveMIB ();
                    if (i4Retval == MSR_FALSE)
                    {
                        if (u1MsrNodeStandbyState == TRUE)
                        {
                            if (OsixSemGive (MsrSemId) != OSIX_SUCCESS)
                            {
                                MSR_TRC (MSR_TRACE_TYPE,
                                         "Unable to give semaphore!\n");
                            }
                        }

                        MsrInitSaveFlag ();
                        MSR_TRC (MSR_TRACE_TYPE, "MIB Save failed!!!\n");
                        gi4MibSaveStatus = LAST_MIB_SAVE_FAILED;
                        SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                                      "MIB Save failed!!!"));
                        issMoveLocalFile ((UINT1 *) ISS_BKP_CONF_FILE,
                                          (UINT1 *) au1LocalFileName);
                        return (FALSE);
                    }
                }
                else
                {

                    /* Configuration must be saved to flash */
                    if (MSRUpdateISSConfFile (FLASH_ALL_CONFIG,
                                              (const char *) au1LocalFileName)
                        != MSR_SUCCESS)
                    {
                        if (u1MsrNodeStandbyState == TRUE)
                        {
                            if (OsixSemGive (MsrSemId) != OSIX_SUCCESS)
                            {
                                MSR_TRC (MSR_TRACE_TYPE,
                                         "Unable to give semaphore!\n");
                            }
                        }

                        gi4MibSaveStatus = LAST_MIB_SAVE_FAILED;
                        MsrInitSaveFlag ();
                        SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                                      "Saving configuration to flash failed!"));
                        issMoveLocalFile ((UINT1 *) ISS_BKP_CONF_FILE,
                                          (UINT1 *) au1LocalFileName);
                        return (FALSE);
                    }
                    if (b1LocalIssAutoConfigSave == ISS_TRUE)
                    {
                        pu1FileName = u1FileName;
                        MSR_MEMSET (u1FileName, 0, MSR_MAX_FILE_NME_LEN);
                        sprintf (pu1FileName, "%s%s", FLASH,
                                 au1LocalIssIncrSaveFileName);

                        if ((fp =
                             FOPEN ((CONST CHR1 *) pu1FileName, "w")) != NULL)
                        {
                            fclose (fp);
                        }
                        gu4IncrLength = 0;
                    }
                }

                MsrStoreSaveFlagNvRam ();

                /* The gi4MibSaveStatus should be updated only after we 
                 * write all the parameters in the flash
                 */

                /* Save the restore flag to true here */
                if (IssGetRestoreOptionFromNvRam () != ISS_CONFIG_NO_RESTORE)
                {
                    IssSetRestoreFlagToNvRam (SET_FLAG);
                    IssSetRestoreFileNameToNvRam ((INT1 *) au1LocalFileName);
                }
                gi4MibSaveStatus = LAST_MIB_SAVE_SUCCESSFUL;
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4MsrSysLogId,
                              "Saved configuration to flash successfully!"));
            }
            else
            {
                /* Clear the gpu1StrFlash before saving the configuration to it */
                MSR_MEMSET (gpu1StrFlash, 0, gu4MibEntryBlkSize);
                gpu1StrCurrent = gpu1StrFlash;
                if (gMSRIncrSaveFlag == ISS_FALSE)
                {
                    /* this is remote save with incremental save off */
                    /* complete data is save to gpu1StrFlash array and then 
                     * tftped or sftped  to remote */
                    i4Retval = MsrSaveMIB ();
                    if (i4Retval == MSR_FALSE)
                    {
                        if (u1MsrNodeStandbyState == TRUE)
                        {
                            if (OsixSemGive (MsrSemId) != OSIX_SUCCESS)
                            {
                                MSR_TRC (MSR_TRACE_TYPE,
                                         "Unable to give semaphore!\n");
                            }
                        }

                        MsrInitSaveFlag ();
                        MSR_TRC (MSR_TRACE_TYPE, "MIB Save failed!!!\n");
                        gi4MibSaveStatus = LAST_MIB_SAVE_FAILED;
                        gi4RemoteSaveStatus = LAST_MIB_SAVE_FAILED;
                        SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                                      "MIB Save failed!!!"));
                        issMoveLocalFile ((UINT1 *) ISS_BKP_CONF_FILE,
                                          (UINT1 *) au1LocalFileName);
                        return (FALSE);
                    }
                }
                else
                {
                    /*Function to populate gpu1StrFlash from RBTtree */
                    if (MsrPopulateFlashArrayFromRBTree () != MSR_SUCCESS)
                    {
                        if (u1MsrNodeStandbyState == TRUE)
                        {
                            if (OsixSemGive (MsrSemId) != OSIX_SUCCESS)
                            {
                                MSR_TRC (MSR_TRACE_TYPE,
                                         "Unable to give semaphore!\n");
                            }
                        }

                        gi4RemoteSaveStatus = LAST_MIB_SAVE_FAILED;
                        MsrInitSaveFlag ();
                        SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                                      "Saving configuration to remote failed!"));
                        issMoveLocalFile ((UINT1 *) ISS_BKP_CONF_FILE,
                                          (UINT1 *) au1LocalFileName);
                        return (FALSE);
                    }
                    else
                    {
                        if (b1LocalIssAutoConfigSave == ISS_TRUE)
                        {
                            gu4IncrLength = 0;
                        }
                    }

                }
                MsrStoreSaveFlagNvRam ();
                /* Save the restore flag to true here */
                if (IssGetRestoreOptionFromNvRam () != ISS_CONFIG_NO_RESTORE)
                {
                    IssSetRestoreFlagToNvRam (SET_FLAG);
                }
                gi4RemoteSaveStatus = LAST_MIB_SAVE_SUCCESSFUL;
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4MsrSysLogId,
                              "Saved configuration to remote successfully!"));

            }
        }
#ifdef CLI_WANTED
        else
        {
            if (CSRCliGetShowCmdOutputToFile ((UINT1 *) au1LocalFileName) !=
                CSR_SUCCESS)
            {
                if (OsixSemGive (MsrSemId) != OSIX_SUCCESS)
                {
                    MSR_TRC (MSR_TRACE_TYPE, "Unable to give " "semaphore!\n");
                }
                gi4RemoteSaveStatus = LAST_MIB_SAVE_FAILED;
                gi4MibSaveStatus = LAST_MIB_SAVE_FAILED;
                SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                              "MIB Save failed!!!"));
                return FALSE;
            }
            if (OsixSemGive (MsrSemId) != OSIX_SUCCESS)
            {
                MSR_TRC (MSR_TRACE_TYPE, "Unable to give " "semaphore!\n");
            }

            MsrStoreSaveFlagNvRam ();

            /* The gi4MibSaveStatus should be updated only after we
             * write all the parameters in the flash  */

            /* Save the restore flag to true here */
            if (IssGetRestoreOptionFromNvRam () != ISS_CONFIG_NO_RESTORE)
            {
                IssSetRestoreFlagToNvRam (SET_FLAG);
                IssSetRestoreFileNameToNvRam ((INT1 *) au1LocalFileName);
            }
            if (gu1MibSaveLocal == MSR_TRUE)
            {
                gi4MibSaveStatus = LAST_MIB_SAVE_SUCCESSFUL;
            }
            else
            {
                gi4RemoteSaveStatus = LAST_MIB_SAVE_SUCCESSFUL;
            }
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4MsrSysLogId,
                          "Saved configuration to Flash successfully!"));
        }
#endif
        if (u1MsrNodeStandbyState == TRUE)
        {
            if (OsixSemGive (MsrSemId) != OSIX_SUCCESS)
            {
                MSR_TRC (MSR_TRACE_TYPE, "Unable to give semaphore!\n");
            }
        }

        UtlGetTimeStr (ac1Time);
        MSR_TRC_ARG1 (MSR_TRACE_TYPE, "%s Configurations saved completely\r\n",
                      ac1Time);
    }
    /* Remove the backup local file */
    issDeleteLocalFile ((UINT1 *) ISS_BKP_CONF_FILE);
    return (TRUE);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrSaveMIB                                       */
/*                                                                          */
/*    Description        : This function is invoked to save the current     */
/*                         configuration in the required format to the      */
/*                         StrFlash Array. It uses the function provided by */
/*                         the SNMP Manager to get the values of all the    */
/*                         mandatory objects stored in the SaveArray.       */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MSR_TRUE if the save operation is successful     */
/****************************************************************************/
INT1
MsrSaveMIB (VOID)
{
    UINT4               u4Index = 0;
    UINT1               au1Oid[MSR_MAX_OID_LEN + 1], *pau1Oid = NULL;
    UINT1               au1tOid[MSR_MAX_OID_LEN + 1];
    tMsrOidArrListBlock *pu1Instance = NULL;
    UINT1               au1Tmp[] = ".0";
    tSnmpIndex         *pCurIndex = MsrIndexPool1;
    tSnmpIndex         *pNextIndex = MsrIndexPool2;
    UINT4               u4Error = 0;
    UINT4               u4SnmpErr = 0;
    tSNMP_OID_TYPE     *pCurOid = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pNextOid = NULL;
    tSNMP_OID_TYPE     *pInstance = NULL;
    tSNMP_OID_TYPE     *pTmpOid = NULL;
    tSNMP_MULTI_DATA_TYPE *pData = NULL, *pdefData = NULL;
    tMbDbEntry         *pCur = NULL;
    tMbDbEntry         *pNext = NULL;
    UINT4               u4Len = 0;
    INT4                i4RetVal = 0, i4Result = 0, i4SnmpRetval = SNMP_FAILURE;
    tSnmpDbEntry        SnmpDbEntry;
    tConnStatus         ConnStatus;
    UINT2               u2ChkSum = 0;
    UINT2               u2CkSum = 0;
    UINT4               u4Sum = 0;
    UINT1               u1Tmp = 0;
    UINT4               u4Length = 0;
    UINT4               u4PrevIndex = 0;
    UINT1              *pu1Data = NULL;
    UINT4               u4DataSize = 0;
    UINT4               u4PrevObjId = 0;
    UINT4               u4PrevObjIdDef = 0;
    UINT1              *pTemp = NULL;
    UINT1               u1IsDataToBeSaved = MSR_TRUE;
    CHR1                ac1Time[24];
    INT4                i4FileRetVal = 0;

    pau1Oid = au1Oid;
    MSR_TRC (MSR_TRACE_TYPE, "MIB Save is in progress ........\n");

    /* Initialize the counters and data structures used */
    gu4MsrMultiDataCount = 0;
    gu4MsrOctetStrCount = 0;
    gu4MsrOidCount = 0;

    if ((pCurOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FALSE;
    }
    if ((pNextOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FALSE;
    }
    if ((pInstance = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FALSE;
    }
    if ((pTmpOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FALSE;
    }
    if ((pOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FALSE;
    }

    if ((pData = MsrAllocMultiData ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FALSE;
    }
    if ((pdefData = MsrAllocMultiData ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        gu4MsrMultiDataCount = 0;
        gu4MsrOctetStrCount = 0;
        return MSR_FALSE;
    }
    pu1Instance = MemAllocMemBlk (gMsrOidArrListPoolId);
    if (pu1Instance == NULL)
    {
        gu4MsrOidCount = 0;
        gu4MsrMultiDataCount = 0;
        gu4MsrOctetStrCount = 0;
        return MSR_FALSE;
    }

    /* Get all the object from the gaSaveArray */
    while (gaSaveArray[u4Index].pOidString != NULL)
    {
        /* clear the data structures used for getting the previous element */
        MSR_CLEAR_OID (pCurOid);
        MSR_CLEAR_OID (pNextOid);
        MSR_CLEAR_OID (pInstance);
        MSR_CLEAR_OID (pTmpOid);
        MSR_CLEAR_MULTIDATA (pData);

        /* The contents of the gpu1StrFlash array will be in the 
         * following format:
         * OID   data-type   value
         * Here OID is the OID string
         *      data-type is the type in the tSNMP_MULTI_DATA_TYPE
         *      value is the actual value of the object.
         */

        if (gaSaveArray[u4Index].u2Type == MSR_SCALAR)
        {
            if (STRLEN (gaSaveArray[u4Index].pOidString) > MSR_MAX_OID_LEN)
            {
                continue;
            }
            MSR_STRCPY (au1Oid, gaSaveArray[u4Index].pOidString);
            WebnmConvertStringToOid (pCurOid, au1Oid, MSR_FALSE);
            if (SNMPGet (*pCurOid, pData, &u4Error, pCurIndex,
                         SNMP_DEFAULT_CONTEXT) == SNMP_SUCCESS)
            {
                i4RetVal = 0;
                if ((FN_VAL (u4Index)) != NULL)
                {
                    i4RetVal = (FN_VAL (u4Index)) (pCurOid, NULL, pData);

                    if (((i4RetVal != MSR_SAVE) &&
                         (gMSRIncrSaveFlag == ISS_FALSE) &&
                         (gu1RmStConfReq == RM_FALSE)) ||
                        ((i4RetVal != MSR_SAVE) &&
                         (i4RetVal != MSR_VOLATILE_SAVE)))
                    {
                        u4Index++;
                        continue;
                    }
                }
                /* When the Default Value Save Option is disabled, get the
                 * default value of the given Oid and compare it with the
                 * value obtained in the SNMPGet
                 */
                if (gIssSysGroupInfo.b1IssDefValSaveFlag == ISS_FALSE)
                {
                    MSR_CLEAR_MULTIDATA (pdefData);
                    /* Default values for the objects should not be
                     * saved in both incremental save on and off modes */
                    i4SnmpRetval = SNMPGetDefaultValueForObj (*pCurOid,
                                                              pdefData,
                                                              &u4Error);

                    if ((i4SnmpRetval == SNMP_FAILURE) &&
                        (u4Error != SNMP_EXCEPTION_NO_DEF_VAL))
                    {
                        u4Index++;
                        continue;
                    }

                    if (i4SnmpRetval == SNMP_SUCCESS)
                    {
                        i4Result = MSRCompareSnmpMultiDataValues (pData,
                                                                  pdefData);
                        if (i4Result == MSR_SUCCESS)
                        {
                            u4Index++;
                            continue;
                        }
                    }
                }

                if (i4RetVal == MSR_VOLATILE_SAVE)
                {
                    u1IsDataToBeSaved = MSR_FALSE;
                }
                else
                {
                    u1IsDataToBeSaved = MSR_TRUE;
                }

                if (MSR_FALSE == MsrStoreData (pCurOid, pData,
                                               MSR_NONROWSTATUS, au1Oid,
                                               &u4Sum, &u2CkSum,
                                               &u4PrevIndex, u4Index,
                                               &u4Length, u1IsDataToBeSaved))
                {
                    gu4MsrOidCount = 0;
                    gu4MsrMultiDataCount = 0;
                    gu4MsrOctetStrCount = 0;
                    MemReleaseMemBlock (gMsrOidArrListPoolId, (UINT1 *)
                                        (pu1Instance));
                    return MSR_FALSE;
                }
            }
        }
        else
            /* when table objects have to be stored */
        {
            /* If there is a rowstatus associated with the table, first
             * store the rowstatus instances and set the value of the 
             * rowstatus as CREATE_AND_WAIT. When there is a rowstatus,
             * an entry for a instance gets created only when the 
             * rowstatus is set to CREATE_AND_WAIT.
             */

            if (gaSaveArray[u4Index].pRowStatus != NULL)
            {
                STRNCPY (au1Oid, gaSaveArray[u4Index].pRowStatus,
                         (sizeof (au1Oid) - 1));
                au1Oid[(sizeof (au1Oid) - 1)] = '\0';
                WebnmConvertStringToOid (pTmpOid, au1Oid, MSR_FALSE);
                WebnmConvertStringToOid (pCurOid, au1Oid, MSR_FALSE);
                MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);
                WebnmConvertOidToString (pTmpOid, au1Oid);

                /* Get all the rowstatus objects in the table */
                while (1)
                {
                    /* Append the latest instance to the pCurOid.
                     * This is needed to get the next instance of
                     * the object.
                     */
                    WebnmCopyOid (pCurOid, pInstance);
                    if (SNMPGetNextOID (*pCurOid, pNextOid, pData,
                                        &u4Error, pCurIndex,
                                        pNextIndex, SNMP_DEFAULT_CONTEXT)
                        != SNMP_SUCCESS)
                    {
                        break;
                    }
                    WebnmGetInstanceOid (pTmpOid, pNextOid, pInstance);
                    if (pInstance->u4_Length == 0)
                    {
                        break;
                    }
                    i4RetVal = 0;
                    pData->i2_DataType = SNMP_DATA_TYPE_INTEGER;
                    pData->i4_SLongValue = MSR_CREATE_AND_WAIT;
                    pData->u4_ULongValue = MSR_CREATE_AND_WAIT;
                    if (FN_VAL (u4Index) != NULL)
                    {
                        i4RetVal = (FN_VAL (u4Index)) (pTmpOid,
                                                       pInstance, pData);
                        if (i4RetVal == MSR_SKIP)
                        {
                            /* Reset the pCurOid to the original OID */
                            MSR_CLEAR_OID (pCurOid);
                            MSR_CLEAR_MULTIDATA (pData);
                            WebnmCopyOid (pCurOid, pTmpOid);
                            continue;
                        }
                        else if (i4RetVal == MSR_NEXT)
                        {
                            /* Reset the pCurOid to the original OID */
                            MSR_CLEAR_OID (pCurOid);
                            MSR_CLEAR_MULTIDATA (pData);
                            WebnmCopyOid (pCurOid, pTmpOid);
                            break;
                        }
                        else if ((i4RetVal == MSR_VOLATILE_SAVE) &&
                                 (gMSRIncrSaveFlag == ISS_FALSE))
                        {
                            MSR_CLEAR_OID (pCurOid);
                            MSR_CLEAR_MULTIDATA (pData);
                            WebnmCopyOid (pCurOid, pTmpOid);
                            continue;
                        }
                    }
                    pData->i4_SLongValue = (INT4) pData->u4_ULongValue;
                    MSR_MEMSET (pu1Instance->au1MsrOidArrListBlock,
                                0, MSR_MAX_OID_LEN);
                    MSR_MEMSET (au1tOid, 0, MSR_MAX_OID_LEN + 1);
                    WebnmConvertOidToString (pInstance,
                                             pu1Instance->
                                             au1MsrOidArrListBlock);
                    SNPRINTF ((char *) au1tOid, MSR_MAX_OID_LEN, "%s.%s",
                              au1Oid, pu1Instance->au1MsrOidArrListBlock);
                    MSR_CLEAR_OID (pOid);
                    WebnmConvertStringToOid (pOid, au1tOid, MSR_FALSE);

                    /* row status colums ahve no default value so store with
                     * out checking  */

                    if (i4RetVal == MSR_VOLATILE_SAVE)
                    {
                        u1IsDataToBeSaved = MSR_FALSE;
                    }
                    else
                    {
                        u1IsDataToBeSaved = MSR_TRUE;
                    }

                    if (MSR_FALSE == MsrStoreData (pOid, pData,
                                                   MSR_ROWSTATUS, au1tOid,
                                                   &u4Sum, &u2CkSum,
                                                   &u4PrevIndex, u4Index,
                                                   &u4Length,
                                                   u1IsDataToBeSaved))
                    {
                        gu4MsrOidCount = 0;
                        gu4MsrMultiDataCount = 0;
                        gu4MsrOctetStrCount = 0;
                        MemReleaseMemBlock (gMsrOidArrListPoolId, (UINT1 *)
                                            (pu1Instance));
                        return MSR_FALSE;
                    }

                    /* Reset the pCurOid to the original OID */
                    MSR_CLEAR_OID (pCurOid);
                    WebnmCopyOid (pCurOid, pTmpOid);
                }
            }

            u4PrevObjId = 0;
            u4PrevObjIdDef = 0;
            /* clear the data structures used for getting the rowstatus */
            MSR_CLEAR_OID (pCurOid);
            MSR_CLEAR_OID (pNextOid);
            MSR_CLEAR_OID (pInstance);
            MSR_CLEAR_OID (pTmpOid);

            /* reset the au1Oid to the current OID */
            /* Copy a maximum of MSR_MAX_OID_LEN - STRLEN(au1Tmp) bytes only
             * so that ".0" can be appended tp au1Oid. This will avoid array
             * bound violation */
            MEMSET (au1Oid, 0, MSR_MAX_OID_LEN + 1);
            STRNCPY (au1Oid, gaSaveArray[u4Index].pOidString,
                     MSR_MAX_OID_LEN - STRLEN (au1Tmp));

            WebnmConvertStringToOid (pTmpOid, au1Oid, MSR_FALSE);
            MEMSET (au1Oid, 0, MSR_MAX_OID_LEN + 1);
            if ((STRLEN (au1Tmp) + STRLEN (gaSaveArray[u4Index].pOidString))
                < MSR_MAX_OID_LEN)
            {
                sprintf ((char *) pau1Oid, "%2s%s",
                         gaSaveArray[u4Index].pOidString, au1Tmp);
            }
            else
            {
                gu4MsrOidCount = 0;
                gu4MsrMultiDataCount = 0;
                gu4MsrOctetStrCount = 0;
                MemReleaseMemBlock (gMsrOidArrListPoolId, (UINT1 *)
                                    (pu1Instance));
                return MSR_FALSE;
            }
            WebnmConvertStringToOid (pCurOid, au1Oid, MSR_FALSE);
            MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);
            WebnmConvertOidToString (pTmpOid, au1Oid);

            /* Get all the objects in the table */
            while (1)
            {
                /* Append the latest instance to the pCurOid.
                 * This is needed to get the next instance of
                 * the object.
                 */
                MSR_CLEAR_MULTIDATA (pData);
                u4Error = 0;
                WebnmCopyOid (pCurOid, pInstance);
                if (SNMPGetNextOID (*pCurOid, pNextOid, pData,
                                    &u4Error, pCurIndex,
                                    pNextIndex, SNMP_DEFAULT_CONTEXT)
                    != SNMP_SUCCESS)
                {
                    break;
                }

                WebnmGetInstanceOid (pTmpOid, pNextOid, pInstance);
                if (pInstance->u4_Length == 0)
                {
                    break;
                }

                /* Do not store the object if it is a read-only 
                 * object. Read-only objects will not have SET 
                 * routines associated with them.
                 */
                if (u4PrevObjId != pInstance->pu4_OidList[0])
                {
                    GetMbDbEntry (*pNextOid, &pNext, &u4Len, &SnmpDbEntry);
                    pCur = SnmpDbEntry.pMibEntry;
                    u4PrevObjId = pInstance->pu4_OidList[0];
                }

                if (pCur != NULL)
                {
                    if (pCur->Set == NULL)
                    {
                        /* Reset the pCurOid to the original OID */
                        MSR_CLEAR_OID (pCurOid);
                        if ((OIDCompare (*pTmpOid, pNext->ObjectID, &u4Len)
                             == SNMP_LESSER) && (u4Len == pTmpOid->u4_Length))
                        {
                            WebnmCopyOid (pCurOid, pTmpOid);
                            WebnmGetInstanceOid (pTmpOid, &(pNext->ObjectID),
                                                 pInstance);
                            continue;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                i4RetVal = 0;
                if (FN_VAL (u4Index) != NULL)
                {
                    i4RetVal = (FN_VAL (u4Index)) (pTmpOid, pInstance, pData);
                    if (i4RetVal == MSR_SKIP)
                    {
                        /* Reset the pCurOid to the original OID */
                        MSR_CLEAR_OID (pCurOid);
                        MSR_CLEAR_MULTIDATA (pData);
                        WebnmCopyOid (pCurOid, pTmpOid);
                        continue;
                    }
                    else if (i4RetVal == MSR_NEXT)
                    {
                        break;
                    }
                    else if ((i4RetVal == MSR_VOLATILE_SAVE) &&
                             (gMSRIncrSaveFlag == ISS_FALSE))
                    {
                        MSR_CLEAR_OID (pCurOid);
                        MSR_CLEAR_MULTIDATA (pData);
                        WebnmCopyOid (pCurOid, pTmpOid);
                        continue;
                    }
                }

                MSR_MEMSET (pu1Instance->au1MsrOidArrListBlock,
                            0, MSR_MAX_OID_LEN);
                MSR_MEMSET (au1tOid, 0, MSR_MAX_OID_LEN);
                WebnmConvertOidToString (pInstance,
                                         pu1Instance->au1MsrOidArrListBlock);
                SNPRINTF ((char *) au1tOid, MSR_MAX_OID_LEN,
                          "%s.%s", au1Oid, pu1Instance->au1MsrOidArrListBlock);
                MSR_CLEAR_OID (pOid);
                WebnmConvertStringToOid (pOid, au1tOid, MSR_FALSE);

                /* When the Default Value Save Option is disabled, get the
                 * default value of the given Oid and compare it with the
                 * value obtained in the SNMPGet
                 */
                if (gIssSysGroupInfo.b1IssDefValSaveFlag == ISS_FALSE)
                {
                    if (u4PrevObjIdDef != pInstance->pu4_OidList[0])
                    {
                        MSR_CLEAR_MULTIDATA (pdefData);
                        i4SnmpRetval = SNMPGetDefaultValueForObj (*pOid,
                                                                  pdefData,
                                                                  &u4SnmpErr);
                        u4PrevObjIdDef = pInstance->pu4_OidList[0];
                    }
                    if ((i4SnmpRetval == SNMP_FAILURE) &&
                        (u4SnmpErr != SNMP_EXCEPTION_NO_DEF_VAL))
                    {
                        MSR_CLEAR_OID (pCurOid);
                        MSR_CLEAR_MULTIDATA (pData);
                        WebnmCopyOid (pCurOid, pTmpOid);
                        continue;
                    }

                    if (i4SnmpRetval == SNMP_SUCCESS)
                    {
                        i4Result = MSRCompareSnmpMultiDataValues (pData,
                                                                  pdefData);
                        if (i4Result == MSR_SUCCESS)
                        {
                            MSR_CLEAR_OID (pCurOid);
                            MSR_CLEAR_MULTIDATA (pData);
                            WebnmCopyOid (pCurOid, pTmpOid);
                            continue;
                        }
                    }
                }
                if (i4RetVal == MSR_VOLATILE_SAVE)
                {
                    u1IsDataToBeSaved = MSR_FALSE;
                }
                else
                {
                    u1IsDataToBeSaved = MSR_TRUE;
                }

                if (MSR_FALSE == MsrStoreData (pOid, pData,
                                               MSR_NONROWSTATUS, au1tOid,
                                               &u4Sum, &u2CkSum,
                                               &u4PrevIndex, u4Index,
                                               &u4Length, u1IsDataToBeSaved))
                {
                    gu4MsrOidCount = 0;
                    gu4MsrMultiDataCount = 0;
                    gu4MsrOctetStrCount = 0;
                    return MSR_FALSE;
                }
                /* Reset the pCurOid to the original OID */
                MSR_CLEAR_OID (pCurOid);
                MSR_CLEAR_MULTIDATA (pData);
                WebnmCopyOid (pCurOid, pTmpOid);
            }
        }
        UtlGetTimeStr (ac1Time);
        MSR_TRC_ARG2 (MSR_TRACE_TYPE, "%s Object %s saved\r\n",
                      ac1Time, gaSaveArray[u4Index].pOidString);
        u4Index++;
    }
    MemReleaseMemBlock (gMsrOidArrListPoolId, (UINT1 *) (pu1Instance));
    if (gMSRIncrSaveFlag == ISS_FALSE)
    {
        if (MSR_DATA_ALLOC_MEM_BLOCK (pu1Data) == NULL)
        {
            gu4MsrOidCount = 0;
            gu4MsrMultiDataCount = 0;
            gu4MsrOctetStrCount = 0;
            return MSR_FALSE;
        }
        pTemp = (UINT1 *) pu1Data;
        MSR_MEMSET (pu1Data, 0, MSR_MAX_DATA_LEN);
        sprintf ((char *) pTemp, "CKSUM:");
        u4DataSize = MSR_STRLEN (pTemp);
        pTemp += MSR_STRLEN (pTemp);
        MEMCPY (pTemp, &u2ChkSum, sizeof (UINT2));
        u4DataSize += sizeof (UINT2);
        pTemp += sizeof (UINT2);
        u4Length += u4DataSize;
        if ((u4Length % 2) == 1)
        {
            MEMCPY (pTemp, &u1Tmp, sizeof (UINT1));
            u4Length++;
            u4DataSize++;
        }
        UtilCalcCheckSum (pu1Data, u4DataSize, &u4Sum, &u2CkSum,
                          CKSUM_LASTDATA);
        /* gu2FlashCksum used for statndby checksum comparision */
        gu2FlashCksum = u2CkSum;
        pTemp -= 2;
        MEMCPY (pTemp, &u2CkSum, sizeof (UINT2));
        pTemp = (UINT1 *) pu1Data;
        /*u4Length += sizeof (UINT2); */
        if (gu1MibSaveLocal == MSR_TRUE)
        {
            /* save to FILE */
            i4FileRetVal = (INT4) FileWrite (gi4SaveFd, (CHR1 *) pTemp,
                                             MEM_MAX_BYTES (u4DataSize,
                                                            MSR_MAX_DATA_LEN));
            FileClose (gi4SaveFd);
            if (i4FileRetVal < 0)
            {
                gu4MsrOidCount = 0;
                gu4MsrMultiDataCount = 0;
                gu4MsrOctetStrCount = 0;
                MSR_DATA_FREE_MEM_BLOCK (pu1Data);
                return MSR_FALSE;
            }
        }
        else
        {
            if (gpu1StrCurrent + u4DataSize >= gpu1StrEnd)
            {
                ConnStatus = SEND_DATA;
                if (MsrSendDataToRemote (&ConnStatus) == MSR_FAILURE)
                {
                    gu4MsrOidCount = 0;
                    gu4MsrMultiDataCount = 0;
                    gu4MsrOctetStrCount = 0;
                    MSR_DATA_FREE_MEM_BLOCK (pu1Data);
                    return MSR_FALSE;
                }
            }

            /* save to remot so update gpu1StrFlash */
            if (u4DataSize >= MSR_MAX_DATA_LEN)
            {
                MSR_DATA_FREE_MEM_BLOCK (pu1Data);
                return MSR_FALSE;
            }
            MSR_MEMCPY (gpu1StrCurrent, pu1Data, u4DataSize);
            gpu1StrCurrent += u4DataSize;
            gu4Length = (UINT4) (gpu1StrCurrent - gpu1StrFlash);
            ConnStatus = TERMINATE;
            if (MsrSendDataToRemote (&ConnStatus) == MSR_FAILURE)
            {
                gu4MsrOidCount = 0;
                gu4MsrMultiDataCount = 0;
                gu4MsrOctetStrCount = 0;
                MSR_DATA_FREE_MEM_BLOCK (pu1Data);
                return MSR_FALSE;
            }
        }
        MSR_DATA_FREE_MEM_BLOCK (pu1Data);
    }
    gu4MsrOidCount = 0;
    gu4MsrMultiDataCount = 0;
    gu4MsrOctetStrCount = 0;
    return MSR_TRUE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrMibRestore                                    */
/*                                                                          */
/*    Description        : This function is used to restore the             */
/*                         configuration present in the conf file.          */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
INT4
MsrMibRestore (INT4 i4ConfFd)
{
    UINT4               u4Index = 0;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_MULTI_DATA_TYPE *pData = NULL;
    tSnmpIndex         *pCurIndex = MsrIndexPool1;
    tMsrOidArrListBlock *pu1Oid = NULL;
    UINT1               au1Data[MSR_MAX_STR_LEN];
    UINT1              *pu1Cfg = NULL;
    UINT1              *pu1FirstData = NULL;
    UINT1              *pChar = NULL;
    UINT2               u2Type = 0;
    UINT4               u4Error = 0;
    /* The u4Total will store the total number of objects stored in the 
     * restoration file */
    UINT4               u4Total = 0;
    /* The u4ErrorCount will store the number of objects from the restoration
     * file, for which the SET operation failed. */
    UINT4               u4ErrorCount = 0;
    UINT4               u4Len = 0;
    tMsrOidArrListBlock *pu1PrefixOid = NULL;
    tMsrOidArrListBlock *pu1SuffixOid = NULL;
    UINT1               u1DataCntr = 0;
    UINT2               u2CfgCntr = 0;
    UINT1               u1LineFlag = MSR_FALSE;
    INT4                i4DataLen = 0;
#ifdef CLI_WANTED
    INT4                i4DefExecTimeOut = 0;
#endif
    MEMSET (&gNotifMsg, 0, sizeof (tMsrNotifyMsg));

    /* Initialization of the data structures used */
    gi4MibResStatus = MIB_RESTORE_IN_PROGRESS;

    if ((pOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gi4MibResFailureReason = MIB_RES_MEMORY_FAILURE;
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        return MSR_FAILURE;
    }

    MSR_MEMSET (au1Data, 0, MSR_MAX_STR_LEN);
    if (MSR_DATA_ALLOC_MEM_BLOCK (pu1Cfg) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gi4MibResFailureReason = MIB_RES_MEMORY_FAILURE;
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        return MSR_FAILURE;
    }
    MSR_MEMSET (pu1Cfg, 0, MSR_MAX_DATA_LEN);
    if (MSR_DATA_ALLOC_MEM_BLOCK (pu1FirstData) == NULL)
    {
        MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gi4MibResFailureReason = MIB_RES_MEMORY_FAILURE;
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        return MSR_FAILURE;
    }
    MSR_MEMSET (pu1FirstData, 0, MSR_MAX_DATA_LEN);
    if ((pu1Oid = MemAllocMemBlk (gMsrOidArrListPoolId)) == NULL)
    {
        MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);
        MSR_DATA_FREE_MEM_BLOCK (pu1FirstData);
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gi4MibResFailureReason = MIB_RES_MEMORY_FAILURE;
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        return MSR_FAILURE;
    }
    MSR_MEMSET (pu1Oid->au1MsrOidArrListBlock, 0, MSR_MAX_STR_LEN);
    if ((pu1PrefixOid = MemAllocMemBlk (gMsrOidArrListPoolId)) == NULL)
    {
        MemReleaseMemBlock (gMsrOidArrListPoolId, (UINT1 *) (pu1Oid));
        MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);
        MSR_DATA_FREE_MEM_BLOCK (pu1FirstData);
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gi4MibResFailureReason = MIB_RES_MEMORY_FAILURE;
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        return MSR_FAILURE;
    }
    MSR_MEMSET (pu1PrefixOid->au1MsrOidArrListBlock, 0, MSR_MAX_STR_LEN);
    if ((pu1SuffixOid = MemAllocMemBlk (gMsrOidArrListPoolId)) == NULL)
    {
        MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);
        MSR_DATA_FREE_MEM_BLOCK (pu1FirstData);
        MemReleaseMemBlock (gMsrOidArrListPoolId, (UINT1 *) (pu1Oid));
        MemReleaseMemBlock (gMsrOidArrListPoolId, (UINT1 *) (pu1PrefixOid));
        MemReleaseMemBlock (gMsrOidArrListPoolId, (UINT1 *) (pu1SuffixOid));
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gi4MibResFailureReason = MIB_RES_MEMORY_FAILURE;
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        return MSR_FAILURE;
    }
    MSR_MEMSET (pu1SuffixOid->au1MsrOidArrListBlock, 0, MSR_MAX_STR_LEN);
    while (((i4DataLen = FileRead (i4ConfFd, (CHR1 *) au1Data,
                                   MSR_MAX_FILE_READ_LEN)) > 0)
           && (i4DataLen != (INT4) MSR_LEN_MAX))
    {
        while ((INT4) u1DataCntr < i4DataLen)
        {
            pu1Cfg[u2CfgCntr] = au1Data[u1DataCntr++];
            if ((pu1Cfg[u2CfgCntr++] != '\n') && (u2CfgCntr < MSR_MAX_DATA_LEN))
            {
                continue;
            }
            u4Total++;
            pChar = pu1Cfg;

            if (STRNCMP (pChar, "CKSUM:", 6) == 0)
            {
                break;
            }

            /* Get the OID of the object to be restored and convert
             * * it from string to the OID type.
             * */

            /* In iss.conf version 1.31 the file format has changed.
             * To provide backward compatibility for all iss.conf
             * version (1.28 to 1.31) ,The below logic has modified.
             * */

            if (*pChar == '@')
            {
                MSR_MEMSET (pu1PrefixOid->au1MsrOidArrListBlock,
                            0, MSR_MAX_OID_LEN);
                pChar += 1;

                while (*pChar != '\t')
                {
                    pChar++;
                }
                pChar++;

                /* get the prefix oid is present */
                for (u4Index = 0; u4Index < MSR_MAX_OID_LEN; u4Index++)
                {
                    if (*pChar != '\n')
                    {
                        pu1PrefixOid->au1MsrOidArrListBlock[u4Index] = *pChar;
                        pChar++;
                    }
                    else
                    {
                        pu1PrefixOid->au1MsrOidArrListBlock[u4Index] = '\0';
                        break;
                    }
                }
            }
            else
            {
                /* No need to increment pChar by 2 for the first 2 lines
                 * read from iss.conf.
                 */

                if (u4Total > MSR_RES_FILE_VER_FORMAT)
                {
                    if (STRCMP (pu1FirstData, ISS_RES_FILE_VERSION_FORMAT) >= 0)
                    {
                        /* ISS_RES_FILE_VERSION_FORMAT is the
                         * version indicator from where iss.conf format is changed.
                         * This is compared with old restore file version, to check
                         * whether way of reading to be changed or not.
                         */

                        pChar += 2;
                    }
                }

                for (u4Index = 0; u4Index < MSR_MAX_OID_LEN; u4Index++)
                {
                    if (*pChar != '\t')
                    {
                        pu1SuffixOid->au1MsrOidArrListBlock[u4Index] = *pChar;
                        pChar++;
                    }
                    else
                    {
                        pu1SuffixOid->au1MsrOidArrListBlock[u4Index] = '\0';
                        break;
                    }
                }
                /* concatenate the prefix oid and the suffix oids read */
                sprintf ((char *) pu1Oid->au1MsrOidArrListBlock, "%s%s",
                         pu1PrefixOid->au1MsrOidArrListBlock,
                         pu1SuffixOid->au1MsrOidArrListBlock);
                SnmpUpdateEoidString (pu1Oid->au1MsrOidArrListBlock);

                WebnmConvertStringToOid (pOid, pu1Oid->au1MsrOidArrListBlock,
                                         MSR_FALSE);

                /* Get the multidata data type of the object to be restored */
                u2Type = (UINT2) ATOI (pChar + 1);
                pChar += 4;

                if (u2Type == OCTETSTRING)
                {
                    u4Len = (UINT4) ATOI (pChar);
                    pChar += 5;
                    while (pChar[u4Len] != '\n')
                    {
                        u1LineFlag = MSR_FALSE;
                        while ((INT4) u1DataCntr < i4DataLen)
                        {
                            pu1Cfg[u2CfgCntr] = au1Data[u1DataCntr++];
                            if ((pu1Cfg[u2CfgCntr++] == '\n') ||
                                (u2CfgCntr >= MSR_MAX_DATA_LEN))
                            {
                                u1LineFlag = MSR_TRUE;
                                break;
                            }
                        }
                        if (u1LineFlag == MSR_FALSE)
                        {
                            u1DataCntr = 0;
                            MSR_MEMSET (au1Data, 0, MSR_MAX_STR_LEN);
                            i4DataLen = FileRead (i4ConfFd,
                                                  (CHR1 *) au1Data,
                                                  MSR_MAX_FILE_READ_LEN);
                        }
                        if (u2CfgCntr >= MSR_MAX_DATA_LEN)
                        {
                            break;
                        }
                    }
                }
                else
                {
                    u4Len = 0;
                }

                pData = MsrConvertStringToData (pChar, u2Type, u4Len);

                if (pData == NULL)
                {
                    MSR_TRC (MSR_TRACE_TYPE, "Memory allocation failed!!!\n");
                    gi4MibResFailureReason = MIB_RES_MEMORY_FAILURE;
                    gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                    gu4MsrOidCount = 0;
                    FileClose (i4ConfFd);
                    MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);
                    MSR_DATA_FREE_MEM_BLOCK (pu1FirstData);
                    MemReleaseMemBlock (gMsrOidArrListPoolId,
                                        (UINT1 *) (pu1Oid));
                    MemReleaseMemBlock (gMsrOidArrListPoolId,
                                        (UINT1 *) (pu1PrefixOid));
                    MemReleaseMemBlock (gMsrOidArrListPoolId,
                                        (UINT1 *) (pu1SuffixOid));
                    return MSR_FAILURE;
                }

                /* Restoration should not be done if restore file version
                 * is incompatible with executable's restore file version
                 */
                if (u4Total == 1)
                {
                    /* Verify the first OID is issConfigRestoreFileVersion
                     * or Not. If So then check the version
                     */
                    if ((STRCMP (pu1Oid->au1MsrOidArrListBlock,
                                 ISS_RESTORE_FILE_VERSION_OID) != 0) ||
                        (MSR_MEMCMP (pData->pOctetStrValue->pu1_OctetList,
                                     ISS_OLD_RESTORE_FILE_VERSION,
                                     pData->pOctetStrValue->i4_Length) < 0))
                    {
                        MSR_TRC (MSR_TRACE_TYPE,
                                 "Incompatable restore file!!!\n");
                        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                        gi4MibResFailureReason = MIB_RES_INCOMPATABLE_FILE;
                        gu4MsrOidCount = 0;
                        SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                                      "Incompatable restore file version!!!!"));
                        FileClose (i4ConfFd);
                        MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);
                        MSR_DATA_FREE_MEM_BLOCK (pu1FirstData);
                        return MSR_FAILURE;
                    }
                    MSR_MEMCPY (pu1FirstData,
                                pData->pOctetStrValue->pu1_OctetList,
                                pData->pOctetStrValue->i4_Length);

                    MSR_CLEAR_MULTIDATA (pCurIndex->pIndex);
                    MSR_MEMSET (pu1Oid->au1MsrOidArrListBlock, 0,
                                MSR_MAX_OID_LEN);
                    MSR_MEMSET (pu1SuffixOid->au1MsrOidArrListBlock, 0,
                                MSR_MAX_OID_LEN);
                }

                /* In Old version iss.conf file,File format OID is not present.
                 * File Format OID is introduced from iss.conf version 1.32 only. 
                 * So In future if we modify the version means, the below check 
                 * will be hit and useful to check compatibility with respect
                 * to file format also.
                 * */

                else
                {
                    if ((u4Total == 2) &&
                        (STRCMP (pu1FirstData,
                                 ISS_RES_FILE_VERSION_FORMAT) > 0))
                    {
                        if ((STRCMP (pu1Oid->au1MsrOidArrListBlock,
                                     ISS_RESTORE_FILE_FORMAT_OID) != 0)
                            ||
                            (STRCMP
                             (pData->pOctetStrValue->pu1_OctetList,
                              ISS_RESTORE_FILE_FORMAT) != 0))
                        {
                            MSR_TRC (MSR_TRACE_TYPE,
                                     "Incompatable restore file!!!\n");
                            gi4MibResFailureReason = MIB_RES_INCOMPATABLE_FILE;
                            gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                            gu4MsrOidCount = 0;
                            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                                          "Incompatable restore file Format!!!!"));
                            FileClose (i4ConfFd);
                            MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);
                            MSR_DATA_FREE_MEM_BLOCK (pu1FirstData);
                            return MSR_FAILURE;
                        }
                        MSR_CLEAR_MULTIDATA (pCurIndex->pIndex);
                        MSR_MEMSET (pu1Oid->au1MsrOidArrListBlock, 0,
                                    MSR_MAX_OID_LEN);
                        MSR_MEMSET (pu1SuffixOid->au1MsrOidArrListBlock, 0,
                                    MSR_MAX_OID_LEN);
                    }
                    else
                    {
                        /* Try to SET the object.
                         * If the SET fails increment the ErrorCount 
                         * and continue. */
                        u4Error = 0;

                        if (SNMPSet (*pOid, pData, &u4Error, pCurIndex,
                                     SNMP_DEFAULT_CONTEXT) == SNMP_FAILURE)
                        {
                            u4ErrorCount++;
                        }
                        MSR_CLEAR_MULTIDATA (pCurIndex->pIndex);
                        MSR_MEMSET (pu1Oid->au1MsrOidArrListBlock, 0,
                                    MSR_MAX_OID_LEN);
                        MSR_MEMSET (pu1SuffixOid->au1MsrOidArrListBlock, 0,
                                    MSR_MAX_OID_LEN);
                    }
                }
            }
            u2CfgCntr = 0;
            MSR_MEMSET (pu1Cfg, 0, MSR_MAX_DATA_LEN);
        }
        u1DataCntr = 0;
        MSR_MEMSET (au1Data, 0, MSR_MAX_STR_LEN);
    }
    MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);
    MSR_DATA_FREE_MEM_BLOCK (pu1FirstData);
    MemReleaseMemBlock (gMsrOidArrListPoolId, (UINT1 *) (pu1Oid));
    MemReleaseMemBlock (gMsrOidArrListPoolId, (UINT1 *) (pu1PrefixOid));
    MemReleaseMemBlock (gMsrOidArrListPoolId, (UINT1 *) (pu1SuffixOid));
    /* Restore operation completed successfully */
    gi4MibResStatus = LAST_MIB_RESTORE_SUCCESSFUL;
    gu4MsrOidCount = 0;
    /* Set the notification information */
    gNotifMsg.u4ConfigRestoreStatus = (UINT4) gi4MibResStatus;
#ifdef CLI_WANTED
    CliIssGetDefaultExecTimeOut (&i4DefExecTimeOut);
    CliSetSessionTimer (i4DefExecTimeOut);
#endif
    FileClose (i4ConfFd);
    return MSR_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MSRDeleteAllRBNodesinRBTree                      */
/*                                                                          */
/*    Description        : This function is used to erase the               */
/*                         configuration saved all the  RBT node.           */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MSRDeleteAllRBNodesinRBTree (VOID)
{

    RBTreeDrain (gMSRRBTable, (tRBKeyFreeFn) MSRFreeRBTreeNode, 1);

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrProcessMibEraseEvent                          */
/*                                                                          */
/*    Description        : This function is used to erase the               */
/*                         configuration saved in the gpu1StrFlash and RBT  */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
MsrProcessMibEraseEvent (VOID)
{
    MSR_TRC (MSR_TRACE_TYPE, "MIB Erase is in progress ........\n");

    if (gMSRIncrSaveFlag == ISS_FALSE)
    {
        MSR_MEMSET (gpu1StrFlash, 0, gu4MibEntryBlkSize);
        gu2FlashCksum = 0;
    }
    else if (gIssConfigSaveInfo.IssAutoConfigSave == ISS_FALSE)
    {
        /* Clear the gpu1StrFlash & gu2FlashCksum for erasing
           the configurations */
        MSRDeleteAllRBNodesinRBTree ();
        MSR_MEMSET (gpu1StrFlash, 0, gu4MibEntryBlkSize);
        gu2FlashCksum = 0;

        /* In the auto save off mode, No need clear the incremental buffer */
        MSRInsertVerOrFormatInfoToRBTree ();
        MsrisSaveComplete = ISS_FALSE;
    }
    else
    {
        MSR_TRC (MSR_TRACE_TYPE,
                 "Auto save option is enabled, erase event not processed ..\n");
    }
    return;
}

#endif

/***********************************************************************/
/*                   PRIVATE FUNCTIONS OF MSR MODULE                   */
/***********************************************************************/

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssRestartNotification                           */
/*                                                                          */
/*    Description        : This function Posts a ISS Restart event to the   */
/*                         MSR Task                                         */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MSR_TRUE/MSR_FALSE                               */
/****************************************************************************/
INT4
IssRestartNotification ()
{

    OsixEvtSend (MsrId, ISS_RESTART_EVENT);
    MSR_TRC (MSR_TRACE_TYPE, "Posted ISS RESTART event to MSR Successfully\n");

    return (MSR_TRUE);

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssLoadVersionRestartNotification                */
/*                                                                          */
/*    Description        : This function Posts a ISS Load Version           */
/*                         Restart event to the MSR Task                    */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MSR_TRUE/MSR_FALSE                               */
/****************************************************************************/
INT4
IssLoadVersionRestartNotification (VOID)
{

    OsixEvtSend (MsrId, ISS_LOAD_VERSION_RESTART_EVENT);
    MSR_TRC (MSR_TRACE_TYPE,
             "Posted ISS LOAD_VERSION RESTART event to MSR Successfully\n");

    return (MSR_TRUE);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : IssMsrInit                                       */
/*                                                                          */
/*    Description        : This function posts an event to the MSR task to  */
/*                         initiate restoration of the saved configuration. */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
IssMsrInit ()
{
    INT4                i4RetValue = 0;

    /* Post the restore event depending on the type of restore */

    if (IssGetRestoreOptionFromNvRam () == ISS_RESTORE_LOCAL)
    {
        OsixEvtSend (MsrId, MIB_RESTORE_EVENT);
    }
    else if (IssGetRestoreOptionFromNvRam () == ISS_RESTORE_REMOTE)
    {
        MsrRemRestoreInit ();
    }
    else
    {
#ifdef FIREWALL_WANTED
        /* Enable firewall by default after setting some default rules */
        FwlAddDefaultRules ();
#endif
        /* By default set to Not Initiated */
        gi4MibResStatus = MIB_RESTORE_NOT_INITIATED;

        ISSSetHealthConfigRestoreStatus ((UINT1) (gi4MibResStatus));

        if (gMSRIncrSaveFlag == ISS_TRUE)
        {
            /* incase of incremental save, initially the Rbtree does not
             * contain the complete sysytem data
             * eg (snmp community, user notify entry etc). Hence, when no 
             * restoration is done all the initial configuration present 
             * in the switch are updated to the Rbtree. */

            i4RetValue = MsrSaveMIB ();

            if (i4RetValue == MSR_TRUE)
            {
                if (gIssConfigSaveInfo.IssAutoConfigSave == ISS_TRUE)
                {
                    if (MSRUpdateISSConfFile
                        (FLASH_ALL_CONFIG,
                         (const char *)
                         gIssConfigSaveInfo.au1IssConfigSaveFileName)
                        == MSR_SUCCESS)
                    {
                        gu4IncrLength = 0;
                        MsrStoreSaveFlagNvRam ();
                        IssSetRestoreFlagToNvRam (SET_FLAG);
                        IssSetRestoreOptionToNvRam (ISS_CONFIG_RESTORE);
                        IssSetRestoreFileNameToNvRam
                            ((INT1 *)
                             gIssConfigSaveInfo.au1IssConfigSaveFileName);
                        MsrisSaveComplete = ISS_TRUE;
                    }
                }
                else
                {
                    MsrisSaveComplete = ISS_TRUE;
                }
            }
        }

        MsrSendRestorationCompleteToProtocols ();
        MsrResumeUserConfiguration ();
    }
}

/************************************************************************
 *  Function Name   : MsrClrCfgAllocOid
 *  Description     : Allocate OID from OID Pool
 *  Input           : None
 *  Output          : None
 *  Returns         : Pointer to OID or null
 ************************************************************************/
tSNMP_OID_TYPE     *
MsrClrCfgAllocOid ()
{
    tSNMP_OID_TYPE     *pOid = NULL;
    if (gu4MsrClrCfgOidCount >= MSR_MAX_OID_TYPE)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Unable to allocate memory for OID list!\n");
        return NULL;
    }
    pOid = &gMsrClrCfgOidType[gu4MsrClrCfgOidCount];
    pOid->pu4_OidList = &gau4MsrClrCfgOidType[gu4MsrClrCfgOidCount++][0];
    pOid->u4_Length = 0;
    return pOid;
}

/************************************************************************
 *  Function Name   : MsrAllocOid
 *  Description     : Allocate OID from OID Pool
 *  Input           : None
 *  Output          : None
 *  Returns         : Pointer to OID or null
 ************************************************************************/
tSNMP_OID_TYPE     *
MsrAllocOid ()
{
    tSNMP_OID_TYPE     *pOid = NULL;
    if (gu4MsrOidCount >= MSR_MAX_OID_TYPE)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Unable to allocate memory for OID list!\n");
        return NULL;
    }
    pOid = &gMsrOidType[gu4MsrOidCount];
    pOid->pu4_OidList = &gau4MsrOidType[gu4MsrOidCount++][0];
    pOid->u4_Length = 0;
    return pOid;
}

/************************************************************************
 *  Function Name   : MsrDeAllocOid
 *  Description     : DeAllocate OID from OID Pool. DeAllocation should
 *                      happen in the reverse order in which the memory
 *                      has been allocated. Otherwise it may lead to
 *                      memory corruption as an array being used as a 
 *                      memory pool.
 *  Input           : Pointer to OID that needs to be deallocated
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
MsrDeAllocOid (tSNMP_OID_TYPE ** ppOid)
{
    UNUSED_PARAM (ppOid);
    if (NULL == ppOid)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Invalid OID pointer has been passed "
                 "for memory deallocation!\n");
        return;
    }
    MSR_CLEAR_OID ((*ppOid));
    (*ppOid)->pu4_OidList = NULL;
    *ppOid = NULL;
    gu4MsrOidCount = gu4MsrOidCount - 1;

    return;
}

/************************************************************************
 *  Function Name   : MsrAllocMultiData
 *  Description     : Allocate a Multidata from multidata pool
 *  Input           : None
 *  Output          : None
 *  Returns         : Allocated Multidata pointer or null
 ************************************************************************/

tSNMP_MULTI_DATA_TYPE *
MsrAllocMultiData ()
{
    tSNMP_MULTI_DATA_TYPE *pMultiData = NULL;
    if (gu4MsrMultiDataCount >= MSR_MAX_MULTI_DATA ||
        gu4MsrOctetStrCount >= MSR_MAX_OCTET_STRING ||
        gu4MsrOidCount >= MSR_MAX_OID_TYPE)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Unable to allocate memory for MultiData\n");
        return NULL;
    }
    pMultiData = &gMsrMultiData[gu4MsrMultiDataCount++];
    pMultiData->pOctetStrValue = MsrAllocOctetString ();
    pMultiData->pOidValue = MsrAllocOid ();

    if ((pMultiData->pOctetStrValue == NULL) || (pMultiData->pOidValue == NULL))
    {
        return NULL;
    }

    return pMultiData;
}

/************************************************************************
 *  Function Name   : MsrAllocOctetString
 *  Description     : Allocate octetstring from octetstring pool
 *  Input           : None
 *  Output          : None
 *  Returns         : Pointer to the allocated octetstring or null
 ************************************************************************/

tSNMP_OCTET_STRING_TYPE *
MsrAllocOctetString ()
{
    tSNMP_OCTET_STRING_TYPE *pOctetString = NULL;
    if (gu4MsrOctetStrCount >= MSR_MAX_OCTET_STRING)
    {
        MSR_TRC (MSR_TRACE_TYPE,
                 "Unable to allocate memory for octet string\n");
        return NULL;
    }
    pOctetString = &gMsrOctetString[gu4MsrOctetStrCount];
    pOctetString->pu1_OctetList = &gau1MsrOctetString[gu4MsrOctetStrCount++][0];
    pOctetString->i4_Length = 0;
    return pOctetString;
}

/************************************************************************
 *  Function Name   : MsrValidateConfigSaveRestoreFileName
 *  Description     : This function validates given configuration file
 *                    name before a local save.
 *  Input           : None
 *  Output          : None
 *  Returns         : MSR_TRUE If file is valid
 *                    MSR_FALSE If file is invalid
 ************************************************************************/

INT4
MsrValidateConfigSaveRestoreFileName (VOID)
{
    FILE               *Fp = NULL;
    UINT1               au1FlashRestoreFileName[ISS_CONFIG_FILE_NAME_LEN];

    if (gMsrSavResPriority == EXTERN_STORAGE)
    {
        SNPRINTF ((CHR1 *) au1FlashRestoreFileName, ISS_CONFIG_FILE_NAME_LEN,
                  "%s%s", MSR_SYS_EXTN_STRG_PATH, gMsrData.au1FileName);
    }
    else
    {
        SNPRINTF ((CHR1 *) au1FlashRestoreFileName, ISS_CONFIG_FILE_NAME_LEN,
                  "%s%s", FLASH, gMsrData.au1FileName);
    }

    if ((Fp = FOPEN ((CONST CHR1 *) au1FlashRestoreFileName,
                     (CONST CHR1 *) "w")) == 0)
    {
        if (gMsrSavResPriority == EXTERN_STORAGE)
        {
            MSR_TRC (MSR_TRACE_TYPE,
                     "Cannot open file for config save on External Storage Path!!! \n");
            SNPRINTF ((CHR1 *) au1FlashRestoreFileName,
                      ISS_CONFIG_FILE_NAME_LEN, "%s%s", FLASH,
                      gMsrData.au1FileName);
            if ((Fp =
                 FOPEN ((CONST CHR1 *) au1FlashRestoreFileName,
                        (CONST CHR1 *) "w")) == 0)
            {
                MSR_TRC (MSR_TRACE_TYPE,
                         "Cannot open file for config save !!! \n");
                return MSR_FALSE;
            }
        }
        else
        {
            MSR_TRC (MSR_TRACE_TYPE, "Cannot open file for config save !!! \n");
            return MSR_FALSE;
        }
    }

    fclose (Fp);
    return MSR_TRUE;
}

/***************************************************************************  
 * FUNCTION NAME : MsrSendRestorationCompleteToProtocols 
 * DESCRIPTION   : This function sends event to Protocols on completion
                   of mib save restoration .
 *                 
 * INPUT         : NONE.
 * 
 * OUTPUT        : NONE
****************************************************************************/
VOID
MsrSendRestorationCompleteToProtocols ()
{
#ifdef L2RED_WANTED
#ifdef RM_WANTED
    tRmProtoEvt         ProtoEvt;
#endif
#endif
    tFmFaultMsg         FmFaultMsg;
    MEMSET (&FmFaultMsg, 0, sizeof (tFmFaultMsg));

    if (FIPS_MODE == FipsGetFipsCurrOperMode ())
    {
        MSR_TRC (MSR_TRACE_TYPE,
                 "Current Mode is FIPS!!! Default configurations "
                 "for FIPS is set\n");
        /* Function call to configure the default
         * status of FIPS mode */
        if (LAST_MIB_SAVE_SUCCESSFUL == gi4MibResStatus)
        {
            FipsApiSetDefaultFipsModeConfig (OSIX_TRUE);
        }
        else
        {
            FipsApiSetDefaultFipsModeConfig (OSIX_FALSE);
        }
    }

#ifdef MBSM_WANTED
    MbsmSendEventToMbsmTask (MSR_RESTORE_COMPLETE_EVENT);
#endif /* MBSM_WANTED */
#ifdef QOSX_WANTED
    QOSSendEventToQOSTask (MSR_RESTORE_COMPLETE_EVENT);
#endif /* QOSX_WANTED */
#ifdef L2RED_WANTED
#ifdef RM_WANTED
    /* Send an event to RM to notify that the restore 
     * operation is complete. */
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_MSR_APP_ID;
    ProtoEvt.u4Error = RM_NONE;
    ProtoEvt.u4Event = RM_PROTOCOL_SEND_EVENT;
    ProtoEvt.u4SendEvent = MSR_RESTORE_COMPLETE;

    /* Set the configuration restoration status as restored. */
    RmSetStaticConfigStatus (RM_STATIC_CONFIG_RESTORED);

    if (RmApiHandleProtocolEvent (&ProtoEvt) != RM_SUCCESS)
    {
        MSR_TRC (MSR_TRACE_TYPE | MSR_RM_TRACE_TYPE,
                 "MSR_RESTORE_COMPLETE: Send evt to RM failure\n");
    }
    else
    {
        MSR_TRC (MSR_TRACE_TYPE | MSR_RM_TRACE_TYPE,
                 "MsrSendRestorationCompleteToProtocols: "
                 "Restoration completed event sent to RM module\r\n");
    }
#endif /* RM_WANTED */
#endif
    if (LAST_MIB_RESTORE_FAILED == gi4MibResStatus)
    {
        /* When MSR restoration failed, default entries would not have
         * been created in the protocol modules. Indicate the protocol
         * modules to create the default entries */
#ifdef VLAN_WANTED
        VlanProcessRestorationComplete ();
#endif
#ifdef CFA_WANTED
        CfaIndicateConfigRestoreFail ();
#endif
#ifdef FIREWALL_WANTED
        /* Enable firewall by default after setting some default rules */
        FwlAddDefaultRules ();
#endif
    }

    /*If the msr restore is enabled then task complete flag has to be set 
     * here otherwise it will be set from IssSystemStartup function in
     * lrmain.c
     */
    if (IssGetRestoreOptionFromNvRam () == ISS_CONFIG_RESTORE)
    {
        LrSetRestoreDefConfigFlag (OSIX_TRUE);
    }
#ifdef MBSM_WANTED
    MbsmInformRestorationComplete ();
#endif
#ifdef CLI_WANTED
    /*CLI will be wailting for me to complete.. Let him resume */
    CliInformRestorationComplete ();
#endif
#ifdef ICCH_WANTED
    HbIcchResumeProtocolOperation ();
#endif /* ICCH_WANTED */

    /* Send the MSR restoration complete event to HTTP */
#ifdef WEBNM_WANTED
    HttpSendEventToHttpTask (MSR_INFORM_HTTP_EVENT);
#endif

    FmFaultMsg.u4EventType = ISS_FM_MSR_RELOAD_COMPLETE;
    FmFaultMsg.u4CriticalInfo = (UINT4) gi4MibResStatus;
    ISS_STRCPY (FmFaultMsg.ModuleName, ((UINT1 *) "MSR"));
#ifdef FM_WANTED
    FmApiNotifyFaults (&FmFaultMsg);
#endif
#ifdef IGS_WANTED
    SnoopInformRestorationComplete ();
#endif
    MsrIndMsrCompleteStatusToProto (gi4MibResStatus);
    return;
}

/******************************************************************************
 * Function           : MsrSendEventToMsrTask
 * Input(s)           : u4Event - Event
 * Output(s)          : None
 * Returns            : MSR_SUCCESS/MSR_FAILURE
 * Action             : Other modules should call this routine to
 *                      post any event to MSR task.
 ******************************************************************************/

INT4
MsrSendEventToMsrTask (UINT4 u4Event)
{
    if (MsrId == 0)
    {
        if (OsixGetTaskId (SELF, (const UINT1 *) MSR_TASK_NAME,
                           &MsrId) != OSIX_SUCCESS)
        {
            MSR_TRC (MSR_TRACE_TYPE, "Event send to MSR failed!!!\n");
            return MSR_FAILURE;
        }
    }

    OsixEvtSend (MsrId, u4Event);
    return MSR_SUCCESS;
}

/******************************************************************************
* Function :   MsrParseSubIdNew
* 
* Description : Parse the string format in number.number..format.
*
* Input       : ppu1TempPtr - pointer to the string.
*               pu4Value    - Pointer the OID List value.
*               
* Output      : value of ppu1TempPtr
*
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*******************************************************************************/

INT4
MsrpParseSubIdNew (UINT1 **ppu1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = 0;
    UINT1              *pu1Tmp = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    for (pu1Tmp = *ppu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                                 ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                                 ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        u4Value = (u4Value * 10) + (*pu1Tmp & 0xf);
    }

    if (*ppu1TempPtr == pu1Tmp)
    {
        i4RetVal = OSIX_FAILURE;
    }
    *ppu1TempPtr = pu1Tmp;
    *pu4Value = u4Value;
    return (i4RetVal);
}

/******************************************************************************
* Function :  MsrpMakeObjIdFrmString 
*
* Description: This Function retuns the OID  of the given string for the 
*              proprietary MIB.
*
* Input    :   pi1TextStr - pointer to the string.
*              pTableName - TableName has to be fetched.
*
* Output   :   None.
*
* Returns  :   pOidPtr or NULL
*******************************************************************************/

tSNMP_OID_TYPE     *
MsrpMakeObjIdFrmString (INT1 *pi1TextStr, UINT1 *pu1TableName)
{
    tSNMP_OID_TYPE     *pOidPtr;
    INT1               *pi1DotPtr;
    UINT2               u2Index;
    UINT2               u2DotCount;
    INT1                ai1TempBuffer[MSR_MAX_OID_LEN + 1];
    UINT1              *pu1TempPtr = NULL;
    struct MIB_OID     *pTableName = NULL;

    pTableName = (struct MIB_OID *) (VOID *) pu1TableName;

    MEMSET (ai1TempBuffer, 0, (MSR_MAX_OID_LEN + 1));

    /* see if there is an alpha descriptor at begining */
    if (isalpha (*pi1TextStr))
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pu1TempPtr = (UINT1 *) pi1TextStr;

        for (u2Index = 0;
             ((pu1TempPtr < (UINT1 *) pi1DotPtr) && (u2Index < 256)); u2Index++)
        {
            ai1TempBuffer[u2Index] = (INT1) *pu1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';
        for (u2Index = 0; pTableName[u2Index].pName != NULL; u2Index++)
        {
            if ((STRCMP
                 (pTableName[u2Index].pName,
                  (INT1 *) ai1TempBuffer) == 0)
                && (STRLEN ((INT1 *) ai1TempBuffer) ==
                    STRLEN (pTableName[u2Index].pName)))
            {
                STRNCPY ((INT1 *) ai1TempBuffer, pTableName[u2Index].pNumber,
                         (sizeof (ai1TempBuffer) - 1));
                break;
            }
        }

        if (pTableName[u2Index].pName == NULL)
        {
            return (NULL);
        }
        /* now concatenate the non-alpha part to the begining */
        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr,
                 (sizeof (ai1TempBuffer) - STRLEN (ai1TempBuffer) - 1));
    }
    else
    {                            /* is not alpha, so just copy into ai1TempBuffer */
        STRCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr);
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    u2DotCount = 0;
    for (u2Index = 0; u2Index < sizeof (ai1TempBuffer) &&
         ai1TempBuffer[u2Index] != '\0'; u2Index++)
    {
        if (ai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }
    pOidPtr = alloc_oid (SNMP_MAX_OID_LENGTH);
    if (pOidPtr == NULL)
    {
        return (NULL);
    }

    pOidPtr->u4_Length = (UINT4) (u2DotCount + 1);

    /* now we convert number.number.... strings */
    pu1TempPtr = (UINT1 *) ai1TempBuffer;
    for (u2Index = 0; u2Index < u2DotCount + 1; u2Index++)
    {
        if (MsrpParseSubIdNew (&pu1TempPtr, &(pOidPtr->pu4_OidList[u2Index])) ==
            OSIX_FAILURE)
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }

        if (*pu1TempPtr == '.')
        {
            pu1TempPtr++;        /* to skip over dot */
        }
        else if (*pu1TempPtr != '\0')
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MSRSendUpdateFailureNotification                 */
/*                                                                          */
/*   Description        : This function is used to send trap in case of failure  */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MSR_TRUE if the save operation is successful     */
/*****************************************************************************/

VOID
MSRSendUpdateFailureNotification (tMSRUpdatedata * pQData)
{

    /*Form varbind */
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    UINT1               au1Buf[MSR_MAX_OID_LEN];
    UINT1               au1Oid[MSR_MAX_OID_LEN + 1];
    tSNMP_OCTET_STRING_TYPE *pOctet = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *ptOid = NULL;
    tSNMP_OID_TYPE     *ptempOid = NULL;
    UINT4               au4SnmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;
    tSNMP_COUNTER64_TYPE u8CounterVal;
    UINT4               u4Count = 0, u4Value = 0, u4Temp = 0;
    tSNMP_OID_TYPE      MsrPrefixTrapOid;
    tSNMP_OID_TYPE      MsrSuffixTrapOid;
    tSNMP_OID_TYPE     *pMsrTrapOid;
    UINT4               au4MsrPrefixTrapOid[] = { 1, 3, 6, 1, 4, 1 };
    UINT4               au4MsrSuffixTrapOid[] = { 81, 0, 0 };

    u8CounterVal.msn = 0;
    u8CounterVal.lsn = 0;

    MsrPrefixTrapOid.pu4_OidList = au4MsrPrefixTrapOid;
    MsrPrefixTrapOid.u4_Length = sizeof (au4MsrPrefixTrapOid) / sizeof (UINT4);

    MsrSuffixTrapOid.pu4_OidList = au4MsrSuffixTrapOid;
    MsrSuffixTrapOid.u4_Length = sizeof (au4MsrSuffixTrapOid) / sizeof (UINT4);

    pMsrTrapOid = alloc_oid (SNMP_MAX_OID_LENGTH);
    if (pMsrTrapOid == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE,
                 "Unable to allocate memory for pMsrTrapOid!!!\n");
        return;
    }

    MEMSET (pMsrTrapOid->pu4_OidList, 0,
            (sizeof (UINT4) * SNMP_MAX_OID_LENGTH));
    pMsrTrapOid->u4_Length = 0;

    if (SNMPAddEnterpriseOid (&MsrPrefixTrapOid, &MsrSuffixTrapOid,
                              pMsrTrapOid) == OSIX_FAILURE)
    {
        free_oid (pMsrTrapOid);
        MSR_TRC (MSR_TRACE_TYPE,
                 "MSRSendUpdateFailureNotification: Adding EnterpriseOid failed \n");
        return;
    }

    MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);
    STRNCPY (au1Oid, pQData->pOid, (sizeof (au1Oid) - 1));
    au1Oid[(sizeof (au1Oid) - 1)] = '\0';
    pOctet = allocmem_octetstring (MSR_MAX_DATA_LEN);
    if (pOctet == NULL)
    {
        free_oid (pMsrTrapOid);
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        return;
    }
    pEnterpriseOid = alloc_oid (SNMP_MAX_OID_LENGTH);
    if (pEnterpriseOid == NULL)
    {
        free_oid (pMsrTrapOid);
        free_octetstring (pOctet);
        MSR_TRC (MSR_TRACE_TYPE,
                 "Unable to allocate memory for pEnterpriseOid!!!\n");
        return;
    }

    MEMCPY (pEnterpriseOid->pu4_OidList, pMsrTrapOid->pu4_OidList,
            pMsrTrapOid->u4_Length * sizeof (UINT4));
    pEnterpriseOid->u4_Length = pMsrTrapOid->u4_Length;
    free_oid (pMsrTrapOid);
    pSnmpTrapOid = alloc_oid (MSR_SNMPV2_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        free_octetstring (pOctet);
        FREE_OID (pEnterpriseOid);
        MSR_TRC (MSR_TRACE_TYPE,
                 "Unable to allocate memory for pSnmpTrapOid!!!\n");
        return;
    }
    MEMCPY (pSnmpTrapOid->pu4_OidList, au4SnmpTrapOid,
            MSR_SNMPV2_TRAP_OID_LEN * sizeof (UINT4));
    pEnterpriseOid->u4_Length = pEnterpriseOid->u4_Length - 1;
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = 1;

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0, 0, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        free_octetstring (pOctet);
        FREE_OID (pEnterpriseOid);
        FREE_OID (pSnmpTrapOid);
        MSR_TRC (MSR_TRACE_TYPE,
                 "Unable to allocate memory for pSnmpTrapOid!!!\n");
        return;
    }
    pStartVb = pVbList;

    /*forming varbind for OID value */

    SPRINTF ((char *) au1Buf, "issMsrFailedOid");
    pOid =
        MsrpMakeObjIdFrmString ((INT1 *) au1Buf,
                                (UINT1 *) fs_iss_mib_oid_table);
    if (pOid == NULL)
    {
        free_octetstring (pOctet);
        SNMP_free_snmp_vb_list (pStartVb);
        MSR_TRC (MSR_TRACE_TYPE, "OID Not Found!!\n");
        return;
    }
    ptempOid = alloc_oid (MSR_MAX_OID_LEN);
    if (ptempOid == NULL)
    {
        free_octetstring (pOctet);
        FREE_OID (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        MSR_TRC (MSR_TRACE_TYPE,
                 "Unable to allocate memory for pEnterpriseOid!!!\n");
        return;
    }

    WebnmConvertStringToOid (ptempOid, au1Oid, MSR_FALSE);
    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_OBJECT_ID,
                                                  0, 0, NULL,
                                                  (tSNMP_OID_TYPE *) (ptempOid),
                                                  u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        free_octetstring (pOctet);
        FREE_OID (pOid);
        FREE_OID (ptempOid);
        SNMP_free_snmp_vb_list (pStartVb);
        MSR_TRC (MSR_TRACE_TYPE,
                 "Unable to allocate memory for pVbList->pNextVarBind!!!\n");
        return;
    }

    /*forming varbind for value */
    pVbList = pVbList->pNextVarBind;
    SPRINTF ((char *) au1Buf, "issMsrFailedValue");
    pOid =
        MsrpMakeObjIdFrmString ((INT1 *) au1Buf,
                                (UINT1 *) fs_iss_mib_oid_table);
    if (pOid == NULL)
    {
        free_octetstring (pOctet);
        SNMP_free_snmp_vb_list (pStartVb);
        MSR_TRC (MSR_TRACE_TYPE, "OID Not Found!!\n");
        return;
    }
    ptOid = alloc_oid (MSR_SNMPV2_TRAP_OID_LEN);
    if (ptOid == NULL)
    {
        free_octetstring (pOctet);
        SNMP_free_snmp_vb_list (pStartVb);
        FREE_OID (pOid);
        MSR_TRC (MSR_TRACE_TYPE,
                 "Unable to allocate memory for pEnterpriseOid!!!\n");
        return;
    }

    MEMSET (pOctet->pu1_OctetList, 0, MSR_MAX_OID_LEN);
    switch (pQData->pSnmpData->i2_DataType)
    {
        case INTEGER32:
            SPRINTF ((char *) pOctet->pu1_OctetList, "%d",
                     pQData->pSnmpData->i4_SLongValue);
            pOctet->i4_Length = (INT4) MSR_STRLEN (pOctet->pu1_OctetList);

            break;

        case TIMETICKS:
        case UNSIGNED32:
        case COUNTER32:
            SPRINTF ((char *) pOctet->pu1_OctetList, "%u",
                     pQData->pSnmpData->u4_ULongValue);
            pOctet->i4_Length = (INT4) MSR_STRLEN (pOctet->pu1_OctetList);
            break;

        case COUNTER64:
            SPRINTF ((char *) pOctet->pu1_OctetList, "%u:%u",
                     pQData->pSnmpData->u8_Counter64Value.lsn,
                     pQData->pSnmpData->u8_Counter64Value.msn);
            pOctet->i4_Length = (INT4) MSR_STRLEN (pOctet->pu1_OctetList);
            break;

        case OBJECTIDENTIFIER:
            if (pQData->pSnmpData->pOidValue->u4_Length == 0)
                break;
            MEMSET (au1Buf, 0, sizeof (au1Buf));
            for (u4Count = 0;
                 u4Count < (pQData->pSnmpData->pOidValue->u4_Length - 1);
                 u4Count++)
            {
                au1Buf[0] = '\0';
                SPRINTF ((char *) au1Buf, "%d.",
                         pQData->pSnmpData->pOidValue->pu4_OidList[u4Count]);
                MSR_STRCAT (pOctet->pu1_OctetList, au1Buf);
            }
            au1Buf[0] = '\0';
            SPRINTF ((char *) au1Buf, "%d",
                     pQData->pSnmpData->pOidValue->pu4_OidList[u4Count]);
            MSR_STRCAT (pOctet->pu1_OctetList, au1Buf);
            au1Buf[0] = '\0';
            MSR_STRCAT (pOctet->pu1_OctetList, au1Buf);

            pOctet->i4_Length = (INT4) MSR_STRLEN (pOctet->pu1_OctetList);
            break;

        case OCTETSTRING:
            if (pQData->pSnmpData->pOctetStrValue->i4_Length == 0)
                break;
            MSR_MEMCPY (pOctet->pu1_OctetList,
                        pQData->pSnmpData->pOctetStrValue->pu1_OctetList,
                        pQData->pSnmpData->pOctetStrValue->i4_Length);
            pOctet->i4_Length = pQData->pSnmpData->pOctetStrValue->i4_Length;
            break;

        case IPADDRESS:
            u4Value = pQData->pSnmpData->u4_ULongValue;
            MEMSET (au1Buf, 0, sizeof (au1Buf));
            for (u4Count = 0; u4Count < (IPADDRESS_LEN - 1); u4Count++)
            {
                u4Temp = (u4Value & 0xff000000);
                u4Temp = u4Temp >> 24;
                au1Buf[0] = '\0';
                SPRINTF ((char *) au1Buf, "%u.", u4Temp);
                MSR_STRCAT (pOctet->pu1_OctetList, au1Buf);
                u4Value = u4Value << BYTE_LEN;
            }
            u4Temp = (u4Value & 0xff000000);
            u4Temp = u4Temp >> 24;
            SPRINTF ((char *) au1Buf, "%u", u4Temp);
            MSR_STRCAT (pOctet->pu1_OctetList, au1Buf);
            pOctet->i4_Length = (INT4) MSR_STRLEN (pOctet->pu1_OctetList);
            break;

        default:
            free_octetstring (pOctet);
            FREE_OID (ptOid);
            FREE_OID (pOid);
            SNMP_free_snmp_vb_list (pStartVb);
            return;
    }

    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_OCTET_PRIM,
                                                  0, 0,
                                                  (tSNMP_OCTET_STRING_TYPE *)
                                                  pOctet, NULL, u8CounterVal);

    if (pVbList->pNextVarBind == NULL)
    {
        free_octetstring (pOctet);
        FREE_OID (ptOid);
        FREE_OID (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        MSR_TRC (MSR_TRACE_TYPE,
                 "Unable to allocate memory for pVbList->pNextVarBind!!!\n");
        return;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->ObjValue.pOidValue = ptOid;
    pVbList->pNextVarBind = NULL;
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
    KW_FALSEPOSITIVE_FIX (ptOid);
    /*freeing memory if all is succcessfull */
}

/************************************************************************
 *  Function Name   : MSRInsertVerOrFormatInfoToRBTree 
 *  Description     : Copys MSR version and File format info into RB tree 
 *  Input           : None 
 *  Output          : None
 *  Returns         : MSR_SUCCESS or MSR_FAILURE
 ************************************************************************/
INT4
MSRInsertVerOrFormatInfoToRBTree (VOID)
{
    UINT4               u4Index = 0;
    tMSRRbtree         *pRBNodeCur = NULL;
    INT4                i4RetVal = 0, i4SnmpRetval = SNMP_FAILURE;
    INT4                i4Result = MSR_SUCCESS;
    tSNMP_OID_TYPE     *pCurOid = NULL;
    tSNMP_MULTI_DATA_TYPE *pData = NULL;
    UINT4               u4Error = 0;
    tSnmpIndex         *pCurIndex = MsrIndexPool1;
    UINT1               au1Oid[MSR_MAX_OID_LEN];

    gu4MsrMultiDataCount = 0;
    gu4MsrOctetStrCount = 0;
    gu4MsrOidCount = 0;

    if ((pCurOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        return MSR_FAILURE;
    }
    if ((pData = MsrAllocMultiData ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FAILURE;
    }

    while (u4Index < MSR_RES_FILE_VER_FORMAT)
    {
        MSR_MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);
        MSR_CLEAR_OID (pCurOid);

        if (u4Index == 0)
        {
            WebnmConvertStringToOid (pCurOid,
                                     (UINT1 *) ISS_RESTORE_FILE_VERSION_OID,
                                     MSR_FALSE);
        }
        else
        {
            WebnmConvertStringToOid (pCurOid,
                                     (UINT1 *) ISS_RESTORE_FILE_FORMAT_OID,
                                     MSR_FALSE);
        }

        i4SnmpRetval = SNMPGet (*pCurOid, pData, &u4Error, pCurIndex,
                                SNMP_DEFAULT_CONTEXT);
        if (i4SnmpRetval == SNMP_SUCCESS)
        {
            i4RetVal = 0;
            if ((FN_VAL (u4Index)) != NULL)
            {
                i4RetVal = (FN_VAL (u4Index)) (pCurOid, NULL, pData);
                if (i4RetVal != MSR_SAVE)
                {
                    gu4MsrOidCount = 0;
                    gu4MsrMultiDataCount = 0;
                    gu4MsrOctetStrCount = 0;
                    return MSR_FAILURE;
                }
            }
            i4Result = MSRSearchNodeinRBTree (pCurOid, u4Index,
                                              MSR_NONROWSTATUS, &pRBNodeCur);
            KW_FALSEPOSITIVE_FIX (pRBNodeCur);
            if (i4Result == MSR_TRUE)
            {
                /* data exists in the RB tree so Modify the 
                 * data in the RBNode with new value. */
                i4Result = MSRModifyRBNodeinRBTree (pData, pRBNodeCur);
                if (i4Result == MSR_FALSE)
                {
                    gu4MsrOidCount = 0;
                    gu4MsrMultiDataCount = 0;
                    gu4MsrOctetStrCount = 0;
                    return MSR_FAILURE;
                }
            }
            else
            {
                /* data not exist in the RBTree, Add new node into 
                 * RBTree and update Linklist */
                i4Result = MSRInsertRBNodeinRBTree (pCurOid,
                                                    pData, u4Index,
                                                    MSR_NONROWSTATUS, MSR_TRUE);

                if (i4Result != MSR_TRUE)
                {
                    gu4MsrOidCount = 0;
                    gu4MsrMultiDataCount = 0;
                    gu4MsrOctetStrCount = 0;
                    return MSR_FAILURE;
                }
            }
        }
        else
        {
            gu4MsrOidCount = 0;
            gu4MsrMultiDataCount = 0;
            gu4MsrOctetStrCount = 0;
            return MSR_FAILURE;
        }

        /* For Inserting File Format */
        u4Index++;
    }

    gu4MsrOidCount = 0;
    gu4MsrMultiDataCount = 0;
    gu4MsrOctetStrCount = 0;
    return MSR_SUCCESS;
}

/************************************************************************
 *  Function Name   : MsrProcesssnmpEvt 
 *  Description     : This function process SNMP data the update event
 *  Input(s)        : pQData                                           
 *  Output(s)       : None.                                            
 *  Returns         : None. 
 *  
 ************************************************************************/
VOID
MsrProcesssnmpEvt (tMSRUpdatedata * pQData)
{

    /* msr debug stats counter */
    gU4NoOfQMsgRead++;

    if (MSR_ISSCALAR (pQData))
    {
        /* scalar Case */
        MSRProcessUpdateScalarObj (pQData);
    }
    else if ((pQData->isRowStatusPresent == 1) &&
             (pQData->pSnmpData->i4_SLongValue == MSR_DESTROY))
    {
        /* Tabular case with ACTIVE received for Rowstatus column */
        /* convert OID received in to string to search in msrarray */
        MSRProcessUpdateDestroyRS (pQData);
    }
    else if (pQData->isRowStatusPresent == 0)
    {
        /* Tabular case with update received for nonRowstatus column
         * get the current and default value of this instance only
         * (not for complete column elements in the table) and update 
         * the RB Tree according */
        MSRProcessUpdateNonRSObj (pQData);
    }
    else if ((pQData->isRowStatusPresent == 1) &&
             (pQData->pSnmpData->i4_SLongValue != MSR_DESTROY))
    {
        if (pQData->pSnmpData->i4_SLongValue == MSR_CREATE_AND_WAIT)
        {
            MSRProcessUpdateRSCWandCG (pQData, MSR_FALSE);
        }
        else
        {
            if (pQData->pSnmpData->i4_SLongValue == MSR_CREATE_AND_GO)
            {
                /* If CREATE_AND_GO comes, treat as ACTIVE. because
                 * in following api MSRProcessUpdateRSActive, internally
                 * a node will be added for create_and_wait state
                 */
                pQData->pSnmpData->i4_SLongValue = MSR_ACTIVE;
            }
            /* in this case row status received with value 
             * other than CREATE_AND_WAIT or  CREATE_AND_GO */
            MSRProcessUpdateRSActive (pQData);
        }
    }
    else
    {
        /* data received in update event is not matching to 
         * any case so send sedn trap and free the data received 
         *  and release lock if snmp wating for it */
        MSRSendUpdateFailureNotification (pQData);
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MSRUpdateISSConfFile                             */
/*                                                                          */
/*   Description        : This function is used to  update iss.conf file    */
/*                        with data stored in RBtree                        */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MSR_TRUE if the save operation is successful     */
/****************************************************************************/
INT4
MSRUpdateISSConfFile (UINT4 u4FlashDataType, CONST CHR1 * pu1FlashFileName)
{
    INT4                i4SaveFd = 0;
    UINT1              *pTemp = NULL;
    UINT4               u4Size = 0;
    CHR1                au1RestoreFileName[MSR_MAX_FLASH_FILE_LEN];
    tMSRRbtree         *pRBCurNode = NULL;
    tMSRRbtree         *pRBNextNode = NULL;
    UINT2               u2ChkSum = 0;
    UINT2               u2CkSum = 0;
    UINT4               u4Sum = 0;
    UINT1               u1Tmp = 0;
    UINT4               u4Length = 0;
    UINT1               au1Oid[MSR_MAX_OID_LEN];
    UINT1              *pu1Data = NULL;
    UINT1               au1PrefixOid[MSR_MAX_OID_LEN];
    UINT1              *pOid = NULL;
    UINT2               u2PrefLen = 0;
    UINT4               u4arrayIndx = 0;
    INT4                i4RetVal = 0;

    UNUSED_PARAM (u4FlashDataType);

    gu4Length = 0;
    MSR_MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);
    MSR_MEMSET (au1PrefixOid, 0, MSR_MAX_OID_LEN);

    MEMSET (au1RestoreFileName, 0, sizeof (au1RestoreFileName));

    MSR_STRCPY (au1RestoreFileName, FLASH);
    MSR_STRNCAT (au1RestoreFileName, pu1FlashFileName,
                 (sizeof (au1RestoreFileName) -
                  STRLEN (au1RestoreFileName) - 1));

    if ((i4SaveFd = FileOpen ((UINT1 *) au1RestoreFileName,
                              (OSIX_FILE_CR | OSIX_FILE_WO | OSIX_FILE_TR))) <
        0)
    {
        MSR_TRC (MSR_TRACE_TYPE, "File Open Failed ... \n");
        return MSR_FAILURE;
    }

    /* get lowest key entry from RB tree and prepare pTemp data and 
     * write the same data to iss.conf file */
    pRBCurNode = (tMSRRbtree *) RBTreeGetFirst (gMSRRBTable);
    if (pRBCurNode == NULL)
    {
        /* no data in the RB Tree */
        FileClose (i4SaveFd);
        return MSR_FAILURE;
    }
    if (MSR_DATA_ALLOC_MEM_BLOCK (pu1Data) == NULL)
    {
        FileClose (i4SaveFd);
        return MSR_FAILURE;
    }
    MSR_MEMSET (pu1Data, 0, MSR_MAX_DATA_LEN);
    pTemp = pu1Data;
    WebnmConvertOidToString (pRBCurNode->pOidValue, au1Oid);
    if (MSR_STRCMP (au1Oid, gaSaveArray[0].pOidString) == 0)
    {
        SnmpRevertEoidString (au1Oid);
        sprintf ((char *) pTemp, "%s\t", au1Oid);
        u4Size = MSR_STRLEN (pTemp);
        pTemp += MSR_STRLEN (pTemp);
        u4arrayIndx = pRBCurNode->MSRArrayIndex;
        MSR_MEMCPY (pTemp, pRBCurNode->pData, pRBCurNode->u4pDataSize);
        u4Size += pRBCurNode->u4pDataSize;
        gu4Length += u4Size;
        pTemp = pu1Data;
        UtilCalcCheckSum (pTemp, u4Size, &u4Sum, &u2CkSum, CKSUM_DATA);
        i4RetVal = (INT4) FileWrite (i4SaveFd, (CHR1 *) pTemp,
                                     MEM_MAX_BYTES (u4Size, MSR_MAX_DATA_LEN));
        if (i4RetVal < 0)
        {
            FileClose (i4SaveFd);
            MSR_DATA_FREE_MEM_BLOCK (pu1Data);
            return MSR_FAILURE;
        }
    }

    pTemp = pu1Data;
    MSR_MEMSET (pu1Data, 0, MSR_MAX_DATA_LEN);
    MSR_MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);

    pRBNextNode =
        (tMSRRbtree *) RBTreeGetNext (gMSRRBTable, pRBCurNode,
                                      (tRBCompareFn) MSRCompareRBNodes);
    if (pRBNextNode == NULL)
    {
        /* no data in the RB Tree */
        FileClose (i4SaveFd);
        MSR_DATA_FREE_MEM_BLOCK (pu1Data);
        return MSR_FAILURE;
    }

    WebnmConvertOidToString (pRBNextNode->pOidValue, au1Oid);
    if (MSR_STRCMP (au1Oid, gaSaveArray[1].pOidString) == 0)
    {
        SnmpRevertEoidString (au1Oid);
        sprintf ((char *) pTemp, "%s\t", au1Oid);
        u4Size = MSR_STRLEN (pTemp);
        pTemp += MSR_STRLEN (pTemp);
        u4arrayIndx = pRBNextNode->MSRArrayIndex;
        MSR_MEMCPY (pTemp, pRBNextNode->pData, pRBNextNode->u4pDataSize);
        u4Size += pRBNextNode->u4pDataSize;
        gu4Length += u4Size;
        pTemp = pu1Data;
        UtilCalcCheckSum (pTemp, u4Size, &u4Sum, &u2CkSum, CKSUM_DATA);
        i4RetVal = (INT4) FileWrite (i4SaveFd, (CHR1 *) pTemp,
                                     MEM_MAX_BYTES (u4Size, MSR_MAX_DATA_LEN));
        if (i4RetVal < 0)
        {
            FileClose (i4SaveFd);
            MSR_DATA_FREE_MEM_BLOCK (pu1Data);
            return MSR_FAILURE;
        }

    }

    pRBCurNode = pRBNextNode;
    pRBNextNode = NULL;

    /* Get all the elements in the RB tree by using this first node and 
     * RBTreeGetNext() function */
    while (1)
    {
        pRBNextNode =
            (tMSRRbtree *) RBTreeGetNext (gMSRRBTable, pRBCurNode,
                                          (tRBCompareFn) MSRCompareRBNodes);

        if ((pRBNextNode != NULL) &&
            (pRBNextNode->u1IsDataToBeSaved == MSR_TRUE))
        {
            pTemp = pu1Data;
            MSR_MEMSET (pu1Data, 0, MSR_MAX_DATA_LEN);
            MSR_MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);
            WebnmConvertOidToString (pRBNextNode->pOidValue, au1Oid);
            pOid = au1Oid;

            if (u4arrayIndx != pRBNextNode->MSRArrayIndex)
            {
                u4arrayIndx = pRBNextNode->MSRArrayIndex;
                MsrObtainTablePrefixOid (u4arrayIndx, au1PrefixOid);
                u2PrefLen = (UINT2) MSR_STRLEN (au1PrefixOid);
                SnmpRevertEoidString (au1PrefixOid);
                sprintf ((char *) pTemp, "@%4u\t%s\n",
                         pRBNextNode->MSRArrayIndex, au1PrefixOid);
                u4Size = MSR_STRLEN (pTemp);
                gu4Length += u4Size;
                UtilCalcCheckSum (pTemp, u4Size, &u4Sum, &u2CkSum, CKSUM_DATA);
                i4RetVal = (INT4) FileWrite (i4SaveFd, (CHR1 *) pTemp, u4Size);
                if (i4RetVal < 0)
                {
                    FileClose (i4SaveFd);
                    MSR_DATA_FREE_MEM_BLOCK (pu1Data);
                    return MSR_FAILURE;
                }

                u4Size = 0;
                pTemp = pu1Data;
                MSR_MEMSET (pu1Data, 0, MSR_MAX_DATA_LEN);
            }

            pOid += u2PrefLen;
            SnmpRevertEoidString (pOid);
            sprintf ((char *) pTemp, "%u\t%s\t",
                     pRBNextNode->MSRRowStatusType, pOid);

            u4Size = MSR_STRLEN (pTemp);
            pTemp += MSR_STRLEN (pTemp);
            MSR_MEMCPY (pTemp, pRBNextNode->pData, pRBNextNode->u4pDataSize);
            u4Size += pRBNextNode->u4pDataSize;
            gu4Length += u4Size;
            pTemp = pu1Data;
            UtilCalcCheckSum (pTemp, u4Size, &u4Sum, &u2CkSum, CKSUM_DATA);
            i4RetVal = (INT4) FileWrite (i4SaveFd, (CHR1 *) pTemp, u4Size);
            if (i4RetVal < 0)
            {
                FileClose (i4SaveFd);
                MSR_DATA_FREE_MEM_BLOCK (pu1Data);
                return MSR_FAILURE;
            }

            /* to get the new node update the current node with the value in 
             * pRBNextNode */
            pRBCurNode = pRBNextNode;
            pRBNextNode = NULL;
        }
        else if ((pRBNextNode != NULL) &&
                 (pRBNextNode->u1IsDataToBeSaved == MSR_FALSE))
        {
            /* This is marked as a volatile entry. Therefore, this should not
             * be stored in the conf file. Get the next RB Tree node.
             */
            pRBCurNode = pRBNextNode;
            pRBNextNode = NULL;
        }
        else
        {
            /* end of tree is reached no new element present in the 
             * tree, brek form the loop */
            break;
        }
    }
    pTemp = (UINT1 *) pu1Data;
    MSR_MEMSET (pu1Data, 0, MSR_MAX_DATA_LEN);
    sprintf ((char *) pTemp, "CKSUM:");
    u4Length = MSR_STRLEN (pTemp);
    pTemp += MSR_STRLEN (pTemp);
    MEMCPY (pTemp, &u2ChkSum, sizeof (UINT2));
    u4Length += sizeof (UINT2);
    pTemp += sizeof (UINT2);
    gu4Length += u4Length;
    if ((gu4Length % 2) == 1)
    {
        MEMCPY (pTemp, &u1Tmp, sizeof (UINT1));
        gu4Length++;
        u4Length++;
    }
    UtilCalcCheckSum (pu1Data, u4Length, &u4Sum, &u2CkSum, CKSUM_LASTDATA);
    /* gu2FlashCksum used for statndby checksum comparision */
    gu2FlashCksum = u2CkSum;
    pTemp -= sizeof (UINT2);
    MEMCPY (pTemp, &u2CkSum, sizeof (UINT2));
    pTemp = (UINT1 *) pu1Data;
    /*u4Length += sizeof (UINT2); */
    i4RetVal = (INT4) FileWrite (i4SaveFd, (CHR1 *) pTemp, u4Length);
    if (i4RetVal < 0)
    {
        FileClose (i4SaveFd);
        MSR_DATA_FREE_MEM_BLOCK (pu1Data);
        return MSR_FAILURE;
    }
    FileClose (i4SaveFd);

#if defined(SWC)
    system ("etc flush");
#endif
    MSR_DATA_FREE_MEM_BLOCK (pu1Data);
    return MSR_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrPopulateFlashArrayFromRBTree                  */
/*                                                                          */
/*   Description        : This function is used to populate gpu1StrCurrent  */
/*                          buffer from data in RBTree                      */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MSR_TRUE if the save operation is successful     */
/****************************************************************************/
INT4
MsrPopulateFlashArrayFromRBTree (VOID)
{
    tMSRRbtree         *pRBCurNode = NULL;
    tMSRRbtree         *pRBNextNode = NULL;
    tConnStatus         ConnStatus = INITIATE;
    UINT4               u4Length = 0;
    UINT4               u4Size = 0;
    UINT4               u4MaxSize = 0;
    UINT4               u4arrayIndx = 0;
    UINT2               u4CkSum = 0;
    UINT2               u2ChkSum = 0;
    UINT4               u4Sum = 0;
    UINT2               u2PrefLen = 0;
    UINT1               u1Tmp = 0;
    UINT1               au1Oid[MSR_MAX_OID_LEN];
    UINT1               au1PrefixOid[MSR_MAX_OID_LEN];
    UINT1              *pOid = NULL;
    UINT1              *pTemp = NULL;
    UINT1              *pu1Data = NULL;

    gu4Length = 0;
    MSR_MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);
    MSR_MEMSET (au1PrefixOid, 0, MSR_MAX_OID_LEN);

    pRBCurNode = (tMSRRbtree *) RBTreeGetFirst (gMSRRBTable);
    if (pRBCurNode == NULL)
    {
        /* no data in the RB Tree so returning failure */
        return MSR_FAILURE;
    }
    if (MSR_DATA_ALLOC_MEM_BLOCK (pu1Data) == NULL)
    {
        return MSR_FAILURE;
    }
    MSR_MEMSET (pu1Data, 0, MSR_MAX_DATA_LEN);

    WebnmConvertOidToString (pRBCurNode->pOidValue, au1Oid);
    if (MSR_STRCMP (au1Oid, gaSaveArray[0].pOidString) == 0)
    {
        SnmpRevertEoidString (au1Oid);
        MSR_MEMSET (pu1Data, 0, MSR_MAX_DATA_LEN);
        pTemp = pu1Data;
        SNPRINTF ((char *) pTemp, MSR_MAX_OID_LEN, "%s\t", au1Oid);
        u4Size = MSR_STRLEN (pTemp);
        pTemp += MSR_STRLEN (pTemp);
        MSR_MEMCPY (pTemp, pRBCurNode->pData, pRBCurNode->u4pDataSize);
        u4Size += pRBCurNode->u4pDataSize;
        pTemp = pu1Data;
        MSR_MEMCPY (gpu1StrCurrent, pTemp, u4Size);
        UtilCalcCheckSum (pTemp, u4Size, &u4Sum, &u4CkSum, CKSUM_DATA);
        gpu1StrCurrent += u4Size;
    }

    pRBNextNode =
        (tMSRRbtree *) RBTreeGetNext (gMSRRBTable, pRBCurNode,
                                      (tRBCompareFn) MSRCompareRBNodes);

    if (pRBNextNode == NULL)
    {
        /* no data in the RB Tree so returning failure */
        MSR_DATA_FREE_MEM_BLOCK (pu1Data);
        return MSR_FAILURE;
    }

    pRBCurNode = pRBNextNode;
    pRBNextNode = NULL;

    WebnmConvertOidToString (pRBCurNode->pOidValue, au1Oid);
    if (MSR_STRCMP (au1Oid, gaSaveArray[1].pOidString) == 0)
    {
        SnmpRevertEoidString (au1Oid);
        MSR_MEMSET (pu1Data, 0, MSR_MAX_DATA_LEN);
        pTemp = pu1Data;
        sprintf ((char *) pTemp, "%s\t", au1Oid);
        u4Size = MSR_STRLEN (pTemp);
        pTemp += MSR_STRLEN (pTemp);
        MSR_MEMCPY (pTemp, pRBCurNode->pData, pRBCurNode->u4pDataSize);
        u4Size += pRBCurNode->u4pDataSize;
        pTemp = pu1Data;
        u4MaxSize = MEM_MAX_BYTES (u4Size, MSR_MAX_DATA_LEN);
        MSR_MEMCPY (gpu1StrCurrent, pTemp, u4MaxSize);
        UtilCalcCheckSum (pTemp, u4Size, &u4Sum, &u4CkSum, CKSUM_DATA);
        gpu1StrCurrent += u4MaxSize;
    }

    while (1)
    {
        pRBNextNode =
            (tMSRRbtree *) RBTreeGetNext (gMSRRBTable, pRBCurNode,
                                          (tRBCompareFn) MSRCompareRBNodes);

        /* Store the RB Tree information if the node is not marked as
         * a volatile entry
         */
        if ((pRBNextNode != NULL) &&
            (pRBNextNode->u1IsDataToBeSaved == MSR_TRUE))
        {
            pTemp = pu1Data;
            pOid = au1Oid;
            MSR_MEMSET (pu1Data, 0, MSR_MAX_DATA_LEN);
            MSR_MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);
            WebnmConvertOidToString (pRBNextNode->pOidValue, au1Oid);
            if (u4arrayIndx != pRBNextNode->MSRArrayIndex)
            {
                u4arrayIndx = pRBNextNode->MSRArrayIndex;
                MsrObtainTablePrefixOid (u4arrayIndx, au1PrefixOid);
                u2PrefLen = (UINT2) MSR_STRLEN (au1PrefixOid);
                SnmpRevertEoidString (au1PrefixOid);
                sprintf ((char *) pTemp, "@%4u\t%s\n",
                         pRBNextNode->MSRArrayIndex, au1PrefixOid);
                u4Size = MSR_STRLEN (pTemp);
                UtilCalcCheckSum (pTemp, u4Size, &u4Sum, &u4CkSum, CKSUM_DATA);
                if (gpu1StrCurrent + u4Size >= gpu1StrEnd)
                {
                    if (MsrSendDataToRemote (&ConnStatus) == MSR_FAILURE)
                    {
                        MSR_DATA_FREE_MEM_BLOCK (pu1Data);
                        return MSR_FAILURE;
                    }
                }
                MSR_MEMCPY (gpu1StrCurrent, pTemp, u4Size);
                gpu1StrCurrent += u4Size;

                MSR_MEMSET (pu1Data, 0, MSR_MAX_DATA_LEN);
            }

            pOid += u2PrefLen;
            SnmpRevertEoidString (pOid);
            sprintf ((char *) pTemp, "%u\t%s\t",
                     pRBNextNode->MSRRowStatusType, pOid);
            u4Size = MSR_STRLEN (pTemp);
            pTemp += MSR_STRLEN (pTemp);
            MSR_MEMCPY (pTemp, pRBNextNode->pData, pRBNextNode->u4pDataSize);
            u4Size += pRBNextNode->u4pDataSize;
            pTemp = pu1Data;
            if (gpu1StrCurrent + u4Size >= gpu1StrEnd)
            {
                if (MsrSendDataToRemote (&ConnStatus) == MSR_FAILURE)
                {
                    MSR_DATA_FREE_MEM_BLOCK (pu1Data);
                    return MSR_FAILURE;
                }
            }
            MSR_MEMCPY (gpu1StrCurrent, pTemp, u4Size);
            UtilCalcCheckSum (pTemp, u4Size, &u4Sum, &u4CkSum, CKSUM_DATA);
            gpu1StrCurrent += u4Size;
        }
        else if (pRBNextNode == NULL)
        {
            break;
        }
        pRBCurNode = pRBNextNode;
        pRBNextNode = NULL;
    }
    pTemp = pu1Data;
    sprintf ((char *) pTemp, "CKSUM:");
    u4Length = MSR_STRLEN (pTemp);
    pTemp += u4Length;

    MSR_MEMCPY (pTemp, &u2ChkSum, sizeof (UINT2));
    u4Length += sizeof (UINT2);
    pTemp += sizeof (UINT2);

    if ((u4Length % 2) == 1)
    {
        MSR_MEMCPY (pTemp, &u1Tmp, sizeof (UINT1));
        u4Length++;
    }

    UtilCalcCheckSum (pu1Data, u4Length, &u4Sum, &u4CkSum, CKSUM_LASTDATA);

    /* Store the checksum value in a global var for easy access,
     * instead of calculating every time. */
    gu2FlashCksum = u4CkSum;

    /* Replace the checksum with the calculated 
     * checksum in the gpu1StrFlash 
     */

    if ((gpu1StrCurrent + u4Length) > gpu1StrEnd)
    {
        if (MsrSendDataToRemote (&ConnStatus) == MSR_FAILURE)
        {
            MSR_DATA_FREE_MEM_BLOCK (pu1Data);
            return MSR_FAILURE;
        }
    }

    pTemp -= sizeof (UINT2);

    MSR_MEMCPY (pTemp, &u4CkSum, sizeof (UINT2));
    u4MaxSize = MEM_MAX_BYTES (u4Length, MSR_MAX_DATA_LEN);
    MSR_MEMCPY (gpu1StrCurrent, pu1Data, u4MaxSize);
    gpu1StrCurrent += u4MaxSize;

    ConnStatus = TERMINATE;
    MSR_DATA_FREE_MEM_BLOCK (pu1Data);
    if (MsrSendDataToRemote (&ConnStatus) == MSR_FAILURE)
    {
        return MSR_FAILURE;
    }
    return MSR_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MSRPopulateRBtreefromISSConf                     */
/*                                                                          */
/*   Description        : This function is used to populate RB tree from    */
/*                          iss.conf file                                   */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/****************************************************************************/

INT4
MSRPopulateRBtreefromISSConf (INT4 i4ConfFd)
{
    tMsrOidArrListBlock *pu1Oid = NULL;
    UINT2               u2Type = 0;
    UINT4               u4Len = 0;
    UINT1              *pTemp = NULL;
    UINT1               au1Data[MSR_MAX_STR_LEN];
    UINT1              *pu1Cfg = NULL;
    UINT4               u4Index = 0;
    UINT4               u4MSRArrayIndex = 0;
    UINT4               u4MSRRowStatusType = 0;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_MULTI_DATA_TYPE *pData = NULL;
    UINT4               u4Total = 0;
    UINT4               u4Error = 0;
    tSNMP_MULTI_DATA_TYPE *pdefData = NULL;
    INT4                i4Result = 0;
    INT4                i4SnmpRetval = SNMP_FAILURE;
    UINT4               u4CurrArrayIndex = 0;
    tMSRRbtree         *pRBNodeCur = NULL;
    UINT2               u2CfgCntr = 0;
    UINT1               au1PrefixOid[MSR_MAX_OID_LEN];
    UINT1               au1SuffixOid[MSR_MAX_OID_LEN];
    UINT1               u1DataCntr = 0;
    UINT1               u1LineFlag = MSR_FALSE;
    INT4                i4DataLen;

    gu4MsrMultiDataCount = 0;
    gu4MsrOctetStrCount = 0;
    gu4MsrOidCount = 0;

    gi4MibResStatus = MIB_RESTORE_IN_PROGRESS;
    if ((pOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gi4MibResFailureReason = MIB_RES_MEMORY_FAILURE;
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        FileClose (i4ConfFd);
        return ISS_FAILURE;
    }
    if ((pdefData = MsrAllocMultiData ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gi4MibResFailureReason = MIB_RES_MEMORY_FAILURE;
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        gu4MsrOidCount = 0;
        FileClose (i4ConfFd);
        return ISS_FAILURE;
    }

    MSR_MEMSET (au1PrefixOid, 0, MSR_MAX_OID_LEN);
    MSR_MEMSET (au1SuffixOid, 0, MSR_MAX_OID_LEN);
    MSR_MEMSET (au1Data, 0, MSR_MAX_STR_LEN);

    if (MSR_DATA_ALLOC_MEM_BLOCK (pu1Cfg) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gi4MibResFailureReason = MIB_RES_MEMORY_FAILURE;
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        gu4MsrOidCount = 0;
        FileClose (i4ConfFd);
        return ISS_FAILURE;
    }
    MSR_MEMSET (pu1Cfg, 0, MSR_MAX_DATA_LEN);
    if ((pu1Oid = MemAllocMemBlk (gMsrOidArrListPoolId)) == NULL)
    {
        MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gi4MibResFailureReason = MIB_RES_MEMORY_FAILURE;
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        gu4MsrOidCount = 0;
        FileClose (i4ConfFd);
        return ISS_FAILURE;
    }
    MSR_MEMSET (pu1Oid->au1MsrOidArrListBlock, 0, MSR_MAX_OID_LEN);
    while (((i4DataLen = FileRead (i4ConfFd, (CHR1 *) au1Data,
                                   MSR_MAX_FILE_READ_LEN)) > 0)
           && (i4DataLen != (INT4) MSR_LEN_MAX))
    {
        while ((INT4) u1DataCntr < i4DataLen)
        {
            pu1Cfg[u2CfgCntr] = au1Data[u1DataCntr++];
            if ((pu1Cfg[u2CfgCntr++] != '\n') && (u2CfgCntr < MSR_MAX_DATA_LEN))
            {
                continue;
            }
            u4Total++;
            if (u2CfgCntr <= 1)
            {
                u2CfgCntr = 0;
                MSR_MEMSET (pu1Cfg, 0, MSR_MAX_DATA_LEN);
                continue;
            }
            /*The "CKSUM:" acts as a delimiter marking the end of
             * information so no need to process it*/
            if (STRNCMP (pu1Cfg, "CKSUM:", 6) == 0)
            {
                break;
            }
            pTemp = pu1Cfg;
            if (*pTemp == '@')
            {
                MSR_MEMSET (au1PrefixOid, 0, MSR_MAX_OID_LEN);
                pTemp += 1;
                /* get the MSR index value */
                u4MSRArrayIndex = (UINT4) ATOI (pTemp);
                while (*pTemp != '\t')
                {
                    pTemp++;
                }
                pTemp++;

                /* get the prefix oid is present */
                for (u4Index = 0; u4Index < MSR_MAX_OID_LEN; u4Index++)
                {
                    if (*pTemp != '\n')
                    {
                        au1PrefixOid[u4Index] = *pTemp;
                        pTemp++;
                    }
                    else
                    {
                        au1PrefixOid[u4Index] = '\0';
                        break;
                    }
                }
                pTemp = pu1Cfg;
            }
            else
            {
                if (u4Total > MSR_RES_FILE_VER_FORMAT)
                {
                    /* get the MSR Rowstatus type value */
                    u4MSRRowStatusType = (UINT4) ATOI (pTemp);
                    pTemp += 2;
                }
                else
                {
                    /* For Version OID and File Format OID */

                    u4MSRRowStatusType = MSR_SCALAR;
                }

                /* Get the OID of the object to be restored and convert
                 * it from string to the OID type.*/
                for (u4Index = 0; u4Index < MSR_MAX_OID_LEN; u4Index++)
                {
                    if (*pTemp != '\t')
                    {
                        au1SuffixOid[u4Index] = *pTemp;
                        pTemp++;
                    }
                    else
                    {
                        au1SuffixOid[u4Index] = '\0';
                        break;
                    }
                }

                if (au1PrefixOid[0] != '\0')
                {
                    SnmpUpdateEoidString (au1PrefixOid);
                }
                else
                {
                    SnmpUpdateEoidString (au1SuffixOid);
                }
                /* concatenate the prefix oid and the suffix oids read */
                SNPRINTF ((char *) pu1Oid->au1MsrOidArrListBlock,
                          MSR_MAX_OID_LEN, "%s%s", au1PrefixOid, au1SuffixOid);
                WebnmConvertStringToOid (pOid, pu1Oid->au1MsrOidArrListBlock,
                                         MSR_FALSE);

                if (u4MSRArrayIndex != u4CurrArrayIndex)
                {
                    /* Get Current MSR array Index and Check that value with
                     * the one present in iss.conf file.If both are same means,
                     * the object is not moved in the gaSaveArray. Hence, 
                     * populate the RBTree with that ArrayIndex 
                     * (u4MSRArrayIndex) for that object. Else, change the 
                     * MSR array index with the current position of the object
                     * and proceed with RBTree population. */

                    /* Scalar mib objects are always present in the 
                     * au1SuffixOid and ends with 0 */
                    if ((au1SuffixOid[0] != '.')
                        && (pOid->pu4_OidList[pOid->u4_Length] == 0))
                    {
                        i4Result =
                            MSRGetArryIndexWithOID (pu1Oid->
                                                    au1MsrOidArrListBlock,
                                                    &u4CurrArrayIndex,
                                                    MSR_SCALAR,
                                                    (u4MSRArrayIndex - 1));
                        if (i4Result == MSR_FALSE)
                        {
                            i4Result =
                                MSRGetArryIndexWithOID (pu1Oid->
                                                        au1MsrOidArrListBlock,
                                                        &u4CurrArrayIndex,
                                                        MSR_SCALAR, 0);
                        }
                    }
                    else
                    {
                        /* For Tabular Object */
                        i4Result = MSRGetArrayIndexWithTableOID
                            (au1PrefixOid, &u4CurrArrayIndex,
                             MSR_TABLE, (u4MSRArrayIndex - 1));
                        if (i4Result == MSR_FALSE)
                        {
                            i4Result =
                                MSRGetArrayIndexWithTableOID (au1PrefixOid,
                                                              &u4CurrArrayIndex,
                                                              MSR_TABLE, 0);
                        }
                    }
                    if ((i4Result == MSR_TRUE) &&
                        (u4MSRArrayIndex != u4CurrArrayIndex))
                    {
                        u4MSRArrayIndex = u4CurrArrayIndex;
                    }
                }
                /* Get the multidata data type of the object to be restored */
                u2Type = (UINT2) ATOI (pTemp + 1);
                pTemp += 4;
                u4Len = 0;
                if (u2Type == OCTETSTRING)
                {
                    u4Len = (UINT4) ATOI (pTemp);
                    pTemp += 5;
                    while (pTemp[u4Len] != '\n')
                    {
                        u1LineFlag = MSR_FALSE;
                        while ((INT4) u1DataCntr < i4DataLen)
                        {
                            pu1Cfg[u2CfgCntr] = au1Data[u1DataCntr++];
                            if ((pu1Cfg[u2CfgCntr++] == '\n') ||
                                (u2CfgCntr >= MSR_MAX_DATA_LEN))
                            {
                                u1LineFlag = MSR_TRUE;
                                break;
                            }
                        }
                        if (u1LineFlag == MSR_FALSE)
                        {
                            u1DataCntr = 0;
                            MSR_MEMSET (au1Data, 0, MSR_MAX_STR_LEN);
                            i4DataLen = FileRead (i4ConfFd,
                                                  (CHR1 *) au1Data,
                                                  MSR_MAX_FILE_READ_LEN);
                        }
                        if (u2CfgCntr >= MSR_MAX_DATA_LEN)
                        {
                            break;
                        }
                    }
                }
                pData = MsrConvertStringToData (pTemp, u2Type, u4Len);
                if (pData == NULL)
                {
                    MSR_TRC (MSR_TRACE_TYPE, "Memory allocation failed!!!\n");
                    gi4MibResFailureReason = MIB_RES_MEMORY_FAILURE;
                    gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                    gu4MsrOidCount = 0;
                    gu4MsrMultiDataCount = 0;
                    gu4MsrOctetStrCount = 0;
                    FileClose (i4ConfFd);
                    MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);
                    MemReleaseMemBlock (gMsrOidArrListPoolId,
                                        (UINT1 *) (pu1Oid));
                    return ISS_FAILURE;
                }

                pTemp = pu1Cfg;

                /* Restoration should not be done if restore file version
                 * is incompatible with executable's restore file version
                 */
                if (u4Total == 1)
                {
                    /* Verify the first OID is issConfigRestoreFileVersion
                     * or Not. If So then check the version
                     */
                    if ((STRCMP (pu1Oid->au1MsrOidArrListBlock,
                                 ISS_RESTORE_FILE_VERSION_OID) != 0) ||
                        (MSR_MEMCMP (pData->pOctetStrValue->pu1_OctetList,
                                     ISS_RESTORE_FILE_VERSION,
                                     pData->pOctetStrValue->i4_Length) != 0))
                    {
                        MSR_TRC (MSR_TRACE_TYPE,
                                 "Incompatable restore file!!!\n");
                        gi4MibResFailureReason = MIB_RES_INCOMPATABLE_FILE;
                        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                        gu4MsrOidCount = 0;
                        gu4MsrMultiDataCount = 0;
                        gu4MsrOctetStrCount = 0;
                        FileClose (i4ConfFd);
                        MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);
                        return ISS_FAILURE;
                    }
                }
                /* If the File format is wrong means, throws Erroe message.
                 * */
                else if (u4Total == 2)
                {
                    if ((STRCMP (pu1Oid->au1MsrOidArrListBlock,
                                 ISS_RESTORE_FILE_FORMAT_OID) != 0) ||
                        (STRCMP (pData->pOctetStrValue->pu1_OctetList,
                                 ISS_RESTORE_FILE_FORMAT) != 0))
                    {
                        MSR_TRC (MSR_TRACE_TYPE,
                                 "Incompatable restore file!!!\n");
                        gi4MibResFailureReason = MIB_RES_INCOMPATABLE_FILE;
                        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                        gu4MsrOidCount = 0;
                        gu4MsrMultiDataCount = 0;
                        gu4MsrOctetStrCount = 0;
                        FileClose (i4ConfFd);
                        MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);
                        return ISS_FAILURE;
                    }
                }
                else
                {
                    /* get default value of the object and compare then 
                     * store (if not default value) the data in to RB tree 
                     * if INCR_SVAE_ENABLED is true */
                    if ((gMSRIncrSaveFlag == ISS_TRUE) &&
                        (gIssSysGroupInfo.b1IssDefValSaveFlag != ISS_TRUE))
                    {
                        i4SnmpRetval = SNMPGetDefaultValueForObj (*pOid,
                                                                  pdefData,
                                                                  &u4Error);
                    }
                    else
                    {
                        i4SnmpRetval = SNMP_FAILURE;
                        u4Error = SNMP_EXCEPTION_NO_DEF_VAL;
                    }

                    if ((i4SnmpRetval == SNMP_SUCCESS) ||
                        ((i4SnmpRetval == SNMP_FAILURE)
                         && (u4Error == SNMP_EXCEPTION_NO_DEF_VAL)))
                    {
                        if (i4SnmpRetval != SNMP_FAILURE)
                        {
                            i4Result = MSRCompareSnmpMultiDataValues (pData,
                                                                      pdefData);
                        }
                        else
                        {
                            i4Result = MSR_FAILURE;
                        }

                        if (i4Result == MSR_FAILURE)
                        {
                            /* store the Row status information into RB tree 
                             * and iss.conf file */
                            i4Result = MSRSearchNodeinRBTree (pOid,
                                                              u4MSRArrayIndex,
                                                              u4MSRRowStatusType,
                                                              &pRBNodeCur);
                            KW_FALSEPOSITIVE_FIX (pRBNodeCur);

                            if (i4Result == MSR_TRUE)
                            {
                                /* data exists in the RB tree so Modify the 
                                 * data in the RBNode with new value. */
                                i4Result = MSRModifyRBNodeinRBTree (pData,
                                                                    pRBNodeCur);
                                if (i4Result != MSR_TRUE)
                                {
                                    MSR_TRC (MSR_TRACE_TYPE,
                                             "RB Tree Updation failure!!!\n");
                                    gi4MibResFailureReason =
                                        MIB_RES_RBTREE_UPDATION_FAILURE;
                                    gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                                    gu4MsrOidCount = 0;
                                    gu4MsrMultiDataCount = 0;
                                    gu4MsrOctetStrCount = 0;
                                    FileClose (i4ConfFd);
                                    MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);
                                    return MSR_FALSE;
                                }
                            }
                            else
                            {
                                /* data not exist in the RBTree, Add new node 
                                 * into  RBTree and update Linklist */
                                if (MSRInsertRBNodeinRBTree (pOid, pData,
                                                             u4MSRArrayIndex,
                                                             u4MSRRowStatusType,
                                                             MSR_TRUE) ==
                                    MSR_FALSE)
                                {
                                    MSR_TRC (MSR_TRACE_TYPE,
                                             "RB Tree Insertion failure!!!\n");
                                    FileClose (i4ConfFd);
                                    gi4MibResFailureReason =
                                        MIB_RES_RBTREE_INSERTION_FAILURE;
                                    gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                                    gu4MsrOidCount = 0;
                                    gu4MsrMultiDataCount = 0;
                                    gu4MsrOctetStrCount = 0;
                                    MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);
                                    return ISS_FAILURE;
                                }
                            }
                        }
                    }
                }
                MSR_MEMSET (pu1Oid->au1MsrOidArrListBlock, 0, MSR_MAX_OID_LEN);
                MSR_MEMSET (au1SuffixOid, 0, MSR_MAX_OID_LEN);
            }
            u2CfgCntr = 0;
            MSR_MEMSET (pu1Cfg, 0, MSR_MAX_DATA_LEN);
        }
        u1DataCntr = 0;
        MSR_MEMSET (au1Data, 0, MSR_MAX_STR_LEN);
    }

    MemReleaseMemBlock (gMsrOidArrListPoolId, (UINT1 *) pu1Oid);
    MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);
    FileClose (i4ConfFd);
    gu4MsrOidCount = 0;
    gu4MsrMultiDataCount = 0;
    gu4MsrOctetStrCount = 0;

    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrQueueId                                   */
/*                                                                          */
/*    Description        : Return the MSR queue ID                          */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                            */
/*                                                                          */
/****************************************************************************/

tOsixQId           *
MsrQueueId (VOID)
{
    return &MsrsnmpQId;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MSRMibRestorefromRBtree                          */
/*                                                                          */
/*   Description        : This function is used to set data to various modules*/
/*                         when restore event is recieved                   */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ISS_SUCCESS if population of RBT is successful   */
/****************************************************************************/
VOID
MSRMibRestorefromRBtree ()
{
    tMSRRbtree         *pRBCurNode = NULL;
    tMSRRbtree         *pRBNextNode = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSnmpIndex         *pCurIndex = MsrIndexPool1;
    tSNMP_MULTI_DATA_TYPE *pData = NULL;
    UINT4               u4Error = 0;
    /* The u4ErrorCount will store the number of objects from the gpu1StrFlash 
     * for which the SET operation failed. 
     */
    UINT4               u4ErrorCount = 0, u4Total = 0;
    UINT4               u4Len = 0;
    UINT2               u2Type = 0;
    UINT1              *pTemp = NULL;

    /* Obtain the first conf file version node.
     * This node is used only for verification. Hence, no need to set it */
    pRBCurNode = (tMSRRbtree *) RBTreeGetFirst (gMSRRBTable);
    u4Total++;

    if (pRBCurNode == NULL)
    {
        return;
    }

    /* For File Format Version OID. */

    pRBNextNode = (tMSRRbtree *) RBTreeGetNext (gMSRRBTable,
                                                pRBCurNode,
                                                (tRBCompareFn)
                                                MSRCompareRBNodes);

    if (pRBNextNode == NULL)
    {
        return;
    }

    u4Total++;
    while (1)
    {
        pRBCurNode = pRBNextNode;
        pRBNextNode = NULL;
        pRBNextNode = (tMSRRbtree *) RBTreeGetNext (gMSRRBTable,
                                                    pRBCurNode,
                                                    (tRBCompareFn)
                                                    MSRCompareRBNodes);
        u4Total++;
        if (pRBNextNode != NULL)
        {
            pOid = pRBNextNode->pOidValue;
            pTemp = (UINT1 *) pRBNextNode->pData;
            /* Get the multidata data type of the object to be restored */
            u2Type = (UINT2) ATOI (pTemp);
            pTemp += 3;
            if (u2Type == OCTETSTRING)
            {
                u4Len = (UINT4) ATOI (pTemp);
                pTemp += 5;
            }
            else
            {
                u4Len = 0;
            }
            pData = MsrConvertStringToData (pTemp, u2Type, u4Len);
            if (pData == NULL)
            {
                MSR_TRC (MSR_TRACE_TYPE, "Memory allocation failed!!!\n");
                gi4MibResFailureReason = MIB_RES_MEMORY_FAILURE;
                gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                return;
            }
            u4Error = 0;
            if (SNMPSet (*pOid, pData, &u4Error, pCurIndex,
                         SNMP_DEFAULT_CONTEXT) == SNMP_FAILURE)
            {
                u4ErrorCount++;
            }
        }
        else
        {
            break;
        }
    }
    /* Restore operation completed successfully */
    gi4MibResStatus = LAST_MIB_RESTORE_SUCCESSFUL;
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MSRCheckforOldFileVersion                        */
/*                                                                          */
/*   Description        : This function is used to file version             */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          :                                                  */
/*                                                                          */
/*    Returns            :                                                  */
/****************************************************************************/
INT4
MSRCheckforOldFileVersion (UINT4 *pVersion, INT4 i4ConfFd)
{
    UINT1               u1DataCntr = 0;
    UINT1              *pTemp = NULL;
    UINT1               au1Cfg[MSR_MAX_STR_LEN];
    UINT1               au1Oid[MSR_MAX_OID_LEN];
    UINT1               au1Data[MSR_MAX_STR_LEN];
    INT4                i4DataLen;

    MSR_MEMSET (au1Cfg, 0, MSR_MAX_STR_LEN);
    MSR_MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);
    MSR_MEMSET (au1Data, 0, 20);

    i4DataLen = FileRead (i4ConfFd, (CHR1 *) au1Cfg, 50);

    pTemp = au1Oid;
    while (((INT4) u1DataCntr < i4DataLen) && (au1Cfg[u1DataCntr] != '\n'))
    {
        if (au1Cfg[u1DataCntr] == '\t')
        {
            pTemp = au1Data;
            u1DataCntr++;
            continue;
        }
        *pTemp = au1Cfg[u1DataCntr];
        pTemp++;
        u1DataCntr++;
    }

    if (STRCMP (au1Oid, au1RestoreFileVersionOid) == 0)
    {
        if (MSR_MEMCMP (au1Data, ISS_RESTORE_FILE_VERSION,
                        MSR_STRLEN (ISS_RESTORE_FILE_VERSION)) != 0)
        {
            *pVersion = MSR_TRUE;
        }
        else
        {
            *pVersion = MSR_FALSE;
        }
        FileClose (i4ConfFd);
        return MSR_SUCCESS;
    }
    FileClose (i4ConfFd);
    MSR_TRC (MSR_TRACE_TYPE, "Restore File Version is not supported\n");
    return MSR_FAILURE;
}

/************************************************************************
 *  Function Name   : MSRNotifyConfChg
 *  Description     : Sends update event trigger to MSR if Set operation 
 *                            for a varbind is successful inside nmhSet
 *  Input           : pMultiIndex - This variable contains the number of 
 *                    index in the received OID and index value in case of 
 *                    tabular OID and in case of scalar num index is 0
 *                    isRowStatusPresent- in case of tabular 
 *                     True when row status oid recived in update.
 *                     False when other object is received.
 *                    pMultiData - This variable contains the OID ,
 *                    OID tye and its value. In case of tabular OID 
 *                    shall be Rowstatus OID can be  ACTIVE or DESTROY.
 *  Output          : pOid - Full Oid (Oid + Instance)
 *  Returns         : None
 ************************************************************************/
INT1
MSRNotifyConfChg (tSnmpIndex * pMultiIndex,
                  BOOL1 isRowStatusPresent, tRetVal * pMultiData, UINT1 *pOid)
{
    if ((gMSRIncrSaveFlag == ISS_FALSE)
        || (gi4MibSaveStatus == MIB_SAVE_IN_PROGRESS))
    {
        /* When the incremental save is off, the allocation and release of
         * memory cannot be avoided because the multiIndex and multiData
         * are also used to call the wrapper set routines from the 
         * common nmhSet routine */

        /* Freeing MultiIndex, MultiData, pOid */
        free_MultiData (pMultiData);
        free_MultiIndex (pMultiIndex, pMultiIndex->u4No);

        if (pOid != NULL)
        {
            free_MultiOid (pOid);
        }

        return SUCCESS;
    }

    if (MSRNotifyConfigChange (pMultiIndex, isRowStatusPresent,
                               pMultiData, pOid) == FAILURE)
    {
        return FAILURE;
    }
    return SUCCESS;
}

/************************************************************************
 *  Function Name   : MSRNotifyConfigChange
 *  Description     : Sends update event trigger to MSR if Set operation 
 *                            for a varbind is successful inside nmhSet
 *  Input           : pMultiIndex - This variable contains the number of 
 *                    index in the received OID and index value in case of 
 *                    tabular OID and in case of scalar num index is 0
 *                    isRowStatusPresent- in case of tabular 
 *                     True when row status oid recived in update.
 *                     False when other object is received.
 *                    pMultiData - This variable contains the OID ,
 *                    OID tye and its value. In case of tabular OID 
 *                    shall be Rowstatus OID can be  ACTIVE or DESTROY.
 *  Output          : pOid - Full Oid (Oid + Instance)
 *  Returns         : None
 ************************************************************************/
INT1
MSRNotifyConfigChange (tSnmpIndex * pMultiIndex,
                       BOOL1 isRowStatusPresent,
                       tRetVal * pMultiData, UINT1 *pOid)
{
    UINT1               taskName[] = "MSR\0";
    tMsrConfigData     *pMsrConfigData = NULL;
    tMSRUpdatedata     *pUpdateData = NULL;
    tOsixQId           *QId;

    if (MsrisRetoreinProgress == ISS_FALSE)
    {
        /* in case of mib restore in progress no need to send update 
         * request to MSR Q*/
        pUpdateData = MemAllocMemBlk (gMsrQUpdDataPoolId);
    }
    if (pUpdateData == NULL)
    {
        MSRUpdateMemFree (pMultiIndex, pMultiData, pOid);

        if (MsrisRetoreinProgress == ISS_TRUE)
        {
            return SUCCESS;

        }
        return FAILURE;
    }

    MEMSET (pUpdateData, 0, sizeof (tMSRUpdatedata));
    pUpdateData->pSnmpindx = pMultiIndex;
    pUpdateData->isRowStatusPresent = isRowStatusPresent;
    pUpdateData->pSnmpData = pMultiData;
    pUpdateData->pOid = pOid;
    pUpdateData->u1IsDataToBeSaved = MSR_TRUE;

    /* Fill the MsrConfigData structure */
    pMsrConfigData = MemAllocMemBlk (gMsrQConDataPoolId);
    if (pMsrConfigData == NULL)
    {
        MSRUpdateMemFree (pMultiIndex, pMultiData, pOid);
        MemReleaseMemBlock (gMsrQUpdDataPoolId, (UINT1 *) pUpdateData);
        return FAILURE;
    }
    MSR_MEMSET (pMsrConfigData, 0, sizeof (tMsrConfigData));
    pMsrConfigData->u2MsgType = MSR_CONFIG_SAVE;
    pMsrConfigData->MsrCfgData.pMsrUpdateData = pUpdateData;

    MsrUpdateQCountLock ();
    gu4noMsrUpdateQReq++;
    if (gu4noMsrUpdateQReq >= MSR_MAX_UPDATE_QUEUE_SIZE)
    {
        MsrUpdateQCountUnLock ();
        /* get the Q status lock on the reaching the maximum 
         * messages of the Q, this lock is released by MSR on 
         * processing the updated event */
        MsrUpdateQStatusLock ();

    }
    else
    {
        MsrUpdateQCountUnLock ();
    }

    QId = MsrQueueId ();
    if (OsixQueSend (*QId, (UINT1 *) &pMsrConfigData, OSIX_DEF_MSG_LEN) ==
        OSIX_FAILURE)
    {
        MSRFreeSnmpDataRcvfromQ (pUpdateData);
        MemReleaseMemBlock (gMsrQConDataPoolId, (UINT1 *) (pMsrConfigData));
        return FAILURE;
    }
    /* msr debug stats counter */
    gU4NoOfQMsgSend++;
    if (OSIX_FAILURE == OsixSendEvent (SELF, taskName, MSR_UPDATE_EVENT))
    {
        gU4NoOfQMsgSendFail++;
    }
    return SUCCESS;
}

/*****************************************************************************/
/* Function     : MsrUpdateQCountLock                                        */
/*                                                                           */
/*  Description : Takes the MSR update Q Semaphore                           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MSR_SUCCESS/MSR_FAILURE                                    */
/*****************************************************************************/
INT4
MsrUpdateQCountLock ()
{
    if (OsixSemTake (MsrUpdateQsemId) != OSIX_SUCCESS)
    {
        return MSR_FAILURE;
    }
    return MSR_SUCCESS;
}

/*****************************************************************************/
/* Function     : MsrUpdateQCountUnLock                                           */
/*                                                                           */
/*  Description : Releases the MSR update Q Semaphore                        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MSR_SUCCESS/MSR_FAILURE                                    */
/*****************************************************************************/
INT4
MsrUpdateQCountUnLock ()
{
    OsixSemGive (MsrUpdateQsemId);
    return MSR_SUCCESS;
}

/*****************************************************************************/
/* Function     : MsrUpdateQStatusLock                                            */
/*                                                                           */
/*  Description : Takes the MSR update Q Semaphore                           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MSR_SUCCESS/MSR_FAILURE                                    */
/*****************************************************************************/
INT4
MsrUpdateQStatusLock ()
{
    if (OsixSemTake (MsrUpdateQSsemId) != OSIX_SUCCESS)
    {
        return MSR_FAILURE;
    }
    return MSR_SUCCESS;
}

/*****************************************************************************/
/* Function     : MsrUpdateQStatusUnLock                                     */
/*                                                                           */
/*  Description : Releases the MSR update Q Semaphore                        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MSR_SUCCESS/MSR_FAILURE                                    */
/*****************************************************************************/
INT4
MsrUpdateQStatusUnLock ()
{
    OsixSemGive (MsrUpdateQSsemId);
    return MSR_SUCCESS;
}

/*****************************************************************************/
/* Function     : MsrReleaseUpdateQLock                                      */
/*                                                                           */
/*  Description : Releases the MSR update Q Semaphore                        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MSR_SUCCESS/MSR_FAILURE                                    */
/*****************************************************************************/
INT4
MsrReleaseUpdateQLock (VOID)
{
    MsrUpdateQCountLock ();
    if (gu4noMsrUpdateQReq >= MSR_MAX_UPDATE_QUEUE_SIZE)
    {
        /* unlock the snmp which is waitng for the lock */
        MsrUpdateQStatusUnLock ();
    }
    gu4noMsrUpdateQReq--;
    MsrUpdateQCountUnLock ();
    /* msr debug stats counter */
    gU4NoOfQMsgcomplete++;
    return MSR_SUCCESS;
}

/*****************************************************************************/
/* Function     : MSRSearchandUpdateRBTree                                   */
/*                                                                           */
/*  Description : This function searches the given oid in the Rbtree, If     */
/*                 the node is found, the data is modified. Else, a new      */
/*                 node is added to the RBtree.                              */
/*                 Also, when a configuration is reset, the corresponding    */
/*                 node is removed from the tree.                            */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MSR_SUCCESS/MSR_FAILURE                                    */
/*****************************************************************************/
INT4
MSRSearchandUpdateRBTree (tMSRUpdatedata * pQData,
                          tSNMP_OID_TYPE * pOid,
                          UINT4 u4Index, UINT4 u4RowstatusType, INT4 i4IsDelete)
{
    tMSRRbtree         *pRBNodeCur = NULL;
    INT4                i4Result;
    UINT1               u1Action = 0;

    if (i4IsDelete == MSR_TRUE)
    {
        i4Result = MSRSearchNodeinRBTree (pOid, u4Index,
                                          u4RowstatusType, &pRBNodeCur);
        if (i4Result == MSR_TRUE)
        {
            MSRDeleteNodefromRBTree (pOid, u4Index, u4RowstatusType);
            MsrSaveIncrData (pQData, pOid, u4Index, u4RowstatusType,
                             MSR_DELETE);
        }
    }
    else
    {
        i4Result = MSRSearchNodeinRBTree (pOid, u4Index,
                                          u4RowstatusType, &pRBNodeCur);
        KW_FALSEPOSITIVE_FIX (pRBNodeCur);
        if (i4Result == MSR_TRUE)
        {
            /* data exists in the RB tree so Modify the 
             * data in the RBNode with new value. */
            i4Result = MSRModifyRBNodeinRBTree (pQData->pSnmpData, pRBNodeCur);
            if (i4Result != MSR_TRUE)
            {
                return MSR_FAILURE;
            }
            /* Reset the u1IsDataToBeSaved flag for the RbNode */
            /* For eg. the volatile entries can be converted to
             * non-volatile entries or vice-versa 
             * Based on the new value of u1IsDataToBeSaved, 
             * add / delete the configuration from incremental conf file */
            pRBNodeCur->u1IsDataToBeSaved = pQData->u1IsDataToBeSaved;
            if (pRBNodeCur->u1IsDataToBeSaved == MSR_FALSE)
            {
                u1Action = MSR_DELETE;
            }
            else
            {
                u1Action = MSR_MODIFY;
            }
            MsrSaveIncrData (pQData, pOid, u4Index, u4RowstatusType, u1Action);
        }
        else
        {
            /* data not exist in the RBTree, Add new node into 
             * RBTree and update Linklist */
            i4Result = MSRInsertRBNodeinRBTree (pOid, pQData->pSnmpData,
                                                u4Index, u4RowstatusType,
                                                pQData->u1IsDataToBeSaved);
            if (i4Result != MSR_TRUE)
            {
                return MSR_FAILURE;
            }
            MsrSaveIncrData (pQData, pOid, u4Index, u4RowstatusType, MSR_ADD);
        }
    }
    return MSR_SUCCESS;
}

/*****************************************************************************/
/* Function     : MsrAddDatatoIncrfile                                       */
/*                                                                           */
/*  Description : This function saves the current configuration to the       */
/*                 local incremental conf file or the incremental buffer     */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MSR_SUCCESS/MSR_FAILURE                                    */
/*****************************************************************************/
INT4
MsrSaveIncrData (tMSRUpdatedata * pQData,
                 tSNMP_OID_TYPE * pOid,
                 UINT4 u4ArrayIndex, UINT4 u4RowstatusType, UINT4 u4UpdateType)
{
    UINT1              *pTemp = NULL;
    UINT1              *pu1Data = NULL;
    UINT1               au1Oid[MSR_MAX_OID_LEN];
    UINT4               u4Size = 0;
    INT4                i4RetVal;

    if (gIssConfigSaveInfo.IssAutoConfigSave == ISS_FALSE)
    {
        if (u4ArrayIndex != gu4MsrAutoSaveOIDIndex)
        {
            return MSR_SUCCESS;
        }
        else
        {
            u4Size = MSR_INCREMENTAL_SAVE_BUFF_SIZE;
        }
    }
    else if (u4ArrayIndex == gu4MsrAutoSaveOIDIndex)
    {
        u4Size = MSR_INCREMENTAL_SAVE_BUFF_SIZE;
    }
    else
    {
        if (MSR_FALSE == MsrisSaveAllowed (u4ArrayIndex))
        {
            return MSR_SUCCESS;
        }

        if ((u4UpdateType != MSR_DELETE) &&
            (pQData->u1IsDataToBeSaved == MSR_FALSE))
        {
            return MSR_SUCCESS;
        }

        if (MSR_DATA_ALLOC_MEM_BLOCK (pu1Data) == NULL)
        {
            return MSR_FAILURE;
        }

        MSR_MEMSET (pu1Data, 0, MSR_MAX_DATA_LEN);
        pTemp = pu1Data;
        MSR_MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);

        WebnmConvertOidToString (pOid, au1Oid);
        SnmpRevertEoidString (au1Oid);
        sprintf ((char *) pTemp, "%u\t%4u\t%u\t%s\t%2d\t",
                 u4UpdateType, u4ArrayIndex, u4RowstatusType,
                 au1Oid, pQData->pSnmpData->i2_DataType);
        pTemp += MSR_STRLEN (pTemp);

        if (pQData->pSnmpData->i2_DataType == OCTETSTRING)
        {
            sprintf ((char *) pTemp, "%4d\t",
                     (int) pQData->pSnmpData->pOctetStrValue->i4_Length);
            pTemp += MSR_STRLEN (pTemp);
        }
        u4Size = MSR_STRLEN (pu1Data);

        MsrConvertDataToString (pQData->pSnmpData,
                                pTemp, (UINT2) pQData->pSnmpData->i2_DataType);

        if (pQData->pSnmpData->i2_DataType == OCTETSTRING)
        {
            pTemp += pQData->pSnmpData->pOctetStrValue->i4_Length;
            u4Size += (UINT4) pQData->pSnmpData->pOctetStrValue->i4_Length;

        }
        else
        {
            pTemp += MSR_STRLEN (pTemp);
            u4Size = MSR_STRLEN (pu1Data);
        }
        sprintf ((char *) pTemp, "\n");
        u4Size += 1;
        pTemp = pu1Data;
    }

    if ((gIssConfigSaveInfo.IssConfigSaveOption == ISS_CONFIG_FLASH_SAVE) ||
        (gIssConfigSaveInfo.IssConfigSaveOption == ISS_CONFIG_STARTUP_SAVE))
    {
        if (gi4MibSaveStatus == MIB_SAVE_IN_PROGRESS)
        {
            i4RetVal = MSR_SUCCESS;
        }
        i4RetVal = MsrSaveDatatoIncrFile (pTemp, u4Size);

    }
    else if (gIssConfigSaveInfo.IssConfigSaveOption == ISS_CONFIG_REMOTE_SAVE)
    {
        if (gi4RemoteSaveStatus == MIB_SAVE_IN_PROGRESS)
        {
            i4RetVal = MSR_SUCCESS;
        }
        i4RetVal = MsrSaveDatatoIncrBuffer (pTemp, u4Size);
    }
    else
    {
        i4RetVal = MSR_SUCCESS;
    }
    MSR_DATA_FREE_MEM_BLOCK (pu1Data);
    return i4RetVal;
}

/*****************************************************************************/
/* Function     : MsrSaveDatatoIncrFile                                      */
/*                                                                           */
/* Description  : This function writes the given data to the incremental     */
/*                 conf file. When the incremental file size is exceeded     */
/*                 all the configurations are saved to the iss conf file.    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MSR_SUCCESS/MSR_FAILURE                                    */
/*****************************************************************************/
INT4
MsrSaveDatatoIncrFile (UINT1 *pData, UINT4 u4DataSize)
{
    INT4                i4FileId = 0;
    INT4                i4FileMode = 0;
    CHR1                u1FileName[MSR_MAX_FILE_NME_LEN];
    CHR1               *pu1FileName = NULL;
    INT4                i4RetVal = 0;
    tRmProtoEvt         ProtoEvt;
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4AppId = RM_MSR_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    MEMSET (gMsrData.au1FileName, 0, sizeof (gMsrData.au1FileName));
    if ((gu4IncrLength + u4DataSize) >= MSR_INCREMENTAL_SAVE_BUFF_SIZE)
    {
#ifdef RM_WANTED
        if (IssGetColdStandbyFromNvRam () == ISS_COLDSTDBY_ENABLE)
        {
            ProtoEvt.u4Event = RM_PROTOCOL_SEND_EVENT;
            ProtoEvt.u4SendEvent = RM_SEND_TO_FILES_PEERS;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
#endif

        gTempi4MibSaveStatus = MIB_SAVE_IN_PROGRESS;
        STRNCPY ((char *) gMsrData.au1FileName,
                 (char *) gIssConfigSaveInfo.au1IssConfigSaveFileName,
                 (sizeof (gMsrData.au1FileName) - 1));

        MSR_TRC (MSR_TRACE_TYPE, "The incremental buffer is filled. Syncing"
                 " the Rbtree with the configuration file\n");

        if (MSRUpdateISSConfFile (FLASH_ALL_CONFIG,
                                  (const char *) gMsrData.au1FileName) !=
            MSR_SUCCESS)
        {
            gTempi4MibSaveStatus = LAST_MIB_SAVE_FAILED;
            MsrInitSaveFlag ();
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                          "Saving configuration to file failed!"));
            return MSR_FAILURE;
        }
        MsrStoreSaveFlagNvRam ();

        /* The gi4MibSaveStatus should be updated only after we 
         * write all the parameters in the flash
         */
        /* Save the restore flag to true here */
        if (IssGetRestoreOptionFromNvRam () != ISS_CONFIG_NO_RESTORE)
        {
            IssSetRestoreFlagToNvRam (SET_FLAG);
            IssSetRestoreFileNameToNvRam ((INT1 *) gMsrData.au1FileName);
        }
        gTempi4MibSaveStatus = LAST_MIB_SAVE_SUCCESSFUL;

        /* set the gu4IncrLength =0 */
        gu4IncrLength = 0;

        if (pData == NULL)
        {
            /* clear the incremental file */
            pu1FileName = u1FileName;
            MSR_MEMSET (u1FileName, 0, MSR_MAX_FILE_NME_LEN);
            sprintf (pu1FileName, "%s%s", FLASH,
                     gIssConfigSaveInfo.au1IssIncrSaveFileName);

            if ((i4FileId = FileOpen ((UINT1 *) pu1FileName,
                                      (OSIX_FILE_CR | OSIX_FILE_WO |
                                       OSIX_FILE_TR))) < 0)
            {
                return MSR_SUCCESS;
            }
            FileClose (i4FileId);
            return MSR_SUCCESS;
        }
    }
    i4FileMode = OSIX_FILE_WO | OSIX_FILE_CR;
    if (gu4IncrLength != 0)
    {
        i4FileMode |= OSIX_FILE_AP;
    }
    else
    {
        i4FileMode |= OSIX_FILE_TR;
    }

    pu1FileName = u1FileName;
    MSR_MEMSET (u1FileName, 0, MSR_MAX_FILE_NME_LEN);
    sprintf (pu1FileName, "%s%s", FLASH,
             gIssConfigSaveInfo.au1IssIncrSaveFileName);
    if (pData == NULL)
    {
        return MSR_FAILURE;
    }
    if ((i4FileId = FileOpen ((UINT1 *) pu1FileName, i4FileMode)) < 0)
    {
        SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                      "Saving Incremental Data to Flash failed !"));
        return MSR_FAILURE;
    }
    i4RetVal = (INT4) FileWrite (i4FileId, (CHR1 *) pData, u4DataSize);
    if (i4RetVal < 0)
    {
        FileClose (i4FileId);
        return MSR_FAILURE;
    }
    gu4IncrLength += u4DataSize;

    FileClose (i4FileId);
    return MSR_SUCCESS;
}

/*****************************************************************************/
/* Function     : MsrSaveDatatoIncrBuffer                                    */
/*                                                                           */
/* Description  : This function writes the given data to the incremental     */
/*                 buffer. When the incremental buffer is full all the       */
/*                 configurations are saved to the remote iss conf file.     */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MSR_SUCCESS/MSR_FAILURE                                    */
/*****************************************************************************/
INT4
MsrSaveDatatoIncrBuffer (UINT1 *pData, UINT4 u4DataSize)
{
    if ((gu4IncrLength + u4DataSize) >= MSR_INCREMENTAL_SAVE_BUFF_SIZE)
    {
        /* Update remote location with complete data
         * from RB tree */
        /* Clear the gpu1StrFlash before saving the configuration to it */
        MSR_MEMSET (gpu1StrFlash, 0, gu4MibEntryBlkSize);
        gpu1StrCurrent = gpu1StrFlash;
        gTempi4RemoteSaveStatus = MIB_SAVE_IN_PROGRESS;

        if (MsrPopulateFlashArrayFromRBTree () == MSR_FAILURE)
        {
            gTempi4RemoteSaveStatus = LAST_MIB_SAVE_FAILED;
            MsrInitSaveFlag ();
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                          "Saving configuration to remote failed!"));
            return (MSR_FAILURE);
        }
        else
        {
            MsrStoreSaveFlagNvRam ();
            /* Save the restore flag to true here */
            if (IssGetRestoreOptionFromNvRam () != ISS_CONFIG_NO_RESTORE)
            {
                IssSetRestoreFlagToNvRam (SET_FLAG);
            }
            gTempi4RemoteSaveStatus = LAST_MIB_SAVE_SUCCESSFUL;
        }

        /* set the gu4IncrLength = 0 */
        /* return success */
        gu4IncrLength = 0;
        if (pData == NULL)
        {
            return MSR_SUCCESS;
        }
    }
    gu4IncrLength += u4DataSize;
    return MSR_SUCCESS;
}

/*****************************************************************************/
/* Function     : MSRUpdateRBtreeWithIncrFile                                */
/*                                                                           */
/* Description  : This function updates the RBTree with the configrations    */
/*                 present in the incremental configuration file.            */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MSR_SUCCESS/MSR_FAILURE                                    */
/*****************************************************************************/
INT4
MSRUpdateRBtreeWithIncrFile ()
{
    INT4                i4ConfFd;
    UINT1              *pTemp = NULL;
    UINT1              *pu1Cfg = NULL;
    UINT1               au1Data[MSR_MAX_STR_LEN];
    UINT1               au1Oid[MSR_MAX_OID_LEN];
    UINT4               u4Index = 0;
    UINT2               u2Type = 0;
    UINT4               u4Len = 0;
    UINT4               u4MSRArrayIndex;
    UINT4               u4MSRRowStatusType;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_MULTI_DATA_TYPE *pData = NULL;
    UINT4               u4UpdateType;
    tMSRRbtree         *pRBNodeCur = NULL;
    INT4                i4DataLen;
    INT4                i4Result = 0;
    UINT2               u2CfgCntr = 0;
    UINT1               u1DataCntr = 0;
    UINT1               u1LineFlag = MSR_FALSE;
    CHR1                u1FileName[MSR_MAX_FILE_NME_LEN], *pu1FileName = NULL;

    pu1FileName = u1FileName;
    MSR_MEMSET (u1FileName, 0, MSR_MAX_FILE_NME_LEN);
    sprintf (pu1FileName, "%s%s", FLASH,
             gIssConfigSaveInfo.au1IssIncrSaveFileName);
    if ((i4ConfFd = FileOpen ((CONST UINT1 *) pu1FileName, OSIX_FILE_RO)) < 0)
    {
        gu4IncrLength = 0;
        MSR_TRC (MSR_TRACE_TYPE, "Incremental file Not Present ... \n");
        gi4MibResStatus = LAST_MIB_RESTORE_SUCCESSFUL;
        return MSR_SUCCESS;
    }
    gi4MibResStatus = MIB_RESTORE_IN_PROGRESS;
    if ((pOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        FileClose (i4ConfFd);
        gi4MibResFailureReason = MIB_RES_MEMORY_FAILURE;
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        return MSR_FAILURE;
    }
    if (MSR_DATA_ALLOC_MEM_BLOCK (pu1Cfg) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        FileClose (i4ConfFd);
        gi4MibResFailureReason = MIB_RES_MEMORY_FAILURE;
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        return MSR_FAILURE;
    }

    while (((i4DataLen = FileRead (i4ConfFd, (CHR1 *) au1Data,
                                   MSR_MAX_FILE_READ_LEN)) > 0)
           && (i4DataLen != (INT4) MSR_LEN_MAX))
    {
        while ((INT4) u1DataCntr < i4DataLen)
        {
            pu1Cfg[u2CfgCntr] = au1Data[u1DataCntr++];
            if ((pu1Cfg[u2CfgCntr++] != '\n')
                && (u2CfgCntr < (MSR_MAX_DATA_LEN - 1)))
            {
                continue;
            }
            pTemp = pu1Cfg;
            u4UpdateType = (UINT4) ATOI (pTemp);
            pTemp += 2;
            u4MSRArrayIndex = (UINT4) ATOI (pTemp);
            pTemp += 5;
            u4MSRRowStatusType = (UINT4) ATOI (pTemp);
            pTemp += 2;
            /* Get the OID of the object to be restored and convert
             * it from string to the OID type.*/
            for (u4Index = 0; u4Index < MSR_MAX_OID_LEN; u4Index++)
            {
                if (*pTemp != '\t')
                {
                    au1Oid[u4Index] = *pTemp;
                    pTemp++;
                }
                else
                {
                    au1Oid[u4Index] = '\0';
                    break;
                }
            }
            SnmpUpdateEoidString (au1Oid);
            WebnmConvertStringToOid (pOid, au1Oid, MSR_FALSE);
            /* Get the multidata data type of the object to be restored */
            u2Type = (UINT2) ATOI (pTemp + 1);
            pTemp += 4;
            if (u2Type == OCTETSTRING)
            {
                u4Len = (UINT4) ATOI (pTemp);
                pTemp += 5;
                while (pTemp[u4Len] != '\n')
                {
                    u1LineFlag = MSR_FALSE;
                    while ((INT4) u1DataCntr < i4DataLen)
                    {
                        pu1Cfg[u2CfgCntr] = au1Data[u1DataCntr++];
                        if ((pu1Cfg[u2CfgCntr++] == '\n') ||
                            (u2CfgCntr >= MSR_MAX_DATA_LEN))
                        {
                            u1LineFlag = MSR_TRUE;
                            break;
                        }
                    }
                    if (u1LineFlag == MSR_FALSE)
                    {
                        u1DataCntr = 0;
                        MSR_MEMSET (au1Data, 0, MSR_MAX_STR_LEN);
                        i4DataLen = FileRead (i4ConfFd,
                                              (CHR1 *) au1Data,
                                              MSR_MAX_FILE_READ_LEN);
                    }
                    if (u2CfgCntr >= MSR_MAX_DATA_LEN)
                    {
                        break;
                    }
                }
            }
            else
            {
                u4Len = 0;
            }
            pData = MsrConvertStringToData (pTemp, u2Type, u4Len);
            if (pData == NULL)
            {
                MSR_TRC (MSR_TRACE_TYPE, "Memory allocation failed!!!\n");
                gi4MibResFailureReason = MIB_RES_MEMORY_FAILURE;
                gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                FileClose (i4ConfFd);
                gu4MsrOidCount = 0;
                MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);
                return MSR_FAILURE;
            }

            i4Result = MSRSearchNodeinRBTree (pOid, u4MSRArrayIndex,
                                              u4MSRRowStatusType, &pRBNodeCur);
            KW_FALSEPOSITIVE_FIX (pRBNodeCur);
            if (i4Result == MSR_TRUE)
            {
                if (u4UpdateType == MSR_ADD)
                {
                    MSRDeleteNodefromRBTree (pOid, u4MSRArrayIndex,
                                             u4MSRRowStatusType);

                    MSRInsertRBNodeinRBTree (pOid, pData, u4MSRArrayIndex,
                                             u4MSRRowStatusType, MSR_TRUE);
                }
                else if (u4UpdateType == MSR_MODIFY)
                {
                    if (MSRModifyRBNodeinRBTree (pData, pRBNodeCur) ==
                        MSR_FALSE)
                    {
                        MSR_TRC (MSR_TRACE_TYPE,
                                 "MSR Modifying tree node failed!!!\n");
                        gi4MibResFailureReason = MIB_RES_MEMORY_FAILURE;
                        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                        FileClose (i4ConfFd);
                        gu4MsrOidCount = 0;
                        MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);
                        return MSR_FAILURE;
                    }
                }
                else
                {
                    MSRDeleteNodefromRBTree (pOid, u4MSRArrayIndex,
                                             u4MSRRowStatusType);
                }
            }
            else
            {
                /* The configuration is not present in the Rbtree.
                 * If the update type is add / modify insert the node */
                if (u4UpdateType != MSR_DELETE)
                {
                    MSRInsertRBNodeinRBTree (pOid, pData, u4MSRArrayIndex,
                                             u4MSRRowStatusType, MSR_TRUE);
                }
            }
            u2CfgCntr = 0;
            MSR_MEMSET (pu1Cfg, 0, MSR_MAX_DATA_LEN);
        }
        u1DataCntr = 0;
        MSR_MEMSET (au1Data, 0, MSR_MAX_STR_LEN);
    }
    MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);
    /* initialize the Incremental file */
    gu4IncrLength = 0;
    FileClose (i4ConfFd);
    gu4MsrOidCount = 0;
    return MSR_SUCCESS;
}

/*****************************************************************************/
/* Function     : MsrProcessSyncSaveEvent                                    */
/*                                                                           */
/*  Description : This fucntion updates the iss configuration file with all  */
/*                 the configurations present in the RBtree.                 */
/*                                                                           */
/* Input        : u4Event -Event to notify syncup or port deletion           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MSR_SUCCESS/MSR_FAILURE                                    */
/*****************************************************************************/
INT4
MsrProcessSyncSaveEvent (UINT4 u4Event)
{
    INT4                i4RetVal = MSR_FAILURE;
    tMsrConfigData     *pMsrConfigData = NULL;

    if (gMSRIncrSaveFlag == ISS_FALSE)
    {
        MSR_TRC (MSR_TRACE_TYPE,
                 "MSR SYNC not allowed with Incremental save off!!!\n");
        SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                      "MSR SYNC not allowed with Incremental save off!!!"));
        return (TRUE);
    }

/*For both port/bridge type change and port,VLAN deletion indications, many MIB tables need to be deleted and re-populated in case of incremental save option enabled. This is because, VLAN and port id will make the index for many tables.To avoid such tedious process, the entire configuration file is deleted and re-created with new running configuration in the system after the port or VLAN is deleted/bridge or port type is changed. To have such similar handling, the check is done as below*/
    if ((gIssConfigSaveInfo.IssConfigSaveOption == ISS_CONFIG_FLASH_SAVE)
        || (gIssConfigSaveInfo.IssConfigSaveOption == ISS_CONFIG_STARTUP_SAVE)
        || (u4Event == MSR_PORT_DELETE) || (u4Event == MSR_VLAN_DELETE)
        || (u4Event == MSR_PB_PORT_TYPE_CHANGE)
        || (u4Event == MSR_BRIDGE_MODE_CHANGE))

    {
        if (gi4MibSaveStatus == MIB_SAVE_IN_PROGRESS)
        {
            return (FALSE);
        }

        gi4MibSaveStatus = MIB_SAVE_IN_PROGRESS;

        /* Since MsrSaveMIB will save all the latest configurations,
         * flush the Update events posted in the queue and release the memory,
         * before calling MsrSaveMIB*/
        while (OsixQueRecv (MsrsnmpQId, (UINT1 *) &pMsrConfigData,
                            OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
        {
            if (pMsrConfigData->u2MsgType == MSR_CONFIG_SAVE)
            {
                MSRFreeSnmpDataRcvfromQ (pMsrConfigData->MsrCfgData.
                                         pMsrUpdateData);
            }
            else if (pMsrConfigData->u2MsgType == MSR_PROTO_SHUT)
            {
                MsrFreeOidList (pMsrConfigData->MsrCfgData.pMsrOidList,
                                pMsrConfigData->MsrCfgData.pMsrOidList->
                                u2OidNo);
            }

            MemReleaseMemBlock (gMsrQConDataPoolId, (UINT1 *) (pMsrConfigData));
            MsrReleaseUpdateQLock ();
        }

        MSRDeleteAllRBNodesinRBTree ();
        MGMT_LOCK ();
        i4RetVal = MsrSaveMIB ();
        if (i4RetVal == MSR_FALSE)
        {
            MSR_TRC (MSR_TRACE_TYPE, "MSR SYNC failed!!!\n");
            gi4MibSaveStatus = LAST_MIB_SAVE_FAILED;
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                          "MSR SYNC to Flash failed!!!"));
            MGMT_UNLOCK ();
            return (FALSE);
        }

        /* set the global valribale indicating RB tree contains the complete
         * Data read from protols */
        MsrisSaveComplete = ISS_TRUE;
        if ((u4Event == MSR_PORT_DELETE) || (u4Event == MSR_VLAN_DELETE)
            || (u4Event == MSR_PB_PORT_TYPE_CHANGE)
            || (u4Event == MSR_BRIDGE_MODE_CHANGE))

        {
            gi4MibSaveStatus = LAST_MIB_SAVE_SUCCESSFUL;
            MGMT_UNLOCK ();
            /* Write all the latest configurations in iss.conf file */
            if (gIssConfigSaveInfo.IssAutoConfigSave == ISS_TRUE)
            {
                MsrSaveDatatoIncrFile (NULL, MSR_INCREMENTAL_SAVE_BUFF_SIZE);
            }
            return (TRUE);
        }
        MGMT_UNLOCK ();

        /* following function updates the config file from RB Tree
         * and sets incremental file contents to NULL */
        i4RetVal = MsrSaveDatatoIncrFile (NULL, MSR_INCREMENTAL_SAVE_BUFF_SIZE);
        gi4MibSaveStatus = gTempi4MibSaveStatus;
        if (gi4MibSaveStatus == LAST_MIB_SAVE_FAILED)
        {
            MSR_TRC (MSR_TRACE_TYPE, "MSR SYNC failed!!!\n");
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                          "MSR SYNC to Flash failed!!!"));
            return (FALSE);
        }
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4MsrSysLogId,
                      "MSR Configuration SYNC to flash successful!"));
    }
    else if (gIssConfigSaveInfo.IssConfigSaveOption == ISS_CONFIG_REMOTE_SAVE)
    {
        if (gi4RemoteSaveStatus == MIB_SAVE_IN_PROGRESS)
        {
            return (FALSE);
        }
        MSRDeleteAllRBNodesinRBTree ();
        i4RetVal = MsrSaveMIB ();
        if (i4RetVal == MSR_FALSE)
        {
            MSR_TRC (MSR_TRACE_TYPE, "MSR SYNC failed!!!\n");
            gi4RemoteSaveStatus = LAST_MIB_SAVE_FAILED;
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                          "MSR SYNC to Flash failed!!!"));
            return (FALSE);
        }
        /* set the global valribale indicating RB tree contains the complete
         * Data read from protols */
        MsrisSaveComplete = ISS_TRUE;

        gi4RemoteSaveStatus = MIB_SAVE_IN_PROGRESS;
        /* following function updates the gaFlash array from RB Tree, sends
         * data to remote.
         * and sets incremental file/buffer remote contents to NULL */
        i4RetVal =
            MsrSaveDatatoIncrBuffer (NULL, MSR_INCREMENTAL_SAVE_BUFF_SIZE);
        gi4RemoteSaveStatus = gTempi4RemoteSaveStatus;
        if (gi4RemoteSaveStatus == LAST_MIB_SAVE_FAILED)
        {
            MSR_TRC (MSR_TRACE_TYPE, "MSR SYNC failed!!!\n");
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                          "MSR SYNC to remote failed!!!"));
            return (FALSE);
        }
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4MsrSysLogId,
                      "MSR configuration SYNC to remote successful!"));
    }
    return (TRUE);
}

/*****************************************************************************/
/* Function     : MsrisSaveAllowed                                           */
/*                                                                           */
/*  Description : Releases the MSR update Q Semaphore                        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MSR_SUCCESS/MSR_FAILURE                                    */
/*****************************************************************************/
INT4
MsrisSaveAllowed (UINT4 u4ArrayIndex)
{
    if ((gu4MsrSaveIPOptionOIDIndex == u4ArrayIndex)
        || (gu4MsrSaveIPAddrOIDIndex == u4ArrayIndex)
        || (gu4MsrSaveFileNameOIDIndex == u4ArrayIndex))
    {
        return MSR_FALSE;
    }
    else
    {
        return MSR_TRUE;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MSRPopulateRBtreefromFlashArrray                 */
/*                                                                          */
/*   Description        : This function is used to populate RB tree from    */
/*                          gaFlashArray                                    */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/****************************************************************************/
INT4
MSRPopulateRBtreefromFlashArrray ()
{
    UINT1               au1Oid[MSR_MAX_OID_LEN];
    UINT1               au1PrefixOid[MSR_MAX_OID_LEN];
    UINT1               au1SuffixOid[MSR_MAX_OID_LEN];
    UINT2               u2Type = 0;
    UINT4               u4Len = 0;
    UINT1              *pTemp = NULL;
    UINT1              *pu1Data = NULL;
    UINT4               u4Index = 0;
    UINT4               u4MSRArrayIndex = 0;
    UINT4               u4MSRRowStatusType = 0;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_MULTI_DATA_TYPE *pData = NULL;
    UINT4               u4Total = 0;
    UINT4               u4CurrArrayIndex = 0;
    UINT4               u4Error = 0;
    tSNMP_MULTI_DATA_TYPE *pdefData = NULL;
    INT4                i4Result = 0;
    INT4                snmpRetval = SNMP_FAILURE;
    UINT2               u2CfgCntr = 0;
    tMSRRbtree         *pRBNodeCur = NULL;
    UINT4               u4DataCntr = 0;
    gu4MsrMultiDataCount = 0;
    gu4MsrOctetStrCount = 0;
    gu4MsrOidCount = 0;

    gi4MibResStatus = MIB_RESTORE_IN_PROGRESS;

    UtlCalcIpCheckSum (gpu1StrFlash, gu4Length);

    if ((pOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        /*MSR_FREE (pu1RestoreFileName); */
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        return ISS_FAILURE;
    }
    if ((pdefData = MsrAllocMultiData ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        gu4MsrOidCount = 0;
        return ISS_FAILURE;
    }

    MSR_MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);
    MSR_MEMSET (au1PrefixOid, 0, MSR_MAX_OID_LEN);
    MSR_MEMSET (au1SuffixOid, 0, MSR_MAX_OID_LEN);

    gpu1StrCurrent = gpu1StrFlash;
    /* Restore the objects retrieved from the gpu1StrFlash array. 
     * The "CKSUM:" acts as a delimiter marking the end of all
     * saved objects information.
     */
    if (MSR_DATA_ALLOC_MEM_BLOCK (pu1Data) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        gu4MsrOidCount = 0;
        return ISS_FAILURE;
    }
    while (u4DataCntr < gu4Length)
    {
        pu1Data[u2CfgCntr] = gpu1StrCurrent[u4DataCntr++];
        if ((pu1Data[u2CfgCntr++] != '\n')
            && (u2CfgCntr < (MSR_MAX_DATA_LEN - 1)))
        {
            continue;
        }
        u4Total++;
        if (u2CfgCntr <= 1)
        {
            u2CfgCntr = 0;
            MSR_MEMSET (pu1Data, 0, MSR_MAX_DATA_LEN);
            continue;
        }
        /*The "CKSUM:" acts as a delimiter marking the end of
         * information so no need to process it*/
        if (STRNCMP (pu1Data, "CKSUM:", 6) == 0)
        {
            break;
        }
        pTemp = pu1Data;
        if (*pTemp == '@')
        {
            MSR_MEMSET (au1PrefixOid, 0, MSR_MAX_OID_LEN);
            pTemp += 1;
            /* get the MSR index value */
            u4MSRArrayIndex = (UINT4) ATOI (pTemp);
            while (*pTemp != '\t')
            {
                pTemp++;
            }
            pTemp++;

            /* get the prefix oid is present */
            for (u4Index = 0; u4Index < MSR_MAX_OID_LEN; u4Index++)
            {
                if (*pTemp != '\n')
                {
                    au1PrefixOid[u4Index] = *pTemp;
                    pTemp++;
                }
                else
                {
                    au1PrefixOid[u4Index] = '\0';
                    break;
                }
            }
            pTemp = pu1Data;
        }
        else
        {
            if (u4Total > MSR_RES_FILE_VER_FORMAT)
            {
                /* get the MSR Rowstatus type value */
                u4MSRRowStatusType = (UINT4) ATOI (pTemp);
                pTemp += 2;
            }
            else
            {
                u4MSRRowStatusType = MSR_SCALAR;
            }
            /* Get the OID of the object to be restored and convert
             * it from string to the OID type.*/
            for (u4Index = 0; u4Index < MSR_MAX_OID_LEN; u4Index++)
            {
                if (*pTemp != '\t')
                {
                    au1SuffixOid[u4Index] = *pTemp;
                    pTemp++;
                }
                else
                {
                    au1SuffixOid[u4Index] = '\0';
                    break;
                }
            }
            if (au1PrefixOid[0] != '\0')
            {
                SnmpUpdateEoidString (au1PrefixOid);
            }
            else
            {
                SnmpUpdateEoidString (au1SuffixOid);
            }

            /* concatenate the prefix oid and the suffix oids read */
            SNPRINTF ((char *) au1Oid, MSR_MAX_OID_LEN,
                      "%s%s", au1PrefixOid, au1SuffixOid);
            WebnmConvertStringToOid (pOid, au1Oid, MSR_FALSE);

            if (u4MSRArrayIndex != u4CurrArrayIndex)
            {
                /* Get Current MSR array Index and Check that value with
                 * the one present in iss.conf file.If both are same means,
                 * the object is not moved in the gaSaveArray. Hence,
                 * populate the RBTree with that ArrayIndex
                 * (u4MSRArrayIndex) for that object. Else, change the
                 * MSR array index with the current position of the object
                 * and proceed with RBTree population. */

                /* Scalar mib objects are always present in the
                 * au1SuffixOid and ends with 0 */
                if ((au1SuffixOid[0] != '.')
                    && (pOid->pu4_OidList[pOid->u4_Length] == 0))
                {
                    i4Result = MSRGetArryIndexWithOID (au1Oid,
                                                       &u4CurrArrayIndex,
                                                       MSR_SCALAR,
                                                       (u4MSRArrayIndex - 1));
                    if (i4Result == MSR_FALSE)
                    {
                        i4Result =
                            MSRGetArryIndexWithOID (au1Oid,
                                                    &u4CurrArrayIndex,
                                                    MSR_SCALAR, 0);
                    }
                }
                else
                {
                    /* For Tabular Object */
                    i4Result = MSRGetArrayIndexWithTableOID (au1Oid,
                                                             &u4CurrArrayIndex,
                                                             MSR_TABLE,
                                                             (u4MSRArrayIndex -
                                                              1));
                    if (i4Result == MSR_FALSE)
                    {
                        i4Result =
                            MSRGetArrayIndexWithTableOID (au1PrefixOid,
                                                          &u4CurrArrayIndex,
                                                          MSR_TABLE, 0);
                    }

                    if ((i4Result == MSR_TRUE) &&
                        (u4MSRArrayIndex != u4CurrArrayIndex))

                    {
                        u4MSRArrayIndex = u4CurrArrayIndex;
                    }
                }
            }
            /* Get the multidata data type of the object to be restored */
            u2Type = (UINT2) ATOI (pTemp + 1);
            pTemp += 4;
            u4Len = 0;
            if (u2Type == OCTETSTRING)
            {
                u4Len = (UINT4) ATOI (pTemp);
                pTemp += 5;
                while (pTemp[u4Len] != '\n')
                {
                    while (u4DataCntr < gu4Length)
                    {
                        pu1Data[u2CfgCntr] = gpu1StrCurrent[u4DataCntr++];
                        if ((pu1Data[u2CfgCntr++] == '\n') ||
                            (u2CfgCntr >= MSR_MAX_DATA_LEN))
                        {
                            break;
                        }
                    }
                    if (u2CfgCntr >= MSR_MAX_DATA_LEN)
                    {
                        break;
                    }
                }

            }
            pData = MsrConvertStringToData (pTemp, u2Type, u4Len);
            if (pData == NULL)
            {
                MSR_TRC (MSR_TRACE_TYPE, "Memory allocation failed!!!\n");
                gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                gu4MsrOidCount = 0;
                MSR_DATA_FREE_MEM_BLOCK (pu1Data);
                return ISS_FAILURE;
            }

            pTemp = pu1Data;

            /* Restoration should not be done if restore file version
             * is incompatible with executable's restore file version
             */
            if (u4Total == 1)
            {
                /* Verify the first OID is issConfigRestoreFileVersion
                 * or Not. If So then check the version
                 */
                if ((STRCMP (au1Oid, ISS_RESTORE_FILE_VERSION_OID) != 0) ||
                    (MSR_MEMCMP (pData->pOctetStrValue->pu1_OctetList,
                                 ISS_RESTORE_FILE_VERSION,
                                 pData->pOctetStrValue->i4_Length) != 0))
                {
                    MSR_TRC (MSR_TRACE_TYPE, "Incompatable restore file!!!\n");
                    gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                    gu4MsrOidCount = 0;
                    gu4MsrMultiDataCount = 0;
                    gu4MsrOctetStrCount = 0;
                    MSR_DATA_FREE_MEM_BLOCK (pu1Data);
                    return ISS_FAILURE;
                }
            }
            else if (u4Total == 2)
            {
                if ((STRCMP (au1Oid, ISS_RESTORE_FILE_FORMAT_OID) != 0) ||
                    (STRCMP (pData->pOctetStrValue->pu1_OctetList,
                             ISS_RESTORE_FILE_FORMAT) != 0))
                {
                    MSR_TRC (MSR_TRACE_TYPE, "Incompatable restore file!!!\n");
                    gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                    gu4MsrOidCount = 0;
                    gu4MsrMultiDataCount = 0;
                    gu4MsrOctetStrCount = 0;
                    MSR_DATA_FREE_MEM_BLOCK (pu1Data);
                    return ISS_FAILURE;
                }
            }
            else
            {
                /* get default value of the object and compare then 
                 * store (if not default value) the data in to RB tree 
                 * if INCR_SVAE_ENABLED is true / false */
                if ((gMSRIncrSaveFlag == ISS_TRUE) &&
                    (gIssSysGroupInfo.b1IssDefValSaveFlag != ISS_TRUE))
                {
                    snmpRetval = SNMPGetDefaultValueForObj (*pOid,
                                                            pdefData, &u4Error);
                }
                else
                {
                    snmpRetval = SNMP_FAILURE;
                    u4Error = SNMP_EXCEPTION_NO_DEF_VAL;
                }

                if ((snmpRetval == SNMP_SUCCESS) ||
                    ((snmpRetval == SNMP_FAILURE)
                     && (u4Error == SNMP_EXCEPTION_NO_DEF_VAL)))
                {
                    if (snmpRetval != SNMP_FAILURE)
                    {
                        i4Result =
                            MSRCompareSnmpMultiDataValues (pData, pdefData);
                    }
                    else
                    {
                        i4Result = MSR_FAILURE;
                    }

                    if (i4Result == MSR_FAILURE)
                    {
                        /* store the information into RB tree */
                        i4Result = MSRSearchNodeinRBTree (pOid, u4MSRArrayIndex,
                                                          u4MSRRowStatusType,
                                                          &pRBNodeCur);
                        KW_FALSEPOSITIVE_FIX (pRBNodeCur);
                        if (i4Result == MSR_TRUE)
                        {
                            /* data exists in the RB tree so Modify the 
                             * data in the RBNode with new value. */
                            i4Result =
                                MSRModifyRBNodeinRBTree (pData, pRBNodeCur);
                            if (i4Result != MSR_TRUE)
                            {
                                MSR_TRC (MSR_TRACE_TYPE,
                                         "RB Tree Updation failure!!!\n");
                                gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                                gu4MsrOidCount = 0;
                                gu4MsrMultiDataCount = 0;
                                gu4MsrOctetStrCount = 0;
                                MSR_DATA_FREE_MEM_BLOCK (pu1Data);
                                return MSR_FALSE;
                            }
                        }
                        else
                        {
                            /* data not exist in the RBTree, Add new node into 
                             * RBTree and update Linklist */
                            if (MSRInsertRBNodeinRBTree (pOid, pData,
                                                         u4MSRArrayIndex,
                                                         u4MSRRowStatusType,
                                                         MSR_TRUE) == MSR_FALSE)
                            {
                                MSR_TRC (MSR_TRACE_TYPE,
                                         "RB Tree Insertion failure!!!\n");
                                gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                                gu4MsrOidCount = 0;
                                gu4MsrMultiDataCount = 0;
                                gu4MsrOctetStrCount = 0;
                                MSR_DATA_FREE_MEM_BLOCK (pu1Data);
                                return ISS_FAILURE;
                            }
                        }
                    }
                }
            }
            MSR_MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);
            MSR_MEMSET (au1SuffixOid, 0, MSR_MAX_OID_LEN);
        }
        u2CfgCntr = 0;
        MSR_MEMSET (pu1Data, 0, MSR_MAX_DATA_LEN);
    }
    MSR_DATA_FREE_MEM_BLOCK (pu1Data);
    /* Restore operation completed successfuly */
    gi4MibResStatus = LAST_MIB_RESTORE_SUCCESSFUL;
    gu4MsrOidCount = 0;
    gu4MsrMultiDataCount = 0;
    gu4MsrOctetStrCount = 0;
    return ISS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MSRProcessUpdateRSCWandCG
 *  Input           : tMSRUpdatedata
 *  Output          : None
 *  Returns         : MSR_SUCCESS/MSR_FAILURE
 ************************************************************************/
INT4
MSRProcessUpdateRSCWandCG (tMSRUpdatedata * pQData, INT4 isInternalGen)
{
    tSNMP_OID_TYPE     *pCurOid = NULL;
    tSNMP_OID_TYPE     *pTmpOid = NULL;
    tSNMP_OID_TYPE     *pCmplOID = NULL;
    tSNMP_OID_TYPE     *pInstance = NULL;
    tSnmpDbEntry        SnmpDbEntry;
    tMbDbEntry         *pCur = NULL;
    tMbDbEntry         *pNext = NULL;
    tMSRRbtree         *pRBNodeCur = NULL;
    tSNMP_MULTI_DATA_TYPE *pData = NULL;
    INT4                i4Result = MSR_SUCCESS, i4RetVal;
    UINT4               u4Index = 0, u4Index1 = 0, u4tIndx = 0;
    UINT4               u4Len = 0;
    UINT1               au1Oid[MSR_MAX_OID_LEN + 1];
    UINT1               au1Oidin[MSR_MAX_OID_LEN + 1];
    UINT1               u1Action = 0;

    MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);
    MEMSET (au1Oidin, 0, MSR_MAX_OID_LEN);
    STRNCPY (au1Oid, pQData->pOid, (sizeof (au1Oid) - 1));
    au1Oid[(sizeof (au1Oid) - 1)] = '\0';
    STRNCPY (au1Oidin, pQData->pOid, (sizeof (au1Oidin) - 1));
    au1Oidin[(sizeof (au1Oidin) - 1)] = '\0';

    gu4MsrMultiDataCount = 0;
    gu4MsrOctetStrCount = 0;
    gu4MsrOidCount = 0;

    if ((pCurOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        return MSR_FAILURE;
    }
    if ((pTmpOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FAILURE;
    }
    if ((pCmplOID = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FAILURE;
    }
    if ((pInstance = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FAILURE;
    }
    if ((pData = MsrAllocMultiData ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FAILURE;
    }

    for (u4tIndx = STRLEN (au1Oidin); u4tIndx > 0; u4tIndx--)
    {
        if (au1Oidin[u4tIndx] == '.')
        {
            au1Oidin[u4tIndx] = '\0';
            break;
        }
    }
    MSR_CLEAR_OID (pCurOid);
    MSR_CLEAR_OID (pTmpOid);
    MSR_CLEAR_OID (pCmplOID);
    MSR_CLEAR_OID (pInstance);

    pQData->pSnmpData->u4_ULongValue = MSR_CREATE_AND_WAIT;
    /* change create_and_go value to create_and_wait
     */
    pQData->pSnmpData->i4_SLongValue = MSR_CREATE_AND_WAIT;

    i4Result = MSRGetArryIndexWithOID (au1Oid, &u4Index, MSR_TABLE, 0);
    if (i4Result != MSR_TRUE)
    {
        MSR_TRC (MSR_TRACE_TYPE,
                 "MSR Update: Tabular R/S OID not present in MSR array \n");
        gu4MsrOidCount = 0;
        gu4MsrMultiDataCount = 0;
        gu4MsrOctetStrCount = 0;
        return MSR_FAILURE;
    }

    WebnmConvertStringToOid (pCurOid, au1Oid, MSR_FALSE);
    WebnmConvertStringToOid (pTmpOid, au1Oid, MSR_FALSE);
    GetMbDbEntry (*pCurOid, &pNext, &u4Len, &SnmpDbEntry);
    pCur = SnmpDbEntry.pMibEntry;

    if ((pCur == NULL) || (pCur->Set == NULL))
    {
        MSRSendUpdateFailureNotification (pQData);
        gu4MsrOidCount = 0;
        gu4MsrMultiDataCount = 0;
        gu4MsrOctetStrCount = 0;
        return MSR_FAILURE;
    }

    /* Do not store the object if it is a read-only 
     * object. Read-only objects will not have SET 
     * routines associated with them.
     */
    SNMPFormOid (pCmplOID, pCur, pQData->pSnmpindx);
    WebnmGetInstanceOid (pTmpOid, pCmplOID, pInstance);
    if (pInstance->u4_Length == 0)
    {
        MSRSendUpdateFailureNotification (pQData);
        gu4MsrOidCount = 0;
        gu4MsrMultiDataCount = 0;
        gu4MsrOctetStrCount = 0;
        return MSR_FAILURE;
    }
    u4Index1 = u4Index;
    u4tIndx = 0;
    MSRCopySnmpMultiDataValues (pQData->pSnmpData, pData);
    while (1)
    {
        if (u4tIndx > 0)
        {
            if (((gArrayIndxcount[u4Index][2] == 0) && u4tIndx == 1))
            {
                /* convert OID received in to string to search in msrarray */
                i4Result = MSRGetArrayIndexWithTableOID (au1Oidin,
                                                         &u4Index, MSR_TABLE,
                                                         0);
                if (i4Result != MSR_TRUE)
                {
                    MSR_TRC (MSR_TRACE_TYPE,
                             "MSR Update: Tabular OID not present in MSR array \n");
                    break;
                }
                if (u4Index1 == u4Index)
                {
                    break;
                }

            }
            else
            {
                i4Result = MSRGetArrayIndexWithTableOID (au1Oidin,
                                                         &u4Index, MSR_TABLE,
                                                         u4Index);
                if (i4Result != MSR_TRUE)
                {
                    MSR_TRC (MSR_TRACE_TYPE,
                             "MSR Update: Tabular OID not present in MSR array \n");
                    break;
                }
            }
            MSRCopySnmpMultiDataValues (pData, pQData->pSnmpData);
        }
        i4RetVal = MSR_SAVE;
        if (FN_VAL (u4Index) != NULL)
        {
            i4RetVal = (FN_VAL (u4Index)) (pTmpOid,
                                           pInstance, pQData->pSnmpData);
        }
        if ((i4RetVal != MSR_SKIP) && (i4RetVal != MSR_NEXT))
        {
            /* row status elements do not have default values 
             * default value validation not required here */
            if (i4RetVal == MSR_VOLATILE_SAVE)
            {
                /* This is a volatile entry. This should not be saved
                 * to non-volatile storage but should be sent to standby
                 * node. Set the flag to false.
                 */
                pQData->u1IsDataToBeSaved = MSR_FALSE;
            }

            if (isInternalGen == MSR_TRUE)
            {
                i4Result = MSRSearchNodeinRBTree (pCmplOID, u4Index,
                                                  MSR_ROWSTATUS, &pRBNodeCur);

                if (i4Result == MSR_TRUE)
                {
                    /* internal generated and node already present in the 
                     * RB tree non need to add the node */
                    gu4MsrOidCount = 0;
                    gu4MsrMultiDataCount = 0;
                    gu4MsrOctetStrCount = 0;

                    /* If the u1IsDataToBeSaved is modified, reset its
                     * value and add / delete the configuration
                     * from incremental conf file */
                    if (pRBNodeCur->u1IsDataToBeSaved !=
                        pQData->u1IsDataToBeSaved)
                    {
                        pRBNodeCur->u1IsDataToBeSaved
                            = pQData->u1IsDataToBeSaved;
                        if (pRBNodeCur->u1IsDataToBeSaved == MSR_FALSE)
                        {
                            u1Action = MSR_DELETE;
                        }
                        else
                        {
                            u1Action = MSR_MODIFY;
                        }
                        MsrSaveIncrData (pQData, pCmplOID, u4Index,
                                         MSR_ROWSTATUS, u1Action);
                    }
                    return MSR_SUCCESS;
                }
            }
            /* if any validation function changes the 
             * valu the changed value is present in u4_ULongValue */
            pQData->pSnmpData->i4_SLongValue =
                (INT4) pQData->pSnmpData->u4_ULongValue;

            /* Msr internal debug counters */
            gU4NoOfComdsAfterValidations++;

            i4Result = MSRSearchandUpdateRBTree (pQData, pCmplOID, u4Index,
                                                 MSR_ROWSTATUS, MSR_FALSE);
            if (i4Result != MSR_SUCCESS)
            {
                MSRSendUpdateFailureNotification (pQData);
                gu4MsrOidCount = 0;
                gu4MsrMultiDataCount = 0;
                gu4MsrOctetStrCount = 0;
                return MSR_FAILURE;
            }
            /* only one time insertion required for Create and Wait 
             * on successful insertion break from the loop*/
            break;
        }
        u4tIndx++;
    }
    gu4MsrOidCount = 0;
    gu4MsrMultiDataCount = 0;
    gu4MsrOctetStrCount = 0;
    return MSR_SUCCESS;
}

/************************************************************************
 *  Function Name   : MSRProcessUpdateRSActive
 *  Input           : tMSRUpdatedata
 *  Output          : None
 *  Returns         : MSR_SUCCESS/MSR_FAILURE
 ************************************************************************/
INT4
MSRProcessUpdateRSActive (tMSRUpdatedata * pQData)
{
    UINT4               u4Index = 0, u4tIndx = 0;
    UINT4               u4Len = 0;
    UINT1               au1Oid[MSR_MAX_OID_LEN + 1];
    UINT1               au1Oidin[MSR_MAX_OID_LEN + 1];
    INT4                i4Result = MSR_SUCCESS, i4RowstatusValue, i4RetVal;
    tSNMP_OID_TYPE     *pCurOid = NULL;
    tSNMP_OID_TYPE     *pTmpOid = NULL;
    tSNMP_OID_TYPE     *pCmplOID = NULL;
    tSNMP_OID_TYPE     *pInstance = NULL;
    tSnmpDbEntry        SnmpDbEntry;
    tMbDbEntry         *pCur = NULL;
    tMbDbEntry         *pNext = NULL;
    tSNMP_MULTI_DATA_TYPE *pData = NULL;

    MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);
    MEMSET (au1Oidin, 0, MSR_MAX_OID_LEN);
    STRNCPY (au1Oid, pQData->pOid, (sizeof (au1Oid) - 1));
    au1Oid[(sizeof (au1Oid) - 1)] = '\0';
    STRNCPY (au1Oidin, pQData->pOid, (sizeof (au1Oidin) - 1));
    au1Oidin[(sizeof (au1Oidin) - 1)] = '\0';

    i4RowstatusValue = pQData->pSnmpData->i4_SLongValue;
    /* before inserting the Active node into RB tree
     * check and add element with the create_and_wait 
     * no need to check for the failure scenario here*/
    if (pQData->pSnmpData->i4_SLongValue == MSR_ACTIVE)
    {
        pQData->pSnmpData->i4_SLongValue = MSR_CREATE_AND_WAIT;
        MSRProcessUpdateRSCWandCG (pQData, MSR_TRUE);
    }

    pQData->pSnmpData->i4_SLongValue = i4RowstatusValue;

    gu4MsrMultiDataCount = 0;
    gu4MsrOctetStrCount = 0;
    gu4MsrOidCount = 0;

    if ((pCurOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        return MSR_FAILURE;
    }
    if ((pTmpOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FAILURE;
    }
    if ((pCmplOID = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FAILURE;
    }
    if ((pInstance = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FAILURE;
    }
    if ((pData = MsrAllocMultiData ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FAILURE;
    }

    for (u4tIndx = STRLEN (au1Oidin); u4tIndx > 0; u4tIndx--)
    {
        if (au1Oidin[u4tIndx] == '.')
        {
            au1Oidin[u4tIndx] = '\0';
            break;
        }
    }
    MSR_CLEAR_OID (pCurOid);
    MSR_CLEAR_OID (pTmpOid);
    MSR_CLEAR_OID (pCmplOID);
    MSR_CLEAR_OID (pInstance);

    i4Result = MSRGetArrayIndexWithTableOID (au1Oidin, &u4Index, MSR_TABLE, 0);
    if (i4Result != MSR_TRUE)
    {
        MSR_TRC (MSR_TRACE_TYPE,
                 "MSR Update: Tabular OID is not present in MSR array \n");
        gu4MsrMultiDataCount = 0;
        gu4MsrOctetStrCount = 0;
        gu4MsrOidCount = 0;
        return MSR_FAILURE;
    }

    WebnmConvertStringToOid (pCurOid, au1Oid, MSR_FALSE);
    WebnmConvertStringToOid (pTmpOid, au1Oidin, MSR_FALSE);
    GetMbDbEntry (*pCurOid, &pNext, &u4Len, &SnmpDbEntry);
    pCur = SnmpDbEntry.pMibEntry;

    if ((pCur == NULL) || (pCur->Set == NULL))
    {
        MSRSendUpdateFailureNotification (pQData);
        gu4MsrOidCount = 0;
        gu4MsrMultiDataCount = 0;
        gu4MsrOctetStrCount = 0;
        return MSR_FAILURE;
    }

    /* Do not store the object if it is a read-only 
     * object. Read-only objects will not have SET 
     * routines associated with them.
     */
    SNMPFormOid (pCmplOID, pCur, pQData->pSnmpindx);
    WebnmGetInstanceOid (pTmpOid, pCmplOID, pInstance);
    if (pInstance->u4_Length == 0)
    {
        MSRSendUpdateFailureNotification (pQData);
        gu4MsrOidCount = 0;
        gu4MsrMultiDataCount = 0;
        gu4MsrOctetStrCount = 0;
        return MSR_FAILURE;
    }
    u4tIndx = 0;
    MSRCopySnmpMultiDataValues (pQData->pSnmpData, pData);
    while (1)
    {
        if (u4tIndx > 0)
        {
            i4Result = MSRGetArrayIndexWithTableOID (au1Oidin,
                                                     &u4Index, MSR_TABLE,
                                                     u4Index);
            if (i4Result != MSR_TRUE)
            {
                break;
            }
            MSRCopySnmpMultiDataValues (pData, pQData->pSnmpData);
        }
        i4RetVal = MSR_SAVE;
        if (FN_VAL (u4Index) != NULL)
        {
            i4RetVal = (FN_VAL (u4Index)) (pTmpOid,
                                           pInstance, pQData->pSnmpData);
        }
        if ((i4RetVal != MSR_SKIP) && (i4RetVal != MSR_NEXT))
        {
            if (i4RetVal == MSR_VOLATILE_SAVE)
            {
                /* This is a volatile entry. This should not be saved
                 * to non-volatile storage but should be sent to standby
                 * node. Set the flag to false.
                 */
                pQData->u1IsDataToBeSaved = MSR_FALSE;
            }

            /* row status elements do not have default values 
             * default value validation not required here */
            /* Msr internal debug counters */
            gU4NoOfComdsAfterValidations++;

            i4Result = MSRSearchandUpdateRBTree (pQData, pCmplOID, u4Index,
                                                 MSR_NONROWSTATUS, MSR_FALSE);
            if (i4Result != MSR_SUCCESS)
            {
                MSRSendUpdateFailureNotification (pQData);
                gu4MsrOidCount = 0;
                gu4MsrMultiDataCount = 0;
                gu4MsrOctetStrCount = 0;
                return MSR_FAILURE;
            }
        }
        u4tIndx++;
    }
    gu4MsrOidCount = 0;
    gu4MsrMultiDataCount = 0;
    gu4MsrOctetStrCount = 0;
    return MSR_SUCCESS;
}

/************************************************************************
 *  Function Name   : MSRProcessUpdateNonRSObj
 *  Input           : tMSRUpdatedata
 *  Output          : None
 *  Returns         : MSR_SUCCESS/MSR_FAILURE
 ************************************************************************/
INT4
MSRProcessUpdateNonRSObj (tMSRUpdatedata * pQData)
{
    UINT4               u4Index = 0, u4Index1 = 0, u4tIndx = 0;
    UINT4               u4Len = 0, u4Error = 0;
    UINT1               au1Oid[MSR_MAX_OID_LEN + 1];
    UINT1               au1Oidin[MSR_MAX_OID_LEN + 1];
    INT4                i4Result = MSR_SUCCESS, i4RetVal;
    INT4                i4SnmpRetval = SNMP_FAILURE;
    UINT1               au1Tmp[] = ".0";
    tSNMP_OID_TYPE     *pCurOid = NULL;
    tSNMP_OID_TYPE     *pTmpOid = NULL;
    tSNMP_OID_TYPE     *pCmplOID = NULL;
    tSNMP_OID_TYPE     *pInstance = NULL;
    tSnmpDbEntry        SnmpDbEntry;
    tMbDbEntry         *pCur = NULL;
    tMbDbEntry         *pNext = NULL;
    tSNMP_MULTI_DATA_TYPE *pdefData = NULL;
    tSNMP_MULTI_DATA_TYPE *pData = NULL;

    gu4MsrMultiDataCount = 0;
    gu4MsrOctetStrCount = 0;
    gu4MsrOidCount = 0;

    if ((pCurOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        return MSR_FAILURE;
    }
    if ((pTmpOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FAILURE;
    }
    if ((pCmplOID = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FAILURE;
    }
    if ((pInstance = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FAILURE;
    }
    if ((pdefData = MsrAllocMultiData ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FAILURE;
    }
    if ((pData = MsrAllocMultiData ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        gu4MsrMultiDataCount = 0;
        gu4MsrOctetStrCount = 0;
        return MSR_FAILURE;
    }

    MSR_CLEAR_OID (pCurOid);
    MSR_CLEAR_OID (pTmpOid);
    MSR_CLEAR_OID (pCmplOID);
    MSR_CLEAR_OID (pInstance);

    MEMSET (au1Oidin, 0, MSR_MAX_OID_LEN);
    STRNCPY (au1Oidin, pQData->pOid, (sizeof (au1Oidin) - 1));
    au1Oidin[(sizeof (au1Oidin) - 1)] = '\0';
    WebnmConvertStringToOid (pCurOid, au1Oidin, MSR_FALSE);

    for (u4tIndx = STRLEN (au1Oidin); u4tIndx > 0; u4tIndx--)
    {
        if (au1Oidin[u4tIndx] == '.')
        {
            au1Oidin[u4tIndx] = '\0';
            break;
        }
    }
    /* convert OID received in to string to search in msrarray */
    i4Result = MSRGetArrayIndexWithTableOID (au1Oidin, &u4Index, MSR_TABLE, 0);

    if (i4Result != MSR_TRUE)
    {
        gu4MsrOidCount = 0;
        gu4MsrMultiDataCount = 0;
        gu4MsrOctetStrCount = 0;
        return MSR_FAILURE;
    }
    MEMSET (au1Oid, 0, MSR_MAX_OID_LEN + 1);
    STRNCPY (au1Oid, gaSaveArray[u4Index].pOidString,
             MSR_MAX_OID_LEN - STRLEN (au1Tmp));
    WebnmConvertStringToOid (pTmpOid, au1Oid, MSR_FALSE);

    GetMbDbEntry (*pCurOid, &pNext, &u4Len, &SnmpDbEntry);
    pCur = SnmpDbEntry.pMibEntry;
    if (pCur == NULL)
    {
        gu4MsrOidCount = 0;
        gu4MsrMultiDataCount = 0;
        gu4MsrOctetStrCount = 0;
        return MSR_FAILURE;
    }
    else
    {
        if (pCur->Set == NULL)
        {
            gu4MsrOidCount = 0;
            gu4MsrMultiDataCount = 0;
            gu4MsrOctetStrCount = 0;
            return MSR_FAILURE;
        }
    }
    if ((pCur->u4OIDAccess != SNMP_READWRITE) &&
        (pCur->u4OIDAccess != SNMP_READCREATE))
    {
        gu4MsrOidCount = 0;
        gu4MsrMultiDataCount = 0;
        gu4MsrOctetStrCount = 0;
        return MSR_FAILURE;
    }

    WebnmConvertOidToString (pTmpOid, au1Oidin);
    SNMPFormOid (pCmplOID, pCur, pQData->pSnmpindx);
    WebnmGetInstanceOid (pTmpOid, pCmplOID, pInstance);
    if (pInstance->u4_Length == 0)
    {
        MSRSendUpdateFailureNotification (pQData);
        gu4MsrOidCount = 0;
        gu4MsrMultiDataCount = 0;
        gu4MsrOctetStrCount = 0;
        return MSR_FAILURE;
    }

    MSR_CLEAR_OID (pCurOid);
    WebnmCopyOid (pCurOid, pTmpOid);
    u4Index1 = u4Index;
    MSRCopySnmpMultiDataValues (pQData->pSnmpData, pData);

    for (u4tIndx = 0; u4tIndx < gArrayIndxcount[u4Index][0]; u4tIndx++)
    {
        if (u4tIndx != 0)
        {
            i4Result = MSRGetArrayIndexWithTableOID (au1Oidin,
                                                     &u4Index1, MSR_TABLE,
                                                     u4Index1);
            if (i4Result != MSR_TRUE)
            {
                continue;
            }

            MSR_CLEAR_OID (pTmpOid);
            WebnmCopyOid (pTmpOid, pCurOid);
            MSRCopySnmpMultiDataValues (pData, pQData->pSnmpData);
        }

        if ((FN_VAL (u4Index1)) != NULL)
        {
            i4RetVal = (FN_VAL (u4Index1)) (pTmpOid,
                                            pInstance, pQData->pSnmpData);
            if (i4RetVal == MSR_VOLATILE_SAVE)
            {
                /* This is a volatile entry. This should not be saved
                 * to non-volatile storage but should be sent to standby
                 * node. Set the flag to false.
                 */
                pQData->u1IsDataToBeSaved = MSR_FALSE;
            }
            else if (i4RetVal != MSR_SAVE)
            {
                continue;
            }
        }

        /* Msr internal debug counters */
        gU4NoOfComdsAfterValidations++;

        WebnmCopyOid (pTmpOid, pInstance);

        /* When the Default Value Save Option is disabled, get the
         * default value of the given Oid and compare it with the
         * value obtained in the SNMPGet
         */
        i4Result = MSR_FAILURE;
        if (gIssSysGroupInfo.b1IssDefValSaveFlag == ISS_FALSE)
        {
            MSR_CLEAR_MULTIDATA (pdefData);
            i4SnmpRetval = SNMPGetDefaultValueForObj (*pTmpOid, pdefData,
                                                      &u4Error);
            if ((i4SnmpRetval == SNMP_FAILURE) &&
                (u4Error != SNMP_EXCEPTION_NO_DEF_VAL))
            {
                MSRSendUpdateFailureNotification (pQData);
                gu4MsrOidCount = 0;
                gu4MsrMultiDataCount = 0;
                gu4MsrOctetStrCount = 0;
                return MSR_FAILURE;
            }
            if (i4SnmpRetval == SNMP_SUCCESS)
            {
                i4Result = MSRCompareSnmpMultiDataValues (pQData->pSnmpData,
                                                          pdefData);
            }
        }
        if (i4Result == MSR_SUCCESS)
        {
            MSRSearchandUpdateRBTree (pQData, pTmpOid, u4Index1,
                                      MSR_NONROWSTATUS, MSR_TRUE);
        }
        else
        {
            i4Result = MSRSearchandUpdateRBTree (pQData, pTmpOid, u4Index1,
                                                 MSR_NONROWSTATUS, MSR_FALSE);
            if (i4Result != MSR_SUCCESS)
            {
                MSRSendUpdateFailureNotification (pQData);
                gu4MsrOidCount = 0;
                gu4MsrMultiDataCount = 0;
                gu4MsrOctetStrCount = 0;
                return MSR_FAILURE;
            }
        }
    }
    gu4MsrOidCount = 0;
    gu4MsrMultiDataCount = 0;
    gu4MsrOctetStrCount = 0;
    return MSR_SUCCESS;
}

/************************************************************************
 *  Function Name   : MSRProcessUpdateDestroyRS
 *  Input           : tMSRUpdatedata
 *  Output          : None
 *  Returns         : MSR_SUCCESS/MSR_FAILURE
 ************************************************************************/
INT4
MSRProcessUpdateDestroyRS (tMSRUpdatedata * pQData)
{
    UINT4               u4Index = 0, u4Index1 = 0, u4Index2 = 0;
    UINT4               u4Len = 0, u4tIndx = 0, u4tIndx1 = 0;
    UINT1               au1Oid[MSR_MAX_OID_LEN + 1], *pau1Oid = NULL;
    UINT4               u4Error = 0;
    UINT1               au1Oidin[MSR_MAX_OID_LEN + 1];
    INT4                i4Result = MSR_SUCCESS;
    INT4                i4SnmpRetval = SNMP_FAILURE;
    UINT1               au1Tmp[] = ".0";
    tSNMP_OID_TYPE     *pCurOid = NULL;
    tSNMP_OID_TYPE     *pTmpOid = NULL;
    tSNMP_OID_TYPE     *pCmplOID = NULL;
    tSNMP_OID_TYPE     *pInstance = NULL;
    tSNMP_OID_TYPE     *pInstance1 = NULL;
    tSNMP_OID_TYPE     *pNextOid = NULL;
    tSNMP_OID_TYPE     *pOID = NULL;
    tSnmpDbEntry        SnmpDbEntry;
    tMbDbEntry         *pCur = NULL;
    tMbDbEntry         *pNext = NULL;
    tSnmpIndex         *pCurIndex = MsrIndexPool1;
    tSnmpIndex         *pNextIndex = MsrIndexPool2;
    tSNMP_MULTI_DATA_TYPE *pData = NULL;

    pau1Oid = au1Oid;
    MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);
    MEMSET (au1Oidin, 0, MSR_MAX_OID_LEN);
    STRNCPY (au1Oid, pQData->pOid, (MSR_MAX_OID_LEN));
    au1Oid[MSR_MAX_OID_LEN] = '\0';
    STRNCPY (au1Oidin, pQData->pOid, (MSR_MAX_OID_LEN));
    au1Oidin[MSR_MAX_OID_LEN] = '\0';

    gu4MsrMultiDataCount = 0;
    gu4MsrOctetStrCount = 0;
    gu4MsrOidCount = 0;

    if ((pCurOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        return MSR_FAILURE;
    }
    if ((pNextOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FAILURE;
    }
    if ((pTmpOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FAILURE;
    }
    if ((pCmplOID = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FAILURE;
    }
    if ((pInstance = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FAILURE;
    }
    if ((pInstance1 = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FAILURE;
    }
    if ((pData = MsrAllocMultiData ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FAILURE;
    }
    if ((MsrAllocMultiData ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        gu4MsrMultiDataCount = 0;
        gu4MsrOctetStrCount = 0;
        return MSR_FAILURE;
    }

    MSR_CLEAR_OID (pCurOid);
    MSR_CLEAR_OID (pTmpOid);
    MSR_CLEAR_OID (pCmplOID);
    MSR_CLEAR_OID (pInstance);

    i4Result = MSRGetArryIndexWithOID (au1Oid, &u4Index, MSR_TABLE, 0);
    if (i4Result != MSR_TRUE)
    {
        MSR_TRC (MSR_TRACE_TYPE,
                 "MSR Update: Tabular R/S OID not present in MSR array \n");
        gu4MsrOidCount = 0;
        gu4MsrMultiDataCount = 0;
        gu4MsrOctetStrCount = 0;
        return MSR_FAILURE;
    }

    WebnmConvertStringToOid (pCurOid, au1Oid, MSR_FALSE);
    WebnmConvertStringToOid (pTmpOid, au1Oid, MSR_FALSE);
    GetMbDbEntry (*pCurOid, &pNext, &u4Len, &SnmpDbEntry);
    pCur = SnmpDbEntry.pMibEntry;

    if (pCur == NULL)
    {
        gu4MsrOidCount = 0;
        gu4MsrMultiDataCount = 0;
        gu4MsrOctetStrCount = 0;
        return MSR_FAILURE;
    }
    else
    {
        /* Do not store the object if it is a read-only 
         * object. Read-only objects will not have SET 
         * routines associated with them.
         */

        SNMPFormOid (pCmplOID, pCur, pQData->pSnmpindx);
        WebnmGetInstanceOid (pCurOid, pCmplOID, pInstance);
        if (pInstance->u4_Length == 0)
        {
            MSRSendUpdateFailureNotification (pQData);
            gu4MsrOidCount = 0;
            gu4MsrMultiDataCount = 0;
            gu4MsrOctetStrCount = 0;
            return MSR_FAILURE;
        }
        if (pCur->Set != NULL)
        {
            /* Delete the create and wait entry corresponding to the 
             * Row status element received */
            MSRSearchandUpdateRBTree (pQData, pCmplOID, u4Index,
                                      MSR_ROWSTATUS, MSR_TRUE);
        }
    }
    u4Index1 = u4Index;
    for (u4tIndx = 0; u4tIndx < gArrayIndxcount[u4Index][1]; u4tIndx++)
    {
        if (u4tIndx != 0)
        {
            i4Result = MSRGetArryIndexWithOID (au1Oidin,
                                               &u4Index1, MSR_TABLE, u4Index1);
            if (i4Result != MSR_TRUE)
            {
                continue;
            }
        }
        MSR_CLEAR_OID (pNextOid);
        MSR_CLEAR_OID (pTmpOid);
        /* reset the au1Oid to the current OID */
        /* Copy a maximum of MSR_MAX_OID_LEN - STRLEN(au1Tmp) bytes only
         * so that ".0" can be appended tp au1Oid. This will avoid array
         * bound violation */

        MEMSET (au1Oid, 0, MSR_MAX_OID_LEN + 1);
        STRNCPY (au1Oid, gaSaveArray[u4Index1].pOidString,
                 MSR_MAX_OID_LEN - STRLEN (au1Tmp));
        WebnmConvertStringToOid (pTmpOid, au1Oid, MSR_FALSE);
        MEMSET (au1Oid, 0, MSR_MAX_OID_LEN + 1);
        if ((STRLEN (au1Tmp) + STRLEN (gaSaveArray[u4Index1].pOidString)) <
            MSR_MAX_OID_LEN)
        {
            SNPRINTF ((char *) pau1Oid, (MSR_MAX_OID_LEN + 1), "%2s%s",
                      gaSaveArray[u4Index1].pOidString, au1Tmp);
        }
        else
        {
            gu4MsrOidCount = 0;
            gu4MsrMultiDataCount = 0;
            gu4MsrOctetStrCount = 0;
            return MSR_FAILURE;
        }

        WebnmConvertStringToOid (pCurOid, au1Oid, MSR_FALSE);

        /* to Remover */
        GetMbDbEntry (*pCurOid, &pNext, &u4Len, &SnmpDbEntry);
        pCur = SnmpDbEntry.pMibEntry;
        if ((pCur == NULL) && (pNext != NULL))
        {
            while ((pNext != NULL) && (pNext->u4OIDAccess == SNMP_NOACCESS))
            {
                pOID = NULL;
                pOID = &(pNext->ObjectID);
                GetMbDbEntry (*pOID, &pNext, &u4Len, &SnmpDbEntry);
                if ((pNext != NULL) && (pNext->u4OIDAccess != SNMP_NOACCESS))
                {
                    break;
                }
            }
            pCur = pNext;
            pOID = &(pNext->ObjectID);
        }
        if ((pCur == NULL) || (pOID == NULL))
        {
            continue;
        }
        MSR_CLEAR_OID (pCurOid);
        /* Reset the pCurOid to the original OID */
        WebnmCopyOid (pCurOid, &(pCur->ObjectID));
        /* Get all the objects in the table with same instance value */
        while (1)
        {
            MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);
            WebnmConvertOidToString (pCurOid, au1Oid);
            if (STRNCMP (au1Oid, gaSaveArray[u4Index1].pOidString,
                         STRLEN (gaSaveArray[u4Index1].pOidString)) != 0)
            {
                break;
            }
            WebnmConvertOidToString (pTmpOid, au1Oid);

            WebnmCopyOid (pCurOid, pInstance);
            GetMbDbEntry (*pCurOid, &pNext, &u4Len, &SnmpDbEntry);
            pCur = SnmpDbEntry.pMibEntry;
            if (pCur == NULL)
            {
                gu4MsrOidCount = 0;
                gu4MsrMultiDataCount = 0;
                gu4MsrOctetStrCount = 0;
                return MSR_FAILURE;
            }
            else
            {
                if (pCur->Set == NULL)
                {
                    if (pNext == NULL)
                    {
                        break;
                    }
                    MSR_CLEAR_OID (pCurOid);
                    /* Reset the pCurOid to the original OID */
                    WebnmCopyOid (pCurOid, &(pNext->ObjectID));
                    continue;
                }
            }
            if ((pCur->u4OIDAccess != SNMP_READWRITE) &&
                (pCur->u4OIDAccess != SNMP_READCREATE))
            {
                if (pNext != NULL)
                {
                    MSR_CLEAR_OID (pCurOid);
                    /* Reset the pCurOid to the original OID */
                    WebnmCopyOid (pCurOid, &(pNext->ObjectID));
                    continue;
                }
                else
                {
                    break;
                }
            }

            /* Delete the entry from RB Tree */
            MSRSearchandUpdateRBTree (pQData, pCurOid, u4Index1,
                                      MSR_NONROWSTATUS, MSR_TRUE);
            if (pNext != NULL)
            {
                MSR_CLEAR_OID (pCurOid);
                /* Reset the pCurOid to the original OID */
                WebnmCopyOid (pCurOid, &(pNext->ObjectID));
                continue;
            }
            else
            {
                break;
            }

        }
    }
    while (gArrayIndxcount[u4Index][2] == 0)
    {
        if (u4tIndx1 > 0)
        {
            break;
        }
        u4tIndx1++;

        /* convert OID received in to string to search in msrarray */
        MEMSET (au1Oidin, 0, MSR_MAX_OID_LEN);
        MSR_STRCPY (au1Oidin, pQData->pOid);
        for (u4tIndx = STRLEN (au1Oidin); u4tIndx > 0; u4tIndx--)
        {
            if (au1Oidin[u4tIndx] == '.')
            {
                au1Oidin[u4tIndx] = '\0';
                break;
            }
        }
        i4Result = MSRGetArrayIndexWithTableOID (au1Oidin,
                                                 &u4Index2, MSR_TABLE, 0);
        if (i4Result != MSR_TRUE)
        {
            break;
        }
        u4Index1 = u4Index2;
        for (u4tIndx = 0; u4tIndx < gArrayIndxcount[u4Index2][0]; u4tIndx++)
        {
            if (u4tIndx != 0)
            {
                i4Result = MSRGetArrayIndexWithTableOID (au1Oidin,
                                                         &u4Index1, MSR_TABLE,
                                                         u4Index1);
                if (i4Result != MSR_TRUE)
                {
                    break;
                }
            }
            MSR_CLEAR_OID (pNextOid);
            MSR_CLEAR_OID (pTmpOid);
            MSR_CLEAR_MULTIDATA (pData);

            /* reset the au1Oid to the current OID */
            /* Copy a maximum of MSR_MAX_OID_LEN - STRLEN(au1Tmp) bytes only
             * so that ".0" can be appended tp au1Oid. This will avoid array
             * bound violation */
            MEMSET (au1Oid, 0, MSR_MAX_OID_LEN + 1);
            STRNCPY (au1Oid, gaSaveArray[u4Index1].pOidString,
                     MSR_MAX_OID_LEN - STRLEN (au1Tmp));
            WebnmConvertStringToOid (pTmpOid, au1Oid, MSR_FALSE);
            MEMSET (au1Oid, 0, MSR_MAX_OID_LEN + 1);
            if ((STRLEN (au1Tmp) + STRLEN (gaSaveArray[u4Index1].pOidString))
                < MSR_MAX_OID_LEN)
            {
                SNPRINTF ((char *) pau1Oid, (MSR_MAX_OID_LEN + 1), "%2s%s",
                          gaSaveArray[u4Index1].pOidString, au1Tmp);
            }
            else
            {
                gu4MsrOidCount = 0;
                gu4MsrMultiDataCount = 0;
                gu4MsrOctetStrCount = 0;
                return MSR_FAILURE;
            }
            WebnmConvertStringToOid (pCurOid, au1Oid, MSR_FALSE);

            i4SnmpRetval = SNMPGetNextOID (*pCurOid, pNextOid, pData,
                                           &u4Error, pCurIndex,
                                           pNextIndex, SNMP_DEFAULT_CONTEXT);

            if (i4SnmpRetval != SNMP_SUCCESS)
            {
                break;
            }

            WebnmGetInstanceOid (pTmpOid, pNextOid, pInstance1);
            if (pInstance1->u4_Length == 0)
            {
                break;
            }
            pCurOid->pu4_OidList[pCurOid->u4_Length - 1] =
                pInstance1->pu4_OidList[0];
            WebnmCopyOid (pInstance1, pInstance);

            /* Get all the objects in the table with same instance value */
            while (1)
            {
                MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);
                WebnmConvertOidToString (pCurOid, au1Oid);
                if (STRNCMP (au1Oid, gaSaveArray[u4Index1].pOidString,
                             STRLEN (gaSaveArray[u4Index1].pOidString)) != 0)
                {
                    break;
                }
                WebnmCopyOid (pCurOid, pInstance);
                GetMbDbEntry (*pCurOid, &pNext, &u4Len, &SnmpDbEntry);
                pCur = SnmpDbEntry.pMibEntry;
                if (pCur == NULL)
                {
                    gu4MsrOidCount = 0;
                    gu4MsrMultiDataCount = 0;
                    gu4MsrOctetStrCount = 0;
                    return MSR_FAILURE;
                }
                else
                {
                    if (pCur->Set == NULL)
                    {
                        if (pNext != NULL)
                        {
                            MSR_CLEAR_OID (pCurOid);
                            /* Reset the pCurOid to the original OID */
                            WebnmCopyOid (pCurOid, &(pNext->ObjectID));
                            continue;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                if ((pCur->u4OIDAccess != SNMP_READWRITE) &&
                    (pCur->u4OIDAccess != SNMP_READCREATE))
                {
                    if (pNext != NULL)
                    {
                        MSR_CLEAR_OID (pCurOid);
                        /* Reset the pCurOid to the original OID */
                        WebnmCopyOid (pCurOid, &(pNext->ObjectID));
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }

                MSRSearchandUpdateRBTree (pQData, pCurOid, u4Index1,
                                          MSR_NONROWSTATUS, MSR_TRUE);

                if (pNext != NULL)
                {
                    MSR_CLEAR_OID (pCurOid);
                    /* Reset the pCurOid to the original OID */
                    WebnmCopyOid (pCurOid, &(pNext->ObjectID));
                    continue;
                }
                else
                {
                    break;
                }
            }
        }
    }
    gu4MsrOidCount = 0;
    gu4MsrMultiDataCount = 0;
    gu4MsrOctetStrCount = 0;
    return MSR_SUCCESS;
}

/************************************************************************
 *  Function Name   : MSRProcessUpdateScalarObj
 *  Input           : tMSRUpdatedata
 *  Output          : None
 *  Returns         : MSR_SUCCESS/MSR_FAILURE
 ************************************************************************/
INT4
MSRProcessUpdateScalarObj (tMSRUpdatedata * pQData)
{
    UINT4               u4Index = 0, u4Index1 = 0;
    UINT4               u4Len = 0, u4tIndx = 0;
    UINT1               au1Oid[MSR_MAX_OID_LEN + 1];
    UINT4               u4Error = 0;
    INT4                i4Result = MSR_SUCCESS, i4RetVal;
    INT4                i4SnmpRetval = SNMP_FAILURE;
    UINT1               au1Tmp[] = ".0";
    tSNMP_OID_TYPE     *pCurOid = NULL;
    tSNMP_OID_TYPE     *pTmpOid = NULL;
    tSnmpDbEntry        SnmpDbEntry;
    tMbDbEntry         *pCur = NULL;
    tMbDbEntry         *pNext = NULL;
    tSNMP_MULTI_DATA_TYPE *pdefData = NULL;
    tSNMP_MULTI_DATA_TYPE *pData = NULL;
    gu4MsrMultiDataCount = 0;
    gu4MsrOctetStrCount = 0;
    gu4MsrOidCount = 0;

    if ((pCurOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        return MSR_FAILURE;
    }
    if ((pTmpOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FAILURE;
    }
    if ((pdefData = MsrAllocMultiData ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return MSR_FAILURE;
    }
    if ((pData = MsrAllocMultiData ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        gu4MsrMultiDataCount = 0;
        gu4MsrOctetStrCount = 0;
        return MSR_FAILURE;
    }

    MSR_CLEAR_OID (pCurOid);
    MSR_CLEAR_OID (pTmpOid);

    MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);
    STRNCPY (au1Oid, pQData->pOid, (sizeof (au1Oid) - 1));
    au1Oid[(sizeof (au1Oid) - 1)] = '\0';
    WebnmConvertStringToOid (pTmpOid, au1Oid, MSR_FALSE);
    if ((STRLEN (au1Oid) + STRLEN (au1Tmp)) < MSR_MAX_OID_LEN)
    {
        SNPRINTF ((char *) au1Oid, MSR_MAX_OID_LEN, "%2s%s", au1Oid, au1Tmp);
    }
    else
    {
        MSR_TRC (MSR_TRACE_TYPE,
                 "MSR Update:String length exceeds the max length allowed \n");
        gu4MsrOidCount = 0;
        gu4MsrMultiDataCount = 0;
        gu4MsrOctetStrCount = 0;
        return MSR_FAILURE;
    }

    i4Result = MSRGetArryIndexWithOID (au1Oid, &u4Index, MSR_SCALAR, 0);

    if (i4Result != MSR_TRUE)
    {
        MSR_TRC (MSR_TRACE_TYPE,
                 "MSR Update: Scalar OID not present in MSR array \n");
        gu4MsrOidCount = 0;
        gu4MsrMultiDataCount = 0;
        gu4MsrOctetStrCount = 0;
        return MSR_FAILURE;
    }
    WebnmConvertStringToOid (pCurOid, au1Oid, MSR_FALSE);

    GetMbDbEntry (*pCurOid, &pNext, &u4Len, &SnmpDbEntry);
    pCur = SnmpDbEntry.pMibEntry;

    if (pCur == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE,
                 "MSR Update: No MBDB entry present for Scalar OID \n");
        gu4MsrOidCount = 0;
        gu4MsrMultiDataCount = 0;
        gu4MsrOctetStrCount = 0;
        return MSR_FAILURE;
    }
    if ((pCur->u4OIDAccess != SNMP_READWRITE) &&
        (pCur->u4OIDAccess != SNMP_READCREATE))
    {
        gu4MsrOidCount = 0;
        gu4MsrMultiDataCount = 0;
        gu4MsrOctetStrCount = 0;
        return MSR_FAILURE;
    }
    u4Index1 = u4Index;
    MSRCopySnmpMultiDataValues (pQData->pSnmpData, pData);

    for (u4tIndx = 0; u4tIndx < gArrayIndxcount[u4Index][0]; u4tIndx++)
    {
        if (u4tIndx != 0)
        {
            i4Result = MSRGetArryIndexWithOID (au1Oid,
                                               &u4Index1, MSR_SCALAR, u4Index1);
            if (i4Result != MSR_TRUE)
            {
                continue;
            }
            MSRCopySnmpMultiDataValues (pData, pQData->pSnmpData);
        }

        if ((FN_VAL (u4Index1)) != NULL)
        {
            i4RetVal = (FN_VAL (u4Index1)) (pCurOid, NULL, pQData->pSnmpData);

            if (i4RetVal == MSR_VOLATILE_SAVE)
            {
                /* This is a volatile entry. This should not be saved
                 * to non-volatile storage but should be sent to standby
                 * node. Set the flag to false.
                 */
                pQData->u1IsDataToBeSaved = MSR_FALSE;
            }
            else if (i4RetVal != MSR_SAVE)
            {
                continue;
            }
        }
        /* Msr internal debug counters */
        gU4NoOfComdsAfterValidations++;
        /* When the Default Value Save Option is disabled, get the
         * default value of the given Oid and compare it with the
         * value obtained in the SNMPGet
         */
        i4Result = MSR_FAILURE;
        if (gIssSysGroupInfo.b1IssDefValSaveFlag == ISS_FALSE)
        {
            i4SnmpRetval = SNMPGetDefaultValueForObj (*pCurOid, pdefData,
                                                      &u4Error);
            if ((i4SnmpRetval == SNMP_FAILURE) &&
                (u4Error != SNMP_EXCEPTION_NO_DEF_VAL))
            {
                MSRSendUpdateFailureNotification (pQData);
                gu4MsrOidCount = 0;
                gu4MsrMultiDataCount = 0;
                gu4MsrOctetStrCount = 0;
                return MSR_FAILURE;
            }
            if (i4SnmpRetval == SNMP_SUCCESS)
            {
                i4Result = MSRCompareSnmpMultiDataValues (pQData->pSnmpData,
                                                          pdefData);
            }
        }
        if (i4Result == MSR_SUCCESS)
        {
            MSRSearchandUpdateRBTree (pQData, pCurOid, u4Index1,
                                      MSR_NONROWSTATUS, MSR_TRUE);
        }
        else
        {
            i4Result = MSRSearchandUpdateRBTree (pQData, pCurOid,
                                                 u4Index1, MSR_NONROWSTATUS,
                                                 MSR_FALSE);
            if (i4Result != MSR_SUCCESS)
            {
                MSRSendUpdateFailureNotification (pQData);
                gu4MsrOidCount = 0;
                gu4MsrMultiDataCount = 0;
                gu4MsrOctetStrCount = 0;
                return MSR_FAILURE;
            }
        }
    }
    gu4MsrOidCount = 0;
    gu4MsrMultiDataCount = 0;
    gu4MsrOctetStrCount = 0;
    return MSR_SUCCESS;
}

/************************************************************************
 *  Function Name   : MsrStoreData
 *  Input           : tMSRUpdatedata
 *  Output          : None
 *  Returns         : MSR_TRUE/MSR_FALSE
 ************************************************************************/
INT4
MsrStoreData (tSNMP_OID_TYPE * pOid,
              tSNMP_MULTI_DATA_TYPE * pData,
              UINT4 u4MSRRowStatusType,
              UINT1 *pau1Oid,
              UINT4 *pu4Sum,
              UINT2 *pu2CkSum,
              UINT4 *pu4PrevIndex,
              UINT4 u4Index, UINT4 *pu4Length, UINT1 u1IsDataToBeSaved)
{
    UINT4               u4DataSize = 0;
    UINT1              *pu1Data = NULL;
    UINT1              *pTemp;
    tMSRRbtree         *pRBNodeCur = NULL;
    INT4                i4Result;
    static tConnStatus  ConnStatus;
    static UINT2        u2PrefLen = 0;
    UINT1               au1PrefixOid[MSR_MAX_OID_LEN];
    INT4                i4CustSaveOption = ISS_DEFAULT_MODE;
    INT4                i4RetVal = 0;

    if (gMSRIncrSaveFlag == ISS_FALSE)
    {
        if (NULL != MSR_CALLBACK[MSR_SECURE_PROCESS].pMsrCustSecureStorage)
        {
            MSR_CALLBACK[MSR_SECURE_PROCESS].pMsrCustSecureStorage
                (ISS_SECURE_MEM_SAVE, ISS_MIB_OBJECT,
                 pau1Oid, pData, &i4CustSaveOption);
        }
        if (ISS_TRUE == i4CustSaveOption)
        {
            return MSR_TRUE;
        }
        else if (ISS_FALSE == i4CustSaveOption)
        {
            return MSR_FALSE;
        }
        if (MSR_DATA_ALLOC_MEM_BLOCK (pu1Data) == NULL)
        {
            return MSR_FALSE;
        }

        MSR_MEMSET (pu1Data, 0, MSR_MAX_DATA_LEN);
        if ((u4Index >= MSR_RES_FILE_VER_FORMAT) && (u4Index != *pu4PrevIndex))
        {
            pTemp = pu1Data;
            *pu4PrevIndex = u4Index;
            MsrObtainTablePrefixOid (u4Index, au1PrefixOid);
            u2PrefLen = (UINT2) MSR_STRLEN (au1PrefixOid);
            SnmpRevertEoidString (au1PrefixOid);
            sprintf ((char *) pTemp, "@%4u\t%s\n", u4Index, au1PrefixOid);
            u4DataSize = MSR_STRLEN (pTemp);
            *pu4Length += u4DataSize;
            UtilCalcCheckSum (pTemp, u4DataSize, pu4Sum, pu2CkSum, CKSUM_DATA);
            if (gu1MibSaveLocal == MSR_TRUE)
            {
                /* save to FILE */
                i4RetVal =
                    (INT4) FileWrite (gi4SaveFd, (CHR1 *) pTemp, u4DataSize);
                if (i4RetVal < 0)
                {
                    FileClose (gi4SaveFd);
                    MSR_DATA_FREE_MEM_BLOCK (pu1Data);
                    return MSR_FALSE;
                }
            }
            else
            {
                if (gpu1StrCurrent + u4DataSize >= gpu1StrEnd)
                {
                    if (MsrSendDataToRemote (&ConnStatus) == MSR_FAILURE)
                    {
                        MSR_DATA_FREE_MEM_BLOCK (pu1Data);
                        return MSR_FALSE;
                    }
                }
                MSR_MEMCPY (gpu1StrCurrent, pu1Data, u4DataSize);
                gpu1StrCurrent += u4DataSize;
            }
        }
        else if ((u4Index == 0) || (u4Index == 1))
        {
            u2PrefLen = 0;
        }
        pTemp = pu1Data;
        pau1Oid += u2PrefLen;
        SnmpRevertEoidString (pau1Oid);
        if ((u4Index == 0) || (u4Index == 1))
        {
            ConnStatus = INITIATE;
            sprintf ((char *) pTemp, "%s\t%2d\t", pau1Oid, pData->i2_DataType);
        }
        else
        {
            sprintf ((char *) pTemp, "%u\t%s\t%2d\t",
                     u4MSRRowStatusType, pau1Oid, pData->i2_DataType);
        }
        pTemp += MSR_STRLEN (pTemp);
        if (pData->i2_DataType == OCTETSTRING)
        {
            sprintf ((char *) pTemp, "%4d\t",
                     (int) pData->pOctetStrValue->i4_Length);
            pTemp += MSR_STRLEN (pTemp);
        }
        u4DataSize = MSR_STRLEN (pu1Data);
        MsrConvertDataToString (pData, pTemp, (UINT2) pData->i2_DataType);
        if (pData->i2_DataType == OCTETSTRING)
        {
            pTemp += pData->pOctetStrValue->i4_Length;
            u4DataSize += (UINT4) pData->pOctetStrValue->i4_Length;
        }
        else
        {
            pTemp += MSR_STRLEN (pTemp);
            u4DataSize = MSR_STRLEN (pu1Data);
        }
        sprintf ((char *) pTemp, "\n");
        u4DataSize += 1;
        *pu4Length += u4DataSize;
        pTemp = pu1Data;
        UtilCalcCheckSum (pTemp, u4DataSize, pu4Sum, pu2CkSum, CKSUM_DATA);
        if (gu1MibSaveLocal == MSR_TRUE)
        {
            /* save to FILE */
            i4RetVal = (INT4) FileWrite (gi4SaveFd, (CHR1 *) pTemp, u4DataSize);
            if (i4RetVal < 0)
            {
                FileClose (gi4SaveFd);
                MSR_DATA_FREE_MEM_BLOCK (pu1Data);
                return MSR_FALSE;
            }

        }
        else
        {
            if (gpu1StrCurrent + u4DataSize >= gpu1StrEnd)
            {
                if (MsrSendDataToRemote (&ConnStatus) == MSR_FAILURE)
                {
                    MSR_DATA_FREE_MEM_BLOCK (pu1Data);
                    return MSR_FALSE;
                }
            }
            /* save to remot so update gpu1StrFlash */
            MSR_MEMCPY (gpu1StrCurrent, pu1Data, u4DataSize);
            gpu1StrCurrent += u4DataSize;
        }
        MSR_DATA_FREE_MEM_BLOCK (pu1Data);
    }
    else
    {
        /* store the data its not a efault val */
        i4Result = MSRSearchNodeinRBTree (pOid, u4Index,
                                          u4MSRRowStatusType, &pRBNodeCur);
        KW_FALSEPOSITIVE_FIX (pRBNodeCur);
        if (i4Result == MSR_TRUE)
        {
            /* data exists in the RB tree so Modify the 
             * data in the RBNode with new value. */
            i4Result = MSRModifyRBNodeinRBTree (pData, pRBNodeCur);
            if (i4Result != MSR_TRUE)
            {
                gu4MsrOidCount = 0;
                return MSR_FALSE;
            }
        }
        else
        {
            /* data not exist in the RBTree,
             * Add new node into 
             * RBTree and update Linklist */
            i4Result = MSRInsertRBNodeinRBTree (pOid,
                                                pData,
                                                u4Index,
                                                u4MSRRowStatusType,
                                                u1IsDataToBeSaved);
            if (i4Result != MSR_TRUE)
            {
                gu4MsrOidCount = 0;
                return MSR_FALSE;
            }
        }
    }
    return MSR_TRUE;
}

/************************************************************************
 *  Function Name   : MsrNotifyProtocolShutdown
 *  Description     : In the incremental save on mode, this function 
 *                     Sends an protocol shutdown event to the MSR process.
 *  Input           : pu1Oid       - Oids of the objects registered for 
 *                                    the protocol and the oid of the 
 *                                    system control object.
 *                    u2OidNo      - Number of Objects registered for the 
 *                                    protocol
 *  Output          : None                            
 *  Returns         : MSR_SUCCESS / MSR_FAILURE
 ************************************************************************/
INT1
MsrNotifyProtocolShutdown (UINT1 pu1Oid[][SNMP_MAX_OID_LENGTH], UINT2 u2OidNo,
                           INT4 i4ContextId)
{
    tMsrConfigData     *pMsrConfigData = NULL;
    tMsrOidList        *pMsrOidList = NULL;
    UINT1               u1Index = 0;

    if (MsrisRetoreinProgress == ISS_TRUE)
    {
        /* The configurations are restored now. No need to process
         * the shutdown event as none of the protocol configurations
         * are present in the conf file */
        return MSR_SUCCESS;
    }

    if (gMSRIncrSaveFlag == ISS_FALSE)
    {
        /* In the incremental save off mode, the current configurations
         * are not saved by the MSR. Hence do not process this event */
        return MSR_SUCCESS;
    }

    /* Allocate memory for the oidlist and send a
     * protocol shutdown event to MSR */
    if (u2OidNo > MAX_OID_NO)
    {
        MSR_TRC (MSR_TRACE_TYPE, "MsrNotifyProtocolShutdown: "
                 "Memory allocation failure for oid list !!!\r\n");
        return MSR_FAILURE;
    }
    pMsrOidList = MemAllocMemBlk (gMsrOidListPoolId);
    if (pMsrOidList == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "MsrNotifyProtocolShutdown: "
                 "OID memory allocation failed !!!\r\n");
        return MSR_FAILURE;
    }
    MSR_MEMSET (pMsrOidList, 0, sizeof (tMsrOidList));
    pMsrOidList->pOidArray = MemAllocMemBlk (gMsrOidArrPoolId);
    if (pMsrOidList->pOidArray == NULL)
    {
        MsrFreeOidList (pMsrOidList, u1Index);
        return MSR_FAILURE;
    }
    MSR_MEMSET (pMsrOidList->pOidArray, 0, (MAX_OID_NO * sizeof (UINT1 *)));

    pMsrOidList->u2OidNo = u2OidNo;
    pMsrOidList->i4ContextId = i4ContextId;

    for (u1Index = 0; u1Index < u2OidNo; u1Index++)
    {
        pMsrOidList->pOidArray[u1Index] = MemAllocMemBlk (gMsrOidArrListPoolId);

        if (pMsrOidList->pOidArray[u1Index] == NULL)
        {
            MsrFreeOidList (pMsrOidList, u1Index);
            return MSR_FAILURE;
        }

        MSR_MEMSET (pMsrOidList->pOidArray[u1Index], 0, SNMP_MAX_OID_LENGTH);

        MEMCPY (pMsrOidList->pOidArray[u1Index], pu1Oid[u1Index],
                SNMP_MAX_OID_LENGTH);
    }

    u1Index = 0;
    /* Fill the MsrConfigData structure */
    pMsrConfigData = MemAllocMemBlk (gMsrQConDataPoolId);
    if (NULL == pMsrConfigData)
    {
        MsrFreeOidList (pMsrOidList, u1Index);
        return MSR_FAILURE;
    }
    MSR_MEMSET (pMsrConfigData, 0, sizeof (tMsrConfigData));
    pMsrConfigData->u2MsgType = MSR_PROTO_SHUT;
    pMsrConfigData->MsrCfgData.pMsrOidList = pMsrOidList;

    MsrUpdateQCountLock ();
    gu4noMsrUpdateQReq++;
    if (gu4noMsrUpdateQReq >= MSR_MAX_UPDATE_QUEUE_SIZE)
    {
        MsrUpdateQCountUnLock ();
        /* get the Q status lock on the reaching the maximum
         * messages of the Q, this lock is released by MSR on
         * processing the updated event */
        MsrUpdateQStatusLock ();

    }
    else
    {
        MsrUpdateQCountUnLock ();
    }

    if (OsixQueSend (MsrsnmpQId, (UINT1 *) &pMsrConfigData, OSIX_DEF_MSG_LEN)
        == OSIX_FAILURE)
    {
        MsrFreeOidList (pMsrOidList, u1Index);
        MemReleaseMemBlock (gMsrQConDataPoolId, (UINT1 *) pMsrConfigData);
        return MSR_FAILURE;
    }

    /* msr debug stats counter */
    gU4NoOfQMsgSend++;

    OsixEvtSend (MsrId, MSR_UPDATE_EVENT);

    return MSR_SUCCESS;
}

/************************************************************************
 *  Function Name   : MsrFreeOidList
 *  Description     : This function frees the oid list that is allocated
 *                     to send protocol shutdown messages to the MSR process.
 *  Input           : pMsrOidList  - Oid List to be freed.
 *                    u2OidNo      - Number of Objects for which memory is 
 *                                    allocated in the Oid List.
 *  Output          : None                            
 *  Returns         : None
 ************************************************************************/
VOID
MsrFreeOidList (tMsrOidList * pMsrOidList, UINT2 u2OidNo)
{
    UINT1               u1Cntr = 0;

    for (u1Cntr = 0; u1Cntr < u2OidNo; u1Cntr++)
    {
        if (pMsrOidList->pOidArray[u1Cntr] != NULL)
        {
            MemReleaseMemBlock (gMsrOidArrListPoolId, (UINT1 *)
                                (pMsrOidList->pOidArray[u1Cntr]));
        }
    }
    MemReleaseMemBlock (gMsrOidArrPoolId, (UINT1 *) (pMsrOidList->pOidArray));
    MemReleaseMemBlock (gMsrOidListPoolId, (UINT1 *) (pMsrOidList));
    return;
}

/*****************************************************************************/
/* Function     : MsrDeleteRbNodes                                           */
/*                                                                           */
/* Description  : This function removes the nodes from the RBtree whose      */
/*                 configurations have been removed from the protocol        */
/*                 database when the module is shutdown.                     */
/*                                                                           */
/* Input        : pMsrOidList - The Oid list whose child oids has to be      */
/*                               removed from the RBtree.                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
MsrDeleteRbNodes (tMsrOidList * pMsrOidList)
{
    tMSRRbtree         *pRBNode = NULL;
    tMSRRbtree         *pRBNextNode = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pInstance = NULL;
    UINT4               u4PrevMsrArrayIndex = 0;
    UINT2               u2Index = 0;
    UINT2               u2Oids = 0;
    UINT1               au1Oid[MSR_MAX_OID_LEN];
    UINT1               au1EntryOid[MSR_MAX_OID_LEN];
    tPortList          *pPortList = NULL;
    BOOL1               bResult = OSIX_FALSE;

    pRBNode = (tMSRRbtree *) RBTreeGetFirst (gMSRRBTable);

    if ((pInstance = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return;
    }

    if ((pOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return;
    }

    u2Oids = (UINT2) (pMsrOidList->u2OidNo - 1);
    MSR_STRCAT (pMsrOidList->pOidArray[u2Oids], ".");

    /* If the a protocol is shutdown for any given context, get
     * the list of ports mapped for that context */

    pPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPortList == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Error in Allocating memory for bitlist\n");
        return;
    }

    MSR_MEMSET (*pPortList, 0, sizeof (tPortList));

    if (pMsrOidList->i4ContextId > MSR_INVALID_CNTXT)
    {
        MsrGetContextPortList ((UINT4) pMsrOidList->i4ContextId, *pPortList);
    }

    /* Scan each node in the RBtree. If the oid of the obtained node 
     * has the prefix as the oids in the given list remove the node 
     * from the RBTree */

    while (pRBNode != NULL)
    {
        MSR_MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);

        WebnmConvertOidToString (pRBNode->pOidValue, au1Oid);

        pRBNextNode = (tMSRRbtree *) RBTreeGetNext (gMSRRBTable, pRBNode, NULL);

        /* Compare the node oid with each oid in the list */
        for (u2Index = 0; u2Index < u2Oids; u2Index++)
        {
            if (STRNCMP (au1Oid, pMsrOidList->pOidArray[u2Index],
                         STRLEN (pMsrOidList->pOidArray[u2Index])) == 0)
            {
                /* if incoming context < 0 do the following */
                /* else get ports mapped to the incoming context 
                 * (there will not be any scalar objects with mi mode)
                 * so all objects are tabular objects. Hence, obtain the 
                 * first index of the node. if context based table directly
                 * compare it with the incoming context. else compare with 
                 * the port array. if port present remove the node. else donot
                 * remove the node */

                if (pMsrOidList->i4ContextId > MSR_INVALID_CNTXT)
                {
                    if (pRBNode->MSRArrayIndex != u4PrevMsrArrayIndex)
                    {
                        MSR_MEMSET (au1EntryOid, 0, MSR_MAX_OID_LEN);
                        STRNCPY
                            (au1EntryOid,
                             gaSaveArray[pRBNode->MSRArrayIndex].pOidString,
                             (sizeof (au1EntryOid) - 1));
                        au1EntryOid[(sizeof (au1EntryOid) - 1)] = '\0';
                        MSR_CLEAR_OID (pOid);
                        WebnmConvertStringToOid (pOid, au1EntryOid, MSR_FALSE);
                        u4PrevMsrArrayIndex = pRBNode->MSRArrayIndex;
                    }

                    MSR_CLEAR_OID (pInstance);
                    WebnmGetInstanceOid (pOid, pRBNode->pOidValue, pInstance);

                    /* if context based directly remove it, else if port based
                     * verify portlist else break if the conditions fail
                     * also break */
                    if (gaSaveArray[pRBNode->MSRArrayIndex].u2TableIndexType
                        == 1)
                    {
                        if (pInstance->pu4_OidList[1] !=
                            (UINT4) pMsrOidList->i4ContextId)
                        {
                            /* The incoming node is a child object of the mib
                             * present in u2Index position. As the object is 
                             * configured for some other object, move to the 
                             * next node in the rbtree */
                            break;
                        }
                    }
                    else if (gaSaveArray[pRBNode->MSRArrayIndex].
                             u2TableIndexType == 2)
                    {
                        OSIX_BITLIST_IS_BIT_SET ((*pPortList),
                                                 pInstance->pu4_OidList[1],
                                                 BRG_PORT_LIST_SIZE, bResult);
                        if (bResult == OSIX_FALSE)
                        {
                            break;
                        }
                    }
                    else
                    {
                        break;
                    }
                }

                /* Remove all the nodes in the RBTree which were added
                 * for configuring the protocol except the SystemControl 
                 * object whose oid is added at the u2Oids position */
                if (STRNCMP (au1Oid, pMsrOidList->pOidArray[u2Oids],
                             STRLEN (pMsrOidList->pOidArray[u2Oids])) != 0)
                {
                    RBTreeRem (gMSRRBTable, pRBNode);
                    MSRFreeRBTreeNode (pRBNode, 1);
                    break;
                }
            }
        }
        pRBNode = pRBNextNode;
    }

    if (gIssConfigSaveInfo.IssAutoConfigSave == ISS_TRUE)
    {
        if ((gIssConfigSaveInfo.IssConfigSaveOption == ISS_CONFIG_FLASH_SAVE) ||
            (gIssConfigSaveInfo.IssConfigSaveOption == ISS_CONFIG_STARTUP_SAVE))
        {
            if (gi4MibSaveStatus != MIB_SAVE_IN_PROGRESS)
            {
                MsrSaveDatatoIncrFile (NULL, MSR_INCREMENTAL_SAVE_BUFF_SIZE);
            }
        }
        else if (gIssConfigSaveInfo.IssConfigSaveOption ==
                 ISS_CONFIG_REMOTE_SAVE)
        {
            if (gi4RemoteSaveStatus != MIB_SAVE_IN_PROGRESS)
            {
                MsrSaveDatatoIncrBuffer (NULL, MSR_INCREMENTAL_SAVE_BUFF_SIZE);
            }
        }
    }

    gu4MsrOidCount = 0;
    FsUtilReleaseBitList ((UINT1 *) pPortList);
    return;
}

/*****************************************************************************/
/* Function     : MsrUpdateConfiguration                                     */
/*                                                                           */
/* Description  : This function in the auto save modes, saves the deleted    */
/*                 configurations in the incremental conf file.              */
/*                                                                           */
/* Input        : pRBNode - The RbNode containing the deleted configurations */
/*                pOid    - The oid of the deleted configuraiton.            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
MsrUpdateConfiguration (tMSRRbtree * pRBNode, UINT1 *pOid)
{
    UINT2               u2Len = 0;
    UINT1              *pu1Data = NULL;
    UINT1              *pRemData = NULL;

    if (gIssConfigSaveInfo.IssAutoConfigSave == ISS_FALSE)
    {
        return;
    }

    if (MSR_DATA_ALLOC_MEM_BLOCK (pu1Data) == NULL)
    {
        return;
    }
    MSR_MEMSET (pu1Data, 0, MSR_MAX_DATA_LEN);
    pRemData = pu1Data;
    sprintf ((char *) pRemData, "%d\t%4u\t%u\t%s\t",
             MSR_DELETE, pRBNode->MSRArrayIndex,
             pRBNode->MSRRowStatusType, pOid);
    pRemData += MSR_STRLEN (pRemData);

    sprintf ((char *) pRemData, "%s\n", pRBNode->pData);
    u2Len = (UINT2) MSR_STRLEN (pu1Data);

    if ((gIssConfigSaveInfo.IssConfigSaveOption == ISS_CONFIG_FLASH_SAVE) ||
        (gIssConfigSaveInfo.IssConfigSaveOption == ISS_CONFIG_STARTUP_SAVE))
    {
        if (gi4MibSaveStatus != MIB_SAVE_IN_PROGRESS)
        {
            MsrSaveDatatoIncrFile (pu1Data, u2Len);
        }

    }
    else if (gIssConfigSaveInfo.IssConfigSaveOption == ISS_CONFIG_REMOTE_SAVE)
    {
        if (gi4RemoteSaveStatus != MIB_SAVE_IN_PROGRESS)
        {
            MsrSaveDatatoIncrBuffer (pu1Data, u2Len);
        }
    }
    MSR_DATA_FREE_MEM_BLOCK (pu1Data);
}

/*****************************************************************************/
/* Function     : MsrDeleteMsrResources                                      */
/*                                                                           */
/* Description  : This function deletes all the semaphores and queues        */
/*                 allocated for the MSR process on event of a failure.      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
MsrDeleteMsrResources (VOID)
{
    /* Delete the MsrSem */
    if (MsrSemId != 0)
    {
        OsixSemDel (MsrSemId);
        MsrSemId = 0;
    }

    /* Delete the TftpSem */
    if (TftSemId != 0)
    {
        OsixSemDel (TftSemId);
        TftSemId = 0;
    }

    /* Delete the MsrUpdateQsem */
    if (MsrUpdateQsemId != 0)
    {
        OsixSemDel (MsrUpdateQsemId);
        MsrUpdateQsemId = 0;
    }

    /* Delete the MsrUpdateQSsem */
    if (MsrUpdateQSsemId != 0)
    {
        OsixSemDel (MsrUpdateQSsemId);
        MsrUpdateQSsemId = 0;
    }

    /* Delete the Msr Update Queue */
    if (MsrsnmpQId != 0)
    {
        OsixQueDel (MsrsnmpQId);
        MsrsnmpQId = 0;
    }

    /* Delete the Msr Queue allocate to process Http events */
    if (MsrHttpQId != 0)
    {
        OsixQueDel (MsrHttpQId);
        MsrHttpQId = 0;
    }

    /* Delete the timer list allocated for notifing the default interface up */
    if (gMsrDefSysIfUpTmrId != 0)
    {
        if (TmrDeleteTimerList (gMsrDefSysIfUpTmrId) != TMR_SUCCESS)
        {

            MSR_TRC (MSR_TRACE_TYPE,
                     "Err in OsixTskIdSelf - DEF_IFUP_TMR release fail.\n");
        }
        gMsrDefSysIfUpTmrId = 0;
    }

    /* Free the memory allocated for the flash array */
    if (gpu1StrFlash != NULL)
    {
        MSR_CONFIGENTRY_FREE_MEM_BLOCK (gpu1StrFlash);
        gpu1StrFlash = NULL;
    }

    MsrSizingMemDeleteMemPools ();

    /* Delete the RBTree created to save the incremental configurations */
    if (gMSRRBTable != NULL)
    {
        RBTreeDestroy (gMSRRBTable, (tRBKeyFreeFn) MSRFreeRBTreeNode, 0);
        gMSRRBTable = NULL;
    }

    return;
}

/*****************************************************************************/
/* Function     : MsrVerifyRestFileChecksum                                  */
/*                                                                           */
/* Description  : This function reads the configuration file and calculates  */
/*                 the checksum for the entries present in the conf file.    */
/*                 If the calculated checksum is equivalent to the checksum  */
/*                 present in the conf file, return success else failure is  */
/*                 returned.                                                 */
/*                                                                           */
/* Input        : u4Index      - The index position the gaSaveArray          */
/*                                                                           */
/* Output       : pu1PrefixOid - The prefix oid for the given index.         */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
INT4
MsrVerifyRestFileChecksum (INT4 i4ConfFd)
{
    UINT4               u4Length = 0;
    UINT1              *pChar = NULL;
    UINT1              *pu1Cfg = NULL;
    UINT1               au1Data[MSR_MAX_STR_LEN];
    UINT1               pu1CheckSum[FLASH_CKSUM_LEN];
    UINT4               u4Type = CKSUM_DATA;
    UINT4               u4Sum = 0;
    UINT2               u2ChkSum = 0;
    UINT1               u1Tmp = 0;
    UINT4               u4TotLength = 0;
    UINT1               u1DataCntr = 0;
    UINT2               u2CfgCntr = 0;
    INT4                i4DataLen = 0;

    pChar = au1Data;
    MSR_MEMSET (pChar, 0, MSR_MAX_STR_LEN);
    u4Length = MSR_STRLEN ("CKSUM:");
    if (MSR_DATA_ALLOC_MEM_BLOCK (pu1Cfg) == NULL)
    {
        return ISS_FAILURE;
    }
    MSR_MEMSET (pu1Cfg, 0, MSR_MAX_DATA_LEN);

    while (((i4DataLen = FileRead (i4ConfFd, (CHR1 *) au1Data,
                                   MSR_MAX_FILE_READ_LEN)) > 0)
           && (i4DataLen != (INT4) MSR_LEN_MAX))
    {
        while ((INT4) u1DataCntr < i4DataLen)
        {
            pu1Cfg[u2CfgCntr] = au1Data[u1DataCntr++];
            if (pu1Cfg[u2CfgCntr++] != '\n')
            {
                continue;
            }
            if (STRNCMP (pu1Cfg, "CKSUM:", u4Length) != 0)
            {
                u4TotLength += u2CfgCntr;
                UtilCalcCheckSum (pu1Cfg, (UINT4) u2CfgCntr, &u4Sum,
                                  &u2ChkSum, u4Type);
                u2CfgCntr = 0;
                MSR_MEMSET (pu1Cfg, 0, MSR_MAX_DATA_LEN);
            }
        }
        u1DataCntr = 0;
        MSR_MEMSET (au1Data, 0, MSR_MAX_STR_LEN);
    }

    u4TotLength += u2CfgCntr;

    if (STRNCMP (pu1Cfg, "CKSUM:", u4Length) != 0)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Invalid Configuration file !!!\n");
    }

    MSR_MEMCPY (pu1CheckSum, (pu1Cfg + u4Length), FLASH_CKSUM_LEN);
    MEMCPY (&gu2FlashCksum, pu1CheckSum, sizeof (UINT2));
    MSR_MEMSET ((pu1Cfg + u4Length), 0, FLASH_CKSUM_LEN);

    u4Type = CKSUM_LASTDATA;

    if ((u4TotLength % 2) == 1)
    {
        MEMCPY ((pu1Cfg + u2CfgCntr), &u1Tmp, sizeof (UINT1));
        u2CfgCntr++;
    }
    UtilCalcCheckSum (pu1Cfg, u2CfgCntr, &u4Sum, &u2ChkSum, u4Type);
    MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);

    if (u2ChkSum != gu2FlashCksum)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Configuration File Corrupted!!!\n");
        gi4MibResFailureReason = MIB_RES_CHKSUM_FAILURE;

        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        FileClose (i4ConfFd);
        return ISS_FAILURE;
    }
    FileClose (i4ConfFd);
    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function     : MsrObtainFilePointer                                       */
/*                                                                           */
/* Description  : This function verifies if the restoration file is present, */
/*                 opens the file for reading and provides the file          */
/*                 descriptor.                                               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : fp - File Pointer for the restoration file or NUll.        */
/*                                                                           */
/* Returns      : ISS_SUCCESS / ISS_FAILURE                                  */
/*****************************************************************************/
INT4
MsrObtainFilePointer (INT4 *pi4FileFd)
{
    UINT1              *pTemp = NULL;
    CHR1                au1RestoreFileName[MSR_MAX_FLASH_FILE_LEN];

    pTemp = (UINT1 *) IssGetRestoreFileNameFromNvRam ();

    MSR_MEMSET (au1RestoreFileName, 0, sizeof (au1RestoreFileName));
    if (gMsrSavResPriority == EXTERN_STORAGE)
    {
        MSR_STRCPY (au1RestoreFileName, MSR_SYS_EXTN_STRG_PATH);
    }
    else
    {
        MSR_STRCPY (au1RestoreFileName, FLASH);
    }
    MSR_STRNCAT (au1RestoreFileName, pTemp,
                 (sizeof (au1RestoreFileName) -
                  STRLEN (au1RestoreFileName) - 1));

    if (((*pi4FileFd) =
         FileOpen ((UINT1 *) au1RestoreFileName, OSIX_FILE_RO)) < 0)
    {
        if (gMsrSavResPriority == EXTERN_STORAGE)
        {
            MSR_TRC (MSR_TRACE_TYPE,
                     "No Configuration in External Storage ... \n");
        }
        else
        {
            MSR_TRC (MSR_TRACE_TYPE, "No Configuration in Flash ... \n");
        }
        gi4MibResFailureReason = MIB_RES_FILE_OPEN_FAILURE;
        return ISS_FAILURE;
    }

    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function     : MsrObtainAltResFilefd                                      */
/*                                                                           */
/* Description  : This function verifies if the alternate restoration file   */
/*                is present,                                                */
/*                 opens the file for reading and provides the file descriptor */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : fp - File Pointer for the restoration file or NUll.        */
/*                                                                           */
/* Returns      : ISS_SUCCESS / ISS_FAILURE                                  */
/*****************************************************************************/
INT4
MsrObtainAltResFilefd (INT4 *pi4FileFd)
{
    CHR1                au1RestoreFileName[MSR_MAX_FLASH_FILE_LEN];

    MSR_MEMSET (au1RestoreFileName, 0, sizeof (au1RestoreFileName));
    if (gMsrSavResPriority == EXTERN_STORAGE)
    {
        MSR_STRCPY (au1RestoreFileName, MSR_SYS_EXTN_STRG_PATH);
    }
    else
    {
        MSR_STRCPY (au1RestoreFileName, FLASH);
    }
    MSR_STRCAT (au1RestoreFileName, ISS_CONFIG_FILE2);

    if (((*pi4FileFd) =
         FileOpen ((UINT1 *) au1RestoreFileName, OSIX_FILE_RO)) < 0)
    {
        if (gMsrSavResPriority == EXTERN_STORAGE)
        {
            MSR_TRC (MSR_TRACE_TYPE,
                     "No alternate Configuration in External Storage ... \n");
        }
        else
        {
            MSR_TRC (MSR_TRACE_TYPE,
                     "No alternate configuration in Flash ... \n");
        }
        gi4MibResFailureReason = MIB_RES_FILE_OPEN_FAILURE;
        return ISS_FAILURE;
    }

    return ISS_SUCCESS;
}

/*****************************************************************************/
/* Function     : MsrSendDataToRemote                                        */
/*                                                                           */
/* Description  : This function verifies if the restoration file is present, */
/*                 opens the file for reading and provides the file          */
/*                 descriptor.                                               */
/*                                                                           */
/* Input        : ConnStatus - INITIATE / SEND_DATA /              */
/*                               TERMINATE                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MSR_SUCCESS / MSR_FAILURE                                  */
/*****************************************************************************/
INT4
MsrSendDataToRemote (tConnStatus * ConnStatus)
{
    INT4                i4RetErr = 0;
#ifdef SSH_WANTED
    CHR1                i1SftpError[MSG_BUF_SIZE];

    MEMSET (&i1SftpError, '\0', MSG_BUF_SIZE);
#endif
    MSR_TRC (MSR_TRACE_TYPE, "Sending the data to " "the remote host\n");
    gu4Length = (UINT4) (gpu1StrCurrent - gpu1StrFlash);

    if (gMsrData.ServerAddr.u1AddrLen == 0)
    {
        MEMSET (gMsrData.au1FileName, 0, sizeof (gMsrData.au1FileName));
        MEMCPY (&gMsrData.ServerAddr, &gIssConfigSaveInfo.IssConfigSaveIpAddr,
                sizeof (tIPvXAddr));

        ISS_STRNCPY (gMsrData.au1FileName,
                     gIssConfigSaveInfo.au1IssConfigSaveFileName,
                     (sizeof (gMsrData.au1FileName) - 1));
    }

    if (gMsrData.u4TransMode == ISS_TFTP_TRANSFER_MODE)
    {
        if ((i4RetErr = tftpcSendMultipleBuffers (gMsrData.ServerAddr,
                                                  gMsrData.au1FileName,
                                                  gpu1StrFlash, gu4Length,
                                                  *ConnStatus)) != TFTPC_OK)
        {
            MSR_TRC (MSR_TRACE_TYPE,
                     "Saving configuration to Remote host failed !!!\n");
            gu4MsrOidCount = 0;
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                          "Saving configuration to remote failed [TFTP:%s]!",
                          TFTPC_ERR_MSG (i4RetErr)));
            return MSR_FAILURE;
        }
    }
#ifdef SSH_WANTED
    else
    {
        if ((i4RetErr = sftpcArSendMultipleBuffers (gMsrData.au1UserName,
                                                    gMsrData.au1Password,
                                                    gMsrData.ServerAddr,
                                                    gMsrData.au1FileName,
                                                    gpu1StrFlash, gu4Length,
                                                    *ConnStatus)) !=
            SFTP_SUCCESS)
        {
            MSR_TRC (MSR_TRACE_TYPE,
                     "Saving configuration to Remote host failed !!!\n");
            gu4MsrOidCount = 0;
            SftpcArErrString (i1SftpError);
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                          "Saving configuration to remote failed [SFTP:%s]!",
                          i1SftpError));
            return MSR_FAILURE;
        }
    }
#endif
    MSR_MEMSET (gpu1StrFlash, 0, gu4MibEntryBlkSize);
    gpu1StrCurrent = gpu1StrFlash;

    /* set the ConnStatus to SEND_DATA */
    *ConnStatus = SEND_DATA;
    return MSR_SUCCESS;
}

/*****************************************************************************/
/* Function     : MsrObtainTablePrefixOid                                    */
/*                                                                           */
/* Description  : This function returns the table entry oid as the prefix    */
/*                 oid for the tabular entries present in the gaSaveArray.   */
/*                 For scalar entries and tabular entries whose OidString    */
/*                 differs from its rowstatus oid, null string is returned.  */
/*                                                                           */
/* Input        : u4Index      - The index position the gaSaveArray          */
/*                                                                           */
/* Output       : pu1PrefixOid - The prefix oid for the given index.         */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
MsrObtainTablePrefixOid (UINT4 u4Index, UINT1 *pu1PrefixOid)
{
    MSR_MEMSET (pu1PrefixOid, 0, MSR_MAX_OID_LEN);

    if (gaSaveArray[u4Index].u2Type == MSR_TABLE)
    {
        /* Verify if the rowstatus object exists if yes,
         * get the entry oid in the prefix string */
        if ((gaSaveArray[u4Index].pRowStatus == NULL) ||
            (STRNCMP (gaSaveArray[u4Index].pOidString,
                      gaSaveArray[u4Index].pRowStatus,
                      MSR_STRLEN (gaSaveArray[u4Index].pOidString)) == 0))

        {
            MSR_MEMCPY (pu1PrefixOid, gaSaveArray[u4Index].pOidString,
                        MSR_STRLEN (gaSaveArray[u4Index].pOidString));
        }
    }
}

/*****************************************************************************/
/* Function     : MsrAllocateIncrPools                                       */
/*                                                                           */
/*  Description : Allocates the Mem pools and buddy pools required for the   */
/*                 incremental Rbtree nodes                                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MSR_SUCCESS/MSR_FAILURE                                    */
/*****************************************************************************/
INT4
MsrAllocateIncrPools ()
{
    /* Create a buddy mempool to allocate memory for the data
     * saved in the RBtree nodes */
    MSR_INCR_DATA_POOL_ID = MSR_CREATE_INCR_BUDDY_POOL (MSR_MAX_DATA_BLK_SIZE,
                                                        MSR_MIN_DATA_BLK_SIZE,
                                                        MSR_MAX_DATA_BLKS);
    if (MSR_INCR_DATA_POOL_ID == BUDDY_FAILURE)
    {
        MSR_TRC (MSR_TRACE_TYPE,
                 "MSR buddy pool creation for saving data in RBTree nodes failed!!!\n");
        return MSR_FAILURE;
    }

    /* Create a buddy mempool to allocate memory for the oid list
     * saved in the RBtree nodes */
    MSR_INCR_OIDLIST_POOL_ID = MSR_CREATE_INCR_BUDDY_POOL (MSR_MAX_OID_BLK_SIZE,
                                                           MSR_MIN_OID_BLK_SIZE,
                                                           MSR_MAX_OID_BLKS);
    if (MSR_INCR_OIDLIST_POOL_ID == BUDDY_FAILURE)
    {
        MSR_TRC (MSR_TRACE_TYPE,
                 "MSR buddy pool creation for saving oids in RBTree nodes failed!!!\n");
        return MSR_FAILURE;
    }

    return MSR_SUCCESS;
}

/************************************************************************
 *  Function Name   : MsrProcessUpdateEvent
 *  Description     : This function process the update configuration request
 *  Input(s)        : None                                             
 *  Output(s)       : None.                                            
 *  Returns         : None. 
 *  
 ************************************************************************/
VOID
MsrProcessUpdateEvent (VOID)
{
    tMsrConfigData     *pMsrConfigData = NULL;

    if (OsixQueRecv (MsrsnmpQId, (UINT1 *) &pMsrConfigData,
                     OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (pMsrConfigData->u2MsgType == MSR_CONFIG_SAVE)
        {
            gU4NoOfUpdateEvents++;
            MsrProcesssnmpEvt (pMsrConfigData->MsrCfgData.pMsrUpdateData);
            /* processing update request complete
             * free the data read from Q and release lock if taken */
            MSRFreeSnmpDataRcvfromQ (pMsrConfigData->MsrCfgData.pMsrUpdateData);

        }
        else if (pMsrConfigData->u2MsgType == MSR_PROTO_SHUT)
        {
            MsrDeleteRbNodes (pMsrConfigData->MsrCfgData.pMsrOidList);
            MsrFreeOidList (pMsrConfigData->MsrCfgData.pMsrOidList,
                            pMsrConfigData->MsrCfgData.pMsrOidList->u2OidNo);
        }
        else if (pMsrConfigData->u2MsgType == MSR_CONTEXT_DELETE)
        {
            MsrHandleContextDeletion (pMsrConfigData->MsrCfgData.u4Index);
        }
        else if (pMsrConfigData->u2MsgType == MSR_PORT_DELETE)
        {
            MsrProcessSyncSaveEvent (MSR_PORT_DELETE);
        }
        else if (pMsrConfigData->u2MsgType == MSR_BRIDGE_MODE_CHANGE)
        {
            MsrProcessSyncSaveEvent (MSR_BRIDGE_MODE_CHANGE);
        }
        else if (pMsrConfigData->u2MsgType == MSR_VLAN_DELETE)
        {
            MsrProcessSyncSaveEvent (MSR_VLAN_DELETE);
        }
        else if (pMsrConfigData->u2MsgType == MSR_PB_PORT_TYPE_CHANGE)
        {
            MsrProcessSyncSaveEvent (MSR_PB_PORT_TYPE_CHANGE);
        }
        else if (pMsrConfigData->u2MsgType == MSR_STP_PORT_DELETE)
        {
            MsrHandleStpPortDelete (pMsrConfigData->MsrCfgData.u4Index);
        }

        MemReleaseMemBlock (gMsrQConDataPoolId, (UINT1 *) (pMsrConfigData));
        MsrReleaseUpdateQLock ();

        /* If some more notification messages are present in the SNMPQ send an
         * UPDATE event to MSR to process them */
        MsrUpdateQCountLock ();
        if (gu4noMsrUpdateQReq)
        {
            OsixEvtSend (MsrId, MSR_UPDATE_EVENT);
        }
        MsrUpdateQCountUnLock ();

    }
}

/************************************************************************
 *  Function Name   : MsrNotifyUpdateEvent
 *  Description     : In the incremental save on mode, this function 
 *                     sends the context deletion event or port deletion
 *                     event to the MSR process.
 *  Input           : u2EventType  - The event type that has to be sent 
 *                                    to the MSR task.
 *                    u2Value      - The context id in case of context deletion
 *                                    or the Interface index in case of port
 *                                    deletion
 *  Output          : None                            
 *  Returns         : MSR_SUCCESS / MSR_FAILURE
 ************************************************************************/
INT1
MsrNotifyUpdateEvent (UINT2 u2EventType, UINT4 u4Value)
{
    tMsrConfigData     *pMsrConfigData = NULL;

    if (MsrisRetoreinProgress == ISS_TRUE)
    {
        /* The configurations are restored now. No need to process
         * the shutdown event as none of the protocol configurations
         * are present in the conf file */
        return MSR_SUCCESS;
    }

    if (gMSRIncrSaveFlag == ISS_FALSE)
    {
        /* In the incremental save off mode, the current configurations
         * are not saved by the MSR. Hence do not process this event */
        return MSR_SUCCESS;
    }

    if (MsrGetSaveStatus () == ISS_TRUE)
    {
        /* when MSR is ongoing, no need to notify to MSR */
        return MSR_SUCCESS;
    }

    /* Fill the MsrConfigData structure */
    pMsrConfigData = MemAllocMemBlk (gMsrQConDataPoolId);
    if (NULL == pMsrConfigData)
    {
        return MSR_FAILURE;
    }
    MSR_MEMSET (pMsrConfigData, 0, sizeof (tMsrConfigData));
    pMsrConfigData->u2MsgType = u2EventType;
    pMsrConfigData->MsrCfgData.u4Index = u4Value;

    MsrUpdateQCountLock ();
    gu4noMsrUpdateQReq++;
    if (gu4noMsrUpdateQReq >= MSR_MAX_UPDATE_QUEUE_SIZE)
    {
        MsrUpdateQCountUnLock ();
        /* get the Q status lock on the reaching the maximum
         * messages of the Q, this lock is released by MSR on
         * processing the updated event */
        MsrUpdateQStatusLock ();

    }
    else
    {
        MsrUpdateQCountUnLock ();
    }

    if (OsixQueSend (MsrsnmpQId, (UINT1 *) &pMsrConfigData, OSIX_DEF_MSG_LEN)
        == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gMsrQConDataPoolId, (UINT1 *) pMsrConfigData);
        return MSR_FAILURE;
    }

    /* msr debug stats counter */
    gU4NoOfQMsgSend++;

    OsixEvtSend (MsrId, MSR_UPDATE_EVENT);

    return MSR_SUCCESS;
}

/************************************************************************
 *  Function Name   : MsrHandleContextDeletion
 *  Description     : This function scans the MSR rbtree and removes all
 *                     configurations saved for the given context.
 *  Input           : u4Index   - The Context Id for which the
 *                                 configurations have to be deleted.
 *  Output          : None                            
 *  Returns         : MSR_SUCCESS / MSR_FAILURE
 ************************************************************************/
VOID
MsrHandleContextDeletion (UINT4 u4Index)
{
    tMSRRbtree          RBNode;
    tMSRRbtree         *pRBNode = NULL;
    tMSRRbtree         *pRBNextNode = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pInstance = NULL;
    UINT4               u4PrevMsrArrayIndex = 0;
    UINT1               au1EntryOid[MSR_MAX_OID_LEN];

    if ((pInstance = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return;
    }

    if ((pOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return;
    }

    MSR_MEMSET (&RBNode, 0, sizeof (tMSRRbtree));
    RBNode.MSRArrayIndex = 2;
    pRBNode = (tMSRRbtree *) RBTreeGetNext (gMSRRBTable, &RBNode, NULL);

    while (pRBNode != NULL)
    {
        if (gaSaveArray[pRBNode->MSRArrayIndex].u2TableIndexType != 1)
        {
            RBNode.MSRArrayIndex = pRBNode->MSRArrayIndex + 1;
            pRBNode = (tMSRRbtree *) RBTreeGetNext (gMSRRBTable, &RBNode, NULL);
            continue;
        }

        if (pRBNode->MSRArrayIndex != u4PrevMsrArrayIndex)
        {
            MSR_MEMSET (au1EntryOid, 0, MSR_MAX_OID_LEN);
            STRNCPY
                (au1EntryOid, gaSaveArray[pRBNode->MSRArrayIndex].pOidString,
                 (sizeof (au1EntryOid) - 1));
            au1EntryOid[(sizeof (au1EntryOid) - 1)] = '\0';
            MSR_CLEAR_OID (pOid);
            WebnmConvertStringToOid (pOid, au1EntryOid, MSR_FALSE);
            u4PrevMsrArrayIndex = pRBNode->MSRArrayIndex;
        }

        MSR_CLEAR_OID (pInstance);
        WebnmGetInstanceOid (pOid, pRBNode->pOidValue, pInstance);

        pRBNextNode = (tMSRRbtree *) RBTreeGetNext (gMSRRBTable, pRBNode, NULL);
        if (pInstance->pu4_OidList[1] == u4Index)
        {
            RBTreeRem (gMSRRBTable, pRBNode);
            MSRFreeRBTreeNode (pRBNode, 1);
        }

        pRBNode = pRBNextNode;
    }

    if (gIssConfigSaveInfo.IssAutoConfigSave == ISS_TRUE)
    {
        if ((gIssConfigSaveInfo.IssConfigSaveOption == ISS_CONFIG_FLASH_SAVE) ||
            (gIssConfigSaveInfo.IssConfigSaveOption == ISS_CONFIG_STARTUP_SAVE))
        {
            if (gi4MibSaveStatus != MIB_SAVE_IN_PROGRESS)
            {
                MsrSaveDatatoIncrFile (NULL, MSR_INCREMENTAL_SAVE_BUFF_SIZE);
            }
        }
        else if (gIssConfigSaveInfo.IssConfigSaveOption ==
                 ISS_CONFIG_REMOTE_SAVE)
        {
            if (gi4RemoteSaveStatus != MIB_SAVE_IN_PROGRESS)
            {
                MsrSaveDatatoIncrBuffer (NULL, MSR_INCREMENTAL_SAVE_BUFF_SIZE);
            }
        }
    }

    gu4MsrOidCount = 0;
    return;
}

/*****************************************************************************/
/* Function Name      : MsrNotifyBridgeModeChange                            */
/*                                                                           */
/* Description        : This function is to notify MSR module about bridge   */
/*                      mode change                                          */
/*                                                                           */
/* Input(s)           : Context id                                           */
/*                                                                           */
/* Output(s)          : MSR_SUCCESS/MSR_FAILURE                              */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : MSR_SUCCESS                                          */
/*****************************************************************************/
INT1
MsrNotifyBridgeModeChange (UINT4 u4ContextId)
{
    return (MsrNotifyUpdateEvent (MSR_BRIDGE_MODE_CHANGE, u4ContextId));
}

INT1
MsrNotifyVlanDeletion (UINT4 u4ContextId)
{
    return (MsrNotifyUpdateEvent (MSR_VLAN_DELETE, u4ContextId));
}

INT1
MsrNotifyPortTypeChange (UINT4 u4ContextId)
{
    return (MsrNotifyUpdateEvent (MSR_PB_PORT_TYPE_CHANGE, u4ContextId));
}

INT1
MsrNotifyStpPortDelete (UINT4 u4IfIndex)
{
    return (MsrNotifyUpdateEvent (MSR_STP_PORT_DELETE, u4IfIndex));
}

/************************************************************************
 *  Function Name   : MsrNotifyContextDeletion
 *  Description     : In the incremental save on mode, this function 
 *                     notifies the MSR process that a context has been deleted.
 *  Input           : u4ContextId  - The Context identifier that has been 
 *                                    deleted from the switch.
 *  Output          : None                            
 *  Returns         : MSR_SUCCESS / MSR_FAILURE
 ************************************************************************/
INT1
MsrNotifyContextDeletion (UINT4 u4ContextId)
{
    return (MsrNotifyUpdateEvent (MSR_CONTEXT_DELETE, u4ContextId));
}

/******************************************************************************
 * Function           : MsrSetHigPortAdminStatus
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : None
 * Action             : To set all higig port admin status as UP
 ******************************************************************************/
VOID
MsrSetHigPortAdminStatus (VOID)
{
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4IntIndex = 0;

    if (IssGetRestoreOptionFromNvRam () == ISS_RESTORE_LOCAL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Restoration completed; No need "
                 "to set ADMIN state!!!\n");
        return;
    }

    for (u4IntIndex = 1; u4IntIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES;
         u4IntIndex++)
    {
        if (CfaCheckIsHgIndex ((UINT2) u4IntIndex) == CFA_SUCCESS)
        {
            /* Set default admin state as CFA_IF_UP for
             * for all XE ports, after getting attach indication */
            if (CfaGetIfInfo (u4IntIndex, &CfaIfInfo) == CFA_SUCCESS)
            {
                if (CfaIfInfo.u1IfAdminStatus != CFA_IF_UP)
                {
                    nmhSetIfMainAdminStatus ((INT4) u4IntIndex, CFA_IF_UP);
                }
            }
        }
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrIsMibRestoreInProgress                        */
/*                                                                          */
/*    Description        : This function gets the MIB Restoration Status.   */
/*                         It checks the variable gi4MibResStatus. If it is */
/*                         MIB_RESTORE_IN_PROGRESS, returns MSR_TRUE. Else  */
/*                         returns False                                    */
/*                                                                          */
/*    Input(s)           : None.                                            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MSR_TRUE or MSR_FALSE                            */
/****************************************************************************/

INT1
MsrIsMibRestoreInProgress (VOID)
{
    if (gi4MibResStatus == MIB_RESTORE_IN_PROGRESS)
    {
        return MSR_TRUE;
    }

    return MSR_FALSE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrUpdateStaticConfig                            */
/*                                                                          */
/*    Description        : To update IfIndex OIDs in restoration file       */
/*                                                                          */
/*    Input(s)           : u4PrevSwitchId, u4CurrSwitchId                   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/

VOID
MsrUpdateStaticConfig (UINT4 u4PrevSwitchId, UINT4 u4CurrSwitchId)
{
    INT4                i4SaveOption = 0;
    UINT1               au1FileName[ISS_CONFIG_FILE_NAME_LEN];

    UNUSED_PARAM (u4PrevSwitchId);
    UNUSED_PARAM (u4CurrSwitchId);

    nmhGetIssConfigSaveOption (&i4SaveOption);
    if (i4SaveOption != ISS_CONFIG_NO_SAVE)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Removing restoration "
                 "file because of switchid change!!!\n");
        SPRINTF ((CHR1 *) au1FileName, "%s%s", FLASH,
                 (CHR1 *) IssGetRestoreFileNameFromNvRam ());
        issDeleteLocalFile (au1FileName);
        nmhSetIssConfigSaveOption (ISS_CONFIG_NO_SAVE);
        nmhSetIssConfigRestoreOption (ISS_CONFIG_NO_RESTORE);
    }
    return;

}

/************************************************************************
 *  Function Name   : AuditConstructLogMsg
 *  Description     : Constructs the Log message from the audit
 *                    information structure and stores in pointer passed
 *  Input           : pAuditInfo - Pointer  to the Structure that contains 
 *                    all the information got from CLI/SNMP
 *  Output          : pu1Msg - Pointer for the character buffer.
 *  Returns         : MSR_SUCCESS / MSR_FAILURE
 ************************************************************************/
INT4
AuditConstructLogMsg (UINT1 *pu1LogMsg, tAuditInfo * pAuditInfo)
{
    UINT1               au1TempResult[AUDIT_LEN] = "SUCCESS";
    UINT1               au1TempMode[AUDIT_LEN] = "CONSOLE";
    UINT4               au4IpAddr[AUDIT_IP_LEN];
    UINT1               au1TempUidString[AUDIT_UID_STRING_LEN];
    UINT4               u4StorageChkFlag = AUDIT_ZERO;
    UINT4               u4OverWriteChkFlag = AUDIT_ZERO;

    ISS_MEMSET (au1TempUidString, 0, AUDIT_UID_STRING_LEN);
    if (pu1LogMsg != NULL)
    {
        IssCustCheckLogOption (&u4StorageChkFlag, &u4OverWriteChkFlag);
        if (LOG_DIR_STORAGE == u4StorageChkFlag)
        {
            STRCPY (au1TempUidString, "MGMT: ");
        }
        if (pAuditInfo->i2Flag == AUDIT_CLI_MSG)
        {
            if (pAuditInfo->TempCmd.u4CmdStatus == AUDIT_ZERO)
            {
                STRCPY (au1TempResult, "SUCCESS");
            }
            else
            {
                STRCPY (au1TempResult, "FAILURE");
            }
            IPADDR_HEX2STR (OSIX_NTOHL (pAuditInfo->TempCmd.u4ClientIpAddr),
                            au4IpAddr);
            switch (pAuditInfo->TempCmd.u4CliMode)
            {
                case MODE_CONSOLE:
                    STRCPY (au1TempMode, "CONSOLE");
                    SPRINTF ((CHR1 *) pu1LogMsg, "%s%s %s %s %s %s",
                             (CHR1 *) au1TempUidString,
                             (CHR1 *) pAuditInfo->TempCmd.ai1UserName,
                             (CHR1 *) pAuditInfo->TempCmd.au1TempCmd,
                             (CHR1 *) au1TempResult, (CHR1 *) au1TempMode,
                             (CHR1 *) pAuditInfo->au1Time);
                    break;
                case MODE_TELNET:
                    STRCPY (au1TempMode, "TELNET");
                    SPRINTF ((CHR1 *) pu1LogMsg, "%s%s %s %s %s %s %s",
                             (CHR1 *) au1TempUidString,
                             (CHR1 *) pAuditInfo->TempCmd.ai1UserName,
                             (CHR1 *) pAuditInfo->TempCmd.au1TempCmd,
                             (CHR1 *) au1TempResult,
                             (CHR1 *) au1TempMode, (CHR1 *) au4IpAddr,
                             (CHR1 *) pAuditInfo->au1Time);

                    break;
                case MODE_SSH:
                    STRCPY (au1TempMode, "SSH");
                    SPRINTF ((CHR1 *) pu1LogMsg, "%s%s %s %s %s %s %s",
                             (CHR1 *) au1TempUidString,
                             (CHR1 *) pAuditInfo->TempCmd.ai1UserName,
                             (CHR1 *) pAuditInfo->TempCmd.au1TempCmd,
                             (CHR1 *) au1TempResult,
                             (CHR1 *) au1TempMode, (CHR1 *) au4IpAddr,
                             (CHR1 *) pAuditInfo->au1Time);
                    break;
                default:
                    MSR_TRC (MSR_TRACE_TYPE,
                             "Does not match any of the CLI mode!!!\n");
                    break;
            }
            return MSR_SUCCESS;
        }
        else if (pAuditInfo->i2Flag == AUDIT_SNMP_MSG)
        {
            IPADDR_HEX2STR (pAuditInfo->TempSnmp.u4MngrIpAddr, au4IpAddr);
            SPRINTF ((CHR1 *) pu1LogMsg, "%s%s %s %s %s %s %s",
                     (CHR1 *) au1TempUidString,
                     (CHR1 *) pAuditInfo->TempSnmp.au4_OidList,
                     (CHR1 *) pAuditInfo->TempSnmp.pu1ObjVal,
                     (CHR1 *) pAuditInfo->TempSnmp.ai1Status,
                     (CHR1 *) au4IpAddr,
                     (CHR1 *) pAuditInfo->TempSnmp.ai1SnmpUser,
                     (CHR1 *) pAuditInfo->au1Time);

#ifdef SNMP_3_WANTED
            SnmpApiClearData (pAuditInfo->TempSnmp.pu1ObjVal);
#endif
            return MSR_SUCCESS;
        }
        else if (pAuditInfo->i2Flag == AUDIT_SYSLOG_MSG)
        {
            SPRINTF ((CHR1 *) pu1LogMsg, "%s%s\n", (CHR1 *) au1TempUidString,
                     (CHR1 *) pAuditInfo->au1SyslogBuf);
            return MSR_SUCCESS;
        }
    }
    return MSR_FAILURE;

}

/************************************************************************
 *  Function Name   : MsrProcessAuditLogEvent
 *  Description     : This function process the Audit Event and logs the
 *                    CLI/SNMP information into Audit log file
 *  Input(s)        : None                                             
 *  Output(s)       : None.                                            
 *  Returns         : None. 
 *  
 ************************************************************************/
VOID
MsrProcessAuditLogEvent (VOID)
{
    FILE               *fp;
    UINT1               au1SrcFile[ISS_AUDIT_FILE_NAME_LENGTH];
    UINT1               au1CurDateString[32];
    UINT1               au1FileName[ISS_AUDIT_FILE_NAME_LENGTH];
    INT4                i4RetVal = AUDIT_ZERO;
    UINT4               u4MsgLen = AUDIT_ZERO;
    UINT4               u4StorageChkFlag = AUDIT_ZERO;
    UINT4               u4OverWriteChkFlag = AUDIT_ZERO;
    tAuditInfo         *pAuditInfo = NULL;
    tAuditTrapMsg       TrapMsg;

    MEMSET (au1SrcFile, 0, ISS_AUDIT_FILE_NAME_LENGTH);

    MEMSET (au1FileName, 0, ISS_AUDIT_FILE_NAME_LENGTH);

    while (OsixQueRecv (gAuditQId,
                        (UINT1 *) &pAuditInfo, OSIX_DEF_MSG_LEN,
                        0) == OSIX_SUCCESS)
    {

        if (ISS_FAILURE == IssCustLocalLoggingCheck (SYSLOG_DEBUG_LEVEL))
        {
            return;
        }

        IssCustCheckLogOption (&u4StorageChkFlag, &u4OverWriteChkFlag);
        if (LOG_DIR_STORAGE == u4StorageChkFlag)
        {
            UtlGetCurDate ((CHR1 *) au1CurDateString);
            ISS_MEMSET (au1SrcFile, 0, ISS_AUDIT_FILE_NAME_LENGTH);
            SPRINTF ((CHR1 *) au1SrcFile, "%s/%s_%s", AUDIT_LOG_FILE_PATH,
                     au1CurDateString, AUDIT_FILE_NAME_EXT);
            STRCPY (gau1IssAuditLogFileName, au1SrcFile);
            STRCPY (pAuditInfo->au1IssLogFileName, au1SrcFile);
        }
        if (AUDIT_SYSLOG_MSG != pAuditInfo->i2Flag)
        {

            STRCPY (au1SrcFile, gau1IssAuditLogFileName);
        }
        else
        {
            SNPRINTF ((CHR1 *) au1SrcFile, ISS_CONFIG_FILE_NAME_LEN, "%s/%s",
                      ISS_LOG_DIRECTORY, pAuditInfo->au1IssLogFileName);
        }
        if ((fp = FOPEN ((CONST CHR1 *) au1SrcFile, "a+")) == NULL)
        {
            ISS_MEMSET (&TrapMsg, 0, sizeof (tAuditTrapMsg));
            STRNCPY (TrapMsg.au1IssLogFileName, au1SrcFile,
                     ISS_CONFIG_FILE_NAME_LEN);
            TrapMsg.u4Event = AUDIT_TRAP_OPEN_FAILED;
            UtlGetTimeStr (TrapMsg.ac1DateTime);
            AuditTrapSendNotifications (&TrapMsg);

            MemReleaseMemBlock (gAuditQMsgPoolId, (UINT1 *) pAuditInfo);
            MSR_TRC (MSR_TRACE_TYPE, "Audit Information  - "
                     "File Open Operation Failed\n");
            return;
        }
        fclose (fp);

        if ((gu4MsrAuditLogSize == 0) ||
            (STRCMP (au1SrcFile, gau1AuditPrevLogFileName) != 0))
        {
            MEMSET (gau1AuditPrevLogFileName, 0,
                    sizeof (gau1AuditPrevLogFileName));
            STRNCPY (gau1AuditPrevLogFileName, au1SrcFile,
                     sizeof (gau1AuditPrevLogFileName) - 1);
            if (LOG_FILE_STORAGE == u4StorageChkFlag)
            {
                issGetLocalFileSize (au1SrcFile, &gu4MsrAuditLogSize);
                STRNCPY (au1FileName, au1SrcFile, ISS_CONFIG_FILE_NAME_LEN);
            }
            else
            {
                IssGetLocalDirSize (AUDIT_LOG_FILE_PATH, &gu4MsrAuditLogSize);
                STRCPY (au1FileName, AUDIT_LOG_FILE_PATH);
            }
        }

        i4RetVal = AuditConstructLogMsg (gau1LogMsg, pAuditInfo);
        if (MSR_FAILURE == i4RetVal)
        {
            MemReleaseMemBlock (gAuditQMsgPoolId, (UINT1 *) pAuditInfo);
            return;
        }
        u4MsgLen = STRLEN (gau1LogMsg);
        i4RetVal = MsrUtilChkAuditFileSize (gu4MsrAuditLogSize + u4MsgLen,
                                            au1SrcFile, pAuditInfo->i2Flag);

        if (MSR_SUCCESS == i4RetVal)
        {
            if ((fp = FOPEN ((CONST CHR1 *) au1SrcFile, "a+")) == NULL)
            {
                ISS_MEMSET (&TrapMsg, 0, sizeof (tAuditTrapMsg));
                STRNCPY (TrapMsg.au1IssLogFileName, au1SrcFile,
                         ISS_CONFIG_FILE_NAME_LEN);
                TrapMsg.u4Event = AUDIT_TRAP_OPEN_FAILED;
                UtlGetTimeStr (TrapMsg.ac1DateTime);
                AuditTrapSendNotifications (&TrapMsg);
                MemReleaseMemBlock (gAuditQMsgPoolId, (UINT1 *) pAuditInfo);
                MSR_TRC (MSR_TRACE_TYPE, "Audit Information  - "
                         "File Open Operation Failed\n");
                return;
            }
            fprintf (fp, "%s", gau1LogMsg);
            gu4MsrAuditLogSize += u4MsgLen;
            fclose (fp);
            issGetLocalFileSize (au1SrcFile, &gu4MsrAuditLogSize);
        }
        MemReleaseMemBlock (gAuditQMsgPoolId, (UINT1 *) pAuditInfo);
    }
    MSR_TRC (MSR_TRACE_TYPE, "Audit Information  - Queue Receive Finished\n");
    return;
}

/************************************************************************
 *  Function Name   : MsrProcessSysLogEvent
 *  Description     : This function process the Syslog write event and sends 
 *                    Message into Log File
 *  Input(s)        : None                                             
 *  Output(s)       : None.                                            
 *  Returns         : None. 
 *  
 ************************************************************************/
VOID
MsrProcessSysLogEvent (VOID)
{
    INT4                i4CurFileSize = 0;
    INT4                i4MaxFileSize = MAX_FILE_SIZE;
    INT4                i4MaxReqSize = MAX_TRACE_LEN;
    FILE               *fp;
    UINT1               au1SrcFile[ISS_AUDIT_FILE_NAME_LENGTH];
    UINT1               au1DesFile[ISS_AUDIT_FILE_NAME_LENGTH];
    UINT4               u4Size = AUDIT_ZERO;
    tAuditInfo         *pAuditInfo = NULL;

    if (OsixQueRecv (gAuditQId,
                     (UINT1 *) &pAuditInfo, OSIX_DEF_MSG_LEN,
                     0) != OSIX_SUCCESS)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Audit Information  - Queue Receive Failed\n");
        return;
    }

    STRCPY (au1SrcFile, pAuditInfo->au1IssLogFileName);

    if ((fp = FOPEN ((CONST CHR1 *) au1SrcFile, "a+")) == NULL)
    {
        MemReleaseMemBlock (gAuditQMsgPoolId, (UINT1 *) pAuditInfo);
        MSR_TRC (MSR_TRACE_TYPE, "Audit Information  - "
                 "File Open Operation Failed\n");
        return;
    }
    issGetLocalFileSize (au1SrcFile, &u4Size);

    if (u4Size > gu4AuditLogSize)
    {
        /* If the Syslog and Audit filenames match, Do as Audit does ->*
         * Take a backup copy                                           */
        if (STRCMP (au1SrcFile, gau1IssAuditLogFileName) == 0)
        {
            issCopyLocalFile (au1SrcFile, au1DesFile);
            fclose (fp);
            if ((fp = FOPEN ((CONST CHR1 *) au1SrcFile, "w")) == NULL)
            {
                MemReleaseMemBlock (gAuditQMsgPoolId, (UINT1 *) pAuditInfo);
                MSR_TRC (MSR_TRACE_TYPE, "Audit Information  - "
                         "File Open Operation Failed\n");
                return;
            }
        }
        else
        {
            i4CurFileSize = ftell (fp);
            if ((i4MaxFileSize - i4CurFileSize) < i4MaxReqSize)
            {
                fclose (fp);
                if ((fp = FOPEN ((CONST CHR1 *) au1SrcFile, "r+")) == NULL)
                {
                    MemReleaseMemBlock (gAuditQMsgPoolId, (UINT1 *) pAuditInfo);
                    MSR_TRC (MSR_TRACE_TYPE, "Audit Information  - "
                             "File Open Operation Failed\n");
                    return;
                }
            }
        }
    }
    pAuditInfo->i2Flag = AUDIT_SYSLOG_MSG;
    AuditConstructLogMsg (gau1LogMsg, pAuditInfo);
    fprintf (fp, "%s", gau1LogMsg);
    MemReleaseMemBlock (gAuditQMsgPoolId, (UINT1 *) pAuditInfo);
    fclose (fp);
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrResumeUserConfiguration                       */
/*                                                                          */
/*    Description        : This function releases the MGMT_LOCK taken       */
/*                          during intialization, so that the user          */
/*                          interface are available for confiugration.      */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/

VOID
MsrResumeUserConfiguration (VOID)
{
    /* Release the MGMT_LOCK taken, so that the UI can be available to the user
       The lock is taken only for the first time ISS is initialized. Hence, the
       release is also done the first time alone */
    if (gu1MsrRestoreCount == 0)
    {
        MGMT_UNLOCK ();
        gu1MsrRestoreCount = 1;
    }
    return;
}

/**********************************************************************************
 *  Function Name   : MsrNotifyPortDeletion                                       *
 *  Description     : In the incremental save on mode, this function              *
 *                     notifies the MSR process that a Port has been deleted.     *
 *  Input           : u4InterfaceId  - The Port identifier that has been          *
 *                                    deleted                                     *
 *  Output          : None                                                        *
 *  Returns         : MSR_SUCCESS / MSR_FAILURE                                   *
 **********************************************************************************/
INT1
MsrNotifyPortDeletion (UINT4 u4InterfaceId)
{
    return (MsrNotifyUpdateEvent (MSR_PORT_DELETE, u4InterfaceId));
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrGetIncrementalSaveStatus                      */
/*                                                                          */
/*    Description        : This function returns the incremental save       */
/*                          flag status.                                    */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ISS_TRUE / ISS_FALSE                             */
/****************************************************************************/
tIssBool
MsrGetIncrementalSaveStatus (VOID)
{
    return (gMSRIncrSaveFlag);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrGetRestorationStatus                          */
/*                                                                          */
/*    Description        : This function returns the whether the restoration*/
/*                          process is ongoing or completed.                */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ISS_TRUE / ISS_FALSE                             */
/****************************************************************************/
tIssBool
MsrGetRestorationStatus ()
{
    return (MsrisRetoreinProgress);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrGetClearConfigStatus                          */
/*                                                                          */
/*    Description        : This function returns the whether the clear      */
/*                          config status is ongoing or completed            */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ISS_TRUE / ISS_FALSE                             */
/****************************************************************************/

tIssBool
MsrGetClearConfigStatus ()
{
    return (gi4ClearConfig);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrGetSaveStatus                                 */
/*                                                                          */
/*    Description        : This function returns the whether the save       */
/*                          process is ongoing or completed.                */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ISS_TRUE / ISS_FALSE                             */
/****************************************************************************/
tIssBool
MsrGetSaveStatus ()
{
    if (gi4MibSaveStatus == MIB_SAVE_IN_PROGRESS)
    {
        return (ISS_TRUE);
    }
    else
    {
        return (ISS_FALSE);
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrIsSaveComplete                                */
/*                                                                          */
/*    Description        : This function returns whether Mib save is        */
/*                         complete or not                                  */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ISS_TRUE / ISS_FALSE                             */
/****************************************************************************/
tIssBool
MsrIsSaveComplete ()
{
    return (MsrisSaveComplete);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrValidateVersionAndCheckSum                    */
/*                                                                          */
/*    Description        : This function checks the version(file format) and*/
/*                         validates the checksum of restoration file       */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ISS_TRUE / ISS_FALSE                             */
/****************************************************************************/
INT4
MsrValidateVersionAndCheckSum (UINT4 *pu4FileVersion, UINT1 *pu1AltConf)
{
    UINT4               u4FileVersion = 0;
    INT4                i4RetVal = MSR_FAILURE;
    INT4                i4ConfFd = 0;
    UINT1               u1AltFile = MSR_FALSE;

    /* The format of saving the configurations in the flash
     * is modified. Verify, if the restoration file is in the
     * new format or in the old format */
    if (MsrObtainFilePointer (&i4ConfFd) != ISS_SUCCESS)
    {
        MSR_TRC (MSR_TRACE_TYPE,
                 "Unable to obtain the configuration file" "pointer\n");
        u1AltFile = MSR_TRUE;
        SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                      "Unable to open the original conf file or "
                      "Invalid conf file !!!"));
    }
    else
    {
        if (MSR_CALLBACK[MSR_CONF_FILE_VALIDATE].pMsrCustValidateConfResFile !=
            NULL)
        {
            if (MSR_CALLBACK[MSR_CONF_FILE_VALIDATE].
                pMsrCustValidateConfResFile (i4ConfFd) == ISS_FAILURE)
            {
                gi4MibResFailureReason = MIB_RES_CUST_VALIDATION_FAILURE;
                FileClose (i4ConfFd);
                return MSR_FAILURE;
            }
        }
        i4RetVal = MSRCheckforOldFileVersion (&u4FileVersion, i4ConfFd);
        if (i4RetVal != MSR_SUCCESS)
        {
            u1AltFile = MSR_TRUE;
            MSR_TRC (MSR_TRACE_TYPE, "Incompatible version of original file\n");
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                          "Incompatible original restore file version !!!"));
            gi4MibResFailureReason = MIB_RES_FILE_VERSION_FAILURE;
        }
        else
        {
            *pu4FileVersion = u4FileVersion;
            /* Read the configurations saved in the file and verify
             * the checksum */
            if (MsrObtainFilePointer (&i4ConfFd) == ISS_SUCCESS)
            {
                if (MsrVerifyRestFileChecksum (i4ConfFd) == ISS_FAILURE)
                {
                    MSR_TRC (MSR_TRACE_TYPE, "MIB restore failed!!!\n");
                    SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                                  "Checksum verification failed for original"
                                  "restoration file...!!!"));
                    u1AltFile = MSR_TRUE;
                }
            }
        }
    }

    if (u1AltFile == MSR_TRUE)
    {
        if (MsrObtainAltResFilefd (&i4ConfFd) != ISS_SUCCESS)
        {
            SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                          "Unable to open the alternate conf file or "
                          "Invalid conf file !!!"));
            gi4MibResFailureReason = MIB_RES_FILE_OPEN_FAILURE;
            return MSR_FAILURE;
        }
        else
        {

            if (MSR_CALLBACK[MSR_CONF_FILE_VALIDATE].
                pMsrCustValidateConfResFile != NULL)
            {
                if (MSR_CALLBACK[MSR_CONF_FILE_VALIDATE].
                    pMsrCustValidateConfResFile (i4ConfFd) == ISS_FAILURE)
                {
                    gi4MibResFailureReason = MIB_RES_CUST_VALIDATION_FAILURE;
                    FileClose (i4ConfFd);
                    return MSR_FAILURE;
                }
            }

            i4RetVal = MSRCheckforOldFileVersion (&u4FileVersion, i4ConfFd);
            if (i4RetVal != MSR_SUCCESS)
            {
                SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                              "Incompatible alternate restore file version !!!"));
                gi4MibResFailureReason = MIB_RES_FILE_VERSION_FAILURE;
            }
            else
            {
                *pu4FileVersion = u4FileVersion;

                /* Read the configurations saved in the file and verify
                 * the checksum */
                if (MsrObtainAltResFilefd (&i4ConfFd) == ISS_SUCCESS)
                {
                    if (MsrVerifyRestFileChecksum (i4ConfFd) == ISS_FAILURE)
                    {
                        MSR_TRC (MSR_TRACE_TYPE, "MIB restore failed!!!\n");
                        SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4MsrSysLogId,
                                      "Checksum verification failed for alternate"
                                      "restoration file...!!!"));
                        return MSR_FAILURE;
                    }
                    *pu1AltConf = MSR_TRUE;
                }
            }
        }
    }

    return i4RetVal;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrStartConfigurationRestore                     */
/*                                                                          */
/*    Description        : This function starts the configuration restore   */
/*                         from a remote server                             */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
INT4
MsrStartConfigurationRestore ()
{

    if (OsixSendEvent ((UINT4) SELF, (const UINT1 *) MSR_TASK_NAME,
                       (UINT4) MIB_RESTORE_FROM_REMOTE_EVENT) != OSIX_SUCCESS)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Remote restore event send failed!!!\n");
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
    }

    return MSR_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MSRRestoreFromRestoreArray                     */
/*                                                                          */
/*   Description        : This function is used to restore from the flash   */
/*                        array. From remote configuration restore file the */
/*                        contents are copied to flash array                */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/****************************************************************************/

INT4
MSRRestoreFromRestoreArray ()
{
    UINT1               au1Oid[MSR_MAX_OID_LEN];
    UINT2               u2Type = 0;
    UINT4               u4Len = 0;
    UINT1              *pTemp = NULL;
    UINT1              *pu1Cfg = NULL;
    UINT4               u4Index = 0;
    UINT4               u4ErrorCount = 0;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_MULTI_DATA_TYPE *pData = NULL;
    UINT4               u4Total = 0;
    UINT4               u4Error = 0;
    UINT2               u2CfgCntr = 0;
    UINT1               au1PrefixOid[MSR_MAX_OID_LEN];
    UINT1               au1SuffixOid[MSR_MAX_OID_LEN];
    UINT4               u4DataCntr = 0;
    tSnmpIndex         *pCurIndex = MsrIndexPool1;

    gu4MsrMultiDataCount = 0;
    gu4MsrOctetStrCount = 0;
    gu4MsrOidCount = 0;

    gi4MibResStatus = MIB_RESTORE_IN_PROGRESS;
    if ((pOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        return ISS_FAILURE;
    }
    if ((MsrAllocMultiData ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        gu4MsrOidCount = 0;
        return ISS_FAILURE;
    }

    MSR_MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);
    MSR_MEMSET (au1PrefixOid, 0, MSR_MAX_OID_LEN);
    MSR_MEMSET (au1SuffixOid, 0, MSR_MAX_OID_LEN);
    if (MSR_DATA_ALLOC_MEM_BLOCK (pu1Cfg) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        gu4MsrOidCount = 0;
        return ISS_FAILURE;
    }
    MSR_MEMSET (pu1Cfg, 0, MSR_MAX_DATA_LEN);
    gpu1StrCurrent = gpu1StrFlash;

    while (u4DataCntr < gu4Length)
    {
        pu1Cfg[u2CfgCntr] = gpu1StrCurrent[u4DataCntr++];
        if ((pu1Cfg[u2CfgCntr++] != '\n')
            && (u2CfgCntr < (MSR_MAX_DATA_LEN - 1)))
        {
            continue;
        }
        u4Total++;
        if (u2CfgCntr <= 1)
        {
            u2CfgCntr = 0;
            MSR_MEMSET (pu1Cfg, 0, MSR_MAX_DATA_LEN);
            continue;
        }
        /*The "CKSUM:" acts as a delimiter marking the end of
         * information so no need to process it*/
        if (STRNCMP (pu1Cfg, "CKSUM:", 6) == 0)
        {
            break;
        }
        pTemp = pu1Cfg;
        if (*pTemp == '@')
        {
            MSR_MEMSET (au1PrefixOid, 0, MSR_MAX_OID_LEN);
            pTemp += 1;
            /* get the MSR index value */
            while (*pTemp != '\t')
            {
                pTemp++;
            }
            pTemp++;

            /* get the prefix oid is present */
            for (u4Index = 0; u4Index < MSR_MAX_OID_LEN; u4Index++)
            {
                if (*pTemp != '\n')
                {
                    au1PrefixOid[u4Index] = *pTemp;
                    pTemp++;
                }
                else
                {
                    au1PrefixOid[u4Index] = '\0';
                    break;
                }
            }
            pTemp = pu1Cfg;
        }
        else
        {
            if (u4Total > MSR_RES_FILE_VER_FORMAT)
            {
                /* get the MSR Rowstatus type value */
                pTemp += 2;
            }

            /* Get the OID of the object to be restored and convert
             * it from string to the OID type.*/
            for (u4Index = 0; u4Index < MSR_MAX_OID_LEN; u4Index++)
            {
                if (*pTemp != '\t')
                {
                    au1SuffixOid[u4Index] = *pTemp;
                    pTemp++;
                }
                else
                {
                    au1SuffixOid[u4Index] = '\0';
                    break;
                }
            }

            if (au1PrefixOid[0] != '\0')
            {
                SnmpUpdateEoidString (au1PrefixOid);
            }
            else
            {
                SnmpUpdateEoidString (au1SuffixOid);
            }
            /* concatenate the prefix oid and the suffix oids read */
            SNPRINTF ((char *) au1Oid, MSR_MAX_OID_LEN,
                      "%s%s", au1PrefixOid, au1SuffixOid);
            WebnmConvertStringToOid (pOid, au1Oid, MSR_FALSE);

            /* Get the multidata data type of the object to be restored */
            u2Type = (UINT2) ATOI (pTemp + 1);
            pTemp += 4;
            u4Len = 0;
            if (u2Type == OCTETSTRING)
            {
                u4Len = (UINT4) ATOI (pTemp);
                pTemp += 5;
                while (pTemp[u4Len] != '\n')
                {
                    while (u4DataCntr < gu4Length)
                    {
                        pu1Cfg[u2CfgCntr] = gpu1StrCurrent[u4DataCntr++];
                        if ((pu1Cfg[u2CfgCntr++] == '\n') ||
                            (u2CfgCntr >= MSR_MAX_DATA_LEN))
                        {
                            break;
                        }
                    }
                    if (u2CfgCntr >= MSR_MAX_DATA_LEN)
                    {
                        break;
                    }
                }
            }
            pData = MsrConvertStringToData (pTemp, u2Type, u4Len);
            if (pData == NULL)
            {
                MSR_TRC (MSR_TRACE_TYPE, "Memory allocation failed!!!\n");
                gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                gu4MsrOidCount = 0;
                gu4MsrMultiDataCount = 0;
                gu4MsrOctetStrCount = 0;
                MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);
                return ISS_FAILURE;
            }

            pTemp = pu1Cfg;

            /* Restoration should not be done if restore file version
             * is incompatible with executable's restore file version
             */
            if (u4Total == 1)
            {
                /* Verify the first OID is issConfigRestoreFileVersion
                 * or Not. If So then check the version
                 */
                if ((STRCMP (au1Oid, ISS_RESTORE_FILE_VERSION_OID) != 0) ||
                    (MSR_MEMCMP (pData->pOctetStrValue->pu1_OctetList,
                                 ISS_RESTORE_FILE_VERSION,
                                 pData->pOctetStrValue->i4_Length) != 0))
                {
                    MSR_TRC (MSR_TRACE_TYPE, "Incompatable restore file!!!\n");
                    gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                    gu4MsrOidCount = 0;
                    gu4MsrMultiDataCount = 0;
                    gu4MsrOctetStrCount = 0;
                    MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);
                    return ISS_FAILURE;
                }
            }
            /* If the File format is wrong means, throws Erroe message.
             * */
            else if (u4Total == 2)
            {
                if ((STRCMP (au1Oid, ISS_RESTORE_FILE_FORMAT_OID) != 0) ||
                    (STRCMP (pData->pOctetStrValue->pu1_OctetList,
                             ISS_RESTORE_FILE_FORMAT) != 0))
                {
                    MSR_TRC (MSR_TRACE_TYPE, "Incompatable restore file!!!\n");
                    gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
                    gu4MsrOidCount = 0;
                    gu4MsrMultiDataCount = 0;
                    gu4MsrOctetStrCount = 0;
                    MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);
                    return ISS_FAILURE;
                }
            }
            else
            {
                MSR_CLEAR_MULTIDATA (pCurIndex->pIndex);
                if (SNMPSet (*pOid, pData, &u4Error, pCurIndex,
                             SNMP_DEFAULT_CONTEXT) == SNMP_FAILURE)
                {
                    u4ErrorCount++;
                }
                MSR_MEMSET (au1Oid, 0, MSR_MAX_OID_LEN);
                MSR_MEMSET (au1SuffixOid, 0, MSR_MAX_OID_LEN);
            }
        }
        u2CfgCntr = 0;
        MSR_MEMSET (pu1Cfg, 0, MSR_MAX_DATA_LEN);
    }
    MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);
    /* Restore operation completed successfully */
    gi4MibResStatus = LAST_MIB_RESTORE_SUCCESSFUL;
    gu4MsrOidCount = 0;
    /* Set the notification information */
    gNotifMsg.u4ConfigRestoreStatus = (UINT4) gi4MibResStatus;

    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrValidRemFileCheckSum                         */
/*                                                                          */
/*   Description        : This function is used to validate the checksum of*/
/*                        flash array. From remote configuration restore    */
/*                        file the contents are copied to flash array       */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/****************************************************************************/
INT4
MsrValidRemFileCheckSum ()
{
    UINT4               u4Length = 0;
    UINT1               au1CheckSum[FLASH_CKSUM_LEN];
    UINT4               u4Type = CKSUM_DATA;
    UINT4               u4Sum = 0;
    UINT4               u4TotLength = 0;
    UINT4               u4DataCntr = 0;
    UINT2               u2CfgCntr = 0;
    UINT2               u2ChkSum = 0;
    UINT1               u1Tmp = 0;
    UINT1              *pu1Cfg = NULL;

    u4Length = MSR_STRLEN ("CKSUM:");
    if (MSR_DATA_ALLOC_MEM_BLOCK (pu1Cfg) == NULL)
    {
        return ISS_FAILURE;
    }
    MSR_MEMSET (pu1Cfg, 0, MSR_MAX_DATA_LEN);
    gpu1StrCurrent = gpu1StrFlash;

    while (u4DataCntr < gu4Length)
    {
        pu1Cfg[u2CfgCntr] = gpu1StrCurrent[u4DataCntr++];
        if ((pu1Cfg[u2CfgCntr++] != '\n') && (u2CfgCntr < MSR_MAX_DATA_LEN))
        {
            continue;
        }
        if (STRNCMP (pu1Cfg, "CKSUM:", u4Length) != 0)
        {
            u4TotLength += u2CfgCntr;
            UtilCalcCheckSum (pu1Cfg, (UINT4) u2CfgCntr, &u4Sum,
                              &u2ChkSum, u4Type);
            u2CfgCntr = 0;
            MSR_MEMSET (pu1Cfg, 0, MSR_MAX_DATA_LEN);
        }
        else
        {
            /*Break here, since CKSUM is the last line */
            break;
        }
    }

    u4TotLength += u2CfgCntr;

    if (STRNCMP (pu1Cfg, "CKSUM:", u4Length) != 0)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Invalid Configuration file !!!\n");
    }

    MSR_MEMCPY (au1CheckSum, (pu1Cfg + u4Length), FLASH_CKSUM_LEN);
    MEMCPY (&gu2FlashCksum, au1CheckSum, sizeof (UINT2));
    MSR_MEMSET ((pu1Cfg + u4Length), 0, FLASH_CKSUM_LEN);

    u4Type = CKSUM_LASTDATA;

    if ((u4TotLength % 2) == 1)
    {
        MEMCPY ((pu1Cfg + u2CfgCntr), &u1Tmp, sizeof (UINT1));
        u2CfgCntr++;
    }
    UtilCalcCheckSum (pu1Cfg, u2CfgCntr, &u4Sum, &u2ChkSum, u4Type);

    if (u2ChkSum != gu2FlashCksum)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Configuration File Corrupted!!!\n");
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);
        return ISS_FAILURE;
    }
    MSR_DATA_FREE_MEM_BLOCK (pu1Cfg);
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRemRestoreInit                                */
/*                                                                          */
/*    Description        : This function will start a timer and wait for 30 */
/*                         seconds, to allow spanning tree to converge      */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
INT4
MsrRemRestoreInit ()
{
    INT1               *pi1Param = NULL;

    MsrisRetoreinProgress = ISS_TRUE;

    VlanCreateDefaultVlan (pi1Param);

    TmrStartTimer (gIssMsrTimerListId, &gIssMsrTmrApp,
                   MSR_REMOTE_RESTORE_TIMEOUT * SYS_TIME_TICKS_IN_A_SEC);
    MSR_TRC (MSR_TRACE_TYPE,
             "Waiting for spanning-tree topology to settle down.....\n");
    return ISS_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrRemoteRestore                                 */
/*                                                                          */
/*    Description        : This function will check whether any of the port */
/*                         is in forwarding state.If so it will initiate     */
/*                         a remote restore.else it will declare restoration*/
/*                         as failure                                       */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/

VOID
MsrRemoteRestore ()
{
    UINT2               u2IfaceIndex = 0;
    UINT2               u2DefaultMsti = 0;
    UINT1               u1IsForwarding = 0;
    UINT4               i4RetValue = 0;

    /* Delete Timer List */
    TmrDeleteTimerList (gIssMsrTimerListId);

    for (u2IfaceIndex = 1; u2IfaceIndex <= SYS_DEF_MAX_PHYSICAL_INTERFACES;
         u2IfaceIndex++)
    {
        i4RetValue = L2IwfGetInstPortState (u2DefaultMsti, u2IfaceIndex);

        if (i4RetValue == AST_PORT_STATE_FORWARDING)
        {
            u1IsForwarding = 1;
            break;
        }
    }

    if (u1IsForwarding == 1)
    {
        MsrStartConfigurationRestore ();
    }
    else
    {
        gi4MibResStatus = LAST_MIB_RESTORE_FAILED;
        MSR_TRC (MSR_TRACE_TYPE,
                 "Remote Restore Failed.Unable to connect TFTP server !!!\n");
    }
    return;
}

#ifdef VPN_WANTED
/************************************************************************
 *  Function Name   : MsrValidateVpnPolicyEntry
 *  Description     : The function saves the VPN Policy Configurations 
 *                    According to the Mode type.If it is IKE then IKE       
 *                    releated configuration will store skips the Manual     
 *                    configurations.If it is Manual then IKE Configurations 
 *                    will be skiped. Refere the MIB for IKE and Manual         
 *                    configurations.                                           
 *  Input           : pOid - pointer to the OID 
 *                    pInstance - pointer to the instance OID 
 *                    pData - pointer to the data to be saved
 *  Output          : None
 *  Returns         : MSR_SAVE if the object should be saved
 *                    MSR_SKIP if just the current instance of the object
 *                             should not be saved   
 *                    MSR_NEXT if the current scalar/table should not be
 *                             saved 
 ************************************************************************/
INT4
MsrValidateVpnPolicyEntry (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData)
{
#define VPN_MANUAL_POLICY  1
    UINT4               u4PolicyType = 0;
    UINT4               u4Count = 0;
    tSNMP_OCTET_STRING_TYPE FsVpnPolicyName;
    UINT1               au1PolicyName[SNMP_MAX_OCTETSTRING_SIZE];

    UNUSED_PARAM (pData);

    if (pOid->u4_Length == 12)
    {
        /* fsVpnPolicyRowstatus object always comes with full OID i.e
         * pOid length 12 and pInstance containg the instance value alone.
         * For other objects in the table pOid contains pVpnEntry OID
         * whose length is 11 and pInstance starts with its identifier
         * in thae table i.e between 2 to 43 and the rest contains instance
         * information.
         */
        return (MSR_SAVE);
    }

    FsVpnPolicyName.i4_Length = pInstance->pu4_OidList[1];
    FsVpnPolicyName.pu1_OctetList = &au1PolicyName[0];
    MSR_MEMSET (FsVpnPolicyName.pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);

    for (u4Count = 2; u4Count < (pInstance->u4_Length); u4Count++)
    {
        au1PolicyName[u4Count - 2] = (CHR1) (pInstance->pu4_OidList[u4Count]);
    }
    nmhGetFsVpnPolicyType (&FsVpnPolicyName, (INT4 *) &u4PolicyType);

    /* if Policy Type is Manual Skip the objects from
     * fsVpnIkePhase1HashAlgo(26) to fsVpnIkeVersion (41)
     */
    if (u4PolicyType == VPN_MANUAL_POLICY)
    {
        if ((pInstance->pu4_OidList[0] > 25) &&
            (pInstance->pu4_OidList[0] < 42))
        {
            return (MSR_SKIP);
        }
    }
    /* SKIP From : fsVpnInboundSpi(15)  to : fsVpnAntiReplay(22)
     * !(fsVpnMode (17)) */
    else
    {
        if (((pInstance->pu4_OidList[0] > 16) &&
             (pInstance->pu4_OidList[0] < 23)) &&
            !(pInstance->pu4_OidList[0] == 17))
        {
            return (MSR_SKIP);
        }
    }

    return (MSR_SAVE);
}
#endif /* VPN_WANTED */

/*****************************************************************************/
/* Function Name      : MsrSysRegHL                                          */
/*                                                                           */
/* Description        : This function is called to register the              */
/*                        Call back function to indicate the protocol about    */
/*                      MSR completion Status                                */
/*                                                                           */
/* Input(s)           : MSR register parameters Structure pointer            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : MSR_SUCCESS/MSR_FAILURE                              */
/*****************************************************************************/
INT4
MsrSysRegHL (tMsrRegParams * pMsrRegParams)
{
    UINT4               u4Index;

    for (u4Index = 0; u4Index < ISS_MAX_PROTOCOLS; u4Index++)
    {
        if (gMsrRegTbl[u4Index].bFlag == MSR_FALSE)
        {
            gMsrRegTbl[u4Index].bFlag = MSR_TRUE;
            gMsrRegTbl[u4Index].pMsrCompleteStatusCallBack =
                pMsrRegParams->pMsrCompleteStatusCallBack;
            return MSR_SUCCESS;
        }
    }
    return MSR_FAILURE;

}

/*****************************************************************************/
/* Function Name      : MsrIndMsrCompleteStatusToProto                       */
/*                                                                           */
/* Description        : This function is called to indicate the registered   */
/*                        protocol about MSR Completion  status                */
/*                                                                           */
/* Input(s)           : MSR completion status                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : MSR_SUCCESS                                          */
/*****************************************************************************/
INT4
MsrIndMsrCompleteStatusToProto (INT4 i4MsrCompleStatus)
{
    UINT4               u4Index;

    for (u4Index = 0; u4Index < ISS_MAX_PROTOCOLS; u4Index++)
    {
        if ((gMsrRegTbl[u4Index].bFlag == MSR_TRUE)
            && gMsrRegTbl[u4Index].pMsrCompleteStatusCallBack != NULL)
        {
            gMsrRegTbl[u4Index].pMsrCompleteStatusCallBack (i4MsrCompleStatus);
        }
    }
    return MSR_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : MsrInitiateConfigSave                                */
/*                                                                           */
/* Description        : This function is called to store the static          */
/*                      Configuration when it is invoked by RM Module        */
/*                      for Hitless Restart Support Feature                  */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
MsrInitiateConfigSave (VOID)
{
    MEMSET (gMsrData.au1FileName, 0, sizeof (gMsrData.au1FileName));
    /* Copying file name from Nvram to  gMsrData to store the static configuration */
    ISS_STRNCPY (gMsrData.au1FileName, IssGetRestoreFileNameFromNvRam (),
                 (sizeof (gMsrData.au1FileName) - 1));
    gMsrData.au1FileName[(sizeof (gMsrData.au1FileName) - 1)] = '\0';
    /* Pass an event to MSR Task to Start the local saving of MIB */
    OsixEvtSend (MsrId, MIB_SAVE_EVENT);
}

/*****************************************************************************/
/* Function Name      : MsrHandleStpPortDelete                               */
/*                                                                           */
/* Description        : This function takes care of STP port properties      */
/*                      reset in case of port deletion in STP.               */
/*                                                                           */
/* Input(s)           : MSR completion status                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : MSR_SUCCESS                                          */
/*****************************************************************************/
VOID
MsrHandleStpPortDelete (UINT4 u4IfIndex)
{
    tMSRRbtree          RBNode;
    tMSRRbtree         *pRBNode = NULL;
    tMSRRbtree         *pRBNextNode = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pInstance = NULL;
    UINT4               u4PrevMsrArrayIndex = 0;
    UINT1               au1EntryOid[MSR_MAX_OID_LEN];
    UINT2               u2Len = 0;

    UNUSED_PARAM (u4IfIndex);
    MEMSET (au1EntryOid, 0, MSR_MAX_OID_LEN);
    if ((pInstance = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return;
    }

    if ((pOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        MsrDeAllocOid (&(pInstance));
        return;
    }

    MSR_MEMSET (&RBNode, 0, sizeof (tMSRRbtree));
    RBNode.MSRArrayIndex = 2;
    pRBNode = (tMSRRbtree *) RBTreeGetNext (gMSRRBTable, &RBNode, NULL);

    while (pRBNode != NULL)
    {
        if (gaSaveArray[pRBNode->MSRArrayIndex].u2TableIndexType != 2)
        {
            RBNode.MSRArrayIndex = pRBNode->MSRArrayIndex + 1;
            pRBNode = (tMSRRbtree *) RBTreeGetNext (gMSRRBTable, &RBNode, NULL);
            continue;
        }

        if (pRBNode->MSRArrayIndex != u4PrevMsrArrayIndex)
        {
            MSR_MEMSET (au1EntryOid, 0, MSR_MAX_OID_LEN);
            u2Len = (UINT2) ((MSR_MAX_OID_LEN >
                              (STRLEN
                               (gaSaveArray[pRBNode->MSRArrayIndex].
                                pOidString))) ? (STRLEN (gaSaveArray[pRBNode->
                                                                     MSRArrayIndex].
                                                         pOidString)) :
                             MSR_MAX_OID_LEN - 1);
            MSR_STRNCPY (au1EntryOid,
                         gaSaveArray[pRBNode->MSRArrayIndex].pOidString, u2Len);
            au1EntryOid[u2Len] = '\0';
            MSR_CLEAR_OID (pOid);
            WebnmConvertStringToOid (pOid, au1EntryOid, MSR_FALSE);
            u4PrevMsrArrayIndex = pRBNode->MSRArrayIndex;
        }

        MSR_CLEAR_OID (pInstance);
        WebnmGetInstanceOid (pOid, pRBNode->pOidValue, pInstance);

        pRBNextNode = (tMSRRbtree *) RBTreeGetNext (gMSRRBTable, pRBNode, NULL);
        if (((STRNCMP
              (au1EntryOid, MSR_CONF_SAVE_STP_PORT_PROP_OID,
               sizeof (au1EntryOid)) == 0) ||
             (STRNCMP
              (au1EntryOid, MSR_CONF_SAVE_STP_EXT_PORT_PROP_OID,
               sizeof (au1EntryOid)) == 0))
            && (pInstance->pu4_OidList[1] == u4IfIndex))

        {
            RBTreeRem (gMSRRBTable, pRBNode);
            MSRFreeRBTreeNode (pRBNode, 1);
        }

        pRBNode = pRBNextNode;
    }

    if (gIssConfigSaveInfo.IssAutoConfigSave == ISS_TRUE)
    {
        if ((gIssConfigSaveInfo.IssConfigSaveOption == ISS_CONFIG_FLASH_SAVE) ||
            (gIssConfigSaveInfo.IssConfigSaveOption == ISS_CONFIG_STARTUP_SAVE))
        {
            if (gi4MibSaveStatus != MIB_SAVE_IN_PROGRESS)
            {
                MsrSaveDatatoIncrFile (NULL, MSR_INCREMENTAL_SAVE_BUFF_SIZE);
            }
        }
        else if (gIssConfigSaveInfo.IssConfigSaveOption ==
                 ISS_CONFIG_REMOTE_SAVE)
        {
            if (gi4RemoteSaveStatus != MIB_SAVE_IN_PROGRESS)
            {
                MsrSaveDatatoIncrBuffer (NULL, MSR_INCREMENTAL_SAVE_BUFF_SIZE);
            }
        }
    }

    gu4MsrOidCount = 0;
    MsrDeAllocOid (&(pInstance));
    MsrDeAllocOid (&(pOid));
    return;
}

/*****************************************************************************/
/* Function Name      : MsrHandleVlanDel                                     */
/*                                                                           */
/* Description        : This function takes care of VLAN deletion on bridge  */
/*                      mode change                                          */
/*                                                                           */
/* Input(s)           : MSR completion status                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : MSR_SUCCESS                                          */
/*****************************************************************************/
VOID
MsrHandleVlanDel (UINT4 u4Index)
{
    tMSRRbtree          RBNode;
    tMSRRbtree         *pRBNode = NULL;
    tMSRRbtree         *pRBNextNode = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pInstance = NULL;
    UINT4               u4PrevMsrArrayIndex = 0;
    UINT1               au1EntryOid[MSR_MAX_OID_LEN];
    UINT2               u2Len = 0;

    MEMSET (au1EntryOid, 0, MSR_MAX_OID_LEN);
    if ((pInstance = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        return;
    }

    if ((pOid = MsrAllocOid ()) == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        gu4MsrOidCount = 0;
        MsrDeAllocOid (&(pInstance));
        return;
    }

    MSR_MEMSET (&RBNode, 0, sizeof (tMSRRbtree));
    RBNode.MSRArrayIndex = 2;
    pRBNode = (tMSRRbtree *) RBTreeGetNext (gMSRRBTable, &RBNode, NULL);

    while (pRBNode != NULL)
    {
        if (gaSaveArray[pRBNode->MSRArrayIndex].u2TableIndexType != 1)
        {
            RBNode.MSRArrayIndex = pRBNode->MSRArrayIndex + 1;
            pRBNode = (tMSRRbtree *) RBTreeGetNext (gMSRRBTable, &RBNode, NULL);
            continue;
        }

        if (pRBNode->MSRArrayIndex != u4PrevMsrArrayIndex)
        {
            MSR_MEMSET (au1EntryOid, 0, MSR_MAX_OID_LEN);
            u2Len = (UINT2) ((MSR_MAX_OID_LEN >
                              (STRLEN
                               (gaSaveArray[pRBNode->MSRArrayIndex].
                                pOidString))) ? (STRLEN (gaSaveArray[pRBNode->
                                                                     MSRArrayIndex].
                                                         pOidString)) :
                             MSR_MAX_OID_LEN - 1);
            MSR_STRNCPY (au1EntryOid,
                         gaSaveArray[pRBNode->MSRArrayIndex].pOidString, u2Len);
            au1EntryOid[u2Len] = '\0';
            MSR_CLEAR_OID (pOid);
            WebnmConvertStringToOid (pOid, au1EntryOid, MSR_FALSE);
            u4PrevMsrArrayIndex = pRBNode->MSRArrayIndex;
        }

        MSR_CLEAR_OID (pInstance);
        WebnmGetInstanceOid (pOid, pRBNode->pOidValue, pInstance);

        pRBNextNode = (tMSRRbtree *) RBTreeGetNext (gMSRRBTable, pRBNode, NULL);
        if (STRNCMP
            (au1EntryOid, MSR_CONF_SAVE_STATIC_VLAN_OID,
             sizeof (au1EntryOid)) == 0 && (pInstance->pu4_OidList[2] != 1)
            && (pInstance->pu4_OidList[1] == u4Index))
        {
            RBTreeRem (gMSRRBTable, pRBNode);
            MSRFreeRBTreeNode (pRBNode, 1);
        }

        pRBNode = pRBNextNode;
    }

    if (gIssConfigSaveInfo.IssAutoConfigSave == ISS_TRUE)
    {
        if ((gIssConfigSaveInfo.IssConfigSaveOption == ISS_CONFIG_FLASH_SAVE) ||
            (gIssConfigSaveInfo.IssConfigSaveOption == ISS_CONFIG_STARTUP_SAVE))
        {
            if (gi4MibSaveStatus != MIB_SAVE_IN_PROGRESS)
            {
                MsrSaveDatatoIncrFile (NULL, MSR_INCREMENTAL_SAVE_BUFF_SIZE);
            }
        }
        else if (gIssConfigSaveInfo.IssConfigSaveOption ==
                 ISS_CONFIG_REMOTE_SAVE)
        {
            if (gi4RemoteSaveStatus != MIB_SAVE_IN_PROGRESS)
            {
                MsrSaveDatatoIncrBuffer (NULL, MSR_INCREMENTAL_SAVE_BUFF_SIZE);
            }
        }
    }

    gu4MsrOidCount = 0;
    MsrDeAllocOid (&(pInstance));
    MsrDeAllocOid (&(pOid));
    return;
}

/*****************************************************************************/
/* Function Name      : MsrAuditSendLogInfo                                  */
/*                                                                           */
/* Description        : Fills the Audit Log info and posts event             */
/*                      to MSR                                               */
/* Input(s)           : pu1LogMsg       - Pointer to log message             */
/*                      u4SeverityLevel - Severity level                     */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
MsrAuditSendLogInfo (UINT1 *pu1LogMsg, UINT4 u4SeverityLevel)
{
    tAuditInfo         *pTempAudit = NULL;
    time_t              t;
    struct tm          *tp;
    CHR1               *pTime = NULL;
    UINT4               u4Len = 0;

    pTempAudit = MemAllocMemBlk (gAuditQMsgPoolId);

    if (pTempAudit == NULL)
    {
        return;
    }

    MEMSET (pTempAudit, 0, sizeof (tAuditInfo));
    MEMSET (pTempAudit->au1SyslogBuf, 0, AUDIT_LOG_SIZE);
    MEMCPY (pTempAudit->au1SyslogBuf, pu1LogMsg,
            MEM_MAX_BYTES (AUDIT_LOG_SIZE, STRLEN (pu1LogMsg)));

    t = time (NULL);
    tp = localtime (&t);
    if (tp != NULL)
    {
        pTime = asctime (tp);
        if (pTime != NULL)
        {
            u4Len = ((STRLEN (pTime) < sizeof (pTempAudit->au1Time)) ?
                     STRLEN (pTime) : sizeof (pTempAudit->au1Time) - 1);
            STRCPY (pTempAudit->au1Time, pTime);
            pTempAudit->au1Time[u4Len] = '\0';
        }
    }
    pTempAudit->i2Flag = AUDIT_EXT_CRYPTO_MSG;
    pTempAudit->u4SeverityLevel = u4SeverityLevel;
    MSRSendAuditInfo (pTempAudit);
    MemReleaseMemBlock (gAuditQMsgPoolId, (UINT1 *) pTempAudit);
    return;
}
#endif /* _MSR_C_ */
/**************************** END OF FILE *********************************/
