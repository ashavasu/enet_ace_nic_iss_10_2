/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                         */
/* $Id: msrutil.c,v 1.32 2015/06/08 04:17:52 siva Exp $*/
/*****************************************************************************/
/*    FILE  NAME            : msrutil.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           : MSR                                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*    DESCRIPTION           : This file has utility functions for config save*/
/*                            and restore through SNMP.                      */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
#ifndef _MSR_UTIL_C_
#define _MSR_UTIL_C_

#include "msrsys.h"
#include "msr.h"
#include "tftpc.h"
#include "ifmmacs.h"
#include "msrtrap.h"
#ifdef SNMP_WANTED
#include "snmptdfs.h"
#include "snmptrap.h"
#include "snmputil.h"
#include "fssnmp.h"
#endif
#ifdef QOSX_WANTED
#include "qosxtd.h"
#endif
#ifdef MBSM_WANTED
#include "mbsm.h"
#endif
extern UINT1        gArrayIndxcount[][3];
extern tMemPoolId   gAuditQMsgPoolId;
extern tOsixQId     gAuditQId;
extern INT4         gi4ClearConfigSupport;

/************************************************************************
 *  Function Name   : MsrConvertDataToString
 *  Description     : Convert the given multidata to string.
 *  Input           : pData - Pointer to multidata
 *                    u2Type - Type of element in multidata
 *  Output          : pu1String - Pointer to a string where the result will
 *                    written
 *  Returns         : None
 ************************************************************************/

VOID
MsrConvertDataToString (tSNMP_MULTI_DATA_TYPE * pData,
                        UINT1 *pu1String, UINT2 u2Type)
{
    UINT4               u4Count = 0, u4Value = 0, u4Temp = 0;
    UINT1               au1Temp[MSR_MAX_OID_LEN];
    pu1String[0] = '\0';
    switch (u2Type)
    {
        case INTEGER32:
            SPRINTF ((char *) pu1String, "%d", pData->i4_SLongValue);
            break;
        case TIMETICKS:
        case UNSIGNED32:
        case COUNTER32:
            SPRINTF ((char *) pu1String, "%u", pData->u4_ULongValue);
            break;
        case COUNTER64:
            SPRINTF ((char *) pu1String, "%u", pData->u8_Counter64Value.msn);
            SPRINTF ((char *) au1Temp, "%u", pData->u8_Counter64Value.lsn);
            MSR_STRCAT (pu1String, au1Temp);
            break;
        case IPADDRESS:
            u4Value = pData->u4_ULongValue;
            for (u4Count = 0; u4Count < (IPADDRESS_LEN - 1); u4Count++)
            {
                u4Temp = (u4Value & 0xff000000);
                u4Temp = u4Temp >> 24;
                au1Temp[0] = '\0';
                SPRINTF ((char *) au1Temp, "%u.", u4Temp);
                MSR_STRCAT (pu1String, au1Temp);
                u4Value = u4Value << BYTE_LEN;
            }
            u4Temp = (u4Value & 0xff000000);
            u4Temp = u4Temp >> 24;
            SPRINTF ((char *) au1Temp, "%u", u4Temp);
            MSR_STRCAT (pu1String, au1Temp);
            break;
        case OBJECTIDENTIFIER:
            if (pData->pOidValue->u4_Length == 0)
                break;
            for (u4Count = 0; u4Count < (pData->pOidValue->u4_Length - 1);
                 u4Count++)
            {
                au1Temp[0] = '\0';
                SPRINTF ((char *) au1Temp, "%u.",
                         pData->pOidValue->pu4_OidList[u4Count]);
                MSR_STRCAT (pu1String, au1Temp);
            }
            au1Temp[0] = '\0';
            SPRINTF ((char *) au1Temp, "%u",
                     pData->pOidValue->pu4_OidList[u4Count]);
            MSR_STRCAT (pu1String, au1Temp);
            au1Temp[0] = '\0';
            MSR_STRCAT (pu1String, au1Temp);
            break;
        case OCTETSTRING:
            if (pData->pOctetStrValue->i4_Length == 0)
                break;
            MSR_MEMCPY (pu1String, pData->pOctetStrValue->pu1_OctetList,
                        MEM_MAX_BYTES (pData->pOctetStrValue->i4_Length,
                                       (MSR_MAX_DATA_LEN - 1)));
            break;
        default:
            MSR_TRC (MSR_TRACE_TYPE, "Unknown type!\n");
    }
}

/************************************************************************
 *  Function Name   : MsrConvertStringToData
 *  Description     : Convert a data in string format to multidata.
 *  Input           : pu1String - String contain data in string format.
 *                    u2Type - Type of the data
 *  Output          : None
 *  Returns         : pointer to a multidata or null
 ************************************************************************/

tSNMP_MULTI_DATA_TYPE *
MsrConvertStringToData (UINT1 *pu1String, UINT2 u2Type, UINT4 u4Len)
{
    UINT4               u4Count1 = 0;
    UINT1               au1Tmp[MSR_MAX_OID_LEN];
    /* Instead of alloting a tSNMP_MULTI_DATA_TYPE everytime this function
     * is called (this results in the MsrAllocMultiData() function returning
     * an error when the number of octetstrings or OIDs exceeds the MAX numbers), 
     * a static variable is used. This pointer is used in the calling function.
     */
    static UINT1        u1Count = 0;
    static tSNMP_MULTI_DATA_TYPE *pData = NULL;
    if (u1Count == 0)
    {
        if ((pData = MsrAllocMultiData ()) == NULL)
        {
            return NULL;
        }
        u1Count++;
    }
    else
    {
        MSR_CLEAR_MULTIDATA (pData);
    }
    switch (u2Type)
    {
        case INTEGER32:
            pData->i4_SLongValue = ATOI (pu1String);
            pData->i2_DataType = SNMP_DATA_TYPE_INTEGER;
            break;
        case TIMETICKS:
            pData->i2_DataType = SNMP_DATA_TYPE_TIME_TICKS;
            pData->u4_ULongValue = (UINT4) ATOI (pu1String);
            break;
        case UNSIGNED32:
            pData->i2_DataType = SNMP_DATA_TYPE_UNSIGNED32;
            pData->u4_ULongValue = (UINT4) ATOI (pu1String);
            break;
        case COUNTER32:
            pData->u4_ULongValue = (UINT4) ATOI (pu1String);
            pData->i2_DataType = SNMP_DATA_TYPE_COUNTER32;
            break;
        case COUNTER64:
            pData->u8_Counter64Value.msn = (UINT4) ATOI (pu1String);
            pData->u8_Counter64Value.lsn =
                (UINT4) ATOI (pu1String + sizeof (UINT4));
            pData->i2_DataType = SNMP_DATA_TYPE_COUNTER64;
            break;
        case IPADDRESS:
            for (u4Count1 = 0; pu1String[u4Count1] != '\n'; u4Count1++)
            {
                au1Tmp[u4Count1] = pu1String[u4Count1];
            }
            au1Tmp[u4Count1] = '\0';
            WebnmConvertStringToOctet (pData->pOctetStrValue, au1Tmp);
            MEMCPY (&pData->u4_ULongValue,
                    pData->pOctetStrValue->pu1_OctetList, 4);
            pData->u4_ULongValue = OSIX_NTOHL (pData->u4_ULongValue);
            pData->i2_DataType = SNMP_DATA_TYPE_IP_ADDR_PRIM;
            break;
        case OBJECTIDENTIFIER:
            for (u4Count1 = 0; pu1String[u4Count1] != '\n'; u4Count1++)
            {
                au1Tmp[u4Count1] = pu1String[u4Count1];
            }
            au1Tmp[u4Count1] = '\0';
            WebnmConvertStringToOid (pData->pOidValue, au1Tmp,
                                     (UINT1) MSR_FAILURE);
            pData->i2_DataType = SNMP_DATA_TYPE_OBJECT_ID;
            break;
        case OCTETSTRING:
            for (u4Count1 = 0; u4Count1 < u4Len; u4Count1++)
            {
                pData->pOctetStrValue->pu1_OctetList[u4Count1]
                    = pu1String[u4Count1];
            }
            pData->pOctetStrValue->i4_Length = (INT4) u4Count1;
            pData->i2_DataType = SNMP_DATA_TYPE_OCTET_PRIM;
            break;
        default:
            MSR_TRC (MSR_TRACE_TYPE, "Unknown Type \n");
    }
    return pData;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : InitMSRArryOIDStatus                             */
/*                                                                          */
/*   Description        : This function is used to  find the number of      */
/*                       enteries with same OID and row status in gSaveArray*/
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MSR_TRUE if the save operation is successful     */
/****************************************************************************/

VOID
InitMSRArryOIDStatus (VOID)
{
    UINT4               u4Index = 0, u4Index1 = 0;
    UINT1               u1type = 0;
    for (u4Index = 0; gaSaveArray[u4Index].pOidString != NULL; u4Index++)
    {
        /* this is used for no of times the same OID repeted in gasavearray */
        gArrayIndxcount[u4Index][0] = 0;
        /* this is used for no of times the same row status 
         * OID repeted in gasavearray */
        gArrayIndxcount[u4Index][1] = 0;
        /* this is can be 0/1, */
        gArrayIndxcount[u4Index][2] = 0;
        u1type = (UINT1) gaSaveArray[u4Index].u2Type;

        if (u1type == MSR_SCALAR)
        {
            for (u4Index1 = 0; gaSaveArray[u4Index1].pOidString != NULL;
                 u4Index1++)
            {
                if (gaSaveArray[u4Index].u2Type == MSR_SCALAR)
                {
                    if (!STRCMP (gaSaveArray[u4Index].pOidString,
                                 gaSaveArray[u4Index1].pOidString))
                    {
                        gArrayIndxcount[u4Index][0] =
                            (UINT1) (gArrayIndxcount[u4Index][0] + 1);
                    }
                }
            }
        }
        else
        {
            if ((gaSaveArray[u4Index].pOidString != NULL) &&
                (gaSaveArray[u4Index].pRowStatus != NULL))
            {
                if (!STRNCMP (gaSaveArray[u4Index].pOidString,
                              gaSaveArray[u4Index].pRowStatus,
                              STRLEN (gaSaveArray[u4Index].pOidString)))
                {
                    gArrayIndxcount[u4Index][2] =
                        (UINT1) (gArrayIndxcount[u4Index][2] + 1);
                }
            }

            for (u4Index1 = 0; gaSaveArray[u4Index1].pOidString != NULL;
                 u4Index1++)
            {
                if (gaSaveArray[u4Index1].u2Type == MSR_TABLE)
                {
                    if (!STRCMP (gaSaveArray[u4Index].pOidString,
                                 gaSaveArray[u4Index1].pOidString))
                    {
                        gArrayIndxcount[u4Index][0] =
                            (UINT1) (gArrayIndxcount[u4Index][0] + 1);
                    }

                    if ((gaSaveArray[u4Index].pRowStatus != NULL) &&
                        (gaSaveArray[u4Index1].pRowStatus != NULL))
                    {
                        if (!STRCMP (gaSaveArray[u4Index].pRowStatus,
                                     gaSaveArray[u4Index1].pRowStatus))
                        {
                            gArrayIndxcount[u4Index][1] =
                                (UINT1) (gArrayIndxcount[u4Index][1] + 1);
                        }
                    }
                }
            }
        }
    }
    for (u4Index = 0; gaSaveArray[u4Index].pOidString != NULL; u4Index++)
    {
        if (gaSaveArray[u4Index].u2Type == MSR_SCALAR)
        {
            if (!STRCMP (gaSaveArray[u4Index].pOidString, MSR_AUTO_SAVE_OID))
            {
                gu4MsrAutoSaveOIDIndex = u4Index;
            }
            else if (!STRCMP (gaSaveArray[u4Index].pOidString,
                              MSR_CONF_SAVE_OPTION_OID))
            {
                gu4MsrSaveIPOptionOIDIndex = u4Index;
            }
            else if (!STRCMP (gaSaveArray[u4Index].pOidString,
                              MSR_CONF_SAVE_IPADDR_OID))
            {
                gu4MsrSaveIPAddrOIDIndex = u4Index;
            }
            else if (!STRCMP (gaSaveArray[u4Index].pOidString,
                              MSR_CONF_SAVE_FILENAME_OID))
            {
                gu4MsrSaveFileNameOIDIndex = u4Index;
            }
        }
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MSRGetArryIndexWithOID                           */
/*                                                                          */
/*   Description        : This function is used to msr array index from     */
/*                            gsavearray                                    */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MSR_TRUE if the save operation is successful     */
/*****************************************************************************/

INT4
MSRGetArryIndexWithOID (UINT1 *pOidStr, UINT4 *pu4Index, UINT1 u1Type,
                        UINT4 u4PrevIndex)
{
    UINT4               u4Index = 0;

    if (u4PrevIndex == 0)
    {
        u4Index = 0;
    }
    else
    {
        u4Index = u4PrevIndex + 1;
    }

    if (u1Type == MSR_SCALAR)
    {
        for (; gaSaveArray[u4Index].pOidString != NULL; u4Index++)
        {
            if (gaSaveArray[u4Index].u2Type == MSR_SCALAR)
            {
                if (STRCMP (pOidStr, gaSaveArray[u4Index].pOidString) == 0)
                {
                    *pu4Index = u4Index;
                    return MSR_TRUE;
                }
            }
        }
    }
    else
    {
        /* u1Type is MSR_TABLE,
         * search for rowstatus oid for tabular entries only i.e 
         * u1Type is 2 "table" in gaSaveArray*/
        for (; gaSaveArray[u4Index].pOidString != NULL; u4Index++)
        {
            if ((gaSaveArray[u4Index].u2Type == MSR_TABLE) &&
                (gaSaveArray[u4Index].pRowStatus != NULL))
            {
                if (STRCMP (pOidStr, gaSaveArray[u4Index].pRowStatus) == 0)
                {
                    *pu4Index = u4Index;
                    return MSR_TRUE;
                }
            }
        }
    }

    *pu4Index = 0;
    return MSR_FALSE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MSRGetArrayIndexWithTableOID                     */
/*                                                                          */
/*   Description        : This function is used to msr array index from     */
/*                            gsavearray                                    */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MSR_TRUE if the save operation is successful     */
/*****************************************************************************/
INT4
MSRGetArrayIndexWithTableOID (UINT1 *pOid, UINT4 *pu4Index, UINT1 u1Type,
                              UINT4 u4PrevIndex)
{
    UINT4               u4Index = 0;

    if (u1Type != MSR_TABLE)
    {
        *pu4Index = 0;
        return MSR_FALSE;
    }

    if (u4PrevIndex == 0)
    {
        u4Index = 0;
    }
    else
    {
        u4Index = u4PrevIndex + 1;
    }

    /* u1Type is MSR_TABLE,
     * search for rowstatus oid for tabular entries only i.e 
     * u1Type is 2 "table" in gaSaveArray*/
    for (; gaSaveArray[u4Index].pOidString != NULL; u4Index++)
    {
        if ((gaSaveArray[u4Index].u2Type == MSR_TABLE))
        {
            if (STRCMP (pOid, gaSaveArray[u4Index].pOidString) == 0)
            {
                *pu4Index = u4Index;
                return MSR_TRUE;
            }
        }
    }

    *pu4Index = 0;
    return MSR_FALSE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MSRFreeRBTreeNode                                */
/*                                                                          */
/*    Description        : This function is used to erase thei              */
/*                         configuration saved in the gpu1StrFlash and delete 
                           all the  RBT node.                               */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
MSRFreeRBTreeNode (tMSRRbtree * pRBNode, UINT4 u4arg)
{
    UNUSED_PARAM (u4arg);

    if (gMSRIncrSaveFlag == ISS_TRUE)
    {
        MSR_RELEASE_INCR_BUDDY_BLK (pRBNode);
        MSR_INCR_RB_FREE_MEM_BLOCK (pRBNode);
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MSRCompareRBNodes                                */
/*                                                                          */
/*   Description        : This function is used to OID value at two node    */
/*                        recieve in update event                           */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MSR_TRUE if the save operation is successful     */
/*****************************************************************************/

UINT4
MSRCompareRBNodes (tMSRRbtree * RBNode1, tMSRRbtree * RBNode2)
{
    UINT4               u4Max = 0;
    UINT4               u4Count = 0;
    tSNMP_OID_TYPE     *pOID1 = RBNode1->pOidValue;
    tSNMP_OID_TYPE     *pOID2 = RBNode2->pOidValue;

    if (RBNode1->MSRArrayIndex < RBNode2->MSRArrayIndex)
    {
        return ((UINT4) -1);
    }
    else if (RBNode1->MSRArrayIndex > RBNode2->MSRArrayIndex)
    {
        return +1;
    }
    /* now both the nodes having same array index now 
     * check for MSRRowStatusType */
    if (RBNode1->MSRRowStatusType < RBNode2->MSRRowStatusType)
    {
        return ((UINT4) -1);
    }
    else if (RBNode1->MSRRowStatusType > RBNode2->MSRRowStatusType)
    {
        return +1;
    }

    /* now both the nodes having same MSRRowStatusType now 
     * check for OID value */
    if (pOID1->u4_Length < pOID2->u4_Length)
    {
        u4Max = pOID1->u4_Length;
    }
    else
    {
        u4Max = pOID2->u4_Length;
    }

    for (u4Count = 0; u4Count < u4Max; (u4Count)++)
    {
        if (pOID1->pu4_OidList[u4Count] < pOID2->pu4_OidList[u4Count])
        {
            return ((UINT4) -1);
        }
        else if (pOID1->pu4_OidList[u4Count] > pOID2->pu4_OidList[u4Count])
        {
            return +1;
        }
    }
    if (pOID1->u4_Length < pOID2->u4_Length)
    {
        return ((UINT4) -1);
    }
    else if (pOID1->u4_Length > pOID2->u4_Length)
    {
        return +1;
    }
    else
    {
        return 0;
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MSRSearchNodeinRBTree                            */
/*                                                                          */
/*   Description        : This function is used to search a node in RB tree */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MSR_TRUE if the save operation is successful     */
/*****************************************************************************/
INT4
MSRSearchNodeinRBTree (tSNMP_OID_TYPE * pOid,
                       UINT4 u4MSRArrayIndex,
                       UINT4 u4MSRRowStatusType, tMSRRbtree ** psearchRes)
{
    tMSRRbtree          key;
    key.pOidValue = pOid;
    key.MSRArrayIndex = u4MSRArrayIndex;
    key.MSRRowStatusType = u4MSRRowStatusType;
    *psearchRes = (tMSRRbtree *) RBTreeGet (gMSRRBTable, &key);
    if (*psearchRes == NULL)
    {
        /* Element not present in the key */
        return MSR_FALSE;
    }
    return MSR_TRUE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MSRDeleteNodefromRBTree                          */
/*                                                                          */
/*   Description        : This function is used to delete a node in RB tree */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MSR_TRUE if the save operation is successful     */
/*****************************************************************************/
VOID
MSRDeleteNodefromRBTree (tSNMP_OID_TYPE * pOid, UINT4 u4MSRArrayIndex,
                         UINT4 u4MSRRowStatusType)
{
    tMSRRbtree         *ptobefreed = NULL;
    tMSRRbtree          key;

    key.pOidValue = pOid;
    key.MSRArrayIndex = u4MSRArrayIndex;
    key.MSRRowStatusType = u4MSRRowStatusType;

    ptobefreed = RBTreeRem (gMSRRBTable, &key);
    if (ptobefreed == NULL)
    {
        /* Element not present in the key */
        return;
    }
    /* free the memory allocated for RBNode */
    MSRFreeRBTreeNode (ptobefreed, 1);
}

/************************************************************************
 *  Function Name   : MSRInsertRBNodeinRBTree
 *  Description     : Allocates memory for RB tree node updates the data
 *                   into that node and inserts into RB tree.
 *  Input           : None
 *  Output          : None
 *  Returns         : TRUE, Incase of any failure returns FALSE
 ************************************************************************/
INT4
MSRInsertRBNodeinRBTree (tSNMP_OID_TYPE * pOid,
                         tSNMP_MULTI_DATA_TYPE * pData,
                         UINT4 u4MSRArrayIndex, UINT4 u4MSRRowStatusType,
                         UINT1 u1IsDataToBeSaved)
{
    tMSRRbtree         *pRBNode = NULL;
    UINT4               u4Result, u4pDataSize = 0;
    UINT1              *pTemp = NULL;
    UINT1              *pu1Data = NULL;

    if (MSR_DATA_ALLOC_MEM_BLOCK (pu1Data) == NULL)
    {
        return MSR_FALSE;
    }
    MSR_MEMSET (pu1Data, 0, MSR_MAX_DATA_LEN);
    pTemp = pu1Data;

    sprintf ((char *) pTemp, "%2d\t", pData->i2_DataType);
    pTemp += MSR_STRLEN (pTemp);

    if (pData->i2_DataType == OCTETSTRING)
    {
        sprintf ((char *) pTemp, "%4d\t",
                 (int) pData->pOctetStrValue->i4_Length);
        pTemp += MSR_STRLEN (pTemp);
    }
    u4pDataSize = MSR_STRLEN (pu1Data);

    MsrConvertDataToString (pData, pTemp, (UINT2) pData->i2_DataType);

    if (pData->i2_DataType == OCTETSTRING)
    {
        pTemp += pData->pOctetStrValue->i4_Length;
        u4pDataSize += (UINT4) pData->pOctetStrValue->i4_Length;
    }
    else
    {
        pTemp += MSR_STRLEN (pTemp);
        u4pDataSize = MSR_STRLEN (pu1Data);
    }
    sprintf ((char *) pTemp, "\n");
    u4pDataSize += 1;

    /* allocate memory for pRBNode, store the values in to 
     * pRBNode and add it to RBTree */
    pRBNode = MsrAllocRBNode (u4pDataSize, pOid->u4_Length);
    if (pRBNode == NULL)
    {
        MSR_TRC (MSR_TRACE_TYPE, "Insufficient memory!!!\n");
        MSR_DATA_FREE_MEM_BLOCK (pu1Data);
        return MSR_FALSE;
    }

    WebnmCopyOid (pRBNode->pOidValue, pOid);
    pRBNode->MSRArrayIndex = u4MSRArrayIndex;
    pRBNode->MSRRowStatusType = u4MSRRowStatusType;
    pRBNode->u4pDataSize = u4pDataSize;
    MSR_MEMSET (pRBNode->pData, 0, u4pDataSize + 1);
    MSR_MEMCPY (pRBNode->pData, pu1Data, u4pDataSize);

    /* Set the u1IsDataToBeSaved flag as MSR_TRUE initially */
    pRBNode->u1IsDataToBeSaved = u1IsDataToBeSaved;

    MSR_DATA_FREE_MEM_BLOCK (pu1Data);
    u4Result = RBTreeAdd (gMSRRBTable, pRBNode);
    if (u4Result == RB_FAILURE)
    {
        /* free the memory allocated for RBNode */
        MSRFreeRBTreeNode (pRBNode, 1);
        return MSR_FALSE;
    }
    return MSR_TRUE;
}

/************************************************************************
 *  Function Name   : MsrAllocRBNode
 *  Description     : Allocates memory for RB tree node
 *  Input           : None
 *  Output          : None
 *  Returns         : Allocated RB node  pointer or null
 ************************************************************************/
tMSRRbtree         *
MsrAllocRBNode (UINT4 u4DataSize, UINT4 OidLength)
{
    tMSRRbtree         *pRBNode = NULL;
    static tMSRRbtree   RbNode;
    static tSNMP_OID_TYPE Oid;
    static UINT4        au4Oid[MSR_MAX_OID_LEN];
    static UINT1        au1Data[MSR_MAX_DATA_LEN];

    if (gMSRIncrSaveFlag == ISS_TRUE)
    {
        MSR_INCR_RB_ALLOC_MEM_BLOCK (pRBNode);

        if (pRBNode == NULL)
        {
            return NULL;
        }

        MSR_ALLOC_INCR_BUDDY_BLK ((u4DataSize + 1),
                                  (sizeof (UINT4) * (OidLength + 2)), pRBNode);

        if ((pRBNode->pData == NULL)
            || (pRBNode->pOidValue->pu4_OidList == NULL))
        {
            MSR_RELEASE_INCR_BUDDY_BLK (pRBNode);
            MSR_INCR_RB_FREE_MEM_BLOCK (pRBNode);
            return NULL;
        }
        MSR_MEMSET (pRBNode->pData, 0, (u4DataSize + 1));
        MSR_MEMSET (pRBNode->pOidValue->pu4_OidList, 0,
                    (sizeof (UINT4) * (OidLength + 2)));

    }
    else
    {
        MSR_MEMSET (&RbNode, 0, sizeof (tMSRRbtree));
        MSR_MEMSET (au1Data, 0, MSR_MAX_DATA_LEN);
        MSR_MEMSET (au4Oid, 0, (sizeof (UINT4) * MSR_MAX_OID_LEN));
        RbNode.pData = au1Data;
        Oid.pu4_OidList = au4Oid;
        RbNode.pOidValue = &Oid;
        pRBNode = &RbNode;
    }
    return pRBNode;
}

/************************************************************************
 *  Function Name   :MSRModifyRBNodeinRBTree 
 *  Description     : This function is used to modify the data in the already 
 existing node.
 *  Input           : None
 *  Output          : None
 *  Returns         : TRUE, Incase of any failure returns FALSE
 ************************************************************************/
INT4
MSRModifyRBNodeinRBTree (tSNMP_MULTI_DATA_TYPE * pModData, tMSRRbtree * pRBNode)
{
    UINT1              *pTemp = NULL;
    UINT1              *pu1Data = NULL;
    UINT4               u4pDataSize = 0;
    INT4                i4RetVal = MSR_TRUE;

    if (MSR_DATA_ALLOC_MEM_BLOCK (pu1Data) == NULL)
    {
        return MSR_FALSE;
    }
    pTemp = pu1Data;
    MSR_MEMSET (pu1Data, 0, MSR_MAX_DATA_LEN);

    sprintf ((char *) pTemp, "%2d\t", pModData->i2_DataType);
    pTemp += MSR_STRLEN (pTemp);

    if (pModData->i2_DataType == OCTETSTRING)
    {
        sprintf ((char *) pTemp, "%4d\t",
                 (int) pModData->pOctetStrValue->i4_Length);
        pTemp += MSR_STRLEN (pTemp);
    }
    u4pDataSize = MSR_STRLEN (pu1Data);

    MsrConvertDataToString (pModData, pTemp, (UINT2) pModData->i2_DataType);

    if (pModData->i2_DataType == OCTETSTRING)
    {
        pTemp += pModData->pOctetStrValue->i4_Length;
        u4pDataSize += (UINT4) pModData->pOctetStrValue->i4_Length;

    }
    else
    {
        pTemp += MSR_STRLEN (pTemp);
        u4pDataSize = MSR_STRLEN (pu1Data);
    }
    sprintf ((char *) pTemp, "\n");
    u4pDataSize += 1;

    MsrUpdateRBDataSize (pRBNode, (u4pDataSize + 1));

    if (pRBNode->pData == NULL)
    {
        i4RetVal = MSR_FALSE;
    }
    else
    {
        MSR_MEMSET (pRBNode->pData, 0, u4pDataSize + 1);
        MSR_MEMCPY (pRBNode->pData, pu1Data, u4pDataSize);
        pRBNode->u4pDataSize = u4pDataSize;
    }
    MSR_DATA_FREE_MEM_BLOCK (pu1Data);
    if (i4RetVal == MSR_FALSE)
    {
        MSRDeleteNodefromRBTree (pRBNode->pOidValue,
                                 pRBNode->MSRArrayIndex,
                                 pRBNode->MSRRowStatusType);
    }
    return i4RetVal;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrProcessMibSaveEvt                             */
/*                                                                          */
/*   Description        : This function is used to comapre the data recieved*/
/*                        in update event with that of default value        */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MSR_TRUE if the save operation is successful     */
/****************************************************************************/
INT4
MSRCompareSnmpMultiDataValues (tSNMP_MULTI_DATA_TYPE * pSnmpData,
                               tSNMP_MULTI_DATA_TYPE * defData)
{
    INT4                i4Result = MSR_FAILURE;
    UINT4               u4Max = 0;
    UINT4               u4Count = 0;
    tSNMP_OCTET_STRING_TYPE *pOctetStrValue = NULL;

    switch (pSnmpData->i2_DataType)
    {
        case SNMP_DATA_TYPE_INTEGER32:
            if (pSnmpData->i4_SLongValue == defData->i4_SLongValue)
            {
                i4Result = MSR_SUCCESS;
            }
            break;
        case SNMP_DATA_TYPE_COUNTER32:
        case SNMP_DATA_TYPE_GAUGE32:
        case SNMP_DATA_TYPE_TIME_TICKS:
        case SNMP_DATA_TYPE_IP_ADDR_PRIM:
            if (pSnmpData->u4_ULongValue == defData->u4_ULongValue)
            {
                i4Result = MSR_SUCCESS;
            }
            break;
        case SNMP_DATA_TYPE_COUNTER64:
            if ((pSnmpData->u8_Counter64Value.msn ==
                 defData->u8_Counter64Value.msn) &&
                (pSnmpData->u8_Counter64Value.lsn ==
                 defData->u8_Counter64Value.lsn))
            {
                i4Result = MSR_SUCCESS;
            }
            break;
        case SNMP_DATA_TYPE_OBJECT_ID:
            if (pSnmpData->pOidValue->u4_Length < defData->pOidValue->u4_Length)
            {
                u4Max = pSnmpData->pOidValue->u4_Length;
            }
            else
            {
                u4Max = defData->pOidValue->u4_Length;
            }

            for (u4Count = 0; u4Count < u4Max; (u4Count)++)
            {
                if (pSnmpData->pOidValue->pu4_OidList[u4Count] <
                    defData->pOidValue->pu4_OidList[u4Count])
                {
                    return MSR_FAILURE;
                }
                else if (pSnmpData->pOidValue->pu4_OidList[u4Count] >
                         defData->pOidValue->pu4_OidList[u4Count])
                {
                    return MSR_FAILURE;
                }
            }
            if (pSnmpData->pOidValue->u4_Length < defData->pOidValue->u4_Length)
            {
                return MSR_FAILURE;
            }
            else if (pSnmpData->pOidValue->u4_Length >
                     defData->pOidValue->u4_Length)
            {
                return MSR_FAILURE;
            }
            else
            {
                return MSR_SUCCESS;
            }
            break;
        case SNMP_DATA_TYPE_OCTET_PRIM:
        case SNMP_DATA_TYPE_OPAQUE:
            pOctetStrValue = pSnmpData->pOctetStrValue;
            if (MSR_MEMCMP ((char *) pOctetStrValue->pu1_OctetList,
                            (char *) defData->pOctetStrValue->pu1_OctetList,
                            pSnmpData->pOctetStrValue->i4_Length) == 0)
            {
                i4Result = MSR_SUCCESS;
            }
            break;
        default:
            MSR_TRC (MSR_TRACE_TYPE, "Invalid data type........\n");
            i4Result = MSR_FAILURE;
            break;
    }
    return i4Result;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MSRFreeSnmpDataRcvfromQ                          */
/*                                                                          */
/*   Description        : This function is used to free snmp data structure */
/*                        recieve in update event                           */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/*****************************************************************************/
VOID
MSRFreeSnmpDataRcvfromQ (tMSRUpdatedata * pData)
{
    free_MultiData (pData->pSnmpData);
    free_MultiIndex (pData->pSnmpindx, pData->pSnmpindx->u4No);
    free_MultiOid (pData->pOid);
    MemReleaseMemBlock (gMsrQUpdDataPoolId, (UINT1 *) pData);
}

/************************************************************************
 *  Function Name   : MSRUpdateMemFree
 *  Description     : Freeing the memory alloced for pMultidata, pMultiIndex 
 *                    and pOid
 *  Input           :
 *  Output          : 
 *  Returns         : None
 ************************************************************************/

VOID
MSRUpdateMemFree (tSnmpIndex * pMultiIndex,
                  tSNMP_MULTI_DATA_TYPE * pMultiData, UINT1 *pOid)
{
    free_MultiData (pMultiData);
    free_MultiIndex (pMultiIndex, pMultiIndex->u4No);
    if (pOid != NULL)
    {
        free_MultiOid (pOid);
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrUpdateRBDataSize                              */
/*                                                                          */
/*    Description        : This function is called when the RBtree nodes    */
/*                          are modified to accomodate the configuration    */
/*                          change. It releases the block of memory         */
/*                          allocated for saving data in the RB node and    */
/*                          allocated memory for the new given size.        */
/*                                                                          */
/*    Input(s)           : pRBNode - The RB node for which the data block is*/
/*                                    modified.                             */
/*                         u4Size  - New size of the data block.            */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/*                                                                          */
/****************************************************************************/
VOID
MsrUpdateRBDataSize (tMSRRbtree * pRBNode, UINT4 u4Size)
{
    MSR_RELEASE_INCR_BUDDY_DATA_BLK (pRBNode);
    MSR_ALLOC_INCR_BUDDY_DATA_BLK (pRBNode, u4Size);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrGetContextPortList                            */
/*                                                                          */
/*    Description        : This function obtains the list of the ports      */
/*                          are mapped to the given context from the vcm.   */
/*                                                                          */
/*    Input(s)           : u4ContextId - Context Id                         */
/*                                                                          */
/*    Output(s)          : PortList    - PortList for the given context     */
/*                                                                          */
/*    Returns            : MSR_SUCCESS/MSR_FAILURE                          */
/*                                                                          */
/****************************************************************************/
INT4
MsrGetContextPortList (UINT4 u4ContextId, tPortList PortList)
{
    return (VcmGetContextPortList (u4ContextId, PortList));

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrCopySnmpMultiDataValues                       */
/*                                                                          */
/*   Description        : This function is used to copy the multidata       */
/*                        values                                            */
/*    Input(s)           : pSrcSnmpData, pDstSnmpData                       */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : NOne.                                            */
/****************************************************************************/
VOID
MSRCopySnmpMultiDataValues (tSNMP_MULTI_DATA_TYPE * pSrcSnmpData,
                            tSNMP_MULTI_DATA_TYPE * pDstSnmpData)
{
    UINT4               u4Count = 0;

    pDstSnmpData->i2_DataType = pSrcSnmpData->i2_DataType;
    switch (pSrcSnmpData->i2_DataType)
    {
        case SNMP_DATA_TYPE_INTEGER32:
            pDstSnmpData->i4_SLongValue = pSrcSnmpData->i4_SLongValue;
            break;
        case SNMP_DATA_TYPE_COUNTER32:
        case SNMP_DATA_TYPE_GAUGE32:
        case SNMP_DATA_TYPE_TIME_TICKS:
        case SNMP_DATA_TYPE_IP_ADDR_PRIM:
            pDstSnmpData->u4_ULongValue = pSrcSnmpData->u4_ULongValue;
            break;
        case SNMP_DATA_TYPE_COUNTER64:
            pDstSnmpData->u8_Counter64Value.msn =
                pSrcSnmpData->u8_Counter64Value.msn;
            pDstSnmpData->u8_Counter64Value.lsn =
                pSrcSnmpData->u8_Counter64Value.lsn;
            break;
        case SNMP_DATA_TYPE_OBJECT_ID:
            pDstSnmpData->pOidValue->u4_Length =
                pSrcSnmpData->pOidValue->u4_Length;

            for (u4Count = 0; u4Count < pSrcSnmpData->pOidValue->u4_Length;
                 (u4Count)++)
            {
                pDstSnmpData->pOidValue->pu4_OidList[u4Count] =
                    pSrcSnmpData->pOidValue->pu4_OidList[u4Count];
            }
            break;
        case SNMP_DATA_TYPE_OCTET_PRIM:
        case SNMP_DATA_TYPE_OPAQUE:
            pDstSnmpData->pOctetStrValue->i4_Length =
                pSrcSnmpData->pOctetStrValue->i4_Length;

            if (pSrcSnmpData->pOctetStrValue->i4_Length > 0)
            {
                MSR_MEMCPY (pDstSnmpData->pOctetStrValue->pu1_OctetList,
                            pSrcSnmpData->pOctetStrValue->pu1_OctetList,
                            pSrcSnmpData->pOctetStrValue->i4_Length);
            }

            break;
        default:
            MSR_TRC (MSR_TRACE_TYPE, "Invalid data type........\n");
            break;
    }
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MSRSendAuditInfo                                 */
/*                                                                          */
/*   Description         : This function retrieves the Audit Info from the  */
/*                         Application and Sends it through MSR Queue       */
/*    Input(s)           : AuditInfo Struct Pointer from the Application    */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : MSR_SUCCESS / MSR_FAILURE                        */
/****************************************************************************/
INT1
MSRSendAuditInfo (tAuditInfo * pTempAuditInfo)
{
    tAuditInfo         *pAuditInfo = NULL;

    pAuditInfo = MemAllocMemBlk (gAuditQMsgPoolId);
    if (pAuditInfo == NULL)
    {
        return MSR_FAILURE;
    }

    MEMSET (pAuditInfo, 0, sizeof (tAuditInfo));
    MEMCPY (pAuditInfo, pTempAuditInfo, sizeof (tAuditInfo));

    if (OsixQueSend (gAuditQId, (UINT1 *) &pAuditInfo, OSIX_DEF_MSG_LEN)
        != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (gAuditQMsgPoolId, (UINT1 *) pAuditInfo);
        return (MSR_FAILURE);
    }
    if (MsrSendEventToMsrTask (AUDIT_EVENT) == MSR_FAILURE)
    {
        MemReleaseMemBlock (gAuditQMsgPoolId, (UINT1 *) pAuditInfo);
        return (MSR_FAILURE);
    }

    return MSR_SUCCESS;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrUtilConvertMultiDataToString                  */
/*                                                                          */
/*   Description        : This function is used to copy the multidata       */
/*                        values to the given string.                       */
/*    Input(s)           : pData - Data to be converted.                    */
/*                                                                          */
/*    Output(s)          : pu1Data - String format of the data.             */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/

VOID
MsrUtilConvertMultiDataToString (tSNMP_MULTI_DATA_TYPE * pData,
                                 UINT1 *pu1Data, UINT4 *pu4DataSize)
{
    UINT1              *pTemp = NULL;
    UINT4               u4pDataSize = 0;

    pTemp = pu1Data;
    MSR_MEMSET (pTemp, 0, MSR_MAX_DATA_LEN);

    sprintf ((char *) pTemp, "%2d\t", pData->i2_DataType);
    pTemp += MSR_STRLEN (pTemp);

    if (pData->i2_DataType == OCTETSTRING)
    {
        sprintf ((char *) pTemp, "%4d\t",
                 (int) pData->pOctetStrValue->i4_Length);
        pTemp += MSR_STRLEN (pTemp);
    }
    u4pDataSize = MSR_STRLEN (pu1Data);

    MsrConvertDataToString (pData, pTemp, (UINT2) pData->i2_DataType);

    if (pData->i2_DataType == OCTETSTRING)
    {
        pTemp += pData->pOctetStrValue->i4_Length;
        u4pDataSize += (UINT4) pData->pOctetStrValue->i4_Length;

    }
    else
    {
        pTemp += MSR_STRLEN (pTemp);
        u4pDataSize = MSR_STRLEN (pu1Data);
    }
    sprintf ((char *) pTemp, "\n");
    u4pDataSize += 1;
    *pu4DataSize = u4pDataSize;
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MsrUtilFillRbNode                                */
/*                                                                          */
/*    Description        : This function compares the given data with the   */
/*                         default data for the object and if they differ   */
/*                         adds the given data to the RBNode.               */
/****************************************************************************/
INT4
MsrUtilFillRbNode (tMSRRbtree * pRBNode, UINT4 u4Index, tSNMP_OID_TYPE * pOid,
                   tSNMP_MULTI_DATA_TYPE * pData,
                   UINT4 u4RSType, INT4 i4SaveVal)
{
    UINT4               u4DataLen;

    MSR_CLEAR_OID (pRBNode->pOidValue);
    MSR_MEMSET (pRBNode->pData, 0, MSR_MAX_DATA_LEN);

    MsrUtilConvertMultiDataToString (pData, pRBNode->pData, &u4DataLen);
    SNMPCopyOid (pRBNode->pOidValue, pOid);
    pRBNode->MSRArrayIndex = u4Index;
    pRBNode->MSRRowStatusType = u4RSType;
    pRBNode->u4pDataSize = u4DataLen;
    if (i4SaveVal == MSR_VOLATILE_SAVE)
    {
        pRBNode->u1IsDataToBeSaved = MSR_FALSE;
    }
    else
    {
        pRBNode->u1IsDataToBeSaved = MSR_TRUE;
    }

    return MSR_SUCCESS;
}

/************************************************************************
 *  Function Name   : MsrConvertOidToString 
 *  Description     : Convert the given oid to string
 *  Input           : pOid - Pointer to OID List
 *  Output          : pu1String - String contains OID in string format
 *  Returns         : None
 ************************************************************************/
VOID
MsrConvertOidToString (tSNMP_OID_TYPE * pOid, UINT1 *pu1String)
{
    UINT4               u4Count;
    UINT1               au1Array[MSR_MAX_STR_LEN];
    pu1String[0] = '\0';
    au1Array[0] = '\0';
    if (pOid->u4_Length == 0)
    {
        return;
    }
    for (u4Count = 0; u4Count < (pOid->u4_Length - 1); u4Count++)
    {
        SPRINTF ((char *) au1Array, "%u.", pOid->pu4_OidList[u4Count]);
        MSR_STRCAT (pu1String, au1Array);
    }
    SPRINTF ((char *) au1Array, "%u", pOid->pu4_OidList[u4Count]);
    MSR_STRCAT (pu1String, au1Array);
    return;
}

/************************************************************************
 *  Function Name   : MsrGetInstanceOid 
 *  Description     : Get the Instance OID from the Given OID and Table
 *                    element actual OID
 *  Input           : pCurOid - Table element Actual OID
 *                    pNextOid - Table element OID with Instance
 *  Output          : pInstance - Instance OID Pointer 
 *  Returns         : None
 ************************************************************************/
VOID
MsrGetInstanceOid (tSNMP_OID_TYPE * pCurOid,
                   tSNMP_OID_TYPE * pNextOid, tSNMP_OID_TYPE * pInstance)
{
    UINT4               u4Count = 0, u4Index = 0;
    pInstance->u4_Length = 0;
    if (pNextOid->u4_Length > pCurOid->u4_Length)
    {
        if (MEMCMP (pCurOid->pu4_OidList, pNextOid->pu4_OidList,
                    (pCurOid->u4_Length * 4)) != 0)
        {
            return;
        }
        for (u4Count = pCurOid->u4_Length; u4Count < pNextOid->u4_Length;
             u4Count++, u4Index++)
        {
            pInstance->pu4_OidList[u4Index] = pNextOid->pu4_OidList[u4Count];
            pInstance->u4_Length++;
        }
    }
}

/************************************************************************
 *  Function Name   : MsrAppendOid      
 *  Description     : This function appends the given Source oid to the
 *                    end of the destination oid.
 *  Input           : pDest - Destination Oid            
 *                    pSrc  - Source Oid                       
 *  Output          : pDest - Updated destination Oid  
 *  Returns         : None
 ************************************************************************/
VOID
MsrAppendOid (tSNMP_OID_TYPE * pDest, tSNMP_OID_TYPE * pSrc)
{
    UINT4               u4Count = 0;

    for (u4Count = 0; u4Count < pSrc->u4_Length; u4Count++)
    {
        pDest->pu4_OidList[pDest->u4_Length] = pSrc->pu4_OidList[u4Count];
        pDest->u4_Length++;
    }
}

/************************************************************************
 *  Function Name   : MsrUtilChkAuditFileSize                           *
 *  Description     : This function Checks for the file size and sends  *
 *                    the trap message if required.                     *
 *  Input           : u4Size - Current log size                         *
 *                    pu1FileName - Name of the file being processed    *
 *                    i2Flag - flag                                     *
 *  Output          : None                                              *
 *  Returns         : MSR_FAILURE/MSR_SUCCESS                           *
 ************************************************************************/
INT4
MsrUtilChkAuditFileSize (UINT4 u4Size, UINT1 *pu1FileName, INT2 i2Flag)
{
    UINT4               u4ThresholdValue = AUDIT_ZERO;
    UINT4               u4StorageChkFlag = AUDIT_ZERO;
    UINT4               u4OverWriteChkFlag = AUDIT_ZERO;
    tAuditTrapMsg       TrapMsg;
    UINT1               au1DesFile[ISS_AUDIT_FILE_NAME_LENGTH];
    FILE               *pFp = NULL;

    MSR_MEMSET (au1DesFile, 0, ISS_AUDIT_FILE_NAME_LENGTH);
    u4ThresholdValue = (UINT4) ((gu4AuditLogSize *
                                 gu4AuditLogSizeThreshold) / 100);

    /* A trap is sent when the log size exceeds a certain
     * percentage of the maximum value */
    if (u4Size >= u4ThresholdValue)
    {
        MSR_MEMSET (&TrapMsg, 0, sizeof (tAuditTrapMsg));
        STRNCPY (TrapMsg.au1IssLogFileName, pu1FileName,
                 (sizeof (TrapMsg.au1IssLogFileName) - 1));
        TrapMsg.au1IssLogFileName[(sizeof (TrapMsg.au1IssLogFileName) - 1)] =
            '\0';
        TrapMsg.u4Event = AUDIT_TRAP_LOG_SIZE_THRESHOLD_HIT;
        UtlGetTimeStr (TrapMsg.ac1DateTime);
        AuditTrapSendNotifications (&TrapMsg);
        MSR_TRC (MSR_TRACE_TYPE, "Audit Information  - "
                 "Log size has hit its threshold value\n");
    }
    /* A trap is sent when the log size exceeds the maximum size */
    if (u4Size >= gu4AuditLogSize)
    {
        MSR_MEMSET (&TrapMsg, 0, sizeof (tAuditTrapMsg));
        STRNCPY (TrapMsg.au1IssLogFileName, pu1FileName,
                 (sizeof (TrapMsg.au1IssLogFileName) - 1));
        TrapMsg.au1IssLogFileName[(sizeof (TrapMsg.au1IssLogFileName) - 1)] =
            '\0';

        TrapMsg.u4Event = AUDIT_TRAP_FILE_SIZE_EXCEEDED;
        UtlGetTimeStr (TrapMsg.ac1DateTime);
        AuditTrapSendNotifications (&TrapMsg);
        MSR_TRC (MSR_TRACE_TYPE, "Audit Information  - "
                 "Log size has hit its maximum value\n");

        /* "Stop logging when the storage space is exhausted" is the cusom
         * requirement and hence this check is placed here. In case the 
         * program is for custom specific, file contents will not be flushed*/
        IssCustCheckLogOption (&u4StorageChkFlag, &u4OverWriteChkFlag);
        if (ISS_FALSE == u4OverWriteChkFlag)
        {
            return MSR_FAILURE;
        }
        SPRINTF ((CHR1 *) au1DesFile, "%s%s", FLASH, "config.bkp");
        if (AUDIT_SYSLOG_MSG == i2Flag)
        {
            if (STRCMP (pu1FileName, gau1IssAuditLogFileName) != 0)
            {
                if (STRLEN (pu1FileName) + STRLEN (".bkp") <
                    ISS_AUDIT_FILE_NAME_LENGTH)
                {
                    SNPRINTF ((CHR1 *) au1DesFile,
                              (STRLEN (pu1FileName) + STRLEN (".bkp")), "%s%s",
                              pu1FileName, ".bkp");
                }

            }
        }
        if ((pFp = FOPEN ((CONST CHR1 *) au1DesFile, "w")) == NULL)
        {
            return MSR_FAILURE;
        }
        fclose (pFp);
        issCopyLocalFile (pu1FileName, au1DesFile);
        if ((pFp = FOPEN ((CONST CHR1 *) pu1FileName, "w")) == NULL)
        {
            MSR_MEMSET (&TrapMsg, 0, sizeof (tAuditTrapMsg));
            STRCPY (TrapMsg.au1IssLogFileName, pu1FileName);
            TrapMsg.u4Event = AUDIT_TRAP_OPEN_FAILED;
            UtlGetTimeStr (TrapMsg.ac1DateTime);
            AuditTrapSendNotifications (&TrapMsg);
            MSR_TRC (MSR_TRACE_TYPE, "Audit Information  - "
                     "File Open Operation Failed\n");
            return MSR_FAILURE;
        }
        fclose (pFp);
    }
    return MSR_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : MsrUtilIsClrCfgSupported                             */
/*                                                                           */
/* Description        : This Function checks whether Clear configuration     */
/*                      feature is supported for Package registered with SNMP*/
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : MSR_SUCCESS/MSR_FAILURE                              */
/*****************************************************************************/
INT4
MsrUtilIsClrCfgSupported (VOID)
{
    tSNMP_OCTET_STRING_TYPE *pOctetStrValue = NULL;
#ifdef SNMP_3_WANTED
    UINT4               u4Index = 0;
    INT4                i4ModIndex = 0;
    INT4                i4MatchFound = ISS_FALSE;
#endif

    pOctetStrValue = allocmem_octetstring (SNMP_MAX_OCTETSTRING_SIZE);
    if (pOctetStrValue == NULL)
    {
        return MSR_FAILURE;
    }
    MSR_MEMSET (pOctetStrValue->pu1_OctetList, 0, SNMP_MAX_OCTETSTRING_SIZE);
    pOctetStrValue->i4_Length = 0;
#ifdef SNMP_3_WANTED
    while (SNMP_SUCCESS == SnmpGetSysOrDescriptor (i4ModIndex, pOctetStrValue))
    {
        u4Index = 0;
        while (gaClrPackageArray[u4Index] != NULL)
        {
            if (STRNCMP
                (pOctetStrValue->pu1_OctetList,
                 (UINT1 *) gaClrPackageArray[u4Index],
                 STRLEN (pOctetStrValue->pu1_OctetList)) == 0)
            {
                i4MatchFound = ISS_TRUE;
                break;
            }
            u4Index++;
        }

        if (i4MatchFound == ISS_FALSE)
        {
            gi4ClearConfigSupport = ISS_FALSE;
            free_octetstring (pOctetStrValue);
            return MSR_FAILURE;
        }
        i4MatchFound = ISS_FALSE;
        MSR_CLEAR_OCTET_STRING (pOctetStrValue);
        i4ModIndex++;
    }
#endif

    gi4ClearConfigSupport = ISS_TRUE;
    free_octetstring (pOctetStrValue);
    return MSR_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : MsrUtilCallBackRegister                              */
/*                                                                           */
/* Description        : This function registers the call backs from          */
/*                      application                                          */
/*                                                                           */
/* Input(s)           : u4Event - Event for which callback is registered     */
/*                      pFsCbInfo - Call Back Function                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : MSR_SUCCESS/MSR_FAILURE                              */
/*****************************************************************************/
INT4
MsrUtilCallBackRegister (UINT4 u4Event, tFsCbInfo * pFsCbInfo)
{
    INT4                i4RetVal = MSR_SUCCESS;
    switch (u4Event)
    {
        case MSR_SECURE_PROCESS:
            MSR_CALLBACK[u4Event].pMsrCustSecureStorage =
                pFsCbInfo->pIssCustSecureProcess;
            break;
        case MSR_CONF_FILE_VALIDATE:
            MSR_CALLBACK[u4Event].pMsrCustValidateConfResFile =
                pFsCbInfo->pIssCustValidateConfResFile;
            break;

        default:
            i4RetVal = MSR_FAILURE;
    }

    return i4RetVal;
}

#endif /* _MSR_UTIL_C_ */
