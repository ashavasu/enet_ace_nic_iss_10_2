/* $Id: msrsz.c,v 1.4 2013/11/29 11:04:10 siva Exp $*/
#define _MSRSZ_C
#include "msrsys.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
MsrSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < MSR_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            (INT4) MemCreateMemPool (FsMSRSizingParams[i4SizingId].u4StructSize,
                                     FsMSRSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(MSRMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            MsrSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
MsrSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsMSRSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, MSRMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
MsrSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < MSR_MAX_SIZING_ID; i4SizingId++)
    {
        if (MSRMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (MSRMemPoolIds[i4SizingId]);
            MSRMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
