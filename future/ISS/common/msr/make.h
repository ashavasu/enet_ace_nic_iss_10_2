####################################################
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
#
# $Id: make.h,v 1.14 2013/03/20 13:28:11 siva Exp $
#
####################################################

#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 30/04/2001                                    |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            including for building the FutureMSR          |
# |                            product.                                      |
# |                                                                          |
# +--------------------------------------------------------------------------+
#
#     CHANGE RECORD :
# +--------------------------------------------------------------------------+
# | VERSION | AUTHOR/    | DESCRIPTION OF CHANGE                             |
# |         | DATE       |                                                   |
# +---------|------------|---------------------------------------------------+
# |   1     | MSR        | Creation of makefile                              |
# |         | 30/04/2001 |                                                   |
# +--------------------------------------------------------------------------+


# Set the PROJ_BASE_DIR as the directory where you untar the project files
PROJECT_NAME  = FutureMSR
PROJECT_BASE_DIR = ${ISS_COMMON_DIR}/msr
PROJECT_SOURCE_DIR = ${PROJECT_BASE_DIR}/src
PROJECT_INCLUDE_DIR = ${PROJECT_BASE_DIR}/inc
PROJECT_OBJECT_DIR = ${PROJECT_BASE_DIR}/obj
MSR_TOOL_DIR            = ${ISS_COMMON_DIR}/tool/msrscan

SNMP_INC_DIR  = ${BASE_DIR}/inc/snmp
CFA_INC_DIR  = ${BASE_DIR}/cfa2/inc
SNMP_MAIN_INC_DIR       = ${BASE_DIR}/snmpv3
ISS_INC_DIR  = ${ISS_COMMON_DIR}/system/inc
MST_INC_DIR  = ${BASE_DIR}/stp/mstp/inc
PVRST_INC_DIR = ${BASE_DIR}/stp/pvrst/inc
#FUTURE_DIR      = ${BASE_DIR}/inc

ifneq ($(TARGET_OS), OS_VXWORKS)
ifeq (${RMGR}, YES)
ifeq (${RM_LNXIP},YES)
PROJECT_COMPILATION_SWITCHES += -USLI_WANTED -DBSDCOMP_SLI_WANTED \
                              -DIS_SLI_WRAPPER_MODULE
endif
endif
endif
# Specify the project include directories and dependencies
PROJECT_INCLUDE_FILES  = $(PROJECT_INCLUDE_DIR)/msrsys.h \
                         $(PROJECT_INCLUDE_DIR)/msrdef.h \
                         $(PROJECT_INCLUDE_DIR)/msrglb.h \
    $(PROJECT_INCLUDE_DIR)/msrrm.h \
    $(PROJECT_INCLUDE_DIR)/msrtrap.h \
    $(PROJECT_INCLUDE_DIR)/clrcfg.h \
    $(COMN_INCL_DIR)/msr.h

PROJECT_FINAL_INCLUDES_DIRS = -I$(PROJECT_INCLUDE_DIR) \
     -I$(SNMP_INC_DIR) \
     -I$(CFA_INC_DIR) \
     -I$(ISS_INC_DIR) \
     -I$(MST_INC_DIR) \
     -I$(PVRST_INC_DIR) \
     -I$(SNMP_MAIN_INC_DIR)

PROJECT_FINAL_INCLUDES_DIRS += $(COMMON_INCLUDE_DIRS)

PROJECT_FINAL_INCLUDE_FILES = $(PROJECT_INCLUDE_FILES)

PROJECT_DEPENDENCIES = $(COMMON_DEPENDENCIES) \
    $(PROJECT_FINAL_INCLUDE_FILES) \
    $(PROJECT_BASE_DIR)/Makefile \
    $(PROJECT_BASE_DIR)/make.h

