/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                         */
/* $Id: msrglb.h,v 1.28 2016/06/30 10:13:50 siva Exp $ */
/*****************************************************************************/
/*    FILE  NAME            : msrglb.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*    DESCRIPTION           : This file has all the includes,  nd globale    */
/*                              variable definations for MSR module.            */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#ifndef _MSRGLB_H_
#define _MSRGLB_H_
#ifdef _MSR_C_

tOsixTaskId         MsrId;           /* The task-id of the MSR task */
tOsixSemId          MsrSemId;        /* Sema4 Id of the MSR Task */
tOsixSemId          TftSemId;        /* Sema4 Id of TFT */
/*SNMP nessage queue name added*/
tOsixQId      MsrsnmpQId;
tOsixQId      MsrHttpQId;
tOsixQId      MsrDelQId;
tOsixQId      gAuditQId;
#ifdef RM_WANTED
tOsixQId      MsrQId;
#endif

tMemPoolId  gMsrMibPoolId; 
tMemPoolId  gMsrIncrRbPoolId = 0;
tMemPoolId  gMsrIncrOidPoolId = 0;
tMemPoolId  gAuditQMsgPoolId = 0;
tMemPoolId  gMsrQUpdDataPoolId = 0;
tMemPoolId  gMsrQConDataPoolId = 0;
tMemPoolId  gMsrOidListPoolId = 0;
tMemPoolId  gMsrOidArrPoolId = 0;
tMemPoolId  gMsrOidArrListPoolId = 0;
tMemPoolId  gMsrDataPoolId = 0;
INT4        gMsrIncrOidListPoolId = -1;
INT4        gMsrIncrDataPoolId = -1;
UINT1       gau1AuditPrevLogFileName [ISS_CONFIG_FILE_NAME_LEN];

UINT1    *gpu1StrEnd = NULL;         
/* A pointer to the end of StrFlash array */

INT4      gi4MibSaveStatus = MIB_SAVE_NOT_INITIATED;  
/* Reflects the status of the last MIB save operation */

INT4      gi4RemoteSaveStatus = MIB_SAVE_NOT_INITIATED;   
/* Reflects the status of the Remote save  operation */

/* To return the status as per mib definition*/
INT4      gi4MibResStatus = MIB_RESTORE_INVALID_VALUE; 
/* Reflects the status of the last MIB restore operation */

/*To Reflect the failure reason of the restoration failure*/
INT4      gi4MibResFailureReason = MIB_RES_INVALID_FAILURE;

UINT1     gu1MibSaveLocal = 0;  
/* Is TRUE if the configuration is to be 
* saved to flash and FALSE if it is to be 
* saved to remote */

UINT1     gu1MibRestoreLocal = 0; 
/* Is TRUE if the configuration is to be restored from flash and 
 * FALSE if it is to be restored from remote */

tMsrPacket    gMsrData; 
/* Used to store TFTP server related information for 
 * remote save/restore */

UINT4     gu4MsrTrcFlag = 0x00000000;
/* Set the value as 0xff, for enabling MSR trace logs */


UINT1 *gpu1StrTempCurr = NULL;

UINT1 *gpu1StrFlash = NULL;   
/* Points to the character array to be stored in / retrieved from flash */

UINT1 *gpu1StrCurrent = NULL; 
/* Current position in the StrFlash array */

UINT4  gu4Length = 0;     
/* The length(in bytes) of gpu1StrFlash array */
UINT1  gu1CfaBootFlag = MSR_FALSE;
UINT1  gu1MsrRestoreCount = 0;
UINT4  gu4MsrSysLogId = 0;
INT4  gi4TftpSysLogId = 0;

UINT4         gu4IncrLength = 0;

tTimerListId      gMsrDefSysIfUpTmrId = 0; 
tTimerListId      gIssMsrTimerListId = 0;
/* Timer List Id of System default interface oper up timer */

tMsrNotifyMsg     gNotifMsg;
/* Notification Information */

tSnmpIndex MsrIndexPool1[MSR_MAX_INDICES];
tSNMP_MULTI_DATA_TYPE MsrMultiPool1[MSR_MAX_INDICES];
tSNMP_OCTET_STRING_TYPE MsrOctetPool1[MSR_MAX_INDICES];
tSNMP_OID_TYPE  MsrOIDPool1[MSR_MAX_INDICES];
UINT1 au1MsrData1[MSR_MAX_INDICES][1024];

tSnmpIndex MsrIndexPool2[MSR_MAX_INDICES];
tSNMP_MULTI_DATA_TYPE MsrMultiPool2[MSR_MAX_INDICES];
tSNMP_OCTET_STRING_TYPE MsrOctetPool2[MSR_MAX_INDICES];
tSNMP_OID_TYPE  MsrOIDPool2[MSR_MAX_INDICES];
UINT1 au1MsrData2[MSR_MAX_INDICES][1024];

tSNMP_OID_TYPE gMsrOidType[MSR_MAX_OID_TYPE];
UINT4 gau4MsrOidType[MSR_MAX_OID_TYPE][MSR_MAX_OID_LEN];
UINT4 gu4MsrOidCount = 0;
tSNMP_OID_TYPE gMsrClrCfgOidType[MSR_MAX_OID_TYPE];
UINT4 gau4MsrClrCfgOidType[MSR_MAX_OID_TYPE][MSR_MAX_OID_LEN];
UINT4 gu4MsrClrCfgOidCount = 0;
tSNMP_MULTI_DATA_TYPE  gMsrMultiData[MSR_MAX_MULTI_DATA];
UINT4 gu4MsrMultiDataCount = 0;
tSNMP_OCTET_STRING_TYPE gMsrOctetString[MSR_MAX_OCTET_STRING];
UINT1 gau1MsrOctetString[MSR_MAX_OCTET_STRING][MSR_MAX_OCTET_STRING_LEN];
UINT4 gu4MsrOctetStrCount = 0;
UINT4 gu4MsrAutoSaveOIDIndex = 0;
UINT4 gu4MsrSaveIPOptionOIDIndex = 0;
UINT4 gu4MsrSaveIPAddrOIDIndex = 0;
UINT4 gu4MsrSaveFileNameOIDIndex = 0;


/* Indicated the state of RAM. Initially RAM content is empty */
UINT1               gu1RamState = MSR_RAM_EMPTY;
UINT2               gu2FlashCksum = 0;
UINT4               gu4noMsrUpdateQReq = 0;
tOsixSemId          MsrUpdateQsemId;
tOsixSemId          MsrUpdateQSsemId;
tIssBool            MsrisRetoreinProgress;
tIssBool            MsrisSaveComplete;
tIssBool            gMSRIncrSaveFlag = ISS_FALSE;
UINT4               gu4MibEntryBlkSize = 0;
INT4                gTempi4MibSaveStatus = MIB_SAVE_NOT_INITIATED;
tOsixSemId          MsrDeleteQsemId;
UINT2               gu2DelQCount = 0;
/* Reflects the status of the last MIB save operation */

INT4                gTempi4RemoteSaveStatus = MIB_SAVE_NOT_INITIATED;
/* Reflects the status of the Remote save operation */

#ifdef RM_WANTED
/* Global variable to store the configuration change state in
 * active node
 */
INT1                gi1ConfigChange = MSR_FALSE;
#endif
tTmrAppTimer        gSysDefIfUpTimer;
tTmrAppTimer        gIssMsrTmrApp;
INT4                gi4SaveFd = 0;

/* MSR internal counters used for testing of Q messages */
UINT4               gU4NoOfQMsgSend = 0;
UINT4               gU4NoOfQMsgRead = 0;
UINT4               gU4NoOfUpdationsToRBTree = 0;
UINT4               gU4NoOfQMsgcomplete = 0;
UINT4               gU4NoOfDefaultRecv = 0;
UINT4               gU4NoOfComdsAfterValidations = 0;
UINT4               gU4NoOfUpdateEvents = 0;
UINT4               gU4NoOfQMsgSendFail = 0;
tRBTree             gMSRRBTable = NULL;
UINT1               au1RestoreFileVersionOid [MSR_MAX_OID_LEN];

#ifdef RM_WANTED
tSnmpIndex MsrRmIndexPool1[MSR_MAX_INDICES];
tSNMP_MULTI_DATA_TYPE MsrRmMultiPool1[MSR_MAX_INDICES];
tSNMP_OCTET_STRING_TYPE MsrRmOctetPool1[MSR_MAX_INDICES];
tSNMP_OID_TYPE  MsrRmOidPool1[MSR_MAX_INDICES];
UINT1 au1MsrRmData1[MSR_MAX_INDICES][MSR_MAX_DATA_LEN];

tSnmpIndex MsrRmIndexPool2[MSR_MAX_INDICES];
tSNMP_MULTI_DATA_TYPE MsrRmMultiPool2[MSR_MAX_INDICES];
tSNMP_OCTET_STRING_TYPE MsrRmOctetPool2[MSR_MAX_INDICES];
tSNMP_OID_TYPE  MsrRmOidPool2[MSR_MAX_INDICES];
UINT1 au1MsrRmData2[MSR_MAX_INDICES][MSR_MAX_DATA_LEN];

tSNMP_OID_TYPE gMsrRmOidType[MSR_RM_MAX_OID];
UINT4 gau4MsrRmOidType[MSR_RM_MAX_OID][MSR_MAX_OID_LEN];
tSNMP_MULTI_DATA_TYPE  gMsrRmMultiData[MSR_RM_MAX_DATA];
tSNMP_OCTET_STRING_TYPE gMsrRmOctetString[MSR_RM_MAX_DATA];
UINT1 gau1MsrRmOctetString[MSR_RM_MAX_DATA][MSR_MAX_DATA_LEN];

tOsixTaskId         gMsRmTaskId = 0;
#endif

/* For call back */
unMsrUtilCallBackEntry gaMsrUtilCallBack[MSR_MAX_CALLBACK_EVENTS];

/* Global variable to store the priority (FLASH/EXTERNAL) 
 * storage for save/restore */
tMsrSavResOption gMsrSavResPriority = FLASH_STORAGE;
#endif /* _MSR_C_ */
#endif /* _MSRGLB_H_ */
