/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/* $Id: csrsys.h,v 1.10 2015/10/16 10:49:21 siva Exp $*/
/*****************************************************************************/
/*    FILE  NAME            : csrsys.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*    DESCRIPTION           : This file has all the includes, type defines,  */
/*                            prototypes, externs, defines for CSR module.   */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
#ifndef _CSRSYS_H
#define _CSRSYS_H

#include "lr.h"
#include "iss.h"
#include "msr.h"
#include "msrsys.h"
#include "csr.h"


tMemPoolId gCsrPoolId[MAX_PROTOCOLS]; /* The memory pool ID assigned to Csr */

UINT1 *gpu1Flash [MAX_PROTOCOLS];    /* Points to the character array to be
                                      * stored in / retrieved from flash */
UINT1 *gpu1Current;   /* Current position in the StrFlash array */

UINT4  gu4Len;        /* The length(in bytes) of gStrFlash array */


extern VOID CliRestoreConfig (UINT1 *);
extern INT4 UtilOspfGetFirstCxtId (UINT4 * pu4OspfCxtId);
extern INT4 OspfShowRunningConfigInCxt (tCliHandle CliHandle,UINT4 u4Module,UINT4 u4OspfCxtId);
extern INT4 UtilOspfGetNextCxtId (UINT4 u4OspfCxtId, UINT4 *pu4NextOspfCxtId);

PUBLIC INT4 V3UtilOspfGetFirstCxtId (UINT4 * pu4ContextId);
PUBLIC INT4
V3UtilOspfGetNextCxtId (UINT4 u4ContextId, UINT4 * pu4NextContextId);
PUBLIC INT4 Ospfv3ShowRunningConfigInCxt (tCliHandle CliHandle,UINT4 u4Module,UINT4 u4OspfCxtId);


extern INT4 BgpShowRunningConfig (tCliHandle, UINT1 *pu1ContextName);
extern INT4 IsIsShowRunningConfigInCxt (tCliHandle CliHandle,UINT4 u4Module,UINT4 u4ContextId);
extern INT4       UtilIsisGetFirstCxtId (UINT4 *pu4IsisCxtId);
extern INT4       UtilIsisGetNextCxtId (UINT4 u4ContextId, UINT4 *pu4NextContextId);
#ifdef MPLS_WANTED
extern INT4 RpteCliRsvpTeShowRunnningConfig (tCliHandle CliHandle);
extern INT4 LdpCliShowRunningConfig (tCliHandle);
extern INT4 RpteFrrFacShowRunningConfig (tCliHandle CliHandle);
#endif

#endif /* _CSRSYS_H */
