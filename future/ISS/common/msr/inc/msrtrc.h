/********************************************************************
 * * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 * *
 * * $Id: msrtrc.h,v 1.4 2016/07/22 09:45:45 siva Exp $
 * *
 * * Description: This file contains the trace related macros
 * *              defined for MSR module.
 * *******************************************************************/

#ifndef __MSR_TRC_H
#define __MSR_TRC_H

#include "trace.h"

/* Trace related defines */
#define MSR_TRC_FLAG                     gu4MsrTrcFlag
#define MSR_MOD_NAME                     ((const char *)"MSR")
#define MSR_TRACE_TYPE                   0x00000080
#define MSR_RM_TRACE_TYPE                0x00000100
#define MSR_GENERAL_TRACE_TYPE           0x00000400    /* Used for displaying successful MSR Restoration  */
#define MSR_RM_CRITICAL_TRACE_TYPE       0x00000200
#define MSR_TRC(TraceType, Str) \
        MOD_TRC(MSR_TRC_FLAG, TraceType, MSR_MOD_NAME, (const char *)Str)

#define MSR_TRC_ARG1(TraceType, Str, Arg) \
        MOD_TRC_ARG1(MSR_TRC_FLAG, TraceType, MSR_MOD_NAME, (const char *)Str, Arg)
#define MSR_TRC_ARG2(TraceType, Str, Arg1, Arg2) \
        MOD_TRC_ARG2(MSR_TRC_FLAG, TraceType, MSR_MOD_NAME, (const char *)Str, Arg1, Arg2)

#endif
