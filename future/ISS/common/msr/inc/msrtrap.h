/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: msrtrap.h,v 1.4 2011/10/25 11:17:46 siva Exp $
 * 
 * Description: Header file MSR-TRAP definitions. 
 *******************************************************************************/

#ifndef  MSR_TRAP_H
#define  MSR_TRAP_H
#include "fm.h"
#include "fssnmp.h"
#include "snmputil.h"

#define MSR_CFG_RESTORE_TRAP   1


tSNMP_OID_TYPE* MsrMakeObjIdFrmDotNew (INT1 *pi1TextStr);
UINT1*          MsrParseSubIdNew (UINT1 *pu1TempPtr, UINT4 *pu4Value);
VOID            MsrTrapSendNotifications (tMsrNotifyMsg *pNotifMsg);

#endif  /* MSR_TRAP_H */
