/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: msrdef.h,v 1.37 2015/02/03 12:48:17 siva Exp $
 * 
 *******************************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                         */
/* $Id:        */
/*****************************************************************************/
/*    FILE  NAME            : msrdef.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*    DESCRIPTION           : This file has all the includes, type defines,  */
/*                            defines for MSR module.                        */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "msrtrc.h"

#define FN_VAL(u4Index)  (gaSaveArray[u4Index].fnVal)

/* Validate routine return values */
#define MSR_SAVE                1 /* The object should be saved */
#define MSR_SKIP                2       /* Skip only this instance of 
        the object */
#define MSR_NEXT         3       /* Skip this whole object/entry */
#define MSR_VOLATILE_SAVE       4       /* The object should not be saved
                                           but should be send to standby  */

/* This macro represents File Version and File Format ie. 2*/ 

#define MSR_RES_FILE_VER_FORMAT  2

/* Row status */
#define MSR_ACTIVE                       1
#define MSR_NOT_IN_SERVICE               2
#define MSR_NOT_READY                    3
#define MSR_CREATE_AND_GO                4
#define MSR_CREATE_AND_WAIT              5
#define MSR_DESTROY                      6

/* Entry status */
#define MSR_CREATE_REQUEST               2

/* Type of OID */
#define MSR_SCALAR                       1
#define MSR_TABLE                        2
/* Type of OID */
#define MSR_ADD                       1
#define MSR_MODIFY                    2
#define MSR_DELETE                    3


/* Size of the Queue allocated to receive Delete notifications */
#define MSR_DEL_QUEUE_SIZE   10


/* Length of Oid */
#define OID_LENGTH_ELEVEN                11
#define OID_LENGTH_TWELVE                12
#define OID_LENGTH_THIRTEEN              13
/*defination added for SCALAR and TABULAR in MSR Update event*/
#define MSR_ISSCALAR(x) ((x->pSnmpindx != NULL) && (x->pSnmpindx->u4No == 0))
#define MSR_ISTABLE(x) ((x->pSnmpindx != NULL) && (x->pSnmpindx->u4No > 0))

#define  MSR_ROWSTATUS                 1
#define  MSR_NONROWSTATUS                 2

/* Memory and string functions */
#define MSR_MEMSET                       MEMSET
#define MSR_MEMCMP                       MEMCMP
#define MSR_MEMCPY                       MEMCPY
#define MSR_STRCPY                       STRCPY
#define MSR_STRNCPY                      STRNCPY
#define MSR_STRLEN                       STRLEN
#define MSR_STRCAT                       STRCAT
#define MSR_STRNCAT                      STRNCAT
#define MSR_STRCMP                       STRCMP
#define MSR_ATOI                         ATOI
#define MSR_ATOL                         ATOL
#define MSR_NTOA                         UtlInetNtoa
#define MSR_INET_ADDR                    INET_ADDR

#define IPADDRESS_LEN 4
#define BYTE_LEN      8

#define MSR_DELAY     10

/* Defines related to data structures */
#define MSR_MAX_OID_TYPE            10
#define MSR_MAX_OID_LEN             272
#define MSR_MAX_MULTI_DATA          5
#define MSR_MAX_OCTET_STRING        6
#define MSR_MAX_STR_LEN             256
#define MSR_MULTISIZE               MSR_MAX_STR_LEN + 44
#define MSR_MAX_INDICES             SNMP_MAX_INDICES_2
#define MSR_CHECKSUM_LENGTH         8  /* length of "CKSUM:xx" */
#define MSR_MAX_OCTET_STRING_LEN    SNMP_MAX_OCTETSTRING_SIZE
#define MSR_MAX_IP_ADDR_LEN         16
#define MSR_MAX_IPVX_ADDR_LEN         16
#define MSR_MAX_DATA_LEN            (MSR_MAX_OID_LEN + MSR_MAX_OCTET_STRING_LEN)
#define MSR_MAX_FILE_READ_LEN       240
#define MSR_MAX_FILE_NME_LEN        150

#define MSR_SNMPV2_TRAP_OID_LEN     11

#define FLASH_CKSUM_LEN       2
#define FLASH_CKSUM_OFFSET   -4

#define MSR_CLEAR_OID(x)\
  {\
  MSR_MEMSET(x->pu4_OidList, 0, MSR_MAX_OID_LEN);\
  x->u4_Length = 0;\
  }
  
#define MSR_CLEAR_OCTET_STRING(x)\
  {\
  MSR_MEMSET(x->pu1_OctetList,0, MSR_MAX_OCTET_STRING_LEN);\
  x->i4_Length = 0;\
  }

#define MSR_CLEAR_MULTIDATA(x)\
  {\
  x->u8_Counter64Value.msn = 0;\
  x->u8_Counter64Value.lsn = 0;\
  x->u4_ULongValue = 0;\
  x->i4_SLongValue = 0;\
  x->i2_DataType = 0;\
  MSR_CLEAR_OID(x->pOidValue);\
  MSR_CLEAR_OCTET_STRING(x->pOctetStrValue);\
  }


#define MSR_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)
 
/* Memory block size, count and type */

      /* Assumptions for arriving at the required memory block size:
       * 1. An average of 35 to 40 bytes needed for storing the
       *    OID, type and value of each object.
       *    
       * 2. There are 144 scalars and around 87 tables to be saved.
       * 
       * 3. Considering a maximum of 10 objects in a row and 20 
       *    instances (in a table) the memory block size is around:
       *    (144 * 40) + (87 * 10 * 20 * 40) = 701760 bytes
       *
       * NOTE: In case the number of objects to be saved varies, 
       *       this size may have to be modified for the same!!!
       */

                                
#define MSR_MEMORY_TYPE                  MEM_DEFAULT_MEMORY_TYPE

/* MEM Pool Id Definition */
#define MSR_MIBENTRY_POOL_ID             gMsrMibPoolId

/* Macro for Memory Block Allocation from Memory Pool */

#define  MSR_MIBENTRY_ALLOC_MEM_BLOCK(ppu1Msg)\
         MemAllocateMemBlock(MSR_MIBENTRY_POOL_ID, (UINT1 **)ppu1Msg)

/* Macro for Freeing Memory Blocks from Memory Pool */

#define  MSR_CONFIGENTRY_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(MSR_MIBENTRY_POOL_ID, (UINT1 *)pu1Msg)

/* SYNTAX  definitions */
#define INTEGER          SNMP_DATA_TYPE_INTEGER
#define INTEGER32        SNMP_DATA_TYPE_INTEGER32 
#define OCTETSTRING      SNMP_DATA_TYPE_OCTET_PRIM 
#define OBJECTIDENTIFIER SNMP_DATA_TYPE_OBJECT_ID 
#define IPADDRESS        SNMP_DATA_TYPE_IP_ADDR_PRIM 
#define COUNTER32        SNMP_DATA_TYPE_COUNTER32 
#define GAUGE32          SNMP_DATA_TYPE_GAUGE32 
#define TIMETICKS        SNMP_DATA_TYPE_TIME_TICKS 
#define UNSIGNED32       SNMP_DATA_TYPE_UNSIGNED32 
#define UINTEGER         UNSIGNED32 
#define ROWSTATUS        INTEGER32
#define DISPLAYSTRING    OCTETSTRING  
#define OCTET            OCTETSTRING 
#define TruthValue       INTEGER32
#define COUNTER64        SNMP_DATA_TYPE_COUNTER64
#define INTERFACEINDEX   INTEGER32
#define VariablePointer  OBJECTIDENTIFIER
#define RowPointer       OBJECTIDENTIFIER


/* Mem pool id for the mem pool allocated for RBTree */
#define MSR_INCR_RB_POOL_ID              gMsrIncrRbPoolId
#define MSR_INCR_OID_POOL_ID             gMsrIncrOidPoolId

/* Macro for Memory Block Allocation from Memory Pool */

#define  MSR_INCR_RB_ALLOC_MEM_BLOCK(Node)\
{ \
    Node = (tMSRRbtree *) MemAllocMemBlk(MSR_INCR_RB_POOL_ID); \
    if (Node != NULL) \
    { \
        MSR_MEMSET (Node, 0, sizeof (tMSRRbtree)); \
        Node->pOidValue = \
            (tSNMP_OID_TYPE *) MemAllocMemBlk(MSR_INCR_OID_POOL_ID); \
        if (Node->pOidValue == NULL) \
        { \
            MemReleaseMemBlock (MSR_INCR_RB_POOL_ID, (UINT1 *)Node); \
            Node = NULL; \
        } \
        else \
        { \
        MSR_MEMSET (Node->pOidValue, 0, sizeof(tSNMP_OID_TYPE)); \
    } \
    } \
}


/* Macro for Freeing Memory Blocks from Memory Pool */

#define  MSR_INCR_RB_FREE_MEM_BLOCK(Node)\
{ \
    MemReleaseMemBlock (MSR_INCR_OID_POOL_ID, (UINT1 *)Node->pOidValue); \
    MemReleaseMemBlock (MSR_INCR_RB_POOL_ID, (UINT1 *)Node); \
}

/* The size of the RBTree Node */
#define MSR_INCR_MIBENTRY_MEMBLK_SIZE  sizeof(tMSRRbtree)

/* The size of the OidType objects */
#define MSR_INCR_OID_BLK_SIZE          sizeof(tSNMP_OID_TYPE)

/* Mem pool ids for the pools allocated for data and oid list in Rbtree */
#define MSR_INCR_DATA_POOL_ID          gMsrIncrDataPoolId 
#define MSR_INCR_OIDLIST_POOL_ID       gMsrIncrOidListPoolId

/* Size allocation for Data blk in the Data buddy pool */
#define MSR_MAX_DATA_BLK_SIZE          1024  /* The data saved to Rbtree can
                                                be maximum of 1k */
#define MSR_MIN_DATA_BLK_SIZE          8     /* The minimum size of data saved
                                                can be 7 bytes (for rowstatus
                                                and other numerical values) */
#define MSR_MAX_DATA_BLKS              100 /* 500 */   /* Approx. 20 bytes of data
                                                allocated for each node added
                                                to RBtree */

/* Size allocation for Oid list in the buddy pool */
#define MSR_MAX_OID_BLK_SIZE          1024   /* MSR_MAX_OID_LEN * 4 (bytes)*/
#define MSR_MIN_OID_BLK_SIZE          64     /* Assumption - 10 digits in the
                                                oid tree */
#define MSR_MAX_OID_BLKS              200  /* 2000 */ /* Approx 32 oids for each node */

#define MSR_CREATE_INCR_BUDDY_POOL(maxsize, minsize, u4NumBlk) \
    MemBuddyCreate(maxsize, minsize, u4NumBlk, BUDDY_HEAP_EXTN)

#define MSR_FREE_INCR_BUDDY_POOL(poolId) \
    MemBuddyDestroy(poolId)

#define MSR_ALLOC_INCR_BUDDY_BLK(dataSize, oidSize, Node) \
{ \
    Node->pData = (UINT1 *) MemBuddyAlloc ((UINT1) MSR_INCR_DATA_POOL_ID, \
                                          dataSize); \
    Node->pOidValue->pu4_OidList = \
        (UINT4 *)(VOID *) MemBuddyAlloc ((UINT1) MSR_INCR_OIDLIST_POOL_ID, oidSize); \
}

#define MSR_RELEASE_INCR_BUDDY_BLK(Node) \
{ \
    if (Node->pData != NULL) \
    { \
        MemBuddyFree((UINT1) MSR_INCR_DATA_POOL_ID, Node->pData); \
    } \
    if (Node->pOidValue->pu4_OidList != NULL)  \
    { \
        MemBuddyFree((UINT1) MSR_INCR_OIDLIST_POOL_ID, \
                     (UINT1 *) Node->pOidValue->pu4_OidList); \
    } \
}

#define MSR_ALLOC_INCR_BUDDY_DATA_BLK(Node, dataSize) \
    Node->pData = (UINT1 *) MemBuddyAlloc ((UINT1) MSR_INCR_DATA_POOL_ID, dataSize);

#define MSR_RELEASE_INCR_BUDDY_DATA_BLK(Node) \
    MemBuddyFree((UINT1) MSR_INCR_DATA_POOL_ID, Node->pData); 


#define MSR_DATA_POOL_ID      gMsrDataPoolId 

#define  MSR_DATA_ALLOC_MEM_BLOCK(pu1Msg)\
         (pu1Msg = (UINT1 *)\
          MemAllocMemBlk(MSR_DATA_POOL_ID))

#define  MSR_DATA_FREE_MEM_BLOCK(pu1Msg)\
         MemReleaseMemBlock(MSR_DATA_POOL_ID, (UINT1 *)pu1Msg)

#define MSR_ROUTE_PROTO_MGMT  0x03
#define MSR_ROUTE_TYPE_LOCAL  0x03

#define VLAN_PERMANENT          3
#define OSPF_DYNAMIC            1
#define BGP_ADMINDOWN           2

#define MSR_LA_PORT_STATE_MASK  0xE0
#define PNAC_MAX_STR_LEN        20

/* To speed-up configuration restore operation, the configuration file
 * iss.conf (which is stored in flash) is copied to RAM (gu1StrFlash[]).
 * When conf is restored, RAM content is read instead of accessing the flash.
 * MSR_RAM_EMPTY: Indicates that RAM content is empty.
 * MSR_RAM_IN_SYNC_WITH_FLASH: Flash content is copied to RAM and
 *                              RAM is in-sync with flash */
#define    MSR_RAM_EMPTY               0
#define    MSR_RAM_IN_SYNC_WITH_FLASH  1
#define    MSR_AUTO_SAVE_OID  "1.3.6.1.4.1.2076.81.1.43.0"
#define    MSR_CONF_SAVE_OPTION_OID  "1.3.6.1.4.1.2076.81.1.10.0"
#define    MSR_CONF_SAVE_IPADDR_OID  "1.3.6.1.4.1.2076.81.1.11.0"
#define    MSR_CONF_SAVE_FILENAME_OID  "1.3.6.1.4.1.2076.81.1.12.0"
#define    MSR_CONF_SAVE_STATIC_VLAN_OID  "1.3.6.1.4.1.2076.116.7.1.4.4.1"
#define    MSR_CONF_SAVE_STP_PORT_PROP_OID  "1.3.6.1.4.1.2076.116.2.2.1"
#define    MSR_CONF_SAVE_STP_EXT_PORT_PROP_OID  "1.3.6.1.4.1.2076.116.2.4.1"

/* Msr Message Types */
# define   MSR_CONFIG_SAVE        1
# define   MSR_PROTO_SHUT         2
# define   MSR_CONTEXT_DELETE     3
# define   MSR_PORT_DELETE        4
# define   MSR_VLAN_DELETE        5
# define   MSR_PB_PORT_TYPE_CHANGE       6
# define   MSR_BRIDGE_MODE_CHANGE        7
# define   MSR_STP_PORT_DELETE        8

/*  To handle remote mib save thru TFTP/SFTP*/
typedef enum {
    INITIATE,
    SEND_DATA,
    TERMINATE
}tConnStatus;

#define MSR_MAX_FLASH_FILE_LEN   (ISS_CONFIG_FILE_NAME_LEN + STRLEN (FLASH) + 1)
#define MAX_OID_NO                5

#define MSR_CALLBACK gaMsrUtilCallBack

#define MSR_LEN_MAX 0xffffffff
