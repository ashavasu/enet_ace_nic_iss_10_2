/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/* $Id: clrcfg.h,v 1.44 2016/01/11 11:41:59 siva Exp $                        */
/*****************************************************************************/
/*    FILE  NAME            : clrcfg.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : ISS System                                     */                                   /*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */                                   /*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */                                   /*    AUTHOR                : ISS Team                                       */
/*    DESCRIPTION           : This file has all the includes, function       */
/*                            prototypes and macro definitions related to    */
/*                            clear-config feature.                          */
/*---------------------------------------------------------------------------*/                                   /* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/                                   /* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */                                   /*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/ 

#ifndef _CLEARCONFIG_H_
#define _CLEARCONFIG_H_

#ifdef QOSX_WANTED
#include "qosxtd.h"
#endif
#ifdef BGP_WANTED
#include "bgp.h"
#endif
#if defined (PIM_WANTED) || defined (PIMV6_WANTED)
#include "pim.h"
#endif
#ifdef ISIS_WANTED
#include "isis.h"
#endif
#ifdef DVMRP_WANTED
#include "dvmrp.h"
#endif
#ifdef VRRP_WANTED
#include "vrrp.h"
#endif
#ifdef OSPF_WANTED
#include "ospf.h"
#endif
#ifdef OSPF3_WANTED
#include "ospf3.h"
#endif
#include "ospfte.h"
#ifdef IGMP_WANTED
#include "igmp.h"
#endif
#ifdef OPENSSL_WANTED 
#include "httpssl.h"
#endif

/* MACRO definitions */

#define IF_MAIN_OID         "1.3.6.1.4.1.2076.27.1.4.1.8"
#define VCM_IF_MAIN_OID     "1.3.6.1.4.1.2076.93.1.3.1.5"
#define VC_STATUS_OID       "1.3.6.1.4.1.2076.93.1.2.1.6"
#define IF_STACK_OID        "1.3.6.1.2.1.31.1.2.1.3"
#define ETHER_STATUS_OID    "1.3.6.1.2.1.16.1.1.1.21"
#define HISTORY_STATUS_OID  "1.3.6.1.2.1.16.2.1.1.7"
#define EVENT_STATUS_OID    "1.3.6.1.2.1.16.9.1.1.7"
#define ALARM_STATUS_OID    "1.3.6.1.2.1.16.3.1.1.12"
#define IF_MAIN_EXT_OID     "1.3.6.1.4.1.2076.27.1.12.1"
#define BRIDGE_MODE_OID     "1.3.6.1.4.1.2076.138.1.1.1.2.0"
#define ECFM_OUI_OID        "1.3.6.1.4.1.2076.160.1.1.2"
#define EOAM_OUI_OID        "1.3.6.1.4.1.2076.121.1.4"
#define LA_SYSTEM_OID       "1.3.6.1.4.1.2076.63.1.7"
#define LLDP_SYSTEM_OID     "1.3.6.1.4.1.2076.158.2.2"
#define MST_REGION_OID      "1.3.6.1.4.1.2076.118.1.3.1.27"
#define PB_MUL_LIMIT_OID    "1.3.6.1.4.1.2076.127.1.1.1.2"
#define SYS_LOCATION_OID    "1.3.6.1.2.1.1.6"
#define SYS_CONTACT_OID     "1.3.6.1.2.1.1.4"
#define SYS_NAME_OID        "1.3.6.1.2.1.1.5"
#define DHCP_CIRCUIT_OPTION_OID      "1.3.6.1.4.1.2076.24.1.16.0"
#define DHCP_CIRCUIT_OPTION_VALUE    "\001"
                                     /* Local Interface */
#define OSPF_AREA_ROUTES_OID_LOCAL   "1.3.6.1.4.1.29601.2.31.4.1.5.0.2" 
#define OSPF_EXT_ROUTES_OID_LOCAL    "1.3.6.1.4.1.29601.2.31.4.1.6.0.2"
                                     /* Static route */
#define OSPF_AREA_ROUTES_OID_NETMGMT "1.3.6.1.4.1.29601.2.31.4.1.5.0.3"
#define OSPF_EXT_ROUTES_OID_NETMGMT  "1.3.6.1.4.1.29601.2.31.4.1.6.0.3"
#define SNMP_USER_OID       "1.3.6.1.6.3.15.1.2.2.1.13"
#define SYSLOG_FILE_OID     "1.3.6.1.4.1.2076.89.1.17.1.3"
#define SYSLOG_LOGGING_SERVER_OID "1.3.6.1.4.1.2076.89.2.3.1.6"
#define DNS_DOMAIN_NAME_OID "1.3.6.1.4.1.29601.2.99.3.1.1.3"
#define DNS_NAME_SERVER_OID "1.3.6.1.4.1.29601.2.99.2.1.1.4"
#define ISS_PORT_ISOLATION_OID       "1.3.6.1.4.1.2076.81.2.3.1.5"
#define STATIC_MCAST_ENTRY_OID "1.3.6.1.4.1.2076.116.7.1.3.3.1"
#define STATIC_MCAST_STATUS_OID "1.3.6.1.4.1.2076.116.7.1.3.3.1.4"
#define OFCL_CFG_ENTRY_ROW_STATUS "1.3.6.1.4.1.29601.2.81.1.1.1.10"
#define CLR_OUI_STR_LEN     8
#define CLR_MAX_MAC_LEN     64
#define MSR_INVALID         4



typedef struct _ScalarObject
{
 const char      *pScalarOidString;
 CHR1            *pu1DefVal;
}tScalarObject;

tScalarObject gaClearArray[] =
{
 {
  "1.3.6.1.4.1.2076.84.1.1",       /* dhcpSrvEnable */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.84.1.5",       /* dhcpSrvBootServerAddress */
  "0.0.0.0"
 },
 {
  "1.3.6.1.4.1.2076.84.1.6",       /* dhcpSrvDefBootFilename */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.121.1.4",      /* fsEoamOui */
  "00:01:02"
 },
 {
  "1.3.6.1.4.1.2076.44.1",         /* rmonDebugType */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.44.2",         /* rmonEnableStatus */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.44.3",         /* rmonHwStatsSupp */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.44.4",         /* rmonHwHistorySupp */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.44.5",         /* rmonHwAlarmSupp */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.44.6",         /* rmonHwHostSupp */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.44.7",         /* rmonHwHostTopNSupp */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.44.8",         /* rmonHwMatrixSupp */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.44.9",         /* rmonHwMatrixSupp */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.149.1.1.11",   /* fsSntpTimeZone */
  "+00:00"
 },
 {
  "1.3.6.1.4.1.2076.149.1.1.8",    /* fsSntpAuthKeyId */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.149.1.1.10",   /* fsSntpAuthKey */
  "00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00"
 },
 {
  "1.3.6.1.4.1.2076.149.1.1.12",   /* fsSntpDSTStartTime  */
  "0"
 },
 {
  "1.3.6.1.2.1.158.1.1.1.3",        /* dot3OamMode */
  "2"
 },
 {
  "1.3.6.1.2.1.158.1.5.1.2",        /* dot3OamErrSymPeriodWindowLo */
  "625000000"
 },
 {
  "1.3.6.1.2.1.158.1.5.1.4",        /* dot3OamErrSymPeriodThresholdLo */
  "1"
 },
 {
  "1.3.6.1.2.1.158.1.5.1.6",        /* dot3OamErrFramePeriodWindow */
  "10000000"
 },
 {
  "1.3.6.1.2.1.158.1.5.1.7",        /* dot3OamErrFramePeriodThreshold */
   "1"
 },
 {
  "1.3.6.1.4.1.2076.149.1.1.13",   /* fsSntpDSTEndTime  */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.89.1.14",      /* fsSyslogFileNameOne */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.89.1.15",      /* fsSyslogFileNameTwo */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.89.1.16",      /* fsSyslogFileNameThree */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.89.3.3",       /* fsSyslogSmtpSenderMailId */
  ""
 },
 {
  "1.3.6.1.4.1.2076.158.2.2",      /* fsLldpLocChassisId */
  "00:01:02:03:04:01"
 }, 
 {
  "1.3.6.1.4.1.2076.64.1.1",       /* fsPnacSystemControl */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.64.1.3",       /* fsPnacAuthenticServer */ 
  "2"
 },
 {
  "1.3.6.1.4.1.2076.64.1.4",       /* fsPnacNasId */
  "66:73:4e:61:73:31"
 },
 {
  "1.3.6.1.4.1.2076.64.6.1",     /* fsDPnacSystemStatus */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.64.6.2",     /* fsDPnacPeriodicSyncTime */
  "60"
 },
 {
  "1.3.6.1.4.1.2076.64.6.3",     /* fsDPnacMaxKeepAliveCount */
  "3"
 },
 {
  "1.3.6.1.4.1.2076.63.1.1",       /* fsLaSystemControl */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.63.1.2",       /* fsLaStatus */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.63.1.7",       /* fsLaActorSystemID */
  "00:01:02:03:04:01"
 },
 {
  "1.3.6.1.4.1.2076.63.1.9",       /* fsLaDLAGSystemStatus */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.63.1.10",       /* fsLaDLAGSystemID */
  "00:00:00:00:00:00"
 },
 {
  "1.3.6.1.4.1.2076.63.1.11",       /* fsLaDLAGSystemPriority */
  "32768"
 },
 {
  "1.3.6.1.4.1.2076.63.1.12",       /* fsLaDLAGPeriodicSyncTime */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.63.1.13",       /* fsLaDLAGRolePlayed */
  ""
 },
 {
  "1.3.6.1.4.1.2076.63.1.14",       /* fsLaDLAGDistributingPortIndex */
  ""
 },
 {
  "1.3.6.1.4.1.2076.63.1.15",       /* fsLaDLAGDistributingPortList */
  ""
 },
 {
  "1.3.6.1.4.1.2076.63.2.1.1.12",  /* fsLaPortChannelDLAGDistributingPortIndex */
  ""
 },
 {
  "1.3.6.1.4.1.2076.63.2.1.1.13",  /* fsLaPortChannelDLAGSystemID */
  ""
 },
 {
  "1.3.6.1.4.1.2076.63.2.1.1.19",  /* fsLaPortChannelDLAGRedundancy */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.63.2.1.1.20",  /* fsLaPortChannelDLAGMaxKeepAliveCount */
  "3"
 },
 {
  "1.3.6.1.4.1.2076.63.2.1.1.21",  /* fsLaPortChannelDLAGPeriodicSyncPduTxCount */
  ""
 },
 {
  "1.3.6.1.4.1.2076.63.2.1.1.22",  /* fsLaPortChannelDLAGPeriodicSyncPduRxCount */
  ""
 },
 {
  "1.3.6.1.4.1.2076.63.2.1.1.23",  /* fsLaPortChannelDLAGEventUpdatePduTxCount */
  ""
 },
 {
  "1.3.6.1.4.1.2076.63.2.1.1.24",  /* fsLaPortChannelDLAGEventUpdatePduRxCount */
  ""
 },
 {
  "1.3.6.1.4.1.2076.63.2.1.1.25",  /* fsLaPortChannelDLAGElectedAsMasterCount */
  ""
 },
 {
  "1.3.6.1.4.1.2076.63.2.1.1.26",  /* fsLaPortChannelDLAGElectedAsSlaveCount */
  ""
 },
 {
  "1.3.6.1.4.1.2076.63.2.1.1.27",  /* fsLaPortChannelTrapTxCount */
  ""
 },
 {
  "1.3.6.1.4.1.2076.63.2.1.1.28",  /* fsLaPortChannelDLAGDistributingPortList */
  ""
 },
 {
  "1.2.840.10006.300.43.1.2.1.1.15",  /* dot3adAggPortActorPortPriority */
  "128"
 },
 {
  "1.2.840.10006.300.43.1.2.1.1.2",     /* dot3adAggPortActorSystemPriority */
  "32768"
 },
 {
  "1.3.6.1.4.1.2076.112.11",       /* snmpColdStartTrapControl */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.112.15.30",    /* snmpAgentxContextName*/
  ""
 },
 {
  "1.3.6.1.4.1.2076.119.1.3.1.2",  /* fsMIRstSystemControl */
  "2"                              /* shutdown */
 },
 {
  "1.3.6.1.4.1.2076.119.1.3.1.3",  /* fsMIRstModuleStatus */
  "2"                              /* shutdown */
 },
 {
  "1.3.6.1.4.1.2076.118.1.3.1.2",  /* fsMIMstSystemControl */
  "1"                              /* start */
 },
 {
  "1.3.6.1.4.1.2076.118.1.3.1.3",  /* fsMIMstModuleStatus */
  "1"                              /* start */
 },
 {
  "1.3.6.1.4.1.2076.118.1.3.1.4",  /* fsMIMstMaxMstInstanceNumber */
  "64"                              
 },
 {
  "1.3.6.1.4.1.2076.118.1.3.1.26", /* fsMIMstMstiConfigIdSel */
  "0"                              
 },
 {
  "1.3.6.1.4.1.2076.118.1.3.1.27", /* fsMIMstMstiRegionName */
  "30:30:3a:30:31:3a:30:32:3a:30:33:3a:30:34:3a:30:31:00"                              
 },
 {
  "1.3.6.1.4.1.2076.118.1.3.1.28",  /* fsMIMstMstiRegionVersion */
  "0"                              
 },
 {
  "1.3.6.1.4.1.2076.118.1.3.1.38",  /* fsMIMstCistBridgeHelloTime */
  "200"                              
 },
 {
  "1.3.6.1.4.1.2076.118.2.3.1.1",   /* fsMIMstSetTraps */
  "3"
 },
 {
  "1.3.6.1.4.1.29601.2.6.1.1.1",   /* fsQoSSystemControl */
  "1"                              /* start */
 },
 {
  "1.3.6.1.4.1.29601.2.6.1.1.2",   /* fsQoSStatus */
  "1"                              
 },
 {
  "1.3.6.1.4.1.29601.2.6.1.1.3",   /* fsQoSTrcFlag*/
  "0"                             
 },
 {
  "1.3.6.1.4.1.29601.2.6.1.4.8.1.1",  /* fsQoSPortDefaultUserPriority */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.159.1.1",      /* fsElmiSystemControl */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.159.2.1",      /* fsElmiSetGlobalTrapOption */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.159.2.2",      /* fsElmiSetTraps */
  "0"
 },
 {
  "1.3.6.1.4.1.29601.2.45.2.4",    /* fsPtpNotification */
  ""
 },
 {
  "1.3.6.1.4.1.2076.153.1.1",      /* fsVrrpStatus */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.77.1.1",       /* fsTacClntActiveServer */
  "0.0.0.0"
 },
 {
  "1.3.6.1.4.1.2076.77.1.2",       /* fsTacClntTraceLevel */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.97.1.4",       /* sshTrace */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.97.1.6",       /*sshTransportMaxAllowedBytes */
  "32768"
 },
 {
  "1.3.6.1.4.1.2076.96.1.4",       /* sslTrace */
  "0"
 },
 {
  "1.3.6.1.4.1.29601.2.29.1.1",    /* fsTacClntExtActiveServerAddressType*/
  "0"
 },
 {
  "1.3.6.1.4.1.29601.2.29.1.2",    /* fsTacClntExtActiveServer */
  "00:00:00:00"
 },
 {
  "1.3.6.1.4.1.29601.2.8.1.2",     /* fsTacTraceOption */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.126.1.1.2",    /* ipCmnMRouteEnableCmdb */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.118.1.1",      /* fsMIMstGlobalTrace */
  "2",
 },
 {
  "1.3.6.1.4.1.2076.118.1.2",      /* fsMIMstGlobalDebug */
  "2",
 },
 {
  "1.3.6.1.4.1.2076.118.2.1",      /* fsMIDot1sFsMstSetGlobalTrapOption */
  "0",
 },
 {
  "1.3.6.1.4.1.2076.119.1.1",      /* fsMIRstGlobalTrace */
  "2",
 },
 {
  "1.3.6.1.4.1.2076.119.1.2",      /* fsMIRstGlobalDebug */
  "2",
 },
 {
  "1.3.6.1.4.1.2076.119.2.1",      /* fsMIRstSetGlobalTraps */
  "0",
 },
 {
  "1.3.6.1.4.1.2076.84.1.1",       /* dhcpSrvEnable */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.84.1.5",       /* dhcpSrvBootServerAddress */
  "0.0.0.0"
 },
 {
  "1.3.6.1.4.1.2076.84.1.6",       /* dhcpSrvDefBootFilename */
  "0"
 },
 {
  "1.3.6.1.4.1.29601.2.14.1.1.1.2",/* fsPbbBackboneEdgeBridgeName */
  "44:65:66:61:75:6c:74"
 },
 {
  "1.3.6.1.4.1.29601.3.4.1",       /* fsDsmonTrace */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.16.4.1.1",     /* fwlTrapMemFailMessage*/
  "4d:45:4d:4f:52:59:20:46:41:49:4c:55:52:45"
 },
 {
  "1.3.6.1.4.1.2076.16.4.1.2",     /* fwlTrapAttackMessage */
  "41:54:54:41:43:4b:20:45:58:43:45:45:44:45:44"
 },
 {
  "1.3.6.1.4.1.29601.2.31.1.1",     /* fsMIRtmThrottleLimit */
  "1000"
 },
 {
  "1.3.6.1.4.1.2076.13.3.1.10",     /* fsMplsFrrRevertiveMode */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.18.1",          /* fsTcpAckOption */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.18.2",          /* fsTcpTimeStampOption */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.18.3",          /* fsTcpBigWndOption */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.18.4",          /* fsTcpIncrIniWnd */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.18.5",          /* fsTcpMaxNumOfTCB */
  "500"
 },
 {
  "1.3.6.1.4.1.2076.18.6",          /* fsTcpTraceDebug */
  "0"
 },
 {
  "1.3.6.1.2.1.17.2.2",             /* dot1dStpPriority */
  "0"
 },
 {
  "1.3.6.1.2.1.17.2.12",            /* dot1dStpBridgeMaxAge */
  "2000"
 },
 {
  "1.3.6.1.2.1.17.2.13",            /* dot1dStpBridgeHelloTime */
  "200"
 },
 {
  "1.3.6.1.2.1.17.2.14",            /* dot1dStpBridgeForwardDelay */
  "1500"
 },
 {
  "1.3.6.1.2.1.17.4.2",             /* dot1dTpAgingTime */
  "300"
 },
 {
  "1.3.6.1.4.1.29601.2.38.1",       /* fsMIFsIpGlobalDebug */
  "0"
 },
 {
  "1.3.6.1.4.1.29601.2.38.25",      /* fsMIFsRarpServerTableMaxEntries */
  "20"
 },
 {
  "1.3.6.1.4.1.29601.2.22.1.1",     /* fsDcbPfcMinThreshold */
  "1"
 },
 {
  "1.3.6.1.4.1.29601.2.22.1.2",     /* fsDcbPfcMaxThreshold */
  "65535"
 },
 {
  "1.3.6.1.4.1.2076.41.1.3",        /* fsbgp4Identifier */
  "0.0.0.0"
 },
 {
  "1.3.6.1.4.1.2076.41.1.8",        /* fsbgp4DebugEnable */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.41.1.7.2",      /* fsbgp4RRDProtoMaskForEnable */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.41.1.7.3",      /* fsbgp4RRDSrcProtoMaskForDisable */
  "4230"
 },
 {
  "1.3.6.1.4.1.2076.41.1.7.5",      /* fsbgp4RRDRouteMapName */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.41.1.10.1.1",   /* fsbgp4RflbgpClusterId */
  "00:00:00:00"
 },
 {
  "1.3.6.1.4.1.2076.2.3.11",        /* fsDomainName */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.2.3.12",        /* fsTimeToLive */
  "600"
 },
 {
  "1.3.6.1.4.1.2076.2.10.3",        /* fsRarpServerTableMaxEntries */
  "20"
 },
 {
  "1.3.6.1.4.1.2076.2.16.1",        /* fsNoOfStaticRoutes */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.2.16.2",        /* fsNoOfAggregatedRoutes */
  "50"
 },
 {
  "1.3.6.1.4.1.2076.2.16.3",        /* fsNoOfRoutes */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.2.16.4",        /* fsNoOfReassemblyLists */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.2.16.5",        /* fsNoOfFragmentsPerList */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.2.17.1",        /* fsIpGlobalDebug */
  "0"
 },
 {
  "1.3.6.1.4.1.29601.2.48.1.5",     /* fsMIIpDbTraceLevel */
  "0"
 },
 {
  "1.3.6.1.2.1.32.2.1.1.3",         /* dnsResConfigMaxCnames */
  "0"
 },
 {
  "1.3.6.1.2.1.32.2.1.4.1",         /* dnsResCacheStatus */
  "1"
 },
 {
  "1.3.6.1.2.1.32.2.1.4.2",         /* dnsResCacheMaxTTL */
  "100"
 },
 {
  "1.3.6.1.2.1.32.2.1.5.1",         /* dnsResNCacheStatus */
  "0"
 },
 {
  "1.3.6.1.2.1.32.2.1.5.2",         /* dnsResNCacheMaxTTL */
  "0"
 },
 {
  "1.3.6.1.2.1.16.26.1.1.2",        /* dsmonAggControlLocked */
  "1"
 },
 {
  "1.3.6.1.2.1.4.1",                /* ipForwarding */
  "1"
 },
 {
  "1.3.6.1.2.1.4.2",                /* ipDefaultTTL */
  "64"
 },
 {
  "1.0.8802.1.1.1.1.1.1",           /* dot1xPaeSystemAuthControl */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.13.5.1.11",     /* pwNotifRate */
  "0"
 },
 {
  "1.3.6.1.2.1.16.13.3",            /* addressMapMaxDesiredEntries */
  "0"
 },
 {
  "1.3.6.1.2.1.16.19.4",            /* probeDateTime */
  "07:dc:01:0d:14:0a:05:00:00:00:00"
 },
 {
  "1.3.6.1.2.1.16.19.5",            /* probeDateTime */
  "1"
 },
 {
  "1.3.6.1.2.1.1.4",                /* sysContact */
  "41:72:69:63:65:6e:74:20:4c:74:64:2c:20:49:6e:64:69:61"
 },
 {
  "1.3.6.1.2.1.1.5",                /* sysName */
  "41:72:69:63:65:6e:74:20:4c:69:6e:75:78:20:52:6f:75:74:65:72:20:56:65:72:20:31:2e:30"
 },
 {
  "1.3.6.1.2.1.1.6",                /* sysLocation */
  "41:72:69:63:65:6e:74:20:4c:74:64:2c:20:49:6e:64:69:61"
 },
 {
  "1.3.6.1.2.1.11.30",              /* snmpEnableAuthenTraps */
  "2"
 },
 {
  "1.3.6.1.6.3.1.1.6.1",            /* snmpSetSerialNo */
  "0"
 },
 {
  "1.3.6.1.6.3.12.1.1",             /* snmpTargetSpinLock */
  "0"
 },
 {
  "1.3.6.1.6.3.15.1.2.1",           /* usmUserSpinLock */
  "0"
 },
 {
  "1.3.6.1.6.3.16.1.5.1",           /* vacmViewSpinLock */
  "0"
 },
 {
  "1.3.6.1.2.1.14.1.1",             /* ospfRouterId */
  "0.0.0.0"
 },
 {
  "1.3.6.1.2.1.14.1.2",             /* OspfAdminStat */
  "2"
 },
 {
  "1.3.6.1.2.1.14.1.5",             /* ospfASBdrRtrStatus */
  "2"
 },
 {
  "1.3.6.1.2.1.14.1.8",             /* ospfTOSSupport */
  "2"
 },
 {
  "1.3.6.1.2.1.14.1.12",            /* ospfDemandExtensions */
  "1"
 },
 {
  "1.3.6.1.2.1.14.16.1.1",          /* ospfSetTrap */
  "00:00:00:00"
 },
 {
  "1.3.6.1.2.1.4.25",               /* ipv6IpForwarding */
  ""
 },
 {
  "1.3.6.1.2.1.4.26",               /* ipv6IpDefaultHopLimit */
  "0"
 },
 {
  "1.3.6.1.2.1.4.33",               /* ipAddressSpinLoc */
  "0"
 },
 {
  "1.3.6.1.2.1.4.38",               /* ipv6RouterAdvertSpinLock */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.81.1.5",        /* issDefaultIpAddr */
  ""
 },
 {
  "1.3.6.1.4.1.2076.81.1.6",        /* issDefaultIpSubnetMask */
  "255.0.0.0"
 },
 {
  "1.3.6.1.4.1.2076.81.1.22",        /* issInitiateDlImage */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.81.1.23",        /* issLoggingOption */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.81.1.26",        /* issInitiateUlLogFile */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.81.1.34",        /* issSwitchDate */
  "30:34:20:30:31:20:31:31:20:32:30:3a:30:34:3a:35:38:20:32:30:31:32"
 },
 {
  "1.3.6.1.4.1.2076.81.1.35",        /* issNoCliConsole */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.81.1.47",        /* issFrontPanelPortCount */
  "1000"
 },
 {
  "1.3.6.1.4.1.2076.81.1.56",        /* issDownLoadUserName */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.81.1.57",        /* issDownLoadPassword */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.81.1.59",        /* issUploadLogUserName */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.81.1.60",        /* issUploadLogPasswd */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.81.1.62",        /* issConfigSaveUserName */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.81.1.60",        /* issConfigSavePassword */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.81.1.77",        /* issDebugOption */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.81.1.79",        /* issConfigSaveIpAddrType */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.81.1.80",        /* issConfigSaveIpvxAddr */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.81.1.81",        /* issConfigRestoreIpAddrType */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.81.1.82",        /* issConfigRestoreIpvxAddr */
  "0a:00:00:02"
 },
 {
  "1.3.6.1.4.1.2076.81.1.83",        /* issDlImageFromIpAddrType */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.81.1.84",        /* issDlImageFromIpvx */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.81.1.85",        /* issUploadLogFileToIpAddrType */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.81.1.86",        /* issUploadLogFileToIpvx */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.81.1.87",        /* issAuditLogRemoteIpAddrType */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.81.1.88",        /* issAuditLogRemoteIpvxAddr */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.81.1.89",        /* issSystemTimerSpeed */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.81.3.2",         /* issMirrorToPort */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.111.1.1.14",     /* fsPimCmnIpStatus */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.111.1.1.15",     /* fsPimCmnIpv6Status */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.20.1.1.14",      /* fsPimStatus */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.114.1.1.1",      /* fsPimStdJoinPruneInterval */
  "60"
 },
 {
  "1.3.6.1.3.61.1.1.1",              /* pimJoinPruneInterval */
  "60"
 },
 {
  "1.3.6.1.4.1.2076.120.1.2.1.2",    /* fsMIDot1qFutureVlanStatus */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.120.1.2.1.3",    /* fsMIDot1qFutureVlanMacBasedOnAllPorts */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.120.1.2.1.4",    /* fsMIDot1qFutureVlanPortProtoBasedOnAllPorts */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.120.1.2.1.7",    /* fsMIDot1qFutureVlanDebug */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.120.2.1.1.1",    /* fsMIDot1qFutureVlanBridgeMode */
  "7"
 },
 {
  "1.3.6.1.4.1.2076.120.1.2.1.14",   /* fsMIDot1qFutureGarpDebug */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.120.1.2.1.15",   /* fsMIDot1qFutureUnicastMacLearningLimit */
  "900"
 },
 {
  "1.3.6.1.4.1.2076.120.1.2.1.16",   /* fsMIDot1qFutureVlanBaseBridgeMode */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.120.1.2.1.17",   /* fsMIDot1qFutureVlanSubnetBasedOnAllPorts */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.120.1.12.1.1",   /* fsMIDot1qFutureStaticConnectionIdentifier */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.65.1.28",        /* dot1qFutureVlanSwStatsEnabled */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.149.1.1.8",      /* fsSntpAuthKeyId */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.149.1.1.10",      /* fsSntpAuthKey */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.149.1.1.11",      /* fsSntpTimeZone */
  "2b:20:30:3a:20:30"
 },
 {
  "1.3.6.1.4.1.2076.149.1.1.12",      /* fsSntpDSTStartTime */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.149.1.1.13",      /* fsSntpDSTEndTime */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.149.1.4.4",       /* fsSntpGrpAddrTypeInMcastMode */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.149.1.4.5",       /* fsSntpGrpAddrInMcastMode */
  "e0:00:01:01"
 },
 {
  "1.3.6.1.4.1.2076.149.1.5.5",       /* fsSntpGrpAddrTypeInAcastMode */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.149.1.5.6",       /* fsSntpGrpAddrInAcastMode */
  "00:00:00:00"
 },
 {
  "1.3.6.1.4.1.29601.2.6.1.1.1",      /* fsQoSSystemControl */
  "1"
 },
 {
  "1.3.6.1.4.1.29601.2.6.1.1.3",      /* fsQoSTrcFlag */
  "0"
 },
 {
  "1.3.6.1.4.1.29601.2.30.1.1.1",     /* fsRadExtDebugMask */
  "0"
 },
 {
  "1.3.6.1.4.1.29601.2.30.1.1.2",     /* fsRadExtMaxNoOfUserEntries */
  "10"
 },
 {
  "1.3.6.1.4.1.29601.2.30.1.1.3",     /* fsRadExtPrimaryServerAddressType */
  "0"
 },
 {
  "1.3.6.1.4.1.29601.2.30.1.1.4",     /* fsRadExtPrimaryServer */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.25.1.1",          /* radiusExtDebugMask */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.25.1.2",          /* radiusMaxNoOfUserEntries */
  "10"
 },
 {
  "1.3.6.1.4.1.2076.70.1.3",          /* fsmldTraceDebug */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.70.1.5",          /* fsmldProtocolUpDown */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.27.1.17",         /* ifmDebug */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.120.1.1",         /* fsMIDot1qFutureVlanGlobalTrace */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.120.1.8",         /* fsMIDot1qFutureGarpGlobalTrace */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.120.1.14",        /* fsMIDot1qFutureVlanSwStatsEnabled */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.150.1.1",         /* fsEcfmSystemControl */
  "1"
 },
 {
  "1,3,6,1,4,1,29601,2,105,1,1,1,3",  /* Fs2544ContextSystemControl*/
  "1"
 },
 {
  "1.3.6.1.4.1.2076.160.1.1.2",       /* fsMIEcfmOui */
  "00:02:02"
 },
 {
  "1.3.6.1.4.1.2076.150.1.14",        /* fsEcfmMemoryFailureCount */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.150.1.16",        /* fsEcfmUpCount */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.150.1.17",        /* fsEcfmDownCount */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.150.1.18",        /* fsEcfmNoDftCount */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.150.1.19",        /* fsEcfmRdiDftCount */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.150.1.20",        /* fsEcfmMacStatusDftCount */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.150.1.21",        /* fsEcfmRemoteCcmDftCount */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.150.1.22",        /* fsEcfmErrorCcmDftCount */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.150.1.23",        /* fsEcfmXconDftCount */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.150.1.24",        /* fsEcfmCrosscheckDelay */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.150.1.25",        /* fsEcfmMipDynamicEvaluationStatus */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.150.3.1",         /* fsEcfmTrapControl */
  "00"
 },
 {
  "1.3.6.1.4.1.2076.160.1.0.1.1.2",    /* fsMIEcfmSystemControl */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.160.1.0.1.1.27",   /* fsMIEcfmMipDynamicEvaluationStatus */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.160.1.0.1.1.16",   /* fsMIEcfmMemoryFailureCount */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.160.1.0.1.1.17",  /* fsMIEcfmBufferFailureCount */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.160.1.0.1.1.18",  /* fsMIEcfmUpCount */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.160.1.0.1.1.19",  /* fsMIEcfmDownCount */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.160.1.0.1.1.20",  /* fsMIEcfmNoDftCount */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.160.1.0.1.1.21",  /* fsMIEcfmRdiDftCount */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.160.1.0.1.1.22",  /* fsMIEcfmMacStatusDftCount */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.160.1.0.1.1.23",  /* fsMIEcfmRemoteCcmDftCount */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.160.1.0.1.1.24",  /* fsMIEcfmErrorCcmDftCount */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.160.1.0.1.1.25",  /* fsMIEcfmXconDftCount */
  "1"
 },
 {
  "1.3.6.1.4.1.29601.2.7.1.2.1.1.11", /* fsMIY1731TrapControl */
  "00:00"
 },
 {
  "1.3.6.1.4.1.29601.2.46.2.2",       /* fsClkIwfNotification */
  ""
 },
 {
  "1.3.6.1.4.1.29601.2.46.1.1.6",  /* fsClkIwfARBTime */
  "0"
 },
 {
  "1.3.6.1.4.1.29601.2.38.2.1.13",    /* fsMIFsIpContextDebug */
  "0"
 },
 {
  "1.3.6.1.4.1.29601.2.38.10.1.11",   /* fsMIFsIcmpDomainName */
  "0"
 },
 {
  "1.3.6.1.4.1.29601.2.38.10.1.12",   /* fsMIFsIcmpTimeToLive */
  "600"
 },
 {
  "1.3.6.1.4.1.29601.2.37.4.1.2",     /* fsMIStdIpForwarding */
  "1"
 },
 {
  "1.3.6.1.4.1.29601.2.37.4.1.3",     /* fsMIStdIpDefaultTTL */
  "64"
 },
 {
  "1.3.6.1.4.1.29601.2.37.4.1.5",     /* fsMIStdIpv6IpForwarding */
  "1"
 },
 {
  "1.3.6.1.4.1.29601.2.37.4.1.6",     /* fsMIStdIpv6IpDefaultHopLimit */
  "64"
 },
 {
  "1.3.6.1.4.1.2076.116.7.1.4.11.1.1",/* fsDot1qConstraintSetDefault */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.116.7.1.4.11.1.2",/* fsDot1qConstraintTypeDefault */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.120.1.2.1.6",    /* fsMIDot1qFutureGarpShutdownStatus */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.116.7.1.1.1.1.6", /* fsDot1qGvrpStatus */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.116.6.1.1.1.1.4", /* fsDot1dGmrpStatus */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.138.2.1.1.2",     /* fsMIVlanTunnelStpAddress */
  "01:00:0c:cd:cd:d0"
 },
 {
  "1.3.6.1.4.1.2076.138.2.1.1.2",     /* fsMIVlanTunnelLacpAddress */
  "01:00:0c:cd:cd:d4"
 },
 {
  "1.3.6.1.4.1.2076.138.2.1.1.2",     /* fsMIVlanTunnelDot1xAddress */
  "01:00:0c:cd:cd:d3"
 },
 {
  "1.3.6.1.4.1.2076.138.2.1.1.2",     /* fsMIVlanTunnelGvrpAddress */
  "01:00:0c:cd:cd:d1"
 },
 {
  "1.3.6.1.4.1.2076.138.2.1.1.2",     /* fsMIVlanTunnelGmrpAddress */
  "01:00:0c:cd:cd:d2"
 },
 {
  "1.3.6.1.4.1.2076.138.2.1.1.2",     /* fsMIVlanTunnelMvrpAddress */
  "01:00:0c:cd:cd:d5"
 },
 {
  "1.3.6.1.4.1.2076.138.2.1.1.2",     /* fsMIVlanTunnelMmrpAddress */
  "01:00:0c:cd:cd:d6"
 },
 {
  "1.3.6.1.4.1.2076.62.1.1",          /* fsIsisMaxInstances */
  "10"
 },
 {
  "1.3.6.1.4.1.2076.62.1.2",          /* fsIsisMaxCircuits */
  "10"
 },
 {
  "1.3.6.1.4.1.2076.62.1.3",          /* fsIsisMaxAreaAddrs */
  "5"
 },
 {
  "1.3.6.1.4.1.2076.62.1.4",          /* fsIsisMaxAdjs */
  "320"
 },
 {
  "1.3.6.1.4.1.2076.62.1.5",          /* fsIsisMaxIPRAs */
  "2500"
 },
 {
  "1.3.6.1.4.1.2076.62.1.6",          /* fsIsisMaxEvents */
  "10"
 },
 {
  "1.3.6.1.4.1.2076.62.1.7",          /* fsIsisMaxSummAddr */
  "20"
 },
 {
  "1.3.6.1.4.1.2076.62.1.8",          /* fsIsisStatus */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.62.1.9",          /* fsIsisMaxLSPEntries */
  "1000"
 },
 {
  "1.3.6.1.4.1.2076.62.1.10",         /* fsIsisMaxMAA */
  "5"
 },
 {
  "1.3.6.1.4.1.2076.62.1.11",         /* fsIsisFTStatus */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.62.1.13",         /* fsIsisFactor */
  "60"
 },
 {
  "1.3.6.1.4.1.2076.62.1.14",         /* fsIsisMaxRoutes */
  "3000"
 },
 {
  "1.3.6.1.4.1.2076.145.1.1",         /* fsMIOspfGlobalTraceLevel */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.145.1.10",        /* fsMIOspfGlobalExtTraceLevel */
  "0"
 },
 {
  "1.3.6.1.4.1.29601.2.32.1.1",       /* fsMIRtm6GlobalTrace */
  "0"
 },
 {
  "1.3.6.1.4.1.29601.2.35.1.11",      /* fsMIIpv6GlobalDebug */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.71.1.1.1",        /* ipMRouteEnable */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.71.1.1.3",        /* ipMRouteEnableCmdb */
  "2"
 },
 {
  "1.3.6.1.2.1.55.1.1",               /* ipv6Forwarding */
  "2"
 },
 {
  "1.3.6.1.2.1.4.2",                  /* ipDefaultTTL */
  "64"
 },
 {
  "1.3.6.1.2.1.4.1",                  /* ipForwarding */
  "1"
 },
 {
  "1.3.6.1.3.92.1.1.1",              /* msdpEnabled */
  "0"
 },
 {
  "1.3.6.1.3.92.1.1.2",              /* msdpCacheLifetime */
  "0"
 },
 {
  "1.3.6.1.3.92.1.1.11",             /* msdpRPAddress */
  "0.0.0.0"
 },
 {
  "1.3.6.1.4.1.2076.13.1.1.3",        /* fsMplsFmDebugLevel */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.13.1.1.6",        /* fsMplsDiffServElspPreConfExpPhbMapIndex */
  "-1"
 },
 {
  "1.3.6.1.4.1.2076.13.1.1.4",        /* fsMplsTeDebugLevel */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.13.1.4.1",        /* fsMplsCrlspDebugLevel */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.13.1.4.2",        /* fsMplsCrlspDumpType */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.13.1.5.1.7",      /* fsMplsLocalCCTypesCapabilities */
  "e0"
 },
 {
  "1.3.6.1.4.1.2076.13.1.5.1.8",      /* fsMplsLocalCVTypesCapabilities */
  "40"
 },
 {
  "1.0.8802.1.1.2.1.1.6.1.4",         /* lldpPortConfigTLVsTxEnable */
  "00"
 },
 {
  "1.3.6.1.4.1.2076.60.1.5",          /* dvmrpStatus */
  "2"
 },
 {
  "1.3.6.1.4.1.2076.60.1.6",          /* dvmrpLogEnabled */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.60.1.7",          /* dvmrpLogMask */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.127.1.1.1.2",     /* fsMIPbMulticastMacLimit */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.24.1.16",           /* dhcpConfigDhcpCircuitOption */
  "01:00:00:00"                         /* router-index */
 },
 {
  "1.3.6.1.4.1.2076.87.1.4",          /* dhcpClientFastAccess */
  "2"
 },
 {
  NULL,
  NULL,
 }
};

/* gaClrBridgeArray is used to handle the mib objects which is not present in the mibsave.h.
 * Here all the Oid's has to be added along with its index */

tScalarObject gaClrBridgeArray[] =
{
 {
  "1.3.6.1.4.1.29601.2.99.1.3.0",       /* fsDnsTraceOption */
  "0"
 },
 {
   "1.3.6.1.4.1.2076.119.1.3.1.2.0",     /* fsMIRstSystemControl */
   "2"                                   /* shutdown */
 },
 {
  "1.3.6.1.4.1.2076.81.1.43.0",           /* issConfigAutoSaveTrigger */
  "2"                                   /* disable */
 },
 {
   "1.3.6.1.4.1.2076.81.1.44.0",           /* issConfigIncrSaveFlag */
   "2"                                  /* disable */
 },
 {
  "1.3.6.1.4.1.2076.118.1.3.1.2.0",     /* fsMIMstSystemControl */
  "2"                                   /* shutdown */
 },
 {
  "1.3.6.1.4.1.2076.116.7.1.1.1.1.6.0", /* fsDot1qGvrpStatus */
  "2"           /* disable */
 },
 {
  "1.3.6.1.4.1.2076.116.6.1.1.1.1.4.0", /* fsDot1dGmrpStatus */
  "2"           /* disable */
 },
 {
  "1.3.6.1.4.1.2076.120.1.2.1.6.0",     /* fsMIDot1qFutureGarpShutdownStatus */
  "1"                                   /* disable */
 },
 {
  "1.3.6.1.4.1.2076.150.1.1.0",         /* fsEcfmSystemControl */
  "2"                                   /* shutdown */
 },
 {
  "1,3,6,1,4,1,29601,2,105,1,1,1,3.0",   /* Fs2544ContextSystemControl*/
  "1"
 },
 {
  "1.3.6.1.4.1.29601.2.25.2.1.1.2.0",   /* fsElpsContextSystemControl */
  "2"                                   /* shutdown */
 },
 {
  "1.3.6.1.4.1.29601.2.19.1.0",         /* fsRmon2Trace */
  "6"
 },
 {
  "1.3.6.1.4.1.2076.41.1.2.0",          /* fsbgp4LocalAs */
  "0"                                   /* disable */ 
 },
 {
  "1.3.6.1.4.1.2076.126.1.1.1.0",       /* ipCmnMRouteEnable */
  "1"                                   /* enable */
 },
 {
  "1.3.6.1.4.1.29601.2.30.1.1.1.0",     /* fsRadExtDebugMask */
  "0"
 },
 {
  "1.3.6.1.4.1.2076.44.6.0",            /* rmonHwHostSupp */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.44.7.0",           /* rmonHwHostTopNSupp */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.44.8.0",           /* rmonHwMatrixSupp */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.138.1.1.1.2.0",     /* fsMIVlanBridgeMode */
  "1"
 },
 {
  "1.3.6.1.4.1.2076.118.1.3.1.2.0",     /* fsMIMstSystemControl */
  "1"                                   /* start */
 },
 {
  "1.3.6.1.4.1.2076.118.1.3.1.3.0",     /* fsMIMstModuleStatus */
  "1"                                   /* start */
 },
 {
  "1.3.6.1.4.1.2076.120.1.2.1.6.0",     /* fsMIDot1qFutureGarpShutdownStatus */
  "2"                                   /* enable */
 },
 {
  "1.3.6.1.4.1.2076.116.7.1.1.1.1.6.0", /* fsDot1qGvrpStatus */
  "1"           /* enable */
 },
 {
  "1.3.6.1.4.1.2076.116.6.1.1.1.1.4.0", /* fsDot1dGmrpStatus */
  "1"           /* enable */
 },
 {
  "1.3.6.1.4.1.2076.150.1.1.0",         /* fsEcfmSystemControl */
  "1"                                   /* start */
 },
  {
  NULL,
  NULL,
 }
};

CHR1 *gaClrPackageArray[] =
{
      "fsclkiwf",
      "fsm1ad",
      "fsmpb",
      "fsmpvlan",
      "fsmsbex",
      "fstunl",
      "fsmsbext",
      "fsmsbrg",
      "fsmsvlan",
      "fsmvlext",
      "fspb",
      "fsdot1ad",
      "fsvlan",
      "fsvlnext",
      "std1d1ap",
      "std1q1ap",
      "pBridgeMIB",
      "stdbridge",
      "stddot1ad",
      "qBridgeMIB",
      "stdotqbr",
      "fsmpbrst",
      "fsmprst",
      "fsmsrst",
      "fspbrst",
      "fsrst",
      "fsmppvrst",
      "fspvrst",
      "std1w1ap",
      "fsmpmst",
      "fsmst",
      "std1s1ap",
      "fsla",
      "stdla",
      "fslldp",
      "sd1lv2",
      "sd3lv2",
      "slldv2",
      "stdlldp",
      "stdot1lldp",
      "stdot3lldp",
      "STDIP",
      "stdtcp",
      "fsipv6",
      "fsmpipv6",
      "fsmirtm",
      "futureip",
      "fsmpip",
      "fsrtm",
      "ipv6MIB",
      "fseoamex",
      "fsfm",
      "stdeoam",
      "cfmv2ext",
      "fsipvx",
      "fsmpipvx",
      "fsmsipvx",
      "stdipvx",
      "cfmv2",
      "fscfmext",
      "fscfmmi",
      "fsecfm",
      "fs2544",
      "fsmiy1731",
      "fselps",
      "fserps",
      "fssnmp3",
      "snmcom",
      "snmmpd",
      "snmpnotify",
      "stdsnproxy",
      "fsmptcp",
      "fsmstcpipvx",
      "stdtcpipvx",
      "fsmsudpipvx",
      "stdudpipvx",
      "fstcp",
      "snmusm",
      "sntarget",
      "snmpMIB",
      "stdvac",
      "fstacacs",
      "fstacsxt",
      "fspnac",
      "stdpnac",
      "fssnp",
      "fselmi",
      "fsdhcpRelay",
      "fsdhclient",
      "futureDhcpSrvMIB",
      "fsdh6s",
      "fsdh6r",
      "fsdh6c",
      "fsdns",
      "stddns",
      "fssyslg",
      "fssntp",
      "fsrmon",
      "stdrmon",
      "fsrmn2",
      "fsuser",
      "stdrmon2",
      "stdrmonv2",
      "fsiss",
      "fscfa",
      "fsipdb",
      "fsmiipdb",
      "fssystem",
      "fsdhcsnp",
      "fsmidhcsnp",
      "fsmptunl",
      "ifMIB",
      "stdeth",
      "mauMod",
      "fsradext",
      "fsradi",
      "radacc",
      "radaut",
      "fshttp",
      "fssshmib",
      "fsssl",
      "fsarp",
      "fsmparp",
      "fsmpping",
      "fsping",
      "fsvcm",
      "fssisp",
      "fssispwf",
      "fsrm",
      "fsmbsm",
      "fsqosxtd",
      "stdqos",
      "fsissext",
      "fsissmet",
      "fsissacl",
      "stdent",
      "fsmirtm6",
      "fsrtm6",
      "pppoe",
      "ppipcpfs",
      "pplcpfs",
      "pppgenfs",
      "stdauth",
      "stdbcp",
      "stdipcp",
      "stdlcp",
      "fsEsat",
      "fsY1564",
      "fsofc",
      "fsos3",
   "stdisis",
   "fsisis",
   "fsmidhcpRelay",
      NULL,
};

/* Function prototypes */
INT4 MsrSetDefaultValue ARG_LIST((tSNMP_OID_TYPE *, tSNMP_MULTI_DATA_TYPE *,
   tSNMP_MULTI_DATA_TYPE *, tSnmpIndex *));
INT4 MsrValidateTableRowStatus ARG_LIST((tSNMP_OID_TYPE *,
   tSNMP_OID_TYPE *,
   tSNMP_MULTI_DATA_TYPE *,
   UINT4));
INT4                                                        
ClearConfigDefaultValueForObj (tSNMP_OID_TYPE *pOID);
INT4
ClearConfigSetValueForArray (tScalarObject *pArray);
extern VOID  SntpSetDstDefaults (VOID);
extern INT4 SntpLock (VOID);
extern INT4 SntpUnLock (VOID);
extern VOID SntpGblParamsInit (VOID);
#endif
