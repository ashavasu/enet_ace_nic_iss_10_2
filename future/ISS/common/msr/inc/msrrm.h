/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: msrrm.h,v 1.16 2016/06/30 10:13:50 siva Exp $
*
* Description: Header file MSR-RM definitions. 
*********************************************************************/
#ifndef _MSR_RM_H
#define _MSR_RM_H

#define MSR_Q_NAME  "MSRQ"

#define MSR_FILE_RCV_TASK "MFR"
#define MSR_FILE_RCV_PRI  12 
#define MSR_SLOT_INFO_RCV_TASK "MSIR"
#define MSR_SLOT_INFO_RCV_PRI  12 

/* TCP port to be used to static bulk updates */
#define MSR_SERVER_TCP_PORT 7214

/* Maximum packet size to be transmitted */
#define MSR_RM_MAX_PKT_SIZE (1500 + RM_HDR_LENGTH)
#define MSR_MAX_FRAGMENT 65535

/* Macros to add data to linear buffer */
#define MSR_RM_ADD_BYTE(dest,src,len) \
                        MEMCPY (dest, src, len);dest += len;

/* Macros to get data from linear buffer */
#define MSR_RM_GET_BYTE(dest,src,len) \
                        MEMCPY (dest, src, len);src += len;

/* Maximum RB Nodes that can be sent to the standby node */
#define MSR_RM_MAX_NODE_COUNT 15

/* Macro to copy the index of tMSRRbtree node */
#define MSR_RM_UPDATE_INDEX(pDestNode,pSrcNode) \
        pDestNode->MSRArrayIndex = pSrcNode->MSRArrayIndex; \
        pDestNode->MSRRowStatusType = pSrcNode->MSRRowStatusType; \
        MEMSET (pDestNode->pOidValue->pu4_OidList, 0, MSR_MAX_OID_LEN); \
        pDestNode->pOidValue->u4_Length = pSrcNode->pOidValue->u4_Length; \
        MEMCPY (pDestNode->pOidValue->pu4_OidList, \
                pSrcNode->pOidValue->pu4_OidList, \
                (pSrcNode->pOidValue->u4_Length * 4));


/* Timer Id used for RM module */
enum
{
    MSR_RM_RECONNECT_TIMER  = 0,
    MSR_RM_MAX_TIMER_TYPES
};

/* Timer Interval */
#define MSR_RM_RECONNECT_INTERVAL 500


/* MSR data format */
typedef struct {
   /* MsgType - CONFIG_CHANGE/FLASH_CKSUM_REQ/FLASH_CKSUM_RESP */
   UINT4 u4MsgType;

   /* Flash Checksum */
   UINT2 u2FlashCksum;

   /* Padding */
   UINT2 u2Pad;

#define MSR_DATA_OFF_MSG_TYPE     0
#define MSR_DATA_OFF_FLASH_CKSUM  4

} tMsrData;

/* Global structure used for redundancy support */
typedef struct MSRRmGlobalInfo {
    VOID               *pBuf;           /* Temporary buffer used by active node
                                         * during unsuccessful write operation
                                         * and by standby node during
                                         * unsuccessful read operation
                                         */
    tMemPoolId          MsrQPoolId;     /* Memory pool id for the MSR Queue
                                         * message
                                         */
    tMemPoolId          MsrRmHdrPoolId; /* Memory Pool id for the RM Header */                                               
    tMemPoolId          MsrRmPktPoolId; /* Memory Pool id for the RM Packet */
    tOsixQId            MsrQId;         /* Queue for redundancy support */
    tTimerListId        TmrListId;      /* Timer list id */
    tTmrDesc            aTmrDesc[MSR_RM_MAX_TIMER_TYPES];
                                        /* Timer descriptor used to maintain
                                         * the timer function pointers and
                                         * structure offset used for RM
                                         */
    tTmrBlk             aTmrBlk[MSR_RM_MAX_TIMER_TYPES];
                                        /* Timer block containing the pointer
                                         * to the timer structure and timer
                                         * id
                                         */
    UINT4               u4BytesSentOrRcvd;
                                        /* No of bytes sent or received */
    INT4                i4SrvSockFd;    /* Server socket fd used in the active
                                         * node for receiving a new connection
                                         */
    INT4                i4ConnFd;       /* Connection fd used by the active and
                                         * standby node for sending and
                                         * receiving packets
                                         */
    tMemPoolId          BufPoolId;      /* Id to differtiate whether pBuf holds
                                           the block from MsrRmHdrPoolId or
                                           MsrRmPktPoolId 
                                         */
    UINT1               u1NumPeers;     /* Number of peers */
    INT1                i1IsTmrRunning; /* Flag used to indicate whether the
                                         * re-connect timer is running or not
                                         */
    UINT1               au1Reserved[2]; /* 4-byte alignment */
} tMSRRmGlobalInfo;

/* Structure used to store the last RB Tree index transmitted in
 * the current static bulk update. This is used to get the next RB Tree
 * node when MSR_RM_SUB_BULK_UPDATE is received. This structure contains
 * the index of tMSRRbtree
 */
typedef struct MSRRmLastUpdatedNode {
    UINT4               MSRArrayIndex;
    UINT4               MSRRowStatusType;
    tSNMP_OID_TYPE     *pOidValue;
} tMSRRmLastUpdatedNode;

/* Structure used to store the queue messages received from RM module */
typedef struct MSRRmQueueMsg {
    tRmMsg              *pRmMsg;  /* Pointer to the message from RM */
    UINT1                u1Event; /* Event received from RM */
    UINT1                au1Reserved[3];
                                  /* 4-byte alignment */
} tMSRRmQueueMsg;

typedef struct _tMsrRmHdrBlock 
{
    UINT1              au1MsrRmHdrBlock[RM_HDR_LENGTH + 1];
    UINT1              au1Reserved[3];
}tMsrRmHdrBlock;

typedef struct _tMsrRmPktBlock
{
    UINT1              au1MsrRmPktBlock[MSR_RM_MAX_PKT_SIZE];
}tMsrRmPktBlock;

/* Id for the oid pool and multidata pool */
enum 
{
    MSR_RM_OID1 = 0,
    MSR_RM_OID2,
    MSR_RM_OID3,
    MSR_RM_OID4,
    MSR_RM_OID5,
    MSR_RM_OID6,
    MSR_RM_MULTIDATA_OID,
    MSR_RM_DEF_MULTIDATA1_OID,
    MSR_RM_DEF_MULTIDATA2_OID,
    MSR_RM_MAX_OID
};
enum
{
    MSR_RM_MULTIDATA = 0,
    MSR_RM_DEF_MULTIDATA1,
    MSR_RM_DEF_MULTIDATA2,
    MSR_RM_MAX_DATA
};

/* Function prototypes */
INT4  MsrRmGlobalInit PROTO ((VOID));
VOID  MsrRmGlobalDeInit PROTO ((VOID));
VOID  MsrFileRcvTaskMain (INT1 *piArg);
VOID  MsrProcessRmGoActive (VOID);
VOID  MsrProcessRmGoStandby (VOID);
INT4  MsrRcvPktFromRm (UINT1 u1Event, tRmMsg *pData, UINT2 u2DataLen);
INT4  MsrSendMsgToRm (UINT4 u4MsgType);
VOID  MsrProcessRmEvt (VOID);
VOID  MsrProcessRmFileTransferComplete (VOID);
VOID  MsrRestartProtocolModules (VOID);

/* Funtion prototypes used for static configuration sync up */

VOID MsrRmTmrInitTmrDesc PROTO ((VOID));

VOID MsrRmTmrHandleExpiry PROTO ((VOID));

VOID MsrRmReConnectTmrExpiry PROTO ((VOID * pArg));

VOID MsrRmProcessStandbyDownEvent PROTO ((VOID));

VOID MsrRmProcessNewConnEvent PROTO ((VOID));

VOID MsrRmSendStaticBulkRequest PROTO ((VOID));

UINT1 * MsrRmConstructStaticBulkReqOrTail PROTO ((UINT1 u1MessageType));

UINT1 * MsrRmConstructStaticBulkUpdate
PROTO ((UINT1 * pu1NodeCount, UINT4 * pu4PktLen));

VOID MsrRmSendStaticBulkUpdate PROTO ((VOID));

UINT4 MsrRmAddRbNodeToBuf
PROTO ((UINT1 * pu1CurPtr, tMSRRbtree * pMsrRbNode, UINT4  u4TotalLen));

VOID MsrRmSendStaticBulkTail PROTO ((VOID));

VOID MsrRmProcessIncomingPacket PROTO ((VOID));

VOID MsrRmProcessPacket PROTO ((UINT1 * pu1Buf, UINT4 u4PktLen));

VOID MsrRmInitiateStaticBulkUpdate PROTO ((VOID));

VOID MsrRmSendRestorationComplete PROTO ((VOID));

VOID MsrRmProcessStaticBulkUpdate PROTO ((UINT1 * pu1Buf, UINT4 u4PktLen));

tMSRRbtree * MsrRmGetRBNodeFromBuf PROTO ((UINT1 * pu1Buf));

INT4 MsrRmHandleFailureBuffer PROTO ((UINT1 * pu1Buf, INT4 i4WriteBytes,
                                      tMemPoolId u1BufPoolId));

VOID MsrRmSelWriteCallBkFn PROTO ((INT4 i4SockFd));

VOID MsrRmSendRemainingUpdate PROTO ((UINT4 *pu4RemainingStatus));

VOID MsrRmHandlePartialRcvPkt PROTO ((UINT1 * pu1Buf, INT4 i4ReadBytes));

INT4 MsrRmRcvRemainingUpdate PROTO ((UINT1 * pu1Buf));

/********** Function prototypes used in msrrmskt.c **********/

INT4 MsrRmServerInit PROTO ((UINT4 u4Addr));

INT4 MsrRmClientInit PROTO ((UINT4 u4Addr));

VOID MsrRmServerDeInit PROTO ((INT4 i4Sockfd));

VOID MsrRmChannelDeInit PROTO ((INT4 i4Sockfd));

VOID MsrRmNewConnReq PROTO ((INT4 i4SockFd));

VOID MsrRmPktArrival PROTO ((INT4 i4SockFd));

INT4 MsrRmAcceptConnection PROTO ((INT4 i4SrvSockFd));

INT4 MsrRmSendPkt
PROTO ((INT4 i4SockFd, UINT1 * pu1Buf, UINT2 u2Len, INT4 * pi4WriteBytes));

INT4 MsrRmRcvPkt
PROTO ((INT4 i4SockFd, UINT1 * pu1Buf, UINT2 u2Len, INT4 * pi4ReadBytes));

VOID MsrRmSelAddFd
PROTO ((INT4 i4Fd, VOID (*pCallBk) (INT4), INT1 i1ReadFlag));

VOID
MsrRmSelRemoveFd PROTO ((INT4 i4Sockfd, INT1 i1ReadFlag));

VOID MsrRmSockClose PROTO ((INT4 i4Sockfd));
/*************************************************************/


VOID MsrRmFileReadForSelfAttach (VOID);
#endif /* MSR_RM_H */
