/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                         */
/* $Id: msrsys.h,v 1.282.2.1 2018/05/29 10:46:45 siva Exp $*/
/*****************************************************************************/
/*    FILE  NAME            : msrsys.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : ISS System                                     */
/*    MODULE NAME           :                                                */
/*    ISSNGUAGE             : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : ISS Team                                       */
/*    DESCRIPTION           : This file has all the includes, type defines,  */
/*                            prototypes, externs, defines for MSR module.   */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
#ifndef _MSRSYS_H
#define _MSRSYS_H

#include "lr.h"
#include "iss.h"
#include "cfa.h"
#include "vcm.h"
#include "ip.h"
#include "arp.h"
#include "bridge.h"
#include "la.h"
#include "elm.h"
#include "eoam.h"
#include "lldp.h"
#include "fsvlan.h"
#include "pbbte.h"
#include "l2iwf.h"
#include "garp.h"
#include "rstp.h"
#include "elm.h"
#include "mstp.h"
#include "pvrst.h"
#include "ecfm.h"
#include "pnac.h"
#include "snp.h"
#include "fssyslog.h"
#include "fssnmp.h"
#include "tftpc.h"
#include "fsbuddy.h"
#include "dhcp.h"
#include "fips.h"
#include "bgp.h"
#include "dvmrp.h"
#include "ospf3.h"
#include "vrrp.h"
#include "rip.h"

#ifdef ERPS_WANTED
#include "erps.h"
#endif

#ifdef EOAM_FM_WANTED
#include "eoamfm.h"
#endif
#ifdef ELPS_WANTED
#include "elps.h"
#endif

#ifdef MBSM_WANTED
#include "cfambsm.h"
#endif

#ifdef L2RED_WANTED
#include "hwaud.h"
#include "cfared.h"
#endif

#include "cfaport.h"
#include "ifmmain.h"
#include "cfaport.h"
#include "msr.h"

#include "rmgr.h"
#include "msrrm.h"

#include "astmlow.h"
#include "astmcons.h"

#include "issmacro.h"
#include "isstdfs.h"
#include "issglob.h"
#ifdef CLI_WANTED
#include "cli.h"
#endif

#include "snmpcmn.h"


#include "ipv6.h"

#ifdef IGMP_WANTED
#include "igmp.h"
#endif

#ifdef OSPF_WANTED
#include "ospf.h"
#endif

#ifdef MPLS_WANTED
#include "cfa.h"
#include "mpls.h"
#endif

#ifdef VRRP_WANTED
#include "vrrp.h"
#endif

#ifdef WLC_WANTED
#ifdef RFMGMT_WANTED
#include "rfmconst.h"
#endif
#ifdef WSSUSER_WANTED
#include "userrole.h"
#endif
#endif

#include "msrdef.h"


#define MSR_Q_NAME  "MSRQ"

#define ISS_ARE_MAC_ADDR_EQUAL(pMacAddr1, pMacAddr2) \
        ((MEMCMP(pMacAddr1,pMacAddr2,ISS_ETHERNET_ADDR_SIZE)) ? \
          ISS_FALSE : ISS_TRUE)
/* Type defines */

typedef tMemPoolId          tMsrMemPoolId;
/* All the scalar and the table names to be to be saved are stored in this 
 * datastructure format */

typedef struct _SaveObject
{
   const char *pOidString;    /* The scalar / table object OID   */
   const char *pRowStatus;    /* The rowstatus OID of the table  */
   INT4 (*fnVal)(tSNMP_OID_TYPE *, 
         tSNMP_OID_TYPE *, 
         tSNMP_MULTI_DATA_TYPE *);
                              /* Pointer to a validate routine which
                   * takes the following arguments:
                   * pointer to OID, 
                   * pointer to Instance OID,
                   * pointer to the data to be saved.
                   */
   UINT2 u2Type;              /* 1 : scalar, 2 : table, 0 : none */
   UINT2 u2TableIndexType;    /* 1: Context based  2: Interface Index based
                               * 0: None  */
}tSaveObject;

typedef struct {
   UINT4 u4ConfigRestoreStatus;  /* Restoration status - Successful */
} tMsrNotifyMsg;


typedef struct {
    UINT1  **pOidArray;   /* List of oids registered by protocol */
    INT4   i4ContextId;
    UINT2  u2OidNo;       /* No. of Oids in the Oid Array */
    UINT2  u2Resvd;
} tMsrOidList;

typedef struct {
    union {
        tMsrOidList     *pMsrOidList;
        tMSRUpdatedata  *pMsrUpdateData;
        UINT4            u4Index;
    }MsrCfgData;
    UINT2    u2MsgType;
    UINT2 u2Padding;
}tMsrConfigData;

typedef struct _tMsrOidArrBlock
{
    UINT1      *pu1MsrOidArrBlock[MAX_OID_NO];
}tMsrOidArrBlock;

typedef struct _tMsrOidArrListBlock
{
    UINT1      au1MsrOidArrListBlock[MSR_MAX_OID_LEN];
}tMsrOidArrListBlock;

typedef struct _tMsrMibEntryBlock
{
    UINT1      au1MsrMibEntryBlock[MSR_MIBENTRY_MEMBLK_SIZE];
}tMsrMibEntryBlock;

/* Application CallBack Structure */
typedef union MsrUtilCallBackEntry
{
    INT4 (*pMsrCustSecureStorage) PROTO ((UINT4, UINT4, ...));
 INT4 (*pMsrCustValidateConfResFile) PROTO ((INT4));
}unMsrUtilCallBackEntry;

#include "msrsz.h"

extern UINT1    *gpu1StrEnd ;         

extern INT4      gi4MibSaveStatus;  
extern INT4      gi4RemoteSaveStatus;   
extern INT4      gi4MibResStatus;
extern INT4   gi4MibResFailureReason;
extern UINT1     gu1MibSaveLocal;  
extern UINT1     gu1MibRestoreLocal; 
extern tMsrPacket    gMsrData;       
extern UINT4     gu4MsrTrcFlag;   
extern tMsrMemPoolId  gMsrMibPoolId; 
extern UINT4  gu4MsrSysLogId;
extern INT4  gi4TftpSysLogId;
extern UINT4         gu4IncrLength;
extern UINT1 gu1CfaBootFlag;
extern CHR1 *gaClrPackageArray[];
extern tTimerListId      gMsrDefSysIfUpTmrId; 
extern tMsrNotifyMsg     gNotifMsg;
extern tSnmpIndex MsrIndexPool1[];
extern tSNMP_MULTI_DATA_TYPE MsrMultiPool1[];
extern tSNMP_OCTET_STRING_TYPE MsrOctetPool1[];
extern tSNMP_OID_TYPE  MsrOIDPool1[];
extern UINT1 au1MsrData1[MSR_MAX_INDICES][1024];
extern tSnmpIndex MsrIndexPool2[];
extern tSNMP_MULTI_DATA_TYPE MsrMultiPool2[];
extern tSNMP_OCTET_STRING_TYPE MsrOctetPool2[];
extern tSNMP_OID_TYPE  MsrOIDPool2[];
extern UINT1 au1MsrData2[MSR_MAX_INDICES][1024];
extern tSNMP_OID_TYPE gMsrOidType[];
extern tSNMP_OID_TYPE gMsrClrCfgOidType[];
extern UINT4 gau4MsrOidType[MSR_MAX_OID_TYPE][MSR_MAX_OID_LEN];
extern UINT4 gu4MsrOidCount;
extern tSNMP_MULTI_DATA_TYPE  gMsrMultiData[];
extern UINT4 gau4MsrClrCfgOidType[MSR_MAX_OID_TYPE][MSR_MAX_OID_LEN];
extern UINT4 gu4MsrClrCfgOidCount;
extern UINT4 gu4MsrMultiDataCount;
extern tSNMP_OCTET_STRING_TYPE gMsrOctetString[];
extern UINT1 gau1MsrOctetString[MSR_MAX_OCTET_STRING][MSR_MAX_OCTET_STRING_LEN];
extern UINT4 gu4MsrOctetStrCount;
extern UINT4 gu4MsrAutoSaveOIDIndex;
extern UINT4 gu4MsrSaveIPOptionOIDIndex;
extern UINT4 gu4MsrSaveIPAddrOIDIndex;
extern UINT4 gu4MsrSaveFileNameOIDIndex;
extern UINT1               gu1RamState ;
extern UINT2               gu2FlashCksum;
extern UINT4               gu4noMsrUpdateQReq;
extern tOsixSemId          MsrUpdateQsemId;
extern tOsixSemId          MsrUpdateQSsemId;
extern tIssBool            MsrisRetoreinProgress;
extern tIssBool            MsrisSaveComplete;
extern UINT4               gu4MibEntryBlkSize;
extern INT4                gTempi4MibSaveStatus;
extern INT4                gTempi4RemoteSaveStatus;
#ifdef RM_WANTED
extern INT1                gi1ConfigChange;
extern UINT1        gu1MsrNodeStatus;
extern UINT1        gu1MsrPrevNodeStatus;
extern tSnmpIndex          MsrRmIndexPool1[];
extern tSnmpIndex          MsrRmIndexPool2[];
extern tSNMP_MULTI_DATA_TYPE MsrRmMultiPool1[];
extern tSNMP_MULTI_DATA_TYPE MsrRmMultiPool2[];
extern tSNMP_OCTET_STRING_TYPE MsrRmOctetPool1[];
extern tSNMP_OCTET_STRING_TYPE MsrRmOctetPool2[];
extern tSNMP_OID_TYPE      MsrRmOidPool1[];
extern tSNMP_OID_TYPE      MsrRmOidPool2[];
extern UINT1               au1MsrRmData1[][MSR_MAX_DATA_LEN];
extern UINT1               au1MsrRmData2[][MSR_MAX_DATA_LEN];
extern tSNMP_OID_TYPE      gMsrRmOidType[];
extern UINT4               gau4MsrRmOidType[][MSR_MAX_OID_LEN];
extern tSNMP_MULTI_DATA_TYPE  gMsrRmMultiData[];
extern tSNMP_OCTET_STRING_TYPE gMsrRmOctetString[];
extern UINT1               gau1MsrRmOctetString[][MSR_MAX_DATA_LEN];
extern tOsixTaskId         gMsRmTaskId;
extern UINT1               gau1MsrBuffer[];
#endif
extern tTmrAppTimer        gSysDefIfUpTimer;
extern FILE               *gSavefp ;
extern UINT4               gU4NoOfQMsgSend;
extern UINT4               gU4NoOfQMsgRead;
extern UINT4               gU4NoOfUpdationsToRBTree;
extern UINT4               gU4NoOfQMsgcomplete;
extern UINT4               gU4NoOfDefaultRecv;
extern UINT4               gU4NoOfComdsAfterValidations;
extern UINT4               gU4NoOfUpdateEvents;
extern UINT4               gU4NoOfQMsgSendFail;
extern tRBTree             gMSRRBTable;
extern tMemPoolId          gMsrIncrRbPoolId;
extern tMemPoolId          gMsrIncrOidPoolId;
extern tMemPoolId          gMsrQUpdDataPoolId;
extern tMemPoolId          gMsrQConDataPoolId;
extern tMemPoolId          gMsrOidListPoolId;
extern tMemPoolId          gMsrOidArrPoolId;
extern tMemPoolId          gMsrOidArrListPoolId;
extern tMemPoolId          gMsrDataPoolId;
extern INT4                gMsrIncrOidListPoolId;
extern INT4                gMsrIncrDataPoolId;
extern tSaveObject         gaSaveArray[];

extern unMsrUtilCallBackEntry gaMsrUtilCallBack[MSR_MAX_CALLBACK_EVENTS];

/* Prototypes */


INT4 MsrProcessMibSaveEvt PROTO ((VOID));

INT1 MsrSaveMIB PROTO ((VOID));

INT4
MsrCheckAliasName PROTO ((tSNMP_OID_TYPE * pOid,
                    tSNMP_OID_TYPE * pInstance, tSNMP_MULTI_DATA_TYPE * pData));

INT4
MsrValidateAliasName PROTO ((tSNMP_OID_TYPE * pOid,
                    tSNMP_OID_TYPE * pInstance, tSNMP_MULTI_DATA_TYPE * pData));

VOID MsrProcessMibRestoreEvent PROTO ((VOID));

VOID MsrInitRestoreFlag PROTO ((VOID));

VOID MsrInitSaveFlag PROTO ((VOID));

VOID MsrStoreSaveFlagNvRam PROTO ((VOID));

INT4  MsrMibRestore PROTO ((INT4 i4ConfFd));

VOID MsrProcessMibEraseEvent PROTO ((VOID));


VOID MsrSendRestorationCompleteToProtocols PROTO((VOID));

INT4 MsrValidateConfigSaveRestoreFileName PROTO ((VOID));

VOID MsrResumeUserConfiguration PROTO ((VOID));

INT4 MsrValidateVersionAndCheckSum PROTO ((UINT4 *pu4FileVersion,
                                          UINT1 *pu1AltConf));


/*prototype added for snmp update event */
VOID  MsrProcesssnmpEvt PROTO ((tMSRUpdatedata *));
INT4
MsrpParseSubIdNew (UINT1 **ppu1TempPtr, UINT4 *pu4Value);
tSNMP_OID_TYPE     *MsrpMakeObjIdFrmString (INT1 *pi1TextStr, 
                                            UINT1 *pu1TableName);

tMSRRbtree  *MsrAllocRBNode (UINT4 u4DataSize, UINT4 u4OidLength);
VOID InitMSRArryOIDStatus (VOID);
INT4  MsrPopulateFlashArrayFromRBTree (VOID);
INT4  MSRPopulateRBtreefromFlashArrray (VOID);
INT4  MSRPopulateRBtreefromISSConf (INT4 i4ConfFd);

INT4  MSRInsertVerOrFormatInfoToRBTree (VOID);
INT4  MSRCheckforOldFileVersion (UINT4 *pVersion,
                                 INT4  i4ConfFd);
INT4  MSRGetArrayIndexWithTableOID (UINT1 *pOid, UINT4 *pu4Index,
                                    UINT1 u4Type,
                                    UINT4 u4PrevIndex);
INT4  MsrUpdateQCountUnLock (VOID);
INT4  MsrUpdateQCountLock (VOID);
INT4  MsrReleaseUpdateQLock (VOID);
VOID  MSRFreeMultiData (tSNMP_MULTI_DATA_TYPE * pMultiData);
INT4  MsrUpdateQStatusUnLock (VOID);
INT4  MsrUpdateQStatusLock (VOID);

INT1  MSRUpdateMemAlloc (tSnmpIndex * (*ppMultiIndex),
                         UINT4 IndicesNo, tRetVal ** ppMultiData,
                         UINT1 **ppOid, UINT4 u4OidLen);
INT1  MSRNotifyConfigChange (tSnmpIndex * pMultiIndex,
                             BOOL1 IsRowStatusPresent,
                             tRetVal * pMultiData, UINT1 *pOid);

INT4  MSRSearchandUpdateRBTree (tMSRUpdatedata * pQData,
                                tSNMP_OID_TYPE * pOid,
                                UINT4 u4Index,
                                UINT4 u4RowstatusType,
                                INT4 i4IsDelete);
INT4  MsrSaveIncrData (tMSRUpdatedata * pQData,
                       tSNMP_OID_TYPE * pOid,
                       UINT4 u4ArrayIndex,
                       UINT4 u4RowstatusType, UINT4 u4UpdateType);
INT4  MsrSaveDatatoIncrFile (UINT1 *pData, UINT4 u4DataSize);
INT4  MsrSaveDatatoIncrBuffer (UINT1 *pData, UINT4 u4DataSize);
INT4  MSRUpdateRBtreeWithIncrFile (VOID);
INT4  MsrAllocateIncrBuffer (VOID);
INT4  MsrProcessSyncSaveEvent (UINT4);
INT4  MsrisSaveAllowed (UINT4 u4ArrayIndex);
INT4  MSRProcessUpdateRSCWandCG (tMSRUpdatedata * pQData,
                                 INT4 i4IsInternalGen);
INT4  MSRProcessUpdateRSActive (tMSRUpdatedata * pQData);
INT4  MSRProcessUpdateNonRSObj (tMSRUpdatedata * pQData);
INT4  MSRProcessUpdateDestroyRS (tMSRUpdatedata * pQData);
INT4  MSRProcessUpdateScalarObj (tMSRUpdatedata * pQData);
INT4  MsrStoreData (tSNMP_OID_TYPE *pOid,
                    tSNMP_MULTI_DATA_TYPE *pData,
                    UINT4 u4MSRRowStatusType,
                    UINT1 *pau1Oid, UINT4 *pu4Sum, UINT2 *pu2CkSum, 
                    UINT4 *pu4PrevIndex ,UINT4 u4Index, 
                    UINT4 *pu4Length, UINT1 u1IsDataToBeSaved);



INT4  MsrAllocateIncrPools PROTO ((VOID));
VOID  MsrUpdateRBDataSize PROTO ((tMSRRbtree *, UINT4));

INT4
MsrObtainAltResFilefd PROTO((INT4 *pi4FileFd));

INT4  MsrObtainFilePointer PROTO ((INT4 * pi4FileFd));
INT4  MsrSendDataToRemote PROTO ((tConnStatus *));
VOID  MsrObtainTablePrefixOid (UINT4, UINT1 *);
VOID  MsrFreeOidList PROTO ((tMsrOidList *, UINT2));
VOID  MsrDeleteRbNodes PROTO ((tMsrOidList *));
VOID  MsrUpdateConfiguration PROTO ((tMSRRbtree *, UINT1 *));
VOID  MsrDeleteMsrResources PROTO ((VOID));
INT4  MsrGetContextPortList PROTO ((UINT4, tPortList));
VOID  MsrProcessUpdateEvent PROTO ((VOID));
VOID  MsrHandleContextDeletion PROTO ((UINT4));
VOID  MsrHandleVlanDel PROTO ((UINT4));
VOID  MsrHandleStpPortDelete PROTO ((UINT4));
INT1 MsrNotifyUpdateEvent PROTO ((UINT2, UINT4));
INT4  AuditConstructLogMsg PROTO ((UINT1 *, tAuditInfo *));
VOID  MsrProcessSysLogEvent PROTO ((VOID));
VOID  MsrProcessAuditLogEvent PROTO ((VOID));
VOID MSRCopySnmpMultiDataValues PROTO ((tSNMP_MULTI_DATA_TYPE *,
                                        tSNMP_MULTI_DATA_TYPE *));

extern INT1 IssGetSaveFlagFromNvRam PROTO ((VOID));
extern VOID IssSetRestoreFlagToNvRam PROTO ((INT1));
extern VOID IssSetSaveFlagToNvRam PROTO ((INT1));
extern VOID IssSetRestoreFileNameToNvRam (INT1 *pi1RestoreFileName);

extern INT1* IssGetRestoreFileNameFromNvRam PROTO ((VOID));
extern tIPvXAddr *IssGetRestoreIpAddrFromNvRam PROTO ((VOID));
extern INT1 nmhSetFstftpcReadTrigger PROTO ((INT4));



extern VOID TftpConfigInit (VOID);
extern INT1 nmhSetFsTftpClientStatus (INT4);
extern INT1 nmhSetFstftpcTftpServerAddress (UINT4);
extern INT1 nmhSetFstftpcWriteTrigger (INT4);

extern VOID cli_print_wheel (UINT1 *);

extern VOID WebnmConvertOidToString (tSNMP_OID_TYPE * pOid, 
                                     UINT1 *pu1String);
extern VOID WebnmCopyOid (tSNMP_OID_TYPE * pDestOid, 
                          tSNMP_OID_TYPE * pSrcOid);
extern VOID WebnmGetInstanceOid (tSNMP_OID_TYPE * pCurOid,
                                 tSNMP_OID_TYPE * pNextOid, 
                                 tSNMP_OID_TYPE * pInstance);
extern INT4 WebnmConvertStringToOid (tSNMP_OID_TYPE * pOid, 
                                     UINT1 *pu1Data, 
                                     UINT1 u1HexFlag);
extern INT4 WebnmConvertStringToOctet (tSNMP_OCTET_STRING_TYPE * pOctetStrValue,
                                      UINT1 *pu1Data);
extern INT4 WebnmConvertColonStringToOctet   PROTO ((tSNMP_OCTET_STRING_TYPE *, 
                                                     UINT1 *));
extern INT1         nmhGetDot1qFutureVlanLearningMode (INT4 *);
extern INT1         nmhGetIssDefaultIpAddrCfgMode (INT4 *);
extern INT1         nmhSetDot1dBaseBridgeStatus (INT4);

extern INT1         nmhGetFsipv6AddrType (INT4, tSNMP_OCTET_STRING_TYPE *, INT4,
                                          INT4 *);
extern INT1         nmhGetFsipv6NdLanCacheState (INT4,
                                                 tSNMP_OCTET_STRING_TYPE *,
                                                 INT4 *);
extern VOID         Ip6AddrCopy (tIp6Addr * pDst, tIp6Addr * pSrc);
extern INT1         Ip6AddrType (tIp6Addr * pAddr);
#ifndef LNXIP6_WANTED
extern VOID         Ipv6UtilGetConfigMethod (UINT4 u4IfIndex,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsipv6AddrAddress,
                                             UINT1 u1PrefixLen,
                                             UINT1 u1AddressType,
                                             UINT1 * pu1CfgMethod);
#else
extern VOID         Ipv6UtilGetConfigMethod (UINT4 u4IfIndex,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsipv6AddrAddress,
                                             UINT1 * pu1CfgMethod);
#endif


extern UINT1        IpifGetIfOperStatus (UINT4 u4Port);
extern INT1         nmhGetIssHttpStatus (INT4 *pi4RetValIssHttpStatus);

extern INT1         nmhGetFsDot1qVlanStatus (INT4, UINT4, UINT4, INT4 *);
extern INT1         nmhGetDot1qVlanStatus (UINT4, UINT4, INT4 *);
extern INT1         VlanIsUnRegForwardPortsConfigured (UINT4, UINT4);


extern INT1 nmhGetIssHttpPort PROTO ((INT4 *));





#ifdef SNMP_3_WANTED
/* extern functions used by MSR */

INT4 MsrValidateVacmSecurityToGroupEntry (tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pInstance,
                                          tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateVacmAccessEntry (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateVacmViewTreeFamilyEntry (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateSnmpCommunityEntry (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateSnmpNotifyEntry (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidateSnmpNotifyFilterProfileEntry (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidateSnmpNotifyFilterEntry (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidateUsmEntry (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateSnmpTargetParamsEntry (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateSnmpTargetAddrEntry (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateSnmpProxyEntry (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData);
#endif

#ifdef PBBTE_WANTED
extern INT1 
nmhGetIeee8021PbbTeTeSidStorageType (UINT4, UINT4, INT4 *);
extern INT1 
nmhGetIeee8021PbbTeTeSidRowStatus (UINT4, UINT4, INT4 *);
#endif
#ifdef MPLS_WANTED
extern INT1        nmhGetFsMplsL2VpnAdminStatus ARG_LIST((INT4 *));

extern INT1
                   nmhGetPwRowStatus (UINT4 , INT4 *);
    
extern INT1
                   nmhGetMplsLdpEntityGenericLRRowStatus (tSNMP_OCTET_STRING_TYPE *, UINT4, UINT4, UINT4 , INT4 *);

extern INT1
                   nmhGetMplsLdpEntityRowStatus (tSNMP_OCTET_STRING_TYPE * ,UINT4, INT4 *);

extern INT1
                   nmhGetPwAdminStatus (UINT4 , INT4 *);

extern INT1
                   nmhGetIfMainRowStatus (INT4, INT4 *);

extern INT1
                   nmhGetMplsTunnelRowStatus (UINT4,UINT4,UINT4,UINT4, INT4 *);
extern INT1
                   nmhGetMplsTunnelAdminStatus (UINT4,UINT4,UINT4,UINT4, INT4 *);

extern INT1 nmhGetMplsTunnelCRLDPResRowStatus (UINT4, UINT4 *);
extern INT1 nmhGetMplsXCOwner (tSNMP_OCTET_STRING_TYPE * pMplsXCIndex,
                   tSNMP_OCTET_STRING_TYPE * pMplsXCInSegmentIndex,
                   tSNMP_OCTET_STRING_TYPE * pMplsXCOutSegmentIndex,
                   INT4 *pi4RetValMplsXCOwner);

extern INT1 nmhGetMplsLabelStackStorageType (tSNMP_OCTET_STRING_TYPE *, 
                                             UINT4, INT4 *);

extern INT1
nmhGetMplsInSegmentOwner (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                          INT4 *pi4RetValMplsInSegmentOwner);
extern INT1
nmhGetMplsOutSegmentOwner (tSNMP_OCTET_STRING_TYPE * pMplsOutSegmentIndex,
                           INT4 *pi4RetValMplsOutSegmentOwner);
extern INT1
nmhGetMplsTunnelOwner (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                       UINT4 u4MplsTunnelIngressLSRId,
                       UINT4 u4MplsTunnelEgressLSRId,
                       INT4 *pi4RetValMplsTunnelOwner);
extern INT1
nmhGetMplsFTNActionType (UINT4 u4MplsFTNIndex,
                         INT4 *pi4RetValMplsFTNActionType);
extern INT1
nmhGetMplsFTNActionPointer (UINT4 u4MplsFTNIndex,
                            tSNMP_OID_TYPE * pRetValMplsFTNActionPointer);
extern INT1
nmhGetMplsTunnelSignallingProto (UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 INT4 *pi4RetValMplsTunnelSignallingProto);
INT4
MsrValidateMplsFsMplsL2VpnMaxPwVcEntries PROTO ((tSNMP_OID_TYPE * pOid,
                                        tSNMP_OID_TYPE * pInstance,
                                        tSNMP_MULTI_DATA_TYPE * pData));

#ifdef MPLS_RSVPTE_WANTED
extern INT1 nmhGetFsMplsRsvpTeGenLblSpaceMaxLbl (INT4
                                     *pi4RetValFsMplsRsvpTeGenLblSpaceMaxLbl);
#endif
#endif


INT4
MsrDisableHttpStatusBeforeHttpPortChange (tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pInstance,
                                          tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidateQoSSystemControl (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateQoSSystemStatus  (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidateQoSClassMapTable  (tSNMP_OID_TYPE * pOid,
                                              tSNMP_OID_TYPE * pInstance,
                                              tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateQosSkipClassToPriority (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData);


#ifdef EOAM_WANTED
INT4 MsrValidateDot3OamLoopbackEntry (tSNMP_OID_TYPE * pOid,
                                              tSNMP_OID_TYPE * pInstance,
                                              tSNMP_MULTI_DATA_TYPE * pData);
#ifdef EOAM_FM_WANTED
extern INT1
nmhGetFsFmSystemControl ARG_LIST ((INT4 *));

INT4 MsrValidateFmLoopbackEntry (tSNMP_OID_TYPE * pOid,
                                         tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidateFmVarRetrievalEntry (tSNMP_OID_TYPE * pOid,
                                             tSNMP_OID_TYPE * pInstance,
                                             tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateFsFmModuleStatus PROTO ((tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData));

#endif/*EOAM_FM_WANTED*/
#endif/*EOAM_WANTED*/             

#ifdef LLDP_WANTED

INT4 MsrValidateFsLldpTraceInput (tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pInstance,
                                          tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidateFsLldpLocChassisId (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidateFsLldpConfiguredMgmtIpv4Address (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValLldpMedSkipLocRowStatus (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValLldpMedSaveLocRowStatus (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData);

#endif
#ifdef ISS_WANTED
INT4
MsrValidateIssLogging (tSNMP_OID_TYPE * pOid, tSNMP_OID_TYPE * pInstance,
                       tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateIssMirrorToPort (tSNMP_OID_TYPE * pOid, tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateEntPhysicalTable (tSNMP_OID_TYPE * pOid, tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData);
extern INT1 nmhGetEntPhysicalIsFRU (INT4 i4EntPhysicalIndex,
                                    INT4 *pi4RetValEntPhysicalIsFRU);
INT4
MsrValidateIssSwitchModeType (tSNMP_OID_TYPE * pOid, tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData);
#endif
#ifdef RM_WANTED
INT4
MsrValidateRmTrc (tSNMP_OID_TYPE * pOid, tSNMP_OID_TYPE * pInstance,
                  tSNMP_MULTI_DATA_TYPE * pData);

#endif

#ifdef VCM_WANTED
INT4
MsrValidateSkipDefaultVCStatus (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);

INT4   
MsrValidateVcmIfMapEntry (tSNMP_OID_TYPE * pOid, 
                          tSNMP_OID_TYPE * pInstance,   
                          tSNMP_MULTI_DATA_TYPE * pData);
#ifdef VRF_WANTED
INT4   
MsrValidateVrfIfMapEntry (tSNMP_OID_TYPE * pOid, 
                          tSNMP_OID_TYPE * pInstance,   
                          tSNMP_MULTI_DATA_TYPE * pData);

INT4   
MsrValidateVrfL2CxtIdIfMapEntry (tSNMP_OID_TYPE * pOid, 
                                 tSNMP_OID_TYPE * pInstance,   
                                 tSNMP_MULTI_DATA_TYPE * pData);

INT4   
MsrValidateVrfIfMapRowStatus (tSNMP_OID_TYPE * pOid, 
                              tSNMP_OID_TYPE * pInstance,   
                              tSNMP_MULTI_DATA_TYPE * pData);
#endif
INT4   
MsrValidateVcmConfigEntry (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,   
                           tSNMP_MULTI_DATA_TYPE * pData); 
extern INT1
nmhGetFsSispPortCtrlStatus (UINT4 u4FsSispPortIndex,
                            INT4 *pi4RetValFsSispPortCtrlStatus);

INT4
MsrValidateSispPortCtrlEntry (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData);
#endif

#ifdef PVRST_WANTED
INT4
MsrValidatePvrstSaveSystemControl (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData);
INT4 
MsrValidatePvrstSkipSystemControl (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidatePvrstObjects (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidatePvrstPortBasedObjects (tSNMP_OID_TYPE * pOid,
                                               tSNMP_OID_TYPE * pInstance,
                                               tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidatePvrstInstBrgBasedObjects (tSNMP_OID_TYPE * pOid,
                                                  tSNMP_OID_TYPE * pInstance,
                                                  tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidatePvrstInstPortBasedObjects (tSNMP_OID_TYPE * pOid,
                                                  tSNMP_OID_TYPE * pInstance,
                                                  tSNMP_MULTI_DATA_TYPE * pData);
extern INT1 nmhGetFsMIPvrstSystemControl 
    (UINT4 u4ContextId , INT4 *pi4RetValFsPvrstSystemControl);
#endif

INT4  MsrValidateIfMainEntry (tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData);

INT4  MsrValidateACIfMainEntry (tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pInstance,
                                          tSNMP_MULTI_DATA_TYPE * pData);


INT4  MsrValidateACSkipSbpInterfaces (tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pInstance,
                                          tSNMP_MULTI_DATA_TYPE * pData);

INT4  MsrValidateMtuIfMainEntry (tSNMP_OID_TYPE * pOid,
                                                  tSNMP_OID_TYPE * pInstance,
                                                  tSNMP_MULTI_DATA_TYPE * pData);

INT4  MsrValidateWssIfMainTypeStatus (tSNMP_OID_TYPE * pOid,
                                                  tSNMP_OID_TYPE * pInstance,
                                                  tSNMP_MULTI_DATA_TYPE * pData);

INT4   MsrValidateIfIvrEntry (tSNMP_OID_TYPE * pOid,
        tSNMP_OID_TYPE * pInstance,
        tSNMP_MULTI_DATA_TYPE * pData);


INT4
MsrValidateMtuForceAgain (tSNMP_OID_TYPE * pOid,
  tSNMP_OID_TYPE * pInstance,
  tSNMP_MULTI_DATA_TYPE * pData);

INT4  MsrValidateIfMainEntrySaveAdminStatus (tSNMP_OID_TYPE * pOid,
                                                      tSNMP_OID_TYPE * pInstance,
                                                     tSNMP_MULTI_DATA_TYPE * pData);

INT4  MsrValidateIfMainEntrySaveAdminDownStatus (tSNMP_OID_TYPE * pOid,
                                                  tSNMP_OID_TYPE * pInstance,
                                                 tSNMP_MULTI_DATA_TYPE * pData);



INT4  MsrValidateIfEntrySkipAdminStatus (tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData);

INT4  MsrValidateIfMainEntrySaveEncapDot1q (tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData);

INT4  MsrValidateSysSpecificPortID(tSNMP_OID_TYPE * pOid,
                                         tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData);


extern INT4 AstGetPseudoRootIdConfigStatus (UINT2, UINT2, UINT4);

#ifdef VLAN_WANTED
extern INT1 
nmhGetFsMIDot1qFutureVlanLearningMode (INT4,INT4*);

extern INT1
nmhGetFsDot1qStaticUnicastStatus (INT4 i4FsDot1qVlanContextId,
                  UINT4 u4FsDot1qFdbId,
                  tMacAddr FsDot1qStaticUnicastAddress,
                  INT4 i4FsDot1qStaticUnicastReceivePort,
                  INT4 *pi4RetValFsDot1qStaticUnicastStatus);

extern INT1
nmhGetFsDot1qStaticUnicastRowStatus (INT4 i4FsDot1qVlanContextId,
                     UINT4 u4FsDot1qFdbId,
                     tMacAddr FsDot1qStaticUnicastAddress,
                     INT4 i4FsDot1qStaticUnicastReceivePort,
                     INT4 *pi4RetValFsDot1qStaticUnicastRowStatus);

extern INT1
nmhGetFsDot1qStaticMulticastStatus (INT4 i4FsDot1qVlanContextId,
                    UINT4 u4FsDot1qVlanIndex,
                    tMacAddr FsDot1qStaticMulticastAddress,
                    INT4 i4FsDot1qStaticMulticastReceivePort,
                    INT4 *pi4RetValFsDot1qStaticMulticastStatus);

extern INT1
nmhGetFsDot1qStaticMulticastRowStatus (INT4 i4FsDot1qVlanContextId,
                     UINT4 u4Dot1qVlanIndex,
                     tMacAddr FsDot1qStaticUnicastAddress,
                     INT4 i4FsDot1qStaticUnicastReceivePort,
                     INT4 *pi4RetValFsDot1qStaticUnicastRowStatus);

extern INT1
nmhGetDot1dStaticStatus (tMacAddr Dot1dStaticAddress,
                         INT4 i4Dot1dStaticReceivePort,
                         INT4 *pi4RetValDot1dStaticStatus);

extern INT1
nmhGetFsDot1qForwardAllRowStatus (UINT4 u4ContextId, UINT4 u4Dot1qVlanIndex,
                                  INT4* pi4RowStatus);

extern INT1
nmhGetFsDot1qForwardUnregRowStatus (UINT4 u4ContextId, UINT4 u4Dot1qVlanIndex,
                                    INT4* pi4RowStatus);
        
INT4 
MsrValidateVlanFidMapEntry (tSNMP_OID_TYPE *pOid,
                                                 tSNMP_OID_TYPE *pInstance,
                                                 tSNMP_MULTI_DATA_TYPE *pData);
INT4
MsrValidateBrgModeObjects (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateDot1dTpAgingTime PROTO ((tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData));

INT4
MsrValidateVlanObjects (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateVlanShutdownStatus (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateDefaultContextGarpStatus (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateGarpStatus (tSNMP_OID_TYPE * pOid,
                       tSNMP_OID_TYPE * pInstance,
                       tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateSaveBaseBridgeMode (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateSaveCVlanCounterEntry (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData);
#ifdef GARP_WANTED
extern INT4 GarpGetContextInfoFromIfIndex PROTO ((UINT2 u2Port,
                           UINT4 *pu4ContextId, UINT2 *pu2LocalPort));

#endif
INT4
MsrValidateFsDot1qPortGvrpObjects PROTO ((tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData));

INT4
MsrValidateStaticUnicastPortEntry (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData);


INT4
MsrValidateStaticUnicastExtnEntry (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData);


INT4
MsrValidateStaticUnicastSetStatusCreate (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData);


INT4
MsrValidateVlanStaticSetStatusCreate (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateVlanStaticSetStatusActive (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateStaticUnicastSetStatusActive (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateStaticMulticastPortEntry (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData);


INT4
MsrValidateStaticMulticastSetStatusCreate (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateStaticMulticastSetStatusActive (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateForwardAllSetStatusCreate (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateForwardAllSetStatusActive (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateForwardUnregSetStatusCreate (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateForwardUnregSetStatusActive (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateForwardUnregPortEntry (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateVlanCounterEntry (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateUnicastMacControlEntry (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateFsMIPbPortInfoEntry PROTO ((tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData));

INT4
MsrValidateWildCardSetStatusCreate (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateWildCardSetStatusActive (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateDot1adMIPortEntry PROTO ((tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData));

INT4
MsrValidatedot1adMICVidRegistrationEntry PROTO ((tSNMP_OID_TYPE * pOid,
                                                 tSNMP_OID_TYPE * pInstance,
                                                 tSNMP_MULTI_DATA_TYPE * pData));

INT4
MsrValidatedot1adMICVidRegistrationEntrySaveRowStatus PROTO ((tSNMP_OID_TYPE * pOid,
                                                              tSNMP_OID_TYPE * pInstance,
                                                              tSNMP_MULTI_DATA_TYPE * pData));

INT4
MsrValidateFsMIPbContextInfoEntry PROTO ((tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData));

#endif

#ifdef MSTP_WANTED
extern INT1
nmhGetFsMIMstForceProtocolVersion ARG_LIST((INT4 ,INT4 *));

INT4 
MsrValidateMstpSaveSystemControl (tSNMP_OID_TYPE * pOid,
                                                  tSNMP_OID_TYPE * pInstance,
                                                  tSNMP_MULTI_DATA_TYPE * pData);
INT4 
MsrValidateMstpSkipSystemControl (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);

INT4 
MsrValidateMstpObjects (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData);
INT4 
MsrValidateMstpCistPortBasedObjects (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData);
INT4 
MsrValidateMstpMstiPortBasedObjects (tSNMP_OID_TYPE * pOid,
                                                  tSNMP_OID_TYPE * pInstance,
                                                  tSNMP_MULTI_DATA_TYPE * pData);
INT4 
MsrValidateMstpSavePortInfo (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData);
INT4 
MsrValidateMstpSaveExtPortInfo (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);
INT4 
MsrValidateMstpMapTableObjects (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateMstPortExtTable (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);

extern INT1
nmhGetFsMIMstSystemControl ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsMIMstMstiRegionName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhGetFsMIMstInstanceVlanMapped ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE
* ));

extern INT1
nmhGetFsMIMstInstanceVlanMapped2k ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsMIMstInstanceVlanMapped3k ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsMIMstInstanceVlanMapped4k ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1 
nmhSetFsMIMstSetVlanList ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhGetFsMIMstSetVlanList ARG_LIST((INT4 , INT4 , tSNMP_OCTET_STRING_TYPE * ));
#endif
#ifdef RSTP_WANTED

INT4 
MsrValidateRstpObjects (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData);


INT4 
MsrValidateRstpSkipSystemControl (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);

INT4 
MsrValidateRstpPortSkipUnCfgPseudoRootId (tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pInstance,
                                          tSNMP_MULTI_DATA_TYPE * pData);

INT4 
MsrValidateRstpSavePortInfo (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData);
INT4 
MsrValidateRstpSaveExtPortInfo (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);
INT4 
MsrValidateRstpSaveSystemControl (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);
INT4 
MsrValidateRstpPortBasedObjects (tSNMP_OID_TYPE * pOid,
                                              tSNMP_OID_TYPE * pInstance,
                                              tSNMP_MULTI_DATA_TYPE * pData);

extern INT1
nmhGetFsMIRstSystemControl ARG_LIST((INT4 ,INT4 *));
extern INT1
nmhGetFsMIRstPortRowStatus ARG_LIST((INT4 ,INT4 *));
#endif

#if defined (PB_WANTED) || (EVB_WANTED)
INT4
MsrValidateSetBrgPortType (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData);
#endif /*PB_WANTED or EVB_WANTED*/

#ifdef OPENFLOW_WANTED
INT4
MsrValidateOfcCfgEntry (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData);
#endif /* OPENFLOW_WANTED */

#ifdef DHCP6_CLNT_WANTED
INT4 MsrValidateInterfaceD6ClObjects (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData);

#endif
#ifdef DHCP6_RLY_WANTED
INT4 MsrValidateInterfaceD6RlObjects (tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                        tSNMP_MULTI_DATA_TYPE * pData);

#endif
#ifdef ELMI_WANTED
extern INT1 nmhGetFsElmiSystemControl (INT4 *pi4RetValFsElmiSystemControl);
INT4 MsrValidateElmiObjects (tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                        tSNMP_MULTI_DATA_TYPE * pData);

#endif
#ifdef RIP_WANTED
extern INT4 RipIfGetConfAuthKey ARG_LIST ((UINT4  , tSNMP_OCTET_STRING_TYPE * ));
INT4 MsrValidaterip2IfConfAuthKey (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData);
#endif

#ifdef OSPF_WANTED
extern INT1 nmhGetOspfAdminStat (INT4 *pi4RetValOspfAdminStat);
extern INT1 nmhGetOspfNbmaNbrPermanence (UINT4 u4OspfNbrIpAddr,
                                         INT4 i4OspfNbrAddressLessIndex,
                                         INT4 *pi4RetValOspfNbmaNbrPermanence);

extern INT1 nmhGetFsMIOspfRouterIdPermanence (UINT4 u4OspfCxtId,INT4 *i4Permanence);

extern INT4 
GetfsMIStdOspfIfAuthKey (UINT4 u4OspfCxtId, UINT4 u4OspfIfIpAddress,
                         UINT4 u4AddrlessIf,
                         tSNMP_OCTET_STRING_TYPE *pOctetAuthKeyValue);

extern INT1 nmhGetOspfASBdrRtrStatus (INT4 *pi4RetValOspfASBdrRtrStatus);
INT4 MsrValidateMIOspfRRDRouteEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidateFsMIStdOspfEntry (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateFsMIOspfEntry (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateFsMIOspfRouterIdPermanence (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateFsMIStdOspfv3AdminStat (tSNMP_OID_TYPE * pOid,
  tSNMP_OID_TYPE * pInstance,
  tSNMP_MULTI_DATA_TYPE * pData);


INT4 MsrValidateFsMIOspfRestartSupport (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateFsMIOspfAreaEntry (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidatefsMIStdOspfIfAuthKey (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateFsMIStdOspfNbrEntry (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateFsMIStdOspfAreaEntry(tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                    tSNMP_MULTI_DATA_TYPE * pData);
#endif

#ifdef RIP_WANTED
INT4 MsrValidateFsMIRip2Entry(tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                    tSNMP_MULTI_DATA_TYPE * pData);

#endif

#ifdef ISIS_WANTED
INT4 MsrValidateIsIsSetAdminStatusCreate (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateIsIsSetAdminStatusActive(tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
     tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateIsIsSetProtSuppRowStatus(tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
     tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateIsIsSetMAARowStatus(tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
     tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateIsIsSetCircAdminActive(tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
     tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateIsIsSetCircAdminActive1(tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
     tSNMP_MULTI_DATA_TYPE * pData);


INT4 MsrValidateIsIsSetCircAdminCreate(tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
     tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateIsIsSetRowStatusChange(tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                    tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateIsIsSetExtIPIfAddr(tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                    tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateIsIsSetSummAddrExistCreate(tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                    tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateIsIsSkipAutoIPRA (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateIsIsSetSummAddrExistActive (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                    tSNMP_MULTI_DATA_TYPE * pData);


INT4 MsrValidateIsIsSetIPRAExistCreate(tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                    tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateIsIsSetIPRAExistActive(tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                    tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrSkipProtoMaskSysTable (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                    tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrSaveProtoMaskSysTable (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                    tSNMP_MULTI_DATA_TYPE * pData);

#endif


#ifdef OSPF3_WANTED

INT4
MsrValidateFsMIStdOspfv3Entry (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateOspfv3AreaEntry (tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateFsMIOspfv3RRDRouteEntry (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData);


INT4
MsrValidateFsMIOspfv3Entry (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData);


INT4
MsrValidateFsMIOspfv3RouterIdPermanence (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData);

extern INT1 nmhGetOspfv3ASBdrRtrStatus (INT4 *);

#endif

INT4  MsrValidateIfEntry (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);
#ifdef MPLS_WANTED

#define MPLS_INSTANCE_TO_SEGMENT_COPY(pSegmentIndex, pInstance, u1OffSet) { \
        UINT1 u1Length = 0; \
        UINT1 u1Index = 0; \
        u1Length = (UINT1) pInstance->pu4_OidList[u1OffSet];\
        for (u1Index = 0,u1OffSet = (UINT1) (u1OffSet + 1);u1Index < u1Length; u1Index++, u1OffSet++)\
        {\
        (pSegmentIndex)->pu1_OctetList[u1Index] = (UINT1) pInstance->pu4_OidList[u1OffSet];\
        }\
        (pSegmentIndex)->i4_Length = u1Length;}\

INT4 MsrValidateMplsIfMainEntryDoActive(tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);


INT4 MsrValidateMplsIfMainEntryDoAdminUp(tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateMplsLdpEntityEntrySkipActive(tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateMplsIfStackEntry(tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateMplsLdpEntityEntryDoActive (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);

INT4 
MsrValidateMplsOutSegmentEntryOwner (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);

INT4 
MsrValidateMplsOutSegmentEntryDoActive (tSNMP_OID_TYPE * pOid,
                                        tSNMP_OID_TYPE * pInstance,
                                        tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateMplsInSegmentEntryOwner (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateMplsInSegmentEntryDoActive (tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateMplsXCEntryOwner (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData);

INT4 
MsrValidateLblStackTable (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);

INT4 
MsrValidateMplsTunnelCRLDPResEntrySkipActive(tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);

INT4 
MsrValidateMplsTunnelCRLDPResEntryDoActive(tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateMplsTunnelEntryDoActive (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateMplsTunnelEntryDoAdminUp (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateMplsTunnelEntrySkipActive (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateMplsPwEntrySkipAdmin (tSNMP_OID_TYPE * pOid,
                     tSNMP_OID_TYPE * pInstance,
                     tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrSkipAutoDiscoveredPw (tSNMP_OID_TYPE * pOid,
                         tSNMP_OID_TYPE * pInstance,
                         tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateMplsPwEntrySaveAdmin (tSNMP_OID_TYPE * pOid,
                     tSNMP_OID_TYPE * pInstance,
                     tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateSkipAdminActivePw (tSNMP_OID_TYPE * pOid,
                     tSNMP_OID_TYPE * pInstance,
                     tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateMplsFTNMapEntryDoCreateAndGo (tSNMP_OID_TYPE * pOid,
                     tSNMP_OID_TYPE * pInstance,
                     tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateMplsFTNEntrySkipActive (tSNMP_OID_TYPE * pOid,
                     tSNMP_OID_TYPE * pInstance,
                     tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateMplsFTNEntryDoActive (tSNMP_OID_TYPE * pOid,
                     tSNMP_OID_TYPE * pInstance,
                     tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateMplsTunnelResourceEntrySkipActive (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateMplsTunnelResourceEntryDoActive (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateMplsDiffServEntryTnlOwnerCheck (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateMplsRsvpTeGenPduDumpMsgType (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateMplsDiffServElspPreConfExpPhbMapIndex (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateMplsPwEntrySaveRowStatus (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateMplsTunnelResEntrySkipDefaultIndex (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateMplsRsvpTeRMDPolicyObject (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateMplsTunnelHopEntryIngress (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateMplsTunnelHopEntryDoActive (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateFsMplsBypassTunnelIfEntryDoCreateAndGo (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData);

#endif

INT4 MsrValidateIlanIfMainEntryDoActive (tSNMP_OID_TYPE * pOid,
                                         tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateIfStackEntry (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData);



extern INT1 nmhGetIpCidrRouteProto (UINT4 u4IpCidrRouteDest,
                                    UINT4 u4IpCidrRouteMask,
                                    INT4 i4IpCidrRouteTos,
                                    UINT4 u4IpCidrRouteNextHop,
                                    INT4 *pi4RetValIpCidrRouteProto);

extern INT1 nmhGetIpNetToMediaType (INT4 i4IpNetToMediaIfIndex,
                                    UINT4 u4IpNetToMediaNetAddress,
                                    INT4 *pi4RetValIpNetToMediaType);

extern INT1 nmhGetFsMIStdInetCidrRouteProto
           (INT4 i4FsMIStdIpContextId,
            INT4 i4FsMIStdInetCidrRouteDestType,
            tSNMP_OCTET_STRING_TYPE * pFsMIStdInetCidrRouteDest,
            UINT4 u4FsMIStdInetCidrRoutePfxLen,
            tSNMP_OID_TYPE * pFsMIStdInetCidrRoutePolicy,
            INT4 i4FsMIStdInetCidrRouteNextHopType,
            tSNMP_OCTET_STRING_TYPE * pFsMIStdInetCidrRouteNextHop,
            INT4 *pi4RetValFsMIStdInetCidrRouteProto);

extern INT1
nmhGetFsMIStdInetCidrRouteMetric5 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

#ifdef PNAC_WANTED
INT4 MsrValidatePNACObjects (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidatePnacMacBasedObjects (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData);
extern INT1
nmhGetFsPnacPaePortAuthMode (INT4 i4FsPnacPaePortNumber,
                             INT4 *pi4RetValFsPnacPaePortAuthMode);
#endif

extern VOID CfaSnmpifSendTrap (UINT2, UINT1, UINT1);
extern INT4 CfaGetIfUfdOperStatus PROTO ((UINT4, UINT1*));
#ifdef LA_WANTED
extern INT1 nmhGetDot3adAggPortActorAdminState (INT4 , 
        tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetDot3adAggPortPartnerAdminState (INT4 , tSNMP_OCTET_STRING_TYPE *);

INT4 MsrValidateLAPortActorAdminState (tSNMP_OID_TYPE * pOid,
                                               tSNMP_OID_TYPE * pInstance,
                                               tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateLAPortResetAdminState (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData);
INT4  MsrValidateLaSkipLacpPortMode(tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData);
INT4  MsrValidateLaSaveLacpPortMode (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData);
INT4  MsrValidateLASaveDefaultPort (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateSkipLAAdminMacAddressAndMacSelection PROTO ((tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData));

INT4
MsrValidateSkipDLAGProperties PROTO ((tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData));

INT4
MsrValidateSkipLADot3adAggActorAdminKey PROTO ((tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData));

INT4
MsrValidateSkipLAPortActorAdminState PROTO ((tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData));
INT4
MsrValidateLaActorSystemId PROTO ((tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData));

extern INT1
nmhGetFsLaActorSystemID ARG_LIST((tMacAddr * ));

extern INT1
nmhGetIssSwitchBaseMacAddress ARG_LIST((tMacAddr * ));

#endif

/* private functions of MSR */

VOID  MsrConvertOidToString (tSNMP_OID_TYPE * pOid, UINT1 *pu1String);

VOID  MsrGetInstanceOid (tSNMP_OID_TYPE * pCurOid, tSNMP_OID_TYPE * pNextOid,
                         tSNMP_OID_TYPE * pInstance);

VOID  MsrUtilConvertMultiDataToString (tSNMP_MULTI_DATA_TYPE *pData,
                                       UINT1 *pu1Data, UINT4 *pu4DataSize);
tSNMP_MULTI_DATA_TYPE * MsrAllocMultiData (VOID);

tSNMP_OCTET_STRING_TYPE * MsrAllocOctetString (VOID);

INT4 MsrUtilFillRbNode (tMSRRbtree *pRBNode, UINT4 u4Index,
                        tSNMP_OID_TYPE *pOid, tSNMP_MULTI_DATA_TYPE *pData,
                        UINT4 u4RSType, INT4 i4SaveVal);

VOID  MsrAppendOid (tSNMP_OID_TYPE * pDest, tSNMP_OID_TYPE *pSrc);

 

INT4 MsrValidateIfIpEntry (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData);

#ifdef RMON_WANTED
INT4 MsrValidateRmonTables (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData);
#endif
#ifndef SNMP_3_WANTED
INT4 MsrValidateSnmpTables (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData);
#endif


#ifdef CFA_WANTED
INT4
MsrValIfTypeProtoDenyTable (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData);
#endif

INT4
MsrValidateIfMainExtInterfaceType PROTO ((tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData));

#if defined (IP_WANTED) || defined (LNXIP4_WANTED)    
INT4 MsrValidateIpRouteEntry(tSNMP_OID_TYPE *pOid,
                             tSNMP_OID_TYPE *pInstance,
                             tSNMP_MULTI_DATA_TYPE *pData);


INT4 MsrValidateIpNetToMediaEntry (tSNMP_OID_TYPE *pOid,
                                           tSNMP_OID_TYPE *pInstance,
                                           tSNMP_MULTI_DATA_TYPE *pData);
#endif


#if defined (PIM_WANTED) || defined (PIMV6_WANTED)
INT4 MsrValidatePimComponentEntry (tSNMP_OID_TYPE * pOid,
                                           tSNMP_OID_TYPE * pInstance,
                                           tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidatePimStatus (tSNMP_OID_TYPE * pOid,
                      tSNMP_OID_TYPE * pInstance,
                      tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateFsPimCmnStaticRowStatusSkip (tSNMP_OID_TYPE * pOid,
                                        tSNMP_OID_TYPE * pInstance,
                                        tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateFsPimCmnStaticRowStatusSave (tSNMP_OID_TYPE * pOid,
                                        tSNMP_OID_TYPE * pInstance,
                                        tSNMP_MULTI_DATA_TYPE * pData);

extern INT1 nmhGetFsPimStdComponentScopeZoneName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

#endif
#if defined(IGS_WANTED) ||  defined (MLDS_WANTED) 
INT4
MsrValidateRtrPortEntry (tSNMP_OID_TYPE * pOid,
                         tSNMP_OID_TYPE * pInstance,
                         tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateFsSnoopSendLeaveOnTopoChangeObj PROTO
                            ((tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData));

#endif
#ifdef IGMP_WANTED
INT4 MsrValidateIsStaticEntry (tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateIsStaticSrcEntry (tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateIgmpSSMMapRowStatus (tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData);
#endif

#ifdef DIFFSRV_WANTED
#ifndef MRVLLS
INT4 MsrValidateDiffServMFClfrEntry (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateDiffServInProfileActionEntry (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidateDiffServOutProfileActionEntry (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidateDiffServNoMatchActionEntry (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData);
#endif
#endif

#ifdef VRRP_WANTED
extern INT4 
GetVrrpOperAuthKey ARG_LIST ((UINT4 , INT4 , tSNMP_OCTET_STRING_TYPE *));

extern INT4
nmhGetFsVrrpVersionSupported ARG_LIST ((INT4 *pi4Version));

INT4 MsrValidateVrrpSkipAdminStateAndPriority (tSNMP_OID_TYPE * pOid,
                                            tSNMP_OID_TYPE * pInstance,
                                            tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateVrrpSaveAdminState (tSNMP_OID_TYPE * pOid,
                                            tSNMP_OID_TYPE * pInstance,
                                            tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateVrrpAssoIpAddrSetCreateAndGo (tSNMP_OID_TYPE * pOid,
                                              tSNMP_OID_TYPE * pInstance,
                                              tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateVrrpv3SkipRowStatus (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateVrrpv3SaveRowStatus (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateVrrpv3AssoIpAddrSetCreateAndGo (tSNMP_OID_TYPE * pOid,
                                                tSNMP_OID_TYPE * pInstance,
                                                tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateFsVrrpTable (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateFsVrrpv3Table (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateVrrpv3NotificationCntl (tSNMP_OID_TYPE * pOid,
                                        tSNMP_OID_TYPE * pInstance,
                                        tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateVrrpv3Status (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateVrrpSaveOnlyTrackRowStatusCW (tSNMP_OID_TYPE * pOid,
                                              tSNMP_OID_TYPE * pInstance,
                                              tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateVrrpTrackIfSetCreateAndGo (tSNMP_OID_TYPE * pOid,
                                           tSNMP_OID_TYPE * pInstance,
                                           tSNMP_MULTI_DATA_TYPE * pData);


INT4 MsrValidateVrrpSaveTrack (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateVrrpv3SaveOnlyTrackRowStatusCW (tSNMP_OID_TYPE * pOid,
                                                tSNMP_OID_TYPE * pInstance,
                                                tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateVrrpv3TrackIfSetCreateAndGo (tSNMP_OID_TYPE * pOid,
                                             tSNMP_OID_TYPE * pInstance,
                                             tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateVrrpv3SaveTrack (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData);
#endif

#ifdef RRD_WANTED
extern INT1 nmhGetFsMIRrdRouterId ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsMIRrdRouterASNumber ARG_LIST((INT4 ,INT4 *));
INT4
MsrValidateRtmContextEntry (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateRtmControlEntry (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData);
#endif

#ifdef DHCP_RLY_WANTED
INT4
MsrValidatedhcpConfigDhcpCircuitOption (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidateFsMIDhcpRelaySrvAddressTable (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData);
#endif

#ifdef ECFM_WANTED
INT4
MsrValidateEcfmDefaultMdEntry (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateEcfmMepEntry (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateEcfmMaEntry (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateMeTable (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData);


INT4
MsrValidateEcfmContextEntry (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateSaveMipDynEvalStatus (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateSaveY1731MepEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateSaveEcfmMepEntry (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateEcfmPortEntry (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateY1731PortEntry (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateY1731MegEntry (tSNMP_OID_TYPE * pOid,
                         tSNMP_OID_TYPE * pInstance,
                         tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateY1731MepEntry (tSNMP_OID_TYPE * pOid,
                         tSNMP_OID_TYPE * pInstance,
                         tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateEcfmExtDefaultMdEntry (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateEcfmMaIsidMplstpEntry (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateEcfmMaVlanEntry (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData);

extern INT1  nmhGetFsMIEcfmMaPrimaryVlanId (UINT4,UINT4,UINT4,INT4 *);
extern INT1  nmhGetNextIndexFsMIEcfmExtMaTable (UINT4, UINT4 *, UINT4, 
                                                UINT4 *, UINT4, UINT4 * , 
                                                INT4, INT4 *, UINT4, UINT4 *);


#define IS_MEP_ISID_AWARE(u4Isid)\
        (u4Isid > 4095)
INT4
MsrValidateEcfmMipIsidEntry (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateEcfmMipVlanEntry (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateEcfmIsidMplstpMepEntry (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData);

#endif
#ifdef ELPS_WANTED
INT4
MsrValidateElpsContextEntry PROTO ((tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData));

INT4 MsrValidateElpsSkipPgCfmRowStatus (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidateElpsSavePgCfmRowStatus (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidateElpsSkipPgRowStatus (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidateElpsSavePgRowStatus (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidateElpsSkipPgCmdFreeze (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValElpsSetPgServListRowStatus (tSNMP_OID_TYPE * pOid, 
                                       tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData);
#endif
#ifdef ERPS_WANTED
INT4
MsrValErpsSkipRingRowStatus (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValErpsRingConfigClear (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValErpsSaveRingRowStatus (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValErpsRingConfigDistPortSave (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData);

#endif

#ifdef BFD_WANTED
INT4
MsrValidateBfdSkipSessRowStatus (tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateBfdSaveSessRowStatus (tSNMP_OID_TYPE * pOid,
                                        tSNMP_OID_TYPE * pInstance,
                                        tSNMP_MULTI_DATA_TYPE * pData);
#endif
#ifdef ROUTEMAP_WANTED
INT4
MsrValidateRMapSkipRowStatus (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateRMapSaveRowStatus (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData);
#endif

#ifdef IP6_WANTED
INT4 MsrValidateSavePmtuAdminStatus (tSNMP_OID_TYPE * pOid,
                                               tSNMP_OID_TYPE * pInstance,
                                               tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidateSkipPmtuAdminStatus (tSNMP_OID_TYPE * pOid,
                                               tSNMP_OID_TYPE * pInstance,
                                               tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidateIpv6RouteEntry (tSNMP_OID_TYPE * pOid,
                                               tSNMP_OID_TYPE * pInstance,
                                               tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidateIpv6AddrEntry (tSNMP_OID_TYPE * pOid,
                                               tSNMP_OID_TYPE * pInstance,
                                               tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidateFsMIIpv6IfScopeZoneEntry(tSNMP_OID_TYPE * pOid,
                                               tSNMP_OID_TYPE * pInstance,
                                               tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidateIpv6NdLanCacheEntry (tSNMP_OID_TYPE * pOid,
                                               tSNMP_OID_TYPE * pInstance,
                                               tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidateIpv6AddrSelPolicyEntry(tSNMP_OID_TYPE * pOid,
                                               tSNMP_OID_TYPE * pInstance,
                                               tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidateRtm6ControlEntry (tSNMP_OID_TYPE * pOid,
                                               tSNMP_OID_TYPE * pInstance,
                                               tSNMP_MULTI_DATA_TYPE * pData);
#endif

#if defined(IGS_WANTED) ||  defined (MLDS_WANTED)
INT4
MsrValidateSkipReadonlyEntries (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateSkipVlanFilterRowStatus (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateVlanExtensionTableActive (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData);
#endif

#ifdef TAC_WANTED
INT4
MsrValidateTacPrfEntrySkipRowStatus (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateTacPrfEntrySaveRowStatus (tSNMP_OID_TYPE * pOid,
                                     tSNMP_OID_TYPE * pInstance,
                                     tSNMP_MULTI_DATA_TYPE * pData);
#endif

INT4
MsrValidateTeSidSetStatusCreate (tSNMP_OID_TYPE * pOid,
     tSNMP_OID_TYPE * pInstance,
     tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateTeSidSetStatusActive (tSNMP_OID_TYPE * pOid,
     tSNMP_OID_TYPE * pInstance,
     tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateTeSidStatus (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData);


#ifdef BGP_WANTED

INT4
MsrValidateVerifyRouterId (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateVerifyAdminStatus (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateVerifyLocalAsNo (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateVerifyPeerLocalAs (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateVerifyPeerAdminStatus (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateFsBgpPassword (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateFsBgpPwdSet (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData);
INT4 
MsrValidateFsTCPAOMKT(tSNMP_OID_TYPE * pOid,
 tSNMP_OID_TYPE * pInstance,
 tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateFsBgpAggrAdmin (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateFsBgpAggrAdminSet (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValBgp4AggregateSaveAdminStateAsDown (tSNMP_OID_TYPE * pOid,
                                         tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValBgp4AggregateSaveAdminState (tSNMP_OID_TYPE * pOid,
  tSNMP_OID_TYPE * pInstance,
  tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidateSavePeerRemoteAs (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);

INT4 MsrValidatePeerSRCAddr (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData);
INT4 MsrValidateSkipPeerRemoteAs (tSNMP_OID_TYPE * pOid,
                                  tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);
extern INT4  Bgp4GetPeerLocalAsCfgStatus (UINT4, INT4, tSNMP_OCTET_STRING_TYPE *, UINT1 *);
extern INT4  Bgp4GetLocalAsCfgStatus (UINT4, UINT1 *);
extern INT4  Bgp4GetBgpIdConfigType (UINT4 u4Context, UINT1 *pu1Type);
extern INT4  Bgp4GetNeighborPasswd (UINT4 u4Context,INT4 i4Fsbgp4TCPMD5AuthPeerType,
                       tSNMP_OCTET_STRING_TYPE *pFsbgp4TCPMD5AuthPeerAddr,
                       tSNMP_OCTET_STRING_TYPE *pRetValFsbgp4TCPMD5AuthPassword);
extern INT4  Bgp4GetTcpAoMKTPasswd(UINT4 u4Context,INT4 i4Fsbgp4TCPMKTId,
                       tSNMP_OCTET_STRING_TYPE *pRetValFsbgp4TCPMD5AuthPassword);
extern INT4 Bgp4GetPeerLocalAddrCfgStatus (UINT4 u4Context, tAddrPrefix PeerAddress);
#endif /* BGP_WANTED */

extern INT1 nmhGetDhcpRelayRAICircuitIDSubOptionControl (INT4 *);

extern INT1  nmhGetFsLldpLocChassisIdSubtype (INT4 *);

extern INT1  nmhGetFsLldpLocPortIdSubtype (INT4, INT4 *);

extern INT1  nmhGetIfMainType (INT4, INT4 *);

extern INT1  nmhGetIfMainSubType (INT4, INT4 *);

extern INT1  nmhGetIfMainBrgPortType (INT4 ,INT4 *);

extern INT1 nmhGetIfMainSubSessionInitMethod (INT4 ,INT4 *);

extern INT1  nmhGetIfMainMtu (INT4, INT4 *);

extern INT1  nmhGetIfIvrBridgedIface (INT4, INT4 *);

extern INT1  nmhGetIfMainEncapType (INT4, INT4 *);

INT4 MsrValidateSkipRouteRowStatus(tSNMP_OID_TYPE *pOid,
                                tSNMP_OID_TYPE *pInstance,
                                tSNMP_MULTI_DATA_TYPE *pData);
INT4 MsrValidateSavePrivateRoute (tSNMP_OID_TYPE *pOid,
                                tSNMP_OID_TYPE *pInstance,
                                tSNMP_MULTI_DATA_TYPE *pData);
INT4 MsrValidateSaveRouteRowStatus (tSNMP_OID_TYPE *pOid,
                                tSNMP_OID_TYPE *pInstance,
                                tSNMP_MULTI_DATA_TYPE *pData);
extern INT1
nmhGetIpNetToPhysicalType (INT4 i4IpNetToPhysicalIfIndex,
                           INT4 i4IpNetToPhysicalNetAddressType,
                           tSNMP_OCTET_STRING_TYPE *pIpNetToPhysicalNetAddress,
                           INT4 *pi4RetValIpNetToPhysicalType);
INT4
MsrValidateIpvxNetToPhyEntry (tSNMP_OID_TYPE *pOid,
                              tSNMP_OID_TYPE *pInstance,
                              tSNMP_MULTI_DATA_TYPE *pData);
INT4
MsrValidateSkipIpAddressIfIndex (tSNMP_OID_TYPE *pOid,
                                 tSNMP_OID_TYPE *pInstance,
                                 tSNMP_MULTI_DATA_TYPE *pData);
#ifdef PBB_WANTED
INT4
MsrValidatePbbInstanceEntry PROTO((tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData));
#endif
INT4
MsrValidateLldpXDot3PortConfigEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateLldpXDot1ConfigProtoVlanEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateLldpXDot1ConfigPortVlanEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateLldpPortConfigEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateFsLldpLocPortEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateLldpV2Xdot3PortConfigEntry (tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateLldpV2Xdot1ConfigProtoVlanEntry (tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pInstance,
                                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateLldpV2Xdot1ConfigPortVlanEntry (tSNMP_OID_TYPE * pOid,
                                         tSNMP_OID_TYPE * pInstance,
                                         tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateLldpV2PortConfigEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateLldpV2Xdot1ConfigVlanNameEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateFslldpv2ConfigPortMapEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateFslldpV2DestAddressTableEntry (tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pInstance,
                                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateDot3PauseEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateIfMauAutoNegEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateFsDot1qVlanStaticPortConfigEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateFsMIDot1qFutureStVlanExtEntry (tSNMP_OID_TYPE * pOid,
                                          tSNMP_OID_TYPE * pInstance,
                                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateFsDot1qPortVlanEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateFsMIDot1qFutureVlanPortEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateFsDot1dPortPriorityEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateFsSyslogConfigEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateFsSyslogLogSrvAddr PROTO ((tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData));

INT4
MsrValidateFsSyslogFwdAddr (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateFsDot1dUserPriorityRegenEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateFsDot1dTrafficClassEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateFsDot1qPortConfigEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateFsDot1dPortGarpEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateFsDot1dPortGmrpEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateIeee8021BridgePortMmrpEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateDot1xPaePortEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateDot1xAuthConfigEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateFsPnacPaePortEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateIssConfigCtrlEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateIssMirrorCtrlEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                         tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateSkipIssMirrorCtrlExtnEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                         tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateSaveIssMirrorCtrlExtnEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                         tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateIssRateCtrlEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateIssExtRateCtrlEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateIssPortCtrlEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateIssAclL2FilterEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateIssAclL3FilterEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateIssAclUserDefinedFilterEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateIssAclL2SaveRowStatus (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateIssAclL3SaveRowStatus (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateIssAclUserDefinedSaveRowStatus (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateIssMgmtInterfaceRouting (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateFsDiffServAlgorithmEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateFsDiffServWeightBwEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateLldpdot1ConfigEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateDot3OamEntry (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData);
extern VOID RipGetIfAuthKey (UINT4 u4RipIp, tSNMP_OCTET_STRING_TYPE * pu1AuthKey);
extern INT1 nmhSetIfMainAdminStatus PROTO ((INT4 i4IfMainIndex,
                                            INT4 i4SetValIfMainAdminStatus));
extern INT1 nmhSetIssConfigSaveOption (INT4);
extern INT1 nmhSetIssConfigRestoreOption (INT4);
extern INT1 nmhGetIssConfigSaveOption (INT4 *);
extern INT1 CliReadUsers (VOID);
extern INT1 CliReadPrivileges (VOID);
extern UINT4 IssGetMgmtPortFromNvRam (VOID);
extern INT1 nmhGetIssPortCtrlMode ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetIssPortCtrlSpeed ARG_LIST((INT4 ,INT4 *));

#ifdef DVMRP_WANTED
INT4
MsrValidateDvmrpInterfaceEntryDoCreateAndGo (tSNMP_OID_TYPE * pOid,
                     tSNMP_OID_TYPE * pInstance,
                     tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateDvmrpLogEnabled (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateDvmrpLogMask (tSNMP_OID_TYPE * pOid,
                         tSNMP_OID_TYPE * pInstance,
                         tSNMP_MULTI_DATA_TYPE * pData);

extern INT1 nmhGetDvmrpStatus (INT4 *);
#endif

#ifdef IPVX_WANTED
INT4 MsrValidateIpGlobalEntry (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData);
#endif
#ifdef FIREWALL_WANTED
INT4
MsrValidateFwlStatIfEntry (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValSetFwlFilterAddrInfo (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValSkipFwlFilterAddrInfo (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
#endif


INT4
MsrValidateIssPIStorageType PROTO ((tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData));

INT4
MsrValidateIssAclTriggerCommit PROTO ((tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData));
extern INT1
nmhGetIssAclProvisionMode PROTO ((INT4 *pi4RetValIssAclProvisionMode));
#ifdef QOSX_WANTED
INT4
MsrValidateQosxSkipPolicyRowStatus (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateQoSPriorityMapTable (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateQosxSavePolicyRowStatus (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateQosxSkipQSchedRowStatus (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateQosxSkipQRowStatus (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateQosxSaveQRowStatus (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateQosxSkipClassMapRowStatus (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateQosxSkipMeterTableRowStatus (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateQosxSaveClassMapRowStatus (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateQosxSkipPriorityMapStatus (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateQosxSavePriorityMapStatus (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                 tSNMP_MULTI_DATA_TYPE * pData);

extern INT4
MsrValidateQoSDefUserPriorityEntry ARG_LIST ((tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData));
INT4 
MsrValidateQosHwCpuRateLimitEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateQoSRandomDetectCfgEntry (tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateQoSRandomDetectCfgSaveRowStatus (tSNMP_OID_TYPE * pOid,
                                            tSNMP_OID_TYPE * pInstance,
                                            tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateQosPolicerStatsStatus (tSNMP_OID_TYPE * pOid,
                                            tSNMP_OID_TYPE * pInstance,
                                            tSNMP_MULTI_DATA_TYPE * pData);

#endif
#ifdef VPN_WANTED
INT4
MsrValidateVpnPolicyEntry (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData);
extern INT1    nmhGetFsVpnPolicyType (tSNMP_OCTET_STRING_TYPE *
                                      pFsVpnPolicyName,
                                      INT4 *pi4RetValFsVpnPolicyType);

#endif

#ifdef PPP_WANTED
INT4
MsrValidatePppIpObjects (tSNMP_OID_TYPE * pOid,
                         tSNMP_OID_TYPE * pInstance,
                         tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrSavePppRowStatus (tSNMP_OID_TYPE * pOid,
                        tSNMP_OID_TYPE * pInstance,
                        tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidatePPPDependency (tSNMP_OID_TYPE * pOid,
                         tSNMP_OID_TYPE * pInstance,
                         tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateIfPppEntry (tSNMP_OID_TYPE * pOid,
                       tSNMP_OID_TYPE * pInstance,
                       tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateIfIpPppEntry (tSNMP_OID_TYPE * pOid,
                         tSNMP_OID_TYPE * pInstance,
                         tSNMP_MULTI_DATA_TYPE * pData);
#endif

#ifdef TLM_WANTED
INT4
MsrValidateTeLinkEntryDoActive (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateSkipTeLinkEntry (tSNMP_OID_TYPE * pOid,
                            tSNMP_OID_TYPE * pInstance,
                            tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateSkipTeLinkBandwidthEntry(tSNMP_OID_TYPE * pOid,
                                    tSNMP_OID_TYPE * pInstance,
                                    tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateSkipTeLinkSrlgEntry (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateComponentLinkEntryDoActive (tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateSkipComponentLinkEntry (tSNMP_OID_TYPE * pOid,
                                   tSNMP_OID_TYPE * pInstance,
                                   tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateSkipTeLinkDescriptorEntry (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateSkipCompLinkDescriptorEntry (tSNMP_OID_TYPE * pOid,
                                        tSNMP_OID_TYPE * pInstance,
                                        tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateSkipComponentLinkBandwidthEntry (tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData);
#endif

INT4
MsrSysRegHL (tMsrRegParams *pMsrRegParams);

/* For Audit Log configuration related changes */
extern UINT4        gu4AuditLogSize;
extern UINT4        gu4AuditLogSizeThreshold;
extern UINT1        gau1IssAuditLogFileName[ISS_AUDIT_FILE_NAME_LENGTH];
INT4 MsrUtilChkAuditFileSize PROTO ((UINT4 , UINT1 *, INT2 ));

INT4 MsrValidRemFileCheckSum PROTO ((VOID));
INT4 MSRRestoreFromRestoreArray PROTO ((VOID));


VOID MsrRemoteRestore PROTO ((VOID));
INT4 MsrRemRestoreInit PROTO ((VOID));
INT4 MsrStartConfigurationRestore PROTO ((VOID));
INT4 MsrIndMsrCompleteStatusToProto (INT4);

extern INT1
nmhGetUsmUserStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetSnmpCommunityStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));


extern INT1
nmhGetSnmpNotifyStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1
nmhGetSnmpNotifyFilterStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE *,INT4 *));
extern INT1
nmhGetSnmpNotifyFilterProfileStorType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1
nmhGetSnmpTargetParamsStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1
nmhGetSnmpTargetAddrStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetVacmViewTreeFamilyStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OID_TYPE *,INT4 *));

extern INT1
nmhGetVacmAccessStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetSnmpProxyStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1 nmhGetTeLinkStorageType ARG_LIST((INT4, INT4 *));
extern INT1 nmhGetComponentLinkStorageType ARG_LIST((INT4,INT4 *));
extern INT1 nmhGetComponentLinkBandwidthStorageType ARG_LIST((INT4,UINT4, INT4 *));
extern INT1 nmhGetTeLinkDescrStorageType ARG_LIST((INT4,UINT4,INT4 *));
extern INT1 nmhGetTeLinkSrlgStorageType ARG_LIST((INT4,UINT4,INT4 *));
extern INT1 nmhGetTeLinkBandwidthStorageType ARG_LIST((INT4,UINT4,INT4 *));
extern INT1 nmhGetComponentLinkDescrStorageType ARG_LIST((INT4,UINT4,INT4 *));
extern INT1 nmhGetFsMplsL2VpnPwMode ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetDot3PauseAdminMode ARG_LIST((INT4, INT4 *)); 

INT4
MsrValidateVplsConfigTableSkip (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateVplsConfigTableSave (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateMplsPwEntrySkipVplsInstance (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);
#ifdef MRP_WANTED
INT4 
MsrValidateMRPInstanceEntry (tSNMP_OID_TYPE * pOid,
                         tSNMP_OID_TYPE * pInstance,
                         tSNMP_MULTI_DATA_TYPE * pData);
extern INT1 nmhGetFsMrpInstanceSystemControl (UINT4 u4Ieee8021BridgeBaseComponentId,
                                                INT4 *pi4RetValFsMrpInstanceSystemControl);
#endif
#ifdef SYNCE_WANTED
INT4
MsrValidateSynceQLValue (tSNMP_OID_TYPE * pOid,
                               tSNMP_OID_TYPE * pInstance,
                               tSNMP_MULTI_DATA_TYPE * pData);
#endif

INT4
MsrValidateSecondaryIp (tSNMP_OID_TYPE *pOid,
                        tSNMP_OID_TYPE *pInstance,
                        tSNMP_MULTI_DATA_TYPE *pData);

#ifdef WLC_WANTED
INT4
MsrValidateApGroupBindingEntry (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateApGroupIfMainEntry (tSNMP_OID_TYPE * pOid,
                                       tSNMP_OID_TYPE * pInstance,
                                       tSNMP_MULTI_DATA_TYPE * pData);

extern INT1 
nmhGetFsApGroupEnabledStatus(INT4 *pi4RetValFsApGroupEnabledStatus);

#ifdef RFMGMT_WANTED
INT4 MsrValidateFsRrmAPConfigEntry (tSNMP_OID_TYPE * pOid,
         tSNMP_OID_TYPE * pInstance,
         tSNMP_MULTI_DATA_TYPE * pData);
extern INT4 nmhGetFsDot11RadioType (INT4, INT4 *);
extern INT4 nmhGetFsRrmDcaMode (INT4, INT4 *);
extern INT4 nmhGetFsRrmDcaSelectionMode (INT4, INT4 *);
extern INT4 nmhGetFsRrmAPChannelMode (INT4, INT4 *);

INT4 MsrValidateFsRrmTpcConfigEntry (tSNMP_OID_TYPE * pOid,
         tSNMP_OID_TYPE * pInstance,
         tSNMP_MULTI_DATA_TYPE * pData);
extern INT4 nmhGetFsRrmTpcMode (INT4, INT4 *);
extern INT4 nmhGetFsRrmTpcSelectionMode (INT4, INT4 *);
extern INT4 nmhGetFsRrmAPTpcMode (INT4, INT4 *);
#endif
#ifdef WSSUSER_WANTED
INT4
MsrValWssUserSkipGroupDefaultGroup (tSNMP_OID_TYPE * pOid,
                             tSNMP_OID_TYPE * pInstance,
                             tSNMP_MULTI_DATA_TYPE * pData);
#endif
#endif

#ifdef Y1564_WANTED
INT4
MsrValY1564SlaEntry (tSNMP_OID_TYPE * pOid,
                     tSNMP_OID_TYPE * pInstance,
                     tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValY1564PerfEntry (tSNMP_OID_TYPE * pOid,
                      tSNMP_OID_TYPE * pInstance,
                      tSNMP_MULTI_DATA_TYPE * pData);
INT4 
MsrValY1564TrafProfEntry (tSNMP_OID_TYPE * pOid,
                          tSNMP_OID_TYPE * pInstance,
                          tSNMP_MULTI_DATA_TYPE * pData);

#endif

#ifdef VXLAN_WANTED
INT4
MsrValidateSkipVxlanNveEntry (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateSkipVxlanStatistics (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);

#ifdef EVPN_VXLAN_WANTED 
INT4
MsrValidateSkipVxlanEvpnStatistics (tSNMP_OID_TYPE * pOid,
                                tSNMP_OID_TYPE * pInstance,
                                tSNMP_MULTI_DATA_TYPE * pData);
#endif
extern INT1
nmhGetFsVxlanNveStorageType (INT4 i4FsVxlanNveIfIndex,
                             UINT4 u4FsVxlanNveVniNumber,
                             tMacAddr u1FsVxlanNveDestVmMac,
                             INT4 *pi4RetValFsVxlanNveStorageType);

#endif
INT4
MsrValidateL2SkipRowStatus (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateL2SaveRowStatus (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateL3SkipRowStatus (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateL3SaveRowStatus (tSNMP_OID_TYPE * pOid,
                           tSNMP_OID_TYPE * pInstance,
                           tSNMP_MULTI_DATA_TYPE * pData);

#ifdef EVB_WANTED
extern INT4 nmhGetIeee8021BridgeEvbUAPConfigStorageType (UINT4, INT4 *);

extern INT4 nmhGetFsMIEvbUAPSchCdcpMode( UINT4 , INT4 *);

extern INT4 nmhGetIeee8021BridgeEvbUAPSchCdcpAdminEnable (UINT4 ,INT4 *);

INT4
MsrValidateEvbSystemEntry    (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateEvbUapConfigEntry (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData);

INT4
MsrValidateEvbSbpSaveRowStatus (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateStdEvbSbpConfigEntry (tSNMP_OID_TYPE * pOid,
                                 tSNMP_OID_TYPE * pInstance,
                                  tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrValidateFsEvbSbpConfigEntry (tSNMP_OID_TYPE * pOid,
                              tSNMP_OID_TYPE * pInstance,
                              tSNMP_MULTI_DATA_TYPE * pData);
#endif

#ifdef FSB_WANTED
INT4
MsrHandleFsbSkipCxtModuleStatus (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrHandleFsbSaveIntfPortRole (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData);
INT4
MsrHandleFsbSaveCxtModuleStatus (tSNMP_OID_TYPE * pOid,
                                      tSNMP_OID_TYPE * pInstance,
                                      tSNMP_MULTI_DATA_TYPE * pData);
#endif

#ifdef RFC2544_WANTED
INT4
MsrValidate2544SkipSlaTestStatus (tSNMP_OID_TYPE * pOid,
                                        tSNMP_OID_TYPE * pInstance,
                                        tSNMP_MULTI_DATA_TYPE * pData);

#endif
#ifdef RFC6374_WANTED
INT4 MsrValidate6374ParamsConfigTable(tSNMP_OID_TYPE * pOid, tSNMP_OID_TYPE * pInstance, tSNMP_MULTI_DATA_TYPE * pData);
#endif
#endif /* _MSRSYS_H */
