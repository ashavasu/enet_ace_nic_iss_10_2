/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: msrsz.h,v 1.3 2012/04/06 12:09:31 siva Exp $
 * *
 * * Description: MSR-SIZING.
 * *********************************************************************/
enum {
    MAX_MSR_CONFIG_DATA_BLOCKS_SIZING_ID,
    MAX_MSR_DATA_BLOCKS_SIZING_ID,
    MAX_MSR_INCR_OID_BLOCKS_SIZING_ID,
    MAX_MSR_INCR_RB_NODES_SIZING_ID,
    MAX_MSR_MIBENTRY_BLOCKS_SIZING_ID,
    MAX_MSR_OID_ARR_BLOCKS_SIZING_ID,
    MAX_MSR_OID_ARR_LIST_BLOCKS_SIZING_ID,
    MAX_MSR_OID_LIST_BLOCKS_SIZING_ID,
    MAX_MSR_Q_DEPTH_SIZING_ID,
    MAX_MSR_RM_HDR_BLOCKS_SIZING_ID,
    MAX_MSR_RM_PKT_BLOCKS_SIZING_ID,
    MAX_MSR_RM_QUEUE_MSG_SIZING_ID,
    MAX_MSR_UPDATE_QUEUE_SIZE_SIZING_ID,
    MSR_MAX_SIZING_ID
};


#ifdef  _MSRSZ_C
tMemPoolId MSRMemPoolIds[ MSR_MAX_SIZING_ID];
INT4  MsrSizingMemCreateMemPools(VOID);
VOID  MsrSizingMemDeleteMemPools(VOID);
INT4  MsrSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _MSRSZ_C  */
extern tMemPoolId MSRMemPoolIds[ ];
extern INT4  MsrSizingMemCreateMemPools(VOID);
extern VOID  MsrSizingMemDeleteMemPools(VOID);
extern INT4  MsrSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _MSRSZ_C  */


#ifdef  _MSRSZ_C
tFsModSizingParams FsMSRSizingParams [] = {
{ "tMsrConfigData", "MAX_MSR_CONFIG_DATA_BLOCKS", sizeof(tMsrConfigData),MAX_MSR_CONFIG_DATA_BLOCKS, MAX_MSR_CONFIG_DATA_BLOCKS,0 },
{ "UINT1[MSR_MAX_DATA_LEN]", "MAX_MSR_DATA_BLOCKS", sizeof(UINT1[MSR_MAX_DATA_LEN]),MAX_MSR_DATA_BLOCKS, MAX_MSR_DATA_BLOCKS,0 },
{ "tSNMP_OID_TYPE", "MAX_MSR_INCR_OID_BLOCKS", sizeof(tSNMP_OID_TYPE),MAX_MSR_INCR_OID_BLOCKS, MAX_MSR_INCR_OID_BLOCKS,0 },
{ "tMSRRbtree", "MAX_MSR_INCR_RB_NODES", sizeof(tMSRRbtree),MAX_MSR_INCR_RB_NODES, MAX_MSR_INCR_RB_NODES,0 },
{ "tMsrMibEntryBlock", "MAX_MSR_MIBENTRY_BLOCKS", sizeof(tMsrMibEntryBlock),MAX_MSR_MIBENTRY_BLOCKS, MAX_MSR_MIBENTRY_BLOCKS,0 },
{ "tMsrOidArrBlock", "MAX_MSR_OID_ARR_BLOCKS", sizeof(tMsrOidArrBlock),MAX_MSR_OID_ARR_BLOCKS, MAX_MSR_OID_ARR_BLOCKS,0 },
{ "tMsrOidArrListBlock", "MAX_MSR_OID_ARR_LIST_BLOCKS", sizeof(tMsrOidArrListBlock),MAX_MSR_OID_ARR_LIST_BLOCKS, MAX_MSR_OID_ARR_LIST_BLOCKS,0 },
{ "tMsrOidList", "MAX_MSR_OID_LIST_BLOCKS", sizeof(tMsrOidList),MAX_MSR_OID_LIST_BLOCKS, MAX_MSR_OID_LIST_BLOCKS,0 },
{ "tAuditInfo", "MAX_MSR_Q_DEPTH", sizeof(tAuditInfo),MAX_MSR_Q_DEPTH, MAX_MSR_Q_DEPTH,0 },
{ "tMsrRmHdrBlock", "MAX_MSR_RM_HDR_BLOCKS", sizeof(tMsrRmHdrBlock),MAX_MSR_RM_HDR_BLOCKS, MAX_MSR_RM_HDR_BLOCKS,0 },
{ "tMsrRmPktBlock", "MAX_MSR_RM_PKT_BLOCKS", sizeof(tMsrRmPktBlock),MAX_MSR_RM_PKT_BLOCKS, MAX_MSR_RM_PKT_BLOCKS,0 },
{ "tMSRRmQueueMsg", "MAX_MSR_RM_QUEUE_MSG", sizeof(tMSRRmQueueMsg),MAX_MSR_RM_QUEUE_MSG, MAX_MSR_RM_QUEUE_MSG,0 },
{ "tMSRUpdatedata", "MAX_MSR_UPDATE_QUEUE_SIZE", sizeof(tMSRUpdatedata),MAX_MSR_UPDATE_QUEUE_SIZE, MAX_MSR_UPDATE_QUEUE_SIZE,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _MSRSZ_C  */
extern tFsModSizingParams FsMSRSizingParams [];
#endif /*  _MSRSZ_C  */


