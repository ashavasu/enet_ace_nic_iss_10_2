/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: isswebnm.h,v 1.30 2015/04/01 06:57:36 siva Exp $
 *
 * Description: macros, typdefs and proto type for ISS web module 
 *******************************************************************/
#ifdef ISS_WANTED

#ifndef _ISSWEBNM_H
#define _ISSWEBNM_H

#include "snmccons.h"
#include "snmcdefn.h"
#include "fssnmp.h"
#include "fswebnm.h"
#include "params.h"
#include "fsvlan.h"
#include "l2iwf.h"

#define  ISS_WEBNM_ALLOC_OID(x) if((x=WebnmAllocOid()) == NULL){return FAILURE;}
#define  ISS_ALLOC_OID(x) if((x=WebnmAllocOid()) == NULL){return;}
#define  ISS_ALLOC_MULTIDATA(x) if((x=WebnmAllocMultiData()) == NULL){return;}

#define  ISS_MAX_QUERY                       50

#ifdef MI_WANTED
#define ISS_PORTS_PER_PAGE  100 
#else
#ifdef MBSM_WANTED
#define ISS_PORTS_PER_PAGE  ((MBSM_MAX_LC_SLOTS * MBSM_MAX_POSSIBLE_PORTS_PER_SLOT) + LA_MAX_AGG)
#else
#define ISS_PORTS_PER_PAGE  (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG) 
#endif 
#endif 

#define  INTERFACES_PER_PAGE                 ISS_PORTS_PER_PAGE

#ifdef MI_WANTED
#define  ISS_DISP_CNT                        (ISS_PORTS_PER_PAGE - 1)
#else
#define  ISS_DISP_CNT                        (ISS_PORTS_PER_PAGE)
#endif /* MI_WANTED */

#define ISS_SPECIFIC_REQUEST 4 

char ISS_KEY_STRING[] = "_KEY";
INT1 ISS_NO_SUCH_OBJ_STR[] = "No_Such_Object";
INT1 ISS_NO_SUCH_INS_STR[] = "No_Such_Instance";

/* for storing the query, from POST */
typedef struct Query{
   UINT1   u1Name[ENM_MAX_NAME_LEN];
   UINT1   u1Value[ENM_MAX_NAME_LEN];
   UINT1   u1Type;      /* for future use - rollback */
   UINT1   u1Access;    /* for future use - rollback */
   UINT2 u2Pad;
} tsQuery;

INT4 HttpNameValueFromPostQuery(tsQuery*, UINT1*);
VOID IssSendCommonError (tHttp*, UINT4);


typedef struct Error{
        UINT1  au1errMsg[256];
}tsError;

/* for storing the array of error messages page wise*/
typedef struct ErrorMsg{
        UINT1  au1Url[100];
        tsError  sError[15];
}tsErrorMsg;

tsErrorMsg  asErrorMsg[] = {
        {"rrd_globalconf.html", 
                {
                        {"RRD Status cannot be disabled"},
                        {"Router ID should be one of the system interface"},
                        {"RRD Status cannot be enabled"},
                        {""}
                }
        },
        {"rrd_ospfconf.html", 
  { 
   {""}, 
   {"Not an AS Border router"}, 
   {""} 
  } 
 }, 
        
        {"rrd_filterconf.html", 
                {
                        {"Unable to create, check whether ROW already exist"},
                        {"Unable to create, check whether ROW already exist"},
                        {"Unable to set Source protocol"},
                        {"Unable to set Destination protocol"},
                        {"Unable to set Action"},
                        {"Unable to change the ROW status"},
                        {""}
                }
        },
        {"bgp_globalconf.html", 
                {
                        {"Unable to set AS number"},
                        {"Unable to set Sychronisation"},
                        {"Unable to set Overlap router policy"},
                        {"Unable to set BGP status"},
                        {""}
                }
        },
        {"dhcp_poolconf.html", 
                {
                        {""},
                        {"Unable to create, check whether ROW already exist"},
                        {"Unable to set Subnet"},
                        {"Unable to set Port number"},
                        {"Unable to set Subnet mask"},
                        {"Unable to set Start IP"},
                        {"Unable to set End IP"},
                        {"Unable to set Lease time"},
                        {"Unable to modify, check whether ROW already exist"},
                        {""}
                }
        },
        {"dhcp_excl.html", 
                {
                        {""},
                        {""},
                        {""},
                        {"Invalid EndIPAddress"}, 
                        {""},
                        {""}
                }
        },
        {"eoam_loopbackconfig.html", 
                {
                        {""},
                        {"Please select the row"}, 
                        {"Remote peer does not have loopback capability"},
                        {""}
                }
        },
        {"pim_component.html",
                {
                        {""},
                        {""},
                        {"Component In use. Cannot Proceed"},
                        {""},
                        {""},
                        
                }
        },
        {"vlan_basicinfo.html", 
                {
                        {""},
                        {""},
                        {""},
                        {""},
                        {""},
                        {""},
                        {""},
                        {""},
                        {""},
                        {""},
                        {"Refer Note 4, for knowing the pre-requisites required for setting Base Bridge Mode configuration"},
                }
        },
        {"mstp_timersconf.html",
                {
                        {""},
                        {""},
                        {""},
                        {"Relation not satisfied between entities Forward Delay,Max Age,Hello Time"},
                        {"Relation not satisfied between entities Forward Delay,Max Age,Hello Time"},
                        {""},
                        {"Relation not satisfied between entities Forward Delay,Max Age,Hello Time"},
                        
                }
        },
        {"isis_circlevelconf.html",
            {
                {"Invalid Circuit Level Indices"},
                {"Invalid Circuit Level Indices"},
                {"Invalid Circuit Level Indices"},
                {"Failed in configuring Ckt Level Metric"},
                {"Failed in configuring Ckt Level ISPriority"},
                {"Failed in configuring Ckt Level LANDesISChanges"},
                {"Failed in configuring Ckt Level HelloMultiplier"},
                {"Failed in configuring Hello Timer"},
                {"Failed in configuring DR Hello Timer"},
                {"Failed in configuring CktLevel LSP throttle"},
                {"Failed in configuring CktLevel MinLSReTransInt"},
                {"Failed in configuring CktLevel CSNP Interval"},
                {"Failed in configuring CktLevel Part SNP Interval"},
            }
        },

        {"isis_sysinstconf2.html",
            {
                {""},
                {"Failed in configuring Originated L1LSP BuffSize"},
                {"Failed in configuring Originated L2LSP BuffSize"},
                {"Failed in configuring Max Area Addresses"},
                {"Failed in configuring Max Area Check"},
                {"Failed in configuring Wait Time"},
                {""},
                {""},
                {"Failed in configuring OverLoad Bit"},
                {"Failed in configuring Max Age"},
                {"Failed in configuring Delay Metric"},
                {"Failed in configuring Error Bit"},
                {"Failed in configuring Expense"},
                {""},
                {""},
            }
        },
        {"isis_arearxpwdconf.html",
            {
                {"Invalid Instance Index"},
                {"Invalid AreaRxPassword"},
            }
        },
        {"isis_domainrxpwdconf.html",
            {
                {"Invalid Instance Index"},
                {"Invalid DomainRxPassword"},
            }
        },
        {"gen_infoclearconfig.html",
            {
                {""},
  {"Clear Config: Invalid Clear configuration Restoration File"},
                {"Clear config is not supported for packages included in the build"},
            }
        },
        {"", {{""}}}
};

INT1                IssSnmpError[19][60] = {
 /* 0  */ "",
 /* 1  */ "SNMP Error Too Big",
 /* 2  */ "SNMP Error No Such Name",
 /* 3  */ "SNMP Error Bad Value",
 /* 4  */ "SNMP Error Read Only",
 /* 5  */ "SNMP General Error",
 /* 6  */ "SNMP Error No Access",
 /* 7  */ "SNMP Error Wrong Type",
 /* 8  */ "SNMP Error Wrong Length",
 /* 9  */ "SNMP Error Wrong Encoding",
 /* 10 */ "SNMP Error Wrong Value",
 /* 11 */ "SNMP Error No Creation",
 /* 12 */ "SNMP Error Inconsistent Value",
 /* 13 */ "SNMP Error Resource Unavailable",
 /* 14 */ "SNMP Error Commit Failed",
 /* 15 */ "SNMP Error Undo Failed",
 /* 16 */ "SNMP Error Authorization Error",
 /* 17 */ "SNMP Error Not Writable",
 /* 18 */ "SNMP Error Inconsistent Name"
      }; 


extern tSnmpIndex WebnmIndexPool1[MAX_NUM_PROCESS][ISS_MAX_INDICES];
extern tSNMP_MULTI_DATA_TYPE WebnmMultiPool1[MAX_NUM_PROCESS][ISS_MAX_INDICES];
extern tSNMP_OCTET_STRING_TYPE WebnmOctetPool1[MAX_NUM_PROCESS][ISS_MAX_INDICES];
extern tSNMP_OID_TYPE  WebnmOIDPool1[MAX_NUM_PROCESS][ISS_MAX_INDICES];
extern UINT1 au1WebnmData1[MAX_NUM_PROCESS][ISS_MAX_INDICES][WEBNM_MEMBLK_SIZE];

extern tSnmpIndex WebnmIndexPool2[MAX_NUM_PROCESS][ISS_MAX_INDICES];
extern tSNMP_MULTI_DATA_TYPE WebnmMultiPool2[MAX_NUM_PROCESS][ISS_MAX_INDICES];
extern tSNMP_OCTET_STRING_TYPE WebnmOctetPool2[MAX_NUM_PROCESS][ISS_MAX_INDICES];
extern tSNMP_OID_TYPE  WebnmOIDPool2[MAX_NUM_PROCESS][ISS_MAX_INDICES];
extern UINT1 au1WebnmData2[MAX_NUM_PROCESS][ISS_MAX_INDICES][WEBNM_MEMBLK_SIZE];

/* Prototypes */

VOID IssProcessGet (tHttp * pHttp);
INT4 IssProcessPost (tHttp * pHttp);


/* externs */
extern UINT1 ISS_MI_FLAG[];

extern VOID WebnmFormInstance(tHttp *,UINT1 *);
extern tSNMP_OID_TYPE * WebnmAllocOid(VOID);

VOID IssConvertDataToString (tSNMP_MULTI_DATA_TYPE*, UINT1*, UINT1);
tSNMP_MULTI_DATA_TYPE* IssConvertStringToData (UINT1*, UINT1);


#define IPVX_IPV6_ADDR_LEN   16
#define IPVX_MAX_INET_ADDR_LEN  IPVX_IPV6_ADDR_LEN
#define WEB_INET_ATON6(s, pin)  UtlInetAton6((const CHR1 *)s, (tUtlIn6Addr *)pin)
#define WEB_INET_NTOA6(in)      UtlInetNtoa6(in)
#define SYSLOG_NTOHL                        (UINT4 )OSIX_NTOHL
#define   SYSLOG_HTONL                      (UINT4 )OSIX_HTONL
#define WEB_INET_NTOHL(GrpIp1)\
{\
    UINT4   u4TmpGrpAddr = 0;\
    UINT4   u4Count1 = 0;\
    UINT1   u1Index = 0;\
    UINT1   au1TmpGrpIp[IPVX_MAX_INET_ADDR_LEN];\
      \
    MEMSET (au1TmpGrpIp, 0, IPVX_MAX_INET_ADDR_LEN);\
    \
    for (u4Count1 = 0, u1Index = 0; u4Count1 < IPVX_MAX_INET_ADDR_LEN; \
            u1Index++, u4Count1 = u4Count1 + 4)\
    {\
   MEMCPY (&u4TmpGrpAddr, (GrpIp1 + u4Count1), sizeof(UINT4));\
        u4TmpGrpAddr = SYSLOG_NTOHL(u4TmpGrpAddr);\
   MEMCPY (&(au1TmpGrpIp[u4Count1]), &u4TmpGrpAddr, sizeof(UINT4));\
    }\
   MEMCPY (GrpIp1, au1TmpGrpIp, IPVX_MAX_INET_ADDR_LEN);\
}


#define WEB_INET_HTONL(GrpIp1)\
{\
    UINT4   u4TmpGrpAddr = 0;\
    UINT4   u4Count1 = 0;\
    UINT1   u1Index = 0;\
    UINT1   au1TmpGrpIp[IPVX_MAX_INET_ADDR_LEN];\
      \
    MEMSET (au1TmpGrpIp, 0, IPVX_MAX_INET_ADDR_LEN);\
    \
    for (u4Count1 = 0, u1Index = 0; u4Count1 < IPVX_MAX_INET_ADDR_LEN; \
            u1Index++, u4Count1 = u4Count1 + 4)\
    {\
   MEMCPY (&u4TmpGrpAddr, (GrpIp1 + u4Count1), sizeof(UINT4));\
        u4TmpGrpAddr = SYSLOG_HTONL(u4TmpGrpAddr);\
   MEMCPY (&(au1TmpGrpIp[u4Count1]), &u4TmpGrpAddr, sizeof(UINT4));\
    }\
    MEMCPY (GrpIp1, au1TmpGrpIp, IPVX_MAX_INET_ADDR_LEN);\
}

#endif /* _ISSWEBNM_H */
#endif /* ISS_WANTED */

