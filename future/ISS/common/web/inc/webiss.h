/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved 
 *
 * $Id: webiss.h,v 1.315.2.1 2018/03/15 12:58:48 siva Exp $
 * 
 * Description:  macros,typedefs and prototypes for ISS web Module
 *******************************************************************/

#ifndef _ISSWEB_H
#define _ISSWEB_H



#define WLAN_CREATENEW                  1
#define WLAN_PROFILE_CAPABILITY         2
#define WLAN_PROFILE_AUTHENTICATION     3
#define WLAN_PROFILE_QOS                4



#include "snmccons.h"
#include "snmcdefn.h"
#include "fssnmp.h"
#include "fswebnm.h"
#include "cfa.h"
#include "bridge.h"
#include "tcp.h"
#include "sli.h"
#include "fsvlan.h"
#include "rstp.h"
#include "pnac.h"
#include "ip.h"
#include "ipcli.h"
#include "iss.h"
#include "params.h"
#include "l2iwf.h"
#include "igmp.h"
#include "dhcp.h"
#include "ip6util.h"
#include "bgp.h"
#include "isis.h"
#include "bfd.h"
#ifdef VXLAN_WANTED
#include "fsvxlan.h"
#endif
#ifdef IP6_WANTED
#include "ipv6.h"
#endif
#ifdef BGP_WANTED
#include "bgp4dfs.h"
#include "bgp4mac.h"
#include "bgpweb.h"
#ifdef BGP4_IPV6_WANTED
#include "bgip6dfs.h"
#endif /* BGP4_IPV6_WANTED */
#endif /* BGP_WANTED */
#ifdef CN_WANTED
#include "cn.h"
#endif /*CN_WANTED */
#ifdef DCBX_WANTED
#include "dcbx.h"
#endif /*DCBX_WANTED */
#ifdef CLKIWF_WANTED
#include "clkiwfweb.h"
#endif
#ifdef TAC_WANTED
#include "tacweb.h"
#endif
#ifdef LLDP_WANTED
#include "lldp.h"
#include "lldpweb.h"
#endif
#include "rmapweb.h"
#include "fltrweb.h"
#ifdef MPLS_WANTED
#include "mplsweb.h"
#endif
#ifdef PTP_WANTED
#include "ptpweb.h"
#endif
#ifdef ECFM_WANTED
#include "ecfm.h"
#include "ecfmweb.h"
#endif

#ifdef FIREWALL_WANTED
#include "firewall.h"
#include "fwlweb.h"
#endif
#ifdef DHCP_SRV_WANTED
#include "dhcpsweb.h"
#endif


typedef struct specificpage
{
    UINT1               au1Page[100];
    VOID                (*pfunctPtr) (tHttp *);
}
tSpecificPage;
#ifdef _ISS_WEB_C_
UINT1               gau1WebPortBitMask[8] =
    { 0x01, 0x80, 0x40, 0x20, 0x10,
    0x08, 0x04, 0x02
};
#else
extern UINT1               gau1WebPortBitMask[];
#endif
extern UINT4          gu4VcmContext;


extern INT4 EntUtilMaxLogIndex (VOID);


#define SNOOP_ACTIVE 0x01
#define SNOOP_NOT_IN_SERVICE 0x02
#define SNOOP_NOT_READY 0x03
#define SNOOP_CREATE_AND_WAIT 0x05
#define SNOOP_DESTROY 0x06
#define SNOOP_INIT_VAL 0
#define SNOOP_CREATE 1
#define SNOOP_SET_ACTIVE 2
#define SNOOP_DELETE 3
#define WEB_MAX_ENTRIES_B4_RELINQUISHING        100
#define FIRST_PORT_LIST              1
#define WEB_LEDS_PER_ROW            42      
#define INITIAL_INDEX               0
#define SAVE_PORT_LEN               2      
#define ADD                         10
#define ISS_IPV4_PING_INDEX          9 
#define MAX_LENGTH                  10
#define ISS_IPV6_PING_INDEX          4
#define ISS_IPV6_PING_UP             1
#define ISS_IPV6_PING_DOWN           2
#define ISS_IPV6_PING_INVALID        3
#define ISS_IPV6_PING_CREATE         4
#define ISS_IPV6_PINGDATA            6
#define ISS_IPV6_PING_DEF_INTERVAL   1
#define ISS_IPV6_PING_DEF_RCVTIMEOUT 5
#define ISS_IPV6_PING_DEF_PINGTRIES  5
#define ISS_IPV6_PING_DEF_PINGSIZE  100
#define ISS_MRP                      1
#define ISS_MRP_MMRP                 3
#define ISS_MRP_MVRP                 2
#define ISS_STATISTICS               1
#define ISS_CONFIGURATIONS           2
#define ISS_MRP_VID_ATTR_TYPE        1
#define ISS_MRP_NORMAL_PARTICIPANT   1
#define ISS_MRP_SERVICE_REQ_ATTR_TYPE 1
#define ISS_MRP_MAC_ADDR_ATTR_TYPE    2
#define ISS_IPV6_PING_MAX_ADDRESS_LEN  16 
#define ISS_PAGE_LINK_BUF_SIZE               512
#define ISS_PAGE_STRING_SIZE                 128
#define ISS_STRING_LENGTH                  64
#define ISS_DHCP_MAX_OPT_LEN   255
#define  DHCP_INFINITE_LEASE           0x7fffffff
#define TRACE_ROUTE_ENABLE   1
#define TRACE_ROUTE_DISABLE    2
#define CFA_RS_ACTIVE        1
#define CHECK_ROUTER_PORT    2
#define IP6_WEB_ND_M_BIT            2
#define IP6_WEB_ND_O_BIT             4
#define QOS_CLASS_MAX_LEN    100     

#define  IP6_IF_ROUT_ADV_M_BIT       1                /* M bit */
#define  IP6_IF_ROUT_ADV_O_BIT       2                /* O bit */
#define  IP6_IF_ROUT_ADV_NO_M_BIT    3                /* No M Bit */
#define  IP6_IF_ROUT_ADV_NO_O_BIT    4                /* No O Bit */
#define  IP6_IF_ROUT_ADV_BOTH_BIT    5                /* M and O Bits */
#define  IP6_IF_ROUT_ADV_NO_BIT      6                /* None */

/* For WSS Web Authentication definitions */
#define WSS_WEBAUTH_LANG_ENGLISH            0x1
#define WSS_WEBAUTH_LANG_FRENCH             0x2
#define WSS_WEBAUTH_LANG_GERMAN             0x4
#define WSS_WEBAUTH_LANG_SPANISH            0x8
#define WSS_WEBAUTH_LANG_PORTUGUESE         0x10
#define WSS_WEBAUTH_LANG_RUSSIAN            0x20
#define WSS_WEBAUTH_LANG_ARABIC             0x40
#define WSS_WEBAUTH_LANG_ITALIAN            0x80
#define WSS_WEBAUTH_LANG_CHINESE            0x100
#define WSS_WEBAUTH_LANG_HINDI              0x200
#define WSS_WEBAUTH_LANG_TELUGU             0x400
#define WSS_WEBAUTH_LANG_TAMIL              0x800
#define WSS_WEBAUTH_LOCAL_AUTH              1
#define WSS_WEBAUTH_RADIUS_AUTH             2
#define WSS_WEBAUTH_SUCCESS                 1
#define WSS_WEBAUTH_FAILED                  0
#define WSS_WEBAUTH_DEFAULT_LIFETIME        1440

#define WSS_AP_MAX_PROFILE        4097


#define WEB_MAX_STR_LEN        100
#define WEB_SUCCESS                         1
#define WEB_FAILURE                         2
#define LOOPBACK_NAME_LEN                       15
#define MAX_RATE_LEVEL                      2
#define LA_MAX_PORTS_PER_CHANNEL            24
#define MSTP_MAX_BRIDGEID_BUFFER       8
#define AST_INIT_VAL                   0
#define AST_BRG_PRIORITY_SIZE          2
#define AST_MAC_ADDR_SIZE              6
#define MST_CIST_CONTEXT               0
#define MAX_WEB_BUFFER_SIZE         1024
#define AST_BRG_ADDRS_DIS_LEN          21
#define WEB_INET_ATON(s, pin)   UtlInetAton((const CHR1 *)s, (tUtlInAddr *)pin)
#define WEB_INTEGER_TO_OCTETSTRING(u4Index,pOctetString) {\
            UINT4 u4LocalIndex = OSIX_HTONL(u4Index);\
            MEMCPY(pOctetString->pu1_OctetList,(UINT1*)&u4LocalIndex,\
                   sizeof(UINT4));\
             pOctetString->i4_Length=sizeof(UINT4);};

#define WEB_CONVERT_VAL_TO_STR(pString, u4Value) \
MEMCPY(pString, &u4Value, sizeof(UINT4));

#define WEB_OCTETSTRING_TO_INTEGER(pOctetString,u4Index) { \
          u4Index = 0;\
         if(pOctetString->i4_Length > 0 && pOctetString->i4_Length <= 4){\
             MEMCPY((UINT1 *)&u4Index,pOctetString->pu1_OctetList,\
                   pOctetString->i4_Length);\
                u4Index = OSIX_NTOHL(u4Index);}}

#define WEB_CONVERT_DOT_STR_TO_ARRAY(pDotStr, pu1Arr) \
{ \
   UINT1 u1Index = 0; \
   UINT1 *pu1Temp = pDotStr; \
      \
    for (u1Index = 0; u1Index < OctetLen(pDotStr); u1Index++) \
    { \
       UINT1 u1MacIndex = 0; \
       UINT4 u4Value = 0; \
       UINT1 u1Char; \
       \
       for (u1MacIndex = 0; ((*pu1Temp != ':') && \
                       (*pu1Temp != '\0')); u1MacIndex++) \
       { \
          if (ISXDIGIT (*pu1Temp)) \
          {\
             if (!ISDIGIT (*pu1Temp)) \
             { \
                u1Char = (UINT1) (10 + ((*pu1Temp) - 'a')); \
             } \
             else \
             { \
              u1Char = *pu1Temp; \
             } \
             u4Value = (u4Value * 16) + (0x0f & u1Char); \
          } \
          (pu1Temp)++; \
       } \
       if (u1Index < sizeof(pu1Arr)) \
       {\
       (pu1Arr)[u1Index] = (UINT1) u4Value; \
       (pu1Temp)++; \
    } \
    } \
}
#define WEB_IS_MEMBER_PORT(au1PortArray, u2Port, u1Result) \
{\
   UINT2 u2PortBytePos;\
      UINT2 u2PortBitPos;\
      u2PortBytePos = (UINT2)(u2Port / WEB_PORTS_PER_BYTE);\
      u2PortBitPos  = (UINT2)(u2Port % WEB_PORTS_PER_BYTE);\
      if (u2PortBitPos  == 0) {u2PortBytePos = (UINT2) (u2PortBytePos - 1);} \
         \
            if ((au1PortArray[u2PortBytePos] \
                 & gau1WebPortBitMask[u2PortBitPos]) != 0) {\
               \
                  u1Result = ISS_TRUE;\
            }\
            else {\
               \
                  u1Result = ISS_FALSE; \
            } \
}

#define WEB_SET_MEMBER_PORT(au1PortArray, u2Size, u2Port) \
           {\
              UINT2 u2PortBytePos;\
              UINT2 u2PortBitPos;\
              if (u2Port)\
              {\
               u2PortBytePos = (UINT2)(u2Port / WEB_PORTS_PER_BYTE);\
               u2PortBitPos  = (UINT2)(u2Port % WEB_PORTS_PER_BYTE);\
               if (u2PortBitPos  == 0) {u2PortBytePos = (UINT2)(u2PortBytePos - 1);} \
                   if(u2PortBytePos < u2Size)\
                      {\
               au1PortArray[u2PortBytePos] =\
                             (UINT1)(au1PortArray[u2PortBytePos] \
                             | gau1WebPortBitMask[u2PortBitPos]);\
              }\
              }\
           }    
#define WEB_INET_ATON6(s, pin)  UtlInetAton6((const CHR1 *)s, (tUtlIn6Addr *)pin)
#define WEB_INET_NTOA6(in)      UtlInetNtoa6(in)
#define SYSLOG_NTOHL                        (UINT4 )OSIX_NTOHL
#define   SYSLOG_HTONL                      (UINT4 )OSIX_HTONL
#define SMTP_DOMAIN_NAME_LEN 100
#define WEB_INET_NTOHL(GrpIp1)\
{\
    UINT4   u4TmpGrpAddr = 0;\
    UINT4   u4Count = 0;\
    UINT1   u1Index = 0;\
    UINT1   au1TmpGrpIp[IPVX_MAX_INET_ADDR_LEN];\
      \
    MEMSET (au1TmpGrpIp, 0, IPVX_MAX_INET_ADDR_LEN);\
    \
    for (u4Count = 0, u1Index = 0; u4Count < IPVX_MAX_INET_ADDR_LEN; \
            u1Index++, u4Count = u4Count + 4)\
    {\
   MEMCPY (&u4TmpGrpAddr, (GrpIp1 + u4Count), sizeof(UINT4));\
        u4TmpGrpAddr = SYSLOG_NTOHL(u4TmpGrpAddr);\
   MEMCPY (&(au1TmpGrpIp[u4Count]), &u4TmpGrpAddr, sizeof(UINT4));\
    }\
   MEMCPY (GrpIp1, au1TmpGrpIp, IPVX_MAX_INET_ADDR_LEN);\
}

#define IPVX_IPV6_ADDR_LEN   16
#define IPVX_MAX_INET_ADDR_LEN  IPVX_IPV6_ADDR_LEN

#define IP6_ND_NO_SUPPRESS_RA    1
#define IP6_IF_MIN_RA_MIN_TIME   3
#define IP6_ND_INVLID_INTERVAL   0xFFFF0000

#define WEB_INET_HTONL(GrpIp1)\
{\
    UINT4   u4TmpGrpAddr = 0;\
    UINT4   u4Count1 = 0;\
    UINT1   u1Index = 0;\
    UINT1   au1TmpGrpIp[IPVX_MAX_INET_ADDR_LEN];\
      \
    MEMSET (au1TmpGrpIp, 0, IPVX_MAX_INET_ADDR_LEN);\
    \
    for (u4Count1 = 0, u1Index = 0; u4Count1 < IPVX_MAX_INET_ADDR_LEN; \
            u1Index++, u4Count1 = u4Count1 + 4)\
    {\
   MEMCPY (&u4TmpGrpAddr, (GrpIp1 + u4Count1), sizeof(UINT4));\
        u4TmpGrpAddr = SYSLOG_HTONL(u4TmpGrpAddr);\
   MEMCPY (&(au1TmpGrpIp[u4Count1]), &u4TmpGrpAddr, sizeof(UINT4));\
    }\
    MEMCPY (GrpIp1, au1TmpGrpIp, IPVX_MAX_INET_ADDR_LEN);\
}

#define INACTIVESLOT        3

#define ISS_VLANLIST_LEN  512
#define ISS_VLAN_PORTLIST_LEN  5000

#define VLAN_IFPORT_LIST_SIZE  BRG_PORT_LIST_SIZE


#define ISS_WEB_SNMP_MASK             0x01
#define ISS_WEB_TELNET_MASK           0x02
#define ISS_WEB_HTTP_MASK             0x04   
#define ISS_WEB_HTTPS_MASK            0x08
#define ISS_WEB_SSH_MASK              0x10
#define ISS_WEB_ALL_MASK              0x1f

#define  ISS_WEB_ETH_TYPE   0x01
#define  ISS_WEB_VLAN_TYPE  0x02
#define  ISS_WEB_PPP_TYPE   0x04
#define  ISS_WEB_MP_TYPE    0x08
#define  ISS_WEB_ALL_TYPE   0x0f

#define   NAT_ENABLE                   1
#define   NAT_DISABLE                  2

#define  VLANID_ERROR                  1
#define MEMBER_PORTS_ERROR             2
#define FORBIDDEN_PORTS_ERROR          3
#define UNTAGGED_PORTS_ERROR           4

/* Constants defined for specific pages processing */
#define ISS_WEB_ZERO                  0
#define ISS_WEB_GEN_INFO              1    
#define ISS_IVR_PAGE                  2
#define ISS_IP_SETTINGS_PAGE          3
#define ISS_IGS_PAGE                  4 
#define ISS_PORT_SETTINGS_PAGE        5 
#define ISS_OSPF_PAGE                 6 
#define ISS_PIM_IFACE_PAGE            7 
#define ISS_PIM_CRP_PAGE              8
#define ISS_IGMP_PAGE                 9
#define IGMP_WEB_ENABLE               1  
#define IGMP_ZERO                    0  
#define ISS_IGMP_NOT_IN_SERVICE       2
#define ISS_STATIC_VLAN_PAGE          10
#define ISS_L2_UNICAST_FILTER_PAGE    11
#define ISS_L2_MCAST_FILTER_PAGE      12
#define ISS_TRAFFIC_CLASS_PAGE        13
#define ISS_PORT_MIRRORING_PAGE       14
#define ISS_MSTI_VLAN_MAP_PAGE        15
#define ISS_SAVE_PAGE                 16
#define ISS_LED_PAGE                  17
#define ISS_OSPF_AREA_PAGE            18
#define   TAC_CLI_SECRET_KEY_LEN      64
#define OCTETSTR_SIZE                50
#define ISS_MANUAL                    1 
#define ISS_ADDRESS_PROTO_RARP        2
#define ISS_ADDRESS_PROTO_DHCP        3

#define MAX_ADDRESS_BUFFER_VLAN      32 
#define MAX_ADDRESS_BUFFER_PNAC      21  

#define   TAC_SRV_DEFAULT_KEY   "Aricent"   
/* TACACS+ protocol TCP Port */
#define   TAC_TCP_IPV4_PORT   49
#define   TAC_TCP_IPV6_PORT   4949
#define   TAC_SRV_DEFAULT_TIMEOUT   5 
#define WEB_PORTS_PER_BYTE           8
#define ISS_INVALID                  2
#define ISS_MAX_VLAN_PRIORITY        8
#define ISS_MSTP_MAX_VLANMAP_BUFFER  128
#define ISS_MSTP_FORWARDING          5
#define ISS_MSTP_DISABLED            1
#define ISS_MSTP_LEARNING            4
#define ISS_MSTP_BLOCKING            2
#define ISS_OSPF_NORMAL_AREA         1
#define ISS_OSPF_NSSA_AREA         3
#define ISS_OSPF_NO_AS_IMPORT_EXTERN 2
#define ISS_OSPF_SEND_AREA_SUMMARY   2
#define ISS_OSPF_NO_AREA_SUMMARY   1
#define ISS_OSPF_DEF_NSSA_STABILITY_INTERVAL 40
#define ISS_OSPF_DEF_STUB_METRIC    10
#define ISS_OSPF_TRANSROLE_CANDIDATE 2 
#define ISS_AUTH_NONE               0
#define ISS_AUTH_SIMPLE_PASSWD      1
#define ISS_AUTH_MD5                2  
#define ISS_AUTH_SHA1               3
#define ISS_AUTH_SHA2_224           4
#define ISS_AUTH_SHA2_256           5
#define ISS_AUTH_SHA2_384           6
#define ISS_AUTH_SHA2_512           7
#define ISS_VLAN_5BYTE_PROTO_TEMP_SIZE            5  
#define ISS_MAX_NAME_LEN            1024  
#define ISS_MSTP_MAX_BRIDGEID_BUFFER 8
#define ISS_MSTP_MAX_PORTID_BUFFER 2
#define ISS_MSTP_MAX_DIGEST_BUFFER 16
#define ISS_MAX_ADDR_BUFFER 256
#define ISS_RIP_AUTH_MAX_BUFFER 20
#define ISS_RIP_NBR_LIST_DISABLE 2
#define ISS_RIP_RRD_DEF_RT_METRIC 3

#define ISS_ROUTE_LOCAL_ID        0x02
#define ISS_ROUTE_STATIC_ID       0x03
#define ISS_ROUTE_RIP_ID          0x08
#define ISS_ROUTE_OSPF_ID         0x0d 
#define ISS_ROUTE_BGP_ID          0x0e

#define ISS_WEB_BIT8              0x80

#define ISS_PVRST_MAX_BRIDGEID_BUFFER     8
#define ISS_PVRST_MAX_PORTID_BUFFER      2
#define ISS_PVRST_DEFAULT_CONTEXT  L2IWF_DEFAULT_CONTEXT

#ifdef MI_WANTED
#define ISS_PORTS_PER_PAGE  100 
#else
#ifdef MBSM_WANTED
#define ISS_PORTS_PER_PAGE  (MBSM_MAX_POSSIBLE_PORTS_PER_SLOT + LA_MAX_AGG)
#else
#define ISS_PORTS_PER_PAGE  (SYS_DEF_MAX_PHYSICAL_INTERFACES + LA_MAX_AGG) 
#endif 
#endif 

#ifdef MI_WANTED
#define ISS_INSTANCES_PER_PAGE  100

#else
#define ISS_INSTANCES_PER_PAGE  12
#endif

#define ISS_MAX_LACP_STATE_LENGTH     100

#define OTHER                0xFFFF    
#define ISS_PROT_OTHER       0xFF    
#define HUNDEREDTH_OF_SEC  100

#define   PRIMARY_IP                  1
#define   SECONDARY_IP                2
  
/* Constants defined for Pb-vlan */
#define ISS_VLAN_HIGHEST_PCPSELROW    4

#define ISS_INIT_VAL                0xFFFFFFFF
#define WEB_BRG_PRIORITY_SIZE               2

#define ISS_LA_CONFIGPORTS_BUFSIZE        64
/* constants defined for LoopBack Processing */
#define LOOPBACK_MODIFY        1
#define LOOPBACK_ADD        2
#ifdef VXLAN_WANTED
#define VXLANNVE_MODIFY        1
#define VXLANNVE_ADD        2
#endif
/*#define RIP_ADMIN_ENABLE  1
#define RIP_ADMIN_DISABLE 2*/
/* constants defined for Snmp AgentXSubAgent */
#define ISS_SNX_IPv4                    1
#define ISS_SNX_IPv6                    2
#define ISS_SNMP_AGT_AGTX_DISABLE       3
/*constants defined for Sntp*/
#define  SNTP_CLIENT_ADDR_MODE_UNICAST     1

/* constants defined for Ssl Certificate */
#define ISS_WEB_SSL_COMMON_NAME_SIZE 100
#define ISS_WEB_APP_CLI_HANDLE  1
#define ISS_WEB_SSL_MAX_CERT_REQ_LEN  4096
#define ISS_WEB_AUTH_FILE_NAME 32

#ifdef MEF_WANTED 
#define CONTEXT_ID 0
#define MEG_INDEX  1
#define ME_INDEX 2
#define MEP_IDENTIFIER 3
#endif
#ifdef WEBNM_TEST_WANTED   
#define HTTP_LOCK()   HttpLock ()   
#define HTTP_UNLOCK() HttpUnLock ()   
extern INT4 HttpLock (VOID);   
extern INT4 HttpUnLock (VOID);   
extern INT1   
nmhGetFirstIndexFsHttpAuthTestTable ARG_LIST((INT4 *));   
extern INT1   
nmhGetNextIndexFsHttpAuthTestTable ARG_LIST((INT4 , INT4 *));   
extern INT1   
nmhGetFsHttpWWWAuthHeader ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));   
extern INT1   
nmhGetFsHttpAuthorizeHeader ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));   
extern INT1   
nmhGetFsHttpAuthInfoHeader ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));   
extern INT1   
nmhGetFsHttpWWWAuthScheme ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));   
extern INT1   
nmhGetFsHttpWWWAuthRealm ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));   
extern INT1   
nmhGetFsHttpWWWAuthUsername ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));   
extern INT1   
nmhGetFsHttpWWWAuthNonce ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));   
extern INT1   
nmhGetFsHttpWWWAuthQop ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));   
extern INT1   
nmhGetFsHttpWWWAuthAlgorithm ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));   
extern INT1   
nmhGetFsHttpWWWAuthStale ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));   
extern INT1   
nmhGetFsHttpAuthInfoQop ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));   
extern INT1   
nmhGetFsHttpAuthInfoRespAuth ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));   
extern INT1   
nmhGetFsHttpAuthInfoCnonce ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));   
extern INT1   
nmhGetFsHttpAuthInfoNonceCount ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));   
VOID   
IssProcessHttpStatsTablePage (tHttp *);   
#endif /* WEBNM_TEST_WANTED */   

#ifdef RFMGMT_WANTED
extern VOID IssRfmTpcConfigPage PROTO ((tHttp *pHttp));
extern VOID IssRfmTpcConfigPageGet (tHttp * pHttp);
extern VOID IssRfmTpcConfigPageSet (tHttp * pHttp);

extern VOID IssRfmTpcWirelessPage PROTO ((tHttp *pHttp));
extern VOID IssRfmTpcWirelessPageGet (tHttp * pHttp);
extern VOID IssRfmTpcWirelessPageSet (tHttp * pHttp);

extern VOID IssRfmRadioANTpcPage PROTO ((tHttp *pHttp));
extern VOID IssRfmRadioANTpcPageGet (tHttp * pHttp);
extern VOID IssRfmRadioANTpcPageSet (tHttp * pHttp);

extern VOID IssRfmRadioBGNTpcPage (tHttp * pHttp);
extern VOID IssRfmRadioBGNTpcPageGet (tHttp * pHttp);
extern VOID IssRfmRadioBGNTpcPageSet (tHttp * pHttp);

extern VOID IssRfmRadioGTpcPage (tHttp * pHttp);
extern VOID IssRfmRadioGTpcPageGet (tHttp * pHttp);
extern VOID IssRfmRadioGTpcPageSet (tHttp * pHttp);

extern VOID IssRfmDfsInfoPage (tHttp * pHttp);
extern VOID IssRfmDfsInfoPageGet (tHttp * pHttp);
extern VOID IssRfmDfsInfoPageSet (tHttp * pHttp);

extern VOID IssRfmDfsConfigGlobalPage PROTO ((tHttp *pHttp));
extern VOID IssRfmDfsConfigGlobalPageGet (tHttp * pHttp);
extern VOID IssRfmDfsConfigGlobalPageSet (tHttp * pHttp);

extern VOID IssRfmDfsConfigPage PROTO ((tHttp *pHttp));
extern VOID IssRfmDfsConfigPageGet (tHttp * pHttp);
extern VOID IssRfmDfsConfigPageSet (tHttp * pHttp);

extern VOID IssRfmRadioANDpaPage (tHttp * pHttp);
extern VOID IssRfmRadioANDpaPageGet (tHttp * pHttp);
extern VOID IssRfmRadioANDpaPageSet (tHttp * pHttp);

extern VOID IssRfmRadioBGNDpaPage (tHttp * pHttp);
extern VOID IssRfmRadioBGNDpaPageGet (tHttp * pHttp);
extern VOID IssRfmRadioBGNDpaPageSet (tHttp * pHttp);

extern VOID IssRfmRadioGDpaPage (tHttp * pHttp);
extern VOID IssRfmRadioGDpaPageGet (tHttp * pHttp);
extern VOID IssRfmRadioGDpaPageSet (tHttp * pHttp);

extern VOID IssRfmRadioANShaPage (tHttp * pHttp);
extern VOID IssRfmRadioANShaPageGet (tHttp * pHttp);
extern VOID IssRfmRadioANShaPageSet (tHttp * pHttp);

extern VOID IssRfmRadioBGNShaPage (tHttp * pHttp);
extern VOID IssRfmRadioBGNShaPageGet (tHttp * pHttp);
extern VOID IssRfmRadioBGNShaPageSet (tHttp * pHttp);

extern VOID IssRfmRadioGShaPage (tHttp * pHttp);
extern VOID IssRfmRadioGShaPageGet (tHttp * pHttp);
extern VOID IssRfmRadioGShaPageSet (tHttp * pHttp);

extern VOID IssRfmRadioANNbrAPDtlPage (tHttp * pHttp);
extern VOID IssRfmRadioANNbrAPDtlPageGet (tHttp * pHttp);
extern VOID IssRfmRadioANNbrAPDtlPageSet (tHttp * pHttp);

extern VOID IssRfmRadioANFldAPDtlPage (tHttp * pHttp);
extern VOID IssRfmRadioANFldAPDtlPageGet (tHttp * pHttp);
extern VOID IssRfmRadioANFldAPDtlPageSet (tHttp * pHttp);

extern VOID IssRfmRadioANCltScanDtlPage (tHttp * pHttp);
extern VOID IssRfmRadioANCltScanDtlPageGet (tHttp * pHttp);
extern VOID IssRfmRadioANCltScanDtlPageSet (tHttp * pHttp);

extern VOID IssRfmRadioANClientInfoPage (tHttp * pHttp);
extern VOID IssRfmRadioANClientInfoPageGet (tHttp * pHttp);
extern VOID IssRfmRadioANClientInfoPageSet (tHttp * pHttp);

extern VOID IssRfmRadioANAPScanInfoPage (tHttp * pHttp);
extern VOID IssRfmRadioANAPScanInfoPageGet (tHttp * pHttp);
extern VOID IssRfmRadioANAPScanInfoPageSet (tHttp * pHttp);

extern VOID IssRfmRadioANAPScanCfgPage (tHttp * pHttp);
extern VOID IssRfmRadioANAPScanCfgPageGet (tHttp * pHttp);
extern VOID IssRfmRadioANAPScanCfgPageSet (tHttp * pHttp);

extern VOID IssRfmRadioANSNRCfgPage (tHttp * pHttp);
extern VOID IssRfmRadioANSNRCfgPageGet (tHttp * pHttp);
extern VOID IssRfmRadioANSNRCfgPageSet (tHttp * pHttp);

extern VOID IssRfmRadioBGNNbrAPDtlPage (tHttp * pHttp);
extern VOID IssRfmRadioBGNNbrAPDtlPageGet (tHttp * pHttp);
extern VOID IssRfmRadioBGNNbrAPDtlPageSet (tHttp * pHttp);

extern VOID IssRfmRadioBGNFldAPDtlPage (tHttp * pHttp);
extern VOID IssRfmRadioBGNFldAPDtlPageGet (tHttp * pHttp);
extern VOID IssRfmRadioBGNFldAPDtlPageSet (tHttp * pHttp);

extern VOID IssRfmRadioBGNCltScanDtlPage (tHttp * pHttp);
extern VOID IssRfmRadioBGNCltScanDtlPageGet (tHttp * pHttp);
extern VOID IssRfmRadioBGNCltScanDtlPageSet (tHttp * pHttp);

extern VOID IssRfmRadioBGNClientInfoPage (tHttp * pHttp);
extern VOID IssRfmRadioBGNClientInfoPageGet (tHttp * pHttp);
extern VOID IssRfmRadioBGNClientInfoPageSet (tHttp * pHttp);

extern VOID IssRfmRadioBGNAPScanInfoPage (tHttp * pHttp);
extern VOID IssRfmRadioBGNAPScanInfoPageGet (tHttp * pHttp);
extern VOID IssRfmRadioBGNAPScanInfoPageSet (tHttp * pHttp);

extern VOID IssRfmRadioBGNAPScanCfgPage (tHttp * pHttp);
extern VOID IssRfmRadioBGNAPScanCfgPageGet (tHttp * pHttp);
extern VOID IssRfmRadioBGNAPScanCfgPageSet (tHttp * pHttp);

extern VOID IssRfmRadioBGNSNRCfgPage (tHttp * pHttp);
extern VOID IssRfmRadioBGNSNRCfgPageGet (tHttp * pHttp);
extern VOID IssRfmRadioBGNSNRCfgPageSet (tHttp * pHttp);

extern VOID IssRfmRadioGNbrAPDtlPage (tHttp * pHttp);
extern VOID IssRfmRadioGNbrAPDtlPageGet (tHttp * pHttp);
extern VOID IssRfmRadioGNbrAPDtlPageSet (tHttp * pHttp);

extern VOID IssRfmRadioGFldAPDtlPage (tHttp * pHttp);
extern VOID IssRfmRadioGFldAPDtlPageGet (tHttp * pHttp);
extern VOID IssRfmRadioGFldAPDtlPageSet (tHttp * pHttp);

extern VOID IssRfmRadioGCltScanDtlPage (tHttp * pHttp);
extern VOID IssRfmRadioGCltScanDtlPageGet (tHttp * pHttp);
extern VOID IssRfmRadioGCltScanDtlPageSet (tHttp * pHttp);

extern VOID IssRfmRadioGClientInfoPage (tHttp * pHttp);
extern VOID IssRfmRadioGClientInfoPageGet (tHttp * pHttp);
extern VOID IssRfmRadioGClientInfoPageSet (tHttp * pHttp);

extern VOID IssRfmRadioGAPScanInfoPage (tHttp * pHttp);
extern VOID IssRfmRadioGAPScanInfoPageGet (tHttp * pHttp);
extern VOID IssRfmRadioGAPScanInfoPageSet (tHttp * pHttp);

extern VOID IssRfmRadioGAPScanCfgPage (tHttp * pHttp);
extern VOID IssRfmRadioGAPScanCfgPageGet (tHttp * pHttp);
extern VOID IssRfmRadioGAPScanCfgPageSet (tHttp * pHttp);

extern VOID IssRfmRadioGSNRCfgPage (tHttp * pHttp);
extern VOID IssRfmRadioGSNRCfgPageGet (tHttp * pHttp);
extern VOID IssRfmRadioGSNRCfgPageSet (tHttp * pHttp);

extern VOID IssRfmRadioBGNDcaPage (tHttp * pHttp);
extern VOID IssRfmRadioBGNDcaPageGet (tHttp * pHttp);
extern VOID IssRfmRadioBGNDcaPageSet (tHttp * pHttp);

extern VOID IssRfmRadioANDcaPage (tHttp * pHttp);
extern VOID IssRfmRadioANDcaPageGet (tHttp * pHttp);
extern VOID IssRfmRadioANDcaPageSet (tHttp * pHttp);

extern VOID IssRfmRadioGDcaPage (tHttp * pHttp);
extern VOID IssRfmRadioGDcaPageGet (tHttp * pHttp);
extern VOID IssRfmRadioGDcaPageSet (tHttp * pHttp);

extern VOID IssRfmGlobalPage (tHttp * pHttp);
extern VOID IssRfmGlobalPageGet (tHttp * pHttp);
extern VOID IssRfmGlobalPageSet (tHttp * pHttp);

extern VOID IssRfmTpcInfoPage (tHttp * pHttp);
extern VOID IssRfmTpcInfoPageGet (tHttp * pHttp);
extern VOID IssRfmTpcInfoPageSet (tHttp * pHttp);

extern VOID IssRfmTpcRequestPage (tHttp * pHttp);
extern VOID IssRfmTpcRequestPageGet (tHttp * pHttp);
extern VOID IssRfmTpcRequestPageSet (tHttp * pHttp);



#ifdef WLC_WANTED
#ifdef ROGUEAP_WANTED
extern VOID IssRfmRogueBasicPage (tHttp * pHttp);
extern VOID IssRfmRogueBasicPageGet( tHttp * pHttp);
extern VOID IssRfmRogueBasicPageSet( tHttp * pHttp);

extern VOID IssRfmRogueRulePage (tHttp * pHttp);
extern VOID IssRfmRogueRulePageGet (tHttp * pHttp);
extern VOID IssRfmRogueRulePageSet (tHttp * pHttp);

extern VOID IssRfmRogueFriendPage (tHttp * pHttp);
extern VOID IssRfmRogueFriendPageGet (tHttp * pHttp);
extern VOID IssRfmRogueFriendPageSet (tHttp * pHttp);

extern VOID IssRfmRogueMaliciousPage (tHttp * pHttp);
extern VOID IssRfmRogueMaliciousPageGet (tHttp * pHttp);
extern VOID IssRfmRogueMaliciousPageSet (tHttp * pHttp);

extern VOID IssRfmRogueUnclassifyPage (tHttp * pHttp);
extern VOID IssRfmRogueUnclassifyPageGet (tHttp * pHttp);
extern VOID IssRfmRogueUnclassifyPageSet (tHttp * pHttp);

extern VOID IssRfmRogueDetailedSummaryPage (tHttp * pHttp);
extern VOID IssRfmRogueDetailedSummaryPageGet (tHttp * pHttp);
extern VOID IssRfmRogueDetailedSummaryPageSet (tHttp * pHttp);
#endif 
#endif 
#endif
/* Prototypes for specific pages processing */
VOID IssSendResponse (tHttp * pHttp, INT1 *pi1Response);
INT4 IssAddSlotLinksToPage(tHttp *pHttp);
INT4 IssAddLinksToPage(tHttp *pHttp);
INT4 IssAddLPMappingLinksToPage(tHttp *pHttp);
VOID IssProcessOspfAreaPage (tHttp *pHttp);
VOID IssProcessLedPage (tHttp *pHttp);
VOID IssPrintLedPage (tHttp * pHttp, UINT4  u4StartCount, UINT4  u4EndCount,
                     UINT1*  pu1IntfUp);
VOID IssProcessLoopBackSettingsPage(tHttp *pHttp);
VOID IssProcessLoopBackSettingsPageGet (tHttp *pHttp);
VOID IssProcessLoopBackSettingsPageSet (tHttp *pHttp);
INT4 IssLoopBackSettingsSetRequest (tHttp * pHttp, INT4 i4LoopBackId);
VOID IssLoopBackPageDeinit (tHttp * pHttp);
VOID  IssPrintTBParam (tHttp * pHttp);

VOID IssPrintMILedPage (tHttp * pHttp, UINT1* pu1IntfUp);
VOID IssProcessContextPage (tHttp *pHttp);
VOID IssProcessL3ContextPage (tHttp *pHttp);
VOID IssProcessL3ContextInterfaceMappingPage (tHttp *pHttp);
VOID IssProcessContextPageGet (tHttp *pHttp);
VOID IssProcessContextPageSet (tHttp *pHttp);
VOID IssProcessL3ContextPageGet (tHttp *pHttp);
VOID IssProcessL3ContextPageSet (tHttp *pHttp);
#ifdef MEF_WANTED
VOID IssProcessTransModeSelectionPage (tHttp *pHttp);
VOID IssProcessTransModeSelectionPageGet (tHttp *pHttp);
VOID IssProcessTransModeSelectionPageSet (tHttp *pHttp);
VOID IssProcessUniConfPage (tHttp *pHttp);
VOID IssProcessUniConfPageGet (tHttp *pHttp);
VOID IssProcessUniConfPageSet (tHttp *pHttp);
VOID IssProcessEvcConfPage (tHttp *pHttp);
VOID IssProcessEvcConfPageGet (tHttp *pHttp);
VOID IssProcessEvcConfPageSet (tHttp *pHttp);
VOID IssProcessUniCVlanEvcConfPage(tHttp * pHttp);
VOID IssProcessUniCVlanEvcConfPageGet (tHttp * pHttp);
VOID IssProcessUniCVlanEvcConfPageSet (tHttp * pHttp);
VOID IssProcessEvcFilterConfPage(tHttp * pHttp);
VOID IssProcessEvcFilterConfPageGet(tHttp * pHttp);
VOID IssProcessEvcFilterConfPageSet(tHttp * pHttp);
VOID IssProcessMefETreeConfPage(tHttp * pHttp);
VOID IssProcessMefETreeConfPageGet(tHttp * pHttp);
VOID IssProcessMefETreeConfPageSet(tHttp * pHttp);
VOID IssProcessMefFilterConfPage(tHttp * pHttp);
VOID IssProcessMefFilterConfPageGet(tHttp * pHttp);
VOID IssProcessMefFilterConfPageSet(tHttp * pHttp);
VOID IssProcessClassMapConfPage (tHttp * pHttp);
VOID IssProcessClassMapConfPageGet (tHttp * pHttp);
VOID IssProcessClassMapConfPageSet (tHttp * pHttp);
VOID IssProcessClassConfPage (tHttp * pHttp);
VOID IssProcessClassConfPageGet (tHttp * pHttp);
VOID IssProcessClassConfPageSet (tHttp * pHttp);
VOID IssProcessMeterConfPage (tHttp * pHttp);
VOID IssProcessMeterConfPageGet (tHttp * pHttp);
VOID IssProcessMeterConfPageSet (tHttp * pHttp);
VOID IssProcessPolicyMapConfPage (tHttp * pHttp);
VOID IssProcessPolicyMapConfPageGet (tHttp * pHttp);
VOID IssProcessPolicyMapConfPageSet (tHttp * pHttp);
VOID IssProcessMefMepFlConfPage (tHttp * pHttp);
VOID IssProcessMefMepFlConfPageGet (tHttp * pHttp);
VOID IssProcessMefMepFlConfPageSet (tHttp * pHttp);
VOID IssProcessMefMepFdConfPage (tHttp * pHttp);
VOID IssProcessMefMepFdConfPageGet (tHttp * pHttp);
VOID IssProcessMefMepFdConfPageSet (tHttp * pHttp);
VOID IssProcessFdStatsConfPage (tHttp * pHttp);
VOID IssProcessFdStatsConfPageGet (tHttp * pHttp);
VOID IssProcessFdStatsConfPageSet (tHttp * pHttp);
VOID IssProcessFlStatsConfPage (tHttp * pHttp);
VOID IssProcessFlStatsConfPageGet (tHttp * pHttp);
VOID IssProcessFlStatsConfPageSet (tHttp * pHttp);
VOID IssProcessMefMepAvailabilityConfPage (tHttp * pHttp);
VOID IssProcessMefMepAvailabilityConfPageGet (tHttp * pHttp);
VOID IssProcessMefMepAvailabilityConfPageSet (tHttp * pHttp);
VOID IssProcessUniListConfPage (tHttp * pHttp);
VOID IssProcessUniListConfPageGet (tHttp * pHttp);
VOID IssProcessUniListConfPageSet (tHttp * pHttp);
#endif
VOID
IssProcessL3ContextInterfaceMappingPageGet (tHttp * pHttp, INT4 i4IsSwitchSelect,
                                            UINT1 * au1VcAliasName, 
                                            UINT1 * au1IfAliasName);
VOID IssProcessL3ContextInterfaceMappingPageSet (tHttp *pHttp);
VOID IssPrintAvailableContexts (tHttp *pHttp);
VOID IssPrintAvailableContextNames (tHttp *pHttp);
VOID IssPrintAvailableContextID (tHttp *pHttp);
VOID IssProcessMiPortCreationPage  (tHttp *pHttp);
VOID IssProcessMiPortCreationPageSet (tHttp *pHttp);
VOID IssProcessAcMappingPage (tHttp *pHttp);
VOID IssProcessAcMappingPageGet (tHttp *pHttp);
VOID IssProcessAcMappingPageSet (tHttp *pHttp);
VOID IssPrintAvailableSlots (tHttp *pHttp);
VOID IssPrintAvailableUni (tHttp * pHttp);
VOID IssPrintAvailableEvc (tHttp * pHttp);
VOID IssPrintAvailableEvcId (tHttp * pHttp);
VOID IssPrintAvailableFilterId (tHttp * pHttp);
VOID IssPrintAvailableMefClass (tHttp * pHttp);
VOID IssPrintAvailableMeterId (tHttp * pHttp);
VOID IssPrintAvailableMefMep(tHttp * pHttp,INT4 );
INT4 IssGetStringPort (UINT1 *pOctet, INT4 i4OctetLen, UINT1 *pu1Temp);
   
VOID IssPrintVcmAvailableContextId (tHttp * pHttp);

#ifdef ISS_WANTED
VOID IssProcessMirrCtrlExtPage (tHttp *pHttp);
VOID IssProcessMirrCtrlExtPageGet (tHttp *pHttp);
VOID IssProcessMirrCtrlExtPageSet (tHttp *pHttp);
VOID MirrWebMemRelease PROTO ((INT4 u4RelCount, ...));
#endif

#ifdef VCM_WANTED
VOID IssProcessMiPortMapPage (tHttp *pHttp);
VOID IssProcessMiPortMapPageSet (tHttp *pHttp);
#endif
#ifdef ERPS_WANTED
VOID
IssProcessErpsRingStatusPageGet (tHttp * pHttp);
VOID
IssProcessErpsRingStatusPage (tHttp * pHttp);
VOID
IssProcessErpsRingTablePage (tHttp * pHttp);
VOID
IssProcessErpsRingTablePageGet (tHttp * pHttp);
VOID
IssProcessErpsRingTablePageSet (tHttp * pHttp);
VOID
IssProcessErpsRingTcPropPage (tHttp * pHttp);
VOID
IssProcessErpsRingTcPropPageGet (tHttp * pHttp);
INT4
IssProcessErpsRingTcPropTable (tHttp *pHttp, UINT4 u4ContextId,
                               UINT4 u4RingId);
VOID
IssProcessErpsRingTcPropPageSet (tHttp * pHttp);
VOID
IssProcessErpsRingVlanPage (tHttp * pHttp);
VOID
IssProcessErpsRingVlanPageGet (tHttp * pHttp);
VOID
IssProcessErpsRingVlanPageSet (tHttp * pHttp);

INT4
ErpsDelAllTopologyList (UINT4 u4ContextId,
                           UINT4 u4RingId);
VOID
IssProcessErpsRingTcPropTableGet (tHttp *pHttp, UINT4 u4ContextId,
                                  UINT4 u4RingId);
INT4
IssProcessErpsRingTcPropTableSet (UINT4 u4ContextId,
                                  UINT4 u4RingId, UINT4 *pu4TcProgList);
VOID 
IssProcessErpsRingVlanTableGet(tHttp *pHttp,UINT1 *pu1VlanList);

INT4
IssProcessErpsRingVlanTableSet (UINT4 u4ContextId,
    INT4 i4VlanGroupId,UINT1 *pu1VlanList);
INT4
IssProcessErpsRingVlanTableDelete (UINT4 u4ContextId,
    INT4 i4VlanGroupId,UINT1 *pu1VlanList);
INT4
IssProcessErpsRingVlanTableModify (UINT4 u4ContextId,
    INT4 i4VlanGroupId,UINT1 *pu1VlanList);
#endif
#ifdef TACACS_WANTED
VOID IssProcessTacacsServerPage (tHttp *pHttp);
VOID IssProcessTacacsServerPageGet (tHttp *pHttp);
VOID IssProcessTacacsServerPageSet (tHttp *pHttp);
VOID IssProcessTacacsActiveServerPage (tHttp *pHttp);
VOID IssProcessTacacsActiveServerPageGet (tHttp *pHttp);
VOID IssProcessTacacsActiveServerPageSet (tHttp *pHttp);
#endif
#ifdef ISS_METRO_WANTED
VOID IssProcessMiBridgeModeSelectionPage (tHttp *pHttp);
VOID IssProcessMiBridgeModeSelectionPageGet (tHttp *pHttp);
VOID IssProcessMiBridgeModeSelectionPageSet (tHttp *pHttp);
#endif
#ifdef PB_WANTED

VOID IssProcessPbRstpPortInfoPage (tHttp * pHttp);
VOID IssProcessPbRstpPortStatsPage (tHttp *pHttp);
VOID IssProcessPbEtherTypePage (tHttp *pHttp);
VOID IssProcessPbEtherTypePageGet (tHttp *pHttp);
VOID IssProcessPbEtherTypePageSet (tHttp *pHttp);
VOID IssProcessPbPortConfPage (tHttp *pHttp);
VOID IssProcessPbPortConfPageGet (tHttp *pHttp);
VOID IssProcessPbPortConfPageSet (tHttp *pHttp);
VOID IssProcessPbServTypeConfPage (tHttp *pHttp);
VOID IssProcessPbServTypeConfPageGet (tHttp *pHttp);
VOID IssProcessPbServTypeConfPageSet (tHttp *pHttp);
VOID IssProcessPbvlanPcpEncodingPage(tHttp *pHttp);
VOID IssProcessPbvlanPcpEncodingPageGet(tHttp *pHttp);
VOID IssProcessPbvlanPcpEncodingPageSet(tHttp *pHttp);

VOID IssProcessPbvlanPcpDecodingPage(tHttp *pHttp);
VOID IssProcessPbvlanPcpDecodingPageGet(tHttp *pHttp);
VOID IssProcessPbvlanPcpDecodingPageSet(tHttp *pHttp);

VOID IssProcessPbvlanPriorityRegenPage(tHttp *pHttp);
VOID IssProcessPbvlanPriorityRegenPageGet(tHttp *pHttp ,INT4 ,UINT4);
VOID IssProcessPbvlanPriorityRegenPageSet(tHttp *pHttp);

VOID IssPrintInternalCNPs (tHttp * pHttp);

VOID IssProcessPbTunnelStatusConfigPage (tHttp *pHttp);
VOID IssProcessPbTunnelStatusConfigPageGet (tHttp *pHttp);
VOID IssProcessPbTunnelStatusConfigPageSet (tHttp *pHttp);

VOID IssProcessPbvlanVidTransPage (tHttp *pHttp);
VOID IssPbProcessPbvlanVidTransPageGet (tHttp *pHttp);

VOID IssProcessPbvlanCvidRegiPage (tHttp *pHttp);
VOID IssPbProcessPbvlanCvidRegiPageGet (tHttp *pHttp);

VOID IssProcessCosPreservationPage (tHttp *pHttp);
VOID IssProcessCosPreservationPageGet (tHttp *pHttp);
VOID IssProcessCosPreservationPageSet (tHttp *pHttp);

#endif/* ISS_METRO_WANTED */

VOID IssProcessVlanFwdPage (tHttp * pHttp);
VOID IssProcessVlanFwdPageSet (tHttp * pHttp);
VOID IssProcessVlanFwdPageGet (tHttp * pHttp);
VOID IssProcessVlanCapabilitiesPage (tHttp * pHttp);
VOID IssVlanPageDeinit (tHttp *pHttp);
VOID IssProcessPortMonitoringPage (tHttp *pHttp);       
VOID IssProcessTrafficClassPage (tHttp *pHttp);       
VOID IssProcessL2MulticastFilterPage (tHttp *pHttp);       
VOID IssProcessL2UnicastFilterPage (tHttp *pHttp);       
VOID IssProcessVlanInterfaceSettingsPage (tHttp *pHttp);       
VOID IssProcessStaticVlanPage (tHttp *pHttp);       
VOID IssProcessIgmpPage (tHttp *pHttp);
VOID IssProcessMldPage (tHttp *pHttp);
VOID IssProcessIgpUpIfPage (tHttp *pHttp);
VOID IssProcessIgmpRouteConfPage (tHttp *pHttp);
VOID IssProcessIgmpMembershipPage (tHttp *pHttp);
VOID IssProcessIgmpGroupListConfPage (tHttp *pHttp);
VOID IssProcessPimSRpPage (tHttp *pHttp);
VOID IssProcessPimCrpPage (tHttp *pHttp);
VOID IssProcessPimMRoutePage (tHttp * pHttp);
VOID IssProcessPimElectedRPPage (tHttp * pHttp);
VOID IssProcessPimDFInfoPage (tHttp * pHttp);
VOID IssProcessPimIfacePage (tHttp *pHttp);
VOID IssProcessPimComponentPage (tHttp * pHttp);
VOID IssProcessOspfGlobalConfPage (tHttp *pHttp);
VOID IssProcessOspfIfacePage (tHttp *pHttp);
VOID IssProcessOspfVirtualIfacePage (tHttp *pHttp);
VOID IssProcessOspfNeighborPage (tHttp *pHttp);
VOID IssProcessOspfRRDRoutePage (tHttp *pHttp);
VOID IssProcessOspfAreaAggrPage (tHttp *pHttp);
VOID IssProcessOspfExtAggrPage (tHttp *pHttp);
VOID IssProcessOspfRouteStatsPage (tHttp *pHttp);

VOID
IssProcessOspfRedPageGet (tHttp * pHttp);
VOID
IssProcessOspfRedStatsPage (tHttp * pHttp);
VOID IssProcessOspfLsdbPage (tHttp *pHttp);
VOID IssProcessOspfGRPage (tHttp *pHttp);
VOID IssProcessSnmpHomePage (tHttp *pHttp);
VOID IssProcessSnmpAgentxPage (tHttp *pHttp);
VOID IssProcessIgsPage (tHttp *pHttp);
VOID IssProcessIpSettingsPage (tHttp *pHttp);
VOID IssProcessSpecificPage PROTO((tHttp *pHttp));
INT1 IssConstructPostQueryMsg PROTO ((tHttp *pHttp, UINT1 *pu1FormattedMsg));
VOID IssProcessBottomPage(tHttp *pHttp);
VOID IssProcessObjTreePage(tHttp *pHttp);
VOID IssProcessSendTitle(tHttp *pHttp);
VOID IssProcessL2Page(tHttp *pHttp);
VOID IssProcessL3Page(tHttp *pHttp);
VOID IssProcessNetOverlaypage(tHttp *pHttp);
VOID IssProcessSecurityPage(tHttp *pHttp);
VOID IssProcessMulticastPage(tHttp *pHttp);
VOID IssProcessRRDGlobalFrame(tHttp *pHttp);
VOID IssProcessRRDBgpFrame(tHttp *pHttp);
VOID IssProcessRRDRipFrame(tHttp *pHttp);
VOID IssProcessRRDOspfFrame(tHttp *pHttp);
VOID IssProcessGlobalPage(tHttp *pHttp);
VOID IssProcessGenInfoPage(tHttp *pHttp);
VOID IssProcessStaticVlanPageGet (tHttp *pHttp);       
VOID IssProcessStaticVlanPageSet (tHttp *pHttp);
VOID SecPrintIpInterfaces (tHttp * pHttp, UINT4 u4BitMap,
                           UINT1 u1NetwokType, UINT1 u1WanType);
INT4 VlanWebTestModificationInfo (tHttp *pHttp, UINT4 u4VlanId,
        tSNMP_OCTET_STRING_TYPE * pEgressPorts,
        tSNMP_OCTET_STRING_TYPE * pUntaggedPorts,
        tSNMP_OCTET_STRING_TYPE * pForbiddenPorts);

INT4 IssPortStringParser(UINT1 * pIfPort , UINT1 *pPortList);
INT4 IssGetIfPortList (UINT1 * pIfPort ,UINT1 *pPortList);
INT4 IssPortListStringParser(UINT1 * pIfPort , UINT1 *pPortList);
INT4 IssGetAllowedPortList (UINT1 * pIfPort ,UINT1 *pPortList);
INT4 IssGetIfIndex (UINT1 * pIfPort ,UINT4 * pIfIndex);
VOID IssConvertArrayToPortList (UINT2 au2PortArray[], tPortList PortList,
                            INT4 i4Length);
VOID IssConvertLocalPortListToArray (tLocalPortList LocalPortList,
                                 UINT4 *pu4IfPortArray, UINT4 *u4NumPorts);
                    

 
VOID IssProcessVlanInterfaceSettingsPageGet (tHttp *pHttp);       
VOID IssProcessVlanInterfaceSettingsPageSet (tHttp *pHttp);      
VOID IssProcessWildCardSettingsPage (tHttp *pHttp);
VOID IssProcessWildCardSettingsPageGet (tHttp *pHttp);
VOID IssProcessWildCardSettingsPageSet (tHttp *pHttp);

#ifdef PNAC_WANTED 
VOID IssProcessPnacLocalAsPage (tHttp *pHttp);       
VOID IssProcessPnacLocalAsPageGet (tHttp *pHttp);       
VOID IssProcessPnacLocalAsPageSet (tHttp *pHttp);       
VOID IssProcessPnacMacSessionInfoPage (tHttp *pHttp);       
VOID IssProcessPnacMacSessionInfoPageGet (tHttp *pHttp);       
VOID IssProcessPnacMacSessionInfoPageSet (tHttp *pHttp);
VOID IssProcessPnacMacSessionStatsPage (tHttp *pHttp);       
VOID IssProcessPnacMacSessionStatsPageGet (tHttp *pHttp);       
VOID IssProcessPnacRadiusPage (tHttp * pHttp);
VOID IssProcessPnacRadiusPageGet (tHttp *pHttp);
VOID IssProcessPnacRadiusPageSet (tHttp *pHttp);
VOID IssProcessPnacRadiusStatsPage (tHttp *pHttp);
VOID IssProcessPnacRadiusStatsPageGet (tHttp *pHttp);
VOID IssProcessPnacRadiusStatsPageSet (tHttp *pHttp);
 
#endif

#ifdef MRP_WANTED 
VOID IssProcessMrpApplicationPage (tHttp *pHttp);       
VOID IssProcessMrpApplicationPageSet (tHttp *pHttp);
VOID IssProcessMrpClearStatConfigPage (tHttp *pHttp);
VOID IssProcessMrpClearStatConfigPageSet (tHttp *pHttp);
       
#endif

#ifdef DHCP6_CLNT_WANTED 
VOID IssProcessDhcp6ClientGlobalPage (tHttp *pHttp);       
VOID IssProcessDhcp6ClientGlobalPageGet (tHttp *pHttp);       
VOID IssProcessDhcp6ClientGlobalPageSet (tHttp *pHttp);       
VOID IssProcessDhcp6ClientIfPage (tHttp *pHttp);       
VOID IssProcessDhcp6ClientIfPageGet (tHttp *pHttp);       
VOID IssProcessDhcp6ClientIfPageSet (tHttp *pHttp);       
VOID IssProcessDhcp6ClientOptionPage (tHttp *pHttp);       
VOID IssProcessDhcp6ClientOptionPageGet (tHttp *pHttp);       
#endif

#ifdef DHCP6_RLY_WANTED 
VOID IssProcessDhcp6RelayGlobalPage (tHttp *pHttp);       
VOID IssProcessDhcp6RelayGlobalPageGet (tHttp *pHttp);       
VOID IssProcessDhcp6RelayGlobalPageSet (tHttp *pHttp);       
VOID IssProcessDhcp6RelayIfPage (tHttp *pHttp);       
VOID IssProcessDhcp6RelayIfPageGet (tHttp *pHttp);       
VOID IssProcessDhcp6RelayIfPageSet (tHttp *pHttp);       
VOID IssProcessDhcp6RelaySrvAddrPage (tHttp *pHttp);       
VOID IssProcessDhcp6RelaySrvAddrPageGet (tHttp *pHttp);       
VOID IssProcessDhcp6RelaySrvAddrPageSet (tHttp *pHttp);       
VOID IssProcessDhcp6RelayOutIfPage (tHttp *pHttp);       
VOID IssProcessDhcp6RelayOutIfPageGet (tHttp *pHttp);       
VOID IssProcessDhcp6RelayOutIfPageSet (tHttp *pHttp);       
#endif

#ifdef DHCP6_SRV_WANTED 
VOID IssPrintAvailablePoolID (tHttp *pHttp);       
VOID IssPrintAvailableOptionID (tHttp *pHttp, UINT4, UINT4);       
VOID IssPrintAvailableRealmIndex (tHttp *pHttp);       

VOID IssProcessDhcp6ServerGlobalPage (tHttp *pHttp);       
VOID IssProcessDhcp6ServerGlobalPageGet (tHttp *pHttp);       
VOID IssProcessDhcp6ServerGlobalPageSet (tHttp *pHttp);       
VOID IssProcessDhcp6ServerPoolPage (tHttp *pHttp);       
VOID IssProcessDhcp6ServerPoolPageGet (tHttp *pHttp);       
VOID IssProcessDhcp6ServerPoolPageSet (tHttp *pHttp);       
VOID IssProcessDhcp6ServerClientPage (tHttp *pHttp);       
VOID IssProcessDhcp6ServerClientPageGet (tHttp *pHttp);       
VOID IssProcessDhcp6ServerClientPageSet (tHttp *pHttp);       
VOID IssProcessDhcp6ServerRealmPage (tHttp *pHttp);       
VOID IssProcessDhcp6ServerRealmPageGet (tHttp *pHttp);       
VOID IssProcessDhcp6ServerRealmPageSet (tHttp *pHttp);       
VOID IssProcessDhcp6ServerKeyPage (tHttp *pHttp);       
VOID IssProcessDhcp6ServerKeyPageGet (tHttp *pHttp);       
VOID IssProcessDhcp6ServerKeyPageSet (tHttp *pHttp);       
VOID IssProcessDhcp6ServerIfPage (tHttp *pHttp);       
VOID IssProcessDhcp6ServerIfPageGet (tHttp *pHttp);       
VOID IssProcessDhcp6ServerIfPageSet (tHttp *pHttp);       
VOID IssProcessDhcp6ServerPrefixPage (tHttp *pHttp);       
VOID IssProcessDhcp6ServerPrefixPageGet (tHttp *pHttp);       
VOID IssProcessDhcp6ServerPrefixPageSet (tHttp *pHttp);       
VOID IssProcessDhcp6ServerOptionPage (tHttp *pHttp);       
VOID IssProcessDhcp6ServerOptionPageGet (tHttp *pHttp);       
VOID IssProcessDhcp6ServerOptionPageSet (tHttp *pHttp);       
VOID IssProcessDhcp6ServerSubOptionPage (tHttp *pHttp);       
VOID IssProcessDhcp6ServerSubOptionPageGet (tHttp *pHttp);       
VOID IssProcessDhcp6ServerSubOptionPageSet (tHttp *pHttp);       
#endif

#ifdef NAT_WANTED
VOID IssProcessNatConfPage (tHttp * pHttp);
VOID IssProcessNatConfPageGet (tHttp * pHttp);
VOID IssProcessNatConfPageSet (tHttp * pHttp);
VOID IssProcessNatStaticPage (tHttp * pHttp);
VOID IssProcessNatStaticPageGet (tHttp * pHttp);
VOID IssProcessNatStaticPageSet (tHttp * pHttp);
VOID IssProcessNatDynamicPage (tHttp * pHttp);
VOID IssProcessNatDynamicPageGet (tHttp * pHttp);
VOID IssProcessNatDynamicPageSet (tHttp * pHttp);
VOID IssProcessNatVSPage (tHttp * pHttp);
VOID IssProcessNatVSPageGet (tHttp * pHttp);
VOID IssProcessNatVSPageSet (tHttp * pHttp);
VOID IssProcessNatTranslationPage (tHttp * pHttp);
#endif /* NAT_WANTED */
#ifdef DNS_WANTED
VOID IssProcessDnsNameServerPage (tHttp * pHttp);
VOID IssProcessDnsNameServerPageGet (tHttp * pHttp);
VOID IssProcessDnsNameServerPageSet (tHttp * pHttp);
VOID IssProcessDnsCachePage (tHttp * pHttp);
VOID IssProcessDnsCachePageGet (tHttp * pHttp);
VOID IssProcessDnsCachePageSet (tHttp * pHttp);
#endif
#ifdef BFD_WANTED
VOID IssProcessBfdIpv6ConfPage (tHttp * pHttp);
VOID IssProcessBfdIpv6ConfPageGet (tHttp * pHttp);
VOID IssProcessBfdIpv6ConfPageSet (tHttp * pHttp);
#endif
#ifdef ISIS_WANTED
VOID IssProcessIsisInstConfPage (tHttp * pHttp);
VOID IssProcessIsisInstConfPageGet (tHttp * pHttp);
VOID IssProcessIsisInstConfPageSet (tHttp * pHttp);
VOID IssProcessIsisMAAConfPage (tHttp * pHttp);
VOID IssProcessIsisMAAConfPageGet (tHttp * pHttp);
VOID IssProcessIsisMAAConfPageSet (tHttp * pHttp);
VOID IssProcessIsisSummAddrConfPage (tHttp * pHttp);
VOID IssProcessIsisSAddrConfPageGet (tHttp * pHttp);
VOID IssProcessIsisSAddrConfPageSet (tHttp * pHttp);
VOID IssProcessIsisIPRAConfPage (tHttp * pHttp);
VOID IssProcessIsisIPRAConfPageGet (tHttp * pHttp);
VOID IssProcessIsisIPRAConfPageSet (tHttp * pHttp);
VOID IssProcessIsisPktCntPageGet (tHttp * pHttp);
VOID IssProcessIsisAdjPageGet (tHttp * pHttp);
VOID IssProcessIsisCircConfPage (tHttp * pHttp);
VOID IssProcessIsisCircConfPageGet (tHttp * pHttp);
VOID IssProcessIsisCircConfPageSet (tHttp * pHttp);
VOID IssProcessIsisAAddrConfPageGet (tHttp * pHttp);
VOID IsisPrintIp6Addr (UINT1 *pu1IpAddr, UINT1 *pu1IpAddrBuffer);
UINT4
IsisOctetLen (UINT1 *pu1DotStr);
VOID
IsisDotStrToStr (UINT1 *pu1DotStr, UINT1 *pu1Val);
VOID IssProcessIsisGRPage (tHttp *pHttp);
VOID IssProcessIsisGRPageGet (tHttp *pHttp);
VOID IssProcessIsisGRPageSet (tHttp *pHttp);
#ifdef ROUTEMAP_WANTED
VOID IssPrintAvailableIsisContextId (tHttp * pHttp);
#endif
#endif/* ISIS_WANTED */

#ifdef WLC_WANTED
VOID IssProcessCapwapGlobalPage (tHttp *pHttp);       
VOID IssProcessCapwapGlobalPageGet (tHttp *pHttp);       
VOID IssProcessCapwapGlobalPageSet (tHttp *pHttp);       

VOID IssProcessCapwapTracesPage (tHttp *pHttp);       
VOID IssProcessCapwapTracesPageGet (tHttp *pHttp);       
VOID IssProcessCapwapTracesPageSet (tHttp *pHttp);       


VOID IssProcessCapwapWhitelistPage (tHttp *pHttp);       
VOID IssProcessCapwapWhitelistPageGet (tHttp *pHttp);       
VOID IssProcessCapwapWhitelistPageSet (tHttp *pHttp);       

VOID IssProcessCapwapStationWhitelistPage (tHttp *pHttp);       
VOID IssProcessCapwapStationWhitelistPageGet (tHttp *pHttp);       
VOID IssProcessCapwapStationWhitelistPageSet (tHttp *pHttp);       

VOID IssProcessCapwapStationBlacklistPage (tHttp *pHttp);       
VOID IssProcessCapwapStationBlacklistPageGet (tHttp *pHttp);       
VOID IssProcessCapwapStationBlacklistPageSet (tHttp *pHttp);       

VOID IssProcessCapwapBlacklistPage (tHttp *pHttp);       
VOID IssProcessCapwapBlacklistPageGet (tHttp *pHttp);       
VOID IssProcessCapwapBlacklistPageSet (tHttp *pHttp);       

VOID IssProcessCapwapDtlsPage (tHttp *pHttp);       
VOID IssProcessCapwapDtlsPageGet (tHttp *pHttp);       
VOID IssProcessCapwapDtlsPageSet (tHttp *pHttp);       

VOID IssProcessCapwapTimerPage (tHttp *pHttp);       
VOID IssProcessCapwapTimerPageGet (tHttp *pHttp);       
VOID IssProcessCapwapTimerPageSet (tHttp *pHttp);       

VOID IssProcessRadioANTablePage (tHttp * pHttp);
VOID IssProcessRadioANTablePageGet (tHttp * pHttp);
VOID IssProcessRadioANTablePageSet (tHttp * pHttp);

VOID IssProcessGenRadioANConfigTablePage (tHttp * pHttp);
VOID IssProcessGenRadioANConfigTablePageGet (tHttp * pHttp);
VOID IssProcessGenRadioANConfigTablePageSet (tHttp * pHttp);

VOID IssProcessAdvRadioANConfigTablePage (tHttp * pHttp);
VOID IssProcessAdvRadioANConfigTablePageGet (tHttp * pHttp);
VOID IssProcessAdvRadioANConfigTablePageSet (tHttp * pHttp);

VOID IssProcessHTRadioANConfigTablePage (tHttp * pHttp);
VOID IssProcessHTRadioANConfigTablePageGet (tHttp * pHttp);
VOID IssProcessHTRadioANConfigTablePageSet (tHttp * pHttp);

VOID IssProcessHTRadioACConfigTablePage (tHttp * pHttp);
VOID IssProcessHTRadioACConfigTablePageGet (tHttp * pHttp);
VOID IssProcessHTRadioACConfigTablePageSet (tHttp * pHttp);

VOID IssProcessBeamFormingRadioANConfigTablePage (tHttp * pHttp);
VOID IssProcessBeamFormingRadioANConfigTablePageGet (tHttp * pHttp);
VOID IssProcessBeamFormingRadioANConfigTablePageSet (tHttp * pHttp);

VOID IssProcessHTOperationRadioANConfigTablePage (tHttp * pHttp);
VOID IssProcessHTOperationRadioANConfigTablePageGet (tHttp * pHttp);
VOID IssProcessHTOperationRadioANConfigTablePageSet (tHttp * pHttp);

VOID IssProcessRadioACTablePage (tHttp * pHttp);
VOID IssProcessRadioACTablePageGet (tHttp * pHttp);
VOID IssProcessRadioACTablePageSet (tHttp * pHttp);

VOID IssProcessGenRadioACConfigTablePage (tHttp * pHttp);
VOID IssProcessGenRadioACConfigTablePageGet (tHttp * pHttp);
VOID IssProcessGenRadioACConfigTablePageSet (tHttp * pHttp);

VOID IssProcessAdvRadioACConfigTablePage (tHttp * pHttp);
VOID IssProcessAdvRadioACConfigTablePageGet (tHttp * pHttp);
VOID IssProcessAdvRadioACConfigTablePageSet (tHttp * pHttp);

VOID IssProcessVHTOperConfigTablePage (tHttp * pHttp);
VOID IssProcessVHTOperConfigTablePageGet (tHttp * pHttp);
VOID IssProcessVHTOperConfigTablePageSet (tHttp * pHttp);

VOID IssProcessVHTCapabConfigTablePage (tHttp * pHttp);
VOID IssProcessVHTCapabConfigTablePageGet (tHttp * pHttp);
VOID IssProcessVHTCapabConfigTablePageSet (tHttp * pHttp);

VOID IssProcessVHTInfoConfigTablePage (tHttp * pHttp);
VOID IssProcessVHTInfoConfigTablePageGet (tHttp * pHttp);
VOID IssProcessVHTInfoConfigTablePageSet (tHttp * pHttp);

VOID IssProcessRadioANFilter (tHttp * pHttp);
VOID IssProcessRadioANFilterGet (tHttp * pHttp);
VOID IssProcessRadioANFilterSet (tHttp * pHttp);

VOID IssChangeRadioANFilter (tHttp * pHttp);
VOID IssChangeRadioANFilterGet (tHttp * pHttp);
VOID IssChangeRadioANFilterSet (tHttp * pHttp);

VOID IssProcessRadioBGNTablePage (tHttp * pHttp);
VOID IssProcessRadioBGNTablePageGet (tHttp * pHttp);
VOID IssProcessRadioBGNTablePageSet (tHttp * pHttp);

VOID IssProcessGenRadioBGNConfigTablePage (tHttp * pHttp);
VOID IssProcessGenRadioBGNConfigTablePageGet (tHttp * pHttp);
VOID IssProcessGenRadioBGNConfigTablePageSet (tHttp * pHttp);

VOID IssProcessAdvRadioBGNConfigTablePage (tHttp * pHttp);
VOID IssProcessAdvRadioBGNConfigTablePageGet (tHttp * pHttp);
VOID IssProcessAdvRadioBGNConfigTablePageSet (tHttp * pHttp);

VOID IssProcessRadioBGNFilter (tHttp * pHttp);
VOID IssProcessRadioBGNFilterGet (tHttp * pHttp);
VOID IssProcessRadioBGNFilterSet (tHttp * pHttp);

VOID IssProcessHTCapabilitiesRadioBGNConfigTablePage (tHttp * pHttp);
VOID IssProcessHTCapabilitiesRadioBGNConfigTablePageGet (tHttp * pHttp);
VOID IssProcessHTCapabilitiesRadioBGNConfigTablePageSet (tHttp * pHttp);

VOID IssProcessBeamFormingRadioBGNConfigTablePage (tHttp * pHttp);
VOID IssProcessBeamFormingRadioBGNConfigTablePageGet (tHttp * pHttp);
VOID IssProcessBeamFormingRadioBGNConfigTablePageSet (tHttp * pHttp);

VOID IssProcessHTOperationRadioBGNConfigTablePage (tHttp * pHttp);
VOID IssProcessHTOperationRadioBGNConfigTablePageGet (tHttp * pHttp);
VOID IssProcessHTOperationRadioBGNConfigTablePageSet (tHttp * pHttp);

VOID IssProcessHTOperationRadioACConfigTablePage (tHttp * pHttp);
VOID IssProcessHTOperationRadioACConfigTablePageGet (tHttp * pHttp);
VOID IssProcessHTOperationRadioACConfigTablePageSet (tHttp * pHttp);

VOID IssProcessRadioACFilter (tHttp * pHttp);
VOID IssProcessRadioACFilterGet (tHttp * pHttp);
VOID IssProcessRadioACFilterSet (tHttp * pHttp);

VOID IssChangeRadioBGNFilter (tHttp * pHttp);
VOID IssChangeRadioBGNFilterGet (tHttp * pHttp);
VOID IssChangeRadioBGNFilterSet (tHttp * pHttp);

VOID IssChangeRadioACFilter (tHttp * pHttp);
VOID IssChangeRadioACFilterGet (tHttp * pHttp);
VOID IssChangeRadioACFilterSet (tHttp * pHttp);

VOID IssProcessConfigEdcaTablePage (tHttp * pHttp);
VOID IssProcessConfigEdcaTablePageSet (tHttp * pHttp);
VOID IssProcessConfigEdcaTablePageGet (tHttp * pHttp);

VOID IssProcessConfigAntennaTablePage (tHttp * pHttp);
VOID IssProcessConfigAntennaTablePageSet (tHttp * pHttp);
VOID IssProcessConfigAntennaTablePageGet (tHttp * pHttp);

VOID IssProcessApCountryTablePage (tHttp * pHttp);
VOID IssProcessApCountryTablePageSet (tHttp * pHttp);
VOID IssProcessApCountryTablePageGet (tHttp * pHttp);

VOID IssProcessCreateApGroupTablePage  (tHttp * pHttp);
VOID IssProcessCreateApGroupTablePageGet (tHttp * pHttp);
VOID IssProcessCreateApGroupTablePageSet (tHttp * pHttp);

VOID IssProcessAddWlanTablePage (tHttp * pHttp);
VOID IssProcessAddWlanTablePageGet (tHttp * pHttp);
VOID IssProcessAddWlanTablePageSet (tHttp * pHttp);

VOID IssProcessAddApTablePage (tHttp * pHttp);
VOID IssProcessAddApTablePageGet (tHttp * pHttp);
VOID IssProcessAddApTablePageSet (tHttp * pHttp);


VOID IssProcessQosProfileTablePage (tHttp * pHttp);
VOID IssProcessQosProfileTablePageSet (tHttp * pHttp);
VOID IssProcessQosProfileTablePageGet (tHttp * pHttp);

VOID IssProcessConfigQosProfileTablePage (tHttp * pHttp);
VOID IssProcessConfigQosProfileTablePageSet (tHttp * pHttp);
VOID IssProcessConfigQosProfileTablePageGet (tHttp * pHttp);

VOID IssProcessConfigQosHomeTablePage (tHttp * pHttp);
VOID IssProcessConfigQosHomeTablePageSet (tHttp * pHttp);
VOID IssProcessConfigQosHomeTablePageGet (tHttp * pHttp);

VOID IssProcessConfigQosBronzeProfile (tHttp * pHttp);
VOID IssProcessConfigQosBronzeProfileSet (tHttp * pHttp);
VOID IssProcessConfigQosBronzeProfileGet (tHttp * pHttp);

VOID IssProcessConfigQosGoldProfile (tHttp * pHttp);
VOID IssProcessConfigQosGoldProfileSet (tHttp * pHttp);
VOID IssProcessConfigQosGoldProfileGet (tHttp * pHttp);

VOID IssProcessConfigQosSilverProfile (tHttp * pHttp);
VOID IssProcessConfigQosSilverProfileSet (tHttp * pHttp);
VOID IssProcessConfigQosSilverProfileGet (tHttp * pHttp);

VOID IssProcessConfigQosPlatinumProfile (tHttp * pHttp);
VOID IssProcessConfigQosPlatinumProfileSet (tHttp * pHttp);
VOID IssProcessConfigQosPlatinumProfileGet (tHttp * pHttp);

VOID IssMonitorRadioANTablePage (tHttp * pHttp);
VOID IssMonitorRadioANTablePageGet (tHttp * pHttp);
VOID IssMonitorRadioANTablePageSet (tHttp * pHttp);

VOID IssMonitorRadioANFilter (tHttp * pHttp);
VOID IssMonitorRadioANFilterGet (tHttp * pHttp);
VOID IssMonitorRadioANFilterSet (tHttp * pHttp);

VOID IssMonitorChangeRadioANFilter (tHttp * pHttp);
VOID IssMonitorChangeRadioANFilterGet (tHttp * pHttp);
VOID IssMonitorChangeRadioANFilterSet (tHttp * pHttp);

VOID IssRadioANStatisticsPage (tHttp * pHttp);
VOID IssRadioANStatisticsPageGet (tHttp * pHttp);
VOID IssRadioANStatisticsPageSet (tHttp * pHttp);

VOID IssMonitorRadioBGNTablePage (tHttp * pHttp);
VOID IssMonitorRadioBGNTablePageGet (tHttp * pHttp);
VOID IssMonitorRadioBGNTablePageSet (tHttp * pHttp);

VOID IssMonitorRadioBGNFilter (tHttp * pHttp);
VOID IssMonitorRadioBGNFilterGet (tHttp * pHttp);
VOID IssMonitorRadioBGNFilterSet (tHttp * pHttp);

VOID IssMonitorChangeRadioBGNFilter (tHttp * pHttp);
VOID IssMonitorChangeRadioBGNFilterGet (tHttp * pHttp);
VOID IssMonitorChangeRadioBGNFilterSet (tHttp * pHttp);

VOID IssRadioBGNStatisticsPage (tHttp * pHttp);
VOID IssRadioBGNStatisticsPageGet (tHttp * pHttp);
VOID IssRadioBGNStatisticsPageSet (tHttp * pHttp);

VOID IssMonitorRadioACTablePage (tHttp * pHttp);
VOID IssMonitorRadioACTablePageGet (tHttp * pHttp);
VOID IssMonitorRadioACTablePageSet (tHttp * pHttp);

VOID IssMonitorRadioACFilter (tHttp * pHttp);
VOID IssMonitorRadioACFilterGet (tHttp * pHttp);
VOID IssMonitorRadioACFilterSet (tHttp * pHttp);

VOID IssMonitorChangeRadioACFilter (tHttp * pHttp);
VOID IssMonitorChangeRadioACFilterGet (tHttp * pHttp);
VOID IssMonitorChangeRadioACFilterSet (tHttp * pHttp);

VOID IssRadioACStatisticsPage (tHttp * pHttp);
VOID IssRadioACStatisticsPageGet (tHttp * pHttp);
VOID IssRadioACStatisticsPageSet (tHttp * pHttp);

VOID IssMonitorClientsPage (tHttp * pHttp);
VOID IssMonitorClientsPageGet (tHttp * pHttp);
VOID IssMonitorClientsPageSet (tHttp * pHttp);

VOID IssMonitorClientsFilterPage (tHttp * pHttp);
VOID IssMonitorClientsFilterPageGet (tHttp * pHttp);
VOID IssMonitorClientsFilterPageSet (tHttp * pHttp);

VOID IssMonitorClientsChangeFilterPage (tHttp * pHttp);
VOID IssMonitorClientsChangeFilterPageGet (tHttp * pHttp);
VOID IssMonitorClientsChangeFilterPageSet (tHttp * pHttp);

VOID IssMonitorClientTablePage (tHttp * pHttp);
VOID IssMonitorClientTablePageGet (tHttp * pHttp);
VOID IssMonitorClientTablePageSet (tHttp * pHttp);

VOID IssClientsStatisticsPage (tHttp * pHttp);
VOID IssClientsStatisticsPageGet (tHttp * pHttp);
VOID IssClientsStatisticsPageSet (tHttp * pHttp);

VOID IssWlanStatisticsPage (tHttp * pHttp);
VOID IssWlanStatisticsPageGet (tHttp * pHttp);
VOID IssWlanStatisticsPageSet (tHttp * pHttp);

VOID IssMonitorMobilityStatsTablePage (tHttp * pHttp);
VOID IssMonitorMobilityStatsTablePageGet (tHttp * pHttp);
VOID IssMonitorMobilityStatsTablePageSet (tHttp * pHttp);

VOID IssApWlanStatisticsPage (tHttp * pHttp);
VOID IssApWlanStatisticsPageGet (tHttp * pHttp);
VOID IssApWlanStatisticsPageSet (tHttp * pHttp);

VOID IssProcessWlanBindingTablePage (tHttp * pHttp);
VOID IssProcessWlanBindingTablePageGet (tHttp * pHttp);
VOID IssProcessWlanBindingTablePageSet (tHttp * pHttp);

VOID IssProcessWlanBindCreateTablePage (tHttp * pHttp);
VOID IssProcessWlanBindCreateTablePageGet (tHttp * pHttp);
VOID IssProcessWlanBindCreateTablePageSet (tHttp * pHttp);

VOID IssProcessWirelessAllApConfigTablePage (tHttp * pHttp);
VOID IssProcessWirelessAllApConfigTablePageGet (tHttp * pHttp);
VOID IssProcessWirelessAllApConfigTablePageSet (tHttp * pHttp);

VOID IssProcessWirelessApAdvEditTablePage (tHttp * pHttp);
VOID IssProcessWirelessApAdvEditTablePageGet (tHttp * pHttp);
VOID IssProcessWirelessApAdvEditTablePageSet (tHttp * pHttp);

VOID IssProcessWirelessApCreateTablePage (tHttp * pHttp);
VOID IssProcessWirelessApCreateTablePageGet (tHttp * pHttp);
VOID IssProcessWirelessApCreateTablePageSet (tHttp * pHttp);

VOID IssProcessWirelessApEditTablePage (tHttp * pHttp);
VOID IssProcessWirelessApEditTablePageGet (tHttp * pHttp);
VOID IssProcessWirelessApEditTablePageSet (tHttp * pHttp);

VOID IssProcessWirelessApConfigTablePage (tHttp * pHttp);
VOID IssProcessWirelessApConfigTablePageGet (tHttp * pHttp);
VOID IssProcessWirelessApConfigTablePageSet (tHttp * pHttp);

VOID IssProcessAPModelEntryPage (tHttp *pHttp);       
VOID IssProcessAPModelEntryPageSet (tHttp *pHttp);       
VOID IssProcessAPModelEntryPageGet (tHttp *pHttp,tSNMP_OCTET_STRING_TYPE *);       

VOID IssProcessAPModelEntryEditPage (tHttp *pHttp);       
VOID IssProcessAPModelEntryEditPageGet (tHttp *pHttp);       
VOID IssProcessAPModelEntryEditPageSet (tHttp *pHttp);       

VOID IssProcessControllerGeneralPage (tHttp *pHttp);       
VOID IssProcessControllerGeneralPageSet (tHttp *pHttp);       
VOID IssProcessControllerGeneralPageGet (tHttp *pHttp);       

VOID IssProcessAPModelTablePage (tHttp *pHttp);       
VOID IssProcessAPModelTablePageGet (tHttp *pHttp);       
VOID IssProcessAPModelTablePageSet (tHttp *pHttp);       

VOID IssProcessRadioTablePage(tHttp *pHttp);       
VOID IssProcessRadioTablePageGet(tHttp *pHttp);       
VOID IssProcessRadioTablePageSet(tHttp *pHttp);       

VOID IssProcessModelBindingTablePage (tHttp * pHttp);
VOID IssProcessModelBindingTablePageGet (tHttp * pHttp);
VOID IssProcessModelBindingTablePageSet (tHttp * pHttp);

VOID IssProcessModelBindCreateTablePage (tHttp * pHttp);
VOID IssProcessModelBindCreateTablePageGet (tHttp * pHttp);
VOID IssProcessModelBindCreateTablePageSet (tHttp * pHttp);

VOID IssProcessWlanTablePage(tHttp *pHttp);       
VOID IssProcessWlanTablePageGet(tHttp *pHttp);       
VOID IssProcessWlanTablePageSet(tHttp *pHttp);       

VOID IssProcessWlanCreatePage(tHttp *pHttp);       
VOID IssProcessWlanCreatePageSet(tHttp *pHttp);       
VOID IssProcessWlanCreatePageGet(tHttp *pHttp);       

VOID IssProcessWlanGeneralPage (tHttp *pHttp);       
VOID IssProcessWlanGeneralPageSet (tHttp *pHttp);       
VOID IssProcessWlanGeneralPageGet (tHttp *pHttp);       

VOID IssProcessWlanCapabilityPage (tHttp *pHttp);       
VOID IssProcessWlanCapabilityPageSet (tHttp *pHttp);       
VOID IssProcessWlanCapabilityPageGet (tHttp *pHttp);       

VOID IssProcessCapabilityProfilePage (tHttp *pHttp);       
VOID IssProcessCapabilityProfilePageSet (tHttp *pHttp);       
VOID IssProcessCapabilityProfilePageGet (tHttp *pHttp);       

VOID IssProcessCapabilityProfileEditPage (tHttp *pHttp);       
VOID IssProcessCapabilityProfileEditPageSet (tHttp *pHttp);       
VOID IssProcessCapabilityProfileEditPageGet (tHttp *pHttp);       

VOID IssProcessCapabilityProfileCreatePage (tHttp *pHttp);       
VOID IssProcessCapabilityProfileCreatePageGet (tHttp *pHttp);       
VOID IssProcessCapabilityProfileCreatePageSet (tHttp *pHttp);       

VOID IssProcessWlanQosPage (tHttp *pHttp);       
VOID IssProcessWlanQosPageSet (tHttp *pHttp);       
VOID IssProcessWlanQosPageGet (tHttp *pHttp);       

VOID IssProcessQosProfilePage (tHttp *pHttp);       
VOID IssProcessQosProfilePageSet (tHttp *pHttp);       
VOID IssProcessQosProfilePageGet (tHttp *pHttp);       

VOID IssProcessQosProfileEditPage (tHttp *pHttp);       
VOID IssProcessQosProfileEditPageSet (tHttp *pHttp);       
VOID IssProcessQosProfileEditPageGet (tHttp *pHttp);       

VOID IssProcessQosProfileCreatePage (tHttp *pHttp);       
VOID IssProcessQosProfileCreatePageSet (tHttp *pHttp);       

VOID IssProcessWlanSecurityPage(tHttp *pHttp);       
VOID IssProcessWlanSecurityPageSet(tHttp *pHttp);       
VOID IssProcessWlanSecurityPageGet(tHttp *pHttp);       

VOID IssProcessAuthenticationProfilePage (tHttp *pHttp);       
VOID IssProcessAuthenticationProfilePageSet (tHttp *pHttp);       
VOID IssProcessAuthenticationProfilePageGet (tHttp *pHttp);       

VOID IssProcessRsnaCipherPage (tHttp *pHttp);       
VOID IssProcessRsnaCipherPageSet (tHttp *pHttp);       
VOID IssProcessRsnaCipherPageGet (tHttp *pHttp);       

VOID IssProcessRsnaConfigPage (tHttp *pHttp);       
VOID IssProcessRsnaConfigPageSet (tHttp *pHttp);       
VOID IssProcessRsnaConfigPageGet (tHttp *pHttp);       


VOID IssProcessRsnaConfigEditPage (tHttp *pHttp);       
VOID IssProcessRsnaConfigEditPageSet (tHttp *pHttp);       
VOID IssProcessRsnaConfigEditPageGet (tHttp *pHttp);       

VOID IssProcessRsnaAuthsuitePage (tHttp *pHttp);       
VOID IssProcessRsnaAuthsuitePageSet (tHttp *pHttp);       
VOID IssProcessRsnaAuthsuitePageGet (tHttp *pHttp);       
#ifdef PMF_WANTED
VOID IssProcessRsnaProtectedMgmtPage (tHttp *pHttp);       
VOID IssProcessRsnaProtectedMgmtPageSet (tHttp *pHttp);       
VOID IssProcessRsnaProtectedMgmtPageGet (tHttp *pHttp);       
#endif
#ifdef WPA_WANTED
VOID IssProcessWpaCipherPage (tHttp *pHttp);       
VOID IssProcessWpaCipherPageSet (tHttp *pHttp);       
VOID IssProcessWpaCipherPageGet (tHttp *pHttp);       

VOID IssProcessWpaConfigPage (tHttp *pHttp);       
VOID IssProcessWpaConfigPageSet (tHttp *pHttp);       
VOID IssProcessWpaConfigPageGet (tHttp *pHttp);       


VOID IssProcessWpaConfigEditPage (tHttp *pHttp);       
VOID IssProcessWpaConfigEditPageSet (tHttp *pHttp);       
VOID IssProcessWpaConfigEditPageGet (tHttp *pHttp);       

VOID IssProcessWpaAuthsuitePage (tHttp *pHttp);       
VOID IssProcessWpaAuthsuitePageSet (tHttp *pHttp);       
VOID IssProcessWpaAuthsuitePageGet (tHttp *pHttp);       

VOID IssProcessWpaProtectedMgmtPage (tHttp *pHttp);       
VOID IssProcessWpaProtectedMgmtPageSet (tHttp *pHttp);       
VOID IssProcessWpaProtectedMgmtPageGet (tHttp *pHttp);       
#endif
#ifdef BAND_SELECT_WANTED
VOID IssProcessWlanBandSelectGlobal (tHttp *pHttp);
VOID IssProcessWlanBandSelectGlobalSet (tHttp *pHttp);
VOID IssProcessWlanBandSelectGlobalGet (tHttp *pHttp);

VOID IssProcessWlanBandSelectLocal (tHttp *pHttp);
VOID IssProcessWlanBandSelectLocalSet (tHttp *pHttp);
VOID IssProcessWlanBandSelectLocalGet (tHttp *pHttp);

VOID IssProcessWlanBandSelectClientTable (tHttp *pHttp);
VOID IssProcessWlanBandSelectClientTableSet (tHttp *pHttp);
VOID IssProcessWlanBandSelectClientTableGet (tHttp *pHttp);
#endif

#ifdef WLC_WANTED
VOID IssDhcpPoolTablePage (tHttp *pHttp);
VOID IssDhcpPoolTablePageSet (tHttp *pHttp);
VOID IssDhcpPoolTablePageGet (tHttp *pHttp);

VOID IssIpRouteTablePage (tHttp *pHttp);
VOID IssIpRouteTablePageSet (tHttp *pHttp);
VOID IssIpRouteTablePageGet (tHttp *pHttp);

VOID IssIvrTablePage (tHttp *pHttp);
VOID IssIvrTablePageSet (tHttp *pHttp);
VOID IssIvrTablePageGet (tHttp *pHttp);

VOID IssGeneralTablePage (tHttp *pHttp);
VOID IssGeneralTablePageSet (tHttp *pHttp);
VOID IssGeneralTablePageGet (tHttp *pHttp);

VOID IssFirewallTablePage (tHttp *pHttp);
VOID IssFirewallTablePageSet (tHttp *pHttp);
VOID IssFirewallTablePageGet (tHttp *pHttp);

VOID IssIpNatTablePage (tHttp *pHttp);
VOID IssIpNatTablePageSet (tHttp *pHttp);
VOID IssIpNatTablePageGet (tHttp *pHttp, INT4 i4NatPool2);
#endif

#ifdef WPS_WANTED
VOID IssProcessWpsConfigPage (tHttp *pHttp);
VOID IssProcessWpsConfigPageSet (tHttp *pHttp);
VOID IssProcessWpsConfigPageGet (tHttp *pHttp);
VOID IssProcessWpsConfigEditPage (tHttp *pHttp);
VOID IssProcessWpsConfigEditPageSet (tHttp *pHttp);
VOID IssProcessWpsConfigEditPageGet (tHttp *pHttp);
VOID IssProcessWpsAuthStaPage(tHttp *pHttp);
VOID IssProcessWpsAuthStaPageGet(tHttp *pHttp);
#endif
VOID IssProcessAuthenticationProfileCreatePage(tHttp *pHttp);       
VOID IssProcessAuthenticationProfileCreatePageGet (tHttp *pHttp);       
VOID IssProcessAuthenticationProfileCreatePageSet (tHttp *pHttp);       

VOID IssProcessAuthenticationProfileEditPage (tHttp *pHttp);       
VOID IssProcessAuthenticationProfileEditPageSet (tHttp *pHttp);       
VOID IssProcessAuthenticationProfileEditPageGet (tHttp *pHttp);       

VOID IssProcessMonitorAllApTablePage (tHttp * pHttp);
VOID IssProcessMonitorAllApTablePageGet (tHttp * pHttp);

VOID IssProcessMonitorApFilterTablePage (tHttp * pHttp);
VOID IssProcessMonitorApFilterTablePageGet (tHttp * pHttp);
VOID IssProcessMonitorApFilterTablePageSet (tHttp * pHttp);

VOID IssProcessMonitorFilterAllApTablePage (tHttp * pHttp);
VOID IssProcessMonitorFilterAllApTablePageGet (tHttp * pHttp);

VOID IssMonitorApJoinStatsTablePage (tHttp * pHttp);
VOID IssMonitorApJoinStatsTablePageGet (tHttp * pHttp);
VOID IssMonitorApJoinStatsTablePageSet (tHttp * pHttp);

VOID IssProcessUrmBasicSettingsPage(tHttp * pHttp);
VOID IssProcessUrmBasicSettingsPageGet(tHttp * pHttp);
VOID IssProcessUrmBasicSettingsPageSet(tHttp * pHttp);

VOID IssProcessUrmUserGroupPage(tHttp * pHttp);
VOID IssProcessUrmUserGroupPageGet(tHttp * pHttp); 
VOID IssProcessUrmUserGroupPageSet(tHttp * pHttp);

VOID IssProcessUrmUserRolePage(tHttp * pHttp);
VOID IssProcessUrmUserRolePageGet(tHttp * pHttp);
VOID IssProcessUrmUserRolePageSet(tHttp * pHttp);

VOID IssProcessUrmUsernameRestrictPage(tHttp * pHttp);
VOID IssProcessUrmUsernameRestrictPageGet(tHttp * pHttp);
VOID IssProcessUrmUsernameRestrictPageSet(tHttp * pHttp);

VOID IssProcessUrmStationMacRestrictPage(tHttp * pHttp);
VOID IssProcessUrmStationMacRestrictPageGet(tHttp * pHttp);
VOID IssProcessUrmStationMacRestrictPageSet(tHttp * pHttp);

VOID IssProcessUrmUserMacMappingPage(tHttp * pHttp);
VOID IssProcessUrmUserMacMappingPageGet(tHttp * pHttp);
VOID IssProcessUrmUserMacMappingPageSet(tHttp * pHttp);

VOID IssProcessUrmSessionDetailsPage(tHttp * pHttp);
VOID IssProcessUrmSessionDetailsPageGet(tHttp * pHttp);
VOID IssProcessUrmSessionDetailsPageSet(tHttp * pHttp);

#ifdef WLC_WANTED
VOID IssProcessMcastSettingsPage (tHttp *pHttp);
VOID IssProcessMcastSettingsPageSet (tHttp *pHttp);
VOID IssProcessMcastSettingsPageGet (tHttp *pHttp);

VOID IssProcessUrmSqlBasicPage(tHttp * pHttp);
VOID IssProcessUrmSqlBasicPageGet(tHttp * pHttp);
VOID IssProcessUrmSqlBasicPageSet(tHttp * pHttp);

VOID IssProcessUrmSqlShowPageGet (tHttp * pHttp);
VOID IssProcessUrmSqlShowPageSet (tHttp * pHttp);

VOID IssProcessUrmSqlInsertPage(tHttp * pHttp);
VOID IssProcessUrmSqlInsertPageGet(tHttp * pHttp);
VOID IssProcessUrmSqlInsertPageSet(tHttp * pHttp);

VOID IssProcessUrmSqlUpdatePage(tHttp * pHttp);
VOID IssProcessUrmSqlUpdatePageGet(tHttp * pHttp);
VOID IssProcessUrmSqlUpdatePageSet(tHttp * pHttp);

VOID IssProcessUrmSqlDeletePage(tHttp * pHttp);
VOID IssProcessUrmSqlDeletePageGet(tHttp * pHttp);
VOID IssProcessUrmSqlDeletePageSet(tHttp * pHttp);
#endif


VOID IssMonitorApJoinDetailedStatsTablePage (tHttp * pHttp);
VOID IssMonitorApJoinDetailedStatsTablePageGet (tHttp * pHttp);
#endif

UINT4 getPortFromAppIndex (INT4 i4Index);
UINT4 getAppIndex (INT4 i4Port);
    
#ifdef VXLAN_WANTED
INT4 IssVxlanNveInterfaceSetRequest (tHttp * pHttp, INT4 i4LoopBackId);
VOID IssProcessVxlanNveInterfacePage (tHttp *pHttp);
VOID IssProcessVxlanNveInterfacePageGet (tHttp *pHttp);
VOID IssProcessVxlanNveInterfacePageSet (tHttp *pHttp);
VOID IssVxlanNveInterfacePageDeinit (tHttp * pHttp);
#endif

#ifdef ISS_WANTED
extern INT1
nmhGetFirstIndexIssMirrorCtrlExtnTable ARG_LIST((INT4 *));
extern INT1
nmhGetNextIndexIssMirrorCtrlExtnTable ARG_LIST((INT4 , INT4 *));
extern INT1
nmhGetIssMirrorCtrlExtnMirrType ARG_LIST((INT4 ,INT4 *));
extern INT1
nmhGetIssMirrorCtrlExtnRSpanStatus ARG_LIST((INT4 ,INT4 *));
extern INT1
nmhGetIssMirrorCtrlExtnRSpanVlanId ARG_LIST((INT4 ,INT4 *));
extern INT1
nmhGetIssMirrorCtrlExtnRSpanContext ARG_LIST((INT4 ,INT4 *));
extern INT1
nmhGetIssMirrorCtrlExtnStatus ARG_LIST((INT4 ,INT4 *));
extern INT1
nmhSetIssMirrorCtrlExtnMirrType ARG_LIST((INT4  ,INT4 ));
extern INT1
nmhSetIssMirrorCtrlExtnRSpanStatus ARG_LIST((INT4  ,INT4 ));
extern INT1
nmhSetIssMirrorCtrlExtnRSpanVlanId ARG_LIST((INT4  ,INT4 ));
extern INT1
nmhSetIssMirrorCtrlExtnRSpanContext ARG_LIST((INT4  ,INT4 ));
extern INT1
nmhSetIssMirrorCtrlExtnStatus ARG_LIST((INT4  ,INT4 ));
extern INT1
nmhTestv2IssMirrorCtrlExtnMirrType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1
nmhTestv2IssMirrorCtrlExtnRSpanStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1
nmhTestv2IssMirrorCtrlExtnRSpanVlanId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1
nmhTestv2IssMirrorCtrlExtnRSpanContext ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1
nmhTestv2IssMirrorCtrlExtnStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT4 IssMirrRemoveSrcRecrd(UINT4 u4SessionNo, UINT4 u4ContextId,
               UINT4 u4SrcNum);
extern INT4 IssMirrRemoveDestRecrd(UINT4 u4SessionNo, UINT4 u4DestNum);
extern INT4 IssMirrGetNextSrcRecrd(UINT4 u4SessionNo, UINT4 u4ContextId,
               UINT4 u4SrcNum, UINT4 *pu4NextContextNum, UINT4 *pu4NextSrcNum,
               UINT1 *pu1Mode, UINT1 *pu1ControlStatus);
extern INT4 IssMirrGetNextDestRecrd(UINT4 u4SessionNo, UINT4 u4DestNum,
               UINT4 *pu4NextDestNum);

extern INT1 nmhGetIssMirrorCtrlExtnSrcCfg ARG_LIST((INT4  , INT4 ,INT4 *));
extern INT1 nmhGetIssMirrorCtrlExtnSrcMode ARG_LIST((INT4  , INT4 ,INT4 *));
extern INT1 nmhSetIssMirrorCtrlExtnSrcCfg ARG_LIST((INT4  , INT4  ,INT4 ));
extern INT1 nmhSetIssMirrorCtrlExtnSrcMode ARG_LIST((INT4  , INT4  ,INT4 ));
extern INT1 
nmhTestv2IssMirrorCtrlExtnSrcCfg ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));
extern INT1 
nmhTestv2IssMirrorCtrlExtnSrcMode ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));
extern INT1 
nmhGetFirstIndexIssMirrorCtrlExtnDestinationTable ARG_LIST((INT4 * , INT4 *));
extern INT1 
nmhGetNextIndexIssMirrorCtrlExtnDestinationTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));
extern INT1 nmhGetIssMirrorCtrlExtnDestCfg ARG_LIST((INT4  , INT4 ,INT4 *));
extern INT1 nmhSetIssMirrorCtrlExtnDestCfg ARG_LIST((INT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2IssMirrorCtrlExtnDestCfg ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));
extern INT1 
nmhSetIssMirrorCtrlExtnSrcVlanCfg ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));
extern INT1
nmhSetIssMirrorCtrlExtnSrcVlanMode ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));
extern INT1
nmhTestv2IssMirrorCtrlExtnSrcVlanCfg ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));
extern INT1
nmhTestv2IssMirrorCtrlExtnSrcVlanMode ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhGetFirstIndexIssMirrorCtrlExtnSrcTable ARG_LIST((INT4 * , INT4 *));
extern INT1
nmhGetNextIndexIssMirrorCtrlExtnSrcTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

extern INT1
nmhGetNextIndexIssMirrorCtrlExtnSrcVlanTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));
extern INT1
nmhGetIssMirrorCtrlExtnSrcVlanCfg ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));
extern INT1
nmhGetIssMirrorCtrlExtnSrcVlanMode ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));
extern INT1 nmhGetDot1qFutureVlanLearningMode (INT4 *pVlanLearningMode);

extern INT4 IssMirrorCtrlExtnMirrTypeGet(tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnRSpanStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnRSpanVlanIdGet(tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnRSpanContextGet(tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnMirrTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnRSpanStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnRSpanVlanIdSet(tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnRSpanContextSet(tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnMirrTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnRSpanStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnRSpanVlanIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnRSpanContextTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnDestCfgGet(tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnDestCfgSet(tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnDestCfgTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnSrcCfgGet(tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnSrcModeGet(tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnSrcCfgSet(tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnSrcModeSet(tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnSrcCfgTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnSrcModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnSrcVlanCfgGet(tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnSrcVlanModeGet(tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnSrcVlanCfgSet(tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorCtrlExtnSrcVlanModeSet(tSnmpIndex *, tRetVal *);
extern INT4 IssHealthChkClearCtrSet(tSnmpIndex *, tRetVal *);
extern INT1 nmhSetIssHealthChkClearCtr(tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2IssHealthChkClearCtr(UINT4 *, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetIssHealthChkStatus ARG_LIST((INT4 *));
extern INT1 nmhGetIssHealthChkErrorReason ARG_LIST((INT4 *));
extern INT1 nmhGetIssHealthChkMemAllocErrPoolId ARG_LIST((INT4 *));
extern INT1 nmhGetIssHealthChkConfigRestoreStatus ARG_LIST((INT4 *));
#endif


#ifdef NAT_WANTED
extern INT1 nmhGetNextIndexNatIfTable ARG_LIST((INT4 , INT4 *));
extern INT1 nmhGetNatIfNat ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetNatIfNapt ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetNatIfEntryStatus ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhSetNatIfNat ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetNatIfNapt ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetNatIfEntryStatus ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhTestv2NatIfEntryStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhGetFirstIndexNatStaticTable ARG_LIST((INT4 * , UINT4 *));
extern INT1 nmhGetNextIndexNatStaticTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));
extern INT1 nmhGetNatStaticTranslatedLocalIp ARG_LIST((INT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetNatStaticEntryStatus ARG_LIST((INT4  , UINT4 ,INT4 *));
extern INT1 nmhSetNatStaticTranslatedLocalIp ARG_LIST((INT4  , UINT4  ,UINT4 ));
extern INT1 nmhSetNatStaticEntryStatus ARG_LIST((INT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2NatStaticTranslatedLocalIp ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));
extern INT1 nmhTestv2NatStaticEntryStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));
extern INT1 nmhGetFirstIndexNatGlobalAddressTable ARG_LIST((INT4 * , UINT4 *));
extern INT1 nmhGetNextIndexNatGlobalAddressTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));
extern INT1 nmhGetNatGlobalAddressMask ARG_LIST((INT4  , UINT4 ,UINT4 *));
extern INT1 nmhSetNatGlobalAddressMask ARG_LIST((INT4  , UINT4  ,UINT4 ));
extern INT1 nmhSetNatGlobalAddressEntryStatus ARG_LIST((INT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2NatGlobalAddressEntryStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));
extern INT1 nmhGetFirstIndexNatStaticNaptTable ARG_LIST((INT4 * , UINT4 * , INT4 * , INT4 * , INT4 *));
extern INT1 nmhGetNextIndexNatStaticNaptTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));
extern INT1 nmhGetNatStaticNaptTranslatedLocalPort ARG_LIST((INT4  , UINT4  , INT4  , INT4  , INT4 ,INT4 *));
extern INT1 nmhGetNatStaticNaptDescription ARG_LIST((INT4  , UINT4  , INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetNatStaticNaptEntryStatus ARG_LIST((INT4  , UINT4  , INT4  , INT4  , INT4 ,INT4 *));

extern INT1 nmhSetNatStaticNaptTranslatedLocalIp ARG_LIST((INT4  , UINT4  , INT4  , INT4  , INT4  ,UINT4 ));
extern INT1 nmhSetNatStaticNaptTranslatedLocalPort ARG_LIST((INT4  , UINT4  , INT4  , INT4  , INT4  ,INT4 ));
extern INT1 nmhSetNatStaticNaptDescription ARG_LIST((INT4  , UINT4  , INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetNatStaticNaptLeaseDuration ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,
                                           INT4   ,INT4 ));
extern INT1 nmhSetNatStaticNaptEntryStatus ARG_LIST((INT4  , UINT4  , INT4  , INT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2NatStaticNaptEntryStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2NatStaticNaptTranslatedLocalPort ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  , INT4  ,INT4 ));
extern INT1 nmhGetFirstIndexNatDynamicTransTable ARG_LIST((INT4 * , UINT4 * , INT4 * , UINT4 * , INT4 *));
extern INT1 nmhGetNextIndexNatDynamicTransTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));
extern INT1 nmhGetNatDynamicTransTranslatedLocalIp ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ,UINT4 *));
extern INT1 nmhGetNatDynamicTransTranslatedLocalPort ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , INT4 ,INT4 *));
extern INT1 nmhGetFirstIndexNatIfTable ARG_LIST((INT4 *));
extern INT1 nmhGetNatGlobalAddressEntryStatus ARG_LIST((INT4  , UINT4 ,INT4 *));
extern INT1 nmhGetNatEnable ARG_LIST((INT4 *));

#endif

#ifdef TACACS_WANTED
extern INT1 nmhGetFsTacClntExtActiveServerAddressType ARG_LIST((INT4 *));
extern INT1 nmhGetFsTacClntExtActiveServer ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsTacClntExtTraceLevel ARG_LIST((UINT4 *));
extern INT1 nmhGetFsTacClntExtRetransmit ARG_LIST((INT4 *));
extern INT1 nmhSetFsTacClntExtActiveServerAddressType ARG_LIST((INT4 ));
extern INT1 nmhSetFsTacClntExtActiveServer ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsTacClntExtTraceLevel ARG_LIST((UINT4 ));
extern INT1 nmhSetFsTacClntExtRetransmit ARG_LIST((INT4 ));
extern INT1 nmhTestv2FsTacClntExtActiveServerAddressType ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FsTacClntExtActiveServer ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsTacClntExtTraceLevel ARG_LIST((UINT4 *  ,UINT4 ));
extern INT1 nmhTestv2FsTacClntExtRetransmit ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhValidateIndexInstanceFsTacClntExtServerTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhGetFirstIndexFsTacClntExtServerTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));
extern INT4
TacacsUtlGetKey ARG_LIST((INT4 , tSNMP_OCTET_STRING_TYPE*, tSNMP_OCTET_STRING_TYPE*));
extern INT1 nmhGetNextIndexFsTacClntExtServerTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsTacClntExtServerStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1 nmhGetFsTacClntExtServerSingleConnect ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1 nmhGetFsTacClntExtServerPort ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1 nmhGetFsTacClntExtServerTimeout ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1 nmhGetFsTacClntExtServerKey ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhSetFsTacClntExtServerStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhSetFsTacClntExtServerSingleConnect ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhSetFsTacClntExtServerPort ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhSetFsTacClntExtServerTimeout ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhSetFsTacClntExtServerKey ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsTacClntExtServerStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhTestv2FsTacClntExtServerSingleConnect ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhTestv2FsTacClntExtServerPort ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhTestv2FsTacClntExtServerTimeout ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhTestv2FsTacClntExtServerKey ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

#endif

#ifdef WLC_WANTED
VOID IssProcessWebAuthConfigPage (tHttp *pHttp);
VOID IssProcessWebAuthConfigPageGet (tHttp *pHttp);
VOID IssProcessWebAuthConfigPageSet (tHttp *pHttp);
VOID IssProcessWebAuthLoginPage (tHttp *pHttp);
VOID IssProcessWebAuthLoginPageGet (tHttp *pHttp);
VOID IssProcessWebAuthLoginPageSet (tHttp *pHttp);
VOID IssProcessWebAuthResultPage (tHttp *pHttp);
VOID IssProcessWebAuthResultPageGet (tHttp *pHttp);
VOID IssProcessWebAuthUserCreationPage (tHttp *pHttp);
VOID IssProcessWebAuthUserCreationPageSet (tHttp *pHttp);
VOID IssProcessWebAuthUserCreationPageGet (tHttp *pHttp);
VOID IssProcessWebAuthCustomizationPage(tHttp * pHttp);
VOID IssProcessWebAuthCustomizationPageGet(tHttp * pHttp);
VOID IssProcessWebAuthCustomizationPageSet(tHttp * pHttp);
VOID IssProcessWebAuthExternalCustomizationPage(tHttp * pHttp);
VOID IssProcessWebAuthExternalCustomizationPageGet(tHttp * pHttp);
VOID IssProcessWebAuthExternalCustomizationPageSet(tHttp * pHttp);
VOID IssProcessWebAuthInternalCustomizationPage(tHttp * pHttp);
VOID IssProcessWebAuthInternalCustomizationPageGet(tHttp * pHttp);
VOID IssProcessWebAuthInternalCustomizationPageSet(tHttp * pHttp);
#endif


VOID IssProcessGenInfoPageGet(tHttp *pHttp);
VOID IssProcessGenInfoPageSet(tHttp *pHttp);
VOID IssProcessIpSettingsPageSet(tHttp *pHttp);
VOID IssProcessIpSettingsPageGet(tHttp *pHttp);
VOID IssProcessL2UnicastFilterPageGet(tHttp *pHttp);
VOID IssProcessL2MulticastFilterPageSet(tHttp *pHttp);
VOID IssProcessL2MulticastFilterPageGet(tHttp *pHttp);
VOID IssProcessL2UnicastFilterPageSet(tHttp *pHttp);
VOID IssProcessTrafficClassPageGet(tHttp *pHttp);
VOID IssProcessTrafficClassPageSet(tHttp *pHttp);
VOID IssProcessPortMonitoringPageGet(tHttp *pHttp);
VOID IssProcessPortMonitoringPageSet(tHttp *pHttp);
VOID IssProcessIvrPage (tHttp *pHttp);
VOID IssPrintAvailableVlans (tHttp *pHttp);
VOID IssPrintAvailableProfiles (tHttp *pHttp, INT4);
VOID IssPrintAvailableInterfacesInContext (tHttp *pHttp);
VOID IssPrintAvailableLogicalInterfacesInContext(tHttp *pHttp);
VOID IssPrintAvailableAcInterfaces (tHttp *pHttp);
VOID IssProcessIpv6PingPage(tHttp *pHttp);
VOID IssProcessIpv6PingPageSet(tHttp *pHttp);
VOID IssProcessIpv6PingPageGet(tHttp *pHttp);
VOID IssProcessClearCountersPage(tHttp *pHttp);
VOID IssProcessClearCountersPageSet(tHttp *pHttp);

/*VRF Change*/
VOID IssProcessIvrInfoGet(tHttp * pHttp, INT4 i4IsSwitchSelect, UINT4 u4IfIndex);
VOID IssProcessIvrSet (tHttp *pHttp);
INT4  IssSetIfIpAddr (tHttp *pHttp, UINT4 u4IfIndex);
INT4 IssSetIfSecondaryIpAddr (tHttp * pHttp, UINT4 u4IfIndex,UINT4 u4IpAddress);
INT4 IssProcessIvrDelete (tHttp * pHttp,INT4 i4AddressType,UINT4 u4IfIndex);
VOID IssProcessIvrSecInfoGet (tHttp * pHttp,tSNMP_OCTET_STRING_TYPE * pIfAlias,
                              INT4 i4CurIndex, UINT4 u4SecondaryAddr);
INT4
IssWebIvrSetAddressAlloc PROTO ((UINT4 u4IfIndex,
                                 UINT1 u1AddrAllocMethod, UINT1 u1AddrAllocProto));

VOID IssPrintL3Interfaces (tHttp *pHttp, INT4 i4SentIfIndex);
VOID IssPrintL3VRFInterfaces (tHttp *pHttp, INT4 i4SentIfIndex, UINT1*, INT1);
VOID IssPrintAvailableTargetParams (tHttp *pHttp);
/*VRF Change*/
VOID IssPrintIpInterfaces (tHttp *pHttp, UINT4, INT1);

VOID IssPrintIpAddress (tHttp *pHttp);
VOID IssPrintIpAddressAndIfaceName(tHttp *pHttp);
VOID IssPrintAvailableL2Ports (tHttp *pHttp);
VOID IssPrintAvailablePhysicalInterface (tHttp *pHttp);
VOID IssPrintAvailableInterface (tHttp *pHttp);
VOID IssPrintValuesFromMinToMax (tHttp * pHttp, UINT4 u4Min, UINT4 u4Max, UINT1 *pu1KeyString);
VOID IssPrintAvailableFdbIds (tHttp *pHttp);
VOID IssPrintAvailableLAPortChannels (tHttp * pHttp);
/*VRF Changes*/
VOID IssPrintAvailableVlanInterface (tHttp *pHttp, UINT1*, INT1);

VOID IssPbPrintCustomerEdgePorts (tHttp * pHttp);
VOID IssPbPrintNetworkPorts (tHttp * pHttp);
VOID IssProcessOspfGlobalConfPageGet (tHttp *pHttp);
VOID IssProcessOspfGlobalConfPageSet (tHttp *pHttp);
VOID IssProcessOspfIfacePageGet(tHttp *pHttp);
VOID IssProcessOspfIfacePageSet (tHttp * pHttp);
VOID IssProcessOspfVirtualIfacePageSet (tHttp * pHttp);
VOID IssProcessOspfVirtualIfacePageGet (tHttp * pHttp);
VOID IssProcessOspfNeighborPageGet(tHttp *pHttp);
VOID IssProcessOspfNeighborPageSet (tHttp * pHttp);
VOID IssProcessOspfRRDRoutePageGet(tHttp *pHttp);
VOID IssProcessOspfRRDRoutePageSet (tHttp * pHttp);
VOID IssProcessOspfAreaAggrPageGet (tHttp *pHttp);
VOID IssProcessOspfAreaAggrPageSet (tHttp * pHttp);
VOID IssProcessOspfExtAggrPageGet (tHttp *pHttp);
VOID IssProcessOspfExtAggrPageSet (tHttp * pHttp);
VOID IssProcessOspfAreaPageGet (tHttp *pHttp);
VOID IssProcessOspfAreaPageSet (tHttp *pHttp);
VOID IssProcessOspfRouteStatsPageGet(tHttp *pHttp);
VOID IssProcessOspfLsdbPageGet (tHttp *pHttp);
VOID IssProcessOspfGRPageGet (tHttp *pHttp);
VOID IssProcessOspfGRPageSet (tHttp *pHttp);
VOID IssProcessRRDOspfConfPage (tHttp *pHttp);
VOID IssProcessRRDOspfConfPageGet (tHttp *pHttp);
VOID IssProcessRRDOspfConfPageSet (tHttp *pHttp);
VOID IssProcessRRDOspfGet (tHttp *pHttp, UINT4, INT4 , INT4);

VOID IssProcessPimSRpPageGet (tHttp *pHttp);
VOID IssProcessPimIfacePageGet (tHttp *pHttp);
VOID IssProcessPimComponentPageGet (tHttp * pHttp);
VOID IssProcessPimCrpPageGet(tHttp *pHttp);
VOID IssProcessPimSRpPageSet (tHttp *pHttp);
VOID IssProcessPimIfacePageSet (tHttp *pHttp);
VOID IssProcessPimCrpPageSet (tHttp * pHttp);
VOID IssProcessPimComponentPageSet (tHttp * pHttp);
INT4 PimCheckAndDestroyPimStdComponentStatus (INT4 , BOOL1);
VOID IssProcessIgmpPageGet (tHttp *pHttp);
VOID IssProcessIgmpPageSet (tHttp * pHttp);
VOID IssProcessMldPageGet (tHttp *pHttp);
VOID IssProcessMldPageSet (tHttp * pHttp);
VOID IssProcessIgpUpIfPageGet (tHttp * pHttp);
VOID IssProcessIgpUpIfPageSet (tHttp * pHttp);
VOID IssProcessIgmpRouteConfPageGet (tHttp *pHttp);
VOID IssProcessIgmpRouteConfPageSet (tHttp * pHttp);
VOID IssProcessIgmpMembershipPageGet (tHttp *pHttp);
VOID IssProcessIgmpGroupListConfPageGet(tHttp *pHttp);
VOID IssProcessIgmpGroupListConfPageSet (tHttp * pHttp);
VOID IssProcessIgsPageGet (tHttp *pHttp);
VOID IssProcessMstiVlanMapPage(tHttp *pHttp);
VOID IssProcessMstiVlanMapPageGet (tHttp *pHttp);
VOID IssProcessMstiVlanMapPageSet(tHttp *pHttp);
VOID IssProcessMstiPortConfPage(tHttp *pHttp);
VOID IssProcessMstiPortConfPageGet (tHttp *pHttp);
VOID IssProcessMstiPortConfPageSet (tHttp *pHttp);
VOID IssProcessMstiBridgeConfPage (tHttp *pHttp);
VOID IssProcessMstiBridgeConfPageGet (tHttp *pHttp);
VOID IssProcessMstiBridgeConfPageSet (tHttp *pHttp);
VOID IssProcessPortSettingsPage(tHttp *pHttp);
VOID IssProcessPortSettingsPageGet(tHttp *pHttp);
VOID IssProcessPortSettingsPageSet (tHttp *pHttp);
VOID IssProcessSavePage (tHttp *pHttp);
VOID IssProcessSavePageGet (tHttp *pHttp);
VOID IssProcessSavePageSet (tHttp *pHttp);
VOID IssConvertStrToVlanList (UINT1 *pu1Str, UINT1 *au1VlanList);
VOID IssConvertOctetToVlan (UINT1 *pOctet, UINT4 u4OctetLen, UINT1 *pu1Temp,UINT1 u1Flag);
VOID IssProcessIPFilterConfPage(tHttp *pHttp);
VOID IssProcessIPFilterConfPageGet(tHttp *pHttp);
VOID IssProcessIPFilterConfPageSet(tHttp *pHttp);

VOID IssProcessIPStdFilterConfPage(tHttp *pHttp);
VOID IssProcessIPStdFilterConfPageGet(tHttp *pHttp);
VOID IssProcessIPStdFilterConfPageSet(tHttp *pHttp);
VOID IssProcessIpRouteConfPage(tHttp *pHttp);
/*VRF Change*/
VOID IssProcessIpRouteGet(tHttp *pHttp, INT4, UINT4, UINT4, UINT4, UINT4);

VOID IssProcessIpRouteSet(tHttp *pHttp);
VOID IssProcessArpEntryPage(tHttp *pHttp);
VOID IssProcessArpEntryPageSet(tHttp *pHttp);
VOID IssProcessArpEntryPageGet(tHttp *pHttp);
VOID IssProcessIpv4TraceRoutePage(tHttp *pHttp);
VOID IssProcessIpv4TraceRoutePageGet(tHttp *pHttp);
VOID IssProcessIpv4TraceRoutePageSet(tHttp *pHttp);
VOID IssProcessMACFilterConfPage(tHttp *pHttp);
VOID IssProcessMACFilterConfPageGet(tHttp *pHttp);
VOID IssProcessMACFilterConfPageSet(tHttp *pHttp);
VOID IssProcessPortVidSetPage(tHttp *pHttp);
VOID IssProcessPortVidSetPageGet(tHttp *pHttp);
VOID IssProcessPortVidSetPageSet(tHttp *pHttp);
VOID IssPrintVlanGroupIds (tHttp * pHttp);
VOID IssProcessVlanProtoGrpPage (tHttp *pHttp);
VOID IssProcessVlanProtoGrpPageGet (tHttp *pHttp);
VOID IssProcessVlanProtoGrpPageSet(tHttp *pHttp);
VOID IssConvertOctetToString (UINT1 *pOctet, UINT4 u4OctetLen, UINT1 *pu1Temp);
VOID IssConvertStringToOctet (UINT1 *pString, UINT4 u4StrLen, UINT1 *pu1Temp);

VOID IssProcessVlanFdbEntriesPage (tHttp *pHttp);
VOID IssProcessVlanFdbEntriesPageGet (tHttp *pHttp);
VOID IssProcessVlanFdbEntriesPageSet (tHttp *pHttp);
VOID IssProcessVlanCurrentDbPage (tHttp * pHttp);
VOID IssProcessBridgeInfoPage (tHttp * pHttp);
VOID IssProcessMstpCistPortStatsPage (tHttp * pHttp);
VOID IssProcessMstpMstiPortStatsPage (tHttp * pHttp);
VOID IssProcessMstpCistPortStatusPage (tHttp * pHttp);
   
VOID IssProcessRstpInfoPage (tHttp * pHttp);
VOID IssProcessRstpPortStatsPage (tHttp * pHttp);
VOID IssProcessAstClearCountersPage (tHttp * pHttp);
VOID IssProcessRstpPortInfoPage (tHttp * pHttp);
VOID IssProcessRstpTimersPage (tHttp * pHttp);
VOID IssProcessRstpTimersPageGet (tHttp * pHttp);
VOID IssProcessArpCacheStatsPage (tHttp * pHttp);
VOID IssProcessRstpPortStatusPage (tHttp * pHttp);

VOID IssProcessLogSavePage (tHttp * pHttp);
VOID IssProcessLogSavePageSet (tHttp * pHttp);
VOID IssProcessLogSavePageGet (tHttp * pHttp);
VOID IssProcessFileUploadPage (tHttp * pHttp);    
VOID IssProcessFileUploadPageGet (tHttp * pHttp);    
VOID IssProcessFileUploadPageSet (tHttp * pHttp);    

VOID IssProcessFileDownloadPage (tHttp * pHttp);    
VOID IssProcessFileDownloadPageGet (tHttp * pHttp);    
VOID IssProcessFileDownloadPageSet (tHttp * pHttp);    
VOID IssProcessSwUpgradePage (tHttp * pHttp);
VOID IssProcessSwUpgradePageGet (tHttp * pHttp);
VOID IssProcessSwUpgradePageSet (tHttp * pHttp);
    
    
    
#ifdef LA_WANTED
VOID IssProcessLAPortChannelPage (tHttp * pHttp);
VOID IssProcessLASelectionPolicyPage (tHttp * pHttp);
VOID IssProcessLAInterfaceSettingsPage (tHttp * pHttp);
VOID IssProcessLAAggreConfPage (tHttp * pHttp);
VOID IssProcessLAPortStateInfoPage (tHttp * pHttp);
VOID IssProcessLAPortChannelPageGet (tHttp *pHttp);       
VOID IssProcessLAPortChannelPageSet (tHttp *pHttp);       
VOID IssProcessLASelectionPolicyPageGet (tHttp *pHttp);
VOID IssProcessLASelectionPolicyPageSet (tHttp *pHttp);
VOID IssProcessLAInterfaceSettingsPageGet (tHttp *pHttp);       
VOID IssProcessLAInterfaceSettingsPageSet (tHttp *pHttp);      
VOID IssProcessLAAggreConfPageGet (tHttp *pHttp);       
VOID IssProcessLAAggreConfPageSet (tHttp *pHttp);       
#endif
VOID IssProcessVlanStatsPage (tHttp * pHttp);

#ifdef VRRP_WANTED
VOID IssProcessVrrpBasicPage (tHttp * pHttp);
VOID IssProcessVrrpConfPage (tHttp * pHttp);
VOID IssProcessVrrpConfPageGet (tHttp * pHttp);
VOID IssProcessVrrpConfPageSet (tHttp * pHttp);
VOID IssProcessVrrpStatsPage (tHttp * pHttp);
VOID IssProcessVrrpTrackPage (tHttp * pHttp);
VOID IssProcessVrrpAssocPage (tHttp * pHttp);
#endif /* VRRP_WANTED */

#ifdef RMON_WANTED
VOID IssProcessRmonEtherStatsPage (tHttp *);
#endif /* RMON_WANTED */

#ifdef DVMRP_WANTED
VOID IssProcessDvmrpIfacePage (tHttp * pHttp);
VOID IssProcessDvmrpIfacePageGet (tHttp *pHttp);
#endif
#ifdef DHCP_SRV_WANTED
VOID IssProcessDhcpPoolOptConfPage (tHttp *);
VOID IssProcessDhcpHostIpConfPage (tHttp *);
VOID IssProcessDhcpHostOptConfPage (tHttp *);
#endif
#ifdef DHCPC_WANTED
VOID IssProcessDhcpOptionTypePage (tHttp * pHttp);
VOID IssProcessDhcpOptionTypePageGet (tHttp * pHttp);
VOID IssProcessDhcpOptionTypePageSet (tHttp * pHttp);
VOID IssProcessDhcpClientIdPage (tHttp * pHttp);
VOID IssProcessDhcpClientIdPageGet (tHttp * pHttp);
VOID IssProcessDhcpClientIdPageSet (tHttp * pHttp);
#endif
#ifdef DHCP_RLY_WANTED
VOID IssProcessDhcpRlyInterfaceConfPage (tHttp * pHttp);
VOID IssProcessDhcpRlyInterfaceConfPageSet (tHttp * pHttp);
VOID IssProcessDhcpRlyInterfaceConfPageGet (tHttp * pHttp);
#endif

#ifdef DIFFSRV_WANTED
VOID IssProcessDfsPolicyMapPage (tHttp * pHttp);
VOID IssProcessDfsPolicyMapPage (tHttp * pHttp);
VOID IssProcessDfsPolicyMapPageSet (tHttp * pHttp);
VOID IssProcessDfsPolicyMapPageGet (tHttp * pHttp);
#endif

#ifdef RIP_WANTED
VOID IssProcessRipVRFCreationPage (tHttp * pHttp);
VOID IssProcessRipVRFCreationPageSet (tHttp * pHttp);
VOID IssProcessRipVRFCreationPageGet (tHttp * pHttp);

VOID IssProcessRipGlobalPage (tHttp * pHttp);
VOID IssProcessRipGlobalPageSet (tHttp * pHttp);
VOID IssProcessRipGlobalPageGet (tHttp * pHttp);
VOID IssProcessRipIfacePage (tHttp * pHttp);
VOID IssProcessRipIfacePageGet (tHttp *pHttp);
VOID IssProcessRipIfacePageSet (tHttp *pHttp);
VOID IssProcessRipIfaceConfKeyPage(tHttp *pHttp);
VOID IssProcessRipIfaceKeyConfGet (tHttp *pHttp);
VOID IssProcessRipIfaceKeyConfSet (tHttp *pHttp);
VOID IssProcessRipKeyConfGet(tHttp *pHttp);
VOID IssProcessRipTrustNBRPage (tHttp * pHttp);
VOID IssProcessRipTrustNBRPageGet (tHttp *pHttp);
VOID IssProcessRipTrustNBRPageSet (tHttp *pHttp);
VOID IssProcessRipAggPage (tHttp * pHttp);
VOID IssProcessRipAggPageGet (tHttp *pHttp);
VOID IssProcessRipAggPageSet (tHttp *pHttp);
VOID
IssPrintAvailableRipContextId (tHttp * pHttp);
VOID IssProcessRRDRipConfPage (tHttp *pHttp);
VOID IssProcessRRDRipConfPageGet (tHttp *pHttp);
VOID IssProcessRRDRipConfPageSet (tHttp *pHttp);
VOID IssProcessRRDRipGet (tHttp *pHttp, UINT4);
extern INT1
nmhGetFirstIndexFsMIStdRip2GlobalTable (INT4 *);
extern INT1
nmhGetNextIndexFsMIStdRip2GlobalTable (INT4 , INT4 *);
extern INT1
nmhGetFsMIRipAdminStatus (INT4 ,INT4 *);
extern INT1
nmhSetFsMIRipAdminStatus (INT4  ,INT4 );
extern INT1
nmhTestv2FsMIRipAdminStatus (UINT4 *  ,INT4  ,INT4 );


extern INT1 
nmhGetFirstIndexFsMIRipCryptoAuthTable (INT4 * , INT4 * , UINT4 * , INT4 *);
extern INT1
nmhGetNextIndexFsMIRipCryptoAuthTable (INT4 , INT4 * , INT4 , INT4 * , 
                                        UINT4 , UINT4 * , INT4 , INT4 *);
extern INT1
nmhGetFsMIRipCryptoKeyStartAccept (INT4  , INT4  , UINT4  , INT4 ,
                                    tSNMP_OCTET_STRING_TYPE * );
extern INT1
nmhGetFsMIRipCryptoKeyStartGenerate (INT4  , INT4  , UINT4  , INT4 ,
                                    tSNMP_OCTET_STRING_TYPE * );
extern INT1
nmhGetFsMIRipCryptoKeyStopAccept (INT4  , INT4  , UINT4  , INT4 ,
                                    tSNMP_OCTET_STRING_TYPE * );
extern INT1
nmhGetFsMIRipCryptoKeyStopGenerate (INT4  , INT4  , UINT4  , INT4 ,
                                    tSNMP_OCTET_STRING_TYPE * );
extern INT1
nmhSetFsMIRipCryptoAuthKey (INT4  , INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *);
extern INT1
nmhSetFsMIRipCryptoKeyStartAccept (INT4  , INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *);
extern INT1
nmhSetFsMIRipCryptoKeyStartGenerate (INT4  , INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *);
extern INT1
nmhSetFsMIRipCryptoKeyStopAccept (INT4  , INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *);
extern INT1
nmhSetFsMIRipCryptoKeyStopGenerate (INT4  , INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *);
extern INT1
nmhTestv2FsMIRipCryptoAuthKey (UINT4 *  ,INT4  , INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *);
extern INT1
nmhTestv2FsMIRipCryptoKeyStartAccept (UINT4 *  ,INT4  , INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *);
extern INT1
nmhTestv2FsMIRipCryptoKeyStartGenerate (UINT4 *  ,INT4  , INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *);
extern INT1
nmhTestv2FsMIRipCryptoKeyStopGenerate (UINT4 *, INT4  , INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *);
extern INT1
nmhTestv2FsMIRipCryptoKeyStopAccept (UINT4 *  ,INT4  , INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *);
extern INT1
nmhSetFsMIRipCryptoKeyStatus (INT4  , INT4  , UINT4  , INT4  , INT4);
extern INT4 FsRip2IfConfDefRtInstallSet(tSnmpIndex *, tRetVal *);
extern INT4 Rip2IfConfSendSet (tSnmpIndex *, tRetVal *);
extern INT4 Rip2IfConfReceiveSet (tSnmpIndex *, tRetVal *);
extern INT4 FsRip2IfConfRouteAgeTmrSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRip2IfConfUpdateTmrSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRip2IfConfGarbgCollectTmrSet(tSnmpIndex *, tRetVal *);
extern INT4 Rip2IfConfStatusSet (tSnmpIndex *, tRetVal *);
extern INT4 FsRipMd5KeyRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRipMd5KeyStartTimeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRipMd5KeyExpiryTimeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRipMd5AuthKeySet(tSnmpIndex *, tRetVal *);
extern INT4 Rip2IfConfAuthKeySet (tSnmpIndex *, tRetVal *);
extern INT4 Rip2IfConfAuthTypeSet (tSnmpIndex *, tRetVal *);
extern INT4 FsRip2TrustNBRRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRipAggStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsMIRipRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRipAdminStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRip2SpacingEnableSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRip2TrustNBRListEnableSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRip2AutoSummaryStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Rip2IfConfSrcAddressSet (tSnmpIndex *, tRetVal *);
extern INT4 Rip2IfConfDefaultMetricSet (tSnmpIndex *, tRetVal *);
extern INT4 FsRip2IfAdminStatSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRip2IfSplitHorizonStatusSet(tSnmpIndex *, tRetVal *);

#endif /*RIP_WANTED*/

extern INT4 IpCidrRouteStatusSet (tSnmpIndex *, tRetVal *);
extern INT4 IpCidrRouteIfIndexSet (tSnmpIndex *, tRetVal *);
extern INT4 IpCidrRouteMetric1Set (tSnmpIndex *, tRetVal *);

VOID IssProcessVlanDynGrpTablePage (tHttp * pHttp);
#ifdef EOAM_WANTED
VOID IssProcessEoamLinkSettingsPage(tHttp * phttp);
VOID IssProcessEoamNeighborStatusPage(tHttp * pHttp);
VOID IssProcessEoamEventLogPage(tHttp * pHttp);
VOID IssProcessEoamLinkSettingsGet(tHttp *phttp);
VOID IssProcessEoamLinkSettingsSet(tHttp *phttp);
#ifdef EOAM_FM_WANTED
VOID IssProcessFmMibResponsePage (tHttp * pHttp);
INT1 IssProcessFmDispMibRespDetail (tHttp *,UINT1 *, UINT1 *,INT4 *,INT4,UINT4);
INT1 IssProcessFmDispMibResp (tHttp *,INT4 i4IfIndex,UINT4 u4RespId);
#endif
#endif
#ifdef QOSX_WANTED
#define QOS_RD_CONFIG_ACTION_FLAG_LENGTH 1
#define QOS_START_INITIAL_INDEX 0
VOID IssProcessBasicSettingsPage(tHttp * phttp);
VOID IssProcessBasicSettingsGet(tHttp *phttp);
VOID IssProcessBasicSettingsSet(tHttp *phttp);
VOID IssProcessDataPathPage (tHttp *phttp);
VOID IssProcessDataPathSet (tHttp * phttp);
VOID IssProcessDataPathGet (tHttp * phttp);
VOID IssProcessClfrElementPage(tHttp *phttp);
VOID IssProcessClfrElementSet (tHttp *phttp);
VOID IssProcessClfrElementGet (tHttp *phttp);
VOID IssPrintClassifier(tHttp * pHttp);
VOID IssPrintMeter(tHttp *pHttp);
VOID IssPrintAction(tHttp *pHttp);
VOID IssPrintAlgo(tHttp *pHttp);
VOID IssPrintQ(tHttp *pHttp);
VOID IssPrintMfc(tHttp *pHttp);
VOID IssPrintMinRate (tHttp * pHttp);
VOID IssPrintMaxRate (tHttp * pHttp);
VOID IssPrintCountAct (tHttp * pHttp);
VOID IssPrintRed (tHttp * pHttp);
VOID IssPrintStdsch (tHttp * pHttp);

VOID WebnmConvertOidToString (tSNMP_OID_TYPE *, UINT1 *);
VOID IssProcessClassifierPage (tHttp *phttp);
VOID IssProcessClassifierSet (tHttp *phttp);
VOID IssProcessClassifierGet (tHttp *phttp);
/*VOID IssProcessClfrElementPage (tHttp *phttp);
VOID IssProcessClfrElementGet (tHttp *phttp);*/
VOID IssProcessQueuePage (tHttp *phttp);
VOID IssProcessQueueGet (tHttp *phttp);
VOID IssProcessQueueSet (tHttp *phttp);
VOID IssProcessMeterPage  (tHttp *phttp);
VOID IssProcessMeterGet  (tHttp *phttp);
VOID IssProcessMeterSet  (tHttp *phttp);
VOID IssProcessActionPage (tHttp *phttp);
VOID IssProcessActionGet (tHttp *phttp);
VOID IssProcessActionSet (tHttp *phttp);
VOID IssProcessCountActionPage (tHttp *phttp);
VOID IssProcessCountActionGet (tHttp *phttp);
VOID IssProcessCountActionSet (tHttp *phttp);
VOID IssProcessMinRatePage (tHttp *phttp);
VOID IssProcessMinRateGet (tHttp *phttp);
VOID IssProcessMinRateSet (tHttp *phttp);
VOID IssProcessMaxRatePage (tHttp *phttp);
VOID IssProcessMaxRateSet (tHttp *phttp);
VOID IssProcessMaxRateGet (tHttp *phttp);
VOID IssProcessStdSchedulerPage (tHttp *phttp);
VOID IssProcessStdSchedulerSet (tHttp *phttp);
VOID IssProcessStdSchedulerGet (tHttp *phttp);


VOID IssProcessPriorityMapSettingsPage(tHttp * phttp);
VOID IssProcessPriorityMapSettingsGet(tHttp *phttp);
VOID IssProcessPriorityMapSettingsSet(tHttp *phttp);
VOID IssProcessClassMapSettingsPage(tHttp *phttp);
VOID IssProcessClassMapSettingsSet(tHttp *phttp);
VOID IssProcessClassMapSettingsGet(tHttp *phttp);
VOID IssProcessClassToPriSettingsPage(tHttp *phttp);
VOID IssProcessClassToPriSettingsGet(tHttp *phttp);
VOID IssProcessClassToPriSettingsSet(tHttp *phttp);
VOID IssProcessMeterTableSettingsPage(tHttp *phttp);
VOID IssProcessMeterTableSettingsGet(tHttp *phttp);
VOID IssProcessMeterTableSettingsSet(tHttp *phttp);
VOID IssProcessPolicyMapSettingsPage(tHttp *phttp);
VOID IssProcessPolicyMapSettingsGet(tHttp *phttp);
VOID IssProcessPolicyMapSettingsSet(tHttp *phttp);
VOID IssProcessQueueTemplateSettingsPage(tHttp *phttp);
VOID IssProcessQueueTemplateSettingsSet(tHttp *phttp);
VOID IssProcessQueueTemplateSettingsGet(tHttp *phttp);
VOID IssProcessHierarchyTableSettingsPage(tHttp *phttp);
VOID IssProcessHierarchyTableSettingsSet(tHttp *phttp);
VOID IssProcessHierarchyTableSettingsGet(tHttp *phttp);
VOID IssProcessRedConfSettingsPage(tHttp *phttp);
VOID IssProcessRedConfSettingsSet(tHttp *phttp);
VOID IssProcessRedConfSettingsGet(tHttp *phttp);
VOID IssProcessSchedulerTableSettingsPage(tHttp *phttp);
VOID IssProcessSchedulerTableSettingsSet(tHttp *phttp);
VOID IssProcessSchedulerTableSettingsGet(tHttp *phttp);
VOID IssProcessQueueTableSettingsPage(tHttp *phttp);
VOID IssProcessQueueTableSettingsSet(tHttp *phttp);
VOID IssProcessQueueTableSettingsGet(tHttp *phttp);
VOID IssProcessQueueMapSettingsPage(tHttp *phttp);
VOID IssProcessQueueMapSettingsSet(tHttp *phttp);
VOID IssProcessQueueMapSettingsGet(tHttp *phttp);
VOID IssProcessShapeTemplateSettingsPage(tHttp *phttp);
VOID IssProcessShapeTemplateSettingsSet(tHttp *phttp);
VOID IssProcessShapeTemplateSettingsGet(tHttp *phttp);
VOID IssProcessDefaultUserPrioritySettingsPage(tHttp *phttp);
VOID IssProcessDefaultUserPrioritySettingsGet(tHttp *phttp);
VOID IssProcessDefaultUserPrioritySettingsSet(tHttp *phttp);
VOID IssPrintShapeTemplate (tHttp * pHttp);
VOID IssPrintPriorityMap(tHttp *phttp);
VOID IssPrintTrafficClass (tHttp * pHttp);
VOID IssPrintMeterId (tHttp * pHttp);
VOID IssPrintQueueTemplate (tHttp * pHttp);
VOID IssPrintSchedulerIds (tHttp * pHttp);
VOID IssPrintQueueIds (tHttp * pHttp);
VOID IssProcessPolicerStatsPage (tHttp * pHttp);
VOID IssProcessCosStatsPage (tHttp * pHttp);
#endif

extern UINT1 *allocmem_EnmBlk PROTO ((VOID));
extern VOID free_EnmBlk PROTO((UINT1 *));

#ifdef RM_WANTED

#define RM_PREFERRED_MASTER 100
#define RM_BACKUP_MASTER     50
#define RM_PREFERRED_SLAVE   25
#define INIT   0
#define MASTER 1
#define SLAVE  2

VOID IssProcessHitlessRestartConfPage (tHttp * pHttp);
VOID IssProcessHitlessRestartConfPageGet (tHttp * pHttp);
VOID IssProcessHitlessRestartConfPageSet (tHttp * pHttp);

VOID IssProcessStackingPage (tHttp * phttp);
VOID IssProcessRedundancyManagerConfPage (tHttp * phttp);
VOID IssProcessRedundancyManagerConfPageGet (tHttp * phttp);
VOID IssProcessRedundancyManagerConfPageSet (tHttp * phttp);
VOID IssProcessRedundancyManagerSwitchConfigPage (tHttp * phttp);
VOID IssProcessRedundancyManagerSwitchPageSet (tHttp * phttp);
VOID IssProcessStackShowPage(tHttp * phttp);
VOID IssProcessStackShowBriefPage(tHttp * phttp);
VOID IssProcessStackConfigurePage (tHttp * phttp);
VOID IssProcessStackConfigurePageGet (tHttp * phttp);
VOID IssProcessStackConfigurePageSet (tHttp * phttp);
VOID IssProcessStackShowPageGet (tHttp * phttp);
VOID IssProcessStackShowPageSet (tHttp * phttp);

VOID IssProcessStackShowBriefPageGet(tHttp * phttp);
VOID IssProcessStackShowBriefPageSet(tHttp * phttp);


VOID IssProcessStackCountersPageSet (tHttp * phttp);
VOID IssProcessStackCountersPageGet (tHttp * phttp);
VOID IssProcessStackCountersPage (tHttp * phttp);
#endif

VOID IssProcessIpAuthMgrPage (tHttp * pHttp);
VOID IssProcessIpAuthMgrPageGet (tHttp * pHttp);
VOID IssProcessIpAuthMgrPageSet (tHttp * pHttp);
VOID IssIpAuthConvertOctetToVlan (UINT1 *, UINT4, UINT1 *);
VOID IssIpAuthGetValueForServices (tHttp *, UINT4, UINT4);
INT4 IssIpAuthConvertStrToVlanList (UINT1 *, UINT1 *);

#ifdef IGS_WANTED
VOID IssProcessIgsInstConfPage (tHttp * pHttp);
VOID IssProcessIgsInstConfPageGet (tHttp * pHttp);
VOID IssProcessIgsInstConfPageSet (tHttp * pHttp);
VOID IssProcessIgsTmrConfPage (tHttp * pHttp);
VOID IssProcessIgsTmrConfPageGet (tHttp * pHttp);
VOID IssProcessIgsTmrConfPageSet (tHttp * pHttp);
VOID IssProcessIgsClearStatsPage (tHttp * pHttp);
VOID IssProcessIgsStatsTablePage (tHttp * pHttp);
VOID IssProcessIgsv3StatsTablePage (tHttp * pHttp);
VOID IssProcessIgsVlanMcastFwdTablePage(tHttp *pHttp);
VOID IssProcessIgsVlanRouterTablePage (tHttp * pHttp);
VOID IssProcessIgsVlanPage (tHttp * pHttp);
VOID IssProcessIgsVlanPageGet (tHttp * pHttp);
VOID IssProcessIgsVlanPageSet (tHttp * pHttp);
VOID IssProcessIgsMcastRecvPage (tHttp * pHttp);
VOID IssProcessIgsIntfConfPage (tHttp * pHttp);
VOID IssProcessIgsIntfConfPageGet (tHttp * pHttp);
VOID IssProcessIgsIntfConfPageSet (tHttp * pHttp);
VOID IssProcessIgsRtrPortConfPage (tHttp * pHttp);
VOID IssProcessIgsRtrPortConfPageGet (tHttp * pHttp);
VOID IssProcessIgsRtrPortConfPageSet (tHttp * pHttp);
INT4
SnoopWebSetVlanRouterPorts ( INT4 i4InstId, INT4 i4VlanId,
                            INT4 i4AddrType, UINT1 *pu1RouterPorts);
INT4
 SnoopWebUpdateVlanEntry ( INT4 i4InstId, INT4 i4VlanId,
                           INT4 i4AddrType, UINT1 u1Status);
INT4
SnoopWebDelVlanRouterPorts ( INT4 i4InstId, INT4 i4VlanId,
                            INT4 i4AddrType, UINT1 *pu1RouterPorts);
extern INT1
nmhTestv2FsSnoopVlanRtrLocalPortList (UINT4 *pu4ErrorCode,
                                      INT4 i4FsSnoopVlanFilterInstId,
                                      INT4 i4FsSnoopVlanFilterVlanId,
                                      INT4
                                      i4FsSnoopVlanFilterInetAddressType,
                                      tSNMP_OCTET_STRING_TYPE
                                      * pTestValFsSnoopVlanRtrLocalPortList);
extern VOID
SnoopConvertToLocalPortList ( UINT1 * ,
                             UINT1 * );
extern INT4
FsSnoopVlanRtrLocalPortListSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData);
extern INT1
nmhGetFsSnoopVlanRowStatus (INT4 i4FsSnoopVlanFilterInstId,
                            INT4 i4FsSnoopVlanFilterVlanId,
                            INT4 i4FsSnoopVlanFilterInetAddressType,
                            INT4 *pi4RetValFsSnoopVlanRowStatus);
VOID IssProcessIgsStaticPage(tHttp * pHttp);
VOID IssProcessIgsStaticPageGet (tHttp * pHttp);
VOID IssProcessIgsStaticPageSet (tHttp * pHttp);
#endif

#ifdef MLDS_WANTED
VOID IssProcessMldsInstConfPage (tHttp * pHttp);
VOID IssProcessMldsInstConfPageGet (tHttp * pHttp);
VOID IssProcessMldsInstConfPageSet (tHttp * pHttp);
VOID IssProcessMldsTmrConfPage (tHttp * pHttp);
VOID IssProcessMldsTmrConfPageGet (tHttp * pHttp);
VOID IssProcessMldsTmrConfPageSet (tHttp * pHttp);
VOID IssProcessMldsStatsTablePage (tHttp * pHttp);
VOID IssProcessMldsv2StatsTablePage (tHttp * pHttp);
VOID IssProcessMldsVlanMcastFwdTablePage(tHttp *pHttp);
VOID IssProcessMldsVlanRouterTablePage (tHttp * pHttp);
VOID IssProcessMldsVlanPage (tHttp * pHttp);
VOID IssProcessMldsVlanPageGet (tHttp * pHttp);
VOID IssProcessMldsVlanPageSet (tHttp * pHttp);
VOID IssProcessMldsMcastRecvPage (tHttp * pHttp);
VOID IssProcessMldsIntfConfPage (tHttp * pHttp);
VOID IssProcessMldsIntfConfPageGet (tHttp * pHttp);
VOID IssProcessMldsIntfConfPageSet (tHttp * pHttp);
VOID IssProcessMldsRtrPortConfPage (tHttp * pHttp);
VOID IssProcessMldsRtrPortConfPageGet (tHttp * pHttp);
VOID IssProcessMldsRtrPortConfPageSet (tHttp * pHttp);
#endif
VOID IssProcessSnmpTargetAddressPage (tHttp * pHttp);
VOID IssProcessSnmpTargetAddressPageGet (tHttp * pHttp);
VOID IssProcessSnmpTargetAddressPageSet (tHttp * pHttp);
VOID IssProcessSnmpFilterProfilePage (tHttp * pHttp);
VOID IssProcessSnmpFilterProfilePageGet (tHttp * pHttp);
VOID IssProcessSnmpCommunityPage (tHttp * pHttp);
VOID IssProcessSnmpCommunityPageGet (tHttp * pHttp);
VOID IssProcessSnmpHomePageGet (tHttp * pHttp);
VOID IssProcessSnmpHomePageSet (tHttp * pHttp);
VOID IssProcessSnmpAgentxPageGet (tHttp * pHttp);
VOID IssProcessSnmpAgentxPageSet (tHttp * pHttp);
VOID IssProcessSnmpAgentStatsPage (tHttp * pHttp);
VOID IssProcessSnmpAgentStatsPageGet (tHttp * pHttp);
VOID IssProcessSnmpAgentxStatsPage (tHttp * pHttp);
VOID IssProcessSnmpAgentxStatsPageGet (tHttp * pHttp);

VOID IssProcessErasePage (tHttp *pHttp);
VOID IssProcessErasePageGet (tHttp *pHttp);
VOID IssProcessErasePageSet (tHttp *pHttp);
VOID IssProcessRemoteRestorePage (tHttp *pHttp);
VOID IssProcessRemoteRestoreGet (tHttp *pHttp);
VOID IssProcessRemoteRestoreSet (tHttp *pHttp);
VOID IssProcessNvramSettingsPage (tHttp * pHttp);
VOID IssProcessNvramSettingsPageGet (tHttp * pHttp);
VOID IssProcessNvramSettingsPageSet (tHttp * pHttp);

VOID
IssPrintIp6AddressFourByteFormat(UINT1 *pIp6Addr, UINT1 *pu1Temp);
VOID
IssPrintIp6Address (UINT1 *pIp6Addr, UINT1 *pu1Temp);

VOID IssProcessIpv6IntfConfPage (tHttp *pHttp);
VOID IssProcessIpv6IntfConfGet (tHttp * pHttp);
VOID IssProcessIpv6IntfConfSet (tHttp * pHttp);

VOID IssProcessIpv6IntfForwdPage (tHttp *pHttp);
VOID IssProcessIpv6IntfForwdGet (tHttp * pHttp);
VOID IssProcessIpv6IntfForwdSet (tHttp * pHttp);

VOID IssProcessIpv6RouteConfPage (tHttp *pHttp);
VOID IssProcessIpv6RouteConfGet (tHttp *pHttp);
VOID IssProcessIpv6RouteConfSet (tHttp *pHttp);

VOID IssProcessIpv6NDConfPage (tHttp *pHttp);
VOID IssProcessIpv6NDConfGet (tHttp *pHttp);
VOID IssProcessIpv6NDConfSet (tHttp *pHttp);

VOID IssProcessIpv6PrefixConfPage (tHttp *pHttp);
VOID IssProcessIpv6PrefixConfGet (tHttp *pHttp);
VOID IssProcessIpv6PrefixConfSet (tHttp *pHttp);

VOID IssProcessIpv6PolicyPrefixConfPage(tHttp *pHttp);
VOID IssProcessIpv6PolicyPrefixConfGet(tHttp *pHttp);
VOID IssProcessIpv6PolicyPrefixConfSet(tHttp *pHttp);

VOID IssProcessIpv6InterfaceZoneConfPage(tHttp *pHttp);
VOID IssProcessIpv6InterfaceZoneConfGet(tHttp *pHttp);
VOID IssProcessIpv6InterfaceZoneConfSet(tHttp * pHttp);

VOID IssProcessIpv6ScopeZoneConfPage(tHttp *pHttp);
VOID IssProcessIpv6ScopeZoneConfGet(tHttp *pHttp);
VOID IssProcessIpv6ScopeZoneConfSet(tHttp *pHttp);



VOID IssProcessAddrConfPage (tHttp *pHttp);
VOID IssProcessTnlConfPage (tHttp * pHttp);
VOID IssProcessAddrConfInfoGet (tHttp * pHttp);
VOID IssProcessAddrConfSet (tHttp * pHttp);
VOID IssProcessTnlConfInfoGet (tHttp * pHttp);
VOID IssProcessTnlConfSet (tHttp * pHttp);
INT4 IssSetIfTunlParams(tHttp * pHttp, UINT4 u4IfIndex, tSNMP_OCTET_STRING_TYPE *TnlIfAlias);
VOID IssDeleteIfTunnelandTnlParams (tHttp * pHttp, UINT4 u4IfIndex);
VOID IssSetIfIp6Addr (tHttp * pHttp, UINT4 u4IfIndex);
VOID IssDeleteIfIp6Addr (tHttp * pHttp, UINT4 u4IfIndex);
extern UINT1 CfaGddGetPhyAndLinkStatus (UINT2);
#ifdef PVRST_WANTED
VOID IssProcessPvrstInstPortStatusPage (tHttp *pHttp);
VOID IssProcessPvrstInstPortConfPage (tHttp *pHttp);
VOID IssProcessPvrstInstPortConfPageGet (tHttp *pHttp);
VOID IssProcessPvrstInstPortConfPageSet (tHttp *pHttp);
VOID IssProcessPvrstInstBrigConfPage (tHttp * pHttp);
VOID IssProcessPvrstInstBrigConfPageSet(tHttp *pHttp);
VOID IssProcessPvrstInstBrigConfPageGet(tHttp *pHttp);
VOID IssProcessPvrstInfoPage (tHttp *pHttp);
VOID IssProcessPvrstInstInfoPage (tHttp * pHttp);
VOID IssProcessPvrstInstPortInfoPage (tHttp * pHttp);
VOID IssProcessPvrstPortInfoPage (tHttp * pHttp);
VOID IssProcessPvrstPortConfPage (tHttp * pHttp);
VOID IssProcessPvrstPortConfPageGet (tHttp * pHttp);
VOID IssProcessPvrstPortConfPageSet (tHttp * pHttp);
VOID IssPrintPvrstCurrentContext (tHttp *pHttp);
VOID IssProcessPvrstBasicConfPage (tHttp * pHttp);
VOID IssProcessPvrstBasicConfPageSet (tHttp * pHttp);
VOID IssProcessPvrstBasicConfPageGet (tHttp * pHttp);
#endif

#ifdef ELMI_WANTED
VOID IssProcessElmiBasicStatsPage (tHttp * pHttp);
VOID IssProcessElmiPortStatsPage (tHttp * pHttp);
VOID IssProcessElmiErrorStatsPage (tHttp * pHttp);
VOID IssProcessElmiPortConfPageGet (tHttp * pHttp);
VOID IssProcessElmiPortConfPageSet (tHttp * pHttp);
VOID IssProcessElmiPortConfPage (tHttp * pHttp);
#endif

#ifdef ELPS_WANTED
VOID IssPrintAvailablePgId (tHttp * pHttp);
VOID IssProcessElpsPGinfoPage (tHttp * pHttp);
VOID IssProcessElpsPGinfoPageGet (tHttp * pHttp);
VOID IssProcessElpsPGinfoPageSet (tHttp * pHttp);
VOID IssProcessElpsPgCfmPage (tHttp * pHttp);
VOID IssProcessElpsPgCfmPageGet (tHttp * pHttp);
VOID IssProcessElpsPgCfmPageSet (tHttp * pHttp);
VOID IssProcessElpsPGServicePage (tHttp * pHttp);
VOID IssProcessElpsPGServicePageGet (tHttp * pHttp);
VOID IssProcessElpsPGServicePageSet (tHttp * pHttp);
#endif

VOID IssPrintAvailableL3Contexts (tHttp *pHttp, UINT1*);

/*SI MI Compatability externs*/
extern INT4 VlanSelectContext (UINT4);
extern INT4 VlanReleaseContext (VOID);
   
extern INT4 AstSelectContext (UINT4);
extern INT4 AstReleaseContext (VOID);

/* Protocol related externs */

/* VRF_WANTED*/
/* VCM externs*/
extern INT1 nmhGetFirstIndexFsVcmIfMappingTable (INT4 *pi4FsVcmIfIndex);
extern INT1
nmhGetNextIndexFsVcmIfMappingTable (INT4 i4FsVcmIfIndex, INT4 *pi4NextFsVcmIfIndex);
extern INT1 nmhGetFsVcId (INT4 i4FsVcmIfIndex, INT4 *pi4RetValFsVcId);
extern INT1 nmhGetFsVcIfRowStatus (INT4 i4FsVcmIfIndex, INT4 *pi4RetValFsVcIfRowStatus);
extern INT1
nmhGetFsVcL2ContextId (INT4 i4FsVcmIfIndex, INT4 *pi4RetValFsVcL2ContextId);
extern INT1 nmhGetDot1dDeviceCapabilities (tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFirstIndexDot1qForwardAllTable (UINT4 *);
extern INT1 nmhGetNextIndexDot1qForwardAllTable (UINT4 , UINT4 *);
extern INT1 nmhGetDot1qForwardAllPorts (UINT4 ,tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetDot1qForwardAllStaticPorts (UINT4 ,
            tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetDot1qForwardAllForbiddenPorts (UINT4 ,
            tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhSetDot1qForwardAllStaticPorts (UINT4  ,
            tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetDot1qForwardAllForbiddenPorts (UINT4  ,
            tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2Dot1qForwardAllStaticPorts (UINT4 *  ,UINT4,
            tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2Dot1qForwardAllForbiddenPorts (UINT4 *  ,UINT4  ,
            tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetDot1qForwardUnregisteredPorts (UINT4 ,
            tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetDot1qForwardUnregisteredStaticPorts (UINT4 ,
            tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetDot1qForwardUnregisteredForbiddenPorts (UINT4 ,
            tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhSetDot1qForwardUnregisteredStaticPorts (UINT4  ,
            tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetDot1qForwardUnregisteredForbiddenPorts (UINT4  ,
            tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2Dot1qForwardUnregisteredStaticPorts (UINT4 *  ,UINT4  ,
            tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2Dot1qForwardUnregisteredForbiddenPorts (UINT4 *  ,UINT4  ,
            tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFirstIndexIfTable (INT4 *);
extern INT1 nmhGetIfAlias (INT4 ,tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhTestv2IfIpAddrAllocMethod (UINT4 *, INT4 ,INT4);
extern INT1 nmhGetIfIpAddrAllocProtocol (INT4 , INT4 *);
extern INT1 nmhTestv2IfIpAddrAllocProtocol (UINT4 *, INT4 , INT4);
extern INT4 IfIpAddrAllocProtocolSet (tSnmpIndex *, tRetVal * );
extern INT1 nmhGetIfIpBroadcastAddr (INT4 ,UINT4 *);
extern INT1 nmhGetNextIndexIfTable (INT4 , INT4 *);
extern INT4 IssGetIncrSaveFlagnFromNvRam (VOID);
extern INT4 IssGetAutoFlagnFromNvRam (VOID);
extern INT1 nmhGetIssFirmwareVersion (tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetIssAclTrafficSeperationCtrl (INT4 *);
extern INT1 nmhGetIssHardwareVersion (tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetSysUpTime (UINT4 *);
extern INT1 nmhGetIssSwitchName (tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetIssSwitchDate (tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetSysContact (tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetSysLocation (tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetIssDownloadStatus (INT4 *);
extern INT1 nmhSetIssSwitchName (tSNMP_OCTET_STRING_TYPE * pSetValSysName);
extern INT1 nmhSetIssSwitchDate (tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetSysContact (tSNMP_OCTET_STRING_TYPE * pSetValSysName);
extern INT1 nmhSetSysLocation (tSNMP_OCTET_STRING_TYPE * pSetValSysName);
extern VOID IssProcessGet (tHttp * pHttp);
extern INT4 IssProcessPost (tHttp * pHttp);
extern INT1 nmhGetFirstIndexDot1qVlanCurrentTable (UINT4 *pu4Dot1qVlanTimeMark,
                                                   UINT4 *pu4Dot1qVlanIndex);

extern INT1 nmhGetNextIndexDot1qVlanCurrentTable (UINT4 u4Dot1qVlanTimeMark,
                                      UINT4 *pu4NextDot1qVlanTimeMark,
                                      UINT4 u4Dot1qVlanIndex,
                                      UINT4 *pu4NextDot1qVlanIndex);

extern INT1 nmhGetIfMainRowStatus (INT4 i4IfMainIndex, 
                                   INT4 *pi4RetValIfMainRowStatus);
extern INT1 nmhTestv2IfMainAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhSetIfMainAdminStatus ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetIfIpAddrAllocMethod ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhTestv2IfIpAddr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhSetIfIpAddr ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhTestv2IfIpSubnetMask ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhSetIfIpSubnetMask ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhTestv2IfIpBroadcastAddr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhSetIfIpBroadcastAddr ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhGetFirstIndexIpAddrTable (UINT4 *pu4IpAdEntAddr);
extern INT1 nmhGetIpAdEntIfIndex (UINT4 u4IpAdEntAddr, INT4 *pi4RetValIpAdEntIfIndex);
extern INT4  CfaValidateIfNum (UINT4 u4IfType, INT1 *pi1IfNum, INT4 *pi4SlotNum,
                               UINT4 *pu4RetIfNum);
extern INT1 nmhGetIpAdEntNetMask (UINT4 u4IpAdEntAddr, UINT4 *pu4RetValIpAdEntNetMask);
extern INT1 nmhGetIpAdEntBcastAddr (UINT4 u4IpAdEntAddr, INT4 *pi4RetValIpAdEntBcastAddr);
extern INT1 nmhGetNextIndexIpAddrTable (UINT4 u4IpAdEntAddr, UINT4 *pu4NextIpAdEntAddr);
extern INT1 nmhSetIfMainRowStatus ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhTestv2IfAlias ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetIfAlias ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhGetIfPhysAddress ARG_LIST ((INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pRetValIfPhysAddress));
extern INT1 nmhTestv2IfMainType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhSetIfMainType ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhTestv2IfMainRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2IfMainExtSubType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhSetIfMainExtSubType ARG_LIST((INT4  ,INT4 ));
extern INT4 CfaTestIfIpAddrAndIfIpSubnetMask PROTO ((INT4,UINT4,UINT4));
extern INT1 nmhTestv2IssUploadLogTransferMode ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhSetIssUploadLogTransferMode ARG_LIST((INT4 ));
extern INT1 nmhTestv2IssUploadLogFileToIpvx (UINT4 *pu4ErrorCode,
                         tSNMP_OCTET_STRING_TYPE *pTestValIssUploadLogFileToIp);
extern INT1 nmhTestv2IssLogFileName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2IssUploadLogUserName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2IssUploadLogPasswd ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetIssUploadLogUserName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetIssUploadLogPasswd ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

extern INT1 nmhGetIssUploadLogUserName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

extern INT1 nmhGetIssUploadLogPasswd ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetIfACPortIdentifier (INT4 ,INT4 *);
extern INT1 nmhGetIfACCustomerVlan (INT4 ,INT4 *);
extern INT1 nmhGetNextIndexIfACTable(INT4 ,INT4 *);
extern INT1 nmhGetFirstIndexIfACTable(INT4 *);
extern INT1 nmhTestv2IfACPortIdentifier (UINT4 * ,INT4 ,INT4 );
extern INT1 nmhSetIfACPortIdentifier(INT4 ,INT4 );
extern INT1 nmhTestv2IfACCustomerVlan (UINT4 *,INT4 ,INT4 );
extern INT1 nmhSetIfACCustomerVlan(INT4 , INT4 );
extern INT4 IfACPortIdentifierSet(tSnmpIndex *, tRetVal *);
extern INT4 IfACCustomerVlanSet(tSnmpIndex *, tRetVal *);

extern INT1 nmhGetIssUploadLogTransferMode ARG_LIST((INT4 *));
extern INT1 nmhSetIssUploadLogFileToIpvx (tSNMP_OCTET_STRING_TYPE *
                            pSetValIssUploadLogFileToIp);
extern INT1 nmhSetIssInitiateUlLogFile ARG_LIST((INT4 ));
extern INT1 nmhGetIssLogFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetIssUlRemoteLogFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhSetIssUlRemoteLogFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhTestv2IssUlRemoteLogFileName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2IssInitiateUlLogFile ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2IssConfigSaveTransferMode ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhSetIssConfigSaveTransferMode ARG_LIST((INT4 ));
extern INT1 nmhTestv2IssConfigSaveUserName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetIssConfigSaveUserName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2IssConfigSavePassword ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetIssConfigSavePassword ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhGetIssConfigSaveTransferMode ARG_LIST((INT4 *));
extern INT1 nmhGetIssConfigSaveUserName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetIssConfigSavePassword ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetIssConfigSaveIpAddrType ARG_LIST ((INT4 *pi4RetValIssConfigSaveIpAddrType));
extern INT1 nmhSetIssConfigSaveIpAddrType ARG_LIST ((INT4 i4SetValIssConfigSaveIpAddrType));
extern INT1 nmhSetIssConfigRestoreIpAddrType(INT4 i4SetValIssConfigRestoreIpAddrType);
extern INT1 nmhTestv2IssDlImageFromIpvx (UINT4 *pu4ErrorCode,
                           tSNMP_OCTET_STRING_TYPE *pTestValIssDlImageFromIp);
extern INT1 nmhSetIssDlImageFromIpAddrType(INT4 i4SetValIssDlImageFromIpAddrType);
extern INT1 nmhSetIssUploadLogFileToIpAddrType(INT4 i4SetValIssUploadLogFileToIpAddrType);
extern INT1 nmhTestv2IssConfigRestoreIpAddrType(UINT4 *pu4ErrorCode ,
                                    INT4 i4TestValIssConfigRestoreIpAddrType);
extern INT1 nmhTestv2IssUploadLogFileToIpAddrType(UINT4 *pu4ErrorCode ,
                                    INT4 i4TestValIssUploadLogFileToIpAddrType);
extern INT1 nmhTestv2IssConfigSaveIpAddrType(UINT4 *pu4ErrorCode ,
                                 INT4 i4TestValIssConfigSaveIpAddrType);
extern INT1 nmhGetIssDlImageFromIpAddrType(INT4 *pi4RetValIssDlImageFromIpAddrType);
extern INT1 nmhTestv2IssDlImageFromIpAddrType(UINT4 *pu4ErrorCode ,
                                  INT4 i4TestValIssDlImageFromIpAddrType);
extern INT1 nmhTestv2IssDlImageName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2IssDownLoadTransferMode ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhSetIssDownLoadTransferMode ARG_LIST((INT4 ));
extern INT1 nmhTestv2IssDownLoadUserName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2IssDownLoadPassword ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetIssDownLoadPassword ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhGetIssDownLoadTransferMode ARG_LIST((INT4 *));
extern INT1 nmhGetIssDlImageFromIpvx (tSNMP_OCTET_STRING_TYPE * pRetValIssDlImageFromIp);
extern INT1 nmhGetIssDlImageName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetIssDownLoadUserName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetIssDownLoadPassword ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhTestv2IssConfigRestoreFileName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2IssConfigRestoreIpvxAddr(UINT4 *pu4ErrorCode ,
                     tSNMP_OCTET_STRING_TYPE *pTestValIssConfigRestoreIpAddr);
extern INT1 nmhTestv2IssInitiateConfigRestore ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhGetIssConfigRestoreIpvxAddr(tSNMP_OCTET_STRING_TYPE *
                             pRetValIssConfigRestoreIpAddr);
extern INT1 nmhGetIssConfigRestoreIpAddrType(INT4 *pi4RetValIssConfigRestoreIpAddrType);
extern INT1 nmhGetIssUploadLogFileToIpAddrType(INT4 *pi4RetValIssUploadLogFileToIpAddrType);
extern INT1 nmhGetIssUploadLogFileToIpvx ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhTestv2IssInitiateDlImage ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhSetIssDownLoadUserName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetIssDlImageFromIpvx (tSNMP_OCTET_STRING_TYPE *pSetValIssDlImageFromIp);
extern INT1 nmhSetIssDlImageName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhGetIssConfigRestoreFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT4 IssDlImageFromIpvxSet(tSnmpIndex *, tRetVal *);
extern INT4 IssDlImageNameSet(tSnmpIndex *, tRetVal *);
extern INT4 IssInitiateDlImageSet(tSnmpIndex *, tRetVal *);
extern INT4 IssConfigRestoreFileNameSet(tSnmpIndex *, tRetVal *);
extern INT4 IssConfigRestoreIpAddrSet(tSnmpIndex *, tRetVal *);
extern INT4 IssInitiateConfigRestoreSet(tSnmpIndex *, tRetVal *);
extern INT4 IssInitiateUlLogFileSet(tSnmpIndex *, tRetVal *);
extern INT4 IssUploadLogFileToIpvxSet(tSnmpIndex *, tRetVal *);
extern INT4 IssLogFileNameSet(tSnmpIndex *, tRetVal *);

extern INT1 nmhGetFirstIndexFsipv6RouteTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,
     INT4 *, INT4 * ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetNextIndexFsipv6RouteTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *,
     tSNMP_OCTET_STRING_TYPE *  ,INT4 ,INT4 * ,INT4 ,
    INT4 * ,tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsipv6RouteIfIndex ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,
    INT4  ,INT4  ,tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1 nmhGetFsipv6RouteMetric ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ,
    INT4  ,tSNMP_OCTET_STRING_TYPE *,UINT4 *));

extern INT1 nmhTestv2Fsipv6RouteIfIndex ARG_LIST((UINT4 *, tSNMP_OCTET_STRING_TYPE *,
     INT4 ,INT4 ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhTestv2Fsipv6RouteAddrType ARG_LIST((UINT4 *, tSNMP_OCTET_STRING_TYPE *,
     INT4 ,INT4 ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhSetFsipv6RouteIfIndex ARG_LIST((tSNMP_OCTET_STRING_TYPE *, INT4,
     INT4 ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhSetFsipv6RouteAddrType ARG_LIST((tSNMP_OCTET_STRING_TYPE *, INT4,
     INT4 ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhTestv2Fsipv6RouteMetric ARG_LIST((UINT4 * ,tSNMP_OCTET_STRING_TYPE *,
     INT4 ,INT4 ,tSNMP_OCTET_STRING_TYPE * ,UINT4 ));
extern INT1 nmhSetFsipv6RouteMetric ARG_LIST((tSNMP_OCTET_STRING_TYPE *, INT4 ,
     INT4 ,tSNMP_OCTET_STRING_TYPE * ,UINT4 ));
extern INT1 nmhTestv2Fsipv6RouteAdminStatus ARG_LIST((UINT4 * ,tSNMP_OCTET_STRING_TYPE *,
     INT4 ,INT4 ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhSetFsipv6RouteAdminStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *, INT4 ,
     INT4 ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT4 Fsipv6RouteIfIndexGet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6RouteMetricGet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6RouteAdminStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6RouteIfIndexSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6RouteMetricSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6RouteAdminStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6RouteIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6RouteMetricTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6RouteAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);

extern INT1 nmhGetFirstIndexFsipv6NdLanCacheTable ARG_LIST((INT4 * ,
     tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetNextIndexFsipv6NdLanCacheTable ARG_LIST((INT4 ,INT4 * ,
    tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsipv6NdLanCacheStatus ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1 nmhGetFsipv6NdLanCacheState ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1 nmhGetFsipv6NdLanCacheUseTime ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE *,UINT4 *));
extern INT1 nmhGetFsipv6NdLanCachePhysAddr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ,
      tSNMP_OCTET_STRING_TYPE *));

extern INT4 GetNextIndexFsipv6NdLanCacheTable(tSnmpIndex *, tSnmpIndex *);
extern INT4 Fsipv6NdLanCachePhysAddrGet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6NdLanCacheStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6NdLanCacheStateGet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6NdLanCacheUseTimeGet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6NdLanCachePhysAddrSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6NdLanCacheStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6NdLanCachePhysAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6NdLanCacheTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
extern INT4 Fsipv6NdLanCacheStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 GetNextIndexFsipv6NdWanCacheTable(tSnmpIndex *, tSnmpIndex *);

extern INT1 nmhTestv2Fsipv6NdLanCacheStatus ARG_LIST((UINT4 * ,INT4 ,
      tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhSetFsipv6NdLanCacheStatus ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ,
      INT4 ));
extern INT1 nmhTestv2Fsipv6NdLanCachePhysAddr ARG_LIST((UINT4 * ,INT4 ,tSNMP_OCTET_STRING_TYPE *
      ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsipv6NdLanCachePhysAddr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ,
      tSNMP_OCTET_STRING_TYPE *));

extern INT4 GetNextIndexFsipv6PrefixTable(tSnmpIndex *, tSnmpIndex *);
extern INT4 Fsipv6PrefixProfileIndexGet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6PrefixAdminStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6PrefixProfileIndexSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6PrefixAdminStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6PrefixProfileIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6PrefixAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);

extern INT4 Fsipv6AddrSelPolicyRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6AddrSelPolicyPrecedenceSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6AddrSelPolicyAddrTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6AddrSelPolicyLabelSet(tSnmpIndex *, tRetVal *);
extern INT1 nmhGetIpForwarding ARG_LIST((INT4 *));
extern INT1 nmhGetIfIpAddr PROTO ((INT4, UINT4 *));

extern INT1 nmhGetFirstIndexFsipv6PrefixTable ARG_LIST((INT4 * ,tSNMP_OCTET_STRING_TYPE * ,INT4 *));
extern INT1 nmhGetNextIndexFsipv6PrefixTable ARG_LIST((INT4 ,INT4 * ,tSNMP_OCTET_STRING_TYPE *, 
   tSNMP_OCTET_STRING_TYPE * ,INT4 ,INT4 *));
extern INT1 nmhGetFsipv6PrefixProfileIndex ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE *, INT4 ,INT4 *));
extern INT1 nmhTestv2Fsipv6PrefixProfileIndex ARG_LIST((UINT4 *, INT4, tSNMP_OCTET_STRING_TYPE * ,
      INT4  ,INT4 ));
extern INT1 nmhSetFsipv6PrefixProfileIndex ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ,
      INT4  ,INT4 ));
extern INT1 nmhTestv2Fsipv6PrefixAdminStatus ARG_LIST((UINT4 *, INT4, tSNMP_OCTET_STRING_TYPE * ,
      INT4  ,INT4 ));
extern INT1 nmhSetFsipv6PrefixAdminStatus ARG_LIST((INT4, tSNMP_OCTET_STRING_TYPE * ,INT4 ,INT4 ));
extern INT4 Fsipv6SupportEmbeddedRpSet(tSnmpIndex *, tRetVal *);
extern INT1 nmhGetFsipv6SupportEmbeddedRp ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE *, INT4 ,INT4 *));
extern INT1 nmhTestv2Fsipv6SupportEmbeddedRp ARG_LIST((UINT4 *, INT4, tSNMP_OCTET_STRING_TYPE * ,
      INT4  ,INT4 ));
extern INT1 nmhSetFsipv6SupportEmbeddedRp ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ,
      INT4  ,INT4 ));


extern INT1 nmhGetNextIndexFsipv6AddrSelPolicyTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 *));

extern INT1 nmhGetFirstIndexFsipv6AddrSelPolicyTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 *));

extern INT1 nmhGetFsipv6AddrSelPolicyPrecedence ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

extern INT1 nmhGetFsipv6AddrSelPolicyLabel ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

extern INT1 nmhGetFsipv6AddrSelPolicyAddrType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

extern INT1 nmhSetFsipv6AddrSelPolicyPrecedence ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

extern INT1 nmhSetFsipv6AddrSelPolicyLabel ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

extern INT1 nmhSetFsipv6AddrSelPolicyAddrType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

extern INT1 nmhSetFsipv6AddrSelPolicyRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

extern INT1 nmhTestv2Fsipv6AddrSelPolicyPrecedence ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

extern INT1 nmhTestv2Fsipv6AddrSelPolicyLabel ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

extern INT1 nmhTestv2Fsipv6AddrSelPolicyAddrType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

extern INT1 nmhTestv2Fsipv6AddrSelPolicyRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  ,INT4 ));

/*If Scope Zone Map Table Start*/
extern INT1 nmhGetFirstIndexFsipv6IfScopeZoneMapTable ARG_LIST((INT4 *));

extern INT1 nmhGetNextIndexFsipv6IfScopeZoneMapTable ARG_LIST((INT4 , INT4 *));

extern INT1
nmhGetFsipv6ScopeZoneIndexInterfaceLocal ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsipv6ScopeZoneIndexLinkLocal ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsipv6ScopeZoneIndex3 ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsipv6ScopeZoneIndexAdminLocal ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsipv6ScopeZoneIndexSiteLocal ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern  INT1
nmhGetFsipv6ScopeZoneIndex6 ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsipv6ScopeZoneIndex7 ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsipv6ScopeZoneIndexOrganizationLocal ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsipv6ScopeZoneIndex9 ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsipv6ScopeZoneIndexA ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsipv6ScopeZoneIndexB ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsipv6ScopeZoneIndexC ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsipv6ScopeZoneIndexD ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsipv6ScopeZoneIndexE ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhSetFsipv6ScopeZoneIndexInterfaceLocal ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsipv6ScopeZoneIndexLinkLocal ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsipv6ScopeZoneIndex3 ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsipv6ScopeZoneIndexAdminLocal ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsipv6ScopeZoneIndexSiteLocal ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsipv6ScopeZoneIndex6 ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsipv6ScopeZoneIndex7 ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsipv6ScopeZoneIndexOrganizationLocal ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsipv6ScopeZoneIndex9 ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsipv6ScopeZoneIndexA ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsipv6ScopeZoneIndexB ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsipv6ScopeZoneIndexC ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsipv6ScopeZoneIndexD ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsipv6ScopeZoneIndexE ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsipv6IfScopeZoneRowStatus ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhTestv2Fsipv6ScopeZoneIndexInterfaceLocal ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2Fsipv6ScopeZoneIndexLinkLocal ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2Fsipv6ScopeZoneIndex3 ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2Fsipv6ScopeZoneIndexAdminLocal ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2Fsipv6ScopeZoneIndexSiteLocal ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2Fsipv6ScopeZoneIndex6 ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2Fsipv6ScopeZoneIndex7 ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2Fsipv6ScopeZoneIndexOrganizationLocal ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2Fsipv6ScopeZoneIndex9 ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2Fsipv6ScopeZoneIndexA ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2Fsipv6ScopeZoneIndexB ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2Fsipv6ScopeZoneIndexC ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2Fsipv6ScopeZoneIndexD ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2Fsipv6ScopeZoneIndexE ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2Fsipv6IfScopeZoneRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT4 Fsipv6ScopeZoneIndexInterfaceLocalSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6ScopeZoneIndexLinkLocalSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6ScopeZoneIndex3Set(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6ScopeZoneIndexAdminLocalSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6ScopeZoneIndexSiteLocalSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6ScopeZoneIndex6Set(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6ScopeZoneIndex7Set(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6ScopeZoneIndexOrganizationLocalSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6ScopeZoneIndex9Set(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6ScopeZoneIndexASet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6ScopeZoneIndexBSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6ScopeZoneIndexCSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6ScopeZoneIndexDSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6ScopeZoneIndexESet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6IfScopeZoneRowStatusSet(tSnmpIndex *, tRetVal *);

/*If Scope Zone Map Table Start*/

/* Scope Zone Table Start */
INT1 nmhGetFirstIndexFsipv6ScopeZoneTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetNextIndexFsipv6ScopeZoneTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsipv6ScopeZoneIndex ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsipv6ScopeZoneCreationStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsipv6ScopeZoneInterfaceList ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsipv6IsDefaultScopeZone ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsipv6IsDefaultScopeZone ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Fsipv6IsDefaultScopeZone ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT4 Fsipv6IsDefaultScopeZoneSet(tSnmpIndex *, tRetVal *);

VOID IssPrintIpv6Zones (tHttp *pHttp);

VOID IssConvertToL3IfList(tHttp * pHttp,UINT1* LocalInterfaceList);



/* Scope Zone Table End */


extern INT1 nmhGetFirstIndexFsipv6IfTable ARG_LIST((INT4 *));

extern INT1 nmhGetNextIndexFsipv6IfTable ARG_LIST((INT4 , INT4 *));

extern INT1 nmhGetFsipv6IfAdminStatus ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsipv6IfRouterAdvFlags ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhTestv2Fsipv6IfRouterAdvFlags ARG_LIST(( UINT4* ,INT4, INT4)); 

extern INT1 nmhSetIpv6RouterAdvertManagedFlag ARG_LIST((INT4 ,INT4 ));

extern INT1 nmhSetIpv6RouterAdvertOtherConfigFlag ARG_LIST((INT4 ,INT4 ));

extern INT1 nmhGetFsipv6IfOperStatus ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsipv6IfRouterAdvStatus ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetIpv6RouterAdvertRowStatus ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsipv6IfHopLimit ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsipv6IfDefRouterTime ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsipv6IfReachableTime ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsipv6IfRetransmitTime ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsipv6IfPrefixAdvStatus ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsipv6IfMinRouterAdvTime ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetIpv6RouterAdvertMinInterval ARG_LIST((INT4 ,UINT4 *));

extern INT1 nmhGetFsipv6IfMaxRouterAdvTime ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsipv6IfDADRetries ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsipv6IfIcmpErrInterval ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsipv6IfForwarding ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsipv6IfRoutingStatus ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsipv6IfIcmpTokenBucketSize ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsipv6IfDestUnreachableMsg ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetIpv6IfIdentifier ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1 nmhGetFsipv6IfAdvSrcLLAdr ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsipv6IfAdvIntOpt ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsipv6IfNDProxyAdminStatus ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsipv6IfNDProxyMode ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsipv6IfNDProxyOperStatus ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsipv6IfNDProxyUpStream ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhSetFsipv6IfAdminStatus ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsipv6IfRouterAdvStatus ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetIpv6RouterAdvertSendAdverts ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetIpv6RouterAdvertRowStatus ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetIpv6RouterAdvertDefaultLifetime ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetIpv6RouterAdvertReachableTime ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsipv6IfHopLimit ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetIpv6RouterAdvertCurHopLimit  ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsipv6IfDefRouterTime ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsipv6IfReachableTime ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsipv6IfRetransmitTime ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetIpv6RouterAdvertRetransmitTime ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsipv6IfPrefixAdvStatus ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsipv6IfMinRouterAdvTime ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetIpv6RouterAdvertMinInterval ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsipv6IfMaxRouterAdvTime ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetIpv6RouterAdvertMaxInterval ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsipv6IfDADRetries ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsipv6IfIcmpErrInterval ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsipv6IfForwarding ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsipv6IfIcmpTokenBucketSize ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsipv6IfDestUnreachableMsg ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetIpv6IfIdentifier ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1 nmhSetFsipv6IfAdvSrcLLAdr ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsipv6IfAdvIntOpt ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsipv6IfNDProxyAdminStatus ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsipv6IfNDProxyMode ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsipv6IfNDProxyUpStream ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhTestv2Fsipv6IfAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Fsipv6IfRouterAdvStatus ARG_LIST((UINT4 *, INT4, INT4 ));

extern INT1 nmhTestv2Ipv6RouterAdvertSendAdverts ARG_LIST((UINT4 *, INT4,INT4 ));

extern INT1 nmhTestv2Ipv6RouterAdvertRowStatus ARG_LIST((UINT4 *, INT4, INT4 ));

extern INT1 nmhTestv2Ipv6RouterAdvertDefaultLifetime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Ipv6RouterAdvertReachableTime ARG_LIST((UINT4 *,INT4 ,INT4 ));

extern INT1 nmhTestv2Fsipv6IfHopLimit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Ipv6RouterAdvertCurHopLimit ARG_LIST((UINT4 *, INT4, INT4 ));

extern INT1 nmhTestv2Fsipv6IfDefRouterTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Fsipv6IfReachableTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Fsipv6IfRetransmitTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Ipv6RouterAdvertRetransmitTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Fsipv6IfPrefixAdvStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Fsipv6IfMinRouterAdvTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Ipv6RouterAdvertMinInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Fsipv6IfMaxRouterAdvTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Ipv6RouterAdvertMaxInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Fsipv6IfDADRetries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Fsipv6IfIcmpErrInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Fsipv6IfForwarding ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Fsipv6IfIcmpTokenBucketSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Fsipv6IfDestUnreachableMsg ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Ipv6IfIdentifier ARG_LIST((UINT4 * ,INT4  ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1 nmhTestv2Fsipv6IfAdvSrcLLAdr ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Fsipv6IfAdvIntOpt ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Fsipv6IfNDProxyAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Fsipv6IfNDProxyMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Fsipv6IfNDProxyUpStream ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT4 Fsipv6IfAdminStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6IfRouterAdvStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6IfRouterAdvFlagsSet (tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6IfHopLimitSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6IfDefRouterTimeSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6IfReachableTimeSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6IfRetransmitTimeSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6IfPrefixAdvStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6IfMinRouterAdvTimeSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6IfMaxRouterAdvTimeSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6IfDADRetriesSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6IfForwardingSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6IfAdvSrcLLAdrSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6IfAdvIntOptSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6IfNDProxyAdminStatusSet (tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6IfNDProxyModeSet (tSnmpIndex *, tRetVal *); 
extern INT4 Fsipv6IfNDProxyUpStreamSet (tSnmpIndex *, tRetVal *);

PUBLIC INT1 nmhGetIfSecondaryIpRowStatus ARG_LIST ((INT4 ,UINT4,INT4 *));
extern INT1 nmhTestv2IfMainExtMacAddress (UINT4 *pu4ErrorCode, INT4 i4IfMainIndex, tMacAddr TestValIfMainExtMacAddress);

extern INT1 nmhSetIfMainExtMacAddress (INT4 i4IfMainIndex, tMacAddr TestValIfMainExtMacAddress);


extern INT4
IfMainExtMacAddressSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData);

PUBLIC INT1
nmhTestv2IfSecondaryIpRowStatus ARG_LIST ((UINT4 *pu4ErrorCode,
                                 INT4 i4IfMainIndex,
                                 UINT4 u4IfSecondaryIpAddress,
                                 INT4 i4TestValIfSecondaryIpRowStatus));
PUBLIC INT1
nmhSetIfSecondaryIpRowStatus ARG_LIST ((INT4 i4IfMainIndex,
                                 UINT4 u4IfSecondaryIpAddress,
                                 INT4 i4SetValIfSecondaryIpRowStatus));

PUBLIC INT1
nmhTestv2IfSecondaryIpSubnetMask ARG_LIST ((UINT4 *pu4ErrorCode, 
                                 INT4 i4IfMainIndex, 
                                 UINT4 u4IfSecondaryIpAddress, 
                                 UINT4 u4TestValIfSecondaryIpSubnetMask));

extern INT1                                                                                                              nmhGetIfIpAddrAllocMethod (INT4 i4IfMainIndex,
                           INT4 *pi4RetValIfIpAddrAllocMethod);
PUBLIC INT1
nmhGetIfSecondaryIpSubnetMask ARG_LIST ((INT4 i4IfMainIndex,
                                 UINT4 u4IfSecondaryIpAddress,
                                 UINT4 *pu4RetValIfSecondaryIpSubnetMask));

PUBLIC INT1
nmhGetIfSecondaryIpBroadcastAddr ARG_LIST ((INT4 i4IfMainIndex,
                                 UINT4 u4IfSecondaryIpAddress,
                                 UINT4 *pu4RetValIfSecondaryIpBroadcastAddr));

PUBLIC INT1
nmhSetIfSecondaryIpSubnetMask ARG_LIST ((INT4 i4IfMainIndex,
                               UINT4 u4IfSecondaryIpAddress,
                               UINT4 u4SetValIfSecondaryIpSubnetMask));

PUBLIC INT1
nmhTestv2IfSecondaryIpBroadcastAddr ARG_LIST((UINT4 *pu4ErrorCode,
                                INT4 i4IfMainIndex,
                                UINT4 u4IfSecondaryIpAddress,
                                UINT4 u4TestValIfSecondaryIpBroadcastAddr));

PUBLIC INT1
nmhSetIfSecondaryIpBroadcastAddr ARG_LIST((INT4 i4IfMainIndex,
                                  UINT4 u4IfSecondaryIpAddress,
                                  UINT4 u4SetValIfSecondaryIpBroadcastAddr));

PUBLIC INT1
nmhGetFirstIndexIfSecondaryIpAddressTable ARG_LIST ((INT4 *,
                                                     UINT4 *));

PUBLIC INT1              
nmhGetNextIndexIfSecondaryIpAddressTable ARG_LIST ((INT4,INT4 *,UINT4,UINT4 *));

PUBLIC INT1
nmhTestv2IfAdminStatus ARG_LIST ((UINT4 *, INT4, INT4));

PUBLIC INT1
nmhSetIfAdminStatus ARG_LIST ((INT4, INT4));
   
extern INT4 IfAdminStatusSet (tSnmpIndex *, tRetVal *);
extern INT4 IfLinkUpDownTrapEnableSet (tSnmpIndex *, tRetVal *);
extern INT4 IfMainMtuSet(tSnmpIndex *, tRetVal *);
extern INT4 IfIpAddrSet(tSnmpIndex *, tRetVal *);
extern INT4 IfIpSubnetMaskSet(tSnmpIndex *, tRetVal *);
extern INT4 IfIpBroadcastAddrSet(tSnmpIndex *, tRetVal *);
extern INT4 IfIpAddrAllocMethodSet(tSnmpIndex *, tRetVal *);
extern INT4 IfSecondaryIpSubnetMaskSet(tSnmpIndex *, tRetVal *);
extern INT4 IfSecondaryIpBroadcastAddrSet(tSnmpIndex *, tRetVal *);
extern INT4 IfSecondaryIpRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 IfMainRowStatusSet (tSnmpIndex * , tRetVal * );
extern INT4 IfMainAdminStatusSet (tSnmpIndex * , tRetVal * );
extern INT4 IfMainTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 IfMainExtSubTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 IfMainPortRoleSet(tSnmpIndex *, tRetVal *);
extern INT4 IfMainDesigUplinkStatusSet(tSnmpIndex *, tRetVal *); 
extern INT1 nmhGetFirstIndexOspfAreaTable (UINT4 *pu4OspfAreaId);
extern INT1 nmhGetNextIndexOspfAreaTable (UINT4 u4OspfAreaId, UINT4 *pu4NextOspfAreaId);
extern INT1 nmhGetIssAclProvisionMode (INT4 *);
extern INT1 nmhGetIssAclTriggerCommit (INT4 *);
extern INT1 nmhSetIssAclProvisionMode (INT4 );
extern INT1 nmhSetIssAclTriggerCommit (INT4 );
extern INT1 nmhTestv2IssAclProvisionMode (UINT4 *  ,INT4 );
extern INT1 nmhTestv2IssAclTriggerCommit (UINT4 *  ,INT4 );
extern INT1 nmhTestv2IssSwitchName (UINT4 *, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2IssSwitchDate (UINT4 *, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2SysContact (UINT4 *, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2SysLocation (UINT4 *, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2IssLoginAuthentication (UINT4 *, UINT4);
extern INT1 nmhTestv2IssHttpPort (UINT4 *, INT4);
extern INT1 nmhTestv2IssTelnetStatus (UINT4 *, INT4);
extern INT1 nmhTestv2IssMgmtInterfaceRouting (UINT4 *, UINT4);
extern INT1 nmhTestv2IssLoggingOption (UINT4 *, INT4);
extern UINT4 IssGetMgmtPortFromNvRam (VOID);
extern INT1 nmhSetIssLoginAuthentication (UINT4);
extern INT1 nmhSetIssHttpPort (INT4);
extern INT1 nmhSetIssHttpStatus (INT4);
extern INT1 nmhSetIssTelnetStatus (INT4);
extern INT1 nmhSetIssMgmtInterfaceRouting (UINT4);
extern INT1 nmhSetIssLoggingOption (INT4);
extern INT4 IssTelnetStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 IssLoggingOptionSet(tSnmpIndex *, tRetVal *);

extern INT1 nmhSetIssNoCliConsole (UINT4 );
extern UINT4 IssGetCliSerialConsoleFromNvRam (VOID); 
extern INT1 nmhSetIssDefaultVlanId ARG_LIST((INT4 ));
extern INT1 nmhTestv2IssDefaultVlanId ARG_LIST((UINT4 *  ,INT4 ));
extern INT4 MstDeleteInstance (UINT2 );

extern INT4 IfAliasSet (tSnmpIndex *, tRetVal *);
extern INT4 IssDefaultIpAddrCfgModeSet(tSnmpIndex *, tRetVal *);
extern INT4 IssDefaultIpAddrSet(tSnmpIndex *, tRetVal *);
extern INT4 IssDefaultIpSubnetMaskSet(tSnmpIndex *, tRetVal *);
extern INT4 IssDefaultIpAddrAllocProtocolSet(tSnmpIndex *, tRetVal *);
extern INT4 IssSwitchBaseMacAddressSet(tSnmpIndex *, tRetVal *);
extern INT4 IssDefaultInterfaceSet(tSnmpIndex *, tRetVal *);
extern INT4 IssConfigAutoSaveTriggerSet(tSnmpIndex *, tRetVal *);
extern INT4 IssConfigIncrSaveFlagSet(tSnmpIndex *, tRetVal *);
extern INT4 IssConfigRollbackFlagSet(tSnmpIndex *, tRetVal *);
extern INT4 IssNoCliConsoleSet(tSnmpIndex *, tRetVal *);
extern INT4 IssDefaultVlanIdSet(tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 IssMirrorToPortSet(tSnmpIndex *, tRetVal *);
extern INT4 IssSwitchNameSet(tSnmpIndex *, tRetVal *);
extern INT4 IssSwitchDateSet(tSnmpIndex *, tRetVal *);
extern INT4 SysContactSet (tSnmpIndex *, tRetVal *);
extern INT4 SysLocationSet (tSnmpIndex *, tRetVal *);
extern INT4 IssLoginAuthenticationSet(tSnmpIndex *, tRetVal *);
extern INT4 IssHttpPortSet(tSnmpIndex *, tRetVal *);
extern INT4 IssHttpStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 IssIpAuthMgrRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 IssIpAuthMgrPortListSet(tSnmpIndex *, tRetVal *);
extern INT4 IssIpAuthMgrVlanListSet(tSnmpIndex *, tRetVal *);
extern INT4 IssIpAuthMgrOOBPortSet(tSnmpIndex *, tRetVal *);
extern INT4 IssIpAuthMgrAllowedServicesSet(tSnmpIndex *, tRetVal *);
extern INT4 IssConfigSaveOptionSet(tSnmpIndex *, tRetVal *);
extern INT4 IssConfigRestoreOptionSet(tSnmpIndex *, tRetVal *);
extern INT4 IssConfigSaveFileNameSet(tSnmpIndex *, tRetVal *);
extern INT4 IssConfigSaveIpvxAddrSet(tSnmpIndex *, tRetVal *);
extern INT4 IssConfigSaveIpAddrTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 IssDlImageFromIpAddrTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 IssUploadLogFileToIpAddrTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 IssInitiateConfigSaveSet(tSnmpIndex *, tRetVal *);
#ifdef PIM_WANTED
extern INT1 nmhTestv2FsPimCmnInterfaceCompId ARG_LIST((UINT4 *, INT4, INT4, 
                                                           INT4));
extern INT1 nmhSetFsPimCmnInterfaceCompId ARG_LIST((INT4, INT4, INT4));
extern INT1 nmhGetFirstIndexFsPimStdInterfaceTable ARG_LIST((INT4 *, INT4 *));
extern INT1 nmhGetNextIndexFsPimStdInterfaceTable ARG_LIST((INT4, INT4 *, INT4, 
                                                            INT4 *));
extern INT1 nmhGetFsPimStdInterfaceStatus ARG_LIST((INT4, INT4, INT4 *));
extern INT1 nmhGetFsPimStdInterfaceAddress ARG_LIST((INT4, INT4, 
                                                    tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhGetFsPimStdInterfaceNetMaskLen ARG_LIST((INT4, INT4, INT4 *));
extern INT1 nmhGetFsPimStdInterfaceMode ARG_LIST((INT4, INT4, INT4 *));
extern INT1 nmhGetFsPimStdInterfaceDR ARG_LIST((INT4, INT4, 
                                                tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhGetFsPimStdInterfaceHelloInterval ARG_LIST((INT4, INT4, INT4 *));
extern INT1 nmhGetFsPimStdInterfaceJoinPruneInterval ARG_LIST((INT4, INT4, 
                                                               INT4 *));
extern INT1 nmhGetFsPimStdInterfaceCBSRPreference ARG_LIST((INT4, INT4, 
                                                           INT4 *));
extern INT1 nmhTestv2FsPimStdComponentStatus ARG_LIST((UINT4 *, INT4, INT4));
extern INT1 nmhSetFsPimStdComponentStatus ARG_LIST((INT4, INT4));
extern INT1 nmhGetFsPimStdComponentStatus ARG_LIST((INT4, INT4 *));
extern INT1 nmhGetFsPimStdComponentBSRExpiryTime ARG_LIST((INT4, INT4 *));
extern INT1 nmhGetFsPimCmnInterfaceCompId ARG_LIST((INT4, INT4, INT4 *));
extern INT1 nmhTestv2FsPimComponentMode ARG_LIST((UINT4 *, INT4,  INT4));
extern INT1 nmhSetFsPimComponentMode ARG_LIST((INT4, INT4));
extern INT1 nmhGetFsPimComponentMode ARG_LIST((INT4, INT4 *));
extern INT1 nmhTestv2FsPimStdComponentCRPHoldTime ARG_LIST((UINT4 *, INT4, 
                                                            INT4));
extern INT1 nmhSetFsPimStdComponentCRPHoldTime ARG_LIST((INT4, INT4));
extern INT1 nmhGetFsPimStdComponentCRPHoldTime ARG_LIST((INT4, INT4 *));
extern INT1 nmhTestv2FsPimStdComponentScopeZoneName ARG_LIST((UINT4 *, INT4, 
                                                    tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsPimStdComponentScopeZoneName  ARG_LIST((INT4, 
                                                    tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhGetFsPimStdComponentScopeZoneName  ARG_LIST((INT4, 
                                                    tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsPimStdInterfaceStatus ARG_LIST((UINT4 *, INT4, INT4, 
                                                       INT4));
extern INT1 nmhSetFsPimStdInterfaceStatus ARG_LIST((INT4, INT4, INT4));
extern INT1 nmhTestv2FsPimStdInterfaceMode ARG_LIST((UINT4 *, INT4, INT4, 
                                                     INT4));
extern INT1 nmhTestv2FsPimStdInterfaceHelloInterval ARG_LIST((UINT4 *, INT4, 
                                                             INT4, INT4));
extern INT1 nmhTestv2FsPimStdInterfaceJoinPruneInterval ARG_LIST((UINT4 *, INT4,
                                                                  INT4, INT4));
extern INT1 nmhTestv2FsPimStdInterfaceCBSRPreference ARG_LIST((UINT4 *, INT4, 
                                                               INT4, INT4));
extern INT1 nmhSetFsPimStdInterfaceMode ARG_LIST((INT4, INT4, INT4));
extern INT1 nmhSetFsPimStdInterfaceHelloInterval ARG_LIST((INT4, INT4, INT4));
extern INT1 nmhSetFsPimStdInterfaceJoinPruneInterval ARG_LIST((INT4, INT4, 
                                                               INT4));
extern INT1 nmhSetFsPimStdInterfaceCBSRPreference ARG_LIST((INT4, INT4, INT4));
extern INT4 FsPimStdInterfaceStatusSet (tSnmpIndex *, tRetVal *);
extern INT4 FsPimStdInterfaceModeSet (tSnmpIndex *, tRetVal *);
extern INT4 FsPimStdInterfaceJoinPruneIntervalSet (tSnmpIndex *, tRetVal *);
extern INT4 FsPimStdInterfaceHelloIntervalSet (tSnmpIndex *, tRetVal *);
extern INT4 FsPimStdInterfaceCBSRPreferenceSet (tSnmpIndex *, tRetVal *);
extern INT4 FsPimStdComponentStatusSet (tSnmpIndex *, tRetVal *);
extern INT4 FsPimStdComponentCRPHoldTimeSet (tSnmpIndex *, tRetVal *);
extern INT4 FsPimComponentModeSet (tSnmpIndex *, tRetVal *);
extern INT4 FsPimStdComponentScopeZoneNameSet (tSnmpIndex *, tRetVal *);
extern INT4 FsPimCmnInterfaceCompIdSet (tSnmpIndex *, tRetVal *);
extern INT1 nmhGetFirstIndexFsPimStdComponentTable (INT4 *);
extern INT1 nmhGetNextIndexFsPimStdComponentTable (INT4, INT4 *);
extern INT1 nmhGetFirstIndexFsPimCmnElectedRPTable ARG_LIST((INT4 * , INT4 * , 
                                        tSNMP_OCTET_STRING_TYPE *  , INT4 *));
extern INT1 nmhGetNextIndexFsPimCmnElectedRPTable ARG_LIST((INT4, INT4 *, INT4,
 INT4 *, tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *, INT4, INT4 *));
extern INT1 nmhGetFsPimCmnElectedRPAddress ARG_LIST((INT4  , INT4  , 
                tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsPimCmnElectedRPPriority ARG_LIST((INT4  , INT4  , 
                                    tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));
extern INT1 nmhGetFsPimCmnElectedRPHoldTime ARG_LIST((INT4  , INT4  , 
                                    tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));
extern INT1 nmhGetFirstIndexFsPimCmnDFTable ARG_LIST((INT4 * , 
                                          tSNMP_OCTET_STRING_TYPE *  , INT4 *));
extern INT1 nmhGetNextIndexFsPimCmnDFTable ARG_LIST((INT4 , INT4 * , 
        tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));
extern INT1 nmhGetFsPimCmnDFState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,
                                             INT4 ,INT4 *));
extern INT1 nmhGetFsPimCmnDFWinnerAddr ARG_LIST((INT4  , 
                 tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsPimCmnDFWinnerUptime ARG_LIST((INT4  , 
                                    tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));
extern INT1 nmhGetFsPimCmnDFElectionStateTimer ARG_LIST((INT4  , 
                                    tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));
extern INT1 nmhGetFsPimCmnDFWinnerMetric ARG_LIST((INT4  , 
                                    tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));
extern INT1 nmhGetFsPimCmnDFWinnerMetricPref ARG_LIST((INT4  , 
                                    tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));
extern INT1 nmhGetFsPimCmnDFMessageCount ARG_LIST((INT4  , 
                                    tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));
extern INT1 nmhSetFsPimCmnCandidateRPPriority ARG_LIST((INT4  , INT4  , 
         tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhTestv2FsPimCmnCandidateRPPriority ARG_LIST((UINT4 *  ,INT4  , 
 INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhGetFsPimCmnCandidateRPPriority ARG_LIST((INT4  , INT4  , 
        tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1 nmhSetFsPimCmnCandidateRPPimMode ARG_LIST((INT4  , INT4  , 
         tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhTestv2FsPimCmnCandidateRPPimMode ARG_LIST((UINT4 *  ,INT4  , 
 INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhGetFsPimCmnCandidateRPPimMode ARG_LIST((INT4  , INT4  , 
        tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1 nmhGetFsPimCmnIpMRoutePimMode ARG_LIST((INT4  , INT4  , 
        tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));
extern INT1 nmhTestv2PimInterfaceStatus (UINT4 *  ,INT4  ,INT4 );
extern INT4 PimInterfaceStatusSet(tSnmpIndex *, tRetVal *);
extern INT1 nmhTestv2FsPimInterfaceCompId (UINT4 *  ,INT4  ,INT4 );
extern INT4 FsPimInterfaceCompIdSet(tSnmpIndex *, tRetVal *);
extern INT4 FsPimCmnCandidateRPRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsPimCmnCandidateRPPimModeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsPimCmnCandidateRPPrioritySet(tSnmpIndex *, tRetVal *);
extern INT1 nmhGetNextIndexPimInterfaceTable (INT4 , INT4 *);
extern INT1 nmhGetFsPimCmnInterfaceGraftRetryInterval (INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetPimInterfaceCBSRPreference (INT4 ,INT4 *);
extern INT1 nmhGetPimInterfaceJoinPruneInterval (INT4 ,INT4 *);
extern INT1 nmhGetPimInterfaceHelloInterval (INT4 ,INT4 *);
extern INT1 nmhGetFsPimInterfaceCompId (INT4 ,INT4 *);
extern INT1 nmhGetFsPimCmnIpMRouteUpstreamNeighbor (INT4, INT4, 
               tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *, INT4, 
               tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsPimCmnIpMRouteInIfIndex (INT4, INT4, 
                tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *, INT4,
                INT4 *);
extern INT1 nmhGetFirstIndexFsPimCmnIpMRouteTable (INT4 * , INT4 * , 
                  tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetNextIndexFsPimCmnIpMRouteTable (INT4 , INT4 * , INT4 , INT4 *,
            tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *,
            tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *, INT4, INT4 *);
extern INT1 nmhGetFirstIndexPimInterfaceTable (INT4 *);
extern INT4 FsPimCmnInterfaceGraftRetryIntervalSet(tSnmpIndex *, tRetVal *);
extern INT1 nmhTestv2FsPimCmnInterfaceGraftRetryInterval (UINT4 *  ,INT4  , INT4  ,UINT4 );
extern INT4 PimInterfaceCBSRPreferenceSet(tSnmpIndex *, tRetVal *);
extern INT1 nmhTestv2PimInterfaceCBSRPreference (UINT4 *  ,INT4  ,INT4 );
extern INT4 PimInterfaceJoinPruneIntervalSet(tSnmpIndex *, tRetVal *);
extern INT1 nmhTestv2PimInterfaceJoinPruneInterval (UINT4 *  ,INT4  ,INT4 );
extern INT4 PimInterfaceHelloIntervalSet(tSnmpIndex *, tRetVal *);
extern INT1 nmhTestv2PimInterfaceHelloInterval (UINT4 *  ,INT4  ,INT4 );
extern INT1 nmhValidateIndexInstanceFsPimStdComponentTable (INT4 );
extern INT1 nmhGetFsPimCmnIpMRouteRPFVectorAddr(INT4, INT4, 
            tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *, INT4, 
            tSNMP_OCTET_STRING_TYPE *);
extern INT4 FsPimCmnStaticRPAddressSet(tSnmpIndex *, tRetVal *);
extern INT4 FsPimCmnStaticRPRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsPimCmnStaticRPEmbdFlagSet(tSnmpIndex *, tRetVal *);
extern INT4 FsPimCmnStaticRPPimModeSet(tSnmpIndex *, tRetVal *);
extern INT1 nmhGetFsPimCmnStaticRPAddress ARG_LIST((INT4  ,   INT4  ,
             tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsPimCmnStaticRPEmbdFlag ARG_LIST((INT4  , INT4  ,
             tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));
extern INT1 nmhGetFsPimCmnStaticRPPimMode ARG_LIST((INT4  , INT4  ,
             tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));
extern INT1 nmhSetFsPimCmnStaticRPAddress ARG_LIST((INT4  , INT4  ,
             tSNMP_OCTET_STRING_TYPE * ,  INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsPimCmnStaticRPEmbdFlag ARG_LIST((INT4  , INT4  ,
             tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));
extern INT1 nmhSetFsPimCmnStaticRPPimMode ARG_LIST((INT4  , INT4  ,
             tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));
extern INT1 nmhTestv2FsPimCmnStaticRPAddress ARG_LIST((UINT4 *  ,INT4  , INT4  ,
             tSNMP_OCTET_STRING_TYPE * , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsPimCmnStaticRPEmbdFlag ARG_LIST((UINT4 *  ,INT4  , INT4  ,
             tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));
extern INT1 nmhTestv2FsPimCmnStaticRPPimMode ARG_LIST((UINT4 *  ,INT4  , INT4  ,
             tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));
extern INT1 nmhTestv2FsPimCmnStaticRPRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,
             tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));
extern INT1 nmhGetNextIndexFsPimCmnStaticRPSetTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * ,
             tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *,INT4 , INT4 *));
extern INT1 nmhGetFsPimCmnStaticRPRowStatus ARG_LIST((INT4  , INT4  ,
             tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));
extern INT1 nmhGetFirstIndexFsPimCmnStaticRPSetTable ARG_LIST((INT4 * , INT4 * ,
             tSNMP_OCTET_STRING_TYPE *  , INT4 *));

#endif
#ifdef VCM_WANTED
extern INT1 nmhSetFsVcId (INT4  ,INT4 );
extern INT1 nmhSetFsVcIfRowStatus (INT4  ,INT4 );
extern INT1 nmhTestv2FsVcId (UINT4 *  ,INT4  ,INT4 );
extern INT1 nmhTestv2FsVcIfRowStatus (UINT4 *  ,INT4  ,INT4 );
extern INT4 FsVcIfRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsVcIdSet(tSnmpIndex *, tRetVal *);
extern INT1 nmhSetFsVcL2ContextId (INT4 i4FsVcmIfIndex, INT4 i4SetValFsVcL2ContextId);
extern INT4 FsVcL2ContextIdSet (tSnmpIndex * , tRetVal * );
#endif

#ifdef PB_WANTED

extern INT1 nmhGetFirstIndexFsPbSVlanConfigTable (UINT4 *);
extern INT1 nmhGetNextIndexFsPbSVlanConfigTable (UINT4 , UINT4 *);
extern INT1 nmhGetFsPbSVlanConfigServiceType (UINT4 ,INT4 *);
extern INT1 nmhSetFsPbSVlanConfigServiceType (UINT4  ,INT4 );
extern INT1 nmhTestv2FsPbSVlanConfigServiceType (UINT4 *  ,UINT4  ,INT4 );

/* Proto Validate Index Instance for FsVlanTunnelProtocolTable. */
extern INT1 nmhGetFirstIndexFsVlanTunnelProtocolTable ARG_LIST((INT4 *));
extern INT1 nmhGetNextIndexFsVlanTunnelProtocolTable ARG_LIST((INT4 , INT4 *));
extern INT1 nmhGetFsVlanTunnelProtocolDot1x ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsVlanTunnelProtocolLacp ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsVlanTunnelProtocolStp ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsVlanTunnelProtocolGvrp ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsVlanTunnelProtocolGmrp ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsVlanTunnelProtocolIgmp ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsVlanTunnelProtocolEoam ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhSetFsVlanTunnelProtocolDot1x ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsVlanTunnelProtocolLacp ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsVlanTunnelProtocolStp ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsVlanTunnelProtocolGvrp ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsVlanTunnelProtocolGmrp ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsVlanTunnelProtocolIgmp ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsVlanTunnelProtocolEoam ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhTestv2FsVlanTunnelProtocolDot1x ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsVlanTunnelProtocolLacp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsVlanTunnelProtocolStp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsVlanTunnelProtocolGvrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsVlanTunnelProtocolGmrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsVlanTunnelProtocolIgmp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsVlanTunnelProtocolEoam ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/********Functions Related to Pbvlan *************/
extern INT4 nmhGetDot1adServicePriorityRegenRegeneratedPriority
            (INT4 ,UINT4 ,INT4 ,INT4 * );

extern INT4 nmhGetNextIndexDot1adServicePriorityRegenerationTable
            (INT4 ,INT4 * ,UINT4 ,UINT4 * ,INT4 ,INT4 *);
extern INT1 nmhTestv2Dot1adServicePriorityRegenRegeneratedPriority
           (UINT4 * ,INT4 ,UINT4 ,INT4 ,INT4); 
   
extern INT1 nmhSetDot1adServicePriorityRegenRegeneratedPriority 
            (INT4 ,UINT4 ,INT4 ,INT4); 
     
extern INT4 nmhGetFirstIndexDot1adServicePriorityRegenerationTable
        (INT4 * ,INT4 * ,INT4 *);
extern INT1 nmhGetFsVlanBridgeMode (INT4 *);

extern INT1 nmhGetNextIndexDot1adPcpEncodingTable(INT4 ,INT4 * ,INT4 ,INT4 * ,
                                                  INT4 ,INT4 * ,INT4 ,INT4 *);
extern INT4 nmhGetDot1adPcpEncodingPcpValue(INT4 ,INT4 ,INT4 ,INT4 ,INT4 *);     

extern INT1 nmhTestv2Dot1adPcpEncodingPcpValue(UINT4 *, INT4 ,INT4 ,INT4 ,
                                               INT4 ,INT4); 
extern INT1 nmhSetDot1adPcpEncodingPcpValue (INT4 ,INT4 ,INT4 ,INT4 ,INT4);


extern INT1 nmhGetNextIndexDot1adPcpDecodingTable(INT4 ,INT4 * ,INT4 ,INT4 * ,
                                                  INT4 ,INT4 *);

extern INT4 nmhGetDot1adPcpDecodingPriority(INT4 ,INT4 ,INT4 ,INT4 *);     
extern INT4 nmhGetDot1adPcpDecodingDropEligible(INT4 ,INT4 ,INT4 ,INT4 *);     

extern INT1 nmhTestv2Dot1adPcpDecodingDropEligible (UINT4 *, INT4, INT4, INT4,
                                                    INT4);
extern INT1 nmhTestv2Dot1adPcpDecodingPriority (UINT4 *, INT4, INT4, INT4,
                                                INT4);

extern INT1 nmhSetDot1adPcpDecodingDropEligible (INT4 , INT4, INT4, INT4);
extern INT1 nmhSetDot1adPcpDecodingPriority (INT4, INT4, INT4, INT4);

extern INT1 nmhGetFirstIndexFsMIPbEtherTypeSwapTable (INT4 * , INT4 *);
extern INT1 nmhGetNextIndexFsMIPbEtherTypeSwapTable (INT4 , INT4 * , INT4 , INT4 *);
extern INT1 nmhGetFsMIPbRelayEtherType (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsMIPbEtherTypeSwapRowStatus (INT4  , INT4 ,INT4 *);
extern INT1 nmhSetFsMIPbRelayEtherType (INT4  , INT4  ,INT4 );
extern INT1 nmhSetFsMIPbEtherTypeSwapRowStatus (INT4  , INT4  ,INT4 );
extern INT1 nmhTestv2FsMIPbRelayEtherType (UINT4 *  ,INT4  , INT4  ,INT4 );
extern INT1 nmhTestv2FsMIPbPortSVlanEgressEtherType (UINT4 *  ,INT4  ,INT4 );
extern INT1 nmhTestv2FsMIPbPortSVlanIngressEtherType (UINT4 *  ,INT4  ,INT4 );
extern INT1 nmhSetFsMIPbPortSVlanIngressEtherType (INT4  ,INT4 );
extern INT1 nmhSetFsMIPbPortSVlanEgressEtherType (INT4  ,INT4 );
extern INT1 nmhGetFsMIPbPortSVlanIngressEtherType (INT4 ,INT4 *);
extern INT1 nmhGetFsMIPbPortSVlanEgressEtherType (INT4 ,INT4 *);
extern INT1 nmhGetFirstIndexFsMIPbPortInfoTable (INT4 *);
extern INT1 nmhGetNextIndexFsMIPbPortInfoTable  (INT4 , INT4 *);
extern INT1 nmhGetFirstIndexFsMIVlanBridgeInfoTable (INT4 *);
extern INT1 nmhGetNextIndexFsMIVlanBridgeInfoTable (INT4 , INT4 *);
extern INT1 nmhGetFsMIVlanBridgeMode (INT4 ,INT4 *);
extern INT1 nmhSetFsMIVlanBridgeMode (INT4  ,INT4 );
extern INT1 nmhTestv2FsMIVlanBridgeMode (UINT4 *  ,INT4  ,INT4 );
extern INT1 nmhGetFirstIndexFsMefContextTable (UINT4 *);
extern INT1 nmhGetNextIndexFsMefContextTable (UINT4 , UINT4 *);
extern INT1 nmhGetFsMefTransmode (UINT4 ,INT4 *);
extern INT1 nmhTestv2FsMefTransmode (UINT4 *  ,UINT4  ,INT4 );
extern INT1 nmhSetFsMefTransmode (UINT4  ,INT4 );      
extern INT1 nmhGetFsMefFrameLossBufferSize (UINT4 ,INT4 *);
extern INT1 nmhSetFsMefFrameLossBufferSize(UINT4  ,INT4 );
extern INT1 nmhTestv2FsMefFrameLossBufferSize(UINT4 *  ,UINT4  ,INT4 );
extern INT1 nmhGetFsMefFrameDelayBufferSize (UINT4 ,INT4 *);
extern INT1 nmhSetFsMefFrameDelayBufferSize(UINT4  ,INT4 );
extern INT1 nmhTestv2FsMefFrameDelayBufferSize (UINT4 *  ,UINT4  ,INT4 );
extern INT1 nmhGetFsMefFrameLossBufferClear (UINT4 ,INT4 *);
extern INT1 nmhSetFsMefFrameLossBufferClear (UINT4  ,INT4 );
extern INT1 nmhTestv2FsMefFrameLossBufferClear (UINT4 *  ,UINT4  ,INT4 );
extern INT1 nmhGetFsMefFrameDelayBufferClear (UINT4 ,INT4 *);
extern INT1 nmhSetFsMefFrameDelayBufferClear (UINT4  ,INT4 );
extern INT1 nmhTestv2FsMefFrameDelayBufferClear (UINT4 *  ,UINT4  ,INT4 );

extern INT1 nmhGetFirstIndexFsUniTable ARG_LIST((INT4 *));
extern INT1 nmhGetNextIndexFsUniTable ARG_LIST((INT4 , INT4 *));
extern INT1 nmhGetFsUniId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsUniCVlanId ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsUniRowStatus ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhTestv2FsUniRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhSetFsUniRowStatus ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhTestv2FsUniId ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *)); 
extern INT1 nmhSetFsUniId ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsUniCVlanId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhSetFsUniCVlanId ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhGetFsUniPhysicalMedium ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsUniSpeed ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsUniMode ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsUniMacLayer ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsUniMtu ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsUniServiceMultiplexingBundling ARG_LIST((INT4 ,                                                                           tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsUniMaxEvcs ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsUniL2CPDot1x ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsUniL2CPLacp ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsUniL2CPStp ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsUniL2CPGvrp ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsUniL2CPGmrp ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsUniL2CPMvrp ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsUniL2CPMmrp ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsUniL2CPLldp ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsUniL2CPElmi ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhSetFsUniServiceMultiplexingBundling ARG_LIST((INT4  ,                                                                    tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsUniL2CPDot1x ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsUniL2CPLacp ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsUniL2CPStp ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsUniL2CPGvrp ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsUniL2CPGmrp ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsUniL2CPMvrp ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsUniL2CPMmrp ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsUniL2CPLldp ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsUniL2CPElmi ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhTestv2FsUniServiceMultiplexingBundling ARG_LIST((UINT4 *  ,INT4  ,                                          tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsUniL2CPDot1x ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsUniL2CPLacp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsUniL2CPStp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsUniL2CPGvrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsUniL2CPGmrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsUniL2CPMvrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsUniL2CPMmrp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsUniL2CPLldp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsUniL2CPElmi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhGetFirstIndexFsEvcTable ARG_LIST((INT4 * , INT4 *));
extern INT1 nmhGetNextIndexFsEvcTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));
extern INT1 nmhGetFsEvcId ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsEvcType ARG_LIST((INT4  , INT4 ,INT4 *));
extern INT1 nmhGetFsEvcMaxUni ARG_LIST((INT4  , INT4 ,INT4 *));
extern INT1 nmhGetFsEvcCVlanIdPreservation ARG_LIST((INT4  , INT4 ,INT4 *));
extern INT1 nmhGetFsEvcCVlanCoSPreservation ARG_LIST((INT4  , INT4 ,INT4 *));
extern INT1 nmhGetFsEvcRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));
extern INT1 nmhSetFsEvcId ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsEvcType ARG_LIST((INT4  , INT4  ,INT4 ));
extern INT1 nmhSetFsEvcCVlanIdPreservation ARG_LIST((INT4  , INT4  ,INT4 ));
extern INT1 nmhSetFsEvcCVlanCoSPreservation ARG_LIST((INT4  , INT4  ,INT4 ));
extern INT1 nmhSetFsEvcRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2FsEvcId ARG_LIST((UINT4 *  ,INT4  , INT4  ,
                                         tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsEvcType ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2FsEvcCVlanIdPreservation ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2FsEvcCVlanCoSPreservation ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2FsEvcRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

extern INT1 nmhGetFirstIndexFsUniCVlanEvcTable ARG_LIST((INT4 * , INT4 *));
extern INT1 nmhGetNextIndexFsUniCVlanEvcTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));
extern INT1 nmhGetFsUniCVlanEvcEvcIndex ARG_LIST((INT4  , INT4 ,INT4 *));
extern INT1 nmhGetFsUniCVlanEvcRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));
extern INT1 nmhSetFsUniCVlanEvcEvcIndex ARG_LIST((INT4  , INT4  ,INT4 ));
extern INT1 nmhSetFsUniCVlanEvcRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2FsUniCVlanEvcEvcIndex ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2FsUniCVlanEvcRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

extern INT1 nmhGetFirstIndexFsEvcFilterTable ARG_LIST((INT4 * , INT4 * , INT4 *));
extern INT1 nmhGetNextIndexFsEvcFilterTable ARG_LIST((INT4 , INT4 * , 
             INT4 , INT4 * , INT4 , INT4 *));
extern INT1 nmhGetFsEvcFilterDestMacAddress ARG_LIST((INT4  , INT4  ,
              INT4 ,tMacAddr * ));

extern INT1 nmhGetFsEvcFilterAction ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));
extern INT1 nmhGetFsEvcFilterId ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));
extern  INT1 nmhGetFsEvcFilterRowStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1 nmhSetFsEvcFilterDestMacAddress ARG_LIST((INT4  , INT4  ,
                        INT4  ,tMacAddr ));
extern INT1 nmhSetFsEvcFilterAction ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));
extern INT1 nmhSetFsEvcFilterId ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));
extern INT1  nmhSetFsEvcFilterRowStatus ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

extern INT1 nmhTestv2FsEvcFilterDestMacAddress ARG_LIST((UINT4 *  ,INT4  ,
                    INT4  , INT4  ,tMacAddr ));
extern INT1 nmhTestv2FsEvcFilterAction ARG_LIST((UINT4 *  ,INT4  ,
                     INT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2FsEvcFilterId ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));
extern  INT1  nmhTestv2FsEvcFilterRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));
extern INT1 nmhGetFirstIndexFsMefETreeTable ARG_LIST((INT4 * , INT4 * , INT4 *));
extern INT1 nmhGetNextIndexFsMefETreeTable ARG_LIST((INT4 , INT4 * , 
                INT4 , INT4 * , INT4 , INT4 *));
extern INT1 nmhGetFsMefETreeRowStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));
extern INT1 nmhSetFsMefETreeRowStatus ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2FsMefETreeRowStatus ARG_LIST((UINT4 *  ,INT4  ,
                                 INT4  , INT4  ,INT4 ));
extern INT1 nmhGetFirstIndexFsMefFilterTable ARG_LIST((INT4 *));
extern INT1 nmhGetNextIndexFsMefFilterTable ARG_LIST((INT4 , INT4 *));
extern INT1 nmhGetFsMefFilterDscp ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsMefFilterDirection ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsMefFilterEvc ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsMefFilterCVlanPriority ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsMefFilterIfIndex ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsMefFilterStatus ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhSetFsMefFilterDscp ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsMefFilterDirection ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsMefFilterEvc ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsMefFilterCVlanPriority ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsMefFilterIfIndex ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsMefFilterStatus ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhTestv2FsMefFilterDscp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2FsMefFilterDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2FsMefFilterEvc ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2FsMefFilterCVlanPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2FsMefFilterIfIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2FsMefFilterStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhGetFirstIndexFsMefClassMapTable ARG_LIST((UINT4 *)); 
extern INT1 nmhGetNextIndexFsMefClassMapTable ARG_LIST((UINT4 , UINT4 *)); 
extern INT1 nmhGetFsMefClassMapName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsMefClassMapFilterId ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefClassMapCLASS ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefClassMapStatus ARG_LIST((UINT4 ,INT4 *)); 
extern INT1 nmhSetFsMefClassMapName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsMefClassMapFilterId ARG_LIST((UINT4  ,UINT4 ));
extern INT1 nmhSetFsMefClassMapCLASS ARG_LIST((UINT4  ,UINT4 )); 
extern INT1 nmhSetFsMefClassMapStatus ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefClassMapName ARG_LIST((UINT4 *  ,UINT4  ,
                                   tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsMefClassMapFilterId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsMefClassMapCLASS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 )); 
extern INT1 nmhTestv2FsMefClassMapStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1 nmhGetFirstIndexFsMefClassTable ARG_LIST((UINT4 *));
extern INT1 nmhGetNextIndexFsMefClassTable ARG_LIST((UINT4 , UINT4 *));
extern INT1 nmhGetFsMefClassStatus ARG_LIST((UINT4 ,INT4 *)); 
extern INT1 nmhSetFsMefClassStatus ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefClassStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1 nmhGetFirstIndexFsMefMeterTable ARG_LIST((UINT4 *));
extern INT1 nmhGetNextIndexFsMefMeterTable ARG_LIST((UINT4 , UINT4 *));
extern INT1 nmhGetFsMefMeterName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsMefMeterType ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMeterColorMode ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMeterCIR ARG_LIST((UINT4 ,UINT4 *)); 
extern INT1 nmhGetFsMefMeterCBS ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefMeterEIR ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefMeterEBS ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefMeterStatus ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhSetFsMefMeterName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsMefMeterType ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMeterColorMode ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMeterCIR ARG_LIST((UINT4  ,UINT4 ));
extern INT1 nmhSetFsMefMeterCBS ARG_LIST((UINT4  ,UINT4 ));
extern INT1 nmhSetFsMefMeterEIR ARG_LIST((UINT4  ,UINT4 ));
extern INT1 nmhSetFsMefMeterEBS ARG_LIST((UINT4  ,UINT4 ));
extern INT1 nmhSetFsMefMeterStatus ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMeterName ARG_LIST((UINT4 *  ,UINT4  ,
                                   tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsMefMeterType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMeterColorMode ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMeterCIR ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsMefMeterCBS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 )); 
extern INT1 nmhTestv2FsMefMeterEIR ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsMefMeterEBS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsMefMeterStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1 nmhGetFirstIndexFsMefPolicyMapTable ARG_LIST((UINT4 *));
extern INT1 nmhGetNextIndexFsMefPolicyMapTable ARG_LIST((UINT4 , UINT4 *));
extern INT1 nmhGetFsMefPolicyMapName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsMefPolicyMapCLASS ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefPolicyMapMeterTableId ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefPolicyMapStatus ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhSetFsMefPolicyMapName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsMefPolicyMapCLASS ARG_LIST((UINT4  ,UINT4 ));
extern INT1 nmhSetFsMefPolicyMapMeterTableId ARG_LIST((UINT4  ,UINT4 ));
extern INT1 nmhSetFsMefPolicyMapStatus ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefPolicyMapName ARG_LIST((UINT4 *  ,UINT4  ,
                          tSNMP_OCTET_STRING_TYPE *)); 
extern INT1 nmhTestv2FsMefPolicyMapCLASS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsMefPolicyMapMeterTableId ARG_LIST((UINT4 *  ,
                                             UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsMefPolicyMapStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhGetFirstIndexFsMefMepTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * ,
                                          UINT4 *));
extern INT1 nmhGetNextIndexFsMefMepTable ARG_LIST((UINT4 , UINT4 * , UINT4 ,
                          UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));
extern INT1 nmhGetFsMefMepTransmitLmmStatus ARG_LIST((UINT4  , UINT4  , 
                                            UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepTransmitLmmResultOK ARG_LIST((UINT4  , UINT4  , 
                                      UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepTransmitLmmInterval ARG_LIST((UINT4  , UINT4  , 
                                  UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepTransmitLmmDeadline ARG_LIST((UINT4  , UINT4  , 
                                                      UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefMepTransmitLmmDropEnable ARG_LIST((UINT4  , UINT4  ,
                                       UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepTransmitLmmPriority ARG_LIST((UINT4  , UINT4  ,
                                      UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepTransmitLmmDestMacAddress ARG_LIST((UINT4  , 
                         UINT4  , UINT4  , UINT4 ,tMacAddr * ));
extern INT1 nmhGetFsMefMepTransmitLmmDestMepId ARG_LIST((UINT4  , 
                                   UINT4  , UINT4  , UINT4 ,UINT4 *)); 
extern INT1 nmhGetFsMefMepTransmitLmmDestIsMepId ARG_LIST((UINT4  ,
                          UINT4  , UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepTransmitLmmMessages ARG_LIST((UINT4  , 
                          UINT4  , UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepTxFCf ARG_LIST((UINT4  , UINT4  , 
                                     UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefMepRxFCb ARG_LIST((UINT4  , UINT4  , UINT4  , 
                                            UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefMepTxFCb ARG_LIST((UINT4  , UINT4  , UINT4  , 
                                                        UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefMepNearEndFrameLossThreshold ARG_LIST((UINT4  , UINT4  , 
                                         UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefMepFarEndFrameLossThreshold ARG_LIST((UINT4  ,
                                         UINT4  , UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefMepTransmitDmStatus ARG_LIST((UINT4  , UINT4  ,
                                     UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepTransmitDmResultOK ARG_LIST((UINT4  , 
                              UINT4  , UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepTransmitDmType ARG_LIST((UINT4  ,
                                  UINT4  , UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepTransmitDmInterval ARG_LIST((UINT4  , 
                                    UINT4  , UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepTransmitDmMessages ARG_LIST((UINT4  ,
                                 UINT4  , UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepTransmitDmmDropEnable ARG_LIST((UINT4  ,
                                  UINT4  , UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepTransmit1DmDropEnable ARG_LIST((UINT4  , 
                                  UINT4  , UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepTransmitDmmPriority ARG_LIST((UINT4  , 
                               UINT4  , UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepTransmit1DmPriority ARG_LIST((UINT4  , 
                                  UINT4  , UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepTransmitDmDestMacAddress ARG_LIST((UINT4  , 
                                    UINT4  , UINT4  , UINT4 ,tMacAddr * ));
extern INT1 nmhGetFsMefMepTransmitDmDestMepId ARG_LIST((UINT4  , 
                                       UINT4  , UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefMepTransmitDmDestIsMepId ARG_LIST((UINT4  ,
                                        UINT4  , UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepTransmitDmDeadline ARG_LIST((UINT4  , 
                                       UINT4  , UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefMepDmrOptionalFields ARG_LIST((UINT4  , 
                                         UINT4  , UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMep1DmRecvCapability ARG_LIST((UINT4  , UINT4  , 
                                               UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepFrameDelayThreshold ARG_LIST((UINT4  , 
                                      UINT4  , UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefMepRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , 
                                                   UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepRxFCf ARG_LIST((UINT4  , UINT4  , 
                                             UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefMep1DmTransInterval ARG_LIST((UINT4  , UINT4  ,
                                                UINT4  , UINT4 ,INT4 *));
extern INT1 nmhSetFsMefMepTransmitLmmStatus ARG_LIST((UINT4  , UINT4  ,
                                                 UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMepTransmitLmmInterval ARG_LIST((UINT4  , UINT4  , 
                                                UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMepTransmitLmmDeadline ARG_LIST((UINT4  , UINT4  ,
                                              UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhSetFsMefMepTransmitLmmDropEnable ARG_LIST((UINT4  , 
                                         UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMepTransmitLmmPriority ARG_LIST((UINT4  , 
                                    UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMepTransmitLmmDestMacAddress ARG_LIST((UINT4  , 
                                 UINT4  , UINT4  , UINT4  ,tMacAddr )); 
extern INT1 nmhSetFsMefMepTransmitLmmDestMepId ARG_LIST((UINT4  , 
                                     UINT4  , UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhSetFsMefMepTransmitLmmDestIsMepId ARG_LIST((UINT4  , 
                                        UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMepTransmitLmmMessages ARG_LIST((UINT4  ,
                                      UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMepNearEndFrameLossThreshold ARG_LIST((UINT4  , 
                                    UINT4  , UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhSetFsMefMepFarEndFrameLossThreshold ARG_LIST((UINT4  ,
                                    UINT4  , UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhSetFsMefMepTransmitDmStatus ARG_LIST((UINT4  , 
                                        UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMepTransmitDmType ARG_LIST((UINT4  , UINT4  ,
                                               UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMepTransmitDmInterval ARG_LIST((UINT4  , 
                                     UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMepTransmitDmMessages ARG_LIST((UINT4  , 
                                      UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMepTransmitDmmDropEnable ARG_LIST((UINT4  ,
                                         UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMepTransmit1DmDropEnable ARG_LIST((UINT4  , 
                                        UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMepTransmitDmmPriority ARG_LIST((UINT4  , 
                                     UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMepTransmit1DmPriority ARG_LIST((UINT4  , 
                                      UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMepTransmitDmDestMacAddress ARG_LIST((UINT4  , 
                                  UINT4  , UINT4  , UINT4  ,tMacAddr ));
extern INT1 nmhSetFsMefMepTransmitDmDestMepId ARG_LIST((UINT4  , UINT4  , 
                                                  UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhSetFsMefMepTransmitDmDestIsMepId ARG_LIST((UINT4  ,
                                    UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMepTransmitDmDeadline ARG_LIST((UINT4  , 
                                    UINT4  , UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhSetFsMefMepDmrOptionalFields ARG_LIST((UINT4  , 
                                     UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMep1DmRecvCapability ARG_LIST((UINT4  , 
                                         UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMepFrameDelayThreshold ARG_LIST((UINT4  ,
                                    UINT4  , UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhSetFsMefMepRowStatus ARG_LIST((UINT4  , 
                                  UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMep1DmTransInterval ARG_LIST((UINT4  ,
                                     UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMepTransmitLmmStatus ARG_LIST((UINT4 *  ,
                        UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMepTransmitLmmInterval ARG_LIST((UINT4 *  ,
                          UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMepTransmitLmmDeadline ARG_LIST((UINT4 *  ,
                          UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsMefMepTransmitLmmDropEnable ARG_LIST((UINT4 *  ,
                         UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMepTransmitLmmPriority ARG_LIST((UINT4 *  ,
                              UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMepTransmitLmmDestMacAddress ARG_LIST((UINT4 *  ,
                      UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));
extern INT1 nmhTestv2FsMefMepTransmitLmmDestMepId ARG_LIST((UINT4 *  ,
                       UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsMefMepTransmitLmmDestIsMepId ARG_LIST((UINT4 *  ,
                             UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMepTransmitLmmMessages ARG_LIST((UINT4 *  ,
                                   UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMepNearEndFrameLossThreshold ARG_LIST((UINT4 *  ,
                              UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsMefMepFarEndFrameLossThreshold ARG_LIST((UINT4 *  ,
                            UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsMefMepTransmitDmStatus ARG_LIST((UINT4 *  ,
                           UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMepTransmitDmType ARG_LIST((UINT4 *  ,
                             UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMepTransmitDmInterval ARG_LIST((UINT4 *  ,
                           UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMepTransmitDmMessages ARG_LIST((UINT4 *  ,
                            UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMepTransmitDmmDropEnable ARG_LIST((UINT4 *  ,
                           UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMepTransmit1DmDropEnable ARG_LIST((UINT4 *  ,
                         UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMepTransmitDmmPriority ARG_LIST((UINT4 *  ,
                           UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMepTransmit1DmPriority ARG_LIST((UINT4 *  ,
                            UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMepTransmitDmDestMacAddress ARG_LIST((UINT4 *  ,
                           UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));
extern INT1 nmhTestv2FsMefMepTransmitDmDestMepId ARG_LIST((UINT4 *  ,
                              UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsMefMepTransmitDmDestIsMepId ARG_LIST((UINT4 *  ,
                          UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMepTransmitDmDeadline ARG_LIST((UINT4 *  ,
                               UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsMefMepDmrOptionalFields ARG_LIST((UINT4 *  ,
                               UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMep1DmRecvCapability ARG_LIST((UINT4 *  ,
                                UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMepFrameDelayThreshold ARG_LIST((UINT4 *  ,
                            UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsMefMepRowStatus ARG_LIST((UINT4 *  ,UINT4  , 
                                       UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMep1DmTransInterval ARG_LIST((UINT4 *  ,
                           UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhGetFirstIndexFsMefFdStatsTable ARG_LIST((UINT4 * , UINT4 * , 
                                           UINT4 * , UINT4 * , UINT4 *));
extern INT1 nmhGetNextIndexFsMefFdStatsTable ARG_LIST((UINT4 , UINT4 * ,
       UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));
extern INT1 nmhGetFsMefFdStatsTimeStamp ARG_LIST((UINT4  , UINT4  ,
                                        UINT4  , UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefFdStatsDmmOut ARG_LIST((UINT4  , UINT4  , UINT4  ,
                                                    UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefFdStatsDmrIn ARG_LIST((UINT4  , UINT4  , UINT4  ,
                                              UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefFdStatsDelayAverage ARG_LIST((UINT4  , UINT4  , 
                   UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsMefFdStatsFDVAverage ARG_LIST((UINT4  , UINT4  , 
                        UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsMefFdStatsIFDVAverage ARG_LIST((UINT4  , UINT4  , 
                        UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsMefFdStatsDelayMin ARG_LIST((UINT4  , UINT4  , 
                      UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsMefFdStatsDelayMax ARG_LIST((UINT4  , UINT4  , UINT4  , 
                        UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFirstIndexFsMefFlStatsTable ARG_LIST((UINT4 * , UINT4 * ,
                                            UINT4 * , UINT4 * , UINT4 *));
extern INT1 nmhGetNextIndexFsMefFlStatsTable ARG_LIST((UINT4 , UINT4 * , 
       UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));
extern INT1 nmhGetFsMefFlStatsTimeStamp ARG_LIST((UINT4  , UINT4  ,
                                        UINT4  , UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefFlStatsMessagesOut ARG_LIST((UINT4  , UINT4  , 
                                           UINT4  , UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefFlStatsMessagesIn ARG_LIST((UINT4  , UINT4  ,
                                            UINT4  , UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefFlStatsFarEndLossAverage ARG_LIST((UINT4  , 
                                   UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefFlStatsNearEndLossAverage ARG_LIST((UINT4  , UINT4  , 
                                           UINT4  , UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefFlStatsMeasurementType ARG_LIST((UINT4  , UINT4  , 
                                           UINT4  , UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefFlStatsFarEndLossMin ARG_LIST((UINT4  , UINT4  ,
                                           UINT4  , UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefFlStatsFarEndLossMax ARG_LIST((UINT4  , UINT4  ,
                                          UINT4  , UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefFlStatsNearEndLossMin ARG_LIST((UINT4  , UINT4  ,
                                        UINT4  , UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefFlStatsNearEndLossMax ARG_LIST((UINT4  , UINT4  ,
                                           UINT4  , UINT4  , UINT4 ,UINT4 *)); 
extern INT1 nmhGetFirstIndexFsMefMepAvailabilityTable ARG_LIST((UINT4 * , UINT4 * ,
                                                   UINT4 * , UINT4 *));
extern INT1 nmhGetNextIndexFsMefMepAvailabilityTable ARG_LIST((UINT4 , UINT4 * ,
                         UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));
extern INT1 nmhGetFsMefMepAvailabilityStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,
                                                   UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepAvailabilityResultOK ARG_LIST((UINT4  , UINT4  ,
                                              UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepAvailabilityInterval ARG_LIST((UINT4  , UINT4  , 
                                                 UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepAvailabilityDeadline ARG_LIST((UINT4  , UINT4  , UINT4  , 
                                                       UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefMepAvailabilityLowerThreshold ARG_LIST((UINT4  , UINT4  , 
                                  UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsMefMepAvailabilityUpperThreshold ARG_LIST((UINT4  , UINT4  ,
                           UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsMefMepAvailabilityModestAreaIsAvailable ARG_LIST((UINT4  , UINT4  ,
                                        UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepAvailabilityWindowSize ARG_LIST((UINT4  , UINT4  , UINT4  , 
                                                       UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefMepAvailabilityDestMacAddress ARG_LIST((UINT4  , UINT4  , 
                                                      UINT4  , UINT4 ,tMacAddr * ));
extern INT1 nmhGetFsMefMepAvailabilityDestMepId ARG_LIST((UINT4  , UINT4  , UINT4  , 
                                                     UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefMepAvailabilityDestIsMepId ARG_LIST((UINT4  , UINT4  , 
                                             UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepAvailabilityType ARG_LIST((UINT4  , UINT4  , UINT4  ,
                                                           UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepAvailabilitySchldDownInitTime ARG_LIST((UINT4  ,
                                               UINT4  , UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefMepAvailabilitySchldDownEndTime ARG_LIST((UINT4  ,
                                                UINT4  , UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefMepAvailabilityPriority ARG_LIST((UINT4  , UINT4  , 
                                                UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsMefMepAvailabilityDropEnable ARG_LIST((UINT4  , UINT4  , 
                                            UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsMefMepAvailabilityPercentage ARG_LIST((UINT4  , UINT4  ,
                                 UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsMefMepAvailabilityRowStatus ARG_LIST((UINT4  , UINT4  ,
                                       UINT4  , UINT4 ,INT4 *));
extern INT1 nmhSetFsMefMepAvailabilityStatus ARG_LIST((UINT4  , UINT4  , 
                                             UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMepAvailabilityInterval ARG_LIST((UINT4  , UINT4  ,
      UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMepAvailabilityDeadline ARG_LIST((UINT4  , UINT4  ,
                                                        UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhSetFsMefMepAvailabilityLowerThreshold ARG_LIST((UINT4  ,
                       UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsMefMepAvailabilityUpperThreshold ARG_LIST((UINT4  , 
                      UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsMefMepAvailabilityModestAreaIsAvailable ARG_LIST((UINT4  ,
                                 UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMepAvailabilityWindowSize ARG_LIST((UINT4  , UINT4  , 
                                                   UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhSetFsMefMepAvailabilityDestMacAddress ARG_LIST((UINT4  , UINT4  ,
                                          UINT4  , UINT4  ,tMacAddr ));
extern INT1 nmhSetFsMefMepAvailabilityDestMepId ARG_LIST((UINT4  , UINT4  ,
                                                     UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhSetFsMefMepAvailabilityDestIsMepId ARG_LIST((UINT4  , UINT4  ,
                                             UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMepAvailabilityType ARG_LIST((UINT4  , UINT4  , UINT4  , 
                                                     UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMepAvailabilitySchldDownInitTime ARG_LIST((UINT4  , UINT4  ,
                                              UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhSetFsMefMepAvailabilitySchldDownEndTime ARG_LIST((UINT4  , 
                                          UINT4  , UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhSetFsMefMepAvailabilityPriority ARG_LIST((UINT4  , 
                                           UINT4  , UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhSetFsMefMepAvailabilityDropEnable ARG_LIST((UINT4  , UINT4  , 
                                   UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsMefMepAvailabilityRowStatus ARG_LIST((UINT4  , UINT4  ,
                                                   UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMepAvailabilityStatus ARG_LIST((UINT4 *  ,UINT4  , 
                                        UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMepAvailabilityInterval ARG_LIST((UINT4 *  ,UINT4  ,
                                        UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMepAvailabilityDeadline ARG_LIST((UINT4 *  ,UINT4  ,
                                      UINT4  , UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsMefMepAvailabilityLowerThreshold ARG_LIST((UINT4 *  ,
                UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsMefMepAvailabilityUpperThreshold ARG_LIST((UINT4 *  ,
                     UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsMefMepAvailabilityModestAreaIsAvailable ARG_LIST((UINT4 *  ,
                             UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMepAvailabilityWindowSize ARG_LIST((UINT4 *  ,UINT4  ,
                                            UINT4  , UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsMefMepAvailabilityDestMacAddress ARG_LIST((UINT4 *  ,
                                UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr ));
extern INT1 nmhTestv2FsMefMepAvailabilityDestMepId ARG_LIST((UINT4 *  ,UINT4  , 
                                           UINT4  , UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsMefMepAvailabilityDestIsMepId ARG_LIST((UINT4 *  ,
                                 UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMepAvailabilityType ARG_LIST((UINT4 *  ,UINT4  , 
                                       UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMepAvailabilitySchldDownInitTime ARG_LIST((UINT4 *  ,
                                     UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsMefMepAvailabilitySchldDownEndTime ARG_LIST((UINT4 *  ,
                         UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsMefMepAvailabilityPriority ARG_LIST((UINT4 *  ,
                                         UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsMefMepAvailabilityDropEnable ARG_LIST((UINT4 *  ,
                                    UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMefMepAvailabilityRowStatus ARG_LIST((UINT4 *  ,UINT4  ,
                                   UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhGetFsMefFdPeerMepMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  ,
                         UINT4  , UINT4  , UINT4 ,tMacAddr * ));
extern INT1 nmhGetFsMefFlPeerMepMacAddress ARG_LIST((UINT4  , UINT4  , UINT4  , 
                                 UINT4  , UINT4  , UINT4 ,tMacAddr * ));
extern INT1 nmhGetFsUniL2CPEcfm ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhSetFsUniL2CPEcfm ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhTestv2FsUniL2CPEcfm ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsUniEvcId ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsUniEvcId ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhGetFsUniEvcId ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsEvcMtu ARG_LIST((INT4  , INT4 ,INT4 *));
extern INT1 nmhGetFsMefUniId ARG_LIST((UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsMefUniType ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));
extern INT1 nmhGetFirstIndexFsMefUniListTable ARG_LIST((UINT4 * , INT4 * , INT4 *));
extern INT1 nmhGetNextIndexFsMefUniListTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));
extern INT1 nmhTestv2FsMefUniType ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2FsMefUniListRowStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  ,INT4 ));
extern INT1 nmhSetFsMefUniType ARG_LIST((UINT4  , INT4  , INT4  ,INT4 ));
extern INT1 nmhSetFsMefUniListRowStatus ARG_LIST((UINT4  , INT4  , INT4  ,INT4 ));
extern INT1 nmhGetFsMefUniListRowStatus ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));

extern INT4  MefApiLock (VOID);
extern INT4  MefApiUnLock (VOID);
extern INT1 nmhGetFirstIndexFsMIPbRstCVlanPortInfoTable (INT4 * , INT4 *);
extern INT1 nmhGetNextIndexFsMIPbRstCVlanPortInfoTable (INT4 , INT4 * , INT4 , INT4 *);
extern INT1 nmhGetFsMIPbRstCVlanPortPriority (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsMIPbRstCVlanPortPathCost (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsMIPbRstCVlanPortRole (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsMIPbRstCVlanPortState (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsMIPbRstCVlanPortAdminEdgePort (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsMIPbRstCVlanPortOperEdgePort (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsMIPbRstCVlanPortAdminPointToPoint (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsMIPbRstCVlanPortOperPointToPoint (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsMIPbRstCVlanPortAutoEdge (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsMIPbRstCVlanPortDesignatedRoot (INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetFsMIPbRstCVlanPortDesignatedCost (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsMIPbRstCVlanPortDesignatedBridge (INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetFsMIPbRstCVlanPortDesignatedPort (INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetFsMIPbRstCVlanPortForwardTransitions (INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFirstIndexFsMIPbRstCVlanPortStatsTable (INT4 * , INT4 *);
extern INT1 nmhGetNextIndexFsMIPbRstCVlanPortStatsTable (INT4 , INT4 * , INT4 , INT4 *);
extern INT1 nmhGetFsMIPbRstCVlanPortRxRstBpduCount (INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsMIPbRstCVlanPortRxConfigBpduCount (INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsMIPbRstCVlanPortRxTcnBpduCount (INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsMIPbRstCVlanPortTxRstBpduCount (INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsMIPbRstCVlanPortTxConfigBpduCount (INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsMIPbRstCVlanPortTxTcnBpduCount (INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsMIPbRstCVlanPortInvalidRstBpduRxCount (INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsMIPbRstCVlanPortInvalidConfigBpduRxCount (INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsMIPbRstCVlanPortInvalidTcnBpduRxCount (INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsMIPbRstCVlanPortProtocolMigrationCount (INT4  , INT4 ,UINT4 * );
extern INT1 nmhGetFsMIPbRstCVlanPortEffectivePortState (INT4  , INT4 ,INT4 *);

extern INT4 nmhGetNextIndexDot1adMIPepTable (INT4 , INT4 * , INT4 , INT4 *);

extern INT4 nmhTestv2FsMIPbPepExtCosPreservation (UINT4 * , INT4 , INT4 , INT4);
extern INT4 nmhGetFsMIPbPepExtCosPreservation (INT4 , INT4 , INT4 *);
extern INT4 nmhSetFsMIPbPepExtCosPreservation (INT4 , INT4 , INT4);

extern INT4 nmhTestv2Dot1adMIPepIngressFiltering (UINT4 * , INT4 , INT4 , INT4);
extern INT4 nmhGetDot1adMIPepIngressFiltering (INT4 , INT4 , INT4 *);
extern INT4 nmhSetDot1adMIPepIngressFiltering (INT4 , INT4 , INT4);

extern INT4 nmhTestv2Dot1adMIPepDefaultUserPriority (UINT4 * , INT4 , INT4 , INT4);
extern INT4 nmhGetDot1adMIPepDefaultUserPriority (INT4 , INT4 , INT4 *);
extern INT4 nmhSetDot1adMIPepDefaultUserPriority (INT4 , INT4 , INT4);

extern INT4 nmhTestv2Dot1adMIPepAccptableFrameTypes (UINT4 * , INT4 , INT4 , INT4);
extern INT4 nmhGetDot1adMIPepAccptableFrameTypes (INT4 , INT4 , INT4 *);
extern INT4 nmhSetDot1adMIPepAccptableFrameTypes (INT4 , INT4 , INT4);

extern INT4 nmhTestv2Dot1adMIPepPvid (UINT4 * , INT4 , INT4 , INT4);
extern INT4 nmhGetDot1adMIPepPvid (INT4 , INT4 , INT4 *);
extern INT4 nmhSetDot1adMIPepPvid (INT4 , INT4 , INT4);

extern INT4 Dot1adPcpEncodingPcpValueSet(tSnmpIndex *, tRetVal *);
extern INT4 Dot1adPcpDecodingDropEligibleSet(tSnmpIndex *, tRetVal *);
extern INT4 Dot1adPcpDecodingPrioritySet(tSnmpIndex *, tRetVal *);
extern INT4 Dot1adServicePriorityRegenRegeneratedPrioritySet
                                        (tSnmpIndex *, tRetVal *);

#endif

#if defined IGS_WANTED || defined MLDS_WANTED

PUBLIC INT1
nmhGetFsSnoopInstanceGlobalSystemControl (INT4, INT4  *);
PUBLIC INT1
nmhTestv2FsSnoopInstanceGlobalSystemControl (UINT4 *,INT4,INT4);

PUBLIC INT1        
nmhSetFsSnoopInstanceGlobalSystemControl (UINT4, INT4);
        
PUBLIC INT1
nmhGetFirstIndexFsSnoopInstanceGlobalTable (INT4 *);

INT4 SnoopUtilVlanResetStats(INT4,INT4,INT4);

extern INT1 nmhValidateIndexInstanceFsSnoopInstanceConfigTable (INT4, INT4);
extern INT1 nmhValidateIndexInstanceFsSnoopInstanceGlobalTable (INT4);
extern INT1 nmhTestv2FsSnoopInstanceGlobalMcastFwdMode (UINT4 *  ,INT4  ,INT4 );
extern INT1 nmhGetFirstIndexFsSnoopInstanceConfigTable (INT4 * , INT4 *);
extern INT1 nmhGetFsSnoopStatus (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsSnoopOperStatus (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsSnoopProxyReportingStatus (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsSnoopInstanceGlobalMcastFwdMode (INT4 ,INT4 *);
extern INT1 nmhGetFsSnoopReportFwdOnAllPorts (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsSnoopQueryFwdOnAllPorts (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsSnoopRetryCount (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsSnoopSendQueryOnTopoChange (INT4 , INT4, INT4 *);
extern INT1 nmhGetFsSnoopInstanceGlobalLeaveConfigLevel (INT4 ,INT4 *);
extern INT1 nmhGetFsSnoopInstanceGlobalReportProcessConfigLevel (INT4 ,INT4 *);
extern INT1 nmhGetFsSnoopInstanceGlobalSparseMode (INT4 ,INT4 *);
extern INT1 nmhGetFsSnoopFilterStatus (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsSnoopMulticastVlanStatus (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsSnoopProxyStatus (INT4  , INT4 ,INT4 *);
extern INT1 nmhSetFsSnoopVlanBlkRtrLocalPortList (INT4 ,INT4 ,INT4 ,
        tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsSnoopVlanBlkRtrLocalPortList (INT4 ,INT4 ,INT4 ,
        tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhTestv2FsSnoopVlanBlkRtrLocalPortList (UINT4 *,INT4 ,INT4 ,INT4 ,
          tSNMP_OCTET_STRING_TYPE *);


extern INT1 nmhGetNextIndexFsSnoopInstanceConfigTable (INT4 , INT4 * , INT4 , INT4 *);
extern INT1 nmhTestv2FsSnoopStatus (UINT4 *  ,INT4  , INT4  ,INT4 );
extern INT1 nmhTestv2FsSnoopOperStatus (UINT4 *, INT4  , INT4 ,INT4 );
extern INT1 nmhTestv2FsSnoopProxyReportingStatus (UINT4 *, INT4  , INT4 ,INT4);
extern INT1 nmhTestv2FsSnoopReportFwdOnAllPorts (UINT4 *, INT4  , INT4 ,INT4 );
extern INT1 nmhTestv2FsSnoopQueryFwdOnAllPorts (UINT4 *  ,INT4  , INT4  ,INT4 );
extern INT1 nmhTestv2FsSnoopRetryCount (UINT4 *, INT4  , INT4 ,INT4 );
extern INT1 nmhTestv2FsSnoopSendQueryOnTopoChange (UINT4 *, INT4  ,INT4 ,INT4 );
extern INT1 nmhTestv2FsSnoopProxyStatus (UINT4 *, INT4  , INT4 ,INT4);
extern INT1 nmhTestv2FsSnoopFilterStatus (UINT4 *, INT4  , INT4 ,INT4);
extern INT1 nmhTestv2FsSnoopMulticastVlanStatus (UINT4 *, INT4  , INT4 ,INT4);
extern INT1 nmhTestv2FsSnoopInstanceGlobalEnhancedMode (UINT4 *, INT4 ,INT4);
extern INT1 nmhTestv2FsSnoopInstanceGlobalSparseMode (UINT4 *, INT4 ,INT4);
extern INT1 nmhTestv2FsSnoopInstanceGlobalLeaveConfigLevel (UINT4 *, INT4 ,INT4);
extern INT1 nmhTestv2FsSnoopInstanceGlobalReportProcessConfigLevel (UINT4 *  ,INT4  ,INT4 );

extern INT1 nmhGetFsSnoopRouterPortPurgeInterval (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsSnoopPortPurgeInterval (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsSnoopReportForwardInterval (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsSnoopGrpQueryInterval (INT4  , INT4 ,INT4 *);

extern INT1 nmhTestv2FsSnoopRouterPortPurgeInterval (UINT4 *  ,INT4  , INT4  ,INT4 );
extern INT1 nmhTestv2FsSnoopVlanRouterPortList(UINT4 *, INT4 , INT4 , INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2FsSnoopVlanBlkRtrPortList (UINT4 *, INT4 , INT4 , INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2FsSnoopVlanMulticastProfileId (UINT4 *, INT4 , INT4 , INT4, UINT4);
extern INT1 nmhTestv2FsSnoopVlanMaxResponseTime (UINT4 *, INT4 , INT4 , INT4, INT4);
extern INT1 nmhTestv2FsSnoopPortPurgeInterval (UINT4 *  ,INT4  , INT4  ,INT4 );
extern INT1 nmhTestv2FsSnoopReportForwardInterval (UINT4 *  ,INT4  , INT4  ,INT4 );
extern INT1 nmhTestv2FsSnoopGrpQueryInterval (UINT4 *  ,INT4  , INT4  ,INT4 );

extern INT1 nmhSetFsSnoopRouterPortPurgeInterval (INT4  , INT4  ,INT4 );
extern INT1 nmhSetFsSnoopVlanRouterPortList(INT4 , INT4 , INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetFsSnoopVlanBlkRtrPortList (INT4 , INT4 , INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetFsSnoopVlanMulticastProfileId (INT4 , INT4 , INT4, INT4);
extern INT1 nmhSetFsSnoopVlanMaxResponseTime (INT4 , INT4 , INT4, INT4);
extern INT1 nmhSetFsSnoopPortPurgeInterval (INT4  , INT4  ,INT4 );
extern INT1 nmhSetFsSnoopReportForwardInterval (INT4  , INT4  ,INT4 );
extern INT1 nmhSetFsSnoopGrpQueryInterval (INT4  , INT4  ,INT4 );


extern INT1 nmhSetFsSnoopStatus (INT4  , INT4  ,INT4 );
extern INT1 nmhSetFsSnoopProxyReportingStatus (INT4  , INT4  ,INT4 );
extern INT1 nmhSetFsSnoopReportFwdOnAllPorts (INT4  , INT4  ,INT4 );
extern INT1 nmhSetFsSnoopQueryFwdOnAllPorts (INT4  , INT4  ,INT4 );
extern INT1 nmhSetFsSnoopRetryCount (INT4  , INT4  ,INT4 );
extern INT1 nmhSetFsSnoopSendQueryOnTopoChange (INT4  , INT4  ,INT4 );
extern INT1 nmhSetFsSnoopProxyStatus (INT4  , INT4  ,INT4 );
extern INT1 nmhSetFsSnoopFilterStatus (INT4  , INT4 ,INT4);
extern INT1 nmhSetFsSnoopMulticastVlanStatus (INT4  , INT4 ,INT4);
extern INT1 nmhSetFsSnoopInstanceGlobalEnhancedMode (INT4 ,INT4);
extern INT1 nmhSetFsSnoopInstanceGlobalSparseMode (INT4 ,INT4);
extern INT1 nmhSetFsSnoopInstanceGlobalLeaveConfigLevel (INT4 ,INT4);
extern INT1 nmhSetFsSnoopInstanceGlobalReportProcessConfigLevel (INT4  ,INT4 );


extern INT1 nmhSetFsSnoopInstanceGlobalMcastFwdMode (INT4  ,INT4 );


extern INT1 nmhGetFirstIndexFsSnoopVlanRouterTable(INT4 *, INT4 *, INT4 *);
extern INT1 nmhGetNextIndexFsSnoopVlanRouterTable(INT4 ,INT4 * ,
                        INT4 ,INT4 *, INT4, INT4 *);
extern INT1 nmhGetFsSnoopVlanRouterLocalPortList(INT4 , INT4 , INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsSnoopVlanRouterPortList(INT4 , INT4 , INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsSnoopVlanBlkRtrPortList (INT4 , INT4 , INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsSnoopVlanMulticastProfileId (INT4 , INT4 , INT4, INT4 *);
extern INT1 nmhGetFsSnoopVlanMaxResponseTime (INT4 , INT4 , INT4, INT4 *);

extern INT1 nmhGetFsSnoopMcastForwardingMode(INT4 ,INT4 *);
extern INT1 nmhGetFirstIndexFsSnoopVlanMcastMacFwdTable(INT4 * ,INT4 * , INT4*, tMacAddr *);
extern INT1 nmhGetFsSnoopVlanMcastMacFwdPortList(INT4 ,INT4 ,INT4, tMacAddr, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetNextIndexFsSnoopVlanMcastMacFwdTable (INT4 ,INT4 * ,INT4 ,
                               INT4 *, INT4, INT4 *,tMacAddr , tMacAddr *);


extern INT1 
nmhGetFirstIndexFsSnoopVlanMcastIpFwdTable (INT4 *, INT4 *, INT4 *,
             tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *);

extern INT1 
nmhGetNextIndexFsSnoopVlanMcastIpFwdTable (INT4 ,INT4 * ,INT4 ,INT4 * ,INT4 ,
               INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *,
           tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *);

extern INT1 
nmhGetFsSnoopVlanMcastIpFwdPortList (INT4, INT4, INT4,
               tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *, 
                tSNMP_OCTET_STRING_TYPE * );

extern INT1 nmhGetFirstIndexFsSnoopVlanIpFwdTable (INT4 * , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , UINT4 *);

extern INT1 nmhGetNextIndexFsSnoopVlanIpFwdTable (INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *);

extern INT1 nmhGetFsSnoopVlanIpFwdPortList (INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , UINT4 ,tSNMP_OCTET_STRING_TYPE * );

extern INT1 nmhGetFirstIndexFsSnoopVlanMcastReceiverTable (INT4 * , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE * );

extern INT1 nmhGetNextIndexFsSnoopVlanMcastReceiverTable (INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * );

extern INT1 nmhGetFsSnoopVlanMcastReceiverFilterMode (INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *);

extern INT1 nmhGetFsSnoopInstanceGlobalEnhancedMode (INT4 , INT4 *);
extern INT1 nmhGetFirstIndexFsSnoopVlanFilterTable (INT4 * , INT4 *, INT4 *);
extern INT1 nmhGetFsSnoopVlanSnoopStatus (INT4 ,INT4 ,INT4, INT4 *);
extern INT1 nmhGetFsSnoopVlanCfgOperVersion (INT4 ,INT4 , INT4, INT4 *);
extern INT1 nmhGetFsSnoopVlanOperatingVersion (INT4 ,INT4 , INT4, INT4 *);
extern INT1 nmhGetFsSnoopVlanFastLeave (INT4 ,INT4 , INT4, INT4 *);
extern INT1 nmhGetFsSnoopVlanCfgQuerier (INT4 ,INT4 , INT4, INT4 *);
extern INT1 nmhGetFsSnoopVlanQuerier (INT4 ,INT4 , INT4, INT4 *);
extern INT1 nmhGetFsSnoopVlanStartupQueryCount (INT4 ,INT4 , INT4, INT4 *);
extern INT1 nmhGetFsSnoopVlanStartupQueryInterval (INT4 ,INT4 , INT4, INT4 *);
extern INT1 nmhGetFsSnoopVlanQueryInterval (INT4 ,INT4 , INT4, INT4 *);
extern INT1 nmhGetFsSnoopVlanOtherQuerierPresentInterval (INT4 ,INT4 , INT4, INT4 *);
extern INT1 nmhGetFsSnoopVlanRtrPortList (INT4 ,INT4 , INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetNextIndexFsSnoopVlanFilterTable (INT4 ,INT4 * ,
                    INT4 ,INT4 *, INT4, INT4 *);

extern INT1 nmhTestv2FsSnoopVlanRowStatus (UINT4 * ,INT4 ,INT4 ,INT4 , INT4);
extern INT1 nmhTestv2FsSnoopVlanSnoopStatus (UINT4 * ,INT4 ,INT4 ,INT4 ,INT4);
extern INT1 nmhTestv2FsSnoopVlanCfgOperVersion (UINT4 * ,INT4 ,INT4 ,INT4 ,INT4);
extern INT1 nmhTestv2FsSnoopVlanFastLeave (UINT4 * ,INT4 ,INT4 ,INT4 ,INT4);
extern INT1 nmhTestv2FsSnoopVlanCfgQuerier (UINT4 * ,INT4 ,INT4 ,INT4 ,INT4);
extern INT1 nmhTestv2FsSnoopVlanStartupQueryCount (UINT4 * ,INT4 ,INT4 ,INT4 ,INT4);
extern INT1 nmhTestv2FsSnoopVlanStartupQueryInterval (UINT4 * ,INT4 ,INT4 ,INT4 ,INT4);
extern INT1 nmhTestv2FsSnoopVlanQueryInterval (UINT4 * ,INT4 ,INT4 ,INT4 ,INT4);
extern INT1 nmhTestv2FsSnoopVlanOtherQuerierPresentInterval (UINT4 * ,INT4 ,INT4 ,INT4 ,INT4);
extern INT1 nmhTestv2FsSnoopVlanRtrPortList (UINT4 * ,INT4 ,INT4 , INT4, tSNMP_OCTET_STRING_TYPE *);

extern INT1 nmhSetFsSnoopVlanRowStatus (INT4 ,INT4 ,INT4 ,INT4);
extern INT1 nmhSetFsSnoopVlanSnoopStatus (INT4 ,INT4 ,INT4 ,INT4);
extern INT1 nmhSetFsSnoopVlanCfgOperVersion (INT4 ,INT4 ,INT4 ,INT4);
extern INT1 nmhSetFsSnoopVlanFastLeave (INT4 ,INT4 ,INT4 ,INT4);
extern INT1 nmhSetFsSnoopVlanCfgQuerier (INT4 ,INT4 ,INT4 ,INT4);
extern INT1 nmhSetFsSnoopVlanStartupQueryCount (INT4 ,INT4 ,INT4 ,INT4);
extern INT1 nmhSetFsSnoopVlanStartupQueryInterval (INT4 ,INT4 ,INT4 ,INT4);
extern INT1 nmhSetFsSnoopVlanQueryInterval (INT4 ,INT4 ,INT4 ,INT4);
extern INT1 nmhSetFsSnoopVlanOtherQuerierPresentInterval (INT4 ,INT4 ,INT4 ,INT4);

extern INT1 nmhGetFirstIndexFsSnoopStatsTable (INT4 * , INT4 * , INT4 *);
extern INT1 nmhGetFsSnoopStatsRxGenQueries (INT4  , INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsSnoopStatsRxGrpQueries (INT4  , INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsSnoopStatsRxGrpAndSrcQueries (INT4  , INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsSnoopStatsRxAsmReports (INT4  , INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsSnoopStatsRxAsmLeaves (INT4  , INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsSnoopStatsDroppedPkts (INT4  , INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsSnoopStatsTxGenQueries (INT4  , INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsSnoopStatsTxGrpQueries (INT4  , INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsSnoopStatsTxAsmReports (INT4  , INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsSnoopStatsTxAsmLeaves (INT4  , INT4  , INT4 ,UINT4 *);

extern INT1 nmhGetNextIndexFsSnoopStatsTable (INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *);

extern INT1 nmhGetFsSnoopStatsRxSsmReports (INT4  , INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsSnoopStatsRxSsmIsInMsgs (INT4  , INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsSnoopStatsRxSsmIsExMsgs (INT4  , INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsSnoopStatsRxSsmToInMsgs (INT4  , INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsSnoopStatsRxSsmToExMsgs (INT4  , INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsSnoopStatsRxSsmAllowMsgs (INT4  , INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsSnoopStatsRxSsmBlockMsgs (INT4  , INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsSnoopStatsTxSsmReports (INT4  , INT4  , INT4 ,UINT4 *);

/*STATIC**/
extern INT1 nmhGetFirstIndexFsSnoopVlanStaticMcastGrpTable(INT4 *, INT4 *, INT4 *,
                          tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetNextIndexFsSnoopVlanStaticMcastGrpTable(INT4, INT4 *, INT4, 
            INT4 *, INT4, INT4 *,
            tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *, 
            tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2FsSnoopVlanStaticMcastGrpRowStatus(UINT4 *, INT4, INT4, INT4,
            tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhSetFsSnoopVlanStaticMcastGrpRowStatus(INT4, INT4, INT4,
            tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhTestv2FsSnoopVlanStaticMcastGrpPortList(UINT4 *, INT4, INT4, INT4,
            tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *, 
                                                     tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetFsSnoopVlanStaticMcastGrpPortList(INT4, INT4, INT4,
            tSNMP_OCTET_STRING_TYPE*, tSNMP_OCTET_STRING_TYPE *, 
                                          tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsSnoopVlanStaticMcastGrpPortList(INT4, INT4, INT4, 
            tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE *, 
                                           tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsSnoopVlanStaticMcastGrpRowStatus(INT4, INT4, INT4,
            tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * , INT4 *); 


extern INT1 nmhTestv2FsSnoopPortLeaveMode (UINT4 *  ,INT4  , INT4  ,INT4 );
extern INT1 nmhTestv2FsSnoopPortRateLimit (UINT4 *  ,INT4  , INT4  ,UINT4 );
extern INT1 nmhTestv2FsSnoopPortMaxLimitType (UINT4 *  ,INT4  , INT4  ,INT4 );
extern INT1 nmhTestv2FsSnoopPortMaxLimit (UINT4 *  ,INT4  , INT4  ,UINT4 );
extern INT1 nmhTestv2FsSnoopPortProfileId (UINT4 *  ,INT4  , INT4  ,UINT4 );
extern INT1 nmhTestv2FsSnoopPortRowStatus (UINT4 *  ,INT4  , INT4  ,INT4 );
extern INT1 nmhGetFsSnoopEnhPortLeaveMode (INT4  , UINT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsSnoopEnhPortRateLimit (INT4  , UINT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsSnoopEnhPortMaxLimitType (INT4  , UINT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsSnoopEnhPortMaxLimit (INT4  , UINT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsSnoopEnhPortProfileId (INT4  , UINT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsSnoopEnhPortRowStatus (INT4  , UINT4  , INT4 ,INT4 *);
extern INT1 nmhSetFsSnoopEnhPortLeaveMode (INT4  , UINT4  , INT4  ,INT4 );
extern INT1 nmhSetFsSnoopEnhPortRateLimit (INT4  , UINT4  , INT4  ,UINT4 );
extern INT1 nmhSetFsSnoopEnhPortMaxLimitType (INT4  , UINT4  , INT4  ,INT4 );
extern INT1 nmhSetFsSnoopEnhPortMaxLimit (INT4  , UINT4  , INT4  ,UINT4 );
extern INT1 nmhSetFsSnoopEnhPortProfileId (INT4  , UINT4  , INT4  ,UINT4 );
extern INT1 nmhSetFsSnoopEnhPortRowStatus (INT4  , UINT4  , INT4  ,INT4 );
extern INT1 nmhTestv2FsSnoopEnhPortLeaveMode (UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 );
extern INT1 nmhTestv2FsSnoopEnhPortRateLimit (UINT4 *  ,INT4  , UINT4  , INT4  ,UINT4 );
extern INT1 nmhTestv2FsSnoopEnhPortMaxLimitType (UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 );
extern INT1 nmhTestv2FsSnoopEnhPortMaxLimit (UINT4 *  ,INT4  , UINT4  , INT4  ,UINT4 );
extern INT1 nmhTestv2FsSnoopEnhPortProfileId (UINT4 *  ,INT4  , UINT4  , INT4  ,UINT4 );
extern INT1 nmhTestv2FsSnoopEnhPortRowStatus (UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 );
extern INT1 nmhGetFirstIndexFsSnoopEnhPortTable (INT4 * , UINT4 * , INT4 *);
extern INT1 nmhGetNextIndexFsSnoopEnhPortTable (INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *);
extern INT1 nmhGetFirstIndexFsSnoopPortTable (INT4 * , INT4 *);
extern INT1 nmhGetNextIndexFsSnoopPortTable (INT4 , INT4 * , INT4 , INT4 *);
extern INT1 nmhGetFirstIndexFsSnoopRtrPortTable (INT4 * , UINT4 * , INT4 *);
extern INT1 nmhGetNextIndexFsSnoopRtrPortTable (INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *);
extern INT1 nmhGetFsSnoopRtrPortOperVersion (INT4  , UINT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsSnoopRtrPortCfgOperVersion (INT4  , UINT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsSnoopOlderQuerierInterval (INT4  , UINT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsSnoopV3QuerierInterval (INT4  , UINT4  , INT4 ,INT4 *);
extern INT1 nmhSetFsSnoopRtrPortCfgOperVersion (INT4  , UINT4  , INT4  ,INT4 );
extern INT1 nmhSetFsSnoopOlderQuerierInterval (INT4  , UINT4  , INT4  ,INT4 );
extern INT1 nmhTestv2FsSnoopRtrPortCfgOperVersion (UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 );
extern INT1 nmhTestv2FsSnoopOlderQuerierInterval (UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 );

extern INT1 nmhGetFsSnoopVlanRtrLocalPortList (INT4 , INT4 ,INT4 ,
            tSNMP_OCTET_STRING_TYPE *);

extern INT4 FsSnoopRouterPortPurgeIntervalSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopPortPurgeIntervalSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopReportForwardIntervalSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopGrpQueryIntervalSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopInstanceGlobalSystemControlSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopProxyReportingStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopInstanceGlobalMcastFwdModeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopReportFwdOnAllPortsSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopQueryFwdOnAllPortsSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopRetryCountSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopSendQueryOnTopoChangeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopVlanRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopVlanSnoopStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopVlanCfgOperVersionSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopVlanFastLeaveSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopVlanCfgQuerierSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopVlanStartupQueryCountSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopVlanStartupQueryIntervalSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopVlanQueryIntervalSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopVlanOtherQuerierPresentIntervalSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopVlanRtrPortListSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopProxyStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopFilterStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopMulticastVlanStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopInstanceGlobalLeaveConfigLevelSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopInstanceGlobalReportProcessConfigLevelSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopInstanceGlobalEnhancedModeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopInstanceGlobalSparseModeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopVlanBlkRtrPortListSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopVlanBlkRtrLocalPortListSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopVlanMulticastProfileIdSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopVlanMaxResponseTimeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopEnhPortLeaveModeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopEnhPortRateLimitSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopEnhPortMaxLimitTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopEnhPortMaxLimitSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopEnhPortProfileIdSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopEnhPortMaxBandwidthLimitSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopEnhPortDropReportsSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopEnhPortRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopRtrPortCfgOperVersionSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopOlderQuerierIntervalSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopVlanStaticMcastGrpRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSnoopVlanStaticMcastGrpPortListSet(tSnmpIndex *, tRetVal *);

#endif 

extern INT1 nmhTestv2IssConfigSaveOption (UINT4 *pu4ErrorCode,
                              INT4 i4TestValIssConfigSaveOption);
extern INT1
nmhSetIssConfigSaveOption (INT4 i4SetValIssConfigSaveOption);
extern INT1
nmhTestv2IssConfigSaveIpvxAddr(UINT4 *pu4ErrorCode ,
                      tSNMP_OCTET_STRING_TYPE *pTestValIssConfigSaveIpAddr);
extern INT1
nmhSetIssConfigSaveIpvxAddr(tSNMP_OCTET_STRING_TYPE *pSetValIssConfigSaveIpAddr);
extern INT1
nmhTestv2IssConfigSaveFileName (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValIssConfigSaveFileName);
extern INT1
nmhSetIssConfigSaveFileName (tSNMP_OCTET_STRING_TYPE *
                             pSetValIssConfigSaveFileName);
extern INT1
nmhTestv2IssInitiateConfigSave (UINT4 *pu4ErrorCode,
                                INT4 i4TestValIssInitiateConfigSave);
extern INT1
nmhSetIssInitiateConfigSave (INT4 i4SetValIssInitiateConfigSave);

#ifdef PNAC_WANTED 
/* Proto Type for Low Level GET FIRST fn  */


extern INT1 nmhGetFirstIndexRadiusExtServerTable ARG_LIST((INT4 *));
extern INT1  nmhGetNextIndexRadiusExtServerTable ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFirstIndexFsPnacASUserConfigTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 
nmhGetFirstIndexFsPnacAuthSessionTable ARG_LIST((tMacAddr *));
/* Proto type for GET_NEXT Routine.  */
extern INT1
nmhGetNextIndexFsPnacASUserConfigTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, 
                                                 tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetNextIndexFsPnacAuthSessionTable ARG_LIST((tMacAddr, tMacAddr *));
/* Proto type for Low Level GET Routine For Pnac LocalAS.  */

extern INT1
nmhGetFsPnacASUserConfigPortList ARG_LIST((tSNMP_OCTET_STRING_TYPE *,
                                           tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetFsPnacASUserConfigPermission ARG_LIST((tSNMP_OCTET_STRING_TYPE *,
                                             INT4 *));
extern INT1
nmhGetFsPnacASUserConfigAuthTimeout ARG_LIST((tSNMP_OCTET_STRING_TYPE *,
                                             UINT4 *));
extern INT1
nmhGetFsPnacAuthSessionIdentifier ARG_LIST((tMacAddr, INT4 *)); 
extern INT1
nmhGetFsPnacAuthSessionAuthPaeState ARG_LIST((tMacAddr, INT4 *)); 
extern INT1
nmhGetFsPnacAuthSessionPortStatus ARG_LIST((tMacAddr, INT4 *));
extern INT1
nmhGetFsPnacAuthSessionPortNumber ARG_LIST((tMacAddr, INT4 *));
extern INT1
nmhGetFsPnacAuthSessionInitialize ARG_LIST((tMacAddr, INT4 *));
extern INT1
nmhGetFsPnacAuthSessionReauthenticate ARG_LIST((tMacAddr, INT4 *));
extern INT1
nmhTestv2FsPnacAuthSessionInitialize ARG_LIST((UINT4 *, tMacAddr, INT4));
extern INT1
nmhSetFsPnacAuthSessionInitialize ARG_LIST((tMacAddr, INT4  ));
extern INT1
nmhTestv2FsPnacAuthSessionReauthenticate ARG_LIST((UINT4 *, tMacAddr, INT4));
extern INT1
nmhSetFsPnacAuthSessionReauthenticate ARG_LIST((tMacAddr, INT4  ));
extern INT1
nmhGetFsPnacAuthSessionFramesRx ARG_LIST((tMacAddr, UINT4 *));
extern INT1    
nmhGetFsPnacAuthSessionFramesTx ARG_LIST((tMacAddr, UINT4 *));
extern INT1
nmhGetFsPnacAuthSessionId ARG_LIST((tMacAddr, tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhGetFsPnacAuthSessionTerminateCause ARG_LIST((tMacAddr, INT4 *));
extern INT1
nmhGetFsPnacAuthSessionUserName ARG_LIST((tMacAddr, tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for Pnac LocalAS.  */

extern INT1
nmhSetFsPnacASUserConfigPassword ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,
                                           tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhSetFsPnacASUserConfigPortList ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,
                                           tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhSetFsPnacASUserConfigPermission ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,
                                             INT4 ));
extern INT1
nmhSetFsPnacASUserConfigAuthTimeout ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,
                                             UINT4 ));
extern INT1
nmhSetFsPnacASUserConfigRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,
                                            INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsPnacASUserConfigPassword ARG_LIST((UINT4 *  ,
                                              tSNMP_OCTET_STRING_TYPE * ,
                                              tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2FsPnacASUserConfigPortList ARG_LIST((UINT4 *  ,
                                              tSNMP_OCTET_STRING_TYPE * ,
                                              tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2FsPnacASUserConfigPermission ARG_LIST((UINT4 *  ,
                                                tSNMP_OCTET_STRING_TYPE * ,
                                                INT4 ));
extern INT1
nmhTestv2FsPnacASUserConfigAuthTimeout ARG_LIST((UINT4 *  ,
                                                tSNMP_OCTET_STRING_TYPE * ,
                                                UINT4 ));
extern INT1
nmhTestv2FsPnacASUserConfigRowStatus ARG_LIST((UINT4 *  ,
                                               tSNMP_OCTET_STRING_TYPE * ,
                                               INT4 ));

extern INT4 FsPnacASUserConfigRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsPnacASUserConfigPasswordSet(tSnmpIndex *, tRetVal *);
extern INT4 FsPnacASUserConfigPermissionSet(tSnmpIndex *, tRetVal *);
extern INT4 FsPnacASUserConfigAuthTimeoutSet(tSnmpIndex *, tRetVal *);
extern INT4 FsPnacASUserConfigPortListSet(tSnmpIndex *, tRetVal *);
extern INT4 FsPnacAuthSessionInitializeSet (tSnmpIndex * , tRetVal *);
extern INT4 FsPnacAuthSessionReauthenticateSet (tSnmpIndex * , tRetVal *);
extern INT1 nmhGetFsRadExtAuthClientServerPortNumber ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsRadExtAuthClientRoundTripTime ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsRadExtAuthClientAccessRequests ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsRadExtAuthClientAccessRetransmissions ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsRadExtAuthClientAccessAccepts ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsRadExtAuthClientAccessRejects ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsRadExtAuthClientAccessChallenges ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsRadExtAuthClientMalformedAccessResponses ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsRadExtAuthClientBadAuthenticators ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsRadExtAuthClientPendingRequests ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsRadExtAuthClientTimeouts ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsRadExtAuthClientUnknownTypes ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsRadExtAuthClientPacketsDropped ARG_LIST((INT4 ,UINT4 *));


#endif
#ifdef MRP_WANTED
extern INT1
nmhSetFsMrpApplicantControlAdminStatus ARG_LIST((UINT4  , UINT4  , tMacAddr  , INT4  ,INT4 ));
extern INT1
nmhTestv2FsMrpApplicantControlAdminStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , tMacAddr  , INT4  ,INT4 ));

extern INT4 FsMrpApplicantControlAdminStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsMrpApplicantControlAdminStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 
FsMrpApplicantControlAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT1
nmhTestv2FsMrpPortStatsClearStatistics (UINT4 *, UINT4 , UINT4 , tMacAddr ,INT4);
extern INT1
nmhSetFsMrpPortStatsClearStatistics (UINT4 , UINT4 , tMacAddr , INT4);
extern INT1
nmhTestv2FsMvrpPortMvrpEnabledStatus (UINT4 *, UINT4 , UINT4 ,INT4);
extern INT1
nmhSetFsMvrpPortMvrpEnabledStatus (UINT4 , UINT4 , INT4 );
extern INT1
nmhTestv2FsMrpPortRestrictedVlanRegistration (UINT4 *, UINT4 , UINT4 , INT4);
extern INT1
nmhTestv2Ieee8021BridgePortMmrpEnabledStatus (UINT4 *, UINT4, UINT4, INT4);
extern INT1
nmhTestv2Ieee8021BridgePortRestrictedGroupRegistration (UINT4 *, UINT4, UINT4, INT4);
extern INT1
nmhSetFsMrpPortRestrictedVlanRegistration (UINT4 , UINT4 , INT4);
extern INT1
nmhSetIeee8021BridgePortMmrpEnabledStatus(UINT4 , UINT4 ,INT4 );
extern INT1
nmhSetIeee8021BridgePortRestrictedGroupRegistration (UINT4 ,UINT4 ,INT4);
extern INT1
nmhGetNextIndexFsMrpPortTable (UINT4, UINT4 *, UINT4, UINT4 *);
extern INT1
nmhTestv2FsMrpInstanceBridgeMvrpEnabledStatus (UINT4 *,UINT4,INT4);
extern INT1
nmhSetFsMrpInstanceBridgeMvrpEnabledStatus (UINT4, INT4);
extern INT1
nmhTestv2FsMrpInstanceNotifyVlanRegFailure (UINT4 *, UINT4, INT4);
extern INT1
nmhSetFsMrpInstanceNotifyVlanRegFailure (UINT4, INT4);
extern INT1
nmhTestv2FsMrpInstanceBridgeMmrpEnabledStatus (UINT4 *,UINT4,INT4);
extern INT1
nmhSetFsMrpInstanceBridgeMmrpEnabledStatus (UINT4, INT4);
extern INT1
nmhTestv2FsMrpInstanceNotifyMacRegFailure (UINT4 *, UINT4, INT4);
extern INT1
nmhSetFsMrpInstanceNotifyMacRegFailure (UINT4, INT4);

INT4
IssClearConfigurationsOnPort (tHttp *, UINT4 , UINT4 , INT4);
INT4
IssClearStatisticsOnPort (tHttp *, UINT4 , UINT4 , INT4);
#endif
VOID
IssProcessVlanMacMapPage (tHttp * pHttp);


VOID
IssProcessVlanMacMapPageGet (tHttp * pHttp);

VOID
IssProcessVlanMacMapPageSet (tHttp * pHttp);

VOID
IssProcessVlanFdbFlushPage (tHttp * pHttp);

VOID
IssProcessVlanFdbFlushPageSet (tHttp * pHttp);

PUBLIC INT1 
nmhGetFirstIndexDot1qFutureVlanPortMacMapTable ARG_LIST
                                          ((INT4 *, tMacAddr *));
                         
PUBLIC INT1
nmhGetNextIndexDot1qFutureVlanPortMacMapTable ARG_LIST
                                          ((INT4,INT4 *,tMacAddr ,tMacAddr*));

PUBLIC INT1
nmhGetDot1qFutureVlanPortMacMapRowStatus ARG_LIST((INT4,tMacAddr,INT4 *));

PUBLIC INT1
nmhGetDot1qFutureVlanPortMacMapVid  ARG_LIST((INT4,tMacAddr,INT4 *));                      

PUBLIC INT1
nmhGetDot1qFutureVlanPortMacMapMcastBcastOption ARG_LIST((INT4,tMacAddr,INT4 *)); 

PUBLIC INT1
nmhTestv2Dot1qFutureVlanPortMacMapVid ARG_LIST((UINT4 *,INT4,tMacAddr ,INT4));

PUBLIC INT1
nmhSetDot1qFutureVlanPortMacMapRowStatus ARG_LIST ((INT4,tMacAddr ,INT4 ));

PUBLIC INT1
nmhSetDot1qFutureVlanPortMacMapVid ARG_LIST ((INT4,tMacAddr ,INT4)); 


PUBLIC INT1
nmhTestv2Dot1qFutureVlanPortMacMapMcastBcastOption ARG_LIST
                                        ((UINT4 *,INT4  ,tMacAddr ,INT4 ));

PUBLIC INT1
nmhSetDot1qFutureVlanPortMacMapMcastBcastOption ARG_LIST
                                        ((INT4 ,tMacAddr  ,INT4 ));

PUBLIC INT1
nmhTestv2Dot1qFutureVlanPortMacMapRowStatus ARG_LIST
                                        ((UINT4 *,INT4 ,tMacAddr ,INT4));
extern VOID 
VlanGetAddedAndDeletedPorts (tSNMP_OCTET_STRING_TYPE *,
                                  tSNMP_OCTET_STRING_TYPE *,
                                  tLocalPortList, tLocalPortList);
extern INT4
nmhGetFirstIndexDot1qVlanStaticTable ARG_LIST((INT4 *));

extern INT4
nmhGetNextIndexDot1qVlanStaticTable ARG_LIST((INT4 , INT4 *));

extern INT4
VlanCfaCliGetIfList (INT1 *, INT1 *, UINT1 *,UINT4 );

extern INT1
nmhGetDot1qVlanStaticName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetDot1qVlanStaticEgressPorts ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetDot1qVlanForbiddenEgressPorts ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetDot1qVlanStaticUntaggedPorts ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetDot1qFutureStVlanEgressEthertype ARG_LIST((UINT4 ,INT4 *));

extern INT1
nmhSetDot1qFutureStVlanEgressEthertype ARG_LIST((UINT4  ,INT4 ));

extern INT1
nmhTestv2Dot1qFutureStVlanEgressEthertype ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1
nmhGetFirstIndexDot1qStaticMulticastTable ARG_LIST((UINT4 * , tMacAddr *  , INT4 *));
extern INT1
nmhGetNextIndexDot1qStaticMulticastTable ARG_LIST((UINT4 , UINT4 * , tMacAddr , tMacAddr *  , INT4 , INT4 *));
extern INT1
nmhGetDot1qStaticMulticastStaticEgressPorts ARG_LIST((UINT4  , tMacAddr  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetDot1qStaticMulticastForbiddenEgressPorts ARG_LIST((UINT4  , tMacAddr  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetDot1qStaticMulticastStatus ARG_LIST((UINT4  , tMacAddr  , INT4 ,INT4 *));
extern INT1
nmhSetDot1qStaticMulticastStaticEgressPorts ARG_LIST((UINT4  , tMacAddr  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhSetDot1qStaticMulticastForbiddenEgressPorts ARG_LIST((UINT4  , tMacAddr  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhSetDot1qStaticMulticastStatus ARG_LIST((UINT4  , tMacAddr  , INT4  ,INT4 ));
extern INT1
nmhTestv2Dot1qStaticMulticastStaticEgressPorts ARG_LIST((UINT4 *  ,UINT4  , tMacAddr  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2Dot1qStaticMulticastForbiddenEgressPorts ARG_LIST((UINT4 *  ,UINT4  , tMacAddr  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2Dot1qStaticMulticastStatus ARG_LIST((UINT4 *  ,UINT4  , tMacAddr  , INT4  ,INT4 ));
extern INT1
nmhGetFirstIndexDot1qStaticUnicastTable ARG_LIST((UINT4 * , tMacAddr *  , INT4 *));
extern INT1
nmhGetNextIndexDot1qStaticUnicastTable ARG_LIST((UINT4 , UINT4 * , tMacAddr , tMacAddr *  , INT4 , INT4 *));
extern INT1
nmhGetDot1qStaticUnicastAllowedToGoTo ARG_LIST((UINT4  , tMacAddr  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetDot1qFutureStaticConnectionIdentifier ARG_LIST((UINT4  , tMacAddr ,INT4 ,tMacAddr * ));

extern INT1
nmhGetDot1qStaticUnicastStatus ARG_LIST((UINT4  , tMacAddr  , INT4 ,INT4 *));
extern INT1
nmhSetDot1qStaticUnicastAllowedToGoTo ARG_LIST((UINT4  , tMacAddr  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhSetDot1qStaticUnicastStatus ARG_LIST((UINT4  , tMacAddr  , INT4  ,INT4 ));
extern INT1
nmhSetDot1qFutureStaticConnectionIdentifier ARG_LIST((UINT4  , tMacAddr  , INT4  ,tMacAddr ));

extern INT1
nmhTestv2Dot1qStaticUnicastAllowedToGoTo ARG_LIST((UINT4 *  ,UINT4  , tMacAddr  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2Dot1qFutureStaticConnectionIdentifier ARG_LIST((UINT4 *  ,UINT4  , tMacAddr  , INT4  ,tMacAddr ));

extern INT1
nmhTestv2Dot1qStaticUnicastStatus ARG_LIST((UINT4 *  ,UINT4  , tMacAddr  , INT4  ,INT4 ));
extern UINT1 CfaGddGetLinkStatus (UINT2 u2IfIndex);
extern INT1 nmhGetIfMainAdminStatus (INT4 i4IfMainIndex, INT4 *pi4RetValIfMainAdminStatus);

extern INT1 nmhGetDot1dBaseBridgeStatus(INT4 *pi4RetValDot1dBaseBridgeStatus);   

extern INT1 nmhGetDot1qFutureVlanPortType (INT4, INT4 *);
extern INT1 nmhSetDot1qFutureVlanPortType (INT4, INT4);
extern INT1 nmhTestv2Dot1qFutureVlanPortType (UINT4 *, INT4, INT4);

extern INT1 nmhTestv2IfMainBrgPortType (UINT4 *, INT4,
                                        INT4);
extern INT1 nmhGetIfMainBrgPortType (INT4 , INT4 *);

extern INT1 nmhSetIfMainBrgPortType (INT4 , INT4 );

extern INT1 nmhGetIfMainMtu (INT4, INT4 *);
extern INT1 nmhSetIfMainMtu (INT4, INT4);
extern INT1 nmhTestv2IfMainMtu (UINT4 *, INT4, INT4);

extern INT1 nmhGetIfLinkUpDownTrapEnable (INT4, INT4 *);
extern INT1 nmhTestv2IfLinkUpDownTrapEnable (UINT4 *, INT4, INT4);
extern INT1 nmhSetIfLinkUpDownTrapEnable (INT4, INT4);

extern INT1
nmhGetDot1qVlanStaticRowStatus ARG_LIST((UINT4 ,INT4 *));
extern INT1
nmhSetDot1qVlanStaticName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhSetDot1qVlanStaticEgressPorts ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhSetDot1qVlanForbiddenEgressPorts ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhSetDot1qVlanStaticUntaggedPorts ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhSetDot1qVlanStaticRowStatus ARG_LIST((UINT4  ,INT4 ));
extern INT1
nmhTestv2Dot1qVlanStaticName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2Dot1qVlanStaticEgressPorts ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2Dot1qVlanForbiddenEgressPorts ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2Dot1qVlanStaticUntaggedPorts ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2Dot1qVlanStaticRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1
nmhGetFirstIndexDot1qFdbTable ARG_LIST((UINT4 *));
extern INT1
nmhGetNextIndexDot1qFdbTable ARG_LIST((UINT4 , UINT4 *));
extern INT1 nmhGetIssMirrorStatus ARG_LIST((INT4 *pi4RetVal));
extern INT1 nmhSetIssMirrorStatus ARG_LIST((INT4 i4RetVal));
extern INT1 nmhGetIssMirrorToPort ARG_LIST((INT4 *pi4RetVal));
extern INT1 nmhSetIssMirrorToPort ARG_LIST((INT4 i4RetVal));
extern INT1
nmhGetFirstIndexFsMstVlanInstanceMappingTable ARG_LIST((INT4 *));
extern INT1
nmhGetNextIndexFsMstVlanInstanceMappingTable ARG_LIST((INT4 , INT4 *));
extern INT1
nmhGetFsMstInstanceVlanMapped ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetFsMstInstanceVlanMapped2k ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetFsMstInstanceVlanMapped3k ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetFsMstInstanceVlanMapped4k ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhSetFsMstSetVlanList ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhSetFsMstResetVlanList ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhGetFsMIMstMstiFlushIndicationThreshold ARG_LIST((INT4  , INT4  ,INT4 *));
extern INT1
nmhSetFsMIMstMstiFlushIndicationThreshold ARG_LIST((INT4  , INT4  ,INT4 ));
extern INT1
nmhTestv2FsMIMstMstiFlushIndicationThreshold ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));
extern INT1
nmhTestv2FsMstSetVlanList ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2FsMstResetVlanList ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhGetIssLoginAuthentication ARG_LIST((INT4 *));
extern INT1 nmhGetIssConfigSaveStatus (INT4 *pi4RetValIssConfigSaveStatus);
extern INT1 nmhGetIssConfigSaveIpvxAddr(tSNMP_OCTET_STRING_TYPE * pRetValIssConfigSaveIpAddr);
extern INT1 nmhGetIssConfigSaveFileName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhGetFirstIndexIfMainTable (INT4 *pi4IfMainIndex);
extern INT1 nmhGetNextIndexIfMainTable (INT4 i4IfMainIndex, INT4 *pi4NextIfMainIndex);

extern INT1 nmhGetIfIvrBridgedIface (INT4, INT4*);
extern INT4 IfIvrBridgedIfaceSet (tSnmpIndex *, tRetVal *);
extern INT1 nmhTestv2IfIvrBridgedIface (UINT4 *, INT4, INT4);
extern INT1 nmhGetFsDot1qPortAcceptableFrameTypes ARG_LIST((INT4 ,INT4 *));
extern INT1
nmhSetIpNetToMediaPhysAddress ARG_LIST((INT4, UINT4, tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2IpNetToMediaPhysAddress ARG_LIST((UINT4 *,INT4,UINT4,tSNMP_OCTET_STRING_TYPE *));
extern INT1
 nmhGetFirstIndexFsMIStdIpNetToPhysicalTable ARG_LIST(( INT4 *,INT4 *, tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhGetFsMIStdIpNetToPhysicalPhysAddress ARG_LIST((INT4,INT4, tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhGetFsMIStdIpNetToPhysicalType ARG_LIST((INT4,INT4, tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1
nmhGetFsMIStdIpNetToPhysicalState ARG_LIST((INT4,INT4, tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1
nmhGetNextIndexFsMIStdIpNetToPhysicalTable ARG_LIST((INT4,INT4 *,INT4,INT4 *,tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2FsMIStdIpv6RouterAdvertRowStatus ARG_LIST((UINT4 *,INT4,INT4));
extern INT1
nmhSetFsMIStdIpNetToPhysicalRowStatus ARG_LIST((INT4,INT4,tSNMP_OCTET_STRING_TYPE *,INT4));
extern INT1
nmhTestv2FsMIStdIpNetToPhysicalPhysAddress ARG_LIST((UINT4 *,INT4,INT4, tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhSetFsMIStdIpNetToPhysicalPhysAddress ARG_LIST((INT4,INT4,tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2FsMIStdIpAddressRowStatus ARG_LIST ((UINT4 *,INT4,INT4,tSNMP_OCTET_STRING_TYPE *,INT4));
extern INT1
nmhSetFsMIStdIpAddressRowStatus ARG_LIST ((INT4,INT4,tSNMP_OCTET_STRING_TYPE *,INT4));
extern INT1
nmhTestv2FsMIStdIpNetToPhysicalRowStatus ARG_LIST ((UINT4 *,INT4,INT4,tSNMP_OCTET_STRING_TYPE *,INT4));
extern INT1
nmhGetFirstIndexFsMIIpvxTraceConfigTable ARG_LIST ((INT4 *,INT4 *,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhGetFsMIIpvxTraceConfigAdminStatus ARG_LIST ((INT4,INT4,tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1
nmhGetFsMIIpvxTraceConfigMaxTTL ARG_LIST ((INT4,INT4,tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1
nmhGetFsMIIpvxTraceConfigMinTTL ARG_LIST ((INT4,INT4,tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1
nmhGetFsMIIpvxTraceConfigOperStatus ARG_LIST ((INT4,INT4,tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1
nmhGetFsMIIpvxTraceConfigTimeout ARG_LIST ((INT4,INT4,tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1
nmhGetFsMIIpvxTraceConfigMtu ARG_LIST ((INT4,INT4,tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1
nmhGetFsMIIpvxTraceIntermHop ARG_LIST ((INT4,tSNMP_OCTET_STRING_TYPE *,INT4,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhGetFsMIIpvxTraceReachTime1 ARG_LIST ((INT4,tSNMP_OCTET_STRING_TYPE *,INT4,INT4 *));
extern INT1
nmhGetFsMIIpvxTraceReachTime2 ARG_LIST ((INT4,tSNMP_OCTET_STRING_TYPE *,INT4,INT4 *));
extern INT1
nmhGetFsMIIpvxTraceReachTime3 ARG_LIST ((INT4,tSNMP_OCTET_STRING_TYPE *,INT4,INT4 *));
extern INT1
nmhGetNextIndexFsMIIpvxTraceConfigTable ARG_LIST ((INT4,INT4 *,INT4,INT4 *,tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2FsMIIpvxTraceConfigAdminStatus ARG_LIST ((UINT4 *,INT4,INT4,tSNMP_OCTET_STRING_TYPE *,INT4));
extern INT1
nmhTestv2FsMIIpvxTraceConfigMaxTTL ARG_LIST ((UINT4 *,INT4,INT4,tSNMP_OCTET_STRING_TYPE *,INT4));
extern INT1
nmhTestv2FsMIIpvxTraceConfigMinTTL ARG_LIST ((UINT4 *,INT4,INT4,tSNMP_OCTET_STRING_TYPE *,INT4));
extern INT1
nmhTestv2FsMIIpvxTraceConfigMtu ARG_LIST ((UINT4 *,INT4,INT4,tSNMP_OCTET_STRING_TYPE *,INT4));
extern INT1
nmhTestv2FsMIIpvxTraceConfigTimeout ARG_LIST ((UINT4 *,INT4,INT4,tSNMP_OCTET_STRING_TYPE *,INT4));
extern INT1
nmhSetFsMIIpvxTraceConfigAdminStatus ARG_LIST ((INT4,INT4,tSNMP_OCTET_STRING_TYPE *,INT4));
extern INT1
nmhSetFsMIIpvxTraceConfigMaxTTL ARG_LIST ((INT4,INT4,tSNMP_OCTET_STRING_TYPE *,INT4));
extern INT1
nmhSetFsMIIpvxTraceConfigMinTTL ARG_LIST ((INT4,INT4,tSNMP_OCTET_STRING_TYPE *,INT4));
extern INT1
nmhSetFsMIIpvxTraceConfigTimeout ARG_LIST ((INT4,INT4,tSNMP_OCTET_STRING_TYPE *,INT4));
extern INT1
nmhSetFsMIIpvxTraceConfigMtu ARG_LIST ((INT4,INT4,tSNMP_OCTET_STRING_TYPE *,INT4));
extern INT1
nmhGetFirstIndexFsMIIpvxTraceTable ARG_LIST ((INT4 *,tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1
nmhGetNextIndexFsMIIpvxTraceTable ARG_LIST ((INT4,INT4 *,tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE *,INT4,INT4 *));

/*OSPF*/
VOID OspfWebUnlock ARG_LIST((tHttp *pHttp));
/* OSPF Related externs */
extern INT4 UtilOspfReleaseContext ARG_LIST((VOID));

extern INT1 nmhGetOspfAdminStat  ARG_LIST((INT4 *));
extern INT1 nmhGetOspfRouterId ARG_LIST((UINT4 *));
extern INT1 nmhGetOspfASBdrRtrStatus ARG_LIST((INT4 *));
extern INT1 nmhGetFutOspfRFC1583Compatibility ARG_LIST((INT4 *));
extern INT1 nmhGetFutOspfNssaAsbrDefRtTrans ARG_LIST((INT4 *));
extern INT1 nmhGetFutOspfABRType ARG_LIST((INT4 *));
extern INT1 nmhGetFsMIOspfExtRouteMetric ARG_LIST((INT4, INT4, INT4, INT4, INT4 *));
extern INT1 nmhGetFsMIOspfSpfDelay ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIOspfSpfHoldtime ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIStdOspfSpfRuns ARG_LIST((INT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsMIOspfTraceLevel ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIOspfExtTraceLevel ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhTestv2OspfAdminStat ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2OspfRouterId ARG_LIST((UINT4 *  ,UINT4 ));
extern INT1 nmhTestv2OspfASBdrRtrStatus ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FutOspfRFC1583Compatibility ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FutOspfNssaAsbrDefRtTrans ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FutOspfABRType ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FutOspfPreferenceValue ARG_LIST((UINT4 *   ,INT4 ));
extern INT1 nmhTestv2FsMIOspfExtRouteMetric ARG_LIST((UINT4 *, INT4, INT4, INT4, INT4, INT4));
extern INT1 nmhTestv2FsMIOspfExtRouteMetricType ARG_LIST((UINT4 *, INT4, INT4, INT4, INT4, INT4));
extern INT1 nmhTestv2FsMIOspfSpfDelay ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIOspfSpfHoldtime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIOspfTraceLevel ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIOspfExtTraceLevel ARG_LIST((UINT4 *  ,INT4  ,INT4 ));



extern INT1 nmhSetOspfAdminStat ARG_LIST((INT4 ));
extern INT1 nmhSetOspfRouterId ARG_LIST((UINT4 ));
extern INT1 nmhSetOspfASBdrRtrStatus ARG_LIST((INT4 ));
extern INT1 nmhSetFutOspfRFC1583Compatibility ARG_LIST((INT4 ));
extern INT1 nmhSetFutOspfNssaAsbrDefRtTrans ARG_LIST((INT4 ));
extern INT1 nmhSetFutOspfABRType ARG_LIST((INT4 ));
extern INT1 nmhSetFutOspfPreferenceValue ARG_LIST ((INT4 ));
extern INT1 nmhSetFsMIOspfExtRouteStatus ARG_LIST((INT4, INT4, INT4, INT4, INT4 ));
extern INT1 nmhSetFsMIOspfExtRouteMetric ARG_LIST((INT4, INT4, INT4, INT4, INT4 ));
extern INT1 nmhSetFsMIOspfExtRouteMetricType ARG_LIST((INT4, INT4, INT4, INT4, INT4 ));
extern INT1 nmhSetFsMIOspfSpfDelay ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsMIOspfSpfHoldtime ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsMIOspfTraceLevel ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsMIOspfExtTraceLevel ARG_LIST((INT4  ,INT4 ));

#ifdef MI_WANTED
extern INT1 nmhGetFsMIStdOspfStatus ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhTestv2FsMIStdOspfStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhSetFsMIStdOspfStatus ARG_LIST((INT4  ,INT4 ));
#endif
/*Neighbor table*/
extern INT1 nmhGetFirstIndexOspfNbrTable ARG_LIST((UINT4 * , INT4 *));
extern INT1 nmhGetNextIndexOspfNbrTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));
extern INT1 nmhGetOspfNbrPriority ARG_LIST((UINT4  , INT4 ,INT4 *));
extern INT1 nmhGetOspfNbmaNbrStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

extern INT1 nmhTestv2OspfNbmaNbrStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2OspfNbrPriority ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

extern INT1 nmhSetOspfNbmaNbrStatus ARG_LIST((UINT4  , INT4  ,INT4 ));
extern INT1 nmhSetOspfNbrPriority ARG_LIST((UINT4  , INT4  ,INT4 ));

/*RRD route table*/
extern INT1 nmhGetFirstIndexFutOspfRRDRouteConfigTable ARG_LIST((UINT4 * , UINT4 *));
extern INT1 nmhGetNextIndexFutOspfRRDRouteConfigTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));
extern INT1 nmhGetFutOspfRRDRouteMetric ARG_LIST((UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFutOspfRRDRouteMetricType ARG_LIST((UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFutOspfRRDRouteTagType ARG_LIST((UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFutOspfRRDRouteTag ARG_LIST((UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFutOspfRRDRouteStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

extern INT1 nmhSetFutOspfRRDRouteMetric ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFutOspfRRDRouteMetricType ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFutOspfRRDRouteTagType ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFutOspfRRDRouteTag ARG_LIST((UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhSetFutOspfRRDRouteStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

extern INT1 nmhTestv2FutOspfRRDRouteMetric ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FutOspfRRDRouteMetricType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FutOspfRRDRouteTagType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FutOspfRRDRouteTag ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhTestv2FutOspfRRDRouteStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/*Area Aggregation Table*/
extern INT1 nmhGetFirstIndexFutOspfAreaAggregateTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , UINT4 *));
extern INT1 nmhGetNextIndexFutOspfAreaAggregateTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));
extern INT1 nmhGetFutOspfAreaAggregateExternalTag ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetOspfAreaAggregateStatus ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetOspfAreaAggregateEffect ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));

extern INT1 nmhSetFutOspfAreaAggregateExternalTag ARG_LIST((UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetOspfAreaAggregateStatus ARG_LIST((UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetOspfAreaAggregateEffect ARG_LIST((UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));

extern INT1 nmhTestv2FutOspfAreaAggregateExternalTag ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2OspfAreaAggregateStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2OspfAreaAggregateEffect ARG_LIST((UINT4 *  ,UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));


/*Graceful Restart Table*/

extern INT1 nmhGetFutOspfOpaqueOption ARG_LIST((INT4 *));
extern INT1 nmhGetFutOspfRestartSupport ARG_LIST((INT4 *));
extern INT1 nmhGetFutOspfHelperSupport ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFutOspfRestartAckState ARG_LIST((INT4 *));
extern INT1 nmhGetFutOspfGraceLsaRetransmitCount ARG_LIST((INT4 *));
extern INT1 nmhGetFutOspfRestartInterval ARG_LIST((INT4 *));
extern INT1 nmhGetFutOspfRestartReason ARG_LIST((INT4 *));
extern INT1 nmhGetFutOspfRestartStrictLsaChecking ARG_LIST((INT4 *));
extern INT1 nmhGetFutOspfHelperGraceTimeLimit ARG_LIST((INT4 *));

extern INT1 nmhSetFutOspfOpaqueOption ARG_LIST((INT4 ));
extern INT1 nmhSetFutOspfRestartSupport ARG_LIST((INT4 ));
extern INT1 nmhSetFutOspfHelperSupport ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFutOspfRestartAckState ARG_LIST((INT4 ));
extern INT1 nmhSetFutOspfGraceLsaRetransmitCount ARG_LIST((INT4 ));
extern INT1 nmhSetFutOspfRestartInterval ARG_LIST((INT4 ));
extern INT1 nmhSetFutOspfRestartReason ARG_LIST((INT4 ));
extern INT1 nmhSetFutOspfRestartStrictLsaChecking ARG_LIST((INT4 ));
extern INT1 nmhSetFutOspfHelperGraceTimeLimit ARG_LIST((INT4 ));

extern INT1 nmhTestv2FutOspfOpaqueOption ARG_LIST(( UINT4 * , INT4));
extern INT1 nmhTestv2FutOspfRestartSupport ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FutOspfRestartAckState ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FutOspfGraceLsaRetransmitCount ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FutOspfRestartInterval ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FutOspfRestartReason ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FutOspfHelperSupport ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FutOspfRestartStrictLsaChecking ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FutOspfHelperGraceTimeLimit ARG_LIST((UINT4 *  ,INT4 ));




/*External Aggregation Table*/
extern INT1 nmhGetFirstIndexFutOspfAsExternalAggregationTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));
extern INT1 nmhGetNextIndexFutOspfAsExternalAggregationTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));
extern INT1 nmhGetFutOspfAsExternalAggregationEffect ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFutOspfAsExternalAggregationTranslation ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFutOspfAsExternalAggregationStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

extern INT1 nmhSetFutOspfAsExternalAggregationEffect ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFutOspfAsExternalAggregationTranslation ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFutOspfAsExternalAggregationStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

extern INT1 nmhTestv2FutOspfAsExternalAggregationEffect ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FutOspfAsExternalAggregationTranslation ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FutOspfAsExternalAggregationStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

extern INT1 nmhGetOspfAreaStatus ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhSetOspfImportAsExtern ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhSetOspfAreaSummary ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhSetOspfAreaStatus ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhTestv2OspfImportAsExtern ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2OspfAreaSummary ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2OspfAreaStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhSetOspfStubMetric ARG_LIST((UINT4  , INT4  ,INT4 ));
extern INT1 nmhSetOspfStubStatus ARG_LIST((UINT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2OspfStubMetric ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2OspfStubStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2OspfStubMetricType ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));
extern INT1 nmhSetOspfStubMetricType ARG_LIST((UINT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2FutOspfAreaNSSATranslatorRole ARG_LIST((UINT4 *  ,UINT4  ,INT4 )); 
extern INT1 nmhSetFutOspfAreaNSSATranslatorRole ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhTestv2FutOspfAreaNSSATranslatorStabilityInterval ARG_LIST((UINT4 *  ,UINT4  ,INT4 )); 
extern INT1 nmhSetFutOspfAreaNSSATranslatorStabilityInterval ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhGetOspfImportAsExtern ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetOspfStubMetric ARG_LIST((UINT4  , INT4 ,INT4 *));
extern INT1 nmhGetOspfStubMetricType ARG_LIST((UINT4  , INT4 ,INT4 *));
extern INT1 nmhGetFutOspfAreaNSSATranslatorRole ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetFutOspfAreaNSSATranslatorStabilityInterval ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetFirstIndexFutOspfAreaTable ARG_LIST((UINT4 *));
extern INT1 nmhGetOspfAreaSummary ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetNextIndexFutOspfAreaTable ARG_LIST((UINT4 , UINT4 *));

extern INT1 nmhGetOspfDemandExtensions ARG_LIST((INT4 *));
extern INT1 nmhSetOspfDemandExtensions ARG_LIST((INT4 ));
extern INT1 nmhTestv2OspfDemandExtensions ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhGetFirstIndexOspfIfTable ARG_LIST((UINT4 * , INT4 *));
extern INT1 nmhGetNextIndexOspfIfTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));
extern INT1 nmhGetOspfIfRtrPriority ARG_LIST((UINT4  , INT4 ,INT4 *));
extern INT1 nmhGetOspfIfTransitDelay ARG_LIST((UINT4  , INT4 ,INT4 *));
extern INT1 nmhGetOspfIfRetransInterval ARG_LIST((UINT4  , INT4 ,INT4 *));
extern INT1 nmhGetOspfIfHelloInterval ARG_LIST((UINT4  , INT4 ,INT4 *));
extern INT1 nmhGetOspfIfRtrDeadInterval ARG_LIST((UINT4  , INT4 ,INT4 *));
extern INT1 nmhGetOspfIfDesignatedRouter ARG_LIST((UINT4  , INT4 ,UINT4 *));
extern INT1 nmhGetOspfIfStatus ARG_LIST((UINT4  , INT4 ,INT4 *));
extern INT1 nmhGetOspfIfDemand ARG_LIST((UINT4  , INT4 ,INT4 *));
extern INT1 nmhGetOspfIfType ARG_LIST((UINT4  , INT4 ,INT4 *));
extern INT1 nmhGetOspfIfAuthType ARG_LIST((UINT4  , INT4 ,INT4 *));
extern INT1 nmhSetOspfIfAreaId ARG_LIST((UINT4  , INT4  ,UINT4 ));
extern INT1 nmhSetOspfIfRtrPriority ARG_LIST((UINT4  , INT4  ,INT4 ));
extern INT1 nmhSetOspfIfTransitDelay ARG_LIST((UINT4  , INT4  ,INT4 ));
extern INT1 nmhSetOspfIfRetransInterval ARG_LIST((UINT4  , INT4  ,INT4 ));
extern INT1 nmhSetOspfIfHelloInterval ARG_LIST((UINT4  , INT4  ,INT4 ));
extern INT1 nmhSetOspfIfRtrDeadInterval ARG_LIST((UINT4  , INT4  ,INT4 ));
extern INT1 nmhSetOspfIfStatus ARG_LIST((UINT4  , INT4  ,INT4 ));
extern INT1 nmhSetOspfIfDemand ARG_LIST((UINT4  , INT4  ,INT4 ));
extern INT1 nmhSetOspfIfType ARG_LIST((UINT4  , INT4  ,INT4 ));
extern INT1 nmhSetOspfIfAuthType ARG_LIST((UINT4  , INT4  ,INT4 ));
extern INT1 nmhGetOspfIfAreaId  ARG_LIST((UINT4, INT4, UINT4*));
extern INT1 nmhTestv2OspfIfType ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2OspfIfAreaId ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));
extern INT1 nmhTestv2OspfIfRtrPriority ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2OspfIfTransitDelay ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2OspfIfRetransInterval ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2OspfIfHelloInterval ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2OspfIfRtrDeadInterval ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2OspfIfStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2OspfIfDemand ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));
extern INT1 nmhGetFutOspfIfPassive ARG_LIST((UINT4  , INT4 ,INT4 *));
extern INT1 nmhSetFutOspfIfPassive ARG_LIST((UINT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2FutOspfIfPassive ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));
extern INT1 nmhGetOspfIfMetricValue ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));
extern INT1 nmhSetOspfIfMetricValue ARG_LIST((UINT4  , INT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2OspfIfMetricValue ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  ,INT4 ));
extern INT1 nmhGetOspfIfMetricStatus ARG_LIST((UINT4  , INT4  , INT4 ,INT4 *));
extern INT1 nmhSetOspfIfMetricStatus ARG_LIST((UINT4  , INT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2OspfIfMetricStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2FutOspfIfMD5AuthKey ARG_LIST((UINT4 *, UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFutOspfIfMD5AuthKey ARG_LIST((UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFutOspfIfMD5AuthKeyStatus ARG_LIST((UINT4  , INT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2FutOspfIfMD5AuthKeyStatus ARG_LIST((UINT4 *, UINT4  , INT4  , INT4  ,INT4 ));
extern INT1 nmhSetOspfIfAuthKey ARG_LIST((UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2OspfIfAuthKey ARG_LIST((UINT4 *, UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2OspfIfAuthType ARG_LIST((UINT4 *, UINT4  , INT4  ,INT4 ));

extern INT1 nmhGetFirstIndexOspfVirtIfTable ARG_LIST((UINT4 * , UINT4 *));
extern INT1 nmhGetNextIndexOspfVirtIfTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));
extern INT1 nmhGetOspfVirtIfTransitDelay ARG_LIST((UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetOspfVirtIfRetransInterval ARG_LIST((UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetOspfVirtIfHelloInterval ARG_LIST((UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetOspfVirtIfRtrDeadInterval ARG_LIST((UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFutOspfVirtIfMD5AuthKey ARG_LIST((UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetOspfVirtIfAuthKey ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetOspfVirtIfStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetOspfVirtIfAuthType ARG_LIST((UINT4  , UINT4 ,INT4 *));

                                                                                                                             
extern INT1 nmhSetOspfVirtIfTransitDelay ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetOspfVirtIfRetransInterval ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetOspfVirtIfHelloInterval ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetOspfVirtIfRtrDeadInterval ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetOspfVirtIfAuthKey ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetOspfVirtIfStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetOspfVirtIfAuthType ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFutOspfVirtIfMD5AuthKey ARG_LIST((UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFutOspfVirtIfMD5AuthKeyStatus ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

extern INT1 nmhTestv2OspfVirtIfTransitDelay ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2OspfVirtIfRetransInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2OspfVirtIfHelloInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2OspfVirtIfRtrDeadInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2OspfVirtIfAuthType ARG_LIST((UINT4 *, UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2OspfVirtIfAuthKey ARG_LIST((UINT4 *, UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FutOspfVirtIfMD5AuthKey ARG_LIST((UINT4 *, UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FutOspfVirtIfMD5AuthKeyStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));
extern INT1 nmhTestv2OspfVirtIfStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

extern INT1 nmhGetFirstIndexOspfLsdbTable ARG_LIST((UINT4 * , INT4 * , UINT4 * , UINT4 *));
extern INT1 nmhGetNextIndexOspfLsdbTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));
extern INT1 nmhGetOspfLsdbSequence ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetOspfLsdbAge ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetOspfLsdbChecksum ARG_LIST((UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFutOspfHotStandbyAdminStatus ARG_LIST((INT4 *));
extern INT1 nmhGetFutOspfHotStandbyState ARG_LIST((INT4 *));
extern INT1 nmhGetFutOspfDynamicBulkUpdStatus ARG_LIST((INT4 *));
extern INT1 nmhGetFutOspfStanbyHelloSyncCount ARG_LIST((UINT4 *));
extern INT1 nmhGetFutOspfStanbyLsaSyncCount ARG_LIST((UINT4 *));

extern INT1 nmhGetFirstIndexFutOspfRoutingTable ARG_LIST((UINT4 * , UINT4 * , INT4 * , UINT4 *));
extern INT1 nmhGetNextIndexFutOspfRoutingTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));
extern INT1 nmhGetFutOspfRouteType ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFutOspfRouteAreaId ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFutOspfRouteCost ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFutOspfRouteType2Cost ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFutOspfRouteInterfaceIndex ARG_LIST((UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));

extern INT1 nmhGetFirstIndexDot1vProtocolGroupTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetNextIndexDot1vProtocolGroupTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetDot1vProtocolGroupId ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1 nmhGetDot1vProtocolGroupRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1 nmhTestv2Dot1vProtocolGroupId ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhTestv2Dot1vProtocolGroupRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhSetDot1vProtocolGroupId ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhSetDot1vProtocolGroupRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT4 nmhGetFirstIndexDot1qTpFdbTable (UINT4 *, tMacAddr * );
extern INT4 nmhGetDot1qTpFdbPort (UINT4 , tMacAddr , INT4 *);
extern INT4 nmhGetDot1qTpFdbStatus (UINT4 , tMacAddr , INT4 *); 
extern INT4 nmhGetNextIndexDot1qTpFdbTable (UINT4, UINT4 *, 
                                            tMacAddr , tMacAddr *); 
extern INT4 nmhGetDot1qVlanFdbId (UINT4 , UINT4, UINT4 *);
extern INT4 nmhGetDot1qVlanCurrentEgressPorts (UINT4 , UINT4, 
                                               tSNMP_OCTET_STRING_TYPE *);
extern INT4 nmhGetDot1qVlanCurrentUntaggedPorts (UINT4 , UINT4, 
                                                tSNMP_OCTET_STRING_TYPE *);
extern INT4 nmhGetDot1qVlanStatus (UINT4 , UINT4, INT4 *);
extern INT4 nmhGetDot1qVlanCreationTime (UINT4 , UINT4, UINT4 *);
extern INT4 nmhGetFirstIndexFsMstCistPortTable (INT4 *);
extern INT4 nmhGetFsMstCistPortDesignatedRoot (INT4 ,
                                              tSNMP_OCTET_STRING_TYPE *);
extern INT4 nmhGetFirstIndexDot1dStpPortTable (INT4 *);
extern INT4 nmhGetDot1dStpPortDesignatedRoot (INT4 , tSNMP_OCTET_STRING_TYPE *);
extern INT4 nmhGetDot1dStpPortDesignatedCost (INT4 , INT4 *);
extern INT4 nmhGetDot1dStpPortDesignatedBridge (INT4 , tSNMP_OCTET_STRING_TYPE *);
extern INT4 nmhGetDot1dStpPortDesignatedPort (INT4 , tSNMP_OCTET_STRING_TYPE *);
extern INT4 nmhGetDot1dStpPortState (INT4 , INT4 *);
extern INT4 nmhGetNextIndexDot1dStpPortTable (INT4 , INT4 * );
extern INT4 nmhGetDot1dStpProtocolSpecification (INT4 * );
extern INT4 nmhGetDot1dStpTimeSinceTopologyChange (INT4 * );
extern INT4 nmhGetDot1dStpTopChanges (UINT4 * );
extern INT4 nmhGetDot1dStpPriority (INT4 * );
extern INT4 nmhGetDot1dStpDesignatedRoot (tSNMP_OCTET_STRING_TYPE * );
extern INT4 nmhGetDot1dStpRootCost (INT4 * );
extern INT4 nmhGetDot1dStpRootPort (INT4 * );
extern INT4 nmhGetDot1dStpMaxAge (INT4 * );
extern INT4 nmhGetDot1dStpHelloTime (INT4 * );
extern INT4 nmhGetDot1dStpHoldTime (INT4 * );
extern INT4 nmhGetDot1dStpForwardDelay (INT4 * );
extern INT4 nmhGetFsMstBrgAddress (tMacAddr * );
extern INT4 nmhGetFsMIMstBrgAddress(INT4, tMacAddr *);
extern INT1 nmhGetFsMIMstMstiBridgeRegionalRoot (INT4 ,INT4,tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsMIMstiRootPriority(INT4, INT4, INT4 *);
extern INT1 nmhTestv2FsMIMstiRootPriority(UINT4 *, INT4, INT4, INT4);
extern INT1 nmhSetFsMIMstiRootPriority(INT4, INT4, INT4);
extern INT1 nmhGetFsMstMstiBridgeRegionalRoot (INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT4 MstValidateInstanceEntry (INT4);
extern INT4 nmhGetDot1dStpBridgeMaxAge (INT4 * );
extern INT4 nmhGetDot1dStpBridgeHelloTime (INT4 * );
extern INT4 nmhGetDot1dStpBridgeForwardDelay (INT4 * );
extern INT4 nmhGetFirstIndexIpNetToMediaTable (INT4 * , UINT4 *);
extern INT4 nmhGetIpNetToMediaPhysAddress (INT4 , UINT4 , 
                      tSNMP_OCTET_STRING_TYPE *);
extern INT4 nmhGetIpNetToMediaType (INT4 , UINT4 , INT4 * );
extern INT4 nmhGetNextIndexIpNetToMediaTable (INT4 , INT4 * , UINT4 , UINT4 * );
extern INT4 nmhGetFirstIndexFsAggIfTable (INT4 * );
extern INT4 nmhGetNextIndexFsAggIfTable (INT4 , INT4 *);
extern INT4 nmhGetFirstIndexDot1vProtocolPortTable (INT4 * , INT4 *);
extern INT4 nmhGetDot1vProtocolPortGroupVid ( INT4 , INT4 , INT4 *);
extern INT4 nmhGetDot1vProtocolPortRowStatus ( INT4 , INT4 , INT4 *);
extern INT4 nmhGetNextIndexDot1vProtocolPortTable ( INT4 , INT4 *, 
                                            INT4 , INT4 *);
extern INT1 nmhGetIssConfigSaveOption (INT4 *);
extern INT1 nmhGetIfMainOperStatus (INT4 , INT4 *);
extern INT1 nmhGetIfMainType (INT4 , INT4 *);
#ifdef DHCP_SRV_WANTED
extern INT4 DhcpSrvSubnetOptRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 DhcpSrvSubnetOptLenSet(tSnmpIndex *, tRetVal *);
extern INT4 DhcpSrvSubnetOptValSet(tSnmpIndex *, tRetVal *);
#endif
#ifdef DHCPC_WANTED
extern INT1 nmhGetFirstIndexDhcpClientOptTable ARG_LIST((INT4 * , INT4 *));
extern INT1 nmhGetNextIndexDhcpClientOptTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));
extern INT1 nmhSetDhcpClientOptRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));
extern INT1 nmhGetFirstIndexDhcpClientConfigTable ARG_LIST((INT4 *));
extern INT1 nmhGetNextIndexDhcpClientConfigTable ARG_LIST((INT4 , INT4 *));     
extern INT1 nmhGetDhcpClientIdentifier ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhSetDhcpClientIdentifier ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2DhcpClientIdentifier ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT4 DhcpClientOptRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 DhcpClientIdentifierSet(tSnmpIndex *, tRetVal *);
extern INT1 nmhGetDhcpClientOptLen ARG_LIST((INT4  , INT4 ,INT4 *));
extern INT1 nmhGetDhcpClientOptVal ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhSetDhcpClientOptVal (INT4 , INT4 , tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhTestv2DhcpClientOptVal (UINT4 *, INT4 , INT4 , tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetDhcpClientOptLen (INT4 , INT4 , INT4 );
extern INT1 nmhTestv2DhcpClientOptLen (UINT4 *, INT4 , INT4 , INT4 );
extern INT4 DhcpClientOptLenSet(tSnmpIndex *, tRetVal *);
extern INT4 DhcpClientOptValSet(tSnmpIndex *, tRetVal *);
#endif
#ifdef DHCP_RLY_WANTED
extern INT1 nmhGetFirstIndexDhcpRelayIfTable (INT4 *);
extern INT1 nmhGetNextIndexDhcpRelayIfTable (INT4 , INT4 *);
extern INT1 nmhGetDhcpRelayIfRowStatus (INT4, INT4 *);
extern INT1 nmhTestv2DhcpRelayIfRowStatus (UINT4 *, INT4, INT4);
extern INT1 nmhSetDhcpRelayIfRowStatus (INT4, INT4);
extern INT1 nmhGetDhcpRelayIfCircuitId(INT4, INT4 *);
extern INT1 nmhTestv2DhcpRelayIfCircuitId(UINT4 *, INT4, INT4);
extern INT1 nmhSetDhcpRelayIfCircuitId(INT4, INT4);
extern INT1 nmhGetDhcpRelayIfRemoteId(INT4,tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2DhcpRelayIfRemoteId (UINT4 *,UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetDhcpRelayIfRemoteId (INT4,tSNMP_OCTET_STRING_TYPE *);
extern INT4 DhcpRelayIfRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 DhcpRelayIfCircuitIdSet(tSnmpIndex *, tRetVal *);
extern INT4 DhcpRelayIfRemoteIdSet(tSnmpIndex *, tRetVal *);
#endif

#ifdef RIP_WANTED
extern INT1
nmhGetFirstIndexFsMIRip2IfConfTable (INT4 * , UINT4 *);

extern INT1
nmhGetFsMIRip2IfAdminStat (INT4  , UINT4 ,INT4 *);

extern INT1
nmhGetFsMIRip2IfSplitHorizonStatus (INT4  , UINT4 ,INT4 *);

extern INT1
nmhGetFsMIRip2IfConfDefRtInstall (INT4  , UINT4 ,INT4 *);

extern INT1
nmhGetFsMIStdRip2IfConfSend (INT4  , UINT4 ,INT4 *);

extern
INT1
nmhGetFsMIStdRip2IfConfReceive (INT4  , UINT4 ,INT4 *);

extern 
INT1
nmhGetFsMIRip2IfConfRouteAgeTmr (INT4  , UINT4 ,INT4 *);

extern 
INT1
nmhGetFsMIRip2IfConfUpdateTmr (INT4  , UINT4 ,INT4 *);

extern 
INT1
nmhGetFsMIRip2IfConfGarbgCollectTmr (INT4  , UINT4 ,INT4 *);

extern 
INT1
nmhGetNextIndexFsMIStdRip2IfConfTable (INT4 , INT4 * , UINT4 , UINT4 *);


extern INT1
nmhGetFsMIStdRip2IfConfStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

extern INT1
nmhTestv2FsMIStdRip2IfConfStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

extern 
INT1
nmhSetFsMIStdRip2IfConfStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

extern 
INT1
nmhSetFsMIStdRip2IfConfSrcAddress ARG_LIST((INT4  , UINT4  ,UINT4 ));

extern
INT1
nmhSetFsMIStdRip2IfConfDefaultMetric ARG_LIST((INT4  , UINT4  ,INT4 ));

extern
INT1
nmhTestv2FsMIStdRip2IfConfDefaultMetric ARG_LIST((UINT4 * ,INT4  , UINT4  ,INT4 ));


extern
INT1
nmhTestv2FsMIRip2IfAdminStat ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

extern INT1
nmhSetFsMIRip2IfAdminStat ARG_LIST((INT4  , UINT4  ,INT4 ));

extern 
INT1
nmhTestv2FsMIRip2IfSplitHorizonStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));



extern INT1
nmhSetFsMIRip2IfSplitHorizonStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

extern 
INT1
nmhTestv2FsMIRip2IfConfDefRtInstall ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

extern INT1
nmhSetFsMIRip2IfConfDefRtInstall ARG_LIST((INT4  , UINT4  ,INT4 ));

extern
INT1
nmhTestv2FsMIStdRip2IfConfSend ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

extern
INT1
nmhSetFsMIStdRip2IfConfSend ARG_LIST((INT4  , UINT4  ,INT4 ));

extern
INT1
nmhTestv2FsMIStdRip2IfConfReceive ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

extern
INT1
nmhSetFsMIStdRip2IfConfReceive ARG_LIST((INT4  , UINT4  ,INT4 ));




extern
INT1
nmhTestv2FsMIRip2IfConfUpdateTmr ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

extern INT1
nmhSetFsMIRip2IfConfUpdateTmr ARG_LIST((INT4  , UINT4  ,INT4 ));

extern INT1
nmhSetFsMIRip2IfConfGarbgCollectTmr ARG_LIST((INT4  , UINT4  ,INT4 ));

extern INT1
nmhSetFsMIRip2IfConfRouteAgeTmr ARG_LIST((INT4  , UINT4  ,INT4 ));



extern INT1
nmhSetFsMIRip2IfConfSpacingTmr ARG_LIST((INT4  , UINT4  ,INT4 ));



extern INT1
nmhTestv2FsMIRip2IfConfGarbgCollectTmr ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

extern INT1
nmhTestv2FsMIRip2IfConfRouteAgeTmr ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

extern INT1
nmhDepv2FsMIRip2IfConfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));



extern  INT1
nmhGetFsMIStdRip2IfConfAuthType ARG_LIST((INT4  , UINT4 ,INT4 *));

extern INT1
nmhGetFsMIStdRip2IfConfAuthKey ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));


extern INT1
nmhGetFsMIStdRip2IfConfDefaultMetric ARG_LIST((INT4  , UINT4 ,INT4 *));


extern INT1
nmhGetFsMIStdRip2IfConfSrcAddress ARG_LIST((INT4  , UINT4 ,UINT4 *));



extern INT1
nmhSetFsMIStdRip2IfConfAuthType ARG_LIST((INT4  , UINT4  ,INT4 ));

extern INT1
nmhSetFsMIStdRip2IfConfAuthKey ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));




extern INT1
nmhTestv2FsMIStdRip2IfConfAuthType ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

extern INT1
nmhTestv2FsMIStdRip2IfConfAuthKey ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));


extern INT1
nmhGetNextIndexFsMIRip2IfConfTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));


extern 
INT1
nmhSetFsMIRipMd5KeyRowStatus ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

extern INT1
nmhSetFsMIRipMd5KeyExpiryTime ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2FsMIRipMd5AuthKey ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern 
INT1
nmhSetFsMIRipMd5AuthKey ARG_LIST((INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern 
INT1
nmhGetFirstIndexFsMIRip2NBRTrustListTable ARG_LIST((INT4 * , UINT4 *));


extern
INT1
nmhGetNextIndexFsMIRip2NBRTrustListTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

extern INT1
nmhGetFsMIRip2TrustNBRListEnable ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhTestv2FsMIRip2TrustNBRListEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


extern INT1
nmhSetFsMIRip2TrustNBRListEnable ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhGetFirstIndexFsMIRipAggTable ARG_LIST((INT4 * , INT4 * , UINT4 * , UINT4 *));

extern
INT1
nmhGetNextIndexFsMIRipAggTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));


extern INT1
nmhTestv2FsMIRipAggStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , UINT4  , UINT4  ,INT4 ));

extern INT1
nmhSetFsMIRipAggStatus ARG_LIST((INT4  , INT4  , UINT4  , UINT4  ,INT4 ));

extern INT1
nmhSetFsMIRipMd5KeyStartTime ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

extern
INT1
nmhTestv2FsMIRip2TrustNBRRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

extern 
INT1
nmhSetFsMIRip2TrustNBRRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));


















extern INT1 nmhGetRip2IfConfAuthType (UINT4, INT4*);
extern INT1 nmhTestv2Rip2IfConfAuthType (UINT4 *  ,UINT4  ,INT4 );
extern INT1 nmhGetRip2IfConfAuthKey (UINT4 ,tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhSetFsRipMd5KeyRowStatus(UINT4,INT4,INT4);
extern INT1 nmhSetFsRipMd5KeyStartTime(UINT4,INT4,INT4);
extern INT1 nmhSetFsRipMd5KeyExpiryTime(UINT4,INT4,INT4);
extern INT1 nmhSetFsRipMd5AuthKey(UINT4,INT4,tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetNextIndexFsRip2IfConfTable (UINT4 , UINT4 *);
extern INT1 nmhGetFirstIndexFsRip2IfConfTable (UINT4 *);
extern INT1 nmhSetRip2IfConfAuthType(UINT4,INT4);
extern INT1 nmhSetRip2IfConfAuthKey(UINT4,tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetRip2IfConfStatus(UINT4,INT4);
extern INT1 
     nmhTestv2FsRipMd5AuthKey(UINT4*,UINT4,INT4,tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFirstIndexFsRipAggTable (INT4 *,UINT4 *,UINT4 *);
extern INT1 nmhGetNextIndexFsRipAggTable (INT4, INT4 *,UINT4,UINT4 *,
                              UINT4,UINT4 *);
extern INT1 nmhTestv2FsRipAggStatus (UINT4 *,INT4,UINT4,UINT4,INT4);
extern INT1 nmhSetFsRipAggStatus (INT4,UINT4,UINT4,INT4);
extern INT1 nmhGetNextIndexFsRip2NBRTrustListTable (UINT4,UINT4 *);
extern INT1 nmhTestv2FsRip2TrustNBRRowStatus (UINT4 *,UINT4,INT4);
extern INT1 nmhSetFsRip2TrustNBRRowStatus (UINT4,INT4);
extern INT1 nmhGetFirstIndexFsRip2NBRTrustListTable (UINT4 *);
extern INT1 nmhTestv2FsRip2TrustNBRListEnable (UINT4 *, INT4);
extern INT1 nmhGetFsMIRipRowStatus (INT4 ,INT4 *);
extern INT1 nmhSetFsMIRipRowStatus (INT4 ,INT4 );
extern INT1 nmhTestv2FsMIRipRowStatus (UINT4 *  ,INT4  ,INT4 );
extern INT1 nmhTestv2FsRipAdminStatus (UINT4 *  ,INT4 ); 
extern INT1 nmhSetFsRipAdminStatus (INT4 );
extern INT1 nmhSetFsRip2SpacingEnable (INT4 );
extern INT1 nmhTestv2FsRip2SpacingEnable (UINT4 *  ,INT4 );
extern INT1 nmhSetFsRip2TrustNBRListEnable (INT4 );
extern INT1 nmhSetFsRip2AutoSummaryStatus (INT4 );
extern INT1 nmhTestv2FsRip2AutoSummaryStatus (UINT4 *  ,INT4 );
extern INT1 nmhGetFsRipAdminStatus (INT4 *);
extern INT1 nmhGetFsRip2SpacingEnable (INT4 *); 
extern INT1 nmhGetFsRip2AutoSummaryStatus (INT4 *);
extern INT1 nmhGetFsRip2TrustNBRListEnable (INT4 *); 
extern INT1 nmhGetFsRip2IfAdminStat (UINT4,INT4 *);
extern INT1 nmhGetFsRip2IfSplitHorizonStatus (UINT4,INT4 *);
extern INT1 nmhGetFsRip2IfConfDefRtInstall (UINT4,INT4 *);
extern INT1 nmhGetRip2IfConfSend (UINT4,INT4 *);
extern INT1 nmhGetRip2IfConfReceive (UINT4,INT4 *);
extern INT1 nmhGetFsRip2IfConfRouteAgeTmr (UINT4,INT4 *);
extern INT1 nmhGetFsRip2IfConfUpdateTmr (UINT4,INT4 *);
extern INT1 nmhGetFsRip2IfConfGarbgCollectTmr (UINT4,INT4 *);
extern INT1 nmhGetRip2IfConfStatus (UINT4,INT4 *);
extern INT1 nmhTestv2Rip2IfConfStatus (UINT4 *,UINT4,INT4);
extern INT1 nmhSetRip2IfConfSrcAddress (UINT4,UINT4);
extern INT1 nmhSetRip2IfConfDefaultMetric (UINT4,INT4);
extern INT1 nmhTestv2FsRip2IfAdminStat (UINT4 *,UINT4,INT4);
extern INT1 nmhSetFsRip2IfAdminStat (UINT4,INT4);
extern INT1 nmhSetFsRip2IfSplitHorizonStatus (UINT4,INT4);
extern INT1 nmhSetFsRip2IfConfDefRtInstall (UINT4,INT4);
extern INT1 nmhSetRip2IfConfSend (UINT4,INT4);
extern INT1 nmhSetRip2IfConfReceive (UINT4,INT4);
extern INT1 nmhSetFsRip2IfConfRouteAgeTmr (UINT4,INT4);
extern INT1 nmhSetFsRip2IfConfUpdateTmr (UINT4,INT4);
extern INT1 nmhSetFsRip2IfConfGarbgCollectTmr (UINT4,INT4);
extern INT1 nmhTestv2FsRip2IfConfGarbgCollectTmr (UINT4 *,UINT4,INT4);
extern INT1 nmhTestv2FsRip2IfConfUpdateTmr (UINT4 *,UINT4,INT4);
extern INT1 nmhTestv2FsRip2IfConfRouteAgeTmr (UINT4 *,UINT4,INT4);
extern INT1 nmhTestv2FsRip2IfSplitHorizonStatus (UINT4 *,UINT4,INT4);
extern INT1 nmhTestv2FsRip2IfConfDefRtInstall (UINT4 *,UINT4,INT4);
extern INT1 nmhTestv2Rip2IfConfSend (UINT4 *,UINT4,INT4);
extern INT1 nmhTestv2Rip2IfConfReceive (UINT4 *,UINT4,INT4);
extern INT1 nmhGetFsRipRRDGlobalStatus(INT4 *);
extern INT1 nmhGetFsRipRRDRouteDefMetric(INT4 *);
extern INT1 nmhGetFsRipRRDRouteTagType(INT4 *);
extern INT1 nmhGetFsRipRRDRouteTag(INT4 *);
extern INT1 nmhGetFsRipRRDRouteMapEnable(tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsRipRRDSrcProtoMaskEnable(INT4 *);
extern INT1 nmhTestv2FsRipRRDSrcProtoMaskDisable(UINT4 *, INT4);
extern INT1 nmhTestv2FsRipRRDGlobalStatus(UINT4 *, INT4);
extern INT1 nmhTestv2FsRipRRDRouteDefMetric(UINT4 *, INT4);
extern INT1 nmhTestv2FsRipRRDSrcProtoMaskEnable(UINT4 *, INT4);
extern INT1 nmhTestv2FsRipRRDRouteTagType(UINT4 *, INT4);
extern INT1 nmhTestv2FsRipRRDRouteTag(UINT4 *, INT4);
extern INT1 nmhTestv2FsRipRRDRouteMapEnable(UINT4 *, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetFsRipRRDSrcProtoMaskDisable(INT4);
extern INT1 nmhSetFsRipRRDGlobalStatus(INT4);
extern INT1 nmhSetFsRipRRDRouteDefMetric(INT4);
extern INT1 nmhSetFsRipRRDSrcProtoMaskEnable(INT4);
extern INT1 nmhSetFsRipRRDRouteTagType(INT4);
extern INT1 nmhSetFsRipRRDRouteTag(INT4);
extern INT1 nmhSetFsRipRRDRouteMapEnable(tSNMP_OCTET_STRING_TYPE *);
extern INT4 FsRipRRDRouteMapEnableSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRipRRDSrcProtoMaskDisableSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRipRRDGlobalStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRipRRDRouteDefMetricSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRipRRDSrcProtoMaskEnableSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRipRRDRouteTagTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRipRRDRouteTagSet(tSnmpIndex *, tRetVal *);


#endif /* RIP_WANTED*/
#ifdef QOSX_WANTED

extern tSNMP_OID_TYPE * WebnmAllocOid (VOID);

extern INT1 nmhGetFirstIndexDiffServSchedulerTable ARG_LIST((UINT4 *));
extern INT1 nmhGetDiffServSchedulerStatus ARG_LIST((UINT4, INT4 *));
extern INT1 nmhGetNextIndexDiffServSchedulerTable ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetDiffServSchedulerMethod ARG_LIST((UINT4,tSNMP_OID_TYPE *));
extern INT1
nmhTestv2DiffServSchedulerMethod ARG_LIST((UINT4 *,UINT4 ,tSNMP_OID_TYPE *));

extern INT1 nmhTestv2DiffServDataPathStart ARG_LIST((UINT4 * , INT4, INT4, tSNMP_OID_TYPE *));
extern INT1 nmhGetFirstIndexDiffServDataPathTable ARG_LIST((INT4 * , INT4 *));
extern INT1 nmhGetDiffServDataPathStart ARG_LIST((INT4, INT4, tSNMP_OID_TYPE *));
extern INT1 nmhGetDiffServDataPathStorage ARG_LIST((INT4,INT4, INT4 *));
extern INT1 nmhGetNextIndexDiffServDataPathTable ARG_LIST((INT4, INT4 * , INT4,INT4 *));
extern INT1 nmhTestv2DiffServDataPathStatus ARG_LIST((UINT4 *, INT4, INT4, INT4));
extern INT1 nmhSetDiffServDataPathStatus  ARG_LIST((INT4  , INT4, INT4 ));
extern INT1 nmhGetDiffServDataPathStatus ARG_LIST((INT4, INT4 , INT4 *));
extern INT1 nmhSetDiffServDataPathStart ARG_LIST((INT4, INT4, tSNMP_OID_TYPE *)); 
extern INT1 nmhTestv2DiffServDataPathStorage ARG_LIST((UINT4 * , INT4 , INT4, INT4));
extern INT1 nmhSetDiffServDataPathStorage ARG_LIST((INT4, INT4, INT4));
extern INT1 nmhGetFirstIndexDiffServClfrTable ARG_LIST((UINT4 *));
extern INT1 nmhGetDiffServClfrStatus ARG_LIST((UINT4 , INT4 *));
extern INT1 nmhGetNextIndexDiffServClfrTable ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetDiffServActionStatus ARG_LIST((UINT4 , INT4 *));
extern INT1 nmhGetFirstIndexDiffServAlgDropTable ARG_LIST((UINT4 *));
extern INT1 nmhGetDiffServAlgDropStatus ARG_LIST((UINT4 , INT4 *));
extern INT1 nmhGetNextIndexDiffServAlgDropTable ARG_LIST((UINT4,UINT4 *));
extern INT1 nmhGetFirstIndexDiffServQTable ARG_LIST((UINT4 *));
extern INT1 nmhGetDiffServQStatus ARG_LIST((UINT4,INT4 *));
extern INT1 nmhGetNextIndexDiffServQTable ARG_LIST((UINT4,UINT4 *));
extern INT1 nmhGetDiffServClfrNextFree ARG_LIST((UINT4 *));
extern INT1 nmhGetDiffServClfrStorage ARG_LIST((UINT4, INT4*));
extern INT1 nmhTestv2DiffServClfrStatus ARG_LIST((UINT4 *, UINT4 ,INT4));
extern INT1 nmhSetDiffServClfrStatus ARG_LIST((UINT4  , INT4));
extern INT1 nmhTestv2DiffServClfrStorage ARG_LIST((UINT4 *, UINT4 , INT4));
extern INT1 nmhSetDiffServClfrStorage ARG_LIST((UINT4, INT4));
extern INT1
nmhGetFirstIndexDiffServMultiFieldClfrTable ARG_LIST((UINT4 *));
extern INT1 nmhGetDiffServMultiFieldClfrStatus ARG_LIST((UINT4, INT4*));    
extern INT1
nmhGetNextIndexDiffServMultiFieldClfrTable ARG_LIST((UINT4, UINT4*));

extern INT1 nmhGetFirstIndexDiffServTBParamTable ARG_LIST ((UINT4 *));
extern INT1 nmhGetDiffServTBParamStatus ARG_LIST ((UINT4,INT4 *));
extern INT1 nmhGetNextIndexDiffServTBParamTable ARG_LIST ((UINT4,UINT4 *));

extern INT1 nmhTestv2DiffServTBParamStatus ARG_LIST ((UINT4 *, UINT4,INT4));

extern INT1
nmhGetNextIndexDiffServClfrElementTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetDiffServClfrElementPrecedence ARG_LIST((UINT4  , UINT4 ,UINT4 *));

extern INT1
nmhGetDiffServClfrElementNext ARG_LIST((UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

extern INT1
nmhGetDiffServClfrElementSpecific ARG_LIST((UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

extern INT1
nmhGetDiffServClfrElementStorage ARG_LIST((UINT4  , UINT4 ,INT4 *));

extern INT1
nmhGetDiffServClfrElementStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetDiffServClfrElementPrecedence ARG_LIST((UINT4  , UINT4  ,UINT4 ));

extern INT1
nmhSetDiffServClfrElementNext ARG_LIST((UINT4  , UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhSetDiffServClfrElementSpecific ARG_LIST((UINT4  , UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhSetDiffServClfrElementStorage ARG_LIST((UINT4  , UINT4  ,INT4 ));

extern INT1
nmhSetDiffServClfrElementStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));


extern INT1
nmhTestv2DiffServClfrElementPrecedence ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

extern INT1
nmhTestv2DiffServClfrElementNext ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhTestv2DiffServClfrElementSpecific ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhTestv2DiffServClfrElementStorage ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

extern INT1
nmhTestv2DiffServClfrElementStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

extern 
INT1
nmhGetDiffServMeterNextFree ARG_LIST((UINT4 *));

extern INT1
nmhGetFirstIndexDiffServMeterTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexDiffServMeterTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetDiffServMeterSucceedNext ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

extern INT1
nmhGetDiffServMeterFailNext ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

extern INT1
nmhGetDiffServMeterSpecific ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

extern INT1
nmhGetDiffServMeterStorage ARG_LIST((UINT4 ,INT4 *));

extern INT1
nmhGetDiffServMeterStatus ARG_LIST((UINT4 ,INT4 *));

extern INT1
nmhSetDiffServMeterSucceedNext ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhSetDiffServMeterFailNext ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhSetDiffServMeterSpecific ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhSetDiffServMeterStorage ARG_LIST((UINT4  ,INT4 ));

extern INT1
nmhSetDiffServMeterStatus ARG_LIST((UINT4  ,INT4 ));

extern INT1
nmhTestv2DiffServMeterSucceedNext ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhTestv2DiffServMeterFailNext ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhTestv2DiffServMeterSpecific ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhTestv2DiffServMeterStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1
nmhTestv2DiffServMeterStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1
nmhGetDiffServTBParamStorage ARG_LIST((UINT4 ,INT4 *));

extern INT1
nmhSetDiffServTBParamStorage ARG_LIST((UINT4  ,INT4 ));

extern INT1
nmhTestv2DiffServTBParamStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));


extern INT1
nmhGetDiffServActionNextFree ARG_LIST((UINT4 *));

extern INT1
 nmhGetFirstIndexDiffServActionTable ARG_LIST((UINT4 *));
extern INT1
nmhGetNextIndexDiffServActionTable ARG_LIST((UINT4 , UINT4 *));
extern INT1
nmhGetDiffServActionInterface ARG_LIST((UINT4 ,INT4 *));
extern INT1
 GetDiffServActionNext ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

extern INT1
 nmhGetDiffServAlgDropNext ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

extern INT1
nmhSetDiffServRandomDropStatus ARG_LIST((UINT4  ,INT4 ));

extern INT1
nmhGetDiffServSchedulerNext ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));


extern INT1
nmhGetDiffServSchedulerMinRate ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

extern INT1
nmhGetDiffServSchedulerMaxRate ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

extern INT1
nmhGetDiffServSchedulerStorage ARG_LIST((UINT4 ,INT4 *));


extern INT1
nmhTestv2DiffServSchedulerNext ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));


extern INT1
nmhTestv2DiffServSchedulerMinRate ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhTestv2DiffServSchedulerMaxRate ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhTestv2DiffServSchedulerStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1
nmhTestv2DiffServSchedulerStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));


extern INT1
 GetDiffServActionSpecific ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));
extern INT1
 GetDiffServActionStorage ARG_LIST((UINT4 ,INT4 *));
extern INT1
 GetDiffServActionStatus ARG_LIST((UINT4 ,INT4 *));
extern INT1
 SetDiffServActionInterface ARG_LIST((UINT4  ,INT4 ));
extern INT1
 SetDiffServActionNext ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));
extern INT1
 SetDiffServActionSpecific ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));
extern INT1
 SetDiffServActionStorage ARG_LIST((UINT4  ,INT4 ));
extern INT1
 SetDiffServActionStatus ARG_LIST((UINT4  ,INT4 ));
extern INT1
 Testv2DiffServActionInterface ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1
 Testv2DiffServActionNext ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));
extern INT1
 Testv2DiffServActionSpecific ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));
extern INT1
 Testv2DiffServActionStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1
 Testv2DiffServActionStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1
nmhGetDiffServCountActNextFree ARG_LIST((UINT4 *));
extern INT1
nmhGetDiffServQNextFree ARG_LIST((UINT4 *));
extern INT1
nmhGetDiffServQNext ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));


extern INT1
nmhGetDiffServQMinRate ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

extern INT1
nmhGetDiffServQMaxRate ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

extern INT1
nmhGetDiffServQStorage ARG_LIST((UINT4 ,INT4 *));

extern INT1
nmhGetDiffServCountActOctets ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetNextIndexDiffServCountActTable ARG_LIST((UINT4 , UINT4 *));

extern INT1
nmhTestv2DiffServQNext ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhTestv2DiffServQMinRate ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhTestv2DiffServQMaxRate ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhTestv2DiffServQStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1
nmhTestv2DiffServQStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1
nmhGetDiffServMinRateNextFree ARG_LIST((UINT4 *));


extern INT1
nmhGetFirstIndexDiffServMinRateTable ARG_LIST((UINT4 *));

extern INT1
nmhGetNextIndexDiffServMinRateTable ARG_LIST((UINT4 , UINT4 *));


extern INT1
nmhGetDiffServMinRatePriority ARG_LIST((UINT4 ,UINT4 *));

extern INT1
nmhGetDiffServMinRateAbsolute ARG_LIST((UINT4 ,UINT4 *));

extern INT1
nmhGetDiffServMinRateRelative ARG_LIST((UINT4 ,UINT4 *));

extern INT1
nmhGetDiffServMinRateStorage ARG_LIST((UINT4 ,INT4 *));

extern INT1
nmhGetDiffServMinRateStatus ARG_LIST((UINT4 ,INT4 *));

extern INT1
nmhGetDiffServActionNext ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

extern INT1
nmhGetDiffServActionSpecific ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

extern INT1
nmhGetDiffServActionStorage ARG_LIST((UINT4 ,INT4 *));


/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2DiffServActionInterface ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1
nmhTestv2DiffServActionNext ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhTestv2DiffServActionSpecific ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhTestv2DiffServActionStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1
nmhTestv2DiffServActionStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1
nmhGetFirstIndexDiffServCountActTable ARG_LIST((UINT4 *));
extern INT1
 GetNextIndexDiffServCountActTable ARG_LIST((UINT4 , UINT4 *));
extern INT1
 GetDiffServCountActOctets ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));
extern INT1
 GetDiffServCountActPkts ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));
extern INT1
 GetDiffServCountActStorage ARG_LIST((UINT4 ,INT4 *));
extern INT1
 GetDiffServCountActStatus ARG_LIST((UINT4 ,INT4 *));
extern INT1
 SetDiffServCountActStorage ARG_LIST((UINT4  ,INT4 ));
extern INT1
 SetDiffServCountActStatus ARG_LIST((UINT4  ,INT4 ));
extern INT1
 Testv2DiffServCountActStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1
 Testv2DiffServCountActStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1
nmhSetDiffServQNext ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));
extern INT1
nmhSetDiffServQMinRate ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhGetDiffServCountActPkts ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1
nmhGetDiffServCountActStorage ARG_LIST((UINT4 ,INT4 *));

extern INT1
nmhGetDiffServCountActStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetDiffServCountActStorage ARG_LIST((UINT4  ,INT4 ));

 extern INT1
nmhSetDiffServCountActStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2DiffServCountActStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1
nmhTestv2DiffServCountActStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1
 Testv2DiffServQNext ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));
extern INT1
 Testv2DiffServQMinRate ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));
extern INT1
 Testv2DiffServQMaxRate ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));
extern INT1
 Testv2DiffServQStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1
 Testv2DiffServQStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1
nmhTestv2DiffServMinRatePriority ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1
nmhTestv2DiffServMinRateAbsolute ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1
nmhTestv2DiffServMinRateRelative ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1
nmhTestv2DiffServMinRateStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1
nmhTestv2DiffServMinRateStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1
nmhSetDiffServMinRatePriority ARG_LIST((UINT4  ,UINT4 ));

extern INT1
nmhSetDiffServMinRateAbsolute ARG_LIST((UINT4  ,UINT4 ));

extern INT1
nmhSetDiffServMinRateRelative ARG_LIST((UINT4  ,UINT4 ));

extern INT1
nmhSetDiffServMinRateStorage ARG_LIST((UINT4  ,INT4 ));

extern INT1
nmhSetDiffServMinRateStatus ARG_LIST((UINT4  ,INT4 ));

extern INT1
nmhGetDiffServMaxRateNextFree ARG_LIST((UINT4 *));

extern INT1
nmhGetDiffServMaxRateAbsolute ARG_LIST((UINT4  , UINT4 ,UINT4 *));

extern INT1
nmhGetDiffServMaxRateRelative ARG_LIST((UINT4  , UINT4 ,UINT4 *));

extern INT1
nmhGetDiffServMaxRateThreshold ARG_LIST((UINT4  , UINT4 ,INT4 *));

extern INT1
nmhGetDiffServMaxRateStorage ARG_LIST((UINT4  , UINT4 ,INT4 *));

extern INT1
nmhGetDiffServMaxRateStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));
extern INT1
 Testv2DiffServMaxRateAbsolute ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));
extern INT1
 Testv2DiffServMaxRateRelative ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));
extern INT1
 Testv2DiffServMaxRateThreshold ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));
extern INT1
 Testv2DiffServMaxRateStorage ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));
extern INT1
 Testv2DiffServMaxRateStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));



extern INT1
 nmhSetDiffServMaxRateAbsolute ARG_LIST((UINT4  , UINT4  ,UINT4 ));

extern  INT1
 nmhSetDiffServMaxRateRelative ARG_LIST((UINT4  , UINT4  ,UINT4 ));

extern  INT1
 nmhSetDiffServMaxRateThreshold ARG_LIST((UINT4  , UINT4  ,INT4 ));

extern  INT1
 nmhSetDiffServMaxRateStorage ARG_LIST((UINT4  , UINT4  ,INT4 ));

extern  INT1
 nmhSetDiffServMaxRateStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

extern INT1
nmhGetFirstIndexDiffServMaxRateTable ARG_LIST((UINT4 * , UINT4 *));

extern INT1
nmhTestv2DiffServMaxRateAbsolute ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

extern INT1
nmhTestv2DiffServMaxRateRelative ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

extern INT1
nmhTestv2DiffServMaxRateThreshold ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

extern INT1
nmhTestv2DiffServMaxRateStorage ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

extern INT1
nmhTestv2DiffServMaxRateStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

extern INT1
nmhGetDiffServTBParamNextFree ARG_LIST((UINT4 *));
extern INT1
nmhGetNextIndexDiffServMaxRateTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));


extern INT1
nmhSetDiffServQMaxRate ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhSetDiffServQStorage ARG_LIST((UINT4  ,INT4 ));

extern INT1
nmhSetDiffServQStatus ARG_LIST((UINT4  ,INT4 ));

extern INT1
nmhSetDiffServAlgDropType ARG_LIST((UINT4  ,INT4 ));

extern INT1
nmhSetDiffServAlgDropNext ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhSetDiffServAlgDropQMeasure ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhSetDiffServAlgDropQThreshold ARG_LIST((UINT4  ,UINT4 ));

extern INT1
nmhSetDiffServAlgDropSpecific ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhSetDiffServAlgDropStorage ARG_LIST((UINT4  ,INT4 ));

extern INT1
nmhSetDiffServAlgDropStatus ARG_LIST((UINT4  ,INT4 ));

extern INT1
nmhTestv2DiffServAlgDropType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1
nmhTestv2DiffServAlgDropNext ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhTestv2DiffServAlgDropQMeasure ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhTestv2DiffServAlgDropQThreshold ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1
nmhTestv2DiffServAlgDropSpecific ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhTestv2DiffServAlgDropStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1
nmhTestv2DiffServAlgDropStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1
nmhTestv2DiffServRandomDropMinThreshBytes ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1
nmhTestv2DiffServRandomDropMinThreshPkts ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1
nmhTestv2DiffServRandomDropMaxThreshBytes ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1
nmhTestv2DiffServRandomDropMaxThreshPkts ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1
nmhTestv2DiffServRandomDropProbMax ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1
nmhTestv2DiffServRandomDropWeight ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1
nmhTestv2DiffServRandomDropSamplingRate ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1
nmhTestv2DiffServRandomDropStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1
nmhTestv2DiffServRandomDropStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1
nmhSetDiffServSchedulerNext ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhSetDiffServSchedulerMethod ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhSetDiffServSchedulerMinRate ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhSetDiffServSchedulerMaxRate ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhSetDiffServSchedulerStorage ARG_LIST((UINT4  ,INT4 ));

extern INT1
nmhSetDiffServSchedulerStatus ARG_LIST((UINT4  ,INT4 ));


extern INT1
nmhGetDiffServAlgDropNextFree ARG_LIST((UINT4 *));
extern INT1
nmhGetDiffServAlgDropQMeasure ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

extern INT1
nmhGetDiffServAlgDropQThreshold ARG_LIST((UINT4 ,UINT4 *));

extern INT1
nmhGetDiffServAlgDropSpecific ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));
extern INT1
nmhGetDiffServAlgDropStorage ARG_LIST((UINT4 ,INT4 *));


extern INT1
nmhGetDiffServRandomDropMinThreshPkts ARG_LIST((UINT4 ,UINT4 *));

extern INT1
nmhGetDiffServRandomDropMaxThreshPkts ARG_LIST((UINT4 ,UINT4 *));

extern INT1
nmhGetDiffServRandomDropSamplingRate ARG_LIST((UINT4 ,UINT4 *));

extern INT1
nmhGetDiffServRandomDropStorage ARG_LIST((UINT4 ,INT4 *));

extern INT1
nmhSetDiffServRandomDropMinThreshPkts ARG_LIST((UINT4  ,UINT4 ));


extern INT1
nmhSetDiffServRandomDropMaxThreshPkts ARG_LIST((UINT4  ,UINT4 ));

extern INT1
nmhSetDiffServRandomDropSamplingRate ARG_LIST((UINT4  ,UINT4 ));

extern INT1
nmhSetDiffServRandomDropStorage ARG_LIST((UINT4  ,INT4 ));

extern INT1
nmhGetDiffServSchedulerNextFree ARG_LIST((UINT4 *));

extern INT1
 GetFirstIndexDiffServSchedulerTable ARG_LIST((UINT4 *));
extern INT1
 GetNextIndexDiffServSchedulerTable ARG_LIST((UINT4 , UINT4 *));
extern INT1
 GetDiffServSchedulerNext ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));
extern INT1
 GetDiffServSchedulerMethod ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));
extern INT1
 GetDiffServSchedulerMinRate ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));
extern INT1
 GetDiffServSchedulerMaxRate ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));
extern INT1
 GetDiffServSchedulerStorage ARG_LIST((UINT4 ,INT4 *));
extern INT1
 GetDiffServSchedulerStatus ARG_LIST((UINT4 ,INT4 *));
extern INT1
 SetDiffServSchedulerNext ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));
extern INT1
 SetDiffServSchedulerMinRate ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));
extern INT1
 SetDiffServSchedulerMaxRate ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));
extern INT1
 SetDiffServSchedulerStorage ARG_LIST((UINT4  ,INT4 ));
extern INT1
 SetDiffServSchedulerStatus ARG_LIST((UINT4  ,INT4 ));
extern INT1
 Testv2DiffServSchedulerNext ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));
extern INT1
 Testv2DiffServSchedulerMethod ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));
extern INT1
 Testv2DiffServSchedulerMinRate ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));
extern INT1
 Testv2DiffServSchedulerMaxRate ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));
extern INT1
 Testv2DiffServSchedulerStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1
 Testv2DiffServSchedulerStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1
 Depv2DiffServSchedulerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
extern INT1
 GetDiffServMinRateNextFree ARG_LIST((UINT4 *));

extern INT1
nmhSetDiffServActionInterface ARG_LIST((UINT4  ,INT4 ));

extern INT1
nmhSetDiffServActionNext ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhSetDiffServActionSpecific ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

extern INT1
nmhSetDiffServActionStorage ARG_LIST((UINT4  ,INT4 ));

extern INT1
nmhSetDiffServActionStatus ARG_LIST((UINT4  ,INT4 ));



extern INT1
 nmhGetDiffServRandomDropNextFree ARG_LIST((UINT4 *));
extern INT1
 SetDiffServAlgDropType ARG_LIST((UINT4  ,INT4 ));
extern INT1
 SetDiffServAlgDropNext ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));
extern INT1
 SetDiffServAlgDropQMeasure ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));
extern INT1
 SetDiffServAlgDropQThreshold ARG_LIST((UINT4  ,UINT4 ));
extern INT1
 SetDiffServAlgDropSpecific ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));
extern INT1
 SetDiffServAlgDropStorage ARG_LIST((UINT4  ,INT4 ));
extern INT1
 SetDiffServAlgDropStatus ARG_LIST((UINT4  ,INT4 ));
extern INT1
 Testv2DiffServAlgDropType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1
 Testv2DiffServAlgDropNext ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));
extern INT1
 Testv2DiffServAlgDropQMeasure ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));
extern INT1
 Testv2DiffServAlgDropQThreshold ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
extern INT1
 Testv2DiffServAlgDropSpecific ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));
extern INT1
 Testv2DiffServAlgDropStorage ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1
 Testv2DiffServAlgDropStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
 

extern INT1 nmhGetFsQoSSystemControl ARG_LIST((INT4 *));

extern INT1 nmhGetFsQoSStatus ARG_LIST((INT4 *));

extern INT1 nmhGetFsQoSTrcFlag ARG_LIST((UINT4 *));

extern INT1 nmhGetFsQoSRateUnit ARG_LIST((INT4 *));

extern INT1 nmhGetFsQoSRateGranularity ARG_LIST((UINT4 *));

extern INT1 nmhSetFsQoSSystemControl ARG_LIST((INT4 ));

extern INT1 nmhSetFsQoSStatus ARG_LIST((INT4 ));

extern INT1 nmhSetFsQoSTrcFlag ARG_LIST((UINT4 ));
extern INT1 nmhTestv2FsQoSSystemControl ARG_LIST((UINT4 *  ,INT4 ));

extern INT1 nmhTestv2FsQoSStatus ARG_LIST((UINT4 *  ,INT4 ));

extern INT1 nmhGetFirstIndexFsQoSPriorityMapTable ARG_LIST((UINT4 *));
extern  INT1 nmhGetNextIndexFsQoSPriorityMapTable ARG_LIST((UINT4 , UINT4 *));
extern INT1 nmhGetFsQoSPriorityMapName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsQoSPriorityMapIfIndex ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetFsQoSPriorityMapVlanId ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetFsQoSPriorityMapInPriority ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetFsQoSPriorityMapInDP ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetFsQoSPriorityMapRegenPriority ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetFsQoSPriorityMapRegenInnerPriority ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetFsQoSPriorityMapConfigStatus ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetFsQoSPriorityMapStatus ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetFsQoSPriorityMapInPriType ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhSetFsQoSPriorityMapInPriType ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhSetFsQoSPriorityMapIfIndex ARG_LIST((UINT4  ,UINT4 ));
extern INT1 nmhSetFsQoSPriorityMapVlanId ARG_LIST((UINT4  ,UINT4 ));
extern INT1 nmhSetFsQoSPriorityMapInPriority ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhSetFsQoSPriorityMapInDP ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhSetFsQoSPriorityMapRegenPriority ARG_LIST((UINT4  ,UINT4 ));
extern INT1 nmhSetFsQoSPriorityMapRegenInnerPriority ARG_LIST((UINT4  ,UINT4 ));
extern INT1 nmhSetFsQoSPriorityMapStatus ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhTestv2FsQoSPriorityMapIfIndex ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsQoSPriorityMapVlanId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsQoSPriorityMapInPriType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2FsQoSPriorityMapInPriority ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2FsQoSPriorityMapInDP ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2FsQoSPriorityMapRegenPriority ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsQoSPriorityMapRegenInnerPriority ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsQoSPriorityMapStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1 nmhGetFirstIndexFsQoSClassMapTable ARG_LIST((UINT4 *));
extern INT1 nmhGetNextIndexFsQoSClassMapTable ARG_LIST((UINT4 , UINT4 *));
extern INT1 nmhGetFsQoSClassMapName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsQoSClassMapL2FilterId ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetFsQoSClassMapL3FilterId ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetFsQoSClassMapPriorityMapId ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetFsQoSClassMapCLASS ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetFsQoSClassMapStatus ARG_LIST((UINT4 ,INT4 *));


extern INT1 nmhSetFsQoSClassMapL2FilterId ARG_LIST((UINT4  ,UINT4 ));
extern INT1 nmhSetFsQoSClassMapL3FilterId ARG_LIST((UINT4  ,UINT4 ));
extern INT1 nmhSetFsQoSClassMapPriorityMapId ARG_LIST((UINT4  ,UINT4 ));
extern INT1 nmhSetFsQoSClassMapCLASS ARG_LIST((UINT4  ,UINT4 ));
extern INT1 nmhSetFsQoSClassMapStatus ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhTestv2FsQoSClassMapName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsQoSClassMapL2FilterId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsQoSClassMapL3FilterId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsQoSClassMapPriorityMapId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsQoSClassMapCLASS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsQoSClassMapStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1 nmhSetFsQoSPolicyMapName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1 nmhSetFsQoSPolicyMapIfIndex ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSPolicyMapCLASS ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSPolicyMapPHBType ARG_LIST((UINT4  ,INT4 ));

extern INT1 nmhSetFsQoSPolicyMapDefaultPHB ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSClassMapPreColor ARG_LIST((UINT4  ,INT4 ));

extern INT1 nmhSetFsQoSPolicyMapMeterTableId ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSPolicyMapInProfileActionFlag ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1 nmhSetFsQoSPolicyMapInProfileActionSetPort ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSPolicyMapConformActionSetIpTOS ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSPolicyMapConformActionSetDscp ARG_LIST((UINT4  ,INT4 ));

extern INT1 nmhSetFsQoSPolicyMapConformActionSetVlanPrio ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSPolicyMapConformActionSetVlanDE ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSPolicyMapConformActionSetInnerVlanPrio ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSPolicyMapConformActionSetMplsExp ARG_LIST((UINT4  ,UINT4 ));
extern INT1 nmhSetFsQoSPolicyMapConformActionSetNewCLASS ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSPolicyMapExceedActionSetIpTOS ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSPolicyMapExceedActionSetDscp ARG_LIST((UINT4  ,INT4 ));

extern INT1 nmhSetFsQoSPolicyMapExceedActionSetInnerVlanPrio ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSPolicyMapExceedActionSetVlanPrio ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSPolicyMapExceedActionSetVlanDE ARG_LIST((UINT4  ,UINT4 ));
extern INT1 nmhSetFsQoSPolicyMapExceedActionSetMplsExp ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSPolicyMapExceedActionSetNewCLASS ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSPolicyMapOutProfileActionFlag ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1 nmhSetFsQoSPolicyMapOutProfileActionSetIPTOS ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSPolicyMapOutProfileActionSetDscp ARG_LIST((UINT4  ,INT4 ));

extern INT1 nmhSetFsQoSPolicyMapOutProfileActionSetInnerVlanPrio ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSPolicyMapOutProfileActionSetVlanPrio ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSPolicyMapOutProfileActionSetVlanDE ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSPolicyMapOutProfileActionSetMplsExp ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSPolicyMapOutProfileActionSetNewCLASS ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSPolicyMapStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1 nmhTestv2FsQoSPolicyMapName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1 nmhTestv2FsQoSPolicyMapIfIndex ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapCLASS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapPHBType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapDefaultPHB ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSClassMapPreColor ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapMeterTableId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapInProfileActionFlag ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsQoSPolicyMapInProfileActionSetPort ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapConformActionSetIpTOS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapConformActionSetDscp ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapConformActionSetVlanPrio ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapConformActionSetVlanDE ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapConformActionSetInnerVlanPrio ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapConformActionSetMplsExp ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapConformActionSetNewCLASS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapExceedActionSetIpTOS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapExceedActionSetDscp ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapExceedActionSetInnerVlanPrio ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapExceedActionSetVlanPrio ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapExceedActionSetVlanDE ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapExceedActionSetMplsExp ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapExceedActionSetNewCLASS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapOutProfileActionFlag ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1 nmhTestv2FsQoSPolicyMapOutProfileActionSetIPTOS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapOutProfileActionSetDscp ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapOutProfileActionSetInnerVlanPrio ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsQoSPolicyMapOutProfileActionSetVlanPrio ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapOutProfileActionSetVlanDE ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapOutProfileActionSetMplsExp ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapOutProfileActionSetNewCLASS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSPolicyMapStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1 nmhSetFsQoSMeterName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1 nmhSetFsQoSMeterType ARG_LIST((UINT4  ,INT4 ));

extern INT1 nmhSetFsQoSMeterInterval ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSMeterColorMode ARG_LIST((UINT4  ,INT4 ));

extern INT1 nmhSetFsQoSMeterCIR ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSMeterCBS ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSMeterEIR ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSMeterEBS ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSMeterNext ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSMeterStatus ARG_LIST((UINT4  ,INT4 ));


extern INT1 nmhTestv2FsQoSMeterName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1 nmhTestv2FsQoSMeterType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1 nmhTestv2FsQoSMeterInterval ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsQoSMeterColorMode ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1 nmhTestv2FsQoSMeterCIR ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSMeterCBS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSMeterEIR ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSMeterEBS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSMeterNext ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSMeterStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));


extern INT1 nmhSetFsQoSQTemplateName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1 nmhSetFsQoSQTemplateDropType ARG_LIST((UINT4  ,INT4 ));

extern INT1 nmhSetFsQoSQTemplateDropAlgoEnableFlag ARG_LIST((UINT4  ,INT4 ));

extern INT1 nmhSetFsQoSQTemplateSize ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSQTemplateStatus ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhTestv2FsQoSQTemplateName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1 nmhTestv2FsQoSQTemplateDropType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1 nmhTestv2FsQoSQTemplateDropAlgoEnableFlag ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1 nmhTestv2FsQoSQTemplateSize ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSQTemplateStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1 nmhSetFsQoSRandomDetectCfgMinAvgThresh ARG_LIST((UINT4  , INT4  ,UINT4 ));

extern INT1 nmhSetFsQoSRandomDetectCfgMaxAvgThresh ARG_LIST((UINT4  , INT4  ,UINT4 ));

extern INT1 nmhSetFsQoSRandomDetectCfgMaxPktSize ARG_LIST((UINT4  , INT4  ,UINT4 ));

extern INT1 nmhSetFsQoSRandomDetectCfgMaxProb ARG_LIST((UINT4  , INT4  ,UINT4 ));

extern INT1 nmhSetFsQoSRandomDetectCfgExpWeight ARG_LIST((UINT4  , INT4  ,UINT4 ));

extern INT1 nmhSetFsQoSRandomDetectCfgStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

extern INT1 nmhSetFsQoSRandomDetectCfgGain ARG_LIST((UINT4  , INT4  ,UINT4 ));

extern INT1 nmhSetFsQoSRandomDetectCfgDropThreshType ARG_LIST((UINT4  , INT4  ,INT4 ));

extern INT1 nmhSetFsQoSRandomDetectCfgECNThresh ARG_LIST((UINT4  , INT4  ,UINT4 ));

extern INT1 nmhSetFsQoSRandomDetectCfgActionFlag ARG_LIST((UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1 nmhTestv2FsQoSRandomDetectCfgMinAvgThresh ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSRandomDetectCfgMaxAvgThresh ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSRandomDetectCfgMaxPktSize ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSRandomDetectCfgMaxProb ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSRandomDetectCfgExpWeight ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSRandomDetectCfgStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

extern INT1 nmhTestv2FsQoSRandomDetectCfgGain ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSRandomDetectCfgDropThreshType ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));                          
extern INT1 nmhTestv2FsQoSRandomDetectCfgECNThresh ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));                              
extern INT1 nmhTestv2FsQoSRandomDetectCfgActionFlag ARG_LIST((UINT4 *  ,UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT4 FsQoSRandomDetectCfgGainSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSRandomDetectCfgDropThreshTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSRandomDetectCfgECNThreshSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSRandomDetectCfgActionFlagSet(tSnmpIndex *, tRetVal *);

extern INT1 nmhGetFsQoSRandomDetectCfgGain ARG_LIST((UINT4  , INT4 ,UINT4 *));

extern INT1 nmhGetFsQoSRandomDetectCfgDropThreshType ARG_LIST((UINT4  , INT4 ,INT4 *));

extern INT1 nmhGetFsQoSRandomDetectCfgECNThresh ARG_LIST((UINT4  , INT4 ,UINT4 *));

extern INT1 nmhGetFsQoSRandomDetectCfgActionFlag ARG_LIST((UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1 nmhGetFirstIndexFsQoSClassToPriorityTable ARG_LIST((UINT4 *));
extern INT1 nmhGetNextIndexFsQoSClassToPriorityTable ARG_LIST((UINT4 , UINT4 *));
extern INT1 nmhGetFsQoSClassToPriorityGroupName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1 nmhGetFsQoSClassToPriorityRegenPri ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetFsQoSClassToPriorityStatus ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhSetFsQoSClassToPriorityRegenPri ARG_LIST((UINT4  ,UINT4 ));
extern INT1 nmhSetFsQoSClassToPriorityGroupName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsQoSClassToPriorityStatus ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhTestv2FsQoSClassToPriorityRegenPri ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSClassToPriorityGroupName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1 nmhTestv2FsQoSClassToPriorityStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhGetFirstIndexFsQoSRandomDetectCfgTable ARG_LIST((UINT4 * , INT4 *));

extern INT1 nmhGetNextIndexFsQoSRandomDetectCfgTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

extern INT1 nmhGetFsQoSRandomDetectCfgMinAvgThresh ARG_LIST((UINT4  , INT4 ,UINT4 *));

extern INT1 nmhGetFsQoSRandomDetectCfgMaxAvgThresh ARG_LIST((UINT4  , INT4 ,UINT4 *));
extern INT1 nmhGetFsQoSRandomDetectCfgMaxPktSize ARG_LIST((UINT4  , INT4 ,UINT4 *));

extern INT1 nmhGetFsQoSRandomDetectCfgMaxProb ARG_LIST((UINT4  , INT4 ,UINT4 *));

extern INT1 nmhGetFsQoSRandomDetectCfgExpWeight ARG_LIST((UINT4  , INT4 ,UINT4 *));

extern INT1 nmhGetFsQoSRandomDetectCfgStatus ARG_LIST((UINT4  , INT4 ,INT4 *));
extern INT1 nmhGetFirstIndexFsQoSQTemplateTable ARG_LIST((UINT4 *));

extern INT1 nmhGetNextIndexFsQoSQTemplateTable ARG_LIST((UINT4 , UINT4 *));

extern INT1 nmhGetFsQoSQTemplateName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1 nmhGetFsQoSQTemplateDropType ARG_LIST((UINT4 ,INT4 *));

extern INT1 nmhGetFsQoSQTemplateDropAlgoEnableFlag ARG_LIST((UINT4 ,INT4 *));

extern INT1 nmhGetFsQoSQTemplateSize ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSQTemplateStatus ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetFirstIndexFsQoSMeterTable ARG_LIST((UINT4 *));

extern INT1 nmhGetNextIndexFsQoSMeterTable ARG_LIST((UINT4 , UINT4 *));

extern INT1 nmhGetFsQoSMeterName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1 nmhGetFsQoSMeterType ARG_LIST((UINT4 ,INT4 *));

extern INT1 nmhGetFsQoSMeterInterval ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSMeterColorMode ARG_LIST((UINT4 ,INT4 *));

extern INT1 nmhGetFsQoSMeterCIR ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSMeterCBS ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSMeterEIR ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSMeterEBS ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSMeterNext ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSMeterStatus ARG_LIST((UINT4 ,INT4 *));

extern INT1 nmhGetFirstIndexFsQoSPolicyMapTable ARG_LIST((UINT4 *));

extern INT1 nmhGetNextIndexFsQoSPolicyMapTable ARG_LIST((UINT4 , UINT4 *));


extern INT1 nmhGetFsQoSPolicyMapName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1 nmhGetFsQoSPolicyMapIfIndex ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSPolicyMapCLASS ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSPolicyMapPHBType ARG_LIST((UINT4 ,INT4 *));

extern INT1 nmhGetFsQoSPolicyMapDefaultPHB ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSClassMapPreColor ARG_LIST((UINT4 ,INT4 *));

extern INT1 nmhGetFsQoSPolicyMapMeterTableId ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSPolicyMapInProfileConformActionFlag ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1 nmhGetFsQoSPolicyMapInProfileActionSetPort ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSPolicyMapConformActionSetIpTOS ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetFsQoSPolicyMapConformActionSetDscp ARG_LIST((UINT4 ,INT4 *));

extern INT1 nmhGetFsQoSPolicyMapConformActionSetVlanPrio ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSPolicyMapConformActionSetVlanDE ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSPolicyMapConformActionSetInnerVlanPrio ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSPolicyMapConformActionSetMplsExp ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSPolicyMapConformActionSetNewCLASS ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetFsQoSPolicyMapInProfileExceedActionFlag ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1 nmhGetFsQoSPolicyMapExceedActionSetIpTOS ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSPolicyMapExceedActionSetDscp ARG_LIST((UINT4 ,INT4 *));

extern INT1 nmhGetFsQoSPolicyMapExceedActionSetInnerVlanPrio ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSPolicyMapExceedActionSetVlanPrio ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSPolicyMapExceedActionSetVlanDE ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSPolicyMapExceedActionSetMplsExp ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSPolicyMapExceedActionSetNewCLASS ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSPolicyMapOutProfileActionFlag ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1 nmhGetFsQoSPolicyMapOutProfileActionSetIPTOS ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSPolicyMapOutProfileActionSetDscp ARG_LIST((UINT4 ,INT4 *));

extern INT1 nmhGetFsQoSPolicyMapOutProfileActionSetInnerVlanPrio ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSPolicyMapOutProfileActionSetVlanPrio ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSPolicyMapOutProfileActionSetVlanDE ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSPolicyMapOutProfileActionSetMplsExp ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSPolicyMapOutProfileActionSetNewCLASS ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhSetFsQoSPolicyMapInProfileConformActionFlag ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsQoSPolicyMapInProfileExceedActionFlag ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsQoSPolicyMapInProfileConformActionFlag ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsQoSPolicyMapInProfileExceedActionFlag ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1 nmhGetFsQoSPolicyMapStatus ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetFirstIndexFsQoSSchedulerTable ARG_LIST((INT4 *,UINT4 *));


extern INT1 nmhGetNextIndexFsQoSSchedulerTable ARG_LIST((INT4,INT4 *,UINT4 , UINT4 *));

extern INT1 nmhGetFsQoSSchedulerSchedAlgorithm ARG_LIST((INT4,UINT4 ,INT4 *));

extern INT1 nmhGetFsQoSSchedulerGlobalId ARG_LIST((INT4  , UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSSchedulerShapeId ARG_LIST((INT4,UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSSchedulerHierarchyLevel ARG_LIST((INT4,UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSSchedulerStatus ARG_LIST((INT4,UINT4 ,INT4 *));


extern INT1 nmhSetFsQoSSchedulerQueueCount ARG_LIST((INT4,UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSSchedulerSchedAlgorithm ARG_LIST((INT4,UINT4  ,INT4 ));

extern INT1 nmhSetFsQoSSchedulerShapeId ARG_LIST((INT4,UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSSchedulerHierarchyLevel ARG_LIST((INT4,UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSSchedulerStatus ARG_LIST((INT4,UINT4  ,INT4 ));
extern INT1 nmhTestv2FsQoSSchedulerQueueCount ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSSchedulerSchedAlgorithm ARG_LIST((UINT4 *  ,INT4,UINT4  ,INT4 ));

extern INT1 nmhTestv2FsQoSSchedulerShapeId ARG_LIST((UINT4 *  ,INT4,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSSchedulerHierarchyLevel ARG_LIST((UINT4 *  ,INT4,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSSchedulerStatus ARG_LIST((UINT4 *  ,INT4,UINT4  ,INT4 ));

extern INT1 nmhGetFirstIndexFsQoSQTable ARG_LIST((INT4 * , UINT4 *));
extern INT1 nmhGetNextIndexFsQoSQTable ARG_LIST((INT4 , INT4 * ,UINT4 , UINT4 *));

extern INT1 nmhGetFsQoSQCfgTemplateId ARG_LIST((INT4 ,UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSQSchedulerId ARG_LIST((INT4 ,UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSQWeight ARG_LIST((INT4 ,UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSQPriority ARG_LIST((INT4 ,UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSQShapeId ARG_LIST((INT4 ,UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSQStatus ARG_LIST((INT4 ,UINT4 ,INT4 *));


extern INT1 nmhSetFsQoSQCfgTemplateId ARG_LIST((INT4 ,UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSQSchedulerId ARG_LIST((INT4 ,UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSQWeight ARG_LIST((INT4 ,UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSQPriority ARG_LIST((INT4 ,UINT4  ,UINT4 ));
extern INT1 nmhSetFsQoSQShapeId ARG_LIST((INT4 ,UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSQStatus ARG_LIST((INT4 ,UINT4  ,INT4 ));


extern INT1 nmhTestv2FsQoSQCfgTemplateId ARG_LIST((UINT4 *  ,INT4 ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSQSchedulerId ARG_LIST((UINT4 *  ,INT4 ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSQWeight ARG_LIST((UINT4 *  ,INT4 ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSQPriority ARG_LIST((UINT4 *  ,INT4 ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSQShapeId ARG_LIST((UINT4 *  ,INT4 ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSQStatus ARG_LIST((UINT4 *  ,INT4 ,UINT4  ,INT4 ));

extern INT1 nmhGetFirstIndexFsQoSQMapTable ARG_LIST((INT4 * , UINT4 * ,INT4 * , UINT4 *)); 

extern INT1 nmhGetNextIndexFsQoSQMapTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * ,INT4 , INT4 *, UINT4 , UINT4 *));

extern INT1 nmhGetFsQoSQMapQId ARG_LIST((INT4  , UINT4  , UINT4  ,UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSQMapStatus ARG_LIST((INT4  , UINT4  ,UINT4  , UINT4 ,INT4 *)); 

extern INT1 nmhSetFsQoSQMapQId ARG_LIST((INT4  , UINT4  ,UINT4  , UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSQMapStatus ARG_LIST((INT4  , UINT4  , UINT4  ,UINT4  ,INT4 )); 
extern INT1 nmhTestv2FsQoSQMapQId ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsQoSQMapStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4  , UINT4  ,INT4 ));
extern INT1 nmhGetFirstIndexFsQoSShapeTemplateTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1 nmhGetNextIndexFsQoSShapeTemplateTable ARG_LIST((UINT4 , UINT4 *));
extern  INT1
 nmhGetDiffServClfrElementNextFree ARG_LIST((UINT4 *));
extern INT1
nmhValidateIndexInstanceDiffServClfrElementTable ARG_LIST((UINT4  , UINT4 ));
extern INT1
nmhGetFirstIndexDiffServClfrElementTable ARG_LIST((UINT4 * , UINT4 *));

extern INT1 nmhGetFsQoSShapeTemplateName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1 nmhGetFsQoSShapeTemplateCIR ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSShapeTemplateCBS ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSShapeTemplateEIR ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSShapeTemplateEBS ARG_LIST((UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSShapeTemplateStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1 nmhSetFsQoSShapeTemplateName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1 nmhSetFsQoSShapeTemplateCIR ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSShapeTemplateCBS ARG_LIST((UINT4  ,UINT4 ));
 
extern INT1 nmhSetFsQoSShapeTemplateEIR ARG_LIST((UINT4  ,UINT4 ));
extern INT1 nmhSetFsQoSShapeTemplateEBS ARG_LIST((UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSShapeTemplateStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */
 
extern INT1 nmhTestv2FsQoSShapeTemplateName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1 nmhTestv2FsQoSShapeTemplateCIR ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSShapeTemplateCBS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSShapeTemplateEIR ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSShapeTemplateEBS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
 
extern INT1 nmhTestv2FsQoSShapeTemplateStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhGetFirstIndexFsQoSHierarchyTable ARG_LIST((INT4 * ,UINT4 * , 
                                                                 UINT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1 nmhGetNextIndexFsQoSHierarchyTable ARG_LIST((INT4 , INT4 * ,UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1 nmhGetFsQoSHierarchyQNext ARG_LIST((INT4  ,UINT4  , UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSHierarchySchedNext ARG_LIST((INT4  ,UINT4  , UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSHierarchyWeight ARG_LIST((INT4  ,UINT4  , UINT4 ,UINT4 *));

extern INT1 nmhGetFsQoSHierarchyPriority ARG_LIST((INT4  ,UINT4  , UINT4 ,INT4 *));

extern INT1 nmhGetFsQoSHierarchyStatus ARG_LIST((INT4  ,UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1 nmhSetFsQoSHierarchyQNext ARG_LIST((INT4  ,UINT4  , UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSHierarchySchedNext ARG_LIST((INT4  ,UINT4  , UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSHierarchyWeight ARG_LIST((INT4  ,UINT4  , UINT4  ,UINT4 ));

extern INT1 nmhSetFsQoSHierarchyPriority ARG_LIST((INT4  ,UINT4  , UINT4  ,INT4 ));

extern INT1 nmhSetFsQoSHierarchyStatus ARG_LIST((INT4  ,UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsQoSHierarchyQNext ARG_LIST((UINT4 *  ,INT4  ,UINT4  , UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSHierarchySchedNext ARG_LIST((UINT4 *  ,INT4  ,UINT4  , UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSHierarchyWeight ARG_LIST((UINT4 *  ,INT4  ,UINT4  , UINT4  ,UINT4 ));

extern INT1 nmhTestv2FsQoSHierarchyPriority ARG_LIST((UINT4 *  ,INT4  ,UINT4  , UINT4  ,INT4 ));

extern INT1 nmhTestv2FsQoSHierarchyStatus ARG_LIST((UINT4 *  ,INT4  ,UINT4  , 
                                                   UINT4  ,INT4 ));

extern INT1 nmhTestv2FsQoSPortDefaultUserPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhSetFsQoSPortDefaultUserPriority ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhGetFsQoSPortDefaultUserPriority ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFirstIndexFsQoSDefUserPriorityTable ARG_LIST((INT4 *)); 
extern INT1 nmhGetNextIndexFsQoSDefUserPriorityTable ARG_LIST((INT4 , INT4 *));
extern INT1 nmhGetFirstIndexFsQoSPolicerStatsTable ARG_LIST((UINT4 *));


extern INT1 nmhGetNextIndexFsQoSPolicerStatsTable ARG_LIST((UINT4 , UINT4 *));


extern INT1 nmhGetFsQoSPolicerStatsConformPkts ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));
extern INT1 nmhGetFsQoSPolicerStatsConformOctets ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1 nmhGetFsQoSPolicerStatsExceedPkts ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1 nmhGetFsQoSPolicerStatsExceedOctets ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1 nmhGetFsQoSPolicerStatsViolatePkts ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1 nmhGetFsQoSPolicerStatsViolateOctets ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));
extern INT1 nmhGetFirstIndexFsQoSCoSQStatsTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1 nmhGetNextIndexFsQoSCoSQStatsTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1 nmhGetFsQoSCoSQStatsEnQPkts ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1 nmhGetFsQoSCoSQStatsEnQBytes ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1 nmhGetFsQoSCoSQStatsDeQPkts ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1 nmhGetFsQoSCoSQStatsDeQBytes ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1 nmhGetFsQoSCoSQStatsDiscardPkts ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1 nmhGetFsQoSCoSQStatsDiscardBytes ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

extern INT1 nmhGetFsQoSCoSQStatsOccupancy ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));
extern INT1 nmhGetFsQoSCoSQStatsCongMgntAlgoDrop ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));
extern INT4 FsQoSPolicyMapInProfileActionSetPortSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapConformActionSetIpTOSSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapConformActionSetDscpSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapConformActionSetVlanPrioSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapConformActionSetVlanDESet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapConformActionSetInnerVlanPrioSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapConformActionSetMplsExpSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapConformActionSetNewCLASSSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapInProfileExceedActionFlagSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapExceedActionSetIpTOSSet(tSnmpIndex *, tRetVal *);            INT4 FsQoSPolicyMapExceedActionSetDscpSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapExceedActionSetInnerVlanPrioSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapExceedActionSetVlanPrioSet(tSnmpIndex *, tRetVal *);   
extern INT4 FsQoSPolicyMapExceedActionSetVlanDESet(tSnmpIndex *, tRetVal *);    
extern INT4 FsQoSPolicyMapExceedActionSetMplsExpSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapExceedActionSetNewCLASSSet(tSnmpIndex *, tRetVal *);    
extern INT4 FsQoSPolicyMapOutProfileActionFlagSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapOutProfileActionSetIPTOSSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapOutProfileActionSetDscpSet(tSnmpIndex *, tRetVal *); 
extern INT4 FsQoSPolicyMapOutProfileActionSetInnerVlanPrioSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapOutProfileActionSetVlanPrioSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapOutProfileActionSetVlanDESet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapOutProfileActionSetMplsExpSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapOutProfileActionSetNewCLASSSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSRandomDetectCfgMinAvgThreshSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSRandomDetectCfgMaxAvgThreshSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSRandomDetectCfgMaxPktSizeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSRandomDetectCfgMaxProbSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSRandomDetectCfgExpWeightSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSShapeTemplateStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSShapeTemplateNameSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSShapeTemplateCIRSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSShapeTemplateCBSSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSShapeTemplateEIRSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSShapeTemplateEBSSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSQMapQIdSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSHierarchyQNextSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSHierarchySchedNextSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSHierarchyWeightSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSHierarchyPrioritySet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSHierarchyStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPriorityMapNameSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPriorityMapIfIndexSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPriorityMapVlanIdSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPriorityMapInPrioritySet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPriorityMapInPriTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPriorityMapRegenPrioritySet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPriorityMapRegenInnerPrioritySet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPriorityMapStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSSystemControlSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSTrcFlagSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSClassMapNameSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSClassMapL2FilterIdSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSClassMapL3FilterIdSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSClassMapPriorityMapIdSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSClassMapCLASSSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSClassMapPreColorSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSClassMapStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSClassToPriorityRegenPriSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSClassToPriorityGroupNameSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSClassToPriorityStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSMeterNameSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSMeterTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSMeterIntervalSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSMeterColorModeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSMeterCIRSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSMeterCBSSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSMeterEIRSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSMeterEBSSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSMeterNextSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSMeterStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapIfIndexSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapCLASSSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapPHBTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapDefaultPHBSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapMeterTableIdSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPolicyMapInProfileConformActionFlagSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSQTemplateNameSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSQTemplateDropTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSQTemplateDropAlgoEnableFlagSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSQTemplateSizeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSQTemplateStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSRandomDetectCfgStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSSchedulerSchedAlgorithmSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSSchedulerShapeIdSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSSchedulerHierarchyLevelSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSSchedulerStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSQCfgTemplateIdSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSQSchedulerIdSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSQWeightSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSQPrioritySet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSQShapeIdSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSQStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSQMapStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsQoSPortDefaultUserPrioritySet(tSnmpIndex *, tRetVal *);
#endif

#ifdef EOAM_WANTED
PUBLIC INT1 nmhGetFirstIndexDot3OamTable (INT4 *);
PUBLIC INT1 nmhGetNextIndexDot3OamTable (INT4 , INT4 *);
PUBLIC INT1 nmhGetDot3OamPeerStatus (INT4 ,INT4 *);
PUBLIC INT1 nmhGetDot3OamPeerMacAddress (INT4 ,tMacAddr *);
PUBLIC INT1 nmhGetDot3OamPeerVendorOui (INT4 ,tSNMP_OCTET_STRING_TYPE *);
PUBLIC INT1 nmhGetDot3OamPeerVendorInfo (INT4 ,UINT4 *);
PUBLIC INT1 nmhGetDot3OamPeerMode (INT4 ,INT4 *);
PUBLIC INT1 nmhGetDot3OamPeerFunctionsSupported 
            (INT4 ,tSNMP_OCTET_STRING_TYPE * );
PUBLIC INT1 nmhGetDot3OamErrSymPeriodEvNotifEnable (INT4 ,INT4 *);
PUBLIC INT1 nmhGetDot3OamErrSymPeriodWindowHi (INT4, UINT4 *);
PUBLIC INT1 nmhGetDot3OamErrSymPeriodWindowLo (INT4, UINT4 *);
PUBLIC INT1 nmhGetDot3OamErrSymPeriodThresholdHi (INT4, UINT4 *);
PUBLIC INT1 nmhGetDot3OamErrSymPeriodThresholdLo (INT4, UINT4 *);
PUBLIC INT1 nmhGetDot3OamErrFrameEvNotifEnable (INT4 ,INT4 *);
PUBLIC INT1 nmhGetDot3OamErrFrameWindow (INT4, UINT4 *);
PUBLIC INT1 nmhGetDot3OamErrFrameThreshold (INT4, UINT4 *);
PUBLIC INT1 nmhGetDot3OamErrFramePeriodEvNotifEnable (INT4 ,INT4 *);
PUBLIC INT1 nmhGetDot3OamErrFramePeriodWindow (INT4, UINT4 *);
PUBLIC INT1 nmhGetDot3OamErrFramePeriodThreshold (INT4, UINT4 *);
PUBLIC INT1 nmhGetDot3OamErrFrameSecsEvNotifEnable (INT4 ,INT4 *);
PUBLIC INT1 nmhGetDot3OamErrFrameSecsSummaryWindow (INT4, UINT4 *);
PUBLIC INT1 nmhGetDot3OamErrFrameSecsSummaryThreshold (INT4, UINT4 *);
PUBLIC INT1 nmhGetDot3OamCriticalEventEnable (INT4 ,INT4 *);
PUBLIC INT1 nmhGetDot3OamDyingGaspEnable (INT4 ,INT4 *);
PUBLIC INT1 nmhTestv2Dot3OamErrSymPeriodEvNotifEnable (UINT4 *,INT4 ,INT4 );
PUBLIC INT1 nmhSetDot3OamErrSymPeriodEvNotifEnable (INT4 ,INT4 );
PUBLIC INT1 nmhTestv2Dot3OamErrSymPeriodWindowHi (UINT4 *,INT4 ,UINT4 );
PUBLIC INT1 nmhSetDot3OamErrSymPeriodWindowHi (INT4, UINT4);
PUBLIC INT1 nmhTestv2Dot3OamErrSymPeriodWindowLo (UINT4 *,INT4 ,UINT4 );
PUBLIC INT1 nmhSetDot3OamErrSymPeriodWindowLo (INT4 , UINT4 );
PUBLIC INT1 nmhTestv2Dot3OamErrSymPeriodThresholdHi (UINT4 *,INT4 ,UINT4 );
PUBLIC INT1 nmhSetDot3OamErrSymPeriodThresholdHi (INT4 , UINT4 );
PUBLIC INT1 nmhTestv2Dot3OamErrSymPeriodThresholdLo (UINT4 *,INT4 ,UINT4 );
PUBLIC INT1 nmhSetDot3OamErrSymPeriodThresholdLo (INT4 ,UINT4 );
PUBLIC INT1 nmhTestv2Dot3OamErrFrameEvNotifEnable (UINT4 *,INT4 ,INT4 );
PUBLIC INT1 nmhSetDot3OamErrFrameEvNotifEnable (INT4 , INT4 );
PUBLIC INT1 nmhTestv2Dot3OamErrFrameWindow (UINT4 *,INT4 ,UINT4 );
PUBLIC INT1 nmhSetDot3OamErrFrameWindow (INT4, UINT4 );
PUBLIC INT1 nmhTestv2Dot3OamErrFrameThreshold (UINT4 *,INT4 ,UINT4 );
PUBLIC INT1 nmhSetDot3OamErrFrameThreshold (INT4 ,UINT4 );
PUBLIC INT1 nmhTestv2Dot3OamErrFramePeriodEvNotifEnable (UINT4 *,INT4 ,INT4 );
PUBLIC INT1 nmhSetDot3OamErrFramePeriodEvNotifEnable (INT4 ,INT4 );
PUBLIC INT1 nmhTestv2Dot3OamErrFramePeriodWindow (UINT4 *,INT4 ,UINT4 );
PUBLIC INT1 nmhSetDot3OamErrFramePeriodWindow (INT4 ,UINT4 );
PUBLIC INT1 nmhTestv2Dot3OamErrFramePeriodThreshold (UINT4 *,INT4 ,UINT4 );
PUBLIC INT1 nmhSetDot3OamErrFramePeriodThreshold (INT4 ,UINT4 );
PUBLIC INT1 nmhTestv2Dot3OamErrFrameSecsEvNotifEnable (UINT4 *,INT4 ,INT4 );
PUBLIC INT1 nmhSetDot3OamErrFrameSecsEvNotifEnable (INT4 ,INT4 );
PUBLIC INT1 nmhTestv2Dot3OamErrFrameSecsSummaryWindow (UINT4 *,INT4 ,UINT4 );
PUBLIC INT1 nmhSetDot3OamErrFrameSecsSummaryWindow (INT4 ,UINT4 );
PUBLIC INT1 nmhTestv2Dot3OamErrFrameSecsSummaryThreshold (UINT4 *,INT4 ,UINT4 );
PUBLIC INT1 nmhSetDot3OamErrFrameSecsSummaryThreshold (INT4 ,UINT4 );
PUBLIC INT1 nmhTestv2Dot3OamCriticalEventEnable (UINT4 *,INT4 ,UINT4 );
PUBLIC INT1 nmhSetDot3OamCriticalEventEnable (INT4, INT4 );
PUBLIC INT1 nmhTestv2Dot3OamDyingGaspEnable (UINT4 *,INT4 ,UINT4 );
PUBLIC INT1 nmhSetDot3OamDyingGaspEnable (INT4 ,INT4 );
PUBLIC INT1 nmhGetFirstIndexDot3OamEventLogTable (INT4 *,UINT4 *);
PUBLIC INT1 nmhGetDot3OamEventLogTimestamp (INT4 ,UINT4 ,UINT4 *);
PUBLIC INT1 nmhGetDot3OamEventLogOui (INT4 ,UINT4 ,tSNMP_OCTET_STRING_TYPE *);
PUBLIC INT1 nmhGetDot3OamEventLogType (INT4 ,UINT4 ,UINT4 *);
PUBLIC INT1 nmhGetDot3OamEventLogLocation (INT4 ,UINT4 ,INT4 * );
PUBLIC INT1 nmhGetDot3OamEventLogWindowHi (INT4 ,UINT4 ,UINT4 *);
PUBLIC INT1 nmhGetDot3OamEventLogWindowLo (INT4 ,UINT4 ,UINT4 * );
PUBLIC INT1 nmhGetDot3OamEventLogThresholdHi (INT4 ,UINT4 ,UINT4 *);
PUBLIC INT1 nmhGetDot3OamEventLogThresholdLo (INT4 ,UINT4 ,UINT4 *);
PUBLIC INT1 nmhGetDot3OamEventLogValue (INT4 ,UINT4 ,tSNMP_COUNTER64_TYPE *);
PUBLIC INT1 nmhGetDot3OamEventLogRunningTotal 
                                       (INT4 ,UINT4 ,tSNMP_COUNTER64_TYPE * );
PUBLIC INT1 nmhGetDot3OamEventLogEventTotal (INT4 ,UINT4 ,UINT4 *);
PUBLIC INT1 nmhGetNextIndexDot3OamEventLogTable (INT4 ,INT4 *,UINT4 ,UINT4 *);

extern INT4 Dot3OamErrSymPeriodEvNotifEnableSet(tSnmpIndex *, tRetVal *);
extern INT4 Dot3OamErrSymPeriodWindowHiSet(tSnmpIndex *, tRetVal *);
extern INT4 Dot3OamErrSymPeriodWindowLoSet(tSnmpIndex *, tRetVal *);
extern INT4 Dot3OamErrSymPeriodThresholdHiSet(tSnmpIndex *, tRetVal *);
extern INT4 Dot3OamErrSymPeriodThresholdLoSet(tSnmpIndex *, tRetVal *);
extern INT4 Dot3OamErrFrameEvNotifEnableSet(tSnmpIndex *, tRetVal *);
extern INT4 Dot3OamErrFrameWindowSet(tSnmpIndex *, tRetVal *);
extern INT4 Dot3OamErrFrameThresholdSet(tSnmpIndex *, tRetVal *);
extern INT4 Dot3OamErrFramePeriodEvNotifEnableSet(tSnmpIndex *, tRetVal *);
extern INT4 Dot3OamErrFramePeriodWindowSet(tSnmpIndex *, tRetVal *);
extern INT4 Dot3OamErrFramePeriodThresholdSet(tSnmpIndex *, tRetVal *);
extern INT4 Dot3OamErrFrameSecsEvNotifEnableSet(tSnmpIndex *, tRetVal *);
extern INT4 Dot3OamErrFrameSecsSummaryWindowSet(tSnmpIndex *, tRetVal *);
extern INT4 Dot3OamErrFrameSecsSummaryThresholdSet(tSnmpIndex *, tRetVal *);
extern INT4 Dot3OamCriticalEventEnableSet(tSnmpIndex *, tRetVal *);
extern INT4 Dot3OamDyingGaspEnableSet(tSnmpIndex *, tRetVal *);

#ifdef EOAM_FM_WANTED
PUBLIC INT1 nmhGetFirstIndexFsFmVarResponseTable (INT4 *, UINT4 *);
PUBLIC INT1 nmhValidateIndexInstanceFsFmVarResponseTable (INT4 ,UINT4 );
PUBLIC INT1 nmhGetFsFmVarResponseRx1 (INT4 ,UINT4 ,tSNMP_OCTET_STRING_TYPE *);
PUBLIC INT1 nmhGetFsFmVarResponseRx2 (INT4 ,UINT4 ,tSNMP_OCTET_STRING_TYPE *);
PUBLIC INT1 nmhGetFsFmVarResponseRx3 (INT4 ,UINT4 ,tSNMP_OCTET_STRING_TYPE *);
PUBLIC INT1 nmhGetFsFmVarResponseRx4 (INT4 ,UINT4 ,tSNMP_OCTET_STRING_TYPE *);
PUBLIC INT1 nmhGetFsFmVarResponseRx5 (INT4 ,UINT4 ,tSNMP_OCTET_STRING_TYPE *);
PUBLIC INT1 nmhGetFsFmVarResponseRx6 (INT4 ,UINT4 ,tSNMP_OCTET_STRING_TYPE *);
PUBLIC INT1 nmhGetNextIndexFsFmVarResponseTable (INT4 ,INT4 *,UINT4 ,UINT4 *);
PUBLIC INT1 nmhGetFirstIndexFsFmLoopbackTable (INT4 *);
PUBLIC INT1 nmhGetFsFmLBTestCount (INT4 ,UINT4 *);
PUBLIC INT1 nmhGetFsFmLBTestPktSize (INT4 ,UINT4 *);
PUBLIC INT1 nmhGetFsFmLBTestPattern (INT4 ,tSNMP_OCTET_STRING_TYPE *);
PUBLIC INT1 nmhGetFsFmLBTestWaitTime (INT4 ,INT4 *);
PUBLIC INT1 nmhGetFsFmLBTestCommand (INT4 ,INT4 *);
PUBLIC INT1 nmhGetNextIndexFsFmLoopbackTable (INT4 ,INT4 *);
#endif
#endif
#ifdef RM_WANTED
extern INT1 nmhGetFsRmSelfNodeId (tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsRmPeerNodeId (tSNMP_OCTET_STRING_TYPE *); 
extern INT1 nmhGetFsRmActiveNodeId (tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsRmNodeState (INT4 *);
extern INT1 nmhGetFsRmHbInterval (INT4 *);
extern INT1 nmhGetFsRmPeerDeadInterval (INT4 *);
extern INT1 nmhGetFsRmPeerDeadIntMultiplier (INT4 *);
extern INT1 nmhSetFsRmHbInterval (INT4);
extern INT1 nmhSetFsRmPeerDeadInterval (INT4);
extern INT1 nmhSetFsRmPeerDeadIntMultiplier (INT4);
extern INT1 nmhTestv2FsRmHbInterval (UINT4 *, INT4);
extern INT1 nmhTestv2FsRmPeerDeadInterval (UINT4 *, INT4);
extern INT1 nmhTestv2FsRmPeerDeadIntMultiplier (UINT4 *, INT4);
extern INT4 FsRmHbIntervalSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRmPeerDeadIntervalSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRmPeerDeadIntMultiplierSet (tSnmpIndex *, tRetVal *);
extern INT4 FsRmStackPortCountSet (tSnmpIndex *, tRetVal *);
extern INT4 FsRmSwitchIdSet (tSnmpIndex *, tRetVal *);
extern INT4 FsRmConfiguredStateSet (tSnmpIndex *, tRetVal *);
extern INT1 nmhGetFsRmPeerStackIpAddr(INT4 , UINT4*);
extern INT4 IssRmStackingInterfaceTypeSet(tSnmpIndex *, tRetVal *);
extern INT1 nmhGetFsRmIpAddress (UINT4 *);
extern INT1 nmhGetFsRmSubnetMask (UINT4 *);
extern INT1 nmhGetFsRmStackInterface (tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhSetFsRmIpAddress (UINT4 );
extern INT1 nmhSetFsRmSubnetMask (UINT4 );
extern INT1 nmhSetFsRmStackInterface (tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2FsRmIpAddress (UINT4 *  ,UINT4 );
extern INT1 nmhTestv2FsRmSubnetMask (UINT4 *  ,UINT4 );
extern INT1 nmhTestv2FsRmStackInterface (UINT4 *  ,tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetIssRmStackingInterfaceType ARG_LIST((INT4 *));
extern INT1 nmhTestv2IssRmStackingInterfaceType ARG_LIST((UINT4 *  ,INT4 ));

extern INT1 nmhTestv2FsRmSwitchId (UINT4 *,INT4);
extern INT1 nmhSetFsRmSwitchId (INT4);
extern INT1 nmhTestv2FsRmConfiguredState (UINT4 *,INT4);
extern INT1 nmhSetFsRmConfiguredState (INT4);

extern INT1 nmhGetFirstIndexFsRmPeerTable(INT4*);
extern INT1 nmhGetFsRmPeerStackMacAddr (INT4,tMacAddr *);
extern INT1 nmhGetNextIndexFsRmPeerTable (INT4,INT4*);
extern INT1 nmhGetMbsmSlotModuleStatus (INT4,INT4*);
extern INT1 nmhTestv2FsRmStackPortCount (UINT4 *,INT4);
extern INT1 nmhSetFsRmStackPortCount (INT4);
extern INT1 nmhGetMbsmLCConfigCardName (INT4 ,tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetFsRmStackPortCount (INT4 *);
extern INT1 nmhGetFsRmSwitchId (INT4 *);
extern INT1 nmhGetFsRmConfiguredState (INT4 *);
extern INT1 nmhGetFsRmStackMacAddr (tMacAddr * );


extern INT1 nmhGetIfInOctets (INT4 ,UINT4 *);
extern INT1 nmhGetIfInUcastPkts (INT4 ,UINT4 *);
extern INT1 nmhGetIfInNUcastPkts (INT4 ,UINT4 *);
extern INT1 nmhGetIfInDiscards (INT4 ,UINT4 *);
extern INT1 nmhGetIfInErrors (INT4 ,UINT4 *);
extern INT1 nmhGetIfHCOutOctets (INT4 ,tSNMP_COUNTER64_TYPE *);
extern INT1 nmhGetIfHCInOctets (INT4 ,tSNMP_COUNTER64_TYPE *);
extern INT1 nmhGetIfOutOctets (INT4 ,UINT4 *);
extern INT1 nmhGetIfOutUcastPkts (INT4 ,UINT4 *);
extern INT1 nmhGetIfOutNUcastPkts (INT4 ,UINT4 *);
extern INT1 nmhGetIfOutDiscards (INT4 ,UINT4 *);
extern INT1 nmhGetIfOutErrors (INT4 ,UINT4 *);

extern INT1 nmhGetFsRmHitlessRestartFlag ARG_LIST((INT4 *));
extern INT1 nmhSetFsRmHitlessRestartFlag ARG_LIST((INT4 ));
extern INT1 nmhTestv2FsRmHitlessRestartFlag ARG_LIST((UINT4 *  ,INT4 ));
extern INT4 FsRmHitlessRestartFlagSet(tSnmpIndex *, tRetVal *);


#endif
extern INT1 nmhGetFirstIndexIfXTable (INT4 *pi4IfIndex);
extern INT1 nmhGetNextIndexIfXTable (INT4 i4IfIndex, INT4 *pi4NextIfIndex);
extern INT4 VlanGetVlanIdFromFdbId (UINT4 , tVlanIfaceVlanId * );

extern INT1 nmhGetFirstIndexDot1qTpGroupTable (UINT4 * , tMacAddr * );
extern INT1 nmhGetNextIndexDot1qTpGroupTable (UINT4 , UINT4 * , tMacAddr ,
                                              tMacAddr * );
extern INT1 nmhGetDot1qTpGroupEgressPorts (UINT4  , tMacAddr ,
        tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetDot1qTpGroupLearnt (UINT4  , tMacAddr ,
        tSNMP_OCTET_STRING_TYPE * );
extern INT4 nmhGetFirstIndexFsLaPortChannelTable (INT4 *);
extern INT1 nmhGetFsLaPortChannelGroup (INT4 , INT4 *);
extern INT1 nmhGetFsLaPortChannelMode (INT4 , INT4 *);
extern INT4 nmhGetNextIndexFsLaPortChannelTable (INT4, INT4 *);
extern INT1 nmhGetDot3adAggPortActorOperState (INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetDot3adAggPortActorAdminKey (INT4 , INT4 *);
extern INT1 nmhSetDot3adAggPortActorAdminKey (INT4 , INT4 );
extern INT1 nmhSetFsLaPortMode(INT4 , INT4 );
extern INT1 nmhTestv2FsLaPortMode (UINT4 *, INT4 , INT4 );
extern INT1 nmhSetDot3adAggPortActorAdminState (INT4, 
                tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetFsLaPortActorResetAdminState(INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2Dot3adAggPortActorAdminKey (UINT4 *, INT4 ,INT4 );
extern INT1 nmhGetFirstIndexDot3adAggTable (INT4 *);
extern INT1 nmhGetFsLaStatus(INT4 *);
extern INT1 nmhTestv2FsLaStatus(UINT4 *, INT4 );
extern INT1 nmhSetFsLaStatus(INT4);
extern INT1 nmhGetDot3adAggPortActorSystemPriority (INT4 , INT4 *);
extern INT1 nmhTestv2Dot3adAggPortActorSystemPriority (UINT4 *, INT4 , INT4 );
extern INT1 nmhSetDot3adAggPortActorSystemPriority (INT4 , INT4 );
extern INT1 nmhGetFsLaActorSystemID (tMacAddr *);
extern INT1 nmhSetFsLaActorSystemID (tMacAddr );
extern INT1 nmhTestv2FsLaActorSystemID (UINT4 *, tMacAddr );
extern INT4 LaSnmpLowValidatePortIndex (INT4);
extern INT1 nmhGetFsLaPortChannelDefaultPortIndex(INT4 ,INT4 *);
extern INT1 nmhGetFsLaPortSelectAggregator(INT4 ,INT4 *);
extern INT1 nmhSetFsLaPortChannelDefaultPortIndex(INT4 , INT4);
extern INT1 nmhTestv2FsLaPortChannelDefaultPortIndex(UINT4 *,INT4, INT4);

extern INT1 nmhTestv2FsLaPortChannelAdminMacAddress (UINT4 *, INT4 , tMacAddr );
extern INT1 nmhSetFsLaPortChannelAdminMacAddress (INT4 , tMacAddr );
extern INT1 nmhGetDot3adAggMACAddress (INT4 , tMacAddress *);
extern INT1 nmhTestv2FsLaPortChannelMacSelection (UINT4 *, INT4 , INT4 );
extern INT1 nmhSetFsLaPortChannelMacSelection(INT4 , INT4 );
extern INT1 nmhGetFsLaPortChannelMacSelection(INT4 , INT4 *);
extern INT1 nmhSetDot1dStpBridgeMaxAge(INT4 );
extern INT1 nmhSetDot1dStpBridgeHelloTime(INT4 );
extern INT1 nmhSetDot1dStpBridgeForwardDelay(INT4 );
extern INT1 nmhGetFsLaPortBundleState (INT4, INT4 *);
extern INT1 nmhTestv2FsLaPortChannelMaxPorts (UINT4 *  ,INT4  ,INT4 );
extern INT1 nmhSetFsLaPortChannelMaxPorts (INT4  ,INT4 );
extern INT1 nmhGetFsLaPortChannelMaxPorts (INT4  ,INT4 *);
extern INT1 nmhGetFsLaPortChannelDLAGMaxKeepAliveCount (INT4 ,INT4 *);
extern INT1 nmhGetFsLaPortChannelDLAGPeriodicSyncPduTxCount (INT4 ,UINT4 *);
extern INT1 nmhGetFsLaPortChannelDLAGPeriodicSyncPduRxCount (INT4 ,UINT4 *);
extern INT1 nmhGetFsLaPortChannelDLAGEventUpdatePduTxCount (INT4 ,UINT4 *);
extern INT1 nmhGetFsLaPortChannelDLAGEventUpdatePduRxCount (INT4 ,UINT4 *);
extern INT1 nmhGetFsLaPortChannelDLAGElectedAsMasterCount (INT4 ,UINT4 *);
extern INT1 nmhGetFsLaPortChannelDLAGElectedAsSlaveCount (INT4 ,UINT4 *);
extern INT1 nmhGetFsLaPortChannelTrapTxCount (INT4 ,UINT4 *);
extern INT1 nmhGetFsLaPortChannelDLAGRolePlayed (INT4 ,INT4 *);
extern INT1 nmhGetFsLaPortChannelDLAGDistributingPortIndex (INT4 ,INT4 *);
extern INT1 nmhTestv2FsLaPortChannelDLAGDistributingPortIndex (UINT4 *  ,INT4  ,INT4 );
extern INT1 nmhSetFsLaPortChannelDLAGDistributingPortIndex (INT4  ,INT4 );
extern INT1 nmhGetFsLaPortChannelDLAGDistributingPortList (INT4 ,tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhSetFsLaPortChannelDLAGDistributingPortList (INT4  ,tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2FsLaPortChannelDLAGDistributingPortList (UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsLaPortChannelDLAGSystemID (INT4 ,tMacAddr * );
extern INT1 nmhSetFsLaPortChannelDLAGSystemID (INT4  ,tMacAddr );
extern INT1 nmhTestv2FsLaPortChannelDLAGSystemID (UINT4 *  ,INT4  ,tMacAddr );
extern INT1 nmhGetFsLaPortChannelDLAGPeriodicSyncTime (INT4 ,UINT4 *);
extern INT1 nmhSetFsLaPortChannelDLAGPeriodicSyncTime (INT4  ,UINT4 );
extern INT1 nmhTestv2FsLaPortChannelDLAGPeriodicSyncTime (UINT4 *  ,INT4  ,UINT4 );
extern INT1 nmhGetFsLaPortChannelDLAGMSSelectionWaitTime (INT4 ,UINT4 *);
extern INT1 nmhTestv2FsLaPortChannelDLAGMSSelectionWaitTime (UINT4 *  ,INT4  ,UINT4 );
extern INT1 nmhSetFsLaPortChannelDLAGMSSelectionWaitTime (INT4  ,UINT4 );
extern INT1 nmhGetFsLaPortChannelDLAGStatus (INT4 ,INT4 *);
extern INT1 nmhSetFsLaPortChannelDLAGStatus (INT4  ,INT4 );
extern INT1 nmhTestv2FsLaPortChannelDLAGStatus (UINT4 *  ,INT4  ,INT4 );
extern INT1 nmhGetFsLaPortChannelDLAGRedundancy (INT4 ,INT4 *);
extern INT1 nmhSetFsLaPortChannelDLAGRedundancy (INT4  ,INT4 );
extern INT1 nmhTestv2FsLaPortChannelDLAGRedundancy (UINT4 *  ,INT4  ,INT4 );
extern INT1 nmhGetFsLaPortChannelDLAGSystemPriority (INT4 ,INT4 *);
extern INT1 nmhTestv2FsLaPortChannelDLAGSystemPriority (UINT4 *  ,INT4  ,INT4 );
extern INT1 nmhSetFsLaPortChannelDLAGSystemPriority  (INT4  ,INT4 );
extern INT1 nmhTestv2FsLaPortChannelSelectionPolicyBitList(UINT4 *,INT4 ,INT4); 
extern INT1 nmhSetFsLaPortChannelSelectionPolicyBitList(INT4  ,INT4);
extern INT4 FsLaPortChannelSelectionPolicyBitListSet(tSnmpIndex *, tRetVal *);
extern INT1 nmhGetFsLaPortChannelSelectionPolicyBitList(INT4 ,INT4 *);
extern INT1 nmhGetNextIndexDot3adAggTable (INT4 , INT4 *);


extern INT4 FsLaPortChannelMaxPortsSet(tSnmpIndex *, tRetVal *);
extern INT4 FsLaPortChannelDLAGPeriodicSyncTimeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsLaPortChannelDLAGMSSelectionWaitTimeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsLaPortChannelDLAGStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsLaPortChannelDLAGRedundancySet(tSnmpIndex *, tRetVal *);
extern INT4 FsLaPortChannelDLAGDistributingPortIndexSet(tSnmpIndex *, tRetVal *);
extern INT4 FsLaPortChannelDLAGDistributingPortListSet(tSnmpIndex *,tRetVal *);
extern INT4 FsLaPortChannelDLAGSystemIDSet(tSnmpIndex *, tRetVal *);
extern INT4 FsLaPortChannelDLAGSystemPrioritySet(tSnmpIndex *, tRetVal *);

extern INT4 Dot3adAggPortActorAdminKeySet(tSnmpIndex *, tRetVal *);
extern INT4 Dot3adAggPortActorAdminStateSet(tSnmpIndex *, tRetVal *);
extern INT4 Dot3adAggPortActorSystemPrioritySet(tSnmpIndex *, tRetVal *);
extern INT4 FsLaPortChannelDefaultPortIndexSet(tSnmpIndex *, tRetVal *);
extern INT4 FsLaPortModeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsLaPortActorResetAdminStateSet(tSnmpIndex *, tRetVal *);
extern INT4 FsLaPortChannelAdminMacAddressSet(tSnmpIndex *, tRetVal *);
extern INT4 FsLaPortChannelMacSelectionSet(tSnmpIndex *, tRetVal *);
extern INT4 FsLaSystemControlSet(tSnmpIndex *, tRetVal *);
extern INT4 FsLaStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsLaActorSystemIDSet(tSnmpIndex *, tRetVal *);
extern INT4 FsLaNoPartnerIndepSet(tSnmpIndex *, tRetVal *);

extern INT1 nmhGetFirstIndexIssIpAuthMgrTable (UINT4 *, UINT4 *);


extern INT1 nmhGetIssIpAuthMgrPortList (UINT4, UINT4,
                                        tSNMP_OCTET_STRING_TYPE *);

extern INT1 nmhGetIssIpAuthMgrVlanList (UINT4, UINT4, 
                                        tSNMP_OCTET_STRING_TYPE *);

extern INT1 nmhGetIssIpAuthMgrOOBPort (UINT4, UINT4, INT4 *);

extern INT1 nmhGetNextIndexIssIpAuthMgrTable (UINT4, UINT4 *, UINT4, 
                                              UINT4 *);
extern INT1 nmhValidateIndexInstanceIssIpAuthMgrTable (UINT4, UINT4);
extern INT1 nmhGetIssIpAuthMgrRowStatus (UINT4, UINT4, INT4 *);
extern INT1 nmhGetIssIpAuthMgrAllowedServices (UINT4, UINT4, INT4 *);
extern INT1 nmhTestv2IssIpAuthMgrPortList (UINT4 *, UINT4, UINT4,
                                           tSNMP_OCTET_STRING_TYPE *);

extern INT1 nmhTestv2IssIpAuthMgrVlanList (UINT4 *, UINT4, UINT4, 
                                           tSNMP_OCTET_STRING_TYPE *);

extern INT1 nmhTestv2IssIpAuthMgrOOBPort (UINT4 *, UINT4, UINT4, INT4);

extern INT1 nmhTestv2IssIpAuthMgrAllowedServices (UINT4 *, UINT4, UINT4, 
                                                  INT4);

extern INT1 nmhTestv2IssIpAuthMgrRowStatus (UINT4 *, UINT4, UINT4, INT4);

/* Webnm related externs */

extern INT4  HttpGetNonZeroValuebyName PROTO ((UINT1 *, UINT1 *, UINT1 *));
extern INT4  HttpReadhtml (tHttp * pHttp);
#ifdef DIFFSRV_WANTED
extern INT1 nmhGetFsDsStatus PROTO ((INT4 *));
extern INT1 nmhGetFirstIndexFsDiffServMultiFieldClfrTable ARG_LIST((INT4 *));
extern INT1 nmhGetFirstIndexFsDiffServClfrTable ARG_LIST((INT4 *));
extern INT1 nmhGetFsDiffServClfrMFClfrId ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsDiffServClfrDataPathId ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetNextIndexFsDiffServClfrTable ARG_LIST((INT4 , INT4 *));
extern INT1 nmhGetFsDiffServDataPathStatus ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsDiffServPorts ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetNextIndexFsDiffServMultiFieldClfrTable ARG_LIST((INT4 , INT4 *));
extern INT1 nmhTestv2FsDiffServMultiFieldClfrStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsDiffServDataPathStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhSetFsDiffServDataPathStatus ARG_LIST((INT4  ,INT4 )); 
extern INT1 nmhSetFsDiffServMultiFieldClfrStatus ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhTestv2FsDiffServMultiFieldClfrSrcMacAddr ARG_LIST((UINT4 *  ,INT4  ,tMacAddr
 ));
extern INT1 nmhTestv2FsDiffServMultiFieldClfrDstMacAddr ARG_LIST((UINT4 *  ,INT4  ,tMacAddr
 ));
extern INT1 nmhTestv2FsDiffServMultiFieldClfrVlanId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsDiffServMultiFieldClfrFilterFormat ARG_LIST((UINT4 *  ,INT4  ,INT4 )
);
extern INT1 nmhTestv2FsDiffServMultiFieldClfrEtherType ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServMultiFieldClfrSrcMacAddr ARG_LIST((INT4  ,tMacAddr ));
extern INT1 nmhSetFsDiffServMultiFieldClfrDstMacAddr ARG_LIST((INT4  ,tMacAddr ));
extern INT1 nmhSetFsDiffServMultiFieldClfrVlanId ARG_LIST((INT4  ,INT4 )); 
extern INT1 nmhSetFsDiffServMultiFieldClfrFilterFormat ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsDiffServMultiFieldClfrEtherType ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhTestv2FsDiffServPorts ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *))
;
extern INT1 nmhTestv2FsDiffServDataPathIfDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhSetFsDiffServPorts ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsDiffServDataPathIfDirection ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhTestv2FsDiffServMultiFieldClfrIpVersion ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhTestv2FsDiffServMultiFieldClfrIpHLen ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhTestv2FsDiffServMultiFieldClfrProtocol ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhTestv2FsDiffServMultiFieldClfrSrcIpAddr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhTestv2FsDiffServMultiFieldClfrSrcIpPreLen ARG_LIST((UINT4 *  ,INT4  ,INT4 ))
;
extern INT1 nmhTestv2FsDiffServMultiFieldClfrDstIpAddr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhTestv2FsDiffServMultiFieldClfrDstIpPreLen ARG_LIST((UINT4 *  ,INT4  ,INT4 ))
;
extern INT1 nmhTestv2FsDiffServMultiFieldClfrSrcPortMin ARG_LIST((UINT4 *  ,INT4  ,UINT4 ))
;
extern INT1 nmhTestv2FsDiffServMultiFieldClfrSrcPortMax ARG_LIST((UINT4 *  ,INT4  ,UINT4 ))
;
extern INT1 nmhTestv2FsDiffServMultiFieldClfrDstPortMin ARG_LIST((UINT4 *  ,INT4  ,UINT4 ))
;
extern INT1 nmhTestv2FsDiffServMultiFieldClfrDstPortMax ARG_LIST((UINT4 *  ,INT4  ,UINT4 ))
;
extern INT1 nmhTestv2FsDiffServMultiFieldClfrSrcPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhTestv2FsDiffServMultiFieldClfrSrcPortMask ARG_LIST((UINT4 *  ,INT4  ,UINT4 )
);
extern INT1 nmhTestv2FsDiffServMultiFieldClfrDstPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhTestv2FsDiffServMultiFieldClfrDstPortMask ARG_LIST((UINT4 *  ,INT4  ,UINT4 )
);
extern INT1 nmhTestv2FsDiffServMultiFieldClfrPrecedence ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhSetFsDiffServMultiFieldClfrIpVersion ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServMultiFieldClfrIpHLen ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServMultiFieldClfrProtocol ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServMultiFieldClfrSrcIpAddr ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServMultiFieldClfrSrcIpPreLen ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsDiffServMultiFieldClfrDstIpAddr ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServMultiFieldClfrDstIpPreLen ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsDiffServMultiFieldClfrSrcPortMin ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServMultiFieldClfrSrcPortMax ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServMultiFieldClfrDstPortMin ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServMultiFieldClfrDstPortMax ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServMultiFieldClfrSrcPort ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServMultiFieldClfrSrcPortMask ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServMultiFieldClfrDstPort ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServMultiFieldClfrDstPortMask ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServMultiFieldClfrPrecedence ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhTestv2FsDiffServMultiFieldClfrDscp ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServMultiFieldClfrDscp ARG_LIST((INT4  ,UINT4 ));  
extern INT1 nmhGetFsDiffServClfrOutProActionId ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsDiffServOutProfileActionMID ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsDiffServMeterRefreshCount ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsDiffServClfrInProActionId ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsDiffServInProfileActionFlag ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsDiffServInProfileActionIpTOS ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsDiffServInProfileActionDscp ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsDiffServOutProfileActionFlag ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsDiffServOutProfileActionDscp ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsDiffServClfrNoMatchActionId ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsDiffServNoMatchActionFlag ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsDiffServNoMatchActionNewPrio ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsDiffServNoMatchActionIpTOS ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsDiffServNoMatchActionDscp ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsDiffServDataPathIfDirection ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsDiffServInProfileActionNewPrio ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsDiffServClfrStatus ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsDiffServInProfileActionStatus ARG_LIST ((INT4, INT4 *));
extern INT1 nmhGetFsDiffServOutProfileActionStatus ARG_LIST ((INT4, INT4 *));
extern INT1 nmhGetFsDiffServMeterStatus ARG_LIST ((INT4, INT4 *));
extern INT1 nmhGetFsDiffServNoMatchActionStatus ARG_LIST ((INT4, INT4 *));

extern INT1 nmhSetFsDiffServClfrStatus ARG_LIST((INT4  ,INT4 )); 
extern INT1 nmhSetFsDiffServInProfileActionStatus ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsDiffServOutProfileActionStatus ARG_LIST((INT4  ,INT4 )); 
extern INT1 nmhSetFsDiffServMeterStatus ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsDiffServNoMatchActionStatus ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhTestv2FsDiffServClfrStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhGetFsDiffServMultiFieldClfrStatus ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhTestv2FsDiffServClfrMFClfrId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsDiffServClfrDataPathId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsDiffServClfrInProActionId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsDiffServClfrOutProActionId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsDiffServClfrNoMatchActionId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsDiffServInProfileActionDscp ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServInProfileActionDscp ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhTestv2FsDiffServInProfileActionIpTOS ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServInProfileActionIpTOS ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhTestv2FsDiffServInProfileActionNewPrio ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServInProfileActionNewPrio ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServOutProfileActionFlag ARG_LIST((INT4  ,UINT4 ));  
extern INT1 nmhTestv2FsDiffServOutProfileActionDscp ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServOutProfileActionDscp ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhTestv2FsDiffServMeterStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsDiffServMetertokenSize ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhTestv2FsDiffServMeterRefreshCount ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServMetertokenSize ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServMeterRefreshCount ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServOutProfileActionMID ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsDiffServNoMatchActionFlag ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhTestv2FsDiffServNoMatchActionDscp ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServNoMatchActionDscp ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhTestv2FsDiffServNoMatchActionIpTOS ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhSetFsDiffServNoMatchActionIpTOS ARG_LIST((INT4  ,UINT4 ));  
extern INT1 nmhSetFsDiffServClfrMFClfrId ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsDiffServClfrInProActionId ARG_LIST((INT4  ,INT4 )); 
extern INT1 nmhSetFsDiffServClfrOutProActionId ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsDiffServClfrNoMatchActionId ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsDiffServClfrDataPathId ARG_LIST((INT4  ,INT4 )); 

#endif

extern INT1 nmhGetFirstIndexIpCidrRouteTable (UINT4 *, UINT4 *, INT4*, UINT4 *);                                                                                
extern INT1 nmhGetIpCidrRouteIfIndex (UINT4, UINT4, INT4, UINT4, INT4 *);
extern INT1 nmhGetIpCidrRouteMetric1 (UINT4, UINT4, INT4, UINT4, INT4 *); 
extern INT4 CfaGetIfAlias PROTO((UINT4 u4IfIndex, UINT1 *au1IfAlias));
extern INT1 nmhGetIpCidrRouteStatus (UINT4, UINT4, INT4, UINT4, INT4 *);
extern INT1 nmhSetIpCidrRouteStatus (UINT4, UINT4, INT4, UINT4, INT4); 
extern INT1 nmhTestv2IpCidrRouteStatus (UINT4 *, UINT4, UINT4, INT4, UINT4, INT4);
extern INT1 nmhTestv2IpCidrRouteMetric1 (UINT4 *, UINT4, UINT4, INT4, 
                                         UINT4, INT4);
extern INT1 nmhSetIpCidrRouteMetric1 (UINT4, UINT4, INT4, UINT4, INT4); 

extern INT1 nmhTestv2IpCidrRouteIfIndex (UINT4 *, UINT4, UINT4, INT4, UINT4, INT4);
extern INT1 nmhSetIpCidrRouteIfIndex (UINT4, UINT4, INT4, UINT4, INT4);
extern INT1 nmhSetIpCidrRouteType (UINT4, UINT4, INT4, UINT4, INT4);
extern UINT4 IssGetIgsFwdModeFromNvRam (VOID);
extern INT1 nmhTestv2IssConfigRestoreOption (UINT4 *, INT4);
extern INT1 nmhSetIssConfigRestoreOption (INT4);
extern INT1 nmhTestv2IssDefaultIpAddrCfgMode (UINT4 *, INT4);
extern INT1 nmhTestv2IssDefaultIpAddrAllocProtocol PROTO ((UINT4 *, INT4));
extern INT1 nmhSetIssDefaultIpAddrCfgMode (INT4);
extern INT1 nmhSetIssDefaultIpAddrAllocProtocol PROTO ((INT4));
extern INT1 nmhTestv2IssDefaultIpAddr (UINT4 *, UINT4);
extern INT1 nmhSetIssDefaultIpAddr (UINT4);
extern INT1 nmhTestv2IssDefaultIpSubnetMask (UINT4 *, UINT4);
extern INT1 nmhSetIssDefaultIpSubnetMask (UINT4);
extern INT1 nmhTestv2IssSwitchBaseMacAddress (UINT4 *, tMacAddr);
extern INT1 nmhSetIssSwitchBaseMacAddress (tMacAddr);
extern INT1 nmhTestv2IssDefaultInterface (UINT4 *, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetIssDefaultInterface (tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2IssConfigAutoSaveTrigger (UINT4 *, INT4);
extern INT1 nmhSetIssConfigAutoSaveTrigger (INT4);
extern INT1 nmhTestv2IssConfigIncrSaveFlag (UINT4 *, INT4);
extern INT1 nmhSetIssConfigIncrSaveFlag (INT4);
extern INT1 nmhTestv2IssConfigRollbackFlag (UINT4 *, INT4);
extern INT1 nmhSetIssConfigRollbackFlag (INT4);
extern INT1 nmhTestv2Dot1qFutureVlanLearningMode (UINT4 *, INT4);
extern INT1 nmhSetDot1qFutureVlanLearningMode (INT4);
extern INT1 nmhTestv2FsIgsNextMcastFwdMode (UINT4 *, INT4);
extern INT1 nmhSetFsIgsNextMcastFwdMode (INT4);
extern INT1 nmhGetDot1dStpPortOperPointToPoint (INT4, INT4 *);
extern INT1 nmhGetDot1dStpPortOperEdgePort (INT4, INT4 *);
extern INT1 nmhGetIssDefaultIpAddrCfgMode (INT4 *);
extern INT1 nmhGetIssDefaultIpAddrAllocProtocol (INT4 *);
extern INT1 nmhGetIssDefaultIpAddr (UINT4 *);
extern INT1 nmhGetIssDefaultIpSubnetMask (UINT4 *);
extern INT1 nmhGetIssSwitchBaseMacAddress (tMacAddr *);
extern INT1 nmhGetIssOOBInterface (INT4 *);
extern INT1 nmhGetIssDefaultInterface (tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetIssConfigAutoSaveTrigger (INT4 *);
extern INT1 nmhGetIssConfigIncrSaveFlag (INT4 *);
extern INT1 nmhGetIssConfigRollbackFlag (INT4 *);
extern INT1 nmhGetIssRemoteSaveStatus (INT4 *);
extern INT1 nmhGetIssConfigRestoreStatus (INT4 *);
extern INT1 nmhGetIssHttpStatus (INT4 *);
extern INT1 nmhGetIssHttpPort (INT4 *); 
extern INT1 nmhGetIssTelnetStatus (INT4 *);
extern INT1 nmhGetIssMgmtInterfaceRouting (INT4 *); 
extern INT1 nmhGetIssLoggingOption (INT4 *);
extern INT1 nmhGetFsMstCistPortStateTransitionSemState (INT4, INT4 *);
extern INT1 nmhTestv2FsPimCmnCandidateRPRowStatus (UINT4 *, INT4, INT4, 
              tSNMP_OCTET_STRING_TYPE *, INT4, tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhSetFsPimCmnCandidateRPRowStatus (INT4, INT4, 
              tSNMP_OCTET_STRING_TYPE *, INT4, tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhGetFirstIndexFsPimCmnCandidateRPTable (INT4 *, INT4 *, 
             tSNMP_OCTET_STRING_TYPE *, INT4 *, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsRstPortRole (INT4, INT4 *);
extern INT1 nmhGetFsLaSystemControl (INT4 *);

extern INT1 nmhGetFsLaNoPartnerIndep (INT4 *);
extern INT1 nmhTestv2FsLaSystemControl (UINT4 *, INT4 );
extern INT1 nmhTestv2FsLaNoPartnerIndep (UINT4 *, INT4 );
extern INT1 nmhSetFsLaSystemControl (INT4 );
extern INT1 nmhSetFsLaNoPartnerIndep (INT4 );
extern INT1 nmhGetFsPimCmnCandidateRPRowStatus (INT4, INT4, 
            tSNMP_OCTET_STRING_TYPE *, INT4, tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetNextIndexFsPimCmnCandidateRPTable (INT4, INT4 *, INT4, INT4 *,
            tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *, INT4, INT4 *,
            tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *);

extern INT1 nmhGetFirstIndexFsipv6AddrTable (INT4 *,
                                 tSNMP_OCTET_STRING_TYPE *,
                                 INT4 *);
extern INT1 nmhGetFsipv6AddrType (INT4 ,
                      tSNMP_OCTET_STRING_TYPE *,
                      INT4 , INT4 *);

extern INT1 nmhGetFsipv6AddrProfIndex (INT4 ,
                           tSNMP_OCTET_STRING_TYPE * ,
                           INT4 ,
                           INT4 *);
extern INT1 nmhGetNextIndexFsipv6AddrTable (INT4 ,
                                INT4 *,
                                tSNMP_OCTET_STRING_TYPE * ,
                                tSNMP_OCTET_STRING_TYPE * ,
                                INT4 ,
                                INT4 *);
extern INT1 nmhGetFirstIndexFsTunlIfTable (INT4 *,
                               tSNMP_OCTET_STRING_TYPE * ,
                               tSNMP_OCTET_STRING_TYPE * ,
                               INT4 *,
                               INT4 *);

extern UINT4 CfaGetIfIndexFromIpAddr (UINT4 , UINT4 *);

extern INT1 nmhGetFsTunlIfAlias (INT4,
                     tSNMP_OCTET_STRING_TYPE *,
                     tSNMP_OCTET_STRING_TYPE *,
                     INT4, INT4,
                     tSNMP_OCTET_STRING_TYPE *);

extern INT1 nmhGetFsTunlIfSecurity (INT4 ,
                     tSNMP_OCTET_STRING_TYPE * ,
                     tSNMP_OCTET_STRING_TYPE * ,
                     INT4 , INT4 ,
                     INT4 *);
extern INT1  nmhGetFsTunlIfCksumFlag(INT4 ,
                     tSNMP_OCTET_STRING_TYPE * ,
                     tSNMP_OCTET_STRING_TYPE * ,
                     INT4 , INT4 ,
                     INT4 *);
extern INT1  nmhGetFsTunlIfPmtuFlag(INT4 ,
                     tSNMP_OCTET_STRING_TYPE * ,
                     tSNMP_OCTET_STRING_TYPE * ,
                     INT4 , INT4 ,
                     INT4 *);
extern INT1 nmhGetFsTunlIfDirFlag(INT4 ,
                     tSNMP_OCTET_STRING_TYPE * ,
                     tSNMP_OCTET_STRING_TYPE * ,
                     INT4 , INT4 ,
                     INT4 *);
extern INT1 nmhGetFsTunlIfDirection(INT4 ,
                     tSNMP_OCTET_STRING_TYPE * ,
                     tSNMP_OCTET_STRING_TYPE * ,
                     INT4 , INT4 ,
                     INT4 *);
extern INT1 nmhGetFsTunlIfHopLimit(INT4 ,
                     tSNMP_OCTET_STRING_TYPE * ,
                     tSNMP_OCTET_STRING_TYPE * ,
                     INT4 , INT4 ,
                     INT4 *);
extern INT1 nmhGetNextIndexFsTunlIfTable (INT4 ,
                              INT4 *,
                              tSNMP_OCTET_STRING_TYPE * ,
                              tSNMP_OCTET_STRING_TYPE * ,
                              tSNMP_OCTET_STRING_TYPE * ,
                              tSNMP_OCTET_STRING_TYPE * ,
                              INT4 ,
                              INT4 *,
                              INT4 ,
                              INT4 *);


extern INT4 WebnmConvertColonStringToOctet (tSNMP_OCTET_STRING_TYPE * , UINT1 *);
extern INT1 nmhTestv2Fsipv6AddrAdminStatus (UINT4 *, INT4 ,
                                tSNMP_OCTET_STRING_TYPE * ,
                                INT4,
                                INT4);

extern INT1 nmhSetFsipv6AddrAdminStatus (INT4,
                             tSNMP_OCTET_STRING_TYPE *,
                             INT4,
                             INT4);

extern INT1 nmhTestv2Fsipv6AddrType (UINT4 *, INT4 ,
                         tSNMP_OCTET_STRING_TYPE * ,
                         INT4 ,
                         INT4);
extern INT1 nmhSetFsipv6AddrType (INT4 ,
                      tSNMP_OCTET_STRING_TYPE * ,
                      INT4 , INT4);

extern INT1 nmhTestv2Fsipv6AddrProfIndex (UINT4 *, INT4 ,
   tSNMP_OCTET_STRING_TYPE * ,
   INT4 ,
   INT4);
extern INT1 nmhSetFsipv6AddrProfIndex (INT4 ,
   tSNMP_OCTET_STRING_TYPE * ,
   INT4 , INT4 );

extern INT1 nmhTestv2FsTunlIfStatus (UINT4 *, INT4 ,
                         tSNMP_OCTET_STRING_TYPE * ,
                         tSNMP_OCTET_STRING_TYPE * ,
                         INT4 , INT4 ,
                         INT4 );
extern INT1 nmhSetFsTunlIfStatus (INT4 ,
                      tSNMP_OCTET_STRING_TYPE * ,
                      tSNMP_OCTET_STRING_TYPE * ,
                      INT4 , INT4 ,
                      INT4 );

extern INT1 nmhTestv2FsTunlIfCksumFlag (UINT4 *, INT4 , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 , INT4 , INT4 );
extern INT1 nmhSetFsTunlIfCksumFlag (INT4 , tSNMP_OCTET_STRING_TYPE * ,
                         tSNMP_OCTET_STRING_TYPE * , INT4 , INT4 , INT4 );

extern INT1 nmhTestv2FsTunlIfPmtuFlag (UINT4 *, INT4 , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 , INT4 , INT4 );
extern INT1 nmhSetFsTunlIfPmtuFlag (INT4 , tSNMP_OCTET_STRING_TYPE * ,
                         tSNMP_OCTET_STRING_TYPE * , INT4 , INT4 , INT4 );
extern INT1 nmhTestv2FsTunlIfDirFlag (UINT4 *, INT4 , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 , INT4 , INT4 );
extern INT1 nmhSetFsTunlIfDirFlag (INT4 , tSNMP_OCTET_STRING_TYPE * ,
                         tSNMP_OCTET_STRING_TYPE * , INT4 , INT4 , INT4 );

extern INT1 nmhTestv2FsTunlIfDirection (UINT4 *, INT4 , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 , INT4 , INT4 );
extern INT1 nmhSetFsTunlIfDirection (INT4 , tSNMP_OCTET_STRING_TYPE * ,
                         tSNMP_OCTET_STRING_TYPE * , INT4 , INT4 , INT4 );
extern INT1 nmhTestv2FsTunlIfHopLimit (UINT4 *, INT4 , 
                        tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,
                        INT4 , INT4 , INT4 );
extern INT1 nmhSetFsTunlIfHopLimit (INT4 , tSNMP_OCTET_STRING_TYPE * ,
                         tSNMP_OCTET_STRING_TYPE * , INT4 , INT4 , INT4 );
extern INT1 nmhTestv2FsTunlIfAlias (UINT4 *, INT4 ,
                        tSNMP_OCTET_STRING_TYPE * ,
                        tSNMP_OCTET_STRING_TYPE * ,
                        INT4 , INT4 ,
                        tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhSetFsTunlIfAlias (INT4 ,
                     tSNMP_OCTET_STRING_TYPE * ,
                     tSNMP_OCTET_STRING_TYPE * ,
                     INT4 , INT4 ,
                     tSNMP_OCTET_STRING_TYPE * );

extern INT4 FsTunlIfStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsTunlIfAliasSet(tSnmpIndex *, tRetVal *);
extern INT4 FsTunlIfCksumFlagSet(tSnmpIndex *, tRetVal *);
extern INT4 FsTunlIfPmtuFlagSet(tSnmpIndex *, tRetVal *);
extern INT4 FsTunlIfHopLimitSet(tSnmpIndex *, tRetVal *);
extern INT4 FsTunlIfDirFlagSet(tSnmpIndex *, tRetVal *);
extern INT4 FsTunlIfDirectionSet(tSnmpIndex *, tRetVal *);


#ifdef IP6_WANTED
VOID IssPrintAvailableIPv6Vlans (tHttp * pHttp);
VOID IssPrintAvailableIPsecv6Vlans (tHttp * pHttp);
VOID IssPrintAvailableIPv6Interfaces (tHttp * pHttp);
extern INT4 Fsipv6AddrAdminStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6AddrTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6AddrProfIndexSet(tSnmpIndex *, tRetVal *);
VOID IssProcessRRD6FilterConfPage (tHttp * pHttp);
VOID IssProcessRRD6FilterConfPageGet (tHttp * pHttp);
VOID IssProcessRRD6FilterConfPageSet (tHttp * pHttp);
extern INT1 nmhGetFirstIndexFsRrd6ControlTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 *));
extern INT1 nmhGetNextIndexFsRrd6ControlTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));
extern INT1 nmhGetFsRrd6ControlSourceProto ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));
extern INT1 nmhGetFsRrd6ControlDestProto ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));
extern INT1 nmhGetFsRrd6ControlRouteExportFlag ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));
extern INT1 nmhTestv2FsRrd6ControlSourceProto ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));
extern INT1 nmhTestv2FsRrd6ControlDestProto ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));
extern INT1 nmhTestv2FsRrd6ControlRouteExportFlag ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));
extern INT1 nmhTestv2FsRrd6ControlRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));
extern INT1 nmhSetFsRrd6ControlSourceProto ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));
extern INT1 nmhSetFsRrd6ControlDestProto ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));
extern INT1 nmhSetFsRrd6ControlRouteExportFlag ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));
extern INT1 nmhSetFsRrd6ControlRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));
extern INT4 FsRrd6ControlSourceProtoSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRrd6ControlDestProtoSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRrd6ControlRouteExportFlagSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRrd6ControlRowStatusSet(tSnmpIndex *, tRetVal *);
#endif

#ifdef RIP6_WANTED
VOID IssProcessRip6StatsPage(tHttp * pHttp);
VOID IssProcessRip6RouteInfoPage(tHttp * pHttp);
VOID IssProcessRip6IfConfPage (tHttp * pHttp);
VOID IssProcessRip6IfConfInfoGet (tHttp * pHttp);
VOID IssProcessRip6IfConfSet (tHttp * pHttp);
VOID IssRip6ConfChange (tHttp * pHttp, UINT4 u4IfIndex);
VOID IssProcessRip6FilterPage (tHttp * pHttp);
VOID IssProcessRip6FilterGet (tHttp *pHttp);
VOID IssProcessRip6FilterSet (tHttp * pHttp);
VOID IssRip6FilterConf (tHttp * pHttp);
VOID IssRip6DelConf (tHttp * pHttp);
        

/* RIP6 INTERFACE TABLE*/
extern INT1 nmhGetFirstIndexFsrip6RipIfTable (INT4 *);
extern INT1 nmhGetNextIndexFsrip6RipIfTable (INT4 , INT4 *);
extern INT1 nmhGetFsrip6RipIfInMessages (INT4 ,UINT4 *);
extern INT1 nmhGetFsrip6RipIfInRequests (INT4 ,UINT4 *);
extern INT1 nmhGetFsrip6RipIfInResponses (INT4 ,UINT4 *);
extern INT1 nmhGetFsrip6RipIfUnknownCmds (INT4 ,UINT4 *);
extern INT1 nmhGetFsrip6RipIfInOtherVer (INT4 ,UINT4 *);
extern INT1 nmhGetFsrip6RipIfInDiscards (INT4 ,UINT4 *);
extern INT1 nmhGetFsrip6RipIfOutMessages (INT4 ,UINT4 *);
extern INT1 nmhGetFsrip6RipIfOutRequests (INT4 ,UINT4 *);
extern INT1 nmhGetFsrip6RipIfOutResponses (INT4 ,UINT4 *);
extern INT1 nmhGetFsrip6RipIfOutTrigUpdates (INT4 ,UINT4 *);

/*RIP6 ROUTE TABLE */
extern INT1 nmhGetFirstIndexFsrip6RipRouteTable ARG_LIST((
                        tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , INT4 *));
extern INT1 nmhGetNextIndexFsrip6RipRouteTable ARG_LIST((
                        tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  ,                         INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));
extern INT1 nmhGetFsrip6RipRouteNextHop ARG_LIST((
                        tSNMP_OCTET_STRING_TYPE * ,INT4  , INT4 , INT4 ,
                        tSNMP_OCTET_STRING_TYPE * ));

extern INT1 nmhGetFsrip6RipRouteMetric ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,
                        INT4  , INT4  , INT4 ,INT4 *));

extern INT1 nmhGetFsrip6RipRouteTag ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,
                        INT4  , INT4  , INT4 ,INT4 *));

extern INT1 nmhGetFsrip6RipRouteAge ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,
                        INT4  , INT4  , INT4 ,INT4 *));


extern INT1 nmhTestv2Fsrip6RipIfProtocolEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhSetFsrip6RipIfProtocolEnable ARG_LIST((INT4  ,INT4 ));


extern INT1 nmhGetFsrip6RipIfProtocolEnable ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsrip6RipProfileHorizon ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsrip6RipIfCost ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsrip6RipIfDefRouteAdvt ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsrip6RipProfilePeriodicUpdTime ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsrip6RipProfileTrigDelayTime ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsrip6RipProfileRouteAge ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsrip6RipProfileGarbageCollectTime ARG_LIST((INT4 ,INT4 *));


extern INT1 nmhSetFsrip6RipProfileHorizon ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsrip6RipIfCost ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsrip6RipIfDefRouteAdvt ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsrip6RipProfilePeriodicUpdTime ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsrip6RipProfileTrigDelayTime ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsrip6RipProfileRouteAge ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsrip6RipProfileGarbageCollectTime ARG_LIST((INT4  ,INT4 ));



extern INT1 nmhTestv2Fsrip6RipProfileHorizon ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Fsrip6RipIfCost ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Fsrip6RipIfDefRouteAdvt ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Fsrip6RipProfilePeriodicUpdTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Fsrip6RipProfileTrigDelayTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Fsrip6RipProfileRouteAge ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Fsrip6RipProfileGarbageCollectTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


extern INT1 nmhGetFsrip6RipIfProfileIndex ARG_LIST((INT4 ,INT4 *));

/*Filter Tables*/

extern INT1 nmhGetFirstIndexFsrip6RipPeerTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

extern INT1 nmhGetNextIndexFsrip6RipPeerTable ARG_LIST(( tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *));

extern INT1 nmhGetFsrip6RipPeerEntryStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *, INT4 *));

extern INT1 nmhGetFirstIndexFsrip6RipAdvFilterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

extern INT1 nmhGetNextIndexFsrip6RipAdvFilterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *));

extern INT1 nmhGetFsrip6RipAdvFilterStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *, INT4 *));

extern INT1 nmhTestv2Fsrip6RipPeerEntryStatus ARG_LIST((UINT4 *, tSNMP_OCTET_STRING_TYPE *,INT4));

extern INT1 nmhTestv2Fsrip6RipAdvFilterStatus ARG_LIST((UINT4 *, tSNMP_OCTET_STRING_TYPE * , INT4));

extern INT1 nmhSetFsrip6RipPeerEntryStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *, INT4));

extern INT1 nmhSetFsrip6RipAdvFilterStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *, INT4));

extern INT1 nmhValidateIndexInstanceFsrip6RipProfileTable ARG_LIST((INT4 ));

extern INT1 nmhTestv2Fsrip6RipIfProfileIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhSetFsrip6RipIfProfileIndex ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhTestv2Fsrip6RipProfileStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhSetFsrip6RipProfileStatus ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhGetFirstIndexFsrip6RipProfileTable ARG_LIST((INT4 *));

extern INT1 nmhGetNextIndexFsrip6RipProfileTable ARG_LIST((INT4 , INT4 *));
extern INT1 nmhValidateIndexInstanceFsMIrip6RipPeerTable  ARG_LIST((UINT4 , tSNMP_OCTET_STRING_TYPE *));

extern INT1 nmhValidateIndexInstanceFsMIrip6RipAdvFilterTable  ARG_LIST((UINT4 , tSNMP_OCTET_STRING_TYPE *));

extern INT4 Fsrip6RipIfProtocolEnableSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsrip6RipProfileStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsrip6RipIfProfileIndexSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsrip6RipProfileHorizonSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsrip6RipProfilePeriodicUpdTimeSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsrip6RipProfileTrigDelayTimeSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsrip6RipProfileRouteAgeSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsrip6RipProfileGarbageCollectTimeSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsrip6RipIfCostSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsrip6RipIfDefRouteAdvtSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsrip6RipPeerEntryStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsrip6RipAdvFilterStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRip6DistInOutRouteMapValueSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRip6DistInOutRouteMapRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRip6PreferenceValueSet(tSnmpIndex *, tRetVal *);
#endif /*RIP6_WANTED*/

#ifdef OSPF_WANTED
VOID IssPrintAvailableOspfContextId (tHttp * pHttp);
VOID IssProcessOspfContextCreationPage  (tHttp * pHttp);
VOID IssProcessOspfContextCreationPageGet  (tHttp * pHttp);
VOID IssProcessOspfContextCreationPageSet  (tHttp * pHttp);
#endif




#ifdef OSPF3_WANTED

#define  IF_DEMAND_CKT_ENABLED                     1

VOID IssProcessOspf3IfConfPage (tHttp * pHttp);

VOID IssProcessOspf3IfConfInfoGet (tHttp * pHttp);
VOID IssProcessOspf3IfConfSet (tHttp * pHttp);
INT4  IssOspf3IfParamConfig (tHttp * pHttp, INT4 i4IfIndex);
VOID IssProcessOspf3BasicConfPage (tHttp * pHttp);
VOID IssProcessOspf3BasicConfGet (tHttp * pHttp);
VOID IssProcessOspf3BasicConfSet (tHttp * pHttp);
VOID IssProcessOspf3ContextCreation (tHttp * pHttp);
VOID IssProcessOspf3ContextCreationGet (tHttp * pHttp);
VOID IssProcessOspf3ContextCreationSet (tHttp * pHttp);
VOID IssProcessOspf3AreaConfPage (tHttp * pHttp);
VOID IssProcessOspf3AreaConfPageGet (tHttp * pHttp);
VOID IssPrintAvailableOspfv3ContextId (tHttp * pHttp);


VOID IssProcessOspf3GRConfPage (tHttp * pHttp);
VOID IssProcessOspf3GRPageGet (tHttp *pHttp);
VOID IssProcessOspf3GRPageSet (tHttp *pHttp);
VOID IssProcessOspf3RouteStatsPage (tHttp * pHttp);
VOID IssProcessOspf3RouteStatsPageGet (tHttp * pHttp);

extern INT4 V3UtilSetContext ARG_LIST ((UINT4 u4ContextId));
extern VOID V3UtilGetCxtName ARG_LIST ((UINT4 u4ContextId, UINT1 * pu1Alias));
extern INT4 V3OspfGetSystemMode ARG_LIST ((UINT2 u2ProtocolId));
extern INT1 nmhGetFirstIndexOspfv3IfTable ARG_LIST((INT4 *));
extern INT1 nmhGetNextIndexOspfv3IfTable ARG_LIST((INT4 , INT4 *));
extern INT1 nmhGetOspfv3IfAreaId ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetOspfv3IfType ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetOspfv3IfStatus ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetOspfv3IfRtrPriority ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetOspfv3IfTransitDelay ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetOspfv3IfRetransInterval ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetOspfv3IfHelloInterval ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetOspfv3IfRtrDeadInterval ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetOspfv3IfPollInterval ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetOspfv3IfDemand ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetOspfv3IfMetricValue ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetOspfv3IfDemandNbrProbe ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFutOspfv3IfLinkLSASuppression ARG_LIST ((INT4 ,INT4 *));
extern INT1 nmhGetOspfv3IfDemandNbrProbeRetxLimit ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetOspfv3IfDemandNbrProbeInterval ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetOspfv3AreaStatus ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetFsMIOspfv3RestartStrictLsaChecking ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIOspfv3HelperSupport ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsMIOspfv3HelperGraceTimeLimit ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIOspfv3RestartAckState ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIOspfv3GraceLsaRetransmitCount ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIOspfv3RestartReason ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIStdOspfv3RestartSupport ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIStdOspfv3RestartInterval ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIOspfv3ExtTraceLevel ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhSetOspfv3IfAreaId ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetOspfv3IfType ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetOspfv3IfRtrPriority ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetOspfv3IfTransitDelay ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetOspfv3IfRetransInterval ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetOspfv3IfHelloInterval ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetOspfv3IfRtrDeadInterval ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetOspfv3IfPollInterval ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetOspfv3IfStatus ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetOspfv3IfDemand ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetOspfv3IfMetricValue ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFutOspfv3IfLinkLSASuppression ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetOspfv3IfDemandNbrProbe ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetOspfv3IfDemandNbrProbeRetxLimit ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetOspfv3IfDemandNbrProbeInterval ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetOspfv3AreaStatus ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhSetFsMIStdOspfv3RestartSupport ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsMIStdOspfv3RestartInterval ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsMIOspfv3RestartStrictLsaChecking ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsMIOspfv3HelperSupport ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsMIOspfv3HelperGraceTimeLimit ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsMIOspfv3RestartAckState ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsMIOspfv3GraceLsaRetransmitCount ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsMIOspfv3RestartReason ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsMIOspfv3ExtTraceLevel ARG_LIST((INT4  ,INT4 ));


extern INT1 nmhTestv2Ospfv3IfAreaId ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhTestv2Ospfv3IfType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2Ospfv3IfRtrPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2Ospfv3IfTransitDelay ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2Ospfv3IfRetransInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2Ospfv3IfHelloInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2Ospfv3IfRtrDeadInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2Ospfv3IfPollInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhTestv2Ospfv3IfStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2Ospfv3IfDemand ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2Ospfv3IfMetricValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FutOspfv3IfLinkLSASuppression ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2Ospfv3IfDemandNbrProbe ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2Ospfv3IfDemandNbrProbeRetxLimit ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhTestv2Ospfv3IfDemandNbrProbeInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhTestv2Ospfv3AreaStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2FsMIOspfv3RestartStrictLsaChecking ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIOspfv3HelperSupport ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsMIOspfv3HelperGraceTimeLimit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIOspfv3RestartAckState ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIOspfv3GraceLsaRetransmitCount ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIOspfv3RestartReason ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIStdOspfv3RestartSupport ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIStdOspfv3RestartInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIOspfv3ExtTraceLevel ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


extern INT1 nmhGetFirstIndexFsMIStdOspfv3Table ARG_LIST((INT4 *));
extern INT1 nmhGetNextIndexFsMIStdOspfv3Table ARG_LIST((INT4 , INT4 *));
extern INT1 nmhGetFsMIStdOspfv3RouterId ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsMIStdOspfv3AdminStat ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIOspfv3ABRType ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIStdOspfv3ExtAreaLsdbLimit ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIStdOspfv3ExitOverflowInterval ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsMIStdOspfv3DemandExtensions ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIStdOspfv3ReferenceBandwidth ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsMIOspfv3SpfDelay ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIOspfv3SpfHoldTime ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIOspfv3DefaultPassiveInterface ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIOspfv3TraceLevel ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIOspfv3RTStaggeringInterval ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIStdOspfv3ASBdrRtrStatus ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIStdOspfv3AreaSpfRuns ARG_LIST((INT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsMIStdOspfv3Status ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhSetFsMIStdOspfv3RouterId ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetFsMIStdOspfv3AdminStat ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsMIStdOspfv3ASBdrRtrStatus ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsMIStdOspfv3ExtAreaLsdbLimit ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsMIStdOspfv3ExitOverflowInterval ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetFsMIStdOspfv3DemandExtensions ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsMIStdOspfv3ReferenceBandwidth ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetFsMIOspfv3TraceLevel ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsMIOspfv3ABRType ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsMIStdOspfv3Status ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsMIOspfv3SpfDelay ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsMIOspfv3SpfHoldTime ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsMIOspfv3RTStaggeringInterval ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsMIOspfv3DefaultPassiveInterface ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhValidateIndexInstanceFsMIStdOspfv3Table ARG_LIST((INT4 ));
extern INT1 nmhTestv2FsMIStdOspfv3RouterId ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhTestv2FsMIStdOspfv3AdminStat ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIStdOspfv3ASBdrRtrStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIStdOspfv3ExtAreaLsdbLimit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIStdOspfv3ExitOverflowInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhTestv2FsMIStdOspfv3DemandExtensions ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIStdOspfv3ReferenceBandwidth ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhTestv2FsMIStdOspfv3Status ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIOspfv3ABRType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIOspfv3TraceLevel ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIOspfv3SpfDelay ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIOspfv3SpfHoldTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIOspfv3RTStaggeringInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIOspfv3DefaultPassiveInterface ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhGetFirstIndexFutOspfv3RoutingTable ARG_LIST((INT4 *, tSNMP_OCTET_STRING_TYPE *, UINT4 *, INT4 *, tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetNextIndexFutOspfv3RoutingTable ARG_LIST((INT4, INT4 *, tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *, UINT4, UINT4 *, INT4, INT4 *, tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhGetFutOspfv3RouteType ARG_LIST((INT4, tSNMP_OCTET_STRING_TYPE *, UINT4, INT4, tSNMP_OCTET_STRING_TYPE *, INT4 *));
extern INT1 nmhGetFutOspfv3RouteAreaId ARG_LIST((INT4, tSNMP_OCTET_STRING_TYPE *, UINT4, INT4, tSNMP_OCTET_STRING_TYPE *, UINT4 *));
extern INT1 nmhGetFutOspfv3RouteCost ARG_LIST((INT4, tSNMP_OCTET_STRING_TYPE *, UINT4, INT4, tSNMP_OCTET_STRING_TYPE *, INT4 *));
extern INT1 nmhGetFutOspfv3RouteType2Cost ARG_LIST((INT4, tSNMP_OCTET_STRING_TYPE *, UINT4, INT4, tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1 nmhGetFutOspfv3RouteInterfaceIndex ARG_LIST((INT4, tSNMP_OCTET_STRING_TYPE *, UINT4, INT4, tSNMP_OCTET_STRING_TYPE *, INT4 *));
#endif /* OSPF3_WANTED */
extern INT4 SnmpTargetAddrTAddressSet(tSnmpIndex *, tRetVal *);
extern INT4 SnmpTargetAddrTagListSet(tSnmpIndex *, tRetVal *);
extern INT4 SnmpTargetAddrParamsSet(tSnmpIndex *, tRetVal *);
extern INT4 SnmpTargetAddrStorageTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 SnmpTargetAddrRowStatusSet(tSnmpIndex *, tRetVal *);

extern INT1
nmhGetFirstIndexSnmpTargetParamsTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetNextIndexSnmpTargetParamsTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));


extern INT1
nmhSetSnmpTargetAddrTAddress ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhSetSnmpTargetAddrTagList ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhSetSnmpTargetAddrParams ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhSetSnmpTargetAddrStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1
nmhSetSnmpTargetAddrRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1
nmhTestv2SnmpTargetAddrTAddress ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2SnmpTargetAddrTagList ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2SnmpTargetAddrParams ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2SnmpTargetAddrStorageType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1
nmhTestv2SnmpTargetAddrRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1
nmhGetFirstIndexSnmpTargetAddrTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetNextIndexSnmpTargetAddrTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetSnmpTargetAddrTAddress ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetSnmpTargetAddrTagList ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetSnmpTargetAddrParams ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetSnmpTargetAddrStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1
nmhGetSnmpTargetAddrRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1
nmhGetSnmpAgentxTransportDomain ARG_LIST((INT4 *));
extern INT1
nmhGetSnmpAgentxMasterAgentAddr ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhGetSnmpAgentxMasterAgentPortNo ARG_LIST((UINT4 *));
extern INT1
nmhTestv2SnmpAgentxMasterAgentAddr ARG_LIST((UINT4 *, tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhSetSnmpAgentxMasterAgentAddr ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2SnmpAgentxMasterAgentPortNo ARG_LIST((UINT4 *pu4ErrorCode, UINT4));
extern INT1
nmhSetSnmpAgentxMasterAgentPortNo ARG_LIST((UINT4));

extern INT1
nmhGetSnmpAgentControl ARG_LIST((INT4 *));

extern INT1
nmhSetSnmpAgentControl ARG_LIST((INT4 ));

extern INT1
nmhTestv2SnmpAgentControl ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhGetSnmpAgentxSubAgentControl ARG_LIST((INT4 *));

extern INT1
nmhSetSnmpAgentxSubAgentControl ARG_LIST((INT4 ));

extern INT1
nmhTestv2SnmpAgentxSubAgentControl ARG_LIST((UINT4 *  ,INT4 ));

extern INT4 SnmpAgentxSubAgentControlSet(tSnmpIndex *, tRetVal *);
extern INT4 SnmpAgentControlSet(tSnmpIndex *, tRetVal *);
extern INT4 SnmpAgentxMasterAgentAddrSet(tSnmpIndex *, tRetVal *);
extern INT4 SnmpAgentxMasterAgentPortNoSet(tSnmpIndex *, tRetVal *);

extern INT4 FsSnmpListenAgentPortSet(tSnmpIndex *, tRetVal *);
extern INT1 nmhGetFsSnmpListenAgentPort ARG_LIST((UINT4 *));
extern INT1 nmhTestv2FsSnmpListenAgentPort ARG_LIST((UINT4 *  ,UINT4 ));
extern INT1 nmhSetFsSnmpListenAgentPort ARG_LIST((UINT4 ));

#ifdef IPSECv6_WANTED

VOID IssProcessSecv6StatsPage (tHttp * pHttp);

VOID IssProcessSecv6SelectorPage (tHttp * pHttp); 

VOID IssProcessSelectorConfInfoGet (tHttp * pHttp);

VOID IssProcessSelectorConfSet (tHttp * pHttp);

extern INT1
nmhGetFirstIndexFsipv6SecAhEspStatsTable ARG_LIST((INT4 *));

extern INT1
nmhGetNextIndexFsipv6SecAhEspStatsTable ARG_LIST((INT4 , INT4 *));

extern INT1
nmhGetFsipv6SecInAhPkts ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsipv6SecOutAhPkts ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsipv6SecAhPktsAllow ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsipv6SecAhPktsDiscard ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsipv6SecInEspPkts ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsipv6SecOutEspPkts ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsipv6SecEspPktsAllow ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsipv6SecEspPktsDiscard ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFirstIndexFsipv6SecIfStatsTable ARG_LIST((INT4 *));

extern INT1
nmhGetNextIndexFsipv6SecIfStatsTable ARG_LIST((INT4 , INT4 *));

extern INT1
nmhGetFsipv6SecIfInPkts ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsipv6SecIfOutPkts ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsipv6SecIfPktsApply ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsipv6SecIfPktsDiscard ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsipv6SecIfPktsBypass ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhTestv2Fsipv6SelStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , 
                                  INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2Fsipv6SelFilterFlag ARG_LIST((UINT4 *  ,INT4  , INT4  ,
                                       INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2Fsipv6SelPolicyIndex ARG_LIST((UINT4 *  ,INT4  , INT4  ,
                                       INT4  , INT4 , INT4  ,INT4 ));

extern INT1
nmhSetFsipv6SelStatus ARG_LIST((INT4  , INT4  , INT4  , INT4  ,
                                INT4  ,INT4 ));

extern INT1
nmhSetFsipv6SelFilterFlag ARG_LIST((INT4  , INT4  , INT4  ,
                                   INT4  , INT4  ,INT4));

extern INT1
nmhSetFsipv6SelPolicyIndex ARG_LIST((INT4  , INT4  , INT4  ,
                                    INT4  , INT4  ,INT4 ));

extern INT1
nmhGetFirstIndexFsipv6SecSelectorTable ARG_LIST((INT4 * , INT4 * ,
                                                 INT4 * , INT4 * ,
                                                 INT4 *));

extern INT1
nmhGetNextIndexFsipv6SecSelectorTable ARG_LIST((INT4 , INT4 * ,
                                                INT4 , INT4 * ,
                                                INT4 , INT4 * ,
                                                INT4 , INT4 * ,
                                                INT4 , INT4 *));

extern INT1
nmhGetFsipv6SelFilterFlag ARG_LIST((INT4  , INT4  , INT4  ,
                                    INT4  , INT4 ,INT4 *));

extern INT1
nmhGetFsipv6SelPolicyIndex ARG_LIST((INT4  , INT4  , INT4  ,
                                     INT4  , INT4 ,INT4 *));
extern INT4 Fsipv6SelStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6SelFilterFlagSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsipv6SelPolicyIndexSet(tSnmpIndex *, tRetVal *);
#endif /* IPSECv6_WANTED */
 
#ifdef IGMP_WANTED
extern INT1 nmhGetFirstIndexIgmpInterfaceTable (INT4 *);
extern INT1 nmhGetFsIgmpGlobalStatus(INT4 *);
extern INT1 nmhGetIgmpInterfaceStatus (INT4 , INT4 *);
extern INT1 nmhGetIgmpInterfaceVersion (INT4 , UINT4 *);
extern INT1 nmhGetIgmpInterfaceQueryInterval (INT4 , UINT4 *);
extern INT1 nmhGetIgmpInterfaceQueryMaxResponseTime (INT4 , UINT4 *);
extern INT1 nmhGetIgmpInterfaceRobustness (INT4 , UINT4 *);
extern INT1 nmhGetFsIgmpInterfaceFastLeaveStatus (INT4 , INT4 *);
extern INT1 nmhGetFsIgmpInterfaceAdminStatus (INT4 , INT4 *);
extern INT1 nmhGetFsIgmpInterfaceLimit (INT4 , UINT4 *);
extern INT1 nmhGetFsIgmpInterfaceGroupListId (INT4 , UINT4 *);
extern INT1 nmhGetFsIgmpInterfaceCurGrpCount (INT4 , UINT4 *);
extern INT1 nmhGetFsIgmpInterfaceJoinPktRate (INT4 , INT4 *);
extern INT1 nmhGetNextIndexIgmpInterfaceTable (INT4 , INT4 *);
extern INT1 nmhTestv2IgmpInterfaceStatus (UINT4 *, INT4 , INT4);
extern INT1 nmhTestv2IgmpInterfaceVersion (UINT4 *, INT4 , UINT4);
extern INT1 nmhSetIgmpInterfaceVersion (INT4 , INT4);
extern INT1 nmhTestv2IgmpInterfaceQueryInterval (UINT4 *, INT4 , UINT4);
extern INT1 nmhSetIgmpInterfaceQueryInterval (INT4 , INT4);
extern INT1 nmhTestv2IgmpInterfaceQueryMaxResponseTime (UINT4 *, INT4 , UINT4);
extern INT1 nmhSetIgmpInterfaceQueryMaxResponseTime (INT4 , INT4);
extern INT1 nmhTestv2IgmpInterfaceRobustness (UINT4 *, INT4 , UINT4);
extern INT1 nmhSetIgmpInterfaceRobustness (INT4 , INT4);
extern INT1 nmhTestv2FsIgmpInterfaceFastLeaveStatus (UINT4 *, INT4 , INT4);
extern INT1 nmhSetFsIgmpInterfaceFastLeaveStatus (INT4 , INT4);
extern INT1 nmhTestv2FsIgmpInterfaceAdminStatus (UINT4 *, INT4 , INT4);
extern INT1 nmhTestv2FsIgmpInterfaceGroupListId (UINT4 *, INT4 , UINT4);
extern INT1 nmhTestv2FsIgmpInterfaceLimit (UINT4 *, INT4 , UINT4);
extern INT1 nmhSetFsIgmpInterfaceAdminStatus (INT4 , INT4);
extern INT1 nmhSetFsIgmpInterfaceGroupListId (INT4 , UINT4);
extern INT1 nmhSetFsIgmpInterfaceLimit (INT4 , UINT4);
extern INT1 nmhSetIgmpInterfaceStatus (INT4 , INT4);
extern INT1 nmhTestv2FsIgmpInterfaceJoinPktRate (UINT4 *, INT4, INT4);
extern INT1 nmhSetFsIgmpInterfaceJoinPktRate (INT4, INT4);
extern INT1 nmhTestv2FsIgmpGlobalStatus (UINT4 *, INT4);
extern INT1 nmhSetFsIgmpGlobalStatus(INT4 );
extern INT1 nmhGetFirstIndexIgmpCacheTable (UINT4 *, INT4 *);
extern INT1 nmhGetIgmpCacheSourceFilterMode (UINT4 , INT4 , INT4 *);
extern INT1 nmhGetNextIndexIgmpCacheTable (UINT4 , UINT4 *, INT4 , INT4 *);
extern INT1 nmhGetFirstIndexIgmpSrcListTable (UINT4 *, INT4 *, UINT4 *);
extern INT1 nmhGetNextIndexIgmpSrcListTable (UINT4 , UINT4 *, INT4 , INT4 *, 
                                             UINT4 , UINT4 *);
extern INT1 nmhTestv2IgmpCacheStatus (UINT4 *, UINT4 , INT4 , INT4);
extern INT1 nmhSetIgmpCacheStatus (UINT4 , INT4 , INT4);
extern INT1 nmhTestv2IgmpSrcListStatus (UINT4 *, UINT4 , INT4 , UINT4 , INT4);
extern INT1 nmhSetIgmpSrcListStatus (UINT4 , INT4 , UINT4 , INT4);

extern INT1 nmhGetFsIgmpInterfaceChannelTrackStatus (INT4 ,INT4 *);
extern INT1 nmhSetFsIgmpInterfaceChannelTrackStatus (INT4  ,INT4 );
extern INT1 nmhTestv2FsIgmpInterfaceChannelTrackStatus (UINT4 *  ,INT4  ,INT4 );
extern INT4 nmhGetFsIgmpInterfaceOperStatus(INT4 ,INT4 *);
extern INT4 FsIgmpInterfaceAdminStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 IgmpInterfaceVersionSet(tSnmpIndex *, tRetVal *);
extern INT4 FsIgmpInterfaceFastLeaveStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsIgmpInterfaceChannelTrackStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 IgmpInterfaceQueryIntervalSet(tSnmpIndex *, tRetVal *);
extern INT4 IgmpInterfaceQueryMaxResponseTimeSet(tSnmpIndex *, tRetVal *);
extern INT4 IgmpInterfaceRobustnessSet(tSnmpIndex *, tRetVal *);
extern INT4 IgmpInterfaceStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 IgmpCacheStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 IgmpSrcListStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsIgmpInterfaceAdminStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsIgmpProxyRtrIfaceRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsIgmpProxyRtrIfaceCfgOperVersionSet(tSnmpIndex *, tRetVal *);
extern INT4 FsIgmpProxyRtrIfacePurgeIntervalSet(tSnmpIndex *, tRetVal *);
extern INT4 IgmpCheckProxyStatus (VOID);
extern INT1 IgmpUtilGetMembershipInfo (UINT4 u4IgmpGrpAddr, UINT4 u4IgmpSrcAddr,
                            UINT4 u4IgmpReporterAddr, INT4 i4IgmpIndex,
                            UINT4 *pu4IgmpGrpAddr, UINT4 *pu4IgmpSrcAddr,
                            UINT4 *pu4IgmpReporterAddr, INT4 *pi4IgmpIndex);
extern INT1
nmhGetFirstIndexFsIgmpGroupListTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));
extern INT1
nmhGetNextIndexFsIgmpGroupListTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));
extern INT1
nmhSetFsIgmpGrpListRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1
nmhTestv2FsIgmpGrpListRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));
#endif /* IGMP WANTED */

#ifdef MLD_WANTED
extern INT1 nmhGetFirstIndexMgmdHostInterfaceTable(INT4 *, INT4 *);
extern INT4 nmhGetFirstIndexMgmdRouterInterfaceTable(INT4 * , INT4 *);
extern INT1 nmhGetFsMgmdInterfaceAdminStatus(INT4 , INT4, INT4 *);
extern INT1 nmhGetFsMgmdInterfaceOperStatus(INT4, INT4 , INT4 *);
extern INT1 nmhGetMgmdRouterInterfaceVersion(INT4, INT4 , UINT4 *);
extern INT1 nmhGetFsMgmdInterfaceFastLeaveStatus(INT4, INT4 , INT4 *);
extern INT1 nmhGetMgmdRouterInterfaceQueryInterval(INT4 , INT4 , UINT4 *);
extern INT1 nmhGetMgmdRouterInterfaceQueryMaxResponseTime(INT4, INT4 , UINT4 *);
extern INT1 nmhGetMgmdRouterInterfaceRobustness(INT4, INT4, UINT4 *);
extern INT1 nmhGetNextIndexMgmdRouterInterfaceTable(INT4, INT4 *, INT4, INT4 *);
extern INT1 nmhGetFsMgmdInterfaceJoinPktRate(INT4, INT4, INT4 *);
extern INT1 nmhTestv2MgmdRouterInterfaceStatus(UINT4 *  ,INT4  , INT4  ,INT4 );
extern INT1 nmhTestv2FsMgmdInterfaceAdminStatus(UINT4 *  ,INT4  , INT4  ,INT4 );
extern INT1 nmhTestv2MgmdRouterInterfaceVersion(UINT4 *  ,INT4  , INT4  ,UINT4 );
extern INT1 nmhTestv2FsMgmdInterfaceFastLeaveStatus(UINT4 *  ,INT4  , INT4  ,INT4 );
extern INT1 nmhTestv2MgmdRouterInterfaceQueryInterval(UINT4 *  ,INT4  , INT4  ,UINT4 );
extern INT1 nmhTestv2MgmdRouterInterfaceQueryMaxResponseTime(UINT4 *  ,INT4  , INT4  ,UINT4 );
extern INT1 nmhTestv2MgmdRouterInterfaceRobustness(UINT4 *  ,INT4  , INT4  ,UINT4 );
extern INT1 nmhTestv2FsMgmdInterfaceJoinPktRate(UINT4 *  ,INT4  , INT4  ,INT4 );
extern INT1 nmhSetMgmdRouterInterfaceStatus(INT4  , INT4  ,INT4 );
extern INT1 nmhSetFsMgmdInterfaceAdminStatus(INT4  , INT4  ,INT4 );
extern INT1 nmhSetMgmdRouterInterfaceVersion(INT4  , INT4  ,UINT4 );
extern INT1 nmhSetFsMgmdInterfaceFastLeaveStatus(INT4  , INT4  ,INT4 );
extern INT1 nmhSetMgmdRouterInterfaceQueryInterval(INT4  , INT4  ,UINT4 );
extern INT1 nmhSetMgmdRouterInterfaceQueryMaxResponseTime(INT4  , INT4  ,UINT4 );
extern INT1 nmhSetMgmdRouterInterfaceRobustness(INT4  , INT4  ,UINT4 );
extern INT1 nmhSetFsMgmdInterfaceJoinPktRate(INT4  , INT4  ,INT4 );
#endif /* MLD_WANTED */

extern INT4 IssAclProvisionModeSet (tSnmpIndex *, tRetVal *);
extern INT4 IssAclTriggerCommitSet(tSnmpIndex *, tRetVal *);


#if defined (PIM_WANTED) || defined (PIMV6_WANTED)
extern INT4 FsPimCandidateRPRowStatusSet(tSnmpIndex *, tRetVal *);
#endif

#ifdef IGMPPRXY_WANTED
extern INT1 nmhGetFirstIndexFsIgmpProxyRtrIfaceTable (INT4 *);
extern INT1 nmhGetNextIndexFsIgmpProxyRtrIfaceTable (INT4 , INT4 *);
extern INT1 nmhGetFsIgmpProxyRtrIfaceOperVersion (INT4 ,INT4 *);
extern INT1 nmhGetFsIgmpProxyRtrIfaceCfgOperVersion (INT4 ,INT4 *);
extern INT1 nmhGetFsIgmpProxyRtrIfacePurgeInterval (INT4 ,INT4 *);

extern INT1 nmhSetFsIgmpProxyRtrIfaceCfgOperVersion (INT4  ,INT4 );
extern INT1 nmhSetFsIgmpProxyRtrIfacePurgeInterval (INT4  ,INT4 );
extern INT1 nmhSetFsIgmpProxyRtrIfaceRowStatus (INT4  ,INT4 );

extern INT1 nmhTestv2FsIgmpProxyRtrIfaceCfgOperVersion (UINT4 *  ,INT4  ,INT4 );
extern INT1 nmhTestv2FsIgmpProxyRtrIfacePurgeInterval (UINT4 *  ,INT4  ,INT4 );
extern INT1 nmhTestv2FsIgmpProxyRtrIfaceRowStatus (UINT4 *  ,INT4  ,INT4 );
#endif /* IGMPPRXY_WANTED */
extern INT1 nmhValidateIndexInstanceFsDot1dPortPriorityTable (INT4 );
extern INT1 nmhGetFsDot1dPortDefaultUserPriority (INT4 ,INT4 *);
extern INT1 nmhTestv2FsDot1dPortDefaultUserPriority (UINT4 *  ,INT4  ,INT4 );
extern INT1 nmhSetFsDot1dPortDefaultUserPriority (INT4  ,INT4 );
extern INT1 nmhGetFsMIDot1qFutureVlanPortType (INT4 ,INT4 *);
extern INT1 nmhTestv2FsMIDot1qFutureVlanPortType (UINT4 *  ,INT4  ,INT4 );
extern INT1 nmhSetFsMIDot1qFutureVlanPortType (INT4  ,INT4 );

/*Function related to MI_VLAN Statistics Page */
extern INT4 nmhGetFirstIndexFsDot1qPortVlanStatisticsTable (INT4 * , UINT4 *);
extern INT4 nmhGetNextIndexFsDot1qPortVlanStatisticsTable ( INT4 , INT4 *, 
                                                      UINT4 , UINT4 *);
extern INT4 nmhGetFsDot1qTpVlanPortInFrames ( INT4 , UINT4 , UINT4 *);
extern INT4 nmhGetFsDot1qTpVlanPortOutFrames ( INT4 , UINT4 , UINT4 *);
extern INT4 nmhGetFsDot1qTpVlanPortInDiscards ( INT4 , UINT4 , UINT4 *);
extern INT4 nmhGetFsDot1qTpVlanPortInOverflowFrames ( INT4 , UINT4 , UINT4 *);
extern INT4 nmhGetFsDot1qTpVlanPortOutOverflowFrames ( INT4 , UINT4 , UINT4 *);
extern INT4 nmhGetFsDot1qTpVlanPortInOverflowDiscards ( INT4 , UINT4 , UINT4 *);
extern INT4 VlanGetNextPortStatisticsTableIndex ( INT4 , INT4 *, 
                                                      UINT4 , UINT4 *);
/*Function related to VLAN Traffic class Page */
extern INT1
nmhGetFirstIndexFsDot1qPortVlanTable (INT4 *);
extern INT1
nmhGetNextIndexFsDot1qPortVlanTable (INT4 ,INT4 *);
extern INT1
nmhGetFsDot1dTrafficClass ARG_LIST((INT4  , INT4 ,INT4 *));
extern INT1
nmhSetFsDot1dTrafficClass ARG_LIST((INT4  , INT4  ,INT4 ));
extern INT1
nmhTestv2FsDot1dTrafficClass ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));
/*Function related to VLAN Wildcard settings page*/
extern INT1
nmhTestv2Dot1qFutureVlanWildCardRowStatus ARG_LIST((UINT4 *  ,tMacAddr  ,INT4 ));
extern INT1
nmhGetDot1qFutureVlanWildCardRowStatus ARG_LIST((tMacAddr ,INT4 *));
extern INT1
nmhSetDot1qFutureVlanWildCardRowStatus ARG_LIST((tMacAddr  ,INT4 ));
extern INT1
nmhTestv2Dot1qFutureVlanWildCardEgressPorts ARG_LIST((UINT4 *  ,tMacAddr  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhSetDot1qFutureVlanWildCardEgressPorts ARG_LIST((tMacAddr  ,tSNMP_OCTET_STRING_TYPE *));
extern INT4
VlanConvertToLocalPortList (tPortList IfPortList, tLocalPortList LocalPortList);
extern INT1
nmhGetFirstIndexDot1qFutureVlanWildCardTable ARG_LIST((tMacAddr * ));
extern INT1
nmhGetNextIndexFsDot1qFutureVlanWildCardTable ARG_LIST((INT4 , INT4 * , tMacAddr , tMacAddr * ));
extern INT1
nmhGetDot1qFutureVlanWildCardEgressPorts ARG_LIST((tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetNextIndexDot1qFutureVlanWildCardTable ARG_LIST((tMacAddr , tMacAddr * ));
extern INT4
VlanIsPortListPortTypeValid PROTO ((UINT1* pu1Ports, INT4 i4Len,
                                    UINT1 u1PortType));
#ifdef MSTP_WANTED
extern INT1 nmhGetFirstIndexFsMIMstMstiPortTable (INT4 * , INT4 *);
extern INT1 nmhGetNextIndexFsMIMstMstiPortTable (INT4 , INT4 * , INT4 , INT4 *);
extern INT1 nmhGetFsMIMstMstiPortPathCost (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsMIMstMstiPortPriority (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsMIMstMstiForcePortState (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFirstIndexFsVcmConfigTable (INT4 *); 
extern INT1 nmhGetNextIndexFsVcmConfigTable (INT4, INT4 *);

extern INT1 nmhGetFirstIndexFsMIMstMstiBridgeTable (INT4 * , INT4 *);
extern INT1 nmhGetNextIndexFsMIMstMstiBridgeTable (INT4 , INT4 * , INT4 , INT4 *);
extern INT1 nmhGetFsMIMstMstiRootCost (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsMIMstMstiRootPort (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsMIMstMstiBridgePriority(INT4 ,INT4 ,INT4 *);

extern INT1 nmhGetFsMIMstCistPortPseudoRootId (INT4 ,tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetFsMIMstMstiPortPseudoRootId (INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * );

extern INT1 nmhTestv2FsMIMstCistPortPseudoRootId (UINT4 *  ,INT4  ,
                                                tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetFsMIMstCistPortPseudoRootId (INT4  ,tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2FsMIMstMstiPortPseudoRootId (UINT4 *  ,INT4  , INT4  ,
                                                tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetFsMIMstMstiPortPseudoRootId (INT4  , INT4  ,
                                             tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2FsMIMstMstiForcePortState (UINT4 *  ,INT4  , INT4  ,INT4 );
extern INT1 nmhSetFsMIMstMstiForcePortState (INT4  , INT4  ,INT4 );
extern INT1 nmhTestv2FsMIMstMstiPortPriority (UINT4 *  ,INT4  , INT4  ,INT4 );
extern INT1 nmhSetFsMIMstMstiPortPriority (INT4  , INT4  ,INT4 );
extern INT1 nmhTestv2FsMIMstMstiPortPathCost (UINT4 *  ,INT4  , INT4  ,INT4 );
extern INT1 nmhSetFsMIMstMstiPortPathCost (INT4  , INT4  ,INT4 );
extern INT1 nmhTestv2FsMIMstMstiBridgePriority (UINT4 *  ,INT4  , INT4  ,INT4 );
extern INT1 nmhSetFsMIMstMstiBridgePriority (INT4  , INT4  ,INT4 );
extern INT1 nmhGetFirstIndexFsMIMstCistPortTable (INT4 *);
extern INT1 nmhGetNextIndexFsMIMstCistPortTable (INT4 , INT4 *);
extern INT1 nmhGetFsMIMstCistPortRxMstBpduCount (INT4 ,UINT4 *);
extern INT1 nmhGetFsMIMstCistPortRxRstBpduCount (INT4 ,UINT4 *);
extern INT1 nmhGetFsMIMstCistPortRxConfigBpduCount (INT4 ,UINT4 *);
extern INT1 nmhGetFsMIMstCistPortRxTcnBpduCount (INT4 ,UINT4 *);
extern INT1 nmhGetFsMIMstCistPortTxMstBpduCount (INT4 ,UINT4 *);
extern INT1 nmhGetFsMIMstCistPortTxRstBpduCount (INT4 ,UINT4 *);
extern INT1 nmhGetFsMIMstCistPortTxConfigBpduCount (INT4 ,UINT4 *);
extern INT1 nmhGetFsMIMstCistPortTxTcnBpduCount (INT4 ,UINT4 *);
extern INT1 nmhGetFsMIMstCistPortInvalidMstBpduRxCount (INT4 ,UINT4 *);
extern INT1 nmhGetFsMIMstCistPortInvalidRstBpduRxCount (INT4 ,UINT4 *);
extern INT1 nmhGetFsMIMstCistPortInvalidConfigBpduRxCount (INT4 ,UINT4 *);
extern INT1 nmhGetFsMIMstCistPortInvalidTcnBpduRxCount (INT4 ,UINT4 *);
extern INT1 nmhGetFsMIMstCistPortDesignatedRoot (INT4 ,tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetFsMIMstCistPortDesignatedBridge (INT4 ,tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetFsMIMstCistPortDesignatedPort (INT4 ,tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetFsMIMstCistPortOperP2P (INT4 ,INT4 *);
extern INT1 nmhGetFsMIMstCistProtocolMigrationCount (INT4 ,UINT4 *);
extern INT1 nmhGetFsMIMstCistPortDesignatedCost (INT4 ,INT4 *);
extern INT1 nmhGetFsMIMstCistPortRegionalRoot (INT4 ,tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetFsMIMstCistPortRegionalPathCost (INT4 ,INT4 *);
extern INT1 nmhGetFsMIMstCistCurrentPortRole (INT4 ,INT4 *);
extern INT1 nmhGetFsMIMstCistPortState (INT4 ,INT4 *);
extern INT1 nmhGetFsMIMstMstiPortDesignatedRoot (INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetFsMIMstMstiPortDesignatedBridge (INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetFsMIMstMstiPortDesignatedPort (INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetFsMIMstMstiPortState (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsMIMstMstiPortForwardTransitions (INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsMIMstMstiPortReceivedBPDUs (INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsMIMstMstiPortTransmittedBPDUs (INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsMIMstMstiPortInvalidBPDUsRcvd (INT4  , INT4 ,UINT4 *);
extern INT1 nmhGetFsMIMstMstiPortDesignatedCost (INT4  , INT4 ,INT4 *);
extern INT1 nmhGetFsMIMstMstiCurrentPortRole (INT4  , INT4 ,INT4 *);
extern INT1
nmhTestv2FsMIMstMstiPortAdminPathCost (UINT4 * , INT4  , INT4 ,INT4 );

extern INT1
nmhSetFsMIMstMstiPortAdminPathCost (INT4  , INT4 ,INT4 );
#endif
 


#ifdef RSTP_WANTED
extern INT1 nmhGetFirstIndexFsDot1dStpTable (INT4 *);
extern INT1 nmhGetFsDot1dStpProtocolSpecification (INT4 ,INT4 *);
extern INT1 nmhGetFsDot1dStpTimeSinceTopologyChange (INT4 ,UINT4 *);
extern INT1 nmhGetFsDot1dStpDesignatedRoot (INT4 ,tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetFsDot1dStpRootCost (INT4 ,INT4 *);
extern INT1 nmhGetFsDot1dStpRootPort (INT4 ,INT4 *);
extern INT1 nmhGetFsDot1dStpMaxAge (INT4 ,INT4 *);
extern INT1 nmhGetFsDot1dStpHelloTime (INT4 ,INT4 *);
extern INT1 nmhGetFsDot1dStpHoldTime (INT4 ,INT4 *);
extern INT1 nmhGetFsDot1dStpForwardDelay (INT4 ,INT4 *);

extern INT1 nmhGetFirstIndexFsDot1dStpExtTable (INT4 *);
extern INT1 nmhGetNextIndexFsDot1dStpTable (INT4 , INT4 *);
extern INT1 nmhGetFsDot1dStpVersion (INT4 ,INT4 *);
extern INT1 nmhGetFsDot1dStpTxHoldCount (INT4 ,INT4 *);
extern INT1 nmhGetFsDot1dStpPriority (INT4 ,INT4 *);
extern INT1 nmhGetFsDot1dStpBridgeMaxAge (INT4 ,INT4 *);
extern INT1 nmhGetFsDot1dStpBridgeHelloTime (INT4 ,INT4 *);
extern INT1 nmhGetFsDot1dStpBridgeForwardDelay (INT4 ,INT4 *);
extern INT1 nmhGetFirstIndexFsDot1dStpPortTable (INT4 *);
extern INT1 nmhGetFsDot1dStpPortDesignatedRoot (INT4 ,tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsDot1dStpPortDesignatedCost (INT4 ,INT4 *);
extern INT1 nmhGetFsDot1dStpPortDesignatedBridge (INT4 ,tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetNextIndexFsDot1dStpPortTable (INT4 , INT4 *);
extern INT1 nmhGetFsDot1dStpPortDesignatedPort (INT4 ,tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetFsDot1dStpPortState (INT4 ,INT4 *);
extern INT1 nmhGetFsDot1dStpPortOperPointToPoint (INT4 ,INT4 *);
extern INT1 nmhGetFsMIRstPortRole (INT4 ,INT4 *);
#endif 

#ifdef PVRST_WANTED
extern INT1 nmhGetNextIndexFsMIPvrstInstPortTable (INT4, INT4 *, INT4, INT4 *);
extern INT1 nmhGetFirstIndexFsMIFuturePvrstTable (INT4 *);
extern INT1 nmhGetNextIndexFsMIFuturePvrstTable (INT4, INT4 *);
extern INT1 nmhGetFsMIPvrstInstBridgeMaxAge(INT4, INT4, INT4 *);
extern INT1 nmhGetFsMIPvrstInstBridgeHelloTime(INT4, INT4, INT4 *);
extern INT1 nmhGetFsMIPvrstInstBridgeForwardDelay(INT4, INT4, INT4*);
extern INT1 nmhGetFsMIPvrstInstTxHoldCount(INT4,INT4, INT4 *);
extern INT1 nmhGetFsMIPvrstInstBridgePriority(INT4,INT4, INT4 *);
extern INT1 nmhGetFsMIPvrstBrgAddress(INT4, tMacAddr*);
extern INT1 nmhGetFsMIPvrstInstDesignatedRoot(INT4, INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsMIPvrstInstRootCost(INT4, INT4, INT4 *);
extern INT1 nmhGetFsMIPvrstInstRootPort(INT4 , INT4, INT4 *);
extern INT1 nmhGetFsMIPvrstInstHoldTime(INT4, INT4, INT4 *);
extern INT1 nmhGetFsMIPvrstInstTimeSinceTopologyChange(INT4, INT4, UINT4*);
extern INT1 nmhGetFsMIPvrstInstTopChanges(INT4, INT4, UINT4 *);
extern INT1 nmhGetNextIndexFsMIFuturePvrstPortTable(INT4, INT4*);
extern INT1 nmhGetFirstIndexFsMIFuturePvrstPortTable(INT4*);
extern INT1 nmhGetFsMIPvrstPortInvalidBpdusRcvd(INT4, UINT4*);
extern INT1 nmhGetFsMIPvrstPortInvalidConfigBpduRxCount(INT4, UINT4*);
extern INT1 nmhGetFsMIPvrstPortInvalidTcnBpduRxCount(INT4, UINT4*);
extern INT1 nmhGetFsMIPvrstRootGuard(INT4, INT4*);
extern INT1 nmhGetFsMIPvrstBpduGuard(INT4, INT4*);
extern INT1 nmhGetFsMIPvrstPortLoopGuard(INT4 ,INT4 *);
extern INT1 nmhGetFsMIPvrstEncapType(INT4, INT4*);
extern INT1 nmhGetFsMIPvrstInstPortDesignatedRoot(INT4, INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsMIPvrstInstPortDesignatedBridge(INT4, INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsMIPvrstInstPortDesignatedPort(INT4, INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsMIPvrstInstPortState(INT4, INT4, INT4 *);
extern INT1 nmhGetFsMIPvrstInstPortRole(INT4, INT4, INT4 *);
extern INT1 nmhGetNextIndexFsMIPvrstInstBridgeTable(INT4, INT4 *, INT4, INT4 *);
extern INT1 nmhGetFsMIPvrstInstPortEnabledStatus(INT4, INT4, INT4 *);
extern INT1 nmhGetFsMIPvrstInstPortPathCost(INT4, INT4, INT4 *);
extern INT1 nmhGetFsMIPvrstInstPortPriority(INT4, INT4, INT4 *);
extern INT1 nmhGetFsMIPvrstInstPortReceivedBpdus(INT4, INT4, UINT4 *);
extern INT1 nmhGetFsMIPvrstInstPortRxConfigBpduCount(INT4, INT4, UINT4 *);
extern INT1 nmhGetFsMIPvrstInstPortRxTcnBpduCount(INT4, INT4, UINT4 *);
extern INT1 nmhGetFsMIPvrstInstPortTransmittedBpdus(INT4, INT4, UINT4*);
extern INT1 nmhGetFsMIPvrstInstPortTxConfigBpduCount(INT4, INT4, UINT4*);
extern INT1 nmhGetFsMIPvrstInstPortTxTcnBpduCount(INT4, INT4, UINT4 *);
extern INT1 nmhGetFsMIPvrstInstProtocolMigrationCount(INT4, INT4, UINT4*);
extern INT1 nmhGetFsMIPvrstPortAdminPointToPoint(INT4, INT4*);
extern INT1 nmhGetFsMIPvrstPortEnabledStatus(INT4, INT4*);
extern INT1 nmhGetFsMIPvrstInstPortEnableStatus(INT4, INT4, INT4 *);
extern INT1 nmhGetFsMIPvrstSystemControl (INT4, INT4*);
extern INT1 nmhGetFsMIPvrstModuleStatus (INT4, INT4*);
extern INT1 nmhGetFsMIPvrstPortRowStatus (INT4 ,INT4 *);

extern INT1 nmhTestv2FsMIPvrstInstPortEnabledStatus (UINT4 *, INT4, INT4, INT4);
extern INT1 nmhTestv2FsMIPvrstInstPortPathCost (UINT4 *, INT4, INT4, INT4);
extern INT1 nmhTestv2FsMIPvrstInstPortPriority (UINT4*, INT4, INT4, INT4);
extern INT1 nmhTestv2FsMIPvrstInstBridgeMaxAge (UINT4 *, INT4, INT4, INT4);
extern INT1 nmhTestv2FsMIPvrstInstBridgeForwardDelay(UINT4 *, INT4, INT4, INT4);
extern INT1 nmhTestv2FsMIPvrstInstTxHoldCount(UINT4 *, INT4, INT4, INT4);
extern INT1 nmhTestv2FsMIPvrstInstBridgePriority(UINT4 *, INT4, INT4, INT4);
extern INT1 nmhTestv2FsMIPvrstPortEnabledStatus(UINT4 *, INT4, INT4);
extern INT1 nmhTestv2FsMIPvrstPortAdminPointToPoint(UINT4 *, INT4, INT4);
extern INT1 nmhTestv2FsMIPvrstRootGuard(UINT4 *, INT4, INT4);
extern INT1 nmhTestv2FsMIPvrstInstPortEnableStatus(UINT4 *, INT4, INT4, INT4);
extern INT1 nmhTestv2FsMIPvrstBpduGuard(UINT4 *, INT4, INT4);
extern INT1 nmhTestv2FsMIPvrstPortLoopGuard(UINT4 *  ,INT4  ,INT4 );
extern INT1 nmhTestv2FsMIPvrstInstBridgeHelloTime(UINT4 *, INT4, INT4, INT4);
extern INT1 nmhTestv2FsMIPvrstEncapType(UINT4 *, INT4, INT4);
extern INT1 nmhTestv2FsMIPvrstInstPortAdminPathCost (UINT4 *  ,INT4  , INT4  ,INT4 );
extern INT1 nmhTestv2FsMIPvrstSystemControl (UINT4 *, INT4, INT4);
extern INT1 nmhTestv2FsMIPvrstModuleStatus (UINT4*, INT4, INT4);
extern INT1 nmhTestv2FsMIPvrstPortRowStatus (UINT4 *  ,INT4  ,INT4 );

extern INT1 nmhSetFsMIPvrstInstPortEnabledStatus (INT4, INT4, INT4);
extern INT1 nmhSetFsMIPvrstInstPortPathCost (INT4, INT4, INT4);
extern INT1 nmhSetFsMIPvrstInstPortPriority (INT4, INT4, INT4);
extern INT1 nmhSetFsMIPvrstInstBridgeMaxAge(INT4, INT4, INT4);
extern INT1 nmhSetFsMIPvrstInstBridgeForwardDelay(INT4, INT4, INT4);
extern INT1 nmhSetFsMIPvrstInstTxHoldCount(INT4, INT4, INT4);
extern INT1 nmhSetFsMIPvrstInstBridgePriority(INT4, INT4, INT4);
extern INT1 nmhSetFsMIPvrstPortEnabledStatus(INT4, INT4);
extern INT1 nmhSetFsMIPvrstPortAdminPointToPoint(INT4, INT4);
extern INT1 nmhSetFsMIPvrstRootGuard(INT4, INT4);
extern INT1 nmhSetFsMIPvrstBpduGuard(INT4, INT4);
extern INT1 nmhSetFsMIPvrstPortLoopGuard(INT4  ,INT4 );
extern INT1 nmhSetFsMIPvrstEncapType(INT4, INT4);
extern INT1 nmhSetFsMIPvrstInstBridgeHelloTime(INT4, INT4, INT4);
extern INT1 nmhSetFsMIPvrstInstPortEnableStatus(INT4, INT4, INT4);
extern INT1 nmhSetFsMIPvrstSystemControl (INT4, INT4);
extern INT1 nmhSetFsMIPvrstInstPortAdminPathCost (INT4  , INT4  ,INT4 );
extern INT1 nmhSetFsMIPvrstModuleStatus (INT4, INT4);
extern INT1 AstL2IwfGetPortOperStatus (INT4,UINT2,UINT1 *);
extern INT1 nmhSetFsMIPvrstPortRowStatus (INT4  ,INT4 );





#endif  /* PVRST_WANTED */

extern INT1 nmhGetFirstIndexIfIpTable (INT4 *pi4Index);
extern INT4 CfaValidateCfaIfIndex PROTO((UINT2 u2IfIndex));
extern INT4 CfaIfmAddDynamicStackEntry PROTO((UINT2 u2IfStackHigherLayer,
                                              UINT2 u2IfStackLowerLayer));
extern INT1 nmhGetNextIndexIfIpTable (INT4 i4Index, INT4 *pi4NextIndex);

#ifdef ELMI_WANTED
extern INT1 nmhGetFirstIndexFsElmiPortTable(INT4 *);
extern INT1 nmhGetNextIndexFsElmiPortTable(INT4, INT4 *);
extern INT1 nmhGetFsElmiUniSide(INT4, INT4 *);
extern INT1 nmhGetFsElmiPollingCounterValue(INT4, INT4 *);
extern INT1 nmhGetFsElmiStatusCounter(INT4 ,INT4 *);
extern INT1 nmhGetFsElmiPollingTimerValue(INT4, INT4 *);
extern INT1 nmhGetFsElmiPollingVerificationTimerValue(INT4, INT4 *);
extern INT1 nmhGetFsElmiTxElmiCheckEnqMsgCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiTxFullStatusEnqMsgCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiTxElmiCheckMsgCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiTxFullStatusMsgCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiTxFullStatusContMsgCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiTxAsyncStatusMsgCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiRxElmiCheckEnqMsgCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiRxFullStatusEnqMsgCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiRxFullStatusContEnqMsgCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiRxElmiCheckMsgCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiRxFullStatusMsgCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiRxFullStatusContMsgCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiRxAsyncStatusMsgCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiRxValidMsgCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiRxInvalidMsgCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiRelErrStatusTimeOutCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiRelErrInvalidSeqNumCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiRelErrInvalidStatusRespCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiProErrInvalidProtVerCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiProErrInvalidEvcRefIdCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiProErrInvalidMessageTypeCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiProErrOutOfSequenceInfoEleCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiProErrDuplicateInfoEleCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiRelErrRxUnSolicitedStatusCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiProErrMandatoryInfoEleMissingCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiProErrInvalidMandatoryInfoEleCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiProErrInvalidNonMandatoryInfoEleCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiProErrUnrecognizedInfoEleCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiTxFullStatusContEnqMsgCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiProErrUnexpectedInfoEleCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiProErrShortMessageCount(INT4, INT4 *);
extern INT1 nmhGetFsElmiNoOfConfiguredEvcs(INT4, INT4 *);
extern INT1 nmhSetFsElmiUniSide(INT4, INT4);
extern INT1 nmhTestv2FsElmiUniSide(UINT4*, INT4, INT4);
extern INT1 nmhSetFsElmiPollingCounterValue(INT4, INT4);
extern INT1 nmhTestv2FsElmiPollingCounterValue(UINT4*, INT4, INT4);
extern INT1 nmhSetFsElmiStatusCounter(INT4 ,INT4);
extern INT1 nmhTestv2FsElmiStatusCounter(UINT4*, INT4 ,INT4);
extern INT1 nmhSetFsElmiPollingTimerValue(INT4, INT4);
extern INT1 nmhTestv2FsElmiPollingTimerValue(UINT4*, INT4, INT4);
extern INT1 nmhSetFsElmiPollingVerificationTimerValue(INT4, INT4);
extern INT1 nmhTestv2FsElmiPollingVerificationTimerValue(UINT4*, INT4, INT4);
extern INT1 nmhGetFsElmiPortElmiStatus (INT4, INT4*);
extern INT1 nmhSetFsElmiPortElmiStatus (INT4, INT4);
extern INT1 nmhTestv2FsElmiPortElmiStatus (UINT4*, INT4, INT4);
extern INT1 nmhGetFsElmiSystemControl(INT4*);
#endif /*ELMI_WANTED*/

#ifdef ELPS_WANTED
extern INT1 nmhGetFsElpsGlobalTraceOption ARG_LIST((INT4 *));
extern INT1 nmhSetFsElpsGlobalTraceOption ARG_LIST((INT4 ));
extern INT1 nmhTestv2FsElpsGlobalTraceOption ARG_LIST((UINT4 *  ,INT4 ));
/* FsElpsContextTable  */
extern INT1 nmhGetFirstIndexFsElpsContextTable ARG_LIST((UINT4 *));
extern INT1 nmhGetNextIndexFsElpsContextTable ARG_LIST((UINT4 , UINT4 *));
extern INT1 nmhGetFsElpsContextSystemControl ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetFsElpsContextModuleStatus ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetFsElpsContextTraceInputString ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsElpsContextEnableTrap ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhSetFsElpsContextSystemControl ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhSetFsElpsContextModuleStatus ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhSetFsElpsContextTraceInputString ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsElpsContextEnableTrap ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhTestv2FsElpsContextSystemControl ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2FsElpsContextModuleStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2FsElpsContextTraceInputString ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsElpsContextEnableTrap ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
/*FsElpsPgConfigTable. */
extern INT1 nmhValidateIndexInstanceFsElpsPgConfigTable ARG_LIST((UINT4  , UINT4 ));
extern INT1 nmhGetFirstIndexFsElpsPgConfigTable ARG_LIST((UINT4 * , UINT4 *));
extern INT1 nmhGetNextIndexFsElpsPgConfigTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));
extern INT1 nmhGetFsElpsPgConfigType ARG_LIST((UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsElpsPgConfigServiceType ARG_LIST((UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsElpsPgConfigMonitorMechanism ARG_LIST((UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsElpsPgConfigIngressPort ARG_LIST((UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsElpsPgConfigWorkingPort ARG_LIST((UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsElpsPgConfigProtectionPort ARG_LIST((UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsElpsPgConfigWorkingServiceValue ARG_LIST((UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsElpsPgConfigProtectionServiceValue ARG_LIST((UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsElpsPgConfigOperType ARG_LIST((UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsElpsPgConfigProtType ARG_LIST((UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsElpsPgConfigName ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsElpsPgConfigRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));
extern INT1 nmhSetFsElpsPgConfigType ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsElpsPgConfigServiceType ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsElpsPgConfigMonitorMechanism ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsElpsPgConfigIngressPort ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsElpsPgConfigWorkingPort ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsElpsPgConfigProtectionPort ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsElpsPgConfigWorkingServiceValue ARG_LIST((UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhSetFsElpsPgConfigProtectionServiceValue ARG_LIST((UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhSetFsElpsPgConfigOperType ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsElpsPgConfigProtType ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsElpsPgConfigName ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsElpsPgConfigRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsElpsPgConfigType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsElpsPgConfigServiceType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsElpsPgConfigMonitorMechanism ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsElpsPgConfigIngressPort ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsElpsPgConfigWorkingPort ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsElpsPgConfigProtectionPort ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsElpsPgConfigWorkingServiceValue ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsElpsPgConfigProtectionServiceValue ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsElpsPgConfigOperType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsElpsPgConfigProtType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsElpsPgConfigName ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsElpsPgConfigRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/*FsElpsPgCfmTable. */

extern INT1 nmhGetFirstIndexFsElpsPgCfmTable ARG_LIST((UINT4 * , UINT4 *));
extern INT1 nmhGetNextIndexFsElpsPgCfmTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

extern INT1 nmhGetFsElpsPgCfmWorkingMEG ARG_LIST((UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsElpsPgCfmWorkingME ARG_LIST((UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsElpsPgCfmWorkingMEP ARG_LIST((UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsElpsPgCfmProtectionMEG ARG_LIST((UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsElpsPgCfmProtectionME ARG_LIST((UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsElpsPgCfmProtectionMEP ARG_LIST((UINT4  , UINT4 ,UINT4 *));
extern INT1 nmhGetFsElpsPgCfmRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));
extern INT1 nmhGetFsElpsPgPscVersion(UINT4 ,UINT4 ,UINT4 *);

extern INT1 nmhSetFsElpsPgCfmWorkingMEG ARG_LIST((UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhSetFsElpsPgCfmWorkingME ARG_LIST((UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhSetFsElpsPgCfmWorkingMEP ARG_LIST((UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhSetFsElpsPgCfmProtectionMEG ARG_LIST((UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhSetFsElpsPgCfmProtectionME ARG_LIST((UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhSetFsElpsPgCfmProtectionMEP ARG_LIST((UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhSetFsElpsPgCfmRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetFsElpsPgPscVersion(UINT4 ,UINT4 ,UINT4 );

extern INT1 nmhTestv2FsElpsPgCfmWorkingMEG ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsElpsPgCfmWorkingME ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsElpsPgCfmWorkingMEP ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsElpsPgCfmProtectionMEG ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsElpsPgCfmProtectionME ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsElpsPgCfmProtectionMEP ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsElpsPgCfmRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsElpsPgPscVersion(UINT4 *,UINT4 ,UINT4 ,UINT4 );

extern INT1 nmhGetFirstIndexFsElpsPgServiceListTable ARG_LIST((UINT4 * , UINT4 * , 
                                                               UINT4 *));
extern INT1 nmhGetNextIndexFsElpsPgServiceListTable ARG_LIST((UINT4 , UINT4 * , 
                                                              UINT4 , UINT4 * , 
                                                              UINT4 , UINT4 *));
extern INT1 nmhGetFsElpsPgServiceListRowStatus ARG_LIST((UINT4  , UINT4  , 
                                                         UINT4 ,INT4 *));
extern INT1 nmhSetFsElpsPgServiceListRowStatus ARG_LIST((UINT4  , UINT4  , 
                                                  UINT4  ,INT4 ));
extern INT1 nmhTestv2FsElpsPgServiceListRowStatus ARG_LIST((UINT4 *  ,UINT4  , 
                                                     UINT4  , UINT4  ,INT4 ));
extern INT4 FsElpsGlobalTraceOptionGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsGlobalTraceOptionSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsGlobalTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 
FsElpsGlobalTraceOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
extern INT4 GetNextIndexFsElpsContextTable(tSnmpIndex *, tSnmpIndex *);
extern INT4 FsElpsContextSystemControlGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsContextModuleStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsContextTraceInputStringGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsContextEnableTrapGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsContextSystemControlSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsContextModuleStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsContextTraceInputStringSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsContextEnableTrapSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsContextSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsContextModuleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsContextTraceInputStringTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsContextEnableTrapTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsContextTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
extern INT4 GetNextIndexFsElpsPgConfigTable(tSnmpIndex *, tSnmpIndex *);
extern INT4 FsElpsPgConfigTypeGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigServiceTypeGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigMonitorMechanismGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigIngressPortGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigWorkingPortGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigProtectionPortGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigWorkingServiceValueGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigProtectionServiceValueGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigOperTypeGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigProtTypeGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigNameGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigRowStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigServiceTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigMonitorMechanismSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigIngressPortSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigWorkingPortSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigProtectionPortSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigWorkingServiceValueSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigProtectionServiceValueSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigOperTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigProtTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigNameSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigServiceTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 
FsElpsPgConfigMonitorMechanismTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigIngressPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigWorkingPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigProtectionPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 
FsElpsPgConfigWorkingServiceValueTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 
FsElpsPgConfigProtectionServiceValueTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigOperTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigProtTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
extern INT4 GetNextIndexFsElpsPgCmdTable(tSnmpIndex *, tSnmpIndex *);
extern INT4 FsElpsPgCmdHoTimeGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCmdWTRGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCmdExtCmdGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCmdExtCmdStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCmdLocalConditionGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCmdLocalConditionStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCmdFarEndRequestGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCmdFarEndRequestStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCmdActiveRequestGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCmdSemStateGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCmdPgStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCmdApsPeriodicTimeGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCmdHoTimeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCmdWTRSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCmdExtCmdSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCmdApsPeriodicTimeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCmdHoTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCmdWTRTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCmdExtCmdTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCmdApsPeriodicTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCmdTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
extern INT4 GetNextIndexFsElpsPgCfmTable(tSnmpIndex *, tSnmpIndex *);
extern INT4 FsElpsPgCfmWorkingMEGGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCfmWorkingMEGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCfmWorkingMEPGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCfmProtectionMEGGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCfmProtectionMEGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCfmProtectionMEPGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCfmRowStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCfmWorkingMEGSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCfmWorkingMESet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCfmWorkingMEPSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCfmProtectionMEGSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCfmProtectionMESet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCfmProtectionMEPSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCfmRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCfmWorkingMEGTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCfmWorkingMETest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCfmWorkingMEPTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCfmProtectionMEGTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCfmProtectionMETest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCfmProtectionMEPTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCfmRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgCfmTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
extern INT4 GetNextIndexFsElpsPgServiceListTable(tSnmpIndex *, tSnmpIndex *);
extern INT4 FsElpsPgServiceListRowStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgServiceListRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgServiceListRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 
FsElpsPgServiceListTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
extern INT4 GetNextIndexFsElpsPgShareTable(tSnmpIndex *, tSnmpIndex *);
extern INT4 FsElpsPgSharePgStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 GetNextIndexFsElpsPgStatsTable(tSnmpIndex *, tSnmpIndex *);
extern INT4 FsElpsPgStatsAutoProtectionSwitchCountGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgStatsForcedSwitchCountGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgStatsManualSwitchCountGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgStatsClearStatisticsGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgStatsApsPktTxCountGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgStatsApsPktRxCountGet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgStatsClearStatisticsSet(tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgStatsClearStatisticsTest(UINT4 *, tSnmpIndex *, tRetVal *);
extern INT4 FsElpsPgStatsTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

#endif /*ELPS_WANTED*/


/* TCP UDP Prototypes*/
#define  WEB_CLI_U8_STR_LENGTH                          21
#define IPVX_IPV4_ADDR_LEN   4
#define IPVX_IPV6_ADDR_LEN   16
#define IPVX_MAX_INET_ADDR_LEN  IPVX_IPV6_ADDR_LEN
#define IPVX_SYSLOG_RXMAILID_LENGTH 100
#define ADDR_TYPE_ZERO 0
VOID IssProcessIfClearStatsPage (tHttp * pHttp);
VOID IssProcessTcpStatsPage (tHttp * pHttp);
extern INT1 nmhGetTcpRtoAlgorithm(INT4 *);
extern INT1 nmhGetTcpRtoMin(INT4 *);
extern INT1 nmhGetTcpRtoMax(INT4 *);
extern INT1 nmhGetTcpMaxConn(INT4 *);
extern INT1 nmhGetTcpActiveOpens(UINT4 *);
extern INT1 nmhGetTcpPassiveOpens(UINT4 *);
extern INT1 nmhGetTcpAttemptFails(UINT4 *);
extern INT1 nmhGetTcpEstabResets(UINT4 *);
extern INT1 nmhGetTcpCurrEstab(UINT4 *);
extern INT1 nmhGetTcpInSegs(UINT4 *);
extern INT1 nmhGetTcpOutSegs(UINT4 *);
extern INT1 nmhGetTcpRetransSegs(UINT4 *);
extern INT1 nmhGetTcpInErrs(UINT4 *);
extern INT1 nmhGetTcpOutRsts(UINT4 *);
extern INT1 nmhGetTcpHCInSegs(tSNMP_COUNTER64_TYPE *);
extern INT1 nmhGetTcpHCOutSegs(tSNMP_COUNTER64_TYPE *);

VOID IssProcessUdpStatsPage (tHttp * pHttp);
extern INT1 nmhGetUdpInDatagrams(UINT4 *);
extern INT1 nmhGetUdpNoPorts(UINT4 *);
extern INT1 nmhGetUdpInErrors(UINT4 *);
extern INT1 nmhGetUdpOutDatagrams(UINT4 *);
extern INT1 nmhGetUdpHCInDatagrams(tSNMP_COUNTER64_TYPE *);
extern INT1 nmhGetUdpHCOutDatagrams(tSNMP_COUNTER64_TYPE *);

VOID IssProcessTcpListenerPage (tHttp * pHttp);
extern INT1 nmhGetFirstIndexTcpListenerTable (INT4 * , tSNMP_OCTET_STRING_TYPE *                                        ,UINT4 *);
extern INT1 nmhGetNextIndexFsMIStdContextTable ARG_LIST((INT4 , INT4 *));
extern INT1 nmhGetFirstIndexFsMIStdContextTable ARG_LIST((INT4 *));
extern INT1 nmhGetNextIndexTcpListenerTable
                      (INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE * ,
                       tSNMP_OCTET_STRING_TYPE * , UINT4 , UINT4 *);

VOID IssProcessUdpConnectionsPage (tHttp * pHttp);
extern INT1 nmhGetFirstIndexUdpEndpointTable (INT4 * ,tSNMP_OCTET_STRING_TYPE *                        , UINT4 *, INT4 * , tSNMP_OCTET_STRING_TYPE * ,
                      UINT4 * , UINT4 *);
extern INT1 nmhGetNextIndexUdpEndpointTable
                    (INT4  , INT4 * , tSNMP_OCTET_STRING_TYPE *,
                     tSNMP_OCTET_STRING_TYPE * , UINT4 , UINT4 * ,
                     INT4  , INT4 * , tSNMP_OCTET_STRING_TYPE * ,
                     tSNMP_OCTET_STRING_TYPE *, UINT4 , UINT4 *,
                     UINT4 , UINT4 *);

/* IP v4,v6 Prototypes*/
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
VOID IssProcessIpv4SysSpInfoGet (tHttp * pHttp);
VOID IssProcessIpv4IfSpInfoGet (tHttp * pHttp);
extern INT4 FsIpifProxyArpAdminStatusSet(tSnmpIndex *, tRetVal *);
extern
INT1 nmhGetFsIpifProxyArpAdminStatus ARG_LIST((INT4 ,INT4 *));
extern 
INT1 nmhSetFsIpifProxyArpAdminStatus ARG_LIST((INT4  ,INT4 ));
#endif
#ifdef IP_WANTED
VOID IssProcessIpPingPage(tHttp *pHttp);
VOID IssProcessIpPingPageGet(tHttp *pHttp);
VOID IssProcessIpPingPageSet(tHttp *pHttp);
extern INT4 PingLock (VOID);
extern INT4 PingUnLock (VOID);
extern INT1 nmhGetFsMIPingDest ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsMIPingTimeout ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIPingTries ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIPingDataSize ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIPingStatus ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIPingSendCount ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIPingAverageTime ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIPingMaxTime ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIPingMinTime ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhGetFsMIPingSuccesses ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhGetFsMIPingEntryStatus ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhSetFsMIPingDest ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhSetFsMIPingTimeout ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsMIPingTries ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsMIPingDataSize ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhSetFsMIPingEntryStatus ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhTestv2FsMIPingDest ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT1 nmhTestv2FsMIPingTimeout ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIPingTries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIPingDataSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIPingEntryStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

#endif 
#ifdef IP6_WANTED
VOID IssProcessIpv6SysSpInfoGet (tHttp * pHttp);
VOID IssProcessIpv6IfSpInfoGet (tHttp * pHttp);
extern INT1
nmhGetFsMIIpv6PingDest ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsMIIpv6PingIfIndex ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsMIIpv6PingContextId ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsMIIpv6PingAdminStatus ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsMIIpv6PingInterval ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsMIIpv6PingRcvTimeout ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsMIIpv6PingTries ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsMIIpv6PingSize ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsMIIpv6PingSentCount ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsMIIpv6PingAverageTime ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsMIIpv6PingMaxTime ARG_LIST((INT4 ,INT4 *));
                                                                
extern INT1
nmhGetFsMIIpv6PingMinTime ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsMIIpv6PingOperStatus ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsMIIpv6PingSuccesses ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsMIIpv6PingPercentageLoss ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsMIIpv6PingData ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsMIIpv6PingSrcAddr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsMIIpv6PingZoneId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetFsMIIpv6PingDestAddrType ARG_LIST((INT4 ,INT4 *));


extern INT1
nmhSetFsMIIpv6PingDest ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsMIIpv6PingIfIndex ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsMIIpv6PingContextId ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsMIIpv6PingAdminStatus ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsMIIpv6PingInterval ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsMIIpv6PingRcvTimeout ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsMIIpv6PingTries ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsMIIpv6PingSize ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsMIIpv6PingData ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsMIIpv6PingSrcAddr ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsMIIpv6PingZoneId ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsMIIpv6PingDestAddrType ARG_LIST((INT4  ,INT4 ));


extern INT1 nmhTestv2FsMIIpv6PingDest ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1 nmhTestv2FsMIIpv6PingIfIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIIpv6PingContextId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIIpv6PingAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIIpv6PingInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhTestv2FsMIIpv6PingRcvTimeout ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsMIIpv6PingTries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsMIIpv6PingSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsMIIpv6PingData ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2FsMIIpv6PingSrcAddr ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2FsMIIpv6PingZoneId ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2FsMIIpv6PingDestAddrType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
#endif

extern INT1 nmhGetFirstIndexIpSystemStatsTable (INT4 *);
extern INT1 nmhGetNextIndexIpSystemStatsTable (INT4, INT4 *);



/* BSD SYSLOG Prototypes*/
VOID IssProcessBsdSyslogMailPage(tHttp * pHttp);
VOID IssProcessBsdSyslogMailPageGet(tHttp * pHttp);
VOID IssProcessBsdSyslogMailPageSet(tHttp * pHttp);
VOID IssProcessBsdSyslogFwdPage(tHttp * pHttp);
VOID IssProcessBsdSyslogFwdPageGet(tHttp * pHttp);
VOID IssProcessBsdSyslogFwdPageSet(tHttp * pHttp);
VOID IssProcessBsdSyslogLogPage(tHttp * pHttp);
VOID IssProcessBsdSyslogLogPageGet(tHttp * pHttp);
VOID IssProcessBsdSyslogLogPageSet(tHttp * pHttp);
VOID IssProcessBsdSyslogScalarPage (tHttp * pHttp);

extern INT1 nmhTestv2FsSyslogSmtpSenderMailId (UINT4 * ,tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetFsSyslogSmtpSenderMailId (tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetNextIndexFsSyslogFwdTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFirstIndexFsSyslogFwdTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFirstIndexFsSyslogMailTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetNextIndexFsSyslogMailTable ARG_LIST((INT4 , INT4 * , INT4 , 
                  INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhTestv2FsSyslogMailRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhSetFsSyslogMailRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhTestv2FsSyslogFwdRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhSetFsSyslogFwdRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhGetFsSyslogFwdPort ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1 nmhSetFsSyslogFwdPort ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhTestv2FsSyslogFwdPort ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhGetFsSyslogFwdTransType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1 nmhSetFsSyslogFwdTransType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhTestv2FsSyslogFwdTransType ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhGetFsSyslogRxMailId ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsSyslogMailRowStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1 nmhGetFsSyslogMailServUserName ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsSyslogMailServPassword ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhSetFsSyslogRxMailId ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsSyslogMailServUserName ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsSyslogMailServPassword ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsSyslogRxMailId ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsSyslogMailServUserName ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsSyslogMailServPassword ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhGetFirstIndexFsSyslogConfigTable ARG_LIST((INT4 *));
extern INT1 nmhGetFsSyslogConfigLogLevel ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhSetFsSyslogConfigLogLevel ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhTestv2FsSyslogConfigLogLevel ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhGetNextIndexFsSyslogConfigTable ARG_LIST((INT4 , INT4 *));
extern INT4 FsSyslogConfigLogLevelSet(tSnmpIndex *, tRetVal *);
extern INT1 nmhGetFsSyslogLogging ARG_LIST((INT4 *));
extern INT1 nmhGetFsSyslogConsoleLog ARG_LIST((INT4 *));
extern INT1 nmhGetFsSyslogSysBuffers ARG_LIST((INT4 *));
extern INT1 nmhGetFsSyslogFacility ARG_LIST((INT4 *));
extern INT4 FsSyslogConsoleLogSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSyslogSysBuffersSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSyslogLoggingSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSyslogFacilitySet(tSnmpIndex *, tRetVal *);
extern INT4 FsSyslogClearLogSet(tSnmpIndex *, tRetVal *);
extern INT1 nmhTestv2FsSyslogClearLog ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FsSyslogLogging ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FsSyslogConsoleLog ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FsSyslogSysBuffers ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FsSyslogFacility ARG_LIST((UINT4 *  ,INT4 ));
extern INT4 FsSyslogSmtpSenderMailIdSet ARG_LIST((tSnmpIndex *, tRetVal *));

/*Syslog related macros */
#define IPVX_SYSLOG_RXMAILID_LENGTH 100
#define ISS_SMTP_MAX_STRING         64


/* SSL Prototypes */
#ifdef SSL_WANTED
VOID IssProcessSslDigitalCertPage (tHttp * pHttp);
VOID IssProcessSslDigitalCertPageGet (tHttp *, UINT1 *);
VOID IssProcessSslDigitalCertPageSet (tHttp *);
INT4 IssWebCopyDigitalCertificate (UINT1 *, UINT1 *);

extern INT4 SslCliGenRsaKeyPair (tCliHandle, INT4);
extern INT4 SslCliServerCert (tCliHandle, INT1 *);
extern INT4 SslArGenCertReq (UINT1 *, UINT1 **);
#endif /* SSL_WANTED */

/* Prototypes for Subnet Based Vlan Classification */

extern INT1 nmhTestv2Dot1qFutureVlanPortSubnetBasedClassification (UINT4 *,
                                                                INT4, INT4);


extern INT1 nmhSetDot1qFutureVlanPortSubnetBasedClassification ARG_LIST((INT4, INT4));
extern INT1 nmhGetDot1qFutureVlanPortSubnetBasedClassification ARG_LIST((INT4 ,INT4 *));
VOID IssProcessVlanSubnetMapPage (tHttp * pHttp);
VOID IssProcessVlanSubnetMapPageGet (tHttp * pHttp);
VOID IssProcessVlanSubnetMapPageSet (tHttp * pHttp);

extern INT1 nmhGetFirstIndexFsMIDot1qFutureVlanPortSubnetMapTable ARG_LIST((INT4 *,UINT4 *));
extern INT1 nmhGetFsMIDot1qFutureVlanPortSubnetMapVid ARG_LIST((INT4, UINT4,INT4 *));
extern INT1 nmhGetFsMIDot1qFutureVlanPortSubnetMapARPOption ARG_LIST((INT4, UINT4,INT4 *));
extern INT1 nmhGetNextIndexFsMIDot1qFutureVlanPortSubnetMapTable ARG_LIST((INT4 ,INT4 *,UINT4,UINT4 *));
extern INT1 nmhTestv2FsMIDot1qFutureVlanPortSubnetMapRowStatus ARG_LIST((UINT4 *,INT4, UINT4,INT4));
extern INT1 nmhGetFsMIDot1qFutureVlanPortSubnetMapRowStatus ARG_LIST((INT4, UINT4,INT4 *));
extern INT1 nmhSetFsMIDot1qFutureVlanPortSubnetMapRowStatus ARG_LIST((INT4, UINT4,INT4));
extern INT1 nmhTestv2FsMIDot1qFutureVlanPortSubnetMapVid ARG_LIST((UINT4 *,INT4, UINT4,INT4));
extern INT1 nmhSetFsMIDot1qFutureVlanPortSubnetMapVid ARG_LIST((INT4, UINT4,INT4));
extern INT1 nmhTestv2FsMIDot1qFutureVlanPortSubnetMapARPOption ARG_LIST((UINT4 *,INT4,UINT4,INT4));
extern INT1 nmhSetFsMIDot1qFutureVlanPortSubnetMapARPOption ARG_LIST((INT4,UINT4,INT4));
extern INT1 nmhTestv2FsMIDot1qFutureVlanGlobalsFdbFlush ARG_LIST((UINT4 *, INT4 ,INT4));
extern INT1 nmhSetFsMIDot1qFutureVlanGlobalsFdbFlush ARG_LIST((INT4, INT4));
extern INT1 nmhTestv2FsMIDot1qFutureVlanPortFdbFlush ARG_LIST((UINT4 *, INT4, INT4));
extern INT1 nmhSetFsMIDot1qFutureVlanPortFdbFlush ARG_LIST((INT4, INT4));
extern INT1 nmhTestv2FsMIDot1qFutureStVlanFdbFlush ARG_LIST((UINT4 *, INT4, UINT4, INT4));
extern INT1 nmhSetFsMIDot1qFutureStVlanFdbFlush ARG_LIST((INT4, UINT4, INT4));
extern INT1 nmhTestv2FsMIDot1qFuturePortVlanFdbFlush ARG_LIST((UINT4 *, INT4, UINT4, INT4, INT4));
extern INT1 nmhSetFsMIDot1qFuturePortVlanFdbFlush ARG_LIST((INT4, UINT4, INT4, INT4));
extern INT4 CfaGetIfPortRole PROTO((UINT4 u4IfIndex, UINT1 *pu1IfPortRole));
extern INT4 CfaGetIfDesigUplinkStatus PROTO((UINT4 u4IfIndex, UINT1 *pu1IfDesigUplinkStatus));
extern tUfdGroupInfo* CfaIfUfdGroupInfoDbGet PROTO ((UINT4));
extern INT4 CfaUfdSeperatePortlistOnPortRole PROTO ((tPortList IfPortList, tPortList *pUplinkPorts , 
tPortList *pDownlinkPorts));
extern INT1 nmhTestv2IfSplitHorizonSysControl ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2IfSplitHorizonModStatus ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhSetIfSplitHorizonSysControl ARG_LIST((INT4 ));
extern INT1 nmhSetIfSplitHorizonModStatus ARG_LIST((INT4 ));
extern INT1 nmhGetIfSplitHorizonSysControl ARG_LIST((INT4 *));
extern INT1 nmhGetIfSplitHorizonModStatus ARG_LIST((INT4 *));
extern INT1 nmhGetIfMainUfdOperStatus ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhValidateIndexInstanceIfMainTable ARG_LIST((INT4 ));
extern INT4 IfUfdGroupUplinkPortsSet(tSnmpIndex *, tRetVal *);
extern INT4 IfUfdGroupDownlinkPortsSet(tSnmpIndex *, tRetVal *);

extern INT1
nmhGetIfMainUfdDownlinkDisabledCount ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIfMainUfdDownlinkEnabledCount ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIfMainPortRole ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIfMainDesigUplinkStatus ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIfMainUfdGroupId ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhSetIfMainPortRole ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIfMainDesigUplinkStatus ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIfMainUfdGroupId ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhTestv2IfMainPortRole ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IfMainDesigUplinkStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IfMainUfdGroupId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhGetIfUfdSystemControl ARG_LIST((INT4 *));

extern INT1
nmhGetIfUfdModuleStatus ARG_LIST((INT4 *));

extern INT1 nmhSetIfUfdSystemControl ARG_LIST((INT4 ));

extern INT1
nmhSetIfUfdModuleStatus ARG_LIST((INT4 ));

extern INT1
nmhTestv2IfUfdSystemControl ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhTestv2IfUfdModuleStatus ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhDepv2IfUfdSystemControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2IfUfdModuleStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhValidateIndexInstanceIfUfdGroupTable ARG_LIST((INT4 ));


extern INT1
nmhGetFirstIndexIfUfdGroupTable ARG_LIST((INT4 *));


extern INT1
nmhGetNextIndexIfUfdGroupTable ARG_LIST((INT4 , INT4 *));

extern INT1
nmhGetIfUfdGroupName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetIfUfdGroupStatus ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIfUfdGroupUplinkPorts ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetIfUfdGroupDownlinkPorts ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetIfUfdGroupDesigUplinkPort ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIfUfdGroupUplinkCount ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIfUfdGroupDownlinkCount ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIfUfdGroupRowStatus ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhSetIfUfdGroupName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetIfUfdGroupUplinkPorts ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetIfUfdGroupDownlinkPorts ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));


extern INT1
nmhSetIfUfdGroupRowStatus ARG_LIST((INT4  ,INT4 ));


extern INT1
nmhTestv2IfUfdGroupName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2IfUfdGroupUplinkPorts ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2IfUfdGroupDownlinkPorts ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2IfUfdGroupDesigUplinkPort ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhSetIfUfdGroupDesigUplinkPort ARG_LIST((INT4  ,INT4 ));


extern INT1
nmhTestv2IfUfdGroupRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT4 IfUfdModuleStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData);

extern INT4 IfSplitHorizonSysControlSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData);

extern INT4 IfSplitHorizonModStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData);

extern INT4 IfUfdSystemControlSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData);

extern INT4 IfUfdGroupRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData);

extern INT4 IfMainUfdGroupIdSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData);

extern INT4 IfUfdGroupNameSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData);

extern INT4 IfUfdGroupDesigUplinkPortSet(tSnmpIndex *, tRetVal *);



/* SNTP prototypes*/

VOID IssProcessSntpUnicastConfPage (tHttp * pHttp);
VOID IssProcessSntpUnicastConfPageGet (tHttp * pHttp);
VOID IssProcessSntpUnicastConfPageSet (tHttp * pHttp);

VOID IssProcessSntpScalarsConfPage (tHttp * pHttp);
VOID IssProcessSntpScalarsConfPageGet (tHttp * pHttp);
VOID IssProcessSntpScalarsConfPageSet (tHttp * pHttp);

extern INT1 nmhGetFirstIndexFsSntpUnicastServerTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetNextIndexFsSntpUnicastServerTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsSntpUnicastServerVersion ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1 nmhGetFsSntpUnicastServerPort ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1 nmhGetFsSntpUnicastServerType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1 nmhGetFsSntpUnicastServerLastUpdateTime ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsSntpUnicastServerTxRequests ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));
extern INT1 nmhSetFsSntpUnicastServerVersion ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhSetFsSntpUnicastServerPort ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhSetFsSntpUnicastServerType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhSetFsSntpUnicastServerRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhTestv2FsSntpUnicastServerVersion ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhTestv2FsSntpUnicastServerPort ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhTestv2FsSntpUnicastServerType ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhTestv2FsSntpUnicastServerRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1 nmhGetFsSntpGlobalTrace ARG_LIST((INT4 *));
extern INT1 nmhGetFsSntpGlobalDebug ARG_LIST((INT4 *));
extern INT1 nmhGetFsSntpAdminStatus ARG_LIST((INT4 *));
extern INT1 nmhGetFsSntpClientVersion ARG_LIST((INT4 *));
extern INT1 nmhGetFsSntpClientAddressingMode ARG_LIST((INT4 *));
extern INT1 nmhGetFsSntpClientPort ARG_LIST((INT4 *));
extern INT1 nmhGetFsSntpTimeDisplayFormat ARG_LIST((INT4 *));
extern INT1 nmhGetFsSntpAuthKeyId ARG_LIST((INT4 *));
extern INT1 nmhGetFsSntpAuthAlgorithm ARG_LIST((INT4 *));
extern INT1 nmhGetFsSntpAuthKey ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsSntpTimeZone ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsSntpDSTStartTime ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsSntpDSTEndTime ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
/* Low Level SET Routine for All Objects.  */
extern INT1 nmhSetFsSntpGlobalTrace ARG_LIST((INT4 ));
extern INT1 nmhSetFsSntpGlobalDebug ARG_LIST((INT4 ));
extern INT1 nmhSetFsSntpAdminStatus ARG_LIST((INT4 ));
extern INT1 nmhSetFsSntpClientVersion ARG_LIST((INT4 ));
extern INT1 nmhSetFsSntpClientAddressingMode ARG_LIST((INT4 ));
extern INT1 nmhSetFsSntpClientPort ARG_LIST((INT4 ));
extern INT1 nmhSetFsSntpTimeDisplayFormat ARG_LIST((INT4 ));
extern INT1 nmhSetFsSntpAuthKeyId ARG_LIST((INT4 ));
extern INT1 nmhSetFsSntpAuthAlgorithm ARG_LIST((INT4 ));
extern INT1 nmhSetFsSntpAuthKey ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsSntpTimeZone ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsSntpDSTStartTime ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsSntpDSTEndTime ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
/* Low Level TEST Routines for.  */
extern INT1 nmhTestv2FsSntpGlobalTrace ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FsSntpGlobalDebug ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FsSntpAdminStatus ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FsSntpClientVersion ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FsSntpClientAddressingMode ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FsSntpClientPort ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FsSntpTimeDisplayFormat ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FsSntpAuthKeyId ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FsSntpAuthAlgorithm ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FsSntpAuthKey ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsSntpTimeZone ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsSntpDSTStartTime ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsSntpDSTEndTime ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

#ifdef ISIS_WANTED
/* Proto Validate Index Instance for IsisSysTable. */
extern INT1
nmhValidateIndexInstanceIsisSysTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IsisSysTable  */

extern INT1
nmhGetFirstIndexIsisSysTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIsisSysTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIsisSysVersion ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetIsisSysType ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysID ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetIsisSysMaxPathSplits ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysMaxLSPGenInt ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysOrigL1LSPBuffSize ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysMaxAreaAddresses ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysMinL1LSPGenInt ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysMinL2LSPGenInt ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysPollESHelloRate ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysWaitTime ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysAdminState ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysL1State ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysOrigL2LSPBuffSize ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysL2State ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysLogAdjacencyChanges ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysMaxAreaCheck ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysNextCircIndex ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysExistState ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysL2toL1Leaking ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysSetOverload ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysL1MetricStyle ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysL1SPFConsiders ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysL2MetricStyle ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysL2SPFConsiders ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysTEEnabled ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysMaxAge ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisSysReceiveLSPBufferSize ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetIsisSysType ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIsisSysID ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetIsisSysMaxPathSplits ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIsisSysMaxLSPGenInt ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIsisSysOrigL1LSPBuffSize ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIsisSysMaxAreaAddresses ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIsisSysMinL1LSPGenInt ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIsisSysMinL2LSPGenInt ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIsisSysPollESHelloRate ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIsisSysWaitTime ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIsisSysAdminState ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIsisSysOrigL2LSPBuffSize ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIsisSysLogAdjacencyChanges ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIsisSysMaxAreaCheck ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIsisSysExistState ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIsisSysL2toL1Leaking ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIsisSysSetOverload ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIsisSysL1MetricStyle ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIsisSysL1SPFConsiders ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIsisSysL2MetricStyle ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIsisSysL2SPFConsiders ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIsisSysTEEnabled ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIsisSysMaxAge ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIsisSysReceiveLSPBufferSize ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2IsisSysType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IsisSysID ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2IsisSysMaxPathSplits ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IsisSysMaxLSPGenInt ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IsisSysOrigL1LSPBuffSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IsisSysMaxAreaAddresses ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IsisSysMinL1LSPGenInt ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IsisSysMinL2LSPGenInt ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IsisSysPollESHelloRate ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IsisSysWaitTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IsisSysAdminState ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IsisSysOrigL2LSPBuffSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IsisSysLogAdjacencyChanges ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IsisSysMaxAreaCheck ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IsisSysExistState ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IsisSysL2toL1Leaking ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IsisSysSetOverload ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IsisSysL1MetricStyle ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IsisSysL1SPFConsiders ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IsisSysL2MetricStyle ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IsisSysL2SPFConsiders ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IsisSysTEEnabled ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IsisSysMaxAge ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IsisSysReceiveLSPBufferSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2IsisSysTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IsisManAreaAddrTable. */
extern INT1
nmhValidateIndexInstanceIsisManAreaAddrTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for IsisManAreaAddrTable  */

extern INT1
nmhGetFirstIndexIsisManAreaAddrTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIsisManAreaAddrTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIsisManAreaAddrExistState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetIsisManAreaAddrExistState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2IsisManAreaAddrExistState ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2IsisManAreaAddrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IsisAreaAddrTable. */
extern INT1
nmhValidateIndexInstanceIsisAreaAddrTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for IsisAreaAddrTable  */

extern INT1
nmhGetFirstIndexIsisAreaAddrTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIsisAreaAddrTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

/* Proto Validate Index Instance for IsisSysProtSuppTable. */
extern INT1
nmhValidateIndexInstanceIsisSysProtSuppTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IsisSysProtSuppTable  */

extern INT1
nmhGetFirstIndexIsisSysProtSuppTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIsisSysProtSuppTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIsisSysProtSuppExistState ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetIsisSysProtSuppExistState ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2IsisSysProtSuppExistState ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2IsisSysProtSuppTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IsisSummAddrTable. */
extern INT1
nmhValidateIndexInstanceIsisSummAddrTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for IsisSummAddrTable  */

extern INT1
nmhGetFirstIndexIsisSummAddrTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIsisSummAddrTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIsisSummAddrExistState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

extern INT1
nmhGetIsisSummAddrAdminState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

extern INT1
nmhGetIsisSummAddrMetric ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetIsisSummAddrExistState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

extern INT1
nmhSetIsisSummAddrAdminState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

extern INT1
nmhSetIsisSummAddrMetric ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2IsisSummAddrExistState ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

extern INT1
nmhTestv2IsisSummAddrAdminState ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

extern INT1
nmhTestv2IsisSummAddrMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2IsisSummAddrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IsisSysStatsTable. */
extern INT1
nmhValidateIndexInstanceIsisSysStatsTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IsisSysStatsTable  */

extern INT1
nmhGetFirstIndexIsisSysStatsTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIsisSysStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIsisSysStatCorrLSPs ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIsisSysStatAuthTypeFails ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIsisSysStatAuthFails ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIsisSysStatLSPDbaseOloads ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIsisSysStatManAddrDropFromAreas ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIsisSysStatAttmptToExMaxSeqNums ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIsisSysStatSeqNumSkips ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIsisSysStatOwnLSPPurges ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIsisSysStatIDFieldLenMismatches ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIsisSysStatMaxAreaAddrMismatches ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIsisSysStatPartChanges ARG_LIST((INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for IsisCircTable. */
extern INT1
nmhValidateIndexInstanceIsisCircTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IsisCircTable  */

extern INT1
nmhGetFirstIndexIsisCircTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIsisCircTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIsisCircIfIndex ARG_LIST((INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisCircIfSubIndex ARG_LIST((INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisCircLocalID ARG_LIST((INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisCircAdminState ARG_LIST((INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisCircExistState ARG_LIST((INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisCircType ARG_LIST((INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisCircExtDomain ARG_LIST((INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisCircAdjChanges ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIsisCircInitFails ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIsisCircRejAdjs ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIsisCircOutCtrlPDUs ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIsisCircInCtrlPDUs ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIsisCircIDFieldLenMismatches ARG_LIST((INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIsisCircLevel ARG_LIST((INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisCircMCAddr ARG_LIST((INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisCircPtToPtCircID ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetIsisCircPassiveCircuit ARG_LIST((INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisCircMeshGroupEnabled ARG_LIST((INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisCircMeshGroup ARG_LIST((INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisCircSmallHellos ARG_LIST((INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisCircUpTime ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetIsisCircIfIndex ARG_LIST((INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisCircIfSubIndex ARG_LIST((INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisCircLocalID ARG_LIST((INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisCircAdminState ARG_LIST((INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisCircExistState ARG_LIST((INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisCircType ARG_LIST((INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisCircExtDomain ARG_LIST((INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisCircLevel ARG_LIST((INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisCircMCAddr ARG_LIST((INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisCircPassiveCircuit ARG_LIST((INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisCircMeshGroupEnabled ARG_LIST((INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisCircMeshGroup ARG_LIST((INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisCircSmallHellos ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2IsisCircIfIndex ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisCircIfSubIndex ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisCircLocalID ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisCircAdminState ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisCircExistState ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisCircType ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisCircExtDomain ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisCircLevel ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisCircMCAddr ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisCircPassiveCircuit ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisCircMeshGroupEnabled ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisCircMeshGroup ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisCircSmallHellos ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2IsisCircTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IsisCircLevelTable. */
extern INT1
nmhValidateIndexInstanceIsisCircLevelTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IsisCircLevelTable  */

extern INT1
nmhGetFirstIndexIsisCircLevelTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIsisCircLevelTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIsisCircLevelMetric ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisCircLevelISPriority ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisCircLevelDesIS ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetIsisCircLevelLANDesISChanges ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIsisCircLevelHelloMultiplier ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisCircLevelHelloTimer ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisCircLevelDRHelloTimer ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisCircLevelLSPThrottle ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisCircLevelMinLSPRetransInt ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisCircLevelCSNPInterval ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisCircLevelPartSNPInterval ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetIsisCircLevelMetric ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisCircLevelISPriority ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisCircLevelHelloMultiplier ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisCircLevelHelloTimer ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisCircLevelDRHelloTimer ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisCircLevelLSPThrottle ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisCircLevelMinLSPRetransInt ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisCircLevelCSNPInterval ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisCircLevelPartSNPInterval ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2IsisCircLevelMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisCircLevelISPriority ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisCircLevelHelloMultiplier ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisCircLevelHelloTimer ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisCircLevelDRHelloTimer ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisCircLevelLSPThrottle ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisCircLevelMinLSPRetransInt ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisCircLevelCSNPInterval ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisCircLevelPartSNPInterval ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2IsisCircLevelTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IsisPacketCountTable. */
extern INT1
nmhValidateIndexInstanceIsisPacketCountTable ARG_LIST((INT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IsisPacketCountTable  */

extern INT1
nmhGetFirstIndexIsisPacketCountTable ARG_LIST((INT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIsisPacketCountTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIsisPacketCountHello ARG_LIST((INT4  , INT4  , INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIsisPacketCountLSP ARG_LIST((INT4  , INT4  , INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIsisPacketCountCSNP ARG_LIST((INT4  , INT4  , INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIsisPacketCountPSNP ARG_LIST((INT4  , INT4  , INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for IsisISAdjTable. */
extern INT1
nmhValidateIndexInstanceIsisISAdjTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IsisISAdjTable  */

extern INT1
nmhGetFirstIndexIsisISAdjTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIsisISAdjTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIsisISAdjState ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisISAdjNeighSNPAAddress ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetIsisISAdjNeighSysType ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisISAdjNeighSysID ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetIsisISAdjUsage ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisISAdjHoldTimer ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisISAdjNeighPriority ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisISAdjUpTime ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Proto Validate Index Instance for IsisISAdjAreaAddrTable. */
extern INT1
nmhValidateIndexInstanceIsisISAdjAreaAddrTable ARG_LIST((INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for IsisISAdjAreaAddrTable  */

extern INT1
nmhGetFirstIndexIsisISAdjAreaAddrTable ARG_LIST((INT4 * , INT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIsisISAdjAreaAddrTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

/* Proto Validate Index Instance for IsisISAdjIPAddrTable. */
extern INT1
nmhValidateIndexInstanceIsisISAdjIPAddrTable ARG_LIST((INT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IsisISAdjIPAddrTable  */

extern INT1
nmhGetFirstIndexIsisISAdjIPAddrTable ARG_LIST((INT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIsisISAdjIPAddrTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIsisISAdjIPAddressType ARG_LIST((INT4  , INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisISAdjIPAddress ARG_LIST((INT4  , INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for IsisISAdjProtSuppTable. */
extern INT1
nmhValidateIndexInstanceIsisISAdjProtSuppTable ARG_LIST((INT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IsisISAdjProtSuppTable  */

extern INT1
nmhGetFirstIndexIsisISAdjProtSuppTable ARG_LIST((INT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIsisISAdjProtSuppTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

/* Proto Validate Index Instance for IsisIPRATable. */
extern INT1
nmhValidateIndexInstanceIsisIPRATable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for IsisIPRATable  */

extern INT1
nmhGetFirstIndexIsisIPRATable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIsisIPRATable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIsisIPRADestType ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisIPRADest ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetIsisIPRADestPrefixLen ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetIsisIPRAExistState ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisIPRAAdminState ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisIPRAMetric ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisIPRAMetricType ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetIsisIPRASNPAAddress ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetIsisIPRADestType ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisIPRADest ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetIsisIPRADestPrefixLen ARG_LIST((INT4  , INT4  , INT4  ,UINT4 ));

extern INT1
nmhSetIsisIPRAExistState ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisIPRAAdminState ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisIPRAMetric ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisIPRAMetricType ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhSetIsisIPRASNPAAddress ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2IsisIPRADestType ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisIPRADest ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2IsisIPRADestPrefixLen ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,UINT4 ));

extern INT1
nmhTestv2IsisIPRAExistState ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisIPRAAdminState ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisIPRAMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisIPRAMetricType ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2IsisIPRASNPAAddress ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2IsisIPRATable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for IsisNotificationTable. */
extern INT1
nmhValidateIndexInstanceIsisNotificationTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IsisNotificationTable  */

extern INT1
nmhGetFirstIndexIsisNotificationTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIsisNotificationTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIsisTrapLSPID ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetIsisSystemLevel ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisPDUFragment ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetIsisFieldLen ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisMaxAreaAddress ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisProtocolVersion ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisLSPSize ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisOriginatingBufferSize ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIsisProtocolsSupported ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
/* FsSysInstance related Low level routines */

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsIsisMaxInstances ARG_LIST((INT4 *));

extern INT1
nmhGetFsIsisMaxCircuits ARG_LIST((INT4 *));

extern INT1
nmhGetFsIsisMaxAreaAddrs ARG_LIST((INT4 *));

extern INT1
nmhGetFsIsisMaxAdjs ARG_LIST((INT4 *));

extern INT1
nmhGetFsIsisMaxIPRAs ARG_LIST((INT4 *));

extern INT1
nmhGetFsIsisMaxEvents ARG_LIST((INT4 *));

extern INT1
nmhGetFsIsisMaxSummAddr ARG_LIST((INT4 *));

extern INT1
nmhGetFsIsisStatus ARG_LIST((INT4 *));

extern INT1
nmhGetFsIsisMaxLSPEntries ARG_LIST((INT4 *));

extern INT1
nmhGetFsIsisMaxMAA ARG_LIST((INT4 *));

extern INT1
nmhGetFsIsisFTStatus ARG_LIST((INT4 *));

extern INT1
nmhGetFsIsisFTState ARG_LIST((INT4 *));

extern INT1
nmhGetFsIsisFactor ARG_LIST((INT4 *));

extern INT1
nmhGetFsIsisMaxRoutes ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsIsisMaxInstances ARG_LIST((INT4 ));

extern INT1
nmhSetFsIsisMaxCircuits ARG_LIST((INT4 ));

extern INT1
nmhSetFsIsisMaxAreaAddrs ARG_LIST((INT4 ));

extern INT1
nmhSetFsIsisMaxAdjs ARG_LIST((INT4 ));

extern INT1
nmhSetFsIsisMaxIPRAs ARG_LIST((INT4 ));

extern INT1
nmhSetFsIsisMaxEvents ARG_LIST((INT4 ));

extern INT1
nmhSetFsIsisMaxSummAddr ARG_LIST((INT4 ));

extern INT1
nmhSetFsIsisStatus ARG_LIST((INT4 ));

extern INT1
nmhSetFsIsisMaxLSPEntries ARG_LIST((INT4 ));

extern INT1
nmhSetFsIsisMaxMAA ARG_LIST((INT4 ));

extern INT1
nmhSetFsIsisFTStatus ARG_LIST((INT4 ));

extern INT1
nmhSetFsIsisFactor ARG_LIST((INT4 ));

extern INT1
nmhSetFsIsisMaxRoutes ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsIsisMaxInstances ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhTestv2FsIsisMaxCircuits ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhTestv2FsIsisMaxAreaAddrs ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhTestv2FsIsisMaxAdjs ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhTestv2FsIsisMaxIPRAs ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhTestv2FsIsisMaxEvents ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhTestv2FsIsisMaxSummAddr ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhTestv2FsIsisStatus ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhTestv2FsIsisMaxLSPEntries ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhTestv2FsIsisMaxMAA ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhTestv2FsIsisFTStatus ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhTestv2FsIsisFactor ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhTestv2FsIsisMaxRoutes ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2FsIsisMaxInstances ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsIsisMaxCircuits ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsIsisMaxAreaAddrs ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsIsisMaxAdjs ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsIsisMaxIPRAs ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsIsisMaxEvents ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsIsisMaxSummAddr ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsIsisStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsIsisMaxLSPEntries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsIsisMaxMAA ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsIsisFTStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsIsisFactor ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsIsisMaxRoutes ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsIsisExtSysTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsIsisExtSysAreaRxPasswdTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsIsisExtSysDomainRxPasswdTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsIsisExtSummAddrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsIsisExtCircTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsIsisExtCircLevelTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsIsisExtIPRATable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsIsisExtCircLevelRxPasswordTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsIsisExtIPIfAddrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsIsisExtLogTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


/* Proto Validate Index Instance for FsIsisExtSysTable. */
extern INT1
nmhValidateIndexInstanceFsIsisExtSysTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIsisExtSysTable  */

extern INT1
nmhGetFirstIndexFsIsisExtSysTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsIsisExtSysTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsIsisExtSysAuthSupp ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtSysAreaAuthType ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtSysDomainAuthType ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtSysAreaTxPasswd ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsIsisExtSysDomainTxPasswd ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsIsisExtSysMinSPFSchTime ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtSysMaxSPFSchTime ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtSysMinLSPMark ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtSysMaxLSPMark ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtSysDelMetSupp ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtSysErrMetSupp ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtSysExpMetSupp ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtSysActSysType ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtSysActMPS ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtSysActMaxAA ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtSysActSysIDLen ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtSysActSysID ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsIsisExtSysDroppedPDUs ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtSysActOrigL1LSPBufSize ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtSysActOrigL2LSPBufSize ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtSysRouterID ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsIsisExtSysCkts ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtSysActiveCkts ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtSysAdjs ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtSysOperState ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsIsisExtSysAuthSupp ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtSysAreaAuthType ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtSysDomainAuthType ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtSysAreaTxPasswd ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsIsisExtSysDomainTxPasswd ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsIsisExtSysMinSPFSchTime ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtSysMaxSPFSchTime ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtSysMinLSPMark ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtSysMaxLSPMark ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtSysDelMetSupp ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtSysErrMetSupp ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtSysExpMetSupp ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtSysRouterID ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsIsisExtSysAuthSupp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtSysAreaAuthType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtSysDomainAuthType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtSysAreaTxPasswd ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2FsIsisExtSysDomainTxPasswd ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2FsIsisExtSysMinSPFSchTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtSysMaxSPFSchTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtSysMinLSPMark ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtSysMaxLSPMark ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtSysDelMetSupp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtSysErrMetSupp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtSysExpMetSupp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtSysRouterID ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for FsIsisExtSysAreaRxPasswdTable. */
extern INT1
nmhValidateIndexInstanceFsIsisExtSysAreaRxPasswdTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsIsisExtSysAreaRxPasswdTable  */

extern INT1
nmhGetFirstIndexFsIsisExtSysAreaRxPasswdTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsIsisExtSysAreaRxPasswdTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsIsisExtSysAreaRxPasswdExistState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsIsisExtSysAreaRxPasswdExistState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsIsisExtSysAreaRxPasswdExistState ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Proto Validate Index Instance for FsIsisExtSysDomainRxPasswdTable. */
extern INT1
nmhValidateIndexInstanceFsIsisExtSysDomainRxPasswdTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsIsisExtSysDomainRxPasswdTable  */

extern INT1
nmhGetFirstIndexFsIsisExtSysDomainRxPasswdTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsIsisExtSysDomainRxPasswdTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsIsisExtSysDomainRxPasswdExistState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsIsisExtSysDomainRxPasswdExistState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsIsisExtSysDomainRxPasswdExistState ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Proto Validate Index Instance for FsIsisExtSummAddrTable. */
extern INT1
nmhValidateIndexInstanceFsIsisExtSummAddrTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIsisExtSummAddrTable  */

extern INT1
nmhGetFirstIndexFsIsisExtSummAddrTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsIsisExtSummAddrTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsIsisExtSummAddrDelayMetric ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtSummAddrErrorMetric ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtSummAddrExpenseMetric ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsIsisExtSummAddrDelayMetric ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtSummAddrErrorMetric ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtSummAddrExpenseMetric ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsIsisExtSummAddrDelayMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtSummAddrErrorMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtSummAddrExpenseMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Proto Validate Index Instance for FsIsisExtSysEventTable. */
extern INT1
nmhValidateIndexInstanceFsIsisExtSysEventTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIsisExtSysEventTable  */

extern INT1
nmhGetFirstIndexFsIsisExtSysEventTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsIsisExtSysEventTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsIsisExtSysEventStr ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsIsisExtCircTable. */
extern INT1
nmhValidateIndexInstanceFsIsisExtCircTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIsisExtCircTable  */

extern INT1
nmhGetFirstIndexFsIsisExtCircTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsIsisExtCircTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsIsisExtCircIfStatus ARG_LIST((INT4  , INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtCircTxEnable ARG_LIST((INT4  , INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtCircRxEnable ARG_LIST((INT4  , INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtCircTxISHs ARG_LIST((INT4  , INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtCircRxISHs ARG_LIST((INT4  , INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtCircSNPA ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsIsisExtCircIfStatus ARG_LIST((INT4  , INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtCircTxEnable ARG_LIST((INT4  , INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtCircRxEnable ARG_LIST((INT4  , INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtCircSNPA ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsIsisExtCircIfStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtCircTxEnable ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtCircRxEnable ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtCircSNPA ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for FsIsisExtCircLevelTable. */
extern INT1
nmhValidateIndexInstanceFsIsisExtCircLevelTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIsisExtCircLevelTable  */

extern INT1
nmhGetFirstIndexFsIsisExtCircLevelTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsIsisExtCircLevelTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsIsisExtCircLevelDelayMetric ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtCircLevelErrorMetric ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtCircLevelExpenseMetric ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtCircLevelTxPassword ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsIsisExtCircLevelDelayMetric ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtCircLevelErrorMetric ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtCircLevelExpenseMetric ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtCircLevelTxPassword ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsIsisExtCircLevelDelayMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtCircLevelErrorMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtCircLevelExpenseMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtCircLevelTxPassword ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for FsIsisExtIPRATable. */
extern INT1
nmhValidateIndexInstanceFsIsisExtIPRATable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsIsisExtIPRATable  */

extern INT1
nmhGetFirstIndexFsIsisExtIPRATable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsIsisExtIPRATable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsIsisExtIPRADelayMetric ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtIPRAErrorMetric ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtIPRAExpenseMetric ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtIPRADelayMetricType ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtIPRAErrorMetricType ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtIPRAExpenseMetricType ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtIPRANextHop ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsIsisExtIPRADelayMetric ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtIPRAErrorMetric ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtIPRAExpenseMetric ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtIPRADelayMetricType ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtIPRAErrorMetricType ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtIPRAExpenseMetricType ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtIPRANextHop ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsIsisExtIPRADelayMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtIPRAErrorMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtIPRAExpenseMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtIPRADelayMetricType ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtIPRAErrorMetricType ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtIPRAExpenseMetricType ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtIPRANextHop ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Proto Validate Index Instance for FsIsisExtCircLevelRxPasswordTable. */
extern INT1
nmhValidateIndexInstanceFsIsisExtCircLevelRxPasswordTable ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsIsisExtCircLevelRxPasswordTable  */

extern INT1
nmhGetFirstIndexFsIsisExtCircLevelRxPasswordTable ARG_LIST((INT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsIsisExtCircLevelRxPasswordTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsIsisExtCircLevelRxPasswordExistState ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsIsisExtCircLevelRxPasswordExistState ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsIsisExtCircLevelRxPasswordExistState ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Proto Validate Index Instance for FsIsisExtIPIfAddrTable. */
extern INT1
nmhValidateIndexInstanceFsIsisExtIPIfAddrTable ARG_LIST((INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsIsisExtIPIfAddrTable  */

extern INT1
nmhGetFirstIndexFsIsisExtIPIfAddrTable ARG_LIST((INT4 * , INT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsIsisExtIPIfAddrTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsIsisExtIPIfExistState ARG_LIST((INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsIsisExtIPIfExistState ARG_LIST((INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsIsisExtIPIfExistState ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT4 IsisLock (VOID);
extern INT4 IsisUnlock (VOID);

/*ISIS GR*/

/* Low Level GET Routine  */
extern INT1
nmhGetFsIsisExtRestartSupport ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtGRRestartTimeInterval ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtGRT2TimeIntervalLevel1 ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtGRT2TimeIntervalLevel2 ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtGRT1TimeInterval ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtGRT1RetryCount ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtRestartStatus ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtRestartExitReason ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtRestartReason ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtHelperSupport ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsIsisExtHelperGraceTimeLimit ARG_LIST((INT4 ,INT4 *));



/* Low Level TEST Routines  */

extern INT1
nmhTestv2FsIsisExtRestartSupport ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtGRRestartTimeInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtGRT2TimeIntervalLevel1 ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtGRT2TimeIntervalLevel2 ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtGRT1TimeInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtGRT1RetryCount ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtRestartReason ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtHelperSupport ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsIsisExtHelperGraceTimeLimit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));



/* Low Level SET Routine  */

extern INT1
nmhSetFsIsisExtRestartSupport ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtGRRestartTimeInterval ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtGRT2TimeIntervalLevel1 ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtGRT2TimeIntervalLevel2 ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtGRT1TimeInterval ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtGRT1RetryCount ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtRestartReason ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtHelperSupport ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsIsisExtHelperGraceTimeLimit ARG_LIST((INT4  ,INT4 ));

#endif/* ISIS_WANTED*/

#ifdef PNAC_WANTED
extern INT1
nmhGetFsRadExtDebugMask ARG_LIST((INT4 *));

extern INT1
nmhGetFsRadExtMaxNoOfUserEntries ARG_LIST((INT4 *));

extern INT1
nmhGetFsRadExtPrimaryServerAddressType ARG_LIST((INT4 *));

extern INT1
nmhGetFsRadExtPrimaryServer ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsRadExtDebugMask ARG_LIST((INT4 ));

extern INT1
nmhSetFsRadExtMaxNoOfUserEntries ARG_LIST((INT4 ));

extern INT1
nmhSetFsRadExtPrimaryServerAddressType ARG_LIST((INT4 ));

extern INT1
nmhSetFsRadExtPrimaryServer ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsRadExtDebugMask ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhTestv2FsRadExtMaxNoOfUserEntries ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhTestv2FsRadExtPrimaryServerAddressType ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhTestv2FsRadExtPrimaryServer ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2FsRadExtDebugMask ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsRadExtMaxNoOfUserEntries ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsRadExtPrimaryServerAddressType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsRadExtPrimaryServer ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRadExtServerTable. */
extern INT1
nmhValidateIndexInstanceFsRadExtServerTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRadExtServerTable  */

extern INT1
nmhGetFirstIndexFsRadExtServerTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsRadExtServerTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsRadExtServerAddrType ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsRadExtServerAddress ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsRadExtServerType ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsRadExtServerSharedSecret ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsRadExtServerEnabled ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsRadExtServerResponseTime ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsRadExtServerMaximumRetransmission ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsRadExtServerEntryStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsRadExtServerAddrType ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsRadExtServerAddress ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsRadExtServerType ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsRadExtServerSharedSecret ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsRadExtServerEnabled ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsRadExtServerResponseTime ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsRadExtServerMaximumRetransmission ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsRadExtServerEntryStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsRadExtServerAddrType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsRadExtServerAddress ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2FsRadExtServerType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsRadExtServerSharedSecret ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2FsRadExtServerEnabled ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsRadExtServerResponseTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsRadExtServerMaximumRetransmission ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2FsRadExtServerEntryStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2FsRadExtServerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsRadExtAuthClientInvalidServerAddresses ARG_LIST((UINT4 *));

extern INT1
nmhGetFsRadExtAuthClientIdentifier ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsRadExtAuthServerTable. */
extern INT1
nmhValidateIndexInstanceFsRadExtAuthServerTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRadExtAuthServerTable  */

extern INT1
nmhGetFirstIndexFsRadExtAuthServerTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsRadExtAuthServerTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsRadExtAuthServerAddressType ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsRadExtAuthServerAddress ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));


/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsRadExtAuthClientServerPortNumber ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsRadExtAuthClientServerPortNumber ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2FsRadExtAuthServerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsRadExtAccClientInvalidServerAddresses ARG_LIST((UINT4 *));

extern INT1
nmhGetFsRadExtAccClientIdentifier ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsRadExtAccServerTable. */
extern INT1
nmhValidateIndexInstanceFsRadExtAccServerTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRadExtAccServerTable  */

extern INT1
nmhGetFirstIndexFsRadExtAccServerTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsRadExtAccServerTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsRadExtAccServerAddressType ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsRadExtAccServerAddress ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsRadExtAccClientServerPortNumber ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsRadExtAccClientRoundTripTime ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsRadExtAccClientRequests ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsRadExtAccClientRetransmissions ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsRadExtAccClientResponses ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsRadExtAccClientMalformedResponses ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsRadExtAccClientBadAuthenticators ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsRadExtAccClientPendingRequests ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsRadExtAccClientTimeouts ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsRadExtAccClientUnknownTypes ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsRadExtAccClientPacketsDropped ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsRadExtAccClientServerPortNumber ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsRadExtAccClientServerPortNumber ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRadExtAccServerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
#endif /*PNAC_WANTED*/

#ifdef OSPF3_WANTED
VOID IssProcessOspf3RedStatsPage PROTO ((tHttp * pHttp));
VOID IssProcessOspf3RedPageGet PROTO ((tHttp * pHttp));
#endif
VOID IssProcessEntPhysicalPage PROTO ((tHttp * ));
VOID IssProcessEntPhysicalPageSet PROTO ((tHttp * ));
VOID IssProcessEntPhysicalPageGet PROTO ((tHttp * ));
VOID IssProcessEntLPMappingPage PROTO ((tHttp * ));
VOID IssProcessEntLPMappingPageGet PROTO ((tHttp * ));
VOID IssProcessEntAliasMappingPage PROTO ((tHttp * ));
VOID IssProcessEntAliasMappingPageGet PROTO ((tHttp * ));
VOID IssProcessEntContainsPage PROTO ((tHttp * ));
VOID IssProcessEntContainsPageGet PROTO ((tHttp * ));

extern INT1
nmhGetFirstIndexEntLPMappingTable ARG_LIST((INT4 * , INT4 *));

extern INT1
nmhGetNextIndexEntLPMappingTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));
extern INT1
nmhGetNextIndexEntAliasMappingTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));
extern INT1
nmhGetEntAliasMappingIdentifier ARG_LIST((INT4  , INT4 ,tSNMP_OID_TYPE * ));
extern INT1
nmhGetNextIndexEntPhysicalContainsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));
extern INT1
nmhGetNextIndexEntPhysicalTable ARG_LIST((INT4 , INT4 *));
extern INT1
nmhGetEntPhysicalDescr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetEntPhysicalVendorType ARG_LIST((INT4 ,tSNMP_OID_TYPE * ));

extern INT1
nmhGetEntPhysicalContainedIn ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetEntPhysicalClass ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetEntPhysicalParentRelPos ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetEntPhysicalName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetEntPhysicalHardwareRev ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetEntPhysicalFirmwareRev ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetEntPhysicalSoftwareRev ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetEntPhysicalSerialNum ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetEntPhysicalMfgName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetEntPhysicalModelName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetEntPhysicalAlias ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetEntPhysicalAssetID ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetEntPhysicalIsFRU ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetEntPhysicalMfgDate ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetEntPhysicalUris ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhSetEntPhysicalSerialNum ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetEntPhysicalAlias ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetEntPhysicalAssetID ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetEntPhysicalUris ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2EntPhysicalSerialNum ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2EntPhysicalAlias ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2EntPhysicalAssetID ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2EntPhysicalUris ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));


#ifdef OSPF_WANTED


extern INT4 nmhGetFirstIndexFsMIStdOspfLsdbTable ARG_LIST((INT4 * , UINT4 * , INT4 * , UINT4 * , UINT4 *));
extern INT4 nmhGetFsMIStdOspfLsdbSequence ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));
extern INT4 nmhGetFsMIStdOspfLsdbChecksum ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));
extern INT4 nmhGetFsMIStdOspfLsdbAge ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));
extern INT4 nmhGetNextIndexFsMIStdOspfLsdbTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));
extern INT4 nmhGetFirstIndexFsMIOspfRoutingTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , INT4 * , UINT4 *));
extern INT4 nmhGetFsMIOspfRouteType ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));
extern INT4 nmhGetFsMIOspfRouteAreaId ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,UINT4 *));
extern INT4 nmhGetFsMIOspfRouteCost ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));
extern INT4 nmhGetFsMIOspfRouteType2Cost ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));
extern INT4 nmhGetFsMIOspfRouteInterfaceIndex ARG_LIST((INT4  , UINT4  , UINT4  , INT4  , UINT4 ,INT4 *));
extern INT4 nmhGetNextIndexFsMIOspfRoutingTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

extern INT4 nmhGetFirstIndexFsMIStdOspfTable ARG_LIST((INT4 *));
extern INT4       UtilOspfGetVcmAliasName PROTO ((UINT4 u4ContextId, UINT1 *pu1Alias));
extern INT4 nmhGetFsMIStdOspfAdminStat ARG_LIST((INT4 ,INT4 *));
extern INT4 nmhGetNextIndexFsMIStdOspfTable ARG_LIST((INT4 , INT4 *));
extern INT4 nmhSetFsMIStdOspfAdminStat ARG_LIST((INT4  ,INT4 ));
#ifndef MI_WANTED
extern INT1 nmhGetFsMIStdOspfStatus ARG_LIST((INT4 ,INT4 *));
extern INT4 nmhTestv2FsMIStdOspfStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfStatus ARG_LIST((INT4  ,INT4 ));
#endif
extern INT4 nmhTestv2FsMIStdOspfAdminStat ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT4 nmhGetFsMIStdOspfRouterId ARG_LIST((INT4 ,UINT4 *));
extern INT4 nmhGetFsMIStdOspfASBdrRtrStatus ARG_LIST((INT4 ,INT4 *));
extern INT4 nmhGetFsMIOspfRFC1583Compatibility ARG_LIST((INT4 ,INT4 *));
extern INT4 nmhGetFsMIOspfNssaAsbrDefRtTrans ARG_LIST((INT4 ,INT4 *));
extern INT4 nmhGetFsMIOspfABRType ARG_LIST((INT4 ,INT4 *));
extern INT4 OspfCliSetAdminStatInCxt PROTO ((tCliHandle CliHandle, INT4 i4AdminStat, UINT4 u4OspfCxtId));
extern INT4 OspfCliSetDefaultPassive PROTO ((tCliHandle CliHandle, INT4 i4AdminStat));
extern INT4 nmhTestv2FsMIStdOspfRouterId ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));
extern INT4 nmhSetFsMIStdOspfRouterId ARG_LIST((INT4  ,UINT4 ));
extern INT4 nmhTestv2FsMIStdOspfASBdrRtrStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfASBdrRtrStatus ARG_LIST((INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfRFC1583Compatibility ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfRFC1583Compatibility ARG_LIST((INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfNssaAsbrDefRtTrans ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfNssaAsbrDefRtTrans ARG_LIST((INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfABRType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfABRType ARG_LIST((INT4  ,INT4 ));
extern INT4 nmhGetFirstIndexFsMIStdOspfAreaTable ARG_LIST((INT4 * , UINT4 *));
extern INT4 nmhGetNextIndexFsMIStdOspfAreaTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));
extern INT4 nmhGetFirstIndexFsMIStdOspfIfTable ARG_LIST((INT4 * , UINT4 * , INT4 *));
extern INT4 nmhGetFsMIStdOspfIfAreaId ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));
extern INT4 nmhGetFsMIStdOspfIfRtrPriority ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));
extern INT4 nmhGetFsMIStdOspfIfDesignatedRouter ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));
extern INT4 nmhGetFsMIStdOspfIfAuthType ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));
extern INT4 nmhGetFsMIStdOspfIfCryptoAuthType ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));
extern INT4 nmhGetFsMIStdOspfVirtIfCryptoAuthType ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));
extern INT4 nmhGetFsMIStdOspfIfMetricValue ARG_LIST((INT4  , UINT4  , INT4  , INT4 ,INT4 *));
extern INT4 nmhGetFsMIOspfIfPassive ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));
extern INT4 nmhGetFsMIStdOspfIfDemand ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));
extern INT4 nmhGetFsMIStdOspfIfType ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));
extern INT4 nmhGetFsMIStdOspfIfTransitDelay ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));
extern INT4 nmhGetFsMIStdOspfIfRetransInterval ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));
extern INT4 nmhGetFsMIStdOspfIfHelloInterval ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));
extern INT4 nmhGetFsMIStdOspfIfRtrDeadInterval ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));
extern INT4 nmhGetNextIndexFsMIStdOspfIfTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));
extern INT4 nmhTestv2FsMIStdOspfIfStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfIfStatus ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhGetFsMIStdOspfIfStatus ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));
extern INT4 nmhTestv2FsMIStdOspfIfAreaId ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,UINT4 ));
extern INT4 nmhSetFsMIStdOspfIfAreaId ARG_LIST((INT4  , UINT4  , INT4  ,UINT4 ));
extern INT4 nmhTestv2FsMIStdOspfIfRtrPriority ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfIfRtrPriority ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfIfMD5AuthKey ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT4 nmhTestv2FsMIOspfIfMD5AuthKeyStartAccept ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfIfMD5AuthKeyStartGenerate ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfIfMD5AuthKeyStopGenerate ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfIfMD5AuthKeyStopAccept ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfIfAuthKeyStartAccept ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));
extern INT4 nmhTestv2FsMIOspfIfAuthKeyStartGenerate ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));
extern INT4 nmhTestv2FsMIOspfIfAuthKeyStopGenerate ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));
extern INT4 nmhTestv2FsMIOspfIfAuthKeyStopAccept ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));
extern INT4 nmhSetFsMIOspfIfMD5AuthKey ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT4 nmhSetFsMIOspfIfMD5AuthKeyStartAccept ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfIfMD5AuthKeyStartGenerate ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfIfMD5AuthKeyStopGenerate ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfIfMD5AuthKeyStopAccept ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfIfAuthKeyStartAccept ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));
extern INT4 nmhSetFsMIOspfIfAuthKeyStartGenerate ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));
extern INT4 nmhSetFsMIOspfIfAuthKeyStopGenerate ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));
extern INT4 nmhSetFsMIOspfIfAuthKeyStopAccept ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE * ));
extern INT4 nmhTestv2FsMIOspfIfMD5AuthKeyStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfIfMD5AuthKeyStatus ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIStdOspfIfAuthType ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIStdOspfIfCryptoAuthType ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIStdOspfVirtIfCryptoAuthType ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfIfAuthType ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfIfCryptoAuthType ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfVirtIfCryptoAuthType ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIStdOspfIfAuthKey ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT4 nmhSetFsMIStdOspfIfAuthKey ARG_LIST((INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT4 nmhGetFsMIStdOspfIfMetricStatus ARG_LIST((INT4  , UINT4  , INT4  , INT4 ,INT4 *));
extern INT4 nmhTestv2FsMIStdOspfIfMetricStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfIfMetricStatus ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIStdOspfIfMetricValue ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfIfMetricValue ARG_LIST((INT4  , UINT4  , INT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfIfPassive ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfIfPassive ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIStdOspfDemandExtensions ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfDemandExtensions ARG_LIST((INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIStdOspfIfDemand ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfIfDemand ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIStdOspfIfType ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfIfType ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIStdOspfIfTransitDelay ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfIfTransitDelay ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIStdOspfIfRetransInterval ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfIfRetransInterval ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIStdOspfIfHelloInterval ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfIfHelloInterval ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIStdOspfIfRtrDeadInterval ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfIfRtrDeadInterval ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhGetFirstIndexFsMIOspfAreaTable ARG_LIST((INT4 * , UINT4 *));
extern INT4 nmhGetFsMIStdOspfImportAsExtern ARG_LIST((INT4  , UINT4 ,INT4 *));
extern INT4 nmhGetFsMIStdOspfAreaSummary ARG_LIST((INT4  , UINT4 ,INT4 *));
extern INT4 nmhGetFsMIStdOspfStubMetric ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));
extern INT4 nmhGetFsMIStdOspfStubMetricType ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));
extern INT4 nmhGetFsMIOspfAreaNSSATranslatorStabilityInterval ARG_LIST((INT4  , UINT4 ,INT4 *));
extern INT4 nmhGetNextIndexFsMIOspfAreaTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));
extern INT4 nmhTestv2FsMIStdOspfAreaStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfAreaStatus ARG_LIST((INT4  , UINT4  ,INT4 ));
extern INT4 nmhGetFsMIStdOspfAreaStatus ARG_LIST((INT4  , UINT4 ,INT4 *));
extern INT4 nmhTestv2FsMIStdOspfImportAsExtern ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfImportAsExtern ARG_LIST((INT4  , UINT4  ,INT4 ));
extern INT4 nmhTestv2FsMIStdOspfAreaSummary ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfAreaSummary ARG_LIST((INT4  , UINT4  ,INT4 ));
extern INT4 nmhTestv2FsMIStdOspfStubMetric ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIStdOspfStubMetricType ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfStubMetric ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfStubMetricType ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIStdOspfStubStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfStubStatus ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfAreaNSSATranslatorRole ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfAreaNSSATranslatorRole ARG_LIST((INT4  , UINT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfAreaNSSATranslatorStabilityInterval ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfAreaNSSATranslatorStabilityInterval ARG_LIST((INT4  , UINT4  ,INT4 ));
extern INT4 nmhGetFirstIndexFsMIStdOspfVirtIfTable ARG_LIST((INT4 * , UINT4 * , UINT4 *));
extern INT4 nmhGetFsMIStdOspfVirtIfAuthType ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));
extern INT4 nmhGetFsMIStdOspfVirtIfHelloInterval ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));
extern INT4 nmhGetFsMIStdOspfVirtIfRtrDeadInterval ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));
extern INT4 nmhGetFsMIStdOspfVirtIfTransitDelay ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));
extern INT4 nmhGetFsMIStdOspfVirtIfRetransInterval ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));
extern INT4 nmhGetNextIndexFsMIStdOspfVirtIfTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));
extern INT1 nmhGetNextIndexFsMIOspfVirtIfMD5AuthTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));
extern INT4 nmhTestv2FsMIStdOspfVirtIfStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfVirtIfStatus ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhGetFsMIStdOspfVirtIfStatus ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));
extern INT4 nmhTestv2FsMIOspfVirtIfMD5AuthKey ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT4 nmhTestv2FsMIOspfVirtIfMD5AuthKeyStartAccept ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfVirtIfMD5AuthKeyStartGenerate ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfVirtIfMD5AuthKeyStopGenerate ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfVirtIfMD5AuthKeyStopAccept ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,INT4 ));
extern INT1 nmhGetFsMIOspfVirtIfMD5AuthKey ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT4 nmhSetFsMIOspfVirtIfMD5AuthKey ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT4 nmhSetFsMIOspfVirtIfMD5AuthKeyStartAccept ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfVirtIfMD5AuthKeyStartGenerate ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfVirtIfMD5AuthKeyStopGenerate ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfVirtIfMD5AuthKeyStopAccept ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfVirtIfMD5AuthKeyStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfVirtIfMD5AuthKeyStatus ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIStdOspfVirtIfAuthType ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfVirtIfAuthType ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhTestv2FsMIStdOspfVirtIfAuthKey ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT4 nmhSetFsMIStdOspfVirtIfAuthKey ARG_LIST((INT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT4 nmhTestv2FsMIStdOspfVirtIfHelloInterval ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfVirtIfHelloInterval ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhTestv2FsMIStdOspfVirtIfRtrDeadInterval ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfVirtIfRtrDeadInterval ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhTestv2FsMIStdOspfVirtIfTransitDelay ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfVirtIfTransitDelay ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhTestv2FsMIStdOspfVirtIfRetransInterval ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfVirtIfRetransInterval ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhGetFirstIndexFsMIStdOspfNbrTable ARG_LIST((INT4 * , UINT4 * , INT4 *));
extern INT4 nmhGetFsMIStdOspfNbrPriority ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));
extern INT4 nmhGetNextIndexFsMIStdOspfNbrTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));
extern INT4 nmhTestv2FsMIStdOspfNbmaNbrStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfNbmaNbrStatus ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhGetFsMIStdOspfNbmaNbrStatus ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));
extern INT4 nmhTestv2FsMIStdOspfNbrPriority ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfNbrPriority ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));
extern INT4 nmhGetFirstIndexFsMIOspfRRDRouteConfigTable ARG_LIST((INT4 * , UINT4 * , UINT4 *));
extern INT4 nmhGetFsMIOspfRRDRouteMetric ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));
extern INT4 nmhGetFsMIOspfRRDRouteMetricType ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));
extern INT4 nmhGetFsMIOspfRRDRouteTagType ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));
extern INT4 nmhGetFsMIOspfRRDRouteTag ARG_LIST((INT4  , UINT4  , UINT4 ,UINT4 *));
extern INT4 nmhGetNextIndexFsMIOspfRRDRouteConfigTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));
extern INT4 nmhTestv2FsMIOspfRRDRouteStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfRRDRouteStatus ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhGetFsMIOspfRRDRouteStatus ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));
extern INT4 nmhTestv2FsMIOspfRRDRouteMetric ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfRRDRouteMetric ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfRRDRouteMetricType ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfRRDRouteMetricType ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfRRDRouteTagType ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfRRDRouteTag ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,UINT4 ));
extern INT4 nmhSetFsMIOspfRRDRouteTagType ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfRRDRouteTag ARG_LIST((INT4  , UINT4  , UINT4  ,UINT4 ));
extern INT4 nmhGetFirstIndexFsMIOspfAreaAggregateTable ARG_LIST((INT4 * , UINT4 * , INT4 * , UINT4 * , UINT4 *));
extern INT4 nmhGetFsMIStdOspfAreaAggregateEffect ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));
extern INT4 nmhGetFsMIOspfAreaAggregateExternalTag ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));
extern INT4 nmhGetNextIndexFsMIOspfAreaAggregateTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));
extern INT4 nmhTestv2FsMIStdOspfAreaAggregateStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfAreaAggregateStatus ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhGetFsMIStdOspfAreaAggregateStatus ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4 ,INT4 *));
extern INT4 nmhTestv2FsMIStdOspfAreaAggregateEffect ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhSetFsMIStdOspfAreaAggregateEffect ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfAreaAggregateExternalTag ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfAreaAggregateExternalTag ARG_LIST((INT4  , UINT4  , INT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhGetFsMIOspfOpaqueOption ARG_LIST((INT4 ,INT4 *));
extern INT4 nmhGetFsMIOspfRestartSupport ARG_LIST((INT4 ,INT4 *));
extern INT4 nmhGetFsMIOspfRestartAckState ARG_LIST((INT4 ,INT4 *));
extern INT4 nmhGetFsMIOspfGraceLsaRetransmitCount ARG_LIST((INT4 ,INT4 *));
extern INT4 nmhGetFsMIOspfRestartInterval ARG_LIST((INT4 ,INT4 *));
extern INT4 nmhGetFsMIOspfRestartReason ARG_LIST((INT4 ,INT4 *));
extern INT4 nmhGetFsMIOspfHelperSupport ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT4 nmhGetFsMIOspfRestartStrictLsaChecking ARG_LIST((INT4 ,INT4 *));
extern INT4 nmhGetFsMIOspfHelperGraceTimeLimit ARG_LIST((INT4 ,INT4 *));
extern INT4 nmhTestv2FsMIOspfOpaqueOption ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfOpaqueOption ARG_LIST((INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfRestartSupport ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfRestartSupport ARG_LIST((INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfRestartAckState ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfRestartAckState ARG_LIST((INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfGraceLsaRetransmitCount ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfGraceLsaRetransmitCount ARG_LIST((INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfRestartInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfRestartInterval ARG_LIST((INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfRestartReason ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfRestartReason ARG_LIST((INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfHelperSupport ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT4 nmhSetFsMIOspfHelperSupport ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT4 nmhTestv2FsMIOspfRestartStrictLsaChecking ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfRestartStrictLsaChecking ARG_LIST((INT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfHelperGraceTimeLimit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfHelperGraceTimeLimit ARG_LIST((INT4  ,INT4 ));
extern INT4 nmhGetFirstIndexFsMIOspfAsExternalAggregationTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , UINT4 *));
extern INT4 nmhGetFsMIOspfAsExternalAggregationEffect ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));
extern INT4 nmhGetFsMIOspfAsExternalAggregationTranslation ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));
extern INT4 nmhGetNextIndexFsMIOspfAsExternalAggregationTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));
extern INT4 nmhTestv2FsMIOspfAsExternalAggregationStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfAsExternalAggregationStatus ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhGetFsMIOspfAsExternalAggregationStatus ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));
extern INT4 nmhTestv2FsMIOspfAsExternalAggregationEffect ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhTestv2FsMIOspfAsExternalAggregationTranslation ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfAsExternalAggregationEffect ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT4 nmhSetFsMIOspfAsExternalAggregationTranslation ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  ,INT4 ));
extern INT1 nmhGetNextIndexFsMIOspfIfMD5AuthTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));extern INT1 nmhGetFsMIOspfIfMD5AuthKey ARG_LIST((INT4  , UINT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFutOspfRRDSrcProtoMaskEnable (INT4 *);
extern INT1 nmhGetFsMIOspfRRDMetricValue(INT4 i4FsMIStdOspfContextId , INT4 i4FsMIOspfRRDProtocolId , INT4 *pi4RetValFsMIOspfRRDMetricValue);
extern INT1 nmhGetFsMIOspfRRDMetricType(INT4 i4FsMIStdOspfContextId , INT4 i4FsMIOspfRRDProtocolId , INT4 *pi4RetValFsMIOspfRRDMetricType);
extern INT1 nmhGetFutOspfRRDStatus(INT4 *);
extern INT1 nmhGetFutOspfRRDRouteMapEnable(tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhSetFutOspfRRDRouteMapEnable(tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhSetFutOspfRRDSrcProtoMaskDisable(INT4);
extern INT1 nmhSetFutOspfRRDStatus(INT4);
extern INT1 nmhSetFutOspfRRDSrcProtoMaskEnable(INT4);
extern INT1 nmhSetFsMIOspfRRDMetricValue(INT4 i4FsMIStdOspfContextId , INT4 i4FsMIOspfRRDProtocolId , INT4 i4SetValFsMIOspfRRDMetricValue);
extern INT1 nmhSetFsMIOspfRRDMetricType(INT4 i4FsMIStdOspfContextId , INT4 i4FsMIOspfRRDProtocolId , INT4 i4SetValFsMIOspfRRDMetricValue);
extern INT1 nmhTestv2FutOspfRRDStatus(UINT4 *, INT4);
extern INT1 nmhTestv2FutOspfRRDSrcProtoMaskEnable(UINT4 *, INT4);
extern INT1 nmhTestv2FsMIOspfRRDMetricValue(UINT4 *pu4ErrorCode , INT4 i4FsMIStdOspfContextId , INT4 i4FsMIOspfRRDProtocolId , INT4 i4TestValFsMIOspfRRDMetricValue);
extern INT1 nmhTestv2FsMIOspfRRDMetricType(UINT4 *pu4ErrorCode , INT4 i4FsMIStdOspfContextId , INT4 i4FsMIOspfRRDProtocolId , INT4 i4TestValFsMIOspfRRDMetricType);
extern INT1 nmhTestv2FutOspfRRDSrcProtoMaskDisable(UINT4 *, INT4);
extern INT1 nmhTestv2FutOspfRRDRouteMapEnable(UINT4 *, tSNMP_OCTET_STRING_TYPE *);


#endif

extern INT4      UtilRtm6SetContext PROTO ((UINT4 u4Rtm6CxtId));
extern VOID      UtilRtm6ResetContext PROTO ((VOID));
extern INT4 FsRMapRowStatusSet PROTO ((tSnmpIndex * , tRetVal * ));
extern INT4 FsRMapIsIpPrefixListSet PROTO ((tSnmpIndex * , tRetVal * ));
extern INT4 FsRMapAccessSet PROTO ((tSnmpIndex *, tRetVal *));
extern INT4 FsRMapMatchRowStatusSet PROTO ((tSnmpIndex *, tRetVal *));
extern INT4 FsRMapSetRowStatusSet PROTO ((tSnmpIndex *, tRetVal *));

extern INT1
nmhSetFsMIStdInetCidrRouteStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhGetFsMIStdInetCidrRouteStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhTestv2FsMIStdInetCidrRouteStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsMIStdInetCidrRouteIfIndex ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2FsMIStdInetCidrRouteIfIndex ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2FsMIStdInetCidrRouteMetric1 ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhGetFsMIStdInetCidrRouteMetric1 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhSetFsMIStdInetCidrRouteMetric1 ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhGetFsMIStdInetCidrRouteAddrType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetNextIndexFsMIStdInetCidrRouteTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , tSNMP_OID_TYPE *, tSNMP_OID_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsMIStdInetCidrRouteProto ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsMIStdInetCidrRouteIfIndex ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OID_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
#ifdef OSPF3_WANTED
extern INT1 nmhGetFutOspfv3HotStandbyAdminStatus ARG_LIST((INT4 *));
extern INT1 nmhGetFutOspfv3HotStandbyState ARG_LIST((INT4 *));
extern INT1 nmhGetFutOspfv3DynamicBulkUpdStatus ARG_LIST((INT4 *));
extern INT1 nmhGetFutOspfv3StandbyHelloSyncCount ARG_LIST((UINT4 *));
extern INT1 nmhGetFutOspfv3StandbyLsaSyncCount ARG_LIST((UINT4 *));
#endif

VOID IssProcessPortIsolationPage (tHttp * pHttp);
VOID
IssProcessPortIsolationPageGet (tHttp  *pHttp);
VOID
IssProcessPortIsolationPageSet (tHttp  *pHttp);
VOID
IssProcessIvrPvlanMappingPage (tHttp * pHttp);
VOID
IssProcessIvrPvlanMappingPageGet (tHttp * pHttp);
VOID
IssProcessIvrPvlanMappingPageSet (tHttp * pHttp);

#ifdef GARP_WANTED
VOID
IssProcessVlanGarpClearStatsPage (tHttp * pHttp);
extern INT4 GarpVcmGetContextInfoFromIfIndex (UINT4 ,UINT4 *, UINT2 *);
extern INT1 nmhSetFsMIDot1qFutureVlanPortClearGarpStats (INT4  ,INT4 );
extern INT1 nmhTestv2FsMIDot1qFutureVlanPortClearGarpStats (UINT4 *  ,INT4  ,INT4 );
#endif

VOID
IssProcessSplitHorizonConfigPage (tHttp * pHttp);
VOID
IssProcessSplitHorizonConfigPageGet (tHttp * pHttp);
VOID
IssProcessSplitHorizonConfigPageSet (tHttp * pHttp);

VOID 
IssProcessCfaPortRolePage (tHttp * pHttp);
VOID
IssProcessCfaPortRolePageGet(tHttp * pHttp);
VOID
IssProcessCfaPortRolePageSet (tHttp * pHttp);

VOID IssProcessUfdConfigPage(tHttp * pHttp);
VOID IssProcessUfdConfigPageGet (tHttp * pHttp);
VOID IssProcessUfdConfigPageSet (tHttp * pHttp);
VOID IssProcessUfdGroupConfigPage (tHttp * pHttp);
VOID IssProcessUfdGroupConfigPageGet(tHttp * pHttp);
VOID IssProcessUfdGroupConfigPageSet(tHttp * pHttp);

extern INT1 nmhTestv2Dot1qFutureStVlanPVlanType (UINT4 *, UINT4, INT4);
extern INT1 nmhSetDot1qFutureStVlanPVlanType (UINT4, INT4);
extern INT1 nmhGetDot1qFutureStVlanType (UINT4, INT4*);
extern INT1 nmhSetDot1qFutureStVlanPrimaryVid (UINT4, INT4);
extern INT1 nmhGetDot1qFutureStVlanVid (UINT4, INT4*);
extern INT1 nmhTestv2Dot1qFutureStVlanPrimaryVid (UINT4 *, UINT4, INT4);
extern INT1
nmhGetFirstIndexIssPortIsolationTable ARG_LIST((INT4 * , INT4 * , INT4 *));
extern INT1 
nmhTestv2IssPortIsolationRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,
INT4  ,INT4 ));
extern INT1
nmhGetNextIndexIssPortIsolationTable ARG_LIST((INT4 , INT4 * ,
INT4 , INT4 * , INT4 , INT4 *));
extern INT1
nmhSetIssPortIsolationRowStatus ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));
extern INT1
nmhGetFirstIndexIfIvrMappingTable ARG_LIST ((INT4 *, INT4 *));
extern INT1
nmhGetNextIndexIfIvrMappingTable ARG_LIST ((INT4, INT4 *, INT4, INT4*));
extern INT1
nmhTestv2IfIvrMappingRowStatus ARG_LIST ((UINT4*, INT4, INT4, INT4));
extern INT1
nmhSetIfIvrMappingRowStatus ARG_LIST((INT4, INT4, INT4));
extern INT1
nmhGetIssPortIsolationStorageType (INT4, INT4, INT4, INT4*);
INT1
IssPvlanSetVlanTypeandPrimaryId  (tHttp *,
                                  UINT2 ,
                                  UINT1 ,
                                  UINT2 );
VOID
PvlanWebConvertVlanListToArray (UINT1 *, tPortList);
INT4 PvlanWebConvertStrToPortArray (tPortList PortList, UINT4 *pu4Array);
#ifdef RRD_WANTED
extern INT4 FsRrdRouterIdSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRrdRouterASNumberSet(tSnmpIndex *, tRetVal *);
extern INT4 FsRrdAdminStatusSet(tSnmpIndex *, tRetVal *);
extern INT1
nmhTestv2FsRrdRouterASNumber ARG_LIST((UINT4 *  ,INT4 ));
extern INT1
nmhTestv2FsRrdRouterId ARG_LIST((UINT4 *  ,UINT4 ));
extern INT1
nmhTestv2FsRrdAdminStatus ARG_LIST((UINT4 *  ,INT4 ));
extern INT1
nmhGetFsRrdRouterId ARG_LIST((UINT4 *));
extern INT1
nmhGetFsRrdRouterASNumber ARG_LIST((INT4 *));
extern INT1
nmhGetFsRrdAdminStatus ARG_LIST((INT4 *));
extern INT1
nmhTestv2FsMIRrdForce ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1
nmhGetFsMIRrdForce ARG_LIST((INT4 ,INT4 *));
extern INT1
nmhSetFsMIRrdForce ARG_LIST((INT4  ,INT4 ));

VOID
IssProcessRrdGlobalPage (tHttp * );

VOID
IssProcessRrdGlobalPageGet (tHttp * );

VOID
IssProcessRrdGlobalPageSet (tHttp * );
#endif /*RRD_WANTED*/

#ifdef VPN_WANTED
VOID IssProcessIpSecConfPage (tHttp *pHttp);
VOID IssProcessIpSecConfPageSet (tHttp *pHttp);
VOID IssProcessIkeConfPage (tHttp *pHttp);
VOID IssProcessIkeConfPageSet (tHttp *pHttp);
VOID IssProcessIkeConfPageGet (tHttp *pHttp, UINT1 *pu1PolicyName, UINT1 u1DelFlag);
VOID IssProcessIpSecConfPageGet (tHttp *pHttp, UINT1 *pu1PolicyName, UINT1 u1DelFlag);
VOID IssProcessVpnPolicyConfPage (tHttp *pHttp);
VOID IssProcessVpnPolicyConfPageGet (tHttp *pHttp , UINT1 *pu1VpnPolicy);
VOID IssProcessVpnPolicyConfPageSet (tHttp *pHttp);
VOID IssProcessVpnRaUserDBConfPage (tHttp *pHttp);
VOID IssProcessVpnRaUserDBConfPageGet (tHttp *pHttp);
VOID IssProcessVpnRaUserDBConfPageSet (tHttp *pHttp);
VOID IssProcessVpnRaAddressPoolConfPage (tHttp *pHttp);
VOID IssProcessVpnRaAddressPoolConfPageGet (tHttp *pHttp);
VOID IssProcessVpnRaAddressPoolConfPageSet (tHttp *pHttp);
VOID IssProcessVpnRaClientTerminationConfPage (tHttp *pHttp);
VOID IssProcessVpnRaClientTerminationConfPageGet
      (tHttp *pHttp, UINT1 *pu1PolicyName, UINT1 u1DelFlag);
VOID IssProcessVpnRaClientTerminationConfPageSet (tHttp *pHttp);
VOID IssProcessVpnGlobalConfPage(tHttp *pHttp);
VOID IssProcessVpnGlobalConfPageGet(tHttp *pHttp);
VOID IssProcessVpnGlobalConfPageSet(tHttp *pHttp);

#endif /* VPN_WANTED */
extern INT4 FsSyslogMailRowStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsSyslogMailRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSyslogMailRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);

extern INT4 FsSyslogFwdRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsSyslogFwdRowStatusGet(tSnmpIndex *, tRetVal *);
extern INT4 FsSyslogFwdRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);

#ifdef DNS_WANTED
extern INT1
nmhGetFirstIndexFsDnsNameServerTable ARG_LIST((UINT4 *));
extern INT1
nmhGetNextIndexFsDnsNameServerTable ARG_LIST((UINT4 , UINT4 *));
extern INT1
nmhGetFsDnsServerIPAddressType ARG_LIST((UINT4 ,INT4 *));
extern INT1
nmhGetFsDnsServerIPAddress ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetFsDnsNameServerRowStatus ARG_LIST((UINT4 ,INT4 *));
extern INT1
nmhSetFsDnsServerIPAddressType ARG_LIST((UINT4  ,INT4 ));
extern INT1
nmhSetFsDnsServerIPAddress ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhSetFsDnsNameServerRowStatus ARG_LIST((UINT4  ,INT4 ));
extern INT1
nmhTestv2FsDnsServerIPAddressType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1
nmhTestv2FsDnsServerIPAddress ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2FsDnsNameServerRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT4 FsDnsServerIPAddressTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDnsServerIPAddressSet(tSnmpIndex *, tRetVal *);
extern INT4 FsDnsNameServerRowStatusSet(tSnmpIndex *, tRetVal *);

extern INT1
nmhGetFirstIndexDnsResCacheRRTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , INT4 *));

extern INT1
nmhGetDnsResCacheRRTTL ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetDnsResCacheRRData ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern 
 tDnsCacheRRInfo *
 DnsGetCacheRR (UINT1 *pu1RRName, UINT4 u4RRClass, UINT4 u4RRType,
                               UINT4 u4RRIndex);
extern INT1 
nmhGetFsDnsResCacheRRSource (tSNMP_OCTET_STRING_TYPE * pDnsResCacheRRName,
                             INT4 i4DnsResCacheRRClass,
                             INT4 i4DnsResCacheRRType,
                             INT4 i4DnsResCacheRRIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsDnsResCacheRRSource);

extern INT1
nmhGetFirstIndexFsDnsResCacheRRTable (tSNMP_OCTET_STRING_TYPE *
                                      pDnsResCacheRRName,
                                      INT4 *pi4DnsResCacheRRClass,
                                      INT4 *pi4DnsResCacheRRType,
                                      INT4 *pi4DnsResCacheRRIndex);
extern INT1
nmhGetNextIndexFsDnsResCacheRRTable (tSNMP_OCTET_STRING_TYPE *
                                     pDnsResCacheRRName,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextDnsResCacheRRName,
                                     INT4 i4DnsResCacheRRClass,
                                     INT4 *pi4NextDnsResCacheRRClass,
                                     INT4 i4DnsResCacheRRType,
                                     INT4 *pi4NextDnsResCacheRRType,
                                     INT4 i4DnsResCacheRRIndex,
                                     INT4 *pi4NextDnsResCacheRRIndex);


extern INT1
nmhGetDnsResCacheRRSource ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ,UINT4 *));

extern INT1
nmhGetNextIndexDnsResCacheRRTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

extern INT1
nmhTestv2DnsResCacheStatus ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhSetDnsResCacheStatus ARG_LIST((INT4 ));

extern INT4 DnsResCacheStatusSet(tSnmpIndex *, tRetVal *);

#endif

#ifdef BFD_WANTED
extern INT4
BfdMainTaskLock (VOID);

extern INT4
BfdMainTaskUnLock (VOID);

extern INT1
nmhSetFsMIStdBfdSessRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

extern INT1
nmhSetFsMIStdBfdSessType ARG_LIST((UINT4  , UINT4  ,INT4 ));

extern INT1
nmhSetFsMIStdBfdSessInterface ARG_LIST((UINT4  , UINT4  ,INT4 ));

extern INT1
nmhSetFsMIStdBfdSessDstAddrType ARG_LIST((UINT4  , UINT4  ,INT4 ));

extern INT1
nmhSetFsMIStdBfdSessDstAddr ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsMIBfdSessMapType ARG_LIST((UINT4  , UINT4  ,INT4 ));

extern INT1
nmhGetFirstIndexFsMIBfdSessionTable ARG_LIST((UINT4 * , UINT4 *));

extern INT1
nmhGetNextIndexFsMIBfdSessionTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

extern INT1
nmhGetFsMIBfdSessMapType ARG_LIST((UINT4  , UINT4 ,INT4 *));

extern INT1
nmhGetFsMIStdBfdSessDstAddr ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsMIStdBfdSessInterface ARG_LIST((UINT4  , UINT4 ,INT4 *));

extern INT1
nmhGetFsMIStdBfdSessRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

extern INT1
nmhGetFsMIStdBfdSessState ARG_LIST((UINT4  , UINT4 ,INT4 *));
#endif

#ifdef MSDP_WANTED
extern INT4
MsdpMainTaskLock (VOID);
#define MSDP_LOCK MsdpMainTaskLock
extern INT4
MsdpMainTaskUnLock (VOID);
#define MSDP_UNLOCK MsdpMainTaskUnLock

#define MSDP_MAX_NAME_LEN 64
extern INT1
nmhGetFirstIndexFsMsdpPeerTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetNextIndexFsMsdpPeerTable ARG_LIST((INT4 , INT4 * , 
                       tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsMsdpPeerState ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsMsdpPeerLocalAddress ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,
                                      tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsMsdpPeerRPFFailures ARG_LIST((INT4  , 
                                      tSNMP_OCTET_STRING_TYPE *,UINT4 *));

extern INT1
nmhGetFsMsdpPeerInSAs ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

extern INT1
nmhGetFsMsdpPeerOutSAs ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

extern INT1
nmhGetFsMsdpPeerInSARequests ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,
                                      UINT4 *));

extern INT1
nmhGetFsMsdpPeerOutSARequests ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,
                                       UINT4 *));

extern INT1
nmhGetFsMsdpPeerConnectRetryInterval ARG_LIST((INT4  , 
                                            tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsMsdpPeerHoldTimeConfigured ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,
                                            INT4 *));

extern INT1
nmhGetFsMsdpPeerKeepAliveConfigured ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,
INT4 *));

extern INT1
nmhGetFsMsdpPeerDataTtl ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhTestv2FsMsdpPeerLocalAddress ARG_LIST((UINT4 * , INT4  , 
                     tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2FsMsdpPeerConnectRetryInterval ARG_LIST((UINT4 * , INT4  , 
                                           tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2FsMsdpPeerHoldTimeConfigured ARG_LIST((UINT4 * , INT4  , 
                                          tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2FsMsdpPeerKeepAliveConfigured ARG_LIST((UINT4 * , INT4  , 
                                           tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2FsMsdpPeerDataTtl ARG_LIST((UINT4 * , INT4  , 
                                          tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2FsMsdpPeerStatus ARG_LIST((UINT4 * , INT4  , 
                                           tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1
nmhSetFsMsdpPeerLocalAddress ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,
                                       tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsMsdpPeerConnectRetryInterval ARG_LIST((INT4  , 
                                           tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsMsdpPeerHoldTimeConfigured ARG_LIST((INT4  , 
                                           tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsMsdpPeerKeepAliveConfigured ARG_LIST((INT4  , 
                                           tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsMsdpPeerDataTtl ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsMsdpPeerStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhGetFirstIndexFsMsdpRPTable ARG_LIST((INT4 *));

extern INT1
nmhGetNextIndexFsMsdpRPTable ARG_LIST((INT4 , INT4 *));

extern INT1
nmhGetFsMsdpRPStatus ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsMsdpRPOperStatus ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsMsdpRPAddress ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhTestv2FsMsdpRPAddress ARG_LIST((UINT4 * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2FsMsdpRPStatus ARG_LIST((UINT4 * , INT4  ,INT4 ));

extern INT1
nmhSetFsMsdpRPAddress ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsMsdpRPStatus ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhGetFsMsdpMeshGroupStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , 
                                      tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhTestv2FsMsdpMeshGroupStatus ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , 
                                  INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsMsdpMeshGroupStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , 
                                      tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhGetFirstIndexFsMsdpMeshGroupTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , 
                                      INT4 * , tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetNextIndexFsMsdpMeshGroupTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, 
                        tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , 
                        tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFirstIndexFsMsdpPeerFilterTable ARG_LIST((INT4 *));

extern INT1
nmhGetNextIndexFsMsdpPeerFilterTable ARG_LIST((INT4 , INT4 *));

extern INT1
nmhGetFsMsdpPeerFilterRouteMap ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsMsdpPeerFilterStatus ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhTestv2FsMsdpPeerFilterRouteMap ARG_LIST((UINT4 * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2FsMsdpPeerFilterStatus ARG_LIST((UINT4 * , INT4  ,INT4 ));

extern INT1
nmhSetFsMsdpPeerFilterRouteMap ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsMsdpPeerFilterStatus ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhGetFirstIndexFsMsdpSARedistributionTable ARG_LIST((INT4 *));

extern INT1
nmhGetNextIndexFsMsdpSARedistributionTable ARG_LIST((INT4 , INT4 *));

extern INT1
nmhGetFsMsdpSARedistributionStatus ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsMsdpSARedistributionRouteMap ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsMsdpSARedistributionRouteMapStat ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhSetFsMsdpSARedistributionStatus ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsMsdpSARedistributionRouteMap ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsMsdpSARedistributionRouteMapStat ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhTestv2FsMsdpSARedistributionStatus ARG_LIST((UINT4 * , INT4  ,INT4 ));

extern INT1
nmhTestv2FsMsdpSARedistributionRouteMap ARG_LIST((UINT4 * , INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2FsMsdpSARedistributionRouteMapStat ARG_LIST((UINT4 * , INT4  ,INT4 ));

extern INT1
nmhGetFsMsdpTraceLevel ARG_LIST((INT4 *));

extern INT1
nmhGetFsMsdpIPv4AdminStat ARG_LIST((INT4 *));

extern INT1
nmhGetFsMsdpIPv6AdminStat ARG_LIST((INT4 *));

extern INT1
nmhGetFsMsdpCacheLifetime ARG_LIST((UINT4 *));

extern INT1
nmhGetFsMsdpNumSACacheEntries ARG_LIST((UINT4 *));

extern INT1
nmhGetFsMsdpMaxPeerSessions ARG_LIST((INT4 *));

extern INT1
nmhGetFsMsdpMappingComponentId ARG_LIST((INT4 *));

extern INT1
nmhGetFsMsdpListenerPort ARG_LIST((INT4 *));

extern INT1
nmhGetFsMsdpPeerFilter ARG_LIST((INT4 *));

extern INT1
nmhSetFsMsdpTraceLevel ARG_LIST((INT4 ));

extern INT1
nmhSetFsMsdpIPv4AdminStat ARG_LIST((INT4 ));

extern INT1
nmhSetFsMsdpIPv6AdminStat ARG_LIST((INT4 ));

extern INT1
nmhSetFsMsdpCacheLifetime ARG_LIST((UINT4 ));

extern INT1
nmhSetFsMsdpMaxPeerSessions ARG_LIST((INT4 ));

extern INT1
nmhSetFsMsdpMappingComponentId ARG_LIST((INT4 ));

extern INT1
nmhSetFsMsdpListenerPort ARG_LIST((INT4 ));

extern INT1
nmhSetFsMsdpPeerFilter ARG_LIST((INT4 ));

extern INT1
nmhTestv2FsMsdpTraceLevel ARG_LIST((UINT4 * , INT4 ));

extern INT1
nmhTestv2FsMsdpIPv4AdminStat ARG_LIST((UINT4 * , INT4 ));

extern INT1
nmhTestv2FsMsdpIPv6AdminStat ARG_LIST((UINT4 * , INT4 ));

extern INT1
nmhTestv2FsMsdpCacheLifetime ARG_LIST((UINT4 * , UINT4 ));

extern INT1
nmhTestv2FsMsdpMaxPeerSessions ARG_LIST((UINT4 * , INT4 ));

extern INT1
nmhTestv2FsMsdpMappingComponentId ARG_LIST((UINT4 * , INT4 ));

extern INT1
nmhTestv2FsMsdpListenerPort ARG_LIST((UINT4 * , INT4 ));

extern INT1
nmhTestv2FsMsdpPeerFilter ARG_LIST((UINT4 * , INT4 ));

extern INT1 
nmhGetFsIsisExtCircMTID ARG_LIST((INT4  , INT4 ,INT4 *));
extern INT1 
nmhGetFsIsisExtAdjMTID ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));
extern INT1 
nmhTestv2FsIsisMultiTopologySupport ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 
nmhSetFsIsisMultiTopologySupport ARG_LIST((INT4  ,INT4 ));
extern INT1 
nmhGetFsIsisMultiTopologySupport ARG_LIST((INT4 ,INT4 *));

VOID
IssProcessMsdpSARedistPage (tHttp * );

VOID
IssProcessMsdpSARedistPageGet (tHttp * );

VOID
IssProcessMsdpSARedistPageSet (tHttp * );

VOID
IssProcessMsdpRPPage (tHttp * );

VOID
IssProcessMsdpRPPageGet (tHttp * );

VOID
IssProcessMsdpRPPageSet (tHttp * );

VOID
IssProcessMsdpMeshPage (tHttp * );

VOID
IssProcessMsdpMeshPageGet (tHttp * );

VOID
IssProcessMsdpMeshPageSet (tHttp * );

VOID
IssProcessMsdpPeerTablePage (tHttp * );

VOID
IssProcessMsdpPeerTablePageGet (tHttp * );

VOID
IssProcessMsdpPeerTablePageSet (tHttp * );

VOID
IssProcessMsdpPeerFilterPage (tHttp * );

VOID
IssProcessMsdpPeerFilterPageGet (tHttp * );

VOID
IssProcessMsdpPeerFilterPageSet (tHttp * );

VOID
IssProcessMsdpGlobalPage (tHttp * );

VOID
IssProcessMsdpGlobalPageGet (tHttp * );

VOID
IssProcessMsdpGlobalPageSet (tHttp * );

#endif /*MSDP_WANTED*/

#ifdef _ISS_WEB_C_
tSpecificPage       asIsspages[] = {
    {"bottom.html", IssProcessBottomPage},
    {"bottom_wss.html", IssProcessBottomPage},
    {"ObjectTreeFrame.htm", IssProcessObjTreePage},
    {"index.html", IssProcessSendTitle},
    {"homepage.html", IssProcessSendTitle},
    {"about.html", IssProcessBottomPage},
    {"help.html", IssProcessSendTitle},
    {"layer2managementright.html", IssProcessL2Page},
    {"layer3managementright.html", IssProcessL3Page},
    {"networkoverlayright.html", IssProcessNetOverlaypage},
    {"securitymgmtright.html", IssProcessSecurityPage},
    {"multicastright.html", IssProcessMulticastPage},
#ifdef RM_WANTED
#ifdef MBSM_WANTED
    {"stackingright.html", IssProcessStackingPage},
#endif
#endif
    {"GlobalNavigationFrame.htm", IssProcessGlobalPage},
    {"statisticsright.html", IssProcessSendTitle},
    {"rrd_globalconfframe.html", IssProcessRRDGlobalFrame},
    {"context_selection.html", IssProcessContextPage},
    {"ent_phy.html", IssProcessEntPhysicalPage},
    {"ent_LPMapping.html", IssProcessEntLPMappingPage},
    {"ent_AliasMapping.html", IssProcessEntAliasMappingPage},
    {"ent_ContainsMapping.html", IssProcessEntContainsPage},
    {"l3context_selection.html", IssProcessL3ContextPage},
    {"l3context_interfacemapping.html",
     IssProcessL3ContextInterfaceMappingPage},
#ifdef MEF_WANTED
    {"transmode.html", IssProcessTransModeSelectionPage}, 
    {"uni.html", IssProcessUniConfPage}, 
    {"evc.html", IssProcessEvcConfPage}, 
    {"UniCVlanEvc.html", IssProcessUniCVlanEvcConfPage}, 
    {"evc_filter.html", IssProcessEvcFilterConfPage}, 
    {"mef_etree.html", IssProcessMefETreeConfPage}, 
    {"mef_filter.html", IssProcessMefFilterConfPage}, 
    {"classmap.html", IssProcessClassMapConfPage}, 
    {"class.html", IssProcessClassConfPage}, 
    {"meter.html", IssProcessMeterConfPage}, 
    {"policymap.html", IssProcessPolicyMapConfPage}, 
    {"mefmepfl.html", IssProcessMefMepFlConfPage}, 
    {"mefmepfd.html", IssProcessMefMepFdConfPage}, 
    {"meffdstats.html", IssProcessFdStatsConfPageGet}, 
    {"mefflstats.html", IssProcessFlStatsConfPageGet}, 
    {"mefmepavailability.html", IssProcessMefMepAvailabilityConfPage}, 
    {"unilist.html", IssProcessUniListConfPage},
#endif
#ifdef ISS_METRO_WANTED
    {"vcm_bridgemodeselection.html", IssProcessMiBridgeModeSelectionPage},
#endif
#ifdef TACACS_WANTED
    {"tacacs_config.html", IssProcessTacacsServerPage},
    {"tacacs_serverconf.html", IssProcessTacacsActiveServerPage},
#endif
#ifdef WLC_WANTED
    {"wlan_web_auth_login.html", IssProcessWebAuthLoginPage},
#ifdef WSS_WEB_WANTED /* The below pages are valid pages, will be added later */
    {"wlan_web_auth_result.html", IssProcessWebAuthResultPage},
    {"wlan_web_auth_customization.html", IssProcessWebAuthConfigPage},
#endif
    {"wlan_web_auth_user_creation.html", IssProcessWebAuthUserCreationPage},
    {"wlan_web_auth_customization.html", IssProcessWebAuthCustomizationPage},
    {"wlan_web_auth_external.html", IssProcessWebAuthExternalCustomizationPage},
    {"wlan_web_auth_internal_customization.html", IssProcessWebAuthInternalCustomizationPage},
#endif
#ifdef VCM_WANTED
    {"vcm_portmap.html", IssProcessMiPortMapPage},
#endif
    {"port_creation.html", IssProcessMiPortCreationPage},
    {"ac_port_vlanmap.html", IssProcessAcMappingPage},
#ifdef BGP_WANTED
    {"bgp_context_creation.html", IssProcessBgpContextCreationPage},
    {"rrd_bgpconfframe.html", IssProcessRRDBgpFrame},
    {"bgp_timerconf.html", IssProcessBgpTimerconfPage},
    {"bgp_peerstats.html", IssProcessBgpPeerStatsPage},
    {"bgp_globalconfscalars.html", IssProcessBgpglobalConfScalarsPage},
    {"bgp_peerconf.html", IssProcessBgpPeerConfPage},
    {"bgp_routemap.html", IssProcessBgpRouteMapPage},
    {"bgp_filterconf.html", IssProcessBgpFilterConfPage},
    {"bgp_medconf.html", IssProcessBgpMedConfPage},
    {"bgp_routeaggreconf.html", IssProcessBgpRouteAggrConfPage},
    {"bgp_localprefconf.html", IssProcessBgpLocalPrefConfPage},
    {"bgp_commfilterconf.html", IssProcessBgpCommFilterConfPage},
    {"bgp_commlocpolicyconf.html", IssProcessCommBgpLocPolicyConfPage},
    {"bgp_commrouteconf.html", IssProcessBgpCommRouteConfPage},
    {"bgp_ecommfilterconf.html", IssProcessBgpECommFilterConfPage},
    {"bgp_ecommlocpolicyconf.html", IssProcessBgpECommLocPolicyConfPage},
    {"bgp_ecommrouteconf.html", IssProcessBgpECommRouteConfPage},
    {"bgp_confedconf.html", IssProcessBgpConfedConfPage},
    {"bgp_rfdconf.html", IssProcessBgpRfdConfPage},
    {"bgp_importconf.html", IssProcessBgpImportConfPage},
    {"bgp_grconf.html", IssProcessBgpGrConfPage},
    {"bgp_peergroupconf1.html", IssProcessBgpPeerGroupConfPage},
    {"bgp_peergroupconf2.html", IssProcessBgpGroupConfContPage},
    {"bgp_peergroupadd.html", IssProcessBgpPeerGroupAddPage},
    {"bgp_clearbgpconf.html", IssProcessBgpClearConfPage},
    {"bgp_peerorfconf.html", IssProcessBgpOrfConfPage},
    {"bgp_orffilters.html", IssProcessBgpOrfFiltersPage},
    {"rrd_bgpconf.html", IssProcessRRDBgpConfPage},
    {"bgp_tcpaomktconf.html", IssProcessBgpTcpAoConfPage},
#ifdef ROUTEMAP_WANTED
    {"fltr_bgpconf.html", IssProcessFilterBgpConfPage},
#endif
#endif /* BGP_WANTED */
    {"gen_info.html", IssProcessGenInfoPage},
    {"ivr_conf.html", IssProcessIvrPage},
    {"port_settings.html", IssProcessPortSettingsPage},
#ifdef IGMP_WANTED
    {"igmp_iface.html", IssProcessIgmpPage},
    {"igmp_grp.html", IssProcessIgmpRouteConfPage},
    {"igmp_src.html", IssProcessIgmpMembershipPage},
    {"igmp_grouplist.html", IssProcessIgmpGroupListConfPage},
#endif /* IGMP_WANTED */
#ifdef MLD_WANTED
    {"mld_iface.html", IssProcessMldPage},
#endif /* MLD_WANTED */
#ifdef IGMPPRXY_WANTED
    {"igp_upiface.html", IssProcessIgpUpIfPage},
#endif /* IGMPPRXY_WANTED */
    {"vlan_interfacesettings.html", IssProcessVlanInterfaceSettingsPage},
    {"loopback_settings.html", IssProcessLoopBackSettingsPage},
    {"vlan_staticconf.html", IssProcessStaticVlanPage},
    {"vlan_portmacmap.html", IssProcessVlanMacMapPage},
    {"vlan_subnetipmap.html", IssProcessVlanSubnetMapPage},
    {"vlan_portvidset.html", IssProcessPortVidSetPage},
    {"vlan_protgrp.html", IssProcessVlanProtoGrpPage},
    {"vlan_forwardconf.html", IssProcessVlanFwdPage},
    {"vlan_fdbflush.html", IssProcessVlanFdbFlushPage},
    {"l2_ucastfilter.html", IssProcessL2UnicastFilterPage},
    {"l2_mcastfilter.html", IssProcessL2MulticastFilterPage},
    {"vlan_trafficclass.html", IssProcessTrafficClassPage},
    {"cfa_port_role.html", IssProcessCfaPortRolePage},
    {"vlan_devicecapa.html", IssProcessVlanCapabilitiesPage},
    {"port_monitoring.html", IssProcessPortMonitoringPage},
    {"nvram_settings.html", IssProcessNvramSettingsPage},
    {"ivr_pvlanmapping.html", IssProcessIvrPvlanMappingPage},
#ifdef GARP_WANTED
    {"vlan_garpclearstats.html", IssProcessVlanGarpClearStatsPage},
#endif /*GARP_WANTED*/
    {"splithorizon_config.html",IssProcessSplitHorizonConfigPage},
    {"ufd_globalconfig.html",IssProcessUfdConfigPage},
    {"ufd_groupconfig.html",IssProcessUfdGroupConfigPage},
#ifdef PNAC_WANTED
    {"pnac_localAS.html", IssProcessPnacLocalAsPage},
    {"pnac_macsessioninfo.html", IssProcessPnacMacSessionInfoPage},
    {"pnac_macsessionstats.html", IssProcessPnacMacSessionStatsPage},
#ifdef RADIUS_WANTED
    {"pnac_radClient.html", IssProcessPnacRadiusPage},
    {"radius_statistics.html", IssProcessPnacRadiusStatsPage},
#endif
#endif
#ifdef MRP_WANTED
    {"mrp_applicantcontrol.html", IssProcessMrpApplicationPage},
    {"mrp_clearstat.html", IssProcessMrpClearStatConfigPage},

#endif
#ifdef DHCP6_CLNT_WANTED
    {"dhcp6c_globalconf.html", IssProcessDhcp6ClientGlobalPage},
    {"dhcp6c_ifconf.html", IssProcessDhcp6ClientIfPage},
    {"dhcp6c_option.html", IssProcessDhcp6ClientOptionPage},
#endif
#ifdef DHCP6_RLY_WANTED
    {"dhcp6r_globalconf.html", IssProcessDhcp6RelayGlobalPage},
    {"dhcp6r_ifconf.html", IssProcessDhcp6RelayIfPage},
    {"dhcp6r_serveraddr.html", IssProcessDhcp6RelaySrvAddrPage},
    {"dhcp6r_outifconf.html", IssProcessDhcp6RelayOutIfPage},
#endif
#ifdef DHCP6_SRV_WANTED
    {"dhcp6s_globalconf.html", IssProcessDhcp6ServerGlobalPage},
    {"dhcp6s_poolconf.html", IssProcessDhcp6ServerPoolPage},
    {"dhcp6s_clientconf.html", IssProcessDhcp6ServerClientPage},
    {"dhcp6s_realmconf.html", IssProcessDhcp6ServerRealmPage},
    {"dhcp6s_keyconf.html", IssProcessDhcp6ServerKeyPage},
    {"dhcp6s_ifconf.html", IssProcessDhcp6ServerIfPage},
    {"dhcp6s_prefixconf.html", IssProcessDhcp6ServerPrefixPage},
    {"dhcp6s_optionconf.html", IssProcessDhcp6ServerOptionPage},
    {"dhcp6s_suboptionconf.html", IssProcessDhcp6ServerSubOptionPage},
#endif

#ifdef EOAM_WANTED
    {"eoam_linkeventsettings.html", IssProcessEoamLinkSettingsPage},
    {"eoam_neighborstats.html", IssProcessEoamNeighborStatusPage},
    {"eoam_eventlog.html", IssProcessEoamEventLogPage},
#ifdef EOAM_FM_WANTED
    {"fm_mibresponse.html", IssProcessFmMibResponsePage},
#endif /* EOAM_FM_WANTED */
#endif /* EOAM_WANTED */
#ifdef QOSX_WANTED
    {"qos_basicsettings.html", IssProcessBasicSettingsPage},
    {"qos_datapath.html", IssProcessDataPathPage},
    {"qos_classifier.html", IssProcessClassifierPage},
    {"qos_clfrelement.html", IssProcessClfrElementPage},
    {"qos_meter.html", IssProcessMeterPage},
    {"qos_action.html", IssProcessActionPage},
    {"qos_countaction.html", IssProcessCountActionPage},
    {"qos_prioritymapsettings.html", IssProcessPriorityMapSettingsPage},
    {"qos_classmapsettings.html", IssProcessClassMapSettingsPage},
    {"qos_classtoprisettings.html", IssProcessClassToPriSettingsPage},
    {"qos_tbparammeter.html", IssProcessMeterTableSettingsPage},
    {"qos_minrate.html", IssProcessMinRatePage},
    {"qos_maxrate.html", IssProcessMaxRatePage},
    {"qos_policymapsettings.html", IssProcessPolicyMapSettingsPage},
    {"qos_queuetemplate.html", IssProcessQueueTemplateSettingsPage},
    {"qos_redconf.html", IssProcessRedConfSettingsPage},
    {"qos_stdqueuetable.html", IssProcessQueuePage},
    {"qos_queuetable.html", IssProcessQueueTableSettingsPage},
    {"qos_queuemap.html", IssProcessQueueMapSettingsPage},
    {"qos_hierarchytable.html", IssProcessHierarchyTableSettingsPage},
    {"qos_shapetemplate.html", IssProcessShapeTemplateSettingsPage},
    {"qos_schedulertable.html", IssProcessSchedulerTableSettingsPage},
    {"qos_stdscheduler.html", IssProcessStdSchedulerPage},
    {"qos_defaultuserprio.html", IssProcessDefaultUserPrioritySettingsPage},
    {"qos_policerstats.html", IssProcessPolicerStatsPage},
    {"qos_cosstats.html", IssProcessCosStatsPage},
#ifdef CN_WANTED
    {"cn_port_settings.html", CnProcessCnPortSettingsPage},
    {"cn_cp_settings.html", CnProcessCnCpSettingsPage},
    {"cn_statistics.html", CnProcessCnStatisticsPage},
    {"cn_debug.html", CnProcessCnDebugPage},

#endif /*CN WANTED */
#endif
#ifdef DCBX_WANTED
    {"dcb_homePageright.html", DcbIssProcessHomePage},
    {"ets_priToPGIDMapping.html", ETSIssProcessPriorityPage},
    {"ets_bandwidthConfiguration.html", ETSIssProcessBandwidthPage},
    {"ets_localInfo.html", ETSIssProcessLocalInfoPage},
    {"ets_remoteInfo.html", ETSIssProcessRemoteInfoPage},
    {"ets_adminportconfig.html", ETSIssProcessAdminInfoPage},
    {"ets_tsaConfig.html", ETSIssProcessTsaInfoPage},
    {"pfc_statusPerPriority.html", PFCIssProcessPage},
    {"pfc_localInfo.html", PFCIssProcessLocalPage},
    {"pfc_remoteInfo.html", PFCIssProcessRemotePage},
    {"pfc_adminportconfig.html", PFCIssProcessAdminInfoPage},
    {"ap_adminportconfig.html", AppPriIssProcessAdminInfoPage},
    {"ap_localInfo.html", AppPriIssProcessLocalPageGet},
    {"ap_remoteInfo.html", AppPriIssProcessRemotePageGet},
    {"ap_applicationToPriority.html", AppPriIssProcessPage},
#endif /*DCBX_WANTED */
#ifdef RM_WANTED
    {"hrconfig.html", IssProcessHitlessRestartConfPage},
    {"rmconfig.html", IssProcessRedundancyManagerConfPage},
#ifdef MBSM_WANTED
    {"rmconfig.html", IssProcessRedundancyManagerConfPage},
    {"stackconfigure.html", IssProcessStackConfigurePage},
    {"stackshowdetails.html", IssProcessStackShowPage},
    {"stackshowbrief.html", IssProcessStackShowBriefPage},
    {"stackcounters.html", IssProcessStackCountersPage},
#endif
#endif

#ifdef PB_WANTED
#ifdef RSTP_WANTED
    {"pbrstp_portinfo.html", IssProcessPbRstpPortInfoPage},
    {"pbrstp_portstats.html", IssProcessPbRstpPortStatsPage},
#endif
#endif
    {"save.html", IssProcessSavePage},
    {"erase.html", IssProcessErasePage},
    {"remoterestore.html", IssProcessRemoteRestorePage},
    {"top.html", IssProcessLedPage},
    {"log_info.html", IssProcessLogSavePage},
    {"upgrade_sw.html", IssProcessSwUpgradePage},
    {"file_upload.html", IssProcessFileUploadPage},
    {"file_download.html", IssProcessFileDownloadPage},
    {"portisolation_conf.html", IssProcessPortIsolationPage},
#ifdef PIM_WANTED
    {"pim_routeinfo.html", IssProcessPimMRoutePage},
    {"pim_ifaceconf.html", IssProcessPimIfacePage},
    {"pim_crpconf.html", IssProcessPimCrpPage},
    {"pim_electedrp.html", IssProcessPimElectedRPPage},
    {"pim_dfinfo.html", IssProcessPimDFInfoPage},
    {"pim_component.html", IssProcessPimComponentPage},
    {"pim_srpconf.html", IssProcessPimSRpPage},
#endif
#ifdef ROUTEMAP_WANTED
    {"rmap_conf.html", IssProcessRMapCreationPage},
    {"rmapmatch_conf.html", IssProcessRMapMatchPage},
    {"rmapset_conf.html", IssProcessRMapSetPage},
    {"rmap_ipprefix_conf.html", IssProcessRMapIpPrefixPage},
#endif
#ifdef ISIS_WANTED
    {"isis_sysinstconf.html", IssProcessIsisInstConfPage},
    {"isis_maaconf.html", IssProcessIsisMAAConfPage},
    {"isis_summaddrconf.html", IssProcessIsisSummAddrConfPage},
    {"isis_ipraconf.html", IssProcessIsisIPRAConfPage},
    {"isis_pktcountconf.html", IssProcessIsisPktCntPageGet},
    {"isis_adjconf.html", IssProcessIsisAdjPageGet},
    {"isis_circconf.html", IssProcessIsisCircConfPage},
    {"isis_aaconf.html", IssProcessIsisAAddrConfPageGet},
    {"isis_GRconf.html", IssProcessIsisGRPage},
#ifdef ROUTEMAP_WANTED
    {"fltr_isisconf.html", IssProcessFilterIsisConfPage},
#endif /* ROUTEMAP_WANTED */
#endif /* ISIS_WANTED */
#ifdef OSPF_WANTED
    {"ospf_context_creation.html", IssProcessOspfContextCreationPage},
    {"ospf_globalconf.html", IssProcessOspfGlobalConfPage},
    {"ospf_ifaceconf.html", IssProcessOspfIfacePage},
    {"ospf_areaconf.html", IssProcessOspfAreaPage},
    {"ospf_virtualifaceconf.html", IssProcessOspfVirtualIfacePage},
    {"ospf_nbrconf.html", IssProcessOspfNeighborPage},
    {"ospf_RRDrouteconf.html", IssProcessOspfRRDRoutePage},
    {"ospf_AreaAggregateconf.html", IssProcessOspfAreaAggrPage},
    {"ospf_AsExtAggrconf.html", IssProcessOspfExtAggrPage},
    {"ospfroute_stats.html", IssProcessOspfRouteStatsPage},
    {"ospf_linkstdb.html", IssProcessOspfLsdbPage},
    {"ospfred_stats.html", IssProcessOspfRedStatsPage},
    {"ospf_GRconf.html", IssProcessOspfGRPage},
    {"rrd_ospfconfframe.html", IssProcessRRDOspfFrame},
    {"rrd_ospfconf.html", IssProcessRRDOspfConfPage},
#ifdef ROUTEMAP_WANTED
    {"fltr_ospfconf.html", IssProcessFilterOspfConfPage},
#endif
#endif
    {"snmp_homepage.html", IssProcessSnmpHomePage},
    {"snmp_tgtaddrconf.html", IssProcessSnmpTargetAddressPage},
    {"snmp_communityconf.html", IssProcessSnmpCommunityPage},
    {"snmp_filterprof.html", IssProcessSnmpFilterProfilePage},
    {"snmp_agentx_conf.html", IssProcessSnmpAgentxPage},
    {"snmp_statistics.html", IssProcessSnmpAgentStatsPage},
    {"snmp_agentx_statistics.html", IssProcessSnmpAgentxStatsPage},
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
    {"ip_routeconf.html", IssProcessIpRouteConfPage},
    {"arp_entry.html",IssProcessArpEntryPage},
    {"ipv4traceroute.html",IssProcessIpv4TraceRoutePage},

#endif
#ifdef IP_WANTED
    {"ip_ping.html", IssProcessIpPingPage},
#endif
#ifdef IGS_WANTED
    {"igs_conf.html", IssProcessIgsInstConfPage},
    {"igs_tmrconf.html", IssProcessIgsTmrConfPage},
    {"igs_vlan.html", IssProcessIgsVlanPage},
    {"igs_macfwdtable.html", IssProcessIgsVlanMcastFwdTablePage},
    {"igs_ipfwdtable.html", IssProcessIgsVlanMcastFwdTablePage},
    {"igs_enhipfwdtable.html", IssProcessIgsVlanMcastFwdTablePage},
    {"igs_rtrtable.html", IssProcessIgsVlanRouterTablePage},
    {"igs_clearstats.html", IssProcessIgsClearStatsPage},
    {"igs_stats.html", IssProcessIgsStatsTablePage},
    {"igs_v3stats.html", IssProcessIgsv3StatsTablePage},
    {"igs_mcastrecv.html", IssProcessIgsMcastRecvPage},
    {"igs_enhmcastrecv.html", IssProcessIgsMcastRecvPage},
    {"igs_intf.html", IssProcessIgsIntfConfPage},
    {"igs_enhintf.html", IssProcessIgsIntfConfPage},
    {"igs_rtrportconf.html", IssProcessIgsRtrPortConfPage},
    {"igs_static.html", IssProcessIgsStaticPage},
#endif
#ifdef MLDS_WANTED
    {"mlds_conf.html", IssProcessMldsInstConfPage},
    {"mlds_tmrconf.html", IssProcessMldsTmrConfPage},
    {"mlds_vlan.html", IssProcessMldsVlanPage},
    {"mlds_macfwdtable.html", IssProcessMldsVlanMcastFwdTablePage},
    {"mlds_ipfwdtable.html", IssProcessMldsVlanMcastFwdTablePage},
    {"mlds_enhipfwdtable.html", IssProcessMldsVlanMcastFwdTablePage},
    {"mlds_rtrtable.html", IssProcessMldsVlanRouterTablePage},
    {"mlds_stats.html", IssProcessMldsStatsTablePage},
    {"mlds_v2stats.html", IssProcessMldsv2StatsTablePage},
    {"mlds_mcastrecv.html", IssProcessMldsMcastRecvPage},
    {"mlds_enhmcastrecv.html", IssProcessMldsMcastRecvPage},
    {"mlds_intf.html", IssProcessMldsIntfConfPage},
    {"mlds_enhintf.html", IssProcessMldsIntfConfPage},
    {"mlds_rtrportconf.html", IssProcessMldsRtrPortConfPage},
#endif
    {"vlan_fdbentries.html", IssProcessVlanFdbEntriesPage},
    {"vlan_currentdb.html", IssProcessVlanCurrentDbPage},
#ifdef RSTP_WANTED
#ifdef MSTP_WANTED
    {"mstp_cistportstats.html", IssProcessMstpCistPortStatsPage},
    {"mstp_mstiportstats.html", IssProcessMstpMstiPortStatsPage},
    {"mstp_cistportstatus.html", IssProcessMstpCistPortStatusPage},
    {"msti_vlanmap.html", IssProcessMstiVlanMapPage},
    {"msti_portconf.html", IssProcessMstiPortConfPage},
    {"msti_bridgeconf.html",IssProcessMstiBridgeConfPage},
#endif

#ifdef BRIDGE_WANTED
    {"bridge_info.html", IssProcessBridgeInfoPage},
#endif

    {"rstp_portstats.html", IssProcessRstpPortStatsPage},
    {"rstp_info.html", IssProcessRstpInfoPage},
    {"rstp_timersconf.html", IssProcessRstpTimersPage},
    {"rstp_portstatus.html", IssProcessRstpPortStatusPage},
#endif
    {"arpcache_stats.html", IssProcessArpCacheStatsPage},
#ifdef LA_WANTED
    {"la_portchannel.html", IssProcessLAPortChannelPage},
    {"la_interfacesettings.html", IssProcessLAInterfaceSettingsPage},
    {"la_aggreconf.html", IssProcessLAAggreConfPage},
    {"la_portstateinfo.html", IssProcessLAPortStateInfoPage},
    {"la_policyconf.html", IssProcessLASelectionPolicyPage},
#endif
    {"vlan_portstats.html", IssProcessVlanStatsPage},
    {"vlan_wildcard_settings.html", IssProcessWildCardSettingsPage},
#ifdef VRRP_WANTED
    {"vrrp_basic_settings.html",IssProcessVrrpBasicPage},
    {"vrrp_conf.html", IssProcessVrrpConfPage},
    {"vrrp_stats.html", IssProcessVrrpStatsPage},
    {"vrrp_tracksettings.html", IssProcessVrrpTrackPage},
    {"vrrp_assoc.html", IssProcessVrrpAssocPage},
#endif
#ifdef RMON_WANTED
    {"rmon_etherstats.html", IssProcessRmonEtherStatsPage},
#endif
#ifdef MSDP_WANTED
    {"msdp_globalconf.html", IssProcessMsdpGlobalPage},
    {"msdp_peertableconf.html", IssProcessMsdpPeerTablePage},
    {"msdp_rp.html", IssProcessMsdpRPPage},
    {"msdp_mesh.html", IssProcessMsdpMeshPage},
    {"msdp_peerfilter.html", IssProcessMsdpPeerFilterPage},
    {"msdp_saredist.html", IssProcessMsdpSARedistPage},
#endif

#ifdef NAT_WANTED
    {"nat_conf.html", IssProcessNatConfPage},
    {"nat_static.html", IssProcessNatStaticPage},
    {"nat_dynamic.html", IssProcessNatDynamicPage},
    {"nat_virtualserver.html", IssProcessNatVSPage},
    {"nat_translation.html", IssProcessNatTranslationPage},
#endif
#ifdef DVMRP_WANTED
    {"dvmrpiface_conf.html", IssProcessDvmrpIfacePage},
#endif
#ifdef DHCP_RLY_WANTED
    {"dhcpr_interfacemap.html", IssProcessDhcpRlyInterfaceConfPage},
#endif
#ifdef RRD_WANTED
    {"rrd_globalconf.html", IssProcessRrdGlobalPage},
#endif
#ifdef RIP_WANTED
    {"rip_globalconf.html", IssProcessRipGlobalPage},
    {"ripcontext_creation.html", IssProcessRipVRFCreationPage},
    {"rip_ifaceconf.html", IssProcessRipIfacePage},
    {"rip_keyconf.html", IssProcessRipIfaceConfKeyPage},
    {"rrd_ripconfframe.html", IssProcessRRDRipFrame},
    {"rip_nbrfilter.html", IssProcessRipTrustNBRPage},
    {"rip_aggconf.html", IssProcessRipAggPage},
    {"rrd_ripconf.html", IssProcessRRDRipConfPage},
#ifdef ROUTEMAP_WANTED
    {"fltr_ripconf.html", IssProcessFilterRipConfPage},
#endif
#endif
    {"vlan_dyngrptable.html", IssProcessVlanDynGrpTablePage},
    {"ip_auth_mgr.html", IssProcessIpAuthMgrPage},
#ifdef IP6_WANTED
    {"ipv6_if_Forwarding.html", IssProcessIpv6IntfForwdPage},
    {"ivr6_conf.html", IssProcessIpv6IntfConfPage},
    {"ip6_routeconf.html", IssProcessIpv6RouteConfPage},
    {"ND_conf.html", IssProcessIpv6NDConfPage},
    {"addr_conf.html", IssProcessAddrConfPage},
    {"pref_conf.html", IssProcessIpv6PrefixConfPage},
    {"rrd6_filterconf.html", IssProcessRRD6FilterConfPage},
    {"policyprefix_conf.html", IssProcessIpv6PolicyPrefixConfPage},
    {"interfacezone_conf.html", IssProcessIpv6InterfaceZoneConfPage},
    {"scopezone_conf.html", IssProcessIpv6ScopeZoneConfPage},
    {"ipv6_ping.html", IssProcessIpv6PingPage},
#endif
    {"L3_tnlconf.html", IssProcessTnlConfPage},

#ifdef RIP6_WANTED
    {"rip6_interfacestats.html", IssProcessRip6StatsPage},
    {"rip6_route.html", IssProcessRip6RouteInfoPage},
    {"rip6_IfConf.html", IssProcessRip6IfConfPage},
    {"rip6_filt.html", IssProcessRip6FilterPage},
#ifdef ROUTEMAP_WANTED
    {"fltr_rip6conf.html", IssProcessFilterRip6ConfPage},
#endif
#endif

#ifdef OSPF3_WANTED
    {"ospfv3if_conf.html", IssProcessOspf3IfConfPage},
    {"ospfv3basic_conf.html", IssProcessOspf3BasicConfPage},
    {"ospfv3context_creation.html", IssProcessOspf3ContextCreation},
    {"ospfv3gr_conf.html", IssProcessOspf3GRConfPage},
    {"ospfv3area_conf.html", IssProcessOspf3AreaConfPage},
#ifdef ROUTEMAP_WANTED
    {"fltr_ospfv3conf.html", IssProcessFilterOspfv3ConfPage},
#endif
    {"ospfv3route_stats.html", IssProcessOspf3RouteStatsPage},
    {"ospfv3red_stats.html", IssProcessOspf3RedStatsPage},
#endif

#ifdef PB_WANTED
    {"pbvlan_pcpencoding.html", IssProcessPbvlanPcpEncodingPage},
    {"pbvlan_pcpdecoding.html", IssProcessPbvlanPcpDecodingPage},
    {"pbvlan_priorityregen.html", IssProcessPbvlanPriorityRegenPage},
    {"pbvlan_vidtransl.html", IssProcessPbvlanVidTransPage},
    {"pbvlan_cvidregi.html", IssProcessPbvlanCvidRegiPage},
    {"pbvlan_pepconf.html", IssProcessCosPreservationPage},
    {"pbtunnel_statusconfig.html", IssProcessPbTunnelStatusConfigPage},
    {"pb_servtypeconf.html", IssProcessPbServTypeConfPage},
    {"pb_portconf.html", IssProcessPbPortConfPage},
    {"pb_ethertypeswaptable.html", IssProcessPbEtherTypePage},
#endif

#if defined (IPSECv6_WANTED) && !defined (VPN_WANTED)
    {"secv6_selectorconf.html", IssProcessSecv6SelectorPage},
    {"secv6_stats.html", IssProcessSecv6StatsPage},
#endif
#ifdef MPLS_WANTED
    {"mplsif_conf.html", IssProcessMplsIfPage},
    {"mplsftn_conf.html", IssProcessMplsFtnPage},
    {"mplsxc_conf.html", IssProcessMplsXcPage},
    {"mplstunnel_conf.html", IssProcessMplsTunnelPage},
#ifdef MPLS_L3VPN_WANTED
    {"mplsl3vpn_conf.html", IssProcessMplsConfL3vpnPage},
#endif
#ifdef MI_WANTED
    {"mplsvfi_conf.html", IssProcessMplsVfiPage},
#endif
    {"mplspw_conf.html", IssProcessMplsPwPage},
    {"mplsenet_conf.html", IssProcessMplsPwEnetPage},
    {"mplsmap_conf.html", IssProcessMplsMapPage},
#endif
#ifdef ISS_WANTED
    {"mirror_ctrlext.html", IssProcessMirrCtrlExtPage},
    {"gen_infoclearcounters.html", IssProcessClearCountersPage},
#endif
#ifdef PTP_WANTED
    {"ptp_if_conf.html", IssProcessPtpIfPage},
    {"ptp_globalconf.html", IssProcessPtpGlobalConfPage},
    {"ptp_basicsettings.html", IssProcessPtpClockPage},
    {"ptp_alttime.html", IssProcessPtpAltTimePage},
    {"ptp_accmaster.html", IssProcessPtpAccMasterPage},
    {"ptp_traceconf.html", IssProcessPtpTraceConfPage},
#endif

#ifdef CLKIWF_WANTED
    {"clockiwf_settings.html", IssProcessClkiwfConfPage},
#endif

#ifdef PVRST_WANTED
    {"pvrst_instportstatus.html", IssProcessPvrstInstPortStatusPage},
    {"pvrst_instportinfo.html", IssProcessPvrstInstPortInfoPage},
    {"pvrst_instportconf.html", IssProcessPvrstInstPortConfPage},
    {"pvrst_instbrigconf.html", IssProcessPvrstInstBrigConfPage},
    {"pvrst_info.html", IssProcessPvrstInfoPage},
    {"pvrst_instinfo.html", IssProcessPvrstInstInfoPage},
    {"pvrst_portinfo.html", IssProcessPvrstPortInfoPage},
    {"pvrst_portconf.html", IssProcessPvrstPortConfPage},
    {"pvrst_basicconf.html", IssProcessPvrstBasicConfPage},
#endif
#ifdef ELMI_WANTED
    {"elmi_portconf.html", IssProcessElmiPortConfPage},
    {"elmi_basicstats.html", IssProcessElmiBasicStatsPage},
    {"elmi_portstats.html", IssProcessElmiPortStatsPage},
    {"elmi_errorstats.html", IssProcessElmiErrorStatsPage},
#endif
#ifdef ELPS_WANTED
    {"elps_pginfo.html", IssProcessElpsPGinfoPage},
    {"elps_pgcfmconf.html", IssProcessElpsPgCfmPage},
    {"elps_servicelist.html", IssProcessElpsPGServicePage},
#endif
#ifdef ECFM_WANTED
    {"ecfm_globalconf.html", IssProcessEcfmGlobalConfPage},
    {"ecfm_mdconf.html", IssProcessEcfmMdConfPage},
    {"ecfm_maconf.html", IssProcessEcfmMaConfPage},
    {"ecfm_mepconf.html", IssProcessEcfmMepConfPage},
    {"ecfm_mepparamsconf.html", IssProcessEcfmMepDetailConfPage},
    {"ecfm_mipconf.html", IssProcessEcfmMipConfPage},
    {"ecfm_portconf.html", IssProcessEcfmPortConfPage},
    {"ecfm_vlan.html", IssProcessEcfmVlanConfPage},
    {"ecfm_rmeplist.html", IssProcessEcfmRMepListPage},
    {"ecfm_stats.html", IssProcessEcfmStatsPage},
    {"ecfm_portstats.html", IssProcessEcfmPortStatsPage},
    {"ecfm_mepstats.html", IssProcessEcfmMepStatsPage},
    {"ecfm_errors.html", IssProcessEcfmErrorsPage},
    {"ecfm_mipccmdb.html", IssProcessEcfmMipCcmDbPage},
    {"ecfm_defmdconf.html", IssProcessEcfmDefMdConfPage},
    {"ecfm_mameps.html", IssProcessEcfmShowMaMepsPage},
    {"ecfm_miscconf.html", IssProcessEcfmMiscConfPage},
    {"ecfm_mepdetails.html", IssProcessEcfmMepDetailsPage},
    {"ecfm_buffhomepage.html", IssProcessEcfmBuffHomePage},
    {"ecfm_errlog.html", IssProcessEcfmShowErrLogPage},
    {"ecfm_fldetail.html", IssProcessEcfmShowFlDetailPage},
    {"ecfm_fddetail.html", IssProcessEcfmShowFdDetailPage},
    {"ecfm_flbrief.html", IssProcessEcfmShowFlBriefPage},
    {"ecfm_fdbrief.html", IssProcessEcfmShowFdBriefPage},
    {"ecfm_lbdetail.html", IssProcessEcfmShowLbDetailPage},
    {"ecfm_lbbrief.html", IssProcessEcfmShowLbBriefPage},
    {"ecfm_ltrcache.html", IssProcessEcfmShowLtrCachePage},
    {"ecfm_confpduhomepage.html", IssProcessEcfmConfPduHomePage},
    {"ecfm_ccm.html", IssProcessEcfmConfCcmPage},
    {"ecfm_fd.html", IssProcessEcfmConfFdPage},
    {"ecfm_fl.html", IssProcessEcfmConfFlPage},
    {"ecfm_tst.html", IssProcessEcfmConfTstPage},
    {"ecfm_lbm.html", IssProcessEcfmConfLbmPage},
    {"ecfm_ltm.html", IssProcessEcfmConfLtmPage},
    {"ecfm_ais.html", IssProcessEcfmConfAisPage},
    {"ecfm_lck.html", IssProcessEcfmConfLckPage},
    {"ecfm_th.html", IssProcessEcfmConfThPage},
    {"ecfm_availability.html", IssProcessEcfmConfAvlbltyPage},
#endif
#ifdef ERPS_WANTED
    {"erps_ringstatus.html", IssProcessErpsRingStatusPage},
    {"erps_ringtable.html", IssProcessErpsRingTablePage},
    {"erps_ringtcprop.html", IssProcessErpsRingTcPropPage},
    {"erps_ringvlan.html", IssProcessErpsRingVlanPage},
#endif
#ifdef TAC_WANTED
    {"tac_profile.html", IssProcessTacProfilePage},
    {"tac_prffilter.html", IssProcessTacPrfFilterPage},

#endif
#ifdef IP6_WANTED
    {"ipv6ifsp_stats.html", IssProcessIpv6IfSpInfoGet},
    {"ipv6syssp_stats.html", IssProcessIpv6SysSpInfoGet},
#endif
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
    {"ipv4ifsp_stats.html", IssProcessIpv4IfSpInfoGet},
    {"ipv4syssp_stats.html", IssProcessIpv4SysSpInfoGet},
#endif
#ifdef SNTP_WANTED
    {"sntp_unicastconf.html", IssProcessSntpUnicastConfPage},
    {"sntp_scalarsconf.html", IssProcessSntpScalarsConfPage},
#endif
    {"interface_clearstats.html", IssProcessIfClearStatsPage},
    {"tcp_stats.html", IssProcessTcpStatsPage},
    {"udp_stats.html", IssProcessUdpStatsPage},
    {"tcp_listnrs.html", IssProcessTcpListenerPage},
    {"udp_conns.html", IssProcessUdpConnectionsPage},
#ifdef SYSLOG_WANTED
    {"bsdsyslog_mailtable.html", IssProcessBsdSyslogMailPage},
    {"bsdsyslog_fwdtable.html", IssProcessBsdSyslogFwdPage},
    {"bsdsyslog_loggingconf.html", IssProcessBsdSyslogLogPage},
    {"bsdsyslog_scalarsconf.html", IssProcessBsdSyslogScalarPage},
#endif
#ifdef DNS_WANTED
    {"dns_nameserver.html", IssProcessDnsNameServerPage},
    {"dns_cache.html", IssProcessDnsCachePage},
#endif
#ifdef BFD_WANTED
    {"bfd_ipv6_conf.html", IssProcessBfdIpv6ConfPage},
#endif

#ifdef SSL_WANTED
    {"ssl_digitalcert.html", IssProcessSslDigitalCertPage},
#endif /* SSL_WANTED */
#ifdef DHCP_SRV_WANTED
    {"dhcp_poolopt.html", IssProcessDhcpPoolOptConfPage},
    {"dhcp_hostip.html", IssProcessDhcpHostIpConfPage},
    {"dhcp_hostopt.html", IssProcessDhcpHostOptConfPage},
    {"dhcp_globalbootfileconfig.html",IssProcessDhcpBootfileConfPage},
#endif /* DHCP_SRV_WANTED */
#ifdef DHCPC_WANTED
    {"dhcpc_confopttype.html", IssProcessDhcpOptionTypePage},
    {"dhcpc_confclientId.html", IssProcessDhcpClientIdPage},
#endif

#ifdef LLDP_WANTED
    {"lldp_globalconf.html", IssProcessLldpGlobalConfPage},
    {"lldp_errors.html", IssProcessLldpErrorPage},
    {"lldp_basicsettings.html", IssProcessLldpBasicSettingsPage},
    {"lldp_traffic.html", IssProcessLldpTrafficPage},
    {"lldp_stats.html", IssProcessLldpStatsPage},
    {"lldp_interfaces.html", IssProcessLldpInterfacePage},
    {"lldp_neighbors.html", IssProcessLldpNeighborPage},
    {"lldp_agentinfo.html", ISSProcessLldpAgentPage},
    {"lldp_agentdetails.html", ISSProcessLldpAgentDetailsPage},
#endif /* LLDP_WANTED */

#ifdef VPN_WANTED
    {"vpn_ipsec.html", IssProcessIpSecConfPage},
    {"vpn_ikepreshared.html", IssProcessIkeConfPage},
    {"vpn_policy.html", IssProcessVpnPolicyConfPage},
    {"vpn_userdb.html", IssProcessVpnRaUserDBConfPage},
    {"vpn_addresspool.html", IssProcessVpnRaAddressPoolConfPage},
    {"vpn_client_term.html", IssProcessVpnRaClientTerminationConfPage},
    {"vpn_globalconf.html", IssProcessVpnGlobalConfPage},
#endif /* VPN_WANTED */
#ifdef FIREWALL_WANTED
    {"fwl_aclconf.html", IssProcessFwlAclPage},
    {"fwl_stats.html", IssProcessFwlStatsPage},
    {"fwl_interfaceconf.html", IssProcessFwlIntfPage},
    {"fwl_dmzconfv6.html", IssProcessFwlDMZV6Page},
    {"fwl_icmpinspect.html", IssProcessFwlIcmpPage},
    {"fwl_log.html", IssProcessFwlLogPage},
#endif /* FIREWALL_WANTED */
#ifdef WEBNM_TEST_WANTED
    {"http_auth_stats.html", IssProcessHttpStatsTablePage},
#endif
#ifdef WLC_WANTED
    {"capwap_global_settings.html", IssProcessCapwapGlobalPage},
    {"capwap_traces.html", IssProcessCapwapTracesPage},
    {"capwap_whitelist.html", IssProcessCapwapWhitelistPage},
    {"capwap_station_whitelist.html", IssProcessCapwapStationWhitelistPage},
    {"capwap_station_blacklist.html", IssProcessCapwapStationBlacklistPage},
    {"capwap_blacklist.html", IssProcessCapwapBlacklistPage},
    {"capwap_dtls_settings.html", IssProcessCapwapDtlsPage},
    {"capwap_timer_settings.html", IssProcessCapwapTimerPage},
    {"controller_global.html", IssProcessAPModelEntryPage},
    {"controller_globalEdit.html", IssProcessAPModelEntryEditPage},
    {"controller_modeltable.html", IssProcessAPModelTablePage},
    {"controller_radiotable.html", IssProcessRadioTablePage},
    {"controller_general.html", IssProcessControllerGeneralPage},
    {"controller_general_homepage.html", IssProcessWirelessApConfigTablePage},
    {"model_bind.html", IssProcessModelBindingTablePage},
    {"model_bindcreate.html", IssProcessModelBindCreateTablePage},
    {"wlan_createnew.html", IssProcessWlanCreatePage},
    {"wlan_homepage.html", IssProcessWlanTablePage},
    {"wlan_general.html", IssProcessWlanGeneralPage},
    {"wlan_qos.html", IssProcessWlanQosPage},
    {"wlan_profile_qos.html", IssProcessQosProfileCreatePage},
    {"wlan_profile_qos_edit.html", IssProcessQosProfileEditPage},
    {"wlan_profiles_qos.html", IssProcessQosProfilePage},
    {"wlan_capability.html", IssProcessWlanCapabilityPage},
    {"wlan_profile_capability.html", IssProcessCapabilityProfileCreatePage},
    {"wlan_profile_capability_edit.html", IssProcessCapabilityProfileEditPage},
    {"wlan_profiles_capability.html", IssProcessCapabilityProfilePage},
    {"wlan_security.html", IssProcessWlanSecurityPage},
    {"wlan_profile_authentication.html", IssProcessAuthenticationProfileCreatePage},
    {"wlan_profile_authentication_edit.html", IssProcessAuthenticationProfileEditPage},
    {"wlan_profiles_authentication.html", IssProcessAuthenticationProfilePage},
    {"wlan_rsna_config.html", IssProcessRsnaConfigPage},
    {"wlan_rsna_config_edit.html", IssProcessRsnaConfigEditPage},
    {"wlan_rsna_pairwisecipher.html", IssProcessRsnaCipherPage},
    {"wlan_rsna_authsuite.html", IssProcessRsnaAuthsuitePage},
#ifdef PMF_WANTED    
    {"wlan_rsna_protected_management.html", IssProcessRsnaProtectedMgmtPage},
#endif    
#ifdef WPA_WANTED
    {"wlan_wpa_config.html", IssProcessWpaConfigPage},
    {"wlan_wpa_config_edit.html", IssProcessWpaConfigEditPage},
    {"wlan_wpa_pairwisecipher.html", IssProcessWpaCipherPage},
    {"wlan_wpa_authsuite.html", IssProcessWpaAuthsuitePage},
#endif
#ifdef BAND_SELECT_WANTED
    {"wlan_bandselect_global.html", IssProcessWlanBandSelectGlobal},
    {"wlan_bandselect_local.html", IssProcessWlanBandSelectLocal},
    {"wlan_bandselect_clienttable.html", IssProcessWlanBandSelectClientTable},
#endif
#ifdef WPS_WANTED
    {"wlan_wps_config.html", IssProcessWpsConfigPage},
    {"wlan_wps_config_edit.html", IssProcessWpsConfigEditPage},
    {"wlan_wps_authsta.html", IssProcessWpsAuthStaPage},
#endif
    {"wireless_allap.html", IssProcessWirelessAllApConfigTablePage},
    {"wireless_ap_configure.html", IssProcessWirelessApConfigTablePage},
    {"wireless_ap_createnew_basic.html", IssProcessWirelessApCreateTablePage},
    {"wireless_ap_edit_basic.html", IssProcessWirelessApEditTablePage},
    {"wireless_ap_edit_advanced.html", IssProcessWirelessApAdvEditTablePage},
    {"wireless_80211an.html", IssProcessRadioANTablePage},
    {"wireless_80211an_configure_general.html", IssProcessGenRadioANConfigTablePage},
    {"wireless_80211an_configure_advanced.html", IssProcessAdvRadioANConfigTablePage},
    {"wireless_80211an_configure_HT_Capabilities.html", IssProcessHTRadioANConfigTablePage},
    {"wireless_80211an_configure_BeamForming.html", IssProcessBeamFormingRadioANConfigTablePage},
    {"wireless_80211an_configure_HT_Operation.html", IssProcessHTOperationRadioANConfigTablePage},
    {"wireless_80211bgn.html", IssProcessRadioBGNTablePage},
    {"wireless_80211bgn_configure_general.html", IssProcessGenRadioBGNConfigTablePage},
    {"wireless_80211bgn_configure_advanced.html", IssProcessAdvRadioBGNConfigTablePage},
    {"wireless_80211bgn_configure_HT_Capabilities.html", IssProcessHTCapabilitiesRadioBGNConfigTablePage},
    {"wireless_80211bgn_configure_BeamForming.html", IssProcessBeamFormingRadioBGNConfigTablePage},
    {"wireless_80211bgn_configure_HT_Operation.html", IssProcessHTOperationRadioBGNConfigTablePage},
    {"wireless_80211ac.html", IssProcessRadioACTablePage},
    {"wireless_80211ac_configure_general.html", IssProcessGenRadioACConfigTablePage},
    {"wireless_80211ac_configure_advanced.html", IssProcessAdvRadioACConfigTablePage},
    {"wireless_80211ac_configure_vhtoperation.html", IssProcessVHTOperConfigTablePage},
    {"wireless_80211ac_configure_vhtcapabilities.html", IssProcessVHTCapabConfigTablePage},
    {"wireless_80211ac_configure_vhtinfo.html", IssProcessVHTInfoConfigTablePage},
    {"wireless_80211ac_configure_HT_Capabilities.html", IssProcessHTRadioACConfigTablePage},
    {"wireless_80211ac_configure_HT_Operation.html", IssProcessHTOperationRadioACConfigTablePage},
    {"wireless_80211_edcasetting.html", IssProcessConfigEdcaTablePage},
    {"wireless_80211_antennaselection.html", IssProcessConfigAntennaTablePage},
    {"wireless_country.html", IssProcessApCountryTablePage},
    {"wireless_apgroup.html",IssProcessCreateApGroupTablePage},
    {"wireless_addwlan.html",IssProcessAddWlanTablePage},
    {"wireless_addap.html",IssProcessAddApTablePage},
    {"wireless_80211an_change_filter.html", IssChangeRadioANFilter},
    {"wireless_80211bgn_change_filter.html", IssChangeRadioBGNFilter},
    {"wireless_80211ac_change_filter.html", IssChangeRadioACFilter},
    {"wireless_80211an_filter.html", IssProcessRadioANFilter},
    {"wireless_80211bgn_filter.html", IssProcessRadioBGNFilter},
    {"wireless_80211ac_filter.html", IssProcessRadioACFilter},
    {"wireless_qosprofile.html", IssProcessQosProfileTablePage},
    {"wireless_qoshomepage.html", IssProcessConfigQosHomeTablePage},
    {"wireless_qosprofile_configure.html", IssProcessConfigQosProfileTablePage},
    {"wireless_qosbronzeprofile.html", IssProcessConfigQosBronzeProfile},
    {"wireless_qosgoldprofile.html", IssProcessConfigQosGoldProfile},
    {"wireless_qosplatinumprofile.html", IssProcessConfigQosPlatinumProfile},
    {"wireless_qossilverprofile.html", IssProcessConfigQosSilverProfile},
    {"monitor_radio_80211an_filter.html", IssMonitorRadioANFilter},
    {"monitor_radio_80211bn_filter.html", IssMonitorRadioBGNFilter},
    {"monitor_radio_80211ac_filter.html", IssMonitorRadioACFilter},
    {"monitor_radio_80211an_change_filter.html", IssMonitorChangeRadioANFilter},
    {"monitor_radio_80211bn_change_filter.html", IssMonitorChangeRadioBGNFilter},
    {"monitor_radio_80211ac_change_filter.html", IssMonitorChangeRadioACFilter},
    {"radio_statisticspage_80211an.html", IssRadioANStatisticsPage},
    {"radio_statisticspage_80211bn.html", IssRadioBGNStatisticsPage},
    {"radio_statisticspage_80211ac.html", IssRadioACStatisticsPage},
    {"monitor_clients.html", IssMonitorClientsPage},
    {"clients_statistics.html", IssClientsStatisticsPage},
    {"wlan_statistics.html", IssWlanStatisticsPage},
    {"ap_wlan_statistics.html", IssApWlanStatisticsPage},
    {"monitor_client.html", IssMonitorClientTablePage},
    {"monitor_clients_filter.html",IssMonitorClientsFilterPage},
    {"monitor_clients_change_filter.html",IssMonitorClientsChangeFilterPage},
    {"wireless_bindingtable.html", IssProcessWlanBindingTablePage},
    {"wireless_bindcreate.html", IssProcessWlanBindCreateTablePage},
    {"monitor_allap.html", IssProcessMonitorAllApTablePage},
    {"monitor_filterap.html", IssProcessMonitorApFilterTablePage},
    {"monitor_filter_allap.html", IssProcessMonitorFilterAllApTablePage},
    {"monitor_ap_joinstats.html", IssMonitorApJoinStatsTablePage},
    {"monitor_ap_joinstatedit.html", IssMonitorApJoinDetailedStatsTablePage},
    {"monitor_radio_80211an.html", IssMonitorRadioANTablePage},
    {"monitor_radio_80211bn.html", IssMonitorRadioBGNTablePage},
    {"monitor_radio_80211ac.html", IssMonitorRadioACTablePage},
    {"monitor_mobilitystatistics.html", IssMonitorMobilityStatsTablePage},
    {"ap_dhcp_poolconf.html" , IssDhcpPoolTablePage},
    {"ap_ip_routeconf.html" ,  IssIpRouteTablePage},
    {"ap_ivr_conf.html" ,      IssIvrTablePage},
    {"ap_general.html" ,       IssGeneralTablePage},
    {"ap_firewall.html" ,      IssFirewallTablePage},
    {"ap_ip_nat.html" ,        IssIpNatTablePage},
#ifdef RFMGMT_WANTED
    {"rfm_80211an_tpc.html", IssRfmRadioANTpcPage},
    {"rfm_80211an_dca.html", IssRfmRadioANDcaPage},
    {"rfm_80211an_dpa.html", IssRfmRadioANDpaPage},
    {"rfm_80211an_sha.html", IssRfmRadioANShaPage},
    {"rfm_11h_tpc_info.html", IssRfmTpcInfoPage},
    {"rfm_11h_tpc_request.html", IssRfmTpcRequestPage},
    {"rfm_80211an_neighbour_ap_details.html", IssRfmRadioANNbrAPDtlPage},
    {"rfm_11h_tpc_config.html", IssRfmTpcConfigPage},
    {"rfm_11h_tpc_wireless.html", IssRfmTpcWirelessPage},
    {"rfm_80211an_failed_ap_details.html", IssRfmRadioANFldAPDtlPage},
    {"rfm_80211an_client_scan_details.html", IssRfmRadioANCltScanDtlPage},
    {"rfm_80211an_client_info.html", IssRfmRadioANClientInfoPage},
    {"rfm_80211an_ap_scan_info.html", IssRfmRadioANAPScanInfoPage},
    {"rfm_80211an_neighbour_ap_config.html", IssRfmRadioANAPScanCfgPage},
    {"rfm_80211an_client_config.html", IssRfmRadioANSNRCfgPage},
    {"rfm_80211bgn_neighbour_ap_details.html", IssRfmRadioBGNNbrAPDtlPage},
    {"rfm_80211bgn_failed_ap_details.html", IssRfmRadioBGNFldAPDtlPage},
    {"rfm_80211bgn_client_scan_details.html", IssRfmRadioBGNCltScanDtlPage},
    {"rfm_80211bgn_client_info.html", IssRfmRadioBGNClientInfoPage},
    {"rfm_80211bgn_ap_scan_info.html", IssRfmRadioBGNAPScanInfoPage},
    {"rfm_80211bgn_neighbour_ap_config.html", IssRfmRadioBGNAPScanCfgPage},
    {"rfm_80211bgn_client_config.html", IssRfmRadioBGNSNRCfgPage}, 
    {"rfm_80211bgn_tpc.html", IssRfmRadioBGNTpcPage},
    {"rfm_80211bgn_dca.html", IssRfmRadioBGNDcaPage},
    {"rfm_80211bgn_dpa.html", IssRfmRadioBGNDpaPage},
    {"rfm_80211bgn_sha.html", IssRfmRadioBGNShaPage},
    {"rfm_80211g_neighbour_ap_details.html", IssRfmRadioGNbrAPDtlPage},
    {"rfm_80211g_failed_ap_details.html", IssRfmRadioGFldAPDtlPage},
    {"rfm_80211g_client_scan_details.html", IssRfmRadioGCltScanDtlPage},
    {"rfm_80211g_client_info.html", IssRfmRadioGClientInfoPage},
    {"rfm_80211g_ap_scan_info.html", IssRfmRadioGAPScanInfoPage},
    {"rfm_80211g_neighbour_ap_config.html", IssRfmRadioGAPScanCfgPage},
    {"rfm_80211g_client_config.html", IssRfmRadioGSNRCfgPage},
    {"rfm_80211g_tpc.html", IssRfmRadioGTpcPage},
    {"rfm_80211g_dca.html", IssRfmRadioGDcaPage},
    {"rfm_80211g_dpa.html", IssRfmRadioGDpaPage},
    {"rfm_80211g_sha.html", IssRfmRadioGShaPage},
    {"rfm_global.html", IssRfmGlobalPage},
#ifdef WLC_WANTED
#ifdef ROGUEAP_WANTED
    {"rogue_Basicsettings.html", IssRfmRogueBasicPage},
    {"rogue_Ruleconfig.html", IssRfmRogueRulePage},
    {"rogue_Friendlyap.html", IssRfmRogueFriendPage},
    {"rogue_Maliciousap.html", IssRfmRogueMaliciousPage},
    {"rogue_Unclassifiedap.html", IssRfmRogueUnclassifyPage},
    {"rogue_DetailedSummary.html", IssRfmRogueDetailedSummaryPage},
#endif 
#endif 
#endif

#ifdef WSSUSER_WANTED
    {"urm_basicsettings.html", IssProcessUrmBasicSettingsPage},
    {"urm_usergroup.html", IssProcessUrmUserGroupPage},
    {"urm_userrole.html" , IssProcessUrmUserRolePage},
    {"urm_usernamerestrict.html" ,IssProcessUrmUsernameRestrictPage},
    {"urm_stationMACrestrict.html"  ,IssProcessUrmStationMacRestrictPage},
    {"urm_userMACmapping.html", IssProcessUrmUserMacMappingPage},
    {"urm_sessiondetails.html", IssProcessUrmSessionDetailsPage},

    {"urm_sql_basicsettings.html",    IssProcessUrmSqlBasicPage},
    {"urm_sql_insert.html",           IssProcessUrmSqlInsertPage},
    {"urm_sql_update.html",           IssProcessUrmSqlUpdatePage},
    {"urm_sql_delete.html",           IssProcessUrmSqlDeletePage},
#endif

    {"mcast_settings.html",           IssProcessMcastSettingsPage},

#endif
#ifdef VXLAN_WANTED
    {"fsvxlan_nveinterface.html", IssProcessVxlanNveInterfacePage},
    {"fsvxlan_nve.html", IssProcessVxlanNvePage},
    {"fsvxlan_vtep.html", IssProcessVxlanVtepPage},
    {"fsvxlan_mcast.html", IssProcessVxlanMcastPage},
    {"fsvxlan_vnivlan.html", IssProcessVxlanVniVlanPage},
#endif
    {"", NULL}
};
#else
extern tSpecificPage       asIsspages[];
#endif /*_ISS_WEB_C_*/
#endif /* _ISSWEB_H */
