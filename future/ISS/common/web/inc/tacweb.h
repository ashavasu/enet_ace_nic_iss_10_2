#ifndef _TACWEB_H
#define _TACWEB_H
#ifdef TAC_WANTED

/* Low level routines for this module */
/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsTacMcastChannelDefaultBandwidth ARG_LIST((UINT4 *));

extern INT1
nmhGetFsTacTraceOption ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsTacMcastChannelDefaultBandwidth ARG_LIST((UINT4 ));

extern INT1
nmhSetFsTacTraceOption ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsTacMcastChannelDefaultBandwidth ARG_LIST((UINT4 *  ,UINT4 ));

extern INT1
nmhTestv2FsTacTraceOption ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2FsTacMcastChannelDefaultBandwidth ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1
nmhDepv2FsTacTraceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsTacMcastProfileTable. */
extern INT1
nmhValidateIndexInstanceFsTacMcastProfileTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsTacMcastProfileTable  */

extern INT1
nmhGetFirstIndexFsTacMcastProfileTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsTacMcastProfileTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsTacMcastProfileAction ARG_LIST((UINT4  , INT4 ,INT4 *));

extern INT1
nmhGetFsTacMcastProfileDescription ARG_LIST((UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsTacMcastProfileStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsTacMcastProfileAction ARG_LIST((UINT4  , INT4  ,INT4 ));

extern INT1
nmhSetFsTacMcastProfileDescription ARG_LIST((UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsTacMcastProfileStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsTacMcastProfileAction ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2FsTacMcastProfileDescription ARG_LIST((UINT4 *  ,UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2FsTacMcastProfileStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2FsTacMcastProfileTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsTacMcastPrfFilterTable. */
extern INT1
nmhValidateIndexInstanceFsTacMcastPrfFilterTable ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsTacMcastPrfFilterTable  */

extern INT1
nmhGetFirstIndexFsTacMcastPrfFilterTable ARG_LIST((UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsTacMcastPrfFilterTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsTacMcastPrfFilterMode ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhGetFsTacMcastPrfFilterStatus ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsTacMcastPrfFilterMode ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsTacMcastPrfFilterStatus ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsTacMcastPrfFilterMode ARG_LIST((UINT4 *  ,UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2FsTacMcastPrfFilterStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2FsTacMcastPrfFilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsTacMcastChannelTable. */
extern INT1
nmhValidateIndexInstanceFsTacMcastChannelTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsTacMcastChannelTable  */

extern INT1
nmhGetFirstIndexFsTacMcastChannelTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsTacMcastChannelTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsTacMcastChannelBandWidth ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

extern INT1
nmhGetFsTacMcastChannelRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsTacMcastChannelBandWidth ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

extern INT1
nmhSetFsTacMcastChannelRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsTacMcastChannelBandWidth ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

extern INT1
nmhTestv2FsTacMcastChannelRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2FsTacMcastChannelTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsTacMcastPrfStatsTable. */
extern INT1
nmhValidateIndexInstanceFsTacMcastPrfStatsTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsTacMcastPrfStatsTable  */

extern INT1
nmhGetFirstIndexFsTacMcastPrfStatsTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsTacMcastPrfStatsTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsTacMcastPrfStatsPortRefCnt ARG_LIST((UINT4  , INT4 ,UINT4 *));

extern INT1
nmhGetFsTacMcastPrfStatsVlanRefCnt ARG_LIST((UINT4  , INT4 ,UINT4 *));

VOID
IssProcessTacProfilePage (tHttp * pHttp);

VOID
IssProcessTacProfileGet (tHttp * pHttp);

VOID
IssProcessTacProfileSet (tHttp * pHttp);

VOID
IssProcessTacPrfFilterPage (tHttp * pHttp);

VOID
IssProcessTacPrfFilterGet (tHttp * pHttp);

VOID
IssProcessTacPrfFilterSet (tHttp * pHttp);

VOID
IssProcessTacStatsPage (tHttp * pHttp);

extern INT4 FsTacMcastProfileActionSet(tSnmpIndex *, tRetVal *);
extern INT4 FsTacMcastProfileDescriptionSet(tSnmpIndex *, tRetVal *);
extern INT4 FsTacMcastProfileStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsTacMcastPrfFilterModeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsTacMcastPrfFilterStatusSet(tSnmpIndex *, tRetVal *);

#endif
#endif
