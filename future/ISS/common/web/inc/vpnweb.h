
/* $Id: vpnweb.h,v 1.6 2014/03/16 11:29:01 siva Exp $*/

#define  WEB_SEC_FILTER                          1 
#define  WEB_SEC_ALLOW                           2 
#define  WEB1_SEC_APPLY                           3 
#define  WEB_SEC_BYPASS                          4 
#define  WEB_SEC_APPLY                      3 
#define  WEB_SEC_HMACSHA1                        2 

#define WEB_MAX_ARGS 10
#define WEB_MAX_INTF_LEN 10
#define  WEB_MAX_NAME_LENGTH 64

#define VPN_FAILURE OSIX_FAILURE
#define VPN_SUCCESS OSIX_SUCCESS


#define WEB_VPN_CREATE_NEW_CRYPTO_MAP    1
#define WEB_VPN_DEL_CRYPTOMAP            2
#define WEB_VPN_SET_MODE                 3
#define WEB_VPN_SET_PEER_IP              4
#define WEB_VPN_SET_CRYPTO_PARAMS        5
#define WEB_VPN_SET_ACCESS_PARAMS        6
#define WEB_VPN_SET_POLICY_TO_INTERFACE  7
#define WEB_VPN_SET_POLICY_TYPE          8
#define WEB_SHOW_VPN_POLICY              9
#define WEB_VPN_IPSEC_PROPOSAL           10
#define WEB_VPN_IKE_PROPOSAL             11
#define WEB_VPN_ISAKMP_IDENTITY          12 
#define WEB_VPN_PRESHARED_KEY            13
#define WEB_PRESHARED_MAX_KEY_SIZE       31
#define WEB_VPN_MAX_NAME_LEN             63


#define WEB_VPN_IPSEC_MANUAL            1
#define WEB_VPN_PRESHARED               2 
#define WEB_VPN_CERT                     3
#define WEB_VPN_XAUTH                    4
#define WEB_TUNNEL                      "1"
#define WEB_VPN_TUNNEL                   1
#define WEB_TRANSPORT                   "2"
#define WEB_VPN_TRANSPORT                2
#define WEB_AH_VALUE                    "51"
#define WEB_ESP_VALUE                   "50"
#define WEB_HMAC_MD5_VALUE              "1"
#define WEB_HMAC_SHA1_VALUE             "2"
#define WEB_HMAC_MD5                     1
#define WEB_HMAC_SHA1                    2
#define WEB_DES_VALUE                   "4"
#define WEB_3DES_VALUE                  "5"
#define WEB_AES_128_VALUE               "12"
#define WEB_AES_192_VALUE               "13"
#define WEB_AES_256_VALUE               "14" 
#define WEB_NULL                        11
#define WEB_DES                         4
#define WEB_3DES                        5
#define WEB_AES_128                     12
#define WEB_AES_192                     13
#define WEB_AES_256                     14 
#define WEB_AR_ENABLE                   1
#define WEB_AR_DISABLE                  2
#define WEB_TCP                          6 
#define WEB_UDP                         17 
#define WEB_ANY_PROTO                   9000
#define WEB_AH                          51
#define WEB_ESP                         50
#define WEB_ICMP                        1
#define WEB_TCP_VALUE                   "6" 
#define WEB_UDP_VALUE                   "17" 
#define WEB_ANY_PROTO_VALUE             "9000"
#define WEB_ICMP_VALUE                  "1"
#define WEB_ANTI_REPLAY_ENABLE          1
#define WEB_VPN_KEY_MAX_LEN             68
#define WEB_VPN_3DES_KEY_LEN            50
#define VPN_POLICY_ENABLE               1
#define VPN_POLICY_DISABLE              2
enum {
    VPN_WEB_WAN_TYPE_PRIVATE = 1,
    VPN_WEB_WAN_TYPE_PUBLIC,
    VPN_WEB_WAN_TYPE_ALL
};
enum { 
    VPN_WEB_NETWORK_TYPE_LAN = 1,
    VPN_WEB_NETWORK_TYPE_WAN,
    VPN_WEB_NETWORK_TYPE_ALL 
};

#define  VPN_WEB_ETH_TYPE   0x01
#define  VPN_WEB_VLAN_TYPE  0x02
#define  VPN_WEB_PPP_TYPE   0x04
#define  VPN_WEB_MP_TYPE    0x08


/* Values for KeyID Type */
#define WEB_VPN_IKE_KEY_IPV4                 1
#define WEB_VPN_IKE_KEY_IPV6                 2 


#define WEB_VPN_LIFETIME_SECS            "1"
#define WEB_VPN_LIFETIME_KB              "2"

#define WEB_VPN_LIFETIME_MINS            "3"
#define WEB_VPN_LIFETIME_HRS             "4"
#define WEB_VPN_LIFETIME_DAYS            "5"
#define WEB_VPN_NO_LIFETIME              "6"
#define WEB_VPN_NO_LIFETIME_KB           "7" 

/* IKE Group Descriptipns */
#define WEB_VPN_IKE_DH_GROUP_1               1      /* 768-bit MODP */
#define WEB_VPN_IKE_DH_GROUP_2               2      /* 1024-bit MODP group */
#define WEB_VPN_IKE_DH_GROUP_3               3      /* 1024-bit MODP group */
#define WEB_VPN_IKE_DH_GROUP_5               5      /* 1536-bit MODP group */
/* IKE Exchange modes */
#define WEB_VPN_IKE_MAIN_MODE         2 
#define WEB_VPN_IKE_AGGRESSIVE_MODE   4
/* Lifetime Types */
#define WEB_LIFETIME_SECS            1
#define WEB_LIFETIME_KB              2
/* Propreitary */
#define WEB_LIFETIME_MINS            3
#define WEB_LIFETIME_HRS             4
#define WEB_LIFETIME_DAYS            5

#define WEB_CLI_HANDLE 1

enum { 
    VPN_POLICY_PAGE = 0,
    VPN_IPSEC_PAGE ,
    VPN_IKE_PAGE ,
    VPN_IKE_DIG_CERT_PAGE ,
    VPN_IKE_XAUTH_PAGE
};

#ifdef __VPNWEB_C__
VOID
VpnPrintPolicies (tHttp * pHttp ,UINT1 *pu1PolicyName, 
                  UINT1 *pu1TempPolicyName,UINT1 u1PolicyPage);

VOID
VpnGetAddressPoolInfo (tHttp * pHttp, 
                       tSNMP_OCTET_STRING_TYPE *pVpnRaAddressPoolName);
INT4
VpnGetRaPoolInfo (UINT4 *pu4PoolNetwork, UINT4 *pu4PoolNetMask);

VOID VpnPrintAvailableRemIdValues(tHttp *pHttp);
#endif
