/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: ecfmweb.h,v 1.17 2014/02/26 12:12:22 siva Exp $
*
* Description:  macros,typedefs and prototypes for ISS web Module
*******************************************************************/
#ifndef _ECFMWEB_H
#define _ECFMWEB_H
#ifdef ECFM_WANTED

/* Macros to register, unregister ECFM CC/LBLT task with WEB */
#define ISS_ECFM_REGISTER_WEB_CC_LOCK()\
do{\
WebnmRegisterLock (pHttp, EcfmCcLock, EcfmCcUnLock);\
ECFM_CC_LOCK();\
}while(0);

#define ISS_ECFM_REGISTER_WEB_LBLT_LOCK()\
do{\
WebnmRegisterLock (pHttp, EcfmLbLtLock, EcfmLbLtUnLock);\
ECFM_LBLT_LOCK();\
}while(0);

#define ISS_ECFM_UNREGISTER_WEB_CC_LOCK()\
do{\
ECFM_CC_UNLOCK();\
WebnmUnRegisterLock (pHttp);\
}while(0);

#define ISS_ECFM_UNREGISTER_WEB_LBLT_LOCK()\
do{\
ECFM_LBLT_UNLOCK();\
    WebnmUnRegisterLock (pHttp);\
}while(0);

#define ISS_ECFM_GET_4BYTE(u4Val, pu1Buf)\
    do{\
        MEMCPY (&u4Val, pu1Buf, 4);\
        pu1Buf += 4;\
        u4Val = (UINT4) (OSIX_NTOHL(u4Val));\
    }while(0);

#define ISS_ECFM_GET_2BYTE(u2Val, pu1Buf)\
    do{\
        MEMCPY (&u2Val, pu1Buf, 2);\
        pu1Buf += 2;\
        u2Val = (UINT2) (OSIX_NTOHS(u2Val));\
    }while(0);
#define ISS_ECFM_PUT_4BYTE(pu1Buf, u4Val)\
    do {     \
        u4Val = OSIX_HTONL(u4Val);   \
        MEMCPY (pu1Buf, &u4Val, 4); \
        u4Val = OSIX_NTOHL(u4Val); \
        pu1Buf += 4;  \
    }while(0);

#define ISS_ECFM_IS_VLAN_ALREADY_PRINTED(au1Array, i4Member, b1Result) \
do\
{\
   UINT2 u2BytePos;\
   UINT2 u2BitPos;\
   UINT1 au1BitMaskMap[8] =\
   { 0x01, 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02 };\
   u2BytePos = (UINT2)(i4Member / 8);\
   u2BitPos  = (UINT2)(i4Member % 8);\
   if (u2BitPos  == 0)\
   {\
       u2BytePos = (UINT2)(u2BytePos - 1);\
   }\
   if ((au1Array[u2BytePos] & au1BitMaskMap[u2BitPos]) != 0)\
   {\
       b1Result = ISS_TRUE;\
   }\
   else\
   {\
       b1Result = ISS_FALSE; \
   }\
}while(0);

#define ISS_ECFM_SET_VLAN_PRINTED(au1Array, i4Member) \
do\
{\
   UINT2 u2BytePos;\
   UINT2 u2BitPos;\
   UINT1 au1BitMaskMap[8] =\
   { 0x01, 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02 };\
   u2BytePos = (UINT2)(i4Member / 8);\
   u2BitPos  = (UINT2)(i4Member % 8);\
   if (u2BitPos  == 0) {u2BytePos = (UINT2)(u2BytePos - 1);} \
   au1Array[u2BytePos] = (UINT1)(au1Array[u2BytePos] | au1BitMaskMap[u2BitPos]);\
}while(0);

#define ISS_ECFM_SET_USE_FDB_ONLY(u1Flags)    (u1Flags=u1Flags|0x80)
#define ISS_ECFM_GET_USE_FDB_ONLY(u1Flags)    ((u1Flags&(0x01<<7))>>7)
#define ISS_ECFM_MAC_ADDR_ARRAY_LEN 20
#define ISS_ECFM_MD_MA_NAME_MAX_LEN 20
#define ISS_ECFM_MD_MA_NAME_ARRAY_SIZE             (ISS_ECFM_MD_MA_NAME_MAX_LEN)+1  
#define ISS_ECFM_LTR_CACHE_SIZE_MAX 4095
#define ISS_ECFM_DEF_TABLE_SIZE 1000
#define ISS_ECFM_PORT_ID_LENGTH 255
#define ISS_ECFM_MAC_ADDR_LENGTH 6
#define ISS_ECFM_EGRESS_ID_LENGTH 8
#define ISS_ECFM_TRANSMIT_PDU 2
#define ISS_ECFM_STOP_PDU 3
#define ISS_ECFM_DEF_MD_LEVEL_DEFAULT_VAL -1
#define ISS_ECFM_MHF_CRITERIA_DEFER 4
#define ISS_ECFM_SENDER_ID_DEFER 5
#define ISS_ECFM_INVALID_VALUE -1
#define ISS_ECFM_MA_ICC_MAX_LEN 6
#define ISS_ECFM_MA_ICC_ARRAY_SIZE              (ISS_ECFM_MA_ICC_MAX_LEN)+1  
#define ISS_ECFM_MA_UMC_MAX_LEN 6
#define ISS_ECFM_MA_UMC_ARRAY_SIZE              (ISS_ECFM_MA_UMC_MAX_LEN)+1  
#define ISS_ECFM_CC_ROLE_PM  1 
#define ISS_ECFM_INTERVAL_TYPE_SEC 3 
#define ISS_ECFM_INTERVAL_TYPE_MSEC  2 
#define ISS_ECFM_DEST_TYPE_UNICAST 0
#define ISS_ECFM_DEST_TYPE_MEPID 1
#define ISS_ECFM_DEST_TYPE_MULTICAST 2
#define ISS_ECFM_TLV_TYPE_NONE 0
#define ISS_ECFM_TLV_TYPE_DATA 1
#define ISS_ECFM_TLV_TYPE_TEST 2
#define ISS_ECFM_LBR_WITH_BAD_MSDU 1
#define ISS_ECFM_LBR_WITH_BIT_ERR 2
#define ISS_ECFM_PORT_FILTERING_ACTION_ING_OK      1
#define ISS_ECFM_PORT_FILTERING_ACTION_ING_BLOCKED 2
#define ISS_ECFM_PORT_FILTERING_ACTION_ING_VID     3
#define ISS_ECFM_LTR_RLY_HIT                    1
#define ISS_ECFM_LTR_RLY_FDB                    2
#define ISS_ECFM_LTR_RLY_MPDB                   3
#define ISS_ECFM_PORT_FILTERING_ACTION_EGR_OK       1
#define ISS_ECFM_PORT_FILTERING_ACTION_EGR_DOWN     2
#define ISS_ECFM_PORT_FILTERING_ACTION_EGR_BLOCKED  3
#define ISS_ECFM_PORT_FILTERING_ACTION_EGR_VID      4
#define ISS_ECFM_TH_BURST_TYPE_MSG 1
#define ISS_ECFM_TH_BURST_TYPE_DEADLINE 2
#define ISS_ECFM_MEP_INDEX_ARRAY_SIZE             41  
#define ISS_ECFM_CONTEXT_INDEX_ARRAY_SIZE         11 
#define ISS_ECFM_DETAIL_PAGE 1  
#define ISS_ECFM_BRIEF_PAGE  2
#define ISS_ECFM_DATE_ARRAY_SIZE          64
#define ISS_ECFM_MAX_MD_LEVEL             8
#define ISS_ECFM_VLAN_BITMAP_SIZE             512
#define ISS_ECFM_DM_TYPE_1DM 1
#define ISS_ECFM_TRANS_NOT_READY 1
#define ISS_ECFM_MAX_DATA_PATTERN_SIZE 8970
#define ISS_ECFM_PDU_SIZE_WTIH_TST_TLV 31
#define ISS_ECFM_PDU_SIZE_WTIH_DATA_TLV 30
#define ISS_ECFM_PDU_SIZE_WTIHOUT_TLV 27
#define ISS_ECFM_VLAN_TAG_SIZE 4
#define ISS_ECFM_NUM_OF_MSEC_IN_A_NSEC 1000000
#define ISS_ECFM_NUM_OF_MSEC_IN_A_SEC 1000
#define ISS_ECFM_1BYTE_SIZE 8
#define ISS_ECFM_2BYTE_SIZE 16
#define ISS_ECFM_MASK_WITH_VAL_1 0x01
#define ISS_ECFM_MASK_WITH_VAL_255 0x00ff
#define ISS_ECFM_NUM_OF_MSEC_IN_A_INTERVAL 10
#define ISS_ECFM_ERROR_CCM_DEFECT 1
#define ISS_ECFM_XCON_CCM_DEFECT 2
#define ISS_ECFM_REMOTE_CCM_DEFECT 3
#define ISS_ECFM_MAC_STATUS_DEFECT 4
#define ISS_ECFM_RDI_DEFECT 5
#define ISS_Y1731_RDI_DEFECT          15
#define ISS_Y1731_LOC_DEFECT          14
#define ISS_Y1731_UNEXP_PERIOD_DEFECT 13
#define ISS_Y1731_UNEXP_MEP_DEFECT    12
#define ISS_Y1731_MISMERGE_DEFECT     11
#define ISS_Y1731_UNEXP_LEVEL_DEFECT  10
#define ISS_Y1731_LL_FAIL_DEFECT      9
#define ISS_Y1731_HW_FAIL_DEFECT      8
#define ISS_Y1731_SW_FAIL_DEFECT      7
#define ISS_Y1731_AIS_COND_DEFECT 1
#define ISS_Y1731_LCK_COND_DEFECT 0
#define ISS_ECFM_RDI_TRAP 6
#define ISS_ECFM_MAC_STATUS_TRAP 5
#define ISS_ECFM_MEP_MISSING_TRAP 4
#define ISS_ECFM_ERR_CCM_TRAP 3
#define ISS_ECFM_XCONN_CCM_TRAP 2
#define ISS_Y1731_LCK_TRAP 1
#define ISS_Y1731_AIS_TRAP 2
#define ISS_Y1731_HW_TRAP 3
#define ISS_Y1731_SW_TRAP 4
#define ISS_Y1731_LL_TRAP 5
#define ISS_Y1731_UNEXP_LEVEL_TRAP 6
#define ISS_Y1731_MISMERGE_TRAP 7
#define ISS_Y1731_UNEXP_MEP_TRAP 8
#define ISS_Y1731_UNEXP_PERIOD_TRAP 9
#define ISS_Y1731_LOC_TRAP 10
#define ISS_Y1731_RDI_TRAP 11
#define ISS_Y1731_FD_TRAP 12
#define ISS_Y1731_FL_TRAP 13
#define ISS_Y1731_BIT_ERR_TRAP 14

#define ISS_ECFM_PUT_VAL_1 1
#define ISS_ECFM_DEF_SIZE  5

typedef struct tEcfmVlanListRange
{
    UINT4 u4PortFrom;
    UINT4 u4PortTo;
} tEcfmVlanListRange;


typedef struct tEcfmVlanPortList
{ 
    union 
    {
        tEcfmVlanListRange    VlanListRange; 
        UINT4             u4Vlan;
    } uVlanList;
} tEcfmVlanPortList;

#define ECFM_VLAN_LIST_DELIMIT     ","
#define ECFM_VLAN_VALIDPORT_LIST           "0123456789,-"
#define ECFM_VLAN_LIST_TYPE_VAL    1
#define ECFM_VLAN_LIST_TYPE_RANGE  2
#define ECFM_LIST_TYPE_INV    255
#define ECFM_LIST_TYPE_VAL    1
#define ECFM_RANGE_DELIMIT    "-"
#define VLAN_ZERO                0
#define ECFM_LIST_TYPE_RANGE  2

#define ECFM_VLAN_PER_BYTE                  8

VOID IssProcessEcfmStatsPageGet (tHttp * pHttp);
VOID IssProcessEcfmStatsPageSet (tHttp * pHttp);
VOID IssProcessEcfmMdConfPage (tHttp * pHttp);
VOID IssProcessEcfmMdConfPageGet (tHttp * pHttp);
VOID IssProcessEcfmMdConfPageSet (tHttp * pHttp);
VOID IssProcessEcfmMaConfPage (tHttp * pHttp);
VOID IssProcessEcfmMaConfPageGet (tHttp * pHttp);
VOID IssProcessEcfmMaConfPageSet (tHttp * pHttp);
VOID IssProcessEcfmMepConfPage (tHttp * pHttp);
VOID IssProcessEcfmMepConfPageGet (tHttp * pHttp);
VOID IssProcessEcfmMepConfPageSet (tHttp * pHttp);
VOID IssProcessEcfmMipConfPage (tHttp * pHttp);
VOID IssProcessEcfmPortConfPage (tHttp * pHttp);
VOID IssProcessEcfmPortConfPageGet (tHttp * pHttp);
VOID IssProcessEcfmPortConfPageSet (tHttp * pHttp);
VOID IssProcessEcfmMipConfPageGet (tHttp * pHttp);
VOID IssProcessEcfmMipConfPageSet (tHttp * pHttp);
VOID IssProcessEcfmLbmConfPage (tHttp * pHttp);
VOID IssProcessEcfmLtmConfPage (tHttp * pHttp);
VOID IssProcessEcfmVlanConfPage (tHttp * pHttp);
VOID IssProcessEcfmVlanConfPageGet (tHttp * pHttp);
VOID IssProcessEcfmVlanConfPageSet (tHttp * pHttp);
VOID IssProcessEcfmRMepListPage (tHttp * pHttp);
VOID IssProcessEcfmLtrCachePage (tHttp * pHttp);
VOID IssProcessEcfmErrorsPage (tHttp * pHttp);
VOID IssProcessEcfmErrorsPageGet (tHttp * pHttp);
VOID IssProcessEcfmErrorsPageSet (tHttp * pHttp);
VOID IssProcessEcfmPortStatsPage (tHttp * pHttp);
VOID IssProcessEcfmPortStatsPageSet (tHttp * pHttp);
VOID IssProcessEcfmPortStatsPageGet (tHttp * pHttp);
VOID IssProcessEcfmStatsPage (tHttp * pHttp);
VOID IssProcessEcfmMepStatsPage (tHttp * pHttp);
VOID IssProcessEcfmMepStatsPageGet (tHttp * pHttp);
VOID IssProcessEcfmMepStatsPageSet (tHttp * pHttp);
VOID IssProcessEcfmMipCcmDbPage(tHttp * pHttp);
VOID IssProcessEcfmDisplayLtrRcvPage (tHttp *pHttp);
VOID IssProcessEcfmDefMdConfPage (tHttp * pHttp);
VOID IssProcessEcfmDefMdConfPageGet (tHttp * pHttp);
VOID IssProcessEcfmDefMdConfPageSet (tHttp * pHttp);
VOID IssProcessEcfmShowMaMepsPage (tHttp * pHttp);
VOID IssProcessEcfmDisplayLbrRcvPage (tHttp * pHttp);
VOID IssPrintAvailableInterfaces (tHttp * pHttp);
VOID IssProcessEcfmMiscConfPage (tHttp * pHttp);
VOID IssProcessEcfmMiscConfPageGet (tHttp * pHttp);
VOID IssProcessEcfmMiscConfPageSet (tHttp * pHttp);
VOID IssProcessEcfmGlobalConfPage (tHttp * pHttp);
VOID IssProcessEcfmGlobalConfPageGet (tHttp * pHttp);
VOID IssProcessEcfmGlobalConfPageSet (tHttp * pHttp);
VOID IssPrintAvailableDomains (tHttp * pHttp);
VOID IssPrintCurrentContext (tHttp * pHttp);
VOID IssProcessEcfmMepDetailsPage (tHttp * pHttp);
VOID IssPrintAvailableLevel (tHttp * pHttp);
VOID IssPrintAvailableMepId (tHttp * pHttp);
VOID IssPrintAvailableVlan (tHttp * pHttp);
VOID IssProcessEcfmConfPdusPage (tHttp * pHttp);
VOID IssProcessEcfmShowBuffPage (tHttp * pHttp);
VOID IssPrintTraps (tHttp *, UINT4);
VOID IssPrintClntMegLevel (tHttp *, INT4);
VOID IssProcessEcfmShowErrLogPage (tHttp *);
VOID IssProcessEcfmShowErrLogPageGet (tHttp *);
VOID IssProcessEcfmShowErrLogPageSet (tHttp *);
VOID IssProcessEcfmBuffHomePage (tHttp *);
VOID IssProcessEcfmShowLbDetailPage (tHttp *);
VOID IssProcessEcfmShowLbDetailPageGet (tHttp *);
VOID IssProcessEcfmShowLbBriefPage (tHttp *);
VOID IssProcessEcfmShowLbBriefPageGet (tHttp *);
VOID IssProcessEcfmClearLbCachePage (tHttp *, UINT1);
VOID IssProcessEcfmShowFdDetailPage (tHttp *);
VOID IssProcessEcfmShowFdDetailPageGet (tHttp *);
VOID IssProcessEcfmShowFdBriefPage (tHttp *);
VOID IssProcessEcfmShowFdBriefPageGet (tHttp *);
VOID IssProcessEcfmClearFdBufferPage (tHttp *, UINT1);
VOID IssProcessEcfmShowFlDetailPage (tHttp *);
VOID IssProcessEcfmShowFlDetailPageGet (tHttp *);
VOID IssProcessEcfmShowFlBriefPage (tHttp *);
VOID IssProcessEcfmShowFlBriefPageGet (tHttp *);
VOID IssProcessEcfmClearFlBufferPage (tHttp *, UINT1);
VOID IssProcessEcfmShowLtrCachePage (tHttp *);
VOID IssProcessEcfmShowLtrCachePageGet (tHttp *);
VOID IssProcessEcfmClearLtrCachePage (tHttp *);
VOID IssProcessEcfmConfPduHomePage (tHttp *);
VOID IssProcessEcfmConfCcmPage (tHttp *);
VOID IssProcessEcfmConfFdPage (tHttp *);
VOID IssProcessEcfmConfFlPage (tHttp *);
VOID IssProcessEcfmConfTstPage (tHttp *);
VOID IssProcessEcfmConfLbmPage (tHttp *);
VOID IssProcessEcfmConfAisPage (tHttp *);
VOID IssProcessEcfmConfLckPage (tHttp *);
VOID IssProcessEcfmConfLtmPage (tHttp *);
VOID IssProcessEcfmConfThPage (tHttp *);
VOID IssProcessEcfmMepDetailConfPage (tHttp *);
VOID IssProcessEcfmMepDetailConfPageGet (tHttp *);
VOID IssProcessEcfmMepDetailConfPageSet (tHttp *);
VOID IssPrintCurrentDefects (tHttp *, UINT4, UINT4, UINT4, UINT4);
VOID IssProcessEcfmConfAvlbltyPage (tHttp *);

extern VOID EcfmUtilEpochToDate (UINT4, UINT1 *);

extern INT1 nmhGetFirstIndexFsMIEcfmContextTable(UINT4 *);
extern INT1 nmhGetNextIndexFsMIEcfmContextTable(UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmContextName(UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFirstIndexFsMIEcfmMdTable(UINT4 *, UINT4 *);
extern INT1 nmhGetFsMIEcfmSystemControl(UINT4, INT4 *);
extern INT1 nmhTestv2FsMIEcfmDefaultMdLevel(UINT4 *, UINT4, INT4, INT4);
extern INT1 nmhSetFsMIEcfmDefaultMdLevel(UINT4, INT4, INT4);
extern INT1 nmhTestv2FsMIEcfmDefaultMdMhfCreation(UINT4 *, UINT4, INT4, INT4);
extern INT1 nmhSetFsMIEcfmDefaultMdMhfCreation(UINT4, INT4, INT4);
extern INT1 nmhTestv2FsMIEcfmDefaultMdIdPermission(UINT4 *, UINT4, INT4, INT4);
extern INT1 nmhSetFsMIEcfmDefaultMdIdPermission(UINT4, INT4, INT4);
extern INT1 nmhGetFirstIndexFsMIEcfmDefaultMdTable(UINT4 *, INT4 *);
extern INT1 nmhGetNextIndexFsMIEcfmDefaultMdTable(UINT4, UINT4 *, INT4, INT4*);
extern INT1 nmhGetFsMIEcfmDefaultMdIdPermission(UINT4, INT4, INT4*);
extern INT1 nmhGetFsMIEcfmDefaultMdMhfCreation(UINT4, INT4, INT4*);
extern INT1 nmhGetFsMIEcfmDefaultMdLevel(UINT4, INT4, INT4 *);
extern INT1 nmhGetNextIndexFsMIEcfmMdTable(UINT4, UINT4 *, UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmMdName(UINT4, UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetFsMIEcfmMdName(UINT4, UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2FsMIEcfmMdName(UINT4*, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsMIEcfmMdMdLevel(UINT4, UINT4, INT4 *);
extern INT1 nmhSetFsMIEcfmMdMdLevel(UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIEcfmMdMdLevel(UINT4 *, UINT4, UINT4, INT4);
extern INT1 nmhGetFsMIEcfmMdMhfCreation(UINT4, UINT4, INT4 *);
extern INT1 nmhSetFsMIEcfmMdMhfCreation(UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIEcfmMdMhfCreation(UINT4 *, UINT4, UINT4, INT4);
extern INT1 nmhGetFsMIEcfmMdMhfIdPermission(UINT4, UINT4, INT4 *);
extern INT1 nmhSetFsMIEcfmMdMhfIdPermission(UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIEcfmMdMhfIdPermission(UINT4*, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIEcfmMdRowStatus(UINT4 *, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmMdRowStatus(UINT4, UINT4, INT4);
extern INT1 nmhGetFsMIEcfmMdRowStatus(UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIEcfmMdTableNextIndex(UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmMepArchiveHoldTime(UINT4, UINT4, INT4 *);
extern INT1 nmhSetFsMIEcfmMepArchiveHoldTime(UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIEcfmMepArchiveHoldTime(UINT4*, UINT4, UINT4, INT4);
extern INT1 nmhGetFirstIndexFsMIEcfmMaTable(UINT4 *, UINT4 *, UINT4 *);
extern INT1 nmhGetNextIndexFsMIEcfmMaTable(UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmMdMaTableNextIndex(UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmMaName(UINT4, UINT4,  UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsMIEcfmMaPrimaryVlanId(UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIEcfmMaNumberOfVids(UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmMaMhfCreation(UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIEcfmMaIdPermission(UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIEcfmMaCcmInterval(UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIEcfmMaCrosscheckStatus(UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIEcfmMaRowStatus(UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhTestv2FsMIEcfmMaRowStatus(UINT4 *, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIEcfmMaPrimaryVlanId(UINT4 *, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIEcfmMaMhfCreation(UINT4 *, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIEcfmMaIdPermission(UINT4 *, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIEcfmMaCcmInterval(UINT4 *, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIEcfmMaName(UINT4 *, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetFsMIEcfmMaRowStatus(UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmMaPrimaryVlanId(UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmMaMhfCreation(UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmMaIdPermission(UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmMaCcmInterval(UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmMaName(UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2FsMIEcfmMaCrosscheckStatus(UINT4 *, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmMaCrosscheckStatus(UINT4, UINT4, UINT4, INT4);
extern INT1 nmhGetFsMIEcfmMepCciSentCcms(UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmMepLbrOut(UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmMepLbrIn(UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmMepLbrBadMsdu(UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmMepUnexpLtrIn(UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmMemoryFailureCount(UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmBufferFailureCount(UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmUpCount(UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmDownCount(UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmNoDftCount(UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmRdiDftCount(UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmMacStatusDftCount(UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmRemoteCcmDftCount(UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmErrorCcmDftCount(UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmXconDftCount(UINT4, UINT4 *);
extern INT1 nmhGetFirstIndexFsMIEcfmMepTable(UINT4 *, UINT4*, UINT4 *, UINT4*);
extern INT1 nmhGetNextIndexFsMIEcfmMepTable(UINT4, UINT4*, UINT4, UINT4*,
UINT4, UINT4 *, UINT4, UINT4*);
extern INT1 nmhGetNextIndexFsMIEcfmPortTable(INT4, INT4*);
extern INT1 nmhGetFsMIEcfmPortLLCEncapStatus(INT4, INT4 *);
extern INT1 nmhGetFsMIEcfmPortModuleStatus(INT4, INT4 *);
extern INT1 nmhTestv2FsMIEcfmPortLLCEncapStatus(UINT4 *, INT4, INT4);
extern INT1 nmhSetFsMIEcfmPortLLCEncapStatus(INT4, INT4);
extern INT1 nmhTestv2FsMIEcfmPortModuleStatus(UINT4 *, INT4, INT4);
extern INT1 nmhSetFsMIEcfmPortModuleStatus(INT4, INT4);
extern INT1 nmhGetFsMIEcfmPortTxCfmPduCount(INT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmPortTxCcmCount(INT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmPortTxLbmCount(INT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmPortTxLtmCount(INT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmPortTxLbrCount(INT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmPortTxLtrCount(INT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmPortRxCfmPduCount(INT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmPortRxCcmCount(INT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmPortRxLbmCount(INT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmPortRxLtmCount(INT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmPortRxLbrCount(INT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmPortRxLtrCount(INT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmPortRxBadCfmPduCount(INT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmPortFrwdCfmPduCount(INT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmPortDsrdCfmPduCount(INT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmMepPrimaryVid(UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIEcfmMepDirection(UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIEcfmMepCcmLtmPriority(UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIEcfmMepActive(UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIEcfmMepCciEnabled(UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhTestv2FsMIEcfmMepRowStatus(UINT4 *, UINT4, UINT4, UINT4, UINT4,INT4);
extern INT1 nmhSetFsMIEcfmMepRowStatus(UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhGetFsMIEcfmVlanPrimaryVid(UINT4, INT4, INT4 *);
extern INT1 nmhTestv2FsMIEcfmMepDirection(UINT4 *, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmMepDirection(UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIEcfmMepCcmLtmPriority(UINT4 *, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmMepCcmLtmPriority(UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIEcfmMepCciEnabled(UINT4 *, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmMepCciEnabled(UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIEcfmMepPrimaryVid(UINT4 *, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmMepPrimaryVid(UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIEcfmMepActive(UINT4 *, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmMepActive(UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhGetNextIndexFsMIEcfmMipTable(INT4, INT4*, INT4, INT4*, INT4, INT4 *);
extern INT1 nmhTestv2FsMIEcfmMipRowStatus(UINT4 *, INT4, INT4, INT4, INT4);
extern INT1 nmhSetFsMIEcfmMipRowStatus(INT4, INT4, INT4, INT4);
extern INT1 nmhGetFirstIndexFsMIEcfmVlanTable(UINT4 *, INT4 *);
extern INT1 nmhTestv2FsMIEcfmVlanRowStatus(UINT4 *, UINT4, INT4, INT4);
extern INT1 nmhSetFsMIEcfmVlanRowStatus(UINT4, INT4, INT4);
extern INT1 nmhTestv2FsMIEcfmVlanPrimaryVid(UINT4 *, UINT4, INT4, INT4);
extern INT1 nmhSetFsMIEcfmVlanPrimaryVid(UINT4, INT4, INT4);
extern INT1 nmhGetFirstIndexFsMIEcfmMipCcmDbTable(UINT4 *, UINT4 *, tMacAddr *);
extern INT1 nmhGetFsMIEcfmMipCcmIfIndex(UINT4, UINT4, tMacAddr, INT4 *);
extern INT1 nmhGetNextIndexFsMIEcfmMipCcmDbTable(UINT4, UINT4 *, UINT4, UINT4 *, tMacAddr, tMacAddr *);
extern INT1 nmhGetNextIndexFsMIEcfmVlanTable(UINT4, UINT4 *, INT4, INT4 *);
extern INT1 nmhGetFsMIEcfmMipActive(INT4, INT4, INT4, INT4*);
extern INT1 nmhTestv2FsMIEcfmMipActive(UINT4 *, INT4, INT4, INT4, INT4);
extern INT1 nmhSetFsMIEcfmMipActive(INT4, INT4, INT4, INT4);
extern INT1 nmhGetFirstIndexFsMIEcfmLtrTable(UINT4 *, UINT4*, UINT4 *, UINT4 *,UINT4 *, UINT4*);
extern INT1 nmhGetFsMIEcfmLtmTargetMacAddress(UINT4, UINT4, UINT4, UINT4, UINT4, tMacAddr *);
extern INT1 nmhGetFsMIEcfmLtrIngress(UINT4, UINT4, UINT4, UINT4, UINT4, UINT4,INT4*);
extern INT1 nmhGetFsMIEcfmLtrRelay(UINT4, UINT4, UINT4, UINT4, UINT4, UINT4,INT4*);
extern INT1 nmhGetFsMIEcfmLtrForwarded(UINT4, UINT4, UINT4, UINT4, UINT4, UINT4,INT4*);
extern INT1 nmhGetFsMIEcfmLtrTerminalMep(UINT4, UINT4, UINT4, UINT4, UINT4, UINT4,INT4*);
extern INT1 nmhGetFsMIEcfmLtrIngressMac(UINT4, UINT4, UINT4, UINT4, UINT4,UINT4, tMacAddr *);
extern INT1 nmhGetFsMIEcfmLtrNextEgressIdentifier(UINT4, UINT4, UINT4, UINT4,UINT4, UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2FsMIEcfmMepTransmitLtmTargetIsMepId(UINT4 *, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmMepTransmitLtmTargetIsMepId(UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIEcfmMepTransmitLtmTargetMepId(UINT4 *,UINT4, UINT4,UINT4,UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmMepTransmitLtmTargetMepId(UINT4, UINT4, UINT4,UINT4,UINT4);
extern INT1 nmhTestv2FsMIEcfmMepTransmitLtmTargetMacAddress(UINT4 *, UINT4, UINT4, UINT4, UINT4, tMacAddr);
extern INT1 nmhSetFsMIEcfmMepTransmitLtmTargetMacAddress(UINT4, UINT4, UINT4, UINT4, tMacAddr);
extern INT1 nmhTestv2FsMIEcfmMepTransmitLtmTtl(UINT4 *, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmMepTransmitLtmTtl(UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIEcfmMepTransmitLtmStatus(UINT4 *, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmMepTransmitLtmStatus(UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhGetFsMIEcfmMepTransmitLtmStatus(UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIEcfmMepTransmitLtmSeqNumber (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIEcfmMepTransmitLtmResult (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhTestv2FsMIEcfmMepTransmitLbmDestIsMepId(UINT4 *, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmMepTransmitLbmDestIsMepId(UINT4, UINT4, UINT4, UINT4,INT4);
extern INT1 nmhTestv2FsMIEcfmMepTransmitLbmDestMepId(UINT4 *, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmMepTransmitLbmDestMepId(UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIEcfmMepTransmitLbmDestMacAddress(UINT4 *, UINT4, UINT4, UINT4, UINT4, tMacAddr);
extern INT1 nmhSetFsMIEcfmMepTransmitLbmDestMacAddress(UINT4, UINT4, UINT4, UINT4, tMacAddr);
extern INT1 nmhTestv2FsMIEcfmMepTransmitLbmMessages(UINT4 *, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmMepTransmitLbmMessages(UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIEcfmMepTransmitLbmDataTlv(UINT4 *, UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhSetFsMIEcfmMepTransmitLbmDataTlv(UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhTestv2FsMIEcfmMepTransmitLbmStatus(UINT4 *, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmMepTransmitLbmStatus(UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhGetFsMIEcfmLtrTtl(UINT4, UINT4, UINT4, UINT4, UINT4, UINT4,UINT4*);
extern INT1 nmhGetFsMIEcfmMepRowStatus(UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhTestv2FsMIEcfmLtrCacheClear(UINT4 *, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmLtrCacheClear(UINT4, INT4);
extern INT1 nmhTestv2FsMIEcfmMipCcmDbClear(UINT4 *, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmMipCcmDbClear(UINT4, INT4);
extern INT1 nmhGetNextIndexFsMIEcfmLtrTable(UINT4, UINT4*, UINT4, UINT4*, UINT4,
UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *, UINT4,UINT4 *);
extern INT1 nmhGetFirstIndexFsMIEcfmMaMepListTable(UINT4 *, UINT4 *, UINT4 *, UINT4 *);
extern INT1 nmhGetNextIndexFsMIEcfmMaMepListTable(UINT4, UINT4 *, UINT4, UINT4
*, UINT4, UINT4 *, UINT4, UINT4 *);
extern INT1 nmhTestv2FsMIEcfmMaMepListRowStatus(UINT4 *, UINT4, UINT4, UINT4,
UINT4, INT4);
extern INT1 nmhSetFsMIEcfmMaMepListRowStatus(UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhGetFirstIndexFsMIEcfmMepDbTable(UINT4 *, UINT4 *, UINT4 *, UINT4 *, UINT4 *);
extern INT1 nmhGetNextIndexFsMIEcfmMepDbTable(UINT4, UINT4*, UINT4, UINT4*,
UINT4, UINT4*, UINT4, UINT4*, UINT4, UINT4*);
extern INT1 nmhGetFsMIEcfmMepDbRMepState(UINT4, UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIEcfmMepDbRdi(UINT4, UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIEcfmMepDefectErrorCcm(UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIEcfmMepDefectXconnCcm(UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIEcfmMepDefectRemoteCcm(UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIEcfmMepDefectMacStatus(UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIEcfmMepDefectRDICcm(UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIEcfmXconnRMepId(UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmErrorRMepId(UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFirstIndexFsMIEcfmRemoteMepDbExTable(UINT4*, UINT4 *, UINT4 *,
UINT4 *, UINT4 *);
extern INT1 nmhGetNextIndexFsMIEcfmRemoteMepDbExTable(UINT4, UINT4*, UINT4,
UINT4 *, UINT4, UINT4 *,UINT4,UINT4 *, UINT4,UINT4 *);
extern INT1 nmhGetFsMIEcfmRMepCcmDefect(UINT4, UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIEcfmRMepPortStatusDefect(UINT4, UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIEcfmRMepInterfaceStatusDefect(UINT4, UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIEcfmRMepRDIDefect(UINT4, UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIEcfmStackMdIndex(INT4, INT4, INT4, INT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmStackMaIndex(INT4, INT4, INT4, INT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmStackMepId(INT4, INT4, INT4, INT4, UINT4 *);
extern INT1 nmhGetNextIndexFsMIEcfmStackTable(INT4, INT4 *, INT4, INT4 *, INT4, INT4 *, INT4, INT4 *);
extern INT1 nmhGetFsMIEcfmMepLbrInOutOfOrder(UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhTestv2FsMIEcfmSystemControl(UINT4 *, UINT4, INT4);                                                   
extern INT1 nmhSetFsMIEcfmSystemControl(UINT4, INT4);                                                   
extern INT1 nmhGetFsMIEcfmModuleStatus(UINT4, INT4 *);
extern INT1 nmhTestv2FsMIEcfmModuleStatus(UINT4 *, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmModuleStatus(UINT4, INT4);
extern INT1 nmhGetFsMIEcfmLtrCacheStatus(UINT4, INT4 *);
extern INT1 nmhTestv2FsMIEcfmLtrCacheStatus(UINT4 *, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmLtrCacheStatus(UINT4, INT4);
extern INT1 nmhGetFsMIEcfmLtrCacheHoldTime(UINT4,  INT4 *);
extern INT1 nmhTestv2FsMIEcfmLtrCacheHoldTime(UINT4 *, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmLtrCacheHoldTime(UINT4, INT4);
extern INT1 nmhGetFsMIEcfmLtrCacheSize(UINT4,  INT4 *);
extern INT1 nmhTestv2FsMIEcfmLtrCacheSize(UINT4 *, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmLtrCacheSize(UINT4, INT4);
extern INT1 nmhGetFsMIEcfmMipCcmDbStatus(UINT4,  INT4 *);
extern INT1 nmhTestv2FsMIEcfmMipCcmDbStatus(UINT4 *, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmMipCcmDbStatus(UINT4, INT4);
extern INT1 nmhGetFsMIEcfmMipCcmDbSize(UINT4,  INT4 *);
extern INT1 nmhTestv2FsMIEcfmMipCcmDbSize(UINT4 *, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmMipCcmDbSize(UINT4, INT4);
extern INT1 nmhGetFsMIEcfmMipCcmDbHoldTime(UINT4,  INT4 *);
extern INT1 nmhTestv2FsMIEcfmMipCcmDbHoldTime(UINT4 *, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmMipCcmDbHoldTime(UINT4, INT4);
extern INT1 nmhGetFsMIEcfmCrosscheckDelay(UINT4, INT4 *);
extern INT1 nmhTestv2FsMIEcfmCrosscheckDelay(UINT4*, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmCrosscheckDelay(UINT4, INT4);
extern INT1 nmhGetFsMIEcfmDefaultMdDefLevel(UINT4, INT4 *);
extern INT1 nmhSetFsMIEcfmDefaultMdDefLevel(UINT4, INT4);
extern INT1 nmhTestv2FsMIEcfmDefaultMdDefLevel(UINT4*, UINT4, INT4);
extern INT1 nmhGetFsMIEcfmDefaultMdDefIdPermission(UINT4, INT4 *);
extern INT1 nmhSetFsMIEcfmDefaultMdDefIdPermission(UINT4, INT4);
extern INT1 nmhTestv2FsMIEcfmDefaultMdDefIdPermission(UINT4*, UINT4, INT4);
extern INT1 nmhGetFsMIEcfmDefaultMdDefMhfCreation(UINT4, INT4 *);
extern INT1 nmhSetFsMIEcfmDefaultMdDefMhfCreation(UINT4, INT4);
extern INT1 nmhTestv2FsMIEcfmDefaultMdDefMhfCreation(UINT4*, UINT4, INT4);
extern INT1 nmhGetFsMIEcfmMipDynamicEvaluationStatus(UINT4, INT4 *);          
extern INT1 nmhSetFsMIEcfmMipDynamicEvaluationStatus(UINT4, INT4);          
extern INT1 nmhTestv2FsMIEcfmMipDynamicEvaluationStatus(UINT4*, UINT4, INT4);          
extern INT1 nmhGetFsMIEcfmPortIfIndex(UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIEcfmPortTxFailedCount (INT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmMepCcmSequenceErrors (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731MepBitErroredLbrIn (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731MepTstOut (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731MepBitErroredTstIn (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731MepValidTstIn (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhSetFsMIEcfmMepCciSentCcms (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmMepLbrOut (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepTstOut (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmMepCcmSequenceErrors (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmMepUnexpLtrIn (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmMepLbrIn (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmMepLbrInOutOfOrder (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmMepLbrBadMsdu (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepBitErroredLbrIn (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepBitErroredTstIn (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepValidTstIn (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmPortTxCfmPduCount (UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmPortRxCfmPduCount (UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmPortTxCcmCount (UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmPortRxCcmCount (UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmPortTxLbmCount (UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmPortRxLbmCount (UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmPortTxLbrCount (UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmPortRxLbrCount (UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmPortTxLtmCount (UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmPortRxLtmCount (UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmPortTxLtrCount (UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmPortRxLtrCount (UINT4, UINT4);
extern INT1 nmhSetFsMIY1731PortTstOut (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortTstIn (INT4, UINT4);
extern INT1 nmhSetFsMIY1731Port1DmOut (INT4, UINT4);
extern INT1 nmhSetFsMIY1731Port1DmIn (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortDmmOut (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortDmmIn (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortDmrOut (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortDmrIn (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortLmmOut (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortLmmIn (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortLmrOut (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortLmrIn (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortVsmOut (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortVsmIn (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortVsrOut (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortVsrIn (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortExmOut (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortExmIn (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortExrOut (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortExrIn (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortApsOut (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortApsIn (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortMccOut (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortMccIn (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortLckOut (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortLckIn (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortAisOut (INT4, UINT4);
extern INT1 nmhSetFsMIY1731PortAisIn (INT4, UINT4);
extern INT1 nmhSetFsMIEcfmPortRxBadCfmPduCount (UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmPortFrwdCfmPduCount (UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmPortDsrdCfmPduCount (UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmPortTxFailedCount (UINT4, UINT4);
extern INT1 nmhGetFsMIY1731PortTstOut (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortTstIn (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731Port1DmOut (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731Port1DmIn (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortDmmOut (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortDmmIn (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortDmrOut (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortDmrIn (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortLmmOut (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortLmmIn (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortLmrOut (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortLmrIn (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortVsmOut (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortVsmIn (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortVsrOut (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortVsrIn (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortExmOut (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortExmIn (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortExrOut (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortExrIn (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortApsOut (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortApsIn (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortMccOut (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortMccIn (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortLckOut (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortLckIn (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortAisOut (INT4, UINT4*);
extern INT1 nmhGetFsMIY1731PortAisIn (INT4, UINT4*);
extern INT1 nmhSetFsMIEcfmMemoryFailureCount(UINT4, UINT4 *);
extern INT1 nmhSetFsMIEcfmBufferFailureCount(UINT4, UINT4 *);
extern INT1 nmhSetFsMIEcfmUpCount(UINT4, UINT4 *);
extern INT1 nmhSetFsMIEcfmDownCount(UINT4, UINT4 *);
extern INT1 nmhSetFsMIEcfmNoDftCount(UINT4, UINT4 *);
extern INT1 nmhSetFsMIEcfmRdiDftCount(UINT4, UINT4 *);
extern INT1 nmhSetFsMIEcfmMacStatusDftCount(UINT4, UINT4 *);
extern INT1 nmhSetFsMIEcfmRemoteCcmDftCount(UINT4, UINT4 *);
extern INT1 nmhSetFsMIEcfmErrorCcmDftCount(UINT4, UINT4 *);
extern INT1 nmhSetFsMIEcfmXconDftCount(UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731PortOperStatus(INT4, INT4 *);
extern INT1 nmhSetFsMIY1731PortOperStatus(INT4, UINT4);
extern INT1 nmhTestv2FsMIY1731PortOperStatus(UINT4*, INT4, INT4);
extern INT1 nmhGetFsMIY1731MegVlanPriority (UINT4, UINT4, INT4*);                                                                  
extern INT1 nmhGetFsMIY1731MegDropEnable (UINT4, UINT4, INT4*);                                                                  
extern INT1 nmhSetFsMIY1731MegVlanPriority (UINT4, UINT4, INT4);                                                                  
extern INT1 nmhSetFsMIY1731MegDropEnable (UINT4, UINT4, INT4);                                                                  
extern INT1 nmhTestv2FsMIY1731MegVlanPriority (UINT4*, UINT4, UINT4, INT4);                                                                  
extern INT1 nmhTestv2FsMIY1731MegDropEnable (UINT4*, UINT4, UINT4, INT4);                                                                  
extern INT1 nmhTestv2FsMIEcfmMdFormat (UINT4 *, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmMdFormat (UINT4, UINT4, INT4);
extern INT1 nmhGetFsMIY1731ErrorLogStatus (UINT4, INT4*);
extern INT1 nmhSetFsMIY1731ErrorLogStatus (UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731ErrorLogStatus (UINT4*, UINT4, INT4);
extern INT1 nmhGetFsMIY1731ErrorLogSize (UINT4, INT4*);
extern INT1 nmhSetFsMIY1731ErrorLogSize (UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731ErrorLogSize (UINT4*, UINT4, INT4);
extern INT1 nmhGetFsMIY1731FrameDelayBufferSize (UINT4, INT4*);
extern INT1 nmhSetFsMIY1731FrameDelayBufferSize (UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731FrameDelayBufferSize (UINT4*, UINT4, INT4);
extern INT1 nmhGetFsMIY1731FrameLossBufferSize (UINT4, INT4*);
extern INT1 nmhSetFsMIY1731FrameLossBufferSize (UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731FrameLossBufferSize (UINT4*, UINT4, INT4);
extern INT1 nmhGetFsMIY1731LbrCacheSize (UINT4, INT4*);
extern INT1 nmhSetFsMIY1731LbrCacheSize (UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731LbrCacheSize (UINT4*, UINT4, INT4);
extern INT1 nmhGetFsMIY1731LbrCacheHoldTime (UINT4, INT4*);
extern INT1 nmhSetFsMIY1731LbrCacheHoldTime (UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731LbrCacheHoldTime (UINT4*, UINT4, INT4);
extern INT1 nmhTestv2FsMIEcfmMaFormat (UINT4 *, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmMaFormat (UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MeMegIdIcc (UINT4 *, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhSetFsMIY1731MeMegIdIcc (UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetFsMIY1731MeMegIdIcc (UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhTestv2FsMIY1731MeMegIdUmc (UINT4 *, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhSetFsMIY1731MeMegIdUmc (UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetFsMIY1731MeMegIdUmc (UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetFsMIEcfmStackMacAddress (INT4, INT4, INT4, INT4, tMacAddr*); 
extern INT1 nmhGetFsMIEcfmMepDbPortStatusTlv (UINT4, UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIEcfmMepDbInterfaceStatusTlv (UINT4, UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepAisCondition (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepAisInterval (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepAisPeriod (UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731MegClientMEGLevel (UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepAisClientMacAddress (UINT4, UINT4, UINT4, UINT4, tMacAddr*);
extern INT1 nmhGetFsMIY1731MepAisPriority (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepAisDropEnable (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MeCcmApplication (UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepUnicastCcmMacAddress (UINT4, UINT4, UINT4, UINT4, tMacAddr*);
extern INT1 nmhGetFsMIY1731MepRdiPeriod (UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731MepCcmPriority (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepCcmDropEnable (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731Mep1DmTransInterval (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepFrameDelayThreshold (UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731MepTransmit1DmPriority (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepTransmit1DmDropEnable (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepTransmitDmmPriority (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepTransmitDmmDropEnable (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepTransmitLtmPriority (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepTransmitLtmDropEnable (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepLckCondition (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepLckInterval (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepLckDelay (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepLckPeriod (UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731MepLckClientMacAddress (UINT4, UINT4, UINT4, UINT4, tMacAddr*);
extern INT1 nmhGetFsMIY1731MepLckPriority (UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731MepLckDropEnable (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepTransmitLbmPriority (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepTransmitLbmDropEnable (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepNearEndFrameLossThreshold (UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731MepFarEndFrameLossThreshold (UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731MepTransmitLmmPriority (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepTransmitLmmDropEnable (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731OperStatus (UINT4, INT4*);
extern INT1 nmhTestv2FsMIY1731OperStatus (UINT4*, UINT4, INT4);
extern INT1 nmhSetFsMIY1731OperStatus (UINT4, INT4);
extern INT1 nmhGetFsMIEcfmGlobalCcmOffload (UINT4, INT4*);
extern INT1 nmhSetFsMIEcfmGlobalCcmOffload (UINT4, INT4);
extern INT1 nmhTestv2FsMIEcfmGlobalCcmOffload (UINT4*, UINT4, INT4);
extern INT1 nmhValidateIndexInstanceFsMIEcfmContextTable (UINT4);
extern INT1 nmhGetFsMIEcfmMepCcmOffload (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhSetFsMIEcfmMepCcmOffload (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIEcfmMepCcmOffload (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhGetFsMIY1731MepOutOfService (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhSetFsMIY1731MepOutOfService (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepOutOfService (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhGetFsMIY1731MepAisCapability (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhSetFsMIY1731MepAisCapability (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepAisCapability (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhGetFsMIY1731Mep1DmRecvCapability (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhSetFsMIY1731Mep1DmRecvCapability (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731Mep1DmRecvCapability (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhGetFsMIY1731MepDmrOptionalFields (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhSetFsMIY1731MepDmrOptionalFields (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepDmrOptionalFields (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhGetFsMIY1731MepRdiCapability (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhSetFsMIY1731MepRdiCapability (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepRdiCapability (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhGetFsMIY1731MepLoopbackCapability (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhSetFsMIY1731MepLoopbackCapability (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepLoopbackCapability (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhGetFsMIY1731MepMulticastTstRecvCapability (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhSetFsMIY1731MepMulticastTstRecvCapability (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepMulticastTstRecvCapability (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhGetFsMIY1731MepMulticastLbmRecvCapability (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhSetFsMIY1731MepMulticastLbmRecvCapability (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepMulticastLbmRecvCapability (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhGetFsMIY1731MeCciEnabled (UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhTestv2FsMIY1731MeCciEnabled (UINT4*, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MeCciEnabled (UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepRdiPeriod (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepRdiPeriod (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepUnicastCcmMacAddress (UINT4*, UINT4, UINT4, UINT4, UINT4, tMacAddr);
extern INT1 nmhSetFsMIY1731MepUnicastCcmMacAddress (UINT4, UINT4, UINT4, UINT4, tMacAddr);
extern INT1 nmhTestv2FsMIY1731MepCcmPriority (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepCcmPriority (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepCcmDropEnable (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepCcmDropEnable (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepAisPeriod (UINT4 *, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepAisPeriod (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepAisInterval (UINT4 *, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepAisInterval (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepAisDestIsMulticast (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepAisDestIsMulticast (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepAisClientMacAddress (UINT4*, UINT4, UINT4, UINT4, UINT4, tMacAddr);
extern INT1 nmhSetFsMIY1731MepAisClientMacAddress (UINT4, UINT4, UINT4, UINT4, tMacAddr);
extern INT1 nmhTestv2FsMIY1731MepAisPriority (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepAisPriority (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepAisDropEnable (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepAisDropEnable (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepRowStatus (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepRowStatus (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MegRowStatus (UINT4*, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MegRowStatus (UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MegClientMEGLevel (UINT4*, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MegClientMEGLevel (UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepLckDestIsMulticast (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepLckDestIsMulticast (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepLckClientMacAddress (UINT4*, UINT4, UINT4, UINT4, UINT4, tMacAddr);
extern INT1 nmhSetFsMIY1731MepLckClientMacAddress (UINT4, UINT4, UINT4, UINT4, tMacAddr);
extern INT1 nmhTestv2FsMIY1731MepLckPriority (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepLckPriority (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepLckDropEnable (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepLckDropEnable (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepLckDelay (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepLckDelay (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLtmTargetIsMepId (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepTransmitLtmTargetIsMepId (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLtmTargetMacAddress (UINT4*, UINT4, UINT4, UINT4, UINT4, tMacAddr);
extern INT1 nmhSetFsMIY1731MepTransmitLtmTargetMacAddress (UINT4, UINT4, UINT4, UINT4, tMacAddr);
extern INT1 nmhTestv2FsMIY1731MepTransmitLtmTargetMepId (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepTransmitLtmTargetMepId (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLtmTimeout (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitLtmTimeout (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLtmTtl (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepTransmitLtmTtl (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLtmFlags (UINT4*, UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetFsMIY1731MepTransmitLtmFlags (UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2FsMIY1731MepTransmitLtmStatus (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitLtmStatus (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIEcfmMepTransmitLtmFlags (UINT4*, UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetFsMIEcfmMepTransmitLtmFlags (UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2FsMIY1731MepTransmit1DmPriority (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmit1DmPriority (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmit1DmDropEnable (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmit1DmDropEnable (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitDmmPriority (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitDmmPriority (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitDmmDropEnable (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitDmmDropEnable (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitDmDestIsMepId (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitDmDestIsMepId (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitDmDestMacAddress (UINT4*, UINT4, UINT4, UINT4, UINT4, tMacAddr);
extern INT1 nmhSetFsMIY1731MepTransmitDmDestMacAddress (UINT4, UINT4, UINT4, UINT4, tMacAddr);
extern INT1 nmhTestv2FsMIY1731MepTransmitDmDestMepId (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepTransmitDmDestMepId (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitDmType (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitDmType (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitDmMessages (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitDmMessages (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitDmInterval (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitDmInterval (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitDmStatus (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitDmStatus (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepNearEndFrameLossThreshold (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepNearEndFrameLossThreshold (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepFarEndFrameLossThreshold (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepFarEndFrameLossThreshold (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLmmPriority (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitLmmPriority (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLmmDropEnable (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitLmmDropEnable (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLmmStatus (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitLmmStatus (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLmmDestIsMepId (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitLmmDestIsMepId (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLmmDestMepId (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepTransmitLmmDestMepId (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLmmDestMacAddress (UINT4*, UINT4, UINT4, UINT4, UINT4, tMacAddr);
extern INT1 nmhSetFsMIY1731MepTransmitLmmDestMacAddress (UINT4, UINT4, UINT4, UINT4, tMacAddr);
extern INT1 nmhTestv2FsMIY1731MepTransmitLmmMessages (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitLmmMessages (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLmmInterval (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitLmmInterval (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLmmDeadline (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepTransmitLmmDeadline (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MeRowStatus (UINT4*, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MeRowStatus (UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MeCcmApplication (UINT4*, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MeCcmApplication (UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitTstStatus (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitTstStatus (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitTstDestType (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitTstDestType (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitTstDestMepId (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepTransmitTstDestMepId (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitTstDestMacAddress (UINT4*, UINT4, UINT4, UINT4, UINT4, tMacAddr);
extern INT1 nmhSetFsMIY1731MepTransmitTstDestMacAddress (UINT4, UINT4, UINT4, UINT4, tMacAddr);
extern INT1 nmhTestv2FsMIY1731MepTransmitTstPatternType (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitTstPatternType (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitTstPatternSize (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepTransmitTstPatternSize (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitTstIntervalType (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitTstIntervalType (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitTstInterval (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepTransmitTstInterval (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitTstVariableBytes (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitTstVariableBytes (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitTstMessages (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitTstMessages (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitTstDeadline (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepTransmitTstDeadline (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitTstPriority (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitTstPriority (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitTstDropEnable (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitTstDropEnable (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLbmStatus (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitLbmStatus (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLbmDestType (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitLbmDestType (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLbmDestMepId (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepTransmitLbmDestMepId (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLbmDestMacAddress (UINT4*, UINT4, UINT4, UINT4, UINT4, tMacAddr);
extern INT1 nmhSetFsMIY1731MepTransmitLbmDestMacAddress (UINT4, UINT4, UINT4, UINT4, tMacAddr);
extern INT1 nmhTestv2FsMIY1731MepTransmitLbmTlvType (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitLbmTlvType (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLbmDataPattern (UINT4*, UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetFsMIY1731MepTransmitLbmDataPattern (UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhTestv2FsMIY1731MepTransmitLbmDataPatternSize (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepTransmitLbmDataPatternSize (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLbmTestPatternType (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitLbmTestPatternType (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLbmTestPatternSize (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepTransmitLbmTestPatternSize (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLbmIntervalType (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitLbmIntervalType (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLbmInterval (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepTransmitLbmInterval (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLbmVariableBytes (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitLbmVariableBytes (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLbmMessages (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitLbmMessages (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLbmDeadline (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepTransmitLbmDeadline (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLbmPriority (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitLbmPriority (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLbmDropEnable (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitLbmDropEnable (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLtmPriority (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitLtmPriority (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitLtmDropEnable (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitLtmDropEnable (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepLckPeriod (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepLckPeriod (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepLckInterval (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepLckInterval (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepFrameDelayThreshold (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepFrameDelayThreshold (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitDmDeadline (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepTransmitDmDeadline (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhGetFsMIY1731TrapControl (UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsMIEcfmTrapControl (UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2FsMIY1731TrapControl (UINT4 *, UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetFsMIY1731TrapControl (UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2FsMIEcfmTrapControl (UINT4 *, UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetFsMIEcfmTrapControl (UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsMIEcfmMaFormat (UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepTransmitTstPriority (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitTstDropEnable (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhTestv2FsMIY1731LbrCacheClear (UINT4 *, UINT4, INT4);
extern INT1 nmhSetFsMIY1731LbrCacheClear (UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731FrameDelayBufferClear (UINT4 *, UINT4, INT4);
extern INT1 nmhSetFsMIY1731FrameDelayBufferClear (UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731FrameLossBufferClear (UINT4 *, UINT4, INT4);
extern INT1 nmhSetFsMIY1731FrameLossBufferClear (UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731ErrorLogClear (UINT4 *, UINT4, INT4);
extern INT1 nmhSetFsMIY1731ErrorLogClear (UINT4, INT4);
extern INT1 nmhGetNextIndexFsMIY1731LbrTable (UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *,
                                              UINT4, UINT4 *, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731LbrResponderMacAddress (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, tMacAddr*);
extern INT1 nmhGetFsMIY1731LbmBytesSent (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731LbrReceiveTime (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731LbStatsLbmOut (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731LbStatsLbrIn (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731LbStatsTotalResponders (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731LbStatsAvgLbrsPerResponder (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731LbStatsLbrTimeMin (UINT4, UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731LbStatsLbrTimeMax (UINT4, UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetNextIndexFsMIY1731LbStatsTable (UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731LbStatsLbrTimeAverage (UINT4, UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetNextIndexFsMIY1731ErrorLogTable (UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731LbmUnexptedLbrIn (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731LbmDuplicatedLbrIn (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731LbrErrorType (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIEcfmLtmTtl (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetNextIndexFsMIY1731LbmTable (UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *);
extern INT1 nmhGetNextIndexFsMIY1731FdTable (UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731FdMeasurementType (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731FdMeasurementTimeStamp (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731FdPeerMepMacAddress (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, tMacAddr*);
extern INT1 nmhGetFsMIY1731FdDelayValue (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetFsMIY1731FdIFDV (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetFsMIY1731FdFDV (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetFsMIY1731FdStatsDmmOut (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731FdStatsDmrIn (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731FdStatsDelayAverage (UINT4, UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetFsMIY1731FdStatsDelayMin (UINT4, UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetFsMIY1731FdStatsDelayMax (UINT4, UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetFsMIY1731FdStatsIFDVAverage (UINT4, UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetFsMIY1731FdStatsFDVAverage (UINT4, UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetFsMIEcfmLtrEgressMac (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, tMacAddr*);
extern INT1 nmhGetFsMIEcfmLtrEgress (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetNextIndexFsMIY1731FdStatsTable (UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *);
extern INT1 nmhGetNextIndexFsMIY1731FlStatsTable (UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmLtrLastEgressIdentifier (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetFsMIY1731ErrorLogType (UINT4, UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731ErrorLogRMepIdentifier (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731ErrorLogTimeStamp (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731FlStatsFarEndLossMax (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731FlStatsFarEndLossMin (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731FlStatsFarEndLossAverage (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731FlStatsNearEndLossMax (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731FlStatsNearEndLossMin (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731FlStatsNearEndLossAverage (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731FlStatsMessagesOut (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731FlStatsMessagesIn (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731FlPeerMepMacAddress (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, tMacAddr*);
extern INT1 nmhGetFsMIY1731FlNearEndLoss (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731FlFarEndLoss (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731FlStatsMeasurementType (UINT4, UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731FlMeasurementTimeStamp (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIY1731FlMeasurementTime (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetNextIndexFsMIY1731FlTable (UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *, UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmMepIfIndex (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhTestv2FsMIEcfmMepIfIndex (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIEcfmMepIfIndex (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhGetFirstIndexFsMIEcfmPortTable (INT4*);
extern INT1 nmhGetFsMIY1731LbrCacheStatus (UINT4, INT4*);
extern INT1 nmhSetFsMIY1731LbrCacheStatus (UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731LbrCacheStatus (UINT4*, UINT4, INT4);
extern INT1 nmhGetFsMIY1731MepTstCapability (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhSetFsMIY1731MepTstCapability (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTstCapability (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhGetFsMIY1731MepTransmitLbmStatus (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepTransmitLmmStatus (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepTransmitDmStatus (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepTransmitTstStatus (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhGetFsMIY1731MepTransmitThStatus (UINT4, UINT4, UINT4, UINT4, INT4*);
extern INT1 nmhTestv2FsMIY1731MepTransmitThStatus (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitThStatus (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitThDestIsMepId (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitThDestIsMepId (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitThDestMepId (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitThDestMepId (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitThDestMacAddress (UINT4*, UINT4, UINT4, UINT4, UINT4, tMacAddr);
extern INT1 nmhSetFsMIY1731MepTransmitThDestMacAddress (UINT4, UINT4, UINT4, UINT4, tMacAddr);
extern INT1 nmhTestv2FsMIY1731MepTransmitThTestPatternType (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitThTestPatternType (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitThFrameSize (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepTransmitThFrameSize (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitThMessages (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitThMessages (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitThDeadline (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepTransmitThDeadline (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitThBurstType (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitThBurstType (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitThBurstMessages (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitThBurstMessages (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitThBurstDeadline (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepTransmitThBurstDeadline (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitThPps (UINT4*, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIY1731MepTransmitThPps (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2FsMIY1731MepTransmitThType (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731MepTransmitThType (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2FsMIY1731Mep1DmTransInterval (UINT4*, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetFsMIY1731Mep1DmTransInterval (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhGetFsMIY1731MepDefectConditions (UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetFsMIY1731MepTransmitThDeadline (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitThMessages (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitThPps (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitThFrameSize (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitThTestPatternType (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitThDestIsMepId (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitDmType (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitDmDestIsMepId (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitThDestMacAddress (UINT4, UINT4, UINT4, UINT4, tMacAddr*);
extern INT1 nmhGetFsMIY1731MepTransmitThDestMepId (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitDmDeadline (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitTstInterval (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitTstIntervalType (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitTstDestMepId (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitDmMessages (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitDmDestMacAddress (UINT4, UINT4, UINT4, UINT4, tMacAddr*);
extern INT1 nmhGetFsMIY1731MepTransmitLbmInterval (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitLbmIntervalType (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitThType (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitDmInterval (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitTstDeadline (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitTstDestMacAddress (UINT4, UINT4, UINT4, UINT4, tMacAddr*);
extern INT1 nmhGetFsMIY1731MepTransmitTstDestType (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitLbmVariableBytes (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitDmDestMepId (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitTstVariableBytes (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitLbmDestType (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitLbmDestMacAddress (UINT4, UINT4, UINT4, UINT4, tMacAddr*);
extern INT1 nmhGetFsMIY1731MepTransmitLbmTlvType (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitTstMessages (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitTstPatternType (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitLbmDeadline (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitLbmTestPatternSize (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitLbmDataPattern (UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetFsMIY1731MepTransmitLbmDestMepId (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitLbmTestPatternType (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitLbmMessages (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitLbmDataPatternSize (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitTstPatternSize (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhSetFsMIEcfmXconnRMepId (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmErrorRMepId (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmMepDefectRDICcm (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmMepDefectMacStatus (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmMepDefectErrorCcm (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmMepDefectRemoteCcm (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmMepDefectXconnCcm (UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmRMepCcmDefect (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmRMepInterfaceStatusDefect (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmRMepPortStatusDefect (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhSetFsMIEcfmRMepRDIDefect (UINT4, UINT4, UINT4, UINT4, UINT4, UINT4);
extern INT1 nmhGetFsMIEcfmMepTransmitLtmFlags (UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetFsMIEcfmMepTransmitLtmEgressIdentifier (UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhSetFsMIEcfmMepTransmitLtmEgressIdentifier (UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhTestv2FsMIEcfmMepTransmitLtmEgressIdentifier (UINT4*, UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetFsMIY1731MepTransmitLtmTimeout (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIEcfmMepTransmitLtmTargetMacAddress (UINT4, UINT4, UINT4, UINT4, tMacAddr*);
extern INT1 nmhGetFsMIY1731MepTransmitLtmStatus (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitThBurstMessages (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitThBurstDeadline (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitThBurstType (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIEcfmMepTransmitLtmTtl (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmMepTransmitLtmTargetMepId (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIEcfmMepTransmitLtmTargetIsMepId (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepThUnVerifiedBps (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731MepThVerifiedBps (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitLmmDestIsMepId (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitLmmInterval (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitLmmMessages (UINT4, UINT4, UINT4, UINT4, INT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitLmmDestMepId (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitLmmDeadline (UINT4, UINT4, UINT4, UINT4, UINT4 *);
extern INT1 nmhGetFsMIY1731MepTransmitLmmDestMacAddress (UINT4, UINT4, UINT4, UINT4, tMacAddr*);
extern INT1 nmhGetFsMIEcfmMepTransmitLbmDestMacAddress (UINT4, UINT4, UINT4, UINT4, tMacAddr*);
extern INT1 nmhGetFsMIEcfmMepTransmitLbmDestMepId (UINT4, UINT4, UINT4, UINT4, UINT4*);
extern INT1 nmhGetFsMIEcfmMepTransmitLbmDataTlv (UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE*);
extern INT4 FsMIY1731MepTransmitLbmTlvTypeSet (tSnmpIndex *, tRetVal *);
extern INT4 FsMIY1731MepTstOutSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepBitErroredLbrInSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepBitErroredTstInSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepValidTstInSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MegVlanPrioritySet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MegDropEnableSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MegClientMEGLevelSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MeMegIdIccSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MeMegIdUmcSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MeCciEnabledSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortOperStatusSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortTstOutSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortTstInSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731Port1DmOutSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731Port1DmInSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortDmmOutSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortDmmInSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortDmrOutSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortDmrInSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortApsOutSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortApsInSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortMccOutSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortMccInSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortVsmOutSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortVsmInSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortVsrOutSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortVsrInSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortExmOutSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortExmInSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortExrOutSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortExrInSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortLckOutSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortLckInSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortAisOutSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortLmmOutSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortLmmInSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortLmrOutSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortLmrInSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731ErrorLogStatusSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731ErrorLogSizeSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731FrameDelayBufferSizeSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731FrameLossBufferSizeSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731LbrCacheStatusSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731LbrCacheHoldTimeSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731LbrCacheSizeSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731OperStatusSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731TrapControlSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MeCcmApplicationSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepRdiPeriodSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepUnicastCcmMacAddressSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepCcmPrioritySet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepCcmDropEnableSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepAisPeriodSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepAisIntervalSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepAisDestIsMulticastSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepAisClientMacAddressSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepAisPrioritySet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepAisDropEnableSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepLckPeriodSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepLckIntervalSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepLckDestIsMulticastSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepLckClientMacAddressSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepLckPrioritySet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepLckDropEnableSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepLckDelaySet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitLbmStatusSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitLbmDestTypeSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitLbmDestMepIdSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitLbmDestMacAddressSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitLbmDataPatternSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitLbmDataPatternSizeSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitLbmTestPatternTypeSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitLbmTestPatternSizeSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitLbmIntervalTypeSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitLbmIntervalSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitLbmVariableBytesSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitLbmMessagesSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitLbmDeadlineSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitLbmPrioritySet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitLbmDropEnableSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitTstStatusSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitTstDestTypeSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitTstDestMepIdSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitTstDestMacAddressSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitTstPatternTypeSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitTstPatternSizeSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitTstIntervalTypeSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitTstIntervalSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitTstVariableBytesSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitTstMessagesSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitTstDeadlineSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitTstPrioritySet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitTstDropEnableSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitLmmStatusSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitLmmDestIsMepIdSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitLmmDestMepIdSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitLmmDestMacAddressSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitLmmMessagesSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitLmmIntervalSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitLmmDeadlineSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitLmmPrioritySet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitLmmDropEnableSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitDmStatusSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitDmDestIsMepIdSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitDmDestMepIdSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitDmDestMacAddressSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitDmTypeSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitDmMessagesSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitDmIntervalSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitDmDeadlineSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmit1DmPrioritySet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmit1DmDropEnableSet(tSnmpIndex * , tRetVal * );

extern INT4 FsMIY1731MepTransmitDmmDropEnableSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitDmmPrioritySet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731PortAisInSet (tSnmpIndex * , tRetVal * );

extern INT4 FsMIY1731MepTransmitThStatusSet (tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitThDestIsMepIdSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitThDestMepIdSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitThDestMacAddressSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitThTypeSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitThTestPatternTypeSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitThFrameSizeSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitThPpsSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitThMessagesSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitThDeadlineSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitThBurstTypeSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitThBurstMessagesSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTransmitThBurstDeadlineSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731LbrCacheClearSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731FrameLossBufferClearSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731FrameDelayBufferClearSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731ErrorLogClearSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepAisCapabilitySet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepRdiCapabilitySet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731Mep1DmRecvCapabilitySet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepDmrOptionalFieldsSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepLoopbackCapabilitySet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepMulticastLbmRecvCapabilitySet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepTstCapabilitySet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepMulticastTstRecvCapabilitySet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731Mep1DmTransIntervalSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepFrameDelayThresholdSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepFarEndFrameLossThresholdSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepNearEndFrameLossThresholdSet(tSnmpIndex * , tRetVal * );
extern INT4 FsMIY1731MepOutOfServiceSet(tSnmpIndex * , tRetVal * );


/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsMIY1731MepAvailabilityStatus (UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *);

extern INT1
nmhGetFsMIY1731MepAvailabilityResultOK (UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *);

extern INT1
nmhGetFsMIY1731MepAvailabilityInterval (UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *);

extern INT1
nmhGetFsMIY1731MepAvailabilityDeadline (UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *);

extern INT1
nmhGetFsMIY1731MepAvailabilityLowerThreshold (UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * );

extern INT1
nmhGetFsMIY1731MepAvailabilityUpperThreshold (UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * );

extern INT1
nmhGetFsMIY1731MepAvailabilityModestAreaIsAvailable (UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *);

extern INT1
nmhGetFsMIY1731MepAvailabilityWindowSize (UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *);

extern INT1
nmhGetFsMIY1731MepAvailabilityDestMacAddress (UINT4  , UINT4  , UINT4  , UINT4 ,tMacAddr * );

extern INT1
nmhGetFsMIY1731MepAvailabilityDestMepId (UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *);

extern INT1
nmhGetFsMIY1731MepAvailabilityDestIsMepId (UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *);

extern INT1
nmhGetFsMIY1731MepAvailabilityType (UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *);

extern INT1
nmhGetFsMIY1731MepAvailabilitySchldDownInitTime (UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *);

extern INT1
nmhGetFsMIY1731MepAvailabilitySchldDownEndTime (UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *);

extern INT1
nmhGetFsMIY1731MepAvailabilityPriority (UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *);

extern INT1
nmhGetFsMIY1731MepAvailabilityDropEnable (UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *);

extern INT1
nmhGetFsMIY1731MepAvailabilityPercentage (UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * );

extern INT1
nmhGetFsMIY1731MepAvailabilityRowStatus (UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *);

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsMIY1731MepAvailabilityStatus (UINT4  , UINT4  , UINT4  , UINT4  ,INT4 );

extern INT1
nmhSetFsMIY1731MepAvailabilityInterval (UINT4  , UINT4  , UINT4  , UINT4  ,INT4 );

extern INT1
nmhSetFsMIY1731MepAvailabilityDeadline (UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 );

extern INT1
nmhSetFsMIY1731MepAvailabilityLowerThreshold (UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *);

extern INT1
nmhSetFsMIY1731MepAvailabilityUpperThreshold (UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *);

extern INT1
nmhSetFsMIY1731MepAvailabilityModestAreaIsAvailable (UINT4  , UINT4  , UINT4  , UINT4  ,INT4 );

extern INT1
nmhSetFsMIY1731MepAvailabilityWindowSize (UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 );

extern INT1
nmhSetFsMIY1731MepAvailabilityDestMacAddress (UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr );

extern INT1
nmhSetFsMIY1731MepAvailabilityDestMepId (UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 );

extern INT1
nmhSetFsMIY1731MepAvailabilityDestIsMepId (UINT4  , UINT4  , UINT4  , UINT4  ,INT4 );

extern INT1
nmhSetFsMIY1731MepAvailabilityType (UINT4  , UINT4  , UINT4  , UINT4  ,INT4 );

extern INT1
nmhSetFsMIY1731MepAvailabilitySchldDownInitTime (UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 );

extern INT1
nmhSetFsMIY1731MepAvailabilitySchldDownEndTime (UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 );

extern INT1
nmhSetFsMIY1731MepAvailabilityPriority (UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 );

extern INT1
nmhSetFsMIY1731MepAvailabilityDropEnable (UINT4  , UINT4  , UINT4  , UINT4  ,INT4 );

extern INT1
nmhSetFsMIY1731MepAvailabilityRowStatus (UINT4  , UINT4  , UINT4  , UINT4  ,INT4 );

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsMIY1731MepAvailabilityStatus (UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 );

extern INT1
nmhTestv2FsMIY1731MepAvailabilityInterval (UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 );

extern INT1
nmhTestv2FsMIY1731MepAvailabilityDeadline (UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 );

extern INT1
nmhTestv2FsMIY1731MepAvailabilityLowerThreshold (UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *);

extern INT1
nmhTestv2FsMIY1731MepAvailabilityUpperThreshold (UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *);

extern INT1
nmhTestv2FsMIY1731MepAvailabilityModestAreaIsAvailable (UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 );

extern INT1
nmhTestv2FsMIY1731MepAvailabilityWindowSize (UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 );

extern INT1
nmhTestv2FsMIY1731MepAvailabilityDestMacAddress (UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tMacAddr );

extern INT1
nmhTestv2FsMIY1731MepAvailabilityDestMepId (UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 );

extern INT1
nmhTestv2FsMIY1731MepAvailabilityDestIsMepId (UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 );

extern INT1
nmhTestv2FsMIY1731MepAvailabilityType (UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 );

extern INT1
nmhTestv2FsMIY1731MepAvailabilitySchldDownInitTime (UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 );

extern INT1
nmhTestv2FsMIY1731MepAvailabilitySchldDownEndTime (UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 );

extern INT1
nmhTestv2FsMIY1731MepAvailabilityPriority (UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 );

extern INT1
nmhTestv2FsMIY1731MepAvailabilityDropEnable (UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 );

extern INT1
nmhTestv2FsMIY1731MepAvailabilityRowStatus (UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 );

extern INT1 nmhTestv2FsEcfmPortModuleStatus (UINT4 * ,UINT4 ,INT4 );

extern INT1 nmhSetFsEcfmPortModuleStatus (UINT4 ,INT4 );

#endif
#endif


