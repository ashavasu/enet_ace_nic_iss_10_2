/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mplsweb.h,v 1.12 2014/07/01 11:57:51 siva Exp $
 *
 * Description: This file contains variables for MPLS web Module
 ********************************************************************/


#ifndef _MPLSWEB_H
#define _MPLSWEB_H
#ifdef MPLS_WANTED

typedef struct _TunnelArgs
{
    UINT4 u4TunnelIndex;
    UINT4 u4TunnelInstance;
    UINT4 u4IngressId;
    UINT4 u4EgressId;
    UINT4 u4Bandwidth;
    UINT4 u4InLabel;
    UINT4 u4InIndex;
    UINT4 u4OutLabel;
    UINT4 u4NextHop;
    UINT4 u4OutIfIndex;
    INT4  i4AdminStatus;
    INT4  i4TunnelRole;
}tTunnelArgs;

typedef struct _PwArgs
{
    UINT4 u4PeerAddr;
    UINT4 u4LocalLabel;
    UINT4 u4RemoteLabel;
    UINT4 u4MplsType;
    INT4  i4PwMode;
    UINT4 u4TunnelId;
}tPwArgs;

typedef struct _PwEnetArgs
{
    UINT4 u4PortVlan;
    INT4  i4PortIfIndex;
    INT4  i4PwType;
    INT4  i4PwMode;
}tPwEnetArgs;

#ifdef MPLS_L3VPN_WANTED
#define MPLS_L3VPN_ZERO                             0
#define MPLS_L3VPN_ONE                              1
#define L3VPN_MAX_RT_POLICY_LEN                     8
#define L3VPN_MAX_NUMBER_OF_ROUTES_LEN              8
#define L3VPN_MAX_HIGH_ROUTE_THRESHOLD_LEN          8
#define L3VPN_MAX_MID_ROUTE_THRESHOLD_LEN           8
#define L3VPN_MAX_RD_RT_STRING_TOKEN                3
#endif

#define FTN_DEST_BITS_SET                          0x2
#define L2VPN_MAX_VPLS_NAME_LEN                    32

#ifdef VPLSADS_WANTED
#define L2VPN_MAX_TOKEN_IN_RD_RT_STRING            3
#define L2VPN_MAX_VPLS_MODE_LEN                    32
#define L2VPN_MAX_RD_RT_STRING_LEN                 32
#define L2VPN_MAX_VPLS_MTU_LEN                     8
#define L2VPN_MAX_VPLS_CONTROL_WORD_LEN            8
#define L2VPN_MAX_VPLS_RD_LEN                      8
#define L2VPN_MAX_VPLS_RT_LEN                      8
#endif

#ifdef MPLS_L3VPN_WANTED

#define L3VPN_MAX_VPLS_RT_LEN                      8
#define   MPLS_WEB_L3VPN_RD_TYPE_0                 0
#define   MPLS_WEB_L3VPN_RD_TYPE_1                 1
#define   MPLS_WEB_L3VPN_RD_TYPE_2                 2

#define   MPLS_WEB_L3VPN_RT_TYPE_0                 0
#define   MPLS_WEB_L3VPN_RT_TYPE_1       1
#define   MPLS_WEB_L3VPN_RT_TYPE_2                 2 
#define   MAX_RT_POLICY_LEN         7
#endif

#define L2VPN_VPLS_FDB_DEF_VAL    L2VPN_VPLS_FDB_MIN_VAL
#define L2VPN_VPLS_FDB_MIN_VAL    VLAN_VFI_MIN_ID
#define L2VPN_VPLS_FDB_MAX_VAL    VLAN_VFI_MAX_ID
#define L2VPN_MAX_VPLS_VPNID_LEN                    7
#define L2VPN_MAX_VPLS_ENTRIES                     100
#define MPLS_MAX_STR_LEN                           50

#ifdef VPLSADS_WANTED
#define L2VPN_VPLS_RD_SUBTYPE                      0
#define L2VPN_VPLS_RT_SUBTYPE                      2
#define L2VPN_VPLS_SIG_BGP                         2
#define L2VPN_VPLS_RT_IMPORT                       1
#define L2VPN_VPLS_RT_EXPORT                       2
#define L2VPN_VPLS_RT_BOTH                         3
#endif


VOID IssProcessMplsIfPage (tHttp *pHttp);
VOID IssProcessMplsIfGet (tHttp *pHttp);
VOID IssProcessMplsIfSet (tHttp *pHttp);
VOID IssProcessMplsFtnPage (tHttp *pHttp);
VOID IssProcessMplsFtnGet (tHttp *pHttp);
VOID IssProcessMplsFtnSet (tHttp *pHttp);
VOID IssProcessMplsXcPage (tHttp *pHttp);
VOID IssProcessMplsXcGet (tHttp *pHttp);
VOID IssProcessMplsXcSet (tHttp *pHttp);
VOID IssProcessMplsTunnelPage (tHttp *pHttp);
VOID IssProcessMplsTunnelGet (tHttp *pHttp);
VOID IssProcessMplsTunnelSet (tHttp *pHttp);
#ifdef MPLS_L3VPN_WANTED
VOID IssProcessMplsConfL3vpnPage (tHttp * pHttp);
VOID IssProcessMplsL3vpnSet (tHttp * pHttp);
VOID IssProcessMplsL3vpnGet (tHttp * pHttp);
#endif
#ifdef MI_WANTED
VOID IssProcessMplsVfiPage (tHttp *pHttp);
VOID IssProcessMplsVfiGet (tHttp *pHttp);
VOID IssProcessMplsVfiSet (tHttp *pHttp);
#endif
VOID IssProcessMplsPwPage (tHttp *pHttp);
VOID IssProcessMplsPwGet (tHttp *pHttp);
VOID IssProcessMplsPwSet (tHttp *pHttp);
VOID IssProcessMplsPwEnetPage (tHttp *pHttp);
VOID IssProcessMplsPwEnetGet (tHttp *pHttp);
VOID IssProcessMplsPwEnetSet (tHttp *pHttp);
VOID IssProcessMplsMapPage (tHttp *pHttp);
VOID IssProcessMplsMapGet (tHttp *pHttp);
VOID IssProcessMplsMapSet (tHttp *pHttp);


VOID IssMplsDisplayFtn (tHttp *, UINT4, UINT4, UINT4, UINT4, UINT4, UINT4, UINT4);
INT4 IssMplsDeleteFtn (tHttp *, UINT4, UINT4);
INT4 IssMplsDeleteXc (tHttp *, UINT4, UINT4, UINT4);
INT4 IssMplsFtnLabelBinding (tHttp *pHttp, UINT4 u4Prefix, UINT4 u4Mask,
                             UINT4 u4NextHop, UINT1 *pu1InIfName, UINT4 u4InLabel,
                             UINT4 u4OutLabel);
INT1 IssMplsGetOrCreateFTNEntry (UINT4 u4Prefix, UINT4 u4Mask,
                                 UINT4 *pu4FtnIndex, BOOL1 * bFtnEntryExists,
                                 INT4 i4FtnActType);
INT1 IssMplsInFTNEntry (tSNMP_OCTET_STRING_TYPE * InSegmentIndex, UINT4 u4InLabel,
                        UINT4 u4InIfIndex, BOOL1 bFtnEntryExists);

INT1 IssMplsOutFTNEntry (tSNMP_OCTET_STRING_TYPE * OutSegmentIndex,
                         UINT4 u4OutLabel, UINT4 u4NextHop, BOOL1 bFtnEntryExists);

VOID IssMplsDelTables (tSNMP_OCTET_STRING_TYPE * InSegmentIndex,
                       tSNMP_OCTET_STRING_TYPE * OutSegmentIndex,
                       tSNMP_OCTET_STRING_TYPE * XCIndex, UINT4 u4InIfIndex,
                       UINT4 u4FtnIndex);
INT4
IssMplsInOutXcFtnActive (tSNMP_OCTET_STRING_TYPE * InSegmentIndex,
                         tSNMP_OCTET_STRING_TYPE * OutSegmentIndex,
                         tSNMP_OCTET_STRING_TYPE * XCIndex,
                         UINT4 u4FtnIndex, UINT4 u4Flag);

INT4 IssMplsCreateXc (tHttp *, UINT4, UINT4, UINT4, UINT4, UINT4);
INT4 IssMplsDeleteTunnel (tHttp *, UINT4, UINT4, UINT4, UINT4);
INT4 IssMplsCreateTunnel (tHttp *, tTunnelArgs *);
INT1 IssMplsDestroyTunnelXc (tTunnelArgs *);
INT1 IssMplsCreateTunnelXc (tTunnelArgs *);
INT1 IssMplsCreateInSegment (tSNMP_OCTET_STRING_TYPE *, UINT4, UINT4, UINT4 *);
INT1 IssMplsCreateOutSegment (tSNMP_OCTET_STRING_TYPE *, UINT4, UINT4, UINT4, UINT4 *);
INT1 IssMplsCreateOrModifyResource (tTunnelArgs *);
INT1 IssMplsTunnelActive (tTunnelArgs *);
VOID IssMplsCleanUpInSegOutSeg (tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *,
                                INT4);
#ifdef MI_WANTED
INT4 IssMplsDeleteVfi (tHttp *, UINT1 *);
#ifdef VPLSADS_WANTED
INT4 IssMplsCreateVfi (tHttp *, UINT1 *, UINT4, UINT4, INT4, UINT1 *, UINT1 *, UINT1 *, UINT1 **, UINT1 **, UINT1 *, UINT1 *, UINT1 *);
#else
INT4 IssMplsCreateVfi (tHttp *, UINT1 *, UINT4, UINT4, INT4);
#endif
VOID IssPrintAvailableVfi (tHttp *);
#endif

VOID IssMplsGetPwModeFromVplsIndex (UINT4, INT4 *);
INT4 IssMplsCreateEnetEntry (tHttp *, tPwEnetArgs *, UINT4, UINT1 *);
INT4 IssMplsDeleteEnetEntry (tHttp *, INT4, INT4, UINT4, INT4);
INT4 IssMplsCreatePw (tHttp *, tPwArgs *, UINT1 *);
VOID IssPrintAvailablePw (tHttp *);
INT1 IssMplsPwEntryExist (tPwArgs *, UINT1 *, UINT4 *);
INT1 IssMplsPwTablePopulate (tPwArgs *, UINT4, UINT4);
INT1 IssMplsOutBoundTablePopulate (UINT4, UINT4, UINT4, UINT4);

INT1 IssMplsEnetTablePopulate (tPwEnetArgs *, UINT4, UINT4);
UINT4 IssGetLastEnetInstance (UINT4);
VOID IssGetVplsIndexFromVplsName (UINT1 *, UINT4 *);

extern INT1 nmhGetPwIfIndex (UINT4 , INT4 *);
extern INT1 nmhGetPwID (UINT4, UINT4 *);
extern INT1 nmhTestv2PwIfIndex (UINT4 *, UINT4, INT4);
extern INT1 nmhSetPwIfIndex (UINT4 , INT4 );
extern INT4 PwIfIndexSet (tSnmpIndex *, tRetVal *);
extern INT1 nmhGetMplsFTNIndexNext (UINT4 *);
extern INT1 nmhTestv2MplsFTNActionType (UINT4 *, UINT4, INT4);
extern INT1 nmhSetMplsFTNActionType (UINT4, INT4 );
extern INT1 nmhTestv2MplsFTNAddrType (UINT4 *, UINT4, INT4 );
extern INT1 nmhSetMplsFTNAddrType (UINT4, INT4 );
extern INT1 nmhTestv2MplsFTNDestAddrMin (UINT4 *, UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetMplsFTNDestAddrMin (UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2MplsFTNDestAddrMax (UINT4 *, UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetMplsFTNDestAddrMax (UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2MplsFTNMask (UINT4 *, UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetMplsFTNMask (UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetMplsInSegmentIndexNext (tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhTestv2MplsInSegmentAddrFamily (UINT4 *, tSNMP_OCTET_STRING_TYPE * ,INT4 );
extern INT1 nmhSetMplsInSegmentAddrFamily (tSNMP_OCTET_STRING_TYPE * ,INT4 );
extern INT1 nmhTestv2MplsInSegmentLabel (UINT4 *, tSNMP_OCTET_STRING_TYPE * ,UINT4 );
extern INT1 nmhSetMplsInSegmentLabel (tSNMP_OCTET_STRING_TYPE * ,UINT4 );
extern INT1 nmhTestv2MplsInSegmentNPop (UINT4 *, tSNMP_OCTET_STRING_TYPE * ,INT4 );
extern INT1 nmhSetMplsInSegmentNPop (tSNMP_OCTET_STRING_TYPE * ,INT4 );
extern INT1 nmhTestv2MplsInSegmentInterface (UINT4 *, tSNMP_OCTET_STRING_TYPE * ,INT4 );
extern INT1 nmhSetMplsInSegmentInterface (tSNMP_OCTET_STRING_TYPE * ,INT4 );
extern INT1 nmhGetMplsOutSegmentIndexNext (tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhTestv2MplsOutSegmentTopLabel (UINT4 *, tSNMP_OCTET_STRING_TYPE * ,UINT4 );
extern INT1 nmhSetMplsOutSegmentTopLabel (tSNMP_OCTET_STRING_TYPE * ,UINT4 );
extern INT1 nmhTestv2MplsOutSegmentPushTopLabel (UINT4 *, tSNMP_OCTET_STRING_TYPE * ,INT4 );
extern INT1 nmhSetMplsOutSegmentPushTopLabel (tSNMP_OCTET_STRING_TYPE * ,INT4 );
extern INT1 nmhTestv2MplsOutSegmentNextHopAddrType (UINT4 *, tSNMP_OCTET_STRING_TYPE * ,INT4 );
extern INT1 nmhSetMplsOutSegmentNextHopAddrType (tSNMP_OCTET_STRING_TYPE * ,INT4 );
extern INT1 nmhTestv2MplsOutSegmentNextHopAddr (UINT4 *, tSNMP_OCTET_STRING_TYPE * ,
                                                tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhSetMplsOutSegmentNextHopAddr (tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2MplsOutSegmentInterface (UINT4 *, tSNMP_OCTET_STRING_TYPE * ,INT4 );
extern INT1 nmhSetMplsOutSegmentInterface (tSNMP_OCTET_STRING_TYPE * ,INT4 );
extern INT1 nmhGetFirstIndexMplsFTNTable (UINT4 *);
extern INT1 nmhGetNextIndexMplsFTNTable (UINT4 , UINT4 *);
extern INT1 nmhGetNextIndexMplsXCTable (tSNMP_OCTET_STRING_TYPE *, 
                                        tSNMP_OCTET_STRING_TYPE *, 
                                        tSNMP_OCTET_STRING_TYPE *, 
                                        tSNMP_OCTET_STRING_TYPE *, 
                                        tSNMP_OCTET_STRING_TYPE *, 
                                        tSNMP_OCTET_STRING_TYPE *);

extern INT1 nmhGetFirstIndexMplsXCTable (tSNMP_OCTET_STRING_TYPE *, 
                                         tSNMP_OCTET_STRING_TYPE *, 
                                         tSNMP_OCTET_STRING_TYPE *);

extern INT1 nmhGetMplsFTNDestAddrMin (UINT4 ,tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetMplsFTNDestAddrMax (UINT4 ,tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetMplsFTNActionPointer (UINT4 ,tSNMP_OID_TYPE * );
extern INT1 nmhGetMplsXCOwner (tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *, 
                               tSNMP_OCTET_STRING_TYPE *,INT4 *);
extern INT1 nmhGetMplsOutSegmentNextHopAddr (tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetMplsInSegmentRowStatus (tSNMP_OCTET_STRING_TYPE *,INT4 *);
extern INT1 nmhGetMplsInSegmentLabel (tSNMP_OCTET_STRING_TYPE *,UINT4 *);
extern INT1 nmhGetMplsOutSegmentRowStatus (tSNMP_OCTET_STRING_TYPE *,INT4 *);
extern INT1 nmhGetMplsOutSegmentTopLabel (tSNMP_OCTET_STRING_TYPE *,UINT4 *);
extern INT1 nmhGetMplsInSegmentInterface (tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetMplsFTNActionType (UINT4, INT4 *);
extern INT1 nmhTestv2MplsFTNRowStatus (UINT4 *, UINT4, INT4);
extern INT1 nmhSetMplsFTNRowStatus (UINT4, INT4);
extern INT1 nmhTestv2MplsXCRowStatus (UINT4 *, tSNMP_OCTET_STRING_TYPE *, 
                                      tSNMP_OCTET_STRING_TYPE *,
                                      tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhSetMplsXCRowStatus (tSNMP_OCTET_STRING_TYPE *, 
                                      tSNMP_OCTET_STRING_TYPE *,
                                      tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhTestv2MplsInSegmentRowStatus (UINT4 *, tSNMP_OCTET_STRING_TYPE *,
                                             INT4);
extern INT1 nmhSetMplsInSegmentRowStatus (tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhGetMplsOutSegmentInterface (tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhTestv2MplsOutSegmentRowStatus (UINT4 *, tSNMP_OCTET_STRING_TYPE *,
                                             INT4);
extern INT1 nmhSetMplsOutSegmentRowStatus (tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhGetMplsXCIndexNext (tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhTestv2MplsFTNActionPointer (UINT4 *, UINT4, tSNMP_OID_TYPE *);
extern INT1 nmhSetMplsFTNActionPointer (UINT4, tSNMP_OID_TYPE *);

extern INT1 nmhGetFirstIndexMplsTunnelTable (UINT4 *, UINT4 *, UINT4 *, UINT4 *);
extern INT1 nmhGetNextIndexMplsTunnelTable  (UINT4, UINT4 *, UINT4, UINT4 *, 
                                             UINT4, UINT4 *, UINT4, UINT4 *);
extern INT1 nmhGetMplsTunnelRole (UINT4,  UINT4,  UINT4,  UINT4, INT4 *);
extern INT1 nmhGetMplsTunnelXCPointer (UINT4,  UINT4,  UINT4,  UINT4 ,
                                       tSNMP_OID_TYPE *);
extern INT1 nmhGetMplsTunnelResourcePointer (UINT4,  UINT4,  UINT4,  UINT4 ,
                                             tSNMP_OID_TYPE *);
extern INT1 nmhGetMplsTunnelAdminStatus (UINT4,  UINT4,  UINT4,  UINT4 ,
                                         INT4 *);
extern INT1 nmhGetMplsTunnelOperStatus (UINT4,  UINT4,  UINT4,  UINT4, 
                                        INT4 *);
extern INT1 nmhGetMplsTunnelRowStatus (UINT4, UINT4, UINT4, UINT4, INT4 *);

extern INT1 nmhGetMplsTunnelResourceMaxRate (UINT4, UINT4 *);
extern INT1 nmhGetMplsTunnelResourceIndexNext (UINT4 *);

extern INT1 nmhTestv2MplsTunnelRole (UINT4 *, UINT4,  UINT4,  UINT4,  UINT4, INT4);
extern INT1 nmhTestv2MplsTunnelXCPointer (UINT4 *, UINT4,  UINT4,  UINT4,  UINT4, 
                                          tSNMP_OID_TYPE *);
extern INT1 nmhTestv2MplsTunnelResourcePointer (UINT4 *, UINT4,  UINT4,  UINT4,  
                                                UINT4, tSNMP_OID_TYPE *);
extern INT1 nmhTestv2MplsTunnelResourceMaxRate (UINT4 *, UINT4, UINT4);
extern INT1 nmhTestv2MplsTunnelAdminStatus (UINT4 *, UINT4,  UINT4,  UINT4,  UINT4, INT4);
extern INT1 nmhTestv2MplsTunnelRowStatus (UINT4 *, UINT4,  UINT4,  UINT4,  UINT4, INT4);
extern INT1 nmhTestv2MplsTunnelResourceRowStatus (UINT4 *, UINT4, INT4);
extern INT1 nmhTestv2MplsTunnelIsIf (UINT4 *, UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhTestv2MplsTunnelName (UINT4 *, UINT4, UINT4, UINT4, UINT4, tSNMP_OCTET_STRING_TYPE *);

extern INT1 nmhSetMplsTunnelRole (UINT4,  UINT4,  UINT4,  UINT4, INT4);
extern INT1 nmhSetMplsTunnelXCPointer (UINT4,  UINT4,  UINT4,  UINT4, tSNMP_OID_TYPE *);
extern INT1 nmhSetMplsTunnelResourcePointer (UINT4,  UINT4,  UINT4,  UINT4, 
                                             tSNMP_OID_TYPE *);
extern INT1 nmhSetMplsTunnelResourceMaxRate (UINT4, UINT4);
extern INT1 nmhSetMplsTunnelAdminStatus (UINT4,  UINT4,  UINT4,  UINT4, INT4);
extern INT1 nmhSetMplsTunnelRowStatus (UINT4,  UINT4,  UINT4,  UINT4,  INT4);
extern INT1 nmhSetMplsTunnelResourceRowStatus (UINT4, INT4);
extern INT1 nmhSetMplsTunnelIsIf (UINT4, UINT4, UINT4, UINT4, INT4);
extern INT1 nmhSetMplsTunnelName (UINT4, UINT4, UINT4,  UINT4, tSNMP_OCTET_STRING_TYPE *);

extern INT1 nmhGetFirstIndexFsMplsVplsConfigTable (UINT4 *);
extern INT1 nmhGetNextIndexFsMplsVplsConfigTable (UINT4 , UINT4 *);
extern INT1 nmhGetFsMplsVplsVsi (UINT4 ,INT4 *);
extern INT1 nmhGetFsMplsVplsVpnId (UINT4 ,tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetFsMplsVplsName (UINT4 ,tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetFsMplsVplsRowStatus (UINT4  ,INT4 *);
extern INT1 nmhGetFsMplsL2VpnVplsIndex (UINT4 , UINT4 *);
extern INT1 nmhGetFsMplsVplsL2MapFdbId (UINT4 , INT4 *);
extern INT1 nmhTestv2FsMplsVplsL2MapFdbId (UINT4 *, UINT4, INT4);
extern INT4 FsMplsVplsL2MapFdbIdSet(tSnmpIndex *, tRetVal *);

extern INT1 nmhSetFsMplsVplsVsi (UINT4  ,INT4 );
extern INT1 nmhSetFsMplsVplsVpnId (UINT4  ,tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetFsMplsVplsName (UINT4  ,tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetFsMplsVplsRowStatus (UINT4  ,INT4 );
extern INT1 nmhSetFsMplsL2VpnVplsIndex (UINT4, INT4);

extern INT1 nmhTestv2FsMplsVplsVsi (UINT4 *  ,UINT4  ,INT4 );
extern INT1 nmhTestv2FsMplsVplsVpnId (UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2FsMplsVplsName (UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2FsMplsVplsRowStatus (UINT4 *  ,UINT4  ,INT4 );
extern INT1 nmhTestv2FsMplsL2VpnVplsIndex (UINT4 *, UINT4, INT4);


extern INT1 nmhGetPwIndexNext (UINT4 *);
extern INT1 nmhGetFirstIndexPwTable (UINT4 *);
extern INT1 nmhGetNextIndexPwTable (UINT4, UINT4 *);
extern INT1 nmhGetPwPeerAddr (UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetPwOutboundLabel (UINT4 ,UINT4 *);
extern INT1 nmhGetPwInboundLabel (UINT4 ,UINT4 *);
extern INT1 nmhGetFsMplsL2VpnPwMode (UINT4 ,INT4 *);
extern INT1 nmhGetPwType (UINT4 ,INT4 *);
extern INT1 nmhGetPwMplsMplsType (UINT4, tSNMP_OCTET_STRING_TYPE *);

extern INT1 nmhSetPwOutboundLabel (UINT4  ,UINT4 );
extern INT1 nmhSetPwInboundLabel (UINT4  ,UINT4 );
extern INT1 nmhSetFsMplsL2VpnPwMode (UINT4  ,INT4 );
extern INT1 nmhSetPwRowStatus (UINT4, INT4);
extern INT1 nmhSetPwPeerAddr (UINT4  ,tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetPwMplsMplsType (UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetPwAdminStatus (UINT4, INT4);
extern INT1 nmhSetPwOwner (UINT4, INT4);
extern INT1 nmhSetPwType (UINT4, INT4);
extern INT1 nmhSetPwPsnType (UINT4, INT4);
extern INT1 nmhSetPwMplsOutboundTunnelIndex (UINT4, INT4);
extern INT1 nmhSetPwMplsOutboundTunnelLclLSR (UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetPwMplsOutboundTunnelPeerLSR (UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetPwMplsOutboundLsrXcIndex (UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetPwMplsOutboundIfIndex (UINT4, UINT4);

extern INT1 nmhTestv2PwMplsMplsType (UINT4 *, UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2PwPeerAddr (UINT4 *, UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2PwInboundLabel (UINT4 *, UINT4, UINT4);
extern INT1 nmhTestv2PwOutboundLabel (UINT4 *, UINT4, UINT4);
extern INT1 nmhTestv2PwRowStatus (UINT4 *, UINT4, INT4);
extern INT1 nmhTestv2FsMplsL2VpnPwMode (UINT4 *  ,UINT4  ,INT4 );
extern INT1 nmhTestv2PwAdminStatus (UINT4 *, UINT4, INT4);
extern INT1 nmhTestv2PwOwner (UINT4 *, UINT4, INT4);
extern INT1 nmhTestv2PwType (UINT4 *, UINT4, INT4);
extern INT1 nmhTestv2PwPsnType (UINT4 *, UINT4, INT4);
extern INT1 nmhTestv2PwMplsOutboundTunnelIndex (UINT4 *, UINT4, INT4);
extern INT1 nmhTestv2PwMplsOutboundTunnelLclLSR (UINT4 *, UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2PwMplsOutboundTunnelPeerLSR (UINT4 *, UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2PwMplsOutboundLsrXcIndex (UINT4 *, UINT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2PwMplsOutboundIfIndex (UINT4 *, UINT4, UINT4);



extern UINT4 MplsGetXCIndexFromPrefix (UINT4 );

extern INT1 nmhGetFirstIndexPwEnetTable (UINT4 * , UINT4 *);
extern INT1 nmhGetNextIndexPwEnetTable (UINT4 , UINT4 * , UINT4 , UINT4 *);
extern INT1 nmhGetPwEnetPortVlan (UINT4  , UINT4 ,UINT4 *);
extern INT1 nmhGetPwEnetPortIfIndex (UINT4  , UINT4 ,INT4 *);

extern INT1 nmhTestv2PwEnetRowStatus (UINT4 *, UINT4, UINT4, INT4);
extern INT1 nmhTestv2PwEnetPortVlan (UINT4 *, UINT4, UINT4, UINT4);
extern INT1 nmhTestv2PwEnetPortIfIndex (UINT4 *, UINT4, UINT4, INT4);
extern INT1 nmhTestv2PwEnetPwVlan (UINT4 *, UINT4, UINT4, UINT4);

extern INT1 nmhSetPwEnetRowStatus (UINT4, UINT4, INT4);
extern INT1 nmhSetPwEnetPortVlan (UINT4, UINT4, UINT4);
extern INT1 nmhSetPwEnetPortIfIndex (UINT4, UINT4, INT4);
extern INT1 nmhSetPwEnetPwVlan (UINT4, UINT4, UINT4);


#ifdef VPLSADS_WANTED
extern INT4 L2VpnUtilParseAndGenerateRdRt ARG_LIST ((UINT1* pu1RandomString,UINT1* pu1RdRt));
extern INT1 nmhGetFsmplsVplsMtu ARG_LIST((UINT4 ,UINT4 *));
extern INT1 nmhGetFsmplsVplsControlWord ARG_LIST((UINT4 ,INT4 *));
extern INT1 nmhGetVplsBgpADConfigRouteDistinguisher ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT4 L2VpnGetFreeRtIndexForVpls (UINT4 u4VplsIndex, UINT4 * pu4RtIndex);
extern INT1 nmhGetVplsBgpRteTargetRT ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetVplsBgpVEName ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhTestv2FsmplsVplsSignalingType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2FsmplsVplsMtu ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));
extern INT1 nmhTestv2FsmplsVplsControlWord ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhTestv2VplsBgpADConfigRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhSetVplsBgpADConfigRowStatus ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhTestv2VplsBgpADConfigRouteDistinguisher ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetVplsBgpADConfigRouteDistinguisher ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2VplsBgpRteTargetRTRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetVplsBgpRteTargetRTRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2VplsBgpRteTargetRT ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetVplsBgpRteTargetRT ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetVplsBgpRteTargetRTType ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2VplsBgpVERowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));
extern INT1 nmhSetVplsBgpVERowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));
extern INT1 nmhTestv2VplsBgpVEName ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetVplsBgpVEName ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));
#endif  


extern INT4 MplsFTNRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsXCRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsInSegmentRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsOutSegmentRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsFTNActionPointerSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsFTNActionTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsFTNAddrTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsFTNDestAddrMinSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsFTNDestAddrMaxSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsFTNMaskSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsInSegmentAddrFamilySet(tSnmpIndex *, tRetVal *);
extern INT4 MplsInSegmentLabelSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsInSegmentNPopSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsInSegmentInterfaceSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsOutSegmentTopLabelSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsOutSegmentPushTopLabelSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsOutSegmentNextHopAddrTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsOutSegmentNextHopAddrSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsOutSegmentInterfaceSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsTunnelRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsTunnelIsIfSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsTunnelNameSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsTunnelRoleSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsTunnelResourceRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsTunnelResourceMaxRateSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsTunnelResourcePointerSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsTunnelAdminStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsMplsVplsRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsMplsVplsVsiSet(tSnmpIndex *, tRetVal *);
extern INT4 FsMplsVplsNameSet(tSnmpIndex *, tRetVal *);
extern INT4 FsMplsVplsVpnIdSet(tSnmpIndex *, tRetVal *);
#ifdef VPLSADS_WANTED
extern INT4 FsmplsVplsMtuSet(tSnmpIndex *, tRetVal *);
extern INT4 FsmplsVplsControlWordSet(tSnmpIndex *, tRetVal *);
extern INT4 FsmplsVplsSignalingTypeSet(tSnmpIndex *, tRetVal *);
#endif
extern INT4 PwRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 PwAdminStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 PwOwnerSet(tSnmpIndex *, tRetVal *);
extern INT4 PwPsnTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 PwPeerAddrSet(tSnmpIndex *, tRetVal *);
extern INT4 PwInboundLabelSet(tSnmpIndex *, tRetVal *);
extern INT4 PwOutboundLabelSet(tSnmpIndex *, tRetVal *);
extern INT4 FsMplsL2VpnPwModeSet(tSnmpIndex *, tRetVal *);
extern INT4 FsMplsL2VpnVplsIndexSet(tSnmpIndex *, tRetVal *);
extern INT4 PwTypeSet(tSnmpIndex *, tRetVal *);
extern INT4 PwMplsOutboundTunnelIndexSet(tSnmpIndex *, tRetVal *);
extern INT4 PwMplsOutboundTunnelLclLSRSet(tSnmpIndex *, tRetVal *);
extern INT4 PwMplsOutboundTunnelPeerLSRSet(tSnmpIndex *, tRetVal *);
extern INT4 PwMplsOutboundLsrXcIndexSet(tSnmpIndex *, tRetVal *);
extern INT4 PwMplsOutboundIfIndexSet(tSnmpIndex *, tRetVal *);
extern INT4 MplsTunnelXCPointerSet(tSnmpIndex *, tRetVal *);
extern INT4 PwEnetRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 PwEnetPwVlanSet(tSnmpIndex *, tRetVal *);
extern INT4 PwEnetPortIfIndexSet(tSnmpIndex *, tRetVal *);
extern INT4 PwEnetPortVlanSet(tSnmpIndex *, tRetVal *);
extern INT4 PwMplsMplsTypeSet(tSnmpIndex *, tRetVal *);

extern UINT4 MplsTunnelName [13];

#ifdef MPLS_L3VPN_WANTED

INT4 
IssMplsCreateL3vpn (tHttp *, UINT1 *, UINT1 *, UINT1 **,UINT1 [][L3VPN_MAX_VPLS_RT_LEN],UINT1 *, UINT1 *, UINT1 *, UINT1 *);

INT4
IssMplsDeleteL3vpn (tHttp *, UINT1 *);

extern INT1
nmhGetMplsL3VpnVrfVpnId ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetMplsL3VpnVrfConfRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern INT1
nmhTestv2MplsL3VpnVrfConfRowStatus ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetMplsL3VpnVrfConfRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2MplsL3VpnVrfVpnId ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetMplsL3VpnVrfVpnId ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhGetMplsL3VpnVrfRD ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhTestv2MplsL3VpnVrfRD ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetMplsL3VpnVrfRD ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2MplsL3VpnVrfConfMaxRoutes ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

extern INT1
nmhSetMplsL3VpnVrfConfMaxRoutes ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

extern INT1
nmhTestv2MplsL3VpnVrfConfHighRteThresh ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

extern INT1
mhSetMplsL3VpnVrfConfHighRteThresh ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

extern INT1
nmhTestv2MplsL3VpnVrfConfMidRteThresh ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

extern INT1
nmhSetMplsL3VpnVrfConfMidRteThresh ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

extern INT1
nmhGetNextIndexMplsL3VpnVrfTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFirstIndexMplsL3VpnVrfTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFirstIndexMplsL3VpnVrfRTTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 * , INT4 *));

extern INT1
nmhGetMplsL3VpnVrfRT ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetNextIndexMplsL3VpnVrfRTTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 ,UINT4 * , INT4 , INT4 *));

extern INT1
nmhTestv2MplsL3VpnVrfDescription ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetMplsL3VpnVrfDescription ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2MplsL3VpnVrfRTRowStatus ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  ,INT4 ));

extern INT1
nmhTestv2MplsL3VpnVrfRT ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetMplsL3VpnVrfRTRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  ,INT4 ));

extern INT1
nmhSetMplsL3VpnVrfRT ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetMplsL3VpnVrfConfHighRteThresh ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));
extern INT4 IssMplsUpdateL3vpn(tHttp * , UINT1 *,UINT1 **, UINT1 [][L3VPN_MAX_VPLS_RT_LEN]);
extern INT4 IssMplsDeleteRtL3vpn(tHttp * , UINT1 *,UINT1  [L3VPN_MAX_RT_LEN]);
#endif
#endif
#endif
