
/* $Id: rmapweb.h,v 1.6 2013/07/05 15:20:50 siva Exp $            */
#ifndef _RMAPWEB_H
#define _RMAPWEB_H


VOID IssProcessRMapCreationPage (tHttp *pHttp);
VOID IssProcessRMapMatchPage (tHttp *pHttp);
VOID IssProcessRMapSetPage (tHttp *pHttp);
VOID IssProcessRMapIpPrefixPage (tHttp *pHttp);
VOID IssProcessRMapIpPrefixPageSet (tHttp *pHttp);
VOID IssProcessRMapIpPrefixPageGet (tHttp *pHttp);

VOID IssProcessRMapCreationGet (tHttp *pHttp);
VOID IssProcessRMapCreationSet (tHttp *pHttp);

VOID IssProcessRMapSetTableGet (tHttp *pHttp);
VOID IssProcessRMapSetTableSet (tHttp *pHttp);

INT1* str_replace(INT1* income, INT1* find, INT1* replace);

#define  RMAP_ACTIVE                1
#define  RMAP_NOT_IN_SERVICE        2
#define  RMAP_NOT_READY             3
#define  RMAP_CREATE_AND_GO         4
#define  RMAP_CREATE_AND_WAIT       5
#define  RMAP_DESTROY               6

/* Proto Type for Low Level GET FIRST fn for FsRMapTable  */
extern INT1
nmhGetFirstIndexFsRMapTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE *  ,
    UINT4    *));

/* Proto type for GET_NEXT Routine.  */
extern INT1
nmhGetNextIndexFsRMapTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE *,
    tSNMP_OCTET_STRING_TYPE *  ,
    UINT4    ,
    UINT4    *));

/* Proto type for Low Level GET Routine All Objects.  */
extern INT1
nmhGetFsRMapAccess ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4    ,
    INT4    *));

extern INT1
nmhGetFsRMapRowStatus ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4    ,
    INT4    *));

/* Low Level TEST Routines for.  */
extern INT1
nmhTestv2FsRMapAccess ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4    ));

extern INT1
nmhTestv2FsRMapRowStatus ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4    ));

/* Low Level SET Routine for All Objects.  */
extern INT1
nmhSetFsRMapAccess ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4    ));

extern INT1
nmhSetFsRMapRowStatus ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4    ));


/*------------------------------------------------------------*/

VOID IssProcessRMapMatchTableGet (tHttp *pHttp);
VOID IssProcessRMapMatchTableSet (tHttp *pHttp);

/* Proto Validate Index Instance for FsRMapMatchTable. */
extern INT1
nmhValidateIndexInstanceFsRMapMatchTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4     ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4     ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4     ,
    tSNMP_OCTET_STRING_TYPE * ,
    INT4     ,
    INT4     ,
    UINT4     ,
    INT4     ,
    INT4     ,
    UINT4     ,
    UINT4     ,
    INT4     ,
    INT4    ));

/* Proto Type for Low Level GET FIRST fn for FsRMapMatchTable  */

extern INT1
nmhGetFirstIndexFsRMapMatchTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE *  ,
    UINT4    * ,
    INT4    * ,
    tSNMP_OCTET_STRING_TYPE *  ,
    UINT4    * ,
    INT4    * ,
    tSNMP_OCTET_STRING_TYPE *  ,
    UINT4    * ,
    INT4    * ,
    tSNMP_OCTET_STRING_TYPE *  ,
    INT4    * ,
    INT4    * ,
    UINT4    * ,
    INT4    * ,
    INT4    * ,
    UINT4    * ,
    UINT4    * ,
    INT4    * ,
    INT4    *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsRMapMatchTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE *,
    tSNMP_OCTET_STRING_TYPE *  ,
    UINT4    ,
    UINT4    * ,
    INT4    ,
    INT4    * ,
    tSNMP_OCTET_STRING_TYPE *,
    tSNMP_OCTET_STRING_TYPE *  ,
    UINT4    ,
    UINT4    * ,
    INT4    ,
    INT4    * ,
    tSNMP_OCTET_STRING_TYPE *,
    tSNMP_OCTET_STRING_TYPE *  ,
    UINT4    ,
    UINT4    * ,
    INT4    ,
    INT4    * ,
    tSNMP_OCTET_STRING_TYPE *,
    tSNMP_OCTET_STRING_TYPE *  ,
    INT4    ,
    INT4    * ,
    INT4    ,
    INT4    * ,
    UINT4    ,
    UINT4    * ,
    INT4    ,
    INT4    * ,
    INT4    ,
    INT4    * ,
    UINT4    ,
    UINT4    * ,
    UINT4    ,
    UINT4    * ,
    INT4    ,
    INT4    * ,
    INT4    ,
    INT4    *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsRMapMatchRowStatus ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4     ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4     ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4     ,
    tSNMP_OCTET_STRING_TYPE * ,
    INT4     ,
    INT4     ,
    UINT4     ,
    INT4     ,
    INT4     ,
    UINT4     ,
    UINT4     ,
    INT4     ,
    INT4    ,
    INT4    *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsRMapMatchRowStatus ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4     ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4     ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4     ,
    tSNMP_OCTET_STRING_TYPE * ,
    INT4     ,
    INT4     ,
    UINT4     ,
    INT4     ,
    INT4     ,
    UINT4     ,
    UINT4     ,
    INT4     ,
    INT4     ,
    INT4    ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsRMapMatchRowStatus ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4     ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4     ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4     ,
    tSNMP_OCTET_STRING_TYPE * ,
    INT4     ,
    INT4     ,
    UINT4     ,
    INT4     ,
    INT4     ,
    UINT4     ,
    UINT4     ,
    INT4     ,
    INT4     ,
    INT4    ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2FsRMapMatchTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRMapSetTable. */
extern INT1
nmhValidateIndexInstanceFsRMapSetTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4    ));



/* Proto Type for Low Level GET FIRST fn for FsRMapSetTable  */

extern INT1
nmhGetFirstIndexFsRMapSetTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE *  ,
    UINT4    *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexFsRMapSetTable ARG_LIST((
    tSNMP_OCTET_STRING_TYPE *,
    tSNMP_OCTET_STRING_TYPE *  ,
    UINT4    ,
    UINT4    *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsRMapSetNextHopInetType ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4    ,
    INT4    *));

extern INT1
nmhGetFsRMapSetNextHopInetAddr ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4    ,
    tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsRMapSetInterface ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4    ,
    INT4    *));

extern INT1
nmhGetFsRMapSetMetric ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4    ,
    INT4    *));

extern INT1
nmhGetFsRMapSetTag ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4    ,
    UINT4    *));

extern INT1
nmhGetFsRMapSetRouteType ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4    ,
    INT4    *));

extern INT1
nmhGetFsRMapSetASPathTag ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4    ,
    UINT4    *));

extern INT1
nmhGetFsRMapSetCommunity ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4    ,
    UINT4    *));

extern INT1
nmhGetFsRMapSetLocalPref ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4    ,
    INT4    *));

extern INT1
nmhGetFsRMapSetOrigin ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4    ,
    INT4    *));

extern INT1
nmhGetFsRMapSetWeight ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4    ,
    UINT4    *));

extern INT1
nmhGetFsRMapSetEnableAutoTag ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4    ,
    INT4    *));

extern INT1
nmhGetFsRMapSetLevel ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4    ,
    INT4    *));

extern INT1
nmhGetFsRMapSetExtCommId ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4    ,
    UINT4   *));

extern INT1
nmhGetFsRMapSetExtCommCost ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4    ,
    UINT4   *));

extern INT1
nmhGetFsRMapSetRowStatus ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4    ,
    INT4    *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsRMapSetNextHopInetType ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4    ));

extern INT1
nmhSetFsRMapSetNextHopInetAddr ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsRMapSetInterface ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4    ));

extern INT1
nmhSetFsRMapSetMetric ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4    ));

extern INT1
nmhSetFsRMapSetTag ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    UINT4    ));

extern INT1
nmhSetFsRMapSetRouteType ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4    ));

extern INT1
nmhSetFsRMapSetASPathTag ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    UINT4    ));

extern INT1
nmhSetFsRMapSetCommunity ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    UINT4    ));

extern INT1
nmhSetFsRMapSetLocalPref ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4    ));

extern INT1
nmhSetFsRMapSetOrigin ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4    ));

extern INT1
nmhSetFsRMapSetWeight ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    UINT4    ));

extern INT1
nmhSetFsRMapSetEnableAutoTag ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4    ));

extern INT1
nmhSetFsRMapSetLevel ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4    ));

extern INT1
nmhSetFsRMapSetExtCommId ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4    ,
    UINT4   ));

extern INT1
nmhSetFsRMapSetExtCommCost ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4    ,
    UINT4   ));

extern INT1
nmhSetFsRMapSetRowStatus ARG_LIST((
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4    ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsRMapSetNextHopInetType ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4    ));

extern INT1
nmhTestv2FsRMapSetNextHopInetAddr ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2FsRMapSetInterface ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4    ));

extern INT1
nmhTestv2FsRMapSetMetric ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4    ));

extern INT1
nmhTestv2FsRMapSetTag ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    UINT4    ));

extern INT1
nmhTestv2FsRMapSetRouteType ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4    ));

extern INT1
nmhTestv2FsRMapSetASPathTag ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    UINT4    ));

extern INT1
nmhTestv2FsRMapSetCommunity ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    UINT4    ));

extern INT1
nmhTestv2FsRMapSetLocalPref ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4    ));

extern INT1
nmhTestv2FsRMapSetOrigin ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4    ));

extern INT1
nmhTestv2FsRMapSetWeight ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    UINT4    ));

extern INT1
nmhTestv2FsRMapSetEnableAutoTag ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4    ));

extern INT1
nmhTestv2FsRMapSetLevel ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4    ));

extern INT1
nmhTestv2FsRMapSetExtCommId ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    UINT4   ));

extern INT1
nmhTestv2FsRMapSetExtCommCost ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    UINT4   ));

extern INT1
nmhTestv2FsRMapSetRowStatus ARG_LIST((UINT4 *  ,
    tSNMP_OCTET_STRING_TYPE * ,
    UINT4     ,
    INT4    ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2FsRMapSetTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

extern INT1 nmhGetFsRMapMatchDestMaxPrefixLen ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , UINT4  , INT4  , INT4  , UINT4  , UINT4  , INT4  , INT4 ,UINT4 *));

extern INT1 nmhGetFsRMapMatchDestMinPrefixLen ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , UINT4  , INT4  , INT4  , UINT4  , UINT4  , INT4  , INT4 ,UINT4 *));

extern INT1 nmhGetFsRMapIsIpPrefixList ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));
extern INT1 nmhSetFsRMapIsIpPrefixList ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));
extern INT1 nmhTestv2FsRMapIsIpPrefixList ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

extern INT1 nmhSetFsRMapMatchDestMaxPrefixLen ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , UINT4  , INT4  , INT4  , UINT4  , UINT4  , INT4  , INT4  ,UINT4 ));

extern INT1 nmhSetFsRMapMatchDestMinPrefixLen ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , UINT4  , INT4  , INT4  , UINT4  , UINT4  , INT4  , INT4  ,UINT4 ));

extern INT1 nmhTestv2FsRMapMatchDestMaxPrefixLen ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , UINT4  , INT4  , INT4  , UINT4  , UINT4  , INT4  , INT4  ,UINT4 ));

extern INT1 nmhTestv2FsRMapMatchDestMinPrefixLen ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , UINT4  , INT4  , INT4  , UINT4  , UINT4  , INT4  , INT4  ,UINT4 ));

typedef INT1 (*tpnmhSetFsRMapSetINT)(tSNMP_OCTET_STRING_TYPE*, UINT4, INT4);
typedef INT1 (*tpnmhTestv2FsRMapSetINT)(UINT4*, tSNMP_OCTET_STRING_TYPE*, UINT4, INT4);
typedef INT1 (*tpnmhSetFsRMapSetOST)(tSNMP_OCTET_STRING_TYPE*, UINT4, tSNMP_OCTET_STRING_TYPE*);
typedef INT1 (*tpnmhTestv2FsRMapSetOST)(UINT4*, tSNMP_OCTET_STRING_TYPE*, UINT4, tSNMP_OCTET_STRING_TYPE*);

typedef union _unpnmhSetFsRMapSet {
    tpnmhSetFsRMapSetINT pnmhSetFsRMapSetINT;
    tpnmhSetFsRMapSetOST pnmhSetFsRMapSetOST;
} tunpnmhSetFsRMapSet;

typedef union _pnnmhTestv2FsRMapSet {
    tpnmhTestv2FsRMapSetINT pnmhTestv2FsRMapSetINT;
    tpnmhTestv2FsRMapSetOST pnmhTestv2FsRMapSetOST;
} tunpnnmhTestv2FsRMapSet;

typedef struct {
    tunpnmhSetFsRMapSet      pnmhSetFsRMapSet;
    tunpnnmhTestv2FsRMapSet  pnmhTestv2FsRMapSet;
} tSetTestv2FsRMapSetStruct;

#endif
