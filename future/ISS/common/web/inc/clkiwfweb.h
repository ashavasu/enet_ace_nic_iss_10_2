/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: clkiwfweb.h,v 1.3 2014/07/18 12:23:39 siva Exp $
 *
 * Description:  macros,typedefs and prototypes for CLKIWF web Module
 ********************************************************************/
#ifndef _CLKIWFWEB_H
#define _CLKIWFWEB_H

#ifdef CLKIWF_WANTED

#include "fsclk.h"

/* Global CLKIWF configuration routines */
VOID IssProcessClkiwfConfPageSet PROTO ((tHttp * pHttp));
VOID IssProcessClkiwfConfPageGet PROTO ((tHttp * pHttp));
VOID IssProcessClkiwfConfPage PROTO ((tHttp * pHttp));

/* Routines to set the CLKIWF parameters */ 
INT4 ClkIwfWebSetClockHoldOver PROTO ((INT4 i4Status));
INT4 ClkIwfWebSetClockUtcOffset PROTO ((UINT1 *au1ClockUtcOffset));
INT4 ClkIwfWebSetClockTimeSource PROTO ((INT4 i4ClockTimeSource));
INT4 ClkIwfWebSetClockClass PROTO ((INT4 i4ClockClass));
INT4 ClkIwfWebSetClockVariance PROTO ((INT4 i4ClockVariance));
INT4 ClkIwfWebSetClockAccuracy PROTO ((INT4 i4ClockAccuracy));
INT4 ClkIwWebPrintAccuracy PROTO ((INT4 i4ClockAccuracy, CHR1 pu1ClkAccuracyStr[]));
/* nmh routines to get, set and test CLKWIF parameters*/ 
extern INT1 nmhGetFsClkIwfClockVariance ARG_LIST((INT4 *));
extern INT1 nmhGetFsClkIwfClockClass ARG_LIST((INT4 *));
extern INT1 nmhGetFsClkIwfClockAccuracy ARG_LIST((INT4 *));
extern INT1 nmhGetFsClkIwfClockTimeSource ARG_LIST((INT4 *));
extern INT1 nmhGetFsClkIwfUtcOffset ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsClkIwfHoldoverSpecification ARG_LIST((INT4 *));

extern INT1 nmhSetFsClkIwfClockVariance ARG_LIST((INT4 ));
extern INT1 nmhSetFsClkIwfClockClass ARG_LIST((INT4 ));
extern INT1 nmhSetFsClkIwfClockAccuracy ARG_LIST((INT4 ));
extern INT1 nmhSetFsClkIwfClockTimeSource ARG_LIST((INT4 ));
extern INT1 nmhSetFsClkIwfUtcOffset ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsClkIwfHoldoverSpecification ARG_LIST((INT4 ));

extern INT1 nmhTestv2FsClkIwfClockVariance ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FsClkIwfClockClass ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FsClkIwfClockAccuracy ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FsClkIwfClockTimeSource ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FsClkIwfUtcOffset ARG_LIST((UINT4 * ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsClkIwfHoldoverSpecification ARG_LIST((UINT4 *  ,INT4 ));

#endif
#endif
