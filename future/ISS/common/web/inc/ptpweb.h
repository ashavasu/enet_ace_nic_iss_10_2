/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 * 
 * $Id: ptpweb.h,v 1.4 2011/10/25 11:17:53 siva Exp $
 * 
 * Description:  macros,typedefs and prototypes for PTP web Module
 ********************************************************************/
#ifndef _PTPWEB_H
#define _PTPWEB_H

#ifdef PTP_WANTED

/* Macros for system control */
#define PTP_START                        1
#define PTP_SHUTDOWN                     2

/* Macros for module status */
#define PTP_ENABLED                      1
#define PTP_DISABLED                     2

#define PTP_MASTER_MAX_ADDR_LEN     24
#define CLK_U8_STR_LEN              21
/* Trap Octet[0] Mask values */
#define PTP_TRAP_MASK_GLOB_ERR                  0x01
#define PTP_TRAP_MASK_SYS_CTRL_CHNG             0x02
#define PTP_TRAP_MASK_SYS_ADMIN_CHNG            0x04
#define PTP_TRAP_MASK_PORT_STATE_CHNG           0x08
#define PTP_TRAP_MASK_PORT_ADMIN_CHNG           0x10
#define PTP_TRAP_MASK_SYNC_FAULT                0x20
#define PTP_TRAP_MASK_GM_FAULT                  0x40
#define PTP_TRAP_MASK_ACC_MASTER_FAULT          0x80

/* Trap Octet[1] Mask values */                                                                                  
#define PTP_TRAP_MASK_UCAST_ADMIN_CHNG          0x01
                                                                                                                  /* Length of the Ptp Trap Input */
#define PTP_TRAP_MAX_LEN                        2
                                                                                                                  /* Trap information string length */
#define PTP_MAX_TRAP_STR_LEN          32

#define PTP_MAX_ADDR_LEN      30
#define PTP_E2E_TC_CLOCK_MODE 5
#define PTP_P2P_TC_CLOCK_MODE 6


#define PTP_PORT_DELAY_MECH_END_TO_END    1
#define PTP_PORT_DELAY_MECH_PEER_TO_PEER  2

#define PTP_IPADDR_TO_OCT(u4Addr,Oct) \
    Oct[0] = (UINT1) ((u4Addr & 0xFF000000) >> 24); \
    Oct[1] = (UINT1) ((u4Addr & 0x00FF0000) >> 16); \
    Oct[2] = (UINT1) ((u4Addr & 0x0000FF00) >> 8); \
    Oct[3] = (UINT1) ((u4Addr & 0x000000FF))


#define PTP_INVALID_TRC   0x00000000
#define PTP_CRITICAL_TRC  0x00000100

#define PTP_ALL_TRC (INIT_SHUT_TRC | MGMT_TRC | \
                      CONTROL_PLANE_TRC | DUMP_TRC | OS_RESOURCE_TRC | \
                      ALL_FAILURE_TRC | BUFFER_TRC | PTP_CRITICAL_TRC)


/***************************************************************************
                                 ptpweb.c 
****************************************************************************/
INT4
PtpWebDelAccMaster PROTO ((INT4 i4ContextId,
                    INT4 i4DomainId, INT4 i4NwProto, INT4 i4AccLen,
                    UINT1 *u1AccMasterAddr));

VOID
IssProcessPtpAccMasterGet PROTO ((tHttp * pHttp));

INT4
PtpWebSetDebugs PROTO ((UINT1 *pu1TraceInput));

INT4
PtpWebSetCxtDebugs PROTO ((INT4 i4ContextId,
                           UINT1 *pu1TraceInput));

extern INT4 PtpPortVcmGetAliasName  (UINT4 u4ContextId, UINT1 *pu1Alias);

extern INT1 PtpUtilGetDebugLevel (UINT4 u4ContextId, UINT4 * u4DbgLevel);

VOID
IssProcessPtpTraceConfPage PROTO ((tHttp * pHttp));


VOID
IssProcessPtpTraceConfPageGet PROTO ((tHttp * pHttp));


VOID
IssProcessPtpTraceConfPageSet PROTO ((tHttp * pHttp));

INT4 
PtpWebSetPortNumOfAlternateMasters PROTO ((INT4 i4ContextId,                                                                                                 INT4 i4DomainId, INT4 i4PtpPortId,                                                                                INT4 i4MaxAltMasters));

INT4
PtpWebSetNotifyStatus PROTO ((UINT4 u4TrapValue,
                              UINT1 u1TrapFlag));

INT4
PtpWebSetSystemControl PROTO ((INT4 i4ContextId, INT4 i4Status));

INT4
PtpWebSetGlobalSysControl PROTO ((INT4 i4Status));

INT4
PtpWebSetPrimaryDomain PROTO ((INT4 i4ContextId,
                        INT4 i4PrimaryDomain));

INT4
PtpWebSetPrimaryContext PROTO ((INT4 i4PrimaryContext));


VOID
IssProcessPtpGlobalConfPageSet PROTO ((tHttp * pHttp));

VOID
IssProcessPtpGlobalConfPageGet PROTO ((tHttp * pHttp));

VOID
IssProcessPtpGlobalConfPage PROTO ((tHttp * pHttp));

VOID
PtpWebConverPortAddToOct PROTO ((UINT1 *u1AccMasterAddr, INT4 i4NwProto,
                          tSNMP_OCTET_STRING_TYPE * pAccMasterAddr));

INT4
PtpWebSetAccMasterAltPriority PROTO ((INT4 i4ContextId,
                               INT4 i4DomainId, INT4 i4NwProto, INT4 i4AccLen,
                               UINT1 *u1AccMasterAddr, INT4 i4AlternatePriority));

INT4
PtpWebConfigAccMaster PROTO ((INT4 i4ContextId,
                       INT4 i4DomainId, INT4 i4NwProto, INT4 i4AccLen,
                       CHR1 * u1AccMasterAddr));

VOID
IssProcessPtpAltTimeSet PROTO ((tHttp * pHttp));

VOID
IssProcessPtpAccMasterSet PROTO ((tHttp * pHttp));

VOID
IssProcessPtpAltTimePage PROTO ((tHttp * pHttp));


VOID
IssProcessPtpAccMasterPage PROTO ((tHttp * pHttp));

INT4
PtpWebUpdateAccMasterEntry PROTO(( INT4 i4ContextId,
                            INT4 i4DomainId, INT4 i4NwProto,
                            INT4 i4AddrLen,
                            tSNMP_OCTET_STRING_TYPE pAccMasterAddr,
                            INT4 i4RowStatus));

INT4
PtpWebDeletePortFromDomain PROTO (( INT4 i4ContextId,
                            INT4 i4DomainId, INT4 i4PtpPortNo));

INT4
PtpWebConfigAltTimeScale PROTO (( INT4 i4ContextId,
                          INT4 i4DomainId, INT4 i4AltTimeScaleKeyId));

INT4
PtpWebDelAltTimeScale PROTO (( INT4 i4ContextId,
                       INT4 i4DomainId, INT4 i4AltTimeScaleKeyId));


INT4
PtpWebUpdateAltTimeScaleEntry PROTO ( (INT4 i4ContextId,
                               INT4 i4DomainId, INT4 i4AltTimeScaleKeyId,
                               INT4 i4RowStatus));

INT4
PtpWebSetAccMasterFlag PROTO ((INT4 i4ContextId,
                        INT4 i4DomainId, INT4 i4PortId, INT4 i4Status));


INT4
PtpWebSetAltMasterFlag PROTO ((INT4 i4ContextId,
                        INT4 i4DomainId, INT4 i4PortId, INT4 i4Status));
INT4
PtpWebUpdateATSEntryOptParams PROTO ((INT4 i4ContextId,
                               INT4 i4DomainId, INT4 i4KeyIndex,
                               INT4 i4ATSOffset, INT4 i4ATSJmpSecs,
                               UINT1 *pu1AltTimeScaleJump));

INT4
PtpWebSetAltTimeDispName PROTO ( (INT4 i4ContextId,
                          INT4 i4DomainId, INT4 i4KeyIndex,
                          UINT1 *pu1AltTimeDispName));


INT4
PtpWebSetTransPortAdminStatus PROTO((INT4 i4ContextId,
                               INT4 i4DomainId, UINT4 u4PtpPortNo,
                               INT4 i4Status));


INT4
PtpWebSetAltMcastSyncInterval PROTO (( INT4 i4ContextId,
                               INT4 i4DomainId,
                               INT4 i4PortId, INT4 i4SyncInterval));

INT4
PtpWebSetPortAdminStatus PROTO (( INT4 i4ContextId,
                           INT4 i4DomainId, UINT4 u4PtpPortNo, INT4 i4Status));

INT4
PtpWebSetPortAdminStaus PROTO (( INT4 i4ContextId,
                          INT4 i4DomainId, UINT4 u4PtpPortNo,
                          INT4 i4Status));

INT4
PtpWebUpdateTransClkNumberPorts PROTO(( INT4 i4ContextId,
                                        INT4 i4DomainId, INT4 i4NumberPorts));

INT4
PtpWebUpdateClockNumberPorts PROTO (( INT4 i4ContextId,
                                      INT4 i4DomainId, INT4 i4NumberPorts));
INT4
PtpWebSetClockNumberPorts PROTO (( INT4 i4ContextId,
                                   INT4 i4DomainId, INT4 i4NumberPorts));
INT4
PtpWebSetClkPathTrace PROTO ((INT4 i4ContextId,
                        INT4 i4DomainId, INT4 i4Status));

INT4
PtpWebSetDelayRequest PROTO ((INT4 i4ContextId,
                             INT4 i4DomainId, INT4 i4PortId, INT4 i4DelayReq));


INT4
PtpWebSetDelayReqInterval PROTO (( INT4 i4ContextId,
                           INT4 i4DomainId,
                           INT4 i4PortId, INT4 i4DelayReqInterval,
                           UINT1 u1IsTransPort));

INT4
PtpWebSetAnnounceTimeOut PROTO ((INT4 i4ContextId,
                          INT4 i4DomainId,
                          INT4 i4PortId, INT4 i4AnnounceTimeOut));

INT4
PtpWebSetAnnounceInterval PROTO ((INT4 i4ContextId,
                           INT4 i4DomainId,
                           INT4 i4PortId, INT4 i4AnnounceInterval));


INT4
PtpWebSetTwoStepFlag PROTO ((INT4 i4ContextId, INT4 i4DomainId,
                      INT4 i4TwoStepFlag));

INT4
PtpWebSetVersion PROTO ((INT4 i4ContextId,
                  INT4 i4DomainId, INT4 i4PortId, INT4 i4Version));
INT4
PtpWebSetDelayType PROTO ((INT4 i4ContextId,
                     INT4 i4DomainId, INT4 i4PortId, INT4 i4DelayType));


INT4
PtpWebSetSyncInterval PROTO ((INT4 i4ContextId,
                              INT4 i4DomainId, INT4 i4PortId, INT4 i4SyncInterval));


INT4
PtpWebSetClkSlaveOnly PROTO (( INT4 i4ContextId,
                               INT4 i4DomainId, INT4 i4SlaveOnly));

INT4
PtpWebSetClkPriority2 PROTO ((INT4 i4ContextId,
                       INT4 i4DomainId, INT4 i4ClkPriority2));

INT4
PtpWebSetClkPriority1 PROTO ((INT4 i4ContextId,
                       INT4 i4DomainId, INT4 i4ClkPriority1));

INT4
PtpWebSetClkMode PROTO ((INT4 i4ContextId, INT4 i4DomainId, INT4 i4ClockMode));

INT4
PtpWebConfigureContext PROTO ((INT4 i4ContextId, INT4 i4DomainId));

VOID
IssProcessPtpClockSet PROTO((tHttp * pHttp));

VOID
IssProcessPtpClockPage PROTO ((tHttp * pHttp));

VOID
IssProcessPtpIfPage PROTO ((tHttp * pHttp));

VOID 
IssProcessPtpIfGet PROTO ((tHttp * pHttp));

VOID
IssProcessPtpIfSet PROTO ((tHttp * pHttp));


PUBLIC INT4
PtpIfGetPortNumber PROTO ((INT4 i4ContextId, UINT1 u1DomainId, INT4 i4IfType,
                    UINT4 u4IfIndex, INT4 *pi4PtpPortNumber));




PUBLIC INT4
PtpWebMapPortToClk PROTO (( INT4 i4ContextId, INT4 i4DomainId,
                           INT4 i4IfType, INT4 i4IfIndex, INT4 i4PtpPortNo));
PUBLIC INT4
PtpWebMapPortToTransClk PROTO(( INT4 i4ContextId, INT4 i4DomainId,
                          INT4 i4IfType, INT4 i4IfIndex, INT4 i4PtpPortNo));

PUBLIC VOID                                                                                                       PtpIfGetNextAvailableIndex PROTO ((UINT4 u4ContextId, UINT1 u1DomainId,
                            UINT4 *pu4PortNum));


#endif
#endif
