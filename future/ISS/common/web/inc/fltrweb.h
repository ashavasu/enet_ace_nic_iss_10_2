/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: fltrweb.h,v 1.4 2011/11/04 12:47:16 siva Exp $
 *
 * Description:  macros, typdefs and proto type for Filtering web module
 ********************************************************************/
#ifndef _FLTRWEB_H
#define _FLTRWEB_H

#ifdef OSPF_WANTED

VOID IssProcessFilterOspfConfPage (tHttp * pHttp);
VOID IssSetProcessFilterOspfConfPage (tHttp * pHttp);
VOID IssGetProcessFilterOspfConfPage (tHttp * pHttp);


extern INT1 nmhGetFirstIndexFsMIOspfDistInOutRouteMapTable ARG_LIST(( INT4 *, tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1 nmhGetNextIndexFsMIOspfDistInOutRouteMapTable ARG_LIST((INT4, INT4 *, tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *, INT4, INT4 *));
extern INT1 nmhGetFsMIOspfDistInOutRouteMapValue ARG_LIST((INT4, tSNMP_OCTET_STRING_TYPE *, INT4, INT4 *));
extern INT1 nmhGetFsMIOspfDistInOutRouteMapRowStatus ARG_LIST((INT4, tSNMP_OCTET_STRING_TYPE *, INT4, INT4 *));
extern INT1 nmhSetFsMIOspfDistInOutRouteMapValue ARG_LIST((INT4, tSNMP_OCTET_STRING_TYPE *, INT4, INT4));
extern INT1 nmhSetFsMIOspfDistInOutRouteMapRowStatus ARG_LIST((INT4 , tSNMP_OCTET_STRING_TYPE *, INT4, INT4));
extern INT1 nmhTestv2FsMIOspfDistInOutRouteMapValue ARG_LIST((UINT4 *, INT4, tSNMP_OCTET_STRING_TYPE *, INT4, INT4));
extern INT1 nmhTestv2FsMIOspfDistInOutRouteMapRowStatus ARG_LIST((UINT4 *, INT4, tSNMP_OCTET_STRING_TYPE *, INT4, INT4));
extern INT1 nmhGetFsMIOspfPreferenceValue ARG_LIST((INT4, INT4 *));
extern INT1 nmhSetFsMIOspfPreferenceValue ARG_LIST((INT4, INT4));
extern INT1 nmhTestv2FsMIOspfPreferenceValue ARG_LIST((UINT4 *, INT4, INT4));
#endif /* OSPF_WANTED */

#ifdef RIP_WANTED

VOID IssProcessFilterRipConfPage (tHttp * pHttp);
VOID IssSetProcessFilterRipConfPage (tHttp * pHttp);
VOID IssGetProcessFilterRipConfPage (tHttp * pHttp);

extern INT1 nmhGetFirstIndexFsMIRipDistInOutRouteMapTable ARG_LIST((INT4 *, tSNMP_OCTET_STRING_TYPE *, INT4 *));
extern INT1 nmhGetNextIndexFsMIRipDistInOutRouteMapTable ARG_LIST((INT4, INT4 *,tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *, INT4, INT4 *));
extern INT1 nmhGetFsMIRipDistInOutRouteMapValue ARG_LIST((INT4, tSNMP_OCTET_STRING_TYPE *, INT4, INT4*));
extern INT1 nmhGetFsMIRipDistInOutRouteMapRowStatus ARG_LIST((INT4, tSNMP_OCTET_STRING_TYPE *, INT4, INT4 *));
extern INT1 nmhSetFsMIRipDistInOutRouteMapValue ARG_LIST((INT4, tSNMP_OCTET_STRING_TYPE *, INT4, INT4));
extern INT1 nmhSetFsMIRipDistInOutRouteMapRowStatus ARG_LIST((INT4, tSNMP_OCTET_STRING_TYPE *, INT4, INT4));
extern INT1 nmhTestv2FsMIRipDistInOutRouteMapValue ARG_LIST((UINT4 *, INT4, tSNMP_OCTET_STRING_TYPE *, INT4, INT4));
extern INT1 nmhTestv2FsMIRipDistInOutRouteMapRowStatus ARG_LIST((UINT4 *, INT4, tSNMP_OCTET_STRING_TYPE *, INT4, INT4));
extern INT1 nmhGetFsMIRipPreferenceValue ARG_LIST((INT4, INT4 *));
extern INT1 nmhSetFsMIRipPreferenceValue ARG_LIST((INT4, INT4));
extern INT1 nmhTestv2FsMIRipPreferenceValue ARG_LIST((UINT4 *, INT4, INT4));

#endif /* RIP_WANTED */


#ifdef ISIS_WANTED
#ifdef ROUTEMAP_WANTED

VOID IssProcessFilterIsisConfPage (tHttp * pHttp);
VOID IssSetProcessFilterIsisConfPage (tHttp * pHttp);
VOID IssGetProcessFilterIsisConfPage (tHttp * pHttp);

extern INT1 nmhGetFirstIndexFsIsisDistInOutRouteMapTable ARG_LIST((INT4 *, tSNMP_OCTET_STRING_TYPE *, INT4 *));
extern INT1 nmhGetNextIndexFsIsisDistInOutRouteMapTable ARG_LIST((INT4, INT4 *,tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *, INT4, INT4 *));
extern INT1 nmhGetFsIsisDistInOutRouteMapValue ARG_LIST((INT4, tSNMP_OCTET_STRING_TYPE *, INT4, INT4*));
extern INT1 nmhGetFsIsisDistInOutRouteMapRowStatus ARG_LIST((INT4, tSNMP_OCTET_STRING_TYPE *, INT4, INT4 *));
extern INT1 nmhSetFsIsisDistInOutRouteMapValue ARG_LIST((INT4, tSNMP_OCTET_STRING_TYPE *, INT4, INT4));
extern INT1 nmhSetFsIsisDistInOutRouteMapRowStatus ARG_LIST((INT4, tSNMP_OCTET_STRING_TYPE *, INT4, INT4));
extern INT1 nmhTestv2FsIsisDistInOutRouteMapValue ARG_LIST((UINT4 *, INT4, tSNMP_OCTET_STRING_TYPE *, INT4, INT4));
extern INT1 nmhTestv2FsIsisDistInOutRouteMapRowStatus ARG_LIST((UINT4 *, INT4, tSNMP_OCTET_STRING_TYPE *, INT4, INT4));
extern INT1 nmhGetFsIsisPreferenceValue ARG_LIST((INT4, INT4 *));
extern INT1 nmhSetFsIsisPreferenceValue ARG_LIST((INT4, INT4));
extern INT1 nmhTestv2FsIsisPreferenceValue ARG_LIST((UINT4 *, INT4, INT4));
#endif /* ROUTEMAP_WANTED*/
#endif /* ISIS_WANTED */


#ifdef OSPF3_WANTED

VOID IssProcessFilterOspfv3ConfPage (tHttp * pHttp);
VOID IssSetProcessFilterOspfv3ConfPage (tHttp * pHttp);
VOID IssGetProcessFilterOspfv3ConfPage (tHttp * pHttp);

extern INT1 nmhGetFirstIndexFsMIOspfv3DistInOutRouteMapTable ARG_LIST((INT4 *, tSNMP_OCTET_STRING_TYPE *, INT4 *));
extern INT1 nmhGetNextIndexFsMIOspfv3DistInOutRouteMapTable ARG_LIST((INT4, INT4 *, tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *, INT4, INT4 *));
extern INT1 nmhGetFsMIOspfv3DistInOutRouteMapValue ARG_LIST((INT4, tSNMP_OCTET_STRING_TYPE *, INT4, INT4 *));
extern INT1 nmhGetFsMIOspfv3DistInOutRouteMapRowStatus ARG_LIST((INT4, tSNMP_OCTET_STRING_TYPE *, INT4, INT4 *));
extern INT1 nmhSetFsMIOspfv3DistInOutRouteMapValue ARG_LIST((INT4, tSNMP_OCTET_STRING_TYPE *, INT4, INT4));
extern INT1 nmhSetFsMIOspfv3DistInOutRouteMapRowStatus ARG_LIST((INT4, tSNMP_OCTET_STRING_TYPE *, INT4, INT4));
extern INT1 nmhTestv2FsMIOspfv3DistInOutRouteMapValue ARG_LIST((UINT4 *, INT4, tSNMP_OCTET_STRING_TYPE *, INT4, INT4));
extern INT1 nmhTestv2FsMIOspfv3DistInOutRouteMapRowStatus ARG_LIST((UINT4 *, INT4, tSNMP_OCTET_STRING_TYPE *, INT4, INT4));
extern INT1 nmhGetFirstIndexFsMIOspfv3PreferenceTable ARG_LIST((INT4 *));
extern INT1 nmhGetNextIndexFsMIOspfv3PreferenceTable ARG_LIST((INT4 , INT4 *));
extern INT1 nmhGetFsMIOspfv3PreferenceValue ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhSetFsMIOspfv3PreferenceValue ARG_LIST((INT4  ,INT4));
extern INT1 nmhTestv2FsMIOspfv3PreferenceValue ARG_LIST((UINT4 *, INT4, INT4));


#endif /* OSPF3_WANTED */

#ifdef RIP6_WANTED

VOID IssProcessFilterRip6ConfPage (tHttp * pHttp);
VOID IssSetProcessFilterRip6ConfPage (tHttp * pHttp);
VOID IssGetProcessFilterRip6ConfPage (tHttp * pHttp);

/* Proto Validate Index Instance for FsRip6DistInOutRouteMapTable. */
extern INT1
nmhValidateIndexInstanceFsRip6DistInOutRouteMapTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ));
/* Proto Type for Low Level GET FIRST fn for FsRip6DistInOutRouteMapTable  */
extern INT1
nmhGetFirstIndexFsRip6DistInOutRouteMapTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 *));
/* Proto type for GET_NEXT Routine.  */
extern INT1
nmhGetNextIndexFsRip6DistInOutRouteMapTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));
/* Proto type for Low Level GET Routine All Objects.  */
extern INT1
nmhGetFsRip6DistInOutRouteMapValue ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));
extern INT1
nmhGetFsRip6DistInOutRouteMapRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));
/* Low Level SET Routine for All Objects.  */
extern INT1
nmhSetFsRip6DistInOutRouteMapValue ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));
extern INT1
nmhSetFsRip6DistInOutRouteMapRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));
/* Low Level TEST Routines for.  */
extern INT1
nmhTestv2FsRip6DistInOutRouteMapValue ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));
extern INT1
nmhTestv2FsRip6DistInOutRouteMapRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));
/* Low Level DEP Routines for.  */
extern INT1
nmhDepv2FsRip6DistInOutRouteMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
extern INT1
nmhGetFsRip6PreferenceValue ARG_LIST((INT4 *));
extern INT1
nmhSetFsRip6PreferenceValue ARG_LIST((INT4 ));
extern INT1
nmhTestv2FsRip6PreferenceValue ARG_LIST((UINT4 *  ,INT4 ));
extern INT1
nmhDepv2FsRip6PreferenceValue ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

#endif /* RIP6_WANTED */

#ifdef BGP_WANTED

/* Proto Validate Index Instance for FsBgp4DistInOutRouteMapTable. */
extern INT1
nmhValidateIndexInstanceFsBgp4DistInOutRouteMapTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ));
/* Proto Type for Low Level GET FIRST fn for FsBgp4DistInOutRouteMapTable  */
extern INT1
nmhGetFirstIndexFsBgp4DistInOutRouteMapTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 *));
/* Proto type for GET_NEXT Routine.  */
extern INT1
nmhGetNextIndexFsBgp4DistInOutRouteMapTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));
/* Proto type for Low Level GET Routine All Objects.  */
extern INT1
nmhGetFsBgp4DistInOutRouteMapValue ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));
extern INT1
nmhGetFsBgp4DistInOutRouteMapRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));
/* Low Level SET Routine for All Objects.  */
extern INT1
nmhSetFsBgp4DistInOutRouteMapValue ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));
extern INT1
nmhSetFsBgp4DistInOutRouteMapRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));
/* Low Level TEST Routines for.  */
extern INT1
nmhTestv2FsBgp4DistInOutRouteMapValue ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));
extern INT1
nmhTestv2FsBgp4DistInOutRouteMapRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));
/* Low Level DEP Routines for.  */
extern INT1
nmhDepv2FsBgp4DistInOutRouteMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
/* Proto type for Low Level GET Routine All Objects.  */
extern INT1
nmhGetFsBgp4PreferenceValue ARG_LIST((INT4 *));
/* Low Level SET Routine for All Objects.  */
extern INT1
nmhSetFsBgp4PreferenceValue ARG_LIST((INT4 ));
/* Low Level TEST Routines for.  */
extern INT1
nmhTestv2FsBgp4PreferenceValue ARG_LIST((UINT4 *  ,INT4 ));
/* Low Level DEP Routines for.  */
extern INT1
nmhDepv2FsBgp4PreferenceValue ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

VOID IssProcessFilterBgpConfPage (tHttp * pHttp);
VOID IssSetProcessFilterBgpConfPage (tHttp * pHttp);
VOID IssGetProcessFilterBgpConfPage (tHttp * pHttp);

#endif /* BGP_WANTED */

#endif /* _FLTRWEB_H */
