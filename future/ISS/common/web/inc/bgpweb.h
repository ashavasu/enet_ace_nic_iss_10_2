/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: bgpweb.h,v 1.12 2016/12/27 12:35:52 siva Exp $
*
* Description:  macros,typedefs and prototypes for ISS web Module
*******************************************************************/
#ifndef _BGPWEB_H
#define _BGPWEB_H

#ifdef BGP_WANTED

/* Options for Clear bgp*/
#define ISS_BGP_IPV4   1
#define ISS_BGP_IPV6   2
#define ISS_BGP_ALL   3
#define ISS_BGP_EXTERNAL   4
#define ISS_BGP_PEER_ADDR   5
#define ISS_BGP_PEER_GR   6
#define ISS_BGP_AS_NUM   7
#define ISS_BGP_FLAP_STAT  8
#define ISS_BGP_DAMP  9
#define ISS_BGP_HARD  1

#define ISS_BGP_SOFT_IN  2
#define ISS_BGP_SOFT_OUT  3
#define ISS_BGP_SOFT_BOTH  4
#define ISS_BGP_SOFT_IN_PREFIX_FILTER  5

#define ISS_BGP_OP_SOFT_IN 10
#define ISS_BGP_OP_SOFT_OUT 11
#define ISS_BGP_OP_SOFT_BOTH 12

#define ISS_BGP4_ECOMM_LEN           ECOMM_LEN
#define ISS_BGP4_ECOMM_EXT_LEN       ECOMM_EXT_LEN
#define ISS_BGP_PASSWORD_MAX_LENGTH 80
#define ISS_BGP4_VPN4_MAX_VRF_SIZE  16
#define ISS_BGP_COMM_FILTER_DELETE   2 
#define ISS_BGP_COMM_FILTER_ADD      1
#define ISS_BGP_ECOMM_FILTER_DELETE  2 
#define ISS_BGP_ECOMM_FILTER_ADD     1
#define ISS_BGP4_ADMIN_UP            1
#define ISS_BGP4_ADMIN_DOWN          2
#define ISS_BGP4_COMM_IN_FLT_TBL     1 
#define ISS_BGP4_COMM_OUT_FLT_TBL    2
#define ISS_BGP4_ECOMM_IN_FLT_TBL    1 
#define ISS_BGP4_ECOMM_OUT_FLT_TBL   2
#define ISS_BGP4_EXT_COMM_VALUE_LEN  16
#define ISS_BGP4_COMM_ROUTE_ADD      1
#define ISS_BGP4_COMM_ROUTE_DELETE   2
#define ISS_BGP4_APPLY           1
#define ISS_BGP4_DELETE           2
#define ISS_BGP4_IP_MAX_LENGTH       16
#define ISS_BGP4_ENABLE                 1
#define ISS_BGP4_DISABLE                2
#define ISS_BGP4_CONFED_PEER_AS_ENABLE  1 
#define ISS_BGP4_CONFED_PEER_AS_DISABLE 2
#define ISS_BGP4_RFL_CLIENT             2
#define ISS_BGP4_IP6_ADDR_STRING_LEN   200
#define ISS_BGP4_ROUTE_NAME_LEN   24
#define ISS_BGP4_INTER_AS_LENGTH   200
/* BGP4 RFD Default values */
#define ISS_RFD_DEF_DECAY_HALF_LIFE_TIME   900 
#define ISS_RFD_DEF_REUSE_THRESHOLD        500
#define ISS_RFD_DEF_CUTOFF_THRESHOLD       3500
#define ISS_RFD_DEF_MAX_HOLD_DOWN_TIME     3600
#define ISS_RFD_DEF_DECAY_TIMER_GRANULARITY 1
#define ISS_RFD_DEF_REUSE_TIMER_GRANULARITY 15
#define ISS_RFD_DEF_REUSE_INDEX_ARRAY_SIZE 1024

VOID IssProcessBgpglobalConfScalarsPage (tHttp * pHttp);
VOID IssProcessBgpglobalConfScalarsPageGet (tHttp * pHttp);
VOID IssProcessBgpglobalConfScalarsPageSet (tHttp * pHttp);
VOID IssProcessBgpFilterConfPage (tHttp *pHttp);
VOID IssProcessBgpFilterConfPageGet (tHttp *pHttp);
VOID IssProcessBgpFilterConfPageSet (tHttp *pHttp);
VOID IssProcessBgpMedConfPage (tHttp *pHttp);
VOID IssProcessBgpMedConfPageGet (tHttp *pHttp);
VOID IssProcessBgpMedConfPageSet (tHttp *pHttp);
VOID IssProcessBgpRouteAggrConfPage (tHttp *pHttp);
VOID IssProcessBgpRouteAggrConfPageGet (tHttp *pHttp);
VOID IssProcessBgpRouteAggrConfPageSet (tHttp *pHttp);
VOID IssProcessBgpLocalPrefConfPage (tHttp *pHttp);
VOID IssProcessBgpLocalPrefConfPageGet (tHttp *pHttp);
VOID IssProcessBgpLocalPrefConfPageSet (tHttp *pHttp);
VOID IssProcessBgpPeerStatsPage (tHttp *pHttp);
VOID IssProcessBgpConfedConfPage (tHttp * pHttp);
VOID IssProcessBgpConfedConfPageGet (tHttp * pHttp);
VOID IssProcessBgpConfedConfPageSet (tHttp * pHttp);
VOID IssProcessBgpImportConfPage (tHttp * pHttp);
VOID IssProcessBgpImportConfPageGet (tHttp * pHttp);
VOID IssProcessBgpImportConfPageSet (tHttp * pHttp);
VOID IssProcessBgpRfdConfPage (tHttp * pHttp);
VOID IssProcessBgpRfdConfPageGet (tHttp * pHttp);
VOID IssProcessBgpRfdConfPageSet (tHttp * pHttp);
VOID IssProcessBgpCommFilterConfPage (tHttp * pHttp);
VOID IssProcessBgpCommFilterConfPageGet (tHttp * pHttp);
VOID IssProcessBgpCommFilterConfPageSet (tHttp * pHttp);
VOID IssProcessBgpCommRouteConfPage (tHttp * pHttp);
VOID IssProcessBgpCommRouteConfPageGet (tHttp * pHttp);
VOID IssProcessBgpCommRouteConfPageSet (tHttp * pHttp);
VOID IssProcessCommBgpLocPolicyConfPage (tHttp *pHttp);
VOID IssProcessCommBgpLocPolicyConfPageGet (tHttp *pHttp);
VOID IssProcessCommBgpLocPolicyConfPageSet (tHttp *pHttp);
VOID IssProcessBgpECommFilterConfPage (tHttp *pHttp);
VOID IssProcessBgpECommFilterConfPageGet (tHttp *pHttp);
VOID IssProcessBgpECommFilterConfPageSet (tHttp *pHttp);
VOID IssProcessBgpECommLocPolicyConfPage (tHttp *pHttp);
VOID IssProcessBgpECommLocPolicyConfPageGet (tHttp *pHttp);
VOID IssProcessBgpECommLocPolicyConfPageSet (tHttp *pHttp);
VOID IssProcessBgpECommRouteConfPage (tHttp *pHttp);
VOID IssProcessBgpECommRouteConfPageGet (tHttp *pHttp);
VOID IssProcessBgpECommRouteConfPageSet (tHttp *pHttp);
VOID IssProcessBgpTimerconfPage (tHttp *pHttp);
VOID IssProcessBgpTimerconfPageGet (tHttp *pHttp);
VOID IssProcessBgpTimerconfPageSet (tHttp *pHttp);
VOID IssProcessBgpPeerConfPage (tHttp *pHttp);
VOID IssProcessBgpPeerConfPageGet (tHttp *pHttp);
VOID IssProcessBgpPeerConfPageSet (tHttp *pHttp);
VOID IssProcessBgpTcpAoConfPage (tHttp *pHttp);
VOID IssProcessBgpTcpAoConfPageGet(tHttp *pHttp);
VOID IssProcessBgpTcpAoConfPageSet(tHttp *pHttp);
VOID IssProcessBgpRouteMapPage (tHttp *pHttp);
VOID IssProcessBgpRouteMapPageGet (tHttp *pHttp);
VOID IssProcessBgpRouteMapPageSet (tHttp *pHttp);
VOID IssProcessBgpGrConfPage (tHttp *pHttp);
VOID IssProcessBgpPeerGroupConfPage(tHttp *pHttp);
VOID IssProcessBgpPeerGroupConfPageGet(tHttp *pHttp);
VOID IssProcessBgpPeerGroupConfPageSet(tHttp *pHttp);
VOID IssProcessBgpGroupConfContPage(tHttp *pHttp);
VOID IssProcessBgpPeerGroupConfPageContGet(tHttp *pHttp);
VOID IssProcessBgpPeerGroupConfContPageSet(tHttp *pHttp);
VOID IssProcessBgpPeerGroupAddPage(tHttp *pHttp);
VOID IssProcessBgpPeerGroupAddPageGet(tHttp *pHttp);
VOID IssProcessBgpPeerGroupAddPageSet(tHttp *pHttp);
VOID IssProcessBgpClearConfPage(tHttp *pHttp);
VOID IssProcessBgpClearConfPageSet(tHttp *pHttp);
VOID IssProcessBgpOrfConfPage(tHttp *pHttp);
VOID IssProcessBgpOrfConfPageSet(tHttp *pHttp);
VOID IssProcessBgpOrfConfPageGet(tHttp *pHttp);
VOID IssProcessBgpOrfFiltersPage(tHttp *pHttp);
VOID IssProcessBgpOrfFiltersPageGet(tHttp *pHttp);
VOID IssProcessBgpGrConfPageGet (tHttp *pHttp);
VOID IssProcessBgpGrConfPageSet (tHttp *pHttp);
VOID IssProcessRRDBgpConfPage (tHttp *pHttp);
VOID IssProcessRRDBgpConfPageGet (tHttp *pHttp);
VOID IssProcessRRDBgpConfPageSet (tHttp *pHttp);
VOID IssProcessRRDBgpGet(tHttp *pHttp, UINT4, INT4);
VOID IssProcessBgpContextCreationPage (tHttp *pHttp);
VOID IssProcessBgpContextCreationPageGet (tHttp *pHttp);
VOID IssProcessBgpContextCreationPageSet (tHttp *pHttp);
VOID IssPrintAvailableBgpContextId (tHttp * pHttp);

extern INT1 nmhSetFsbgp4GlobalAdminStatus(INT4);
extern INT1 nmhSetFsMIBgp4MaxPeerEntry ARG_LIST((INT4 ));
extern INT1 nmhSetFsMIBgp4MaxNoofRoutes ARG_LIST((INT4 ));
extern INT1 nmhTestv2FsMIBgp4MaxNoofRoutes ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2FsMIBgp4MaxPeerEntry ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhGetFirstIndexFsbgp4CommOutFilterTable(UINT4 *);
extern INT1 nmhGetFsbgp4CommOutgoingFilterStatus(UINT4, INT4 *);
extern INT1 nmhGetNextIndexFsbgp4CommOutFilterTable (UINT4, UINT4*);
extern INT1 nmhGetFirstIndexFsbgp4CommInFilterTable (UINT4 *);
extern INT1 nmhGetFsbgp4CommIncomingFilterStatus (UINT4, INT4*);
extern INT1 nmhGetNextIndexFsbgp4CommInFilterTable (UINT4, UINT4 *);
extern INT1 nmhValidateIndexInstanceFsbgp4CommInFilterTable (UINT4);
extern INT1 nmhGetFsbgp4InFilterRowStatus (UINT4, INT4 *);
extern INT1 nmhValidateIndexInstanceFsbgp4CommOutFilterTable (UINT4);
extern INT1 nmhGetFsbgp4OutFilterRowStatus (UINT4, INT4 *);
extern INT1 nmhTestv2Fsbgp4InFilterRowStatus (UINT4*, UINT4, INT4);
extern INT1 nmhSetFsbgp4InFilterRowStatus (UINT4, INT4);  
extern INT1 nmhTestv2Fsbgp4OutFilterRowStatus (UINT4*, UINT4, INT4);
extern INT1 nmhSetFsbgp4OutFilterRowStatus (UINT4, INT4); 
extern INT1 nmhTestv2Fsbgp4CommIncomingFilterStatus (UINT4*, UINT4, INT4); 
extern INT1 nmhSetFsbgp4CommIncomingFilterStatus (UINT4, INT4);
extern INT1 nmhTestv2Fsbgp4CommOutgoingFilterStatus (UINT4*, UINT4, INT4);
extern INT1 nmhSetFsbgp4CommOutgoingFilterStatus (UINT4, INT4);
extern INT1 nmhGetFirstIndexFsbgp4MpeCommRouteCommSetStatusTable (
            INT4 *,INT4 *,tSNMP_OCTET_STRING_TYPE *,INT4 *);
extern INT1 nmhGetFsbgp4mpeCommSetStatus (INT4, INT4, tSNMP_OCTET_STRING_TYPE *,
                                       INT4, INT4 *);
extern INT1 nmhGetNextIndexFsbgp4MpeCommRouteCommSetStatusTable (INT4, INT4 *,
            INT4, INT4*, tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *,
            INT4, INT4*);
extern INT1 nmhValidateIndexInstanceFsbgp4MpeCommRouteCommSetStatusTable (INT4, 
            INT4, tSNMP_OCTET_STRING_TYPE *,INT4); 
extern INT1 nmhTestv2Fsbgp4mpeCommSetStatus (UINT4 *, INT4, INT4, 
            tSNMP_OCTET_STRING_TYPE *, INT4, INT4);
extern INT1 nmhGetFsbgp4mpeCommSetStatusRowStatus (INT4, INT4, 
             tSNMP_OCTET_STRING_TYPE *,  INT4, INT4 *);
extern INT1 nmhTestv2Fsbgp4mpeCommSetStatusRowStatus (UINT4*, INT4, INT4,
             tSNMP_OCTET_STRING_TYPE *,  INT4, INT4);
extern INT1 nmhSetFsbgp4mpeCommSetStatusRowStatus (INT4, INT4, 
             tSNMP_OCTET_STRING_TYPE *,  INT4, INT4);
extern INT1 nmhSetFsbgp4mpeCommSetStatus (INT4, INT4, 
             tSNMP_OCTET_STRING_TYPE *,  INT4, INT4);
extern INT1 nmhGetFirstIndexFsbgp4MpeCommRouteAddCommTable (INT4*, INT4 *,
             tSNMP_OCTET_STRING_TYPE *, INT4 *, UINT4*);
extern INT1 nmhGetNextIndexFsbgp4MpeCommRouteAddCommTable (INT4, INT4*, 
            INT4, INT4*, tSNMP_OCTET_STRING_TYPE*, tSNMP_OCTET_STRING_TYPE*,
             INT4, INT4*, UINT4, UINT4 *);
extern INT1 nmhGetFirstIndexFsbgp4MpeCommRouteDeleteCommTable (INT4*, INT4 *,
             tSNMP_OCTET_STRING_TYPE *, INT4 *, UINT4*);
extern INT1 nmhGetNextIndexFsbgp4MpeCommRouteDeleteCommTable (INT4, INT4*, 
             INT4, INT4*, tSNMP_OCTET_STRING_TYPE*, tSNMP_OCTET_STRING_TYPE*,
             INT4, INT4*, UINT4, UINT4 *);
extern INT1 nmhValidateIndexInstanceFsbgp4MpeCommRouteAddCommTable (INT4, INT4,
             tSNMP_OCTET_STRING_TYPE*,INT4, UINT4);
extern INT1 nmhGetFsbgp4mpeAddCommRowStatus (INT4, INT4, tSNMP_OCTET_STRING_TYPE*,
             INT4, UINT4, INT4*);
extern INT1 nmhValidateIndexInstanceFsbgp4MpeCommRouteDeleteCommTable (INT4, INT4,
             tSNMP_OCTET_STRING_TYPE*,INT4, UINT4);
extern INT1 nmhGetFsbgp4mpeDeleteCommRowStatus (INT4, INT4, 
            tSNMP_OCTET_STRING_TYPE*,INT4, UINT4, INT4*);
extern INT1 nmhTestv2Fsbgp4mpeAddCommRowStatus (UINT4*, INT4, INT4, 
           tSNMP_OCTET_STRING_TYPE*,INT4, UINT4, INT4);
extern INT1 nmhTestv2Fsbgp4mpeDeleteCommRowStatus (UINT4*, INT4, INT4, 
           tSNMP_OCTET_STRING_TYPE*,INT4, UINT4, INT4);
extern INT1 nmhSetFsbgp4mpeAddCommRowStatus(INT4, INT4, 
           tSNMP_OCTET_STRING_TYPE*,INT4, UINT4, INT4);
extern INT1 nmhSetFsbgp4mpeDeleteCommRowStatus(INT4, INT4, 
           tSNMP_OCTET_STRING_TYPE*,INT4, UINT4, INT4);
extern INT1 nmhGetFirstIndexFsbgp4ExtCommOutFilterTable (
           tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsbgp4ExtCommOutgoingFilterStatus (
           tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetNextIndexFsbgp4ExtCommOutFilterTable (
           tSNMP_OCTET_STRING_TYPE*, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFirstIndexFsbgp4ExtCommInFilterTable (
           tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsbgp4ExtCommIncomingFilterStatus (
           tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetNextIndexFsbgp4ExtCommInFilterTable (
           tSNMP_OCTET_STRING_TYPE*, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhValidateIndexInstanceFsbgp4ExtCommInFilterTable (
              tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetFsbgp4ExtCommInFilterRowStatus (
              tSNMP_OCTET_STRING_TYPE*, INT4 *);
extern INT1 nmhValidateIndexInstanceFsbgp4ExtCommOutFilterTable (
              tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetFsbgp4ExtCommOutFilterRowStatus (
              tSNMP_OCTET_STRING_TYPE*, INT4 *);
extern INT1 nmhTestv2Fsbgp4ExtCommInFilterRowStatus (UINT4 *,
              tSNMP_OCTET_STRING_TYPE*, INT4);
extern INT1 nmhSetFsbgp4ExtCommInFilterRowStatus (tSNMP_OCTET_STRING_TYPE*, 
                                                  INT4);
extern INT1 nmhTestv2Fsbgp4ExtCommOutFilterRowStatus (UINT4 *,
              tSNMP_OCTET_STRING_TYPE*, INT4);
extern INT1 nmhSetFsbgp4ExtCommOutFilterRowStatus (tSNMP_OCTET_STRING_TYPE*, 
                                                  INT4);
extern INT1 nmhGetFirstIndexFsbgp4MpeExtCommRouteExtCommSetStatusTable (INT4*,
            INT4*, tSNMP_OCTET_STRING_TYPE*, INT4*);
extern INT1 nmhGetFsbgp4mpeExtCommSetStatus (INT4, INT4, tSNMP_OCTET_STRING_TYPE*,
                                          INT4, INT4*);
extern INT1 nmhGetNextIndexFsbgp4MpeExtCommRouteExtCommSetStatusTable(INT4, INT4*,
            INT4, INT4*, tSNMP_OCTET_STRING_TYPE*, tSNMP_OCTET_STRING_TYPE*, 
            INT4, INT4*);
extern INT1 nmhValidateIndexInstanceFsbgp4MpeExtCommRouteExtCommSetStatusTable(
            INT4, INT4, tSNMP_OCTET_STRING_TYPE*, INT4);
extern INT1 nmhGetFsbgp4mpeExtCommSetStatusRowStatus (INT4, INT4, 
            tSNMP_OCTET_STRING_TYPE*, INT4, INT4*);
extern INT1 nmhTestv2Fsbgp4mpeExtCommSetStatusRowStatus (UINT4*, INT4, 
            INT4, tSNMP_OCTET_STRING_TYPE*, INT4, INT4);
extern INT1 nmhSetFsbgp4mpeExtCommSetStatusRowStatus (INT4, INT4, 
            tSNMP_OCTET_STRING_TYPE *, INT4, INT4);
extern INT1 nmhSetFsbgp4mpeExtCommSetStatus (INT4,INT4,tSNMP_OCTET_STRING_TYPE *,
                                          INT4, INT4);
extern INT1 nmhTestv2Fsbgp4mpeExtCommSetStatus (UINT4*, INT4, INT4, 
            tSNMP_OCTET_STRING_TYPE*, INT4, INT4);
extern INT1 nmhGetFirstIndexFsbgp4MpeExtCommRouteAddExtCommTable (INT4*, INT4*, 
            tSNMP_OCTET_STRING_TYPE*, INT4*, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetNextIndexFsbgp4MpeExtCommRouteAddExtCommTable (INT4, INT4*,
            INT4, INT4*, tSNMP_OCTET_STRING_TYPE*, tSNMP_OCTET_STRING_TYPE*,
            INT4, INT4*, tSNMP_OCTET_STRING_TYPE*, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetFirstIndexFsbgp4MpeExtCommRouteDeleteExtCommTable (INT4*, INT4*, 
            tSNMP_OCTET_STRING_TYPE*, INT4*, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetNextIndexFsbgp4MpeExtCommRouteDeleteExtCommTable (INT4, INT4*,
            INT4, INT4*, tSNMP_OCTET_STRING_TYPE*, tSNMP_OCTET_STRING_TYPE*,
            INT4, INT4*, tSNMP_OCTET_STRING_TYPE*, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhValidateIndexInstanceFsbgp4MpeExtCommRouteAddExtCommTable(INT4, 
            INT4, tSNMP_OCTET_STRING_TYPE*, INT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetFsbgp4mpeAddExtCommRowStatus(INT4,INT4,tSNMP_OCTET_STRING_TYPE*,             INT4, tSNMP_OCTET_STRING_TYPE*, INT4*);
extern INT1 nmhValidateIndexInstanceFsbgp4MpeExtCommRouteDeleteExtCommTable(INT4, 
            INT4, tSNMP_OCTET_STRING_TYPE*, INT4, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetFsbgp4mpeDeleteExtCommRowStatus(INT4,INT4,tSNMP_OCTET_STRING_TYPE*,             INT4, tSNMP_OCTET_STRING_TYPE*, INT4*);
extern INT1 nmhTestv2Fsbgp4mpeDeleteExtCommRowStatus (UINT4*, INT4, INT4,
             tSNMP_OCTET_STRING_TYPE*, INT4, tSNMP_OCTET_STRING_TYPE*, INT4);
extern INT1 nmhTestv2Fsbgp4mpeAddExtCommRowStatus (UINT4*, INT4, INT4,
             tSNMP_OCTET_STRING_TYPE*, INT4, tSNMP_OCTET_STRING_TYPE*, INT4);
extern INT1 nmhSetFsbgp4mpeDeleteExtCommRowStatus (INT4, INT4, 
              tSNMP_OCTET_STRING_TYPE*, INT4, tSNMP_OCTET_STRING_TYPE*, INT4);
extern INT1 nmhSetFsbgp4mpeAddExtCommRowStatus (INT4, INT4, 
              tSNMP_OCTET_STRING_TYPE*, INT4, tSNMP_OCTET_STRING_TYPE*, INT4);
extern INT1 nmhGetFirstIndexFsbgpAscConfedPeerTable (UINT4*);
extern INT1 nmhGetFsbgpAscConfedPeerStatus (UINT4, INT4*);
extern INT1 nmhGetNextIndexFsbgpAscConfedPeerTable (UINT4, UINT4*);
extern INT1 nmhValidateIndexInstanceFsbgpAscConfedPeerTable (UINT4);
extern INT1 nmhSetFsbgpAscConfedPeerStatus (UINT4, INT4);
extern INT1 nmhTestv2FsbgpAscConfedPeerStatus (UINT4*, UINT4,INT4);
extern INT1 nmhGetFirstIndexFsbgp4MpeImportRouteTable (INT4*, INT4*, 
            tSNMP_OCTET_STRING_TYPE*, INT4*, INT4*, tSNMP_OCTET_STRING_TYPE*,
            INT4*, INT4*, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetNextIndexFsbgp4MpeImportRouteTable (INT4, INT4*, INT4, INT4*,
            tSNMP_OCTET_STRING_TYPE*, tSNMP_OCTET_STRING_TYPE*, INT4, INT4*,
            INT4, INT4*, tSNMP_OCTET_STRING_TYPE*, tSNMP_OCTET_STRING_TYPE*, 
            INT4, INT4*, 
            INT4, INT4*, tSNMP_OCTET_STRING_TYPE*, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhTestv2Fsbgp4mpeImportRouteAction (UINT4*, INT4, INT4, 
            tSNMP_OCTET_STRING_TYPE *, INT4, INT4, tSNMP_OCTET_STRING_TYPE*, 
            INT4, INT4, tSNMP_OCTET_STRING_TYPE*, INT4);
extern INT1 nmhSetFsbgp4mpeImportRouteAction (INT4, INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4, INT4,
            tSNMP_OCTET_STRING_TYPE *, INT4, INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4);
extern INT1 nmhGetFsbgp4RfdDecayHalfLifeTime (INT4*);
extern INT1 nmhGetFsbgp4RfdReuse (INT4*);
extern INT1 nmhGetFsbgp4RfdCutOff (INT4*);
extern INT1 nmhGetFsbgp4RfdMaxHoldDownTime (INT4*);
extern INT1 nmhGetFsbgp4RfdDecayTimerGranularity (INT4*);
extern INT1 nmhGetFsbgp4RfdReuseTimerGranularity (INT4*);
extern INT1 nmhGetFsbgp4RfdReuseIndxArraySize (INT4*);
extern INT1 nmhGetFsbgp4GlobalAdminStatus (INT4*);
extern INT1 nmhSetFsbgp4RfdDecayHalfLifeTime (INT4);
extern INT1 nmhSetFsbgp4RfdReuse (INT4);
extern INT1 nmhSetFsbgp4RfdCutOff (INT4);
extern INT1 nmhSetFsbgp4RfdMaxHoldDownTime (INT4);
extern INT1 nmhSetFsbgp4RfdDecayTimerGranularity (INT4);
extern INT1 nmhSetFsbgp4RfdReuseTimerGranularity (INT4);
extern INT1 nmhSetFsbgp4RfdReuseIndxArraySize (INT4);
extern INT1 nmhGetFirstIndexFsbgp4MpeBgpPeerTable (INT4*,
                                                   tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetFsbgp4mpebgpPeerKeepAliveConfigured (INT4, 
            tSNMP_OCTET_STRING_TYPE*, INT4*);                     
extern INT1 nmhGetFsbgp4mpebgpPeerHoldTimeConfigured (INT4,                                 tSNMP_OCTET_STRING_TYPE*, INT4*);
extern INT1 nmhGetFsbgp4mpebgpPeerMinRouteAdvertisementInterval (INT4, 
            tSNMP_OCTET_STRING_TYPE*, INT4*);
extern INT1 nmhGetFsbgp4mpebgpPeerMinASOriginationInterval (INT4, 
            tSNMP_OCTET_STRING_TYPE*, INT4*);
extern INT1 nmhGetFsbgp4mpebgpPeerConnectRetryInterval (INT4, 
            tSNMP_OCTET_STRING_TYPE*, INT4*);
extern INT1 nmhGetNextIndexFsbgp4MpeBgpPeerTable (INT4, INT4*, 
            tSNMP_OCTET_STRING_TYPE*,tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhValidateIndexInstanceFsbgp4MpeBgpPeerTable (INT4, 
            tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2Fsbgp4mpebgpPeerConnectRetryInterval (UINT4*, INT4, 
            tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhTestv2Fsbgp4mpebgpPeerHoldTimeConfigured (UINT4*, INT4,
            tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhTestv2Fsbgp4mpebgpPeerKeepAliveConfigured (UINT4*, INT4,
            tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhTestv2Fsbgp4mpebgpPeerMinASOriginationInterval (UINT4*, INT4,
            tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhTestv2Fsbgp4mpebgpPeerMinRouteAdvertisementInterval(UINT4*,INT4,
            tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhSetFsbgp4mpebgpPeerKeepAliveConfigured (INT4, 
            tSNMP_OCTET_STRING_TYPE*, INT4);                     
extern INT1 nmhSetFsbgp4mpebgpPeerHoldTimeConfigured (INT4,                                 tSNMP_OCTET_STRING_TYPE*, INT4);
extern INT1 nmhSetFsbgp4mpebgpPeerMinRouteAdvertisementInterval (INT4, 
            tSNMP_OCTET_STRING_TYPE*, INT4);
extern INT1 nmhSetFsbgp4mpebgpPeerMinASOriginationInterval (INT4, 
            tSNMP_OCTET_STRING_TYPE*, INT4);
extern INT1 nmhSetFsbgp4mpebgpPeerConnectRetryInterval (INT4, 
            tSNMP_OCTET_STRING_TYPE*, INT4);
extern INT1 nmhGetFirstIndexFsBgp4PeerGroupTable(tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetFirstIndexFsBgp4PeerGroupListTable (tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetFsBgp4PeerGroupRemoteAs (tSNMP_OCTET_STRING_TYPE *, UINT4 *);
extern INT1 nmhGetFsBgp4PeerGroupHoldTimeConfigured(tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupKeepAliveConfigured(tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupConnectRetryInterval(tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupMinASOriginInterval(tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupMinRouteAdvInterval(tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupAllowAutomaticStart(tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupAllowAutomaticStop(tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupIdleHoldTimeConfigured(tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupDampPeerOscillations(tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupDelayOpen(tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupDelayOpenTimeConfigured(tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupPrefixUpperLimit(tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupTcpConnectRetryCnt(tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetNextIndexFsBgp4PeerGroupTable(tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetNextIndexFsBgp4PeerGroupListTable (tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhGetFsBgp4PeerGroupEBGPMultiHop (tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupStatus (tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupEBGPHopLimit (tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupNextHopSelf (tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupRflClient (tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupTcpSendBufSize (tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupTcpRcvBufSize (tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupCommSendStatus(tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupECommSendStatus (tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupPassive (tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupDefaultOriginate (tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupActivateMPCapability (tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupDeactivateMPCapability (tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsBgp4PeerGroupRouteMapNameIn (tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsBgp4PeerGroupRouteMapNameOut (tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhValidateIndexInstanceFsBgp4PeerGroupTable(tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2FsBgp4PeerGroupRemoteAs(UINT4 * , tSNMP_OCTET_STRING_TYPE *,UINT4);
extern INT1 nmhTestv2FsBgp4PeerGroupEBGPMultiHop (UINT4 * , tSNMP_OCTET_STRING_TYPE *,INT4);
extern INT1 nmhTestv2FsBgp4PeerGroupEBGPHopLimit (UINT4 * , tSNMP_OCTET_STRING_TYPE *,INT4);
extern INT1 nmhTestv2FsBgp4PeerGroupNextHopSelf(UINT4 * , tSNMP_OCTET_STRING_TYPE *,INT4);
extern INT1 nmhTestv2FsBgp4PeerGroupRflClient (UINT4 * , tSNMP_OCTET_STRING_TYPE *,INT4);
extern INT1 nmhTestv2FsBgp4PeerGroupTcpSendBufSize (UINT4 * , tSNMP_OCTET_STRING_TYPE *,INT4);
extern INT1 nmhTestv2FsBgp4PeerGroupTcpRcvBufSize (UINT4 * , tSNMP_OCTET_STRING_TYPE *,INT4);
extern INT1 nmhTestv2FsBgp4PeerGroupCommSendStatus (UINT4 * , tSNMP_OCTET_STRING_TYPE *,INT4);
extern INT1 nmhTestv2FsBgp4PeerGroupECommSendStatus (UINT4 * , tSNMP_OCTET_STRING_TYPE *,INT4);
extern INT1 nmhTestv2FsBgp4PeerGroupPassive (UINT4 * , tSNMP_OCTET_STRING_TYPE *,INT4);
extern INT1 nmhTestv2FsBgp4PeerGroupDefaultOriginate (UINT4 * , tSNMP_OCTET_STRING_TYPE *,INT4);
extern INT1 nmhTestv2FsBgp4PeerGroupDeactivateMPCapability(UINT4 * , tSNMP_OCTET_STRING_TYPE *,INT4);
extern INT1 nmhTestv2FsBgp4PeerGroupRouteMapNameIn (UINT4 * , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2FsBgp4PeerGroupRouteMapNameOut(UINT4 * , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2FsBgp4PeerGroupActivateMPCapability (UINT4 * , tSNMP_OCTET_STRING_TYPE *,INT4);
extern INT1 nmhTestv2FsBgp4PeerGroupHoldTimeConfigured(UINT4 * , tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhTestv2FsBgp4PeerGroupKeepAliveConfigured(UINT4 * , tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhTestv2FsBgp4PeerGroupConnectRetryInterval(UINT4 * , tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhTestv2FsBgp4PeerGroupMinASOriginInterval(UINT4 * , tSNMP_OCTET_STRING_TYPE *,INT4);
extern INT1 nmhTestv2FsBgp4PeerGroupMinRouteAdvInterval(UINT4 * , tSNMP_OCTET_STRING_TYPE *,INT4);
extern INT1 nmhTestv2FsBgp4PeerGroupAllowAutomaticStart(UINT4 * , tSNMP_OCTET_STRING_TYPE *,INT4);
extern INT1 nmhTestv2FsBgp4PeerGroupAllowAutomaticStop(UINT4 * , tSNMP_OCTET_STRING_TYPE *,INT4);
extern INT1 nmhTestv2FsBgp4PeerGroupIdleHoldTimeConfigured(UINT4 * , tSNMP_OCTET_STRING_TYPE *,INT4);
extern INT1 nmhTestv2FsBgp4PeerGroupDampPeerOscillations(UINT4 * , tSNMP_OCTET_STRING_TYPE *,INT4);
extern INT1 nmhTestv2FsBgp4PeerGroupDelayOpen(UINT4 * , tSNMP_OCTET_STRING_TYPE *,INT4);
extern INT1 nmhTestv2FsBgp4PeerGroupDelayOpenTimeConfigured(UINT4 * , tSNMP_OCTET_STRING_TYPE *,INT4);
extern INT1 nmhTestv2FsBgp4PeerGroupPrefixUpperLimit(UINT4 * , tSNMP_OCTET_STRING_TYPE *,INT4);
extern INT1 nmhTestv2FsBgp4PeerGroupTcpConnectRetryCnt(UINT4 * , tSNMP_OCTET_STRING_TYPE *,INT4);
extern INT1 nmhTestv2FsBgp4PeerAddStatus (UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 );
extern INT1 nmhSetFsBgp4PeerAddStatus (tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 );
extern INT1 nmhGetFsMIBgp4ContextStatus ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhSetFsMIBgp4GlobalAdminStatus ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhGetFsMIBgp4GlobalAdminStatus ARG_LIST((INT4  ,INT4 *));
extern INT1 nmhSetFsMIBgp4ContextStatus ARG_LIST((INT4 ,INT4 ));
extern INT1 nmhTestv2FsMIBgp4ContextStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhGetFsMIBgp4LocalAs ARG_LIST ((UINT4 *));
extern INT1 nmhSetFsMIBgp4LocalAs ARG_LIST ((UINT4 ));
extern INT1 nmhTestv2FsMIBgp4LocalAs ARG_LIST ((UINT4 *, UINT4 ));
extern INT1 nmhGetFsMIBgp4LocalAsNo ARG_LIST((INT4 ,UINT4 *));
extern INT1 nmhSetFsMIBgp4LocalAsNo ARG_LIST((INT4  ,UINT4 ));
extern INT1 nmhTestv2FsMIBgp4LocalAsNo  ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1 nmhGetFirstIndexFsbgp4MpePeerExtTable (INT4*,
            tSNMP_OCTET_STRING_TYPE*); 
extern INT1 nmhGetFsbgp4mpePeerExtPeerRemoteAs (INT4, tSNMP_OCTET_STRING_TYPE*, 
            UINT4*);
extern INT1 nmhSetFsbgp4mpePeerAllowAutomaticStart (INT4,
            tSNMP_OCTET_STRING_TYPE*, INT4);
extern INT1 nmhSetFsbgp4mpePeerAllowAutomaticStop (INT4,
            tSNMP_OCTET_STRING_TYPE*, INT4);
extern INT1 nmhSetFsbgp4mpeDampPeerOscillations (INT4,
            tSNMP_OCTET_STRING_TYPE*, INT4);
extern INT1 nmhSetFsbgp4mpePeerDelayOpen (INT4,
            tSNMP_OCTET_STRING_TYPE*, INT4);
extern INT1 nmhTestv2Fsbgp4mpePeerTcpConnectRetryCnt (UINT4*, INT4,
            tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhTestv2Fsbgp4mpePeerPrefixUpperLimit (UINT4*, INT4,
            tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhTestv2Fsbgp4mpePeerAllowAutomaticStart (UINT4*, INT4,
            tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhTestv2Fsbgp4mpePeerAllowAutomaticStop(UINT4*, INT4,
            tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhTestv2Fsbgp4mpeDampPeerOscillations(UINT4*, INT4,
            tSNMP_OCTET_STRING_TYPE*, INT4);
extern INT1 nmhTestv2Fsbgp4mpePeerDelayOpen(UINT4*, INT4,
            tSNMP_OCTET_STRING_TYPE*, INT4);
extern INT1 nmhSetFsbgp4mpePeerPrefixUpperLimit (INT4,
            tSNMP_OCTET_STRING_TYPE*, INT4);
extern INT1 nmhSetFsbgp4mpePeerTcpConnectRetryCnt (INT4,
            tSNMP_OCTET_STRING_TYPE*, INT4);
extern INT1 nmhSetFsbgp4mpebgpPeerDelayOpenTimeConfigured (INT4,
            tSNMP_OCTET_STRING_TYPE*, INT4);
extern INT1 nmhSetFsbgp4mpebgpPeerIdleHoldTimeConfigured (INT4,
            tSNMP_OCTET_STRING_TYPE*, INT4);
extern INT1 nmhTestv2Fsbgp4mpebgpPeerIdleHoldTimeConfigured (UINT4*, INT4,
            tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhTestv2Fsbgp4mpebgpPeerDelayOpenTimeConfigured (UINT4*, INT4,
           tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhGetFsbgp4TCPMD5AuthPassword(INT4, tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *); 
extern INT1
nmhGetFirstIndexFsBgp4NeighborRouteMapTable(INT4 *pi4FsBgp4NeighborRouteMapPeerAddrType ,
       tSNMP_OCTET_STRING_TYPE * pFsBgp4NeighborRouteMapPeer ,INT4 *pi4FsBgp4NeighborRouteMapDirection);
extern INT1 nmhGetNextIndexFsBgp4NeighborRouteMapTable(INT4 i4FsBgp4NeighborRouteMapPeerAddrType ,INT4 *pi4NextFsBgp4NeighborRouteMapPeerAddrType ,tSNMP_OCTET_STRING_TYPE *pFsBgp4NeighborRouteMapPeer ,tSNMP_OCTET_STRING_TYPE * pNextFsBgp4NeighborRouteMapPeer,INT4 i4FsBgp4NeighborRouteMapDirection ,INT4 *pi4NextFsBgp4NeighborRouteMapDirection );
extern INT1 nmhGetFsBgp4NeighborRouteMapName(INT4 i4FsBgp4NeighborRouteMapPeerAddrType ,tSNMP_OCTET_STRING_TYPE *pFsBgp4NeighborRouteMapPeer ,INT4 i4FsBgp4NeighborRouteMapDirection ,tSNMP_OCTET_STRING_TYPE * pRetValFsBgp4NeighborRouteMapName);
extern INT1
nmhValidateIndexInstanceFsBgp4NeighborRouteMapTable(INT4 i4FsBgp4NeighborRouteMapPeerAddrType ,
                                                    tSNMP_OCTET_STRING_TYPE *pFsBgp4NeighborRouteMapPeer ,
                                                    INT4 i4FsBgp4NeighborRouteMapDirection);
extern 
INT1 nmhTestv2FsBgp4NeighborRouteMapRowStatus(UINT4 *pu4ErrorCode ,
                   INT4 i4FsBgp4NeighborRouteMapPeerAddrType ,
                   tSNMP_OCTET_STRING_TYPE *pFsBgp4NeighborRouteMapPeer ,
                   INT4 i4FsBgp4NeighborRouteMapDirection ,
                   INT4 i4TestValFsBgp4NeighborRouteMapRowStatus);
extern 
INT1
nmhSetFsBgp4NeighborRouteMapRowStatus(INT4 i4FsBgp4NeighborRouteMapPeerAddrType ,
                  tSNMP_OCTET_STRING_TYPE *pFsBgp4NeighborRouteMapPeer ,
                  INT4 i4FsBgp4NeighborRouteMapDirection ,
                  INT4 i4SetValFsBgp4NeighborRouteMapRowStatus);
extern 
INT1
nmhTestv2FsBgp4NeighborRouteMapName(UINT4 *pu4ErrorCode ,
                  INT4 i4FsBgp4NeighborRouteMapPeerAddrType ,
                  tSNMP_OCTET_STRING_TYPE *pFsBgp4NeighborRouteMapPeer ,
                  INT4 i4FsBgp4NeighborRouteMapDirection ,
                  tSNMP_OCTET_STRING_TYPE *pTestValFsBgp4NeighborRouteMapName);
extern
INT1 nmhSetFsBgp4NeighborRouteMapName(INT4 i4FsBgp4NeighborRouteMapPeerAddrType ,
                      tSNMP_OCTET_STRING_TYPE *pFsBgp4NeighborRouteMapPeer ,
                      INT4 i4FsBgp4NeighborRouteMapDirection ,
                      tSNMP_OCTET_STRING_TYPE *pSetValFsBgp4NeighborRouteMapName);
extern INT4 FsBgp4NeighborRouteMapRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4NeighborRouteMapNameSet(tSnmpIndex *, tRetVal *);
extern INT1 nmhGetFsbgp4TCPMD5AuthPwdSet (INT4, tSNMP_OCTET_STRING_TYPE * ,INT4 *);
extern INT1 nmhSetFsbgp4TCPMD5AuthPwdSet (INT4, tSNMP_OCTET_STRING_TYPE * ,INT4 );
extern INT1 nmhSetFsbgp4TCPMD5AuthPassword (INT4, tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE * );
extern INT1 nmhTestv2Fsbgp4TCPMD5AuthPassword (UINT4*, INT4  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2Fsbgp4TCPMD5AuthPwdSet (UINT4*, INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 );
extern INT1 nmhGetFirstIndexFsbgp4TCPMD5AuthTable (INT4 *, tSNMP_OCTET_STRING_TYPE * ); 
extern INT1 nmhSetFsbgp4RRDAdminStatus (INT4);
extern INT1 nmhSetFsbgp4RRDDefaultMetric (UINT4);
extern INT1 nmhSetFsBgp4RRDMetricValue(INT4 i4FsBgp4RRDMetricProtocolId , INT4 i4SetValFsBgp4RRDMetricValue);
extern INT1 nmhSetFsbgp4RRDMatchTypeEnable(INT4 i4SetValFsbgp4RRDMatchTypeEnable);
extern INT1 nmhSetFsbgp4RRDProtoMaskForEnable (INT4);
extern INT1 nmhSetFsbgp4RRDRouteMapName (tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetFsbgp4RRDSrcProtoMaskForDisable (INT4);

extern INT1 nmhGetFsbgp4RRDAdminStatus(INT4*);
extern INT1 nmhGetFsbgp4RRDDefaultMetric(UINT4*);
extern INT1 nmhGetFsbgp4RfdAdminStatus(INT4 *pi4RetValFsbgp4RfdAdminStatus);
extern INT1 nmhGetFsBgp4RRDMetricValue(INT4 i4FsBgp4RRDMetricProtocolId , INT4 *pi4RetValFsBgp4RRDMetricValue);
extern INT1 nmhGetFsbgp4RRDMatchTypeEnable(INT4 *pi4RetValFsbgp4RRDMatchTypeEnable);
extern INT1 nmhGetFsbgp4RRDProtoMaskForEnable(INT4*);
extern INT1 nmhGetFsbgp4RRDRouteMapName(tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhTestv2Fsbgp4RRDAdminStatus(UINT4*, INT4);
extern INT1 nmhTestv2Fsbgp4RRDDefaultMetric(UINT4*, UINT4);
extern INT1 nmhTestv2Fsbgp4RfdAdminStatus(UINT4 *pu4ErrorCode , INT4 i4TestValFsbgp4RfdAdminStatus);
extern INT1 nmhTestv2Fsbgp4RRDMatchTypeEnable(UINT4 *pu4ErrorCode, INT4 i4TestValFsbgp4RRDMatchTypeEnable);
extern INT1 nmhTestv2FsBgp4RRDMetricValue(UINT4 *pu4ErrorCode , INT4 i4FsBgp4RRDMetricProtocolId , INT4 i4TestValFsBgp4RRDMetricValue);
extern INT1 nmhTestv2Fsbgp4RRDProtoMaskForEnable(UINT4*, INT4);
extern INT1 nmhTestv2Fsbgp4RRDRouteMapName(UINT4*, tSNMP_OCTET_STRING_TYPE *);


/* RFC 4271 Upgrade */

extern INT1 nmhGetFsbgp4mpePeerPrefixUpperLimit (INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4*);
extern INT1 nmhGetFsbgp4mpePeerTcpConnectRetryCnt (INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4*);
extern INT1 nmhGetFsbgp4mpePeerAllowAutomaticStart (INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4*);
extern INT1 nmhGetFsbgp4mpePeerAllowAutomaticStop (INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4*);
extern INT1 nmhGetFsbgp4mpeDampPeerOscillations (INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4*);
extern INT1 nmhGetFsbgp4mpePeerDelayOpen (INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4*);
extern INT1 nmhGetFsbgp4mpebgpPeerIdleHoldTimeConfigured (INT4,
            tSNMP_OCTET_STRING_TYPE*, INT4*);
extern INT1 nmhGetFsbgp4mpebgpPeerDelayOpenTimeConfigured (INT4, 
            tSNMP_OCTET_STRING_TYPE*, INT4*);

extern INT1 nmhGetFsbgp4mpePeerExtEBGPMultiHop (INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4*);
extern INT1 nmhGetFsbgp4mpePeerExtNextHopSelf (INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4*);
extern INT1 nmhGetFsbgp4mpePeerExtLclAddress (INT4, tSNMP_OCTET_STRING_TYPE*,
            tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhGetFsbgp4mpePeerExtGateway (INT4, tSNMP_OCTET_STRING_TYPE*,
            tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhGetFsbgp4mpePeerExtDefaultOriginate (INT4, tSNMP_OCTET_STRING_TYPE*,            INT4*); 
extern INT1 nmhGetFsbgp4mpePeerExtCommSendStatus (INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4*);
extern INT1 nmhGetFsbgp4mpePeerExtECommSendStatus (INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4*);
extern INT1 nmhGetFsbgp4mpePeerExtRflClient (INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4*);
extern INT1 nmhGetFsbgp4mpebgpPeerAdminStatus (INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4*);
extern INT1 nmhGetFsbgp4mpePeerBfdStatus (INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4*);
extern INT1 nmhGetNextIndexFsbgp4MpePeerExtTable (INT4, INT4*, 
            tSNMP_OCTET_STRING_TYPE*, tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhValidateIndexInstanceFsbgp4MpePeerExtTable(INT4,
            tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhSetFsbgp4mpePeerExtPeerRemoteAs (INT4, tSNMP_OCTET_STRING_TYPE*, 
            UINT4);
extern INT1 nmhSetFsbgp4mpePeerExtEBGPMultiHop (INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4);
extern INT1 nmhSetFsbgp4mpePeerExtNextHopSelf (INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4);
extern INT1 nmhSetFsbgp4mpePeerExtLclAddress (INT4, tSNMP_OCTET_STRING_TYPE*,
            tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetFsbgp4mpePeerExtGateway (INT4, tSNMP_OCTET_STRING_TYPE*,
            tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhSetFsbgp4mpePeerExtDefaultOriginate (INT4, tSNMP_OCTET_STRING_TYPE*,            INT4); 
extern INT1 nmhSetFsbgp4mpePeerExtCommSendStatus (INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4);
extern INT1 nmhSetFsbgp4mpePeerExtECommSendStatus (INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4);
extern INT1 nmhSetFsbgp4mpePeerExtRflClient (INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4);
extern INT1 nmhSetFsbgp4mpebgpPeerAdminStatus (INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4);
extern INT1 nmhSetFsbgp4mpePeerBfdStatus (INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4);
extern INT1 nmhSetFsbgp4mpePeerExtConfigurePeer (INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4);
extern INT1 nmhTestv2Fsbgp4mpePeerExtPeerRemoteAs (UINT4 *,
            INT4, tSNMP_OCTET_STRING_TYPE*, 
            UINT4);
extern INT1 nmhTestv2Fsbgp4mpePeerExtEBGPMultiHop (UINT4*,
            INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4);
extern INT1 nmhTestv2Fsbgp4mpePeerExtNextHopSelf (UINT4*,
            INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4);
extern INT1 nmhTestv2Fsbgp4mpePeerExtLclAddress (UINT4*,
            INT4, tSNMP_OCTET_STRING_TYPE*,
            tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2Fsbgp4mpePeerExtGateway (UINT4*,
            INT4, tSNMP_OCTET_STRING_TYPE*,
            tSNMP_OCTET_STRING_TYPE*);
extern INT1 nmhTestv2Fsbgp4mpePeerExtDefaultOriginate (UINT4*,
            INT4, tSNMP_OCTET_STRING_TYPE*,INT4); 
extern INT1 nmhTestv2Fsbgp4mpePeerExtCommSendStatus (UINT4*,
            INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4);
extern INT1 nmhTestv2Fsbgp4mpePeerExtECommSendStatus (UINT4*,
            INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4);
extern INT1 nmhTestv2Fsbgp4mpePeerExtRflClient (UINT4*,
            INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4);
extern INT1 nmhTestv2Fsbgp4mpebgpPeerAdminStatus (UINT4*,
            INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4);
extern INT1 nmhTestv2Fsbgp4mpePeerBfdStatus (UINT4*,
            INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4);
extern INT1 nmhTestv2Fsbgp4mpePeerExtConfigurePeer (UINT4*,
            INT4, tSNMP_OCTET_STRING_TYPE*,
            INT4);
extern INT1 nmhTestv2Fsbgp4ExtCommIncomingFilterStatus (UINT4*, 
            tSNMP_OCTET_STRING_TYPE*,INT4);
extern INT1 nmhSetFsbgp4ExtCommIncomingFilterStatus (tSNMP_OCTET_STRING_TYPE*,
                                                     INT4);
extern INT1 nmhTestv2Fsbgp4ExtCommOutgoingFilterStatus (UINT4*, 
            tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhSetFsbgp4ExtCommOutgoingFilterStatus (tSNMP_OCTET_STRING_TYPE *,
                                                     INT4);
extern INT1 nmhGetFsbgp4mpePeerExtActivateMPCapability (INT4, 
            tSNMP_OCTET_STRING_TYPE *, INT4*);
extern INT1 nmhGetFsbgp4mpebgpPeerNegotiatedVersion (INT4, 
            tSNMP_OCTET_STRING_TYPE *, INT4*);
extern UINT1* Bgp4MemAllocateMsg (UINT4 );
extern INT4 Bgp4MemReleaseMsg (UINT1 *);
#ifdef BGP4_IPV6_WANTED
extern INT4 Bgp4PrintIp6Addr (UINT1 *, UINT1*);
#endif
extern UINT4 Bgp4GetSubnetmask (UINT1);
extern UINT4 BgpGetTableVersion (UINT4);
extern INT1 nmhGetFsbgp4mpebgpPeerRemoteAs ( INT4 , tSNMP_OCTET_STRING_TYPE *,
                                             UINT4 *);
extern INT1 nmhGetFsbgp4mpebgpPeerFsmEstablishedTime (INT4 ,
            tSNMP_OCTET_STRING_TYPE *, UINT4*);
extern INT1 nmhGetFsbgp4mpebgpPeerOutTotalMessages (INT4 ,
            tSNMP_OCTET_STRING_TYPE *, UINT4*);
extern INT1 nmhGetFsbgp4mpebgpPeerInTotalMessages (INT4,  
            tSNMP_OCTET_STRING_TYPE *, UINT4*);
extern INT1 nmhGetFsbgp4mpebgpPeerInUpdates (INT4, 
            tSNMP_OCTET_STRING_TYPE *, UINT4*);
extern INT1 nmhGetFsbgp4mpebgpPeerOutUpdates (INT4,  
            tSNMP_OCTET_STRING_TYPE *, UINT4*);
extern INT1  nmhGetFsbgp4mpebgpPeerState(INT4,  
            tSNMP_OCTET_STRING_TYPE *, INT4*);
extern INT1 nmhGetFsbgp4mpePeerExtEBGPHopLimit (INT4,
            tSNMP_OCTET_STRING_TYPE *, INT4*);                                  
extern INT1 nmhSetFsbgp4mpePeerExtPassive (INT4,
            tSNMP_OCTET_STRING_TYPE *, INT4);                                  
extern INT1 nmhSetFsbgp4mpePeerExtEBGPHopLimit (INT4,
            tSNMP_OCTET_STRING_TYPE *, INT4);                                  
extern INT1 nmhTestv2Fsbgp4mpePeerExtEBGPHopLimit (UINT4 *,
            INT4,
            tSNMP_OCTET_STRING_TYPE *, INT4);                                  
extern INT1 nmhTestv2Fsbgp4mpePeerExtTcpSendBufSize (UINT4 *,
            INT4,
            tSNMP_OCTET_STRING_TYPE *, INT4);                                  
extern INT1 nmhTestv2Fsbgp4mpePeerExtTcpRcvBufSize(UINT4 *,
            INT4,
            tSNMP_OCTET_STRING_TYPE *, INT4);                                  
extern INT1 nmhTestv2Fsbgp4mpePeerExtActivateMPCapability (UINT4 *,
            INT4,
            tSNMP_OCTET_STRING_TYPE *, INT4);                                  
extern INT1 nmhTestv2Fsbgp4mpePeerExtDeactivateMPCapability (UINT4 *,
            INT4,
            tSNMP_OCTET_STRING_TYPE *, INT4);                                  
extern INT1 nmhSetFsbgp4mpePeerExtDeactivateMPCapability (INT4,
            tSNMP_OCTET_STRING_TYPE *, INT4);                                  
extern INT1 nmhSetFsbgp4mpePeerExtActivateMPCapability (INT4,
            tSNMP_OCTET_STRING_TYPE *, INT4);                                 
extern INT1 nmhGetFsbgp4mpePeerExtPassive (INT4, tSNMP_OCTET_STRING_TYPE *, INT4*);
extern INT1 nmhSetFsbgp4mpePeerExtTcpSendBufSize (INT4, 
            tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhSetFsbgp4mpePeerExtTcpRcvBufSize (INT4, 
            tSNMP_OCTET_STRING_TYPE *, INT4);
extern INT1 nmhGetFsbgp4mpePeerExtTcpSendBufSize (INT4, 
            tSNMP_OCTET_STRING_TYPE *, INT4*);
extern INT1 nmhGetFsbgp4mpePeerExtTcpRcvBufSize (INT4, 
            tSNMP_OCTET_STRING_TYPE *, INT4 *);
extern INT1 nmhGetFsbgp4mpePeerExtNetworkAddress (INT4, 
            tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhSetFsbgp4mpePeerExtNetworkAddress (INT4, 
            tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *);
extern INT1 nmhTestv2Fsbgp4mpePeerExtNetworkAddress (UINT4*, INT4, 
            tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *);

extern INT1
nmhValidateIndexInstanceFsbgp4MpeMEDTable ARG_LIST((INT4 ));


extern INT1
nmhGetFirstIndexFsbgp4MpeMEDTable ARG_LIST((INT4 *));

extern INT1
nmhGetNextIndexFsbgp4MpeMEDTable ARG_LIST((INT4 , INT4 *));


extern INT1
nmhGetFsbgp4mpeMEDAdminStatus ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsbgp4mpeMEDRemoteAS ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsbgp4mpeMEDIPAddrAfi ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsbgp4mpeMEDIPAddrSafi ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsbgp4mpeMEDIPAddrPrefix ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsbgp4mpeMEDIPAddrPrefixLen ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsbgp4mpeMEDIntermediateAS ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsbgp4mpeMEDDirection ARG_LIST((INT4 ,INT4 *));
extern INT1
nmhGetFsMIBgp4mpeMEDVrfName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1
nmhGetFsbgp4mpeMEDValue ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsbgp4mpeMEDPreference ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhValidateIndexInstanceFsbgp4MpeLocalPrefTable ARG_LIST((INT4 ));


extern INT1
nmhGetFirstIndexFsbgp4MpeLocalPrefTable ARG_LIST((INT4 *));


extern INT1
nmhGetNextIndexFsbgp4MpeLocalPrefTable ARG_LIST((INT4 , INT4 *));


extern INT1
nmhGetFsbgp4mpeLocalPrefAdminStatus ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsbgp4mpeLocalPrefRemoteAS ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsbgp4mpeLocalPrefIPAddrAfi ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsbgp4mpeLocalPrefIPAddrSafi ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsbgp4mpeLocalPrefIPAddrPrefix ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsbgp4mpeLocalPrefIPAddrPrefixLen ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsbgp4mpeLocalPrefIntermediateAS ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsbgp4mpeLocalPrefDirection ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsbgp4mpeLocalPrefValue ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsbgp4mpeLocalPrefPreference ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsMIBgp4mpeLocalPrefVrfName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhValidateIndexInstanceFsbgp4MpeUpdateFilterTable ARG_LIST((INT4 ));


extern INT1
nmhGetFirstIndexFsbgp4MpeUpdateFilterTable ARG_LIST((INT4 *));

extern INT1
nmhGetNextIndexFsbgp4MpeUpdateFilterTable ARG_LIST((INT4 , INT4 *));

extern INT1
nmhGetFsbgp4mpeUpdateFilterAdminStatus ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsbgp4mpeUpdateFilterRemoteAS ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetFsbgp4mpeUpdateFilterIPAddrAfi ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsbgp4mpeUpdateFilterIPAddrSafi ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsbgp4mpeUpdateFilterIPAddrPrefix ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsbgp4mpeUpdateFilterIPAddrPrefixLen ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsbgp4mpeUpdateFilterIntermediateAS ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsbgp4mpeUpdateFilterDirection ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsbgp4mpeUpdateFilterAction ARG_LIST((INT4 ,INT4 *));

extern INT1 
nmhGetFsMIBgp4mpeUpdateFilterVrfName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhValidateIndexInstanceFsbgp4MpeAggregateTable ARG_LIST((INT4 ));


extern INT1
nmhGetFirstIndexFsbgp4MpeAggregateTable ARG_LIST((INT4 *));


extern INT1
nmhGetNextIndexFsbgp4MpeAggregateTable ARG_LIST((INT4 , INT4 *));


extern INT1
nmhGetFsbgp4mpeAggregateAdminStatus ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsbgp4mpeAggregateIPAddrAfi ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsbgp4mpeAggregateIPAddrSafi ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsbgp4mpeAggregateIPAddrPrefix ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetFsbgp4mpeAggregateIPAddrPrefixLen ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetFsbgp4mpeAggregateAdvertise ARG_LIST((INT4 ,INT4 *));

extern INT1 nmhGetFsbgp4mpeAggregateAsSet(INT4 i4Fsbgp4mpeAggregateIndex , INT4 *pi4RetValFsbgp4mpeAggregateAsSet);

extern INT1 nmhGetFsbgp4mpeAggregateSuppressRouteMapName(INT4 i4Fsbgp4mpeAggregateIndex , tSNMP_OCTET_STRING_TYPE * pRetValFsbgp4mpeAggregateSuppressRouteMapName);

extern INT1 nmhGetFsbgp4mpeAggregateAdvertiseRouteMapName (INT4 i4Fsbgp4mpeAggregateIndex , tSNMP_OCTET_STRING_TYPE * pRetValFsbgp4mpeAggregateAdvertiseRouteMapName);

extern INT1 nmhGetFsbgp4mpeAggregateAttributeRouteMapName(INT4 i4Fsbgp4mpeAggregateIndex , tSNMP_OCTET_STRING_TYPE * pRetValFsbgp4mpeAggregateAttributeRouteMapName);

extern INT1 
nmhGetFsMIBgp4mpeAggregateVrfName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));


extern INT1
nmhSetFsbgp4mpeAggregateAdminStatus ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsbgp4mpeAggregateIPAddrAfi ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsbgp4mpeAggregateIPAddrSafi ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsbgp4mpeAggregateIPAddrPrefix ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsbgp4mpeAggregateIPAddrPrefixLen ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsbgp4mpeAggregateAdvertise ARG_LIST((INT4  ,INT4 ));

extern INT1 nmhSetFsbgp4mpeAggregateAsSet(INT4 i4Fsbgp4mpeAggregateIndex , INT4 i4SetValFsbgp4mpeAggregateAsSet);

extern INT1
nmhSetFsbgp4mpeAggregateSuppressRouteMapName (INT4 i4Fsbgp4mpeAggregateIndex,tSNMP_OCTET_STRING_TYPE *pSetValFsbgp4mpeAggregateSuppressRouteMapName);

extern INT1 nmhSetFsbgp4mpeAggregateAdvertiseRouteMapName (INT4 i4Fsbgp4mpeAggregateIndex,
tSNMP_OCTET_STRING_TYPE *pSetValFsbgp4mpeAggregateAdvertiseRouteMapName);

extern INT1 nmhSetFsbgp4mpeAggregateAttributeRouteMapName (INT4 i4Fsbgp4mpeAggregateIndex,
tSNMP_OCTET_STRING_TYPE *pSetValFsbgp4mpeAggregateAttributeRouteMapName);

extern INT1 
nmhSetFsMIBgp4mpeAggregateVrfName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2Fsbgp4mpeLocalPrefAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2Fsbgp4mpeLocalPrefRemoteAS ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhTestv2Fsbgp4mpeLocalPrefIPAddrAfi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2Fsbgp4mpeLocalPrefIPAddrSafi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2Fsbgp4mpeLocalPrefIPAddrPrefix ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2Fsbgp4mpeLocalPrefIPAddrPrefixLen ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2Fsbgp4mpeLocalPrefIntermediateAS ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2Fsbgp4mpeLocalPrefDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2Fsbgp4mpeLocalPrefValue ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhTestv2Fsbgp4mpeLocalPrefPreference ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 
nmhTestv2FsMIBgp4mpeLocalPrefVrfName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));


extern INT1
nmhSetFsbgp4mpeUpdateFilterAdminStatus ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsbgp4mpeUpdateFilterRemoteAS ARG_LIST((INT4  ,UINT4 ));

extern INT1
nmhSetFsbgp4mpeUpdateFilterIPAddrAfi ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsbgp4mpeUpdateFilterIPAddrSafi ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsbgp4mpeUpdateFilterIPAddrPrefix ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsbgp4mpeUpdateFilterIPAddrPrefixLen ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsbgp4mpeUpdateFilterIntermediateAS ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsbgp4mpeUpdateFilterDirection ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsbgp4mpeUpdateFilterAction ARG_LIST((INT4  ,INT4 ));

extern INT1 
nmhSetFsMIBgp4mpeUpdateFilterVrfName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsbgp4mpeLocalPrefAdminStatus ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsbgp4mpeLocalPrefRemoteAS ARG_LIST((INT4  ,UINT4 ));

extern INT1
nmhSetFsbgp4mpeLocalPrefIPAddrAfi ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsbgp4mpeLocalPrefIPAddrSafi ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsbgp4mpeLocalPrefIPAddrPrefix ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsbgp4mpeLocalPrefIPAddrPrefixLen ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsbgp4mpeLocalPrefIntermediateAS ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsbgp4mpeLocalPrefDirection ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsbgp4mpeLocalPrefValue ARG_LIST((INT4  ,UINT4 ));

extern INT1
nmhSetFsbgp4mpeLocalPrefPreference ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsMIBgp4mpeLocalPrefVrfName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2Fsbgp4mpeUpdateFilterAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


extern INT1
nmhTestv2Fsbgp4mpeUpdateFilterRemoteAS ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));


extern INT1
nmhTestv2Fsbgp4mpeUpdateFilterIPAddrAfi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


extern INT1
nmhTestv2Fsbgp4mpeUpdateFilterIPAddrSafi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


extern INT1
nmhTestv2Fsbgp4mpeUpdateFilterIPAddrPrefix ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));


extern INT1
nmhTestv2Fsbgp4mpeUpdateFilterIPAddrPrefixLen ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


extern INT1
nmhTestv2Fsbgp4mpeUpdateFilterIntermediateAS ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));


extern INT1
nmhTestv2Fsbgp4mpeUpdateFilterDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


extern INT1
nmhTestv2Fsbgp4mpeUpdateFilterAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 
nmhTestv2FsMIBgp4mpeUpdateFilterVrfName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2Fsbgp4mpeMEDAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2Fsbgp4mpeMEDRemoteAS ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhTestv2Fsbgp4mpeMEDIPAddrAfi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2Fsbgp4mpeMEDIPAddrSafi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2Fsbgp4mpeMEDIPAddrPrefix ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2Fsbgp4mpeMEDIPAddrPrefixLen ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2Fsbgp4mpeMEDIntermediateAS ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2Fsbgp4mpeMEDDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 
nmhTestv2FsMIBgp4mpeMEDVrfName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1
nmhTestv2Fsbgp4mpeMEDValue ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhTestv2Fsbgp4mpeMEDPreference ARG_LIST((UINT4 *  ,INT4  ,INT4 ));


extern INT1
nmhSetFsbgp4mpeMEDAdminStatus ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsbgp4mpeMEDIPAddrAfi ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsbgp4mpeMEDIPAddrSafi ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsbgp4mpeMEDIntermediateAS ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsbgp4mpeMEDDirection ARG_LIST((INT4  ,INT4 ));

extern INT1 
nmhSetFsMIBgp4mpeMEDVrfName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsbgp4mpeMEDValue ARG_LIST((INT4  ,UINT4 ));

extern INT1
nmhSetFsbgp4mpeMEDPreference ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhTestv2Fsbgp4mpeAggregateAdminStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2Fsbgp4mpeAggregateIPAddrAfi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2Fsbgp4mpeAggregateIPAddrSafi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2Fsbgp4mpeAggregateIPAddrPrefix ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2Fsbgp4mpeAggregateIPAddrPrefixLen ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2Fsbgp4mpeAggregateAdvertise ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1 nmhTestv2Fsbgp4mpeAggregateAsSet(UINT4 *pu4ErrorCode , INT4 i4Fsbgp4mpeAggregateIndex , INT4 i4TestValFsbgp4mpeAggregateAsSet);

extern INT1 nmhTestv2Fsbgp4mpeAggregateSuppressRouteMapName(UINT4 *pu4ErrorCode , INT4 i4Fsbgp4mpeAggregateIndex , tSNMP_OCTET_STRING_TYPE *pTestValFsbgp4mpeAggregateSuppressRouteMapName);

extern INT1 nmhTestv2Fsbgp4mpeAggregateAdvertiseRouteMapName(UINT4 *pu4ErrorCode , INT4 i4Fsbgp4mpeAggregateIndex , tSNMP_OCTET_STRING_TYPE *pTestValFsbgp4mpeAggregateAdvertiseRouteMapName);

extern INT1 nmhTestv2Fsbgp4mpeAggregateAttributeRouteMapName(UINT4 *pu4ErrorCode , INT4 i4Fsbgp4mpeAggregateIndex , tSNMP_OCTET_STRING_TYPE *pTestValFsbgp4mpeAggregateAttributeRouteMapName);

extern INT1 
nmhTestv2FsMIBgp4mpeAggregateVrfName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsbgp4mpeMEDIPAddrPrefixLen ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetFsbgp4mpeMEDRemoteASs ARG_LIST((INT4  ,UINT4 ));

extern INT1
Bgp4Testv2Fsbgp4UpdateFilterIntermediateAS (UINT4 *,
                       INT4,
                       UINT1 *,
                       INT4);

extern INT1
Bgp4SetFsbgp4UpdateFilterIntermediateAS (INT4,
                                         UINT1*);


extern INT1
Bgp4SetFsbgp4LocalPrefIntermediateAS (INT4,
                                         UINT1*);
extern INT1
Bgp4SetFsbgp4MEDIntermediateAS (INT4,
                                         UINT1*);
extern INT1
Bgp4Testv2Fsbgp4LocalPrefIntermediateAS (UINT4 *,
                       INT4,
                       UINT1 *,
                       INT4);


extern INT4 Bgp4InitAddrPrefixStruct(tAddrPrefix *,  UINT2);
extern INT1
Bgp4Testv2Fsbgp4MEDIntermediateAS (UINT4 *,
                       INT4,
                       UINT1 *,
                       INT4);
extern INT1
nmhSetFsbgp4mpeMEDIPAddrPrefix (INT4, tSNMP_OCTET_STRING_TYPE *);
extern INT1
nmhSetFsbgp4mpeMEDRemoteAS (INT4, INT4);

extern INT4 Fsbgp4mpeUpdateFilterAdminStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeUpdateFilterIPAddrAfiSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeUpdateFilterIPAddrSafiSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeUpdateFilterIPAddrPrefixSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeUpdateFilterIPAddrPrefixLenSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeUpdateFilterIntermediateASSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeUpdateFilterDirectionSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeMEDAdminStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeMEDRemoteASSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeMEDIPAddrAfiSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeMEDIPAddrSafiSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeMEDIPAddrPrefixSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeMEDIPAddrPrefixLenSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeMEDIntermediateASSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeMEDDirectionSet(tSnmpIndex *, tRetVal *);
extern INT4 FsMIBgp4mpeMEDVrfNameSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeMEDValueSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeMEDPreferenceSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeAggregateAdminStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeAggregateIPAddrAfiSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeAggregateIPAddrSafiSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeAggregateIPAddrPrefixSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeAggregateIPAddrPrefixLenSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeAggregateAdvertiseSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeAggregateAsSetSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData);
extern INT4 Fsbgp4mpeAggregateSuppressRouteMapNameSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData);
extern INT4 Fsbgp4mpeAggregateAdvertiseRouteMapNameSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData);
extern INT4 Fsbgp4mpeAggregateAttributeRouteMapNameSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData);
extern INT4 Fsbgp4mpeLocalPrefAdminStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeLocalPrefRemoteASSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeLocalPrefIPAddrAfiSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeLocalPrefIPAddrSafiSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeLocalPrefIPAddrPrefixSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeLocalPrefIPAddrPrefixLenSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeLocalPrefIntermediateASSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeLocalPrefDirectionSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeLocalPrefValueSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeLocalPrefPreferenceSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4InFilterRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4CommIncomingFilterStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4CommOutgoingFilterStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4OutFilterRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeCommSetStatusRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeCommSetStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeAddCommRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeDeleteCommRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeAddExtCommRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeDeleteExtCommRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeExtCommSetStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeExtCommSetStatusRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeLinkBandWidthSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpePeerLinkBwRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeCapSupportedCapsRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeRtRefreshInboundRequestSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeSoftReconfigOutboundRequestSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4MplsVpnVrfRouteTargetRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4MplsVpnVrfRedisProtoMaskSet(tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4DistInOutRouteMapValueSet(tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4DistInOutRouteMapRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PreferenceValueSet(tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerAddStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeUpdateFilterActionSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeUpdateFilterRemoteASSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4ExtCommInFilterRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4ExtCommOutFilterRowStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4ExtCommIncomingFilterStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4ExtCommOutgoingFilterStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsbgpAscConfedPeerStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeImportRouteActionSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4RfdCutOffSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4RfdReuseSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4RfdMaxHoldDownTimeSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4RfdDecayHalfLifeTimeSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4RfdDecayTimerGranularitySet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4RfdReuseTimerGranularitySet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4RfdReuseIndxArraySizeSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpebgpPeerAdminStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpebgpPeerConnectRetryIntervalSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpebgpPeerHoldTimeConfiguredSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpebgpPeerKeepAliveConfiguredSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpebgpPeerMinASOriginationIntervalSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpebgpPeerMinRouteAdvertisementIntervalSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpePeerAllowAutomaticStartSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpePeerAllowAutomaticStopSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpebgpPeerIdleHoldTimeConfiguredSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpeDampPeerOscillationsSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpePeerDelayOpenSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpebgpPeerDelayOpenTimeConfiguredSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpePeerPrefixUpperLimitSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpePeerTcpConnectRetryCntSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpePeerExtConfigurePeerSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpePeerExtPeerRemoteAsSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpePeerExtEBGPMultiHopSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpePeerExtEBGPHopLimitSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpePeerExtNextHopSelfSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpePeerExtRflClientSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpePeerExtTcpSendBufSizeSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpePeerExtTcpRcvBufSizeSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpePeerExtLclAddressSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpePeerExtNetworkAddressSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpePeerExtGatewaySet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpePeerExtCommSendStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpePeerExtECommSendStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpePeerExtPassiveSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpePeerExtDefaultOriginateSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpePeerExtActivateMPCapabilitySet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4mpePeerExtDeactivateMPCapabilitySet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4GRAdminStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4TCPMD5AuthPwdSetSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4TCPMD5AuthPasswordSet(tSnmpIndex *, tRetVal *);
extern INT1 nmhGetFsbgp4GRAdminStatus ARG_LIST((INT4 *));
extern INT1 nmhGetFsbgp4GRRestartTimeInterval ARG_LIST((INT4 *));
extern INT1 nmhGetFsbgp4GRSelectionDeferralTimeInterval ARG_LIST((INT4 *));
extern INT1 nmhGetFsbgp4GRStaleTimeInterval ARG_LIST((INT4 *));
extern INT1 nmhGetFsbgp4RestartSupport ARG_LIST((INT4 *));
extern INT1 nmhGetFsbgp4RestartReason ARG_LIST((INT4 *));
extern INT1 nmhGetFsbgp4RflbgpClusterId ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsbgp4NextHopProcessingInterval ARG_LIST((INT4 *));
extern INT1 nmhTestv2Fsbgp4GRAdminStatus ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2Fsbgp4GRRestartTimeInterval ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2Fsbgp4GRSelectionDeferralTimeInterval ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2Fsbgp4GRStaleTimeInterval ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2Fsbgp4RestartSupport ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2Fsbgp4RestartReason ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2Fsbgp4RflbgpClusterId ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2Fsbgp4NextHopProcessingInterval ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2Fsbgp4TCPAOAuthKeyStatus(UINT4*, INT4  , tSNMP_OCTET_STRING_TYPE *, INT4, INT4);
extern INT1 nmhSetFsbgp4TCPAOAuthKeyStatus(INT4,tSNMP_OCTET_STRING_TYPE *, INT4, INT4);
extern INT1 Bgp4GetFirstTCPAOKeyId (UINT4, tAddrPrefix , INT4*);
extern INT1 Bgp4LowGetIpAddress (INT4, tSNMP_OCTET_STRING_TYPE*, tAddrPrefix *);
extern INT1 nmhGetNextIndexFsMIBgpContextTable ARG_LIST((INT4 , INT4 *));
extern INT1 nmhGetFsbgp4FourByteASNSupportStatus ARG_LIST((INT4 *));
extern INT1 nmhSetFsbgp4FourByteASNSupportStatus ARG_LIST((INT4 ));
extern INT1 nmhTestv2Fsbgp4FourByteASNSupportStatus ARG_LIST((UINT4 *  ,INT4 ));

extern INT4 Fsbgp4GRRestartTimeIntervalSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4GRSelectionDeferralTimeIntervalSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4GRStaleTimeIntervalSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4RestartSupportSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4RestartReasonSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4RRDAdminStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4RRDDefaultMetricSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4RfdAdminStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4RRDMetricValueSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4RRDMatchTypeEnableSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4RRDProtoMaskForEnableSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4RRDRouteMapNameSet(tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4RRDSrcProtoMaskForDisableSet(tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupStatusSet(tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupRemoteAsSet(tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupHoldTimeConfiguredSet(tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupKeepAliveConfiguredSet(tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupConnectRetryIntervalSet(tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupMinASOriginIntervalSet(tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupMinRouteAdvIntervalSet(tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupAllowAutomaticStartSet(tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupAllowAutomaticStopSet(tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupIdleHoldTimeConfiguredSet(tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupDampPeerOscillationsSet(tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupDelayOpenSet(tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupDelayOpenTimeConfiguredSet(tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupPrefixUpperLimitSet(tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupTcpConnectRetryCntSet(tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupEBGPMultiHopSet (tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupEBGPHopLimitSet (tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupNextHopSelfSet (tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupRflClientSet (tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupTcpSendBufSizeSet (tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupTcpRcvBufSizeSet (tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupCommSendStatusSet (tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupECommSendStatusSet (tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupPassiveSet (tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupDefaultOriginateSet (tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupActivateMPCapabilitySet (tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupDeactivateMPCapabilitySet (tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupRouteMapNameInSet (tSnmpIndex *, tRetVal *);
extern INT4 FsBgp4PeerGroupRouteMapNameOutSet (tSnmpIndex *, tRetVal *);
extern INT4 Fsbgp4RflbgpClusterIdSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData);
extern INT4 Fsbgp4NextHopProcessingIntervalSet(tSnmpIndex *, tRetVal *);

extern INT4  BgpSetContext(INT4);
extern VOID  BgpResetContext (VOID);
extern INT4 Fsbgp4GlobalAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData);
extern INT4 Fsbgp4CapabilitySupportAvailableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData);
extern INT1 nmhGetFsbgp4CapabilitySupportAvailable ARG_LIST((INT4 *));
extern INT1 nmhSetFsbgp4CapabilitySupportAvailable ARG_LIST((INT4 ));
extern INT1 nmhTestv2Fsbgp4CapabilitySupportAvailable ARG_LIST((UINT4 *  ,INT4 ));

extern INT1 nmhGetFirstIndexFsMIBgpContextTable ARG_LIST((INT4 *));

extern INT1 nmhGetFirstIndexFsbgp4TCPMKTAuthTable(INT4 *pi4Fsbgp4TCPMKTAuthKeyId);
extern INT1 nmhGetNextIndexFsbgp4TCPMKTAuthTable(INT4 i4Fsbgp4TCPMKTAuthKeyId ,INT4 *pi4NextFsbgp4TCPMKTAuthKeyId );
extern INT1 nmhGetFsbgp4TCPMKTAuthRecvKeyId(INT4 i4Fsbgp4TCPMKTAuthKeyId , INT4 *pi4RetValFsbgp4TCPMKTAuthRecvKeyId);
extern INT1 nmhGetFsbgp4TCPMKTAuthAlgo(INT4 i4Fsbgp4TCPMKTAuthKeyId , INT4 *pi4RetValFsbgp4TCPMKTAuthAlgo);
extern INT1 nmhSetFsbgp4TCPMKTAuthTcpOptExc(INT4 i4Fsbgp4TCPMKTAuthKeyId , INT4 i4SetValFsbgp4TCPMKTAuthTcpOptExc);
extern INT1 nmhSetFsbgp4TCPMKTAuthRowStatus(INT4 i4Fsbgp4TCPMKTAuthKeyId , INT4 i4SetValFsbgp4TCPMKTAuthRowStatus);
extern INT1 nmhGetFsbgp4TCPMKTAuthRowStatus(INT4 i4Fsbgp4TCPMKTAuthKeyId , INT4* pi4RetValFsbgp4TCPMKTAuthRowStatus);
extern INT1 nmhSetFsbgp4TCPMKTAuthRecvKeyId(INT4 i4Fsbgp4TCPMKTAuthKeyId , INT4 i4SetValFsbgp4TCPMKTAuthRecvKeyId);
extern INT1 nmhTestv2Fsbgp4TCPMKTAuthRowStatus(UINT4 *pu4ErrorCode , INT4 i4Fsbgp4TCPMKTAuthKeyId , INT4 i4TestValFsbgp4TCPMKTAuthRowStatus);
extern INT1 nmhTestv2Fsbgp4TCPMKTAuthRecvKeyId(UINT4 *pu4ErrorCode , INT4 i4Fsbgp4TCPMKTAuthKeyId , INT4 i4TestValFsbgp4TCPMKTAuthRecvKeyId);
extern INT1 nmhTestv2Fsbgp4TCPMKTAuthMasterKey(UINT4 *pu4ErrorCode , INT4 i4Fsbgp4TCPMKTAuthKeyId , tSNMP_OCTET_STRING_TYPE *pTestValFsbgp4TCPMKTAuthMasterKey);
extern INT1 nmhSetFsbgp4TCPMKTAuthMasterKey(INT4 i4Fsbgp4TCPMKTAuthKeyId , tSNMP_OCTET_STRING_TYPE *pSetValFsbgp4TCPMKTAuthMasterKey);
extern INT1 nmhGetFsbgp4TCPMKTAuthTcpOptExc(INT4 i4Fsbgp4TCPMKTAuthKeyId , INT4 *pi4RetValFsbgp4TCPMKTAuthTcpOptExc);


extern INT1
nmhSetFsbgp4GRAdminStatus ARG_LIST((INT4 ));

extern INT1
nmhSetFsbgp4GRRestartTimeInterval ARG_LIST((INT4 ));

extern INT1
nmhSetFsbgp4GRSelectionDeferralTimeInterval ARG_LIST((INT4 ));

extern INT1
nmhSetFsbgp4GRStaleTimeInterval ARG_LIST((INT4 ));

extern INT1
nmhSetFsbgp4NextHopProcessingInterval ARG_LIST((INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupActivateMPCapability ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupAllowAutomaticStart ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupAllowAutomaticStop ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupCommSendStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupConnectRetryInterval ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupDampPeerOscillations ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupDeactivateMPCapability ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupDefaultOriginate ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupDelayOpen ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));


extern INT1
nmhSetFsBgp4PeerGroupDelayOpenTimeConfigured ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupEBGPHopLimit ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupEBGPMultiHop ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupECommSendStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupHoldTimeConfigured ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupIdleHoldTimeConfigured ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupKeepAliveConfigured ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupMinASOriginInterval ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupMinRouteAdvInterval ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupNextHopSelf ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupPassive ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupPrefixUpperLimit ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupRemoteAs ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

extern INT1
nmhSetFsBgp4PeerGroupRflClient ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupRouteMapNameIn ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsBgp4PeerGroupRouteMapNameOut ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetFsBgp4PeerGroupStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupTcpConnectRetryCnt ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupTcpRcvBufSize ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsBgp4PeerGroupTcpSendBufSize ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsbgp4RestartReason ARG_LIST((INT4 ));

extern INT1
nmhSetFsbgp4RestartSupport ARG_LIST((INT4 ));

extern INT1
nmhSetFsbgp4RfdAdminStatus ARG_LIST((INT4 ));

extern INT1
nmhSetFsbgp4RflbgpClusterId ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhGetFsbgp4IBGPMaxPaths ARG_LIST((INT4 *));
extern INT1 nmhGetFsbgp4EBGPMaxPaths ARG_LIST((INT4 *));
extern INT1 nmhGetFsbgp4EIBGPMaxPaths ARG_LIST((INT4 *));
extern INT1 nmhSetFsbgp4IBGPMaxPaths ARG_LIST((INT4 ));
extern INT1 nmhSetFsbgp4EBGPMaxPaths ARG_LIST((INT4 ));
extern INT1 nmhSetFsbgp4EIBGPMaxPaths ARG_LIST((INT4 ));
extern INT1 nmhTestv2Fsbgp4IBGPMaxPaths ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2Fsbgp4EBGPMaxPaths ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2Fsbgp4EIBGPMaxPaths ARG_LIST((UINT4 *  ,INT4 ));

extern INT1 nmhGetFsbgp4mpeCapAnnouncedStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,
            INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1 nmhGetFsbgp4mpeCapReceivedStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , 
            INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1 nmhGetFsbgp4mpePeerIpPrefixNameIn ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,
            tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsbgp4mpePeerIpPrefixNameOut ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,
            tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsBgp4PeerGroupIpPrefixNameIn ARG_LIST((tSNMP_OCTET_STRING_TYPE *,
            tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsBgp4PeerGroupIpPrefixNameOut ARG_LIST((tSNMP_OCTET_STRING_TYPE *,
            tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsBgp4PeerGroupOrfType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));
extern INT1 nmhGetFsBgp4PeerGroupOrfCapMode ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1 nmhGetFsBgp4PeerGroupBfdStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));
extern INT1 nmhSetFsBgp4PeerGroupIpPrefixNameIn ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,
            tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsBgp4PeerGroupIpPrefixNameOut ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,
            tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsBgp4PeerGroupOrfType ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));
extern INT1 nmhSetFsBgp4PeerGroupOrfCapMode ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhSetFsBgp4PeerGroupBfdStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhTestv2FsBgp4PeerGroupIpPrefixNameIn ARG_LIST((UINT4 *  ,
            tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsBgp4PeerGroupIpPrefixNameOut ARG_LIST((UINT4 *  ,
            tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2FsBgp4PeerGroupOrfType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,UINT4 ));
extern INT1 nmhTestv2FsBgp4PeerGroupOrfCapMode ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhTestv2FsBgp4PeerGroupBfdStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));
extern INT1 nmhSetFsbgp4mpePeerIpPrefixNameIn ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,
            tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsbgp4mpePeerIpPrefixNameOut ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,
            tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2Fsbgp4mpePeerIpPrefixNameIn ARG_LIST((UINT4 *  ,INT4  , 
            tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhTestv2Fsbgp4mpePeerIpPrefixNameOut ARG_LIST((UINT4 *  ,INT4  , 
             tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));
extern INT4 Bgp4SnmphOrfCapabilityEnable (UINT4, tAddrPrefix, UINT2, UINT1, UINT1, UINT1);
extern INT4 Bgp4SnmphOrfCapabilityDisable (UINT4, tAddrPrefix, UINT2, UINT1, UINT1, UINT1);
extern INT1 nmhGetFirstIndexFsBgp4ORFListTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  ,
            INT4 * , INT4 * , UINT4 * , UINT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , UINT4 * ,
            UINT4 * , INT4 *));
extern INT1 nmhGetNextIndexFsBgp4ORFListTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *,
            tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , 
            UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , 
            UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

#endif /* BGP_WANTED */
#endif
