/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fwlweb.h,v 1.4 2011/11/03 06:26:15 siva Exp $
 *
 * Description: This file contains prototypes for Firwall routines web 
 *              interface.
 *
 *******************************************************************/

#ifndef __FWLWEB_H__
#define __FWLWEB_H__

#define  FWL_END_OF_STRING  '\0'
#define  FWL_DESTROY                     6

PUBLIC CONST CHR1  *FwlCliErrString [];


VOID IssPrintAvailableFirewallFilters (tHttp * pHttp);
VOID IssProcessFwlLogPage (tHttp * pHttp);
VOID IssProcessFwlAclPage (tHttp *pHttp);
VOID IssProcessFwlStatsPage(tHttp *pHttp);
VOID IssProcessFwlIntfPage (tHttp * pHttp);
VOID IssProcessFwlAclPageGet(tHttp *pHttp);
VOID IssProcessFwlAclPageSet(tHttp *pHttp);
VOID IssProcessFwlIntfPageGet (tHttp * pHttp);
VOID IssProcessFwlIntfPageSet (tHttp * pHttp);

VOID IssProcessFwlDMZV6Page (tHttp * pHttp);
VOID IssProcessFwlDMZV6PageGet (tHttp * pHttp);
VOID IssProcessFwlDMZV6PageSet (tHttp * pHttp);


VOID IssProcessFwlIcmpPage (tHttp * pHttp);
VOID IssProcessFwlicmpPageSet (tHttp * pHttp);
VOID IssProcessFwlicmpPageGet (tHttp * pHttp);

PUBLIC VOID FwlClearLogBuffer (VOID);

PUBLIC INT1 nmhGetFwlStatTotalLargeFragmentPacketsDenied PROTO ((UINT4 *pu4RetValFwlStatTotalLargeFragmentPacketsDenied));

PUBLIC INT1 nmhGetFwlStatTotalAttacksPacketsDenied PROTO ((UINT4  *pu4RetValFwlStatTotalAttacksPacketsDenied));


/* Low Level SET Routine for All Objects.  */

PUBLIC INT1
nmhSetFwlIfIfType ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFwlIfIpOptions ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFwlIfFragments ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFwlIfICMPType ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFwlIfICMPCode ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFwlIfRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

PUBLIC INT1
nmhTestv2FwlIfIfType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FwlIfIpOptions ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FwlIfFragments ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FwlIfICMPType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FwlIfICMPCode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FwlIfRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Validate Index Instance for FwlDefnIfTable. */
PUBLIC INT1
nmhValidateIndexInstanceFwlDefnIfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FwlDefnIfTable  */

PUBLIC INT1
nmhGetFirstIndexFwlDefnIfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

PUBLIC INT1
nmhGetNextIndexFwlDefnIfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

PUBLIC INT1
nmhGetFwlIfIfType ARG_LIST((INT4 ,INT4 *));

PUBLIC INT1
nmhGetFwlIfIpOptions ARG_LIST((INT4 ,INT4 *));

PUBLIC INT1
nmhGetFwlIfFragments ARG_LIST((INT4 ,INT4 *));

PUBLIC INT1
nmhGetFwlIfICMPType ARG_LIST((INT4 ,INT4 *));

PUBLIC INT1
nmhGetFwlIfICMPCode ARG_LIST((INT4 ,INT4 *));

PUBLIC INT1
nmhGetFwlIfRowStatus ARG_LIST((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

PUBLIC INT1
nmhGetFwlStatInspectedPacketsCount ARG_LIST((UINT4 *));

PUBLIC INT1
nmhGetFwlStatTotalPacketsDenied ARG_LIST((UINT4 *));

PUBLIC INT1
nmhGetFwlStatTotalPacketsAccepted ARG_LIST((UINT4 *));

PUBLIC INT1
nmhGetFwlStatTotalIcmpPacketsDenied ARG_LIST((UINT4 *));

PUBLIC INT1
nmhGetFwlStatTotalSynPacketsDenied ARG_LIST((UINT4 *));

PUBLIC INT1
nmhGetFwlStatTotalIpSpoofedPacketsDenied ARG_LIST((UINT4 *));

PUBLIC INT1
nmhGetFwlStatTotalSrcRoutePacketsDenied ARG_LIST((UINT4 *));

PUBLIC INT1
nmhGetFwlStatTotalTinyFragmentPacketsDenied ARG_LIST((UINT4 *));

PUBLIC INT1
nmhGetFwlStatTotalFragmentedPacketsDenied ARG_LIST((UINT4 *));

PUBLIC INT1
nmhGetFwlStatTotalIpOptionPacketsDenied ARG_LIST((UINT4 *));

PUBLIC INT1
nmhGetFwlStatMemoryAllocationFailCount ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for FwlStatIfTable. */
PUBLIC INT1
nmhValidateIndexInstanceFwlStatIfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FwlStatIfTable  */

PUBLIC INT1
nmhGetFirstIndexFwlStatIfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

PUBLIC INT1
nmhGetNextIndexFwlStatIfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

PUBLIC INT1
nmhGetFwlStatIfFilterCount ARG_LIST((INT4 ,INT4 *));

PUBLIC INT1
nmhGetFwlStatIfPacketsDenied ARG_LIST((INT4 ,UINT4 *));

PUBLIC INT1
nmhGetFwlStatIfPacketsAccepted ARG_LIST((INT4 ,UINT4 *));

PUBLIC INT1
nmhGetFwlStatIfSynPacketsDenied ARG_LIST((INT4 ,UINT4 *));

PUBLIC INT1
nmhGetFwlStatIfIcmpPacketsDenied ARG_LIST((INT4 ,UINT4 *));

PUBLIC INT1
nmhGetFwlStatIfIpSpoofedPacketsDenied ARG_LIST((INT4 ,UINT4 *));

PUBLIC INT1
nmhGetFwlStatIfSrcRoutePacketsDenied ARG_LIST((INT4 ,UINT4 *));

PUBLIC INT1
nmhGetFwlStatIfTinyFragmentPacketsDenied ARG_LIST((INT4 ,UINT4 *));

PUBLIC INT1
nmhGetFwlStatIfFragmentPacketsDenied ARG_LIST((INT4 ,UINT4 *));

PUBLIC INT1
nmhGetFwlStatIfIpOptionPacketsDenied ARG_LIST((INT4 ,UINT4 *));

PUBLIC INT1
nmhGetFwlStatIfClear ARG_LIST((INT4 ,INT4 *));

PUBLIC INT1
nmhGetFwlIfTrapThreshold ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

PUBLIC INT1
nmhSetFwlStatIfClear ARG_LIST((INT4  ,INT4 ));

PUBLIC INT1
nmhSetFwlIfTrapThreshold ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

PUBLIC INT1
nmhTestv2FwlStatIfClear ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FwlIfTrapThreshold ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Proto Type for Low Level GET FIRST fn for FwlDefnAclTable  */

PUBLIC INT1
nmhGetFirstIndexFwlDefnAclTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

PUBLIC INT1
nmhGetNextIndexFwlDefnAclTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

PUBLIC INT1
nmhGetFwlAclAction ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

PUBLIC INT1
nmhGetFwlAclSequenceNumber ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

PUBLIC INT1
nmhGetFwlAclAclType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

PUBLIC INT1
nmhGetFwlAclLogTrigger ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

PUBLIC INT1
nmhGetFwlAclFragAction ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

PUBLIC INT1
nmhGetFwlAclRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

PUBLIC INT1
nmhSetFwlAclAction ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

PUBLIC INT1
nmhSetFwlAclSequenceNumber ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

PUBLIC INT1
nmhSetFwlAclLogTrigger ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

PUBLIC INT1
nmhSetFwlAclFragAction ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

PUBLIC INT1
nmhSetFwlAclRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

PUBLIC INT1
nmhTestv2FwlAclAction ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FwlAclSequenceNumber ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FwlAclLogTrigger ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FwlAclFragAction ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

PUBLIC INT1
nmhTestv2FwlAclRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level SET Routine for All Objects.  */

PUBLIC INT1
nmhSetFwlRuleFilterSet ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

PUBLIC INT1
nmhSetFwlRuleRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

PUBLIC INT1
nmhTestv2FwlRuleFilterSet ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

PUBLIC INT1
nmhTestv2FwlRuleRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Proto Type for Low Level GET FIRST fn for FwlDefnRuleTable  */

PUBLIC INT1
nmhGetFirstIndexFwlDefnRuleTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

PUBLIC INT1
nmhGetNextIndexFwlDefnRuleTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

PUBLIC INT1
nmhGetFwlRuleFilterSet ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

PUBLIC INT1
nmhGetFwlRuleRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto Type for Low Level GET FIRST fn for FwlDefnFilterTable  */

PUBLIC INT1
nmhGetFirstIndexFwlDefnFilterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

PUBLIC INT1
nmhGetNextIndexFwlDefnFilterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

PUBLIC INT1
nmhGetFwlFilterRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

PUBLIC INT4 FwlUtilReleaseWebLog(UINT1 *pu1LogPtr);
/* DMZv6 */

INT1 nmhGetFirstIndexFwlDefnIPv6DmzTable(tSNMP_OCTET_STRING_TYPE * pFwlDmzIpv6Index);
INT1 nmhGetNextIndexFwlDefnIPv6DmzTable
                        (tSNMP_OCTET_STRING_TYPE *pFwlDmzIpv6Index ,
                         tSNMP_OCTET_STRING_TYPE * pNextFwlDmzIpv6Index );
INT1 nmhGetFwlDmzAddressType
                 (tSNMP_OCTET_STRING_TYPE *pFwlDmzIpv6Index ,
                  INT4 *pi4RetValFwlDmzAddressType);
INT1 nmhGetFwlDmzIpv6RowStatus
                  (tSNMP_OCTET_STRING_TYPE *pFwlDmzIpv6Index ,
                   INT4 *pi4RetValFwlDmzIpv6RowStatus);

INT1 nmhSetFwlDmzAddressType
                (tSNMP_OCTET_STRING_TYPE *pFwlDmzIpv6Index ,
                 INT4 i4SetValFwlDmzAddressType);

INT1 nmhSetFwlDmzIpv6RowStatus
                  (tSNMP_OCTET_STRING_TYPE *pFwlDmzIpv6Index ,
                   INT4 i4SetValFwlDmzIpv6RowStatus);

INT1 nmhTestv2FwlDmzAddressType
                    (UINT4 *pu4ErrorCode ,
                     tSNMP_OCTET_STRING_TYPE *pFwlDmzIpv6Index ,
                     INT4 i4TestValFwlDmzAddressType);

INT1 nmhTestv2FwlDmzIpv6RowStatus
                  (UINT4 *pu4ErrorCode ,
                   tSNMP_OCTET_STRING_TYPE *pFwlDmzIpv6Index ,
                   INT4 i4TestValFwlDmzIpv6RowStatus);


/*ICMP v6 */

INT1
nmhTestv2FwlIfICMPv6MsgType (UINT4 *pu4ErrorCode, INT4 i4FwlIfIfIndex,
                        INT4 i4TestValFwlIfICMPv6MsgType);
INT1
nmhSetFwlIfICMPv6MsgType (INT4 i4FwlIfIfIndex, INT4 i4SetValFwlIfICMPv6MsgType);

INT1 
nmhGetFwlIfICMPv6MsgType (INT4 i4FwlIfIfIndex,INT4 *pi4RetValFwlIfICMPv6MsgType);
#endif/*__FWLWEB_H__*/
