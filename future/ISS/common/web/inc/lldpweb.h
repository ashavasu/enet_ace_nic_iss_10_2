
/* $Id: lldpweb.h,v 1.12 2013/11/29 11:16:20 siva Exp $            */
#ifndef _LLDPWEB_H
#define _LLDPWEB_H
#ifdef LLDP_WANTED


/* LLDP Admin Status */
enum
{
    LLDP_TRANSMIT     = 1,
    LLDP_RECEIVE      = 2,
    LLDP_NO_TRANSMIT  = 3,
    LLDP_NO_RECEIVE   = 4,
    LLDP_MAX_STATE    = 5                                                                                     
};

#define LLDP_CLI_CHASSIS_DISP_LEN 10

/* Max Remote Chassis Id Length to be displayed in CLI */
#define LLDP_CLI_PORT_DISP_LEN 10

/* LLDP Remote switch MAC Address */
#define LLDP_CLI_MAN_ADDR_TYPE_MAC 0

/* Max No of LLDP System Capabability bits */
#define LLDP_CLI_SYS_CAPAB_MAX_BITS ISS_SYS_CAPABILITIES_LEN*(BITS_PER_BYTE/2)


#define LLDP_MAX_LEN_PORTID            255
#define LLDP_MAX_LEN_CHASSISID         255
#define LLDP_MAX_LEN_ID                50
#define LLDP_MAX_MAC_STRING_SIZE       21
/* LLDP Neighbor info structure */
typedef struct LldpWebNeighborInfo {
    INT4       i4IfIndex;
    INT4       i4Option;
    UINT1      au1ChassisId[LLDP_MAX_LEN_CHASSISID + 1];
    UINT1      au1PortId[LLDP_MAX_LEN_PORTID + 1];
}tLldpWebNeighborInfo;


VOID
IssProcessLldpGlobalConfPage PROTO ((tHttp * pHttp));


PUBLIC INT4
LldpWebSetSystemCtrl PROTO(( INT4 i4Status));

PUBLIC INT4
LldpWebSetModuleStatus PROTO(( INT4 i4Status));

PUBLIC INT4
LldpWebSetVersion PROTO ((INT4 i4Status));

PUBLIC INT4
LldpWebSetTransmitInterval PROTO (( INT4 i4Interval));


PUBLIC INT4 LldpWebSetTransmitDelay PROTO (( INT4 i4TxDelay));


PUBLIC INT4
LldpWebSetNotifyInterval PROTO (( INT4 i4NotifyInterval));

PUBLIC INT4
LldpWebSetChassisSubtype PROTO (( INT4 i4ChassisIdSubtype,
                                  UINT1 *pu1ChassisId));

PUBLIC INT4
LldpWebSetTxCreditMax PROTO ((INT4 i4TxCreditMax));

PUBLIC INT4
LldpWebSetMessageFastTx PROTO ((INT4 i4MessageFastTx));

PUBLIC INT4
LldpWebSetAgent PROTO((INT4 i4InterfaceIndex, UINT1 *pu1MacAddr, UINT1 u1RowStatus));

PUBLIC INT4
LldpWebSetTxFastInit PROTO ((INT4 i4TxFastInit));

PUBLIC INT4
LldpWebClearCounters PROTO ((VOID));

PUBLIC INT4
LldpWebClearTable PROTO ((VOID));

PUBLIC INT4
LldpWebSetReinitDelay PROTO (( INT4 i4ReinitDelay));


PUBLIC INT4
LldpWebSetHoldtimeMuliplier PROTO (( INT4 i4Val));

VOID
IssProcessLldpGlobalConfPageSet PROTO ((tHttp * pHttp));


VOID
IssProcessLldpGlobalConfPageGet PROTO ((tHttp * pHttp));




VOID
LldpWebShowTraffic PROTO ((INT4 i4IfIndex));


VOID
IssProcessLldpErrorPage PROTO ((tHttp * pHttp));


VOID
IssProcessLldpErrorPageGet PROTO ((tHttp * pHttp));


VOID
IssProcessLldpBasicSettingsPage PROTO ((tHttp * pHttp));


VOID
IssProcessLldpBasicSettingsPageGet PROTO ((tHttp * pHttp));



VOID
IssProcessLldpBasicSettingsPageSet PROTO ((tHttp * pHttp));


VOID
IssProcessLldpTrafficPage PROTO ((tHttp * pHttp));


VOID
IssProcessLldpTrafficPageGet PROTO ((tHttp * pHttp));


VOID
IssProcessLldpStatsPageGet PROTO ((tHttp * pHttp));


VOID
IssProcessLldpStatsPage PROTO ((tHttp * pHttp));

VOID
IssProcessLldpTrafficPageSet PROTO ((tHttp * pHttp));

VOID
IssProcessLldpInterfacePage PROTO ((tHttp * pHttp));

VOID
IssProcessLldpInterfacePageSet PROTO ((tHttp * pHttp));

VOID
IssProcessLldpInterfacePageGet PROTO ((tHttp * pHttp));

PUBLIC INT4
LldpWebSetAdminStatus PROTO ((INT4 i4IfIndex, UINT4 u4DestMacInd, INT4 i4AdminStatus));

PUBLIC INT4
LldpWebSetNotification PROTO ((INT4 i4IfIndex, INT4 i4Status,
                        INT4 i4NotifyType, UINT4 u4DestMacIndex));

PUBLIC INT4
LldpWebUpdTraceInput PROTO ((UINT1 *pu1TraceInput));

VOID IssProcessLldpNeighborPageGet PROTO ((tHttp * pHttp));


VOID
IssProcessLldpNeighborPage PROTO ((tHttp * pHttp));


VOID
ISSProcessLldpAgentPage PROTO ((tHttp * pHttp));


VOID 
ISSProcessLldpAgentPageSet PROTO ((tHttp * pHttp));

PUBLIC INT4
LldpWebShowNeighborBriefInfo PROTO ((tHttp * pHttp, UINT4 u4RemTimeMark,
                              INT4 i4RemLocalPortNum, INT4 i4RemIndex, UINT4 u4RemDestMacInd));

VOID
LldpWebShowSysSupportCapab PROTO ((tHttp * pHttp, tSNMP_OCTET_STRING_TYPE SysCapSupported));


VOID
IssProcessLldpNeighborPageSet PROTO ((tHttp * pHttp));

VOID
ISSProcessLldpAgentDetailsPage PROTO ((tHttp * pHttp));

VOID
IssProcessLldpAgentDetailsPageSet PROTO ((tHttp * pHttp));

VOID
IssProcessLldpAgentDetailsPageGet PROTO ((tHttp * pHttp));

PUBLIC VOID 
LldpPortGetSysMacAddr        PROTO ((tMacAddr BaseMacAddr));

extern UINT1 gau1LldpMcastAddr[];
extern UINT4 gu4LldpDestMacAddrTblIndex;
extern INT1 nmhGetFsLldpSystemControl (INT4 *pi4RetValFsLldpSystemControl);
extern INT1 nmhGetFsLldpModuleStatus (INT4 *pi4RetValFsLldpModuleStatus);
extern INT1 nmhTestv2FsLldpSystemControl (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsLldpSystemControl);
extern INT1 nmhGetFslldpv2Version (INT4 *pi4RetValFslldpv2Version);
extern INT1 nmhGetLldpV2StatsRxPortTLVsUnrecognizedTotal ARG_LIST((INT4 ,UINT4 ,UINT4 *));
extern INT1 nmhGetFsLldpMemAllocFailure ARG_LIST((INT4 *));
extern INT1 nmhGetFsLldpInputQOverFlows ARG_LIST((INT4 *));
extern INT1 nmhGetLldpV2StatsRemTablesDrops ARG_LIST((UINT4 *));
extern INT1 nmhGetLldpV2MessageTxInterval ARG_LIST((UINT4 *));

extern INT1 nmhGetLldpV2ReinitDelay ARG_LIST((UINT4 *));
extern INT1 nmhGetLldpV2MessageTxHoldMultiplier ARG_LIST((UINT4 *));
extern INT1 nmhGetLldpTxDelay ARG_LIST((INT4 *));
extern INT1 nmhGetLldpV2NotificationInterval ARG_LIST((UINT4 *));
extern INT1 nmhGetFsLldpLocChassisIdSubtype ARG_LIST((INT4 *));
extern INT1 nmhGetLldpV2LocChassisId ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

extern INT1 nmhTestv2LldpV2MessageTxInterval ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2LldpV2MessageTxHoldMultiplier ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhTestv2LldpV2ReinitDelay ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhSetLldpV2ReinitDelay ARG_LIST((INT4 ));
extern INT1 nmhTestv2LldpTxDelay ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhSetLldpTxDelay ARG_LIST((INT4 ));
extern INT1 nmhTestv2LldpV2NotificationInterval ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhSetLldpV2NotificationInterval ARG_LIST((INT4 ));
extern INT1 nmhTestv2FsLldpLocChassisIdSubtype ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhSetFsLldpLocChassisIdSubtype ARG_LIST((INT4 ));
extern INT4 LldpUtilGetChassisIdLen  PROTO ((INT4 i4Subtype,
                                             UINT1 *pu1ChassId,
                                             UINT2 *pu2Length));
extern tLldpv2AgentToLocPort * LldpGetPortMapTableNode PROTO ((UINT4 u4IfIndex, tMacAddr * pMacAddr));
extern INT1 nmhTestv2FsLldpLocChassisId ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsLldpLocChassisId ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

extern INT4 LldpUtlGetDestMacTblIndex PROTO ((tMacAddr DestMac, UINT4 *pu4DestMacIndex));

extern INT4 LldpTxUtlGetLocPortEntry PROTO ((UINT4 u4PortIndex,
                                                 tLldpLocPortInfo **
                                                 ppLocPortEntry));
extern VOID LldpUtilClearStats           PROTO ((tLldpLocPortInfo *pPortInfo));
extern VOID LldpRxUtlDrainRemSysTables   PROTO ((VOID));
extern INT1 nmhGetLldpV2StatsRemTablesLastChangeTime ARG_LIST((UINT4 *));
extern INT1 nmhGetLldpV2StatsRemTablesInserts ARG_LIST((UINT4 *));
extern INT1 nmhGetLldpV2StatsRemTablesDeletes ARG_LIST((UINT4 *));
extern INT1 nmhGetLldpV2StatsRemTablesAgeouts ARG_LIST((UINT4 *));
extern INT1 nmhGetFsLldpStatsRemTablesUpdates ARG_LIST((UINT4 *));
extern INT1 nmhGetLldpV2PortConfigAdminStatus ARG_LIST((INT4 ,UINT4 ,INT4 *));
extern INT1 nmhGetLldpV2PortConfigNotificationEnable ARG_LIST((INT4 ,UINT4 ,INT4 *));
extern INT1 nmhGetFsLldpPortConfigNotificationType ARG_LIST((INT4 ,INT4 *));
extern INT1 nmhTestv2LldpV2PortConfigAdminStatus ARG_LIST((UINT4 *  ,INT4  ,UINT4 ,INT4 ));
extern INT1 nmhSetLldpV2PortConfigAdminStatus ARG_LIST((INT4  ,UINT4 ,INT4 ));
extern INT1 nmhTestv2LldpV2PortConfigNotificationEnable ARG_LIST((UINT4 *  ,INT4  ,UINT4 ,INT4 ));
extern INT1 nmhSetLldpV2PortConfigNotificationEnable ARG_LIST((INT4  ,UINT4 ,INT4 ));
extern INT1 nmhTestv2FsLldpPortConfigNotificationType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
extern INT1 nmhSetFsLldpPortConfigNotificationType ARG_LIST((INT4  ,INT4 ));
extern INT1 nmhTestv2FsLldpTraceInput ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhSetFsLldpTraceInput ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
extern INT1 nmhGetFirstIndexLldpV2RemTable ARG_LIST((UINT4 * , INT4 * , UINT4 * ,UINT4 *));
extern INT1 nmhGetNextIndexLldpV2RemTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 * ,UINT4 , UINT4 *));
extern INT1 nmhGetLldpV2RemChassisIdSubtype ARG_LIST((UINT4  , INT4  ,UINT4 ,INT4 ,INT4 *));
extern INT1 nmhGetLldpV2RemChassisId ARG_LIST((UINT4  , INT4  ,UINT4 ,INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetLldpV2RemSysCapSupported ARG_LIST((UINT4  , INT4  ,UINT4 , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetLldpV2RemPortIdSubtype ARG_LIST((UINT4  , INT4  ,UINT4 , INT4 ,INT4 *));
extern INT1 nmhGetLldpV2RemPortId ARG_LIST((UINT4  , INT4  ,UINT4 , INT4 ,tSNMP_OCTET_STRING_TYPE * ));
extern INT1 nmhGetFsLldpLocPortDstMac ARG_LIST ((INT4, tMacAddr *));
extern INT1 nmhTestv2FsLldpLocPortDstMac ARG_LIST ((UINT4 *, INT4, tMacAddr));
extern INT1 nmhSetFsLldpLocPortDstMac ARG_LIST ((INT4, tMacAddr));
extern INT1 nmhGetLldpV2TxCreditMax ARG_LIST ((UINT4 *));
extern INT1 nmhGetLldpV2MessageFastTx ARG_LIST ((UINT4 *));
extern INT1 nmhGetLldpV2TxFastInit ARG_LIST ((UINT4 *));
extern INT1 nmhSetLldpV2TxCreditMax ARG_LIST ((UINT4 ));
extern INT1 nmhSetLldpV2MessageFastTx ARG_LIST ((UINT4 ));
extern INT1 nmhSetLldpV2TxFastInit ARG_LIST ((UINT4 ));
extern INT1 nmhTestv2LldpV2TxCreditMax ARG_LIST ((UINT4 * ,UINT4)); 
extern INT1 nmhTestv2LldpV2MessageFastTx ARG_LIST ((UINT4 * ,UINT4));
extern INT1 nmhTestv2LldpV2TxFastInit ARG_LIST ((UINT4 * ,UINT4));
extern INT1 nmhTestv2Fslldpv2DestRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));
extern INT1 nmhSetFslldpv2DestRowStatus ARG_LIST((UINT4  ,INT4 ));
extern INT1 nmhSetFslldpV2DestMacAddress ARG_LIST((UINT4  ,tMacAddr ));
extern INT1 nmhTestv2FslldpV2DestMacAddress ARG_LIST((UINT4 *  ,UINT4  ,tMacAddr ));
extern INT1 nmhTestv2Fslldpv2ConfigPortRowStatus ARG_LIST((UINT4 *  ,INT4  , tMacAddr  ,INT4 ));
extern INT1 nmhSetFslldpv2ConfigPortRowStatus ARG_LIST((INT4  , tMacAddr  ,INT4 ));
extern INT1 nmhGetLldpV2DestMacAddress PROTO ((UINT4 u4LldpV2AddressTableIndex,tMacAddr * pRetValLldpV2DestMacAddress));
extern   INT4 LldpDstMacAddrUtlRBCmpInfo PROTO ((tRBElem * pElem1, tRBElem * pElem2));
/* Low Level SET Routine for All Objects.  */

INT1
nmhSetLldpV2MessageTxInterval ARG_LIST((INT4 ));

INT1
nmhSetLldpV2MessageTxHoldMultiplier ARG_LIST((INT4 ));
#endif
extern INT1 nmhGetNextIndexDot3adAggPortTable ARG_LIST((INT4 , INT4 *));
extern INT1 nmhSetFsLldpSystemControl ARG_LIST((INT4 ));
extern INT1 nmhSetFslldpv2Version ARG_LIST((INT4 ));
extern INT1 nmhTestv2Fslldpv2Version ARG_LIST((UINT4 * ,INT4 ));
extern INT1 nmhTestv2FsLldpModuleStatus ARG_LIST((UINT4 *  ,INT4 ));
extern INT1 nmhSetFsLldpModuleStatus ARG_LIST((INT4 ));
extern INT1 nmhGetLldpV2StatsTxPortFramesTotal ARG_LIST((INT4 ,UINT4 ,UINT4 *));
extern INT1 nmhGetLldpV2StatsRxPortAgeoutsTotal ARG_LIST((INT4 ,UINT4 ,UINT4 *));
extern INT1 nmhGetLldpV2StatsRxPortFramesDiscardedTotal ARG_LIST((INT4 ,UINT4 ,UINT4 *));
extern INT1 nmhGetLldpV2StatsRxPortFramesErrors ARG_LIST((INT4 ,UINT4 ,UINT4 *));
extern INT1 nmhGetLldpV2StatsRxPortFramesTotal ARG_LIST((INT4 ,UINT4 ,UINT4 *));
extern INT1 nmhGetLldpV2StatsRxPortTLVsDiscardedTotal ARG_LIST((INT4 ,UINT4 ,UINT4 *));
extern INT1 nmhGetLldpV2StatsTxLLDPDULengthErrors ARG_LIST ((INT4 ,UINT4 ,UINT4 *));

extern INT1 nmhGetNextIndexLldpV2PortConfigTable ARG_LIST ((INT4 ,INT4 *,UINT4 ,UINT4 *));
extern INT1 nmhGetFirstIndexLldpV2PortConfigTable ARG_LIST ((INT4 *,UINT4 *));
extern INT1
nmhValidateIndexInstanceFslldpv2ConfigPortMapTable PROTO ((INT4
                                                    i4Fslldpv2ConfigPortMapIfIndex,
                                                    tMacAddr
                                                    Fslldpv2ConfigPortMapDestMacAddress));
extern INT1
nmhGetLldpConfigManAddrPortsTxEnable PROTO ((INT4 i4LldpLocManAddrSubtype,
                                      tSNMP_OCTET_STRING_TYPE * pLldpLocManAddr,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pRetValLldpConfigManAddrPortsTxEnable));
extern INT1
nmhTestv2LldpConfigManAddrPortsTxEnable PROTO ((UINT4 *pu4ErrorCode,
                                         INT4 i4LldpLocManAddrSubtype,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pLldpLocManAddr,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pTestValLldpConfigManAddrPortsTxEnable));
extern INT1
nmhSetLldpConfigManAddrPortsTxEnable PROTO ((INT4 i4LldpLocManAddrSubtype,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pLldpLocManAddr,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pSetValLldpConfigManAddrPortsTxEnable));
extern INT1
nmhTestv2LldpV2PortConfigTLVsTxEnable PROTO ((UINT4 *pu4ErrorCode,
                                       INT4 i4LldpV2PortConfigIfIndex,
                                       UINT4 u4LldpV2PortConfigDestAddressIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pTestValLldpV2PortConfigTLVsTxEnable));
extern INT1 nmhSetLldpV2PortConfigTLVsTxEnable PROTO ((INT4 i4LldpV2PortConfigIfIndex,
                                    UINT4 u4LldpV2PortConfigDestAddressIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pSetValLldpV2PortConfigTLVsTxEnable));
extern INT4
LldpGetDestAddrTblIndexFromMacAddr PROTO ((tMacAddr * pMacAddr,
                                    UINT4 *pu4DstMacAddrTblIndex));
extern INT1
nmhTestv2LldpV2Xdot1ConfigPortVlanTxEnable PROTO ((UINT4 *pu4ErrorCode,
                                            INT4 i4LldpV2PortConfigIfIndex,
                                            UINT4
                                            u4LldpV2PortConfigDestAddressIndex,
                                            INT4
                                            i4TestValLldpV2Xdot1ConfigPortVlanTxEnable));
extern INT1
nmhSetLldpV2Xdot1ConfigPortVlanTxEnable PROTO ((INT4 i4LldpV2PortConfigIfIndex,
                                         UINT4
                                         u4LldpV2PortConfigDestAddressIndex,
                                         INT4
                                         i4SetValLldpV2Xdot1ConfigPortVlanTxEnable));
extern INT1
nmhGetFirstIndexLldpXdot1ConfigProtoVlanTable PROTO ((INT4 *pi4LldpLocPortNum,
                                               INT4 *pi4LldpXdot1LocProtoVlanId));
extern INT1
nmhTestv2LldpXdot1ConfigProtoVlanTxEnable PROTO ((UINT4 *pu4ErrorCode,
                                           INT4 i4LldpLocPortNum,
                                           INT4 i4LldpXdot1LocProtoVlanId,
                                           INT4
                                           i4TestValLldpXdot1ConfigProtoVlanTxEnable));
extern INT1
nmhSetLldpXdot1ConfigProtoVlanTxEnable PROTO ((INT4 i4LldpLocPortNum,
                                        INT4 i4LldpXdot1LocProtoVlanId,
                                        INT4
                                        i4SetValLldpXdot1ConfigProtoVlanTxEnable));
extern INT1
nmhGetNextIndexLldpXdot1ConfigProtoVlanTable PROTO ((INT4 i4LldpLocPortNum,
                                              INT4 *pi4NextLldpLocPortNum,
                                              INT4 i4LldpXdot1LocProtoVlanId,
                                              INT4
                                              *pi4NextLldpXdot1LocProtoVlanId));
extern INT1
nmhTestv2LldpV2Xdot1ConfigProtoVlanTxEnable PROTO ((UINT4 *pu4ErrorCode,
                                             INT4 i4LldpV2LocPortIfIndex,
                                             INT4 i4LldpV2Xdot1LocProtoVlanId,
                                             INT4
                                             i4TestValLldpV2Xdot1ConfigProtoVlanTxEnable));
extern INT1
nmhSetLldpV2Xdot1ConfigProtoVlanTxEnable PROTO ((INT4 i4LldpV2LocPortIfIndex,
                                          INT4 i4LldpV2Xdot1LocProtoVlanId,
                                          INT4
                                          i4SetValLldpV2Xdot1ConfigProtoVlanTxEnable));
extern INT1
nmhGetFirstIndexLldpXdot1ConfigVlanNameTable PROTO ((INT4 *pi4LldpLocPortNum,
                                              INT4 *pi4LldpXdot1LocVlanId));
extern INT4
LldpVlndbGetVlanName PROTO ((UINT4 u4LldpLocPortNum, UINT2 u2LldpXdot1LocVlanId,
                      UINT1 *pu1VlanName));
extern INT1
nmhTestv2LldpXdot1ConfigVlanNameTxEnable PROTO ((UINT4 *pu4ErrorCode,
                                          INT4 i4LldpLocPortNum,
                                          INT4 i4LldpXdot1LocVlanId,
                                          INT4
                                          i4TestValLldpXdot1ConfigVlanNameTxEnable));
extern INT1
nmhSetLldpXdot1ConfigVlanNameTxEnable PROTO ((INT4 i4LldpLocPortNum,
                                       INT4 i4LldpXdot1LocVlanId,
                                       INT4
                                       i4SetValLldpXdot1ConfigVlanNameTxEnable));
extern INT1
nmhGetNextIndexLldpXdot1ConfigVlanNameTable PROTO ((INT4 i4LldpLocPortNum,
                                             INT4 *pi4NextLldpLocPortNum,
                                             INT4 i4LldpXdot1LocVlanId,
                                             INT4 *pi4NextLldpXdot1LocVlanId));
extern INT4
LldpUtlGetLocalPort PROTO ((INT4 i4IfIndex, tMacAddr DestMacAddr, UINT4 *pu4LocalPort));
extern INT4
LldpTxUtlHandleLocPortInfoChg PROTO ((tLldpLocPortInfo * pPortInfo));
extern INT1
nmhTestv2LldpV2Xdot1ConfigVlanNameTxEnable PROTO ((UINT4 *pu4ErrorCode,
                                            INT4 i4LldpV2LocPortIfIndex,
                                            INT4 i4LldpV2Xdot1LocVlanId,
                                            INT4
                                            i4TestValLldpV2Xdot1ConfigVlanNameTxEnable));
extern INT1
nmhSetLldpV2Xdot1ConfigVlanNameTxEnable PROTO ((INT4 i4LldpV2LocPortIfIndex,
                                         INT4 i4LldpV2Xdot1LocVlanId,
                                         INT4
                                         i4SetValLldpV2Xdot1ConfigVlanNameTxEnable));
extern INT1
nmhTestv2LldpV2Xdot1ConfigVidUsageDigestTxEnable PROTO ((UINT4 *pu4ErrorCode,
                                                  INT4 i4LldpV2LocPortIfIndex,
                                                  INT4
                                                  i4TestValLldpV2Xdot1ConfigVidUsageDigestTxEnable));
extern INT1
nmhSetLldpV2Xdot1ConfigVidUsageDigestTxEnable PROTO ((INT4 i4LldpV2LocPortIfIndex,
                                               INT4
                                               i4SetValLldpV2Xdot1ConfigVidUsageDigestTxEnable));
extern INT1
nmhTestv2LldpV2Xdot1ConfigManVidTxEnable PROTO ((UINT4 *pu4ErrorCode,
                                          INT4 i4LldpV2LocPortIfIndex,
                                          INT4
                                          i4TestValLldpV2Xdot1ConfigManVidTxEnable));
extern INT1
nmhSetLldpV2Xdot1ConfigManVidTxEnable PROTO ((INT4 i4LldpV2LocPortIfIndex,
                                       INT4
                                       i4SetValLldpV2Xdot1ConfigManVidTxEnable));
extern INT1
nmhTestv2LldpXdot3PortConfigTLVsTxEnable PROTO ((UINT4 *pu4ErrorCode,
                                          INT4 i4LldpPortConfigPortNum,
                                          tSNMP_OCTET_STRING_TYPE
                                          *
                                          pTestValLldpXdot3PortConfigTLVsTxEnable));
extern INT1
nmhGetLldpXdot3PortConfigTLVsTxEnable PROTO ((INT4 i4LldpPortConfigPortNum,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pRetValLldpXdot3PortConfigTLVsTxEnable));
extern INT1
nmhSetLldpXdot3PortConfigTLVsTxEnable PROTO ((INT4 i4LldpPortConfigPortNum,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pSetValLldpXdot3PortConfigTLVsTxEnable));
extern INT1
nmhGetLldpV2PortConfigTLVsTxEnable PROTO ((INT4 i4LldpV2PortConfigIfIndex,
                                    UINT4 u4LldpV2PortConfigDestAddressIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValLldpV2PortConfigTLVsTxEnable));
extern INT1
nmhGetLldpV2Xdot3PortConfigTLVsTxEnable PROTO ((INT4 i4LldpV2PortConfigIfIndex,
                                         UINT4
                                         u4LldpV2PortConfigDestAddressIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValLldpV2Xdot3PortConfigTLVsTxEnable));
extern INT1
nmhGetLldpV2Xdot1ConfigPortVlanTxEnable PROTO ((INT4 i4LldpV2PortConfigIfIndex,
                                         UINT4
                                         u4LldpV2PortConfigDestAddressIndex,
                                         INT4
                                         *pi4RetValLldpV2Xdot1ConfigPortVlanTxEnable));
extern INT1
nmhGetLldpV2Xdot1ConfigVidUsageDigestTxEnable PROTO ((INT4 i4LldpV2LocPortIfIndex,
                                               INT4
                                               *pi4RetValLldpV2Xdot1ConfigVidUsageDigestTxEnable));
extern INT1
nmhGetLldpV2Xdot1ConfigManVidTxEnable PROTO ((INT4 i4LldpV2LocPortIfIndex,
                                       INT4
                                       *pi4RetValLldpV2Xdot1ConfigManVidTxEnable));
extern INT1
nmhGetFirstIndexLldpV2LocManAddrTable PROTO ((INT4 *pi4LldpV2LocManAddrSubtype,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pLldpV2LocManAddr));
extern INT1
nmhGetLldpV2ManAddrConfigTxEnable PROTO ((INT4 i4LldpV2ManAddrConfigIfIndex,
                                   UINT4 u4LldpV2ManAddrConfigDestAddressIndex,
                                   INT4 i4LldpV2ManAddrConfigLocManAddrSubtype,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pLldpV2ManAddrConfigLocManAddr,
                                   INT4 *pi4RetValLldpV2ManAddrConfigTxEnable));
extern UINT4
LldpV1DestMacAddrIndex PROTO ((INT4 i4IfIndex));

extern INT1
nmhGetNextIndexLldpV2LocManAddrTable PROTO ((INT4 i4LldpV2LocManAddrSubtype,
                                      INT4 *pi4NextLldpV2LocManAddrSubtype,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pLldpV2LocManAddr,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNextLldpV2LocManAddr));
extern INT1
nmhGetFirstIndexLldpV2Xdot1LocProtoVlanTable PROTO ((INT4 *pi4LldpV2LocPortIfIndex,
                                              UINT4
                                              *pu4LldpV2Xdot1LocProtoVlanId));
extern INT1
nmhGetLldpV2Xdot1ConfigProtoVlanTxEnable PROTO ((INT4 i4LldpV2LocPortIfIndex,
                                          INT4 i4LldpV2Xdot1LocProtoVlanId,
                                          INT4
                                          *pi4RetValLldpV2Xdot1ConfigProtoVlanTxEnable));
extern INT1
nmhGetNextIndexLldpV2Xdot1LocProtoVlanTable PROTO ((INT4 i4LldpV2LocPortIfIndex,
                                             INT4 *pi4NextLldpV2LocPortIfIndex,
                                             UINT4 u4LldpV2Xdot1LocProtoVlanId,
                                             UINT4
                                             *pu4NextLldpV2Xdot1LocProtoVlanId));
extern INT1
nmhGetFirstIndexLldpV2Xdot1ConfigVlanNameTable PROTO ((INT4 *pi4LldpV2LocPortIfIndex,
                                                INT4 *pi4LldpV2Xdot1LocVlanId));
extern INT1
nmhGetLldpV2Xdot1LocVlanName PROTO ((INT4 i4LldpV2LocPortIfIndex,
                              INT4 i4LldpV2Xdot1LocVlanId,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValLldpV2Xdot1LocVlanName));
extern INT1
nmhGetLldpV2Xdot1ConfigVlanNameTxEnable PROTO ((INT4 i4LldpV2LocPortIfIndex,
                                         INT4 i4LldpV2Xdot1LocVlanId,
                                         INT4
                                         *pi4RetValLldpV2Xdot1ConfigVlanNameTxEnable));
extern INT1
nmhGetNextIndexLldpV2Xdot1ConfigVlanNameTable PROTO ((INT4 i4LldpV2LocPortIfIndex,
                                               INT4
                                               *pi4NextLldpV2LocPortIfIndex,
                                               INT4 i4LldpV2Xdot1LocVlanId,
                                               INT4
                                               *pi4NextLldpV2Xdot1LocVlanId));
#endif
