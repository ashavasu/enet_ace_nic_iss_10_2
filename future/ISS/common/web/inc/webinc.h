/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: webinc.h,v 1.14 2015/12/14 11:14:19 siva Exp $
 *
 * Description: Header files for ISS web Module
 *******************************************************************/
#ifndef __WEBINC_H__
#define __WEBINC_H__

#ifdef BGP_WANTED
#include "stdbgpcli.h"
#include "bgp4cli.h"
#endif /* BGP_WANTED */

#ifdef ISIS_WANTED
#include "iscli.h"
#endif

#ifdef DHCP_SRV_WANTED
#include "fsdhcpscli.h"
#endif

#ifdef DHCPC_WANTED
#include "fsdhclcli.h"
#endif

#ifdef DHCP_RLY_WANTED
#include "fsdhcprelaycli.h"
#endif

#ifdef DVMRP_WANTED
#include "fsdvmrcli.h"
#endif

#ifdef ELMI_WANTED
#include "fselmicli.h"
#endif

#ifdef EOAM_WANTED
#include "fseoamcli.h"
#include "stdeoacli.h"
#include "fsfmcli.h"
#endif

#ifdef IGMP_WANTED
#include "fsigmpcli.h"
#include "stdigmcli.h"
#endif

#ifdef IGMPPRXY_WANTED
#include "fsigpcli.h"
#endif

#ifdef LLDP_WANTED
#include "fslldpcli.h"
#include "stdlldcli.h"
#include "stdot1cli.h"
#include "stdot3cli.h"
#endif

#ifdef MBSM_WANTED
#include "fsmbsmcli.h"
#endif

#ifdef ECFM_WANTED
#include "fsmiy1cli.h"
#endif

#if defined (PIM_WANTED) || defined (PIMV6_WANTED)
#include "stdpimcli.h"
#include "fspimccli.h"
#include "fspimcli.h"
#include "fspimscli.h"
#endif

#if  defined (IP_WANTED) || defined(LNXIP4_WANTED)
#include "fspingcli.h"
#include "fsrmapcli.h"
#endif

#ifdef PNAC_WANTED
#include "fspnaccli.h"
#include "stdpnacli.h"
#endif

#ifdef POE_WANTED
#include "stdpoecli.h"
#include "fspoecli.h"
#endif

#ifdef QOSX_WANTED
#include "fsqosxcli.h"
#include "stdqoscli.h"
#endif

#ifdef RADIUS_WANTED
#include "fsradicli.h"
#include "radacccli.h"
#include "radautcli.h"
#endif

#ifdef RIP6_WANTED
#include "fsrip6cli.h"
#endif

#ifdef RIP_WANTED
#include "fsripcli.h"
#include "stdripcli.h"
#include "fsmistdripcli.h"
#include "fsmiricli.h"
#endif

#ifdef RM_WANTED
#include "fsrmcli.h"
#endif

#ifdef RMON_WANTED
#include "fsrmoncli.h"
#include "stdrmocli.h"
#endif

#ifdef SNMP_3_WANTED
#include "fssnmpcli.h"
#endif

#if defined (IGS_WANTED) || defined (MLDS_WANTED)
#include "fssnpcli.h"
#endif

#ifdef WEBNM_WANTED
#include "fshttpcli.h"
#endif

#ifdef SSH_WANTED
#include "fssshmcli.h"
#endif

#ifdef SSL_WANTED
#include "fssslcli.h"
#endif

#ifdef SYSLOG_WANTED
#include "fssyslcli.h"
#endif

#ifdef TACACS_WANTED
#include "fstacacli.h"
#endif

#ifdef TAC_WANTED
#include "fstaccli.h"
#endif

#ifdef TCP_WANTED
#include "fstcpcli.h"
#include "stdtcpcli.h"
#endif

#ifdef VCM_WANTED
#include "fsvcmcli.h"
#endif

#ifdef WPS_WANTED
#include "wpscli.h"
#endif

#ifdef VRRP_WANTED
#include "fsvrrpcli.h"
#include "stdvrrcli.h"
#endif

#ifdef MFWD_WANTED
#include "mfwdcmcli.h"
#include "fsmfwdcli.h"
#endif

#ifdef OSPF3_WANTED
#include "ospf3cli.h"
#include "fso3tecli.h"
#include "fsos3cli.h"
#endif

#ifdef LA_WANTED
#include "fslacli.h"
#include "stdlacli.h"
#endif

#ifdef CFA_WANTED
#include "fstunlcli.h"
#include "fscfacli.h"
#include "ifmibcli.h"
#include "stdethcli.h"
#include "stdmaucli.h"
#endif

#ifdef MPLS_WANTED
#include "stdmplcli.h"
#include "stdtecli.h"
#include "stdpwmcli.h"
#include "stdmpfcli.h"
#include "stdldpcli.h"
#include "stdldacli.h"
#include "stdgnlcli.h"
#include "fsrsvpcli.h"
#include "fsmpfrcli.h"
#include "fsmplscli.h"
#include "stdpwcli.h"
#include "stdpwecli.h"
#endif

#ifdef IPSECv6_WANTED
#include "fssecvcli.h"
#endif

#ifdef OSPFTE_WANTED
#include "fsoteacli.h"
#include "fsotecli.h"
#include "fsotercli.h"
#endif

#ifdef IP6_WANTED
#include "fsmldcli.h"
#include "fsrtm6cli.h"
#include "stdmldcli.h"
#include "stdipvcli.h"
#endif

#ifdef RRD_WANTED
#include "fsrtmcli.h"
#endif

#ifdef SNMP_3_WANTED
#include "stdsnccli.h"
#include "stdsntcli.h"
#include "stdsnpcli.h"
#include "stdsnncli.h"
#include "stdsnucli.h"
#include "stdvaccli.h"
#endif

#ifdef SNMP_2_WANTED
#include "stdsnmpcli.h"
#endif

#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
#include "fsipcli.h"
#endif


#ifdef DIFFSRV_WANTED
#include "fsissdcli.h"
#endif

#ifdef RADIUS_WANTED
#include "utilipvx.h"
#include "radius.h"
#include "radcli.h"
#endif

#endif
